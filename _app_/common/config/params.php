<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	'WebSocketServer-host' => 'localhost',
	'WebSocketServer-port' => 8001
];
