<?php
return [
	'vendorPath' => dirname(dirname(dirname(__DIR__))) . '/protected/vendor',
	'language' => 'ru_RU',
	'components' => [
		'log' => [
			'logger' => Yii::createObject('yii\log\Logger'),
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
	]
];
