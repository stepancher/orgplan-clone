<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

$config = [
	'id' => 'app-frontend',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'controllerNamespace' => 'frontend\controllers',
	'components' => [
		'request' => [
			'baseUrl' => '',
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'baseUrl' => '',
			'rules' => [
				'site/page/<view:\w+>' => 'site/page'
			]
		],
		'formatter' => [
			'class' => 'frontend\components\Formatter',
			'dateFormat' => 'php:d.m.Y',
			'datetimeFormat' => 'php:d.m.Y H:i:s',
			'timeFormat' => 'php:H:i:s',
		],

	],
	'params' => $params,
];
return array_merge_recursive(
	require(__DIR__ . '/../../common/config/main.php'),
	require(__DIR__ . '/../../common/config/main-local.php'),
	$config,
	require(__DIR__ . '/main-local.php')
);
