<?php
/**
 * Site
 * Точка входа, обрабатывает входящие запросы
 */
require(__DIR__ . '/protected/config/_const.php');
if (YII_DEBUG) {
    // Включение вывода ошибок
    ini_set('display_errors', 1);
    error_reporting(-1);
}


require(__DIR__ . '/protected/components/Pinba.php');


/**
 * Подключем приложение
 */
require(__DIR__ . '/protected/components/Yii.php');
require(__DIR__ . '/protected/vendor/autoload.php');

// configuration for Yii 2 application
$yii2Config = require(__DIR__ . '/_app_/frontend/config/main.php');
new yii\web\Application($yii2Config); // Do NOT call run()

// configuration for Yii 1 application
$yii1Config = require(__DIR__ . '/protected/config/web.php');

if (XHPROF) {
    $utils_path = "/usr/share/php5-xhprof/xhprof_lib/utils/";
    include_once $utils_path . 'xhprof_lib.php';
    include_once $utils_path . 'xhprof_runs.php';
    xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
    ob_start(function ($buffer) {
        $profiler_namespace = 'lsp';
        header('XHProf-report: ' . sprintf('http://' . $_SERVER['SERVER_NAME'] . '/xhprof/index.php?run=%s&source=%s', Yii::app()->user->getState('xhprof_id'), $profiler_namespace));
        return $buffer;
    });
}

/** @var CWebApplication $app */
$app = Yii::createWebApplication($yii1Config);
try {
    $app->run();
} catch (CHttpException $e) {

    throw $e;
}

if (XHPROF) {
    $profiler_namespace = 'lsp';
    $xhprof_data = xhprof_disable();
    $xhprof_runs = new XHProfRuns_Default();
    $run_id = $xhprof_runs->save_run($xhprof_data, $profiler_namespace);
    Yii::app()->user->setState('xhprof_id', $run_id);
    if (!$app->request->isAjaxRequest && !$app->user->isGuest && $app->user->id == 2) {
        $profiler_url = sprintf('http://' . $_SERVER['SERVER_NAME'] . '/xhprof/index.php?run=%s&source=%s', $run_id, $profiler_namespace);
//        echo '<a class="xhprof" style="position:fixed; bottom:5px; left:15px; color: red !important; z-index: 999999" href="' . $profiler_url . '" target="_blank">XHProf</a>';
    }
    $t = Pinba::start(array("category"=>"xhprof","group"=>"xhprof::id","xhprof_id"=>$run_id));
    Pinba::stop($t);
}
