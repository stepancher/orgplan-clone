<?php
/**
 * @var CController $this
 * @var AR $model
 */
$ctrl = $this;
$ctrl->widget('ext.widgets.FieldSetView', array(
	'header' => Yii::t($this->getId(), 'Grid header'),
	'items' => array(
		array(
			'ext.widgets.ActionsView',
			'items' => array(
				'add' => array(
					TbHtml::BUTTON_TYPE_LINK,
					Yii::t($this->getId(), 'Grid action add'),
					array(
						'url' => $this->createUrl('save'),
						'color' => TbHtml::BUTTON_COLOR_SUCCESS
					)
				),
			)
		),
		array(
			'application.widgets.' . get_class($model) . 'GridView',
			'model' => $model,
			'columnsAppend' => array(
				'buttons' => array(
					'class' => 'bootstrap.widgets.TbButtonColumn',
					'template' => '<nobr>{show}&#160;{edit}</nobr>',
					'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
					'buttons' => array(
						'show' => array(
							'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
							'url' => function ($data) use ($ctrl) {
									return $ctrl->createUrl('view', array("id" => $data->id));
								},
							'options' => array('title' => 'Информация', 'class' => 'btn btn-small btn-success'),
						),
						'edit' => array(
							'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
							'url' => function ($data) use ($ctrl) {
									return $ctrl->createUrl('save', array("id" => $data->id));
								},
							'options' => array('title' => 'Редактирование', 'class' => 'btn btn-small btn-primary'),
						),
					)
				)
			)
		),
	)
));
