<?php
/**
 * @var CController $this
 * @var AR $model
 */
$formClass = get_class($model) . 'Form';
Yii::import('application.widgets.'.$formClass);

$this->widget('ext.widgets.FieldSetView', array(
	'header' => Yii::t($this->getId(), $model->isNewRecord ? 'Form header create' : 'Form header update'),
	'items' => array(
		array(
			'ext.widgets.FormView',
			'form' => new $formClass(
					array(
						'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
						//'elementGroupName' => '[]'
					),
					$model, $this
				),
			'items' => array(
				array(
					'ext.widgets.ActionsView',
					'items' => array(
						'submit' => array(
							'type' => TbHtml::BUTTON_TYPE_SUBMIT,
							'label' => Yii::t($this->getId(), 'Form action save'),
							'attributes' => array(
								'color' => TbHtml::BUTTON_COLOR_SUCCESS
							)
						),
						'cancel' => array(
							'type' => TbHtml::BUTTON_TYPE_LINK,
							'label' => Yii::t($this->getId(), 'Form action cancel'),
							'attributes' => array(
								'url' => $this->createUrl('index'),
								'color' => TbHtml::BUTTON_COLOR_DANGER
							)
						)
					)
				)
			)

		),
	)
));