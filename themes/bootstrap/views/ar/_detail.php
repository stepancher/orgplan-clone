<?php
/**
 * @var CController $this
 * @var AR $model
 */
$this->widget(
	'ext.widgets.FieldSetView',
	array(
		'header' => Yii::t($this->getId(), 'Detail header'),
		'items' => array(
			array(
				'ext.widgets.ActionsView',
				'items' => array(
					'cancel' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t($this->getId(), 'Detail action cancel'),
						'attributes' => array(
							'url' => $this->createUrl('index'),
							'color' => TbHtml::BUTTON_COLOR_WARNING
						)
					),
					'edit' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t($this->getId(), 'Detail action edit'),
						'attributes' => array(
							'url' => $this->createUrl('save', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_PRIMARY
						)
					)
				)
			),
			array(
				'application.widgets.' . get_class($model) . 'DetailView',
				'data' => $model,
			),
		)
	)
);