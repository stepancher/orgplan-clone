<?php
/**
 * @var $this CController
 * @var $publishUrl string
 */
?>
<div class="row-fluid modal__modal-content">
    <div class="terms-of-use recover-success"></div>
    <img alt="Загрузка" title="Загрузка" class="ajax-loader js-ajax-loader" src="<?= $publishUrl ?>\img\loader\ajax-loader.gif">

    <div class="modal-content__login">
        <div class="modal-tariff-desc-1">
            Эта услуга без выбора тарифа не доступна.
        </div>
        <div class="modal-tariff-desc-2">
            Необходимо перейти в профиль и подключить тариф TEST.
            Это бесплатно. Вам станут доступны многие функции.
        </div>
        <div class="go-to-tariffs">
            <?php
                echo TbHtml::linkButton('Перейти', [
                    'id' => 'go-to',
                    'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize',
                    'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200)',
                    'url' => Yii::app()->createUrl("profile/default", ["to" => Yii::app()->getRequest()->requestUri, "tab" => "tariffs"]),
                ]);
            ?>
        </div>
    </div>
</div>