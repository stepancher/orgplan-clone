<div class="cols-row header">
    <div class="col-md-12">
        <h1>
            <?= !empty($header) ? $header : ''; ?>
            <?php if (!empty($description)): ?>
                <?php if (!is_array($description)) {
                    $description = [$description];
                } ?>
                <?php foreach ($description as $descriptionItem): ?>
                    <span class="slash"> / </span>
                    <span class="description"><?= $descriptionItem ?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </h1>
    </div>
</div>