<?php
/**
 * @var $this CController
 * @var $publishUrl string
 */
?>
<div class="row-fluid modal__modal-content mini-registration">
    <img alt="Загрузка" title="Загрузка" class="ajax-loader js-ajax-loader"
         src="<?= $publishUrl ?>\img\loader\ajax-loader.gif">

    <div class="social-links-block">
        <?php
        $this->widget('ZAuthWidget', array('action' => '/auth/default/socialAuth'));
        ?>
    </div>
    <?php
    $model = new User('registration');

    Yii::import('application.modules.profile.components.LoginForm');
    $this->widget('ext.widgets.FormView', array(
        'form' => new LoginForm(
            array(
                'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
                'action' => Yii::app()->createUrl('auth/default/registration'),
                'activeForm' => array(
                    'class' => 'TbActiveForm',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnType' => false,
                        'validateOnChange' => false,
                        'afterValidate' => 'js:function(form, data, hasError){
                            if(!hasError) {
                                try {
                                    yaCounter31914033.reachGoal("registracia");
                                    ga("tracker.send", "event", "registration_modal", "click_on_submit");
                                } catch (e) {
                                    console.log(e);
                                    if (Raven != undefined) {
                                        Raven.config("http://6c0aa533332643918320060f986ea813@sentry.orgplan.ru/4").install();
                                        Raven.captureException(e);
                                    }
                                }
                                
                                window.location.reload("site/index");
                            } else {
                                $(".modal__modal-content .js-ajax-loader").fadeOut(200);
                            }
                        }'
                    ),
                ),
            ),
            $model, $this
        ),
        'htmlOptions' => array(),
    ));
    ?>
</div>