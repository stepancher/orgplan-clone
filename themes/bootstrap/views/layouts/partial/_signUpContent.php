<?php
	/**
	 * @var $this CController
	 * @var $publishUrl string
	 */
?>
<div class="row-fluid modal__modal-content">
	<div class="terms-of-use recover-success"></div>
	<img alt="Загрузка" title="Загрузка" class="ajax-loader js-ajax-loader" src="<?= $publishUrl ?>\img\loader\ajax-loader.gif">

	<div class="social-links-block" style="display: none">
		<?php
		$this->widget('ZAuthWidget', array('action' => '/auth/default/socialAuth'));
		?>
	</div>
	<div class="modal-content__login">
		<?php
		$linkRecover = CHtml::link(Yii::t('site', 'Forgot your password?'), 'javascript://', array('onclick' => 'toggleSignUpContent()'));
		$model = new User('login');
		Yii::import('application.modules.profile.components.LoginForm');
		$this->widget('ext.widgets.FormView', array(
			'form' => new LoginForm(
				array(
					'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
					'action' => Yii::app()->createUrl('auth/default/login'),
					'activeForm' => array(
						'class' => 'TbActiveForm',
						'enableAjaxValidation' => true,
						'enableClientValidation' => false,
						'clientOptions' => array(
							'validateOnSubmit' => true,
							'validateOnType' => false,
							'validateOnChange' => false,
							'afterValidate' => 'js:function(form, data, hasError){
								if(!hasError) {
									if(typeof data.matches !== "undefined" && data.matches) {
										$("#match-form").submit();
									}
									if (typeof data.redirectUrl !== "undefined" && data.redirectUrl){
										window.location = data.redirectUrl;
									} else {
										window.location.reload();
									}
								}
								$(".modal__modal-content .js-ajax-loader").fadeOut(200)
							}'
						),
					),
				),
				$model, $this
			),
			'htmlOptions' => array(),
			//'partialMode' => false,
		));
		?>
	</div>
	<div class="modal-content__recover-password hidden">
		<?php
		$model = new Contact('recoverPassword');
		Yii::import('application.modules.profile.components.RecoverPasswordForm');
		$this->widget('ext.widgets.FormView', array(
			'form' => new RecoverPasswordForm(
				array(
					'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
					'action' => Yii::app()->createUrl('auth/default/recoverPassword'),
					'activeForm' => array(
						'class' => 'TbActiveForm',
						'enableAjaxValidation' => true,
						'enableClientValidation' => false,
						'clientOptions' => array(
							'validateOnSubmit' => true,
							'validateOnType' => false,
							'validateOnChange' => false,
							'afterValidate' => 'js:function(form, data, hasError){
								if(!hasError) {
									$(".recover-success").text("' . Yii::t('site', 'Letter sent to the specified email.') . '");
									toggleSignUpContent();
								}
								$(".modal__modal-content .js-ajax-loader").fadeOut(200)
							}'
						),
					),
				),
				$model, $this
			),
			'htmlOptions' => array(),
			//'partialMode' => false,
		));
		?>

	</div>
</div>
<script type="application/javascript">
	function toggleSignUpContent() {
		$('.modal-content__login').toggleClass('hidden');
		$('.modal-content__recover-password').toggleClass('hidden');
	}
</script>