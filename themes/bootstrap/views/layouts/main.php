<?php
/**
 * @var CWebApplication $app
 * @var ARController $this
 * @var string $content
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

$jsVersion  = md5_file($app->theme->basePath . '/assets/dist/global.js');
$cssVersion = md5_file($app->theme->basePath . '/assets/dist/global.css');

$static = $am->publish($app->extensionPath . '/static');

Yii::app()->bootstrap->register();
$cs->registerCssFile($publishUrl . '/css/style.css');
$cs->registerCssFile($publishUrl . '/css/global.css');
$cs->registerCssFile($publishUrl . '/css/inputs/style.css');
$cs->registerScriptFile($publishUrl . '/js/js-inputs/script.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/clamp/clamp.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/additionalFunctions.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/jquery.maskedinput.min.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/mask.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/history-loader.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/dist/global.js?v='.$jsVersion, CClientScript::POS_HEAD);
$cs->registerCssFile($publishUrl . '/dist/global.css?v='.$cssVersion);

$cs->registerScriptFile($publishUrl . '/js/trimming-string.js', CClientScript::POS_END);
$cs->registerScriptFile($static . '/jQuery-Russian-Hyphenation-master/jquery.hyphen.ru.min.js', CClientScript::POS_END);

$cs->registerScriptFile($publishUrl . '/js/clear-field.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/confirmEvent.js', CClientScript::POS_BEGIN);

$properties = [];
if ($app->controller->id == 'help') {
    $cs->registerScriptFile($publishUrl . '/js/custom_jquery_lib/scrollspy.js', CClientScript::POS_END);
    $properties = [
        'data-spy' => 'scroll',
        'data-target' => '.b-tabs--help-page',
        'data-selector' => '.active .b-tabs__tab > a',
        'data-offset' => '200',
    ];
}
$properties['class'] = Yii::$app->session->get('layout-collapsed') == 'true' ? 'minimize-side-bar' : '';

if (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7')) {
    $cs->registerCssFile($publishUrl . '/css/font-for-ie.css');
}

$office = 'workspace';

if (User::ROLE_ORGANIZERS == Yii::app()->user->getRole()) {
    $office = 'backoffice';
}

$isExponent = Yii::app()->user->role == User::ROLE_EXPONENT;
$isOrganizer = Yii::app()->user->role == User::ROLE_ORGANIZERS;
$isAdmin = Yii::app()->user->GetState('role') == User::ROLE_ADMIN;

if (($isAdmin && explode('/', $_SERVER["REQUEST_URI"])[2] == 'admin') || $_SERVER["REQUEST_URI"] == '/ru/profile/default/index/?tab=control') {
    $isDashboard = true;
} else {
    $isDashboard = false;
}

$fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId']);
$proposalVersion = '';
if(!empty($fair->proposalVersion)){
    $proposalVersion = '/'.$fair->proposalVersion;
}

?>
<!DOCTYPE html>
<html lang="en" class="-webkit-">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <META http-equiv="X-UA-Compatible" content="IE=11">
    <meta name='wmail-verification' content='bb4b28284d3e19c4829a70a24fb95f27' />
    <meta name='yandex-verification' content='6d0d4f68b645c3ab' />
    <meta name="google-site-verification" content="JUqR23YuoOgnXhXwZZgy08my4kh97mh6L1hpeOvT5_c" />
    <meta name='yandex-verification' content='54ecea215fb388ac' />
    <meta name="google-site-verification" content="Y4IyZ5M1rGEyZEBYdi3CjPBZwQa6Ixm_GxW3KdR-NdU" />
    <meta name="msvalidate.01" content="93AB6A3241E35BFF516ED236B92D4035" />
    <?php if($this->module->id == 'blog' && $this->id == 'default' && $this->action->id == 'view') : ?>
        <meta property="og:site_name" content="Protoplan">
        <meta property="og:url" content="<?= $this->ogMetaUrl ?>">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?= $this->title ?>">
        <meta property="og:description" content="<?= $this->seoDescription ?>">
        <meta property="og:image" content="<?= $this->ogMetaImage ?>">
        <meta property="description" content="<?= $this->seoDescription ?>">
        <meta name="title" content="<?= $this->title ?>">
    <?php endif; ?>
    <title><?= CHtml::decode(strip_tags($this->pageTitle)); ?></title>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="language" content="<?= CHtml::encode($this->language); ?>"/>
    <?php endif; ?>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="description" content="<?= str_replace('"', '&quot;', strip_tags($this->description)); ?>"/>
    <?php endif; ?>
    <link rel="shortcut icon" href="<?= $app->request->baseUrl . '/favicon.ico'; ?>">

    <?php if ($isDashboard) { ?>
    <style>
        .container-fluid--with-left-menu {
            margin: 50px 0 0 0;
        }
    </style>
    <?php } ?>
    <script src="https://cdn.ravenjs.com/2.1.0/raven.min.js"></script>
</head>
<?= TbHtml::openTag('body', $properties) ?>

<?php if (!IS_MODE_DEV && Yii::app()->user->role != User::ROLE_ADMIN) : ?>

    <?php $this->renderPartial('//layouts/counters/_yandexMetrika');?>

    <?php if (
    (stristr($_SERVER['HTTP_HOST'], 'orgplan.pro') || stristr($_SERVER['HTTP_HOST'], 'protoplan.pro') || stristr($_SERVER['HTTP_HOST'], 'orgplan.ru')) && !$isDashboard
    ) { ?>

        <?php $this->renderPartial('//layouts/counters/_jivoSite');?>

        <?php $this->renderPartial('//layouts/counters/_carriotQuest');?>

    <?php } ?>

        <?php $this->renderPartial('//layouts/counters/_ratingMailRu');?>

        <?php $this->renderPartial('//layouts/counters/_googleTagManager');?>

        <?php $this->renderPartial('//layouts/counters/_ramblerTop100');?>

<?php endif; ?>

<a href="#" class="scroll-up">Наверх</a>
<script language="javascript">
    /* IE11 Fix for SP2010 */
    if (typeof(UserAgentInfo) != 'undefined' && !window.addEventListener) {
        UserAgentInfo.strBrowser = 1;
    }
</script>
<!-- Navbar -->
<?php

$htmlOptions = array();
if(!User::checkTariff(1)){
    $htmlOptions = array(
        'data-toggle' => 'modal',
        'data-target' => '#modal-tariff-insufficient',
    );
}

if(Yii::app()->user->isGuest){
    $htmlOptions = array(
        'data-toggle' => 'modal',
        'data-target' => '#modal-sign-up',
    );
}

Yii::import('application.modules.blog.BlogModule');
Yii::import('application.modules.RUEF.RUEFModule');
$this->widget(
    'bootstrap.widgets.TbNavbar',
    array(
        'display' => TbHtml::NAVBAR_DISPLAY_STATICTOP,
        'htmlOptions' => array(
            'class' => 'nav-bar--main-menu',
            'style' => 'min-width: 1030px;',
        ),
        'brandLabel' => IS_MODE_DEV || YII_DEBUG
            ? (
                (IS_MODE_DEV ? '- DEV -' : '') .
                (YII_DEBUG ? '- DEBUG - ' : '')
            )
            : '',
        'brandUrl' => '/landing/'.Yii::app()->language.'/index.html'/*(isset(Yii::$app->session['myFairViewId']) && !$app->user->isGuest)
            ? Yii::app()->createUrl($office . '/panel')
            : $this->createAbsoluteUrl('/')*/,
        'brandOptions' => [
            'title' => 'Протоплан'
        ],
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbNav', // Главное меню
                'encodeLabel' => false,
                'items' => array(
                    array(
                        'label' => Yii::t('search', 'Fairs catalog'),
                        'url' => $app->createUrl('/catalog'),
                    ),
                    array(
                        'label' => '<div class="menu-bar menu-bar__item--search">
                                        <form action="' . $app->createUrl("/search", array("sector" => "fair")) . '">
                                            <input type="text" name="query" autocomplete="off" placeholder="'.Yii::t('system', 'Search').'"  />
                                            <input type="submit" value="&#xe3bc;"/>
                                        </form>
                                    </div>',
                    ),
                )
            ),
            array(
                'class' => 'bootstrap.widgets.TbNav', // Главное меню
                'encodeLabel' => false,
                'htmlOptions' => array(
                    'class' => 'pull-right header-menu',
                ),
                'items' => array_filter(
                    array(
                        array(
                            'label' => Yii::t('BlogModule.blog','Blog'),
                            'url' => $app->createUrl('blog/'),
                            'htmlOptions' => array(
                                'class' => 'header-menu__item',
                            ),
                        ),
                        array(
                            'label' => Yii::t('system','Help'),
                            'url' => $app->createUrl('/help'),
                            'htmlOptions' => array(
                                'class' => 'header-menu__item',
                            ),
                        ),
                        array(
                            'label' => '|',
                        ),
                        array(
                            'label' => Yii::t('system', 'Registration'),
                            'url' => 'javascript://',
                            'visible' => $app->user->isGuest,
                            'htmlOptions' => array(
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-registration',
                                'class' => 'header-menu__item',
                            ),
                        ),
                        IS_MODE_DEV && FALSE ? array(
                            'label' => Yii::t('user','Balance'). ' ' . User::getUserBalance(),
                            'url' => 'javascript://',
                            'visible' => !$app->user->isGuest,
                        ) : array(),
                        array(
                            'label' => Yii::t('system', Yii::app()->language),
                            'class' => 'bootstrap.widgets.TbNav header-menu__item',
                            'url' => $app->createUrl('auth/default/logout'),
                            'type' => TbHtml::NAV_TYPE_LIST,
                            'encodeLabel' => false,
                            'items' => array(
                                array(
                                    'label' => 'Russian',
                                    'url'   => Yii::app()->urlManager->switchLang(Yii::app()->request, 'ru'),
                                    'htmlOptions' => array(
                                        'class' => 'header-menu__lang__item',
                                    ),
                                ),
                                array(
                                    'label' => 'English',
                                    'url'   => Yii::app()->urlManager->switchLang(Yii::app()->request, 'en'),
                                    'htmlOptions' => array(
                                        'class' => 'header-menu__lang__item',
                                    ),
                                ),
                                array(
                                    'label' => 'Deutsch',
                                    'url'   => Yii::app()->urlManager->switchLang(Yii::app()->request, 'de'),
                                    'htmlOptions' => array(
                                        'class' => 'header-menu__lang__item',
                                    ),
                                ),
                            ),
                        ),
                        $this->widget('ext.widgets.HeaderMenuProfile', ['fairId' => !empty($fair->id)?$fair->id:NULL, 'publishUrl'  => $publishUrl, 'app' => $app])->run(),
                        array(
                            'label' => Yii::t('system', 'Log in'),
                            'url' => 'javascript://',
                            'visible' => $app->user->isGuest,
                            'htmlOptions' => array(
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-sign-up',
                                'class' => 'header-menu__item',
                            )
                        )
                    )
                )
            )
        )
    )
);

if ($app->user->isGuest) {
    $this->widget('ext.widgets.Modal', array(
        'id' => 'modal-sign-up',
        'closeText' => '<i class="icon icon--white-close"></i>',
        'htmlOptions' => array(
            'class' => 'modal--sign-up'
        ),
        'header' => $this->renderPartial('//layouts/partial/_signUpHeader', array(), true),
        'content' => $this->renderPartial('//layouts/partial/_signUpContent', array('publishUrl' => $publishUrl), true),
    ));

    $this->widget('ext.widgets.Modal', array(
        'id' => 'modal-registration',
        'closeText' => '<i class="icon icon--white-close"></i>',
        'htmlOptions' => array(
            'class' => 'modal--sign-up'
        ),
        'header' => $this->renderPartial('//layouts/partial/_registrationHeader', array(), true),
        'content' => $this->renderPartial('//layouts/partial/_registrationContent', array('publishUrl' => $publishUrl), true),
    ));
}

if(!User::checkTariff(1)){
    $this->widget('ext.widgets.Modal', array(
        'id' => 'modal-tariff-insufficient',
        'closeText' => '<i class="icon icon--white-close"></i>',
        'htmlOptions' => array(
            'class' => 'modal--sign-up'
        ),
        'header' => $this->renderPartial('//layouts/partial/_tariffInsufficientHeader', array(), true),
        'content' => $this->renderPartial('//layouts/partial/_tariffInsufficientContent', array('publishUrl' => $publishUrl), true),
    ));
    ?>
    <script type="text/javascript">
        //Скрипт необходим для обработки щелчка мыши по кнопкам,
        // и передаче url в модальное окно, которое предупреждает
        // пользователя об отсутствии тарифного плана
        $(window).ready(function(){

            $('.lock-modal').click(function(){
                var url = $(this).attr('href');
                if(url === '' || url === 0 || url === null || url === undefined || url === 'undefined'){
                    url = '/ru/profile/default/index/?to='+encodeURIComponent(window.location.href)+'&tab=tariffs';
                }
                $('#go-to').attr({'href':url});
            });

            //Клик по кнопке, которая должна будет нажаться после того как
            // пользователь выберет тариф и перейдёт опять на эту страницу
            $('.modal-back-url').click(function(){
                var url = $(this).attr('href');
                if(url === '' || url === 0 || url === null || url === undefined || url === 'undefined' || url === '#'){
                    var buttonBackId = $(this).attr('button-back-id');
                    url = '/ru/profile/default/index/?to='+encodeURIComponent(window.location.href)+'&clickButton='+buttonBackId+'&tab=tariffs';
                    if($(this).attr("data-target") == "#modal-sign-up"){
                        $(".modal-content__login").find("form").prepend('<input type="hidden" name="backUrlButton" value="'+buttonBackId+'">');
                    }
                }
                $('#go-to').attr({'href':url});

            });
        });
    </script>

    <?php
}
?>
<script type="text/javascript">
    //При возвращении пользователя со страницы выбора тарифа
    // получение ID кнопки и автоматическое её нажатие
    $(window).load(function(){

        <?php
            $backUrlId = (string)Yii::$app->session->get('clickButton', null);
            if(empty($backUrlId)){
                $backUrlId = 0;
            }else{
                Yii::$app->session->remove('clickButton');
            }
        ?>
        var backUrlButtonId = '<?=$backUrlId;?>';

        if(
            backUrlButtonId !== 0 &&
            backUrlButtonId !== false &&
            backUrlButtonId !== null &&
            backUrlButtonId !== '' &&
            backUrlButtonId !== '0'
        ){
            $( "[button-back-id='"+backUrlButtonId+"']").click();
        }
    });
</script>
<?php
$userCabinetUrl = '';
if(Yii::app()->user->role == User::ROLE_ORGANIZERS){
    $userCabinetUrl = Yii::app()->createUrl($office . '/panel');
}

if(Yii::app()->user->role == User::ROLE_EXPONENT){
    $userCabinetUrl = Yii::app()->createUrl($office . '/panel');
}

if(Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE){
    $userCabinetUrl = Yii::app()->createUrl( 'workspace/panel');
}


$cs->registerScriptFile($publishUrl . '/js/left-side/script.js');
$this->widget('bootstrap.widgets.TbNav', array(
    'type' => TbHtml::NAV_TYPE_LIST,
    'htmlOptions' => array(
        'class' => 'nav-list__side-left nav-list__side-left--open',
        'style' => $isDashboard ? 'display: none;' : '',
    ),
    'encodeLabel' => false,
    'items' => array(
        array(
            'label' => '<i class="icon icon--white-list"></i>',
            'url' => 'javascript://',
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list--minimize-button js-nav-list--minimize-button'
            )
        ),
        array(
            'label' => '<i class="icon" data-icon="&#xe19c;"></i>' . Yii::t('leftMenu', 'My fairs'),
            'url' => Yii::app()->createUrl('/myFairs'),

            'htmlOptions' => array(
                'class' => 'nav-list__item my-fairs-item-menu' . (
                    (
                        !empty(Yii::app()->controller->module->id) &&
                        (
                            Yii::app()->controller->module->id == 'workspace' ||
                            Yii::app()->controller->module->id == 'backoffice'
                        ) &&
                        (Yii::app()->controller->id == 'panel')
                    )
                        ? ' active'
                        : ''
                    ),
            ),
            'visible' => !Yii::app()->user->isGuest &&
                    ($isExponent || $isOrganizer),
        ),
        array(
            'url' => '#',
            'label' => '',
            'htmlOptions' => [
                'class' => 'protoman_' . Yii::app()->language,
            ],
            'visible' => Yii::$app->session['myFairViewId'] === NULL && Yii::app()->user->role == User::ROLE_EXPONENT && MyFair::isMyFair(184) && MyFair::isExponentFair(10943),
        ),
        array(
            'url' => '#',
            'label' => '',
            'htmlOptions' => [
                'class' => 'protoman_agrorus_' . Yii::app()->language,
            ],
            'visible' => Yii::$app->session['myFairViewId'] === NULL && Yii::app()->user->role == User::ROLE_EXPONENT && MyFair::isMyFair(11699) && MyFair::isExponentFair(10943),
        ),
        array(
            'url' => '#',
            'label' => '',
            'htmlOptions' => [
                'class' => 'protoman2'
            ],
            'visible' => Yii::$app->session['myFairViewId'] === NULL && Yii::app()->user->role == User::ROLE_EXPONENT && MyFair::isMyFair(10556), //@TODO DLE hardcode
        ),
        array(
            'label' => isset(Yii::$app->session['myFairViewId']) ? $fair->loadName() : '#',
            'linkOptions' => [
                'data-max-line' => 3,
                'data-clamping' => 2,
                'class' => 'trim-clamp',
            ],
            'url' => isset(Yii::$app->session['myFairViewId']) && ($isExponent||$isAdmin||$isOrganizer) ? $proposalVersion.Yii::app()->createUrl($office.'/panel/panel' , ['fairId' => Yii::$app->session['myFairViewId']]) : '#',
            'htmlOptions' => array(
                'class' => 'nav-list__item left-menu-fair-info left-menu-fair-name' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ? (
                            ' active-has-fair' .
                            (
                            (Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'workspace/panel' ||
                                Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'backoffice/panel'
                                && isset($_GET['fairId']) && $_GET['fairId'] == Yii::$app->session['myFairViewId'])
                                ? ' active' : ''
                            )
                        )
                        : ' hide'),
            ),
            'visible' => ($app->controller->id == 'workspace' && $app->controller->action->id == 'index')
                || isset(Yii::$app->session['myFairViewId']) || $isExponent || $isOrganizer || $isAdmin
        ),
        TbHtml::menuDivider(['class' => 'h-10 left-menu-fair-info' . (isset(Yii::$app->session['myFairViewId']) ? '' : ' hide')]),
        array(
            'label' => Yii::t('leftMenu', 'rus fairs'),
            'url' => Yii::app()->createUrl(
                '/RUEF/fairReport/report',
                array(
                    'year' => isset($_GET['year']) ? $_GET['year'] : date('Y'),
                )
            ),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item'
                . (Yii::app()->controller->id == 'fairReport' &&  Yii::app()->controller->action->id == 'report' ? ' active' : '')
            ),
            'visible' => Yii::app()->user->role == User::ROLE_RUEF || (Yii::app()->user->role == User::ROLE_ADMIN && isset(Yii::app()->controller->module) && $app->controller->module->id == 'RUEF'),
        ),
        array(
            'label' => Yii::t('leftMenu', 'rus industries'),
            'url' => Yii::app()->createUrl(
                '/RUEF/industryReport/report',
                array(
                    'year' => isset($_GET['year']) ? $_GET['year'] : date('Y'),
                )
            ),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item'
                . (Yii::app()->controller->id == 'industryReport' &&  Yii::app()->controller->action->id == 'report'  ? ' active' : '')
            ),
            'visible' => Yii::app()->user->role == User::ROLE_RUEF || (Yii::app()->user->role == User::ROLE_ADMIN && isset(Yii::app()->controller->module) && $app->controller->module->id == 'RUEF'),
        ),
        array(
            'label' => Yii::t('leftMenu', 'rus districts'),
            'url' => Yii::app()->createUrl(
                '/RUEF/districtReport/report',
                array(
                    'year' => isset($_GET['year']) ? $_GET['year'] : date('Y'),
                )
            ),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item'
                . (Yii::app()->controller->id == 'districtReport' ? ' active' : '')
            ),
            'visible' => Yii::app()->user->role == User::ROLE_RUEF || (Yii::app()->user->role == User::ROLE_ADMIN && isset(Yii::app()->controller->module) && $app->controller->module->id == 'RUEF'),
        ),
        array(
            'label' => Yii::t('leftMenu', 'rus regions'),
            'url' => Yii::app()->createUrl(
                '/RUEF/regionReport/report',
                array(
                    'year' => isset($_GET['year']) ? $_GET['year'] : date('Y'),
                )
            ),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item'
                . (Yii::app()->controller->id == 'regionReport' ? ' active' : '')
            ),
            'visible' => Yii::app()->user->role == User::ROLE_RUEF || (Yii::app()->user->role == User::ROLE_ADMIN && isset(Yii::app()->controller->module) && $app->controller->module->id == 'RUEF'),
        ),
        array(
            'label' => Yii::t('leftMenu', 'cis fairs'),
            'url' => Yii::app()->createUrl(
                '/RUEF/fairReport/CISreport',
                array(
                    'year' => isset($_GET['year']) ? $_GET['year'] : date('Y'),
                )
            ),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item'
                    . (Yii::app()->controller->id == 'fairReport' &&  Yii::app()->controller->action->id == 'CISreport' ? ' active' : '')
            ),
            'visible' => Yii::app()->user->role == User::ROLE_RUEF || (Yii::app()->user->role == User::ROLE_ADMIN && isset(Yii::app()->controller->module) && $app->controller->module->id == 'RUEF'),
        ),
        array(
            'label' => Yii::t('leftMenu', 'cis industries'),
            'url' => Yii::app()->createUrl(
                '/RUEF/industryReport/CISReport',
                array(
                    'year' => isset($_GET['year']) ? $_GET['year'] : date('Y'),
                )
            ),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item'
                    . (Yii::app()->controller->id == 'industryReport' &&  Yii::app()->controller->action->id == 'CISReport'  ? ' active' : '')
            ),
            'visible' => Yii::app()->user->role == User::ROLE_RUEF || (Yii::app()->user->role == User::ROLE_ADMIN && isset(Yii::app()->controller->module) && $app->controller->module->id == 'RUEF'),
        ),
        array(
            'label' => Yii::t('leftMenu', 'About fair'),
            'url' => isset(Yii::$app->session['myFairViewId']) ? $proposalVersion.Yii::app()->createUrl($office . '/list/view', ['id' => Yii::$app->session['myFairViewId']]) : '#',
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ? (
                        (Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'workspace/view' ||
                            Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'backoffice/view'
                            && isset($_GET['id']) && $_GET['id'] == Yii::$app->session['myFairViewId'])
                            ? ' active' : ''
                        )
                        : ' hide'),
            ),
            'visible' => ($app->controller->id == 'workspace' && $app->controller->action->id == 'index')
                || isset(Yii::$app->session['myFairViewId'])
        ),
        array(
            'label' => Yii::t('leftMenu', 'Calendar'),
            'url' => $office == 'backoffice' ?
                $proposalVersion.Yii::app()->createUrl('calendar', ['fairId' => Yii::$app->session['myFairViewId']]) :
                $proposalVersion.Yii::app()->createUrl('calendar', ['fairId' => Yii::$app->session['myFairViewId']]),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ? (
                        (Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'calendar' ||
                            Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'calendar'
                            && isset($_GET['fairId']) && $_GET['fairId'] == Yii::$app->session['myFairViewId'])
                            ? ' active' : ''
                        )
                        : ' hide'),
            ),
            'visible' => (
                !Yii::app()->user->isGuest &&
                Yii::app()->user->role != User::ROLE_USER &&
                Yii::app()->user->role != User::ROLE_EXPONENT_SIMPLE &&
                isset(Yii::$app->session['myFairViewId']) &&
                MyFair::isExponentFair(Yii::$app->session['myFairViewId']) &&
                Yii::app()->user->role == User::ROLE_EXPONENT
            ) || Yii::app()->user->role == User::ROLE_ORGANIZERS
        ),
        array(
            'label' => Yii::t('leftMenu', 'Calendar'),
            'url' => Yii::app()->createUrl('calendarExponent', ['fairId' => Yii::$app->session['myFairViewId']]),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info',
            ),
            'visible' =>
                isset(Yii::$app->session['myFairViewId']) &&
                (
                    Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE ||
                    (
                        Yii::app()->user->role == User::ROLE_EXPONENT &&
                        !MyFair::isExponentFair(Yii::$app->session['myFairViewId'])
                    )
                )
        ),
        array(
            'label' => Yii::t('leftMenu', 'all exponents'),
            'url' =>$proposalVersion.Yii::app()->createUrl('/organizer/exponent'),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info',
            ),
            'visible' =>
                isset(Yii::$app->session['myFairViewId']) &&
                MyFair::isOrganizerOfFair(Yii::$app->session['myFairViewId'])
        ),
        array(
            'label' => Yii::t('leftMenu', 'Tech. tasks'),
            'url' => isset(Yii::$app->session['myFairViewId']) ? Yii::app()->createUrl('tZ/fairTz', ['fairId' => Yii::$app->session['myFairViewId']]) : '#',
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId']) ? '' : ' hide') .
                    (Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'tZ/fairTz' ? ' active' : ''),
            ),
            'visible' => false,
        ),

        array(
            'label' => Yii::t('leftMenu', 'Proposals'),
            'url' => isset(Yii::$app->session['myFairViewId']) ? $proposalVersion.Yii::app()->createUrl($office . '/list/requests', ['fairId' => Yii::$app->session['myFairViewId']]) : '#',
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ? (
                            (Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'workspace/requests' ||
                            Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'backoffice/requests' ||
                            Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'proposal/proposal/create')
                            ? ' active' : ''
                        )
                        : ' hide'),
            ),
            'visible' => (
                (isset(Yii::$app->session['myFairViewId'])) &&
                ((Yii::app()->user->role == User::ROLE_EXPONENT && MyFair::isExponentFair(Yii::$app->session['myFairViewId'])) ||
                    Yii::app()->user->role == User::ROLE_ORGANIZERS
                )
            )
        ),
        array(
            'label' => Yii::t('leftMenu', 'Tenders'),
            'url' => Yii::app()->createUrl('dev/developing', ['tab' => 'tenders_fair']),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId']) ? '' : ' hide') .
                    (
                    $app->controller->id == 'dev'
                    && $app->controller->action->id == 'developing'
                    && isset($_GET['tab']) && $_GET['tab'] == 'tenders_fair'
                        ? ' active'
                        : '')
            ),
            'visible' => FALSE/*($app->controller->id == 'workspace' && $app->controller->action->id == 'index')
                || isset(Yii::$app->session['myFairViewId'])*/
        ), array(
            'label' => Yii::t('leftMenu', 'Reports'),
//            'url' => isset(Yii::$app->session['myFairViewId']) ? Yii::app()->createUrl('workspace/reports', ['id' => Yii::$app->session['myFairViewId']]) : '#',
            'url' => Yii::app()->createUrl('backoffice/list/reports', ['id' => Yii::$app->session['myFairViewId']]),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ?(
                            (
                                Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'backoffice/reports'
                                && isset($_GET['id']) && $_GET['id'] == Yii::$app->session['myFairViewId']
                            )? ' active' : ''
                        ): ' hide'),
            ),
            'visible' =>
                isset(Yii::$app->session['myFairViewId']) &&
                Yii::app()->user->role == User::ROLE_ORGANIZERS

        ),array(
            'label' => Yii::t('leftMenu', 'My documents'),
            'url' => isset(Yii::$app->session['myFairViewId']) ? $proposalVersion.Yii::app()->createUrl($office . '/list/documents', ['id' => Yii::$app->session['myFairViewId']]) : '#',
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ? (
                        (Yii::app()->urlManager->parseUrl(Yii::app()->request) == '/documents'
                            && isset($_GET['id']) && $_GET['id'] == Yii::$app->session['myFairViewId'])
                            ? ' active' : ''
                        )
                        : ' hide'),
            ),
            'visible' => ($app->controller->id == 'workspace' && $app->controller->action->id == 'index')
                || isset(Yii::$app->session['myFairViewId']) && $office == 'workspace'
        ),array(
            'label' => Yii::t('leftMenu', 'Photoreport'),
            'url' =>$proposalVersion. Yii::app()->createUrl('workspace/photo', ['fairId' => Yii::$app->session['myFairViewId']]),
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info' .
                    (isset(Yii::$app->session['myFairViewId'])
                        ? (
                        (Yii::app()->urlManager->parseUrl(Yii::app()->request) == '/documents'
                            && isset($_GET['id']) && $_GET['id'] == Yii::$app->session['myFairViewId'])
                            ? ' active' : ''
                        )
                        : ' hide'),
            ),
            'visible' => Yii::$app->session['myFairViewId'] == 184 && MyFair::isExponentFair(Yii::$app->session['myFairViewId']),
        ),array(
            'label' => Yii::t('leftMenu', 'Orgplan'),
            'url' => isset(Yii::$app->session['myFairViewId']) ? Yii::app()->createUrl('/exponentPrepare', ['fairId' => Yii::$app->session['myFairViewId']]) : '#',
            'htmlOptions' => array(
                'class' => 'nav-list__item nav-list__item-slim left-menu-fair-info',
            ),
            'visible' => isset(Yii::$app->session['myFairViewId']) && !MyFair::isExponentFair(Yii::$app->session['myFairViewId']) && Yii::app()->user->role != User::ROLE_ORGANIZERS
        ),
        TbHtml::menuDivider(['class' => 'h-30 left-menu-fair-info' . (isset(Yii::$app->session['myFairViewId']) ? '' : ' hide')]),
        array(
            'label' => '<i class="icon" data-icon="&#xe42c;"></i>' . Yii::t('system', 'Calendar'),
            'url' => Yii::app()->createUrl('calendarExponent'),
            'htmlOptions' => array(
                'class' => 'nav-list__item' . (
                    !empty(Yii::app()->controller->module->id) &&
                    Yii::app()->controller->module->id == 'calendarExponent' ? ' active' : ''
                )
            ),
            'visible' => !$app->user->isGuest && (Yii::app()->user->role == User::ROLE_EXPONENT || Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE),
        ),
        array(
            'label' =>  Yii::t('leftMenu','My checklists'),
            'url' =>Yii::app()->params['expoplannerUrl'].'/lists',
            'htmlOptions' => array(
                'class' => 'nav-list__item',
            ),
            'visible' => !$app->user->isGuest && Yii::app()->user->role != User::ROLE_ORGANIZERS ,
        ),
        array(
            'label' => '<i class="icon" data-icon="&#xe138;"></i>' . Yii::t('system', 'Tech. tasks'),
            'url' => $this->createUrl('tZ/'),
            'htmlOptions' => array(
                'class' => 'nav-list__item' . ($app->controller->id == 'tZ' && $app->controller->action->id == 'index' ? ' active' : '')
            ),
            'visible' => false,
        ),
    )
));
?>
<?php if ($app->user->isGuest) : ?>
    <div class="left-form">
        <i data-icon="&#xe0cc;"
           class="b-button b-button-icon b-input-icon d-bg-success js-nav-list--minimize-button"><a
                href="#"></a></i>

        <h2><?= Yii::t('system','Create your Protoplan!') ?></h2>

        <div class="social-links-block" style="display: none">
            <?php $this->widget('ZAuthWidget', array('action' => '/auth/default/socialAuth')); ?>
        </div>
        <?php
        $model = new User('registration-left-menu');

        Yii::import('application.modules.profile.components.LoginForm');
        $this->widget('ext.widgets.FormView', array(
            'form' => new LoginForm(
                array(
                    'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
                    'action' => Yii::app()->createUrl('auth/default/registrationLeftMenu'),
                    'activeForm' => array(
                        'class' => 'TbActiveForm',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnType' => false,
                            'validateOnChange' => false,
                            'afterValidate' => 'js:function(form, data, hasError){
                            if(!hasError) {
                                try {
                                    yaCounter31914033.reachGoal("registracia");
                                    ga("tracker.send", "event", "registration_sidebar", "click_on_submit");
                                } catch (e) {
                                    console.log(e);
                                    if (Raven != undefined) {
                                        Raven.config("http://6c0aa533332643918320060f986ea813@sentry.orgplan.ru/4").install();
                                        Raven.captureException(e);
                                    }
                                }
                                window.location.reload();
                            }
                            $(".modal__modal-content .js-ajax-loader").fadeOut(200);
                        }'
                        ),
                    ),
                ),
                $model, $this
            ),
            'htmlOptions' => array(),
        ));
        ?>
        <div class="left-form__copyright">
            Copyright © <?=date('Y')?> KUBEX RUS
        </div>
    </div>
<?php endif ?>

<div
    class="container-fluid container-fluid--with-left-menu <?= ($app->user->getState(CWebUser::FLASH_COUNTERS) ? 'with-flash' : '') ?>">
    <?php if (!empty($this->breadcrumbs) || $app->user->getState(CWebUser::FLASH_COUNTERS)) : ?>
        <?php
        if (property_exists($this, 'breadcrumbs')) {
            $this->widget('zii.widgets.CBreadcrumbs', array('links' => $this->breadcrumbs));
        } ?>
        <div class="clear flash-clear">
            <div class="b-flash">
                <?php
                $this->widget('bootstrap.widgets.TbAlert', array(
                    'alerts' => implode(' ', array(
                        TbHtml::ALERT_COLOR_INFO,
                        TbHtml::ALERT_COLOR_SUCCESS,
                        TbHtml::ALERT_COLOR_WARNING,
                        TbHtml::ALERT_COLOR_ERROR,
                        TbHtml::ALERT_COLOR_DANGER,
                    ))
                ));
                ?>
            </div>
        </div>
    <?php endif; ?>
    <?php echo $content; ?>
</div>
<?= TbHtml::closeTag('body') ?>
</html>
