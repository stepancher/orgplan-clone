var pp = {
    lang: null,
    dictionaries: {},
    hashPrefix: '!',
    
    path: function (route, params)
    {
        var result = '/' + pp.lang + '/' + route;

        if (params != undefined) {
            result = result + '?';
        }

        for(var param in params) {
            result = result + param + '=' + params[param] + '&';

            if (params[param + 1] != undefined) {
                result = result + '&';
            }
        }
        
        return result;
    },
    
    route: function (route) 
    {
        return '#!' + route;
    },
    
    module: function (name, requires, configFn) 
    {
        var result = {};

        result.factory = function () {return result};
        result.config = function () {return result};
        result.controller = function () {return result};
        result.directive = function () {return result};

        if (window.angular != undefined) {
            result = angular.module(name, requires, configFn);
        }

        return result;
    },
    
    ucfirst: function (string)
    {
        return string[0].toUpperCase() + string.slice(1);
    },
    
    viewPath: function (template) 
    {
        return '/static/templates/' + template + '.html'
    },

    getLang: function ()
    {
        var result = pp.lang;

        if (result == null) {
            result = $('.pp-lang').val();
        }

        if (result == undefined) {
            result = location.pathname.split('/')[1];
        }

        return result;
    },

    setLang: function (lang)
    {
        pp.lang = lang;
    }
};

pp.setLang(pp.getLang());

pp.module('workspace', ['t', 'proposal', 'tbl']);
pp.module('organizer', ['ngRoute', 't', 'proposal', 'tbl', 'tabs']);
pp.module('proposal', ['t']).factory('proposal', function($http, t) {
    return {
        STATUS_DRAFT: 1,
        STATUS_REJECTED: 3,
        STATUS_SENT: 2,
        STATUS_ACCEPTED: 4,
        STATUS_REMOVED_BY_ORGANIZER: 5,
        STATUS_REMOVED_BY_EXPONENT: 6,
    
        PAYMENT_NOT: 0,
        PAYMENT_PART: 1,
        PAYMENT_FULL: 2,
        PAYMENT_BILL: 3,
    
        DOCUMENTS_NOT: 0,
        DOCUMENTS_RECIVED: 1,
        
        SEEN_TRUE: 1,
        SEEN_FALSE: 0,

        /**
         * @param type
         * @returns {string}
         */
        getDocumentStatusName: function (type) 
        {
            var result = '';

            switch (type) {
                case this.DOCUMENTS_NOT:
                    result = t.t('proposals', 'DOCUMENTS_NOT');
                    break;
                case this.DOCUMENTS_RECIVED:
                    result = t.t('proposals', 'DOCUMENTS_RECIVED');
                    break;
            }

            return result;
        },

        /**
         * @param type
         * @returns {{}}
         */
        getStatus: function (type) 
        {
            var result = {};

            switch (type) {
                case this.STATUS_ACCEPTED:
                    result.value = t.t('proposals', 'STATUS_ACCEPTED');
                    result.color = 'text_success';
                    result.circle = 'circle_success';
                    break;
                case this.STATUS_DRAFT:
                    result.value = t.t('proposals', 'STATUS_DRAFT');
                    result.color = 'text_muted';
                    result.circle = 'circle_muted';
                    break;
                case this.STATUS_REJECTED:
                    result.value = t.t('proposals', 'STATUS_REJECTED');
                    result.color = 'text_danger';
                    result.circle = 'circle_danger';
                    break;
                case this.STATUS_REMOVED_BY_ORGANIZER:
                    result.value = t.t('proposals', 'STATUS_REMOVED_BY_ORGANIZER');
                    result.color = 'text_warning';
                    result.circle = 'circle_warning';
                    break;
                case this.STATUS_REMOVED_BY_EXPONENT:
                    result.value = t.t('proposals', 'STATUS_REMOVED_BY_EXPONENT');
                    result.color = 'text_warning';
                    result.circle = 'circle_warning';
                    break;
                case this.STATUS_SENT:
                    result.value  = t.t('proposals', 'STATUS_SENT');
                    result.color  = 'text_primary';
                    result.circle = 'circle_primary';
                    break;
            }

            return result;
        },

        /**
         * @param type
         * @returns {string}
         */
        getPaymentStatusName: function (type) 
        {
            var result = '';

            switch (type) {
                case this.PAYMENT_FULL:
                    result = t.t('proposals', 'PAYMENT_FULL');
                    break;
                case this.PAYMENT_NOT:
                    result = t.t('proposals', 'PAYMENT_NOT');
                    break;
                case this.PAYMENT_PART:
                    result = t.t('proposals', 'PAYMENT_PART');
                    break;
                case this.PAYMENT_BILL:
                    result = t.t('proposals', 'PAYMENT_BILL');
                    break;
            }

            return result;
        },
        
        loadWorkspaceProposals: function (callback) 
        {
            
        }
    }
});
pp.module('t', []).factory('t', function($http) {
    return {
        t: function(dictionary, word) {
            var result = word;
            var url = '/static/frontend/translates/' + pp.lang + '/' + dictionary + '.json?v=' + (new Date()).getTime();

            if (pp.dictionaries[dictionary] == undefined) {
                $.ajax({
                    url: url,
                    success: function(response)
                    {
                        pp.dictionaries[dictionary] = response;
                    },
                    async: false
                });
            }

            if (pp.dictionaries[dictionary][word] != undefined) {
                result = pp.dictionaries[dictionary][word];
            }

            return result;
        }
    };
});
function Protoplan()
{
    this.elements = [];
}

/**
 * @class Protoplan.Block
 * @param id
 * @param selector
 * @constructor
 */
Protoplan.Block = function (id, selector)
{
    this.id       = typeof id       == 'undefined' ? 0        : id;
    this.selector = typeof selector == 'undefined' ? '#' + id : selector;

    this.capitalizeFirstLetter = function (string)
    {
        return string[0].toUpperCase() + string.slice(1);
    };

    this.merge = function (obj1, obj2)
    {
        var obj3 = {};
        for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
        for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
        return obj3;
    };

    this.clone = function (obj)
    {
        var clone = {};

        for (var key in obj) {
            clone[key] = obj[key];
        }

        return clone;
    };
};

var protoplan = new Protoplan();
pp.module('organizer').config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider)
    {
        $locationProvider.hashPrefix(pp.hashPrefix);
        // $locationProvider.html5Mode(true).hashPrefix(pp.hashPrefix);

        $routeProvider.
        when('/incoming', {
            template: '<tabs params="tabs"></tabs><tbl params="incoming"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/incoming/:sort/:order/:name/:contractor/:date/:page', {
            template: '<tabs params="tabs"></tabs><tbl params="incoming"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/accepted', {
            template: '<tabs params="tabs"></tabs><tbl params="accepted"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/accepted/:sort/:order/:name/:date/:contractor/:payment/:documents/:page', {
            template: '<tabs params="tabs"></tabs><tbl params="accepted"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/rejected', {
            template: '<tabs params="tabs"></tabs><tbl params="rejected"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/rejected/:sort/:order/:name/:contractor/:date/:comment/:page', {
            template: '<tabs params="tabs"></tabs><tbl params="rejected"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        otherwise('/incoming');
    }
]);

pp.module('organizer').controller('OrganizerProposalCtrl', function ($scope, $http, $sce, t, proposal, $routeParams)
{
    $scope.LIMIT = 20;

    $scope.tabs = [
        {
            link: '/incoming',
            text: t.t('proposals', 'INCOMING'),
            counter: 0
        },
        {
            link: '/accepted',
            text: t.t('proposals', 'ACCEPTED')
        },
        {
            link: '/rejected',
            text: t.t('proposals', 'REJECTED')
        }
    ];

    $scope.getRouteParams = function (params)
    {
        var result = {};

        for (param in params) {
            result[param] = params[param].substr(1);
        }

        return result;
    };

    $scope.getSort = function ()
    {
        var result = $scope.getRouteParams($routeParams)['sort'];

        return result != undefined ? result : 'date';
    };

    $scope.getOrder = function ()
    {
        var result = $scope.getRouteParams($routeParams)['order'];

        return result != undefined ? result : 'DESC';
    };

    $scope.getContractor = function ()
    {
        var result = $scope.getRouteParams($routeParams)['contractor'];

        return result != undefined ? result : '';
    };

    $scope.getName = function ()
    {
        var result = $scope.getRouteParams($routeParams)['name'];

        return result != undefined ? result : '';
    };

    $scope.getDate = function ()
    {
        var result = $scope.getRouteParams($routeParams)['date'];

        return result != undefined ? result : '';
    };

    $scope.getPage = function ()
    {
        var result = $scope.getRouteParams($routeParams)['page'];

        return result != undefined ? result : 1;
    };

    $scope.getPayment = function ()
    {
        var result = $scope.getRouteParams($routeParams)['payment'];

        return result != undefined ? result : '';
    };

    $scope.getDocuments = function ()
    {
        var result = $scope.getRouteParams($routeParams)['documents'];

        return result != undefined ? result : '';
    };

    $scope.getComment = function ()
    {
        var result = $scope.getRouteParams($routeParams)['comment'];

        return result != undefined ? result : '';
    };

    $scope.incoming =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name',
                filter: $scope.getName()
            },
            {
                text: t.t('proposals', 'CONTRACTOR'),
                name: 'contractor',
                filter: $scope.getContractor()
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date',
                filter: $scope.getDate()
            },
            {
                name: 'buttons',
                sortable: false
            }
        ],
        sorting: 'ajax',
        order: $scope.getOrder(),
        sort: function (column, reverse)
        {
            var r = $scope.getOrder();

            if (r == 'ASC') {
                r = 'DESC';
            } else {
                r = 'ASC';
            }

            window.history.pushState(null, null,
                '#!/incoming/:' + column +
                '/:' + r +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getPage());
        },
        filter: function (filter) 
        {
            var name = '',
                contractor = '',
                date = '';

            for (item in filter) {
                switch(filter[item].name) {
                    case 'name':
                        name = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'contractor':
                        contractor = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'date':
                        date = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                }
            }
            
            window.history.pushState(null, null,
                '#!/incoming/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + name +
                '/:' + contractor +
                '/:' + date +
                '/:1');
        },
        page: function (page)
        {
            window.history.pushState(null, null,
                '#!/incoming/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + page);
        }
    };

    $scope.accepted =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name',
                filter: $scope.getName()
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date',
                filter: $scope.getDate()
            },
            {
                text: t.t('proposals', 'CONTRACTOR'),
                name: 'contractor',
                filter: $scope.getContractor()
            },
            {
                text: t.t('proposals', 'PAYMENT'),
                name: 'payment',
                filter: $scope.getPayment(),
                filterType: 'select',
                filterItems: [
                    {id: proposal.PAYMENT_NOT, name: '-'},
                    {id: proposal.PAYMENT_PART, name: t.t('proposals', 'PARTLY')},
                    {id: proposal.PAYMENT_FULL, name: '100%'},
                    {id: proposal.PAYMENT_BILL, name: t.t('proposals', 'INVOICE ISSUED')}
                ]
            },
            {
                text: t.t('proposals', 'DOCUMENTS'),
                name: 'documents',
                filter: $scope.getDocuments(),
                filterType: 'select',
                filterItems: [
                    {id: proposal.DOCUMENTS_NOT, name: '-'},
                    {id: proposal.DOCUMENTS_RECIVED, name: t.t('proposals', 'RECIEVED')}
                ],
                filterDefaultItem: {id: '?', name: t.t('proposals', 'CHOOSE')}
            },
            {
                name: 'buttons',
                sortable: false
            }
        ],

        order: $scope.getOrder(),
        sorting: 'ajax',
        sort: function (column, reverse)
        {
            var r = $scope.getOrder();

            if (r == 'ASC') {
                r = 'DESC';
            } else {
                r = 'ASC';
            }

            window.history.pushState(null, null,
                '#!/accepted/:' + column +
                '/:' + r +
                '/:' + $scope.getName() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getPayment() +
                '/:' + $scope.getDocuments() +
                '/:' + $scope.getPage());
        },
        filter: function (filter)
        {
            var name = '',
                contractor = '',
                payment = '',
                documents = '',
                date = '';

            for (item in filter) {
                switch(filter[item].name) {
                    case 'name':
                        name = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'contractor':
                        contractor = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'date':
                        date = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'payment':
                        payment = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'documents':
                        documents = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                }
            }

            window.history.pushState(null, null,
                '#!/accepted/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + name +
                '/:' + date +
                '/:' + contractor +
                '/:' + payment +
                '/:' + documents +
                '/:1');
        },
        page: function (page)
        {
            window.history.pushState(null, null,
                '#!/accepted/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + $scope.getName() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getPayment() +
                '/:' + $scope.getDocuments() +
                '/:' + page);
        }
    };

    $scope.rejected =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name',
                filter: $scope.getName()
            },
            {
                text: t.t('proposals', 'CONTRACTOR'),
                name: 'contractor',
                filter: $scope.getContractor()
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date',
                filter: $scope.getDate()
            },
            {
                text: t.t('proposals', 'COMMENT'),
                name: 'comment',
                filter: $scope.getComment()
            }
        ],

        order: $scope.getOrder(),
        sorting: 'ajax',
        sort: function (column, reverse)
        {
            var r = $scope.getOrder();

            if (r == 'ASC') {
                r = 'DESC';
            } else {
                r = 'ASC';
            }

            window.history.pushState(null, null,
                '#!/rejected/:' + column +
                '/:' + r +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getComment() +
                '/:' + $scope.getPage());
        },
        filter: function (filter)
        {
            var name = '',
                contractor = '',
                comment = '',
                date = '';

            for (item in filter) {
                switch(filter[item].name) {
                    case 'name':
                        name = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'contractor':
                        contractor = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'date':
                        date = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'comment':
                        comment = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                }
            }

            window.history.pushState(null, null,
                '#!/rejected/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + name +
                '/:' + contractor +
                '/:' + date +
                '/:' + comment +
                '/:1');
        },
        page: function (page)
        {
            window.history.pushState(null, null,
                '#!/rejected/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getComment() +
                '/:' + page);
        }
    };

    $scope.getFairId = function ()
    {
        return $('.pp-fair-id').val();
    };

    $scope.count = 0;

    $scope.getCount = function ()
    {
        return $scope.incoming.rows.length;
    };

    var params;

    params =
    {
        fairId: $scope.getFairId(),
        'order[by]': $scope.getSort(),
        'order[order]': $scope.getOrder(),
        'filter[status]': proposal.STATUS_SENT,
        'filter[name]': $scope.getName(),
        'filter[contractor]': $scope.getContractor(),
        'filter[date]': $scope.getDate(),
        'limit': $scope.LIMIT,
        'page': $scope.getPage() - 1
    };

    $http.get(pp.path('backoffice/panel/requestsAjax/', params)).then(function(response)
    {
        $scope.incoming.rows = $scope.prepareRows(response.data, 'incoming');
        $scope.tabs[0].counter = response.data[0].count;
        $scope.incoming.orderBy = $scope.getSort();

        var totalItems = response.data[0].total;

        $scope.incoming.total = ((totalItems - totalItems % $scope.LIMIT) / $scope.LIMIT) + 1;
        $scope.incoming.count = totalItems;

        $scope.incoming.pageSize = $scope.LIMIT;
        $scope.incoming.currentPage = $scope.getPage() * 1;

        $scope.incoming.params = params;
    });

    params =
    {
        fairId: $scope.getFairId(),
        'order[by]': $scope.getSort(),
        'order[order]': $scope.getOrder(),
        'filter[status]': proposal.STATUS_ACCEPTED,
        'filter[name]': $scope.getName(),
        'filter[contractor]': $scope.getContractor(),
        'filter[date]': $scope.getDate(),
        'filter[payment]': $scope.getPayment(),
        'filter[documents]': $scope.getDocuments(),
        'limit': $scope.LIMIT,
        'page': $scope.getPage() - 1
    };

    $http.get(pp.path('backoffice/panel/requestsAjax/', params)).then(function(response)
    {
        $scope.accepted.rows = $scope.prepareRows(response.data, 'accepted');
        $scope.accepted.orderBy = $scope.getSort();

        var totalItems = response.data[0].total;

        $scope.accepted.total = ((totalItems - totalItems % $scope.LIMIT) / $scope.LIMIT) + 1;
        $scope.accepted.count = totalItems;

        $scope.accepted.pageSize = $scope.LIMIT;
        $scope.accepted.currentPage = $scope.getPage() * 1;

        $scope.accepted.params = params;
    });

    params =
    {
        fairId: $scope.getFairId(),
        'order[by]': $scope.getSort(),
        'order[order]': $scope.getOrder(),
        'filter[status]': proposal.STATUS_REJECTED,
        'filter[name]': $scope.getName(),
        'filter[contractor]': $scope.getContractor(),
        'filter[date]': $scope.getDate(),
        'filter[comment]': $scope.getComment(),
        'limit': $scope.LIMIT,
        'page': $scope.getPage() - 1
    };

    $http.get(pp.path('backoffice/panel/requestsAjax/', params)).then(function(response)
    {
        $scope.rejected.rows = $scope.prepareRows(response.data, 'rejected');
        $scope.rejected.orderBy = $scope.getSort();

        var totalItems = response.data[0].total;

        $scope.rejected.total = ((totalItems - totalItems % $scope.LIMIT) / $scope.LIMIT) + 1;
        $scope.rejected.count = totalItems;

        $scope.rejected.pageSize = $scope.LIMIT;
        $scope.rejected.currentPage = $scope.getPage() * 1;
        
        $scope.rejected.params = params;
    });

    $scope.prepareRows = function (models, table)
    {
        var result = [],
            prepareRow = $scope[table + 'Row'];

        for (var i in models)
        {
            var model = models[i];
            result[i] = prepareRow(model);

            if (model.seen == 0) {
                result[i].classes = {value: 'tbl__row_seen-false', type: 'disable'};
            } else {
                result[i].classes = {value: '', type: 'disable'};
            }
        }

        return result;
    };

    $scope.incomingRow = function (model)
    {
        var result     = {},
            pathView   = pp.path('proposal/proposal/viewProposalForOrganizer/', {eId: model.id, userId: model.exponentId}),
            hasFilesBtnColor = model.hasFiles ? '1' : '0';

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml('<a href="' + pathView + '">' + model.name + '</a>' +
                '<div class="buttons">' +
                '<div class="cell__file" style="opacity:' + hasFilesBtnColor + ';"></div>' +
                '</div>'),
            type: 'html'
        };

        result.contactor = {value: model.exponentName + ' (' + model.exponentEmail + ')'};
        result.date      = {value: model.date};

        console.log(model);
        result.buttons =
        {
            value:
                [
                    {
                        classes: 'button_m button_m_green button_hide',
                        text: t.t('proposals', 'APPROVE'),
                        target: '.modal-accept-' + model.id,
                        modal:
                        {
                            name: 'modal-accept-' + model.id,
                            link: pp.path('proposal/proposal/accept', {eId: model.id}),
                            text: t.t('proposals', 'APPROVE') + '?',
                            ok: t.t('proposals', 'APPROVE')
                        }
                    },
                    {
                        classes: 'button_m button_m_red button_hide',
                        text: t.t('proposals', 'REJECT'),
                        target: '.modal-remove-' + model.id,
                        modal:
                        {
                            name: 'modal-remove-' + model.id,
                            link: {
                                path: '/proposal/proposal/reject',
                                params: {
                                    eId: model.id,
                                    ecId:model.class,
                                    userId: model.exponentId,
                                    fairId: $scope.getFairId()
                                }
                            },
                            text: t.t('proposals', 'REJECT') + '?',
                            ok: t.t('proposals', 'REJECT'),
                            comment: true,
                            type: 'POST'
                        }
                    }
                ],
            type: 'buttons',
            style: {'width':'235px'}
        };

        return result;
    };

    $scope.acceptedRow = function (model)
    {
        var result     = {},
            pathView   = pp.path('proposal/proposal/viewProposalForOrganizer/', {eId: model.id, userId: model.exponentId}),
            hasFilesBtnColor = model.hasFiles ? '1' : '0';
            selected = {id: 0, name: '-'};

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml('<a href="' + pathView + '">' + model.name + '</a>' +
                '<div class="buttons">' +
                '<div class="cell__file" style="opacity:' + hasFilesBtnColor + ';"></div>' +
                '</div>'),
            type: 'html'
        };

        result.date = {value: model.date};

        result.contactor = {value: model.exponentName + ' (' + model.exponentEmail + ')'};

        var paymentItems = [
            {id: proposal.PAYMENT_NOT, name: '-'},
            {id: proposal.PAYMENT_PART, name: t.t('proposals', 'PARTLY')},
            {id: proposal.PAYMENT_FULL, name: '100%'},
            {id: proposal.PAYMENT_BILL, name: t.t('proposals', 'INVOICE ISSUED')}
        ];

        for (item in paymentItems) {
            if (model.payment == paymentItems[item].id) {
                selected = paymentItems[item];
            }
        }

        result.payment =
        {
            value: selected,
            items: paymentItems,
            type: 'select',
            onchange: function (selectedId) 
            {
                $http.get(pp.path('backoffice/panel/updatePayment/',
                    {
                        fairId: $scope.getFairId(),
                        pe: model.id,
                        payment: selectedId * 1
                    }));
            }
        };

        var documentItems = [
            {id: proposal.DOCUMENTS_NOT, name: '-'},
            {id: proposal.DOCUMENTS_RECIVED, name: t.t('proposals', 'RECIEVED')}
        ];

        for (item in documentItems) {
            if (model.documents * 1 == documentItems[item].id) {
                selected = documentItems[item];
            }
        }

        result.documents =
        {
            value: selected,
            items: documentItems,
            type: 'select',
            onchange: function (selectedId) 
            {
                $http.get(pp.path('backoffice/panel/updateDocuments/',
                    {
                        fairId: $scope.getFairId(),
                        pe: model.id,
                        documents: selectedId * 1
                    }));
            }
        };

        result.buttons =
        {
            value:
                [
                    {
                        classes: 'button_m button_m_red button_hide',
                        text: t.t('proposals', 'REJECT'),
                        target: '.modal-reject-' + model.id,
                        modal:
                        {
                            name: 'modal-reject-' + model.id,
                            link: pp.path('proposal/proposal/reject', {eId: model.id, ecId:model.class, userId: model.exponentId}),
                            text: t.t('proposals', 'REJECT') + '?',
                            ok: t.t('proposals', 'REJECT')
                        }
                    },
                    {
                        classes: 'button_small button_small_remove button_hide',
                        text: '',
                        target: '.modal-remove-' + model.id,
                        modal:
                        {
                            name: 'modal-remove-' + model.id,
                            link: pp.path('proposal/proposal/deleteByOrganizer', {eId: model.id}),
                            text: t.t('proposals', 'REMOVE_PROPOSAL'),
                            ok: t.t('proposals', 'REMOVE')
                        }
                    }
                ],
            type: 'buttons',
            style: {'width':'235px'}
        };

        return result;
    };

    $scope.rejectedRow = function (model)
    {
        var result     = {},
            pathView   = pp.path('proposal/proposal/viewProposalForOrganizer/', {eId: model.id, userId: model.exponentId}),
            hasFilesBtnColor = model.hasFiles ? '1' : '0';

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml(model.name +
                '<div class="buttons">' +
                '<div class="cell__file" style="opacity:' + hasFilesBtnColor + ';"></div>' +
                '</div>'),
            type: 'html'
        };

        result.contactor = {value: model.exponentName + ' (' + model.exponentEmail + ')'};
        result.date      = {value: model.date};

        result.comment = {};

        if (model.comment != '') {
            result.comment   = {tooltip: {id: 'tooltip-' + model.eId, content: model.comment, icon: ''}};
        }

        return result;
    };
});
pp.module('workspace').controller('WorkspaceProposalCtrl', function ($scope, $http, t, proposal, $sce)
{
    $scope.params =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name'
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date'
            },
            {
                text: t.t('proposals', 'STATUS'),
                name: 'status'
            },
            {
                text: t.t('proposals', 'PAYMENT'),
                name: 'payment'
            },
            {
                text: t.t('proposals', 'DOCUMENTS'),
                name: 'documents'
            },
            {
                name: 'buttons',
                sortable: false
            }
        ],
        orderBy: 'date',
        reverse: true
    };

    $http.get(pp.path('workspace/list/requestsAjax/')).then(function(response)
    {
        $scope.params.rows = $scope.prepareRows(response.data);
    });

    $scope.prepareRows = function (models)
    {
        var result = [];

        for (var model in models)
        {
            result[model] = $scope.prepareRow(models[model]);
        }

        return result;
    };

    $scope.prepareRow = function (model)
    {
        var result     = {},
            pathEdit   = pp.path('proposal/proposal/create/', {ecId: model.ecId, eId: model.eId}),
            pathView   = pp.path('proposal/proposal/viewProposal/', {ecId: model.ecId, eId: model.eId});

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml('<a href="' + pathView + '">' + model.name + '</a>'),
            type: 'html'
        };
        
        result.date      = {value: model.date};
        result.status    = {value: model.status * 1};
        result.payment   = {value: model.payment * 1};
        result.documents = {value: model.documents * 1};

        result.buttons =
        {
            value:
            [
                {
                    href: pathEdit,
                    classes: 'button_small button_small_edit button_hide',
                    text: ''
                },
                {
                    classes: 'button_small button_small_remove button_hide',
                    text: '',
                    target: '.modal-' + model.eId,
                    modal:
                    {
                        name: 'modal-' + model.eId,
                        link: pp.path('proposal/proposal/prepareDelete', {eId: model.eId, ecId: model.ecId, userId: model.userId}),
                        text: t.t('proposals', 'REMOVE_PROPOSAL'),
                        ok: t.t('proposals', 'REMOVE')
                    }
                }
            ],
            type: 'buttons'
        };

        if ((model.status * 1) == proposal.STATUS_SENT || (model.status * 1) == proposal.STATUS_ACCEPTED) {
            result.buttons = {};
        }

        result.documents.value = proposal.getDocumentStatusName(result.documents.value);
        result.payment.value   = proposal.getPaymentStatusName(result.payment.value);
        result.status          = proposal.getStatus(result.status.value);

        if (model.comment != undefined && model.comment != '') {
            result.status.tooltip = {id: 'tooltip-' + model.eId, content: model.comment, icon: ''};
        }

        return result;
    };
});
pp.module('button', ['modal']).directive('btn', function() {
    return {
        templateUrl: pp.viewPath('button'),
        restrict: 'E',
        scope:
        {
            params: '='
        },
        link: function (scope, element, attrs)
        {
            if (scope.params.target != undefined) {
                scope.show = function ()
                {
                    $(scope.params.target).modal('show')
                };
            }
        }
    };
});
/**
 * @augments Protoplan.Block
 * @class Protoplan.Checkbox
 * @param id
 * @param state
 * @param selector
 * @param disabled
 * @constructor
 */

Protoplan.Checkbox = function (id, selector, state, disabled)
{
    var self = this;

    self.selector         = typeof selector == 'undefined' ? '#checkbox-' + id : selector;
    self.state            = typeof state    == 'undefined' ? false : state;
    self.disabled         = typeof disabled == 'undefined' ? false : disabled;
    self.childrenSelector = '[data-checkbox="' + id + '"]';

    Protoplan.Block.apply(this, [id, self.selector]);

    self.init = function ()
    {
        if ($(self.childrenSelector).length === 0) {
            self.childrenSelector = '.parent-' + self.id;
        }

        self.hasNotEmptyInputs() ? self.setState(true) : self.setState(self.state);

        $(document).delegate(self.selector, 'click', function ()
        {
            self.go();

            return !disabled;
        });
    };

    self.open = function ()
    {
        return $(self.childrenSelector).slideDown('fast');
    };

    self.close = function ()
    {
        $(self.childrenSelector).find('input').val('');
        $(self.childrenSelector).find('input').trigger('input'); //@TODO Refactor it
        return $(self.childrenSelector).slideUp('fast');
    };

    /**
     * @returns {boolean}
     */
    self.getState = function ()
    {
        self.state = $(self.selector).prop('checked');
        return self.state;
    };

    /**
     * @param state
     * @returns {boolean}
     */
    self.setState = function (state)
    {
        $(self.selector).prop('checked', state);
        self.go();
        return self.getState();
    };

    /**
     * @returns {boolean}
     */
    self.hasNotEmptyInputs = function ()
    {
        var ret = false;
        $(self.childrenSelector).each(function (i, value)
        {
            var inputValue = $(value).find('input[type="text"]').val();
            if (inputValue != '' && inputValue != undefined) {
                ret = true;
            }
        });

        return ret;
    };

    self.go = function ()
    {
        self.getState() ? self.open() : self.close();
    };

    setTimeout(function ()
    {
        self.init();
    }, 1000);
};
/**
 * @augments Protoplan.Block
 * @class Protoplan.DatePicker
 * @param id
 * @param start
 * @param end
 * @param params
 * @param blockParams
 * @constructor
 */

Protoplan.DatePicker = function (id, start, end, params, blockParams)
{
    Protoplan.Block.apply(this, [id, '.datePicker' + id]);

    var self = this;

    self.minDate     = typeof start       == 'undefined' ? new Date(0) : new Date(Date.parse(start));
    self.maxDate     = typeof end         == 'undefined' ? new Date()  : new Date(Date.parse(end));
    self.params      = typeof params      == 'undefined' ? {}          : params;
    self.blockParams = typeof blockParams == 'undefined' ? {}          : blockParams;

    self.startMonth    = (new Date(self.minDate)).getMonth() + 1;
    self.endSelector   = self.selector + '-end';
    self.count         = 0;

    self.maxDate.setDate(self.maxDate.getDate() + 1);

    self.init = function ()
    {
        $(self.selector).periodpicker(self.merge({
            end: self.selector + '-end',
            norange: true,
            mousewheel: false,
            likeXDSoftDateTimePicker: true,
            cells: [1, 2],
            withoutBottomPanel: true,
            yearsLine: false,
            title: false,
            closeButton: false,
            fullsizeButton: false,
            minDate: self.minDate.toISOString(),
            maxDate: self.maxDate.toISOString(),
            startMonth: self.startMonth,
            formatDate: 'YYYY/MM/DD',
            timepickerOptions: {
                twelveHoursFormat: false,
                hours: true,
                minutes: true,
                seconds: false,
                ampm: false
            }
        }, self.params));
    };

    self.addEndDate = function ()
    {
        var result = '',
            dates  = $(self.selector).periodpicker('value');

        if (dates.length > 0) {
            result = self.formatDate(dates[0]) + ' - ' + self.formatDate(dates[1]);
            self.setCount(dates[1] - dates[0]);
        }

        $(self.selector).val(result);
        return result;
    };

    self.formatDate = function (date)
    {
        var year    = date.getFullYear(),
            month   = ('0' + (date.getMonth() + 1)).slice(-2),
            day     = ('0' + date.getDate()).slice(-2),
            hours   = ('0' + date.getHours()).slice(-2),
            minutes = ('0' + date.getMinutes()).slice(-2),
            seconds = ('0' + date.getSeconds()).slice(-2),
            result  = year + '/' + month + '/' + day;

        if (hours > 0 || minutes > 0 || seconds > 0) {
            result = result + ' ' + hours + ':' + minutes + ':' + seconds;
        }

        return result;
    };

    self.setCount = function (count)
    {
        self.count = count;
        return count;
    };

    self.getDays = function ()
    {
        return Math.round(self.count / (1000 * 60 * 60 * 24) + 1);
    };

    self.getHours = function ()
    {
        return Math.round(self.count / (1000 * 60 * 60));
    };

    self.go = function ()
    {
        if ($(self.endSelector).length > 0) {
            self.addEndDate();
        }
    };

    self.init();
};

/**
 * @augments Protoplan.Block
 * @class Protoplan.Input
 * @param params
 * @constructor
 */

//@TODO create constants
Protoplan.Input = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.params   = typeof params.params   == 'undefined' ? {}                   : params;
    self.type     = typeof params.type     == 'undefined' ? 'string'             : params.type;
    self.selector = typeof params.selector == 'undefined' ? '.input' + params.id : params.selector;

    self.init = function ()
    {
        if (typeof self['setType' + self.capitalizeFirstLetter(self.type)] == 'function') {
            var fn = self['setType' + self.capitalizeFirstLetter(self.type)];
            fn();
        }
    };

    self.setTypeInt = function ()
    {
        self.type = 'int';

        $(document).delegate(self.selector, 'keyup keypress', function (e)
        {
            var symbol = (e.which) ? e.which : e.keyCode;
            if (symbol == 8) return true; //@TODO rewrite to switch
            if (symbol < 48 || symbol > 57)  return false;
        });
    };

    self.setTypeDouble = function ()
    {
        self.type = 'double';

        $(document).delegate(self.selector, 'keyup keypress', function (e)
        {
            var symbol = (e.which) ? e.which : e.keyCode;
            if (symbol == 46) return true; //@TODO rewrite to switch
            if (symbol == 8) return true;
            // if (symbol == 44) return true;
            if (symbol < 48 || symbol > 57)  return false;
        });
    };

    self.setTypeString = function ()
    {
        self.type = 'string';
    };

    self.onlyNumbers = function () {

    };

    self.init();
};
/**
 * Class Modal
 * @constructor
 */

Protoplan.Modal = function ()
{
    this.init = function ()
    {
        var selector = '.modal';
        $(selector).on('shown.bs.modal', function()
        {
            $(this).find('input').each(function ()
            {
                if ($(this).attr('type') != 'hidden') {
                    this.focus();
                    return false;
                }
            });
        });

        $(selector).on('hidden.bs.modal', function()
        {
            $(this).find('input').val('');
        });
    };

    this.confirm = function (params)
    {
        return {
            confirmText: '',
            path: '',
            after: '',
            id: '',
            method: 'get',
            formId: '',

            run: function(params)
            {
                $('body').on('click', '#' + params.id, function ()
                {
                    params.remove_btn = $(this);

                    var target = $(params.remove_btn).attr('data-target');

                    $(target).modal('show');

                    $(target + ' .js-modal-ok').click(function (e)
                    {
                        e.stopPropagation();

                        if(params.method=='get'){
                            $.get(params.path).done(params.after(params));
                        }

                        if(params.method=='post'){
                            $.post(params.path, $('#' + params.formId).serialize()).done(params.after(params));
                        }

                        $(target).modal('hide');
                    });

                    $('.js-modal-cancel').click(function (e)
                    {
                        e.stopPropagation();
                        $(target).modal('hide');
                    });
                });
            }
        }.run(params);
    };

    this.init();
};

pp.module('modal', ['t']).directive('modal', function($http, t) {
    return {
        templateUrl: pp.viewPath('modal'),
        restrict: 'E',
        scope:
        {
            params: '='
        },
        link: function (scope, element, attrs)
        {
            scope.ok = function ()
            {
                if(scope.params.type == 'POST'){
                    scope.params.link.params.message = scope.params.message;
                    var eId =scope.params.link.params.eId;
                    delete scope.params.link.params.eId;

                    $.ajax({
                        url: '/' + pp.lang + scope.params.link.path + '/?eId='+eId,
                        data:scope.params.link.params,
                        type:'post',
                        success:function (response) {
                            $(element).find('.modal').modal('hide');
                        }
                    });

                }else{

                    $http.get(scope.params.link).then(function(response) {
                        $(element).find('.modal').modal('hide');
                    });
                }

                $('a[data-target=".' + scope.params.name + '"]').parents('.tbl__row').slideUp(300);
            };

            scope.cancel = function ()
            {
                $(element).find('.modal').modal('hide');
            };

            scope.t = function (key)
            {
                return t.t('proposals', key);
            };
        }
    };
});
pp.module('pp-select', []).directive('ppselect', function() {
    return {
        templateUrl: pp.viewPath('pp-select'),
        restrict: 'E',
        scope:
        {
            "params": '='
        },
        link: function (scope, element, attrs)
        {
            scope.change = function ()
            {
                scope.params.onchange(scope.params.value.id);
            }
        }
    };
});
/**
 * Class Radio
 * @constructor
 */

Protoplan.Radio = function ()
{
    var self = this;
};
/**
 * @augments Protoplan.Block
 * @class Protoplan.ScrollUp
 * @constructor
 */

Protoplan.ScrollUp = function ()
{
    var self = this;

    self.selector = '.scroll-up';

    Protoplan.Block.apply(this, ['scroll-up', self.selector]);

    self.init = function ()
    {
        self.update();

        $(document).scroll(function ()
        {
            self.update();
        });

        $(self.selector).click(function ()
        {
            window.scroll(0 ,0);
            return false;
        });
    };

    self.update = function ()
    {
        var display = 'none';
        
        if (window.pageYOffset > 200) {
            display = 'block';
        }
        
        $(self.selector).css('display', display);
    };

    self.init();
};

protoplan.elements['scroll-up'] = new Protoplan.ScrollUp();
/**
 * @augments Protoplan.Block
 * @class Protoplan.Spinner
 * @param params
 * @constructor
 */

Protoplan.Spinner = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.step = params.step != undefined ? params.step : 1;

    self.up   = '.spinner__btn_up-' + self.id;
    self.down = '.spinner__btn_down-' + self.id;
    self.state = 0;
    self.min = 0;

    self.init = function ()
    {
        $(self.up).click(function ()
        {
            self.setState(self.state * 1 + self.step * 1);
            self.render();
            return false;
        });

        $(self.down).click(function ()
        {
            self.setState(self.state * 1 - self.step * 1);
            self.render();
            return false;
        });

        $(document).delegate(self.selector, 'keyup keypress', function (e)
        {
            return false;
        });
    };

    self.render = function ()
    {
        $(self.selector).val(self.state);
        $(self.selector).trigger('change');
    };

    self.setState = function (value)
    {
        var result = self.min;

        if (value >= self.min) {
            result = value
        }

        self.state = result;
    };

    self.init();
};
/**
 * @augments Protoplan.Block
 * @class Protoplan.Spoiler
 * @param params
 * @constructor
 */

Protoplan.Spoiler = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.selector    = params.selector    == undefined ? '.spoiler'                     : params.selector;
    self.clickArea   = params.clickArea   == undefined ? '.spoiler'                     : params.clickArea;
    self.target      = params.target      == undefined ? ''                             : params.target;
    self.hiddenInput = params.hiddenInput == undefined ? '#Attribute_attr-' + params.id : params.hiddenInput;
    self.wrap        = params.wrap        == undefined ? '.spoiler-wrap-' + params.id   : params.wrap;

    self.state = 0;
    
    self.init = function ()
    {
        $(self.clickArea).click(function ()
        {
            if (params.wrap != undefined) {
                $(self.wrap).slideToggle(100);
            } else {
                $(self.target).slideToggle(100);
            }

            $(self.selector).toggleClass('open');
        });

        self.setValue(self.wrapIsNotEmpty());

        $(document).delegate(self.wrap + ' input', 'input change', function ()
        {
            self.setValue(self.wrapIsNotEmpty());
        });
    };

    self.setValue = function (value)
    {
        $(self.hiddenInput).val(0);
        if (value == 1) {
            $(self.hiddenInput).val(1);
        }

        if (value == 1) {
            $(self.selector).addClass('open');
        } else {
            $(self.selector).removeClass('open');
        }

        value == 1 ? $(self.wrap).slideDown() : $(self.wrap).slideUp();
    };

    /**
     * @returns {int}
     */
    self.wrapIsNotEmpty = function ()
    {
        var result = 0;

        $(self.wrap + ' input').each(function (i, item)
        {
            if($(item).val() != '' && $(item).val() != '0') {
                result = 1;
            }
        });

        return result;
    };

    //@TODO Remove
    setTimeout(function ()
    {
        self.init();
    }, 1000);
};
pp.module('textfield', []).directive('textfield', function($rootScope)
{
    return {
        templateUrl: '/static/templates/textfield.html',
        restrict: 'E',
        scope:
        {
            params: '='
        },

        link: function (scope, element, attrs)
        {
            scope.config = scope.params;

            scope.id     = scope.config.id;
            scope.value  = scope.config.value;

            scope.field = {};

            scope.setDefault = function () {
                angular.forEach(scope.field, function(value, key) {
                    scope.field[key] = scope.config.properties[key];
                });
            };

            scope.setDefault();

            angular.forEach(scope.field, function(value, key) {
                scope.field[key] = scope.config.properties[key];
            });

            scope.$on('update', function($event, $signal){
                var dependency = scope.getDependency($signal.id);

                if(dependency != false){
                    var fn = scope.functions[dependency.fn];
                    if(fn != undefined){
                        fn(dependency.fnArgs, $signal);
                        scope.go($signal.init);
                    }
                }
            });

            scope.go = function(init) {
                init = typeof init == 'undefined' ? false : init;

                if(scope.value != null){
                    scope.field.summaryValue = scope.field.price * scope.value * scope.field.discount;

                    if (isNaN(scope.field.summaryValue)) {
                        scope.field.summaryValue = '---'
                    }
                }
                scope.safeApply();

                if (scope.value != '') {
                    $rootScope.$broadcast('update', {
                        id : scope.config.id,
                        field: scope.field,
                        inputValue: scope.value,
                        properties: scope.config.properties,
                        parent: scope.config.parent,
                        init: init
                    });
                }
            };

            scope.getDependency = function(id){
                var result = false;
                angular.forEach(scope.config.dependencies, function(dependency) {
                    if(dependency.id == id){
                        result = dependency;
                    }
                });

                return result;
            };

            scope.functions = {
                inherit : function($args, $signal){
                    angular.forEach(scope.field, function(value, key) {
                        if($signal.properties[key] != undefined){
                            scope.field[key] = $signal.properties[key];
                        }
                    });
                }
            };

            scope.normalize = function($value){
                return parseFloat(parseFloat($value).toFixed(2));
            };

            scope.safeApply = function() {
                var phase = this.$root.$$phase;
                if(phase != '$apply' && phase != '$digest') {
                    this.$apply();
                }
            };

            scope.reset = function(){
                scope.value = '';
                scope.field.summaryValue = '0';
                scope.safeApply();

                $rootScope.$broadcast('update', {
                    id : scope.config.id,
                    field: scope.field,
                    inputValue: scope.value,
                    properties: scope.config.properties
                });
            };

            angular.element(document).ready(function(){
                if(scope.value){
                    scope.go(true);
                }
            });
        }
    };
});
/**
 * @augments Protoplan.Block
 * @class Protoplan.Tooltip
 * @param params
 * @constructor
 */

Protoplan.Tooltip = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.selector = params.selector != undefined ? params.selector : '';
    self.target = params.target != undefined ? params.target : '';

    self.init = function ()
    {
        $(self.target).hover(function ()
        {
            self.show();
        }, function ()
        { 
            self.hide();
        });
    };

    self.show = function ()
    {
        setTimeout(function ()
        {
            $(self.selector).fadeIn(300);
        }, 500);
    };

    self.hide = function ()
    {
        setTimeout(function ()
        {
            $(self.selector).fadeOut(300);
        }, 500);
    };

    self.init();
};

pp.module('tooltip', []).directive('tooltip', function() {
    return {
        templateUrl: pp.viewPath('tooltip'),
        restrict: 'E',
        scope:
        {
            "tooltip": '='
        },
        link: function (scope, element, attrs)
        {
            var tooltipIcon = '';

            if (scope.tooltip.icon != undefined) {
                tooltipIcon = scope.tooltip.icon;
            }

            $($(element).find('.tooltipster')).tooltipster({
                content: $('<div class="tooltip-wrap"><button class="close"></button>' + scope.tooltip.content + '</div>'),
                trigger: 'click',
                delay: 0,
                speed: 0,
                position: 'bottom'
            });
        }
    };
});
/*

 Tooltipster 3.3.0 | 2014-11-08
 A rockin' custom tooltip jQuery plugin

 Developed by Caleb Jacob under the MIT license http://opensource.org/licenses/MIT

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

;(function ($, window, document) {

    var pluginName = "tooltipster",
        defaults = {
            animation: 'fade',
            arrow: true,
            arrowColor: '',
            autoClose: true,
            content: null,
            contentAsHTML: false,
            contentCloning: true,
            debug: true,
            delay: 200,
            minWidth: 0,
            maxWidth: null,
            functionInit: function(origin, content) {},
            functionBefore: function(origin, continueTooltip) {
                continueTooltip();
            },
            functionReady: function(origin, tooltip) {},
            functionAfter: function(origin) {},
            hideOnClick: false,
            icon: '(?)',
            iconCloning: true,
            iconDesktop: false,
            iconTouch: false,
            iconTheme: 'tooltipster-icon',
            interactive: false,
            interactiveTolerance: 350,
            multiple: false,
            offsetX: 0,
            offsetY: 0,
            onlyOne: false,
            position: 'top',
            positionTracker: false,
            positionTrackerCallback: function(origin){
                // the default tracker callback will close the tooltip when the trigger is
                // 'hover' (see https://github.com/iamceege/tooltipster/pull/253)
                if(this.option('trigger') == 'hover' && this.option('autoClose')) {
                    this.hide();
                }
            },
            restoration: 'current',
            speed: 350,
            timer: 0,
            theme: 'tooltipster-default',
            touchDevices: true,
            trigger: 'hover',
            updateAnimation: true
        };

    function Plugin(element, options) {

        // list of instance variables

        this.bodyOverflowX;
        // stack of custom callbacks provided as parameters to API methods
        this.callbacks = {
            hide: [],
            show: []
        };
        this.checkInterval = null;
        // this will be the user content shown in the tooltip. A capital "C" is used because there is also a method called content()
        this.Content;
        // this is the original element which is being applied the tooltipster plugin
        this.$el = $(element);
        // this will be the element which triggers the appearance of the tooltip on hover/click/custom events.
        // it will be the same as this.$el if icons are not used (see in the options), otherwise it will correspond to the created icon
        this.$elProxy;
        this.elProxyPosition;
        this.enabled = true;
        this.options = $.extend({}, defaults, options);
        this.mouseIsOverProxy = false;
        // a unique namespace per instance, for easy selective unbinding
        this.namespace = 'tooltipster-'+ Math.round(Math.random()*100000);
        // Status (capital S) can be either : appearing, shown, disappearing, hidden
        this.Status = 'hidden';
        this.timerHide = null;
        this.timerShow = null;
        // this will be the tooltip element (jQuery wrapped HTML element)
        this.$tooltip;

        // for backward compatibility
        this.options.iconTheme = this.options.iconTheme.replace('.', '');
        this.options.theme = this.options.theme.replace('.', '');

        // launch

        this._init();
    }

    Plugin.prototype = {

        _init: function() {

            var self = this;

            // disable the plugin on old browsers (including IE7 and lower)
            if (document.querySelector) {

                // note : the content is null (empty) by default and can stay that way if the plugin remains initialized but not fed any content. The tooltip will just not appear.

                // let's save the initial value of the title attribute for later restoration if need be.
                var initialTitle = null;
                // it will already have been saved in case of multiple tooltips
                if (self.$el.data('tooltipster-initialTitle') === undefined) {

                    initialTitle = self.$el.attr('title');

                    // we do not want initialTitle to have the value "undefined" because of how jQuery's .data() method works
                    if (initialTitle === undefined) initialTitle = null;

                    self.$el.data('tooltipster-initialTitle', initialTitle);
                }

                // if content is provided in the options, its has precedence over the title attribute.
                // Note : an empty string is considered content, only 'null' represents the absence of content.
                // Also, an existing title="" attribute will result in an empty string content
                if (self.options.content !== null){
                    self._content_set(self.options.content);
                }
                else {
                    self._content_set(initialTitle);
                }

                var c = self.options.functionInit.call(self.$el, self.$el, self.Content);
                if(typeof c !== 'undefined') self._content_set(c);

                self.$el
                // strip the title off of the element to prevent the default tooltips from popping up
                    .removeAttr('title')
                    // to be able to find all instances on the page later (upon window events in particular)
                    .addClass('tooltipstered');

                // detect if we're changing the tooltip origin to an icon
                // note about this condition : if the device has touch capability and self.options.iconTouch is false, you'll have no icons event though you may consider your device as a desktop if it also has a mouse. Not sure why someone would have this use case though.
                if ((!deviceHasTouchCapability && self.options.iconDesktop) || (deviceHasTouchCapability && self.options.iconTouch)) {

                    // TODO : the tooltip should be automatically be given an absolute position to be near the origin. Otherwise, when the origin is floating or what, it's going to be nowhere near it and disturb the position flow of the page elements. It will imply that the icon also detects when its origin moves, to follow it : not trivial.
                    // Until it's done, the icon feature does not really make sense since the user still has most of the work to do by himself

                    // if the icon provided is in the form of a string
                    if(typeof self.options.icon === 'string'){
                        // wrap it in a span with the icon class
                        self.$elProxy = $('<span class="'+ self.options.iconTheme +'"></span>');
                        self.$elProxy.text(self.options.icon);
                    }
                    // if it is an object (sensible choice)
                    else {
                        // (deep) clone the object if iconCloning == true, to make sure every instance has its own proxy. We use the icon without wrapping, no need to. We do not give it a class either, as the user will undoubtedly style the object on his own and since our css properties may conflict with his own
                        if (self.options.iconCloning) self.$elProxy = self.options.icon.clone(true);
                        else self.$elProxy = self.options.icon;
                    }

                    self.$elProxy.insertAfter(self.$el);
                }
                else {
                    self.$elProxy = self.$el;
                }

                // for 'click' and 'hover' triggers : bind on events to open the tooltip. Closing is now handled in _showNow() because of its bindings.
                // Notes about touch events :
                // - mouseenter, mouseleave and clicks happen even on pure touch devices because they are emulated. deviceIsPureTouch() is a simple attempt to detect them.
                // - on hybrid devices, we do not prevent touch gesture from opening tooltips. It would be too complex to differentiate real mouse events from emulated ones.
                // - we check deviceIsPureTouch() at each event rather than prior to binding because the situation may change during browsing
                if (self.options.trigger == 'hover') {

                    // these binding are for mouse interaction only
                    self.$elProxy
                        .on('mouseenter.'+ self.namespace, function() {
                            if (!deviceIsPureTouch() || self.options.touchDevices) {
                                self.mouseIsOverProxy = true;
                                self._show();
                            }
                        })
                        .on('mouseleave.'+ self.namespace, function() {
                            if (!deviceIsPureTouch() || self.options.touchDevices) {
                                self.mouseIsOverProxy = false;
                            }
                        });

                    // for touch interaction only
                    if (deviceHasTouchCapability && self.options.touchDevices) {

                        // for touch devices, we immediately display the tooltip because we cannot rely on mouseleave to handle the delay
                        self.$elProxy.on('touchstart.'+ self.namespace, function() {
                            self._showNow();
                        });
                    }
                }
                else if (self.options.trigger == 'click') {

                    // note : for touch devices, we do not bind on touchstart, we only rely on the emulated clicks (triggered by taps)
                    self.$elProxy.on('click.'+ self.namespace, function() {
                        if (!deviceIsPureTouch() || self.options.touchDevices) {
                            self._show();
                        }
                    });
                }
            }
        },

        // this function will schedule the opening of the tooltip after the delay, if there is one
        _show: function() {

            var self = this;

            if (self.Status != 'shown' && self.Status != 'appearing') {

                if (self.options.delay) {
                    self.timerShow = setTimeout(function(){

                        // for hover trigger, we check if the mouse is still over the proxy, otherwise we do not show anything
                        if (self.options.trigger == 'click' || (self.options.trigger == 'hover' && self.mouseIsOverProxy)) {
                            self._showNow();
                        }
                    }, self.options.delay);
                }
                else self._showNow();
            }
        },

        // this function will open the tooltip right away
        _showNow: function(callback) {

            var self = this;

            // call our constructor custom function before continuing
            self.options.functionBefore.call(self.$el, self.$el, function() {

                // continue only if the tooltip is enabled and has any content
                if (self.enabled && self.Content !== null) {

                    // save the method callback and cancel hide method callbacks
                    if (callback) self.callbacks.show.push(callback);
                    self.callbacks.hide = [];

                    //get rid of any appearance timer
                    clearTimeout(self.timerShow);
                    self.timerShow = null;
                    clearTimeout(self.timerHide);
                    self.timerHide = null;

                    // if we only want one tooltip open at a time, close all auto-closing tooltips currently open and not already disappearing
                    if (self.options.onlyOne) {
                        $('.tooltipstered').not(self.$el).each(function(i,el) {

                            var $el = $(el),
                                nss = $el.data('tooltipster-ns');

                            // iterate on all tooltips of the element
                            $.each(nss, function(i, ns){
                                var instance = $el.data(ns),
                                // we have to use the public methods here
                                    s = instance.status(),
                                    ac = instance.option('autoClose');

                                if (s !== 'hidden' && s !== 'disappearing' && ac) {
                                    instance.hide();
                                }
                            });
                        });
                    }

                    var finish = function() {
                        self.Status = 'shown';

                        // trigger any show method custom callbacks and reset them
                        $.each(self.callbacks.show, function(i,c) { c.call(self.$el); });
                        self.callbacks.show = [];
                    };

                    // if this origin already has its tooltip open
                    if (self.Status !== 'hidden') {

                        // the timer (if any) will start (or restart) right now
                        var extraTime = 0;

                        // if it was disappearing, cancel that
                        if (self.Status === 'disappearing') {

                            self.Status = 'appearing';

                            if (supportsTransitions()) {

                                self.$tooltip
                                    .clearQueue()
                                    .removeClass('tooltipster-dying')
                                    .addClass('tooltipster-'+ self.options.animation +'-show');

                                if (self.options.speed > 0) self.$tooltip.delay(self.options.speed);

                                self.$tooltip.queue(finish);
                            }
                            else {
                                // in case the tooltip was currently fading out, bring it back to life
                                self.$tooltip
                                    .stop()
                                    .fadeIn(finish);
                            }
                        }
                        // if the tooltip is already open, we still need to trigger the method custom callback
                        else if(self.Status === 'shown') {
                            finish();
                        }
                    }
                    // if the tooltip isn't already open, open that sucker up!
                    else {

                        self.Status = 'appearing';

                        // the timer (if any) will start when the tooltip has fully appeared after its transition
                        var extraTime = self.options.speed;

                        // disable horizontal scrollbar to keep overflowing tooltips from jacking with it and then restore it to its previous value
                        self.bodyOverflowX = $('body').css('overflow-x');
                        $('body').css('overflow-x', 'hidden');

                        // get some other settings related to building the tooltip
                        var animation = 'tooltipster-' + self.options.animation,
                            animationSpeed = '-webkit-transition-duration: '+ self.options.speed +'ms; -webkit-animation-duration: '+ self.options.speed +'ms; -moz-transition-duration: '+ self.options.speed +'ms; -moz-animation-duration: '+ self.options.speed +'ms; -o-transition-duration: '+ self.options.speed +'ms; -o-animation-duration: '+ self.options.speed +'ms; -ms-transition-duration: '+ self.options.speed +'ms; -ms-animation-duration: '+ self.options.speed +'ms; transition-duration: '+ self.options.speed +'ms; animation-duration: '+ self.options.speed +'ms;',
                            minWidth = self.options.minWidth ? 'min-width:'+ Math.round(self.options.minWidth) +'px;' : '',
                            maxWidth = self.options.maxWidth ? 'max-width:'+ Math.round(self.options.maxWidth) +'px;' : '',
                            pointerEvents = self.options.interactive ? 'pointer-events: auto;' : '';

                        // build the base of our tooltip
                        self.$tooltip = $('<div class="tooltipster-base '+ self.options.theme +'" style="'+ minWidth +' '+ maxWidth +' '+ pointerEvents +' '+ animationSpeed +'"><div class="tooltipster-content"></div></div>');

                        // only add the animation class if the user has a browser that supports animations
                        if (supportsTransitions()) self.$tooltip.addClass(animation);

                        // insert the content
                        self._content_insert();

                        // attach
                        self.$tooltip.appendTo('body');

                        // do all the crazy calculations and positioning
                        self.reposition();

                        // call our custom callback since the content of the tooltip is now part of the DOM
                        self.options.functionReady.call(self.$el, self.$el, self.$tooltip);

                        // animate in the tooltip
                        if (supportsTransitions()) {

                            self.$tooltip.addClass(animation + '-show');

                            if(self.options.speed > 0) self.$tooltip.delay(self.options.speed);

                            self.$tooltip.queue(finish);
                        }
                        else {
                            self.$tooltip.css('display', 'none').fadeIn(self.options.speed, finish);
                        }

                        // will check if our tooltip origin is removed while the tooltip is shown
                        self._interval_set();

                        // reposition on scroll (otherwise position:fixed element's tooltips will move away form their origin) and on resize (in case position can/has to be changed)
                        //@TODO DELETE IT
                        // $(window).on('scroll.'+ self.namespace +' resize.'+ self.namespace, function() {
                        //     self.reposition();
                        // });

                        // auto-close bindings
                        if (self.options.autoClose) {

                            // in case a listener is already bound for autoclosing (mouse or touch, hover or click), unbind it first
                            $('body').off('.'+ self.namespace);

                            // here we'll have to set different sets of bindings for both touch and mouse
                            if (self.options.trigger == 'hover') {

                                // if the user touches the body, hide
                                if (deviceHasTouchCapability) {
                                    // timeout 0 : explanation below in click section
                                    setTimeout(function() {
                                        // we don't want to bind on click here because the initial touchstart event has not yet triggered its click event, which is thus about to happen
                                        $('body').on('touchstart.'+ self.namespace, function() {
                                            self.hide();
                                        });
                                    }, 0);
                                }

                                // if we have to allow interaction
                                if (self.options.interactive) {

                                    // touch events inside the tooltip must not close it
                                    if (deviceHasTouchCapability) {
                                        self.$tooltip.on('touchstart.'+ self.namespace, function(event) {
                                            event.stopPropagation();
                                        });
                                    }

                                    // as for mouse interaction, we get rid of the tooltip only after the mouse has spent some time out of it
                                    var tolerance = null;

                                    self.$elProxy.add(self.$tooltip)
                                    // hide after some time out of the proxy and the tooltip
                                        .on('mouseleave.'+ self.namespace + '-autoClose', function() {
                                            clearTimeout(tolerance);
                                            tolerance = setTimeout(function(){
                                                self.hide();
                                            }, self.options.interactiveTolerance);
                                        })
                                        // suspend timeout when the mouse is over the proxy or the tooltip
                                        .on('mouseenter.'+ self.namespace + '-autoClose', function() {
                                            clearTimeout(tolerance);
                                        });
                                }
                                // if this is a non-interactive tooltip, get rid of it if the mouse leaves
                                else {
                                    self.$elProxy.on('mouseleave.'+ self.namespace + '-autoClose', function() {
                                        self.hide();
                                    });
                                }

                                // close the tooltip when the proxy gets a click (common behavior of native tooltips)
                                if (self.options.hideOnClick) {

                                    self.$elProxy.on('click.'+ self.namespace + '-autoClose', function() {
                                        self.hide();
                                    });
                                }
                            }
                            // here we'll set the same bindings for both clicks and touch on the body to hide the tooltip
                            else if(self.options.trigger == 'click'){

                                // use a timeout to prevent immediate closing if the method was called on a click event and if options.delay == 0 (because of bubbling)
                                setTimeout(function() {
                                    $('body').on('click.'+ self.namespace +' touchstart.'+ self.namespace, function() {
                                        self.hide();
                                    });
                                }, 0);

                                // if interactive, we'll stop the events that were emitted from inside the tooltip to stop autoClosing
                                if (self.options.interactive) {

                                    // note : the touch events will just not be used if the plugin is not enabled on touch devices
                                    self.$tooltip.on('click.'+ self.namespace +' touchstart.'+ self.namespace, function(event) {
                                        event.stopPropagation();
                                    });
                                }
                            }
                        }
                    }

                    // if we have a timer set, let the countdown begin
                    if (self.options.timer > 0) {

                        self.timerHide = setTimeout(function() {
                            self.timerHide = null;
                            self.hide();
                        }, self.options.timer + extraTime);
                    }
                }
            });
        },

        _interval_set: function() {

            var self = this;

            self.checkInterval = setInterval(function() {

                // if the tooltip and/or its interval should be stopped
                if (
                    // if the origin has been removed
                $('body').find(self.$el).length === 0
                // if the elProxy has been removed
                ||	$('body').find(self.$elProxy).length === 0
                // if the tooltip has been closed
                ||	self.Status == 'hidden'
                // if the tooltip has somehow been removed
                ||	$('body').find(self.$tooltip).length === 0
                ) {
                    // remove the tooltip if it's still here
                    if (self.Status == 'shown' || self.Status == 'appearing') self.hide();

                    // clear this interval as it is no longer necessary
                    self._interval_cancel();
                }
                // if everything is alright
                else {
                    // compare the former and current positions of the elProxy to reposition the tooltip if need be
                    if(self.options.positionTracker){

                        var p = self._repositionInfo(self.$elProxy),
                            identical = false;

                        // compare size first (a change requires repositioning too)
                        if(areEqual(p.dimension, self.elProxyPosition.dimension)){

                            // for elements with a fixed position, we track the top and left properties (relative to window)
                            if(self.$elProxy.css('position') === 'fixed'){
                                if(areEqual(p.position, self.elProxyPosition.position)) identical = true;
                            }
                            // otherwise, track total offset (relative to document)
                            else {
                                if(areEqual(p.offset, self.elProxyPosition.offset)) identical = true;
                            }
                        }

                        if(!identical){
                            self.reposition();
                            self.options.positionTrackerCallback.call(self, self.$el);
                        }
                    }
                }
            }, 200);
        },

        _interval_cancel: function() {
            clearInterval(this.checkInterval);
            // clean delete
            this.checkInterval = null;
        },

        _content_set: function(content) {
            // clone if asked. Cloning the object makes sure that each instance has its own version of the content (in case a same object were provided for several instances)
            // reminder : typeof null === object
            if (typeof content === 'object' && content !== null && this.options.contentCloning) {
                content = content.clone(true);
            }
            this.Content = content;
        },

        _content_insert: function() {

            var self = this,
                $d = this.$tooltip.find('.tooltipster-content');

            if (typeof self.Content === 'string' && !self.options.contentAsHTML) {
                $d.text(self.Content);
            }
            else {
                $d
                    .empty()
                    .append(self.Content);
            }
        },

        _update: function(content) {

            var self = this;

            // change the content
            self._content_set(content);

            if (self.Content !== null) {

                // update the tooltip if it is open
                if (self.Status !== 'hidden') {

                    // reset the content in the tooltip
                    self._content_insert();

                    // reposition and resize the tooltip
                    self.reposition();

                    // if we want to play a little animation showing the content changed
                    if (self.options.updateAnimation) {

                        if (supportsTransitions()) {

                            self.$tooltip.css({
                                'width': '',
                                '-webkit-transition': 'all ' + self.options.speed + 'ms, width 0ms, height 0ms, left 0ms, top 0ms',
                                '-moz-transition': 'all ' + self.options.speed + 'ms, width 0ms, height 0ms, left 0ms, top 0ms',
                                '-o-transition': 'all ' + self.options.speed + 'ms, width 0ms, height 0ms, left 0ms, top 0ms',
                                '-ms-transition': 'all ' + self.options.speed + 'ms, width 0ms, height 0ms, left 0ms, top 0ms',
                                'transition': 'all ' + self.options.speed + 'ms, width 0ms, height 0ms, left 0ms, top 0ms'
                            }).addClass('tooltipster-content-changing');

                            // reset the CSS transitions and finish the change animation
                            setTimeout(function() {

                                if(self.Status != 'hidden'){

                                    self.$tooltip.removeClass('tooltipster-content-changing');

                                    // after the changing animation has completed, reset the CSS transitions
                                    setTimeout(function() {

                                        if(self.Status !== 'hidden'){
                                            self.$tooltip.css({
                                                '-webkit-transition': self.options.speed + 'ms',
                                                '-moz-transition': self.options.speed + 'ms',
                                                '-o-transition': self.options.speed + 'ms',
                                                '-ms-transition': self.options.speed + 'ms',
                                                'transition': self.options.speed + 'ms'
                                            });
                                        }
                                    }, self.options.speed);
                                }
                            }, self.options.speed);
                        }
                        else {
                            self.$tooltip.fadeTo(self.options.speed, 0.5, function() {
                                if(self.Status != 'hidden'){
                                    self.$tooltip.fadeTo(self.options.speed, 1);
                                }
                            });
                        }
                    }
                }
            }
            else {
                self.hide();
            }
        },

        _repositionInfo: function($el) {
            return {
                dimension: {
                    height: $el.outerHeight(false),
                    width: $el.outerWidth(false)
                },
                offset: $el.offset(),
                position: {
                    left: parseInt($el.css('left')),
                    top: parseInt($el.css('top'))
                }
            };
        },

        hide: function(callback) {

            var self = this;

            // save the method custom callback and cancel any show method custom callbacks
            if (callback) self.callbacks.hide.push(callback);
            self.callbacks.show = [];

            // get rid of any appearance timeout
            clearTimeout(self.timerShow);
            self.timerShow = null;
            clearTimeout(self.timerHide);
            self.timerHide = null;

            var finishCallbacks = function() {
                // trigger any hide method custom callbacks and reset them
                $.each(self.callbacks.hide, function(i,c) { c.call(self.$el); });
                self.callbacks.hide = [];
            };

            // hide
            if (self.Status == 'shown' || self.Status == 'appearing') {

                self.Status = 'disappearing';

                var finish = function() {

                    self.Status = 'hidden';

                    // detach our content object first, so the next jQuery's remove() call does not unbind its event handlers
                    if (typeof self.Content == 'object' && self.Content !== null) {
                        self.Content.detach();
                    }

                    self.$tooltip.remove();
                    self.$tooltip = null;

                    // unbind orientationchange, scroll and resize listeners
                    $(window).off('.'+ self.namespace);

                    $('body')
                    // unbind any auto-closing click/touch listeners
                        .off('.'+ self.namespace)
                        .css('overflow-x', self.bodyOverflowX);

                    // unbind any auto-closing click/touch listeners
                    $('body').off('.'+ self.namespace);

                    // unbind any auto-closing hover listeners
                    self.$elProxy.off('.'+ self.namespace + '-autoClose');

                    // call our constructor custom callback function
                    self.options.functionAfter.call(self.$el, self.$el);

                    // call our method custom callbacks functions
                    finishCallbacks();
                };

                if (supportsTransitions()) {

                    self.$tooltip
                        .clearQueue()
                        .removeClass('tooltipster-' + self.options.animation + '-show')
                        // for transitions only
                        .addClass('tooltipster-dying');

                    if(self.options.speed > 0) self.$tooltip.delay(self.options.speed);

                    self.$tooltip.queue(finish);
                }
                else {
                    self.$tooltip
                        .stop()
                        .fadeOut(self.options.speed, finish);
                }
            }
            // if the tooltip is already hidden, we still need to trigger the method custom callback
            else if(self.Status == 'hidden') {
                finishCallbacks();
            }

            return self;
        },

        // the public show() method is actually an alias for the private showNow() method
        show: function(callback) {
            this._showNow(callback);
            return this;
        },

        // 'update' is deprecated in favor of 'content' but is kept for backward compatibility
        update: function(c) {
            return this.content(c);
        },
        content: function(c) {
            // getter method
            if(typeof c === 'undefined'){
                return this.Content;
            }
            // setter method
            else {
                this._update(c);
                return this;
            }
        },

        reposition: function() {

            var self = this;

            // in case the tooltip has been removed from DOM manually
            if ($('body').find(self.$tooltip).length !== 0) {

                // reset width
                self.$tooltip.css('width', '');

                // find variables to determine placement
                self.elProxyPosition = self._repositionInfo(self.$elProxy);
                var arrowReposition = null,
                    windowWidth = $(window).width(),
                // shorthand
                    proxy = self.elProxyPosition,
                    tooltipWidth = self.$tooltip.outerWidth(false),
                    tooltipInnerWidth = self.$tooltip.innerWidth() + 1, // this +1 stops FireFox from sometimes forcing an additional text line
                    tooltipHeight = self.$tooltip.outerHeight(false);

                // if this is an <area> tag inside a <map>, all hell breaks loose. Recalculate all the measurements based on coordinates
                if (self.$elProxy.is('area')) {
                    var areaShape = self.$elProxy.attr('shape'),
                        mapName = self.$elProxy.parent().attr('name'),
                        map = $('img[usemap="#'+ mapName +'"]'),
                        mapOffsetLeft = map.offset().left,
                        mapOffsetTop = map.offset().top,
                        areaMeasurements = self.$elProxy.attr('coords') !== undefined ? self.$elProxy.attr('coords').split(',') : undefined;

                    if (areaShape == 'circle') {
                        var areaLeft = parseInt(areaMeasurements[0]),
                            areaTop = parseInt(areaMeasurements[1]),
                            areaWidth = parseInt(areaMeasurements[2]);
                        proxy.dimension.height = areaWidth * 2;
                        proxy.dimension.width = areaWidth * 2;
                        proxy.offset.top = mapOffsetTop + areaTop - areaWidth;
                        proxy.offset.left = mapOffsetLeft + areaLeft - areaWidth;
                    }
                    else if (areaShape == 'rect') {
                        var areaLeft = parseInt(areaMeasurements[0]),
                            areaTop = parseInt(areaMeasurements[1]),
                            areaRight = parseInt(areaMeasurements[2]),
                            areaBottom = parseInt(areaMeasurements[3]);
                        proxy.dimension.height = areaBottom - areaTop;
                        proxy.dimension.width = areaRight - areaLeft;
                        proxy.offset.top = mapOffsetTop + areaTop;
                        proxy.offset.left = mapOffsetLeft + areaLeft;
                    }
                    else if (areaShape == 'poly') {
                        var areaXs = [],
                            areaYs = [],
                            areaSmallestX = 0,
                            areaSmallestY = 0,
                            areaGreatestX = 0,
                            areaGreatestY = 0,
                            arrayAlternate = 'even';

                        for (var i = 0; i < areaMeasurements.length; i++) {
                            var areaNumber = parseInt(areaMeasurements[i]);

                            if (arrayAlternate == 'even') {
                                if (areaNumber > areaGreatestX) {
                                    areaGreatestX = areaNumber;
                                    if (i === 0) {
                                        areaSmallestX = areaGreatestX;
                                    }
                                }

                                if (areaNumber < areaSmallestX) {
                                    areaSmallestX = areaNumber;
                                }

                                arrayAlternate = 'odd';
                            }
                            else {
                                if (areaNumber > areaGreatestY) {
                                    areaGreatestY = areaNumber;
                                    if (i == 1) {
                                        areaSmallestY = areaGreatestY;
                                    }
                                }

                                if (areaNumber < areaSmallestY) {
                                    areaSmallestY = areaNumber;
                                }

                                arrayAlternate = 'even';
                            }
                        }

                        proxy.dimension.height = areaGreatestY - areaSmallestY;
                        proxy.dimension.width = areaGreatestX - areaSmallestX;
                        proxy.offset.top = mapOffsetTop + areaSmallestY;
                        proxy.offset.left = mapOffsetLeft + areaSmallestX;
                    }
                    else {
                        proxy.dimension.height = map.outerHeight(false);
                        proxy.dimension.width = map.outerWidth(false);
                        proxy.offset.top = mapOffsetTop;
                        proxy.offset.left = mapOffsetLeft;
                    }
                }

                // our function and global vars for positioning our tooltip
                var myLeft = 0,
                    myLeftMirror = 0,
                    myTop = 0,
                    offsetY = parseInt(self.options.offsetY),
                    offsetX = parseInt(self.options.offsetX),
                // this is the arrow position that will eventually be used. It may differ from the position option if the tooltip cannot be displayed in this position
                    practicalPosition = self.options.position;

                // a function to detect if the tooltip is going off the screen horizontally. If so, reposition the crap out of it!
                function dontGoOffScreenX() {

                    var windowLeft = $(window).scrollLeft();

                    // if the tooltip goes off the left side of the screen, line it up with the left side of the window
                    if((myLeft - windowLeft) < 0) {
                        arrowReposition = myLeft - windowLeft;
                        myLeft = windowLeft;
                    }

                    // if the tooltip goes off the right of the screen, line it up with the right side of the window
                    if (((myLeft + tooltipWidth) - windowLeft) > windowWidth) {
                        arrowReposition = myLeft - ((windowWidth + windowLeft) - tooltipWidth);
                        myLeft = (windowWidth + windowLeft) - tooltipWidth;
                    }
                }

                // a function to detect if the tooltip is going off the screen vertically. If so, switch to the opposite!
                function dontGoOffScreenY(switchTo, switchFrom) {
                    // if it goes off the top off the page
                    if(((proxy.offset.top - $(window).scrollTop() - tooltipHeight - offsetY - 12) < 0) && (switchFrom.indexOf('top') > -1)) {
                        practicalPosition = switchTo;
                    }

                    // if it goes off the bottom of the page
                    if (((proxy.offset.top + proxy.dimension.height + tooltipHeight + 12 + offsetY) > ($(window).scrollTop() + $(window).height())) && (switchFrom.indexOf('bottom') > -1)) {
                        practicalPosition = switchTo;
                        myTop = (proxy.offset.top - tooltipHeight) - offsetY - 12;
                    }
                }

                if(practicalPosition == 'top') {
                    var leftDifference = (proxy.offset.left + tooltipWidth) - (proxy.offset.left + proxy.dimension.width);
                    myLeft = (proxy.offset.left + offsetX) - (leftDifference / 2);
                    myTop = (proxy.offset.top - tooltipHeight) - offsetY - 12;
                    dontGoOffScreenX();
                    dontGoOffScreenY('bottom', 'top');
                }

                if(practicalPosition == 'top-left') {
                    myLeft = proxy.offset.left + offsetX;
                    myTop = (proxy.offset.top - tooltipHeight) - offsetY - 12;
                    dontGoOffScreenX();
                    dontGoOffScreenY('bottom-left', 'top-left');
                }

                if(practicalPosition == 'top-right') {
                    myLeft = (proxy.offset.left + proxy.dimension.width + offsetX) - tooltipWidth;
                    myTop = (proxy.offset.top - tooltipHeight) - offsetY - 12;
                    dontGoOffScreenX();
                    dontGoOffScreenY('bottom-right', 'top-right');
                }

                if(practicalPosition == 'bottom') {
                    var leftDifference = (proxy.offset.left + tooltipWidth) - (proxy.offset.left + proxy.dimension.width);
                    myLeft = proxy.offset.left - (leftDifference / 2) + offsetX;
                    myTop = (proxy.offset.top + proxy.dimension.height) + offsetY + 12;
                    // dontGoOffScreenX();
                    // dontGoOffScreenY('top', 'bottom');
                }

                if(practicalPosition == 'bottom-left') {
                    myLeft = proxy.offset.left + offsetX;
                    myTop = (proxy.offset.top + proxy.dimension.height) + offsetY + 12;
                    dontGoOffScreenX();
                    dontGoOffScreenY('top-left', 'bottom-left');
                }

                if(practicalPosition == 'bottom-right') {
                    myLeft = (proxy.offset.left + proxy.dimension.width + offsetX) - tooltipWidth;
                    myTop = (proxy.offset.top + proxy.dimension.height) + offsetY + 12;
                    dontGoOffScreenX();
                    dontGoOffScreenY('top-right', 'bottom-right');
                }

                if(practicalPosition == 'left') {
                    myLeft = proxy.offset.left - offsetX - tooltipWidth - 12;
                    myLeftMirror = proxy.offset.left + offsetX + proxy.dimension.width + 12;
                    var topDifference = (proxy.offset.top + tooltipHeight) - (proxy.offset.top + proxy.dimension.height);
                    myTop = proxy.offset.top - (topDifference / 2) - offsetY;

                    // if the tooltip goes off boths sides of the page
                    if((myLeft < 0) && ((myLeftMirror + tooltipWidth) > windowWidth)) {
                        var borderWidth = parseFloat(self.$tooltip.css('border-width')) * 2,
                            newWidth = (tooltipWidth + myLeft) - borderWidth;
                        self.$tooltip.css('width', newWidth + 'px');

                        tooltipHeight = self.$tooltip.outerHeight(false);
                        myLeft = proxy.offset.left - offsetX - newWidth - 12 - borderWidth;
                        topDifference = (proxy.offset.top + tooltipHeight) - (proxy.offset.top + proxy.dimension.height);
                        myTop = proxy.offset.top - (topDifference / 2) - offsetY;
                    }

                    // if it only goes off one side, flip it to the other side
                    else if(myLeft < 0) {
                        myLeft = proxy.offset.left + offsetX + proxy.dimension.width + 12;
                        arrowReposition = 'left';
                    }
                }

                if(practicalPosition == 'right') {
                    myLeft = proxy.offset.left + offsetX + proxy.dimension.width + 12;
                    myLeftMirror = proxy.offset.left - offsetX - tooltipWidth - 12;
                    var topDifference = (proxy.offset.top + tooltipHeight) - (proxy.offset.top + proxy.dimension.height);
                    myTop = proxy.offset.top - (topDifference / 2) - offsetY;

                    // if the tooltip goes off boths sides of the page
                    if(((myLeft + tooltipWidth) > windowWidth) && (myLeftMirror < 0)) {
                        var borderWidth = parseFloat(self.$tooltip.css('border-width')) * 2,
                            newWidth = (windowWidth - myLeft) - borderWidth;
                        self.$tooltip.css('width', newWidth + 'px');

                        tooltipHeight = self.$tooltip.outerHeight(false);
                        topDifference = (proxy.offset.top + tooltipHeight) - (proxy.offset.top + proxy.dimension.height);
                        myTop = proxy.offset.top - (topDifference / 2) - offsetY;
                    }

                    // if it only goes off one side, flip it to the other side
                    else if((myLeft + tooltipWidth) > windowWidth) {
                        myLeft = proxy.offset.left - offsetX - tooltipWidth - 12;
                        arrowReposition = 'right';
                    }
                }

                // if arrow is set true, style it and append it
                if (self.options.arrow) {

                    var arrowClass = 'tooltipster-arrow-' + practicalPosition;

                    // set color of the arrow
                    if(self.options.arrowColor.length < 1) {
                        var arrowColor = self.$tooltip.css('background-color');
                    }
                    else {
                        var arrowColor = self.options.arrowColor;
                    }

                    // if the tooltip was going off the page and had to re-adjust, we need to update the arrow's position
                    if (!arrowReposition) {
                        arrowReposition = '';
                    }
                    else if (arrowReposition == 'left') {
                        arrowClass = 'tooltipster-arrow-right';
                        arrowReposition = '';
                    }
                    else if (arrowReposition == 'right') {
                        arrowClass = 'tooltipster-arrow-left';
                        arrowReposition = '';
                    }
                    else {
                        arrowReposition = 'left:'+ Math.round(arrowReposition) +'px;';
                    }

                    // building the logic to create the border around the arrow of the tooltip
                    if ((practicalPosition == 'top') || (practicalPosition == 'top-left') || (practicalPosition == 'top-right')) {
                        var tooltipBorderWidth = parseFloat(self.$tooltip.css('border-bottom-width')),
                            tooltipBorderColor = self.$tooltip.css('border-bottom-color');
                    }
                    else if ((practicalPosition == 'bottom') || (practicalPosition == 'bottom-left') || (practicalPosition == 'bottom-right')) {
                        var tooltipBorderWidth = parseFloat(self.$tooltip.css('border-top-width')),
                            tooltipBorderColor = self.$tooltip.css('border-top-color');
                    }
                    else if (practicalPosition == 'left') {
                        var tooltipBorderWidth = parseFloat(self.$tooltip.css('border-right-width')),
                            tooltipBorderColor = self.$tooltip.css('border-right-color');
                    }
                    else if (practicalPosition == 'right') {
                        var tooltipBorderWidth = parseFloat(self.$tooltip.css('border-left-width')),
                            tooltipBorderColor = self.$tooltip.css('border-left-color');
                    }
                    else {
                        var tooltipBorderWidth = parseFloat(self.$tooltip.css('border-bottom-width')),
                            tooltipBorderColor = self.$tooltip.css('border-bottom-color');
                    }

                    if (tooltipBorderWidth > 1) {
                        tooltipBorderWidth++;
                    }

                    var arrowBorder = '';
                    if (tooltipBorderWidth !== 0) {
                        var arrowBorderSize = '',
                            arrowBorderColor = 'border-color: '+ tooltipBorderColor +';';
                        if (arrowClass.indexOf('bottom') !== -1) {
                            arrowBorderSize = 'margin-top: -'+ Math.round(tooltipBorderWidth) +'px;';
                        }
                        else if (arrowClass.indexOf('top') !== -1) {
                            arrowBorderSize = 'margin-bottom: -'+ Math.round(tooltipBorderWidth) +'px;';
                        }
                        else if (arrowClass.indexOf('left') !== -1) {
                            arrowBorderSize = 'margin-right: -'+ Math.round(tooltipBorderWidth) +'px;';
                        }
                        else if (arrowClass.indexOf('right') !== -1) {
                            arrowBorderSize = 'margin-left: -'+ Math.round(tooltipBorderWidth) +'px;';
                        }
                        arrowBorder = '<span class="tooltipster-arrow-border" style="'+ arrowBorderSize +' '+ arrowBorderColor +';"></span>';
                    }

                    // if the arrow already exists, remove and replace it
                    self.$tooltip.find('.tooltipster-arrow').remove();

                    // build out the arrow and append it
                    var arrowConstruct = '<div class="'+ arrowClass +' tooltipster-arrow" style="'+ arrowReposition +'">'+ arrowBorder +'<span style="border-color:'+ arrowColor +';"></span></div>';
                    self.$tooltip.append(arrowConstruct);
                }

                // position the tooltip
                self.$tooltip.css({'top': Math.round(myTop) + 'px', 'left': Math.round(myLeft) + 'px'});
            }

            return self;
        },

        enable: function() {
            this.enabled = true;
            return this;
        },

        disable: function() {
            // hide first, in case the tooltip would not disappear on its own (autoClose false)
            this.hide();
            this.enabled = false;
            return this;
        },

        destroy: function() {

            var self = this;

            self.hide();

            // remove the icon, if any
            if (self.$el[0] !== self.$elProxy[0]) {
                self.$elProxy.remove();
            }

            self.$el
                .removeData(self.namespace)
                .off('.'+ self.namespace);

            var ns = self.$el.data('tooltipster-ns');

            // if there are no more tooltips on this element
            if(ns.length === 1){

                // optional restoration of a title attribute
                var title = null;
                if (self.options.restoration === 'previous'){
                    title = self.$el.data('tooltipster-initialTitle');
                }
                else if(self.options.restoration === 'current'){

                    // old school technique to stringify when outerHTML is not supported
                    title =
                        (typeof self.Content === 'string') ?
                            self.Content :
                            $('<div></div>').append(self.Content).html();
                }

                if (title) {
                    self.$el.attr('title', title);
                }

                // final cleaning
                self.$el
                    .removeClass('tooltipstered')
                    .removeData('tooltipster-ns')
                    .removeData('tooltipster-initialTitle');
            }
            else {
                // remove the instance namespace from the list of namespaces of tooltips present on the element
                ns = $.grep(ns, function(el, i){
                    return el !== self.namespace;
                });
                self.$el.data('tooltipster-ns', ns);
            }

            return self;
        },

        elementIcon: function() {
            return (this.$el[0] !== this.$elProxy[0]) ? this.$elProxy[0] : undefined;
        },

        elementTooltip: function() {
            return this.$tooltip ? this.$tooltip[0] : undefined;
        },

        // public methods but for internal use only
        // getter if val is ommitted, setter otherwise
        option: function(o, val) {
            if (typeof val == 'undefined') return this.options[o];
            else {
                this.options[o] = val;
                return this;
            }
        },
        status: function() {
            return this.Status;
        }
    };

    $.fn[pluginName] = function () {

        // for using in closures
        var args = arguments;

        // if we are not in the context of jQuery wrapped HTML element(s) :
        // this happens when calling static methods in the form $.fn.tooltipster('methodName'), or when calling $(sel).tooltipster('methodName or options') where $(sel) does not match anything
        if (this.length === 0) {

            // if the first argument is a method name
            if (typeof args[0] === 'string') {

                var methodIsStatic = true;

                // list static methods here (usable by calling $.fn.tooltipster('methodName');)
                switch (args[0]) {

                    case 'setDefaults':
                        // change default options for all future instances
                        $.extend(defaults, args[1]);
                        break;

                    default:
                        methodIsStatic = false;
                        break;
                }

                // $.fn.tooltipster('methodName') calls will return true
                if (methodIsStatic) return true;
                // $(sel).tooltipster('methodName') calls will return the list of objects event though it's empty because chaining should work on empty lists
                else return this;
            }
            // the first argument is undefined or an object of options : we are initalizing but there is no element matched by selector
            else {
                // still chainable : same as above
                return this;
            }
        }
        // this happens when calling $(sel).tooltipster('methodName or options') where $(sel) matches one or more elements
        else {

            // method calls
            if (typeof args[0] === 'string') {

                var v = '#*$~&';

                this.each(function() {

                    // retrieve the namepaces of the tooltip(s) that exist on that element. We will interact with the first tooltip only.
                    var ns = $(this).data('tooltipster-ns'),
                    // self represents the instance of the first tooltipster plugin associated to the current HTML object of the loop
                        self = ns ? $(this).data(ns[0]) : null;

                    // if the current element holds a tooltipster instance
                    if (self) {

                        if (typeof self[args[0]] === 'function') {
                            // note : args[1] and args[2] may not be defined
                            var resp = self[args[0]](args[1], args[2]);
                        }
                        else {
                            throw new Error('Unknown method .tooltipster("' + args[0] + '")');
                        }

                        // if the function returned anything other than the instance itself (which implies chaining)
                        if (resp !== self){
                            v = resp;
                            // return false to stop .each iteration on the first element matched by the selector
                            return false;
                        }
                    }
                    else {
                        throw new Error('You called Tooltipster\'s "' + args[0] + '" method on an uninitialized element');
                    }
                });

                return (v !== '#*$~&') ? v : this;
            }
            // first argument is undefined or an object : the tooltip is initializing
            else {

                var instances = [],
                // is there a defined value for the multiple option in the options object ?
                    multipleIsSet = args[0] && typeof args[0].multiple !== 'undefined',
                // if the multiple option is set to true, or if it's not defined but set to true in the defaults
                    multiple = (multipleIsSet && args[0].multiple) || (!multipleIsSet && defaults.multiple),
                // same for debug
                    debugIsSet = args[0] && typeof args[0].debug !== 'undefined',
                    debug = (debugIsSet && args[0].debug) || (!debugIsSet && defaults.debug);

                // initialize a tooltipster instance for each element if it doesn't already have one or if the multiple option is set, and attach the object to it
                this.each(function () {

                    var go = false,
                        ns = $(this).data('tooltipster-ns'),
                        instance = null;

                    if (!ns) {
                        go = true;
                    }
                    else if (multiple) {
                        go = true;
                    }
                    else if (debug) {
                        console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.');
                    }

                    if (go) {
                        instance = new Plugin(this, args[0]);

                        // save the reference of the new instance
                        if (!ns) ns = [];
                        ns.push(instance.namespace);
                        $(this).data('tooltipster-ns', ns)

                        // save the instance itself
                        $(this).data(instance.namespace, instance);
                    }

                    instances.push(instance);
                });

                if (multiple) return instances;
                else return this;
            }
        }
    };

    // quick & dirty compare function (not bijective nor multidimensional)
    function areEqual(a,b) {
        var same = true;
        $.each(a, function(i, el){
            if(typeof b[i] === 'undefined' || a[i] !== b[i]){
                same = false;
                return false;
            }
        });
        return same;
    }

    // detect if this device can trigger touch events
    var deviceHasTouchCapability = !!('ontouchstart' in window);

    // we'll assume the device has no mouse until we detect any mouse movement
    var deviceHasMouse = false;
    $('body').one('mousemove', function() {
        deviceHasMouse = true;
    });

    function deviceIsPureTouch() {
        return (!deviceHasMouse && deviceHasTouchCapability);
    }

    // detecting support for CSS transitions
    function supportsTransitions() {
        var b = document.body || document.documentElement,
            s = b.style,
            p = 'transition';

        if(typeof s[p] == 'string') {return true; }

        v = ['Moz', 'Webkit', 'Khtml', 'O', 'ms'],
            p = p.charAt(0).toUpperCase() + p.substr(1);
        for(var i=0; i<v.length; i++) {
            if(typeof s[v[i] + p] == 'string') { return true; }
        }
        return false;
    }
})( jQuery, window, document );

pp.module('tabs', []).directive('tabs', function($location)
{
    return {
        templateUrl: pp.viewPath('tabs'),
        restrict: 'E',
        transclude: true,
        scope:
        {
            params: '='
        },

        link: function (scope, element, attrs)
        {
            scope.url = $location.url();

            scope.route = function (route)
            {
                return pp.route(route);
            };
            
            scope.log = function (param) 
            {
                console.log(param);
            };
        }
    };
}).directive('tab', function()
{
    return {
        templateUrl: pp.viewPath('tab'),
        restrict: 'E',
        transclude: true,
        scope:
        {
            params: '='
        },

        link: function (scope, element, attrs)
        {

        }
    };
});
pp.module('tbl', ['button', 'tooltip', 'pp-select', 't']).directive('tbl', function(t)
{
    return {
        templateUrl: pp.viewPath('tbl'),
        restrict: 'E',
        scope:
        {
            params: '='
        },
        
        link: function (scope, element, attrs)
        {
            scope.reverse = false;
            scope.orderBy = scope.params != undefined ? scope.params.orderBy : '';

            scope.t = function (string)
            {
                return t.t('proposals', string);
            };

            if (scope.params.sorting == 'ajax') {
                scope.reverse = scope.params.order != 'ASC';
            }

            scope.sort = function (param)
            {
                scope.orderBy = param;

                if (param == scope.orderBy && scope.params.sorting != 'ajax') {
                    scope.reverse = !scope.reverse;
                }

                if (scope.params.sorting == 'ajax') {
                    scope.params.sort(param, scope.reverse);
                } else {
                    scope.sortClient(param);
                }
            };

            scope.sortClient = function (param)
            {
                var arr = scope.params.rows;

                arr = arr.sort(function (x, y)
                {
                    var xObj = x[param],
                        yObj = y[param];

                    if (xObj.value > yObj.value) return 1;
                    if (xObj.value < yObj.value) return -1;
                });

                if (scope.reverse) arr.reverse();

                scope.params.rows = arr;
            };

            scope.filter = function ()
            {
                var filter = [];

                for (column in scope.params.columns) {
                    if (typeof scope.params.columns[column].filter == 'object') {
                        scope.params.columns[column].filter = scope.params.columns[column].filter.id != '?' ? scope.params.columns[column].filter.id : '';
                    }

                    filter[column] =
                    {
                        name: scope.params.columns[column].name,
                        filter: scope.params.columns[column].filter
                    };
                }

                scope.params.filter(filter);
            };

            scope.filterInput = function (e)
            {
                if (e.which == 13) {
                    scope.filter();
                }
            };

            scope.page = function (page)
            {
                scope.params.page(page);
            };

            scope.wait = function () {
                setTimeout(function () {
                    if (scope.params.rows == undefined) {
                        scope.wait();
                    } else {
                        if (scope.params.reverse != undefined) {
                            scope.reverse = !scope.params.reverse;
                        }

                        scope.sort(scope.orderBy);
                    }
                });
            };

            if (scope.params.sorting != 'ajax') {
                scope.wait();
            }
        }
    };
});


//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsInByb3Bvc2FsLmpzIiwidC5qcyIsIl9oZWFkL3Byb3RvcGxhbi5qcyIsImJhY2tvZmZpY2UvT3JnYW5pemVyUHJvcG9zYWxDdHJsLmpzIiwid29ya3NwYWNlL3Byb3Bvc2Fscy9Qcm9wb3NhbEN0cmwuanMiLCJibG9ja3MvZWxlbWVudHMvYnV0dG9uL2J1dHRvbi5qcyIsImJsb2Nrcy9lbGVtZW50cy9jaGVja2JveC9jaGVja2JveC5qcyIsImJsb2Nrcy9lbGVtZW50cy9kYXRlLXBpY2tlci9kYXRlLXBpY2tlci5qcyIsImJsb2Nrcy9lbGVtZW50cy9pbnB1dC9pbnB1dC5qcyIsImJsb2Nrcy9lbGVtZW50cy9tb2RhbC9tb2RhbC5qcyIsImJsb2Nrcy9lbGVtZW50cy9wcC1zZWxlY3QvcHAtc2VsZWN0LmpzIiwiYmxvY2tzL2VsZW1lbnRzL3JhZGlvL3JhZGlvLmpzIiwiYmxvY2tzL2VsZW1lbnRzL3Njcm9sbC11cC9zY3JvbGwtdXAuanMiLCJibG9ja3MvZWxlbWVudHMvc3Bpbm5lci9zcGlubmVyLmpzIiwiYmxvY2tzL2VsZW1lbnRzL3Nwb2lsZXIvc3BvaWxlci5qcyIsImJsb2Nrcy9lbGVtZW50cy90ZXh0ZmllbGQvdGV4dGZpZWxkLmpzIiwiYmxvY2tzL2VsZW1lbnRzL3Rvb2x0aXAvdG9vbHRpcC5qcyIsImJsb2Nrcy9lbGVtZW50cy90b29sdGlwL3Rvb2x0aXBzdGVyLmpzIiwiYmxvY2tzL3dyYXBwZXJzL3Byb3Bvc2FsLWhlYWRlci9wcm9wb3NhbC1oZWFkZXIuanMiLCJibG9ja3Mvd3JhcHBlcnMvdGFicy90YWJzLmpzIiwiYmxvY2tzL3dyYXBwZXJzL3RibC90YmwuanMiLCJibG9ja3MvZWxlbWVudHMvaW5wdXQvY29lZmZpY2llbnQvY29lZmZpY2llbnQuanMiLCJibG9ja3MvZWxlbWVudHMvaW5wdXQvcmVzdWx0L3Jlc3VsdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDL0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuSEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMXZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMvRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMvR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDOURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzFIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNoRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdkhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDL3lDQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzNHQTtBQ0FBIiwiZmlsZSI6Imdsb2JhbC5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBwcCA9IHtcbiAgICBsYW5nOiBudWxsLFxuICAgIGRpY3Rpb25hcmllczoge30sXG4gICAgaGFzaFByZWZpeDogJyEnLFxuICAgIFxuICAgIHBhdGg6IGZ1bmN0aW9uIChyb3V0ZSwgcGFyYW1zKVxuICAgIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9ICcvJyArIHBwLmxhbmcgKyAnLycgKyByb3V0ZTtcblxuICAgICAgICBpZiAocGFyYW1zICE9IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICsgJz8nO1xuICAgICAgICB9XG5cbiAgICAgICAgZm9yKHZhciBwYXJhbSBpbiBwYXJhbXMpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArIHBhcmFtICsgJz0nICsgcGFyYW1zW3BhcmFtXSArICcmJztcblxuICAgICAgICAgICAgaWYgKHBhcmFtc1twYXJhbSArIDFdICE9IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdCArICcmJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9LFxuICAgIFxuICAgIHJvdXRlOiBmdW5jdGlvbiAocm91dGUpIFxuICAgIHtcbiAgICAgICAgcmV0dXJuICcjIScgKyByb3V0ZTtcbiAgICB9LFxuICAgIFxuICAgIG1vZHVsZTogZnVuY3Rpb24gKG5hbWUsIHJlcXVpcmVzLCBjb25maWdGbikgXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0ge307XG5cbiAgICAgICAgcmVzdWx0LmZhY3RvcnkgPSBmdW5jdGlvbiAoKSB7cmV0dXJuIHJlc3VsdH07XG4gICAgICAgIHJlc3VsdC5jb25maWcgPSBmdW5jdGlvbiAoKSB7cmV0dXJuIHJlc3VsdH07XG4gICAgICAgIHJlc3VsdC5jb250cm9sbGVyID0gZnVuY3Rpb24gKCkge3JldHVybiByZXN1bHR9O1xuICAgICAgICByZXN1bHQuZGlyZWN0aXZlID0gZnVuY3Rpb24gKCkge3JldHVybiByZXN1bHR9O1xuXG4gICAgICAgIGlmICh3aW5kb3cuYW5ndWxhciAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IGFuZ3VsYXIubW9kdWxlKG5hbWUsIHJlcXVpcmVzLCBjb25maWdGbik7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0sXG4gICAgXG4gICAgdWNmaXJzdDogZnVuY3Rpb24gKHN0cmluZylcbiAgICB7XG4gICAgICAgIHJldHVybiBzdHJpbmdbMF0udG9VcHBlckNhc2UoKSArIHN0cmluZy5zbGljZSgxKTtcbiAgICB9LFxuICAgIFxuICAgIHZpZXdQYXRoOiBmdW5jdGlvbiAodGVtcGxhdGUpIFxuICAgIHtcbiAgICAgICAgcmV0dXJuICcvc3RhdGljL3RlbXBsYXRlcy8nICsgdGVtcGxhdGUgKyAnLmh0bWwnXG4gICAgfSxcblxuICAgIGdldExhbmc6IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gcHAubGFuZztcblxuICAgICAgICBpZiAocmVzdWx0ID09IG51bGwpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9ICQoJy5wcC1sYW5nJykudmFsKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocmVzdWx0ID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzdWx0ID0gbG9jYXRpb24ucGF0aG5hbWUuc3BsaXQoJy8nKVsxXTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSxcblxuICAgIHNldExhbmc6IGZ1bmN0aW9uIChsYW5nKVxuICAgIHtcbiAgICAgICAgcHAubGFuZyA9IGxhbmc7XG4gICAgfVxufTtcblxucHAuc2V0TGFuZyhwcC5nZXRMYW5nKCkpO1xuXG5wcC5tb2R1bGUoJ3dvcmtzcGFjZScsIFsndCcsICdwcm9wb3NhbCcsICd0YmwnXSk7XG5wcC5tb2R1bGUoJ29yZ2FuaXplcicsIFsnbmdSb3V0ZScsICd0JywgJ3Byb3Bvc2FsJywgJ3RibCcsICd0YWJzJ10pOyIsInBwLm1vZHVsZSgncHJvcG9zYWwnLCBbJ3QnXSkuZmFjdG9yeSgncHJvcG9zYWwnLCBmdW5jdGlvbigkaHR0cCwgdCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIFNUQVRVU19EUkFGVDogMSxcbiAgICAgICAgU1RBVFVTX1JFSkVDVEVEOiAzLFxuICAgICAgICBTVEFUVVNfU0VOVDogMixcbiAgICAgICAgU1RBVFVTX0FDQ0VQVEVEOiA0LFxuICAgICAgICBTVEFUVVNfUkVNT1ZFRF9CWV9PUkdBTklaRVI6IDUsXG4gICAgICAgIFNUQVRVU19SRU1PVkVEX0JZX0VYUE9ORU5UOiA2LFxuICAgIFxuICAgICAgICBQQVlNRU5UX05PVDogMCxcbiAgICAgICAgUEFZTUVOVF9QQVJUOiAxLFxuICAgICAgICBQQVlNRU5UX0ZVTEw6IDIsXG4gICAgICAgIFBBWU1FTlRfQklMTDogMyxcbiAgICBcbiAgICAgICAgRE9DVU1FTlRTX05PVDogMCxcbiAgICAgICAgRE9DVU1FTlRTX1JFQ0lWRUQ6IDEsXG4gICAgICAgIFxuICAgICAgICBTRUVOX1RSVUU6IDEsXG4gICAgICAgIFNFRU5fRkFMU0U6IDAsXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBwYXJhbSB0eXBlXG4gICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAgICAgICAqL1xuICAgICAgICBnZXREb2N1bWVudFN0YXR1c05hbWU6IGZ1bmN0aW9uICh0eXBlKSBcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9ICcnO1xuXG4gICAgICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuRE9DVU1FTlRTX05PVDpcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gdC50KCdwcm9wb3NhbHMnLCAnRE9DVU1FTlRTX05PVCcpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuRE9DVU1FTlRTX1JFQ0lWRUQ6XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHQudCgncHJvcG9zYWxzJywgJ0RPQ1VNRU5UU19SRUNJVkVEJyk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcGFyYW0gdHlwZVxuICAgICAgICAgKiBAcmV0dXJucyB7e319XG4gICAgICAgICAqL1xuICAgICAgICBnZXRTdGF0dXM6IGZ1bmN0aW9uICh0eXBlKSBcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IHt9O1xuXG4gICAgICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuU1RBVFVTX0FDQ0VQVEVEOlxuICAgICAgICAgICAgICAgICAgICByZXN1bHQudmFsdWUgPSB0LnQoJ3Byb3Bvc2FscycsICdTVEFUVVNfQUNDRVBURUQnKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbG9yID0gJ3RleHRfc3VjY2Vzcyc7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jaXJjbGUgPSAnY2lyY2xlX3N1Y2Nlc3MnO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuU1RBVFVTX0RSQUZUOlxuICAgICAgICAgICAgICAgICAgICByZXN1bHQudmFsdWUgPSB0LnQoJ3Byb3Bvc2FscycsICdTVEFUVVNfRFJBRlQnKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbG9yID0gJ3RleHRfbXV0ZWQnO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuY2lyY2xlID0gJ2NpcmNsZV9tdXRlZCc7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgdGhpcy5TVEFUVVNfUkVKRUNURUQ6XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC52YWx1ZSA9IHQudCgncHJvcG9zYWxzJywgJ1NUQVRVU19SRUpFQ1RFRCcpO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuY29sb3IgPSAndGV4dF9kYW5nZXInO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuY2lyY2xlID0gJ2NpcmNsZV9kYW5nZXInO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuU1RBVFVTX1JFTU9WRURfQllfT1JHQU5JWkVSOlxuICAgICAgICAgICAgICAgICAgICByZXN1bHQudmFsdWUgPSB0LnQoJ3Byb3Bvc2FscycsICdTVEFUVVNfUkVNT1ZFRF9CWV9PUkdBTklaRVInKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbG9yID0gJ3RleHRfd2FybmluZyc7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jaXJjbGUgPSAnY2lyY2xlX3dhcm5pbmcnO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuU1RBVFVTX1JFTU9WRURfQllfRVhQT05FTlQ6XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC52YWx1ZSA9IHQudCgncHJvcG9zYWxzJywgJ1NUQVRVU19SRU1PVkVEX0JZX0VYUE9ORU5UJyk7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jb2xvciA9ICd0ZXh0X3dhcm5pbmcnO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuY2lyY2xlID0gJ2NpcmNsZV93YXJuaW5nJztcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSB0aGlzLlNUQVRVU19TRU5UOlxuICAgICAgICAgICAgICAgICAgICByZXN1bHQudmFsdWUgID0gdC50KCdwcm9wb3NhbHMnLCAnU1RBVFVTX1NFTlQnKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbG9yICA9ICd0ZXh0X3ByaW1hcnknO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuY2lyY2xlID0gJ2NpcmNsZV9wcmltYXJ5JztcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBwYXJhbSB0eXBlXG4gICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAgICAgICAqL1xuICAgICAgICBnZXRQYXltZW50U3RhdHVzTmFtZTogZnVuY3Rpb24gKHR5cGUpIFxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gJyc7XG5cbiAgICAgICAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICAgICAgICAgIGNhc2UgdGhpcy5QQVlNRU5UX0ZVTEw6XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHQudCgncHJvcG9zYWxzJywgJ1BBWU1FTlRfRlVMTCcpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuUEFZTUVOVF9OT1Q6XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHQudCgncHJvcG9zYWxzJywgJ1BBWU1FTlRfTk9UJyk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgdGhpcy5QQVlNRU5UX1BBUlQ6XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHQudCgncHJvcG9zYWxzJywgJ1BBWU1FTlRfUEFSVCcpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHRoaXMuUEFZTUVOVF9CSUxMOlxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB0LnQoJ3Byb3Bvc2FscycsICdQQVlNRU5UX0JJTEwnKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH0sXG4gICAgICAgIFxuICAgICAgICBsb2FkV29ya3NwYWNlUHJvcG9zYWxzOiBmdW5jdGlvbiAoY2FsbGJhY2spIFxuICAgICAgICB7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIH1cbn0pOyIsInBwLm1vZHVsZSgndCcsIFtdKS5mYWN0b3J5KCd0JywgZnVuY3Rpb24oJGh0dHApIHtcbiAgICByZXR1cm4ge1xuICAgICAgICB0OiBmdW5jdGlvbihkaWN0aW9uYXJ5LCB3b3JkKSB7XG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gd29yZDtcbiAgICAgICAgICAgIHZhciB1cmwgPSAnL3N0YXRpYy9mcm9udGVuZC90cmFuc2xhdGVzLycgKyBwcC5sYW5nICsgJy8nICsgZGljdGlvbmFyeSArICcuanNvbj92PScgKyAobmV3IERhdGUoKSkuZ2V0VGltZSgpO1xuXG4gICAgICAgICAgICBpZiAocHAuZGljdGlvbmFyaWVzW2RpY3Rpb25hcnldID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihyZXNwb25zZSlcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHAuZGljdGlvbmFyaWVzW2RpY3Rpb25hcnldID0gcmVzcG9uc2U7XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGFzeW5jOiBmYWxzZVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAocHAuZGljdGlvbmFyaWVzW2RpY3Rpb25hcnldW3dvcmRdICE9IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHBwLmRpY3Rpb25hcmllc1tkaWN0aW9uYXJ5XVt3b3JkXTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfVxuICAgIH07XG59KTsiLCJmdW5jdGlvbiBQcm90b3BsYW4oKVxue1xuICAgIHRoaXMuZWxlbWVudHMgPSBbXTtcbn1cblxuLyoqXG4gKiBAY2xhc3MgUHJvdG9wbGFuLkJsb2NrXG4gKiBAcGFyYW0gaWRcbiAqIEBwYXJhbSBzZWxlY3RvclxuICogQGNvbnN0cnVjdG9yXG4gKi9cblByb3RvcGxhbi5CbG9jayA9IGZ1bmN0aW9uIChpZCwgc2VsZWN0b3IpXG57XG4gICAgdGhpcy5pZCAgICAgICA9IHR5cGVvZiBpZCAgICAgICA9PSAndW5kZWZpbmVkJyA/IDAgICAgICAgIDogaWQ7XG4gICAgdGhpcy5zZWxlY3RvciA9IHR5cGVvZiBzZWxlY3RvciA9PSAndW5kZWZpbmVkJyA/ICcjJyArIGlkIDogc2VsZWN0b3I7XG5cbiAgICB0aGlzLmNhcGl0YWxpemVGaXJzdExldHRlciA9IGZ1bmN0aW9uIChzdHJpbmcpXG4gICAge1xuICAgICAgICByZXR1cm4gc3RyaW5nWzBdLnRvVXBwZXJDYXNlKCkgKyBzdHJpbmcuc2xpY2UoMSk7XG4gICAgfTtcblxuICAgIHRoaXMubWVyZ2UgPSBmdW5jdGlvbiAob2JqMSwgb2JqMilcbiAgICB7XG4gICAgICAgIHZhciBvYmozID0ge307XG4gICAgICAgIGZvciAodmFyIGF0dHJuYW1lIGluIG9iajEpIHsgb2JqM1thdHRybmFtZV0gPSBvYmoxW2F0dHJuYW1lXTsgfVxuICAgICAgICBmb3IgKHZhciBhdHRybmFtZSBpbiBvYmoyKSB7IG9iajNbYXR0cm5hbWVdID0gb2JqMlthdHRybmFtZV07IH1cbiAgICAgICAgcmV0dXJuIG9iajM7XG4gICAgfTtcblxuICAgIHRoaXMuY2xvbmUgPSBmdW5jdGlvbiAob2JqKVxuICAgIHtcbiAgICAgICAgdmFyIGNsb25lID0ge307XG5cbiAgICAgICAgZm9yICh2YXIga2V5IGluIG9iaikge1xuICAgICAgICAgICAgY2xvbmVba2V5XSA9IG9ialtrZXldO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNsb25lO1xuICAgIH07XG59O1xuXG52YXIgcHJvdG9wbGFuID0gbmV3IFByb3RvcGxhbigpOyIsInBwLm1vZHVsZSgnb3JnYW5pemVyJykuY29uZmlnKFsnJGxvY2F0aW9uUHJvdmlkZXInLCAnJHJvdXRlUHJvdmlkZXInLFxuICAgIGZ1bmN0aW9uIGNvbmZpZygkbG9jYXRpb25Qcm92aWRlciwgJHJvdXRlUHJvdmlkZXIpXG4gICAge1xuICAgICAgICAkbG9jYXRpb25Qcm92aWRlci5oYXNoUHJlZml4KHBwLmhhc2hQcmVmaXgpO1xuICAgICAgICAvLyAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUodHJ1ZSkuaGFzaFByZWZpeChwcC5oYXNoUHJlZml4KTtcblxuICAgICAgICAkcm91dGVQcm92aWRlci5cbiAgICAgICAgd2hlbignL2luY29taW5nJywge1xuICAgICAgICAgICAgdGVtcGxhdGU6ICc8dGFicyBwYXJhbXM9XCJ0YWJzXCI+PC90YWJzPjx0YmwgcGFyYW1zPVwiaW5jb21pbmdcIj48L3RibD4nLFxuICAgICAgICAgICAgcmVsb2FkT25TZWFyY2g6IGZhbHNlLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ09yZ2FuaXplclByb3Bvc2FsQ3RybCdcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9pbmNvbWluZy86c29ydC86b3JkZXIvOm5hbWUvOmNvbnRyYWN0b3IvOmRhdGUvOnBhZ2UnLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzx0YWJzIHBhcmFtcz1cInRhYnNcIj48L3RhYnM+PHRibCBwYXJhbXM9XCJpbmNvbWluZ1wiPjwvdGJsPicsXG4gICAgICAgICAgICByZWxvYWRPblNlYXJjaDogZmFsc2UsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnT3JnYW5pemVyUHJvcG9zYWxDdHJsJ1xuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL2FjY2VwdGVkJywge1xuICAgICAgICAgICAgdGVtcGxhdGU6ICc8dGFicyBwYXJhbXM9XCJ0YWJzXCI+PC90YWJzPjx0YmwgcGFyYW1zPVwiYWNjZXB0ZWRcIj48L3RibD4nLFxuICAgICAgICAgICAgcmVsb2FkT25TZWFyY2g6IGZhbHNlLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ09yZ2FuaXplclByb3Bvc2FsQ3RybCdcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9hY2NlcHRlZC86c29ydC86b3JkZXIvOm5hbWUvOmRhdGUvOmNvbnRyYWN0b3IvOnBheW1lbnQvOmRvY3VtZW50cy86cGFnZScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnPHRhYnMgcGFyYW1zPVwidGFic1wiPjwvdGFicz48dGJsIHBhcmFtcz1cImFjY2VwdGVkXCI+PC90Ymw+JyxcbiAgICAgICAgICAgIHJlbG9hZE9uU2VhcmNoOiBmYWxzZSxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdPcmdhbml6ZXJQcm9wb3NhbEN0cmwnXG4gICAgICAgIH0pLlxuICAgICAgICB3aGVuKCcvcmVqZWN0ZWQnLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzx0YWJzIHBhcmFtcz1cInRhYnNcIj48L3RhYnM+PHRibCBwYXJhbXM9XCJyZWplY3RlZFwiPjwvdGJsPicsXG4gICAgICAgICAgICByZWxvYWRPblNlYXJjaDogZmFsc2UsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnT3JnYW5pemVyUHJvcG9zYWxDdHJsJ1xuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3JlamVjdGVkLzpzb3J0LzpvcmRlci86bmFtZS86Y29udHJhY3Rvci86ZGF0ZS86Y29tbWVudC86cGFnZScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnPHRhYnMgcGFyYW1zPVwidGFic1wiPjwvdGFicz48dGJsIHBhcmFtcz1cInJlamVjdGVkXCI+PC90Ymw+JyxcbiAgICAgICAgICAgIHJlbG9hZE9uU2VhcmNoOiBmYWxzZSxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdPcmdhbml6ZXJQcm9wb3NhbEN0cmwnXG4gICAgICAgIH0pLlxuICAgICAgICBvdGhlcndpc2UoJy9pbmNvbWluZycpO1xuICAgIH1cbl0pO1xuXG5wcC5tb2R1bGUoJ29yZ2FuaXplcicpLmNvbnRyb2xsZXIoJ09yZ2FuaXplclByb3Bvc2FsQ3RybCcsIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkc2NlLCB0LCBwcm9wb3NhbCwgJHJvdXRlUGFyYW1zKVxue1xuICAgICRzY29wZS5MSU1JVCA9IDIwO1xuXG4gICAgJHNjb3BlLnRhYnMgPSBbXG4gICAgICAgIHtcbiAgICAgICAgICAgIGxpbms6ICcvaW5jb21pbmcnLFxuICAgICAgICAgICAgdGV4dDogdC50KCdwcm9wb3NhbHMnLCAnSU5DT01JTkcnKSxcbiAgICAgICAgICAgIGNvdW50ZXI6IDBcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgbGluazogJy9hY2NlcHRlZCcsXG4gICAgICAgICAgICB0ZXh0OiB0LnQoJ3Byb3Bvc2FscycsICdBQ0NFUFRFRCcpXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIGxpbms6ICcvcmVqZWN0ZWQnLFxuICAgICAgICAgICAgdGV4dDogdC50KCdwcm9wb3NhbHMnLCAnUkVKRUNURUQnKVxuICAgICAgICB9XG4gICAgXTtcblxuICAgICRzY29wZS5nZXRSb3V0ZVBhcmFtcyA9IGZ1bmN0aW9uIChwYXJhbXMpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0ge307XG5cbiAgICAgICAgZm9yIChwYXJhbSBpbiBwYXJhbXMpIHtcbiAgICAgICAgICAgIHJlc3VsdFtwYXJhbV0gPSBwYXJhbXNbcGFyYW1dLnN1YnN0cigxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfTtcblxuICAgICRzY29wZS5nZXRTb3J0ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHZhciByZXN1bHQgPSAkc2NvcGUuZ2V0Um91dGVQYXJhbXMoJHJvdXRlUGFyYW1zKVsnc29ydCddO1xuXG4gICAgICAgIHJldHVybiByZXN1bHQgIT0gdW5kZWZpbmVkID8gcmVzdWx0IDogJ2RhdGUnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0T3JkZXIgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9ICRzY29wZS5nZXRSb3V0ZVBhcmFtcygkcm91dGVQYXJhbXMpWydvcmRlciddO1xuXG4gICAgICAgIHJldHVybiByZXN1bHQgIT0gdW5kZWZpbmVkID8gcmVzdWx0IDogJ0RFU0MnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0Q29udHJhY3RvciA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gJHNjb3BlLmdldFJvdXRlUGFyYW1zKCRyb3V0ZVBhcmFtcylbJ2NvbnRyYWN0b3InXTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0ICE9IHVuZGVmaW5lZCA/IHJlc3VsdCA6ICcnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0TmFtZSA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gJHNjb3BlLmdldFJvdXRlUGFyYW1zKCRyb3V0ZVBhcmFtcylbJ25hbWUnXTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0ICE9IHVuZGVmaW5lZCA/IHJlc3VsdCA6ICcnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0RGF0ZSA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gJHNjb3BlLmdldFJvdXRlUGFyYW1zKCRyb3V0ZVBhcmFtcylbJ2RhdGUnXTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0ICE9IHVuZGVmaW5lZCA/IHJlc3VsdCA6ICcnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0UGFnZSA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gJHNjb3BlLmdldFJvdXRlUGFyYW1zKCRyb3V0ZVBhcmFtcylbJ3BhZ2UnXTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0ICE9IHVuZGVmaW5lZCA/IHJlc3VsdCA6IDE7XG4gICAgfTtcblxuICAgICRzY29wZS5nZXRQYXltZW50ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHZhciByZXN1bHQgPSAkc2NvcGUuZ2V0Um91dGVQYXJhbXMoJHJvdXRlUGFyYW1zKVsncGF5bWVudCddO1xuXG4gICAgICAgIHJldHVybiByZXN1bHQgIT0gdW5kZWZpbmVkID8gcmVzdWx0IDogJyc7XG4gICAgfTtcblxuICAgICRzY29wZS5nZXREb2N1bWVudHMgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9ICRzY29wZS5nZXRSb3V0ZVBhcmFtcygkcm91dGVQYXJhbXMpWydkb2N1bWVudHMnXTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0ICE9IHVuZGVmaW5lZCA/IHJlc3VsdCA6ICcnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0Q29tbWVudCA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gJHNjb3BlLmdldFJvdXRlUGFyYW1zKCRyb3V0ZVBhcmFtcylbJ2NvbW1lbnQnXTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0ICE9IHVuZGVmaW5lZCA/IHJlc3VsdCA6ICcnO1xuICAgIH07XG5cbiAgICAkc2NvcGUuaW5jb21pbmcgPVxuICAgIHtcbiAgICAgICAgY29sdW1uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ05BTUUnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnbmFtZScsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0TmFtZSgpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0NPTlRSQUNUT1InKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnY29udHJhY3RvcicsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0Q29udHJhY3RvcigpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0RBVEUnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnZGF0ZScsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0RGF0ZSgpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdidXR0b25zJyxcbiAgICAgICAgICAgICAgICBzb3J0YWJsZTogZmFsc2VcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc29ydGluZzogJ2FqYXgnLFxuICAgICAgICBvcmRlcjogJHNjb3BlLmdldE9yZGVyKCksXG4gICAgICAgIHNvcnQ6IGZ1bmN0aW9uIChjb2x1bW4sIHJldmVyc2UpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciByID0gJHNjb3BlLmdldE9yZGVyKCk7XG5cbiAgICAgICAgICAgIGlmIChyID09ICdBU0MnKSB7XG4gICAgICAgICAgICAgICAgciA9ICdERVNDJztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgciA9ICdBU0MnO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUobnVsbCwgbnVsbCxcbiAgICAgICAgICAgICAgICAnIyEvaW5jb21pbmcvOicgKyBjb2x1bW4gK1xuICAgICAgICAgICAgICAgICcvOicgKyByICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldE5hbWUoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRDb250cmFjdG9yKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0RGF0ZSgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldFBhZ2UoKSk7XG4gICAgICAgIH0sXG4gICAgICAgIGZpbHRlcjogZnVuY3Rpb24gKGZpbHRlcikgXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBuYW1lID0gJycsXG4gICAgICAgICAgICAgICAgY29udHJhY3RvciA9ICcnLFxuICAgICAgICAgICAgICAgIGRhdGUgPSAnJztcblxuICAgICAgICAgICAgZm9yIChpdGVtIGluIGZpbHRlcikge1xuICAgICAgICAgICAgICAgIHN3aXRjaChmaWx0ZXJbaXRlbV0ubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlICduYW1lJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWUgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdjb250cmFjdG9yJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRyYWN0b3IgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdkYXRlJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGUgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgd2luZG93Lmhpc3RvcnkucHVzaFN0YXRlKG51bGwsIG51bGwsXG4gICAgICAgICAgICAgICAgJyMhL2luY29taW5nLzonICsgJHNjb3BlLmdldFNvcnQoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRPcmRlcigpICtcbiAgICAgICAgICAgICAgICAnLzonICsgbmFtZSArXG4gICAgICAgICAgICAgICAgJy86JyArIGNvbnRyYWN0b3IgK1xuICAgICAgICAgICAgICAgICcvOicgKyBkYXRlICtcbiAgICAgICAgICAgICAgICAnLzoxJyk7XG4gICAgICAgIH0sXG4gICAgICAgIHBhZ2U6IGZ1bmN0aW9uIChwYWdlKVxuICAgICAgICB7XG4gICAgICAgICAgICB3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUobnVsbCwgbnVsbCxcbiAgICAgICAgICAgICAgICAnIyEvaW5jb21pbmcvOicgKyAkc2NvcGUuZ2V0U29ydCgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldE9yZGVyKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0TmFtZSgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldENvbnRyYWN0b3IoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXREYXRlKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyBwYWdlKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICAkc2NvcGUuYWNjZXB0ZWQgPVxuICAgIHtcbiAgICAgICAgY29sdW1uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ05BTUUnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnbmFtZScsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0TmFtZSgpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0RBVEUnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnZGF0ZScsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0RGF0ZSgpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0NPTlRSQUNUT1InKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnY29udHJhY3RvcicsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0Q29udHJhY3RvcigpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ1BBWU1FTlQnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAncGF5bWVudCcsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0UGF5bWVudCgpLFxuICAgICAgICAgICAgICAgIGZpbHRlclR5cGU6ICdzZWxlY3QnLFxuICAgICAgICAgICAgICAgIGZpbHRlckl0ZW1zOiBbXG4gICAgICAgICAgICAgICAgICAgIHtpZDogcHJvcG9zYWwuUEFZTUVOVF9OT1QsIG5hbWU6ICctJ30sXG4gICAgICAgICAgICAgICAgICAgIHtpZDogcHJvcG9zYWwuUEFZTUVOVF9QQVJULCBuYW1lOiB0LnQoJ3Byb3Bvc2FscycsICdQQVJUTFknKX0sXG4gICAgICAgICAgICAgICAgICAgIHtpZDogcHJvcG9zYWwuUEFZTUVOVF9GVUxMLCBuYW1lOiAnMTAwJSd9LFxuICAgICAgICAgICAgICAgICAgICB7aWQ6IHByb3Bvc2FsLlBBWU1FTlRfQklMTCwgbmFtZTogdC50KCdwcm9wb3NhbHMnLCAnSU5WT0lDRSBJU1NVRUQnKX1cbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0RPQ1VNRU5UUycpLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdkb2N1bWVudHMnLFxuICAgICAgICAgICAgICAgIGZpbHRlcjogJHNjb3BlLmdldERvY3VtZW50cygpLFxuICAgICAgICAgICAgICAgIGZpbHRlclR5cGU6ICdzZWxlY3QnLFxuICAgICAgICAgICAgICAgIGZpbHRlckl0ZW1zOiBbXG4gICAgICAgICAgICAgICAgICAgIHtpZDogcHJvcG9zYWwuRE9DVU1FTlRTX05PVCwgbmFtZTogJy0nfSxcbiAgICAgICAgICAgICAgICAgICAge2lkOiBwcm9wb3NhbC5ET0NVTUVOVFNfUkVDSVZFRCwgbmFtZTogdC50KCdwcm9wb3NhbHMnLCAnUkVDSUVWRUQnKX1cbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgIGZpbHRlckRlZmF1bHRJdGVtOiB7aWQ6ICc/JywgbmFtZTogdC50KCdwcm9wb3NhbHMnLCAnQ0hPT1NFJyl9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdidXR0b25zJyxcbiAgICAgICAgICAgICAgICBzb3J0YWJsZTogZmFsc2VcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSxcblxuICAgICAgICBvcmRlcjogJHNjb3BlLmdldE9yZGVyKCksXG4gICAgICAgIHNvcnRpbmc6ICdhamF4JyxcbiAgICAgICAgc29ydDogZnVuY3Rpb24gKGNvbHVtbiwgcmV2ZXJzZSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHIgPSAkc2NvcGUuZ2V0T3JkZXIoKTtcblxuICAgICAgICAgICAgaWYgKHIgPT0gJ0FTQycpIHtcbiAgICAgICAgICAgICAgICByID0gJ0RFU0MnO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByID0gJ0FTQyc7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShudWxsLCBudWxsLFxuICAgICAgICAgICAgICAgICcjIS9hY2NlcHRlZC86JyArIGNvbHVtbiArXG4gICAgICAgICAgICAgICAgJy86JyArIHIgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0TmFtZSgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldERhdGUoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRDb250cmFjdG9yKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0UGF5bWVudCgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldERvY3VtZW50cygpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldFBhZ2UoKSk7XG4gICAgICAgIH0sXG4gICAgICAgIGZpbHRlcjogZnVuY3Rpb24gKGZpbHRlcilcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIG5hbWUgPSAnJyxcbiAgICAgICAgICAgICAgICBjb250cmFjdG9yID0gJycsXG4gICAgICAgICAgICAgICAgcGF5bWVudCA9ICcnLFxuICAgICAgICAgICAgICAgIGRvY3VtZW50cyA9ICcnLFxuICAgICAgICAgICAgICAgIGRhdGUgPSAnJztcblxuICAgICAgICAgICAgZm9yIChpdGVtIGluIGZpbHRlcikge1xuICAgICAgICAgICAgICAgIHN3aXRjaChmaWx0ZXJbaXRlbV0ubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlICduYW1lJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWUgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdjb250cmFjdG9yJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRyYWN0b3IgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdkYXRlJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGUgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdwYXltZW50JzpcbiAgICAgICAgICAgICAgICAgICAgICAgIHBheW1lbnQgPSBmaWx0ZXJbaXRlbV0uZmlsdGVyICE9IHVuZGVmaW5lZCA/IGZpbHRlcltpdGVtXS5maWx0ZXIgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdkb2N1bWVudHMnOlxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnRzID0gZmlsdGVyW2l0ZW1dLmZpbHRlciAhPSB1bmRlZmluZWQgPyBmaWx0ZXJbaXRlbV0uZmlsdGVyIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShudWxsLCBudWxsLFxuICAgICAgICAgICAgICAgICcjIS9hY2NlcHRlZC86JyArICRzY29wZS5nZXRTb3J0KCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0T3JkZXIoKSArXG4gICAgICAgICAgICAgICAgJy86JyArIG5hbWUgK1xuICAgICAgICAgICAgICAgICcvOicgKyBkYXRlICtcbiAgICAgICAgICAgICAgICAnLzonICsgY29udHJhY3RvciArXG4gICAgICAgICAgICAgICAgJy86JyArIHBheW1lbnQgK1xuICAgICAgICAgICAgICAgICcvOicgKyBkb2N1bWVudHMgK1xuICAgICAgICAgICAgICAgICcvOjEnKTtcbiAgICAgICAgfSxcbiAgICAgICAgcGFnZTogZnVuY3Rpb24gKHBhZ2UpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShudWxsLCBudWxsLFxuICAgICAgICAgICAgICAgICcjIS9hY2NlcHRlZC86JyArICRzY29wZS5nZXRTb3J0KCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0T3JkZXIoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXROYW1lKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0RGF0ZSgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldENvbnRyYWN0b3IoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRQYXltZW50KCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0RG9jdW1lbnRzKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyBwYWdlKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICAkc2NvcGUucmVqZWN0ZWQgPVxuICAgIHtcbiAgICAgICAgY29sdW1uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ05BTUUnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnbmFtZScsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0TmFtZSgpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0NPTlRSQUNUT1InKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnY29udHJhY3RvcicsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0Q29udHJhY3RvcigpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0RBVEUnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnZGF0ZScsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0RGF0ZSgpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0NPTU1FTlQnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAnY29tbWVudCcsXG4gICAgICAgICAgICAgICAgZmlsdGVyOiAkc2NvcGUuZ2V0Q29tbWVudCgpXG4gICAgICAgICAgICB9XG4gICAgICAgIF0sXG5cbiAgICAgICAgb3JkZXI6ICRzY29wZS5nZXRPcmRlcigpLFxuICAgICAgICBzb3J0aW5nOiAnYWpheCcsXG4gICAgICAgIHNvcnQ6IGZ1bmN0aW9uIChjb2x1bW4sIHJldmVyc2UpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciByID0gJHNjb3BlLmdldE9yZGVyKCk7XG5cbiAgICAgICAgICAgIGlmIChyID09ICdBU0MnKSB7XG4gICAgICAgICAgICAgICAgciA9ICdERVNDJztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgciA9ICdBU0MnO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUobnVsbCwgbnVsbCxcbiAgICAgICAgICAgICAgICAnIyEvcmVqZWN0ZWQvOicgKyBjb2x1bW4gK1xuICAgICAgICAgICAgICAgICcvOicgKyByICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldE5hbWUoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRDb250cmFjdG9yKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0RGF0ZSgpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldENvbW1lbnQoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRQYWdlKCkpO1xuICAgICAgICB9LFxuICAgICAgICBmaWx0ZXI6IGZ1bmN0aW9uIChmaWx0ZXIpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBuYW1lID0gJycsXG4gICAgICAgICAgICAgICAgY29udHJhY3RvciA9ICcnLFxuICAgICAgICAgICAgICAgIGNvbW1lbnQgPSAnJyxcbiAgICAgICAgICAgICAgICBkYXRlID0gJyc7XG5cbiAgICAgICAgICAgIGZvciAoaXRlbSBpbiBmaWx0ZXIpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2goZmlsdGVyW2l0ZW1dLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnbmFtZSc6XG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lID0gZmlsdGVyW2l0ZW1dLmZpbHRlciAhPSB1bmRlZmluZWQgPyBmaWx0ZXJbaXRlbV0uZmlsdGVyIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnY29udHJhY3Rvcic6XG4gICAgICAgICAgICAgICAgICAgICAgICBjb250cmFjdG9yID0gZmlsdGVyW2l0ZW1dLmZpbHRlciAhPSB1bmRlZmluZWQgPyBmaWx0ZXJbaXRlbV0uZmlsdGVyIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnZGF0ZSc6XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlID0gZmlsdGVyW2l0ZW1dLmZpbHRlciAhPSB1bmRlZmluZWQgPyBmaWx0ZXJbaXRlbV0uZmlsdGVyIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnY29tbWVudCc6XG4gICAgICAgICAgICAgICAgICAgICAgICBjb21tZW50ID0gZmlsdGVyW2l0ZW1dLmZpbHRlciAhPSB1bmRlZmluZWQgPyBmaWx0ZXJbaXRlbV0uZmlsdGVyIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShudWxsLCBudWxsLFxuICAgICAgICAgICAgICAgICcjIS9yZWplY3RlZC86JyArICRzY29wZS5nZXRTb3J0KCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0T3JkZXIoKSArXG4gICAgICAgICAgICAgICAgJy86JyArIG5hbWUgK1xuICAgICAgICAgICAgICAgICcvOicgKyBjb250cmFjdG9yICtcbiAgICAgICAgICAgICAgICAnLzonICsgZGF0ZSArXG4gICAgICAgICAgICAgICAgJy86JyArIGNvbW1lbnQgK1xuICAgICAgICAgICAgICAgICcvOjEnKTtcbiAgICAgICAgfSxcbiAgICAgICAgcGFnZTogZnVuY3Rpb24gKHBhZ2UpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShudWxsLCBudWxsLFxuICAgICAgICAgICAgICAgICcjIS9yZWplY3RlZC86JyArICRzY29wZS5nZXRTb3J0KCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0T3JkZXIoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXROYW1lKCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyAkc2NvcGUuZ2V0Q29udHJhY3RvcigpICtcbiAgICAgICAgICAgICAgICAnLzonICsgJHNjb3BlLmdldERhdGUoKSArXG4gICAgICAgICAgICAgICAgJy86JyArICRzY29wZS5nZXRDb21tZW50KCkgK1xuICAgICAgICAgICAgICAgICcvOicgKyBwYWdlKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICAkc2NvcGUuZ2V0RmFpcklkID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHJldHVybiAkKCcucHAtZmFpci1pZCcpLnZhbCgpO1xuICAgIH07XG5cbiAgICAkc2NvcGUuY291bnQgPSAwO1xuXG4gICAgJHNjb3BlLmdldENvdW50ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHJldHVybiAkc2NvcGUuaW5jb21pbmcucm93cy5sZW5ndGg7XG4gICAgfTtcblxuICAgIHZhciBwYXJhbXM7XG5cbiAgICBwYXJhbXMgPVxuICAgIHtcbiAgICAgICAgZmFpcklkOiAkc2NvcGUuZ2V0RmFpcklkKCksXG4gICAgICAgICdvcmRlcltieV0nOiAkc2NvcGUuZ2V0U29ydCgpLFxuICAgICAgICAnb3JkZXJbb3JkZXJdJzogJHNjb3BlLmdldE9yZGVyKCksXG4gICAgICAgICdmaWx0ZXJbc3RhdHVzXSc6IHByb3Bvc2FsLlNUQVRVU19TRU5ULFxuICAgICAgICAnZmlsdGVyW25hbWVdJzogJHNjb3BlLmdldE5hbWUoKSxcbiAgICAgICAgJ2ZpbHRlcltjb250cmFjdG9yXSc6ICRzY29wZS5nZXRDb250cmFjdG9yKCksXG4gICAgICAgICdmaWx0ZXJbZGF0ZV0nOiAkc2NvcGUuZ2V0RGF0ZSgpLFxuICAgICAgICAnbGltaXQnOiAkc2NvcGUuTElNSVQsXG4gICAgICAgICdwYWdlJzogJHNjb3BlLmdldFBhZ2UoKSAtIDFcbiAgICB9O1xuXG4gICAgJGh0dHAuZ2V0KHBwLnBhdGgoJ2JhY2tvZmZpY2UvcGFuZWwvcmVxdWVzdHNBamF4LycsIHBhcmFtcykpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpXG4gICAge1xuICAgICAgICAkc2NvcGUuaW5jb21pbmcucm93cyA9ICRzY29wZS5wcmVwYXJlUm93cyhyZXNwb25zZS5kYXRhLCAnaW5jb21pbmcnKTtcbiAgICAgICAgJHNjb3BlLnRhYnNbMF0uY291bnRlciA9IHJlc3BvbnNlLmRhdGFbMF0uY291bnQ7XG4gICAgICAgICRzY29wZS5pbmNvbWluZy5vcmRlckJ5ID0gJHNjb3BlLmdldFNvcnQoKTtcblxuICAgICAgICB2YXIgdG90YWxJdGVtcyA9IHJlc3BvbnNlLmRhdGFbMF0udG90YWw7XG5cbiAgICAgICAgJHNjb3BlLmluY29taW5nLnRvdGFsID0gKCh0b3RhbEl0ZW1zIC0gdG90YWxJdGVtcyAlICRzY29wZS5MSU1JVCkgLyAkc2NvcGUuTElNSVQpICsgMTtcbiAgICAgICAgJHNjb3BlLmluY29taW5nLmNvdW50ID0gdG90YWxJdGVtcztcblxuICAgICAgICAkc2NvcGUuaW5jb21pbmcucGFnZVNpemUgPSAkc2NvcGUuTElNSVQ7XG4gICAgICAgICRzY29wZS5pbmNvbWluZy5jdXJyZW50UGFnZSA9ICRzY29wZS5nZXRQYWdlKCkgKiAxO1xuXG4gICAgICAgICRzY29wZS5pbmNvbWluZy5wYXJhbXMgPSBwYXJhbXM7XG4gICAgfSk7XG5cbiAgICBwYXJhbXMgPVxuICAgIHtcbiAgICAgICAgZmFpcklkOiAkc2NvcGUuZ2V0RmFpcklkKCksXG4gICAgICAgICdvcmRlcltieV0nOiAkc2NvcGUuZ2V0U29ydCgpLFxuICAgICAgICAnb3JkZXJbb3JkZXJdJzogJHNjb3BlLmdldE9yZGVyKCksXG4gICAgICAgICdmaWx0ZXJbc3RhdHVzXSc6IHByb3Bvc2FsLlNUQVRVU19BQ0NFUFRFRCxcbiAgICAgICAgJ2ZpbHRlcltuYW1lXSc6ICRzY29wZS5nZXROYW1lKCksXG4gICAgICAgICdmaWx0ZXJbY29udHJhY3Rvcl0nOiAkc2NvcGUuZ2V0Q29udHJhY3RvcigpLFxuICAgICAgICAnZmlsdGVyW2RhdGVdJzogJHNjb3BlLmdldERhdGUoKSxcbiAgICAgICAgJ2ZpbHRlcltwYXltZW50XSc6ICRzY29wZS5nZXRQYXltZW50KCksXG4gICAgICAgICdmaWx0ZXJbZG9jdW1lbnRzXSc6ICRzY29wZS5nZXREb2N1bWVudHMoKSxcbiAgICAgICAgJ2xpbWl0JzogJHNjb3BlLkxJTUlULFxuICAgICAgICAncGFnZSc6ICRzY29wZS5nZXRQYWdlKCkgLSAxXG4gICAgfTtcblxuICAgICRodHRwLmdldChwcC5wYXRoKCdiYWNrb2ZmaWNlL3BhbmVsL3JlcXVlc3RzQWpheC8nLCBwYXJhbXMpKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKVxuICAgIHtcbiAgICAgICAgJHNjb3BlLmFjY2VwdGVkLnJvd3MgPSAkc2NvcGUucHJlcGFyZVJvd3MocmVzcG9uc2UuZGF0YSwgJ2FjY2VwdGVkJyk7XG4gICAgICAgICRzY29wZS5hY2NlcHRlZC5vcmRlckJ5ID0gJHNjb3BlLmdldFNvcnQoKTtcblxuICAgICAgICB2YXIgdG90YWxJdGVtcyA9IHJlc3BvbnNlLmRhdGFbMF0udG90YWw7XG5cbiAgICAgICAgJHNjb3BlLmFjY2VwdGVkLnRvdGFsID0gKCh0b3RhbEl0ZW1zIC0gdG90YWxJdGVtcyAlICRzY29wZS5MSU1JVCkgLyAkc2NvcGUuTElNSVQpICsgMTtcbiAgICAgICAgJHNjb3BlLmFjY2VwdGVkLmNvdW50ID0gdG90YWxJdGVtcztcblxuICAgICAgICAkc2NvcGUuYWNjZXB0ZWQucGFnZVNpemUgPSAkc2NvcGUuTElNSVQ7XG4gICAgICAgICRzY29wZS5hY2NlcHRlZC5jdXJyZW50UGFnZSA9ICRzY29wZS5nZXRQYWdlKCkgKiAxO1xuXG4gICAgICAgICRzY29wZS5hY2NlcHRlZC5wYXJhbXMgPSBwYXJhbXM7XG4gICAgfSk7XG5cbiAgICBwYXJhbXMgPVxuICAgIHtcbiAgICAgICAgZmFpcklkOiAkc2NvcGUuZ2V0RmFpcklkKCksXG4gICAgICAgICdvcmRlcltieV0nOiAkc2NvcGUuZ2V0U29ydCgpLFxuICAgICAgICAnb3JkZXJbb3JkZXJdJzogJHNjb3BlLmdldE9yZGVyKCksXG4gICAgICAgICdmaWx0ZXJbc3RhdHVzXSc6IHByb3Bvc2FsLlNUQVRVU19SRUpFQ1RFRCxcbiAgICAgICAgJ2ZpbHRlcltuYW1lXSc6ICRzY29wZS5nZXROYW1lKCksXG4gICAgICAgICdmaWx0ZXJbY29udHJhY3Rvcl0nOiAkc2NvcGUuZ2V0Q29udHJhY3RvcigpLFxuICAgICAgICAnZmlsdGVyW2RhdGVdJzogJHNjb3BlLmdldERhdGUoKSxcbiAgICAgICAgJ2ZpbHRlcltjb21tZW50XSc6ICRzY29wZS5nZXRDb21tZW50KCksXG4gICAgICAgICdsaW1pdCc6ICRzY29wZS5MSU1JVCxcbiAgICAgICAgJ3BhZ2UnOiAkc2NvcGUuZ2V0UGFnZSgpIC0gMVxuICAgIH07XG5cbiAgICAkaHR0cC5nZXQocHAucGF0aCgnYmFja29mZmljZS9wYW5lbC9yZXF1ZXN0c0FqYXgvJywgcGFyYW1zKSkudGhlbihmdW5jdGlvbihyZXNwb25zZSlcbiAgICB7XG4gICAgICAgICRzY29wZS5yZWplY3RlZC5yb3dzID0gJHNjb3BlLnByZXBhcmVSb3dzKHJlc3BvbnNlLmRhdGEsICdyZWplY3RlZCcpO1xuICAgICAgICAkc2NvcGUucmVqZWN0ZWQub3JkZXJCeSA9ICRzY29wZS5nZXRTb3J0KCk7XG5cbiAgICAgICAgdmFyIHRvdGFsSXRlbXMgPSByZXNwb25zZS5kYXRhWzBdLnRvdGFsO1xuXG4gICAgICAgICRzY29wZS5yZWplY3RlZC50b3RhbCA9ICgodG90YWxJdGVtcyAtIHRvdGFsSXRlbXMgJSAkc2NvcGUuTElNSVQpIC8gJHNjb3BlLkxJTUlUKSArIDE7XG4gICAgICAgICRzY29wZS5yZWplY3RlZC5jb3VudCA9IHRvdGFsSXRlbXM7XG5cbiAgICAgICAgJHNjb3BlLnJlamVjdGVkLnBhZ2VTaXplID0gJHNjb3BlLkxJTUlUO1xuICAgICAgICAkc2NvcGUucmVqZWN0ZWQuY3VycmVudFBhZ2UgPSAkc2NvcGUuZ2V0UGFnZSgpICogMTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5yZWplY3RlZC5wYXJhbXMgPSBwYXJhbXM7XG4gICAgfSk7XG5cbiAgICAkc2NvcGUucHJlcGFyZVJvd3MgPSBmdW5jdGlvbiAobW9kZWxzLCB0YWJsZSlcbiAgICB7XG4gICAgICAgIHZhciByZXN1bHQgPSBbXSxcbiAgICAgICAgICAgIHByZXBhcmVSb3cgPSAkc2NvcGVbdGFibGUgKyAnUm93J107XG5cbiAgICAgICAgZm9yICh2YXIgaSBpbiBtb2RlbHMpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBtb2RlbCA9IG1vZGVsc1tpXTtcbiAgICAgICAgICAgIHJlc3VsdFtpXSA9IHByZXBhcmVSb3cobW9kZWwpO1xuXG4gICAgICAgICAgICBpZiAobW9kZWwuc2VlbiA9PSAwKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0W2ldLmNsYXNzZXMgPSB7dmFsdWU6ICd0YmxfX3Jvd19zZWVuLWZhbHNlJywgdHlwZTogJ2Rpc2FibGUnfTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0W2ldLmNsYXNzZXMgPSB7dmFsdWU6ICcnLCB0eXBlOiAnZGlzYWJsZSd9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuXG4gICAgJHNjb3BlLmluY29taW5nUm93ID0gZnVuY3Rpb24gKG1vZGVsKVxuICAgIHtcbiAgICAgICAgdmFyIHJlc3VsdCAgICAgPSB7fSxcbiAgICAgICAgICAgIHBhdGhWaWV3ICAgPSBwcC5wYXRoKCdwcm9wb3NhbC9wcm9wb3NhbC92aWV3UHJvcG9zYWxGb3JPcmdhbml6ZXIvJywge2VJZDogbW9kZWwuaWQsIHVzZXJJZDogbW9kZWwuZXhwb25lbnRJZH0pLFxuICAgICAgICAgICAgaGFzRmlsZXNCdG5Db2xvciA9IG1vZGVsLmhhc0ZpbGVzID8gJzEnIDogJzAnO1xuXG4gICAgICAgIHJlc3VsdC5uYW1lID1cbiAgICAgICAge1xuICAgICAgICAgICAgdmFsdWU6IG1vZGVsLm5hbWUsXG4gICAgICAgICAgICBodG1sOiAkc2NlLnRydXN0QXNIdG1sKCc8YSBocmVmPVwiJyArIHBhdGhWaWV3ICsgJ1wiPicgKyBtb2RlbC5uYW1lICsgJzwvYT4nICtcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImJ1dHRvbnNcIj4nICtcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImNlbGxfX2ZpbGVcIiBzdHlsZT1cIm9wYWNpdHk6JyArIGhhc0ZpbGVzQnRuQ29sb3IgKyAnO1wiPu6PujwvZGl2PicgK1xuICAgICAgICAgICAgICAgICc8L2Rpdj4nKSxcbiAgICAgICAgICAgIHR5cGU6ICdodG1sJ1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlc3VsdC5jb250YWN0b3IgPSB7dmFsdWU6IG1vZGVsLmV4cG9uZW50TmFtZSArICcgKCcgKyBtb2RlbC5leHBvbmVudEVtYWlsICsgJyknfTtcbiAgICAgICAgcmVzdWx0LmRhdGUgICAgICA9IHt2YWx1ZTogbW9kZWwuZGF0ZX07XG5cbiAgICAgICAgY29uc29sZS5sb2cobW9kZWwpO1xuICAgICAgICByZXN1bHQuYnV0dG9ucyA9XG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NlczogJ2J1dHRvbl9tIGJ1dHRvbl9tX2dyZWVuIGJ1dHRvbl9oaWRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ0FQUFJPVkUnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldDogJy5tb2RhbC1hY2NlcHQtJyArIG1vZGVsLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kYWw6XG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ21vZGFsLWFjY2VwdC0nICsgbW9kZWwuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluazogcHAucGF0aCgncHJvcG9zYWwvcHJvcG9zYWwvYWNjZXB0Jywge2VJZDogbW9kZWwuaWR9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0LnQoJ3Byb3Bvc2FscycsICdBUFBST1ZFJykgKyAnPycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2s6IHQudCgncHJvcG9zYWxzJywgJ0FQUFJPVkUnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc2VzOiAnYnV0dG9uX20gYnV0dG9uX21fcmVkIGJ1dHRvbl9oaWRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ1JFSkVDVCcpLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OiAnLm1vZGFsLXJlbW92ZS0nICsgbW9kZWwuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICBtb2RhbDpcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnbW9kYWwtcmVtb3ZlLScgKyBtb2RlbC5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaW5rOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhdGg6ICcvcHJvcG9zYWwvcHJvcG9zYWwvcmVqZWN0JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlSWQ6IG1vZGVsLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWNJZDptb2RlbC5jbGFzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZDogbW9kZWwuZXhwb25lbnRJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZhaXJJZDogJHNjb3BlLmdldEZhaXJJZCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ1JFSkVDVCcpICsgJz8nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9rOiB0LnQoJ3Byb3Bvc2FscycsICdSRUpFQ1QnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21tZW50OiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdQT1NUJ1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIHR5cGU6ICdidXR0b25zJyxcbiAgICAgICAgICAgIHN0eWxlOiB7J3dpZHRoJzonMjM1cHgnfVxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfTtcblxuICAgICRzY29wZS5hY2NlcHRlZFJvdyA9IGZ1bmN0aW9uIChtb2RlbClcbiAgICB7XG4gICAgICAgIHZhciByZXN1bHQgICAgID0ge30sXG4gICAgICAgICAgICBwYXRoVmlldyAgID0gcHAucGF0aCgncHJvcG9zYWwvcHJvcG9zYWwvdmlld1Byb3Bvc2FsRm9yT3JnYW5pemVyLycsIHtlSWQ6IG1vZGVsLmlkLCB1c2VySWQ6IG1vZGVsLmV4cG9uZW50SWR9KSxcbiAgICAgICAgICAgIGhhc0ZpbGVzQnRuQ29sb3IgPSBtb2RlbC5oYXNGaWxlcyA/ICcxJyA6ICcwJztcbiAgICAgICAgICAgIHNlbGVjdGVkID0ge2lkOiAwLCBuYW1lOiAnLSd9O1xuXG4gICAgICAgIHJlc3VsdC5uYW1lID1cbiAgICAgICAge1xuICAgICAgICAgICAgdmFsdWU6IG1vZGVsLm5hbWUsXG4gICAgICAgICAgICBodG1sOiAkc2NlLnRydXN0QXNIdG1sKCc8YSBocmVmPVwiJyArIHBhdGhWaWV3ICsgJ1wiPicgKyBtb2RlbC5uYW1lICsgJzwvYT4nICtcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImJ1dHRvbnNcIj4nICtcbiAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImNlbGxfX2ZpbGVcIiBzdHlsZT1cIm9wYWNpdHk6JyArIGhhc0ZpbGVzQnRuQ29sb3IgKyAnO1wiPu6PujwvZGl2PicgK1xuICAgICAgICAgICAgICAgICc8L2Rpdj4nKSxcbiAgICAgICAgICAgIHR5cGU6ICdodG1sJ1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlc3VsdC5kYXRlID0ge3ZhbHVlOiBtb2RlbC5kYXRlfTtcblxuICAgICAgICByZXN1bHQuY29udGFjdG9yID0ge3ZhbHVlOiBtb2RlbC5leHBvbmVudE5hbWUgKyAnICgnICsgbW9kZWwuZXhwb25lbnRFbWFpbCArICcpJ307XG5cbiAgICAgICAgdmFyIHBheW1lbnRJdGVtcyA9IFtcbiAgICAgICAgICAgIHtpZDogcHJvcG9zYWwuUEFZTUVOVF9OT1QsIG5hbWU6ICctJ30sXG4gICAgICAgICAgICB7aWQ6IHByb3Bvc2FsLlBBWU1FTlRfUEFSVCwgbmFtZTogdC50KCdwcm9wb3NhbHMnLCAnUEFSVExZJyl9LFxuICAgICAgICAgICAge2lkOiBwcm9wb3NhbC5QQVlNRU5UX0ZVTEwsIG5hbWU6ICcxMDAlJ30sXG4gICAgICAgICAgICB7aWQ6IHByb3Bvc2FsLlBBWU1FTlRfQklMTCwgbmFtZTogdC50KCdwcm9wb3NhbHMnLCAnSU5WT0lDRSBJU1NVRUQnKX1cbiAgICAgICAgXTtcblxuICAgICAgICBmb3IgKGl0ZW0gaW4gcGF5bWVudEl0ZW1zKSB7XG4gICAgICAgICAgICBpZiAobW9kZWwucGF5bWVudCA9PSBwYXltZW50SXRlbXNbaXRlbV0uaWQpIHtcbiAgICAgICAgICAgICAgICBzZWxlY3RlZCA9IHBheW1lbnRJdGVtc1tpdGVtXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJlc3VsdC5wYXltZW50ID1cbiAgICAgICAge1xuICAgICAgICAgICAgdmFsdWU6IHNlbGVjdGVkLFxuICAgICAgICAgICAgaXRlbXM6IHBheW1lbnRJdGVtcyxcbiAgICAgICAgICAgIHR5cGU6ICdzZWxlY3QnLFxuICAgICAgICAgICAgb25jaGFuZ2U6IGZ1bmN0aW9uIChzZWxlY3RlZElkKSBcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAkaHR0cC5nZXQocHAucGF0aCgnYmFja29mZmljZS9wYW5lbC91cGRhdGVQYXltZW50LycsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZhaXJJZDogJHNjb3BlLmdldEZhaXJJZCgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgcGU6IG1vZGVsLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgcGF5bWVudDogc2VsZWN0ZWRJZCAqIDFcbiAgICAgICAgICAgICAgICAgICAgfSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBkb2N1bWVudEl0ZW1zID0gW1xuICAgICAgICAgICAge2lkOiBwcm9wb3NhbC5ET0NVTUVOVFNfTk9ULCBuYW1lOiAnLSd9LFxuICAgICAgICAgICAge2lkOiBwcm9wb3NhbC5ET0NVTUVOVFNfUkVDSVZFRCwgbmFtZTogdC50KCdwcm9wb3NhbHMnLCAnUkVDSUVWRUQnKX1cbiAgICAgICAgXTtcblxuICAgICAgICBmb3IgKGl0ZW0gaW4gZG9jdW1lbnRJdGVtcykge1xuICAgICAgICAgICAgaWYgKG1vZGVsLmRvY3VtZW50cyAqIDEgPT0gZG9jdW1lbnRJdGVtc1tpdGVtXS5pZCkge1xuICAgICAgICAgICAgICAgIHNlbGVjdGVkID0gZG9jdW1lbnRJdGVtc1tpdGVtXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJlc3VsdC5kb2N1bWVudHMgPVxuICAgICAgICB7XG4gICAgICAgICAgICB2YWx1ZTogc2VsZWN0ZWQsXG4gICAgICAgICAgICBpdGVtczogZG9jdW1lbnRJdGVtcyxcbiAgICAgICAgICAgIHR5cGU6ICdzZWxlY3QnLFxuICAgICAgICAgICAgb25jaGFuZ2U6IGZ1bmN0aW9uIChzZWxlY3RlZElkKSBcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAkaHR0cC5nZXQocHAucGF0aCgnYmFja29mZmljZS9wYW5lbC91cGRhdGVEb2N1bWVudHMvJyxcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgZmFpcklkOiAkc2NvcGUuZ2V0RmFpcklkKCksXG4gICAgICAgICAgICAgICAgICAgICAgICBwZTogbW9kZWwuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudHM6IHNlbGVjdGVkSWQgKiAxXG4gICAgICAgICAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICByZXN1bHQuYnV0dG9ucyA9XG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NlczogJ2J1dHRvbl9tIGJ1dHRvbl9tX3JlZCBidXR0b25faGlkZScsXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0LnQoJ3Byb3Bvc2FscycsICdSRUpFQ1QnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldDogJy5tb2RhbC1yZWplY3QtJyArIG1vZGVsLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kYWw6XG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ21vZGFsLXJlamVjdC0nICsgbW9kZWwuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluazogcHAucGF0aCgncHJvcG9zYWwvcHJvcG9zYWwvcmVqZWN0Jywge2VJZDogbW9kZWwuaWQsIGVjSWQ6bW9kZWwuY2xhc3MsIHVzZXJJZDogbW9kZWwuZXhwb25lbnRJZH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ1JFSkVDVCcpICsgJz8nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9rOiB0LnQoJ3Byb3Bvc2FscycsICdSRUpFQ1QnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc2VzOiAnYnV0dG9uX3NtYWxsIGJ1dHRvbl9zbWFsbF9yZW1vdmUgYnV0dG9uX2hpZGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogJ+6PjScsXG4gICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQ6ICcubW9kYWwtcmVtb3ZlLScgKyBtb2RlbC5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vZGFsOlxuICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdtb2RhbC1yZW1vdmUtJyArIG1vZGVsLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbms6IHBwLnBhdGgoJ3Byb3Bvc2FsL3Byb3Bvc2FsL2RlbGV0ZUJ5T3JnYW5pemVyJywge2VJZDogbW9kZWwuaWR9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0LnQoJ3Byb3Bvc2FscycsICdSRU1PVkVfUFJPUE9TQUwnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvazogdC50KCdwcm9wb3NhbHMnLCAnUkVNT1ZFJylcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB0eXBlOiAnYnV0dG9ucycsXG4gICAgICAgICAgICBzdHlsZTogeyd3aWR0aCc6JzIzNXB4J31cbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG5cbiAgICAkc2NvcGUucmVqZWN0ZWRSb3cgPSBmdW5jdGlvbiAobW9kZWwpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ICAgICA9IHt9LFxuICAgICAgICAgICAgcGF0aFZpZXcgICA9IHBwLnBhdGgoJ3Byb3Bvc2FsL3Byb3Bvc2FsL3ZpZXdQcm9wb3NhbEZvck9yZ2FuaXplci8nLCB7ZUlkOiBtb2RlbC5pZCwgdXNlcklkOiBtb2RlbC5leHBvbmVudElkfSksXG4gICAgICAgICAgICBoYXNGaWxlc0J0bkNvbG9yID0gbW9kZWwuaGFzRmlsZXMgPyAnMScgOiAnMCc7XG5cbiAgICAgICAgcmVzdWx0Lm5hbWUgPVxuICAgICAgICB7XG4gICAgICAgICAgICB2YWx1ZTogbW9kZWwubmFtZSxcbiAgICAgICAgICAgIGh0bWw6ICRzY2UudHJ1c3RBc0h0bWwobW9kZWwubmFtZSArXG4gICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJidXR0b25zXCI+JyArXG4gICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJjZWxsX19maWxlXCIgc3R5bGU9XCJvcGFjaXR5OicgKyBoYXNGaWxlc0J0bkNvbG9yICsgJztcIj7uj7o8L2Rpdj4nICtcbiAgICAgICAgICAgICAgICAnPC9kaXY+JyksXG4gICAgICAgICAgICB0eXBlOiAnaHRtbCdcbiAgICAgICAgfTtcblxuICAgICAgICByZXN1bHQuY29udGFjdG9yID0ge3ZhbHVlOiBtb2RlbC5leHBvbmVudE5hbWUgKyAnICgnICsgbW9kZWwuZXhwb25lbnRFbWFpbCArICcpJ307XG4gICAgICAgIHJlc3VsdC5kYXRlICAgICAgPSB7dmFsdWU6IG1vZGVsLmRhdGV9O1xuXG4gICAgICAgIHJlc3VsdC5jb21tZW50ID0ge307XG5cbiAgICAgICAgaWYgKG1vZGVsLmNvbW1lbnQgIT0gJycpIHtcbiAgICAgICAgICAgIHJlc3VsdC5jb21tZW50ICAgPSB7dG9vbHRpcDoge2lkOiAndG9vbHRpcC0nICsgbW9kZWwuZUlkLCBjb250ZW50OiBtb2RlbC5jb21tZW50LCBpY29uOiAn7oOEJ319O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xufSk7IiwicHAubW9kdWxlKCd3b3Jrc3BhY2UnKS5jb250cm9sbGVyKCdXb3Jrc3BhY2VQcm9wb3NhbEN0cmwnLCBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgdCwgcHJvcG9zYWwsICRzY2UpXG57XG4gICAgJHNjb3BlLnBhcmFtcyA9XG4gICAge1xuICAgICAgICBjb2x1bW5zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdGV4dDogdC50KCdwcm9wb3NhbHMnLCAnTkFNRScpLFxuICAgICAgICAgICAgICAgIG5hbWU6ICduYW1lJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0ZXh0OiB0LnQoJ3Byb3Bvc2FscycsICdEQVRFJyksXG4gICAgICAgICAgICAgICAgbmFtZTogJ2RhdGUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ1NUQVRVUycpLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdzdGF0dXMnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRleHQ6IHQudCgncHJvcG9zYWxzJywgJ1BBWU1FTlQnKSxcbiAgICAgICAgICAgICAgICBuYW1lOiAncGF5bWVudCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdGV4dDogdC50KCdwcm9wb3NhbHMnLCAnRE9DVU1FTlRTJyksXG4gICAgICAgICAgICAgICAgbmFtZTogJ2RvY3VtZW50cydcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ2J1dHRvbnMnLFxuICAgICAgICAgICAgICAgIHNvcnRhYmxlOiBmYWxzZVxuICAgICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBvcmRlckJ5OiAnZGF0ZScsXG4gICAgICAgIHJldmVyc2U6IHRydWVcbiAgICB9O1xuXG4gICAgJGh0dHAuZ2V0KHBwLnBhdGgoJ3dvcmtzcGFjZS9saXN0L3JlcXVlc3RzQWpheC8nKSkudGhlbihmdW5jdGlvbihyZXNwb25zZSlcbiAgICB7XG4gICAgICAgICRzY29wZS5wYXJhbXMucm93cyA9ICRzY29wZS5wcmVwYXJlUm93cyhyZXNwb25zZS5kYXRhKTtcbiAgICB9KTtcblxuICAgICRzY29wZS5wcmVwYXJlUm93cyA9IGZ1bmN0aW9uIChtb2RlbHMpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gW107XG5cbiAgICAgICAgZm9yICh2YXIgbW9kZWwgaW4gbW9kZWxzKVxuICAgICAgICB7XG4gICAgICAgICAgICByZXN1bHRbbW9kZWxdID0gJHNjb3BlLnByZXBhcmVSb3cobW9kZWxzW21vZGVsXSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG5cbiAgICAkc2NvcGUucHJlcGFyZVJvdyA9IGZ1bmN0aW9uIChtb2RlbClcbiAgICB7XG4gICAgICAgIHZhciByZXN1bHQgICAgID0ge30sXG4gICAgICAgICAgICBwYXRoRWRpdCAgID0gcHAucGF0aCgncHJvcG9zYWwvcHJvcG9zYWwvY3JlYXRlLycsIHtlY0lkOiBtb2RlbC5lY0lkLCBlSWQ6IG1vZGVsLmVJZH0pLFxuICAgICAgICAgICAgcGF0aFZpZXcgICA9IHBwLnBhdGgoJ3Byb3Bvc2FsL3Byb3Bvc2FsL3ZpZXdQcm9wb3NhbC8nLCB7ZWNJZDogbW9kZWwuZWNJZCwgZUlkOiBtb2RlbC5lSWR9KTtcblxuICAgICAgICByZXN1bHQubmFtZSA9XG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhbHVlOiBtb2RlbC5uYW1lLFxuICAgICAgICAgICAgaHRtbDogJHNjZS50cnVzdEFzSHRtbCgnPGEgaHJlZj1cIicgKyBwYXRoVmlldyArICdcIj4nICsgbW9kZWwubmFtZSArICc8L2E+JyksXG4gICAgICAgICAgICB0eXBlOiAnaHRtbCdcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgIHJlc3VsdC5kYXRlICAgICAgPSB7dmFsdWU6IG1vZGVsLmRhdGV9O1xuICAgICAgICByZXN1bHQuc3RhdHVzICAgID0ge3ZhbHVlOiBtb2RlbC5zdGF0dXMgKiAxfTtcbiAgICAgICAgcmVzdWx0LnBheW1lbnQgICA9IHt2YWx1ZTogbW9kZWwucGF5bWVudCAqIDF9O1xuICAgICAgICByZXN1bHQuZG9jdW1lbnRzID0ge3ZhbHVlOiBtb2RlbC5kb2N1bWVudHMgKiAxfTtcblxuICAgICAgICByZXN1bHQuYnV0dG9ucyA9XG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhbHVlOlxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaHJlZjogcGF0aEVkaXQsXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzZXM6ICdidXR0b25fc21hbGwgYnV0dG9uX3NtYWxsX2VkaXQgYnV0dG9uX2hpZGUnLFxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAn7o+QJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBjbGFzc2VzOiAnYnV0dG9uX3NtYWxsIGJ1dHRvbl9zbWFsbF9yZW1vdmUgYnV0dG9uX2hpZGUnLFxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAn7o+NJyxcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OiAnLm1vZGFsLScgKyBtb2RlbC5lSWQsXG4gICAgICAgICAgICAgICAgICAgIG1vZGFsOlxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnbW9kYWwtJyArIG1vZGVsLmVJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxpbms6IHBwLnBhdGgoJ3Byb3Bvc2FsL3Byb3Bvc2FsL3ByZXBhcmVEZWxldGUnLCB7ZUlkOiBtb2RlbC5lSWQsIGVjSWQ6IG1vZGVsLmVjSWQsIHVzZXJJZDogbW9kZWwudXNlcklkfSksXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0LnQoJ3Byb3Bvc2FscycsICdSRU1PVkVfUFJPUE9TQUwnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG9rOiB0LnQoJ3Byb3Bvc2FscycsICdSRU1PVkUnKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIHR5cGU6ICdidXR0b25zJ1xuICAgICAgICB9O1xuXG4gICAgICAgIGlmICgobW9kZWwuc3RhdHVzICogMSkgPT0gcHJvcG9zYWwuU1RBVFVTX1NFTlQgfHwgKG1vZGVsLnN0YXR1cyAqIDEpID09IHByb3Bvc2FsLlNUQVRVU19BQ0NFUFRFRCkge1xuICAgICAgICAgICAgcmVzdWx0LmJ1dHRvbnMgPSB7fTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJlc3VsdC5kb2N1bWVudHMudmFsdWUgPSBwcm9wb3NhbC5nZXREb2N1bWVudFN0YXR1c05hbWUocmVzdWx0LmRvY3VtZW50cy52YWx1ZSk7XG4gICAgICAgIHJlc3VsdC5wYXltZW50LnZhbHVlICAgPSBwcm9wb3NhbC5nZXRQYXltZW50U3RhdHVzTmFtZShyZXN1bHQucGF5bWVudC52YWx1ZSk7XG4gICAgICAgIHJlc3VsdC5zdGF0dXMgICAgICAgICAgPSBwcm9wb3NhbC5nZXRTdGF0dXMocmVzdWx0LnN0YXR1cy52YWx1ZSk7XG5cbiAgICAgICAgaWYgKG1vZGVsLmNvbW1lbnQgIT0gdW5kZWZpbmVkICYmIG1vZGVsLmNvbW1lbnQgIT0gJycpIHtcbiAgICAgICAgICAgIHJlc3VsdC5zdGF0dXMudG9vbHRpcCA9IHtpZDogJ3Rvb2x0aXAtJyArIG1vZGVsLmVJZCwgY29udGVudDogbW9kZWwuY29tbWVudCwgaWNvbjogJ+6DhCd9O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xufSk7IiwicHAubW9kdWxlKCdidXR0b24nLCBbJ21vZGFsJ10pLmRpcmVjdGl2ZSgnYnRuJywgZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgdGVtcGxhdGVVcmw6IHBwLnZpZXdQYXRoKCdidXR0b24nKSxcbiAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgc2NvcGU6XG4gICAgICAgIHtcbiAgICAgICAgICAgIHBhcmFtczogJz0nXG4gICAgICAgIH0sXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCwgYXR0cnMpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmIChzY29wZS5wYXJhbXMudGFyZ2V0ICE9IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHNjb3BlLnNob3cgPSBmdW5jdGlvbiAoKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgJChzY29wZS5wYXJhbXMudGFyZ2V0KS5tb2RhbCgnc2hvdycpXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG59KTsiLCIvKipcbiAqIEBhdWdtZW50cyBQcm90b3BsYW4uQmxvY2tcbiAqIEBjbGFzcyBQcm90b3BsYW4uQ2hlY2tib3hcbiAqIEBwYXJhbSBpZFxuICogQHBhcmFtIHN0YXRlXG4gKiBAcGFyYW0gc2VsZWN0b3JcbiAqIEBwYXJhbSBkaXNhYmxlZFxuICogQGNvbnN0cnVjdG9yXG4gKi9cblxuUHJvdG9wbGFuLkNoZWNrYm94ID0gZnVuY3Rpb24gKGlkLCBzZWxlY3Rvciwgc3RhdGUsIGRpc2FibGVkKVxue1xuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIHNlbGYuc2VsZWN0b3IgICAgICAgICA9IHR5cGVvZiBzZWxlY3RvciA9PSAndW5kZWZpbmVkJyA/ICcjY2hlY2tib3gtJyArIGlkIDogc2VsZWN0b3I7XG4gICAgc2VsZi5zdGF0ZSAgICAgICAgICAgID0gdHlwZW9mIHN0YXRlICAgID09ICd1bmRlZmluZWQnID8gZmFsc2UgOiBzdGF0ZTtcbiAgICBzZWxmLmRpc2FibGVkICAgICAgICAgPSB0eXBlb2YgZGlzYWJsZWQgPT0gJ3VuZGVmaW5lZCcgPyBmYWxzZSA6IGRpc2FibGVkO1xuICAgIHNlbGYuY2hpbGRyZW5TZWxlY3RvciA9ICdbZGF0YS1jaGVja2JveD1cIicgKyBpZCArICdcIl0nO1xuXG4gICAgUHJvdG9wbGFuLkJsb2NrLmFwcGx5KHRoaXMsIFtpZCwgc2VsZi5zZWxlY3Rvcl0pO1xuXG4gICAgc2VsZi5pbml0ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIGlmICgkKHNlbGYuY2hpbGRyZW5TZWxlY3RvcikubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICBzZWxmLmNoaWxkcmVuU2VsZWN0b3IgPSAnLnBhcmVudC0nICsgc2VsZi5pZDtcbiAgICAgICAgfVxuXG4gICAgICAgIHNlbGYuaGFzTm90RW1wdHlJbnB1dHMoKSA/IHNlbGYuc2V0U3RhdGUodHJ1ZSkgOiBzZWxmLnNldFN0YXRlKHNlbGYuc3RhdGUpO1xuXG4gICAgICAgICQoZG9jdW1lbnQpLmRlbGVnYXRlKHNlbGYuc2VsZWN0b3IsICdjbGljaycsIGZ1bmN0aW9uICgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHNlbGYuZ28oKTtcblxuICAgICAgICAgICAgcmV0dXJuICFkaXNhYmxlZDtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIHNlbGYub3BlbiA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICByZXR1cm4gJChzZWxmLmNoaWxkcmVuU2VsZWN0b3IpLnNsaWRlRG93bignZmFzdCcpO1xuICAgIH07XG5cbiAgICBzZWxmLmNsb3NlID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgICQoc2VsZi5jaGlsZHJlblNlbGVjdG9yKS5maW5kKCdpbnB1dCcpLnZhbCgnJyk7XG4gICAgICAgICQoc2VsZi5jaGlsZHJlblNlbGVjdG9yKS5maW5kKCdpbnB1dCcpLnRyaWdnZXIoJ2lucHV0Jyk7IC8vQFRPRE8gUmVmYWN0b3IgaXRcbiAgICAgICAgcmV0dXJuICQoc2VsZi5jaGlsZHJlblNlbGVjdG9yKS5zbGlkZVVwKCdmYXN0Jyk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuICAgIHNlbGYuZ2V0U3RhdGUgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgc2VsZi5zdGF0ZSA9ICQoc2VsZi5zZWxlY3RvcikucHJvcCgnY2hlY2tlZCcpO1xuICAgICAgICByZXR1cm4gc2VsZi5zdGF0ZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQHBhcmFtIHN0YXRlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgc2VsZi5zZXRTdGF0ZSA9IGZ1bmN0aW9uIChzdGF0ZSlcbiAgICB7XG4gICAgICAgICQoc2VsZi5zZWxlY3RvcikucHJvcCgnY2hlY2tlZCcsIHN0YXRlKTtcbiAgICAgICAgc2VsZi5nbygpO1xuICAgICAgICByZXR1cm4gc2VsZi5nZXRTdGF0ZSgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBzZWxmLmhhc05vdEVtcHR5SW5wdXRzID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHZhciByZXQgPSBmYWxzZTtcbiAgICAgICAgJChzZWxmLmNoaWxkcmVuU2VsZWN0b3IpLmVhY2goZnVuY3Rpb24gKGksIHZhbHVlKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgaW5wdXRWYWx1ZSA9ICQodmFsdWUpLmZpbmQoJ2lucHV0W3R5cGU9XCJ0ZXh0XCJdJykudmFsKCk7XG4gICAgICAgICAgICBpZiAoaW5wdXRWYWx1ZSAhPSAnJyAmJiBpbnB1dFZhbHVlICE9IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHJldCA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiByZXQ7XG4gICAgfTtcblxuICAgIHNlbGYuZ28gPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgc2VsZi5nZXRTdGF0ZSgpID8gc2VsZi5vcGVuKCkgOiBzZWxmLmNsb3NlKCk7XG4gICAgfTtcblxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHNlbGYuaW5pdCgpO1xuICAgIH0sIDEwMDApO1xufTsiLCIvKipcbiAqIEBhdWdtZW50cyBQcm90b3BsYW4uQmxvY2tcbiAqIEBjbGFzcyBQcm90b3BsYW4uRGF0ZVBpY2tlclxuICogQHBhcmFtIGlkXG4gKiBAcGFyYW0gc3RhcnRcbiAqIEBwYXJhbSBlbmRcbiAqIEBwYXJhbSBwYXJhbXNcbiAqIEBwYXJhbSBibG9ja1BhcmFtc1xuICogQGNvbnN0cnVjdG9yXG4gKi9cblxuUHJvdG9wbGFuLkRhdGVQaWNrZXIgPSBmdW5jdGlvbiAoaWQsIHN0YXJ0LCBlbmQsIHBhcmFtcywgYmxvY2tQYXJhbXMpXG57XG4gICAgUHJvdG9wbGFuLkJsb2NrLmFwcGx5KHRoaXMsIFtpZCwgJy5kYXRlUGlja2VyJyArIGlkXSk7XG5cbiAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICBzZWxmLm1pbkRhdGUgICAgID0gdHlwZW9mIHN0YXJ0ICAgICAgID09ICd1bmRlZmluZWQnID8gbmV3IERhdGUoMCkgOiBuZXcgRGF0ZShEYXRlLnBhcnNlKHN0YXJ0KSk7XG4gICAgc2VsZi5tYXhEYXRlICAgICA9IHR5cGVvZiBlbmQgICAgICAgICA9PSAndW5kZWZpbmVkJyA/IG5ldyBEYXRlKCkgIDogbmV3IERhdGUoRGF0ZS5wYXJzZShlbmQpKTtcbiAgICBzZWxmLnBhcmFtcyAgICAgID0gdHlwZW9mIHBhcmFtcyAgICAgID09ICd1bmRlZmluZWQnID8ge30gICAgICAgICAgOiBwYXJhbXM7XG4gICAgc2VsZi5ibG9ja1BhcmFtcyA9IHR5cGVvZiBibG9ja1BhcmFtcyA9PSAndW5kZWZpbmVkJyA/IHt9ICAgICAgICAgIDogYmxvY2tQYXJhbXM7XG5cbiAgICBzZWxmLnN0YXJ0TW9udGggICAgPSAobmV3IERhdGUoc2VsZi5taW5EYXRlKSkuZ2V0TW9udGgoKSArIDE7XG4gICAgc2VsZi5lbmRTZWxlY3RvciAgID0gc2VsZi5zZWxlY3RvciArICctZW5kJztcbiAgICBzZWxmLmNvdW50ICAgICAgICAgPSAwO1xuXG4gICAgc2VsZi5tYXhEYXRlLnNldERhdGUoc2VsZi5tYXhEYXRlLmdldERhdGUoKSArIDEpO1xuXG4gICAgc2VsZi5pbml0ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgICQoc2VsZi5zZWxlY3RvcikucGVyaW9kcGlja2VyKHNlbGYubWVyZ2Uoe1xuICAgICAgICAgICAgZW5kOiBzZWxmLnNlbGVjdG9yICsgJy1lbmQnLFxuICAgICAgICAgICAgbm9yYW5nZTogdHJ1ZSxcbiAgICAgICAgICAgIG1vdXNld2hlZWw6IGZhbHNlLFxuICAgICAgICAgICAgbGlrZVhEU29mdERhdGVUaW1lUGlja2VyOiB0cnVlLFxuICAgICAgICAgICAgY2VsbHM6IFsxLCAyXSxcbiAgICAgICAgICAgIHdpdGhvdXRCb3R0b21QYW5lbDogdHJ1ZSxcbiAgICAgICAgICAgIHllYXJzTGluZTogZmFsc2UsXG4gICAgICAgICAgICB0aXRsZTogZmFsc2UsXG4gICAgICAgICAgICBjbG9zZUJ1dHRvbjogZmFsc2UsXG4gICAgICAgICAgICBmdWxsc2l6ZUJ1dHRvbjogZmFsc2UsXG4gICAgICAgICAgICBtaW5EYXRlOiBzZWxmLm1pbkRhdGUudG9JU09TdHJpbmcoKSxcbiAgICAgICAgICAgIG1heERhdGU6IHNlbGYubWF4RGF0ZS50b0lTT1N0cmluZygpLFxuICAgICAgICAgICAgc3RhcnRNb250aDogc2VsZi5zdGFydE1vbnRoLFxuICAgICAgICAgICAgZm9ybWF0RGF0ZTogJ1lZWVkvTU0vREQnLFxuICAgICAgICAgICAgdGltZXBpY2tlck9wdGlvbnM6IHtcbiAgICAgICAgICAgICAgICB0d2VsdmVIb3Vyc0Zvcm1hdDogZmFsc2UsXG4gICAgICAgICAgICAgICAgaG91cnM6IHRydWUsXG4gICAgICAgICAgICAgICAgbWludXRlczogdHJ1ZSxcbiAgICAgICAgICAgICAgICBzZWNvbmRzOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBhbXBtOiBmYWxzZVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCBzZWxmLnBhcmFtcykpO1xuICAgIH07XG5cbiAgICBzZWxmLmFkZEVuZERhdGUgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9ICcnLFxuICAgICAgICAgICAgZGF0ZXMgID0gJChzZWxmLnNlbGVjdG9yKS5wZXJpb2RwaWNrZXIoJ3ZhbHVlJyk7XG5cbiAgICAgICAgaWYgKGRhdGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IHNlbGYuZm9ybWF0RGF0ZShkYXRlc1swXSkgKyAnIC0gJyArIHNlbGYuZm9ybWF0RGF0ZShkYXRlc1sxXSk7XG4gICAgICAgICAgICBzZWxmLnNldENvdW50KGRhdGVzWzFdIC0gZGF0ZXNbMF0pO1xuICAgICAgICB9XG5cbiAgICAgICAgJChzZWxmLnNlbGVjdG9yKS52YWwocmVzdWx0KTtcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuXG4gICAgc2VsZi5mb3JtYXREYXRlID0gZnVuY3Rpb24gKGRhdGUpXG4gICAge1xuICAgICAgICB2YXIgeWVhciAgICA9IGRhdGUuZ2V0RnVsbFllYXIoKSxcbiAgICAgICAgICAgIG1vbnRoICAgPSAoJzAnICsgKGRhdGUuZ2V0TW9udGgoKSArIDEpKS5zbGljZSgtMiksXG4gICAgICAgICAgICBkYXkgICAgID0gKCcwJyArIGRhdGUuZ2V0RGF0ZSgpKS5zbGljZSgtMiksXG4gICAgICAgICAgICBob3VycyAgID0gKCcwJyArIGRhdGUuZ2V0SG91cnMoKSkuc2xpY2UoLTIpLFxuICAgICAgICAgICAgbWludXRlcyA9ICgnMCcgKyBkYXRlLmdldE1pbnV0ZXMoKSkuc2xpY2UoLTIpLFxuICAgICAgICAgICAgc2Vjb25kcyA9ICgnMCcgKyBkYXRlLmdldFNlY29uZHMoKSkuc2xpY2UoLTIpLFxuICAgICAgICAgICAgcmVzdWx0ICA9IHllYXIgKyAnLycgKyBtb250aCArICcvJyArIGRheTtcblxuICAgICAgICBpZiAoaG91cnMgPiAwIHx8IG1pbnV0ZXMgPiAwIHx8IHNlY29uZHMgPiAwKSB7XG4gICAgICAgICAgICByZXN1bHQgPSByZXN1bHQgKyAnICcgKyBob3VycyArICc6JyArIG1pbnV0ZXMgKyAnOicgKyBzZWNvbmRzO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuXG4gICAgc2VsZi5zZXRDb3VudCA9IGZ1bmN0aW9uIChjb3VudClcbiAgICB7XG4gICAgICAgIHNlbGYuY291bnQgPSBjb3VudDtcbiAgICAgICAgcmV0dXJuIGNvdW50O1xuICAgIH07XG5cbiAgICBzZWxmLmdldERheXMgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQoc2VsZi5jb3VudCAvICgxMDAwICogNjAgKiA2MCAqIDI0KSArIDEpO1xuICAgIH07XG5cbiAgICBzZWxmLmdldEhvdXJzID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHJldHVybiBNYXRoLnJvdW5kKHNlbGYuY291bnQgLyAoMTAwMCAqIDYwICogNjApKTtcbiAgICB9O1xuXG4gICAgc2VsZi5nbyA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICBpZiAoJChzZWxmLmVuZFNlbGVjdG9yKS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBzZWxmLmFkZEVuZERhdGUoKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBzZWxmLmluaXQoKTtcbn07XG4iLCIvKipcbiAqIEBhdWdtZW50cyBQcm90b3BsYW4uQmxvY2tcbiAqIEBjbGFzcyBQcm90b3BsYW4uSW5wdXRcbiAqIEBwYXJhbSBwYXJhbXNcbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cbi8vQFRPRE8gY3JlYXRlIGNvbnN0YW50c1xuUHJvdG9wbGFuLklucHV0ID0gZnVuY3Rpb24gKHBhcmFtcylcbntcbiAgICBQcm90b3BsYW4uQmxvY2suYXBwbHkodGhpcywgW3BhcmFtcy5pZCwgcGFyYW1zLnNlbGVjdG9yXSk7XG5cbiAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICBzZWxmLnBhcmFtcyAgID0gdHlwZW9mIHBhcmFtcy5wYXJhbXMgICA9PSAndW5kZWZpbmVkJyA/IHt9ICAgICAgICAgICAgICAgICAgIDogcGFyYW1zO1xuICAgIHNlbGYudHlwZSAgICAgPSB0eXBlb2YgcGFyYW1zLnR5cGUgICAgID09ICd1bmRlZmluZWQnID8gJ3N0cmluZycgICAgICAgICAgICAgOiBwYXJhbXMudHlwZTtcbiAgICBzZWxmLnNlbGVjdG9yID0gdHlwZW9mIHBhcmFtcy5zZWxlY3RvciA9PSAndW5kZWZpbmVkJyA/ICcuaW5wdXQnICsgcGFyYW1zLmlkIDogcGFyYW1zLnNlbGVjdG9yO1xuXG4gICAgc2VsZi5pbml0ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIGlmICh0eXBlb2Ygc2VsZlsnc2V0VHlwZScgKyBzZWxmLmNhcGl0YWxpemVGaXJzdExldHRlcihzZWxmLnR5cGUpXSA9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICB2YXIgZm4gPSBzZWxmWydzZXRUeXBlJyArIHNlbGYuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKHNlbGYudHlwZSldO1xuICAgICAgICAgICAgZm4oKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBzZWxmLnNldFR5cGVJbnQgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgc2VsZi50eXBlID0gJ2ludCc7XG5cbiAgICAgICAgJChkb2N1bWVudCkuZGVsZWdhdGUoc2VsZi5zZWxlY3RvciwgJ2tleXVwIGtleXByZXNzJywgZnVuY3Rpb24gKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBzeW1ib2wgPSAoZS53aGljaCkgPyBlLndoaWNoIDogZS5rZXlDb2RlO1xuICAgICAgICAgICAgaWYgKHN5bWJvbCA9PSA4KSByZXR1cm4gdHJ1ZTsgLy9AVE9ETyByZXdyaXRlIHRvIHN3aXRjaFxuICAgICAgICAgICAgaWYgKHN5bWJvbCA8IDQ4IHx8IHN5bWJvbCA+IDU3KSAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgc2VsZi5zZXRUeXBlRG91YmxlID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHNlbGYudHlwZSA9ICdkb3VibGUnO1xuXG4gICAgICAgICQoZG9jdW1lbnQpLmRlbGVnYXRlKHNlbGYuc2VsZWN0b3IsICdrZXl1cCBrZXlwcmVzcycsIGZ1bmN0aW9uIChlKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgc3ltYm9sID0gKGUud2hpY2gpID8gZS53aGljaCA6IGUua2V5Q29kZTtcbiAgICAgICAgICAgIGlmIChzeW1ib2wgPT0gNDYpIHJldHVybiB0cnVlOyAvL0BUT0RPIHJld3JpdGUgdG8gc3dpdGNoXG4gICAgICAgICAgICBpZiAoc3ltYm9sID09IDgpIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgLy8gaWYgKHN5bWJvbCA9PSA0NCkgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICBpZiAoc3ltYm9sIDwgNDggfHwgc3ltYm9sID4gNTcpICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBzZWxmLnNldFR5cGVTdHJpbmcgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgc2VsZi50eXBlID0gJ3N0cmluZyc7XG4gICAgfTtcblxuICAgIHNlbGYub25seU51bWJlcnMgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICB9O1xuXG4gICAgc2VsZi5pbml0KCk7XG59OyIsIi8qKlxuICogQ2xhc3MgTW9kYWxcbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cblByb3RvcGxhbi5Nb2RhbCA9IGZ1bmN0aW9uICgpXG57XG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHZhciBzZWxlY3RvciA9ICcubW9kYWwnO1xuICAgICAgICAkKHNlbGVjdG9yKS5vbignc2hvd24uYnMubW9kYWwnLCBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgICQodGhpcykuZmluZCgnaW5wdXQnKS5lYWNoKGZ1bmN0aW9uICgpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWYgKCQodGhpcykuYXR0cigndHlwZScpICE9ICdoaWRkZW4nKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKHNlbGVjdG9yKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICAkKHRoaXMpLmZpbmQoJ2lucHV0JykudmFsKCcnKTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIHRoaXMuY29uZmlybSA9IGZ1bmN0aW9uIChwYXJhbXMpXG4gICAge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgY29uZmlybVRleHQ6ICcnLFxuICAgICAgICAgICAgcGF0aDogJycsXG4gICAgICAgICAgICBhZnRlcjogJycsXG4gICAgICAgICAgICBpZDogJycsXG4gICAgICAgICAgICBtZXRob2Q6ICdnZXQnLFxuICAgICAgICAgICAgZm9ybUlkOiAnJyxcblxuICAgICAgICAgICAgcnVuOiBmdW5jdGlvbihwYXJhbXMpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgJCgnYm9keScpLm9uKCdjbGljaycsICcjJyArIHBhcmFtcy5pZCwgZnVuY3Rpb24gKClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcy5yZW1vdmVfYnRuID0gJCh0aGlzKTtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgdGFyZ2V0ID0gJChwYXJhbXMucmVtb3ZlX2J0bikuYXR0cignZGF0YS10YXJnZXQnKTtcblxuICAgICAgICAgICAgICAgICAgICAkKHRhcmdldCkubW9kYWwoJ3Nob3cnKTtcblxuICAgICAgICAgICAgICAgICAgICAkKHRhcmdldCArICcgLmpzLW1vZGFsLW9rJykuY2xpY2soZnVuY3Rpb24gKGUpXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHBhcmFtcy5tZXRob2Q9PSdnZXQnKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkLmdldChwYXJhbXMucGF0aCkuZG9uZShwYXJhbXMuYWZ0ZXIocGFyYW1zKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHBhcmFtcy5tZXRob2Q9PSdwb3N0Jyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJC5wb3N0KHBhcmFtcy5wYXRoLCAkKCcjJyArIHBhcmFtcy5mb3JtSWQpLnNlcmlhbGl6ZSgpKS5kb25lKHBhcmFtcy5hZnRlcihwYXJhbXMpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgJCh0YXJnZXQpLm1vZGFsKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICQoJy5qcy1tb2RhbC1jYW5jZWwnKS5jbGljayhmdW5jdGlvbiAoZSlcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICQodGFyZ2V0KS5tb2RhbCgnaGlkZScpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfS5ydW4ocGFyYW1zKTtcbiAgICB9O1xuXG4gICAgdGhpcy5pbml0KCk7XG59O1xuXG5wcC5tb2R1bGUoJ21vZGFsJywgWyd0J10pLmRpcmVjdGl2ZSgnbW9kYWwnLCBmdW5jdGlvbigkaHR0cCwgdCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIHRlbXBsYXRlVXJsOiBwcC52aWV3UGF0aCgnbW9kYWwnKSxcbiAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgc2NvcGU6XG4gICAgICAgIHtcbiAgICAgICAgICAgIHBhcmFtczogJz0nXG4gICAgICAgIH0sXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCwgYXR0cnMpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHNjb3BlLm9rID0gZnVuY3Rpb24gKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZihzY29wZS5wYXJhbXMudHlwZSA9PSAnUE9TVCcpe1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5wYXJhbXMubGluay5wYXJhbXMubWVzc2FnZSA9IHNjb3BlLnBhcmFtcy5tZXNzYWdlO1xuICAgICAgICAgICAgICAgICAgICB2YXIgZUlkID1zY29wZS5wYXJhbXMubGluay5wYXJhbXMuZUlkO1xuICAgICAgICAgICAgICAgICAgICBkZWxldGUgc2NvcGUucGFyYW1zLmxpbmsucGFyYW1zLmVJZDtcblxuICAgICAgICAgICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiAnLycgKyBwcC5sYW5nICsgc2NvcGUucGFyYW1zLmxpbmsucGF0aCArICcvP2VJZD0nK2VJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6c2NvcGUucGFyYW1zLmxpbmsucGFyYW1zLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZToncG9zdCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOmZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoZWxlbWVudCkuZmluZCgnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB9ZWxzZXtcblxuICAgICAgICAgICAgICAgICAgICAkaHR0cC5nZXQoc2NvcGUucGFyYW1zLmxpbmspLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoZWxlbWVudCkuZmluZCgnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgJCgnYVtkYXRhLXRhcmdldD1cIi4nICsgc2NvcGUucGFyYW1zLm5hbWUgKyAnXCJdJykucGFyZW50cygnLnRibF9fcm93Jykuc2xpZGVVcCgzMDApO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgc2NvcGUuY2FuY2VsID0gZnVuY3Rpb24gKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAkKGVsZW1lbnQpLmZpbmQoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBzY29wZS50ID0gZnVuY3Rpb24gKGtleSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdC50KCdwcm9wb3NhbHMnLCBrZXkpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgIH07XG59KTsiLCJwcC5tb2R1bGUoJ3BwLXNlbGVjdCcsIFtdKS5kaXJlY3RpdmUoJ3Bwc2VsZWN0JywgZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgdGVtcGxhdGVVcmw6IHBwLnZpZXdQYXRoKCdwcC1zZWxlY3QnKSxcbiAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgc2NvcGU6XG4gICAgICAgIHtcbiAgICAgICAgICAgIFwicGFyYW1zXCI6ICc9J1xuICAgICAgICB9LFxuICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKVxuICAgICAgICB7XG4gICAgICAgICAgICBzY29wZS5jaGFuZ2UgPSBmdW5jdGlvbiAoKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHNjb3BlLnBhcmFtcy5vbmNoYW5nZShzY29wZS5wYXJhbXMudmFsdWUuaWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcbn0pOyIsIi8qKlxuICogQ2xhc3MgUmFkaW9cbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cblByb3RvcGxhbi5SYWRpbyA9IGZ1bmN0aW9uICgpXG57XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xufTsiLCIvKipcbiAqIEBhdWdtZW50cyBQcm90b3BsYW4uQmxvY2tcbiAqIEBjbGFzcyBQcm90b3BsYW4uU2Nyb2xsVXBcbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cblByb3RvcGxhbi5TY3JvbGxVcCA9IGZ1bmN0aW9uICgpXG57XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgc2VsZi5zZWxlY3RvciA9ICcuc2Nyb2xsLXVwJztcblxuICAgIFByb3RvcGxhbi5CbG9jay5hcHBseSh0aGlzLCBbJ3Njcm9sbC11cCcsIHNlbGYuc2VsZWN0b3JdKTtcblxuICAgIHNlbGYuaW5pdCA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICBzZWxmLnVwZGF0ZSgpO1xuXG4gICAgICAgICQoZG9jdW1lbnQpLnNjcm9sbChmdW5jdGlvbiAoKVxuICAgICAgICB7XG4gICAgICAgICAgICBzZWxmLnVwZGF0ZSgpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKHNlbGYuc2VsZWN0b3IpLmNsaWNrKGZ1bmN0aW9uICgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHdpbmRvdy5zY3JvbGwoMCAsMCk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBzZWxmLnVwZGF0ZSA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICB2YXIgZGlzcGxheSA9ICdub25lJztcbiAgICAgICAgXG4gICAgICAgIGlmICh3aW5kb3cucGFnZVlPZmZzZXQgPiAyMDApIHtcbiAgICAgICAgICAgIGRpc3BsYXkgPSAnYmxvY2snO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAkKHNlbGYuc2VsZWN0b3IpLmNzcygnZGlzcGxheScsIGRpc3BsYXkpO1xuICAgIH07XG5cbiAgICBzZWxmLmluaXQoKTtcbn07XG5cbnByb3RvcGxhbi5lbGVtZW50c1snc2Nyb2xsLXVwJ10gPSBuZXcgUHJvdG9wbGFuLlNjcm9sbFVwKCk7IiwiLyoqXG4gKiBAYXVnbWVudHMgUHJvdG9wbGFuLkJsb2NrXG4gKiBAY2xhc3MgUHJvdG9wbGFuLlNwaW5uZXJcbiAqIEBwYXJhbSBwYXJhbXNcbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cblByb3RvcGxhbi5TcGlubmVyID0gZnVuY3Rpb24gKHBhcmFtcylcbntcbiAgICBQcm90b3BsYW4uQmxvY2suYXBwbHkodGhpcywgW3BhcmFtcy5pZCwgcGFyYW1zLnNlbGVjdG9yXSk7XG5cbiAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICBzZWxmLnN0ZXAgPSBwYXJhbXMuc3RlcCAhPSB1bmRlZmluZWQgPyBwYXJhbXMuc3RlcCA6IDE7XG5cbiAgICBzZWxmLnVwICAgPSAnLnNwaW5uZXJfX2J0bl91cC0nICsgc2VsZi5pZDtcbiAgICBzZWxmLmRvd24gPSAnLnNwaW5uZXJfX2J0bl9kb3duLScgKyBzZWxmLmlkO1xuICAgIHNlbGYuc3RhdGUgPSAwO1xuICAgIHNlbGYubWluID0gMDtcblxuICAgIHNlbGYuaW5pdCA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICAkKHNlbGYudXApLmNsaWNrKGZ1bmN0aW9uICgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHNlbGYuc2V0U3RhdGUoc2VsZi5zdGF0ZSAqIDEgKyBzZWxmLnN0ZXAgKiAxKTtcbiAgICAgICAgICAgIHNlbGYucmVuZGVyKCk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoc2VsZi5kb3duKS5jbGljayhmdW5jdGlvbiAoKVxuICAgICAgICB7XG4gICAgICAgICAgICBzZWxmLnNldFN0YXRlKHNlbGYuc3RhdGUgKiAxIC0gc2VsZi5zdGVwICogMSk7XG4gICAgICAgICAgICBzZWxmLnJlbmRlcigpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKGRvY3VtZW50KS5kZWxlZ2F0ZShzZWxmLnNlbGVjdG9yLCAna2V5dXAga2V5cHJlc3MnLCBmdW5jdGlvbiAoZSlcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgc2VsZi5yZW5kZXIgPSBmdW5jdGlvbiAoKVxuICAgIHtcbiAgICAgICAgJChzZWxmLnNlbGVjdG9yKS52YWwoc2VsZi5zdGF0ZSk7XG4gICAgICAgICQoc2VsZi5zZWxlY3RvcikudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgfTtcblxuICAgIHNlbGYuc2V0U3RhdGUgPSBmdW5jdGlvbiAodmFsdWUpXG4gICAge1xuICAgICAgICB2YXIgcmVzdWx0ID0gc2VsZi5taW47XG5cbiAgICAgICAgaWYgKHZhbHVlID49IHNlbGYubWluKSB7XG4gICAgICAgICAgICByZXN1bHQgPSB2YWx1ZVxuICAgICAgICB9XG5cbiAgICAgICAgc2VsZi5zdGF0ZSA9IHJlc3VsdDtcbiAgICB9O1xuXG4gICAgc2VsZi5pbml0KCk7XG59OyIsIi8qKlxuICogQGF1Z21lbnRzIFByb3RvcGxhbi5CbG9ja1xuICogQGNsYXNzIFByb3RvcGxhbi5TcG9pbGVyXG4gKiBAcGFyYW0gcGFyYW1zXG4gKiBAY29uc3RydWN0b3JcbiAqL1xuXG5Qcm90b3BsYW4uU3BvaWxlciA9IGZ1bmN0aW9uIChwYXJhbXMpXG57XG4gICAgUHJvdG9wbGFuLkJsb2NrLmFwcGx5KHRoaXMsIFtwYXJhbXMuaWQsIHBhcmFtcy5zZWxlY3Rvcl0pO1xuXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgc2VsZi5zZWxlY3RvciAgICA9IHBhcmFtcy5zZWxlY3RvciAgICA9PSB1bmRlZmluZWQgPyAnLnNwb2lsZXInICAgICAgICAgICAgICAgICAgICAgOiBwYXJhbXMuc2VsZWN0b3I7XG4gICAgc2VsZi5jbGlja0FyZWEgICA9IHBhcmFtcy5jbGlja0FyZWEgICA9PSB1bmRlZmluZWQgPyAnLnNwb2lsZXInICAgICAgICAgICAgICAgICAgICAgOiBwYXJhbXMuY2xpY2tBcmVhO1xuICAgIHNlbGYudGFyZ2V0ICAgICAgPSBwYXJhbXMudGFyZ2V0ICAgICAgPT0gdW5kZWZpbmVkID8gJycgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogcGFyYW1zLnRhcmdldDtcbiAgICBzZWxmLmhpZGRlbklucHV0ID0gcGFyYW1zLmhpZGRlbklucHV0ID09IHVuZGVmaW5lZCA/ICcjQXR0cmlidXRlX2F0dHItJyArIHBhcmFtcy5pZCA6IHBhcmFtcy5oaWRkZW5JbnB1dDtcbiAgICBzZWxmLndyYXAgICAgICAgID0gcGFyYW1zLndyYXAgICAgICAgID09IHVuZGVmaW5lZCA/ICcuc3BvaWxlci13cmFwLScgKyBwYXJhbXMuaWQgICA6IHBhcmFtcy53cmFwO1xuXG4gICAgc2VsZi5zdGF0ZSA9IDA7XG4gICAgXG4gICAgc2VsZi5pbml0ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgICQoc2VsZi5jbGlja0FyZWEpLmNsaWNrKGZ1bmN0aW9uICgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmIChwYXJhbXMud3JhcCAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAkKHNlbGYud3JhcCkuc2xpZGVUb2dnbGUoMTAwKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJChzZWxmLnRhcmdldCkuc2xpZGVUb2dnbGUoMTAwKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJChzZWxmLnNlbGVjdG9yKS50b2dnbGVDbGFzcygnb3BlbicpO1xuICAgICAgICB9KTtcblxuICAgICAgICBzZWxmLnNldFZhbHVlKHNlbGYud3JhcElzTm90RW1wdHkoKSk7XG5cbiAgICAgICAgJChkb2N1bWVudCkuZGVsZWdhdGUoc2VsZi53cmFwICsgJyBpbnB1dCcsICdpbnB1dCBjaGFuZ2UnLCBmdW5jdGlvbiAoKVxuICAgICAgICB7XG4gICAgICAgICAgICBzZWxmLnNldFZhbHVlKHNlbGYud3JhcElzTm90RW1wdHkoKSk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBzZWxmLnNldFZhbHVlID0gZnVuY3Rpb24gKHZhbHVlKVxuICAgIHtcbiAgICAgICAgJChzZWxmLmhpZGRlbklucHV0KS52YWwoMCk7XG4gICAgICAgIGlmICh2YWx1ZSA9PSAxKSB7XG4gICAgICAgICAgICAkKHNlbGYuaGlkZGVuSW5wdXQpLnZhbCgxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh2YWx1ZSA9PSAxKSB7XG4gICAgICAgICAgICAkKHNlbGYuc2VsZWN0b3IpLmFkZENsYXNzKCdvcGVuJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKHNlbGYuc2VsZWN0b3IpLnJlbW92ZUNsYXNzKCdvcGVuJyk7XG4gICAgICAgIH1cblxuICAgICAgICB2YWx1ZSA9PSAxID8gJChzZWxmLndyYXApLnNsaWRlRG93bigpIDogJChzZWxmLndyYXApLnNsaWRlVXAoKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQHJldHVybnMge2ludH1cbiAgICAgKi9cbiAgICBzZWxmLndyYXBJc05vdEVtcHR5ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHZhciByZXN1bHQgPSAwO1xuXG4gICAgICAgICQoc2VsZi53cmFwICsgJyBpbnB1dCcpLmVhY2goZnVuY3Rpb24gKGksIGl0ZW0pXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmKCQoaXRlbSkudmFsKCkgIT0gJycgJiYgJChpdGVtKS52YWwoKSAhPSAnMCcpIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSAxO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG5cbiAgICAvL0BUT0RPIFJlbW92ZVxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHNlbGYuaW5pdCgpO1xuICAgIH0sIDEwMDApO1xufTsiLCJwcC5tb2R1bGUoJ3RleHRmaWVsZCcsIFtdKS5kaXJlY3RpdmUoJ3RleHRmaWVsZCcsIGZ1bmN0aW9uKCRyb290U2NvcGUpXG57XG4gICAgcmV0dXJuIHtcbiAgICAgICAgdGVtcGxhdGVVcmw6ICcvc3RhdGljL3RlbXBsYXRlcy90ZXh0ZmllbGQuaHRtbCcsXG4gICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgIHNjb3BlOlxuICAgICAgICB7XG4gICAgICAgICAgICBwYXJhbXM6ICc9J1xuICAgICAgICB9LFxuXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCwgYXR0cnMpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHNjb3BlLmNvbmZpZyA9IHNjb3BlLnBhcmFtcztcblxuICAgICAgICAgICAgc2NvcGUuaWQgICAgID0gc2NvcGUuY29uZmlnLmlkO1xuICAgICAgICAgICAgc2NvcGUudmFsdWUgID0gc2NvcGUuY29uZmlnLnZhbHVlO1xuXG4gICAgICAgICAgICBzY29wZS5maWVsZCA9IHt9O1xuXG4gICAgICAgICAgICBzY29wZS5zZXREZWZhdWx0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuZm9yRWFjaChzY29wZS5maWVsZCwgZnVuY3Rpb24odmFsdWUsIGtleSkge1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5maWVsZFtrZXldID0gc2NvcGUuY29uZmlnLnByb3BlcnRpZXNba2V5XTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHNjb3BlLnNldERlZmF1bHQoKTtcblxuICAgICAgICAgICAgYW5ndWxhci5mb3JFYWNoKHNjb3BlLmZpZWxkLCBmdW5jdGlvbih2YWx1ZSwga2V5KSB7XG4gICAgICAgICAgICAgICAgc2NvcGUuZmllbGRba2V5XSA9IHNjb3BlLmNvbmZpZy5wcm9wZXJ0aWVzW2tleV07XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgc2NvcGUuJG9uKCd1cGRhdGUnLCBmdW5jdGlvbigkZXZlbnQsICRzaWduYWwpe1xuICAgICAgICAgICAgICAgIHZhciBkZXBlbmRlbmN5ID0gc2NvcGUuZ2V0RGVwZW5kZW5jeSgkc2lnbmFsLmlkKTtcblxuICAgICAgICAgICAgICAgIGlmKGRlcGVuZGVuY3kgIT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgICAgICB2YXIgZm4gPSBzY29wZS5mdW5jdGlvbnNbZGVwZW5kZW5jeS5mbl07XG4gICAgICAgICAgICAgICAgICAgIGlmKGZuICE9IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICBmbihkZXBlbmRlbmN5LmZuQXJncywgJHNpZ25hbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5nbygkc2lnbmFsLmluaXQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHNjb3BlLmdvID0gZnVuY3Rpb24oaW5pdCkge1xuICAgICAgICAgICAgICAgIGluaXQgPSB0eXBlb2YgaW5pdCA9PSAndW5kZWZpbmVkJyA/IGZhbHNlIDogaW5pdDtcblxuICAgICAgICAgICAgICAgIGlmKHNjb3BlLnZhbHVlICE9IG51bGwpe1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5maWVsZC5zdW1tYXJ5VmFsdWUgPSBzY29wZS5maWVsZC5wcmljZSAqIHNjb3BlLnZhbHVlICogc2NvcGUuZmllbGQuZGlzY291bnQ7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKGlzTmFOKHNjb3BlLmZpZWxkLnN1bW1hcnlWYWx1ZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmZpZWxkLnN1bW1hcnlWYWx1ZSA9ICctLS0nXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgc2NvcGUuc2FmZUFwcGx5KCk7XG5cbiAgICAgICAgICAgICAgICBpZiAoc2NvcGUudmFsdWUgIT0gJycpIHtcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KCd1cGRhdGUnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZCA6IHNjb3BlLmNvbmZpZy5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkOiBzY29wZS5maWVsZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0VmFsdWU6IHNjb3BlLnZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvcGVydGllczogc2NvcGUuY29uZmlnLnByb3BlcnRpZXMsXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQ6IHNjb3BlLmNvbmZpZy5wYXJlbnQsXG4gICAgICAgICAgICAgICAgICAgICAgICBpbml0OiBpbml0XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHNjb3BlLmdldERlcGVuZGVuY3kgPSBmdW5jdGlvbihpZCl7XG4gICAgICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuZm9yRWFjaChzY29wZS5jb25maWcuZGVwZW5kZW5jaWVzLCBmdW5jdGlvbihkZXBlbmRlbmN5KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKGRlcGVuZGVuY3kuaWQgPT0gaWQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gZGVwZW5kZW5jeTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHNjb3BlLmZ1bmN0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICBpbmhlcml0IDogZnVuY3Rpb24oJGFyZ3MsICRzaWduYWwpe1xuICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmZvckVhY2goc2NvcGUuZmllbGQsIGZ1bmN0aW9uKHZhbHVlLCBrZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCRzaWduYWwucHJvcGVydGllc1trZXldICE9IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZmllbGRba2V5XSA9ICRzaWduYWwucHJvcGVydGllc1trZXldO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBzY29wZS5ub3JtYWxpemUgPSBmdW5jdGlvbigkdmFsdWUpe1xuICAgICAgICAgICAgICAgIHJldHVybiBwYXJzZUZsb2F0KHBhcnNlRmxvYXQoJHZhbHVlKS50b0ZpeGVkKDIpKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHNjb3BlLnNhZmVBcHBseSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciBwaGFzZSA9IHRoaXMuJHJvb3QuJCRwaGFzZTtcbiAgICAgICAgICAgICAgICBpZihwaGFzZSAhPSAnJGFwcGx5JyAmJiBwaGFzZSAhPSAnJGRpZ2VzdCcpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kYXBwbHkoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBzY29wZS5yZXNldCA9IGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgc2NvcGUudmFsdWUgPSAnJztcbiAgICAgICAgICAgICAgICBzY29wZS5maWVsZC5zdW1tYXJ5VmFsdWUgPSAnMCc7XG4gICAgICAgICAgICAgICAgc2NvcGUuc2FmZUFwcGx5KCk7XG5cbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoJ3VwZGF0ZScsIHtcbiAgICAgICAgICAgICAgICAgICAgaWQgOiBzY29wZS5jb25maWcuaWQsXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkOiBzY29wZS5maWVsZCxcbiAgICAgICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogc2NvcGUudmFsdWUsXG4gICAgICAgICAgICAgICAgICAgIHByb3BlcnRpZXM6IHNjb3BlLmNvbmZpZy5wcm9wZXJ0aWVzXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBhbmd1bGFyLmVsZW1lbnQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgaWYoc2NvcGUudmFsdWUpe1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5nbyh0cnVlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH07XG59KTsiLCIvKipcbiAqIEBhdWdtZW50cyBQcm90b3BsYW4uQmxvY2tcbiAqIEBjbGFzcyBQcm90b3BsYW4uVG9vbHRpcFxuICogQHBhcmFtIHBhcmFtc1xuICogQGNvbnN0cnVjdG9yXG4gKi9cblxuUHJvdG9wbGFuLlRvb2x0aXAgPSBmdW5jdGlvbiAocGFyYW1zKVxue1xuICAgIFByb3RvcGxhbi5CbG9jay5hcHBseSh0aGlzLCBbcGFyYW1zLmlkLCBwYXJhbXMuc2VsZWN0b3JdKTtcblxuICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgIHNlbGYuc2VsZWN0b3IgPSBwYXJhbXMuc2VsZWN0b3IgIT0gdW5kZWZpbmVkID8gcGFyYW1zLnNlbGVjdG9yIDogJyc7XG4gICAgc2VsZi50YXJnZXQgPSBwYXJhbXMudGFyZ2V0ICE9IHVuZGVmaW5lZCA/IHBhcmFtcy50YXJnZXQgOiAnJztcblxuICAgIHNlbGYuaW5pdCA9IGZ1bmN0aW9uICgpXG4gICAge1xuICAgICAgICAkKHNlbGYudGFyZ2V0KS5ob3ZlcihmdW5jdGlvbiAoKVxuICAgICAgICB7XG4gICAgICAgICAgICBzZWxmLnNob3coKTtcbiAgICAgICAgfSwgZnVuY3Rpb24gKClcbiAgICAgICAgeyBcbiAgICAgICAgICAgIHNlbGYuaGlkZSgpO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgc2VsZi5zaG93ID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKClcbiAgICAgICAge1xuICAgICAgICAgICAgJChzZWxmLnNlbGVjdG9yKS5mYWRlSW4oMzAwKTtcbiAgICAgICAgfSwgNTAwKTtcbiAgICB9O1xuXG4gICAgc2VsZi5oaWRlID0gZnVuY3Rpb24gKClcbiAgICB7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKClcbiAgICAgICAge1xuICAgICAgICAgICAgJChzZWxmLnNlbGVjdG9yKS5mYWRlT3V0KDMwMCk7XG4gICAgICAgIH0sIDUwMCk7XG4gICAgfTtcblxuICAgIHNlbGYuaW5pdCgpO1xufTtcblxucHAubW9kdWxlKCd0b29sdGlwJywgW10pLmRpcmVjdGl2ZSgndG9vbHRpcCcsIGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIHRlbXBsYXRlVXJsOiBwcC52aWV3UGF0aCgndG9vbHRpcCcpLFxuICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICBzY29wZTpcbiAgICAgICAge1xuICAgICAgICAgICAgXCJ0b29sdGlwXCI6ICc9J1xuICAgICAgICB9LFxuICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgdG9vbHRpcEljb24gPSAn7o+NJztcblxuICAgICAgICAgICAgaWYgKHNjb3BlLnRvb2x0aXAuaWNvbiAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICB0b29sdGlwSWNvbiA9IHNjb3BlLnRvb2x0aXAuaWNvbjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJCgkKGVsZW1lbnQpLmZpbmQoJy50b29sdGlwc3RlcicpKS50b29sdGlwc3Rlcih7XG4gICAgICAgICAgICAgICAgY29udGVudDogJCgnPGRpdiBjbGFzcz1cInRvb2x0aXAtd3JhcFwiPjxidXR0b24gY2xhc3M9XCJjbG9zZVwiPu6PjTwvYnV0dG9uPicgKyBzY29wZS50b29sdGlwLmNvbnRlbnQgKyAnPC9kaXY+JyksXG4gICAgICAgICAgICAgICAgdHJpZ2dlcjogJ2NsaWNrJyxcbiAgICAgICAgICAgICAgICBkZWxheTogMCxcbiAgICAgICAgICAgICAgICBzcGVlZDogMCxcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ2JvdHRvbSdcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcbn0pOyIsIi8qXG5cbiBUb29sdGlwc3RlciAzLjMuMCB8IDIwMTQtMTEtMDhcbiBBIHJvY2tpbicgY3VzdG9tIHRvb2x0aXAgalF1ZXJ5IHBsdWdpblxuXG4gRGV2ZWxvcGVkIGJ5IENhbGViIEphY29iIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG5cbiBUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SIElNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0YgTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTiBOTyBFVkVOVCBTSEFMTCBUSEUgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUiBMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEUgVVNFIE9SIE9USEVSIERFQUxJTkdTIElOIFRIRSBTT0ZUV0FSRS5cblxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQpIHtcblxuICAgIHZhciBwbHVnaW5OYW1lID0gXCJ0b29sdGlwc3RlclwiLFxuICAgICAgICBkZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIGFuaW1hdGlvbjogJ2ZhZGUnLFxuICAgICAgICAgICAgYXJyb3c6IHRydWUsXG4gICAgICAgICAgICBhcnJvd0NvbG9yOiAnJyxcbiAgICAgICAgICAgIGF1dG9DbG9zZTogdHJ1ZSxcbiAgICAgICAgICAgIGNvbnRlbnQ6IG51bGwsXG4gICAgICAgICAgICBjb250ZW50QXNIVE1MOiBmYWxzZSxcbiAgICAgICAgICAgIGNvbnRlbnRDbG9uaW5nOiB0cnVlLFxuICAgICAgICAgICAgZGVidWc6IHRydWUsXG4gICAgICAgICAgICBkZWxheTogMjAwLFxuICAgICAgICAgICAgbWluV2lkdGg6IDAsXG4gICAgICAgICAgICBtYXhXaWR0aDogbnVsbCxcbiAgICAgICAgICAgIGZ1bmN0aW9uSW5pdDogZnVuY3Rpb24ob3JpZ2luLCBjb250ZW50KSB7fSxcbiAgICAgICAgICAgIGZ1bmN0aW9uQmVmb3JlOiBmdW5jdGlvbihvcmlnaW4sIGNvbnRpbnVlVG9vbHRpcCkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlVG9vbHRpcCgpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZ1bmN0aW9uUmVhZHk6IGZ1bmN0aW9uKG9yaWdpbiwgdG9vbHRpcCkge30sXG4gICAgICAgICAgICBmdW5jdGlvbkFmdGVyOiBmdW5jdGlvbihvcmlnaW4pIHt9LFxuICAgICAgICAgICAgaGlkZU9uQ2xpY2s6IGZhbHNlLFxuICAgICAgICAgICAgaWNvbjogJyg/KScsXG4gICAgICAgICAgICBpY29uQ2xvbmluZzogdHJ1ZSxcbiAgICAgICAgICAgIGljb25EZXNrdG9wOiBmYWxzZSxcbiAgICAgICAgICAgIGljb25Ub3VjaDogZmFsc2UsXG4gICAgICAgICAgICBpY29uVGhlbWU6ICd0b29sdGlwc3Rlci1pY29uJyxcbiAgICAgICAgICAgIGludGVyYWN0aXZlOiBmYWxzZSxcbiAgICAgICAgICAgIGludGVyYWN0aXZlVG9sZXJhbmNlOiAzNTAsXG4gICAgICAgICAgICBtdWx0aXBsZTogZmFsc2UsXG4gICAgICAgICAgICBvZmZzZXRYOiAwLFxuICAgICAgICAgICAgb2Zmc2V0WTogMCxcbiAgICAgICAgICAgIG9ubHlPbmU6IGZhbHNlLFxuICAgICAgICAgICAgcG9zaXRpb246ICd0b3AnLFxuICAgICAgICAgICAgcG9zaXRpb25UcmFja2VyOiBmYWxzZSxcbiAgICAgICAgICAgIHBvc2l0aW9uVHJhY2tlckNhbGxiYWNrOiBmdW5jdGlvbihvcmlnaW4pe1xuICAgICAgICAgICAgICAgIC8vIHRoZSBkZWZhdWx0IHRyYWNrZXIgY2FsbGJhY2sgd2lsbCBjbG9zZSB0aGUgdG9vbHRpcCB3aGVuIHRoZSB0cmlnZ2VyIGlzXG4gICAgICAgICAgICAgICAgLy8gJ2hvdmVyJyAoc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9pYW1jZWVnZS90b29sdGlwc3Rlci9wdWxsLzI1MylcbiAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbigndHJpZ2dlcicpID09ICdob3ZlcicgJiYgdGhpcy5vcHRpb24oJ2F1dG9DbG9zZScpKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICByZXN0b3JhdGlvbjogJ2N1cnJlbnQnLFxuICAgICAgICAgICAgc3BlZWQ6IDM1MCxcbiAgICAgICAgICAgIHRpbWVyOiAwLFxuICAgICAgICAgICAgdGhlbWU6ICd0b29sdGlwc3Rlci1kZWZhdWx0JyxcbiAgICAgICAgICAgIHRvdWNoRGV2aWNlczogdHJ1ZSxcbiAgICAgICAgICAgIHRyaWdnZXI6ICdob3ZlcicsXG4gICAgICAgICAgICB1cGRhdGVBbmltYXRpb246IHRydWVcbiAgICAgICAgfTtcblxuICAgIGZ1bmN0aW9uIFBsdWdpbihlbGVtZW50LCBvcHRpb25zKSB7XG5cbiAgICAgICAgLy8gbGlzdCBvZiBpbnN0YW5jZSB2YXJpYWJsZXNcblxuICAgICAgICB0aGlzLmJvZHlPdmVyZmxvd1g7XG4gICAgICAgIC8vIHN0YWNrIG9mIGN1c3RvbSBjYWxsYmFja3MgcHJvdmlkZWQgYXMgcGFyYW1ldGVycyB0byBBUEkgbWV0aG9kc1xuICAgICAgICB0aGlzLmNhbGxiYWNrcyA9IHtcbiAgICAgICAgICAgIGhpZGU6IFtdLFxuICAgICAgICAgICAgc2hvdzogW11cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5jaGVja0ludGVydmFsID0gbnVsbDtcbiAgICAgICAgLy8gdGhpcyB3aWxsIGJlIHRoZSB1c2VyIGNvbnRlbnQgc2hvd24gaW4gdGhlIHRvb2x0aXAuIEEgY2FwaXRhbCBcIkNcIiBpcyB1c2VkIGJlY2F1c2UgdGhlcmUgaXMgYWxzbyBhIG1ldGhvZCBjYWxsZWQgY29udGVudCgpXG4gICAgICAgIHRoaXMuQ29udGVudDtcbiAgICAgICAgLy8gdGhpcyBpcyB0aGUgb3JpZ2luYWwgZWxlbWVudCB3aGljaCBpcyBiZWluZyBhcHBsaWVkIHRoZSB0b29sdGlwc3RlciBwbHVnaW5cbiAgICAgICAgdGhpcy4kZWwgPSAkKGVsZW1lbnQpO1xuICAgICAgICAvLyB0aGlzIHdpbGwgYmUgdGhlIGVsZW1lbnQgd2hpY2ggdHJpZ2dlcnMgdGhlIGFwcGVhcmFuY2Ugb2YgdGhlIHRvb2x0aXAgb24gaG92ZXIvY2xpY2svY3VzdG9tIGV2ZW50cy5cbiAgICAgICAgLy8gaXQgd2lsbCBiZSB0aGUgc2FtZSBhcyB0aGlzLiRlbCBpZiBpY29ucyBhcmUgbm90IHVzZWQgKHNlZSBpbiB0aGUgb3B0aW9ucyksIG90aGVyd2lzZSBpdCB3aWxsIGNvcnJlc3BvbmQgdG8gdGhlIGNyZWF0ZWQgaWNvblxuICAgICAgICB0aGlzLiRlbFByb3h5O1xuICAgICAgICB0aGlzLmVsUHJveHlQb3NpdGlvbjtcbiAgICAgICAgdGhpcy5lbmFibGVkID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQoe30sIGRlZmF1bHRzLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5tb3VzZUlzT3ZlclByb3h5ID0gZmFsc2U7XG4gICAgICAgIC8vIGEgdW5pcXVlIG5hbWVzcGFjZSBwZXIgaW5zdGFuY2UsIGZvciBlYXN5IHNlbGVjdGl2ZSB1bmJpbmRpbmdcbiAgICAgICAgdGhpcy5uYW1lc3BhY2UgPSAndG9vbHRpcHN0ZXItJysgTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKjEwMDAwMCk7XG4gICAgICAgIC8vIFN0YXR1cyAoY2FwaXRhbCBTKSBjYW4gYmUgZWl0aGVyIDogYXBwZWFyaW5nLCBzaG93biwgZGlzYXBwZWFyaW5nLCBoaWRkZW5cbiAgICAgICAgdGhpcy5TdGF0dXMgPSAnaGlkZGVuJztcbiAgICAgICAgdGhpcy50aW1lckhpZGUgPSBudWxsO1xuICAgICAgICB0aGlzLnRpbWVyU2hvdyA9IG51bGw7XG4gICAgICAgIC8vIHRoaXMgd2lsbCBiZSB0aGUgdG9vbHRpcCBlbGVtZW50IChqUXVlcnkgd3JhcHBlZCBIVE1MIGVsZW1lbnQpXG4gICAgICAgIHRoaXMuJHRvb2x0aXA7XG5cbiAgICAgICAgLy8gZm9yIGJhY2t3YXJkIGNvbXBhdGliaWxpdHlcbiAgICAgICAgdGhpcy5vcHRpb25zLmljb25UaGVtZSA9IHRoaXMub3B0aW9ucy5pY29uVGhlbWUucmVwbGFjZSgnLicsICcnKTtcbiAgICAgICAgdGhpcy5vcHRpb25zLnRoZW1lID0gdGhpcy5vcHRpb25zLnRoZW1lLnJlcGxhY2UoJy4nLCAnJyk7XG5cbiAgICAgICAgLy8gbGF1bmNoXG5cbiAgICAgICAgdGhpcy5faW5pdCgpO1xuICAgIH1cblxuICAgIFBsdWdpbi5wcm90b3R5cGUgPSB7XG5cbiAgICAgICAgX2luaXQ6IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgICAgIC8vIGRpc2FibGUgdGhlIHBsdWdpbiBvbiBvbGQgYnJvd3NlcnMgKGluY2x1ZGluZyBJRTcgYW5kIGxvd2VyKVxuICAgICAgICAgICAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IpIHtcblxuICAgICAgICAgICAgICAgIC8vIG5vdGUgOiB0aGUgY29udGVudCBpcyBudWxsIChlbXB0eSkgYnkgZGVmYXVsdCBhbmQgY2FuIHN0YXkgdGhhdCB3YXkgaWYgdGhlIHBsdWdpbiByZW1haW5zIGluaXRpYWxpemVkIGJ1dCBub3QgZmVkIGFueSBjb250ZW50LiBUaGUgdG9vbHRpcCB3aWxsIGp1c3Qgbm90IGFwcGVhci5cblxuICAgICAgICAgICAgICAgIC8vIGxldCdzIHNhdmUgdGhlIGluaXRpYWwgdmFsdWUgb2YgdGhlIHRpdGxlIGF0dHJpYnV0ZSBmb3IgbGF0ZXIgcmVzdG9yYXRpb24gaWYgbmVlZCBiZS5cbiAgICAgICAgICAgICAgICB2YXIgaW5pdGlhbFRpdGxlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAvLyBpdCB3aWxsIGFscmVhZHkgaGF2ZSBiZWVuIHNhdmVkIGluIGNhc2Ugb2YgbXVsdGlwbGUgdG9vbHRpcHNcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi4kZWwuZGF0YSgndG9vbHRpcHN0ZXItaW5pdGlhbFRpdGxlJykgPT09IHVuZGVmaW5lZCkge1xuXG4gICAgICAgICAgICAgICAgICAgIGluaXRpYWxUaXRsZSA9IHNlbGYuJGVsLmF0dHIoJ3RpdGxlJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gd2UgZG8gbm90IHdhbnQgaW5pdGlhbFRpdGxlIHRvIGhhdmUgdGhlIHZhbHVlIFwidW5kZWZpbmVkXCIgYmVjYXVzZSBvZiBob3cgalF1ZXJ5J3MgLmRhdGEoKSBtZXRob2Qgd29ya3NcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluaXRpYWxUaXRsZSA9PT0gdW5kZWZpbmVkKSBpbml0aWFsVGl0bGUgPSBudWxsO1xuXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuJGVsLmRhdGEoJ3Rvb2x0aXBzdGVyLWluaXRpYWxUaXRsZScsIGluaXRpYWxUaXRsZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gaWYgY29udGVudCBpcyBwcm92aWRlZCBpbiB0aGUgb3B0aW9ucywgaXRzIGhhcyBwcmVjZWRlbmNlIG92ZXIgdGhlIHRpdGxlIGF0dHJpYnV0ZS5cbiAgICAgICAgICAgICAgICAvLyBOb3RlIDogYW4gZW1wdHkgc3RyaW5nIGlzIGNvbnNpZGVyZWQgY29udGVudCwgb25seSAnbnVsbCcgcmVwcmVzZW50cyB0aGUgYWJzZW5jZSBvZiBjb250ZW50LlxuICAgICAgICAgICAgICAgIC8vIEFsc28sIGFuIGV4aXN0aW5nIHRpdGxlPVwiXCIgYXR0cmlidXRlIHdpbGwgcmVzdWx0IGluIGFuIGVtcHR5IHN0cmluZyBjb250ZW50XG4gICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5jb250ZW50ICE9PSBudWxsKXtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5fY29udGVudF9zZXQoc2VsZi5vcHRpb25zLmNvbnRlbnQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5fY29udGVudF9zZXQoaW5pdGlhbFRpdGxlKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgYyA9IHNlbGYub3B0aW9ucy5mdW5jdGlvbkluaXQuY2FsbChzZWxmLiRlbCwgc2VsZi4kZWwsIHNlbGYuQ29udGVudCk7XG4gICAgICAgICAgICAgICAgaWYodHlwZW9mIGMgIT09ICd1bmRlZmluZWQnKSBzZWxmLl9jb250ZW50X3NldChjKTtcblxuICAgICAgICAgICAgICAgIHNlbGYuJGVsXG4gICAgICAgICAgICAgICAgLy8gc3RyaXAgdGhlIHRpdGxlIG9mZiBvZiB0aGUgZWxlbWVudCB0byBwcmV2ZW50IHRoZSBkZWZhdWx0IHRvb2x0aXBzIGZyb20gcG9wcGluZyB1cFxuICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cigndGl0bGUnKVxuICAgICAgICAgICAgICAgICAgICAvLyB0byBiZSBhYmxlIHRvIGZpbmQgYWxsIGluc3RhbmNlcyBvbiB0aGUgcGFnZSBsYXRlciAodXBvbiB3aW5kb3cgZXZlbnRzIGluIHBhcnRpY3VsYXIpXG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygndG9vbHRpcHN0ZXJlZCcpO1xuXG4gICAgICAgICAgICAgICAgLy8gZGV0ZWN0IGlmIHdlJ3JlIGNoYW5naW5nIHRoZSB0b29sdGlwIG9yaWdpbiB0byBhbiBpY29uXG4gICAgICAgICAgICAgICAgLy8gbm90ZSBhYm91dCB0aGlzIGNvbmRpdGlvbiA6IGlmIHRoZSBkZXZpY2UgaGFzIHRvdWNoIGNhcGFiaWxpdHkgYW5kIHNlbGYub3B0aW9ucy5pY29uVG91Y2ggaXMgZmFsc2UsIHlvdSdsbCBoYXZlIG5vIGljb25zIGV2ZW50IHRob3VnaCB5b3UgbWF5IGNvbnNpZGVyIHlvdXIgZGV2aWNlIGFzIGEgZGVza3RvcCBpZiBpdCBhbHNvIGhhcyBhIG1vdXNlLiBOb3Qgc3VyZSB3aHkgc29tZW9uZSB3b3VsZCBoYXZlIHRoaXMgdXNlIGNhc2UgdGhvdWdoLlxuICAgICAgICAgICAgICAgIGlmICgoIWRldmljZUhhc1RvdWNoQ2FwYWJpbGl0eSAmJiBzZWxmLm9wdGlvbnMuaWNvbkRlc2t0b3ApIHx8IChkZXZpY2VIYXNUb3VjaENhcGFiaWxpdHkgJiYgc2VsZi5vcHRpb25zLmljb25Ub3VjaCkpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBUT0RPIDogdGhlIHRvb2x0aXAgc2hvdWxkIGJlIGF1dG9tYXRpY2FsbHkgYmUgZ2l2ZW4gYW4gYWJzb2x1dGUgcG9zaXRpb24gdG8gYmUgbmVhciB0aGUgb3JpZ2luLiBPdGhlcndpc2UsIHdoZW4gdGhlIG9yaWdpbiBpcyBmbG9hdGluZyBvciB3aGF0LCBpdCdzIGdvaW5nIHRvIGJlIG5vd2hlcmUgbmVhciBpdCBhbmQgZGlzdHVyYiB0aGUgcG9zaXRpb24gZmxvdyBvZiB0aGUgcGFnZSBlbGVtZW50cy4gSXQgd2lsbCBpbXBseSB0aGF0IHRoZSBpY29uIGFsc28gZGV0ZWN0cyB3aGVuIGl0cyBvcmlnaW4gbW92ZXMsIHRvIGZvbGxvdyBpdCA6IG5vdCB0cml2aWFsLlxuICAgICAgICAgICAgICAgICAgICAvLyBVbnRpbCBpdCdzIGRvbmUsIHRoZSBpY29uIGZlYXR1cmUgZG9lcyBub3QgcmVhbGx5IG1ha2Ugc2Vuc2Ugc2luY2UgdGhlIHVzZXIgc3RpbGwgaGFzIG1vc3Qgb2YgdGhlIHdvcmsgdG8gZG8gYnkgaGltc2VsZlxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBpY29uIHByb3ZpZGVkIGlzIGluIHRoZSBmb3JtIG9mIGEgc3RyaW5nXG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBzZWxmLm9wdGlvbnMuaWNvbiA9PT0gJ3N0cmluZycpe1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd3JhcCBpdCBpbiBhIHNwYW4gd2l0aCB0aGUgaWNvbiBjbGFzc1xuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi4kZWxQcm94eSA9ICQoJzxzcGFuIGNsYXNzPVwiJysgc2VsZi5vcHRpb25zLmljb25UaGVtZSArJ1wiPjwvc3Bhbj4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJGVsUHJveHkudGV4dChzZWxmLm9wdGlvbnMuaWNvbik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgaXQgaXMgYW4gb2JqZWN0IChzZW5zaWJsZSBjaG9pY2UpXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gKGRlZXApIGNsb25lIHRoZSBvYmplY3QgaWYgaWNvbkNsb25pbmcgPT0gdHJ1ZSwgdG8gbWFrZSBzdXJlIGV2ZXJ5IGluc3RhbmNlIGhhcyBpdHMgb3duIHByb3h5LiBXZSB1c2UgdGhlIGljb24gd2l0aG91dCB3cmFwcGluZywgbm8gbmVlZCB0by4gV2UgZG8gbm90IGdpdmUgaXQgYSBjbGFzcyBlaXRoZXIsIGFzIHRoZSB1c2VyIHdpbGwgdW5kb3VidGVkbHkgc3R5bGUgdGhlIG9iamVjdCBvbiBoaXMgb3duIGFuZCBzaW5jZSBvdXIgY3NzIHByb3BlcnRpZXMgbWF5IGNvbmZsaWN0IHdpdGggaGlzIG93blxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5pY29uQ2xvbmluZykgc2VsZi4kZWxQcm94eSA9IHNlbGYub3B0aW9ucy5pY29uLmNsb25lKHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBzZWxmLiRlbFByb3h5ID0gc2VsZi5vcHRpb25zLmljb247XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBzZWxmLiRlbFByb3h5Lmluc2VydEFmdGVyKHNlbGYuJGVsKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuJGVsUHJveHkgPSBzZWxmLiRlbDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvLyBmb3IgJ2NsaWNrJyBhbmQgJ2hvdmVyJyB0cmlnZ2VycyA6IGJpbmQgb24gZXZlbnRzIHRvIG9wZW4gdGhlIHRvb2x0aXAuIENsb3NpbmcgaXMgbm93IGhhbmRsZWQgaW4gX3Nob3dOb3coKSBiZWNhdXNlIG9mIGl0cyBiaW5kaW5ncy5cbiAgICAgICAgICAgICAgICAvLyBOb3RlcyBhYm91dCB0b3VjaCBldmVudHMgOlxuICAgICAgICAgICAgICAgIC8vIC0gbW91c2VlbnRlciwgbW91c2VsZWF2ZSBhbmQgY2xpY2tzIGhhcHBlbiBldmVuIG9uIHB1cmUgdG91Y2ggZGV2aWNlcyBiZWNhdXNlIHRoZXkgYXJlIGVtdWxhdGVkLiBkZXZpY2VJc1B1cmVUb3VjaCgpIGlzIGEgc2ltcGxlIGF0dGVtcHQgdG8gZGV0ZWN0IHRoZW0uXG4gICAgICAgICAgICAgICAgLy8gLSBvbiBoeWJyaWQgZGV2aWNlcywgd2UgZG8gbm90IHByZXZlbnQgdG91Y2ggZ2VzdHVyZSBmcm9tIG9wZW5pbmcgdG9vbHRpcHMuIEl0IHdvdWxkIGJlIHRvbyBjb21wbGV4IHRvIGRpZmZlcmVudGlhdGUgcmVhbCBtb3VzZSBldmVudHMgZnJvbSBlbXVsYXRlZCBvbmVzLlxuICAgICAgICAgICAgICAgIC8vIC0gd2UgY2hlY2sgZGV2aWNlSXNQdXJlVG91Y2goKSBhdCBlYWNoIGV2ZW50IHJhdGhlciB0aGFuIHByaW9yIHRvIGJpbmRpbmcgYmVjYXVzZSB0aGUgc2l0dWF0aW9uIG1heSBjaGFuZ2UgZHVyaW5nIGJyb3dzaW5nXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy50cmlnZ2VyID09ICdob3ZlcicpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyB0aGVzZSBiaW5kaW5nIGFyZSBmb3IgbW91c2UgaW50ZXJhY3Rpb24gb25seVxuICAgICAgICAgICAgICAgICAgICBzZWxmLiRlbFByb3h5XG4gICAgICAgICAgICAgICAgICAgICAgICAub24oJ21vdXNlZW50ZXIuJysgc2VsZi5uYW1lc3BhY2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZGV2aWNlSXNQdXJlVG91Y2goKSB8fCBzZWxmLm9wdGlvbnMudG91Y2hEZXZpY2VzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYubW91c2VJc092ZXJQcm94eSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuX3Nob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLm9uKCdtb3VzZWxlYXZlLicrIHNlbGYubmFtZXNwYWNlLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWRldmljZUlzUHVyZVRvdWNoKCkgfHwgc2VsZi5vcHRpb25zLnRvdWNoRGV2aWNlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLm1vdXNlSXNPdmVyUHJveHkgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBmb3IgdG91Y2ggaW50ZXJhY3Rpb24gb25seVxuICAgICAgICAgICAgICAgICAgICBpZiAoZGV2aWNlSGFzVG91Y2hDYXBhYmlsaXR5ICYmIHNlbGYub3B0aW9ucy50b3VjaERldmljZXMpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZm9yIHRvdWNoIGRldmljZXMsIHdlIGltbWVkaWF0ZWx5IGRpc3BsYXkgdGhlIHRvb2x0aXAgYmVjYXVzZSB3ZSBjYW5ub3QgcmVseSBvbiBtb3VzZWxlYXZlIHRvIGhhbmRsZSB0aGUgZGVsYXlcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJGVsUHJveHkub24oJ3RvdWNoc3RhcnQuJysgc2VsZi5uYW1lc3BhY2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuX3Nob3dOb3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHNlbGYub3B0aW9ucy50cmlnZ2VyID09ICdjbGljaycpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBub3RlIDogZm9yIHRvdWNoIGRldmljZXMsIHdlIGRvIG5vdCBiaW5kIG9uIHRvdWNoc3RhcnQsIHdlIG9ubHkgcmVseSBvbiB0aGUgZW11bGF0ZWQgY2xpY2tzICh0cmlnZ2VyZWQgYnkgdGFwcylcbiAgICAgICAgICAgICAgICAgICAgc2VsZi4kZWxQcm94eS5vbignY2xpY2suJysgc2VsZi5uYW1lc3BhY2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFkZXZpY2VJc1B1cmVUb3VjaCgpIHx8IHNlbGYub3B0aW9ucy50b3VjaERldmljZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLl9zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvLyB0aGlzIGZ1bmN0aW9uIHdpbGwgc2NoZWR1bGUgdGhlIG9wZW5pbmcgb2YgdGhlIHRvb2x0aXAgYWZ0ZXIgdGhlIGRlbGF5LCBpZiB0aGVyZSBpcyBvbmVcbiAgICAgICAgX3Nob3c6IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgICAgIGlmIChzZWxmLlN0YXR1cyAhPSAnc2hvd24nICYmIHNlbGYuU3RhdHVzICE9ICdhcHBlYXJpbmcnKSB7XG5cbiAgICAgICAgICAgICAgICBpZiAoc2VsZi5vcHRpb25zLmRlbGF5KSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYudGltZXJTaG93ID0gc2V0VGltZW91dChmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBmb3IgaG92ZXIgdHJpZ2dlciwgd2UgY2hlY2sgaWYgdGhlIG1vdXNlIGlzIHN0aWxsIG92ZXIgdGhlIHByb3h5LCBvdGhlcndpc2Ugd2UgZG8gbm90IHNob3cgYW55dGhpbmdcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMudHJpZ2dlciA9PSAnY2xpY2snIHx8IChzZWxmLm9wdGlvbnMudHJpZ2dlciA9PSAnaG92ZXInICYmIHNlbGYubW91c2VJc092ZXJQcm94eSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLl9zaG93Tm93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sIHNlbGYub3B0aW9ucy5kZWxheSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Ugc2VsZi5fc2hvd05vdygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8vIHRoaXMgZnVuY3Rpb24gd2lsbCBvcGVuIHRoZSB0b29sdGlwIHJpZ2h0IGF3YXlcbiAgICAgICAgX3Nob3dOb3c6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cbiAgICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgICAgICAgLy8gY2FsbCBvdXIgY29uc3RydWN0b3IgY3VzdG9tIGZ1bmN0aW9uIGJlZm9yZSBjb250aW51aW5nXG4gICAgICAgICAgICBzZWxmLm9wdGlvbnMuZnVuY3Rpb25CZWZvcmUuY2FsbChzZWxmLiRlbCwgc2VsZi4kZWwsIGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgLy8gY29udGludWUgb25seSBpZiB0aGUgdG9vbHRpcCBpcyBlbmFibGVkIGFuZCBoYXMgYW55IGNvbnRlbnRcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi5lbmFibGVkICYmIHNlbGYuQ29udGVudCAhPT0gbnVsbCkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIHNhdmUgdGhlIG1ldGhvZCBjYWxsYmFjayBhbmQgY2FuY2VsIGhpZGUgbWV0aG9kIGNhbGxiYWNrc1xuICAgICAgICAgICAgICAgICAgICBpZiAoY2FsbGJhY2spIHNlbGYuY2FsbGJhY2tzLnNob3cucHVzaChjYWxsYmFjayk7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuY2FsbGJhY2tzLmhpZGUgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICAvL2dldCByaWQgb2YgYW55IGFwcGVhcmFuY2UgdGltZXJcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHNlbGYudGltZXJTaG93KTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi50aW1lclNob3cgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoc2VsZi50aW1lckhpZGUpO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnRpbWVySGlkZSA9IG51bGw7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgd2Ugb25seSB3YW50IG9uZSB0b29sdGlwIG9wZW4gYXQgYSB0aW1lLCBjbG9zZSBhbGwgYXV0by1jbG9zaW5nIHRvb2x0aXBzIGN1cnJlbnRseSBvcGVuIGFuZCBub3QgYWxyZWFkeSBkaXNhcHBlYXJpbmdcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5vbmx5T25lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcudG9vbHRpcHN0ZXJlZCcpLm5vdChzZWxmLiRlbCkuZWFjaChmdW5jdGlvbihpLGVsKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGVsID0gJChlbCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5zcyA9ICRlbC5kYXRhKCd0b29sdGlwc3Rlci1ucycpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaXRlcmF0ZSBvbiBhbGwgdG9vbHRpcHMgb2YgdGhlIGVsZW1lbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkLmVhY2gobnNzLCBmdW5jdGlvbihpLCBucyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbnN0YW5jZSA9ICRlbC5kYXRhKG5zKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2UgaGF2ZSB0byB1c2UgdGhlIHB1YmxpYyBtZXRob2RzIGhlcmVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHMgPSBpbnN0YW5jZS5zdGF0dXMoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjID0gaW5zdGFuY2Uub3B0aW9uKCdhdXRvQ2xvc2UnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocyAhPT0gJ2hpZGRlbicgJiYgcyAhPT0gJ2Rpc2FwcGVhcmluZycgJiYgYWMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluc3RhbmNlLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB2YXIgZmluaXNoID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLlN0YXR1cyA9ICdzaG93bic7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRyaWdnZXIgYW55IHNob3cgbWV0aG9kIGN1c3RvbSBjYWxsYmFja3MgYW5kIHJlc2V0IHRoZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICQuZWFjaChzZWxmLmNhbGxiYWNrcy5zaG93LCBmdW5jdGlvbihpLGMpIHsgYy5jYWxsKHNlbGYuJGVsKTsgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLmNhbGxiYWNrcy5zaG93ID0gW107XG4gICAgICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhpcyBvcmlnaW4gYWxyZWFkeSBoYXMgaXRzIHRvb2x0aXAgb3BlblxuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi5TdGF0dXMgIT09ICdoaWRkZW4nKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZSB0aW1lciAoaWYgYW55KSB3aWxsIHN0YXJ0IChvciByZXN0YXJ0KSByaWdodCBub3dcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBleHRyYVRpbWUgPSAwO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiBpdCB3YXMgZGlzYXBwZWFyaW5nLCBjYW5jZWwgdGhhdFxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYuU3RhdHVzID09PSAnZGlzYXBwZWFyaW5nJykge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5TdGF0dXMgPSAnYXBwZWFyaW5nJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdXBwb3J0c1RyYW5zaXRpb25zKCkpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY2xlYXJRdWV1ZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ3Rvb2x0aXBzdGVyLWR5aW5nJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygndG9vbHRpcHN0ZXItJysgc2VsZi5vcHRpb25zLmFuaW1hdGlvbiArJy1zaG93Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5zcGVlZCA+IDApIHNlbGYuJHRvb2x0aXAuZGVsYXkoc2VsZi5vcHRpb25zLnNwZWVkKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLnF1ZXVlKGZpbmlzaCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpbiBjYXNlIHRoZSB0b29sdGlwIHdhcyBjdXJyZW50bHkgZmFkaW5nIG91dCwgYnJpbmcgaXQgYmFjayB0byBsaWZlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdG9wKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5mYWRlSW4oZmluaXNoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGUgdG9vbHRpcCBpcyBhbHJlYWR5IG9wZW4sIHdlIHN0aWxsIG5lZWQgdG8gdHJpZ2dlciB0aGUgbWV0aG9kIGN1c3RvbSBjYWxsYmFja1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZihzZWxmLlN0YXR1cyA9PT0gJ3Nob3duJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbmlzaCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZSB0b29sdGlwIGlzbid0IGFscmVhZHkgb3Blbiwgb3BlbiB0aGF0IHN1Y2tlciB1cCFcbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuU3RhdHVzID0gJ2FwcGVhcmluZyc7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZSB0aW1lciAoaWYgYW55KSB3aWxsIHN0YXJ0IHdoZW4gdGhlIHRvb2x0aXAgaGFzIGZ1bGx5IGFwcGVhcmVkIGFmdGVyIGl0cyB0cmFuc2l0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZXh0cmFUaW1lID0gc2VsZi5vcHRpb25zLnNwZWVkO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBkaXNhYmxlIGhvcml6b250YWwgc2Nyb2xsYmFyIHRvIGtlZXAgb3ZlcmZsb3dpbmcgdG9vbHRpcHMgZnJvbSBqYWNraW5nIHdpdGggaXQgYW5kIHRoZW4gcmVzdG9yZSBpdCB0byBpdHMgcHJldmlvdXMgdmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuYm9keU92ZXJmbG93WCA9ICQoJ2JvZHknKS5jc3MoJ292ZXJmbG93LXgnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5jc3MoJ292ZXJmbG93LXgnLCAnaGlkZGVuJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGdldCBzb21lIG90aGVyIHNldHRpbmdzIHJlbGF0ZWQgdG8gYnVpbGRpbmcgdGhlIHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhbmltYXRpb24gPSAndG9vbHRpcHN0ZXItJyArIHNlbGYub3B0aW9ucy5hbmltYXRpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uU3BlZWQgPSAnLXdlYmtpdC10cmFuc2l0aW9uLWR1cmF0aW9uOiAnKyBzZWxmLm9wdGlvbnMuc3BlZWQgKydtczsgLXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246ICcrIHNlbGYub3B0aW9ucy5zcGVlZCArJ21zOyAtbW96LXRyYW5zaXRpb24tZHVyYXRpb246ICcrIHNlbGYub3B0aW9ucy5zcGVlZCArJ21zOyAtbW96LWFuaW1hdGlvbi1kdXJhdGlvbjogJysgc2VsZi5vcHRpb25zLnNwZWVkICsnbXM7IC1vLXRyYW5zaXRpb24tZHVyYXRpb246ICcrIHNlbGYub3B0aW9ucy5zcGVlZCArJ21zOyAtby1hbmltYXRpb24tZHVyYXRpb246ICcrIHNlbGYub3B0aW9ucy5zcGVlZCArJ21zOyAtbXMtdHJhbnNpdGlvbi1kdXJhdGlvbjogJysgc2VsZi5vcHRpb25zLnNwZWVkICsnbXM7IC1tcy1hbmltYXRpb24tZHVyYXRpb246ICcrIHNlbGYub3B0aW9ucy5zcGVlZCArJ21zOyB0cmFuc2l0aW9uLWR1cmF0aW9uOiAnKyBzZWxmLm9wdGlvbnMuc3BlZWQgKydtczsgYW5pbWF0aW9uLWR1cmF0aW9uOiAnKyBzZWxmLm9wdGlvbnMuc3BlZWQgKydtczsnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1pbldpZHRoID0gc2VsZi5vcHRpb25zLm1pbldpZHRoID8gJ21pbi13aWR0aDonKyBNYXRoLnJvdW5kKHNlbGYub3B0aW9ucy5taW5XaWR0aCkgKydweDsnIDogJycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4V2lkdGggPSBzZWxmLm9wdGlvbnMubWF4V2lkdGggPyAnbWF4LXdpZHRoOicrIE1hdGgucm91bmQoc2VsZi5vcHRpb25zLm1heFdpZHRoKSArJ3B4OycgOiAnJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb2ludGVyRXZlbnRzID0gc2VsZi5vcHRpb25zLmludGVyYWN0aXZlID8gJ3BvaW50ZXItZXZlbnRzOiBhdXRvOycgOiAnJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gYnVpbGQgdGhlIGJhc2Ugb2Ygb3VyIHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAgPSAkKCc8ZGl2IGNsYXNzPVwidG9vbHRpcHN0ZXItYmFzZSAnKyBzZWxmLm9wdGlvbnMudGhlbWUgKydcIiBzdHlsZT1cIicrIG1pbldpZHRoICsnICcrIG1heFdpZHRoICsnICcrIHBvaW50ZXJFdmVudHMgKycgJysgYW5pbWF0aW9uU3BlZWQgKydcIj48ZGl2IGNsYXNzPVwidG9vbHRpcHN0ZXItY29udGVudFwiPjwvZGl2PjwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBvbmx5IGFkZCB0aGUgYW5pbWF0aW9uIGNsYXNzIGlmIHRoZSB1c2VyIGhhcyBhIGJyb3dzZXIgdGhhdCBzdXBwb3J0cyBhbmltYXRpb25zXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3VwcG9ydHNUcmFuc2l0aW9ucygpKSBzZWxmLiR0b29sdGlwLmFkZENsYXNzKGFuaW1hdGlvbik7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGluc2VydCB0aGUgY29udGVudFxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5fY29udGVudF9pbnNlcnQoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gYXR0YWNoXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLmFwcGVuZFRvKCdib2R5Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGRvIGFsbCB0aGUgY3JhenkgY2FsY3VsYXRpb25zIGFuZCBwb3NpdGlvbmluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5yZXBvc2l0aW9uKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNhbGwgb3VyIGN1c3RvbSBjYWxsYmFjayBzaW5jZSB0aGUgY29udGVudCBvZiB0aGUgdG9vbHRpcCBpcyBub3cgcGFydCBvZiB0aGUgRE9NXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLm9wdGlvbnMuZnVuY3Rpb25SZWFkeS5jYWxsKHNlbGYuJGVsLCBzZWxmLiRlbCwgc2VsZi4kdG9vbHRpcCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGFuaW1hdGUgaW4gdGhlIHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdXBwb3J0c1RyYW5zaXRpb25zKCkpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAuYWRkQ2xhc3MoYW5pbWF0aW9uICsgJy1zaG93Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzZWxmLm9wdGlvbnMuc3BlZWQgPiAwKSBzZWxmLiR0b29sdGlwLmRlbGF5KHNlbGYub3B0aW9ucy5zcGVlZCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLnF1ZXVlKGZpbmlzaCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLmNzcygnZGlzcGxheScsICdub25lJykuZmFkZUluKHNlbGYub3B0aW9ucy5zcGVlZCwgZmluaXNoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2lsbCBjaGVjayBpZiBvdXIgdG9vbHRpcCBvcmlnaW4gaXMgcmVtb3ZlZCB3aGlsZSB0aGUgdG9vbHRpcCBpcyBzaG93blxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5faW50ZXJ2YWxfc2V0KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlcG9zaXRpb24gb24gc2Nyb2xsIChvdGhlcndpc2UgcG9zaXRpb246Zml4ZWQgZWxlbWVudCdzIHRvb2x0aXBzIHdpbGwgbW92ZSBhd2F5IGZvcm0gdGhlaXIgb3JpZ2luKSBhbmQgb24gcmVzaXplIChpbiBjYXNlIHBvc2l0aW9uIGNhbi9oYXMgdG8gYmUgY2hhbmdlZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vQFRPRE8gREVMRVRFIElUXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAkKHdpbmRvdykub24oJ3Njcm9sbC4nKyBzZWxmLm5hbWVzcGFjZSArJyByZXNpemUuJysgc2VsZi5uYW1lc3BhY2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIHNlbGYucmVwb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGF1dG8tY2xvc2UgYmluZGluZ3NcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMuYXV0b0Nsb3NlKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpbiBjYXNlIGEgbGlzdGVuZXIgaXMgYWxyZWFkeSBib3VuZCBmb3IgYXV0b2Nsb3NpbmcgKG1vdXNlIG9yIHRvdWNoLCBob3ZlciBvciBjbGljayksIHVuYmluZCBpdCBmaXJzdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5vZmYoJy4nKyBzZWxmLm5hbWVzcGFjZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlJ2xsIGhhdmUgdG8gc2V0IGRpZmZlcmVudCBzZXRzIG9mIGJpbmRpbmdzIGZvciBib3RoIHRvdWNoIGFuZCBtb3VzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMudHJpZ2dlciA9PSAnaG92ZXInKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIHVzZXIgdG91Y2hlcyB0aGUgYm9keSwgaGlkZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGV2aWNlSGFzVG91Y2hDYXBhYmlsaXR5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aW1lb3V0IDAgOiBleHBsYW5hdGlvbiBiZWxvdyBpbiBjbGljayBzZWN0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdlIGRvbid0IHdhbnQgdG8gYmluZCBvbiBjbGljayBoZXJlIGJlY2F1c2UgdGhlIGluaXRpYWwgdG91Y2hzdGFydCBldmVudCBoYXMgbm90IHlldCB0cmlnZ2VyZWQgaXRzIGNsaWNrIGV2ZW50LCB3aGljaCBpcyB0aHVzIGFib3V0IHRvIGhhcHBlblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5vbigndG91Y2hzdGFydC4nKyBzZWxmLm5hbWVzcGFjZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiB3ZSBoYXZlIHRvIGFsbG93IGludGVyYWN0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMuaW50ZXJhY3RpdmUpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdG91Y2ggZXZlbnRzIGluc2lkZSB0aGUgdG9vbHRpcCBtdXN0IG5vdCBjbG9zZSBpdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRldmljZUhhc1RvdWNoQ2FwYWJpbGl0eSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAub24oJ3RvdWNoc3RhcnQuJysgc2VsZi5uYW1lc3BhY2UsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBhcyBmb3IgbW91c2UgaW50ZXJhY3Rpb24sIHdlIGdldCByaWQgb2YgdGhlIHRvb2x0aXAgb25seSBhZnRlciB0aGUgbW91c2UgaGFzIHNwZW50IHNvbWUgdGltZSBvdXQgb2YgaXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0b2xlcmFuY2UgPSBudWxsO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiRlbFByb3h5LmFkZChzZWxmLiR0b29sdGlwKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGlkZSBhZnRlciBzb21lIHRpbWUgb3V0IG9mIHRoZSBwcm94eSBhbmQgdGhlIHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAub24oJ21vdXNlbGVhdmUuJysgc2VsZi5uYW1lc3BhY2UgKyAnLWF1dG9DbG9zZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodG9sZXJhbmNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9sZXJhbmNlID0gc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHNlbGYub3B0aW9ucy5pbnRlcmFjdGl2ZVRvbGVyYW5jZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBzdXNwZW5kIHRpbWVvdXQgd2hlbiB0aGUgbW91c2UgaXMgb3ZlciB0aGUgcHJveHkgb3IgdGhlIHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAub24oJ21vdXNlZW50ZXIuJysgc2VsZi5uYW1lc3BhY2UgKyAnLWF1dG9DbG9zZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodG9sZXJhbmNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGlzIGlzIGEgbm9uLWludGVyYWN0aXZlIHRvb2x0aXAsIGdldCByaWQgb2YgaXQgaWYgdGhlIG1vdXNlIGxlYXZlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJGVsUHJveHkub24oJ21vdXNlbGVhdmUuJysgc2VsZi5uYW1lc3BhY2UgKyAnLWF1dG9DbG9zZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBjbG9zZSB0aGUgdG9vbHRpcCB3aGVuIHRoZSBwcm94eSBnZXRzIGEgY2xpY2sgKGNvbW1vbiBiZWhhdmlvciBvZiBuYXRpdmUgdG9vbHRpcHMpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMuaGlkZU9uQ2xpY2spIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi4kZWxQcm94eS5vbignY2xpY2suJysgc2VsZi5uYW1lc3BhY2UgKyAnLWF1dG9DbG9zZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSdsbCBzZXQgdGhlIHNhbWUgYmluZGluZ3MgZm9yIGJvdGggY2xpY2tzIGFuZCB0b3VjaCBvbiB0aGUgYm9keSB0byBoaWRlIHRoZSB0b29sdGlwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZihzZWxmLm9wdGlvbnMudHJpZ2dlciA9PSAnY2xpY2snKXtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB1c2UgYSB0aW1lb3V0IHRvIHByZXZlbnQgaW1tZWRpYXRlIGNsb3NpbmcgaWYgdGhlIG1ldGhvZCB3YXMgY2FsbGVkIG9uIGEgY2xpY2sgZXZlbnQgYW5kIGlmIG9wdGlvbnMuZGVsYXkgPT0gMCAoYmVjYXVzZSBvZiBidWJibGluZylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5vbignY2xpY2suJysgc2VsZi5uYW1lc3BhY2UgKycgdG91Y2hzdGFydC4nKyBzZWxmLm5hbWVzcGFjZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYgaW50ZXJhY3RpdmUsIHdlJ2xsIHN0b3AgdGhlIGV2ZW50cyB0aGF0IHdlcmUgZW1pdHRlZCBmcm9tIGluc2lkZSB0aGUgdG9vbHRpcCB0byBzdG9wIGF1dG9DbG9zaW5nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMuaW50ZXJhY3RpdmUpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbm90ZSA6IHRoZSB0b3VjaCBldmVudHMgd2lsbCBqdXN0IG5vdCBiZSB1c2VkIGlmIHRoZSBwbHVnaW4gaXMgbm90IGVuYWJsZWQgb24gdG91Y2ggZGV2aWNlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcC5vbignY2xpY2suJysgc2VsZi5uYW1lc3BhY2UgKycgdG91Y2hzdGFydC4nKyBzZWxmLm5hbWVzcGFjZSwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgd2UgaGF2ZSBhIHRpbWVyIHNldCwgbGV0IHRoZSBjb3VudGRvd24gYmVnaW5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy50aW1lciA+IDApIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi50aW1lckhpZGUgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYudGltZXJIaWRlID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIHNlbGYub3B0aW9ucy50aW1lciArIGV4dHJhVGltZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBfaW50ZXJ2YWxfc2V0OiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICAgICAgICBzZWxmLmNoZWNrSW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSB0b29sdGlwIGFuZC9vciBpdHMgaW50ZXJ2YWwgc2hvdWxkIGJlIHN0b3BwZWRcbiAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBvcmlnaW4gaGFzIGJlZW4gcmVtb3ZlZFxuICAgICAgICAgICAgICAgICQoJ2JvZHknKS5maW5kKHNlbGYuJGVsKS5sZW5ndGggPT09IDBcbiAgICAgICAgICAgICAgICAvLyBpZiB0aGUgZWxQcm94eSBoYXMgYmVlbiByZW1vdmVkXG4gICAgICAgICAgICAgICAgfHxcdCQoJ2JvZHknKS5maW5kKHNlbGYuJGVsUHJveHkpLmxlbmd0aCA9PT0gMFxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSB0b29sdGlwIGhhcyBiZWVuIGNsb3NlZFxuICAgICAgICAgICAgICAgIHx8XHRzZWxmLlN0YXR1cyA9PSAnaGlkZGVuJ1xuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSB0b29sdGlwIGhhcyBzb21laG93IGJlZW4gcmVtb3ZlZFxuICAgICAgICAgICAgICAgIHx8XHQkKCdib2R5JykuZmluZChzZWxmLiR0b29sdGlwKS5sZW5ndGggPT09IDBcbiAgICAgICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZlIHRoZSB0b29sdGlwIGlmIGl0J3Mgc3RpbGwgaGVyZVxuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi5TdGF0dXMgPT0gJ3Nob3duJyB8fCBzZWxmLlN0YXR1cyA9PSAnYXBwZWFyaW5nJykgc2VsZi5oaWRlKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gY2xlYXIgdGhpcyBpbnRlcnZhbCBhcyBpdCBpcyBubyBsb25nZXIgbmVjZXNzYXJ5XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuX2ludGVydmFsX2NhbmNlbCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBpZiBldmVyeXRoaW5nIGlzIGFscmlnaHRcbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gY29tcGFyZSB0aGUgZm9ybWVyIGFuZCBjdXJyZW50IHBvc2l0aW9ucyBvZiB0aGUgZWxQcm94eSB0byByZXBvc2l0aW9uIHRoZSB0b29sdGlwIGlmIG5lZWQgYmVcbiAgICAgICAgICAgICAgICAgICAgaWYoc2VsZi5vcHRpb25zLnBvc2l0aW9uVHJhY2tlcil7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwID0gc2VsZi5fcmVwb3NpdGlvbkluZm8oc2VsZi4kZWxQcm94eSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWRlbnRpY2FsID0gZmFsc2U7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbXBhcmUgc2l6ZSBmaXJzdCAoYSBjaGFuZ2UgcmVxdWlyZXMgcmVwb3NpdGlvbmluZyB0b28pXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihhcmVFcXVhbChwLmRpbWVuc2lvbiwgc2VsZi5lbFByb3h5UG9zaXRpb24uZGltZW5zaW9uKSl7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBmb3IgZWxlbWVudHMgd2l0aCBhIGZpeGVkIHBvc2l0aW9uLCB3ZSB0cmFjayB0aGUgdG9wIGFuZCBsZWZ0IHByb3BlcnRpZXMgKHJlbGF0aXZlIHRvIHdpbmRvdylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzZWxmLiRlbFByb3h5LmNzcygncG9zaXRpb24nKSA9PT0gJ2ZpeGVkJyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGFyZUVxdWFsKHAucG9zaXRpb24sIHNlbGYuZWxQcm94eVBvc2l0aW9uLnBvc2l0aW9uKSkgaWRlbnRpY2FsID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gb3RoZXJ3aXNlLCB0cmFjayB0b3RhbCBvZmZzZXQgKHJlbGF0aXZlIHRvIGRvY3VtZW50KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihhcmVFcXVhbChwLm9mZnNldCwgc2VsZi5lbFByb3h5UG9zaXRpb24ub2Zmc2V0KSkgaWRlbnRpY2FsID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCFpZGVudGljYWwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYucmVwb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYub3B0aW9ucy5wb3NpdGlvblRyYWNrZXJDYWxsYmFjay5jYWxsKHNlbGYsIHNlbGYuJGVsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDIwMCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgX2ludGVydmFsX2NhbmNlbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMuY2hlY2tJbnRlcnZhbCk7XG4gICAgICAgICAgICAvLyBjbGVhbiBkZWxldGVcbiAgICAgICAgICAgIHRoaXMuY2hlY2tJbnRlcnZhbCA9IG51bGw7XG4gICAgICAgIH0sXG5cbiAgICAgICAgX2NvbnRlbnRfc2V0OiBmdW5jdGlvbihjb250ZW50KSB7XG4gICAgICAgICAgICAvLyBjbG9uZSBpZiBhc2tlZC4gQ2xvbmluZyB0aGUgb2JqZWN0IG1ha2VzIHN1cmUgdGhhdCBlYWNoIGluc3RhbmNlIGhhcyBpdHMgb3duIHZlcnNpb24gb2YgdGhlIGNvbnRlbnQgKGluIGNhc2UgYSBzYW1lIG9iamVjdCB3ZXJlIHByb3ZpZGVkIGZvciBzZXZlcmFsIGluc3RhbmNlcylcbiAgICAgICAgICAgIC8vIHJlbWluZGVyIDogdHlwZW9mIG51bGwgPT09IG9iamVjdFxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnb2JqZWN0JyAmJiBjb250ZW50ICE9PSBudWxsICYmIHRoaXMub3B0aW9ucy5jb250ZW50Q2xvbmluZykge1xuICAgICAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50LmNsb25lKHRydWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5Db250ZW50ID0gY29udGVudDtcbiAgICAgICAgfSxcblxuICAgICAgICBfY29udGVudF9pbnNlcnQ6IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgICAgICAgICAgJGQgPSB0aGlzLiR0b29sdGlwLmZpbmQoJy50b29sdGlwc3Rlci1jb250ZW50Jyk7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2Ygc2VsZi5Db250ZW50ID09PSAnc3RyaW5nJyAmJiAhc2VsZi5vcHRpb25zLmNvbnRlbnRBc0hUTUwpIHtcbiAgICAgICAgICAgICAgICAkZC50ZXh0KHNlbGYuQ29udGVudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAkZFxuICAgICAgICAgICAgICAgICAgICAuZW1wdHkoKVxuICAgICAgICAgICAgICAgICAgICAuYXBwZW5kKHNlbGYuQ29udGVudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgX3VwZGF0ZTogZnVuY3Rpb24oY29udGVudCkge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgICAgIC8vIGNoYW5nZSB0aGUgY29udGVudFxuICAgICAgICAgICAgc2VsZi5fY29udGVudF9zZXQoY29udGVudCk7XG5cbiAgICAgICAgICAgIGlmIChzZWxmLkNvbnRlbnQgIT09IG51bGwpIHtcblxuICAgICAgICAgICAgICAgIC8vIHVwZGF0ZSB0aGUgdG9vbHRpcCBpZiBpdCBpcyBvcGVuXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYuU3RhdHVzICE9PSAnaGlkZGVuJykge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlc2V0IHRoZSBjb250ZW50IGluIHRoZSB0b29sdGlwXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuX2NvbnRlbnRfaW5zZXJ0KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gcmVwb3NpdGlvbiBhbmQgcmVzaXplIHRoZSB0b29sdGlwXG4gICAgICAgICAgICAgICAgICAgIHNlbGYucmVwb3NpdGlvbigpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHdlIHdhbnQgdG8gcGxheSBhIGxpdHRsZSBhbmltYXRpb24gc2hvd2luZyB0aGUgY29udGVudCBjaGFuZ2VkXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMudXBkYXRlQW5pbWF0aW9uKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdXBwb3J0c1RyYW5zaXRpb25zKCkpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICctd2Via2l0LXRyYW5zaXRpb24nOiAnYWxsICcgKyBzZWxmLm9wdGlvbnMuc3BlZWQgKyAnbXMsIHdpZHRoIDBtcywgaGVpZ2h0IDBtcywgbGVmdCAwbXMsIHRvcCAwbXMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnLW1vei10cmFuc2l0aW9uJzogJ2FsbCAnICsgc2VsZi5vcHRpb25zLnNwZWVkICsgJ21zLCB3aWR0aCAwbXMsIGhlaWdodCAwbXMsIGxlZnQgMG1zLCB0b3AgMG1zJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJy1vLXRyYW5zaXRpb24nOiAnYWxsICcgKyBzZWxmLm9wdGlvbnMuc3BlZWQgKyAnbXMsIHdpZHRoIDBtcywgaGVpZ2h0IDBtcywgbGVmdCAwbXMsIHRvcCAwbXMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnLW1zLXRyYW5zaXRpb24nOiAnYWxsICcgKyBzZWxmLm9wdGlvbnMuc3BlZWQgKyAnbXMsIHdpZHRoIDBtcywgaGVpZ2h0IDBtcywgbGVmdCAwbXMsIHRvcCAwbXMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndHJhbnNpdGlvbic6ICdhbGwgJyArIHNlbGYub3B0aW9ucy5zcGVlZCArICdtcywgd2lkdGggMG1zLCBoZWlnaHQgMG1zLCBsZWZ0IDBtcywgdG9wIDBtcydcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5hZGRDbGFzcygndG9vbHRpcHN0ZXItY29udGVudC1jaGFuZ2luZycpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVzZXQgdGhlIENTUyB0cmFuc2l0aW9ucyBhbmQgZmluaXNoIHRoZSBjaGFuZ2UgYW5pbWF0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzZWxmLlN0YXR1cyAhPSAnaGlkZGVuJyl7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAucmVtb3ZlQ2xhc3MoJ3Rvb2x0aXBzdGVyLWNvbnRlbnQtY2hhbmdpbmcnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYWZ0ZXIgdGhlIGNoYW5naW5nIGFuaW1hdGlvbiBoYXMgY29tcGxldGVkLCByZXNldCB0aGUgQ1NTIHRyYW5zaXRpb25zXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoc2VsZi5TdGF0dXMgIT09ICdoaWRkZW4nKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcC5jc3Moe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJy13ZWJraXQtdHJhbnNpdGlvbic6IHNlbGYub3B0aW9ucy5zcGVlZCArICdtcycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnLW1vei10cmFuc2l0aW9uJzogc2VsZi5vcHRpb25zLnNwZWVkICsgJ21zJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICctby10cmFuc2l0aW9uJzogc2VsZi5vcHRpb25zLnNwZWVkICsgJ21zJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICctbXMtdHJhbnNpdGlvbic6IHNlbGYub3B0aW9ucy5zcGVlZCArICdtcycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndHJhbnNpdGlvbic6IHNlbGYub3B0aW9ucy5zcGVlZCArICdtcydcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgc2VsZi5vcHRpb25zLnNwZWVkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHNlbGYub3B0aW9ucy5zcGVlZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLmZhZGVUbyhzZWxmLm9wdGlvbnMuc3BlZWQsIDAuNSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHNlbGYuU3RhdHVzICE9ICdoaWRkZW4nKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAuZmFkZVRvKHNlbGYub3B0aW9ucy5zcGVlZCwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHNlbGYuaGlkZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIF9yZXBvc2l0aW9uSW5mbzogZnVuY3Rpb24oJGVsKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIGRpbWVuc2lvbjoge1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6ICRlbC5vdXRlckhlaWdodChmYWxzZSksXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAkZWwub3V0ZXJXaWR0aChmYWxzZSlcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9mZnNldDogJGVsLm9mZnNldCgpLFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IHBhcnNlSW50KCRlbC5jc3MoJ2xlZnQnKSksXG4gICAgICAgICAgICAgICAgICAgIHRvcDogcGFyc2VJbnQoJGVsLmNzcygndG9wJykpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSxcblxuICAgICAgICBoaWRlOiBmdW5jdGlvbihjYWxsYmFjaykge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgICAgIC8vIHNhdmUgdGhlIG1ldGhvZCBjdXN0b20gY2FsbGJhY2sgYW5kIGNhbmNlbCBhbnkgc2hvdyBtZXRob2QgY3VzdG9tIGNhbGxiYWNrc1xuICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSBzZWxmLmNhbGxiYWNrcy5oaWRlLnB1c2goY2FsbGJhY2spO1xuICAgICAgICAgICAgc2VsZi5jYWxsYmFja3Muc2hvdyA9IFtdO1xuXG4gICAgICAgICAgICAvLyBnZXQgcmlkIG9mIGFueSBhcHBlYXJhbmNlIHRpbWVvdXRcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChzZWxmLnRpbWVyU2hvdyk7XG4gICAgICAgICAgICBzZWxmLnRpbWVyU2hvdyA9IG51bGw7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQoc2VsZi50aW1lckhpZGUpO1xuICAgICAgICAgICAgc2VsZi50aW1lckhpZGUgPSBudWxsO1xuXG4gICAgICAgICAgICB2YXIgZmluaXNoQ2FsbGJhY2tzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgLy8gdHJpZ2dlciBhbnkgaGlkZSBtZXRob2QgY3VzdG9tIGNhbGxiYWNrcyBhbmQgcmVzZXQgdGhlbVxuICAgICAgICAgICAgICAgICQuZWFjaChzZWxmLmNhbGxiYWNrcy5oaWRlLCBmdW5jdGlvbihpLGMpIHsgYy5jYWxsKHNlbGYuJGVsKTsgfSk7XG4gICAgICAgICAgICAgICAgc2VsZi5jYWxsYmFja3MuaGlkZSA9IFtdO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgLy8gaGlkZVxuICAgICAgICAgICAgaWYgKHNlbGYuU3RhdHVzID09ICdzaG93bicgfHwgc2VsZi5TdGF0dXMgPT0gJ2FwcGVhcmluZycpIHtcblxuICAgICAgICAgICAgICAgIHNlbGYuU3RhdHVzID0gJ2Rpc2FwcGVhcmluZyc7XG5cbiAgICAgICAgICAgICAgICB2YXIgZmluaXNoID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgc2VsZi5TdGF0dXMgPSAnaGlkZGVuJztcblxuICAgICAgICAgICAgICAgICAgICAvLyBkZXRhY2ggb3VyIGNvbnRlbnQgb2JqZWN0IGZpcnN0LCBzbyB0aGUgbmV4dCBqUXVlcnkncyByZW1vdmUoKSBjYWxsIGRvZXMgbm90IHVuYmluZCBpdHMgZXZlbnQgaGFuZGxlcnNcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBzZWxmLkNvbnRlbnQgPT0gJ29iamVjdCcgJiYgc2VsZi5Db250ZW50ICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLkNvbnRlbnQuZGV0YWNoKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwID0gbnVsbDtcblxuICAgICAgICAgICAgICAgICAgICAvLyB1bmJpbmQgb3JpZW50YXRpb25jaGFuZ2UsIHNjcm9sbCBhbmQgcmVzaXplIGxpc3RlbmVyc1xuICAgICAgICAgICAgICAgICAgICAkKHdpbmRvdykub2ZmKCcuJysgc2VsZi5uYW1lc3BhY2UpO1xuXG4gICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKVxuICAgICAgICAgICAgICAgICAgICAvLyB1bmJpbmQgYW55IGF1dG8tY2xvc2luZyBjbGljay90b3VjaCBsaXN0ZW5lcnNcbiAgICAgICAgICAgICAgICAgICAgICAgIC5vZmYoJy4nKyBzZWxmLm5hbWVzcGFjZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jc3MoJ292ZXJmbG93LXgnLCBzZWxmLmJvZHlPdmVyZmxvd1gpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIHVuYmluZCBhbnkgYXV0by1jbG9zaW5nIGNsaWNrL3RvdWNoIGxpc3RlbmVyc1xuICAgICAgICAgICAgICAgICAgICAkKCdib2R5Jykub2ZmKCcuJysgc2VsZi5uYW1lc3BhY2UpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIHVuYmluZCBhbnkgYXV0by1jbG9zaW5nIGhvdmVyIGxpc3RlbmVyc1xuICAgICAgICAgICAgICAgICAgICBzZWxmLiRlbFByb3h5Lm9mZignLicrIHNlbGYubmFtZXNwYWNlICsgJy1hdXRvQ2xvc2UnKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBjYWxsIG91ciBjb25zdHJ1Y3RvciBjdXN0b20gY2FsbGJhY2sgZnVuY3Rpb25cbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vcHRpb25zLmZ1bmN0aW9uQWZ0ZXIuY2FsbChzZWxmLiRlbCwgc2VsZi4kZWwpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGNhbGwgb3VyIG1ldGhvZCBjdXN0b20gY2FsbGJhY2tzIGZ1bmN0aW9uc1xuICAgICAgICAgICAgICAgICAgICBmaW5pc2hDYWxsYmFja3MoKTtcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgaWYgKHN1cHBvcnRzVHJhbnNpdGlvbnMoKSkge1xuXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jbGVhclF1ZXVlKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygndG9vbHRpcHN0ZXItJyArIHNlbGYub3B0aW9ucy5hbmltYXRpb24gKyAnLXNob3cnKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZm9yIHRyYW5zaXRpb25zIG9ubHlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcygndG9vbHRpcHN0ZXItZHlpbmcnKTtcblxuICAgICAgICAgICAgICAgICAgICBpZihzZWxmLm9wdGlvbnMuc3BlZWQgPiAwKSBzZWxmLiR0b29sdGlwLmRlbGF5KHNlbGYub3B0aW9ucy5zcGVlZCk7XG5cbiAgICAgICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcC5xdWV1ZShmaW5pc2gpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcFxuICAgICAgICAgICAgICAgICAgICAgICAgLnN0b3AoKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmZhZGVPdXQoc2VsZi5vcHRpb25zLnNwZWVkLCBmaW5pc2gpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIGlmIHRoZSB0b29sdGlwIGlzIGFscmVhZHkgaGlkZGVuLCB3ZSBzdGlsbCBuZWVkIHRvIHRyaWdnZXIgdGhlIG1ldGhvZCBjdXN0b20gY2FsbGJhY2tcbiAgICAgICAgICAgIGVsc2UgaWYoc2VsZi5TdGF0dXMgPT0gJ2hpZGRlbicpIHtcbiAgICAgICAgICAgICAgICBmaW5pc2hDYWxsYmFja3MoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHNlbGY7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLy8gdGhlIHB1YmxpYyBzaG93KCkgbWV0aG9kIGlzIGFjdHVhbGx5IGFuIGFsaWFzIGZvciB0aGUgcHJpdmF0ZSBzaG93Tm93KCkgbWV0aG9kXG4gICAgICAgIHNob3c6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICB0aGlzLl9zaG93Tm93KGNhbGxiYWNrKTtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vICd1cGRhdGUnIGlzIGRlcHJlY2F0ZWQgaW4gZmF2b3Igb2YgJ2NvbnRlbnQnIGJ1dCBpcyBrZXB0IGZvciBiYWNrd2FyZCBjb21wYXRpYmlsaXR5XG4gICAgICAgIHVwZGF0ZTogZnVuY3Rpb24oYykge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29udGVudChjKTtcbiAgICAgICAgfSxcbiAgICAgICAgY29udGVudDogZnVuY3Rpb24oYykge1xuICAgICAgICAgICAgLy8gZ2V0dGVyIG1ldGhvZFxuICAgICAgICAgICAgaWYodHlwZW9mIGMgPT09ICd1bmRlZmluZWQnKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5Db250ZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gc2V0dGVyIG1ldGhvZFxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fdXBkYXRlKGMpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlcG9zaXRpb246IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgICAgIC8vIGluIGNhc2UgdGhlIHRvb2x0aXAgaGFzIGJlZW4gcmVtb3ZlZCBmcm9tIERPTSBtYW51YWxseVxuICAgICAgICAgICAgaWYgKCQoJ2JvZHknKS5maW5kKHNlbGYuJHRvb2x0aXApLmxlbmd0aCAhPT0gMCkge1xuXG4gICAgICAgICAgICAgICAgLy8gcmVzZXQgd2lkdGhcbiAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLmNzcygnd2lkdGgnLCAnJyk7XG5cbiAgICAgICAgICAgICAgICAvLyBmaW5kIHZhcmlhYmxlcyB0byBkZXRlcm1pbmUgcGxhY2VtZW50XG4gICAgICAgICAgICAgICAgc2VsZi5lbFByb3h5UG9zaXRpb24gPSBzZWxmLl9yZXBvc2l0aW9uSW5mbyhzZWxmLiRlbFByb3h5KTtcbiAgICAgICAgICAgICAgICB2YXIgYXJyb3dSZXBvc2l0aW9uID0gbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgd2luZG93V2lkdGggPSAkKHdpbmRvdykud2lkdGgoKSxcbiAgICAgICAgICAgICAgICAvLyBzaG9ydGhhbmRcbiAgICAgICAgICAgICAgICAgICAgcHJveHkgPSBzZWxmLmVsUHJveHlQb3NpdGlvbixcbiAgICAgICAgICAgICAgICAgICAgdG9vbHRpcFdpZHRoID0gc2VsZi4kdG9vbHRpcC5vdXRlcldpZHRoKGZhbHNlKSxcbiAgICAgICAgICAgICAgICAgICAgdG9vbHRpcElubmVyV2lkdGggPSBzZWxmLiR0b29sdGlwLmlubmVyV2lkdGgoKSArIDEsIC8vIHRoaXMgKzEgc3RvcHMgRmlyZUZveCBmcm9tIHNvbWV0aW1lcyBmb3JjaW5nIGFuIGFkZGl0aW9uYWwgdGV4dCBsaW5lXG4gICAgICAgICAgICAgICAgICAgIHRvb2x0aXBIZWlnaHQgPSBzZWxmLiR0b29sdGlwLm91dGVySGVpZ2h0KGZhbHNlKTtcblxuICAgICAgICAgICAgICAgIC8vIGlmIHRoaXMgaXMgYW4gPGFyZWE+IHRhZyBpbnNpZGUgYSA8bWFwPiwgYWxsIGhlbGwgYnJlYWtzIGxvb3NlLiBSZWNhbGN1bGF0ZSBhbGwgdGhlIG1lYXN1cmVtZW50cyBiYXNlZCBvbiBjb29yZGluYXRlc1xuICAgICAgICAgICAgICAgIGlmIChzZWxmLiRlbFByb3h5LmlzKCdhcmVhJykpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGFyZWFTaGFwZSA9IHNlbGYuJGVsUHJveHkuYXR0cignc2hhcGUnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcE5hbWUgPSBzZWxmLiRlbFByb3h5LnBhcmVudCgpLmF0dHIoJ25hbWUnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcCA9ICQoJ2ltZ1t1c2VtYXA9XCIjJysgbWFwTmFtZSArJ1wiXScpLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWFwT2Zmc2V0TGVmdCA9IG1hcC5vZmZzZXQoKS5sZWZ0LFxuICAgICAgICAgICAgICAgICAgICAgICAgbWFwT2Zmc2V0VG9wID0gbWFwLm9mZnNldCgpLnRvcCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGFyZWFNZWFzdXJlbWVudHMgPSBzZWxmLiRlbFByb3h5LmF0dHIoJ2Nvb3JkcycpICE9PSB1bmRlZmluZWQgPyBzZWxmLiRlbFByb3h5LmF0dHIoJ2Nvb3JkcycpLnNwbGl0KCcsJykgOiB1bmRlZmluZWQ7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKGFyZWFTaGFwZSA9PSAnY2lyY2xlJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFyZWFMZWZ0ID0gcGFyc2VJbnQoYXJlYU1lYXN1cmVtZW50c1swXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJlYVRvcCA9IHBhcnNlSW50KGFyZWFNZWFzdXJlbWVudHNbMV0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZWFXaWR0aCA9IHBhcnNlSW50KGFyZWFNZWFzdXJlbWVudHNbMl0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkuZGltZW5zaW9uLmhlaWdodCA9IGFyZWFXaWR0aCAqIDI7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm94eS5kaW1lbnNpb24ud2lkdGggPSBhcmVhV2lkdGggKiAyO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkub2Zmc2V0LnRvcCA9IG1hcE9mZnNldFRvcCArIGFyZWFUb3AgLSBhcmVhV2lkdGg7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm94eS5vZmZzZXQubGVmdCA9IG1hcE9mZnNldExlZnQgKyBhcmVhTGVmdCAtIGFyZWFXaWR0aDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChhcmVhU2hhcGUgPT0gJ3JlY3QnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYXJlYUxlZnQgPSBwYXJzZUludChhcmVhTWVhc3VyZW1lbnRzWzBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhVG9wID0gcGFyc2VJbnQoYXJlYU1lYXN1cmVtZW50c1sxXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJlYVJpZ2h0ID0gcGFyc2VJbnQoYXJlYU1lYXN1cmVtZW50c1syXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJlYUJvdHRvbSA9IHBhcnNlSW50KGFyZWFNZWFzdXJlbWVudHNbM10pO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkuZGltZW5zaW9uLmhlaWdodCA9IGFyZWFCb3R0b20gLSBhcmVhVG9wO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkuZGltZW5zaW9uLndpZHRoID0gYXJlYVJpZ2h0IC0gYXJlYUxlZnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm94eS5vZmZzZXQudG9wID0gbWFwT2Zmc2V0VG9wICsgYXJlYVRvcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb3h5Lm9mZnNldC5sZWZ0ID0gbWFwT2Zmc2V0TGVmdCArIGFyZWFMZWZ0O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKGFyZWFTaGFwZSA9PSAncG9seScpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcmVhWHMgPSBbXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhWXMgPSBbXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhU21hbGxlc3RYID0gMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhU21hbGxlc3RZID0gMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhR3JlYXRlc3RYID0gMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhR3JlYXRlc3RZID0gMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnJheUFsdGVybmF0ZSA9ICdldmVuJztcblxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhcmVhTWVhc3VyZW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFyZWFOdW1iZXIgPSBwYXJzZUludChhcmVhTWVhc3VyZW1lbnRzW2ldKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcnJheUFsdGVybmF0ZSA9PSAnZXZlbicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFyZWFOdW1iZXIgPiBhcmVhR3JlYXRlc3RYKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmVhR3JlYXRlc3RYID0gYXJlYU51bWJlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJlYVNtYWxsZXN0WCA9IGFyZWFHcmVhdGVzdFg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXJlYU51bWJlciA8IGFyZWFTbWFsbGVzdFgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZWFTbWFsbGVzdFggPSBhcmVhTnVtYmVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJyYXlBbHRlcm5hdGUgPSAnb2RkJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcmVhTnVtYmVyID4gYXJlYUdyZWF0ZXN0WSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJlYUdyZWF0ZXN0WSA9IGFyZWFOdW1iZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaSA9PSAxKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJlYVNtYWxsZXN0WSA9IGFyZWFHcmVhdGVzdFk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXJlYU51bWJlciA8IGFyZWFTbWFsbGVzdFkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZWFTbWFsbGVzdFkgPSBhcmVhTnVtYmVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJyYXlBbHRlcm5hdGUgPSAnZXZlbic7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm94eS5kaW1lbnNpb24uaGVpZ2h0ID0gYXJlYUdyZWF0ZXN0WSAtIGFyZWFTbWFsbGVzdFk7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm94eS5kaW1lbnNpb24ud2lkdGggPSBhcmVhR3JlYXRlc3RYIC0gYXJlYVNtYWxsZXN0WDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb3h5Lm9mZnNldC50b3AgPSBtYXBPZmZzZXRUb3AgKyBhcmVhU21hbGxlc3RZO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkub2Zmc2V0LmxlZnQgPSBtYXBPZmZzZXRMZWZ0ICsgYXJlYVNtYWxsZXN0WDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb3h5LmRpbWVuc2lvbi5oZWlnaHQgPSBtYXAub3V0ZXJIZWlnaHQoZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkuZGltZW5zaW9uLndpZHRoID0gbWFwLm91dGVyV2lkdGgoZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJveHkub2Zmc2V0LnRvcCA9IG1hcE9mZnNldFRvcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb3h5Lm9mZnNldC5sZWZ0ID0gbWFwT2Zmc2V0TGVmdDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIG91ciBmdW5jdGlvbiBhbmQgZ2xvYmFsIHZhcnMgZm9yIHBvc2l0aW9uaW5nIG91ciB0b29sdGlwXG4gICAgICAgICAgICAgICAgdmFyIG15TGVmdCA9IDAsXG4gICAgICAgICAgICAgICAgICAgIG15TGVmdE1pcnJvciA9IDAsXG4gICAgICAgICAgICAgICAgICAgIG15VG9wID0gMCxcbiAgICAgICAgICAgICAgICAgICAgb2Zmc2V0WSA9IHBhcnNlSW50KHNlbGYub3B0aW9ucy5vZmZzZXRZKSxcbiAgICAgICAgICAgICAgICAgICAgb2Zmc2V0WCA9IHBhcnNlSW50KHNlbGYub3B0aW9ucy5vZmZzZXRYKSxcbiAgICAgICAgICAgICAgICAvLyB0aGlzIGlzIHRoZSBhcnJvdyBwb3NpdGlvbiB0aGF0IHdpbGwgZXZlbnR1YWxseSBiZSB1c2VkLiBJdCBtYXkgZGlmZmVyIGZyb20gdGhlIHBvc2l0aW9uIG9wdGlvbiBpZiB0aGUgdG9vbHRpcCBjYW5ub3QgYmUgZGlzcGxheWVkIGluIHRoaXMgcG9zaXRpb25cbiAgICAgICAgICAgICAgICAgICAgcHJhY3RpY2FsUG9zaXRpb24gPSBzZWxmLm9wdGlvbnMucG9zaXRpb247XG5cbiAgICAgICAgICAgICAgICAvLyBhIGZ1bmN0aW9uIHRvIGRldGVjdCBpZiB0aGUgdG9vbHRpcCBpcyBnb2luZyBvZmYgdGhlIHNjcmVlbiBob3Jpem9udGFsbHkuIElmIHNvLCByZXBvc2l0aW9uIHRoZSBjcmFwIG91dCBvZiBpdCFcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBkb250R29PZmZTY3JlZW5YKCkge1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciB3aW5kb3dMZWZ0ID0gJCh3aW5kb3cpLnNjcm9sbExlZnQoKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGUgdG9vbHRpcCBnb2VzIG9mZiB0aGUgbGVmdCBzaWRlIG9mIHRoZSBzY3JlZW4sIGxpbmUgaXQgdXAgd2l0aCB0aGUgbGVmdCBzaWRlIG9mIHRoZSB3aW5kb3dcbiAgICAgICAgICAgICAgICAgICAgaWYoKG15TGVmdCAtIHdpbmRvd0xlZnQpIDwgMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dSZXBvc2l0aW9uID0gbXlMZWZ0IC0gd2luZG93TGVmdDtcbiAgICAgICAgICAgICAgICAgICAgICAgIG15TGVmdCA9IHdpbmRvd0xlZnQ7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGUgdG9vbHRpcCBnb2VzIG9mZiB0aGUgcmlnaHQgb2YgdGhlIHNjcmVlbiwgbGluZSBpdCB1cCB3aXRoIHRoZSByaWdodCBzaWRlIG9mIHRoZSB3aW5kb3dcbiAgICAgICAgICAgICAgICAgICAgaWYgKCgobXlMZWZ0ICsgdG9vbHRpcFdpZHRoKSAtIHdpbmRvd0xlZnQpID4gd2luZG93V2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFycm93UmVwb3NpdGlvbiA9IG15TGVmdCAtICgod2luZG93V2lkdGggKyB3aW5kb3dMZWZ0KSAtIHRvb2x0aXBXaWR0aCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBteUxlZnQgPSAod2luZG93V2lkdGggKyB3aW5kb3dMZWZ0KSAtIHRvb2x0aXBXaWR0aDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIGEgZnVuY3Rpb24gdG8gZGV0ZWN0IGlmIHRoZSB0b29sdGlwIGlzIGdvaW5nIG9mZiB0aGUgc2NyZWVuIHZlcnRpY2FsbHkuIElmIHNvLCBzd2l0Y2ggdG8gdGhlIG9wcG9zaXRlIVxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGRvbnRHb09mZlNjcmVlblkoc3dpdGNoVG8sIHN3aXRjaEZyb20pIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgaXQgZ29lcyBvZmYgdGhlIHRvcCBvZmYgdGhlIHBhZ2VcbiAgICAgICAgICAgICAgICAgICAgaWYoKChwcm94eS5vZmZzZXQudG9wIC0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpIC0gdG9vbHRpcEhlaWdodCAtIG9mZnNldFkgLSAxMikgPCAwKSAmJiAoc3dpdGNoRnJvbS5pbmRleE9mKCd0b3AnKSA+IC0xKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJhY3RpY2FsUG9zaXRpb24gPSBzd2l0Y2hUbztcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIGl0IGdvZXMgb2ZmIHRoZSBib3R0b20gb2YgdGhlIHBhZ2VcbiAgICAgICAgICAgICAgICAgICAgaWYgKCgocHJveHkub2Zmc2V0LnRvcCArIHByb3h5LmRpbWVuc2lvbi5oZWlnaHQgKyB0b29sdGlwSGVpZ2h0ICsgMTIgKyBvZmZzZXRZKSA+ICgkKHdpbmRvdykuc2Nyb2xsVG9wKCkgKyAkKHdpbmRvdykuaGVpZ2h0KCkpKSAmJiAoc3dpdGNoRnJvbS5pbmRleE9mKCdib3R0b20nKSA+IC0xKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJhY3RpY2FsUG9zaXRpb24gPSBzd2l0Y2hUbztcbiAgICAgICAgICAgICAgICAgICAgICAgIG15VG9wID0gKHByb3h5Lm9mZnNldC50b3AgLSB0b29sdGlwSGVpZ2h0KSAtIG9mZnNldFkgLSAxMjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKHByYWN0aWNhbFBvc2l0aW9uID09ICd0b3AnKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBsZWZ0RGlmZmVyZW5jZSA9IChwcm94eS5vZmZzZXQubGVmdCArIHRvb2x0aXBXaWR0aCkgLSAocHJveHkub2Zmc2V0LmxlZnQgKyBwcm94eS5kaW1lbnNpb24ud2lkdGgpO1xuICAgICAgICAgICAgICAgICAgICBteUxlZnQgPSAocHJveHkub2Zmc2V0LmxlZnQgKyBvZmZzZXRYKSAtIChsZWZ0RGlmZmVyZW5jZSAvIDIpO1xuICAgICAgICAgICAgICAgICAgICBteVRvcCA9IChwcm94eS5vZmZzZXQudG9wIC0gdG9vbHRpcEhlaWdodCkgLSBvZmZzZXRZIC0gMTI7XG4gICAgICAgICAgICAgICAgICAgIGRvbnRHb09mZlNjcmVlblgoKTtcbiAgICAgICAgICAgICAgICAgICAgZG9udEdvT2ZmU2NyZWVuWSgnYm90dG9tJywgJ3RvcCcpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKHByYWN0aWNhbFBvc2l0aW9uID09ICd0b3AtbGVmdCcpIHtcbiAgICAgICAgICAgICAgICAgICAgbXlMZWZ0ID0gcHJveHkub2Zmc2V0LmxlZnQgKyBvZmZzZXRYO1xuICAgICAgICAgICAgICAgICAgICBteVRvcCA9IChwcm94eS5vZmZzZXQudG9wIC0gdG9vbHRpcEhlaWdodCkgLSBvZmZzZXRZIC0gMTI7XG4gICAgICAgICAgICAgICAgICAgIGRvbnRHb09mZlNjcmVlblgoKTtcbiAgICAgICAgICAgICAgICAgICAgZG9udEdvT2ZmU2NyZWVuWSgnYm90dG9tLWxlZnQnLCAndG9wLWxlZnQnKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZihwcmFjdGljYWxQb3NpdGlvbiA9PSAndG9wLXJpZ2h0Jykge1xuICAgICAgICAgICAgICAgICAgICBteUxlZnQgPSAocHJveHkub2Zmc2V0LmxlZnQgKyBwcm94eS5kaW1lbnNpb24ud2lkdGggKyBvZmZzZXRYKSAtIHRvb2x0aXBXaWR0aDtcbiAgICAgICAgICAgICAgICAgICAgbXlUb3AgPSAocHJveHkub2Zmc2V0LnRvcCAtIHRvb2x0aXBIZWlnaHQpIC0gb2Zmc2V0WSAtIDEyO1xuICAgICAgICAgICAgICAgICAgICBkb250R29PZmZTY3JlZW5YKCk7XG4gICAgICAgICAgICAgICAgICAgIGRvbnRHb09mZlNjcmVlblkoJ2JvdHRvbS1yaWdodCcsICd0b3AtcmlnaHQnKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZihwcmFjdGljYWxQb3NpdGlvbiA9PSAnYm90dG9tJykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgbGVmdERpZmZlcmVuY2UgPSAocHJveHkub2Zmc2V0LmxlZnQgKyB0b29sdGlwV2lkdGgpIC0gKHByb3h5Lm9mZnNldC5sZWZ0ICsgcHJveHkuZGltZW5zaW9uLndpZHRoKTtcbiAgICAgICAgICAgICAgICAgICAgbXlMZWZ0ID0gcHJveHkub2Zmc2V0LmxlZnQgLSAobGVmdERpZmZlcmVuY2UgLyAyKSArIG9mZnNldFg7XG4gICAgICAgICAgICAgICAgICAgIG15VG9wID0gKHByb3h5Lm9mZnNldC50b3AgKyBwcm94eS5kaW1lbnNpb24uaGVpZ2h0KSArIG9mZnNldFkgKyAxMjtcbiAgICAgICAgICAgICAgICAgICAgLy8gZG9udEdvT2ZmU2NyZWVuWCgpO1xuICAgICAgICAgICAgICAgICAgICAvLyBkb250R29PZmZTY3JlZW5ZKCd0b3AnLCAnYm90dG9tJyk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYocHJhY3RpY2FsUG9zaXRpb24gPT0gJ2JvdHRvbS1sZWZ0Jykge1xuICAgICAgICAgICAgICAgICAgICBteUxlZnQgPSBwcm94eS5vZmZzZXQubGVmdCArIG9mZnNldFg7XG4gICAgICAgICAgICAgICAgICAgIG15VG9wID0gKHByb3h5Lm9mZnNldC50b3AgKyBwcm94eS5kaW1lbnNpb24uaGVpZ2h0KSArIG9mZnNldFkgKyAxMjtcbiAgICAgICAgICAgICAgICAgICAgZG9udEdvT2ZmU2NyZWVuWCgpO1xuICAgICAgICAgICAgICAgICAgICBkb250R29PZmZTY3JlZW5ZKCd0b3AtbGVmdCcsICdib3R0b20tbGVmdCcpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKHByYWN0aWNhbFBvc2l0aW9uID09ICdib3R0b20tcmlnaHQnKSB7XG4gICAgICAgICAgICAgICAgICAgIG15TGVmdCA9IChwcm94eS5vZmZzZXQubGVmdCArIHByb3h5LmRpbWVuc2lvbi53aWR0aCArIG9mZnNldFgpIC0gdG9vbHRpcFdpZHRoO1xuICAgICAgICAgICAgICAgICAgICBteVRvcCA9IChwcm94eS5vZmZzZXQudG9wICsgcHJveHkuZGltZW5zaW9uLmhlaWdodCkgKyBvZmZzZXRZICsgMTI7XG4gICAgICAgICAgICAgICAgICAgIGRvbnRHb09mZlNjcmVlblgoKTtcbiAgICAgICAgICAgICAgICAgICAgZG9udEdvT2ZmU2NyZWVuWSgndG9wLXJpZ2h0JywgJ2JvdHRvbS1yaWdodCcpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKHByYWN0aWNhbFBvc2l0aW9uID09ICdsZWZ0Jykge1xuICAgICAgICAgICAgICAgICAgICBteUxlZnQgPSBwcm94eS5vZmZzZXQubGVmdCAtIG9mZnNldFggLSB0b29sdGlwV2lkdGggLSAxMjtcbiAgICAgICAgICAgICAgICAgICAgbXlMZWZ0TWlycm9yID0gcHJveHkub2Zmc2V0LmxlZnQgKyBvZmZzZXRYICsgcHJveHkuZGltZW5zaW9uLndpZHRoICsgMTI7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0b3BEaWZmZXJlbmNlID0gKHByb3h5Lm9mZnNldC50b3AgKyB0b29sdGlwSGVpZ2h0KSAtIChwcm94eS5vZmZzZXQudG9wICsgcHJveHkuZGltZW5zaW9uLmhlaWdodCk7XG4gICAgICAgICAgICAgICAgICAgIG15VG9wID0gcHJveHkub2Zmc2V0LnRvcCAtICh0b3BEaWZmZXJlbmNlIC8gMikgLSBvZmZzZXRZO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZSB0b29sdGlwIGdvZXMgb2ZmIGJvdGhzIHNpZGVzIG9mIHRoZSBwYWdlXG4gICAgICAgICAgICAgICAgICAgIGlmKChteUxlZnQgPCAwKSAmJiAoKG15TGVmdE1pcnJvciArIHRvb2x0aXBXaWR0aCkgPiB3aW5kb3dXaWR0aCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBib3JkZXJXaWR0aCA9IHBhcnNlRmxvYXQoc2VsZi4kdG9vbHRpcC5jc3MoJ2JvcmRlci13aWR0aCcpKSAqIDIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3V2lkdGggPSAodG9vbHRpcFdpZHRoICsgbXlMZWZ0KSAtIGJvcmRlcldpZHRoO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcC5jc3MoJ3dpZHRoJywgbmV3V2lkdGggKyAncHgnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdG9vbHRpcEhlaWdodCA9IHNlbGYuJHRvb2x0aXAub3V0ZXJIZWlnaHQoZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbXlMZWZ0ID0gcHJveHkub2Zmc2V0LmxlZnQgLSBvZmZzZXRYIC0gbmV3V2lkdGggLSAxMiAtIGJvcmRlcldpZHRoO1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9wRGlmZmVyZW5jZSA9IChwcm94eS5vZmZzZXQudG9wICsgdG9vbHRpcEhlaWdodCkgLSAocHJveHkub2Zmc2V0LnRvcCArIHByb3h5LmRpbWVuc2lvbi5oZWlnaHQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbXlUb3AgPSBwcm94eS5vZmZzZXQudG9wIC0gKHRvcERpZmZlcmVuY2UgLyAyKSAtIG9mZnNldFk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBpZiBpdCBvbmx5IGdvZXMgb2ZmIG9uZSBzaWRlLCBmbGlwIGl0IHRvIHRoZSBvdGhlciBzaWRlXG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYobXlMZWZ0IDwgMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbXlMZWZ0ID0gcHJveHkub2Zmc2V0LmxlZnQgKyBvZmZzZXRYICsgcHJveHkuZGltZW5zaW9uLndpZHRoICsgMTI7XG4gICAgICAgICAgICAgICAgICAgICAgICBhcnJvd1JlcG9zaXRpb24gPSAnbGVmdCc7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZihwcmFjdGljYWxQb3NpdGlvbiA9PSAncmlnaHQnKSB7XG4gICAgICAgICAgICAgICAgICAgIG15TGVmdCA9IHByb3h5Lm9mZnNldC5sZWZ0ICsgb2Zmc2V0WCArIHByb3h5LmRpbWVuc2lvbi53aWR0aCArIDEyO1xuICAgICAgICAgICAgICAgICAgICBteUxlZnRNaXJyb3IgPSBwcm94eS5vZmZzZXQubGVmdCAtIG9mZnNldFggLSB0b29sdGlwV2lkdGggLSAxMjtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRvcERpZmZlcmVuY2UgPSAocHJveHkub2Zmc2V0LnRvcCArIHRvb2x0aXBIZWlnaHQpIC0gKHByb3h5Lm9mZnNldC50b3AgKyBwcm94eS5kaW1lbnNpb24uaGVpZ2h0KTtcbiAgICAgICAgICAgICAgICAgICAgbXlUb3AgPSBwcm94eS5vZmZzZXQudG9wIC0gKHRvcERpZmZlcmVuY2UgLyAyKSAtIG9mZnNldFk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIHRvb2x0aXAgZ29lcyBvZmYgYm90aHMgc2lkZXMgb2YgdGhlIHBhZ2VcbiAgICAgICAgICAgICAgICAgICAgaWYoKChteUxlZnQgKyB0b29sdGlwV2lkdGgpID4gd2luZG93V2lkdGgpICYmIChteUxlZnRNaXJyb3IgPCAwKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGJvcmRlcldpZHRoID0gcGFyc2VGbG9hdChzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLXdpZHRoJykpICogMixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXdXaWR0aCA9ICh3aW5kb3dXaWR0aCAtIG15TGVmdCkgLSBib3JkZXJXaWR0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuJHRvb2x0aXAuY3NzKCd3aWR0aCcsIG5ld1dpZHRoICsgJ3B4Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXBIZWlnaHQgPSBzZWxmLiR0b29sdGlwLm91dGVySGVpZ2h0KGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcERpZmZlcmVuY2UgPSAocHJveHkub2Zmc2V0LnRvcCArIHRvb2x0aXBIZWlnaHQpIC0gKHByb3h5Lm9mZnNldC50b3AgKyBwcm94eS5kaW1lbnNpb24uaGVpZ2h0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIG15VG9wID0gcHJveHkub2Zmc2V0LnRvcCAtICh0b3BEaWZmZXJlbmNlIC8gMikgLSBvZmZzZXRZO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgaXQgb25seSBnb2VzIG9mZiBvbmUgc2lkZSwgZmxpcCBpdCB0byB0aGUgb3RoZXIgc2lkZVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmKChteUxlZnQgKyB0b29sdGlwV2lkdGgpID4gd2luZG93V2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG15TGVmdCA9IHByb3h5Lm9mZnNldC5sZWZ0IC0gb2Zmc2V0WCAtIHRvb2x0aXBXaWR0aCAtIDEyO1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dSZXBvc2l0aW9uID0gJ3JpZ2h0JztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIGlmIGFycm93IGlzIHNldCB0cnVlLCBzdHlsZSBpdCBhbmQgYXBwZW5kIGl0XG4gICAgICAgICAgICAgICAgaWYgKHNlbGYub3B0aW9ucy5hcnJvdykge1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciBhcnJvd0NsYXNzID0gJ3Rvb2x0aXBzdGVyLWFycm93LScgKyBwcmFjdGljYWxQb3NpdGlvbjtcblxuICAgICAgICAgICAgICAgICAgICAvLyBzZXQgY29sb3Igb2YgdGhlIGFycm93XG4gICAgICAgICAgICAgICAgICAgIGlmKHNlbGYub3B0aW9ucy5hcnJvd0NvbG9yLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnJvd0NvbG9yID0gc2VsZi4kdG9vbHRpcC5jc3MoJ2JhY2tncm91bmQtY29sb3InKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnJvd0NvbG9yID0gc2VsZi5vcHRpb25zLmFycm93Q29sb3I7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGUgdG9vbHRpcCB3YXMgZ29pbmcgb2ZmIHRoZSBwYWdlIGFuZCBoYWQgdG8gcmUtYWRqdXN0LCB3ZSBuZWVkIHRvIHVwZGF0ZSB0aGUgYXJyb3cncyBwb3NpdGlvblxuICAgICAgICAgICAgICAgICAgICBpZiAoIWFycm93UmVwb3NpdGlvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dSZXBvc2l0aW9uID0gJyc7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoYXJyb3dSZXBvc2l0aW9uID09ICdsZWZ0Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dDbGFzcyA9ICd0b29sdGlwc3Rlci1hcnJvdy1yaWdodCc7XG4gICAgICAgICAgICAgICAgICAgICAgICBhcnJvd1JlcG9zaXRpb24gPSAnJztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChhcnJvd1JlcG9zaXRpb24gPT0gJ3JpZ2h0Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dDbGFzcyA9ICd0b29sdGlwc3Rlci1hcnJvdy1sZWZ0JztcbiAgICAgICAgICAgICAgICAgICAgICAgIGFycm93UmVwb3NpdGlvbiA9ICcnO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dSZXBvc2l0aW9uID0gJ2xlZnQ6JysgTWF0aC5yb3VuZChhcnJvd1JlcG9zaXRpb24pICsncHg7JztcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGJ1aWxkaW5nIHRoZSBsb2dpYyB0byBjcmVhdGUgdGhlIGJvcmRlciBhcm91bmQgdGhlIGFycm93IG9mIHRoZSB0b29sdGlwXG4gICAgICAgICAgICAgICAgICAgIGlmICgocHJhY3RpY2FsUG9zaXRpb24gPT0gJ3RvcCcpIHx8IChwcmFjdGljYWxQb3NpdGlvbiA9PSAndG9wLWxlZnQnKSB8fCAocHJhY3RpY2FsUG9zaXRpb24gPT0gJ3RvcC1yaWdodCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9vbHRpcEJvcmRlcldpZHRoID0gcGFyc2VGbG9hdChzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLWJvdHRvbS13aWR0aCcpKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwQm9yZGVyQ29sb3IgPSBzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLWJvdHRvbS1jb2xvcicpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKChwcmFjdGljYWxQb3NpdGlvbiA9PSAnYm90dG9tJykgfHwgKHByYWN0aWNhbFBvc2l0aW9uID09ICdib3R0b20tbGVmdCcpIHx8IChwcmFjdGljYWxQb3NpdGlvbiA9PSAnYm90dG9tLXJpZ2h0JykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0b29sdGlwQm9yZGVyV2lkdGggPSBwYXJzZUZsb2F0KHNlbGYuJHRvb2x0aXAuY3NzKCdib3JkZXItdG9wLXdpZHRoJykpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXBCb3JkZXJDb2xvciA9IHNlbGYuJHRvb2x0aXAuY3NzKCdib3JkZXItdG9wLWNvbG9yJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAocHJhY3RpY2FsUG9zaXRpb24gPT0gJ2xlZnQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9vbHRpcEJvcmRlcldpZHRoID0gcGFyc2VGbG9hdChzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLXJpZ2h0LXdpZHRoJykpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXBCb3JkZXJDb2xvciA9IHNlbGYuJHRvb2x0aXAuY3NzKCdib3JkZXItcmlnaHQtY29sb3InKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChwcmFjdGljYWxQb3NpdGlvbiA9PSAncmlnaHQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9vbHRpcEJvcmRlcldpZHRoID0gcGFyc2VGbG9hdChzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLWxlZnQtd2lkdGgnKSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9vbHRpcEJvcmRlckNvbG9yID0gc2VsZi4kdG9vbHRpcC5jc3MoJ2JvcmRlci1sZWZ0LWNvbG9yJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9vbHRpcEJvcmRlcldpZHRoID0gcGFyc2VGbG9hdChzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLWJvdHRvbS13aWR0aCcpKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwQm9yZGVyQ29sb3IgPSBzZWxmLiR0b29sdGlwLmNzcygnYm9yZGVyLWJvdHRvbS1jb2xvcicpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRvb2x0aXBCb3JkZXJXaWR0aCA+IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXBCb3JkZXJXaWR0aCsrO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGFycm93Qm9yZGVyID0gJyc7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0b29sdGlwQm9yZGVyV2lkdGggIT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcnJvd0JvcmRlclNpemUgPSAnJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnJvd0JvcmRlckNvbG9yID0gJ2JvcmRlci1jb2xvcjogJysgdG9vbHRpcEJvcmRlckNvbG9yICsnOyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXJyb3dDbGFzcy5pbmRleE9mKCdib3R0b20nKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnJvd0JvcmRlclNpemUgPSAnbWFyZ2luLXRvcDogLScrIE1hdGgucm91bmQodG9vbHRpcEJvcmRlcldpZHRoKSArJ3B4Oyc7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChhcnJvd0NsYXNzLmluZGV4T2YoJ3RvcCcpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFycm93Qm9yZGVyU2l6ZSA9ICdtYXJnaW4tYm90dG9tOiAtJysgTWF0aC5yb3VuZCh0b29sdGlwQm9yZGVyV2lkdGgpICsncHg7JztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKGFycm93Q2xhc3MuaW5kZXhPZignbGVmdCcpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFycm93Qm9yZGVyU2l6ZSA9ICdtYXJnaW4tcmlnaHQ6IC0nKyBNYXRoLnJvdW5kKHRvb2x0aXBCb3JkZXJXaWR0aCkgKydweDsnO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoYXJyb3dDbGFzcy5pbmRleE9mKCdyaWdodCcpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFycm93Qm9yZGVyU2l6ZSA9ICdtYXJnaW4tbGVmdDogLScrIE1hdGgucm91bmQodG9vbHRpcEJvcmRlcldpZHRoKSArJ3B4Oyc7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcnJvd0JvcmRlciA9ICc8c3BhbiBjbGFzcz1cInRvb2x0aXBzdGVyLWFycm93LWJvcmRlclwiIHN0eWxlPVwiJysgYXJyb3dCb3JkZXJTaXplICsnICcrIGFycm93Qm9yZGVyQ29sb3IgKyc7XCI+PC9zcGFuPic7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGUgYXJyb3cgYWxyZWFkeSBleGlzdHMsIHJlbW92ZSBhbmQgcmVwbGFjZSBpdFxuICAgICAgICAgICAgICAgICAgICBzZWxmLiR0b29sdGlwLmZpbmQoJy50b29sdGlwc3Rlci1hcnJvdycpLnJlbW92ZSgpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGJ1aWxkIG91dCB0aGUgYXJyb3cgYW5kIGFwcGVuZCBpdFxuICAgICAgICAgICAgICAgICAgICB2YXIgYXJyb3dDb25zdHJ1Y3QgPSAnPGRpdiBjbGFzcz1cIicrIGFycm93Q2xhc3MgKycgdG9vbHRpcHN0ZXItYXJyb3dcIiBzdHlsZT1cIicrIGFycm93UmVwb3NpdGlvbiArJ1wiPicrIGFycm93Qm9yZGVyICsnPHNwYW4gc3R5bGU9XCJib3JkZXItY29sb3I6JysgYXJyb3dDb2xvciArJztcIj48L3NwYW4+PC9kaXY+JztcbiAgICAgICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcC5hcHBlbmQoYXJyb3dDb25zdHJ1Y3QpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIHBvc2l0aW9uIHRoZSB0b29sdGlwXG4gICAgICAgICAgICAgICAgc2VsZi4kdG9vbHRpcC5jc3Moeyd0b3AnOiBNYXRoLnJvdW5kKG15VG9wKSArICdweCcsICdsZWZ0JzogTWF0aC5yb3VuZChteUxlZnQpICsgJ3B4J30pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gc2VsZjtcbiAgICAgICAgfSxcblxuICAgICAgICBlbmFibGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhpcy5lbmFibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRpc2FibGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgLy8gaGlkZSBmaXJzdCwgaW4gY2FzZSB0aGUgdG9vbHRpcCB3b3VsZCBub3QgZGlzYXBwZWFyIG9uIGl0cyBvd24gKGF1dG9DbG9zZSBmYWxzZSlcbiAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xuICAgICAgICAgICAgdGhpcy5lbmFibGVkID0gZmFsc2U7XG4gICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuXG4gICAgICAgICAgICBzZWxmLmhpZGUoKTtcblxuICAgICAgICAgICAgLy8gcmVtb3ZlIHRoZSBpY29uLCBpZiBhbnlcbiAgICAgICAgICAgIGlmIChzZWxmLiRlbFswXSAhPT0gc2VsZi4kZWxQcm94eVswXSkge1xuICAgICAgICAgICAgICAgIHNlbGYuJGVsUHJveHkucmVtb3ZlKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHNlbGYuJGVsXG4gICAgICAgICAgICAgICAgLnJlbW92ZURhdGEoc2VsZi5uYW1lc3BhY2UpXG4gICAgICAgICAgICAgICAgLm9mZignLicrIHNlbGYubmFtZXNwYWNlKTtcblxuICAgICAgICAgICAgdmFyIG5zID0gc2VsZi4kZWwuZGF0YSgndG9vbHRpcHN0ZXItbnMnKTtcblxuICAgICAgICAgICAgLy8gaWYgdGhlcmUgYXJlIG5vIG1vcmUgdG9vbHRpcHMgb24gdGhpcyBlbGVtZW50XG4gICAgICAgICAgICBpZihucy5sZW5ndGggPT09IDEpe1xuXG4gICAgICAgICAgICAgICAgLy8gb3B0aW9uYWwgcmVzdG9yYXRpb24gb2YgYSB0aXRsZSBhdHRyaWJ1dGVcbiAgICAgICAgICAgICAgICB2YXIgdGl0bGUgPSBudWxsO1xuICAgICAgICAgICAgICAgIGlmIChzZWxmLm9wdGlvbnMucmVzdG9yYXRpb24gPT09ICdwcmV2aW91cycpe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IHNlbGYuJGVsLmRhdGEoJ3Rvb2x0aXBzdGVyLWluaXRpYWxUaXRsZScpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmKHNlbGYub3B0aW9ucy5yZXN0b3JhdGlvbiA9PT0gJ2N1cnJlbnQnKXtcblxuICAgICAgICAgICAgICAgICAgICAvLyBvbGQgc2Nob29sIHRlY2huaXF1ZSB0byBzdHJpbmdpZnkgd2hlbiBvdXRlckhUTUwgaXMgbm90IHN1cHBvcnRlZFxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA9XG4gICAgICAgICAgICAgICAgICAgICAgICAodHlwZW9mIHNlbGYuQ29udGVudCA9PT0gJ3N0cmluZycpID9cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLkNvbnRlbnQgOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJzxkaXY+PC9kaXY+JykuYXBwZW5kKHNlbGYuQ29udGVudCkuaHRtbCgpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh0aXRsZSkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLiRlbC5hdHRyKCd0aXRsZScsIHRpdGxlKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvLyBmaW5hbCBjbGVhbmluZ1xuICAgICAgICAgICAgICAgIHNlbGYuJGVsXG4gICAgICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygndG9vbHRpcHN0ZXJlZCcpXG4gICAgICAgICAgICAgICAgICAgIC5yZW1vdmVEYXRhKCd0b29sdGlwc3Rlci1ucycpXG4gICAgICAgICAgICAgICAgICAgIC5yZW1vdmVEYXRhKCd0b29sdGlwc3Rlci1pbml0aWFsVGl0bGUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIHJlbW92ZSB0aGUgaW5zdGFuY2UgbmFtZXNwYWNlIGZyb20gdGhlIGxpc3Qgb2YgbmFtZXNwYWNlcyBvZiB0b29sdGlwcyBwcmVzZW50IG9uIHRoZSBlbGVtZW50XG4gICAgICAgICAgICAgICAgbnMgPSAkLmdyZXAobnMsIGZ1bmN0aW9uKGVsLCBpKXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGVsICE9PSBzZWxmLm5hbWVzcGFjZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBzZWxmLiRlbC5kYXRhKCd0b29sdGlwc3Rlci1ucycsIG5zKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHNlbGY7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZWxlbWVudEljb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICh0aGlzLiRlbFswXSAhPT0gdGhpcy4kZWxQcm94eVswXSkgPyB0aGlzLiRlbFByb3h5WzBdIDogdW5kZWZpbmVkO1xuICAgICAgICB9LFxuXG4gICAgICAgIGVsZW1lbnRUb29sdGlwOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLiR0b29sdGlwID8gdGhpcy4kdG9vbHRpcFswXSA6IHVuZGVmaW5lZDtcbiAgICAgICAgfSxcblxuICAgICAgICAvLyBwdWJsaWMgbWV0aG9kcyBidXQgZm9yIGludGVybmFsIHVzZSBvbmx5XG4gICAgICAgIC8vIGdldHRlciBpZiB2YWwgaXMgb21taXR0ZWQsIHNldHRlciBvdGhlcndpc2VcbiAgICAgICAgb3B0aW9uOiBmdW5jdGlvbihvLCB2YWwpIHtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdmFsID09ICd1bmRlZmluZWQnKSByZXR1cm4gdGhpcy5vcHRpb25zW29dO1xuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zW29dID0gdmFsO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBzdGF0dXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuU3RhdHVzO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgICQuZm5bcGx1Z2luTmFtZV0gPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgLy8gZm9yIHVzaW5nIGluIGNsb3N1cmVzXG4gICAgICAgIHZhciBhcmdzID0gYXJndW1lbnRzO1xuXG4gICAgICAgIC8vIGlmIHdlIGFyZSBub3QgaW4gdGhlIGNvbnRleHQgb2YgalF1ZXJ5IHdyYXBwZWQgSFRNTCBlbGVtZW50KHMpIDpcbiAgICAgICAgLy8gdGhpcyBoYXBwZW5zIHdoZW4gY2FsbGluZyBzdGF0aWMgbWV0aG9kcyBpbiB0aGUgZm9ybSAkLmZuLnRvb2x0aXBzdGVyKCdtZXRob2ROYW1lJyksIG9yIHdoZW4gY2FsbGluZyAkKHNlbCkudG9vbHRpcHN0ZXIoJ21ldGhvZE5hbWUgb3Igb3B0aW9ucycpIHdoZXJlICQoc2VsKSBkb2VzIG5vdCBtYXRjaCBhbnl0aGluZ1xuICAgICAgICBpZiAodGhpcy5sZW5ndGggPT09IDApIHtcblxuICAgICAgICAgICAgLy8gaWYgdGhlIGZpcnN0IGFyZ3VtZW50IGlzIGEgbWV0aG9kIG5hbWVcbiAgICAgICAgICAgIGlmICh0eXBlb2YgYXJnc1swXSA9PT0gJ3N0cmluZycpIHtcblxuICAgICAgICAgICAgICAgIHZhciBtZXRob2RJc1N0YXRpYyA9IHRydWU7XG5cbiAgICAgICAgICAgICAgICAvLyBsaXN0IHN0YXRpYyBtZXRob2RzIGhlcmUgKHVzYWJsZSBieSBjYWxsaW5nICQuZm4udG9vbHRpcHN0ZXIoJ21ldGhvZE5hbWUnKTspXG4gICAgICAgICAgICAgICAgc3dpdGNoIChhcmdzWzBdKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnc2V0RGVmYXVsdHMnOlxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2hhbmdlIGRlZmF1bHQgb3B0aW9ucyBmb3IgYWxsIGZ1dHVyZSBpbnN0YW5jZXNcbiAgICAgICAgICAgICAgICAgICAgICAgICQuZXh0ZW5kKGRlZmF1bHRzLCBhcmdzWzFdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRob2RJc1N0YXRpYyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gJC5mbi50b29sdGlwc3RlcignbWV0aG9kTmFtZScpIGNhbGxzIHdpbGwgcmV0dXJuIHRydWVcbiAgICAgICAgICAgICAgICBpZiAobWV0aG9kSXNTdGF0aWMpIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIC8vICQoc2VsKS50b29sdGlwc3RlcignbWV0aG9kTmFtZScpIGNhbGxzIHdpbGwgcmV0dXJuIHRoZSBsaXN0IG9mIG9iamVjdHMgZXZlbnQgdGhvdWdoIGl0J3MgZW1wdHkgYmVjYXVzZSBjaGFpbmluZyBzaG91bGQgd29yayBvbiBlbXB0eSBsaXN0c1xuICAgICAgICAgICAgICAgIGVsc2UgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyB0aGUgZmlyc3QgYXJndW1lbnQgaXMgdW5kZWZpbmVkIG9yIGFuIG9iamVjdCBvZiBvcHRpb25zIDogd2UgYXJlIGluaXRhbGl6aW5nIGJ1dCB0aGVyZSBpcyBubyBlbGVtZW50IG1hdGNoZWQgYnkgc2VsZWN0b3JcbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIHN0aWxsIGNoYWluYWJsZSA6IHNhbWUgYXMgYWJvdmVcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvLyB0aGlzIGhhcHBlbnMgd2hlbiBjYWxsaW5nICQoc2VsKS50b29sdGlwc3RlcignbWV0aG9kTmFtZSBvciBvcHRpb25zJykgd2hlcmUgJChzZWwpIG1hdGNoZXMgb25lIG9yIG1vcmUgZWxlbWVudHNcbiAgICAgICAgZWxzZSB7XG5cbiAgICAgICAgICAgIC8vIG1ldGhvZCBjYWxsc1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBhcmdzWzBdID09PSAnc3RyaW5nJykge1xuXG4gICAgICAgICAgICAgICAgdmFyIHYgPSAnIyokfiYnO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIHJldHJpZXZlIHRoZSBuYW1lcGFjZXMgb2YgdGhlIHRvb2x0aXAocykgdGhhdCBleGlzdCBvbiB0aGF0IGVsZW1lbnQuIFdlIHdpbGwgaW50ZXJhY3Qgd2l0aCB0aGUgZmlyc3QgdG9vbHRpcCBvbmx5LlxuICAgICAgICAgICAgICAgICAgICB2YXIgbnMgPSAkKHRoaXMpLmRhdGEoJ3Rvb2x0aXBzdGVyLW5zJyksXG4gICAgICAgICAgICAgICAgICAgIC8vIHNlbGYgcmVwcmVzZW50cyB0aGUgaW5zdGFuY2Ugb2YgdGhlIGZpcnN0IHRvb2x0aXBzdGVyIHBsdWdpbiBhc3NvY2lhdGVkIHRvIHRoZSBjdXJyZW50IEhUTUwgb2JqZWN0IG9mIHRoZSBsb29wXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmID0gbnMgPyAkKHRoaXMpLmRhdGEobnNbMF0pIDogbnVsbDtcblxuICAgICAgICAgICAgICAgICAgICAvLyBpZiB0aGUgY3VycmVudCBlbGVtZW50IGhvbGRzIGEgdG9vbHRpcHN0ZXIgaW5zdGFuY2VcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBzZWxmW2FyZ3NbMF1dID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbm90ZSA6IGFyZ3NbMV0gYW5kIGFyZ3NbMl0gbWF5IG5vdCBiZSBkZWZpbmVkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlc3AgPSBzZWxmW2FyZ3NbMF1dKGFyZ3NbMV0sIGFyZ3NbMl0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdVbmtub3duIG1ldGhvZCAudG9vbHRpcHN0ZXIoXCInICsgYXJnc1swXSArICdcIiknKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlIGZ1bmN0aW9uIHJldHVybmVkIGFueXRoaW5nIG90aGVyIHRoYW4gdGhlIGluc3RhbmNlIGl0c2VsZiAod2hpY2ggaW1wbGllcyBjaGFpbmluZylcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwICE9PSBzZWxmKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2ID0gcmVzcDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZXR1cm4gZmFsc2UgdG8gc3RvcCAuZWFjaCBpdGVyYXRpb24gb24gdGhlIGZpcnN0IGVsZW1lbnQgbWF0Y2hlZCBieSB0aGUgc2VsZWN0b3JcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBjYWxsZWQgVG9vbHRpcHN0ZXJcXCdzIFwiJyArIGFyZ3NbMF0gKyAnXCIgbWV0aG9kIG9uIGFuIHVuaW5pdGlhbGl6ZWQgZWxlbWVudCcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gKHYgIT09ICcjKiR+JicpID8gdiA6IHRoaXM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBmaXJzdCBhcmd1bWVudCBpcyB1bmRlZmluZWQgb3IgYW4gb2JqZWN0IDogdGhlIHRvb2x0aXAgaXMgaW5pdGlhbGl6aW5nXG4gICAgICAgICAgICBlbHNlIHtcblxuICAgICAgICAgICAgICAgIHZhciBpbnN0YW5jZXMgPSBbXSxcbiAgICAgICAgICAgICAgICAvLyBpcyB0aGVyZSBhIGRlZmluZWQgdmFsdWUgZm9yIHRoZSBtdWx0aXBsZSBvcHRpb24gaW4gdGhlIG9wdGlvbnMgb2JqZWN0ID9cbiAgICAgICAgICAgICAgICAgICAgbXVsdGlwbGVJc1NldCA9IGFyZ3NbMF0gJiYgdHlwZW9mIGFyZ3NbMF0ubXVsdGlwbGUgIT09ICd1bmRlZmluZWQnLFxuICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBtdWx0aXBsZSBvcHRpb24gaXMgc2V0IHRvIHRydWUsIG9yIGlmIGl0J3Mgbm90IGRlZmluZWQgYnV0IHNldCB0byB0cnVlIGluIHRoZSBkZWZhdWx0c1xuICAgICAgICAgICAgICAgICAgICBtdWx0aXBsZSA9IChtdWx0aXBsZUlzU2V0ICYmIGFyZ3NbMF0ubXVsdGlwbGUpIHx8ICghbXVsdGlwbGVJc1NldCAmJiBkZWZhdWx0cy5tdWx0aXBsZSksXG4gICAgICAgICAgICAgICAgLy8gc2FtZSBmb3IgZGVidWdcbiAgICAgICAgICAgICAgICAgICAgZGVidWdJc1NldCA9IGFyZ3NbMF0gJiYgdHlwZW9mIGFyZ3NbMF0uZGVidWcgIT09ICd1bmRlZmluZWQnLFxuICAgICAgICAgICAgICAgICAgICBkZWJ1ZyA9IChkZWJ1Z0lzU2V0ICYmIGFyZ3NbMF0uZGVidWcpIHx8ICghZGVidWdJc1NldCAmJiBkZWZhdWx0cy5kZWJ1Zyk7XG5cbiAgICAgICAgICAgICAgICAvLyBpbml0aWFsaXplIGEgdG9vbHRpcHN0ZXIgaW5zdGFuY2UgZm9yIGVhY2ggZWxlbWVudCBpZiBpdCBkb2Vzbid0IGFscmVhZHkgaGF2ZSBvbmUgb3IgaWYgdGhlIG11bHRpcGxlIG9wdGlvbiBpcyBzZXQsIGFuZCBhdHRhY2ggdGhlIG9iamVjdCB0byBpdFxuICAgICAgICAgICAgICAgIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGdvID0gZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgICAgICBucyA9ICQodGhpcykuZGF0YSgndG9vbHRpcHN0ZXItbnMnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGluc3RhbmNlID0gbnVsbDtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoIW5zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBnbyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAobXVsdGlwbGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGdvID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChkZWJ1Zykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ1Rvb2x0aXBzdGVyOiBvbmUgb3IgbW9yZSB0b29sdGlwcyBhcmUgYWxyZWFkeSBhdHRhY2hlZCB0byB0aGlzIGVsZW1lbnQ6IGlnbm9yaW5nLiBVc2UgdGhlIFwibXVsdGlwbGVcIiBvcHRpb24gdG8gYXR0YWNoIG1vcmUgdG9vbHRpcHMuJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAoZ28pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGluc3RhbmNlID0gbmV3IFBsdWdpbih0aGlzLCBhcmdzWzBdKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gc2F2ZSB0aGUgcmVmZXJlbmNlIG9mIHRoZSBuZXcgaW5zdGFuY2VcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghbnMpIG5zID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICBucy5wdXNoKGluc3RhbmNlLm5hbWVzcGFjZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmRhdGEoJ3Rvb2x0aXBzdGVyLW5zJywgbnMpXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHNhdmUgdGhlIGluc3RhbmNlIGl0c2VsZlxuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5kYXRhKGluc3RhbmNlLm5hbWVzcGFjZSwgaW5zdGFuY2UpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaW5zdGFuY2VzLnB1c2goaW5zdGFuY2UpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgaWYgKG11bHRpcGxlKSByZXR1cm4gaW5zdGFuY2VzO1xuICAgICAgICAgICAgICAgIGVsc2UgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgLy8gcXVpY2sgJiBkaXJ0eSBjb21wYXJlIGZ1bmN0aW9uIChub3QgYmlqZWN0aXZlIG5vciBtdWx0aWRpbWVuc2lvbmFsKVxuICAgIGZ1bmN0aW9uIGFyZUVxdWFsKGEsYikge1xuICAgICAgICB2YXIgc2FtZSA9IHRydWU7XG4gICAgICAgICQuZWFjaChhLCBmdW5jdGlvbihpLCBlbCl7XG4gICAgICAgICAgICBpZih0eXBlb2YgYltpXSA9PT0gJ3VuZGVmaW5lZCcgfHwgYVtpXSAhPT0gYltpXSl7XG4gICAgICAgICAgICAgICAgc2FtZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBzYW1lO1xuICAgIH1cblxuICAgIC8vIGRldGVjdCBpZiB0aGlzIGRldmljZSBjYW4gdHJpZ2dlciB0b3VjaCBldmVudHNcbiAgICB2YXIgZGV2aWNlSGFzVG91Y2hDYXBhYmlsaXR5ID0gISEoJ29udG91Y2hzdGFydCcgaW4gd2luZG93KTtcblxuICAgIC8vIHdlJ2xsIGFzc3VtZSB0aGUgZGV2aWNlIGhhcyBubyBtb3VzZSB1bnRpbCB3ZSBkZXRlY3QgYW55IG1vdXNlIG1vdmVtZW50XG4gICAgdmFyIGRldmljZUhhc01vdXNlID0gZmFsc2U7XG4gICAgJCgnYm9keScpLm9uZSgnbW91c2Vtb3ZlJywgZnVuY3Rpb24oKSB7XG4gICAgICAgIGRldmljZUhhc01vdXNlID0gdHJ1ZTtcbiAgICB9KTtcblxuICAgIGZ1bmN0aW9uIGRldmljZUlzUHVyZVRvdWNoKCkge1xuICAgICAgICByZXR1cm4gKCFkZXZpY2VIYXNNb3VzZSAmJiBkZXZpY2VIYXNUb3VjaENhcGFiaWxpdHkpO1xuICAgIH1cblxuICAgIC8vIGRldGVjdGluZyBzdXBwb3J0IGZvciBDU1MgdHJhbnNpdGlvbnNcbiAgICBmdW5jdGlvbiBzdXBwb3J0c1RyYW5zaXRpb25zKCkge1xuICAgICAgICB2YXIgYiA9IGRvY3VtZW50LmJvZHkgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LFxuICAgICAgICAgICAgcyA9IGIuc3R5bGUsXG4gICAgICAgICAgICBwID0gJ3RyYW5zaXRpb24nO1xuXG4gICAgICAgIGlmKHR5cGVvZiBzW3BdID09ICdzdHJpbmcnKSB7cmV0dXJuIHRydWU7IH1cblxuICAgICAgICB2ID0gWydNb3onLCAnV2Via2l0JywgJ0todG1sJywgJ08nLCAnbXMnXSxcbiAgICAgICAgICAgIHAgPSBwLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcC5zdWJzdHIoMSk7XG4gICAgICAgIGZvcih2YXIgaT0wOyBpPHYubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmKHR5cGVvZiBzW3ZbaV0gKyBwXSA9PSAnc3RyaW5nJykgeyByZXR1cm4gdHJ1ZTsgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7IiwiIiwicHAubW9kdWxlKCd0YWJzJywgW10pLmRpcmVjdGl2ZSgndGFicycsIGZ1bmN0aW9uKCRsb2NhdGlvbilcbntcbiAgICByZXR1cm4ge1xuICAgICAgICB0ZW1wbGF0ZVVybDogcHAudmlld1BhdGgoJ3RhYnMnKSxcbiAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgdHJhbnNjbHVkZTogdHJ1ZSxcbiAgICAgICAgc2NvcGU6XG4gICAgICAgIHtcbiAgICAgICAgICAgIHBhcmFtczogJz0nXG4gICAgICAgIH0sXG5cbiAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbGVtZW50LCBhdHRycylcbiAgICAgICAge1xuICAgICAgICAgICAgc2NvcGUudXJsID0gJGxvY2F0aW9uLnVybCgpO1xuXG4gICAgICAgICAgICBzY29wZS5yb3V0ZSA9IGZ1bmN0aW9uIChyb3V0ZSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcHAucm91dGUocm91dGUpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgc2NvcGUubG9nID0gZnVuY3Rpb24gKHBhcmFtKSBcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwYXJhbSk7XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgfTtcbn0pLmRpcmVjdGl2ZSgndGFiJywgZnVuY3Rpb24oKVxue1xuICAgIHJldHVybiB7XG4gICAgICAgIHRlbXBsYXRlVXJsOiBwcC52aWV3UGF0aCgndGFiJyksXG4gICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgIHRyYW5zY2x1ZGU6IHRydWUsXG4gICAgICAgIHNjb3BlOlxuICAgICAgICB7XG4gICAgICAgICAgICBwYXJhbXM6ICc9J1xuICAgICAgICB9LFxuXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCwgYXR0cnMpXG4gICAgICAgIHtcblxuICAgICAgICB9XG4gICAgfTtcbn0pOyIsInBwLm1vZHVsZSgndGJsJywgWydidXR0b24nLCAndG9vbHRpcCcsICdwcC1zZWxlY3QnLCAndCddKS5kaXJlY3RpdmUoJ3RibCcsIGZ1bmN0aW9uKHQpXG57XG4gICAgcmV0dXJuIHtcbiAgICAgICAgdGVtcGxhdGVVcmw6IHBwLnZpZXdQYXRoKCd0YmwnKSxcbiAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgc2NvcGU6XG4gICAgICAgIHtcbiAgICAgICAgICAgIHBhcmFtczogJz0nXG4gICAgICAgIH0sXG4gICAgICAgIFxuICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKVxuICAgICAgICB7XG4gICAgICAgICAgICBzY29wZS5yZXZlcnNlID0gZmFsc2U7XG4gICAgICAgICAgICBzY29wZS5vcmRlckJ5ID0gc2NvcGUucGFyYW1zICE9IHVuZGVmaW5lZCA/IHNjb3BlLnBhcmFtcy5vcmRlckJ5IDogJyc7XG5cbiAgICAgICAgICAgIHNjb3BlLnQgPSBmdW5jdGlvbiAoc3RyaW5nKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHJldHVybiB0LnQoJ3Byb3Bvc2FscycsIHN0cmluZyk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZiAoc2NvcGUucGFyYW1zLnNvcnRpbmcgPT0gJ2FqYXgnKSB7XG4gICAgICAgICAgICAgICAgc2NvcGUucmV2ZXJzZSA9IHNjb3BlLnBhcmFtcy5vcmRlciAhPSAnQVNDJztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc2NvcGUuc29ydCA9IGZ1bmN0aW9uIChwYXJhbSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzY29wZS5vcmRlckJ5ID0gcGFyYW07XG5cbiAgICAgICAgICAgICAgICBpZiAocGFyYW0gPT0gc2NvcGUub3JkZXJCeSAmJiBzY29wZS5wYXJhbXMuc29ydGluZyAhPSAnYWpheCcpIHtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUucmV2ZXJzZSA9ICFzY29wZS5yZXZlcnNlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmIChzY29wZS5wYXJhbXMuc29ydGluZyA9PSAnYWpheCcpIHtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUucGFyYW1zLnNvcnQocGFyYW0sIHNjb3BlLnJldmVyc2UpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLnNvcnRDbGllbnQocGFyYW0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHNjb3BlLnNvcnRDbGllbnQgPSBmdW5jdGlvbiAocGFyYW0pXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIGFyciA9IHNjb3BlLnBhcmFtcy5yb3dzO1xuXG4gICAgICAgICAgICAgICAgYXJyID0gYXJyLnNvcnQoZnVuY3Rpb24gKHgsIHkpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICB2YXIgeE9iaiA9IHhbcGFyYW1dLFxuICAgICAgICAgICAgICAgICAgICAgICAgeU9iaiA9IHlbcGFyYW1dO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICh4T2JqLnZhbHVlID4geU9iai52YWx1ZSkgcmV0dXJuIDE7XG4gICAgICAgICAgICAgICAgICAgIGlmICh4T2JqLnZhbHVlIDwgeU9iai52YWx1ZSkgcmV0dXJuIC0xO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgaWYgKHNjb3BlLnJldmVyc2UpIGFyci5yZXZlcnNlKCk7XG5cbiAgICAgICAgICAgICAgICBzY29wZS5wYXJhbXMucm93cyA9IGFycjtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHNjb3BlLmZpbHRlciA9IGZ1bmN0aW9uICgpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIGZpbHRlciA9IFtdO1xuXG4gICAgICAgICAgICAgICAgZm9yIChjb2x1bW4gaW4gc2NvcGUucGFyYW1zLmNvbHVtbnMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBzY29wZS5wYXJhbXMuY29sdW1uc1tjb2x1bW5dLmZpbHRlciA9PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucGFyYW1zLmNvbHVtbnNbY29sdW1uXS5maWx0ZXIgPSBzY29wZS5wYXJhbXMuY29sdW1uc1tjb2x1bW5dLmZpbHRlci5pZCAhPSAnPycgPyBzY29wZS5wYXJhbXMuY29sdW1uc1tjb2x1bW5dLmZpbHRlci5pZCA6ICcnO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyW2NvbHVtbl0gPVxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBzY29wZS5wYXJhbXMuY29sdW1uc1tjb2x1bW5dLm5hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXI6IHNjb3BlLnBhcmFtcy5jb2x1bW5zW2NvbHVtbl0uZmlsdGVyXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgc2NvcGUucGFyYW1zLmZpbHRlcihmaWx0ZXIpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgc2NvcGUuZmlsdGVySW5wdXQgPSBmdW5jdGlvbiAoZSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAoZS53aGljaCA9PSAxMykge1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5maWx0ZXIoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBzY29wZS5wYWdlID0gZnVuY3Rpb24gKHBhZ2UpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc2NvcGUucGFyYW1zLnBhZ2UocGFnZSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBzY29wZS53YWl0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoc2NvcGUucGFyYW1zLnJvd3MgPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS53YWl0KCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2NvcGUucGFyYW1zLnJldmVyc2UgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucmV2ZXJzZSA9ICFzY29wZS5wYXJhbXMucmV2ZXJzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuc29ydChzY29wZS5vcmRlckJ5KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaWYgKHNjb3BlLnBhcmFtcy5zb3J0aW5nICE9ICdhamF4Jykge1xuICAgICAgICAgICAgICAgIHNjb3BlLndhaXQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG59KTsiLCIiLCIiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
