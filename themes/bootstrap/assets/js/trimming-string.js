$(window).load(function () {
    trimmingString();
});

function trimmingString() {
    $('.trim-clamp:visible').each(function () {
        var maxLine = $(this).data('max-line');
        var clamping = $(this).data('clamping');
        var opts = {
            clamp: maxLine,
            useNativeClamp: false
            //animate: 100
        };
        if (clamping != 1) {
            if (clamping == 2) { // условие для обрезания строки без переноса слов по слогам
                $(this).css('display', 'block');
                $clamp(this, opts);
            } else {
                $(this).css('display', 'block').hyphenate();
                $clamp(this, opts);
            }
        }
        $(this).data('clamping', 1);
    });
}
