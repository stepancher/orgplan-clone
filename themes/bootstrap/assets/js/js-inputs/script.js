$(function () {
	var $checkboxes = $('label.checkbox > input');
	$checkboxes.each(function () {
		jsCheckboxes($(this))
	});

	$(document).delegate('label.checkbox > input', 'click', function () {
		jsCheckboxes($(this))
	});

	$(document).delegate('label.checkbox > input', 'focus', function () {
		$(this).parent().css('text-decoration', 'underline');
	});

	$(document).delegate('label.checkbox > input', 'focusout', function () {
		$(this).parent().css('text-decoration', 'none');
	});

	$('[data-file-input-target]').click(function () {
		var target = $(this).data('file-input-target');
		$('[data-file-input=' + target + ']').trigger('click')
	});

});

function jsCheckboxes($elm) {
	if ($elm.attr('disabled') == 'disabled') {
		$elm.parent().addClass('disabled');
	}
	if ($elm.is(':checked')) {
		$elm.parent().addClass('checked');
	} else {
		$elm.parent().removeClass('checked');
	}
}
