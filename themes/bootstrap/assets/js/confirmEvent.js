function ConfirmEvent(params){
    return ({
        selector: '',
        confirmText: '',
        status: true,
        init: function(params){
            var me = this;
            if('selector' in params && 'confirmText' in params) {
                me.selector = params.selector;
                me.confirmText = params.confirmText;
                me.status = true;
            } else {
                me.status = false;
            }
        },
        run: function(params){
            var me = this;
            me.init(params);
            if(me.status) {
                $(me.selector).on('click', function(e){
                    if (confirm(me.confirmText)){
                        return true;
                    } else {
                        e.preventDefault();
                        return false;
                    }
                });
            }
        }
    }).run(params);
};
