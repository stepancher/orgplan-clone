$(function(){
    historyLoader();
});

function historyLoader(){
    var href = window.location.href;
    $(window).on("popstate", function(e) {
        if(window.location.href != href) {
            window.location.reload();
        }
    });
}

