hideCopyright();
$(window).on('resize', hideCopyright);

function hideCopyright() {
    var leftForm = $('.left-form');
    var leftFormCopy = $('.left-form__copyright');
    if (leftForm.height() < 570) {
        leftFormCopy.hide();
    } else {
        leftFormCopy.show();
    }
    $('.minimize-side-bar .left-form__copyright').hide();
}

function afterChangeStatusSideBar() {
    $.get('/ru/menu/default/setCollapsed/', {'status': $('body').hasClass('minimize-side-bar')});
    hideCopyright();
}

var minimized = false;

function menuResizeWidth(width) {
    var $body = $('body'), minWidth = 1232;
    if (!minimized) {
        if (width < minWidth && !$body.hasClass('minimize-side-bar')) {
            $body.addClass('minimize-side-bar');
        } else if (width >= minWidth && $body.hasClass('minimize-side-bar')) {
            $body.removeClass('minimize-side-bar');
        }
    }
    afterChangeStatusSideBar();
}
var minHeight = 50;

function menuResizeHeight(height) {
    if (minHeight > height) {
        height = minHeight;
    }
    $('.nav-list__side-left').height(height);
}

$(function () {

    $('.js-nav-list--minimize-button').click(function () {
        var $body = $('body');
        $body.toggleClass('minimize-side-bar');
        minimized = $body.hasClass('minimize-side-bar');
        afterChangeStatusSideBar();
    });

    $('.nav-list__side-left > li').each(function () {
        minHeight += parseInt($(this).height());
    });

    var resize;

    function resizedw() {
        menuResizeWidth(window.innerWidth);
        menuResizeHeight(Math.max(document.documentElement.offsetHeight, document.documentElement.clientHeight));
    }

    window.onresize = function () {
        clearTimeout(resize);
        resize = setTimeout(function () {
            resizedw();
        }, 200);
    };

    hideCopyright();
    menuResizeHeight(Math.max(document.documentElement.offsetHeight, document.documentElement.clientHeight));

});
$(function () {
    $(window).scroll(function () {
        menuResizeHeight(Math.max(document.documentElement.offsetHeight, document.documentElement.clientHeight))
    })
});

