$(function () {
	$('.js-clear-field').click(function () {
		var $elm = $(this);
		var $input = $elm.parent().find('input[type="text"]');
		$input.val('');
	});
});
