$(function () {
	var eventStart = $('input[name="Event[startDate]"]');
	var eventEnd = $('input[name="Event[endDate]"]');

	function calculateDuration() {
		var startDate = eventStart.val();
		var endDate = eventEnd.val();
		startDate = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		endDate = new Date(endDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		startDate = startDate.getTime();
		endDate = endDate.getTime();
		var ONE_DAY = 1000 * 60 * 60 * 24;
		var difference_ms = Math.abs(startDate - endDate);
		$('#Event_duration').val(Math.round(difference_ms / ONE_DAY));
	}

	function calculateEndDate(){
		var duration = $('#Event_duration').val();
		var ONE_DAY = 1000 * 60 * 60 * 24;
		var startDate = eventStart.val();
		startDate = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		startDate = startDate.getTime();
		var endDate = (startDate + (duration * ONE_DAY));
		var jEndDate = new Date();
		jEndDate.setTime(endDate);
		var curr_date = jEndDate.getDate();
		var curr_month = jEndDate.getMonth() + 1;
		var curr_year = jEndDate.getFullYear();
		var curr_hours = jEndDate.getHours();
		var curr_minutes = jEndDate.getMinutes();
		var curr_seconds = jEndDate.getSeconds();
		if (curr_date < 10) {
			curr_date = '0' + curr_date;
		}
		if (curr_month < 10) {
			curr_month = '0' + curr_month;
		}
		if (curr_hours < 10) {
			curr_hours = '0' + curr_hours;
		}
		if (curr_minutes < 10) {
			curr_minutes = '0' + curr_minutes;
		}
		if (curr_seconds < 10) {
			curr_seconds = '0' + curr_seconds;
		}
		endDate = (curr_date + "." + curr_month + "." + curr_year + ' ' + curr_hours + ':' + curr_minutes + ':' + curr_seconds);
		eventEnd.val(endDate);
	}

	$('div.datepicker tbody').click(function () {
		if (eventStart.val() && eventEnd.val()) {
			calculateDuration();
		}
		if (eventStart.val() && $('#Event_duration').val()) {
			calculateEndDate()
		}

	});

	$('.date-field input').bind('keyup', function () {
		if (eventStart.val() && eventEnd.val()) {
			calculateDuration();
		}
	});
	$('#Event_duration').bind('keyup', function () {
		if (eventStart.val() && $('#Event_duration').val()) {
			calculateEndDate()
		}
	})
});