function Protoplan()
{
    this.elements = [];
}

/**
 * @class Protoplan.Block
 * @param id
 * @param selector
 * @constructor
 */
Protoplan.Block = function (id, selector)
{
    this.id       = typeof id       == 'undefined' ? 0        : id;
    this.selector = typeof selector == 'undefined' ? '#' + id : selector;

    this.capitalizeFirstLetter = function (string)
    {
        return string[0].toUpperCase() + string.slice(1);
    };

    this.merge = function (obj1, obj2)
    {
        var obj3 = {};
        for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
        for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
        return obj3;
    };

    this.clone = function (obj)
    {
        var clone = {};

        for (var key in obj) {
            clone[key] = obj[key];
        }

        return clone;
    };
};

var protoplan = new Protoplan();