pp.module('t', []).factory('t', function($http) {
    return {
        t: function(dictionary, word) {
            var result = word;
            var url = '/static/frontend/translates/' + pp.lang + '/' + dictionary + '.json?v=' + (new Date()).getTime();

            if (pp.dictionaries[dictionary] == undefined) {
                $.ajax({
                    url: url,
                    success: function(response)
                    {
                        pp.dictionaries[dictionary] = response;
                    },
                    async: false
                });
            }

            if (pp.dictionaries[dictionary][word] != undefined) {
                result = pp.dictionaries[dictionary][word];
            }

            return result;
        }
    };
});