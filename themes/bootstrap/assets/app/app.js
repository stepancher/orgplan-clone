var pp = {
    lang: null,
    dictionaries: {},
    hashPrefix: '!',
    
    path: function (route, params)
    {
        var result = '/' + pp.lang + '/' + route;

        if (params != undefined) {
            result = result + '?';
        }

        for(var param in params) {
            result = result + param + '=' + params[param] + '&';

            if (params[param + 1] != undefined) {
                result = result + '&';
            }
        }
        
        return result;
    },
    
    route: function (route) 
    {
        return '#!' + route;
    },
    
    module: function (name, requires, configFn) 
    {
        var result = {};

        result.factory = function () {return result};
        result.config = function () {return result};
        result.controller = function () {return result};
        result.directive = function () {return result};

        if (window.angular != undefined) {
            result = angular.module(name, requires, configFn);
        }

        return result;
    },
    
    ucfirst: function (string)
    {
        return string[0].toUpperCase() + string.slice(1);
    },
    
    viewPath: function (template) 
    {
        return '/static/templates/' + template + '.html'
    },

    getLang: function ()
    {
        var result = pp.lang;

        if (result == null) {
            result = $('.pp-lang').val();
        }

        if (result == undefined) {
            result = location.pathname.split('/')[1];
        }

        return result;
    },

    setLang: function (lang)
    {
        pp.lang = lang;
    }
};

pp.setLang(pp.getLang());

pp.module('workspace', ['t', 'proposal', 'tbl']);
pp.module('organizer', ['ngRoute', 't', 'proposal', 'tbl', 'tabs']);