pp.module('organizer').config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider)
    {
        $locationProvider.hashPrefix(pp.hashPrefix);
        // $locationProvider.html5Mode(true).hashPrefix(pp.hashPrefix);

        $routeProvider.
        when('/incoming', {
            template: '<tabs params="tabs"></tabs><tbl params="incoming"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/incoming/:sort/:order/:name/:contractor/:date/:page', {
            template: '<tabs params="tabs"></tabs><tbl params="incoming"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/accepted', {
            template: '<tabs params="tabs"></tabs><tbl params="accepted"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/accepted/:sort/:order/:name/:date/:contractor/:payment/:documents/:page', {
            template: '<tabs params="tabs"></tabs><tbl params="accepted"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/rejected', {
            template: '<tabs params="tabs"></tabs><tbl params="rejected"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        when('/rejected/:sort/:order/:name/:contractor/:date/:comment/:page', {
            template: '<tabs params="tabs"></tabs><tbl params="rejected"></tbl>',
            reloadOnSearch: false,
            controller: 'OrganizerProposalCtrl'
        }).
        otherwise('/incoming');
    }
]);

pp.module('organizer').controller('OrganizerProposalCtrl', function ($scope, $http, $sce, t, proposal, $routeParams)
{
    $scope.LIMIT = 20;

    $scope.tabs = [
        {
            link: '/incoming',
            text: t.t('proposals', 'INCOMING'),
            counter: 0
        },
        {
            link: '/accepted',
            text: t.t('proposals', 'ACCEPTED')
        },
        {
            link: '/rejected',
            text: t.t('proposals', 'REJECTED')
        }
    ];

    $scope.getRouteParams = function (params)
    {
        var result = {};

        for (param in params) {
            result[param] = params[param].substr(1);
        }

        return result;
    };

    $scope.getSort = function ()
    {
        var result = $scope.getRouteParams($routeParams)['sort'];

        return result != undefined ? result : 'date';
    };

    $scope.getOrder = function ()
    {
        var result = $scope.getRouteParams($routeParams)['order'];

        return result != undefined ? result : 'DESC';
    };

    $scope.getContractor = function ()
    {
        var result = $scope.getRouteParams($routeParams)['contractor'];

        return result != undefined ? result : '';
    };

    $scope.getName = function ()
    {
        var result = $scope.getRouteParams($routeParams)['name'];

        return result != undefined ? result : '';
    };

    $scope.getDate = function ()
    {
        var result = $scope.getRouteParams($routeParams)['date'];

        return result != undefined ? result : '';
    };

    $scope.getPage = function ()
    {
        var result = $scope.getRouteParams($routeParams)['page'];

        return result != undefined ? result : 1;
    };

    $scope.getPayment = function ()
    {
        var result = $scope.getRouteParams($routeParams)['payment'];

        return result != undefined ? result : '';
    };

    $scope.getDocuments = function ()
    {
        var result = $scope.getRouteParams($routeParams)['documents'];

        return result != undefined ? result : '';
    };

    $scope.getComment = function ()
    {
        var result = $scope.getRouteParams($routeParams)['comment'];

        return result != undefined ? result : '';
    };

    $scope.incoming =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name',
                filter: $scope.getName()
            },
            {
                text: t.t('proposals', 'CONTRACTOR'),
                name: 'contractor',
                filter: $scope.getContractor()
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date',
                filter: $scope.getDate()
            },
            {
                name: 'buttons',
                sortable: false
            }
        ],
        sorting: 'ajax',
        order: $scope.getOrder(),
        sort: function (column, reverse)
        {
            var r = $scope.getOrder();

            if (r == 'ASC') {
                r = 'DESC';
            } else {
                r = 'ASC';
            }

            window.history.pushState(null, null,
                '#!/incoming/:' + column +
                '/:' + r +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getPage());
        },
        filter: function (filter) 
        {
            var name = '',
                contractor = '',
                date = '';

            for (item in filter) {
                switch(filter[item].name) {
                    case 'name':
                        name = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'contractor':
                        contractor = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'date':
                        date = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                }
            }
            
            window.history.pushState(null, null,
                '#!/incoming/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + name +
                '/:' + contractor +
                '/:' + date +
                '/:1');
        },
        page: function (page)
        {
            window.history.pushState(null, null,
                '#!/incoming/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + page);
        }
    };

    $scope.accepted =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name',
                filter: $scope.getName()
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date',
                filter: $scope.getDate()
            },
            {
                text: t.t('proposals', 'CONTRACTOR'),
                name: 'contractor',
                filter: $scope.getContractor()
            },
            {
                text: t.t('proposals', 'PAYMENT'),
                name: 'payment',
                filter: $scope.getPayment(),
                filterType: 'select',
                filterItems: [
                    {id: proposal.PAYMENT_NOT, name: '-'},
                    {id: proposal.PAYMENT_PART, name: t.t('proposals', 'PARTLY')},
                    {id: proposal.PAYMENT_FULL, name: '100%'},
                    {id: proposal.PAYMENT_BILL, name: t.t('proposals', 'INVOICE ISSUED')}
                ]
            },
            {
                text: t.t('proposals', 'DOCUMENTS'),
                name: 'documents',
                filter: $scope.getDocuments(),
                filterType: 'select',
                filterItems: [
                    {id: proposal.DOCUMENTS_NOT, name: '-'},
                    {id: proposal.DOCUMENTS_RECIVED, name: t.t('proposals', 'RECIEVED')}
                ],
                filterDefaultItem: {id: '?', name: t.t('proposals', 'CHOOSE')}
            },
            {
                name: 'buttons',
                sortable: false
            }
        ],

        order: $scope.getOrder(),
        sorting: 'ajax',
        sort: function (column, reverse)
        {
            var r = $scope.getOrder();

            if (r == 'ASC') {
                r = 'DESC';
            } else {
                r = 'ASC';
            }

            window.history.pushState(null, null,
                '#!/accepted/:' + column +
                '/:' + r +
                '/:' + $scope.getName() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getPayment() +
                '/:' + $scope.getDocuments() +
                '/:' + $scope.getPage());
        },
        filter: function (filter)
        {
            var name = '',
                contractor = '',
                payment = '',
                documents = '',
                date = '';

            for (item in filter) {
                switch(filter[item].name) {
                    case 'name':
                        name = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'contractor':
                        contractor = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'date':
                        date = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'payment':
                        payment = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'documents':
                        documents = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                }
            }

            window.history.pushState(null, null,
                '#!/accepted/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + name +
                '/:' + date +
                '/:' + contractor +
                '/:' + payment +
                '/:' + documents +
                '/:1');
        },
        page: function (page)
        {
            window.history.pushState(null, null,
                '#!/accepted/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + $scope.getName() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getPayment() +
                '/:' + $scope.getDocuments() +
                '/:' + page);
        }
    };

    $scope.rejected =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name',
                filter: $scope.getName()
            },
            {
                text: t.t('proposals', 'CONTRACTOR'),
                name: 'contractor',
                filter: $scope.getContractor()
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date',
                filter: $scope.getDate()
            },
            {
                text: t.t('proposals', 'COMMENT'),
                name: 'comment',
                filter: $scope.getComment()
            }
        ],

        order: $scope.getOrder(),
        sorting: 'ajax',
        sort: function (column, reverse)
        {
            var r = $scope.getOrder();

            if (r == 'ASC') {
                r = 'DESC';
            } else {
                r = 'ASC';
            }

            window.history.pushState(null, null,
                '#!/rejected/:' + column +
                '/:' + r +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getComment() +
                '/:' + $scope.getPage());
        },
        filter: function (filter)
        {
            var name = '',
                contractor = '',
                comment = '',
                date = '';

            for (item in filter) {
                switch(filter[item].name) {
                    case 'name':
                        name = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'contractor':
                        contractor = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'date':
                        date = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                    case 'comment':
                        comment = filter[item].filter != undefined ? filter[item].filter : '';
                        break;
                }
            }

            window.history.pushState(null, null,
                '#!/rejected/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + name +
                '/:' + contractor +
                '/:' + date +
                '/:' + comment +
                '/:1');
        },
        page: function (page)
        {
            window.history.pushState(null, null,
                '#!/rejected/:' + $scope.getSort() +
                '/:' + $scope.getOrder() +
                '/:' + $scope.getName() +
                '/:' + $scope.getContractor() +
                '/:' + $scope.getDate() +
                '/:' + $scope.getComment() +
                '/:' + page);
        }
    };

    $scope.getFairId = function ()
    {
        return $('.pp-fair-id').val();
    };

    $scope.count = 0;

    $scope.getCount = function ()
    {
        return $scope.incoming.rows.length;
    };

    var params;

    params =
    {
        fairId: $scope.getFairId(),
        'order[by]': $scope.getSort(),
        'order[order]': $scope.getOrder(),
        'filter[status]': proposal.STATUS_SENT,
        'filter[name]': $scope.getName(),
        'filter[contractor]': $scope.getContractor(),
        'filter[date]': $scope.getDate(),
        'limit': $scope.LIMIT,
        'page': $scope.getPage() - 1
    };

    $http.get(pp.path('backoffice/panel/requestsAjax/', params)).then(function(response)
    {
        $scope.incoming.rows = $scope.prepareRows(response.data, 'incoming');
        $scope.tabs[0].counter = response.data[0].count;
        $scope.incoming.orderBy = $scope.getSort();

        var totalItems = response.data[0].total;

        $scope.incoming.total = ((totalItems - totalItems % $scope.LIMIT) / $scope.LIMIT) + 1;
        $scope.incoming.count = totalItems;

        $scope.incoming.pageSize = $scope.LIMIT;
        $scope.incoming.currentPage = $scope.getPage() * 1;

        $scope.incoming.params = params;
    });

    params =
    {
        fairId: $scope.getFairId(),
        'order[by]': $scope.getSort(),
        'order[order]': $scope.getOrder(),
        'filter[status]': proposal.STATUS_ACCEPTED,
        'filter[name]': $scope.getName(),
        'filter[contractor]': $scope.getContractor(),
        'filter[date]': $scope.getDate(),
        'filter[payment]': $scope.getPayment(),
        'filter[documents]': $scope.getDocuments(),
        'limit': $scope.LIMIT,
        'page': $scope.getPage() - 1
    };

    $http.get(pp.path('backoffice/panel/requestsAjax/', params)).then(function(response)
    {
        $scope.accepted.rows = $scope.prepareRows(response.data, 'accepted');
        $scope.accepted.orderBy = $scope.getSort();

        var totalItems = response.data[0].total;

        $scope.accepted.total = ((totalItems - totalItems % $scope.LIMIT) / $scope.LIMIT) + 1;
        $scope.accepted.count = totalItems;

        $scope.accepted.pageSize = $scope.LIMIT;
        $scope.accepted.currentPage = $scope.getPage() * 1;

        $scope.accepted.params = params;
    });

    params =
    {
        fairId: $scope.getFairId(),
        'order[by]': $scope.getSort(),
        'order[order]': $scope.getOrder(),
        'filter[status]': proposal.STATUS_REJECTED,
        'filter[name]': $scope.getName(),
        'filter[contractor]': $scope.getContractor(),
        'filter[date]': $scope.getDate(),
        'filter[comment]': $scope.getComment(),
        'limit': $scope.LIMIT,
        'page': $scope.getPage() - 1
    };

    $http.get(pp.path('backoffice/panel/requestsAjax/', params)).then(function(response)
    {
        $scope.rejected.rows = $scope.prepareRows(response.data, 'rejected');
        $scope.rejected.orderBy = $scope.getSort();

        var totalItems = response.data[0].total;

        $scope.rejected.total = ((totalItems - totalItems % $scope.LIMIT) / $scope.LIMIT) + 1;
        $scope.rejected.count = totalItems;

        $scope.rejected.pageSize = $scope.LIMIT;
        $scope.rejected.currentPage = $scope.getPage() * 1;
        
        $scope.rejected.params = params;
    });

    $scope.prepareRows = function (models, table)
    {
        var result = [],
            prepareRow = $scope[table + 'Row'];

        for (var i in models)
        {
            var model = models[i];
            result[i] = prepareRow(model);

            if (model.seen == 0) {
                result[i].classes = {value: 'tbl__row_seen-false', type: 'disable'};
            } else {
                result[i].classes = {value: '', type: 'disable'};
            }
        }

        return result;
    };

    $scope.incomingRow = function (model)
    {
        var result     = {},
            pathView   = pp.path('proposal/proposal/viewProposalForOrganizer/', {eId: model.id, userId: model.exponentId}),
            hasFilesBtnColor = model.hasFiles ? '1' : '0';

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml('<a href="' + pathView + '">' + model.name + '</a>' +
                '<div class="buttons">' +
                '<div class="cell__file" style="opacity:' + hasFilesBtnColor + ';"></div>' +
                '</div>'),
            type: 'html'
        };

        result.contactor = {value: model.exponentName + ' (' + model.exponentEmail + ')'};
        result.date      = {value: model.date};

        console.log(model);
        result.buttons =
        {
            value:
                [
                    {
                        classes: 'button_m button_m_green button_hide',
                        text: t.t('proposals', 'APPROVE'),
                        target: '.modal-accept-' + model.id,
                        modal:
                        {
                            name: 'modal-accept-' + model.id,
                            link: pp.path('proposal/proposal/accept', {eId: model.id}),
                            text: t.t('proposals', 'APPROVE') + '?',
                            ok: t.t('proposals', 'APPROVE')
                        }
                    },
                    {
                        classes: 'button_m button_m_red button_hide',
                        text: t.t('proposals', 'REJECT'),
                        target: '.modal-remove-' + model.id,
                        modal:
                        {
                            name: 'modal-remove-' + model.id,
                            link: {
                                path: '/proposal/proposal/reject',
                                params: {
                                    eId: model.id,
                                    ecId:model.class,
                                    userId: model.exponentId,
                                    fairId: $scope.getFairId()
                                }
                            },
                            text: t.t('proposals', 'REJECT') + '?',
                            ok: t.t('proposals', 'REJECT'),
                            comment: true,
                            type: 'POST'
                        }
                    }
                ],
            type: 'buttons',
            style: {'width':'235px'}
        };

        return result;
    };

    $scope.acceptedRow = function (model)
    {
        var result     = {},
            pathView   = pp.path('proposal/proposal/viewProposalForOrganizer/', {eId: model.id, userId: model.exponentId}),
            hasFilesBtnColor = model.hasFiles ? '1' : '0';
            selected = {id: 0, name: '-'};

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml('<a href="' + pathView + '">' + model.name + '</a>' +
                '<div class="buttons">' +
                '<div class="cell__file" style="opacity:' + hasFilesBtnColor + ';"></div>' +
                '</div>'),
            type: 'html'
        };

        result.date = {value: model.date};

        result.contactor = {value: model.exponentName + ' (' + model.exponentEmail + ')'};

        var paymentItems = [
            {id: proposal.PAYMENT_NOT, name: '-'},
            {id: proposal.PAYMENT_PART, name: t.t('proposals', 'PARTLY')},
            {id: proposal.PAYMENT_FULL, name: '100%'},
            {id: proposal.PAYMENT_BILL, name: t.t('proposals', 'INVOICE ISSUED')}
        ];

        for (item in paymentItems) {
            if (model.payment == paymentItems[item].id) {
                selected = paymentItems[item];
            }
        }

        result.payment =
        {
            value: selected,
            items: paymentItems,
            type: 'select',
            onchange: function (selectedId) 
            {
                $http.get(pp.path('backoffice/panel/updatePayment/',
                    {
                        fairId: $scope.getFairId(),
                        pe: model.id,
                        payment: selectedId * 1
                    }));
            }
        };

        var documentItems = [
            {id: proposal.DOCUMENTS_NOT, name: '-'},
            {id: proposal.DOCUMENTS_RECIVED, name: t.t('proposals', 'RECIEVED')}
        ];

        for (item in documentItems) {
            if (model.documents * 1 == documentItems[item].id) {
                selected = documentItems[item];
            }
        }

        result.documents =
        {
            value: selected,
            items: documentItems,
            type: 'select',
            onchange: function (selectedId) 
            {
                $http.get(pp.path('backoffice/panel/updateDocuments/',
                    {
                        fairId: $scope.getFairId(),
                        pe: model.id,
                        documents: selectedId * 1
                    }));
            }
        };

        result.buttons =
        {
            value:
                [
                    {
                        classes: 'button_m button_m_red button_hide',
                        text: t.t('proposals', 'REJECT'),
                        target: '.modal-reject-' + model.id,
                        modal:
                        {
                            name: 'modal-reject-' + model.id,
                            link: pp.path('proposal/proposal/reject', {eId: model.id, ecId:model.class, userId: model.exponentId}),
                            text: t.t('proposals', 'REJECT') + '?',
                            ok: t.t('proposals', 'REJECT')
                        }
                    },
                    {
                        classes: 'button_small button_small_remove button_hide',
                        text: '',
                        target: '.modal-remove-' + model.id,
                        modal:
                        {
                            name: 'modal-remove-' + model.id,
                            link: pp.path('proposal/proposal/deleteByOrganizer', {eId: model.id}),
                            text: t.t('proposals', 'REMOVE_PROPOSAL'),
                            ok: t.t('proposals', 'REMOVE')
                        }
                    }
                ],
            type: 'buttons',
            style: {'width':'235px'}
        };

        return result;
    };

    $scope.rejectedRow = function (model)
    {
        var result     = {},
            pathView   = pp.path('proposal/proposal/viewProposalForOrganizer/', {eId: model.id, userId: model.exponentId}),
            hasFilesBtnColor = model.hasFiles ? '1' : '0';

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml(model.name +
                '<div class="buttons">' +
                '<div class="cell__file" style="opacity:' + hasFilesBtnColor + ';"></div>' +
                '</div>'),
            type: 'html'
        };

        result.contactor = {value: model.exponentName + ' (' + model.exponentEmail + ')'};
        result.date      = {value: model.date};

        result.comment = {};

        if (model.comment != '') {
            result.comment   = {tooltip: {id: 'tooltip-' + model.eId, content: model.comment, icon: ''}};
        }

        return result;
    };
});