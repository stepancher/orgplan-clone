pp.module('tbl', ['button', 'tooltip', 'pp-select', 't']).directive('tbl', function(t)
{
    return {
        templateUrl: pp.viewPath('tbl'),
        restrict: 'E',
        scope:
        {
            params: '='
        },
        
        link: function (scope, element, attrs)
        {
            scope.reverse = false;
            scope.orderBy = scope.params != undefined ? scope.params.orderBy : '';

            scope.t = function (string)
            {
                return t.t('proposals', string);
            };

            if (scope.params.sorting == 'ajax') {
                scope.reverse = scope.params.order != 'ASC';
            }

            scope.sort = function (param)
            {
                scope.orderBy = param;

                if (param == scope.orderBy && scope.params.sorting != 'ajax') {
                    scope.reverse = !scope.reverse;
                }

                if (scope.params.sorting == 'ajax') {
                    scope.params.sort(param, scope.reverse);
                } else {
                    scope.sortClient(param);
                }
            };

            scope.sortClient = function (param)
            {
                var arr = scope.params.rows;

                arr = arr.sort(function (x, y)
                {
                    var xObj = x[param],
                        yObj = y[param];

                    if (xObj.value > yObj.value) return 1;
                    if (xObj.value < yObj.value) return -1;
                });

                if (scope.reverse) arr.reverse();

                scope.params.rows = arr;
            };

            scope.filter = function ()
            {
                var filter = [];

                for (column in scope.params.columns) {
                    if (typeof scope.params.columns[column].filter == 'object') {
                        scope.params.columns[column].filter = scope.params.columns[column].filter.id != '?' ? scope.params.columns[column].filter.id : '';
                    }

                    filter[column] =
                    {
                        name: scope.params.columns[column].name,
                        filter: scope.params.columns[column].filter
                    };
                }

                scope.params.filter(filter);
            };

            scope.filterInput = function (e)
            {
                if (e.which == 13) {
                    scope.filter();
                }
            };

            scope.page = function (page)
            {
                scope.params.page(page);
            };

            scope.wait = function () {
                setTimeout(function () {
                    if (scope.params.rows == undefined) {
                        scope.wait();
                    } else {
                        if (scope.params.reverse != undefined) {
                            scope.reverse = !scope.params.reverse;
                        }

                        scope.sort(scope.orderBy);
                    }
                });
            };

            if (scope.params.sorting != 'ajax') {
                scope.wait();
            }
        }
    };
});