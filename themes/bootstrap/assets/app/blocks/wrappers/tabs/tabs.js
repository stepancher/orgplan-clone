pp.module('tabs', []).directive('tabs', function($location)
{
    return {
        templateUrl: pp.viewPath('tabs'),
        restrict: 'E',
        transclude: true,
        scope:
        {
            params: '='
        },

        link: function (scope, element, attrs)
        {
            scope.url = $location.url();

            scope.route = function (route)
            {
                return pp.route(route);
            };
            
            scope.log = function (param) 
            {
                console.log(param);
            };
        }
    };
}).directive('tab', function()
{
    return {
        templateUrl: pp.viewPath('tab'),
        restrict: 'E',
        transclude: true,
        scope:
        {
            params: '='
        },

        link: function (scope, element, attrs)
        {

        }
    };
});