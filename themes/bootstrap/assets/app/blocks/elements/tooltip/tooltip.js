/**
 * @augments Protoplan.Block
 * @class Protoplan.Tooltip
 * @param params
 * @constructor
 */

Protoplan.Tooltip = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.selector = params.selector != undefined ? params.selector : '';
    self.target = params.target != undefined ? params.target : '';

    self.init = function ()
    {
        $(self.target).hover(function ()
        {
            self.show();
        }, function ()
        { 
            self.hide();
        });
    };

    self.show = function ()
    {
        setTimeout(function ()
        {
            $(self.selector).fadeIn(300);
        }, 500);
    };

    self.hide = function ()
    {
        setTimeout(function ()
        {
            $(self.selector).fadeOut(300);
        }, 500);
    };

    self.init();
};

pp.module('tooltip', []).directive('tooltip', function() {
    return {
        templateUrl: pp.viewPath('tooltip'),
        restrict: 'E',
        scope:
        {
            "tooltip": '='
        },
        link: function (scope, element, attrs)
        {
            var tooltipIcon = '';

            if (scope.tooltip.icon != undefined) {
                tooltipIcon = scope.tooltip.icon;
            }

            $($(element).find('.tooltipster')).tooltipster({
                content: $('<div class="tooltip-wrap"><button class="close"></button>' + scope.tooltip.content + '</div>'),
                trigger: 'click',
                delay: 0,
                speed: 0,
                position: 'bottom'
            });
        }
    };
});