pp.module('textfield', []).directive('textfield', function($rootScope)
{
    return {
        templateUrl: '/static/templates/textfield.html',
        restrict: 'E',
        scope:
        {
            params: '='
        },

        link: function (scope, element, attrs)
        {
            scope.config = scope.params;

            scope.id     = scope.config.id;
            scope.value  = scope.config.value;

            scope.field = {};

            scope.setDefault = function () {
                angular.forEach(scope.field, function(value, key) {
                    scope.field[key] = scope.config.properties[key];
                });
            };

            scope.setDefault();

            angular.forEach(scope.field, function(value, key) {
                scope.field[key] = scope.config.properties[key];
            });

            scope.$on('update', function($event, $signal){
                var dependency = scope.getDependency($signal.id);

                if(dependency != false){
                    var fn = scope.functions[dependency.fn];
                    if(fn != undefined){
                        fn(dependency.fnArgs, $signal);
                        scope.go($signal.init);
                    }
                }
            });

            scope.go = function(init) {
                init = typeof init == 'undefined' ? false : init;

                if(scope.value != null){
                    scope.field.summaryValue = scope.field.price * scope.value * scope.field.discount;

                    if (isNaN(scope.field.summaryValue)) {
                        scope.field.summaryValue = '---'
                    }
                }
                scope.safeApply();

                if (scope.value != '') {
                    $rootScope.$broadcast('update', {
                        id : scope.config.id,
                        field: scope.field,
                        inputValue: scope.value,
                        properties: scope.config.properties,
                        parent: scope.config.parent,
                        init: init
                    });
                }
            };

            scope.getDependency = function(id){
                var result = false;
                angular.forEach(scope.config.dependencies, function(dependency) {
                    if(dependency.id == id){
                        result = dependency;
                    }
                });

                return result;
            };

            scope.functions = {
                inherit : function($args, $signal){
                    angular.forEach(scope.field, function(value, key) {
                        if($signal.properties[key] != undefined){
                            scope.field[key] = $signal.properties[key];
                        }
                    });
                }
            };

            scope.normalize = function($value){
                return parseFloat(parseFloat($value).toFixed(2));
            };

            scope.safeApply = function() {
                var phase = this.$root.$$phase;
                if(phase != '$apply' && phase != '$digest') {
                    this.$apply();
                }
            };

            scope.reset = function(){
                scope.value = '';
                scope.field.summaryValue = '0';
                scope.safeApply();

                $rootScope.$broadcast('update', {
                    id : scope.config.id,
                    field: scope.field,
                    inputValue: scope.value,
                    properties: scope.config.properties
                });
            };

            angular.element(document).ready(function(){
                if(scope.value){
                    scope.go(true);
                }
            });
        }
    };
});