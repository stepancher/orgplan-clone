/**
 * @augments Protoplan.Block
 * @class Protoplan.Checkbox
 * @param id
 * @param state
 * @param selector
 * @param disabled
 * @constructor
 */

Protoplan.Checkbox = function (id, selector, state, disabled)
{
    var self = this;

    self.selector         = typeof selector == 'undefined' ? '#checkbox-' + id : selector;
    self.state            = typeof state    == 'undefined' ? false : state;
    self.disabled         = typeof disabled == 'undefined' ? false : disabled;
    self.childrenSelector = '[data-checkbox="' + id + '"]';

    Protoplan.Block.apply(this, [id, self.selector]);

    self.init = function ()
    {
        if ($(self.childrenSelector).length === 0) {
            self.childrenSelector = '.parent-' + self.id;
        }

        self.hasNotEmptyInputs() ? self.setState(true) : self.setState(self.state);

        $(document).delegate(self.selector, 'click', function ()
        {
            self.go();

            return !disabled;
        });
    };

    self.open = function ()
    {
        return $(self.childrenSelector).slideDown('fast');
    };

    self.close = function ()
    {
        $(self.childrenSelector).find('input').val('');
        $(self.childrenSelector).find('input').trigger('input'); //@TODO Refactor it
        return $(self.childrenSelector).slideUp('fast');
    };

    /**
     * @returns {boolean}
     */
    self.getState = function ()
    {
        self.state = $(self.selector).prop('checked');
        return self.state;
    };

    /**
     * @param state
     * @returns {boolean}
     */
    self.setState = function (state)
    {
        $(self.selector).prop('checked', state);
        self.go();
        return self.getState();
    };

    /**
     * @returns {boolean}
     */
    self.hasNotEmptyInputs = function ()
    {
        var ret = false;
        $(self.childrenSelector).each(function (i, value)
        {
            var inputValue = $(value).find('input[type="text"]').val();
            if (inputValue != '' && inputValue != undefined) {
                ret = true;
            }
        });

        return ret;
    };

    self.go = function ()
    {
        self.getState() ? self.open() : self.close();
    };

    setTimeout(function ()
    {
        self.init();
    }, 1000);
};