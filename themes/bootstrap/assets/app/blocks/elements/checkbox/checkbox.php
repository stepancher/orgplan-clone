<div class="proposal-checkbox">
    <input class="proposal-checkbox__input" id="checkbox-1337" type="checkbox">
    <label class="proposal-checkbox__label" for="checkbox-1337">Label</label>
</div>

<div class="proposal-checkbox proposal-checkbox_disabled">
    <input class="proposal-checkbox__input" id="checkbox-1338" type="checkbox">
    <label class="proposal-checkbox__label" for="checkbox-1338">Label</label>
</div>