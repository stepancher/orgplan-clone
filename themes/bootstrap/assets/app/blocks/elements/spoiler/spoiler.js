/**
 * @augments Protoplan.Block
 * @class Protoplan.Spoiler
 * @param params
 * @constructor
 */

Protoplan.Spoiler = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.selector    = params.selector    == undefined ? '.spoiler'                     : params.selector;
    self.clickArea   = params.clickArea   == undefined ? '.spoiler'                     : params.clickArea;
    self.target      = params.target      == undefined ? ''                             : params.target;
    self.hiddenInput = params.hiddenInput == undefined ? '#Attribute_attr-' + params.id : params.hiddenInput;
    self.wrap        = params.wrap        == undefined ? '.spoiler-wrap-' + params.id   : params.wrap;

    self.state = 0;
    
    self.init = function ()
    {
        $(self.clickArea).click(function ()
        {
            if (params.wrap != undefined) {
                $(self.wrap).slideToggle(100);
            } else {
                $(self.target).slideToggle(100);
            }

            $(self.selector).toggleClass('open');
        });

        self.setValue(self.wrapIsNotEmpty());

        $(document).delegate(self.wrap + ' input', 'input change', function ()
        {
            self.setValue(self.wrapIsNotEmpty());
        });
    };

    self.setValue = function (value)
    {
        $(self.hiddenInput).val(0);
        if (value == 1) {
            $(self.hiddenInput).val(1);
        }

        if (value == 1) {
            $(self.selector).addClass('open');
        } else {
            $(self.selector).removeClass('open');
        }

        value == 1 ? $(self.wrap).slideDown() : $(self.wrap).slideUp();
    };

    /**
     * @returns {int}
     */
    self.wrapIsNotEmpty = function ()
    {
        var result = 0;

        $(self.wrap + ' input').each(function (i, item)
        {
            if($(item).val() != '' && $(item).val() != '0') {
                result = 1;
            }
        });

        return result;
    };

    //@TODO Remove
    setTimeout(function ()
    {
        self.init();
    }, 1000);
};