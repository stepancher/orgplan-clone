/**
 * @augments Protoplan.Block
 * @class Protoplan.DatePicker
 * @param id
 * @param start
 * @param end
 * @param params
 * @param blockParams
 * @constructor
 */

Protoplan.DatePicker = function (id, start, end, params, blockParams)
{
    Protoplan.Block.apply(this, [id, '.datePicker' + id]);

    var self = this;

    self.minDate     = typeof start       == 'undefined' ? new Date(0) : new Date(Date.parse(start));
    self.maxDate     = typeof end         == 'undefined' ? new Date()  : new Date(Date.parse(end));
    self.params      = typeof params      == 'undefined' ? {}          : params;
    self.blockParams = typeof blockParams == 'undefined' ? {}          : blockParams;

    self.startMonth    = (new Date(self.minDate)).getMonth() + 1;
    self.endSelector   = self.selector + '-end';
    self.count         = 0;

    self.maxDate.setDate(self.maxDate.getDate() + 1);

    self.init = function ()
    {
        $(self.selector).periodpicker(self.merge({
            end: self.selector + '-end',
            norange: true,
            mousewheel: false,
            likeXDSoftDateTimePicker: true,
            cells: [1, 2],
            withoutBottomPanel: true,
            yearsLine: false,
            title: false,
            closeButton: false,
            fullsizeButton: false,
            minDate: self.minDate.toISOString(),
            maxDate: self.maxDate.toISOString(),
            startMonth: self.startMonth,
            formatDate: 'YYYY/MM/DD',
            timepickerOptions: {
                twelveHoursFormat: false,
                hours: true,
                minutes: true,
                seconds: false,
                ampm: false
            }
        }, self.params));
    };

    self.addEndDate = function ()
    {
        var result = '',
            dates  = $(self.selector).periodpicker('value');

        if (dates.length > 0) {
            result = self.formatDate(dates[0]) + ' - ' + self.formatDate(dates[1]);
            self.setCount(dates[1] - dates[0]);
        }

        $(self.selector).val(result);
        return result;
    };

    self.formatDate = function (date)
    {
        var year    = date.getFullYear(),
            month   = ('0' + (date.getMonth() + 1)).slice(-2),
            day     = ('0' + date.getDate()).slice(-2),
            hours   = ('0' + date.getHours()).slice(-2),
            minutes = ('0' + date.getMinutes()).slice(-2),
            seconds = ('0' + date.getSeconds()).slice(-2),
            result  = year + '/' + month + '/' + day;

        if (hours > 0 || minutes > 0 || seconds > 0) {
            result = result + ' ' + hours + ':' + minutes + ':' + seconds;
        }

        return result;
    };

    self.setCount = function (count)
    {
        self.count = count;
        return count;
    };

    self.getDays = function ()
    {
        return Math.round(self.count / (1000 * 60 * 60 * 24) + 1);
    };

    self.getHours = function ()
    {
        return Math.round(self.count / (1000 * 60 * 60));
    };

    self.go = function ()
    {
        if ($(self.endSelector).length > 0) {
            self.addEndDate();
        }
    };

    self.init();
};
