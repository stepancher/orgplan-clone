/**
 * @augments Protoplan.Block
 * @class Protoplan.Spinner
 * @param params
 * @constructor
 */

Protoplan.Spinner = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.step = params.step != undefined ? params.step : 1;

    self.up   = '.spinner__btn_up-' + self.id;
    self.down = '.spinner__btn_down-' + self.id;
    self.state = 0;
    self.min = 0;

    self.init = function ()
    {
        $(self.up).click(function ()
        {
            self.setState(self.state * 1 + self.step * 1);
            self.render();
            return false;
        });

        $(self.down).click(function ()
        {
            self.setState(self.state * 1 - self.step * 1);
            self.render();
            return false;
        });

        $(document).delegate(self.selector, 'keyup keypress', function (e)
        {
            return false;
        });
    };

    self.render = function ()
    {
        $(self.selector).val(self.state);
        $(self.selector).trigger('change');
    };

    self.setState = function (value)
    {
        var result = self.min;

        if (value >= self.min) {
            result = value
        }

        self.state = result;
    };

    self.init();
};