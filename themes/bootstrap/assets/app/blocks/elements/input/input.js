/**
 * @augments Protoplan.Block
 * @class Protoplan.Input
 * @param params
 * @constructor
 */

//@TODO create constants
Protoplan.Input = function (params)
{
    Protoplan.Block.apply(this, [params.id, params.selector]);

    var self = this;

    self.params   = typeof params.params   == 'undefined' ? {}                   : params;
    self.type     = typeof params.type     == 'undefined' ? 'string'             : params.type;
    self.selector = typeof params.selector == 'undefined' ? '.input' + params.id : params.selector;

    self.init = function ()
    {
        if (typeof self['setType' + self.capitalizeFirstLetter(self.type)] == 'function') {
            var fn = self['setType' + self.capitalizeFirstLetter(self.type)];
            fn();
        }
    };

    self.setTypeInt = function ()
    {
        self.type = 'int';

        $(document).delegate(self.selector, 'keyup keypress', function (e)
        {
            var symbol = (e.which) ? e.which : e.keyCode;
            if (symbol == 8) return true; //@TODO rewrite to switch
            if (symbol < 48 || symbol > 57)  return false;
        });
    };

    self.setTypeDouble = function ()
    {
        self.type = 'double';

        $(document).delegate(self.selector, 'keyup keypress', function (e)
        {
            var symbol = (e.which) ? e.which : e.keyCode;
            if (symbol == 46) return true; //@TODO rewrite to switch
            if (symbol == 8) return true;
            // if (symbol == 44) return true;
            if (symbol < 48 || symbol > 57)  return false;
        });
    };

    self.setTypeString = function ()
    {
        self.type = 'string';
    };

    self.onlyNumbers = function () {

    };

    self.init();
};