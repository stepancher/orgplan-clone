pp.module('button', ['modal']).directive('btn', function() {
    return {
        templateUrl: pp.viewPath('button'),
        restrict: 'E',
        scope:
        {
            params: '='
        },
        link: function (scope, element, attrs)
        {
            if (scope.params.target != undefined) {
                scope.show = function ()
                {
                    $(scope.params.target).modal('show')
                };
            }
        }
    };
});