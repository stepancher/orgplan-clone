/**
 * @augments Protoplan.Block
 * @class Protoplan.ScrollUp
 * @constructor
 */

Protoplan.ScrollUp = function ()
{
    var self = this;

    self.selector = '.scroll-up';

    Protoplan.Block.apply(this, ['scroll-up', self.selector]);

    self.init = function ()
    {
        self.update();

        $(document).scroll(function ()
        {
            self.update();
        });

        $(self.selector).click(function ()
        {
            window.scroll(0 ,0);
            return false;
        });
    };

    self.update = function ()
    {
        var display = 'none';
        
        if (window.pageYOffset > 200) {
            display = 'block';
        }
        
        $(self.selector).css('display', display);
    };

    self.init();
};

protoplan.elements['scroll-up'] = new Protoplan.ScrollUp();