pp.module('pp-select', []).directive('ppselect', function() {
    return {
        templateUrl: pp.viewPath('pp-select'),
        restrict: 'E',
        scope:
        {
            "params": '='
        },
        link: function (scope, element, attrs)
        {
            scope.change = function ()
            {
                scope.params.onchange(scope.params.value.id);
            }
        }
    };
});