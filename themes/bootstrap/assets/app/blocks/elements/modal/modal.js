/**
 * Class Modal
 * @constructor
 */

Protoplan.Modal = function ()
{
    this.init = function ()
    {
        var selector = '.modal';
        $(selector).on('shown.bs.modal', function()
        {
            $(this).find('input').each(function ()
            {
                if ($(this).attr('type') != 'hidden') {
                    this.focus();
                    return false;
                }
            });
        });

        $(selector).on('hidden.bs.modal', function()
        {
            $(this).find('input').val('');
        });
    };

    this.confirm = function (params)
    {
        return {
            confirmText: '',
            path: '',
            after: '',
            id: '',
            method: 'get',
            formId: '',

            run: function(params)
            {
                $('body').on('click', '#' + params.id, function ()
                {
                    params.remove_btn = $(this);

                    var target = $(params.remove_btn).attr('data-target');

                    $(target).modal('show');

                    $(target + ' .js-modal-ok').click(function (e)
                    {
                        e.stopPropagation();

                        if(params.method=='get'){
                            $.get(params.path).done(params.after(params));
                        }

                        if(params.method=='post'){
                            $.post(params.path, $('#' + params.formId).serialize()).done(params.after(params));
                        }

                        $(target).modal('hide');
                    });

                    $('.js-modal-cancel').click(function (e)
                    {
                        e.stopPropagation();
                        $(target).modal('hide');
                    });
                });
            }
        }.run(params);
    };

    this.init();
};

pp.module('modal', ['t']).directive('modal', function($http, t) {
    return {
        templateUrl: pp.viewPath('modal'),
        restrict: 'E',
        scope:
        {
            params: '='
        },
        link: function (scope, element, attrs)
        {
            scope.ok = function ()
            {
                if(scope.params.type == 'POST'){
                    scope.params.link.params.message = scope.params.message;
                    var eId =scope.params.link.params.eId;
                    delete scope.params.link.params.eId;

                    $.ajax({
                        url: '/' + pp.lang + scope.params.link.path + '/?eId='+eId,
                        data:scope.params.link.params,
                        type:'post',
                        success:function (response) {
                            $(element).find('.modal').modal('hide');
                        }
                    });

                }else{

                    $http.get(scope.params.link).then(function(response) {
                        $(element).find('.modal').modal('hide');
                    });
                }

                $('a[data-target=".' + scope.params.name + '"]').parents('.tbl__row').slideUp(300);
            };

            scope.cancel = function ()
            {
                $(element).find('.modal').modal('hide');
            };

            scope.t = function (key)
            {
                return t.t('proposals', key);
            };
        }
    };
});