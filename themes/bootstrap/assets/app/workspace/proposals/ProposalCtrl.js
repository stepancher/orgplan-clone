pp.module('workspace').controller('WorkspaceProposalCtrl', function ($scope, $http, t, proposal, $sce)
{
    $scope.params =
    {
        columns: [
            {
                text: t.t('proposals', 'NAME'),
                name: 'name'
            },
            {
                text: t.t('proposals', 'DATE'),
                name: 'date'
            },
            {
                text: t.t('proposals', 'STATUS'),
                name: 'status'
            },
            {
                text: t.t('proposals', 'PAYMENT'),
                name: 'payment'
            },
            {
                text: t.t('proposals', 'DOCUMENTS'),
                name: 'documents'
            },
            {
                name: 'buttons',
                sortable: false
            }
        ],
        orderBy: 'date',
        reverse: true
    };

    $http.get(pp.path('workspace/list/requestsAjax/')).then(function(response)
    {
        $scope.params.rows = $scope.prepareRows(response.data);
    });

    $scope.prepareRows = function (models)
    {
        var result = [];

        for (var model in models)
        {
            result[model] = $scope.prepareRow(models[model]);
        }

        return result;
    };

    $scope.prepareRow = function (model)
    {
        var result     = {},
            pathEdit   = pp.path('proposal/proposal/create/', {ecId: model.ecId, eId: model.eId}),
            pathView   = pp.path('proposal/proposal/viewProposal/', {ecId: model.ecId, eId: model.eId});

        result.name =
        {
            value: model.name,
            html: $sce.trustAsHtml('<a href="' + pathView + '">' + model.name + '</a>'),
            type: 'html'
        };
        
        result.date      = {value: model.date};
        result.status    = {value: model.status * 1};
        result.payment   = {value: model.payment * 1};
        result.documents = {value: model.documents * 1};

        result.buttons =
        {
            value:
            [
                {
                    href: pathEdit,
                    classes: 'button_small button_small_edit button_hide',
                    text: ''
                },
                {
                    classes: 'button_small button_small_remove button_hide',
                    text: '',
                    target: '.modal-' + model.eId,
                    modal:
                    {
                        name: 'modal-' + model.eId,
                        link: pp.path('proposal/proposal/prepareDelete', {eId: model.eId, ecId: model.ecId, userId: model.userId}),
                        text: t.t('proposals', 'REMOVE_PROPOSAL'),
                        ok: t.t('proposals', 'REMOVE')
                    }
                }
            ],
            type: 'buttons'
        };

        if ((model.status * 1) == proposal.STATUS_SENT || (model.status * 1) == proposal.STATUS_ACCEPTED) {
            result.buttons = {};
        }

        result.documents.value = proposal.getDocumentStatusName(result.documents.value);
        result.payment.value   = proposal.getPaymentStatusName(result.payment.value);
        result.status          = proposal.getStatus(result.status.value);

        if (model.comment != undefined && model.comment != '') {
            result.status.tooltip = {id: 'tooltip-' + model.eId, content: model.comment, icon: ''};
        }

        return result;
    };
});