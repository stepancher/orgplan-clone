pp.module('proposal', ['t']).factory('proposal', function($http, t) {
    return {
        STATUS_DRAFT: 1,
        STATUS_REJECTED: 3,
        STATUS_SENT: 2,
        STATUS_ACCEPTED: 4,
        STATUS_REMOVED_BY_ORGANIZER: 5,
        STATUS_REMOVED_BY_EXPONENT: 6,
    
        PAYMENT_NOT: 0,
        PAYMENT_PART: 1,
        PAYMENT_FULL: 2,
        PAYMENT_BILL: 3,
    
        DOCUMENTS_NOT: 0,
        DOCUMENTS_RECIVED: 1,
        
        SEEN_TRUE: 1,
        SEEN_FALSE: 0,

        /**
         * @param type
         * @returns {string}
         */
        getDocumentStatusName: function (type) 
        {
            var result = '';

            switch (type) {
                case this.DOCUMENTS_NOT:
                    result = t.t('proposals', 'DOCUMENTS_NOT');
                    break;
                case this.DOCUMENTS_RECIVED:
                    result = t.t('proposals', 'DOCUMENTS_RECIVED');
                    break;
            }

            return result;
        },

        /**
         * @param type
         * @returns {{}}
         */
        getStatus: function (type) 
        {
            var result = {};

            switch (type) {
                case this.STATUS_ACCEPTED:
                    result.value = t.t('proposals', 'STATUS_ACCEPTED');
                    result.color = 'text_success';
                    result.circle = 'circle_success';
                    break;
                case this.STATUS_DRAFT:
                    result.value = t.t('proposals', 'STATUS_DRAFT');
                    result.color = 'text_muted';
                    result.circle = 'circle_muted';
                    break;
                case this.STATUS_REJECTED:
                    result.value = t.t('proposals', 'STATUS_REJECTED');
                    result.color = 'text_danger';
                    result.circle = 'circle_danger';
                    break;
                case this.STATUS_REMOVED_BY_ORGANIZER:
                    result.value = t.t('proposals', 'STATUS_REMOVED_BY_ORGANIZER');
                    result.color = 'text_warning';
                    result.circle = 'circle_warning';
                    break;
                case this.STATUS_REMOVED_BY_EXPONENT:
                    result.value = t.t('proposals', 'STATUS_REMOVED_BY_EXPONENT');
                    result.color = 'text_warning';
                    result.circle = 'circle_warning';
                    break;
                case this.STATUS_SENT:
                    result.value  = t.t('proposals', 'STATUS_SENT');
                    result.color  = 'text_primary';
                    result.circle = 'circle_primary';
                    break;
            }

            return result;
        },

        /**
         * @param type
         * @returns {string}
         */
        getPaymentStatusName: function (type) 
        {
            var result = '';

            switch (type) {
                case this.PAYMENT_FULL:
                    result = t.t('proposals', 'PAYMENT_FULL');
                    break;
                case this.PAYMENT_NOT:
                    result = t.t('proposals', 'PAYMENT_NOT');
                    break;
                case this.PAYMENT_PART:
                    result = t.t('proposals', 'PAYMENT_PART');
                    break;
                case this.PAYMENT_BILL:
                    result = t.t('proposals', 'PAYMENT_BILL');
                    break;
            }

            return result;
        },
        
        loadWorkspaceProposals: function (callback) 
        {
            
        }
    }
});