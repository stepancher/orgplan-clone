<?php
/**
 * Console
 * Точка входа, обрабатывает входящие запросы
 */
define('YII_DEBUG', true);
require(__DIR__ . '/protected/config/_const.php');
/**
 * Запуск консольных служб через URL
 */
if (isset($_SERVER['REQUEST_URI'])) {
	// Подменяем аргументы коммандной строки
	(isset($_GET['argv']) && is_array($_GET['argv'])) or die('Empty "argv"');
	$_SERVER['argv'] = array_merge(array('cron'), $_GET['argv']);
}
/**
 * Cron
 * Точка входа, обрабатывает входящие запросы
 */
if (YII_DEBUG) {
	// Включение вывода ошибок
	ini_set('display_errors', 1);
	error_reporting(E_ALL | E_NOTICE);
}
// fix for fcgi
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

/**
 * Подключем приложение
 */
require(__DIR__ . '/protected/vendor/autoload.php');
require(__DIR__ . '/protected/vendor/yiisoft/yii/framework/yii.php');

$app = Yii::createConsoleApplication(
	require(__DIR__.'/protected/config/console.php')
);
$app->commandRunner->addCommands(YII_PATH.'/cli/commands');
$app->commandRunner->addCommands(Yii::getPathOfAlias('application.commands'));
$app->commandRunner->addCommands(Yii::getPathOfAlias('ext.commands'));
$app->run();