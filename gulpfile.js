var gulp       = require('gulp'),
    less       = require('gulp-less'),
    cssnano    = require('gulp-cssnano'),
    notify     = require('gulp-notify'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify     = require('gulp-uglify'),
    concat     = require('gulp-concat'),
    rename     = require('gulp-rename');

var pathToApp = 'themes/bootstrap/assets/app/';

gulp.task('styles', function() {
    return gulp.src([pathToApp + '**/*.less'])

        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(concat('global.css'))
        .pipe(sourcemaps.write())

        .pipe(gulp.dest('themes/bootstrap/assets/dist/'));
        // .pipe(notify({ message: 'Style compiled' }));
});

gulp.task('images', function() {
    return gulp.src([pathToApp + '**/*.png'])
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest('themes/bootstrap/assets/dist/'));
        // .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('templates', function() {
    return gulp.src([pathToApp + '**/*.html'])
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest('protected/static/templates/'));
});

gulp.task('styles-production', function() {
    return gulp.src([pathToApp + '**/*.less'])

        .pipe(less())
        .pipe(concat('global.css'))
        .pipe(cssnano({zindex: false}))

        .pipe(gulp.dest('themes/bootstrap/assets/dist/'))
        .pipe(notify({ message: 'Style compiled' }));
});

gulp.task('scripts', function() {
    return gulp.src([pathToApp + '**/*.js'])

        .pipe(sourcemaps.init())
        .pipe(concat('global.js'))
        .pipe(sourcemaps.write())

        .pipe(gulp.dest('themes/bootstrap/assets/dist/'));
        // .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('scripts-production', function() {
    return gulp.src([pathToApp + '**/*.js'])
        .pipe(concat('global.js'))
        .pipe(uglify({mangle:false}))
        .pipe(gulp.dest('themes/bootstrap/assets/dist/'))
        .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('default', function() {
    gulp.run('styles');
    gulp.run('scripts');
    gulp.run('images');
    gulp.run('templates');

    gulp.watch(pathToApp + '**/*.html', function(event) {
        console.log(event.path + ' was changed, running tasks...');
        gulp.run('templates');
    });

    gulp.watch(pathToApp + '**/*.less', function(event) {
        console.log(event.path + ' was changed, running tasks...');
        gulp.run('styles');
    });

    gulp.watch(pathToApp + '**/*.js', function(event) {
        console.log(event.path + ' was changed, running tasks...');
        gulp.run('scripts');
    });
});

gulp.task('p', function() {
    gulp.run('styles-production');
    gulp.run('scripts-production');
});