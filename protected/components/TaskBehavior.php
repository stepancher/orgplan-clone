<?php

/**
 * Правильно сохранение даанных для Task
 */
class TaskBehavior extends CActiveRecordBehavior
{
    public function afterSave($event)
    {
        /** @var Task $model */
        $model = $this->getOwner();
        if (null != $model->duration && is_numeric($model->duration)) {
//            $endDate = date(
//                'Y-m-d H:i:s',
//                strtotime($this->_formatDate($model->startDate)) + (($model->duration) * 24*60*60) - 1
//            );
//            $model->updateByPk($model->id, ['endDate' => $endDate]);
        } else {
            $sdTime = date('H:i:s', strtotime($this->_formatDate($model->startDate)));
            $edTime = date('H:i:s', strtotime($this->_formatDate($model->endDate)));
            $duration = (strtotime($this->_formatDate($model->endDate)) - strtotime($this->_formatDate($model->startDate))) / (24 * 60 * 60);
            if ($sdTime == '00:00:00' && in_array($edTime, ['01:00:00', '00:00:00'])) {
                $duration = $duration < 1 ? 1 : ceil($duration) + 1;
            } else {
                $duration = floor($duration);
            }
            $model->updateByPk($model->id, ['duration' => $duration]);
        }

        if (isset($_POST['ids']) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
            $model->updateByPk($model->id, [
                'progress' => isset($_POST[$id . '_progress']) ? $_POST[$id . '_progress'] : null,
                'source' => isset($_POST[$id . '_source']) ? $_POST[$id . '_source'] : null,
                'parent' => isset($_POST[$id . '_parent']) ? $_POST[$id . '_parent'] : null,
                'target' => isset($_POST[$id . '_target']) ? $_POST[$id . '_target'] : null,
                /*'type' => Task::TYPE_TASK*/
            ]);
            if (isset($_POST[$id . '_parent'])) {
                /*$task = Task::model()->findByPk($_POST[$id . '_parent']);
                if (null != $task) {
                    $model->updateByPk($task->id, [
                        'type' => Task::TYPE_PROJECT
                    ]);
                }*/
        }
    }

    }

    private function _formatDate($value)
    {
        $result = null;
        if (preg_match('#(\d{4}\-\d{2}\-\d{2}\s*)(\d*)(\:*\d*)(\:*\d*)#', $value, $matches)) {
            $result = $matches[1];
            if(!empty($matches[4])) {
                $result.= $matches[2].$matches[3].$matches[4];
            } elseif (!empty($matches[3])) {
                $result.= $matches[2].$matches[3].':00';
            } elseif (!empty($matches[2])) {
                $result.= $matches[2].':00:00';
            }
        }
        return $result;
    }
}