<?php
Yii::import('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget');

/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 04.03.2015
 * Time: 12:49
 */
class IRedactor
{
	static private $_source_dir;

	/**
	 * @return mixed
	 */
	static function getUploadFiles()
	{
		/**
		 * @var CWebApplication $app
		 */
		$app = Yii::app();
		return $app->user->getState('temp_files', array());
	}

	/**
	 * @param $path
	 * @param $filename
	 */
	static function setUploadFiles($path, $filename)
	{
		/**
		 * @var CWebApplication $app
		 */
		$app = Yii::app();

		$tmp_files = $app->user->getState('temp_files');
		if ($tmp_files) {
			$tmp_files = $tmp_files + array($filename => $path);
			$app->user->setState('temp_files', $tmp_files);
		} else {
			$app->user->setState('temp_files', array($filename => $path));
		}
	}

	/**
	 * @param $modelName
	 */
	static function createItems($modelName)
	{
		if (is_dir(self::$_source_dir = self::getPathToUpload($modelName))) {
			self::moveFile();
		}
	}

	static function getPathToTemp()
	{
		/**
		 * Создание папок
		 */
		$DS = DIRECTORY_SEPARATOR;
		$path = Yii::getPathOfAlias('application.uploads') . $DS . 'temp' . $DS;
		if (!is_dir($path)) {
			mkdir($path);
		}
		return $path;
	}

	static function getPathToUpload($modelName)
	{
		$DS = DIRECTORY_SEPARATOR;
		$path = Yii::getPathOfAlias('application.uploads') . $DS;
		foreach (
			array(
				'resources',
				$modelName,
			) as $val) {
			if (!is_dir($path . $val . $DS)) {
				mkdir($path . $val . $DS);
			}
			$path .= $val . $DS;
		}
		return $path;
	}

	/**
	 * @return bool
	 */
	static function moveFile()
	{
		foreach (self::getUploadFiles() as $fileName => $tmpPath) {
			# Копируем файл
			if (copy($tmpPath . $fileName, self::$_source_dir . $fileName)) {
				# Удаляем файл
				unlink($tmpPath . $fileName);
			}
		}
	}

	/**
	 * @param $models
	 */
	static function clearUploadFiles($models = array())
	{
		/**
		 * @var CWebApplication $app
		 */
		$app = Yii::app();
		Yii::app()->user->getState('temp_files');
		foreach ($models as $model) {
			if (method_exists($model, 'getErrors')) {
				if (!empty($model->getErrors())) {
					return;
				}
			}
		}
		if (!empty($app->user->getState('temp_files'))) {
			foreach ($app->user->getState('temp_files') as $fileName => $tmpPath) {
				if (is_dir($tmpPath) && file_exists($tmpPath . $fileName)) {
					unlink($tmpPath . $fileName);
				}
			}
			$app->user->setState('temp_files', null);
		}
	}

}
