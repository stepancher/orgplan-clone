<?php

/**
 * Created by PhpStorm.
 * User: user93
 * Date: 14.10.15
 * Time: 17:54
 */
class DHtmlXHelper
{

    static function getBehaviorName()
    {
        return 'DHtmlXBehavior';
    }

    /**
     * @param Task[] $tasks
     * @return array
     */
    static function convertTasksToEvents($tasks, $isGantt = false)
    {
        $taskData = [];
        foreach ($tasks as $key => $task) {
            $taskData[$key]['id'] = $task->id;
            $taskData[$key]['text'] = $task->name;
            $taskData[$key]['name'] = $task->name;
            $taskData[$key]['description'] = $task->description;
            $taskData[$key]['duration'] = $task->duration;
            $taskData[$key]['milestoneId'] = $task->milestoneId;
            if ($task->parent) {
                $taskData[$key]['parent'] = $task->parent;
            }
            $taskData[$key]['myFairId'] = $task->myFairId;
            $taskData[$key]['status'] = $task->status;
            if ($task->type) {
                /*$taskData[$key]['type'] = $task->type ? Task::types($task->type) : Task::types(Task::TYPE_TASK);*/
            }
            //$taskData[$key]['type'] = $task->type;
            if ($task->responsible) {
                $taskData[$key]['responsibleName'] = $task->responsible->forename;
                $taskData[$key]['responsibleEmail'] = $task->responsible->email;
                $taskData[$key]['responsibleId'] = $task->responsible->id;
            }
            $taskData[$key]['start_date'] = date('Y-m-d H:i', strtotime($task->startDate));
//            if (!$isGantt) {
//                $taskData[$key]['end_date'] = date('Y-m-d 23:59', strtotime($task->endDate));
//            }
            $taskData[$key]['end_date'] = date('Y-m-d H:i', strtotime($task->endDate));
        }
        return $taskData;
    }
}


class DHtmlXBehavior
{
    /**
     * @param DataAction $action
     * @return bool
     */
    static function beforeProcessing($action)
    {
        $data = $action->get_data();
        if (isset($data['startDate']) && strlen($data['startDate']) == 16) {
            $action->set_value('startDate', $data['startDate'] . ':00');
        }
        if (isset($data['endDate']) && strlen($data['endDate']) == 16) {
            $action->set_value('endDate', $data['endDate'] . ':00');
        }
        if (!empty($data['responsibleName']) && !empty($data['responsibleEmail'])) {
            if (!empty($data['responsibleId'])) {
                $responsible = Responsible::model()->findByPk($data['responsibleId']);
            } else {
                $responsible = new Responsible();
            }
            $responsible->forename = $data['responsibleName'];
            $responsible->email = $data['responsibleEmail'];
            $responsible->save(false);
            $action->set_value('responsibleId', $responsible->id);
        }
        return true;
    }

    /**
     * @param DataAction $action
     * @return bool
     */
    static function afterProcessing($action)
    {
        //$data = $action->get_data();
        //Task::model()->findByPk()
        return true;
    }

    /**
     * @param DataAction $action
     * @return bool
     */
    static function beforeDelete($action)
    {
        $data = $action->get_data();
        if (isset($data['id'])) {
            /*$model = Task::model()->findByPk($data['id']);*/
           /* if(null === $model) {
                $model = new Task();
                $model->id = $data['id'];
                $model->save();
            }*/
        }
        return true;
    }
}
