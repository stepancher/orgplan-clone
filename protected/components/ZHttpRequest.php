<?php

class ZHttpRequest extends CHttpRequest{

    private $_requestUri;

    public function init()
    {
        parent::init();
    }

    public function setRequestUri($requestUri)
    {
        $this->_requestUri = $requestUri;

        return $this->_requestUri;
    }

    public function getRequestUri()
    {
        if($this->_requestUri===null){
            $this->_requestUri = parent::getRequestUri();
        }

        return $this->_requestUri;
    }
}