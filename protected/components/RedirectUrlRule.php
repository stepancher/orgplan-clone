<?php

class RedirectUrlRule extends CBaseUrlRule {

    /**
     * @param CUrlManager $manager
     * @param string $route
     * @param array $params
     * @param string $ampersand
     * @return bool
     */
    public function createUrl($manager,$route,$params,$ampersand){

        if($route == 'home'){
            return Yii::app()->language . '/';
        }

        return FALSE;
    }

    /**
     * @param CUrlManager $manager
     * @param CHttpRequest $request
     * @param string $pathInfo
     * @param string $rawPathInfo
     * @return bool
     */
    public function parseUrl($manager,$request,$pathInfo,$rawPathInfo){

        $languages = array_keys(Yii::app()->params['translatedLanguages']);

        $requestUri = $request->requestUri;
        $lang = null;

        $userPreferLanguages = Yii::app()->request->getPreferredLanguages();

        if(is_array($userPreferLanguages) && !empty($userPreferLanguages)){

            foreach ($userPreferLanguages as $userPreferLanguage){

                if(in_array($userPreferLanguage, $languages)){
                    Yii::app()->setLanguage($userPreferLanguage);
                    break;
                }
            }
        }

        if (preg_match('#^/(\w{2})/#', $requestUri, $match) || preg_match('#^/(\w{2})$#', $requestUri, $match)) {
            $lang = $match[1];
            if (in_array($lang, $languages)) {
                Yii::app()->setLanguage($lang);
            }
        }

        if(!empty($_POST) || !empty($_FILES) || !empty(json_decode(file_get_contents('php://input'), FILE_USE_INCLUDE_PATH))){
            return FALSE;
        }

        /**
         * Проверка наличия языка в адресной строке
         */
        if (!stristr($requestUri, '/' . Yii::app()->getLanguage())) {
            $requestUri = '/' . Yii::app()->getLanguage() . $requestUri;
            $request->redirect($requestUri);
        }

        if ($requestUri == '/'.Yii::app()->getLanguage().'/site/') {
            $request->redirect(Yii::app()->createUrl('home', array('langId' => Yii::app()->language)));
        }

        if ($requestUri == '/'.Yii::app()->getLanguage().'/site/index' || $requestUri == '/' . Yii::app()->getLanguage() . '/') {

            $app = Yii::app();
            if (!$app->user->isGuest) {
                if (isset(Yii::$app->session['myFairViewId']) && !empty(Yii::$app->session['myFairViewId'])) {
                    $fr = Fair::model()->findByPk(Yii::$app->session['myFairViewId']);
                    $proposalVersion = '';
                    if(!empty($fr->proposalVersion)){
                        $proposalVersion = '/'.$fr->proposalVersion;
                    }

                    if(Yii::app()->user->role == User::ROLE_ORGANIZERS){
                        $request->redirect($proposalVersion.Yii::app()->createUrl('/backoffice/panel/', array('fairId' => Yii::$app->session['myFairViewId'])));
                    }

                    if(Yii::app()->user->role == User::ROLE_EXPONENT){
                        $request->redirect($proposalVersion.Yii::app()->createUrl('/workspace/panel/', array('fairId' => Yii::$app->session['myFairViewId'])));
                    }
                }

                $request->redirect(Yii::app()->createUrl('catalog/html/view', array('langId' => Yii::app()->language)));
            }

            $request->redirect(Yii::app()->createUrl('catalog/html/view', array('langId' => Yii::app()->language)));
        }

        if($requestUri == '/' . Yii::app()->getLanguage() . '/login/agrosalon'){
            $request->redirect(Yii::app()->createUrl('auth/default/auth'));
        }

        if($requestUri == '/' . Yii::app()->getLanguage() . '/login/agrorus'){
            $request->redirect(Yii::app()->createUrl('auth/default/auth'));
        }

        /**
         * Добавление '/' в конец url адресса
         */
        $result = null;
        $parseUrl = parse_url($requestUri);
        if (substr($parseUrl['path'], -7) == '/index/') {
            $result = substr($parseUrl['path'], 0, -7) . '/';
            if (!empty($parseUrl['query'])) {
                $result .= '?' . $parseUrl['query'];
            }
        } elseif (strpos($requestUri, '?')) {
            $n = explode('?', $requestUri, 2);
            if (substr($n[0], -1) != '/') {
                $n[0] .= '/';
                $result = implode('?', $n);
            }
        } elseif (substr($requestUri, -1) != '/') {
            $result = $requestUri . '/';
        }
        if (null != $result) {
            $request->redirect($result, true, 301);
        }

        return FALSE;

    }
}