<?php

/**
 * Class WebUser
 * @property User $user
 *
 */
class WebUser extends CWebUser {

	public $loginUrl = array('/auth/default/login');
	private $_model = null;

	function getRole() {
		if($user = $this->getModel()){
			// в таблице User есть поле role
			return $user->role;
		}
	}

	public function getModel(){
		if (!$this->isGuest && $this->_model === null){
			$this->_model = User::model()->findByPk($this->id/*, array('select' => 'role')*/);
		}
		return $this->_model;
	}

	public function getTariff(){
		$tariff = false;
		if($user = $this->getModel()){
			$tariff = $user->tariffId;
		}
		return $tariff;
	}
}