<?php

class H
{
    public $prefix;

    /**
     * @param EActiveRecord $object
     * @param $fieldName string
     * @param $method string
     * @param $prefix string
     * @return string url
     * @throws Exception
     */
    static function getImageUrl($object, $fieldName, $method = 'noImage', $prefix = '')
    {
        /**
         * @var ObjectFile $file
         */

        if (is_object($object) && $object->{$fieldName . 'Id'} && null !== ($file = $object->{$fieldName})
            && $file instanceof ObjectFile && ($url = $file->getFileUrl($prefix))
        ) {
            try {
                $path = $file->getFilePath(true, DIRECTORY_SEPARATOR);
                $mimetype = mime_content_type($path);
                if($mimetype == 'inode/x-empty'){
                    $info = false;
                    echo "\n".$path." is broken;\n";
                }else{
                    $info = getimagesize($path);
                }
            } catch (Exception $e) {
                $info = false;
            }
            if (false !== $info) {
                return $url;
            }
        }
        if ($method !== null && !method_exists('ObjectFile', $method)) {
            throw new Exception("Class ObjectFile do not have method ($method)");
        }
        return $method ? ObjectFile::$method() : null;
    }

    static function controlGroup($controlsContent, $label = false, $htmlOptions = array())
    {
        $labelOptions = TbArray::popValue('labelOptions', $htmlOptions, array());
        $groupOptions = TbArray::popValue('groupOptions', $htmlOptions, array());
        $for = TbArray::popValue('for', $labelOptions);
        if (empty($labelOptions) || !is_array($labelOptions)) {
            $labelOptions = array(
                'class' => 'control-label'
            );
        }
        if (array($groupOptions) || !is_array($groupOptions)) {
            $groupOptions = array(
                'class' => 'control-group'
            );
        }
        TbHtml::addCssClass('controls', $htmlOptions);
        TbHtml::addCssClass('control-label', $labelOptions);
        TbHtml::addCssClass('control-group', $groupOptions);
        return TbHtml::tag(
            'div', $groupOptions,
            ($label !== false ? TbHtml::label($label, $for, $labelOptions) : '') .
            TbHtml::tag('div', $htmlOptions, $controlsContent)
        );
    }

    static function renderDateTimer($date)
    {
        /** @var CWebApplication $app */
        $time = strtotime($date) - time();
        if ($time > 0) {
            $d = floor($time / (60 * 60 * 24));
            $h = floor(($time % (60 * 60 * 24)) / (60 * 60));
            $m = floor(($time % (60 * 60)) / 60);
            $s = floor($time % 60);
            return self::getCountText($d, array('Дней', 'День', 'Дня')) . ' ' . $h . ':' . $m . ':' . $s;
        }
        return null;
    }

    static function getThumbnailsArray(array $objectArray, $objectId, $fieldName, $type = null, $caption = null, $htmlOptions = array())
    {
        $images = array();
        if (!empty($objectArray) && !empty($fieldName)) {
            foreach ($objectArray as $object) {
                if (is_object($object) && $object->{$fieldName . 'Id'} &&
                    $object->{$fieldName} && $object->{$fieldName} instanceof ObjectFile
                ) {
                    $content = null;
                    if ($caption) {
                        $content = strtr($caption, array(
                            '%7BfileId%7D' => $object->{$fieldName}->id
                        ));
                    }
                    $object->{$fieldName}->setOwnerId($objectId);
                    $path = $object->{$fieldName}->getFileUrl();
                    if ($path) {
                        if ($type && $object->{$fieldName}->type == $type) {
                            $images[] = array(
                                'image' => $path,
                                'caption' => $content,
                                'htmlOptions' => $htmlOptions
                            );
                        } elseif (!$type) {
                            $images[] = array(
                                'image' => $path,
                                'caption' => $content
                            );
                        }
                    }
                }
            }
        }
        return $images;
    }

    static function getCountText($count, array $arrayString, $onlyText = false)
    {
        $text = null;
        $number = null;
        if ($count == 0) {
            $number = $count;
            $text = $arrayString[0];
        } elseif (($length = strlen($count)) > 1 && ($lastNumb = substr($count, $length - 2, 2)) > 10 && $lastNumb < 15) {
            $number = $count;
            $text = $arrayString[0];
        } elseif ($count % 10 == 1) {
            $number = $count;
            $text = $arrayString[1];
        } elseif ($count % 10 > 1 && $count % 10 < 5) {
            $number = $count;
            $text = $arrayString[2];
        } else {
            $number = $count;
            $text = $arrayString[0];
        }
        return !$onlyText ? $number . ' ' . $text : $text;
    }

    static function getRusDate($time)
    {
        $format = array(
            1 => 'Января',
            2 => 'Февраля',
            3 => 'Марта',
            4 => 'Апреля',
            5 => 'Мая',
            6 => 'Июня',
            7 => 'Июля',
            8 => 'Августа',
            9 => 'Сентября',
            10 => 'Октября',
            11 => 'Ноября',
            12 => 'Декабря'
        );
        $date = date('j', $time) . ' ' . $format[(int)date('n', $time)] . ', ' . date('Y', $time) . ' в ' . date('H:i', $time);

        return $date;
    }

//    /**
//     * @param $model
//     * @param $attribute
//     * @param $dataClassName
//     * @param array $htmlOptions
//     * @param $query
//     * @return string
//     */
//    static function dropDownListActiveGroup($model, $attribute, $dataClassName, array $htmlOptions = [], $query = [])
//    {
//        if (!isset($htmlOptions['groupOptions'])) {
//            $htmlOptions['groupOptions'] = [];
//        }
//        if (!isset($htmlOptions['labelOptions'])) {
//            $htmlOptions['labelOptions'] = [];
//        }
//
//        TbHtml::addCssClass('js-drop-down-list drop-down-list', $htmlOptions['groupOptions']);
//        TbHtml::addCssClass('js-drop-button drop-button', $htmlOptions['labelOptions']);
//
//        return TbHtml::activeControlGroup(
//            TbHtml::INPUT_TYPE_DROPDOWNLIST, $model, $attribute, $htmlOptions,
//            ['' => 'Выбрать'] + TbHtml::listData($dataClassName::model()->findAll($query), 'id', 'name')
//        );
//    }

//    /**
//     * @param $object
//     * @param $attribute
//     * @param $withAttribute
//     * @return string
//     */
//    static function getFileOutputHtml($object, $attribute, $withAttribute = true)
//    {
//        $fileList = null;
//        foreach (FieldHasFile::getFieldFiles($object, $attribute) as $data) {
//            $size = '(' . $data['size'] . ')';
//            $fileList .= TbHtml::tag(
//                'div',
//                [],
//                TbHtml::link(
//                    $data['filename'] . ' ' . $size,
//                    Yii::app()->createUrl('file/default/downloadFieldFile', ['id' => $data['id']]),
//                    ['class' => 'f-link']
//                )
//            );
//        }
//        return ($withAttribute ? ($object->{$attribute} ?: Yii::t('system', 'Not set')) : null) . $fileList;
//    }
    
	public static function switcherArray(){
		return array(
			0 => 'Вкл.',
			1 => 'Выкл.',
		);
	}

    /**
     * Список с чекбоксами
     * @param $model
     * @param $attribute
     * @param array $data
     * @param $options
     * @param bool $controlGroup
     * @return string
     */
    public static function getActiveCheckBoxList($model, $attribute, $data = [], $options, $controlGroup = false)
    {
        $result = '';
        $labelClass = isset($options['labelOptions']['class']) ? $options['labelOptions']['class'] : '';
        $inputClass = isset($options['inputOptions']['class']) ? $options['inputOptions']['class'] : '';
        $selected = [];
        if (isset($options['ownerModel'])) {
            /** @var TZ $ownerModel */
            $ownerModel = $options['ownerModel'];

            if ($ownerModel instanceof TZ && $ownerModel->tzAdditionalData) {
                $selected = CHtml::listData($ownerModel->tzAdditionalData, 'additionalDataId', 'additionalDataId');
            }
        }
        $checkedClass = '';
        foreach ($data as $key => $val) {
            if (in_array($key, $selected)) {
                $checkedClass = ' checked';
            }
            $result .= '
                <div class="controls">
                    <label class="' . $labelClass . '">
                        <input class="' . $inputClass . '" ' . $checkedClass . ' type="checkbox" value="' . $key . '" name="' . get_class($model) . '[' . $attribute . '][]">' . $val . '
                    </label>
                </div>
            ';
            $checkedClass = '';
        }

        if ($controlGroup) {
            $label = $model->getAttributeLabel($attribute);
            $result = '<div class="control-group">
                <label class="control-label control-label-for-checkbox">' . $label . '</label>
                    ' . $result . '
            </div>';
        }

        return $result;
    }

    /**
     * Список с чекбоксами
     * @param array $additionalData
     * @param array $tzAdditionalData
     * @param $options
     * @param bool $controlGroup
     * @return string
     */
    public static function getCheckBoxListForDetailView($additionalData = [], $tzAdditionalData = [], $options, $controlGroup = false)
    {
        $result = '';
        $labelClass = isset($options['labelOptions']['class']) ? $options['labelOptions']['class'] : '';
        $inputClass = isset($options['inputOptions']['class']) ? $options['inputOptions']['class'] : '';
        if (!empty($tzAdditionalData)) {
            foreach ($additionalData as $key => $val) {
                $checkBoxVal = '';
                foreach ($tzAdditionalData as $TAD) {
                    if ($key == $TAD) {
                        $checkBoxVal = 'checked="checked"';
                    }
                }
                $result .= '
                <div class="controls">
                    <label class="' . $labelClass . '">
                        <input class="' . $inputClass . '" type="checkbox" ' . $checkBoxVal . ' disabled="disabled">' . $val . '
                    </label>
                </div>
            ';
            }
        }


        if ($controlGroup) {
            $result = '<div class="control-group">
                    ' . $result . '
            </div>';
        }

        return $result;
    }

    public static function getControlGroupBlock($label = '', $fieldData = '')
    {
        $result = '<div class="control-group">
                <label class="control-label">' . $label . '</label>
                    <div class="controls">
                        ' . $fieldData . '
                    </div>
            </div>';

        return $result;
    }

    public static function wrapGridsterElement($element, $row, $col, $sizex = 1, $sizey = 1){
        return "<li data-row='{$row}' data-col='{$col}' data-sizex='{$sizex}' data-sizey='{$sizey}'>{$element}</li>";
    }

    /**
     * @param $data
     * @param int $depth
     * @param bool|TRUE $highlight
     * @param bool|TRUE $exit
     */
    public static function d($data, $depth = 10, $highlight = TRUE, $exit = TRUE){
        CVarDumper::dump($data,$depth,$highlight);
        $exit?exit:NULL;
    }

    /**
     * Получение адресса сайта, кириллицей и латиницей, при передаче параметра $hostOnly, адресс обрезается
     * @param $attribute
     * @param null $hostOnly
     * @return mixed|string
     */
    static function getSiteAddress($attribute, $hostOnly = null)
    {
        $n = '/...';
        $site = '';
        if (stristr($attribute, 'http://') || stristr($attribute, 'https://')) {
            $myParsedURL = parse_url($attribute);
            if (isset($myParsedURL['host'])) {
                if ($myParsedURL['host'] == str_replace(array('http://', 'https://'), '', $attribute) || $myParsedURL['host'] . '/' == str_replace(array('http://', 'https://'), '', $attribute)) {
                    $n = '';
                }
                $site = str_replace(array('http://', 'https://'), '', $hostOnly ? idn_to_utf8($myParsedURL['host']) . $n : $attribute);
            }
        } else {
            $myParsedURL = parse_url('http://' . $attribute);
            if (isset($myParsedURL['host'])) {
                if ($myParsedURL['host'] == $attribute || $myParsedURL['host'] . '/' == $attribute) {
                    $n = '';
                }
                $site = str_replace('http://', '', $hostOnly ? idn_to_utf8($myParsedURL['host']) . $n : $attribute);
            }
        }
        $fullSite = $site;
//        if ($fullSite != '' && !stristr($fullSite, 'www')) {
//            $fullSite = 'www.' . $fullSite;
//        }
        return $fullSite;
    }

    /**
     * @param $content
     * @param bool $exit exit after dump
     * @param int $depth CVarDumper depth
     */
    static function dump($content, $exit = false, $depth = 10) {

        echo '<pre>';
        CVarDumper::dump($content, $depth, true);
        echo '</pre>';
        if ($exit) {
            exit();
        }
    }



    /**
     * @param string $url
     * @return mixed
     */
    public static function changeLanguage($url, $lang)
    {
        $parts    = explode('/', $url);
        $parts[1] = $lang;

        return implode('/', $parts);
    }
}