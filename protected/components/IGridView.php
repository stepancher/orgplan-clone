<?php
/**
 * File AdditionalExpensesGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class AdditionalExpensesGridView
 *
 * @property AdditionalExpenses $model
 */
class IGridView extends TbGridView
{
	/**
	 * Модель AdditionalExpenses::model() or new AdditionalExpenses('scenario')
	 * @var AdditionalExpenses
	 */
	public $model;

	public $filterEnable = true;

	/**
	 * Двумерный массив аргументов для создания условий CDbCriteria::compare
	 * @see CDbCriteria::compare
	 * @var array
	 */
	public $compare = array(
		array()
	);
	/**
	 * Для добавления колонок в начало таблицы
	 * @var array
	 */
	public $columnsPrepend = array();

	/**
	 * Для добавления колонок в конец таблицы
	 * @var array
	 */
	public $columnsAppend = array();

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate {@link columns} objects.
	 */
	public function init()
	{
		// Включаем фильтрацию по сценарию search
		$this->model->setScenario('search');
		if (false !== $this->filterEnable) {
			$this->filter = ARGridView::createFilter($this->model, true);
		}


		// Создаем провайдер данных для таблицы
		$this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

		if(false === $this->enablePagination)
			$this->dataProvider->getPagination()->pageSize = $this->dataProvider->getTotalItemCount();

		// Инициализация
		parent::init();
	}

	public function initColumns()
	{
		foreach ($this->columns as &$column) {
			if (!isset($column['class'])) {
				$column['class'] = 'IDataColumn';
			}
		}
		parent::initColumns();
	}
}

class IDataColumn extends CDataColumn
{
	/**
	 * Renders the header cell.
	 */
	public function renderHeaderCell()
	{
		$this->headerHtmlOptions['id'] = $this->id;
		echo CHtml::openTag('td', $this->headerHtmlOptions);

		if($this->grid->enableSorting && $this->sortable && $this->name!==null)
			echo $this->grid->dataProvider->getSort()->link($this->name,$this->header,array('class'=>'sort-link'));
		elseif($this->name!==null && $this->header===null)
		{
			if($this->grid->dataProvider instanceof CActiveDataProvider)
				echo CHtml::encode($this->grid->dataProvider->model->getAttributeLabel($this->name));
			else
				echo CHtml::encode($this->name);
		}
		else
			parent::renderHeaderCellContent();

		echo "</td>";
	}
}