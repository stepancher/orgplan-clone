<?php

abstract class ShardedAR extends \AR {

    public static $_db = NULL;

    public function getDbConnection()
    {
        if(self::$_db===null) {
            throw new CDbException('No database connection.');
        }

        return self::$_db;
    }

}