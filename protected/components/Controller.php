<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $actions = array();
	public $filters = array();
/*
	public $filters = [
		[
			'application.filters.PerformanceFilter'
		]
	];
*/
	public $behaviors = array();

	public $_pageTitle;

	public function __construct($id,$module=null){
		parent::__construct($id,$module);
	}

	public function actions()
	{
		return $this->actions;
	}

	public function filters()
	{
		return $this->filters;
	}

	public function behaviors()
	{
		return $this->behaviors;
	}

	public function render($view, $data = null, $return = false)
	{
		if (!Yii::app()->request->isAjaxRequest) {
			parent::render($view, $data, $return);
		} else {
			parent::renderPartial($view, $data, $return);
		}
	}

	protected function afterAction($action) {
		$actionRoute = $action->controller->getUniqueId() . '/' . $action->id;
		$actionParams = array_merge($this->getActionParams(), $_POST);
/*
		$file = 'logs.txt';
		$current = file_get_contents($file);
		$current .= print_r($actionRoute, true). "\n";
		$current .= print_r($actionParams, true);
		file_put_contents($file, $current);
*/
	}

	public function redirect($url, $terminate=true, $statusCode=302) {

		self::afterAction($this->action);
		parent::redirect($url, $terminate, $statusCode);
	}

	/**
	 * @return string the page title. Defaults to the controller name and the action name.
	 */
	public function getPageTitle() {

		if ($this->_pageTitle !== null) {
			return $this->_pageTitle;
		} else {
			$name = ucfirst(basename($this->getId()));
			if ($this->getAction() !== null && strcasecmp($this->getAction()->getId(), $this->defaultAction)) {
				return $this->_pageTitle = ucfirst($this->getAction()->getId()) . ' ' . $name . ' | '. Yii::app()->params['titleName'];
			} else {
				return $this->_pageTitle = $name . ' | '. Yii::app()->params['titleName'];
			}
		}
	}

	/**
	 * @param string $value the page title.
	 */
	public function setPageTitle($value) {

		$this->_pageTitle = $value;
	}
}