<?php

class ZAssetManager extends CAssetManager {

    public function publish($path,$hashByName=false,$level=-1,$forceCopy=null){
        $_timer = Pinba::start(array(
            'category' => 'assets',
            'group' => 'assets::publish',
        ));

        $result = parent::publish($path, $hashByName, $level, $forceCopy);

        Pinba::stop($_timer);

        return $result;
    }

}