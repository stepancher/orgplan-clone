<?php

class ZUrlRule extends CUrlRule {

    public function createUrl($manager,$route,$params,$ampersand){

        if(!isset($params['langId'])){

            $params += [
                'langId' => Yii::app()->language,
            ];
        }

        return parent::createUrl($manager,$route,$params,$ampersand);
    }

}