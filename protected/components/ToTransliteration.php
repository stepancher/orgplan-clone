<?php

class ToTransliteration
{
	/**
	 * Таблица транслитерации ГОСТ 7.79-2000:
	 * @var array
	 */
	public $map = [
		'а' => 'a', 'б' => 'b', 'в' => 'v',
		'г' => 'g', 'д' => 'd', 'е' => 'e',
		'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
		'и' => 'i', 'й' => 'j', 'к' => 'k',
		'л' => 'l', 'м' => 'm', 'н' => 'n',
		'о' => 'o', 'п' => 'p', 'р' => 'r',
		'с' => 's', 'т' => 't', 'у' => 'u',
		'ф' => 'f', 'х' => 'kh', 'ц' => 'c',
		'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh',
		'ь' => '', 'ы' => 'y', 'ъ' => '',
		'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
		'А' => 'A', 'Б' => 'B', 'В' => 'V',
		'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
		'Ё' => 'E', 'Ж' => 'ZH', 'З' => 'Z',
		'И' => 'I', 'Й' => 'J', 'К' => 'K',
		'Л' => 'L', 'М' => 'M', 'Н' => 'N',
		'О' => 'O', 'П' => 'P', 'Р' => 'R',
		'С' => 'S', 'Т' => 'T', 'У' => 'U',
		'Ф' => 'F', 'Х' => 'KH', 'Ц' => 'C',
		'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH',
		'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
		'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',
	];
	public $charset = 'utf-8';

	static function getInstance()
	{
		return new static;
	}

	public function setMap($map)
	{
		$this->map = $map;
		return $this;
	}

	public function setCharset($charset)
	{
		$this->charset = $charset;
		return $this;
	}

	public function replace($text, $that = '-')
	{
		$text = html_entity_decode($text);
		$text = strip_tags($text);
		$text = mb_strtolower($text, $this->charset);
		$text = strtr($text, $this->map);
		$text = strtr($text, ['–' => '-']); // хитрое тире
		$text = strtr($text, ['\'' => '']);
		$text = strtr($text, ['«' => '']);
		$text = strtr($text, ['»' => '']);
		$text = strtr($text, ['&' => 'and']);
		$text = strtr($text, ['!' => '']);
		$text = strtr($text, [':' => '']);
		$text = strtr($text, [';' => '']);
		$text = strtr($text, [',' => '-']);
		$text = strtr($text, ['.' => '-']);
		$text = strtr($text, ['/' => '']);
		$text = strtr($text, ['№' => '-']);
		$text = strtr($text, ['’' => '']);
		$text = strtr($text, ['\\' => '']);
		$text = preg_replace('/[^сca-z0-9а-яА-ЯёЁсС!]+/i', ' ', strip_tags($text));
		$text = strtr(trim($text), ' ', $that);
		return $text;
	}
}