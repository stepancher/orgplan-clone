<?php

class DbMan extends CComponent {

    const DRIVER_DB_CONNECTION = 'db';

    const SHARD_CONNECTION_CLASS = 'ZDbConnection';
    const SHARD_CONNECTION_HOST  = 'localhost';
    const SHARD_CONNECTION_USER  = 'root';
    const SHARD_CONNECTION_PASS  = '';
    const SHARD_CONNECTION_DB_NAME_PATTERN  = 'orgplan_proposal_f{fairId}';

    public function init(){}

    public function getDbName(CDbConnection $connection){

        list($peace1, $peace2, $dbName) = explode('=', $connection->connectionString);

        return $dbName;
    }

    public function getShardConnection($dbName){

        return Yii::createComponent(array(
            'connectionString' => "mysql:host=localhost;dbname={$dbName}",
            'class'    => self::SHARD_CONNECTION_CLASS,
            'username' => self::SHARD_CONNECTION_USER,
            'password' => self::SHARD_CONNECTION_PASS,
            'tablePrefix' => 'tbl_',
            'charset' => 'utf8',
        ));
    }

    public function createShardName($fairId){

        $result = str_replace("{fairId}", $fairId, self::SHARD_CONNECTION_DB_NAME_PATTERN);

        if(!empty(Yii::app()->name)){
            $result = $result. '_' . strtolower(str_replace('-', '_', Yii::app()->name));
        }

        if($fairId == 184){
            list($peace1, $peace2, $dbProposal) = explode('=', Yii::app()->dbProposal->connectionString);
            $result = $dbProposal;
        }

        return $result;
    }

    public function createShardDb($dbName){

        $sql = "CREATE DATABASE `{$dbName}` /*!40100 DEFAULT CHARACTER SET utf8 */;";

        Yii::app()->{self::DRIVER_DB_CONNECTION}->createCommand($sql)->execute();
    }

    public function copyTableToDb($tableName, $sourceDbName, $targetDbName, CDbConnection $targetConn){
        $sql = "
            CREATE TABLE `{$targetDbName}`.`{$tableName}` LIKE `{$sourceDbName}`.`{$tableName}`;
            INSERT INTO  `{$targetDbName}`.`{$tableName}` SELECT * FROM `{$sourceDbName}`.`{$tableName}`;  
        ";

        $targetConn->createCommand($sql)->execute();
    }

}