<?php

/**
 * Created by PhpStorm.
 * User: SoundGoodizer
 * Date: 13.04.2015
 * Time: 9:52
 */
class ObjectRelationCopier
{
	/**
	 * @var CActiveRecord
	 */
	public $newParentObject;

	/**
	 * @param $object
	 * @param $relationMap
	 * @param callable $callback
	 */
	public function __construct($object, $relationMap, $callback = null)
	{
		if ($object) {
			$this->newParentObject = is_callable($callback) ? call_user_func($callback, $object) : $this->copyObject($object);
			$this->resolveRelations($relationMap, $object);
		}
	}

	/**
	 * @param $map
	 * @param CActiveRecord $object
	 */
	protected function resolveRelations($map, $object)
	{
		foreach ($map as $key => $rel) {
			$relName = is_string($rel) ? $rel : $key;
			if (isset($object->relations()[$relName]) && null !== ($relObject = $object->{$relName})) {
				$options = $object->relations()[$relName];
				if (is_array($relObject)) {
					foreach ($relObject as $k => $item) {
						$newObject = $this->copyObject($item, $options);
						if (is_array($rel)) {
							$this->newParentObject = $newObject;
							$this->resolveRelations($rel, $item);
						}
					}
				} else {
					$newObject = $this->copyObject($relObject, $options);
					if (is_array($rel)) {
						$this->newParentObject = $newObject;
						$this->resolveRelations($rel, $relObject);
					}
				}
			}
		}
	}

	/**
	 * @param CActiveRecord $object
	 * @param array $options
	 * @return CActiveRecord
	 */
	public function copyObject($object, $options = array())
	{
		$className = get_class($object);
		/** @var CActiveRecord $newObject */
		$newObject = new $className;
		$newObject->setAttributes($object->attributes, false);
		$newObject->id = null;

		if ($this->newParentObject instanceof CActiveRecord && !empty($options)) {
			$relField = array_keys($options[2])[0];
			$relType = $options[0];
			if ($relType == CActiveRecord::HAS_ONE || $relType == CActiveRecord::HAS_MANY) {
				$newObject->{$relField} = $this->newParentObject->id;
				$newObject->save();
			} elseif ($relType == CActiveRecord::BELONGS_TO) {
				$newObject->save();
				$this->newParentObject->{$relField} = $newObject->id;
				$this->newParentObject->save();
			}
		}

		return $newObject;
	}

}