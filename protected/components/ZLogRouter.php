<?php

class ZLogRouter extends CLogRouter{

    const SENTRY_LOG_ROUTE_NAME = 'RSentryLogRoute';

    public function processLogs(){
        $_timer = Pinba::start(array(
            'category' => 'logs',
            'group' => 'logs::processLogs',
        ));

        $result = parent::processLogs();

        Pinba::stop($_timer);
    }

    /**
     * @return null|RSentryLogRoute
     */
    public static function getSentryLogRoute(){

        $logRoutes = reset(Yii::app()->log->routes);
        array_walk($logRoutes,
            function (&$route){
                $route = get_class($route);
            }
        );
        $sentryRouteKey = array_search(self::SENTRY_LOG_ROUTE_NAME, $logRoutes);

        /** @var RSentryLogRoute $sentryLogRoute */
        $sentryLogRoute = Yii::app()->log->routes[$sentryRouteKey];

        if(!isset($sentryLogRoute)){
            return NULL;
        }

        return $sentryLogRoute;
    }

    /**
     * @return bool|null
     */
    public static function getSentryLogRouteEnabled(){

        $sentryLogRoute = self::getSentryLogRoute();

        if(isset($sentryLogRoute)){
            return $sentryLogRoute->enabled;
        }

        return NULL;
    }

    public static function sentryLogRouteEnable(){

        $sentryLogRoute = self::getSentryLogRoute();

        if(isset($sentryLogRoute)){
            $sentryLogRoute->enabled = TRUE;
        }
    }

    public static function sentryLogRouteDisable(){

        $sentryLogRoute = self::getSentryLogRoute();

        if(isset($sentryLogRoute)){
            $sentryLogRoute->enabled = FALSE;
        }
    }
}