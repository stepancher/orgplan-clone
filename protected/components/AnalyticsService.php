<?php

Yii::import('application.modules.gradoteka.extensions.ZGridView.ZGridView');
Yii::import('ext.ZLineChart.ZLineChart');
Yii::import('application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart');
Yii::import('app.modules.gradoteka.models.*');
Yii::import('app.modules.gradoteka.GradotekaModule');

class AnalyticsService extends CComponent{

    const DEFAULT_EPOCH = 2015;

    const WIDGET_ZEASYPIECHART = 1;
    const WIDGET_ZEASYLINECHART = 2;

    const CONTAINER_TYPE_1 = 1;
    const CONTAINER_TYPE_2 = 2;

    /**
     * @param GValue $data
     * @param Chart $widget
     * @return array
     */
    public static function pie_chart_params($data, $widget){

        $percent = 0;
        $ratingPoint = '?';

        if(!empty($data)){
            $topValue = $data->getTopValue();

            if(!empty($topValue)){
                $percent  = $data?($data->value/($data->getTopValue()/100)):0;
                $ratingPoint = $data?$data->getRatingPoint():'-';
            }
        }

        return array(
            'header'          => $widget->header,
            'type'            => $widget->widgetType,
            'labelName'       => $data?self::filterUnits($widget->unit):'-',
            'innerValueName'  => $widget->measure,
            'barColor'        => $widget->color,
            'innerValueDigit' => $ratingPoint,
            'labelDigit'      => $data?number_format($data->value, 0, ',', ' '):'—',
            'htmlOptions'     => array('data-percent' => $percent),
            'epoch'           => $data ? Yii::t('GradotekaModule.gradoteka','Data for the year', array('$epoch' => $data->epoch)) : Yii::t('GradotekaModule.gradoteka','data not available'),
        );
    }

    /**
     * @param GValue $data
     * @param Chart $widget
     * @return array
     */
    public static function line_params($data, $widget){

        /*
         * @TODO refactor if statement.
         */
        $measure = '-';
        if($widget->measure){
            $measure = $widget->measure;
        }elseif(isset($data->set) && isset($data->set->stat) && isset($data->set->stat->units)){
            $measure = $data->set->stat->units;
        }

        return array(
            'name'          => $widget->header,
            'type'          => ZLineChart::TYPE_BASIC,
            'color'         => $widget->color,
            'valueType'     => $measure,
            'ratingPlace'   => $data?$data->getRatingPoint():'-',
            'value'         => $data?number_format($data->value, 1, ',', ' '):'-',
            'percent'       => $data?$data->value/($data->getTopValue()/100):'-',
            'epoch'         => $data ? Yii::t('GradotekaModule.gradoteka','Data for the year', array('$epoch' => $data->epoch)) : Yii::t('GradotekaModule.gradoteka','data not available'),
        );
    }

    /**
     * @param GValue $data
     * @param Chart $widget
     * @return array
     */
    public static function line_stacked_params($data, $widget){
        return array(
            'name'              => $widget->header,
            'type'              => ZLineChart::TYPE_STACKED,
            'valueType'         => $widget->measure,
            'second_valueType'  => $widget->rel?$widget->rel->measure:'-',
            'color'             => $widget->color?$widget->color:ZLineChart::COLOR_GREEN,
            'colorSecond'       => (isset($widget->rel, $widget->rel->color) && !empty($widget->rel->color))?$widget->rel->color:ZLineChart::COLOR_RED,
            'percent'           => $data?$data->value:'-',
            'second_percent'    => (
                $data &&
                isset($widget->rel)
            ) ?
                GValue::getByGIds(
                    $widget->rel->typeId,
                    $data->set->objId,
                    $data->epoch
                )['value']
        :'-',
            'epoch'           => $data ? Yii::t('GradotekaModule.gradoteka','Data for the year', array('$epoch' => $data->epoch)) : Yii::t('GradotekaModule.gradoteka','data not available'),
        );
    }

    /**
     * @param $id
     * @return null|string
     */
    public static function getWidgetAlias($id){
        $widgets = array(
            self::WIDGET_ZEASYPIECHART  => 'application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart',
            self::WIDGET_ZEASYLINECHART => 'ext.ZLineChart.ZLineChart',
        );

        if(isset($widgets[$id])){
            return $widgets[$id];
        }

        return NULL;
    }

    public static function initParams($widget){

        if(($widget->widget == self::WIDGET_ZEASYLINECHART) && ($widget->widgetType == ZLineChart::TYPE_STACKED)){
            return function($data) use($widget){
                return self::line_stacked_params($data, $widget);
            };
        }
        if(($widget->widget == self::WIDGET_ZEASYLINECHART) && ($widget->widgetType == ZLineChart::TYPE_BASIC)){
            return function($data) use($widget){
                return self::line_params($data, $widget);
            };
        }

        return function($data) use($widget){
            return self::pie_chart_params($data, $widget);
        };
    }

    /**
     * @return array
     */
    public static function loadWidgetsByGroup($groupId){
        return self::loadWidgets(Chart::getByGroup($groupId));
    }

    /**
     * @return array
     */
    public static function loadWidgetsByParent($parentId){
        return self::loadWidgets(Chart::getByParent($parentId));
    }


    /**
     * @param array $charts Array of Charts objects
     * @return array
     */
    public static function loadWidgets($charts){
        return array_map(
            function($widget){
                return array(
                    'widget' => self::getWidgetAlias($widget->widget),
                    'gTypeId' => $widget->typeId,
                    'params' => self::initParams($widget),
                    'widgets' => array(self::loadWidgetsByParent($widget->id)),
                );
            },
            $charts
        );
    }

    /**
     * @param integer $industryId
     * @return array|mixed|null
     */
    public static function widgets($industryId){

        $criteria = new CDbCriteria();
        $criteria->addCondition('industryId = :industryId');
        $criteria->params[':industryId'] = $industryId;

        return ChartGroup::model()->findAll($criteria);
    }

    /**
     * @param array $chart Result of method loadWidgets()
     * @param $regionId
     * @return mixed
     * @throws CException
     */
    public static function loadParams($chart, $regionId, $epoch){

        /** @var GObjectHasRegion $region */
        $region = GObjectHasRegion::model()->findByAttributes(array('regionId' => $regionId));

        if(empty($region)){
            return false;
        }

        $gValue = GValue::getByGIds($chart['gTypeId'], $region->objId, $epoch);

        if($gValue === NULL){
            $gValue = GValue::getByGIds($chart['gTypeId'], $region->objId, $epoch-1);
        }

        if(!is_callable($chart['params'])){
            throw new CException('objects is not callable');
        }

        $closure = $chart['params'];
        $chart['params'] = $closure($gValue);

        return $chart['params'];
    }

    /**
     * @param $str
     * @return mixed
     */
    public static function filterUnits($str){
        return str_replace('&lt;i class="rub"&gt;q&lt;/i&gt;', 'руб.', $str);
    }

    /**
     * @return array
     */
    public static function getWidgetsTypes(){
        return array(
            self::WIDGET_ZEASYLINECHART => 'Линейный',
            self::WIDGET_ZEASYPIECHART => 'Круговой',
        );
    }

    /**
     * @param $id
     * @return array
     */
    public static function getWidgetTypes($id){

        $widgets = array(
            self::WIDGET_ZEASYLINECHART  => array(
                ZLineChart::TYPE_BASIC => 'Стандартный',
                ZLineChart::TYPE_STRIPED => 'С полосами',
                ZLineChart::TYPE_STACKED => 'Составной',
            ),
            self::WIDGET_ZEASYPIECHART => array(
                ZEasyPieChart::BIG_BOLD => 'Большой широкий',
                ZEasyPieChart::BIG_SLIM => 'Большой узкий',
                ZEasyPieChart::SMALL_BOLD => 'Маленький широкий',
                ZEasyPieChart::SMALL_SLIM => 'Маленький узкий',
            ),
        );

        if(isset($widgets[$id])){
            return $widgets[$id];
        }

        return array();
    }

    /**
     * @param $id
     * @return null
     */
    public static function getWidgetTypeById($id){

        $widgetTypes = self::getWidgetsTypes();

        if(!empty($id) && is_numeric($id)){
            return $widgetTypes[$id];
        }

        return NULL;
    }

    /**
     * @param $typeId
     * @param $subtypeId
     * @return bool
     */
    public static function getWidgetSubTypeById($typeId, $subtypeId){

        if(!empty($typeId) && is_numeric($typeId)){
            $widgetType = self::getWidgetTypes($typeId);
        }

        if(!empty($subtypeId) && is_numeric($subtypeId) && isset($widgetType) && !empty($widgetType)){
            return $widgetType[$subtypeId];
        }else{
            return FALSE;
        }
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws CException
     */
    public static function renderChartById($id){

        /** @var Chart $chart */
        $chart = Chart::model()->findByPk($id);

        if($chart === NULL){
            return NULL;
        }

        $widgetAlias = self::getWidgetAlias($chart->widget);

        if($widgetAlias === NULL){
            return '';
        }

        $data = self::loadWidgets(array($chart));

        $widget = reset($data);

        return Yii::app()->controller->widget($widgetAlias, self::loadParams($widget, 23, self::DEFAULT_EPOCH), true);
    }

    /**
     * @param $statId
     * @param $epoch
     * @param $accessory
     * @param $numberFormat
     * @return string
     */
    public static function getStateHeader($statId, $epoch, $accessory, $numberFormat){

        if($accessory !== NULL && $accessory != 'country'){
            return '';
        }

        $rus = TrGObjects::model()->findByAttributes(['trParentId' => GObjects::RUSSIA_COUNTRY_ID, 'langId' => Yii::app()->language]);
        $GObjectsHasGStatTypes = GObjectsHasGStatTypes::model()->findByAttributes(
            array(
                'statId' => $statId,
                'objId' => isset($rus->trParentId) ? $rus->trParentId : NULL,
            )
        );

        if($GObjectsHasGStatTypes === NULL){
            return '';
        } else{

            for($i = 0; $i <= 3; $i++){

                $GValue = GValue::model()->findByAttributes(
                    array(
                        'setId' => $GObjectsHasGStatTypes->id,
                        'epoch' => $epoch - $i,
                    )
                );

                if($GValue !== NULL)
                    break;
            }

            return isset($GValue->value) ? number_format($GValue->value, $numberFormat, ',',' ') : '';
        }

    }

    /**
     * @param $col
     * @return array
     */
    public static function getCountryHeader($col) {

        $colName = ZGridView::COLUMN_PREFIX.$col->position;

        $sql = "
            SELECT 
            CASE WHEN (SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = {$col->epoch}) THEN gv.value END)) IS NOT NULL
                         THEN  SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = {$col->epoch}) THEN gv.value END)
                         WHEN (SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-1).") THEN gv.value END)) IS NOT NULL
                         THEN  SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-1).") THEN gv.value END)
                         WHEN (SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-2).") THEN gv.value END)) IS NOT NULL
                         THEN  SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-2).") THEN gv.value END) END {$colName}
            FROM tbl_gvalue gv 
             JOIN tbl_gobjectshasgstattypes `set` on `set`.id = gv.setId
             JOIN tbl_gstattypes stat on stat.gId = set.statId
             JOIN tbl_gobjects obj on obj.id = set.objId
             JOIN tbl_gstatgrouphasstat sghs on sghs.gObjectHasGStatTypeId = set.id
             JOIN tbl_gstatgroup sg on sg.id = sghs.statGroupId
             JOIN tbl_gstattypegroup gstg on gstg.gId = stat.gId
             WHERE obj.visible IS NOT NULL
        ";

        $sqlData = new CSqlDataProvider($sql, array(
            'keyField' => 'name',
        ));

        $data = $sqlData->getData();

        return $data['0'];
    }

    /**
     * @param $columns
     * @param $industryId
     * @return CSqlDataProvider
     */
    public static function getCountryColumnDataProvider($columns, $industryId){

        $cols = array_map(
            function($col){
                /** @var ChartColumn $col */
                $colName = ZGridView::COLUMN_PREFIX.$col->position;
                return "
                    CASE WHEN (SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = {$col->epoch}) THEN gv.value END)) IS NOT NULL
                         THEN  SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = {$col->epoch}) THEN gv.value END)
                         WHEN (SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-1).") THEN gv.value END)) IS NOT NULL
                         THEN  SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-1).") THEN gv.value END)
                         WHEN (SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-2).") THEN gv.value END)) IS NOT NULL
                         THEN  SUM(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-2).") THEN gv.value END) END {$colName},
                    
                    CASE WHEN (MAX(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = {$col->epoch}) THEN gv.value END)) IS NOT NULL
                         THEN  {$col->epoch}
                         WHEN (MAX(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-1).") THEN gv.value END)) IS NOT NULL
                         THEN  ".($col->epoch-1)."
                         WHEN (MAX(CASE WHEN (gstg.groupId = {$col->statId} and gv.epoch = ".($col->epoch-2).") THEN gv.value END)) IS NOT NULL
                         THEN  ".($col->epoch-2)." END {$colName}Year,
                         
                    GROUP_CONCAT(DISTINCT trcnst.name SEPARATOR ';') AS {$colName}Hint
                 ";
            },
            $columns
        );


        $obj = 'trsg.name,';
        $group = 'GROUP BY sg.id';

        $sql = "
            SELECT 
            $obj
            ".implode(',', $cols)."
            FROM tbl_gvalue gv 
             JOIN tbl_gobjectshasgstattypes `set` on `set`.id = gv.setId
             JOIN tbl_gstattypes stat on stat.gId = set.statId
             JOIN tbl_gobjects obj on obj.id = set.objId
             LEFT JOIN tbl_trgobjects trobj ON trobj.trParentId = obj.id AND trobj.langId = :langId
             JOIN tbl_gstatgrouphasstat sghs on sghs.gObjectHasGStatTypeId = set.id
             JOIN tbl_gstatgroup sg on sg.id = sghs.statGroupId
             LEFT JOIN tbl_trgstatgroup trsg ON trsg.trParentId = sg.id AND trsg.langId = :langId
             JOIN tbl_gstattypegroup gstg on gstg.gId = stat.gId
             LEFT JOIN tbl_gstatgroupconstituents cnst ON cnst.gstatgroupId = sg.id
             LEFT JOIN tbl_trgstatgroupconstituents trcnst ON trcnst.trParentId = cnst.id AND trcnst.langId = :langId
             WHERE sg.industryId = $industryId
             AND obj.visible IS NOT NULL
            $group
        ";

        $sort = new CSort;
        $sort->defaultOrder = 'column_1 DESC';

        $sort->attributes = array(
            'name' => array(
                'asc' => 'trobj.name',
                'desc' => 'trobj.name DESC',
            ),
        );

        foreach ($columns as $column){
            $sort->attributes += array(
                ZGridView::COLUMN_PREFIX.'_'.$column->position => array(
                    'asc' => ZGridView::COLUMN_PREFIX.$column->position,
                    'desc' => ZGridView::COLUMN_PREFIX.$column->position . ' DESC',
                ),
            );
        }
        
        return new CSqlDataProvider($sql, array(
            'keyField' => 'name',
            'sort' => $sort,
            'params' => [
                ':langId' => Yii::app()->language
            ]
        ));
    }

    /**
     * @param $columns
     * @param $area
     * @param bool $agg
     * @return CSqlDataProvider
     */
    public static function getColumnsDataProvider($columns, $area, $agg = FALSE){

        $cols = array_map(
            function($col){
                /** @var ChartColumn $col */
                $colName = ZGridView::COLUMN_PREFIX.$col->position;
                return "
                         CASE WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = {$col->epoch}) THEN gv.value END) IS NOT NULL)
                                THEN MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = {$col->epoch}) THEN gv.value END) 
                              WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-1).") THEN gv.value END) IS NOT NULL)
                                THEN MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-1).") THEN gv.value END) 
                              WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-2).") THEN gv.value END) IS NOT NULL)
                                THEN MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-2).") THEN gv.value END) 
                              WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-3).") THEN gv.value END) IS NOT NULL)
                                THEN MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-3).") THEN gv.value END)
							   END {$colName},
                       CASE WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = {$col->epoch}) THEN gv.value END) IS NOT NULL)
							    THEN {$col->epoch} 
							WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-1).") THEN gv.value END) IS NOT NULL)
							    THEN ".($col->epoch-1)."
                            WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-2).") THEN gv.value END) IS NOT NULL)
							    THEN ".($col->epoch-2)."
                            WHEN(MAX(CASE WHEN (stat.gId = \"{$col->statId}\" AND gv.epoch = ".($col->epoch-3).") THEN gv.value END) IS NOT NULL)
							    THEN ".($col->epoch-3)."
							   END {$colName}Year
                 ";
            },
            $columns
        );

        if($agg){

            $obj = '';
            $group = '';

        } else{

            $obj = 'trobj.name,';
            $group = 'GROUP BY obj.id';

        }

        $sql = "
            SELECT 
            $obj
            ".implode(',', $cols)."
            FROM tbl_gobjects obj
            LEFT JOIN tbl_trgobjects trobj ON trobj.trParentId = obj.id AND trobj.langId = :langId
            LEFT JOIN tbl_gobjectshasgstattypes `set` ON `set`.objId = obj.id
            LEFT JOIN tbl_gstattypes stat ON `set`.statId = stat.gId
            LEFT JOIN tbl_gvalue gv ON `set`.id = gv.setId
            WHERE obj.gType = \"$area\"
            AND obj.visible IS NOT NULL
            $group
        ";

        $sort = new CSort;
        $sort->defaultOrder = 'column_1 DESC';

        $sort->attributes = array(
            'name' => array(
                'asc' => 'trobj.name',
                'desc' => 'trobj.name DESC',
            ),
        );

        foreach ($columns as $column){
            $sort->attributes += array(
                ZGridView::COLUMN_PREFIX.'_'.$column->position => array(
                    'asc' => ZGridView::COLUMN_PREFIX.$column->position,
                    'desc' => ZGridView::COLUMN_PREFIX.$column->position . ' DESC',
                ),
            );
        }

        return new CSqlDataProvider($sql, array(
            'keyField' => 'name',
            'sort' => $sort,
            'params' => [
                ':langId' => Yii::app()->language
            ]
        ));

    }

    /**
     * @param $columns
     * @return array
     */
    public static function getColumnNames($columns){
        $columnNames = array();

        if(empty($columns) || !is_array($columns)){
            return $columnNames;
        }

        foreach($columns as $column){
            $columnNames[] = ZGridView::COLUMN_PREFIX.$column->position;
        }

        return $columnNames;
    }

    /**
     * @param $columns
     * @return array
     */
    public static function getColumnNames2($columns){
        $columnNames = array();

        if(empty($columns) || !is_array($columns)){
            return $columnNames;
        }

        foreach($columns as $column){
            $columnNames[] = ZGridView::COLUMN_PREFIX.'_'.$column->position;
        }

        return $columnNames;
    }

    /**
     * @param $statId
     * @param $epoch
     * @param $objects
     * @return float|int
     */
    public static function getSumByStat($statId, $epoch, $objects){

        $values = GValue::getValuesByStat($statId, $epoch, $objects);

        //@TODO REFACTOR getting old year value if currrent year value is null
        if(!is_array($values) || empty($values)){

            $values = GValue::getValuesByStat($statId, $epoch - 1, $objects);

        }
        $sum = 0;
        foreach($values as $value){
            $sum += round($value->value);
        }

        return $sum;
    }

    /**
     * @param $statId
     * @param $epoch
     * @param $objects
     * @return float
     */
    public static function getPercentSumByStat($statId, $epoch, $objects){

        $values = GValue::getValuesByStat($statId, $epoch, $objects);

        //@TODO REFACTOR getting old year value if currrent year value is null
        if(!is_array($values) || empty($values)){

            $values = GValue::getValuesByStat($statId, $epoch - 1, $objects);

        }

        $sum = 0;
        $iterator = 0;
        foreach($values as $value){

            $sum += round($value->value, 1);
            $iterator++;
        }

        if($iterator == 0){
            $iterator = 1;
        }

        return number_format($sum/$iterator,1,',',' ');
    }

}