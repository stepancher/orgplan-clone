<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_TIME_BLOCK = 300;
	const ERROR_ATTEMPT_BLOCK = 20;

	public function authenticate()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('contact');
		$criteria->compare('contact.email', $this->username);

		/** @var User $record */
		$record = User::model()->find($criteria);
		if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ((empty($this->password) || !password_verify($this->password, $record->password)) && $record->authFailCount < self::ERROR_ATTEMPT_BLOCK) {

            $this->errorCode = self::ERROR_PASSWORD_INVALID;

			$record->attributes = [
				'lastAuthFailDatetime' => date('Y-m-d H:i:s'),
				'authFailCount' => $record->authFailCount + 1,
				'authFailTotalCount' => $record->authFailTotalCount + 1,
			];
			$record->save();
        } elseif($record->authFailCount >= self::ERROR_ATTEMPT_BLOCK){

			$datetime1 = new DateTime($record->lastAuthFailDatetime);
			$datetime2 = new DateTime(date('Y-m-d H:i:s'));
			$interval = $datetime1->diff($datetime2);

			if(
				isset($interval) && (
					!empty($interval->format('%y')) ||
					!empty($interval->format('%m')) ||
					!empty($interval->format('%d')) ||
					!empty($interval->format('%h')) ||
					$interval->format('%i') >= 5
				)
			  )
			{
				$record->authFailCount = 0;
				$record->save();
				$this->authenticateByRecord($record);

			} else {
				$this->errorCode = self::ERROR_TIME_BLOCK;
			}
		} else {

			if($record->authFailCount != 0){

				$record->authFailCount = 0;
				$record->save();
			}

			$this->authenticateByRecord($record);
		}
		return !$this->errorCode;
	}

	public function authenticateByRecord($record, $errorCode = true)
	{
		/** @var User $record */
		$this->_id = $record->id;
		$this->setState('role', $record->role);
		$this->setState('login', $record->login);

		/** Запись даты последнего входа на сайт */
		$record->lastEntry = date('Y-m-d H:i:s');
		$record->save(false);

		if ($errorCode) {
			$this->errorCode = self::ERROR_NONE;
		}
		return $this;
	}

	public function getId()
	{
		return $this->_id;
	}
}