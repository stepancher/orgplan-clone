<?php

/**
 * Created by PhpStorm.
 * User: SoundGoodizer
 * Date: 26.03.2015
 * Time: 17:17
 */
class Planner5D
{
	/** @const API_KEY TODO: вынести в настройки сайта (создать tbl_configuration.5DPlannerApiKey) */
	const API_KEY = '2w4gsdsu6rf3hdg8';
	const STORE_ID = 12;

	const API_ERROR_USER_ACTION = 400;
	const API_ERROR_NONE = 0;

	/**
	 * @var array
	 */
	private $_httpResponseHeader;

	/**
	 * @var string
	 */
	public $email;

	/**
	 * @var string
	 */
	public $sign;

	/**
	 * @var string
	 */
	public $hash;

	/**
	 * @param string $email
	 * @param string $hash
	 * @throws Exception
	 */
	public function __construct($email, $hash)
	{
		if (empty($email)) {
			throw new Planner5DException('Variable $email is empty', Planner5DException::ERROR_EMAIL);
		}

		if (empty($hash)) {
			throw new Planner5DException('Variable $hash is empty', Planner5DException::ERROR_HASH);
		}

		$this->hash = $hash;
		$this->email = $email;
		$this->sign = sha1(self::API_KEY . $this->email);
	}

	/**
	 * Получение ссылки для IFrame
	 *
	 * @param string $url
	 * @param array $htmlOptions
	 * @return string
	 */
	static function getIFrame($url, $htmlOptions = [])
	{
		$headers = @get_headers($url);
		if ($headers && isset($headers[0]) && (strpos($headers[0], '200') || strpos($headers[0], '302'))) {
			return TbHtml::tag('iframe', [
					'src' => $url,
					'mozallowfullscreen' => false,
					'webkitallowfullscreen' => false,
					'allowfullscreen' => false,
				] + $htmlOptions, ''
			);
		}
		return null;
	}

	/**
	 * Получение ссылки для IFrame
	 *
	 * @param $queryData
	 * @return string
	 */
	static function getIFrameUrl($queryData)
	{
		$url = 'https://planner5d.com/api/pub/goto/?' . http_build_query($queryData);
		$headers = @get_headers($url);
		if ($headers && isset($headers[0]) && strpos($headers[0], '200')) {
			return json_decode(file_get_contents($url), true);
		}
		return 'https://planner5d.com/api/pub/goto/?' . http_build_query($queryData);
	}

	/**
	 * @return array
	 */
	public function getSignData()
	{
		return array(
			'email' => $this->email,
			'sign' => $this->sign,
		);
	}

	/**
	 * Создание пользователя
	 *
	 * @return array
	 */
	public function createUser()
	{
		$url = 'https://planner5d.com/api/pub/user/?' . http_build_query($this->getSignData());

		$result = file_get_contents($url, false, stream_context_create(array(
			'http' => array(
				'method' => 'POST'
			),
		)));
		$result = json_decode($result, true);

		return (isset($result['errorMessage']) && $result['errorMessage'] == 'Already exist'
			|| isset($result['error']) && $result['error'] == self::API_ERROR_NONE
		);
	}

	/**
	 * Авторизация пользователя
	 *
	 * @return bool
	 * @throws Planner5DException
	 */
	public function authorize()
	{
		$url = 'https://planner5d.com/api/pub/goto/?' . http_build_query(
				$this->getSignData() + array(
					'to' => 'https://planner5d.com/api/project/' . $this->hash,
				)
			);
		$result = file_get_contents($url, false, stream_context_create(array(
			'http' => array(
				'method' => 'GET'
			),
		)));
		$result = json_decode($result, true);
		$this->_httpResponseHeader = $http_response_header;
		if ($result['error'] == self::API_ERROR_USER_ACTION) {
			throw new Planner5DException('Invalid signature for this user', Planner5DException::ERROR_INVALID_USER);
		}

		return isset($result['error']) && $result['error'] == self::API_ERROR_NONE;
	}

	/**
	 * Копирование проекта авторизовавшемуся пользователю если такого не имееться
	 *
	 * @return array|mixed
	 */
	public function copyProject()
	{
		$url = 'https://planner5d.com/api/pub/project/?' . http_build_query(
				$this->getSignData() + array(
					'from' => $this->hash,
					'name' => 'New Stand'
				)
			);
		$result = file_get_contents($url, false, stream_context_create(array(
			'http' => array(
				'method' => 'PUT',
				'header' => "Cookie: " . http_build_query($this->getCookies()) . "\r\n"
			),
		)));
		$result = json_decode($result, true);
		return isset($result['error']) && $result['error'] == self::API_ERROR_NONE
			? isset($result['copyResult'],$result['copyResult']['hash']) ? $result['copyResult']['hash'] : false
			: false;
	}

	/**
	 * @return mixed
	 */
	public function getCookies()
	{
		preg_match('/^Set-Cookie:\s*([^;]*)/mi', implode(PHP_EOL, $this->_httpResponseHeader), $m);
		parse_str($m[1], $cookies);
		return $cookies;
	}

	/**
	 * Получение данных проекта по хешу
	 *
	 * @param $hash
	 * @return array
	 */
	static function getProjectData($hash)
	{
		$url = 'https://planner5d.com/api/project/' . $hash;
		$headers = @get_headers($url);
		if ($headers && isset($headers[0]) && strpos($headers[0], '200')) {
			try{
				$content = file_get_contents($url);
			}catch(Exception $e){
				return NULL;
			}
			return json_decode($content, true);
		}

		return null;
	}

}

class Planner5DException extends Exception
{
	const ERROR_EMAIL = 1;
	const ERROR_HASH = 2;
	const ERROR_INVALID_USER = 3;
}