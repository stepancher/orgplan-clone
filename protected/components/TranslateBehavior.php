<?php

/**
 * Поиск переводов, если язык приложения отличный от русского
 * Class AfterFindTranslateBehavior
 */
class TranslateBehavior extends CActiveRecordBehavior
{
    /**
     * @var TrFair
     */
    private $_trModel;

    /**
     * Сохраняем перевод, если требуется.
     * @param CModelEvent $event
     */
    public function beforeSave($event)
    {
        /** @var Fair $record */
        $record = $this->getOwner();
        $trClass = 'Tr' . get_class($record);
            /** @var TrFair $trClass */
        if (class_exists($trClass)) {
            if ($record->isNewRecord) {
                $trModel = new $trClass;
            } else {
                $trModel = $trClass::model()->findByAttributes(array(
                    'trParentId' => $record->id,
                    'langId' => Yii::app()->language
                ));
                if (null === $trModel) {
                    $trModel = new $trClass;
                }
            }

            $attributes = $record::model()->attributeNames();

            if(method_exists($record, 'trAttributeNames')){
                $attributes = array_merge($attributes, $record->trAttributeNames());
            }

            $trAttributes = $trModel::model()->attributeNames();
            foreach ($attributes as $name) {
                if ($name != 'id' AND in_array($name, $trAttributes)) {
                    $trModel->$name = $record->$name;
                }
            }
            $trModel->langId = Yii::app()->language;
            $this->_trModel = $trModel;
        }
    }

    /**
     * Обработка записи после сохранения для восстановления состояния объекта (как после поиска).
     * @param CModelEvent $event
     */
    public function afterSave($event)
    {
        /** @var Fair $record */
        $record = $this->getOwner();
        if ($this->_trModel) {
            $this->_trModel->trParentId = $record->id;

            if(method_exists($record, 'updateShortUrl')){
                $this->_trModel->shortUrl = $record->getSeoNameForNewVersion();
            }

            $this->_trModel->save();

        }
    }
}