<?php
/**
 * Расширение для EActiveRecord
 * @author Igor Sapegin aka Rendol sapegin.in@gmail.com
 */

/**
 * Class AR
 */
class AR extends EActiveRecord
{
    public $notPurifyAttributes = [];

    const SCENARIO_DEFAULT = 'default';


    /**
     * Установка дат
     * @param string $name
     * @param mixed $value
     * @return mixed|void
     * @throws Exception
     */
    public function __set($name, $value)
    {
        if (!empty($value)) {
            $description = $this->description();
            if (isset($description[$name])) {
                if (isset($description[$name][0])) {
                    if (in_array($description[$name][0], ['datetime', 'timestamp'])) {
                        $time = strtotime($value);
                        if (!is_numeric($value) && $time > 0) {
                            $date = new DateTime(date('Y-m-d H:i:s', $time));
                            if ($date->format('s') > 0) {
                                $value = date('Y-m-d H:i:s', $time);
                            } elseif ($date->format('i') > 0) {
                                $value = date('Y-m-d H:i', $time);
                            } elseif ($date->format('H') > 0) {
                                $value = date('Y-m-d H', $time);
                            } elseif ($date->format('d') > 0) {
                                $value = date('Y-m-d', $time);
                            } elseif ($date->format('m') > 0) {
                                $value = date('Y-m', $time);
                            }
                        }
                    }
                }
            }
        }
        parent::__set($name, $value);
    }

    /**
     * Returns the form name that this model class should use.
     *
     * The form name is mainly used by [[\yii\widgets\ActiveForm]] to determine how to name
     * the input fields for the attributes in a model. If the form name is "A" and an attribute
     * name is "b", then the corresponding input name would be "A[b]". If the form name is
     * an empty string, then the input name would be "b".
     *
     * By default, this method returns the model class name (without the namespace part)
     * as the form name. You may override it when the model is used in different forms.
     *
     * @return string the form name of this model class.
     */
    public function formName()
    {
        $reflector = new ReflectionClass($this);
        return $reflector->getShortName();
    }

    /**
     * Returns the first error of every attribute in the model.
     * @return array the first errors. The array keys are the attribute names, and the array
     * values are the corresponding error messages. An empty array will be returned if there is no error.
     * @see getErrors()
     * @see getFirstError()
     */
    public function getFirstErrors()
    {
        $_errors = parent::getErrors();
        if (empty($_errors)) {
            return [];
        } else {
            $errors = [];
            foreach ($_errors as $name => $es) {
                if (!empty($es)) {
                    $errors[$name] = reset($es);
                }
            }

            return $errors;
        }
    }

    /**
     * Returns the first error of the specified attribute.
     * @param string $attribute attribute name.
     * @return string the error message. Null is returned if no error.
     * @see getErrors()
     * @see getFirstErrors()
     */
    public function getFirstError($attribute)
    {
        $_errors = parent::getErrors();
        return isset($_errors[$attribute]) ? reset($_errors[$attribute]) : null;
    }


    /**
     * Returns the attribute names that are subject to validation in the current scenario.
     * @return string[] safe attribute names
     */
    public function activeAttributes()
    {
        $scenario = $this->getScenario();
        $scenarios = $this->scenarios();
        if (!isset($scenarios[$scenario])) {
            return [];
        }
        $attributes = $scenarios[$scenario];
        foreach ($attributes as $i => $attribute) {
            if ($attribute[0] === '!') {
                $attributes[$i] = substr($attribute, 1);
            }
        }

        return $attributes;
    }


    /**
     * Returns the validators applicable to the current [[scenario]].
     * @param string $attribute the name of the attribute whose applicable validators should be returned.
     * If this is null, the validators for ALL attributes in the model will be returned.
     * @return \yii\validators\Validator[] the validators applicable to the current [[scenario]].
     */
    public function getActiveValidators($attribute = null)
    {
        $validators = [];
        $scenario = $this->getScenario();
        foreach ($this->getValidators() as $validator) {

            $isActive = !in_array($scenario, $validator->except, true) && (empty($validator->on) || in_array($scenario,
                        $validator->on, true));

            if ($isActive && ($attribute === null || in_array($attribute, $validator->attributes, true))) {
                $validators[] = $validator;
            }
        }
        return $validators;
    }

    /**
     * Returns a list of scenarios and the corresponding active attributes.
     * An active attribute is one that is subject to validation in the current scenario.
     * The returned array should be in the following format:
     *
     * ~~~
     * [
     *     'scenario1' => ['attribute11', 'attribute12', ...],
     *     'scenario2' => ['attribute21', 'attribute22', ...],
     *     ...
     * ]
     * ~~~
     *
     * By default, an active attribute is considered safe and can be massively assigned.
     * If an attribute should NOT be massively assigned (thus considered unsafe),
     * please prefix the attribute with an exclamation character (e.g. '!rank').
     *
     * The default implementation of this method will return all scenarios found in the [[rules()]]
     * declaration. A special scenario named [[SCENARIO_DEFAULT]] will contain all attributes
     * found in the [[rules()]]. Each scenario will be associated with the attributes that
     * are being validated by the validation rules that apply to the scenario.
     *
     * @return array a list of scenarios and the corresponding active attributes.
     */
    public function scenarios()
    {
        $scenarios = [self::SCENARIO_DEFAULT => []];
        foreach ($this->getValidators() as $validator) {
            foreach ($validator->on as $scenario) {
                $scenarios[$scenario] = [];
            }
            foreach ($validator->except as $scenario) {
                $scenarios[$scenario] = [];
            }
        }
        $names = array_keys($scenarios);

        foreach ($this->getValidators() as $validator) {
            if (empty($validator->on) && empty($validator->except)) {
                foreach ($names as $name) {
                    foreach ($validator->attributes as $attribute) {
                        $scenarios[$name][$attribute] = true;
                    }
                }
            } elseif (empty($validator->on)) {
                foreach ($names as $name) {
                    if (!in_array($name, $validator->except, true)) {
                        foreach ($validator->attributes as $attribute) {
                            $scenarios[$name][$attribute] = true;
                        }
                    }
                }
            } else {
                foreach ($validator->on as $name) {
                    foreach ($validator->attributes as $attribute) {
                        $scenarios[$name][$attribute] = true;
                    }
                }
            }
        }

        foreach ($scenarios as $scenario => $attributes) {
            if (empty($attributes) && $scenario !== self::SCENARIO_DEFAULT) {
                unset($scenarios[$scenario]);
            } else {
                $scenarios[$scenario] = array_keys($attributes);
            }
        }

        return $scenarios;
    }

    /**
     * Получение масива идентификаторов объекта привязаных к объекту-"родителю?"
     * @param $object //что-то типа $this->standTechnicalData->standTechnicalDataHasExtraServices
     * @param $field
     * @return array
     */
    public function getObjectIds($object, $field)
    {
        $result = [];
        if (isset($object)) {
            foreach ($object as $item) {
                $result[] = $item->$field;
            }
        }
        return $result;
    }

    protected function beforeValidate()
    {
        $p = new CHtmlPurifier;
        $p->setOptions([
            'HTML.SafeObject' => true,
            //'Output.FlashCompat' => true,
            //'AutoFormat.Linkify' => true,
            //'HTML.Nofollow' => true,
            'Core.EscapeInvalidTags' => true,
        ]);
        foreach ($this->getAttributes() as $attr => $value) {
            if ($this instanceof Recall || $this instanceof User
                || $this instanceof Contact || $this instanceof LegalEntity
                || $this instanceof BankAccount
            ) {
                $value = trim($value);
            }

            if (!in_array($attr, $this->notPurifyAttributes) && $attr != 'id') {
                $this->$attr = $p->purify(htmlspecialchars($value));
                $this->$attr = preg_replace('/(amp;)+/', '', $this->$attr);
            }
        }

        return true;
    }

    public function findByAttributes($attributes, $condition = '', $params = array())
    {
        Yii::trace(get_class($this) . '.findByAttributes()', 'system.db.ar.CActiveRecord');

        $trCriteria = $this->createTrCriteria($attributes, $condition, $params);

        $prefix = $this->getTableAlias(true) . '.';
        $criteria = $this->getCommandBuilder()->createColumnCriteria($this->getTableSchema(), $attributes, $condition, $params, $prefix);

        if ($trCriteria) {
            $criteria->mergeWith($trCriteria);
        }

        return $this->query($criteria);
    }

    public function findAllByAttributes($attributes, $condition = '', $params = array())
    {
        Yii::trace(get_class($this) . '.findAllByAttributes()', 'system.db.ar.CActiveRecord');

        $trCriteria = $this->createTrCriteria($attributes, $condition, $params);

        $prefix = $this->getTableAlias(true) . '.';
        $criteria = $this->getCommandBuilder()->createColumnCriteria($this->getTableSchema(), $attributes, $condition, $params, $prefix);

        if ($trCriteria) {
            $criteria->mergeWith($trCriteria);
        }

        return $this->query($criteria, true);
    }

    /**
     * @param $attributes
     * @param $condition
     * @param $params
     * @return CDbCriteria
     * @throws CDbException
     */
    public function createTrCriteria(&$attributes, $condition, $params)
    {
        $trModel = null;
        $trAttributes = [];
        if (Yii::app()->sourceLanguage != Yii::app()->language) {
            if (array_key_exists('TranslateBehavior', $this->behaviors())) {
                /** @var TrFair $trClass */
                if (class_exists($trClass = 'Tr' . get_class($this))) {
                    $trModel = $trClass::model();
                    foreach ($attributes as $name => $value) {
                        if ('id' != $name && $trModel->hasAttribute($name)) {
                            $trAttributes[$name] = $value;
                            unset($attributes[$name]);
                        }
                    }
                }
            }
        }

        $trCriteria = null;
        if ($trModel) {
            $trCriteria = new CDbCriteria();
            if (sizeof($trAttributes)) {
                $trCriteria = $this->getCommandBuilder()->createColumnCriteria($trModel->getTableSchema(), $trAttributes, $condition, $params, 'tr.');
            }
            $trCriteria->join = 'LEFT JOIN ' . $trModel->tableName() . ' tr ON t.`id` = tr.`trParentId`';
        }
        return $trCriteria;
    }

    /**
     * @param $trModel
     * @param $property
     * @return mixed
     */
    public function loadTr($trModel, $property)
    {
        $model = $trModel::model()->findByAttributes(
            [
                'langId' => Yii::app()->language,
                'trParentId' => $this->id
            ]);

        $output = $this->{$property};

        if ($model !== NULL && !empty($model->{$property})) {
            $output = $model->{$property};
        }

        return $output;
    }
}