<?php

class ZDbHttpSession extends CDbHttpSession {

    public function regenerateID($deleteOldSession=false){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::regenerateID',
        ));

        $result = parent::regenerateID($deleteOldSession);

        Pinba::stop($_timer);

        return $result;
    }

    protected function createSessionTable($db,$tableName){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::createSessionTable',
        ));

        $result = parent::createSessionTable($db,$tableName);

        Pinba::stop($_timer);

        return $result;
    }

    protected function getDbConnection(){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::getDbConnection',
        ));

        $result = parent::getDbConnection();

        Pinba::stop($_timer);

        return $result;
    }

    public function openSession($savePath,$sessionName){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::openSession',
        ));

        $result = parent::openSession($savePath,$sessionName);

        Pinba::stop($_timer);

        return $result;
    }

    public function readSession($id){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::readSession',
        ));

        $result = parent::readSession($id);

        Pinba::stop($_timer);

        return $result;
    }

    public function writeSession($id,$data){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::writeSession',
        ));

        $result = parent::writeSession($id, $data);

        Pinba::stop($_timer);

        return $result;
    }

    public function destroySession($id){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::destroySession',
        ));

        $result = parent::destroySession($id);

        Pinba::stop($_timer);

        return $result;
    }

    public function gcSession($maxLifetime){

        $_timer = Pinba::start(array(
            'category' => 'session',
            'group' => 'session::gcSession',
        ));

        $result = parent::gcSession($maxLifetime);

        Pinba::stop($_timer);

        return $result;
    }
}