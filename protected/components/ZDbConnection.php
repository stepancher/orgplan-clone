<?php

class ZDbConnection extends CDbConnection {

    public function createCommand($query=null){
        $this->setActive(true);
        return new ZDbCommand($this,$query);
    }

}