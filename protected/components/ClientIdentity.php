<?php

class ClientIdentity extends CUserIdentity {

    public function authenticate(){

        if(!isset(Yii::app()->clientList[$this->username])){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }elseif(Yii::app()->clientList[$this->username] != $this->password){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }else{
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;

    }

}