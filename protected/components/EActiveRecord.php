<?php
/**
 * Расширение для CActiveRecord
 * @author Igor Sapegin aka Rendol sapegin.in@gmail.com
 */

/**
 * Class EActiveRecord
 */
class EActiveRecord extends CActiveRecord
{
	/**
	 * @var array attribute values indexed by attribute names
	 */
	private $_attributes = array();

	/**
	 * @var array old attribute values indexed by attribute names.
	 */
	private $_oldAttributes;

	/**
	 * Список динамических пересенных, описанных в description
	 * @var array
	 */
	protected $_dynamic_vars = array();

	/**
	 * @param string $className
	 * @return static
	 */
	static function model($className = null)
	{
		if (null === $className) {
			$className = get_called_class();
		} else {
			/** @var $model self */
			$model = new $className(null);
			if (get_class($model) !== get_called_class()) {
				return $model->model($className);
			}
		}
		return parent::model($className);
	}

	/**
	 * Имя таблицы в БД
	 * Если прописан namespace вида /application/modules/XXX/models/Record,
	 * то он бдует преобразован к имени {{xxx_record}}
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		$cls = get_class($this);

		if (stristr($cls, '\\') !== false) {
			$tableName = array();
			if (stristr($cls, 'modules') !== false) {
				$module = substr($cls, strpos($cls, 'modules') + strlen('modules') + 1);
				$module = substr($module, 0, strpos($module, '\\'));
				$tableName[] = $module;
			}
			$tableName[] = substr($cls, strrpos($cls, '\\') + 1);
			$tableName = implode('_', $tableName);
		} else {
			$tableName = $cls;
		}

		return '{{' . strtolower($tableName) . '}}';
	}

	/**
	 * Выборка записей с условием фильтрации по критериям пользователя
	 * @param CDbCriteria $criteria
	 * @param bool $all
	 * @return mixed
	 */
	protected function query($criteria, $all = false)
	{
		if (class_exists('User', FALSE)) {
			$method = 'getCriteria' . get_class($this);
			if (method_exists('User', $method)) {
				$criteria->mergeWith(call_user_func(array('User', $method)));
			}
		}
		return parent::query($criteria, $all);
	}

	public function populateRecord($attributes,$callAfterFind=true)
	{
        if(class_exists('Pinba', FALSE)){
            $_timer = Pinba::start(array(
                'category' => 'ar',
                'group' => 'ar::populateRecord',
            ));
        }

		$result = parent::populateRecord($attributes, $callAfterFind);

        if(class_exists('Pinba', FALSE)){
            Pinba::stop($_timer);
        }

		return $result;
	}

	public function getMetaData(){

        if(class_exists('Pinba', FALSE)){
            $_timer = Pinba::start(array(
                'category' => 'ar',
                'group' => 'ar::getMetaData',
            ));
        }

        $result = parent::getMetaData();

        if(class_exists('Pinba', FALSE)){
            Pinba::stop($_timer);
        }

        return $result;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @param CDbCriteria $criteria
	 * @param array $ignoreCompareFields
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = null, $ignoreCompareFields = array())
	{
		$conn = $this->getDbConnection();
		$dCriteria = new CDbCriteria;
		$dCriteria->group = $conn->quoteColumnName('t.id');
		$with = array();
		$sort = array();

		$desc = $this->description();
		foreach ($desc as $field => $params) {
			$relation = null;
			if (stristr($field, '_')) {
				$fieldLink = self::createCriteriaWith(explode('_', $field), $with);
			} else {
				$fieldLink = 't.' . $field;
			}

			if (!in_array($fieldLink, $ignoreCompareFields)) {
				$value = $this->$field;
				if (null !== $value) {
					$pMatch = false;

					if (isset($params[0])) {
						$type = $params[0];
						if (in_array($type, array('string', 'text', 'timestamp', 'datetime'))) {
							$pMatch = true;
							if (in_array($type, array('timestamp', 'datetime')) && !empty($value)) {
								if (strlen($value) == 10) {
									$value = date('Y-m-d', strtotime($value));
								} elseif (strlen($value) == 19) {
									$value = date('Y-m-d H:i:s', strtotime($value));
								}
							}
						}
					}

					$dCriteria->compare($fieldLink, $value, $pMatch);
				}
			}
			if (!isset($desc[$relation]) || isset($desc[$relation]) && $desc[$relation]['relation'][0] != EActiveRecord::HAS_MANY) {
				$sort[$field] = array(
					'asc' => $conn->quoteColumnName($fieldLink) . ' ASC',
					'desc' => $conn->quoteColumnName($fieldLink) . ' DESC',
				);
			}
		}

		$dCriteria->with = $with;
		if (null !== $criteria) {
			$dCriteria->mergeWith($criteria);
		}

		if (class_exists('User')) {
			$method = 'getCriteria' . get_class($this);
			if (method_exists('User', $method)) {
				$dCriteria->mergeWith(call_user_func(array('User', $method)));
			}
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $dCriteria,
			'sort' => array(
				'attributes' => $sort,
			)
		));
	}

	static function createCriteriaWith($parts, &$with)
	{
		$link = $parts[0];
		if (!isset($with[$link])) {
			$with[$link] = array(
				'together' => true,
				'with' => array()
			);
		}
		if (sizeof($parts) > 2) {
			array_shift($parts);
			return self::createCriteriaWith($parts, $with[$link]['with']);
		}
		return $parts[0] . '.' . $parts[1];
	}


	/**
	 * Returns the old attribute values.
	 * @return array the old attribute values (name-value pairs)
	 */
	public function getOldAttributes()
	{
		return $this->_oldAttributes === null ? array() : $this->_oldAttributes;
	}

	/**
	 * Sets the old attribute values.
	 * All existing old attribute values will be discarded.
	 * @param array $values old attribute values to be set.
	 */
	public function setOldAttributes($values)
	{
		$this->_oldAttributes = $values;
	}

	/**
	 * Returns the old value of the named attribute.
	 * If this record is the result of a query and the attribute is not loaded,
	 * null will be returned.
	 * @param string $name the attribute name
	 * @return mixed the old attribute value. Null if the attribute is not loaded before
	 * or does not exist.
	 * @see hasAttribute
	 */
	public function getOldAttribute($name)
	{
		return isset($this->_oldAttributes[$name]) ? $this->_oldAttributes[$name] : null;
	}

	/**
	 * Sets the old value of the named attribute.
	 * @param string $name the attribute name
	 * @param mixed $value the old attribute value.
	 * @throws Exception if the named attribute does not exist.
	 * @see hasAttribute
	 */
	public function setOldAttribute($name, $value)
	{
		if (isset($this->_oldAttributes[$name]) || isset($this->getTableSchema()->columns[$name])) {
			$this->_oldAttributes[$name] = $value;
		} else {
			throw new Exception(get_class($this) . ' has no attribute named "' . $name . '".');
		}
	}

	/**
	 * Returns a value indicating whether the named attribute has been changed.
	 * @param string $name the name of the attribute
	 * @return boolean whether the attribute has been changed
	 */
	public function isAttributeChanged($name)
	{
		if (isset($this->_attributes[$name], $this->_oldAttributes[$name])) {
			return $this->_attributes[$name] !== $this->_oldAttributes[$name];
		} else {
			return isset($this->_attributes[$name]) || isset($this->_oldAttributes[$name]);
		}
	}

	/**
	 * Returns the attribute values that have been modified since they are loaded or saved most recently.
	 * @param string[]|null $names the names of the attributes whose values may be returned if they are
	 * changed recently. If null, [[attributes()]] will be used.
	 * @return array the changed attribute values (name-value pairs)
	 */
	public function getDirtyAttributes($names = null)
	{
		if ($names === null) {
			$names = array_keys($this->getAttributes());
		}
		$attributes = array();
		if ($this->_oldAttributes === null) {
			foreach ($this->getAttributes() as $name => $value) {
				if (in_array($name, $names)) {
					$attributes[$name] = $value;
				}
			}
		} else {
			foreach ($this->getAttributes() as $name => $value) {
				if (in_array($name, $names)) {
					if ($value != $this->_oldAttributes[$name]) {
						$attributes[$name] = $value;
					}
				}
			}
		}
		return $attributes;
	}

	/**
	 * Количество изменений объекта
	 * @return bool
	 */
	public function countChanges()
	{
		return count(array_diff_assoc($this->getOldAttributes(), $this->getAttributes()));
	}

	/**
	 * Проверка наличия изменений объекта
	 * @return bool
	 */
	public function existChanges()
	{
		return 0 !== $this->countChanges();
	}

	/**
	 * Сохранить только при условии изменения данных
	 * @param bool $runValidation
	 * @param null $attributes
	 * @return bool|null
	 */
	public function saveChanges($runValidation = true, $attributes = null)
	{
		if (
		$this->existChanges()
		) {
			return $this->save($runValidation, $attributes);
		}
		return true;
	}

	/**
	 * Множественное уровнезависимое сохранение моделей
	 *
	 * Пример использования:
	 *    $records = EActiveRecord::multiSave(
	 *        $_POST,
	 *        array(
	 *            array(
	 *                'name' => 'model',
	 *                'class' => get_class($model),
	 *                'record' => $model,
	 *            ),
	 *            function ($records) {
	 *                foreach ($records['modelsIssue'] as $issue) {
	 *                    $issue->projectId = $records['model']->id;
	 *                }
	 *            }
	 *        ),
	 *        array(
	 *            array(
	 *                'name' => 'modelsIssue',
	 *                'class' => get_class(Issue::model()),
	 *                'records' => $model->isNewRecord ? array(new Issue) : array(),
	 *            ),
	 *            function ($records) use ($ctrl) {
	 *                Yii::app()->user->setFlash('success', Yii::t('system', 'The information is saved.'));
	 *                $ctrl->redirect($ctrl->createUrl('index'));
	 *            }
	 *        )
	 *    );
	 *    $this->render('_form', $records);
	 *
	 * @param array $data
	 * @return array
	 */
	static function multiSave($data = array())
	{
		$validator = !empty($data);
		$args = func_get_args();

		$status = $validator;

		// Результирующий список объектов
		$records = array();
		$levels = array();

		// Функция срабатывающая по умолчанию, т.е. в двух случаях: данные не переданы, данные не валидны
		$defaultCallback = null;

		// Проходим уровни зависимости
		for ($i = 1, $lnI = count($args); $i < $lnI; $i++) {

			// Если последний аргумент функция
			if ($i + 1 == $lnI && is_callable($args[$i])) {
				$defaultCallback = $args[$i];
				break;
			}

			// Список моделей текущего уровня
			$levelModels = $args[$i];
			$lnJ = count($levelModels);

			// Проверяем есть ли callback для текущего уровня
			if (is_callable($levelModels[$lnJ - 1])) {
				$levels[$i]['callback'] = $levelModels[$lnJ - 1];
				$lnJ--;
			};

			// Обрабатываем один уровень моделей
			for ($j = 0; $j < $lnJ; $j++) {

				$attributes = null;
				$opts = $levelModels[$j];
				if (!isset($opts['saved'])) {
					$opts['saved'] = true;
				}

				$scenario = '';
				if (isset($opts['scenario'])) {
					$scenario = $opts['scenario'];
				}
				/** @var EActiveRecord $record */
				$record = null;

				if (isset($opts['record'])) {
					/** @var EActiveRecord $record */
					$record = empty($opts['record']) ? new $opts['class'] : $opts['record'];
					if ($scenario) {
						$record->setScenario($scenario);
					}
					if ($validator && $opts['saved']) {
						if (isset($data[$opts['class']])) {
							$record->setAttributes($data[$opts['class']]);
							if (!$record->validate($attributes)) {
								$status = false;
							}
						}
					} else {
						if (isset($_GET[$opts['class']])) {
							$record->setAttributes($_GET[$opts['class']]);
						}
					}

					$levels[$i]['records'][] = array(
						'record' => $record,
						'saved' => $opts['saved'],
						'attributes' => isset($opts['attributes']) ? $attributes = $opts['attributes'] : null,
					);

				} elseif (isset($opts['records'])) {

					/** @var EActiveRecord[] $record */
					$record = array();

					if ($validator && $opts['saved']) {

						if (isset($data[$opts['class']])) {
							foreach ($data[$opts['class']] as $name => $recordData) {
								/** @var EActiveRecord $recordItem */
								$recordItem = empty($opts['records'][$name]) ? new $opts['class'] : $opts['records'][$name];
								if ($scenario) {
									$recordItem->setScenario($scenario);
								}
								$recordItem->setAttributes($recordData);
								if (!$recordItem->validate($attributes)) {
									$status = false;
								}
								$record[] = $recordItem;
								$levels[$i]['records'][] = array(
									'record' => $recordItem,
									'saved' => $opts['saved'],
									'attributes' => isset($opts['attributes']) ? $attributes = $opts['attributes'] : null,
								);

							}
						}
					} else {

						foreach ($opts['records'] as $recordItem) {
							/** @var EActiveRecord $recordItem */
							if ($scenario) {
								$recordItem->setScenario($scenario);
							}
							if (isset($_GET[$opts['class']])) {
								$recordItem->setAttributes($_GET[$opts['class']]);
							}
							$record[] = $recordItem;
							$levels[$i]['records'][] = array(
								'record' => $recordItem,
								'saved' => $opts['saved'],
								'attributes' => isset($opts['attributes']) ? $attributes = $opts['attributes'] : null,
							);
						}

					}
				}
				$records[$opts['name']] = $record;
			}
		}

		if ($status) {
			// Проходим уровени зависимости
			foreach ($levels as $level) {
				if (!empty($level['records'])) {
					foreach ($level['records'] as $params) {
						if (true == $params['saved']) {
							/** @var EActiveRecord $record */
							$record = $params['record'];
							if (!$record->save(false, $params['attributes'])) {
								break;
							}
						}
					}
				}
				if (isset($level['callback'])) {
					if (false === call_user_func($level['callback'], $records)) {
						return $records;
					}
				}
			}
		} elseif ($defaultCallback) {
			call_user_func($defaultCallback, $records);
		}

		return $records;
	}

	/**
	 * После создания объекта резервируем считанные данные
	 */
	public function afterConstruct()
	{
		$this->setOldAttributes($this->getAttributes());
		parent::afterConstruct();
	}

	/**
	 * После поиска резервируем считанные данные
	 */
	public function afterFind()
	{
		$this->setOldAttributes($this->getAttributes());
		parent::afterFind();
	}

	/**
	 * После сохрранения резервируем новые данные
	 */
	public function afterSave()
	{
		$this->setOldAttributes($this->getAttributes());
		parent::afterSave();
	}

	/**
	 * Описание свойств объекта
	 * @return array
	 */
	public function description()
	{
		return array(
			'id' => array(
				'pk',
				'label' => 'ID'
			),
		);
	}

	/**
	 * Геттер - обработка динамических переменных, описанных в description
	 * @param string $name
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($name)
	{
		try {
			return parent::__get($name);
		} catch (Exception $e) {
			$desc = $this->description();
			if (isset($desc[$name])) {
				if (!isset($this->_dynamic_vars[$name])) {
					$this->_dynamic_vars[$name] = null;
				}
				return $this->_dynamic_vars[$name];
			}
			throw $e;
		}
	}

	/**
	 * Чтение динамических переменных, описанных в description
	 * @param string $name
	 * @param mixed $value
	 * @return mixed|void
	 * @throws Exception
	 */
	public function __set($name, $value)
	{
		try {
			parent::__set($name, $value);
		} catch (Exception $e) {
			$desc = $this->description();
			if (isset($desc[$name])) {
				return $this->_dynamic_vars[$name] = $value;
			}
			throw $e;
		}
	}

	/**
	 * Генерация названий полей на основе описания модели
	 * @return array
	 */
	public function attributeLabels()
	{
		$labels = array();
		$properties = $this->description();
		foreach ($properties as $name => $params) {
			if (isset($params['label'])) {
				$labels[$name] = $params['label'];
			}
		}
		return $labels;
	}

	/**
	 * Генерация связей на основе описания модели
	 * @return array
	 */
	public function relations()
	{
		$relations = array();
		$properties = $this->description();
		foreach ($properties as $name => $params) {
			if (isset($params['relation'])) {
				$relations[str_replace('Id', '', str_replace('_id', '', $name))] = $params['relation'];
			}
		}
		return $relations;
	}

	/**
	 * Генерация правил на основе описания модели
	 * @return array
	 */
	public function rules()
	{
		$rules = array();
		$properties = $this->description();
		foreach ($properties as $name => $params) {
			if (isset($params['rules'])) {
				foreach ($params['rules'] as $scenarioName => $scenarioRules) {
					foreach ($scenarioRules as $rule) {
						if ('default' == $scenarioName) {
							$rules[] = array_merge(
								array($name), $rule
							);
						} else {
							$rules[] = array_merge(
								array($name), $rule, array(
									'on' => $scenarioName
								)
							);
						}
					}
				}
			}
		}
		return $rules;
	}

	/**
	 * Finds a single active record with the specified condition.
	 * @param mixed $condition query condition or criteria.
	 * If a string, it is treated as query condition (the WHERE clause);
	 * If an array, it is treated as the initial values for constructing a {@link CDbCriteria} object;
	 * Otherwise, it should be an instance of {@link CDbCriteria}.
	 * @param array $params parameters to be bound to an SQL statement.
	 * This is only used when the first parameter is a string (query condition).
	 * In other cases, please use {@link CDbCriteria::params} to set parameters.
	 * @return static the record found. Null if no record is found.
	 */
	public function find($condition = '', $params = array())
	{
		return parent::find($condition, $params);
	}

	/**
	 * Finds a single active record with the specified primary key.
	 * See {@link find()} for detailed explanation about $condition and $params.
	 * @param mixed $pk primary key value(s). Use array for multiple primary keys. For composite key, each key value must be an array (column name=>column value).
	 * @param mixed $condition query condition or criteria.
	 * @param array $params parameters to be bound to an SQL statement.
	 * @return static the record found. Null if none is found.
	 */
	public function findByPk($pk, $condition = '', $params = array())
	{
		return parent::findByPk($pk, $condition, $params);
	}

	/**
	 * Finds a single active record that has the specified attribute values.
	 * See {@link find()} for detailed explanation about $condition and $params.
	 * @param array $attributes list of attribute values (indexed by attribute names) that the active records should match.
	 * An attribute value can be an array which will be used to generate an IN condition.
	 * @param mixed $condition query condition or criteria.
	 * @param array $params parameters to be bound to an SQL statement.
	 * @return static the record found. Null if none is found.
	 */
	public function findByAttributes($attributes, $condition = '', $params = array())
	{
		return parent::findByAttributes($attributes, $condition, $params);
	}

	/**
	 * Finds a single active record with the specified SQL statement.
	 * @param string $sql the SQL statement
	 * @param array $params parameters to be bound to the SQL statement
	 * @return static the record found. Null if none is found.
	 */
	public function findBySql($sql, $params = array())
	{
		return parent::findBySql($sql, $params);
	}

}