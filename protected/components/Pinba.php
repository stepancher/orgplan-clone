<?php

/**
 * Pinba wrapper
 *
 * @link https://github.com/tony2001/pinba_engine/wiki
 *
 * Class Pinba
 */
class Pinba {
    protected static $initTags = [];
    protected static $info = [];
    protected static $hasScriptName = FALSE;
    protected static $_scriptName = NULL;
    public static function getInitTags(){
        if(empty(self::$initTags)){
            $info = self::getInfo();
            if (isset($info['hostname'])) {
                self::$initTags['__hostname'] = $info['hostname'];
            }
            if (isset($info['server_name'])) {
                self::$initTags['__server_name'] = $info['server_name'];
            }
        }
        return self::$initTags;
    }
    public static function getInfo(){
        if(PINBA && empty(self::$initTags)){
            self::$info = pinba_get_info();
        }
        return self::$info;
    }
    public static function setScriptName($scriptName = NULL){
        if(PINBA){
            if(self::$_scriptName !== NULL){
                $scriptName = self::$_scriptName;
            }elseif($scriptName === NULL && isset($_SERVER['REQUEST_URI'])){
                $scriptName = self::$_scriptName = self::$hasScriptName = $_SERVER['REQUEST_URI'];
            }else{
                $scriptName = 'undefined';
            }
            pinba_script_name_set($scriptName);
        }
    }
    public static function start($tags){
        $resource = NULL;
        if(PINBA){
            if(!self::$hasScriptName){
                self::setScriptName();
            }
            $resource = pinba_timer_start(array_merge(self::getInitTags(), $tags));
        }
        return $resource;
    }
    public static function stop($timer){
        if(PINBA){
            pinba_timer_stop($timer);
        }
    }
}