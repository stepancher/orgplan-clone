<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.04.2015
 * Time: 14:36
 */
class ParserExcel
{

	/**
	 * @param SimpleXMLElement $xml
	 * @param bool $firstLine
	 * @return array
	 */
	static function parseDocument($xml, $firstLine = false)
	{
		$headers = static::parseRow($xml->Worksheet->Table->Row[0]);
		$data = [];
		if (true || !$firstLine) {
			for ($i = 1, $lnI = sizeof($xml->Worksheet->Table->Row); $i < $lnI; $i++) {
				$row = static::parseRow($xml->Worksheet->Table->Row[$i]);
				if (strlen(trim(implode('', $row))) > 0) {
					$data[] = $row;
				}
			}
		}

		return [
			'headers' => $headers,
			'data' => $data
		];
	}

	static function parseRow($row)
	{
		$data = [];
		for ($j = 0, $index = 0, $lnJ = sizeof($row->Cell); $j < $lnJ; $j++, $index++) {

			list($cellIndex, $cellMergeAcross, $value) = static::parseCell($index, clone $row->Cell[$j]);
			for ($x = $index; $x < $cellIndex; $x++) {
				$data[$x] = null;
			}
			$data[$cellIndex] = trim($value);

			$index = $cellIndex;
			$index+= $cellMergeAcross;
		}
		return $data;
	}

	/**
	 * @param $index
	 * @param SimpleXMLElement $cell
	 * @return array
	 */
	static function parseCell($index, $cell)
	{
		$attributes = [];
		foreach ($cell->attributes('ss', true) as $k => $v) {
			$attributes[$k] = strval($v);
		}

		$cellIndex = $index;
		$cellMergeAcross = 0;
		if (!empty($attributes['Index'])) {
			$cellIndex = intval($attributes['Index'])-1;
		}
		elseif (!empty($attributes['MergeAcross'])) {
			$cellMergeAcross = intval($attributes['MergeAcross']);
		}

		$value = trim(strval($cell->Data));

		$data = [];
		if (sizeof($cell->xpath('ss:Data'))) {
			foreach ($cell->xpath('ss:Data')[0] as $item) {
				$data[] = $item->asXML();
			}
		}
		if (sizeof($data)) {
			$value = implode('', $data);
			// Убираем стили из ячейки
			$value = preg_replace('#\<Font[^\>]+>#', '', $value);
			$value = str_replace('</Font>', '', $value);
		}

		return [
			$cellIndex,
			$cellMergeAcross,
			$value
		];
	}
}