<?php

/**
 * Created by PhpStorm.
 * User: user93
 * Date: 14.10.15
 * Time: 16:49
 */
class DHtmlXFileHelper
{

    static function getBehaviorName()
    {
        return 'DHtmlXFileBehavior';
    }

    /**
     * @return CHttpSession
     */
    static function getStore()
    {
        return Yii::app()->session;
    }

    static function getId()
    {
        return static::getStore()->get('calendar-event-id');
    }

    static function setId($id)
    {
        static::getStore()->add('calendar-event-id', $id);
        static::clearSessionFiles();

    }

    static function getSessionFiles()
    {
        return static::getStore()->get('calendar-event-files', []);
    }

    static function setSessionFiles($fileList)
    {
        static::getStore()->add('calendar-event-files', $fileList);
    }

    static function clearSessionFiles()
    {
        static::setSessionFiles([]);
    }

    /**
     * @param UploadedFile $file
     * @param array $data AdditionalData
     * @return bool
     */
    static function addSessionFile($file, $data = [])
    {
        return static::opSessionFile('add', $file, $data);
    }


    /**
     * @param array $data AdditionalData
     * @return bool
     */
    static function delSessionFile($data = [])
    {
        return static::opSessionFile('del', null, $data);
    }

    /**
     * @param string $operation
     * @param UploadedFile $file
     * @param array $data AdditionalData
     * @return bool
     */
    static function opSessionFile($operation, $file, $data = [])
    {
        $state = true;

        $fileData = $data;
        $fileData['operation'] = $operation;

        if ($file) {
            $fileData['file'] = [
                'name' => $file->getName(),
                'type' => $file->getType(),
                'size' => $file->getSize(),
                'error' => $file->getError()
            ];

            if ($file->getTempName()) {
                $fileTmpName = tempnam($file->getTempName(), 'TSK');
                $state = rename($file->getTempName(), $fileTmpName);
                $fileData['file']['tmp_name'] = $fileTmpName;
            }
        }

        if ($state) {
            $fileList = static::getSessionFiles();
            $fileList[] = $fileData;
            static::setSessionFiles($fileList);
        }

        return $state;
    }

    /**
     * @param integer $taskId
     * @return array|mixed|string
     */
    static function getFileList($taskId)
    {
        $files = [];
        /*$task = Task::model()->findByPk($taskId);
        if ($task) {
            $files = ObjectFile::getFileList($task, ObjectFile::TYPE_TASK_FILES);
            array_walk($files, function (&$file) {
                $file['name'] = $file['filename'];
                $file['serverName'] = $file['id'];
                unset($file['filename']);
                unset($file['id']);
            });
        }*/
        return $files;
    }

    static function addFile($task, $info)
    {
        ObjectFile::createFile(
            new UploadedFile($info['name'], $info['tmp_name'], $info['type'], $info['size'], $info['error']),
            $task, ObjectFile::TYPE_TASK_FILES, ObjectFile::EXT_FILE
        );
    }

    static function delFile($task, $objectFileId)
    {
        $state = true;

        /** @var ObjectFile $file */
        $file = ObjectFile::model()->findByPk($objectFileId);
        if ($file) {
            if ($file->deleteFile($task)) {
                $state = @unlink($file->getFilePath(true));
            }
        }

        return $state;
    }

    /**
     * @param $objectFileId
     * @return null|ObjectFile
     */
    static function getFile($objectFileId)
    {
        /** @var ObjectFile $file */
        $file = ObjectFile::model()->findByPk($objectFileId);
        if ($file) {
            return $file;
        }
        return null;
    }
}


class DHtmlXFileBehavior
{
    /**
     * @param DataAction $action
     */
    static function afterProcessing($action)
    {
        // read
        $fileList = DHtmlXFileHelper::getSessionFiles();

        foreach ($fileList as $fileData) {
            /*$task = Task::model()->findByPk($action->get_new_id());*/
           /* if ($task) {

                switch ($fileData['operation']) {
                    case 'add' :
                        DHtmlXFileHelper::addFile($task, $fileData['file']);
                        break;
                    case 'del' :
                        DHtmlXFileHelper::delFile($task, $fileData['file']);
                        break;
                }
            }*/
        }

        // clear
        DHtmlXFileHelper::clearSessionFiles();
        return true;
    }
}
