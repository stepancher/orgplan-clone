<?php

/**
 * Created by PhpStorm.
 * User: TheCorch
 * Date: 29.12.2014
 * Time: 11:27
 */
Yii::import('vendor.zhdanovartur.yii-easyimage.EasyImage');
class UploadedFile extends CUploadedFile
{
	private $__name;
	private $__tempName;
	private $__type;
	private $__size;
	private $__error;

	public function __construct($name, $tempName, $type, $size, $error)
	{
		parent::__construct($name, $tempName, $type, $size, $error);

		$this->__name = $name;
		$this->__tempName = $tempName;
		$this->__type = $type;
		$this->__size = $size;
		$this->__error = $error;
	}

	public function resize()
	{
		if (in_array(strtoupper($this->extensionName), ObjectFile::$acceptedImageExt)) {
			$image = new EasyImage($this->__tempName);
			$resized = false;
			if ($image->image()->width > 1920) {
				$resized = true;
				$image->image()->resize(1920);
			}
			if ($image->image()->height > 1080) {
				$resized = true;
				$image->image()->resize(null, 1080);
			}
			if ($resized) {
				$image->save($this->__tempName);
			}
		}

	}

}