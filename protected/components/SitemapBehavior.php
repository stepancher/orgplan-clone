<?php

/**
 * Поиск переводов, если язык приложения отличный от русского
 * Class AfterFindTranslateBehavior
 */
class SitemapBehavior extends CActiveRecordBehavior
{
    /**
     * Сохраняем перевод, если требуется.
     * @param CModelEvent $event
     */
    public function afterSave($event)
    {
        return parent::afterSave($event);
    }

}