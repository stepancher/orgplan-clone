<?php

class ZUrlManager extends CUrlManager {

    /**
     * @param CHttpRequest $request
     * @param $lang
     * @return string
     */
    public function switchLang($request, $lang){

        $currentUrlRule = $this->getCurrentUrlRule($request);
        $params = array();

        if(!empty($currentUrlRule->params)){
            foreach($currentUrlRule->params as $key => $value){
                if(!empty($request->getParam($key, NULL))){
                    $params[$key] = $request->getParam($key);
                }
            }
        }

        $params['langId'] = $lang;

        $pathStructure = $this->getPathStructure($request);

        if(
            !empty($pathStructure) &&
            !empty($pathStructure['module']) &&
            !empty(Yii::app()->getModule($pathStructure['module'])) &&
            !empty(Yii::app()->getModule($pathStructure['module'])->getComponent('TrUrl')) &&
            !empty($trComponent = Yii::app()->getModule($pathStructure['module'])->getComponent('TrUrl'))
        ){
            $url = $trComponent->getTrUrl('/'.$this->parseUrl($request), $params);
        }else{
            $url = self::replaceLang($request->requestUri, $params['langId']);
        }

        return $url;
    }

    /**
     * @param CHttpRequest $request
     * @return CUrlRule|mixed|null
     */
    public function getCurrentUrlRule(CHttpRequest $request){

        $result = NULL;

        foreach($this->rules as $pattern => $route){

            if(!is_object($route)){
                $rule = $this->createUrlRule($route,$pattern);
            }else{
                $rule = $route;
            }
            $rawPathInfo = $request->getPathInfo();
            $pathInfo = $this->removeUrlSuffix($rawPathInfo,$this->urlSuffix);

            if(is_array($rule)){
                $this->rules[$pattern] = $rule = Yii::createComponent($rule);
            }

            if(($r = $rule->parseUrl($this,$request,$pathInfo,$rawPathInfo)) !== false){
                $result = $rule;
                break;
            }

        }

        return $result;
    }

    public function getPathStructure($request){

        $path = $this->parseUrl($request);

        $routeArgs = explode('/', $path);
        $info = array();

        if(!empty($this->routePattern)){
            return $info;
        }

        if(count($routeArgs) == 1){
            $info['module'] = NULL;

            if(Yii::app()->hasModule($routeArgs[0])){
                $info['module'] = $routeArgs[0];
                $info['controller'] = Yii::app()->getModule($routeArgs[0])->defaultController;
            }else{
                $info['controller'] = $routeArgs[0];
            }

            $info['action'] = NULL;
        }

        if(count($routeArgs) == 2){
            $info['module'] = NULL;
            $info['controller'] = $routeArgs[0];
            $info['action'] = $routeArgs[1];
        }

        if(count($routeArgs) == 3){
            $info['module'] = $routeArgs[0];
            $info['controller'] = $routeArgs[1];
            $info['action'] = $routeArgs[2];
        }

        return $info;
    }

    public static function replaceLang($url, $lang){

        $parts    = explode('/', $url);
        $parts[1] = $lang;

        return implode('/', $parts);
    }
}