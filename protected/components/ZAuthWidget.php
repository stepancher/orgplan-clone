<?php

class ZAuthWidget extends EAuthWidget
{
    public function run() {
        $this->registerAssets();

        $socialConnected = [];
        if (!Yii::app()->user->isGuest) {
            Yii::import('application.modules.auth.models.SocialAuth');
            $socialConnected = TbHtml::listData(
                SocialAuth::model()->findAllByAttributes(['userId' => Yii::app()->user->id]),
                'id',
                'socialName'
            );
        }

        $this->render('application.modules.auth.views.auth', array(
            'id' => $this->getId(),
            'services' => $this->services,
            'action' => $this->action,
            'socialConnected' => $socialConnected,
        ));
    }
}