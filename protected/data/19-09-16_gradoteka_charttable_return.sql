-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: orgplan
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_migration`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('3', 'ОБЪЕМ И ДОЛЯ ОТРАСЛЕЙ СЕЛЬСКОГО ХОЗЯЙСТВА', '1', '11', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('4', 'РЕГИОНАЛЬНЫЙ ТОВАРНЫЙ ОБЪЕМ СЕЛЬСКОГО ХОЗЯЙСТВА', '3', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('5', 'РЕГИОНАЛЬНЫЙ ТОВАРНЫЙ ОБЪЕМ И ДОЛЯ ОТРАСЛЕЙ СЕЛЬСКОГО ХОЗЯЙСТВА', '1', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('6', 'РЕГИОНАЛЬНЫЙ ТОВАРНЫЙ ОБЪЕМ И ДОЛЯ ОТРАСЛЕЙ СЕЛЬСКОГО ХОЗЯЙСТВА', '1', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('7', 'ОСНОВНЫЕ СЕЛЬСКОХОЗЯЙСТВЕННЫЕ КУЛЬТУРЫ', '2', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('8', 'ОСНОВНАЯ ПРОДУКЦИЯ ЖИВОТНОВОДСТВА', '2', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('9', 'ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ И СЕЛЬСКОХОЗЯЙСТВЕННОЕ СЫРЬЕ', '3', '11', NULL, 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('10', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ СЕЛЬСКОГО ХОЗЯЙСТВА', '2', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('11', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ СЕЛЬСКОГО ХОЗЯЙСТВА', '2', '11', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('15', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '1', '2', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('16', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '3', '2', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('17', 'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ', '3', '2', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('18', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ / ТОП-10 ПО РЕГИОНАМ', '1', '2', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('19', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ / ТОП-10 ПО РЕГИОНАМ', '3', '2', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('20', 'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ / ТОП-10 ПО РЕГИОНАМ', '3', '2', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('21', 'ПРОИЗВОДСТВО ОСНОВНЫХ СТРОИТЕЛЬНЫХ МАТЕРИАЛОВ В РФ', '3', '2', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('22', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '2', '8', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('23', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ (ПРОДОЛЖЕНИЕ)', '3', '8', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('24', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '2', '8', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('25', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ (ПРОДОЛЖЕНИЕ)', '3', '8', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('26', 'ПРОИЗВОДСТВО ОСНОВНЫХ ПИЩЕВЫХ ПРОДУКТОВ / РАСТЕНИЕВОДСТВО', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('27', 'ПРОИЗВОДСТВО ОСНОВНЫХ ПИЩЕВЫХ ПРОДУКТОВ / ЖИВОТНОВОДСТВО', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('28', 'ПРОИЗВОДСТВО НАПИТКОВ', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('29', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('30', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('31', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('32', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ПОТРЕБЛЕНИЯ ПРОДУКТОВ ПИТАНИЯ', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('33', 'ПОТРЕБИТЕЛЬСКАЯ КОРЗИНА', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('34', 'ПОТРЕБИТЕЛЬСКАЯ КОРЗИНА (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('35', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ПОТРЕБЛЕНИЯ ПРОДУКТОВ ПИТАНИЯ', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('36', 'ПОТРЕБИТЕЛЬСКАЯ КОРЗИНА', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('37', 'ПОТРЕБИТЕЛЬСКАЯ КОРЗИНА (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('38', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('39', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('40', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('41', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('42', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('43', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('44', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ (ПРОДОЛЖЕНИЕ)', '2', '14', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('45', 'ПРОИЗВОДСТВО ОСНОВНЫХ ПИЩЕВЫХ ПРОДУКТОВ / РАСТЕНИЕВОДСТВО', '2', '13', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('46', 'ПРОИЗВОДСТВО ОСНОВНЫХ ПИЩЕВЫХ ПРОДУКТОВ / ЖИВОТНОВОДСТВО', '2', '13', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('47', 'ПРОИЗВОДСТВО НАПИТКОВ', '2', '13', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('48', 'СТРУКТУРА СЕЛЬСКОГО ХОЗЯЙСТВА / РАСТЕНИЕВОДСТВО', '2', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('49', 'СТРУКТУРА СЕЛЬСКОГО ХОЗЯЙСТВА / ЖИВОТНОВОДСТВО', '2', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('50', 'ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ И СЕЛЬСКОХОЗЯЙСТВЕННОЕ СЫРЬЕ', '3', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('51', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО', '2', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('52', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО (ПРОДОЛЖЕНИЕ)', '2', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('53', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО', '2', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('54', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО (ПРОДОЛЖЕНИЕ)', '2', '13', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('55', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '3', '1', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('56', 'ОБЪЕМ КРЕДИТОВАНИЯ ФИЗИЧЕСКИХ ЛИЦ', '3', '1', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('57', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '2', '1', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('58', 'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ / ТОП-10 ПО РЕГИОНАМ', '3', '1', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('59', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '2', '3', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('60', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ (ПРОДОЛЖЕНИЕ)', '3', '3', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('61', 'ОБЪЕМ ГЕОЛОГОРАЗВЕДОВАТЕЛЬНЫХ РАБОТ', '3', '3', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('62', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ (ПРОДОЛЖЕНИЕ)', '3', '3', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('63', 'ОБЪЕМ ГЕОЛОГОРАЗВЕДОВАТЕЛЬНЫХ РАБОТ', '3', '3', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('64', 'ЭКСПОРТ/ИМПОРТ МЕТАЛЛОВ И СТАЛЬНЫХ ТРУБ', '2', '3', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('65', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '3', '37', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('66', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ (ПРОДОЛЖЕНИЕ)', '2', '37', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('67', 'ДОБЫЧА НЕФТИ И ГАЗА', '3', '37', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('68', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '3', '37', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('69', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ (ПРОДОЛЖЕНИЕ)', '2', '37', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('70', 'ИМПОРТ/ЭКСПОРТ ГОРЮЧЕ-СМАЗОЧНЫХ МАТЕРИАЛОВ', '2', '37', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('71', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '3', '5', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('72', 'ДРЕВЕСИНА И ЦЕЛЛЮЛОЗА', '3', '5', '', 'state');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('73', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ', '3', '5', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('74', 'ПИЛОМАТЕРИАЛ', '3', '5', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('75', 'ДРЕВЕСИНА И ЦЕЛЛЮЛОЗА', '3', '5', '', 'region');
INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `granularity`, `area`) VALUES ('76', 'ПОКАЗАТЕЛИ ПРОИЗВОДСТВА', '3', '5', '', 'state');
