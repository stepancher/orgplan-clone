-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: orgplan
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_migration`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('283', 'region', 'state', '5', 'Алтайский край', '01000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('284', 'region', 'state', NULL, 'Краснодарский край', '03000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('285', 'region', 'state', '5', 'Красноярский край', '04000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('286', 'region', 'state', '4', 'Приморский край', '05000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('287', 'region', 'state', '8', 'Ставропольский край', '07000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('288', 'region', 'state', '4', 'Хабаровский край', '08000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('289', 'region', 'state', '4', 'Амурская область', '10000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('290', 'region', 'state', NULL, 'Архангельская область', '11000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('291', 'region', 'state', NULL, 'Архангельская область (включая Ненецкий АО)', '11001000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('292', 'region', 'state', NULL, 'Ненецкий автономный округ', '11800000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('293', 'region', 'state', NULL, 'Астраханская область', '12000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('294', 'region', 'state', NULL, 'Белгородская область', '14000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('295', 'region', 'state', NULL, 'Брянская область', '15000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('296', 'region', 'state', NULL, 'Владимирская область', '17000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('297', 'region', 'state', NULL, 'Волгоградская область', '18000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('298', 'region', 'state', NULL, 'Вологодская область', '19000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('299', 'region', 'state', NULL, 'Воронежская область', '20000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('300', 'region', 'state', '7', 'Нижегородская область', '22000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('301', 'region', 'state', NULL, 'Ивановская область', '24000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('302', 'region', 'state', '5', 'Иркутская область', '25000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('303', 'region', 'state', '8', 'Республика Ингушетия', '26000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('304', 'region', 'state', NULL, 'Калининградская область', '27000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('305', 'region', 'state', NULL, 'Тверская область', '28000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('306', 'region', 'state', NULL, 'Калужская область', '29000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('307', 'region', 'state', '4', 'Камчатский край', '30000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('308', 'region', 'state', '5', 'Кемеровская область', '32000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('309', 'region', 'state', '7', 'Кировская область', '33000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('310', 'region', 'state', NULL, 'Костромская область', '34000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('311', 'region', 'state', '9', 'Республика Крым', '35000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('312', 'region', 'state', '7', 'Самарская область', '36000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('313', 'region', 'state', '6', 'Курганская область', '37000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('314', 'region', 'state', NULL, 'Курская область', '38000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('315', 'region', 'state', NULL, 'Город федерального значения Санкт-Петербург', '40000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('316', 'region', 'state', NULL, 'Ленинградская область', '41000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('317', 'region', 'state', NULL, 'Липецкая область', '42000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('318', 'region', 'state', '4', 'Магаданская область', '44000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('319', 'region', 'state', NULL, 'Город федерального значения Москва', '45000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('320', 'region', 'state', NULL, 'Московская область', '46000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('321', 'region', 'state', NULL, 'Мурманская область', '47000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('322', 'region', 'state', NULL, 'Новгородская область', '49000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('323', 'region', 'state', '5', 'Новосибирская область', '50000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('324', 'region', 'state', '5', 'Омская область', '52000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('325', 'region', 'state', '7', 'Оренбургская область', '53000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('326', 'region', 'state', NULL, 'Орловская область', '54000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('327', 'region', 'state', '7', 'Пензенская область', '56000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('328', 'region', 'state', '7', 'Пермский край', '57000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('329', 'region', 'state', NULL, 'Псковская область', '58000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('330', 'region', 'state', NULL, 'Ростовская область', '60000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('331', 'region', 'state', NULL, 'Рязанская область', '61000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('332', 'region', 'state', '7', 'Саратовская область', '63000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('333', 'region', 'state', '4', 'Сахалинская область', '64000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('334', 'region', 'state', '6', 'Свердловская область', '65000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('335', 'region', 'state', NULL, 'Смоленская область', '66000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('336', 'region', 'state', NULL, 'Город федерального значения Севастополь', '67000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('337', 'region', 'state', NULL, 'Тамбовская область', '68000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('338', 'region', 'state', '5', 'Томская область', '69000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('339', 'region', 'state', NULL, 'Тульская область', '70000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('340', 'region', 'state', '6', 'Тюменская область', '71000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('341', 'region', 'state', NULL, 'Тюменская область (включая Ханты-Мансийский АО и Ямало-Ненецкий АО)', '71001000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('342', 'region', 'state', NULL, 'Ханты-Мансийский автономный округ – Югра', '71800000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('343', 'region', 'state', '6', 'Ямало-Ненецкий автономный округ', '71900000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('344', 'region', 'state', '7', 'Ульяновская область', '73000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('345', 'region', 'state', '6', 'Челябинская область', '75000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('346', 'region', 'state', '5', 'Забайкальский край', '76000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('347', 'region', 'state', '4', 'Чукотский автономный округ', '77000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('348', 'region', 'state', NULL, 'Ярославская область', '78000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('349', 'region', 'state', NULL, 'Республика Адыгея (Адыгея)', '79000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('350', 'region', 'state', '7', 'Республика Башкортостан', '80000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('351', 'region', 'state', '5', 'Республика Бурятия', '81000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('352', 'region', 'state', '8', 'Республика Дагестан', '82000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('353', 'region', 'state', '8', 'Кабардино-Балкарская Республика', '83000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('354', 'region', 'state', '5', 'Республика Алтай', '84000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('355', 'region', 'state', NULL, 'Республика Калмыкия', '85000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('356', 'region', 'state', NULL, 'Республика Карелия', '86000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('357', 'region', 'state', NULL, 'Республика Коми', '87000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('358', 'region', 'state', '7', 'Республика Марий Эл', '88000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('359', 'region', 'state', '7', 'Республика Мордовия', '89000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('360', 'region', 'state', '8', 'Республика Северная Осетия - Алания', '90000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('361', 'region', 'state', '8', 'Карачаево-Черкесская Республика', '91000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('362', 'region', 'state', '7', 'Республика Татарстан', '92000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('363', 'region', 'state', '5', 'Республика Тыва', '93000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('364', 'region', 'state', '7', 'Удмуртская Республика', '94000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('365', 'region', 'state', '5', 'Республика Хакасия', '95000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('366', 'region', 'state', '8', 'Чеченская Республика', '96000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('367', 'region', 'state', NULL, 'Чувашская Республика - Чувашия', '97000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('368', 'region', 'state', '4', 'Республика Саха (Якутия)', '98000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('369', 'region', 'state', '4', 'Еврейская автономная область', '99000000');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('370', 'state', '', NULL, 'Центральный федеральный округ', '01');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('371', 'state', '', NULL, 'Северо-Западный федеральный округ', '02');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('372', 'state', '', NULL, 'Южный федеральный округ', '03');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('373', 'state', '', NULL, 'Северо-Кавказский федеральный округ', '04');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('374', 'state', '', NULL, 'Приволжский федеральный округ', '05');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('375', 'state', '', NULL, 'Уральский федеральный округ', '06');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('376', 'state', '', NULL, 'Сибирский федеральный округ', '07');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('377', 'state', '', NULL, 'Дальневосточный федеральный округ', '08');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('378', 'state', '', NULL, 'Крымский федеральный округ', '09');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('379', 'country', '', NULL, 'Российская Федерация', '1');