-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: orgplan
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_migration`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('1', '1', '5');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('2', '10', '30');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('3', '21', '3');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('4', '33', '4');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('5', '2', '6');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('6', '3', '7');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('7', '34', '8');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('8', '22', '9');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('9', '4', '10');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('10', '82', '11');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('11', '68', '12');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('12', '5', '13');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('13', '70', '14');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('14', '38', '15');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('15', '23', '16');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('16', '6', '17');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('17', '77', '18');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('18', '39', '19');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('19', '71', '20');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('20', '50', '21');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('21', '7', '22');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('22', '32', '23');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('23', '69', '24');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('24', '57', '25');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('25', '8', '26');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('26', '24', '27');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('27', '9', '28');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('28', '80', '29');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('29', '11', '31');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('30', '26', '32');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('31', '29', '33');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('32', '51', '34');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('33', '27', '35');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('34', '72', '36');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('35', '73', '37');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('36', '52', '38');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('37', '12', '39');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('38', '53', '40');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('39', '49', '41');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('40', '76', '42');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('41', '28', '43');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('42', '30', '44');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('43', '63', '45');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('44', '43', '46');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('45', '64', '47');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('46', '36', '48');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('47', '37', '49');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('48', '31', '50');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('49', '19', '51');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('50', '20', '52');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('51', '86', '93');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('52', '33', '53');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('53', '45', '54');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('54', '75', '55');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('55', '40', '56');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('56', '46', '57');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('57', '65', '58');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('58', '66', '59');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('59', '35', '60');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('60', '13', '61');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('61', '54', '62');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('62', '55', '64');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('63', '81', '65');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('64', '58', '66');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('65', '14', '67');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('66', '42', '68');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('67', '15', '69');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('68', '16', '70');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('69', '74', '71');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('70', '17', '72');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('71', '59', '73');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('72', '47', '74');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('73', '56', '75');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('74', '78', '76');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('75', '61', '77');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('76', '60', '78');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('77', '41', '79');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('78', '48', '80');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('79', '83', '81');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('80', '62', '82');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('81', '18', '83');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('82', '67', '1');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('83', '25', '63');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('84', '79', '2');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('85', '21', '88');
INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES ('86', '21', '89');
