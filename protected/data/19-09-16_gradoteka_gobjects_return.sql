-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: orgplan
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_migration`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('175', 'region', 'state', '1', 'Белгородская область', '1');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('176', 'region', 'state', '1', 'Брянская область', '2');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('177', 'region', 'state', '1', 'Владимирская область', '3');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('178', 'region', 'state', '1', 'Воронежская область', '4');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('179', 'region', 'state', '1', 'Ивановская область', '5');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('180', 'region', 'state', '1', 'Калужская область', '6');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('181', 'region', 'state', '1', 'Костромская область', '7');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('182', 'region', 'state', '1', 'Курская область', '8');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('183', 'region', 'state', '1', 'Липецкая область', '9');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('184', 'region', 'state', '1', 'Город Москва', '10');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('185', 'region', 'state', '1', 'Московская область', '11');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('186', 'region', 'state', '1', 'Орловская область', '12');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('187', 'region', 'state', '1', 'Рязанская область', '13');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('188', 'region', 'state', '1', 'Смоленская область', '14');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('189', 'region', 'state', '1', 'Тамбовская область', '15');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('190', 'region', 'state', '1', 'Тверская область', '16');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('191', 'region', 'state', '1', 'Тульская область', '17');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('192', 'region', 'state', '1', 'Ярославская область', '18');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('193', 'region', 'state', '3', 'Республика Карелия', '19');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('194', 'region', 'state', '3', 'Республика Коми', '20');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('195', 'region', 'state', '3', 'Архангельская область', '21');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('196', 'region', 'state', '3', 'Вологодская область', '22');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('197', 'region', 'state', '3', 'Калининградская область', '23');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('198', 'region', 'state', '3', 'Ленинградская область', '24');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('199', 'region', 'state', '3', 'Город Санкт-Петербург', '25');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('200', 'region', 'state', '3', 'Мурманская область', '26');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('201', 'region', 'state', '3', 'Новгородская область', '27');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('202', 'region', 'state', '3', 'Псковская область', '28');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('203', 'region', 'state', '3', 'Ненецкий автономный округ', '29');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('204', 'region', 'state', '2', 'Республика Адыгея', '30');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('205', 'region', 'state', '2', 'Республика Калмыкия', '31');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('206', 'region', 'state', '2', 'Краснодарский край', '32');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('207', 'region', 'state', '2', 'Астраханская область', '33');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('208', 'region', 'state', '2', 'Волгоградская область', '34');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('209', 'region', 'state', '2', 'Ростовская область', '35');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('210', 'region', 'state', '8', 'Республика Дагестан', '36');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('211', 'region', 'state', '8', 'Республика Ингушетия', '37');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('212', 'region', 'state', '8', 'Кабардино-Балкарская Республика', '38');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('213', 'region', 'state', '8', 'Карачаево-Черкесская Республика', '39');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('214', 'region', 'state', '8', 'Республика Северная Осетия-Алания', '40');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('215', 'region', 'state', '8', 'Чеченская Республика', '41');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('216', 'region', 'state', '8', 'Ставропольский край', '42');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('217', 'region', 'state', '7', 'Республика Башкортостан', '43');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('218', 'region', 'state', '7', 'Республика Марий Эл', '44');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('219', 'region', 'state', '7', 'Республика Мордовия', '45');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('220', 'region', 'state', '7', 'Республика Татарстан', '46');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('221', 'region', 'state', '7', 'Удмуртская Республика', '47');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('222', 'region', 'state', '7', 'Чувашская Республика', '48');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('223', 'region', 'state', '7', 'Пермский край', '49');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('224', 'region', 'state', '7', 'Кировская область', '50');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('225', 'region', 'state', '7', 'Нижегородская область', '51');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('226', 'region', 'state', '7', 'Оренбургская область', '52');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('227', 'region', 'state', '7', 'Пензенская область', '53');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('228', 'region', 'state', '7', 'Самарская область', '54');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('229', 'region', 'state', '7', 'Саратовская область', '55');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('230', 'region', 'state', '7', 'Ульяновская область', '56');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('231', 'region', 'state', '6', 'Курганская область', '57');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('232', 'region', 'state', '6', 'Свердловская область', '58');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('233', 'region', 'state', '6', 'Тюменская область', '59');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('234', 'region', 'state', '6', 'Челябинская область', '60');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('235', 'region', 'state', '6', 'Ханты-Мансийский автономный округ — Югра', '61');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('236', 'region', 'state', '6', 'Ямало-Ненецкий автономный округ', '62');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('237', 'region', 'state', '5', 'Республика Алтай', '63');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('238', 'region', 'state', '5', 'Республика Бурятия', '64');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('239', 'region', 'state', '5', 'Республика Тыва', '65');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('240', 'region', 'state', '5', 'Республика Хакасия', '66');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('241', 'region', 'state', '5', 'Алтайский край', '67');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('242', 'region', 'state', '5', 'Забайкальский край', '68');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('243', 'region', 'state', '5', 'Красноярский край', '69');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('244', 'region', 'state', '5', 'Иркутская область', '70');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('245', 'region', 'state', '5', 'Кемеровская область', '71');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('246', 'region', 'state', '5', 'Новосибирская область', '72');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('247', 'region', 'state', '5', 'Омская область', '73');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('248', 'region', 'state', '5', 'Томская область', '74');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('249', 'region', 'state', '4', 'Республика Саха (Якутия)', '75');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('250', 'region', 'state', '4', 'Приморский край', '76');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('251', 'region', 'state', '4', 'Камчатский край', '77');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('252', 'region', 'state', '4', 'Хабаровский край', '78');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('253', 'region', 'state', '4', 'Амурская область', '79');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('254', 'region', 'state', '4', 'Магаданская область', '80');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('255', 'region', 'state', '4', 'Сахалинская область', '81');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('256', 'region', 'state', '4', 'Еврейская автономная область', '82');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('257', 'region', 'state', '4', 'Чукотский автономный округ', '83');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('263', 'state', '', NULL, 'Центральный федеральный округ', '1');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('264', 'state', '', NULL, 'Северо-Западный федеральный округ', '3');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('265', 'state', '', NULL, 'Сибирский федеральный округ', '5');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('266', 'state', '', NULL, 'Уральский федеральный округ', '6');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('267', 'state', '', NULL, 'Приволжский федеральный округ', '7');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('268', 'state', '', NULL, 'Южный федеральный округ', '2');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('269', 'state', '', NULL, 'Дальневосточный федеральный округ', '4');
INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES ('270', 'state', '', NULL, 'Северо-Кавказский федеральный округ', '8');
