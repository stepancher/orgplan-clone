<?php echo CHtml::tag('h1', [], 'Создать аналитику по отрасли');?>
<?php echo TbHtml::beginFormTb('vertical',$this->createUrl('saveChartGroup'), 'post');?>

<?php echo TbHtml::activeLabel($model, 'id');?>
<?php $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
    'model'=>$model,
    'attribute'=> 'id',
    'data' =>CHtml::listData(Industry::model()->findAll(), 'id', 'name'),
    'htmlOptions'=>array(
        'empty' => '-',
        'style' => 'width:60%;',
    ),
));?>

<?php echo TbHtml::activeLabel($model, 'header');?>
<?php echo TbHtml::activeTextField($model, 'header');?>

<?php echo TbHtml::activeLabel($model, 'headerColor');?>
<?php echo TbHtml::activeTextField($model, 'headerColor');?>

<?php echo TbHtml::activeLabel($model, 'priority');?>
<?php echo TbHtml::activeTextField($model, 'priority');?>

<div class="clear"></div>

<?php echo TbHtml::submitButton('Сохранить', array('style' => 'margin-top:20px;'));?>
<?php echo TbHtml::endForm();?>