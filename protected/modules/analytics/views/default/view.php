<?php
/**
 * @var CController $this
 * @var Analytics $model
 * @var CWebApplication $app
 */

$industry = NULL;
if(!empty($model->industryId)){
    $industry = Industry::model()->findByPk($model->industryId);
}

if(!empty($industry) && $industry->checkAccess()){
    $statsAccess = User::checkTariff(1);
    $pageParams = array();
    $pageParams['analytics-grid-request'] = isset($_GET['analytics-grid-request'])?$_GET['analytics-grid-request']:'';
    $pageParams['sort'] = isset($_GET['sort'])?$_GET['sort']:'';
    $pageParams['industry'] = $industry->id;
    $pageParams['open'] = $statsAccess;
?>
<div style="max-width:960px;">
    <div id="industry_tab_container">
        <div class="col-md-12">
            <?php //@TODO refactor separated h1_header?>
            <?=$this->renderPartial('_blocks/_h1_header', array(
                'model' => $model,
                'industry' => $industry,
            ), true)?>
        </div>
        <div class="cols-row">
            <?php
            if($this->beginCache(sha1(serialize($pageParams)), ['duration' => 3600 * 24 * 365])){

                $this->renderPartial('application.modules.analytics.views.default._blocks._industry_analytic', array(
                    'industry' => $industry,
                    'statsAccess' => $statsAccess,
                    'model' => $model,
                ));

                $this->endCache();
            }
            ?>
        </div>
    </div>
</div>
<?php }?>

