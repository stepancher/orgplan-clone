<div class="cols-row">
    <div class="col-md-12">
        <?php if (Yii::app()->user->getState('role') == User::ROLE_ADMIN): ?>
            <a href="<?= Yii::app()->createUrl('admin/industry/save', array('id' => $model->id)) ?>"
               class="b-button b-button-icon f-text--tall d-text-light d-bg-warning button-edit"
               data-icon="&#xe0ea;"></a>
        <?php endif; ?>
        <?php
            $this->renderPartial('application.modules.analytics.views.default._blocks._b-header', array(
                'description' => Yii::t('AnalyticsModule.analytics', 'industry detail information'),
                'industry' => $industry,
            ));
        ?>
        <?php /*
        <div class="cols-row">
            <div class="col-md-12">
                <div class="f-text--base">
                    <?= $model->description ?: null ?>
                </div>
            </div>
        </div>
        */ ?>
    </div>
<?php /*
    <div class="col-md-4">
        <div class="offset-small-t-2">
            <div>
                <div>
                    <?= CHtml::link(
                        Yii::t('AnalyticsModule.analytics', 'Back to search'),
                        $this->createUrl(
                            '/search',
                            ['sector' => Search::getSectorNames(null, Search::SECTOR_FAIR)]
                        ),
                        array(
                            'class' => 'js-b-back-history b-button  b-button--no-margin offset-small-b-2 b-button d-bg-base d-bg-bordered d-bg-input--hover d-text-dark btn button-back'
                        )
                    ) ?>
                </div>
                <?php if (IS_MODE_DEV): ?>
                    <a href="#"
                       title="<?= Yii::t('AnalyticsModule.analytics', 'compare') ?>"
                       class="js-add-to-mach b-button b-button-icon d-bg-secondary-icon icon-back  btn b-button--no-margin p-pull-right"
                       data-icon="&#xe3d3;"></a>
                <?php endif; ?>

                <a data-id="<?= $model->id ?>"
                   data-class="<?= get_class($model) ?>"
                   data-toggle="<?= !Yii::app()->user->isGuest ?: 'modal' ?>"
                   data-target="<?= !Yii::app()->user->isGuest ?: '#modal-sign-up' ?>"
                   title="<?= !Favorites::checkFavorite($model->id, Yii::app()->user->id, Favorites::TYPE_FAIR_FAVORITES) ? Yii::t('AnalyticsModule.analytics', 'save to bookmarks') : Yii::t('AnalyticsModule.analytics', 'Remove from favorites') ?>"
                   class="icon-back add-to-favorite d-bg-danger--hover p-pull-right
                   <?= Favorites::getClassForFavoriteBlock($model->id,
                       Yii::app()->user->id,
                       Favorites::TYPE_FAIR_FAVORITES, ['d-bg-danger s-i', 'd-bg-diaphanous-selected-menu d-bg-secondary-icon']); ?>
                    b-button b-button--no-margin b-button-icon f-text--tall d-text-light"
                   data-icon="&#xe429;"
                   href="#">
                </a>
            </div>
        </div>
    </div>
</div>
 */?>