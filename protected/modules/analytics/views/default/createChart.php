<?php echo CHtml::tag('h1', [], 'Создать Чарт');?>
<?php echo TbHtml::beginFormTb('vertical',$this->createUrl('saveChart'), 'post');?>
    <?php echo TbHtml::activeLabel($model, 'typeId');?>
    <?php
        $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
            'model'=>$model,
            'attribute'=> 'typeId',
            'data' => CHtml::listData(
                GStatTypes::getListByType(GStatTypes::GTYPE_REGION),
                'gId',
                function($el){
                    return $el->name.(!empty($el->description)?' ('.ZHtml::cutString($el->description, 100).')':'');
                }
            ),
            'htmlOptions'=>array(
                'empty'=>' - ',
                'style' => 'width:60%;',
            ),
        ));
    ?>

    <?php echo TbHtml::activeLabel($model, 'widget');?>
    <?php
        echo CHtml::activeDropDownList($model,'widget',  AnalyticsService::getWidgetsTypes(), array(
            'empty' => '-',
            'ajax' => array(
                'type'=>'POST',
                'url'=>CController::createUrl('analytics/default/loadWidgetTypes'),
                'update'=>'#widgetType',
            ),
        ));
    ?>

    <?php echo TbHtml::activeLabel($model, 'widgetType');?>
    <?php echo CHtml::dropDownList('widgetType','', array());?>

    <?php echo TbHtml::activeLabel($model, 'header');?>
    <?php echo TbHtml::activeTextField($model, 'header');?>

    <?php echo TbHtml::activeLabel($model, 'measure');?>
    <?php echo TbHtml::activeTextField($model, 'measure');?>

    <?php echo TbHtml::activeLabel($model, 'color');?>
    <?php
        echo TbHtml::dropDownList('color', '',  array(
            '#a0d468' => 'GREEN',
            '#4ec882' => 'LIGHT_GREEN',
            '#fa694c' => 'RED',
            '#43ade3' => 'BLUE',
            '#2b98d5' => 'AZURE',
            '#42c0b4' => 'LIGHT_AZURE',
            '#f6a800' => 'ORANGE',
        ), array(
            'empty' => ' - ',
        ));
    ?>
    <?php echo TbHtml::activeLabel($model, 'groupId');?>
    <?php echo TbHtml::activeDropDownList($model, 'groupId', CHtml::listData(ChartGroup::model()->findAll(), 'id', function($el){return $el->id.' - '.$el->header;}));?>

    <?php echo TbHtml::activeLabel($model, 'related');?>

    <?php $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
        'model'=>$model,
        'attribute'=> 'related',
        'data' =>CHtml::listData(Chart::model()->findAll(), 'id', function($el){return $el->id.' - '.$el->header;}),
        'htmlOptions'=>array(
            'empty' => '-',
            'style' => 'width:60%;',
        ),
    ));?>

    <?php echo TbHtml::activeLabel($model, 'parent');?>

    <?php $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
        'model'=>$model,
        'attribute'=> 'parent',
        'data' =>CHtml::listData(Chart::model()->findAll(), 'id', function($el){return $el->id.' - '.$el->header;}),
        'htmlOptions'=>array(
            'empty' => '-',
            'style' => 'width:60%;',
        ),
    ));?>

    <div class="clear"></div>

    <?php echo TbHtml::submitButton('Сохранить', array('style' => 'margin-top:20px;'));?>
<?php echo TbHtml::endForm();?>


