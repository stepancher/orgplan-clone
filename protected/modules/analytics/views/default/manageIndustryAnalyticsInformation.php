<?php
/**
 * @var CController $this
 * @var AR $model
 */
$ctrl = $this;
$ctrl->widget('application.modules.analytics.components.FieldSetView', array(
    'header' => Yii::t('AnalyticsModule.analytics', 'Grid header'),
    'items' => array(
        array(
            'application.modules.analytics.components.ActionsView',
            'items' => array(
                'add' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('AnalyticsModule.analytics', 'Grid action add'),
                    array(
                        'url' => $this->createUrl('save'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS
                    )
                ),
            )
        ),
        array(
            'application.widgets.' . get_class($model) . 'GridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{edit}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'edit' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array()),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('saveIndustryAnalyticsInformation', array("id" => $data->id));
                            },
                            'options' => array('title' => 'Редактирование', 'class' => 'btn btn-small btn-primary'),
                        ),
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array()),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('deleteIndustryAnalyticsInformation', array("id" => $data->id));
                            },
                            'options' => array('title' => 'Удаление', 'class' => 'btn btn-small btn-danger'),
                        ),
                    )
                )
            )
        ),
    )
));
