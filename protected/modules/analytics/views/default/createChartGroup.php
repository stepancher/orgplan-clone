<?php echo CHtml::tag('h1', [], 'Создать группу чартов');?>
<?php echo TbHtml::beginFormTb('vertical',$this->createUrl('saveChartGroup'), 'post');?>

<?php echo TbHtml::activeLabel($model, 'header');?>
<?php echo TbHtml::activeTextField($model, 'header');?>

<?php echo TbHtml::activeLabel($model, 'type');?>
<?php
    echo TbHtml::dropDownList('type', '',
        ChartGroup::getChartsGroups(),
        array(
            'empty' => ' - ',
        )
    );
?>

<?php echo TbHtml::activeLabel($model, 'industryId');?>
<?php $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
    'model'=>$model,
    'attribute'=> 'industryId',
    'data' =>CHtml::listData(IndustryAnalyticsInformation::model()->findAll(), 'industryId', 'header'),
    'htmlOptions'=>array(
        'empty' => '-',
        'style' => 'width:60%;',
    ),
));?>

    <div class="clear"></div>

    <?php echo TbHtml::submitButton('Сохранить', array('style' => 'margin-top:20px;'));?>
<?php echo TbHtml::endForm();?>


