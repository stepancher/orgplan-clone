<?php
/**
 * @var CController $this
 * @var CActiveDataProvider $dataProvider
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerCssFile($am->publish($app->theme->basePath . '/assets' . '/css/main.css'));
$cs->registerCssFile($am->publish($app->theme->basePath . '/assets' . '/css/autoComplete/style.css'));
$cs->registerScriptFile($am->publish($app->theme->basePath . '/assets' . '/js/autoComplete/script.js'));
$cs->registerScriptFile($am->publish($app->theme->basePath . '/assets' . '/js/analytics/script.js'));

$industries = Industry::model()->findAll();
$this->widget('zii.widgets.CBreadcrumbs', array(
	'separator' => ' / ',
	'links' => array(
		Yii::t('AnalyticsModule.analytics', 'list of industries'),
	),
	'htmlOptions' => array(
		'class' => 'breadcrumbs'
	)
));
?>
<script type="text/javascript">
	$(function () {
		new AutoComplete({
			data: <?=json_encode(CHtml::listData(Industry::model()->findAll(), 'id', 'name'))?>,
			element: $('.auto-complete.input-name'),
			emptyValue: '<?= Yii::t('AnalyticsModule.analytics','all industries')?>',
			init: function (e) {
				$('.drop-box', e.config.element).hide();

				setTimeout(function () {
					$('.drop-box div', e.config.element).click(function () {
						$('.auto-complete-input', e.config.element).val($(this).text());
						$('.input-hidden', e.config.element).val($(this).data('id'));
					});
				}, 100);

				$('.auto-complete-input', e.config.element).focus(function () {
					$('.drop-box', e.config.element).show();
				});

				$('.auto-complete-input', e.config.element).blur(function () {
					setTimeout(function () {
						$('.drop-box', e.config.element).hide();
					}, 200)
				});

				e.config.element.find('.input-arrow').click(function () {
					$('.auto-complete-input', e.config.element).focus();
				});

			}
		});

	});
</script>
<style>
	.auto-complete {
		margin-top: 10px;
		float: right;
	}

	.enter-text-input {
		width: 270px;
	}

	.widget-UserForm .auto-complete {
		margin-top: 0;
		float: none;
	}

	.widget-UserForm .enter-text-input {
		width: auto;
	}
</style>

<div>
	<div class="auto-complete input-name">
		<div class="controls">
			<div class="enter-text-input">
				<input class="auto-complete-input" type="text">
				<input id="Analytics_industryId" class="input-hidden" value="" type="hidden">

				<div class="input-arrow">
					<div class="auto-complete-arrow"></div>
				</div>
			</div>
			<div class="drop-box"></div>
		</div>
	</div>
	<h1><?= Yii::t('AnalyticsModule.analytics', 'industry') ?></h1>

</div>

<?=
$app->user->checkAccess('analyticsActionSave')
	? TbHtml::linkButton(Yii::t('AnalyticsModule.analytics', 'Grid action add'), array(
	'class' => 'btn-success',
	'url' => $this->createUrl('analytics/default/save'),
))
	: null ?>
<p class="tudayinportal">
	<b style="font-weight: 100"><?php Yii::t('AnalyticsModule.analytics', 'today the portal') ?></b>
	<b style="font-weight: 600">
		<?= H::getCountText(count(Industry::model()->findAll()), array(Yii::t('AnalyticsModule.analytics', 'sectors'),
			(Yii::t('AnalyticsModule.analytics', 'branch')
			),
			(Yii::t('AnalyticsModule.analytics', 'industry')
			)
		)) ?>
	</b>
</p>
<?php
if (($page = Page::model()->findByAttributes(array(
		'type' => Page::TYPE_INFO_ABOUT_ANALYTICS,
		'status' => Page::STATUS_PUBLISHED))) !== null
) {
	?>
	<div style="clear: both"></div>
	<div class="global-description">
		<div class="page-name" style="font-size: 24px">
			<?= $page->name ?: '' ?>
		</div>
		<div class="page-description">
			<?= $page->description ?: '' ?>
		</div>
	</div> <?php
};

$array = array();
foreach ($dataProvider->getData() as $analytic) {
	$array[] = array(
		'image' => H::getImageUrl($analytic, 'image'),
		'caption' => '<b style="font-size: 18px; color: #112931; bottom: 20px;">' . $analytic->theme . '</b>',
		'url' => $this->createUrl('analytics/default/view', array('id' => $analytic->id)),
		'htmlOptions' => array('target' => '_blank', 'style' => 'position: relative; height: auto; width: 284px; margin-left: -20px; margin-bottom: 10px; background-size: cover; text-align: center'),
		'imageOptions' => array('style' => 'width: 284px; height: 150px; margin-top: 15px;'),
	);
}
?>
<div id="analytics-thumbnails">
	<?= TbHtml::thumbnails($array) ?>
	<?php
	$this->widget('TbPager', array(
		'pages' => $dataProvider->getPagination(),
	))
	?>
</div>
<style type="text/css">
	.tudayinportal {
		font-weight: normal;
		font-size: 26px;
	}

	ul li:before {
		background: none;
	}

	a:hover {
		text-decoration: none;
	}

</style>
<script type="text/javascript">
	$(function () {
		var $thumbnails = $('.thumbnails li .thumbnail');
		var max = 0;

		$thumbnails.each(function (i, elm) {
			if (max < $(elm).height()) {
				max = $(elm).height();
			}
		});

		$thumbnails.height(max);
	})
</script>
