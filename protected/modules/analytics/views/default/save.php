<?php
/**
 * @var DefaultController $this
 * @var Analytics $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$cs = $app->clientScript;
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.analytics.assets'), false, -1, YII_DEBUG);
$cs->registerScriptFile($publishUrl.'/js/historyButton/script.js');
?>

<h1><?= Yii::t('AnalyticsModule.analytics', $model->isNewRecord ? 'Form header create' : 'Form header update') ?></h1>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('analytics/default/save', !empty($model->id) ? array('id' => $model->id) : array()) ?>">

	<div class="form-group field-analytics-theme">
		<label class="control-label" for="Analytics_theme"><?= Yii::t('AnalyticsModule.analytics', 'Theme') ?></label>
		<input type="text" id="Analytics_theme" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Analytics[theme]" value="<?= $model->theme ?>">
		<div class="help-block"></div>
	</div>

	<?php
	echo $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
			'model' => $model,
			'attribute' => 'description',
			'options' => array(
				'minHeight' => '250px',
				'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array('modelName' => get_class($model))),
				'lang' => 'ru',
				'iframe' => true,
				'css' => 'wym.css',
			)
		), true
	);
	?>

	<?php
	Yii::app()->getClientScript()->registerScriptFile($publishUrl . '/' . 'js/dropdown/script.js', \CClientScript::POS_END);

	echo TbHtml::activeControlGroup(
			TbHtml::INPUT_TYPE_DROPDOWNLIST,
			$model,
			'industryId',
			array(
				'groupOptions' => array(
					'class' => 'js-drop-down-list drop-down-list',
				),
				'labelOptions' => array(
					'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
				),
				'input' => TbHtml::dropDownList(
					'Analytics[industryId]',
					$model->industry->id,
					CHtml::listData(\Industry::model()->findAll(), 'id', 'name')
				),
			)
		);
	?>
	<?php
	echo $this->widget('application.modules.analytics.components.StyledMultiFileUploader', \TbArray::merge([
			'model' => $model,
			'attribute' => 'image',
			'max' => 4,
			'accept' => array(),
			'maxMessage' => Yii::t('AnalyticsModule.analytics', 'Limit of files is exceeded'),
			'duplicate' => Yii::t('AnalyticsModule.analytics', 'This file is already available'),
			'deleteAction' => Yii::app()->controller->createUrl('file/fieldHasFile/remove', ['id' => '{id}']),
			'fileData' => FieldHasFile::getFieldFiles($model, 'image'),
		],
		[
			'max' => 1,
			'outputAsImage' => true,
			'fileData' => \ObjectFile::getFileList($model, \ObjectFile::TYPE_ANALYTICS_IMAGE, 'image'),
			'deleteAction' => Yii::app()->controller->createUrl('analytics/default/removeImage', array('id' => '{id}')),
			'buttonTag' => 'button',
			'buttonOptions' => [
				'class' => 'b-button d-bg-success-dark d-bg-success--hover',
				'type' => 'button',
				'content' => Yii::t('AnalyticsModule.analytics', 'Upload image')
			]
		]
	), true);
	?>
	<div class="cols-row">
		<div class="col-md-12">
			<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AnalyticsModule.analytics', 'Save') ?>
			</button>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
			   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AnalyticsModule.analytics', 'Cancel') ?>
			</a>
		</div>
	</div>
</form>