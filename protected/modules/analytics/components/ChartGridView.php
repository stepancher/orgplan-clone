<?php
/**
 * File ChartGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');
Yii::import('app.modules.gradoteka.models.*');
Yii::import('ext.ZLineChart.ZLineChart');
Yii::import('application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart');

/**
 * Class ChartGridView
 *
 * @property Chart $model
 */
class ChartGridView extends TbGridView
{
    /**
     * Модель Chart::model() or new Chart('scenario')
     * @var Chart	 */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        $this->dataProvider->getPagination()->pageSize = 1000;

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns(){
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'name' => array(
                'name' => 'name',
            ),
            'typeId' => array(
                'name' => 'typeId',
                'value' => function($data){
                    $value = GStatTypes::model()->findByAttributes(array('gId' => $data->typeId));
                    if(!empty($value) && !empty($value->name)){
                        return $value->name;
                    }else{
                        return 'отсутствует';
                    }
                }
            ),
//            'objId' => array(
//                'name' => 'objId',
//            ),
            'epoch' => array(
                'name' => 'epoch',
            ),
//            'setId' => array(
//                'name' => 'setId',
//            ),
            'widget' => array(
                'name' => 'widget',
                'value' => function($data){
                    if (!empty($data->widget)){
                        return AnalyticsService::getWidgetTypeById($data->widget);
                    }
                    return '';
                }
            ),
            'widgetType' => array(
                'name' => 'widgetType',
                'value' => function($data){
                    if (!empty($data->widgetType)){
                        return AnalyticsService::getWidgetSubTypeById($data->widget, $data->widgetType);
                    }
                    return '';
                }
            ),
            'header' => array(
                'name' => 'header',
            ),
            'measure' => array(
                'name' => 'measure',
            ),
            'color' => array(
                'name' => 'color',
                'value' => function($data){
                    echo AnalyticsService::renderChartById($data->id, $this);
                },
            ),
            'groupId' => array(
                'name' => 'groupId',
            ),
            'related' => array(
                'name' => 'related',
            ),
            'parent' => array(
                'name' => 'parent',
            ),
        );
        return $columns;
    }
}