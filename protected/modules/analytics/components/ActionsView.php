<?php
/**
 * File ActionsView.php
 */
Yii::import('ext.widgets.RowView');

/**
 * Class ActionsView
 */
class ActionsView extends RowView
{

	public $htmlOptions = array(
		'class' => 'form-actions'
	);
	public $itemsTagName = 'span';

	public function renderItems()
	{
		echo CHtml::openTag($this->itemsWrapTagName, $this->itemsWrapOptions) . "\n";
		foreach ($this->items as &$item) {
			if (!empty($item)) {
				echo CHtml::openTag($this->itemsTagName, $this->itemsOptions) . "\n";
				echo call_user_func_array('TbHtml::btn', $item) . "\n";
				echo CHtml::closeTag($this->itemsTagName);
			}
		}
		echo CHtml::closeTag($this->itemsWrapTagName);
	}
}