<?php
/**
 * File ChartGroupGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');
Yii::import('app.modules.gradoteka.models.*');
Yii::import('ext.ZLineChart.ZLineChart');
Yii::import('application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart');

/**
 * Class ChartGroupGridView
 *
 * @property ChartGroup $model
 */
class ChartGroupGridView extends TbGridView
{
    /**
     * Модель ChartGroup::model() or new ChartGroup('scenario')
     * @var ChartGroup	 */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        $this->dataProvider->getPagination()->pageSize = 1000;

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns(){
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'header' => array(
                'name' => 'header',
            ),
            'type' => array(
                'name' => 'type',
                'value' => function($data){
                    $chartGroups = ChartGroup::getChartsGroups();

                    if(isset($chartGroups[$data->type]) && !empty($chartGroups[$data->type])){
                        return $chartGroups[$data->type];
                    }

                    return '';
                }
            ),
            'industryId' => array(
                'name' => 'type',
                'value' => function($data){
                    if(isset($data, $data->industry, $data->industry->header)){

                        return $data->industry->header;
                    }

                    return '';
                }
            ),
        );
        return $columns;
    }
}