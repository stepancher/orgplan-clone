/**
 * Created by SoundGoodizer on 28.03.2015.
 */
function StyledMultiFile(params) {
    return ({
        index: 0,
        existedNames: [],
        addButton: '',
        params: {
            $uploaderBlock: null,
            $fileList: null,
            blockSelector: null,
            listSelector: null,
            deleteAction: null,
            fileData: {},
            removeButton: null,
            outputAsImage: false,
            messages: {
                duplicate: null,
                denied: null,
                max: null
            },
            validations: {
                accept: [],
                max: null
            }
        },
        init: function (params) {
            var me = this;
            me.params = $.extend(true, me.params, params);
            if (me.params.blockSelector != null) {
                me.params.$uploaderBlock = $('.styled-uploader[data-selector=' + me.params.blockSelector + ']');
            }

            if (me.params.listSelector != null) {
                me.params.$fileList = $('.styled-uploader__file-list[data-selector=' + me.params.listSelector + ']');
            }

            me.params.$uploaderBlock.find('.styled-uploader__input-list input[type=file]').val('');

            me.params.$removeButton = $(me.params.removeButton);

            if (me.params.fileData.length) {
                for (var i = 0; i < me.params.fileData.length; i++) {
                    var $removeButton = me.params.$removeButton.clone();
                    var url = me.params.deleteAction
                        ? me.params.deleteAction.strtr({
                        "{id}": me.params.fileData[i].id
                    })
                        : false;
                    me.existedNames.push(me.params.fileData[i].filename);

                    $removeButton.find('span').text(me.params.fileData[i].filename);
                    if (me.params.outputAsImage && typeof me.params.fileData[i].assetPath != 'undefined') {
                        $removeButton.prepend('<img src="' + me.params.fileData[i].assetPath + '" />');
                    }
                    if (url) {
                        $removeButton.find('a').attr('href', url);
                    }
                    $removeButton.attr('data-uploaded', true);
                    $removeButton.attr('data-index', false);

                    me.params.$fileList.append($removeButton);
                }
            }

            return me;
        },
        run: function () {
            var me = this;
            var $inputList = me.params.$uploaderBlock.find('.styled-uploader__input-list');

            me.params.$uploaderBlock.on('click', '.styled-uploader__styled-button', function () {
                me.addButton = $(this);
                $inputList.find('.styled-uploader__main-hidden-input').trigger('click');
            });

            me.params.$fileList.delegate('.styled-uploader__file-item a', 'click', function () {
                var $elm = $(this).parent(), index = $elm.data('index'), filename = $elm.find('span').text();
                var $generalBlock = $elm.closest('.styled-uploader');
                me.addButton = $generalBlock.find('.styled-uploader__styled-button');
                if (me.inArray(filename, me.existedNames)) {
                    me.existedNames.splice(me.existedNames.indexOf(filename), 1);
                }
                if (!$elm.data('uploaded')) {
                    $inputList.find('[data-index=' + index + ']').remove();
                    $elm.remove();
                } else {
                    var url = $(this).attr('href');
                    if (url != '') {
                        var $blocked = $elm.find('.styled-uploader__blocked');
                        if ($blocked.length > 0) {
                            $blocked.show();
                        } else {
                            $blocked = $('<div class="styled-uploader__blocked"></div>');
                            $elm.append($blocked);
                        }
                        $.post(url, function (error) {
                            if (error == 0) {
                                $elm.remove();
                            } else {
                                $blocked.hide();
                                alert('Error');
                            }
                        });
                    }
                }

                var $inputs = $generalBlock.find('.styled-uploader__input-list input');
                if($inputs.length <= 1) {
                    me.addButton.removeClass('d-bg-infod-bg-tooltip').addClass('d-bg-tooltip');
                }

                return false;
            });

            $inputList.delegate('.styled-uploader__main-hidden-input', 'change', function (e) {
                var $input = $(this),
                    $newInput = $input.clone(),
                    $removeButton = me.params.$removeButton.clone(),
                    filename = $input.val().replace(/^.*[\\\/]/, ''),
                    ext = me.getExtension(filename);

                if (me.params.validations.max && me.existedNames.length + 1 > me.params.validations.max) {
                    $input.val('');
                    alert(me.params.messages.max + ': ' + me.params.validations.max);
                    return;
                }

                if (me.existedNames.length && me.inArray(filename, me.existedNames)) {
                    $input.val('');
                    alert(me.params.messages.duplicate + ': ' + filename);
                    return;
                }

                if (me.params.validations.accept.length && !me.inArray(ext, me.params.validations.accept)) {
                    $input.val('');
                    alert(me.params.messages.denied + ': .' + ext);
                    return;
                }

                me.existedNames.push(filename);

                $removeButton.find('span').text(filename);
                $removeButton.attr('data-index', me.index);

                $input.removeClass('styled-uploader__main-hidden-input');
                $input.attr('data-index', me.index);
                $input.attr('id', $input.attr('id') + '_' + me.index);
                $newInput.removeAttr('data-index');

                me.params.$fileList.append($removeButton);
                $inputList.append($newInput);
                me.addButton.removeClass('d-bg-tooltip').addClass('d-bg-info');

                me.index++;
            });

            $(document).delegate('.styled-uploader__file-item span', 'mouseenter', function(){
               $(this).parent().find('a').css('display', 'inline-block');
            });

        },
        inArray: function (searchValue, array) {
            for (var i = 0; i < array.length; i++) {
                if (typeof array[i] != 'undefined' && array[i].toLowerCase() == searchValue.toLowerCase()) {
                    return true;
                }
            }
            return false;
        },
        getExtension: function (filename) {
            var a = filename.split(".");
            if (a.length === 1 || ( a[0] === "" && a.length === 2 )) {
                return null;
            }
            return a.pop();
        }
    }).init(params).run();
}