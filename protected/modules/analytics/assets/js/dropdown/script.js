$(function () {



	/* multi-drop-down-list */
	$('.js-multi-drop-down-list').each(function (i, elm) {
		var $elm = $(elm);

		$elm.find('label.js-drop-item').each(function (k) {
			var even = k % 2 ? 'even' : 'odd';
			var odd = k % 2 ? 'odd' : 'even';
			$(this).removeClass(even).addClass(odd);
		});

		var $button = $elm.find('.js-drop-button');
		var count = 0;

		$elm.find('.js-drop-item input:checked').each(function () {
			count++;
		});
		setLabel($button, count);
	});

	$(document).click(function (e) {
		var showBlock = $(e.target).closest('.js-multi-drop-down-list').get(0);
		$('.js-multi-drop-down-list').each(function (k) {
			if (this != showBlock) {
				$(this).removeClass('active');
			}
		});
	});

	$(document).delegate('.js-multi-drop-down-list .js-drop-button', 'click', function () {
		var $elm = $(this).closest('.js-multi-drop-down-list');
		resizeWidthDropDownList();
		$('.js-multi-drop-down-list').not($elm).each(function (i, elm) {
			var $list = $(elm);
			$list.removeClass('active');
		});

		$elm.toggleClass('active');
	});

	$(document).delegate('.js-multi-drop-down-list .js-drop-box > .js-drop-item input', 'change', function () {
		var $elm = $(this).closest('.js-multi-drop-down-list');
		var count = 0;
		$elm.find('.js-drop-item input:checked').each(function () {
			count++;
		});
		var $button = $elm.find('label.js-drop-button');
		setLabel($button, count);
	});

	/* drop-down-list */
	$('.js-drop-down-list').each(function (k, elm) {
		var $elm = $(elm), $controls = $elm.find('.controls');
		$controls.append($('<div class="js-drop-box drop-box"></div>'));
		$elm.find('option').each(function (i, e) {
			var $option = $(e), selected = $option.attr('selected') == 'selected',
				even = i % 2 ? 'even' : 'odd', odd = i % 2 ? 'odd' : 'even',
				$block = $('<div class="js-drop-item drop-item" data-selected="'
				+ selected + '" data-value="' + $option.val() + '">' + $option.text() + '</div>');

			$block.removeClass(even).addClass(odd);
			$controls.find('.js-drop-box').append($block);
			$block.on('click', function(){$elm.find('select').val($option.val()).change();})
		});

		var $button = $elm.find('.js-drop-button');

		var text = $elm.find('.js-drop-item[data-selected=true]').text();
		setLabel($button, text);

	});

	$(document).click(function (e) {
		var showBlock = $(e.target).closest('.js-drop-down-list').get(0);
		$('.js-drop-down-list').each(function () {
			if (this != showBlock) {
				$(this).removeClass('active');
			}
		});
	});

	$(window).on('resize', resizeWidthDropDownList);

	$(document).delegate('.js-drop-down-list .js-drop-button', 'click', function () {
		var $elm = $(this).closest('.js-drop-down-list');
		resizeWidthDropDownList();
		$('.js-drop-down-list').not($elm).each(function (i, elm) {
			var $list = $(elm);
			$list.removeClass('active');
		});

		$elm.toggleClass('active');
	});

	$(document).delegate('.js-drop-down-list .js-drop-item', 'click', function () {
		var $elm = $(this);
		$elm.closest('.js-drop-box').find('.js-drop-item').not($elm).each(function (i, elm) {
			$(elm).attr('data-selected', false);
		});
		var $controls = $elm.closest('.controls');
		$controls.find('option').each(function (i, e) {
			$(e).removeAttr('selected');
		});
		$controls.find('option[value="' + $elm.data('value') + '"]').attr('selected', 'selected');
		$elm.attr('data-selected', true);

		var $button = $elm.closest('.js-drop-down-list').find('.js-drop-button');
		var text = $controls.find('.js-drop-item[data-selected=true]').text();
		setLabel($button, text);
		$elm.parent().parent().parent().toggleClass('active');
	});

	function setLabel($elm, text) {
		if(text == ''){
			if ($elm.hasClass('d-bg-tooltip')){
				$elm.removeClass('d-bg-tooltip');
				$elm.addClass('d-bg-tab');
			}
		}
		if ($elm.data('label-only-selected')) {
			$elm.text(text);
		} else {
			if ($elm.text().match(/\((.+)\)/)) {
				$elm.text($elm.text().replace(/\((.+)\)/, (text ? '(' + (text) + ')' : '')));
			} else {
				if (+text != 0) {
					if ($elm.hasClass('d-bg-tab')){
						$elm.removeClass('d-bg-tab');
						$elm.addClass('d-bg-tooltip');
					}
					$elm.text($elm.text() + ' (' + text + ')');
				}
			}
		}
	}

	function resizeWidthDropDownList() {
		$('.js-multi-drop-down-list, .js-drop-down-list').each(function () {
			var $elm = $(this);
			var $box = $elm.find('.js-drop-box');
			$box.width($elm.width() - 2);
		});
	}

});