<?php

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @var Analytics $model
     */
    public $model = 'Analytics';

    /**
     * Стандартные actions перенаправляют на 404.
     */
    public function actionIndex()
    {
        throw new CHttpException(404, Yii::t('AnalyticsModule.analytics', 'Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionView($id)
    {
        /** @var AR $model */
        if (($model = AR::model($this->model)->findByPk($id)) === null) {
            throw new CHttpException(404, Yii::t('AnalyticsModule.analytics', 'Unable to resolve the request "{route}".',
                array('{route}' => $_SERVER['REQUEST_URI'])));
        }

        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * @param null $id
     * Создание/редактирование записи
     */
    public function actionSave($id = null)
    {
        if(Yii::app()->user->role != User::ROLE_ADMIN){
           $this->redirect(Yii::app()->createUrl('home'));
        }

        /** @var Analytics $model */
        if (null === $id || ($model = Analytics::model()->findByPk($id)) === null)
            $model = new $this->model();

        $ctrl = $this;
        $records = Analytics::multiSave(
            $_POST,
            array(
                array(
                    'name' => 'model',
                    'class' => get_class($model),
                    'record' => $model,
                ),
                function ($records) use ($ctrl, $model) {
                    /** @var Analytics $analytics */

                    $analytics = $records['model'];

                    $logo = CUploadedFile::getInstances(Analytics::model(), 'image');

                    if (!empty($logo)) {
                        if ($analytics->image) {
                            $path = $analytics->image->getDirPath(true);
                            if ($analytics->image->saveFile($logo[0])) {
                                unset($path);
                            }
                        } else {
                            $objFile = new ObjectFile();
                            $objFile->type = ObjectFile::TYPE_ANALYTICS_IMAGE;
                            $objFile->ext = ObjectFile::EXT_IMAGE;
                            $objFile->setOwnerId($analytics->id);
                            $objFile->saveFile($logo[0]);

                            $analytics->imageId = $objFile->id;
                        }

                        $analytics->save(false);
                    }
                    IRedactor::createItems(get_class($analytics));
                    IRedactor::clearUploadFiles();

                    Yii::app()->user->setFlash('success', Yii::t('AnalyticsModule.analytics', 'The information is saved.'));
                    $ctrl->redirect($ctrl->createUrl('view', array('id' => $records['model']->id, 'langId' => Yii::app()->language)));
                }
            )
        );
        /** Очистка файлов временной директории, если все модели прошли валидацию */
        IRedactor::clearUploadFiles($records);

        $this->render('save', $records);
    }

    public function actionManageChart(){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = Chart::model();
        $this->render('manageChart', array(
            'model' => $model,
        ));
    }

    public function actionManageChartGroup(){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = ChartGroup::model();
        $this->render('manageChartGroup', array(
            'model' => $model,
        ));
    }

    public function actionManageIndustryAnalyticsInformation(){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = IndustryAnalyticsInformation::model();
        $this->render('manageIndustryAnalyticsInformation', array(
            'model' => $model,
        ));
    }

    public function actionSaveChart($id = NULL){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        Yii::import('app.modules.gradoteka.models.*');

        $model = Chart::model()->findByPk($id);

        if($model == NULL){
            $model = new Chart();
        }

        if(isset($_POST['Chart']) && !empty($_POST['Chart'])){
            $model->attributes = $_POST['Chart'];

            if($model->save()){
                Yii::app()->user->setFlash('success', 'Информация сохранена');
                $this->redirect($this->createUrl('saveChart', array('id' => $model->id)));
            }else{
                Yii::app()->user->setFlash('danger', 'Ошибка сохранения');
            }
        }

        $this->render('createChart', array(
            'model' => $model,
        ));
    }

    public function actionSaveChartGroup($id = NULL){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        Yii::import('app.modules.gradoteka.models.*');

        $model = ChartGroup::model()->findByPk($id);

        if($model == NULL){
            $model = new ChartGroup();
        }

        if(isset($_POST['ChartGroup']) && !empty($_POST['ChartGroup'])){
            $model->attributes = $_POST['ChartGroup'];

            if($model->save()){
                Yii::app()->user->setFlash('success', 'Информация сохранена');
                $this->redirect($this->createUrl('createChartGroup', array('id' => $model->id)));
            }else{
                Yii::app()->user->setFlash('danger', 'Ошибка сохранения');
            }
        }

        $this->render('createChartGroup', array(
            'model' => $model,
        ));
    }

    public function actionSaveIndustryAnalyticsInformation($id = NULL){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        Yii::import('app.modules.gradoteka.models.*');

        $model = IndustryAnalyticsInformation::model()->findByPk($id);

        if($model == NULL){
            $model = new IndustryAnalyticsInformation();
        }

        if(isset($_POST['IndustryAnalyticsInformation']) && !empty($_POST['IndustryAnalyticsInformation'])){
            $model->attributes = $_POST['IndustryAnalyticsInformation'];

            if($model->save()){
                Yii::app()->user->setFlash('success', 'Информация сохранена');
                $this->redirect($this->createUrl('createIndustryAnalyticsInformation', array('id' => $model->id)));
            }else{
                Yii::app()->user->setFlash('danger', 'Ошибка сохранения');
            }
        }

        $this->render('createIndustryAnalyticsInformation', array(
            'model' => $model,
        ));
    }


    public function actionLoadWidgetTypes(){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(!isset($_POST['Chart']['widget']) || !is_numeric($_POST['Chart']['widget'])){
            echo '';
            exit;
        }

        Yii::import('ext.ZLineChart.ZLineChart');
        Yii::import('application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart');

        $data = AnalyticsService::getWidgetTypes($_POST['Chart']['widget']);

        foreach($data as $typeId=>$label){
            echo CHtml::tag('option',
                array('value'=>$typeId),CHtml::encode($label),true);
        }
    }

    public function actionDeleteChart($id){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(!empty($id) && is_numeric($id)){
            $model = Chart::model()->findByPk($id);

            if(!empty($model)){
                $model->delete();
            }
        }
    }

    public function actionDeleteChartGroup(){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(!empty($id) && is_numeric($id)){
            $model = ChartGroup::model()->findByPk($id);

            if(!empty($model)){
                $model->delete();
            }
        }
    }

    public function actionDeleteIndustryAnalyticsInformation(){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(!empty($id) && is_numeric($id)){
            $model = IndustryAnalyticsInformation::model()->findByPk($id);

            if(!empty($model)){
                $model->delete();
            }
        }
    }

    public function actionRemoveImage($id){

        if(Yii::app()->user->role != User::ROLE_ADMIN){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        /** @var Analytics $model */
        $model = Analytics::model()->findByAttributes(['imageId' => $id]);
        if (null != $model && isset($model->image) && unlink($model->image->getFilePath(true, DIRECTORY_SEPARATOR)) && $model->image->delete()) {
            $model->imageId = null;
            $model->update(['imageId']);
        }

        $this->redirect(Yii::app()->createUrl('analytics/default/save', ['id' => $model->id]));
    }
}