<?php

class OrganizerController extends Controller
{

    const NULL_COUNT = 0;
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array page variants for gridView
     */
    static $pageVariants = [1, 3, 5, 10, 20, 50, 100, 200, 500, 1000, 5000, 10000];
    const STANDARD_PAGE_SIZE = 10;

    /**
     * @var string temporary language for saving multi language organizerCompany
     */
    protected $tempLang = '';
    protected $firstRow = 0;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array(
                    'create',
                    'update',
                    'view',
                    'getOrganizers',
                    'cancel',
                    'cancelOrganizerWithRedirect',
                    'viewContact',
                    'createContact',
                    'admin',
                    'getContactValue',
                    'organizerAttributesSave',
                    'fairAttach',
                    'getDistricts',
                    'getRegions',
                    'getCities',
                    'getFairs',
                    'getAssociations',
                    'saveContact',
                    'deleteContact',
                    'getCityGeoData',
                    'getCitiesByText',
                ),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * @param $organizerCompanyId
     * @throws CHttpException
     */
    public function actionCreateContact($organizerCompanyId){

        $organizerCompany = OrganizerCompany::model()->findByPk($organizerCompanyId);

        if(empty($organizerCompany)){
            throw new CHttpException(404, 'Organizer company didn\t find');
        }

        $organizer = new Organizer;
        $organizer->companyId = $organizerCompany->id;


        $organizer->save();

        foreach (OrganizerContactList::$types as $type => $typeName){

            $organizerContactList = new OrganizerContactList;
            $organizerContactList->organizerId = $organizer->id;
            $organizerContactList->type = $type;
            $organizerContactList->save();
        }

        $data = OrganizerAdminService::loadModelWithContacts(array($organizer->id), TRUE);
        $data = array_diff_key($data, array_flip(OrganizerContactList::$types));

        $rows = array_keys($data);
        array_walk($rows, function (&$row) use($data){
            $row = array(
                'name' => $row,
                'type' => 'raw',
                'label' => Organizer::model()->getAttributeLabel($row),
            );
        });

        $this->render('createContact', array(
            'data' => $data,
            'rows' => $rows,
            'organizerCompanyId' => $organizerCompany->id,
            'dataProvider' => OrganizerAdminService::contactRedactorData($data['id']),
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     * @throws CHttpException
     */
    public function actionViewContact($id)
    {
        /** @var array $data */
        $data = OrganizerAdminService::loadModelWithContacts(array($id), TRUE);

        if(empty($data)){
            throw new CHttpException(404, 'Not Found');
        }

        $data = array_diff_key($data, array_flip(OrganizerContactList::$types));

        $fairs = Fair::model()->findAllBySql(
            'SELECT f.* FROM tbl_fair f LEFT JOIN tbl_fairhasorganizer fho ON fho.fairId = f.id WHERE fho.organizerId = :id',
            array(
                ':id' => $id
            )
        );

        $fairsDataProvider = new CArrayDataProvider($fairs);

        $rows = array_keys($data);
        array_walk($rows, function (&$row) use($data){
            $row = array(
                'name' => $row,
                'type' => 'raw',
                'label' => Organizer::model()->getAttributeLabel($row),
            );
        });

        $contactTypeCount = OrganizerAdminService::contactTypeCountById($data['id']);

        if($contactTypeCount < count(OrganizerContactList::$types))
            foreach (OrganizerContactList::$types as $key => $type)
                if(empty(OrganizerContactList::model()->findByAttributes(['type' => $key, 'organizerId' => $data['id']]))){
                    $contact = new OrganizerContactList;
                    $contact->organizerId = $data['id'];
                    $contact->type = $key;
                    $contact->save();
                }

        $dataProvider = OrganizerAdminService::contactRedactorData($data['id']);

        $this->render('viewContact', array(
            'data' => $data,
            'fairsDataProvider' => $fairsDataProvider,
            'rows' => $rows,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionCancel($organizerId){

        if($organizerId !== NULL){
            $organizer = Organizer::model()->findByPk($organizerId);

            Organizer::model()->deleteByPk($organizerId);
            OrganizerContactList::model()->deleteAllByAttributes(['organizerId' => $organizerId]);

            if(!empty($organizer) && isset($organizer->companyId))
                $this->redirect(Yii::app()->createUrl('admin/organizer/view', array('id' => $organizer->companyId)));
        }
        $this->redirect($this->createUrl('admin'));
    }

    public function actionCancelOrganizerWithRedirect($organizerId){

        if($organizerId !== NULL){
            $organizer = Organizer::model()->findByPk($organizerId);
            if(!empty($organizer)){
                $organizer->delete();
                $this->redirect($this->createUrl('view', array('id' => $organizer->companyId)));
            }
        }
        $this->redirect($this->createUrl('admin'));
    }

    /**
     * @param   integer $id
     * @throws  CHttpException
     */
    public function actionView($id)
    {
        $model = OrganizerCompany::model()->findByPk($id);

        if($model === null){
            throw new CHttpException(404, 'Not Found');
        }

        $modelSearch = new Organizer('search');
        $modelSearch->unsetAttributes();
        $user = Yii::app()->user;

        if (isset($_GET['Organizer'])) {
            $modelSearch->attributes = $_GET['Organizer'];
        }

        if(isset($_GET['Organizer_fields']) && !empty($_GET['Organizer_fields'])){
            $user->setState('Organizer_fields', $_GET['Organizer_fields']);
        }

        $contactFields = [];

        foreach($modelSearch->description() as $key => $description) {

            if (!isset($description['relation']) && (!isset($description[0]) || $description[0] != 'pk')) {
                $contactFields[$key] = !empty($description['label']) ? $description['label'] : $key;
            }
        }

        if(isset($contactFields['companyId'])){
            unset($contactFields['companyId']);
        }

        if(isset($contactFields['organizerName'])){
            unset($contactFields['organizerName']);
        }

        if(isset($contactFields['linkToTheSiteOrganizers'])){
            unset($contactFields['linkToTheSiteOrganizers']);
        }

        if(isset($contactFields['fair'])){
            unset($contactFields['fair']);
        }

        if(isset($contactFields['fairs'])){
            unset($contactFields['fairs']);
        }

        $contactSelectedFields = Yii::app()->user->getState('Organizer_fields');
        $contactSelectedFields = !empty($contactSelectedFields) ? explode(',', $contactSelectedFields) : array_keys($contactFields);

        $dataProvider = OrganizerAdminService::search($contactSelectedFields, self::STANDARD_PAGE_SIZE, $modelSearch, $model->id);

        $fields = $contactSelectedFields;

        array_walk($fields, function(&$field) use($modelSearch){
            $field = array(
                'name' => $field,
                'type' => 'raw',
                'header' => isset($modelSearch->{$field}) ? $modelSearch->getAttributeLabel($field) : $field,
            );
        });

        $this->render('view', array(
            'model'                 => $model,
            'modelSearch'           => $modelSearch,
            'contactSelectedFields' => $contactSelectedFields,
            'contactFields'         => $contactFields,
            'fields'                => $fields,
            'dataProvider'          => $dataProvider,
        ));
    }

    public function actionCreate()
    {
        $model = new OrganizerCompany;

        if (isset($_POST['OrganizerCompany'])) {

            $model->attributes = $_POST['OrganizerCompany'];

            $transaction = Yii::app()->db->beginTransaction();
            $this->tempLang = Yii::app()->language;

            try{

                $langs = Yii::app()->params['translatedLanguages'];

                foreach ($langs as $lang => $language){

                    if(Yii::app()->language != $lang)
                        Yii::app()->language = $lang;

                    $model->save();
                }

                $transaction->commit();

                if(!empty($this->tempLang)){
                    Yii::app()->language = $this->tempLang;
                }

                $this->redirect(array('view', 'id' => $model->id));

            } catch (CException $e){

                $transaction->rollback();

                if(!empty($this->tempLang)){
                    Yii::app()->language = $this->tempLang;
                }

                if(isset($e->errorInfo[2])){
                    $errorText = $e->errorInfo[2];
                } else {
                    $errorText = $e->getMessage();
                }
                Yii::app()->user->setFlash('danger', "Ошибка при создании организатора. Код: {$errorText}");
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = OrganizerCompany::model()->findByPk($id);

        if($model === null){
            throw new CHttpException(404, 'Not Found');
        }

        if (isset($_POST['OrganizerCompany'])) {
            $model->attributes = $_POST['OrganizerCompany'];

            $transaction = Yii::app()->db->beginTransaction();

            try{

                if(!$model->validate()){
                    throw new CHttpException(404, 'Заполните обязательные поля.');
                }

                $model->save();

                if(isset($_POST[get_class($model)][OrganizerCompany::ASSOCIATION_VARIABLE]) &&
                    empty($_POST[get_class($model)][OrganizerCompany::ASSOCIATION_VARIABLE]))
                {
                    OrganizerCompanyHasAssociation::model()->deleteAllByAttributes(['organizerCompanyId' => $model->id]);
                }
                elseif(isset($_POST[get_class($model)][OrganizerCompany::ASSOCIATION_VARIABLE]) &&
                    !empty($_POST[get_class($model)][OrganizerCompany::ASSOCIATION_VARIABLE]))
                {
                    $organizerCompanyAssociationIDs = explode(',', $_POST[get_class($model)][OrganizerCompany::ASSOCIATION_VARIABLE]);

                    if(is_array($organizerCompanyAssociationIDs) && count($organizerCompanyAssociationIDs) > 0){

                        OrganizerCompanyHasAssociation::model()->deleteAllByAttributes(['organizerCompanyId' => $model->id]);

                        foreach ($organizerCompanyAssociationIDs as $organizerCompanyAssociationID){

                            $organizerCompanyAssociation = Association::model()->findByPk($organizerCompanyAssociationID);

                            if($organizerCompanyAssociation !== NULL){

                                $OCHA = new OrganizerCompanyHasAssociation;
                                $OCHA->attributes = [
                                    'organizerCompanyId' => $model->id,
                                    'associationId'  => $organizerCompanyAssociationID,
                                ];

                                $OCHA->save();
                            }
                        }
                    }
                }

                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));

            } catch (CException $e){

                $transaction->rollback();

                if(!empty($this->tempLang)){
                    Yii::app()->language = $this->tempLang;
                }

                if(isset($e->errorInfo[2])){
                    $errorText = $e->errorInfo[2];
                } else {
                    $errorText = $e->getMessage();
                }
                Yii::app()->user->setFlash('danger', "Ошибка при редактировании организатора. Код: {$errorText}");
            }

        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Organizer');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin(){

        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        /** @var WebUser $user */
        $user = Yii::app()->user;
        $pageVariants = array_combine(self::$pageVariants, self::$pageVariants);

        if(isset($_GET['OrganizerCompany_pageSize']) && !is_array($_GET['OrganizerCompany_pageSize']) && !empty($_GET['OrganizerCompany_pageSize'])){
            $user->setState('OrganizerCompany_pageSize', (int)$_GET['OrganizerCompany_pageSize'], reset($pageVariants));
        }

        if(isset($_GET['OrganizerCompany_fields']) && !empty($_GET['OrganizerCompany_fields'])){
            $user->setState('OrganizerCompany_fields', $_GET['OrganizerCompany_fields']);
        }

        $model = new OrganizerCompany('search');

        if (isset($_GET['OrganizerCompany'])) {
            $model->attributes = $_GET['OrganizerCompany'];
        }

        $attributes = array(
            'id',
            'active',
            'name',
            'statusName',
            'linkToTheSiteOrganizers',
            'email',
            'phone',
            'cityName',
            'regionName',
            'districtName',
            'countryName',
            'street',
            'coordinates',
            'exdbId',
            'exdbContacts',
            'associationName',
            'fairsCount',
        );
        $pageSize = Yii::app()->user->getState('OrganizerCompany_pageSize', reset($pageVariants));

        $fields = array();
        foreach ($attributes as $field){
            $fields[$field] = $model->getAttributeLabel($field);
        }

        $selectedFields = Yii::app()->user->getState('OrganizerCompany_fields');
        $selectedFields = !empty($selectedFields) ? explode(',', $selectedFields) : array_values(array_flip($fields));

        $dataProvider = OrganizerCompanyAdminService::search($selectedFields, $pageSize, $model);

        $columns = array();
        foreach ($selectedFields as $field)
        {
            $columns[$field] = array(
                'name' => $field,
                'type' => 'raw',
                'header' => !empty($model->getAttributeLabel($field)) ? $model->getAttributeLabel($field) : $field,
            );

            if($field == 'countryName'){
                $columns[$field] = [
                    'name' => $field,
                    'type' => 'raw',
                    'header' => Yii::t('AdminModule.admin', 'Country'),
                    'filter' => TbHtml::activeDropDownList(
                        $model,
                        'countryName',
                        Country::countries(),
                        array(
                            'empty' => ''
                        )
                    ),
                ];
            } elseif ($field == 'statusName'){
                $columns[$field] = [
                    'name' => $field,
                    'type' => 'raw',
                    'header' => Yii::t('AdminModule.admin', 'Status'),
                    'filter' => TbHtml::activeDropDownList(
                        $model,
                        'statusName',
                        OrganizerCompanyStatus::statuses(),
                        array(
                            'empty' => ''
                        )
                    ),
                ];
            }
        }

        $this->render('admin', array(
            'pageVariants'   => $pageVariants,
            'pageSize'       => $pageSize,
            'fields'         => $fields,
            'selectedFields' => $selectedFields,
            'model'          => $model,
            'dataProvider'   => $dataProvider,
            'columns'        => $columns,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Organizer the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Organizer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * @param $q
     */
    public function actionGetOrganizers($q){

        $sql = "SELECT org.id, trorg.name FROM tbl_organizercompany org
                LEFT JOIN tbl_trorganizercompany trorg ON trorg.trParentId = org.id AND trorg.langId = :langId
                WHERE (trorg.name LIKE :q OR org.id LIKE :q)
                ";

        $data = Yii::app()->db->createCommand($sql)->query([
            ':q' => "%$q%",
            ':langId' => Yii::app()->language,
        ]);

        echo json_encode($data->readAll());
    }

    /**
     * Performs the AJAX validation.
     * @param Organizer $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'organizer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetContactValue(){

        if(!isset($_POST['organizerId']) || !isset($_POST['value']) || !isset($_POST['type'])){
            echo json_encode(array('success' => FALSE,'message' => 'post didn\'t find'));
            return;
        }

        $type = $_POST['type'];
        $organizerId = $_POST['organizerId'];
        $organizer = Organizer::model()->findByPk($organizerId);

        if(empty($organizer)){echo json_encode(array('success' => FALSE,'message' => 'organizer didn\'t find'));
            return;
        }
        $value = $_POST['value'];

        $notInValuesQuery = 'SELECT DISTINCT value FROM tbl_organizercontactlist clist
                             WHERE clist.organizerId = :organizerId AND clist.type = :type
        ';
        $notInValues = Yii::app()->db->createCommand($notInValuesQuery)->queryAll(TRUE, array(':organizerId' => $organizerId, ':type' => $type));

        $notInValues = array_map(
            function ($data){
                return $data['value'];
            },
            $notInValues
        );

        $criteria = new CDbCriteria;
        $criteria->with['organizer'] = array(
            'together' => TRUE,
            'with' => array(
                'company' => array(
                    'together' => TRUE,
                ),
            ),
        );
        $criteria->addCondition(
            array(
                "company.id = :companyId",
                "t.value LIKE :val"
            )
        );

        if(!empty($notInValues))
            $criteria->addNotInCondition('t.value', $notInValues);

        $criteria->params += array(
            ':val' => "%$value%",
            ':companyId' => $organizer->companyId,
        );

        $contacts = OrganizerContactList::model()->findAll($criteria);

        $data = array_map(
            function($val){
                return $val->value;
            },
            $contacts
        );
        echo json_encode(array_unique($data));
    }

    public function actionOrganizerAttributesSave(){

        if(!isset($_POST['organizerId']) || !isset($_POST['type']) || !isset($_POST['value']))
        {
            echo json_encode(array('success' => FALSE,'message' => 'post didn\'t find'));
            return;
        }

        $organizerId = $_POST['organizerId'];
        $type = $_POST['type'];
        $value = $_POST['value'];

        $organizer = Organizer::model()->findByPk($organizerId);

        if(empty($organizer)){
            echo json_encode(array('success' => FALSE,'message' => 'organizer didn\'t find'));
            return;
        }

        $transaction = Yii::app()->db->beginTransaction();
        $organizer->{$type} = $value;

        try {
            $organizer->save();
            $transaction->commit();
            echo json_encode(array('success' => TRUE,'message' => 'organizer save', 'newValue' => $value));
        } catch (CException $e){

            $transaction->rollback();

            if(isset($e->errorInfo[2])){
                $errorText = $e->errorInfo[2];
            } else {
                $errorText = $e->getMessage();
            }
            echo json_encode(array('success' => FALSE,'message' => $errorText));
        }
    }

    public function actionGetDistricts(){

        $criteria = new CDbCriteria;
        $criteria->addCondition("t.countryId = :countryId");
        $criteria->params[':countryId'] = $_POST['countryId'];

        $data = District::model()->findAll($criteria);

        if(!empty($data)) {

            $districtsList = [0 => Yii::t('AdminModule.admin','choose a district')];
            $districtsList = $districtsList + CHtml::listData($data,'id','name');

            foreach ($districtsList as $id=>$name) {
                echo CHtml::tag('option', array('value'=>$id), CHtml::encode($name), TRUE);
            }

        } else echo CHtml::tag('option', array(),Yii::t('AdminModule.admin','the district is not found'));
    }

    public function actionGetRegions(){

        $criteria = new CDbCriteria;
        $criteria->addCondition("t.districtId = :districtId");
        $criteria->params[':districtId'] = $_POST['districtId'];

        $data = Region::model()->findAll($criteria);

        if(!empty($data)) {

            $regionsList = [0 => Yii::t('AdminModule.admin','choose a region')];
            $regionsList = $regionsList + CHtml::listData($data,'id','name');

            foreach ($regionsList as $id=>$name) {
                echo CHtml::tag('option', array('value'=>$id), CHtml::encode($name), TRUE);
            }

        } else echo CHtml::tag('option', array(),Yii::t('AdminModule.admin','the region is not found'));
    }

    public function actionGetCities(){

        $criteria = new CDbCriteria;
        $criteria->addCondition("t.regionId = :regionId");
        $criteria->params[':regionId'] = $_POST['regionId'];

        $data = City::model()->findAll($criteria);

        if(!empty($data)) {

            $citiesList = [0 => Yii::t('AdminModule.admin','choose a city')];
            $citiesList = $citiesList + CHtml::listData($data,'id','name');

            foreach ($citiesList as $id=>$name) {
                echo CHtml::tag('option', array('value'=>$id), CHtml::encode($name), TRUE);
            }

        } else {
            echo CHtml::tag('option', array(), Yii::t('AdminModule.admin','the city is not found'));
        }

    }

    public function actionGetFairs($q){

        $sql = "SELECT f.id, trf.name AS fairName, YEAR(f.beginDate) as beginYear, trc.name AS cityName FROM tbl_fair f
                    LEFT JOIN tbl_trfair trf ON trf.trParentId = f.id AND trf.langId = :langId
                    LEFT JOIN tbl_exhibitioncomplex ec ON f.exhibitionComplexId = ec.id
                    LEFT JOIN tbl_city c ON ec.cityId = c.id
                    LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id AND trc.langId = :langId
                WHERE f.id LIKE :q OR trf.name LIKE :q
                ORDER BY f.id
        ";

        $data = Yii::app()->db->createCommand($sql)->query([
            ':q' => "%$q%",
            ':langId' => Yii::app()->language,
        ]);

        echo json_encode($data->readAll());
    }

    public function actionFairAttach(){

        if(!isset($_POST['organizerId']) || !isset($_POST['fairId']))
        {
            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => 'post didn\'t find',
                )
            );
            return;
        }

        $organizerId = $_POST['organizerId'];
        $fairId = $_POST['fairId'];

        $fair = Fair::model()->findByPk($fairId);

        if(empty($fairId)){
            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => 'fair didn\'t find',
                )
            );
            return;
        }

        $fairHasOrganizer = FairHasOrganizer::model()->findAllByAttributes(['fairId' => $fairId, 'organizerId' => $organizerId]);

        if(!empty($fairHasOrganizer)){
            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => 'organizer contact already saved for this fair',
                )
            );
            return;
        }

        $fairHasOrganizer = new FairHasOrganizer;
        $fairHasOrganizer->fairId = $fairId;
        $fairHasOrganizer->organizerId = $organizerId;

        $transaction = Yii::app()->db->beginTransaction();

        try{

            $fairHasOrganizer->save();
            $transaction->commit();

            echo json_encode(
                array(
                    'success' => 'ok',
                    'message' => 'fair save',
                )
            );
        } catch (CException $e){

            $transaction->rollback();

            if(isset($e->errorInfo[2])){
                $errorText = $e->errorInfo[2];
            } else {
                $errorText = $e->getMessage();
            }

            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => $errorText,
                )
            );
        }
    }

    /**
     * @param $q
     */
    public function actionGetAssociations($q){

        $criteria = new CDbCriteria;
        $criteria->compare('t.name', $q, true,'OR');
        $criteria->compare('t.id', $q, true,'OR');

        $association = CHtml::listData(
            Association::model()->findAll($criteria),
            'id',
            'name');

        $items = array_map(
            function($association_id, $association_name){
                return array(
                    'id' => $association_id,
                    'name' => $association_name
                );
            },
            array_keys($association),
            array_values($association)
        );

        echo json_encode($items);
    }
    
    public function actionSaveContact(){

        if(empty($_POST)){
            echo json_encode(array('success' => FALSE, 'message' => Yii::t('AdminModule.admin', 'Post didn\'t find')));
            return;
        }

        if(isset($_POST['contactId'])){
            $contactId = $_POST['contactId'];
        } else {
            $contactId = '';
        }

        if(isset($_POST['organizerId'])){
            $organizerId = $_POST['organizerId'];
        } else {
            $organizerId = '';
        }

        if(isset($_POST['type'])){
            $type = $_POST['type'];
        } else {
            $type = '';
        }

        if(isset($_POST['value'])){
            $value = $_POST['value'];
        } else {
            $value = '';
        }

        if(!empty($_POST['countryName'])){
            $trCountry = TrCountry::model()->findByAttributes(['name' => $_POST['countryName']]);

            if(!empty($trCountry)){
                $countryId = $trCountry->trParentId;
                $country = Country::model()->findByPk($countryId);
            } else {
                $countryId = '';
            }
        } else {
            $countryId = '';
        }

        if(isset($_POST['contactAdditionalValue'])){
            $contactAdditionalValue = $_POST['contactAdditionalValue'];
        } else {
            $contactAdditionalValue = '';
        }

        $contact = OrganizerContactList::model()->findByPk($contactId);

        if(empty($contact))
            $contact = new OrganizerContactList;

        $contact->organizerId = $organizerId;
        $contact->value = $value;
        $contact->type = $type;

        if(!empty($countryId))
            $contact->countryId = $countryId;

        $contact->additionalContactInformation = $contactAdditionalValue;

        if(!empty($contact->id)){
            $checkQuery = "SELECT COUNT(*) FROM tbl_organizercontactlist ocl 
                        WHERE `id` != :id 
                        AND ocl.organizerId = :organizerId
                        AND ocl.type = :type
                        AND ocl.value = :value
                        AND ocl.additionalContactInformation = :additionalContactInformation
            ";
            $params = array(
                ':id' => $contact->id,
                ':organizerId' => $contact->organizerId,
                ':type' => $contact->type = $type,
                ':value' => $contact->value,
                ':additionalContactInformation' => $contact->additionalContactInformation,
            );
        } else {
            $checkQuery = "SELECT COUNT(*) FROM tbl_organizercontactlist ocl 
                        WHERE ocl.organizerId = :organizerId
                        AND ocl.type = :type
                        AND ocl.value = :value
                        AND ocl.additionalContactInformation = :additionalContactInformation
            ";
            $params = array(
                ':organizerId' => $contact->organizerId,
                ':type' => $contact->type = $type,
                ':value' => $contact->value,
                ':additionalContactInformation' => $contact->additionalContactInformation,
            );
        }

        $checkCount = OrganizerContactList::model()->countBySql($checkQuery, $params);

        if($checkCount > self::NULL_COUNT){
            echo json_encode(array('success' => FALSE, 'message' => Yii::t('AdminModule.admin', 'Contact already exists')));
            return;
        }

        if($contact->type == OrganizerContactList::PHONE_TYPE && empty($contact->countryId)){
            echo json_encode(array('success' => FALSE, 'message' => Yii::t('AdminModule.admin', 'Please, choose the country')));
            return;
        }

        if($contact->type == OrganizerContactList::PHONE_TYPE && !empty($contact->value) && md5(intval($contact->value)) != md5($contact->value)){
            echo json_encode(array('success' => FALSE, 'message' => Yii::t('AdminModule.admin', 'Number only type field')));
            return;
        }
        
        if($contact->type == OrganizerContactList::EMAIL_TYPE && !empty($contact->countryId)){
            echo json_encode(array('success' => FALSE, 'message' => Yii::t('AdminModule.admin', 'Country can\'t be choose for this contact type')));
            return;
        }
        
        if($contact->type == OrganizerContactList::EMAIL_TYPE && !empty($contact->value) && !self::is_email($contact->value)){
            echo json_encode(array('success' => FALSE, 'message' => Yii::t('AdminModule.admin', 'Only email can be save for this contact type')));
            return;
        }

        $transaction = Yii::app()->db->beginTransaction();

        try{
            $contact->save();
            $transaction->commit();

            echo json_encode(
                array(
                    'success' => TRUE,
                    'message' => Yii::t('AdminModule.admin', 'Contact save'),
                    'countryTelephoneCode' => isset($country) ? $country->telephoneCode : '',
                    'contactId' => $contact->id,
                )
            );

        } catch (CException $e){

            $transaction->rollback();

            if(isset($e->errorInfo[2])){
                $errorText = $e->errorInfo[2];
            } else {
                $errorText = $e->getMessage();
            }

            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => $errorText,
                )
            );
        }
    }
    
    public function actionDeleteContact(){
        
        if(!isset($_POST['contactId'])){
            echo json_encode(array('success' => FALSE,'message' => 'post didn\'t find'));
            return;
        }

        $contactId = $_POST['contactId'];

        $contact = OrganizerContactList::model()->findByPk($contactId);

        if(empty($contact)){
            echo json_encode(array('success' => FALSE,'message' => 'organizer contact didn\'t find'));
            return;
        }

        $transaction = Yii::app()->db->beginTransaction();

        try {
            $contact->delete();
            $transaction->commit();
            echo json_encode(array('success' => TRUE,'message' => 'organizer contact delete'));
        } catch (CException $e){

            $transaction->rollback();

            if(isset($e->errorInfo[2])){
                $errorText = $e->errorInfo[2];
            } else {
                $errorText = $e->getMessage();
            }
            echo json_encode(array('success' => FALSE,'message' => $errorText));
        }
    }

    /**
     * @param $email
     * @return bool
     */
    public static function is_email($email){

        if(empty($email))
            return FALSE;

        if(strpos($email, '@') !== FALSE)
            return TRUE;

        $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
        $domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+';
        $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
        $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';

        return preg_match(<<<TAG
/^$user@($domain|(\[($ipv4|$ipv6)\]))$/
TAG
, $email);
    }

    public function actionGetCityGeoData(){

        if(!isset($_POST['cityId']))
        {
            echo json_encode(array('success' => FALSE,'message' => 'post didn\'t find'));
            return;
        }

        $cityId = $_POST['cityId'];
        $city = City::model()->findByPk($cityId);

        if(empty($city)){
            echo json_encode(array('success' => FALSE,'message' => 'city didn\'t find'));
            return;
        }

        echo json_encode(
            array(
                'success' => TRUE,
                'regionName' => isset($city->region->name) ? $city->region->name : '',
                'districtName' => isset($city->region->district->name) ? $city->region->district->name : '',
                'countryName' => isset($city->region->district->country->name) ? $city->region->district->country->name : '',
            )
        );
    }

    public function actionGetCitiesByText($q){
        $sql = "SELECT c.id, trc.name FROM tbl_city c
				LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id AND trc.langId = :langId
                WHERE trc.name LIKE :q
                OR c.id LIKE :q
        ";

        $data = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':q' => "%$q%",
            ':langId' => Yii::app()->language,
        ]);

        echo json_encode($data);
    }
}
