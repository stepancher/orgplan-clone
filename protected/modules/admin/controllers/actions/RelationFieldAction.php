<?php

/**
 * Created by PhpStorm.
 * User: HolyDemon
 * Date: 20.04.2015
 * Time: 15:38
 */
class RelationFieldAction extends CAction
{
    public $model;
    public $filterKey;

    public $fieldValue;
    public $fieldLabel;

    public $criteria = [];

    public function run($id)
    {
        $model = call_user_func(array($this->model, 'model'));

        $criteria = new CDbCriteria($this->criteria);
        $criteria->compare($this->filterKey, $id);
        $records = $model->findAll($criteria);

        $result = [];
        foreach ($records as $rec) {
            $result[] = [
                'value' => $rec->{$this->fieldValue},
                'label' => $rec->{$this->fieldLabel}
            ];
        }
        echo json_encode($result);
    }
}