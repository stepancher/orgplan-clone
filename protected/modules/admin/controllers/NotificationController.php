<?php

class NotificationController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'roles' => array(User::ROLE_ADMIN),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $templatePath = Yii::app()->getRuntimePath() . '/_mail.tpl';
        if (!is_file($templatePath)) {
            file_put_contents(
                $templatePath,
                file_get_contents(dirname(dirname(__DIR__)) . '/data/_mail.php')
            );
        }

        $content = Yii::app()->request->getParam('content');
        if ($content) {
            file_put_contents($templatePath, $content);
        };

        $result = null;
        $user = User::model()->findByPk(Yii::app()->request->getParam('userId', Yii::app()->user->id));
        if ($user && $user->userSetting) {
            $result = $this->renderFile($templatePath, [
                'user' => $user,
                'tasks' => $user->userSetting->getUserTasksByDate(
                    date('Y-m-d', strtotime(Yii::app()->request->getParam('date', date('Y-m-D')))) . ' 00:00:00'
                ),
                'message' => null
            ], true);
        }

        $this->render('index', [
            'content' => file_get_contents($templatePath),
            'result' => $result
        ]);
    }

}
