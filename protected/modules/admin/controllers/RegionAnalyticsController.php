<?php

class RegionAnalyticsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @return array action filters
     */
    public function filters(){
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionManageChart(){
        $model = Chart::model();
        $this->render('manageChart', array(
            'model' => $model,
        ));
    }

    public function actionManageChartGroup(){
        $model = ChartGroup::model();
        $this->render('manageChartGroup', array(
            'model' => $model,
        ));
    }

    public function actionManageIndustryAnalyticsInformation(){
        $model = IndustryAnalyticsInformation::model();
        $this->render('manageIndustryAnalyticsInformation', array(
            'model' => $model,
        ));
    }

    public function actionSaveChart($id = NULL){

        Yii::import('app.modules.gradoteka.models.*');

        $model = Chart::model()->findByPk($id);

        if($model == NULL){
            $model = new Chart();
        }

        if(isset($_POST['Chart']) && !empty($_POST['Chart'])){
            $model->attributes = $_POST['Chart'];
            if($model->save()){
                Yii::app()->user->setFlash('success', 'Информация сохранена');
                $this->redirect($this->createUrl('manageChart', array('id' => $model->id)));
            }else{
                Yii::app()->user->setFlash('danger', 'Ошибка сохранения');
            }
        }

        $this->render('createChart', array(
            'model' => $model,
        ));
    }

    public function actionSaveChartGroup($id = NULL){

        Yii::import('app.modules.gradoteka.models.*');

        $model = ChartGroup::model()->findByPk($id);

        if($model == NULL){
            $model = new ChartGroup();
        }

        if(isset($_POST['ChartGroup']) && !empty($_POST['ChartGroup'])){
            $model->attributes = $_POST['ChartGroup'];

            if($model->save()){
                Yii::app()->user->setFlash('success', 'Информация сохранена');
                $this->redirect($this->createUrl('manageChartGroup', array('id' => $model->id)));
            }else{
                Yii::app()->user->setFlash('danger', 'Ошибка сохранения');
            }
        }

        $this->render('createChartGroup', array(
            'model' => $model,
        ));
    }

    public function actionSaveIndustryAnalyticsInformation($id = NULL){

        Yii::import('app.modules.gradoteka.models.*');

        $model = IndustryAnalyticsInformation::model()->findByPk($id);

        if($model == NULL){
            $model = new IndustryAnalyticsInformation();
        }

        if(isset($_POST['IndustryAnalyticsInformation']) && !empty($_POST['IndustryAnalyticsInformation'])){
            $model->attributes = $_POST['IndustryAnalyticsInformation'];

            if($model->save()){
                Yii::app()->user->setFlash('success', 'Информация сохранена');
                $this->redirect($this->createUrl('manageIndustryAnalyticsInformation'));
            }else{
                Yii::app()->user->setFlash('danger', 'Ошибка сохранения');
            }
        }

        $this->render('createIndustryAnalyticsInformation', array(
            'model' => $model,
        ));
    }


    public function actionLoadWidgetTypes(){

        if(!isset($_POST['Chart']['widget']) || !is_numeric($_POST['Chart']['widget'])){
            echo '';
            exit;
        }

        Yii::import('ext.ZLineChart.ZLineChart');
        Yii::import('application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart');

        $data = AnalyticsService::getWidgetTypes($_POST['Chart']['widget']);

        foreach($data as $typeId=>$label){
            echo CHtml::tag('option',
                array('value'=>$typeId),CHtml::encode($label),true);
        }
    }

    public function actionDeleteChart($id){

        if(!empty($id) && is_numeric($id)){
            $model = Chart::model()->findByPk($id);

            if(!empty($model)){
                $model->delete();
            }
        }
    }

    public function actionDeleteChartGroup($id){

        if(!empty($id) && is_numeric($id)){
            $model = ChartGroup::model()->findByPk($id);

            if(!empty($model)){
                $model->delete();
            }
        }
    }

    public function actionDeleteIndustryAnalyticsInformation($id){

        if(!empty($id) && is_numeric($id)){
            $model = IndustryAnalyticsInformation::model()->findByPk($id);

            if(!empty($model)){
                $model->delete();
            }
        }
    }
}