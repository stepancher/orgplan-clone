<?php

class IndustryController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';
    public $model = 'Industry';

    /**
     * @return array action filters
     */
    public function filters(){
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){

        $model = new Industry('search');
        $model->unsetAttributes();
        if (isset($_GET[get_class($model)]))
            $model->attributes = $_GET[get_class($model)];
        
        $industryFields = array('name', 'synonyms');

        $sortColumn = NULL;

        if(isset($_GET['Industry_sort']) && !empty($_GET['Industry_sort']))
            $sortColumn = $_GET['Industry_sort'];

        $search = $model->search($industryFields, $sortColumn);

        $this->render('index', array(
            'industryFields'   => $industryFields,
            'search'       => $search,
            'model'        => $model
        ));
    }

    public function actionSave($id = null)
    {
        $model = new Industry;

        if($id !== NULL){
            $model = $this->loadModel($id);
        }

        if (isset($_POST[get_class($model)])) {
            $model->attributes = $_POST[get_class($model)];

            if ($model->save()){

                $deleteCriteria = new CDbCriteria();
                $deleteCriteria->addCondition('industryId = :industryId');
                $deleteCriteria->params[':industryId'] = $model->id;
                $deleteCriteria->addNotInCondition('massMediaId', $model->massMedias);
                IndustryHasMassMedia::model()->deleteAll($deleteCriteria);

                $massMedias = $model->massMedias;
                if (isset($massMedias) && !empty($massMedias)){
                    $industryHasMassMedia = $model->massMedias;

                    foreach($industryHasMassMedia as $massMedia){

                        $criteria = new CDbCriteria();
                        $criteria->addCondition('industryId = :industryId and massMediaId = :massMediaId');
                        $criteria->params = array(
                            ':industryId' => $model->id,
                            ':massMediaId' => $massMedia,
                        );
                        if(!IndustryHasMassMedia::model()->exists($criteria)){

                            $rel = new IndustryHasMassMedia();
                            $rel->industryId = $model->id;
                            $rel->massMediaId = $massMedia;
                            $rel->save();
                        }
                    }
                }
                Yii::app()->user->setFlash('success', Yii::t('AdminModule.admin', 'The information is saved.'));

                $this->redirect($this->createUrl('view', array('id' => $model->id)));
                
            }
        }

        $this->grid($model);
    }

    /**
     * @param $model
     * @throws Exception
     */
    public function grid($model){

        $formClass = get_class($model) . 'Form';

        $grid = $this->widget('FieldSetView', array(
            'header' => Yii::t($this->getId(), $model->isNewRecord ? 'Form header create' : 'Form header update'),
            'items' => array(
                array(
                    'ext.widgets.FormView',
                    'form' => new $formClass(
                        array(
                            'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                            //'elementGroupName' => '[]'
                        ),
                        $model, $this
                    ),
                    'items' => array(
                        array(
                            'ActionsView',
                            'items' => array(
                                'submit' => array(
                                    'type' => TbHtml::BUTTON_TYPE_SUBMIT,
                                    'label' => Yii::t($this->getId(), 'Form action save'),
                                    'attributes' => array(
                                        'color' => TbHtml::BUTTON_COLOR_SUCCESS
                                    )
                                ),
                                'cancel' => array(
                                    'type' => TbHtml::BUTTON_TYPE_LINK,
                                    'label' => Yii::t($this->getId(), 'Form action cancel'),
                                    'attributes' => array(
                                        'url' => $this->createUrl('index'),
                                        'color' => TbHtml::BUTTON_COLOR_DANGER
                                    )
                                )
                            )
                        )
                    )

                ),
            )
        ), TRUE);

        $this->renderText($grid);
    }

    public function actionView($id)
    {
        /** @var AR $model */
        if (($model = AR::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        $className = get_called_class();
        $className = str_replace('Controller', '', $className);
        /** @var AR $model */
        if (($model = AR::model($className)->findByPk($id)) !== null) {
            if ($model->delete()) {
                $this->redirect(Yii::app()->createUrl(lcfirst($className) . '/index'));
            }
        }
    }

    public function actionManageSponsorPosition($id){
        if(isset($id) && !empty($id) && is_numeric($id)){
            $model = Industry::model()->findByPk($id);

            $sponsorPos = null;
            if(isset($model->sponsorPos) && !empty($model->sponsorPos)){
                $sponsorPos = $model->sponsorPos;
            }

            if(!empty($model)){
                $this->render('manageSponsorPosition', array(
                    'model' => $model,
                    'sponsorPos' => $sponsorPos,
                ));
            }
        }
    }

    public function actionSaveSponsorPosition(){
        if(Yii::app()->request->isAjaxRequest){
            if(isset($_POST['model_id']) && !empty($_POST['model_id'])){
                $id = $_POST['model_id'];
                if(isset($_POST['data']) && !empty($_POST['data'])){
                    $sponsorPos = $_POST['data'];
                    $model = Industry::model()->findByPk($id);
                    if(!empty($model)){
                        $model->sponsorPos = $sponsorPos;

                        if($model->save()){
                            echo json_encode(array('success' => '1'));
                        }
                    }
                }else{
                    echo json_encode('empty data array');
                }
            }else{
                echo json_encode('empty model id');
            }
        }
    }

    public function actionToggleGeneral(){
        if(Yii::app()->request->isAjaxRequest){
            if(isset($_POST['model_id']) && !empty($_POST['model_id'])){
                $id = $_POST['model_id'];
                if(isset($_POST['isGeneral']) && !empty($_POST['isGeneral'])){
                    $isGeneral = $_POST['isGeneral'];
                    $model = IndustryHasMassMedia::model()->findByPk($id);
                    if(!empty($model)){
                        $model->isGeneral = $isGeneral;
                        if($model->save()){
                            echo json_encode(array('success' => '1'));
                        }

                    }
                }else{
                    echo json_encode('empty data array');
                }
            }else{
                echo json_encode('empty model id');
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ExhibitionComplex the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Industry::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionManageAnalytics($id){
        $industryModel = Industry::model()->findByPk($id);

        if ($industryModel === null){
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model = new ChartTable('search');
        $model->industryId = $industryModel->id;

        $this->render('manageAnalyticsTable', array(
            'model' => $model
        ));
    }
}
