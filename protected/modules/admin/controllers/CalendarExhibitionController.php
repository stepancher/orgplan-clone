<?php

/**
 * Created by PhpStorm.
 * User: HolyDemon
 * Date: 08.09.2015
 * Time: 11:33
 */
class CalendarExhibitionController extends Controller
{
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'save', 'delete'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'actions'=>array('index','view', 'save', 'delete'),
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Список
     */
    public function actionIndex()
    {
        /** @var CalendarExhibition $model */
        $model = new CalendarExhibition();
        $model->unsetAttributes();
        if (isset($_GET['CalendarExhibition'])) {
            $model->attributes = $_GET['CalendarExhibition'];
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Создание/редактирование
     * @param null $id
     */
    public function actionSave($id = null)
    {
        if ($id) {
            $model = CalendarExhibition::model()->findByPk($id);
            if (null === $model) {
                $this->redirect(Yii::app()->createUrl('admin/calendarExhibition/index'));
            }
        } else {
            $model = new CalendarExhibition();
        }

        if(isset($_POST['CalendarExhibition'])) {
            $model->attributes = $_POST['CalendarExhibition'];
            if($model->save()) {
                $this->redirect(Yii::app()->createUrl('admin/calendarExhibition/index'));
            }
        }

        $this->render('save', ['model' => $model]);
    }

    /**
     * Просмотр
     * @param $id
     */
    public function actionView($id)
    {
        $model = CalendarExhibition::model()->findByPk($id);
        if(null === $model) {
            $this->redirect('admin/calendarExhibition/index');
        }
        $this->render('view', ['model' => $model]);
    }

    /**
     * Удаление
     * @param $id
     * @throws CDbException
     */
    public function actionDelete($id)
    {
        $model = CalendarExhibition::model()->findByPk($id);
        if(null != $model) {
            $model->delete();
        }

        $this->redirect(Yii::app()->createUrl('admin/calendarExhibition/index'));
    }
}