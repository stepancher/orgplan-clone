<?php

/**
 * Class SeoController
 */
class SeoController extends Controller
{
    /**
     * @var Seo $model
     */
    public $model = 'Seo';

    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {

            $this->grid(Seo::model($this->model));
        } else {

            $this->redirect($this->createUrl('home'));
        }
    }

    /**
     * @param $model
     * @throws Exception
     */
    public function grid($model){

        $ctrl = $this;
        $rend = $ctrl->widget('FieldSetView', array(
            'header' => Yii::t($this->getId(), 'Grid header'),
            'items' => array(
                array(
                    'ext.widgets.ActionsView',
                    'items' => array(
                        'add' => array(
                            TbHtml::BUTTON_TYPE_LINK,
                            Yii::t($this->getId(), 'Grid action add'),
                            array(
                                'url' => $this->createUrl('save'),
                                'color' => TbHtml::BUTTON_COLOR_SUCCESS
                            )
                        ),
                    )
                ),
                array(
                    get_class($model) . 'GridView',
                    'model' => $model,
                    'columnsAppend' => array(
                        'buttons' => array(
                            'class' => 'bootstrap.widgets.TbButtonColumn',
                            'template' => '<nobr>{show}&#160;{edit}</nobr>',
                            'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                            'buttons' => array(
                                'show' => array(
                                    'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
                                    'url' => function ($data) use ($ctrl) {
                                        return $ctrl->createUrl('view', array("id" => $data->id));
                                    },
                                    'options' => array('title' => 'Информация', 'class' => 'btn btn-small btn-success'),
                                ),
                                'edit' => array(
                                    'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                                    'url' => function ($data) use ($ctrl) {
                                        return $ctrl->createUrl('save', array("id" => $data->id));
                                    },
                                    'options' => array('title' => 'Редактирование', 'class' => 'btn btn-small btn-primary'),
                                ),
                            )
                        )
                    )
                ),
            )
        ), TRUE);
        $this->renderText($rend);
    }

    public function actionView($id)
    {
        if (($model = AR::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }

        $this->gridDetail($model);
    }

    /**
     * @param $model
     * @throws Exception
     */
    public function gridDetail($model){

        $grid = $this->widget(
            'FieldSetView',
            array(
                'header' => Yii::t($this->getId(), 'Detail header'),
                'items' => array(
                    array(
                        'ActionsView',
                        'items' => array(
                            'cancel' => array(
                                'type' => TbHtml::BUTTON_TYPE_LINK,
                                'label' => Yii::t($this->getId(), 'Detail action cancel'),
                                'attributes' => array(
                                    'url' => $this->createUrl('index'),
                                    'color' => TbHtml::BUTTON_COLOR_WARNING
                                )
                            ),
                            'edit' => array(
                                'type' => TbHtml::BUTTON_TYPE_LINK,
                                'label' => Yii::t($this->getId(), 'Detail action edit'),
                                'attributes' => array(
                                    'url' => $this->createUrl('save', array('id' => $model->id)),
                                    'color' => TbHtml::BUTTON_COLOR_PRIMARY
                                )
                            )
                        )
                    ),
                    array(
                        get_class($model) . 'DetailView',
                        'data' => $model,
                    ),
                )
            ), TRUE
        );
        $this->renderText($grid);
    }

    /**
     * @param null $id
     * @throws Exception
     */
    public function actionSave($id = NULL){

        if (($model = AR::model($this->model)->findByPk($id)) === null) {

            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }

        $this->gridForm($model);
    }

    /**
     * @param $model
     * @throws Exception
     */
    public function gridForm($model){

        $formClass = get_class($model) . 'Form';

        $grid = $this->widget('FieldSetView', array(
            'header' => Yii::t($this->getId(), $model->isNewRecord ? 'Form header create' : 'Form header update'),
            'items' => array(
                array(
                    'ext.widgets.FormView',
                    'form' => new $formClass(
                        array(
                            'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                            //'elementGroupName' => '[]'
                        ),
                        $model, $this
                    ),
                    'items' => array(
                        array(
                            'ActionsView',
                            'items' => array(
                                'submit' => array(
                                    'type' => TbHtml::BUTTON_TYPE_SUBMIT,
                                    'label' => Yii::t($this->getId(), 'Form action save'),
                                    'attributes' => array(
                                        'color' => TbHtml::BUTTON_COLOR_SUCCESS
                                    )
                                ),
                                'cancel' => array(
                                    'type' => TbHtml::BUTTON_TYPE_LINK,
                                    'label' => Yii::t($this->getId(), 'Form action cancel'),
                                    'attributes' => array(
                                        'url' => $this->createUrl('index'),
                                        'color' => TbHtml::BUTTON_COLOR_DANGER
                                    )
                                )
                            )
                        )
                    )

                ),
            )
        ), TRUE);

        $this->renderText($grid);
    }
}