<?php

class ExhibitionComplexController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/main';

	/**
	 * @var array page variants for gridView
	 */
	static $pageVariants = [1, 3, 5, 10, 20, 50, 100, 200, 500, 1000, 5000, 10000];

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'view'),
                'roles' => array(User::ROLE_ADMIN),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'update'),
                'roles' => array(User::ROLE_ADMIN),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete'),
                'roles' => array(User::ROLE_ADMIN),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'getAssociations'),
                'roles' => array(User::ROLE_ADMIN),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'getCitiesByText'),
                'roles' => array(User::ROLE_ADMIN),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'getCityGeoData'),
                'roles' => array(User::ROLE_ADMIN),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if(Yii::app()->user->role != User::ROLE_ADMIN) {
			Yii::app()->createUrl('/home');		}

		$model = $this->loadModel($id);

		$fairs = $model->fairs;
		$sort = new CSort;
		$sort->defaultOrder = 'id ASC';
		$sort->attributes = array('id');

		$fairsDataProvider = new CArrayDataProvider($fairs,	array('sort' => $sort));
		
		$this->render('view', array(
			'model' => $model,
			'fairsDataProvider' => $fairsDataProvider,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(Yii::app()->user->role != User::ROLE_ADMIN) {
			Yii::app()->createUrl('/home');		}

		$model = new ExhibitionComplex;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['ExhibitionComplex'])) {
			$model->attributes = $_POST['ExhibitionComplex'];

            if(!empty(Yii::app()->user->id)){
                $model->userId = Yii::app()->user->id;
		    }

			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		if (Yii::app()->user->role != User::ROLE_ADMIN) {
			Yii::app()->createUrl('/home');
		}

		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['ExhibitionComplex'])) {

			$model->attributes = $_POST['ExhibitionComplex'];

            if(!empty(Yii::app()->user->id)){
                $model->userId = Yii::app()->user->id;
            }

			if(isset($_POST['cityId']))
				$model->setAttribute('cityId', $_POST['cityId']);
			if(isset($_POST['regionId']))
				$model->setAttribute('regionId', $_POST['regionId']);

			if ($model->save()){

				if(isset($_POST[get_class($model)][ExhibitionComplex::ASSOCIATION_VARIABLE]) && !empty($_POST[get_class($model)][ExhibitionComplex::ASSOCIATION_VARIABLE])){

					$vcAssociationIDs = explode(',', $_POST[get_class($model)][ExhibitionComplex::ASSOCIATION_VARIABLE]);

					if(is_array($vcAssociationIDs) && count($vcAssociationIDs) > 0){

						ExhibitionComplexHasAssociation::model()->deleteAllByAttributes(['exhibitionComplexId' => $model->id]);

						foreach ($vcAssociationIDs as $vcAssociationID){
							$vcAssociation = Association::model()->findByPk($vcAssociationID);

							if($vcAssociation !== NULL){
								$ECHA = new ExhibitionComplexHasAssociation;
								$ECHA->attributes = [
									'exhibitionComplexId' => $model->id,
									'associationId'  => $vcAssociationID,
								];
								$ECHA->save();
							}
						}
					}
				} elseif(isset($_POST[get_class($model)][ExhibitionComplex::ASSOCIATION_VARIABLE]) && empty($_POST[get_class($model)][ExhibitionComplex::ASSOCIATION_VARIABLE])){
					ExhibitionComplexHasAssociation::model()->deleteAllByAttributes(['exhibitionComplexId' => $model->id]);
				}

				/** Загрузка Заглавного Изображения */
				$ExhibitionComplexImage = CUploadedFile::getInstancesByName('ExhibitionComplex[imageId]');
				if (isset($ExhibitionComplexImage[0])) {
					(new ObjectFile())->createFile($ExhibitionComplexImage[0], $model, ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE, ObjectFile::EXT_IMAGE);
				}

				/** Загрузка Изображений Галереи */
				foreach (CUploadedFile::getInstancesByName('ExhibitionComplex[galleryImages]') as $ExhibitionComplexGalleryImages) {
					(new ObjectFile())->createFile($ExhibitionComplexGalleryImages, $model, ObjectFile::TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES, ObjectFile::EXT_IMAGE);
				}
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->user->role != User::ROLE_ADMIN) {
			Yii::app()->createUrl('/home');		}

		$model = $this->loadModel($id);
		$model->active = User::ACTIVE_OFF;
		$model->save(false);
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->role != User::ROLE_ADMIN) {
			Yii::app()->createUrl('/home');		}

		$dataProvider = new CActiveDataProvider('ExhibitionComplex');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin(){

		if (Yii::app()->user->role != User::ROLE_ADMIN) {
			Yii::app()->createUrl('/home');
		}

		$pageVariants = array_combine(self::$pageVariants, self::$pageVariants);

		if (isset($_GET['ExhibitionComplex_pageSize'])) {
			Yii::app()->user->setState('ExhibitionComplex_pageSize', (int)$_GET['ExhibitionComplex_pageSize'] ?: reset($pageVariants));
		}

		$model = new ExhibitionComplex('search');
		$model->unsetAttributes();
		if (isset($_GET['ExhibitionComplex'])) {
			$model->attributes = $_GET['ExhibitionComplex'];
		}

		$descriptionFields = [];
		foreach($model->description() as $key => $description)
			if (!isset($description['relation']) && (!isset($description[0]) || $description[0] != 'pk'))
				$descriptionFields[$key] = !empty($description['label']) ? $description['label'] : $key;

		if(isset($descriptionFields['exhibitionHasFiles']))
			unset($descriptionFields['exhibitionHasFiles']);
		if(isset($descriptionFields['yaMap']))
			unset($descriptionFields['yaMap']);
		if(isset($descriptionFields['yaMap']))
			unset($descriptionFields['yaMap']);
		if(isset($descriptionFields['fair']))
			unset($descriptionFields['fair']);
		if(isset($descriptionFields['galleryImages']))
			unset($descriptionFields['galleryImages']);

		$fields = array();
		$fieldsCustomOrder = array(
			'statusName',
			'active',
			'name',
			'description',
			'uniqueText',
			'country_name',
			'district_name',
			'region_name',
			'postcode',
			'city_name',
			'street',
			'coordinates',
			'rsva',
			'associationName',
			'square',
			'members',
			'visitors',
			'number',
			'fairsCount',
			'accreditationPrice',
			'accreditationContact',
			'phone',
			'email',
			'site',
			'advertisingContact',
			'standsConstructionContact',
			'cateringContact',
			'services',
			'FoundationYear',
			'plainHash',
			'descriptionSnippet',
			'exdbId',
			'exdbContact',
			'exdbRaw',
            'logoExists',
            'datetimeModified',
            'lastRedactorEmail',
		);

		foreach ($fieldsCustomOrder as $fieldCustom)
			if(key_exists($fieldCustom, $descriptionFields))
				$fields[$fieldCustom] = $descriptionFields[$fieldCustom];
		
		$fieldsDefaults = array_keys($fields);
		$fieldsArr = [];

		if (isset($_GET['ExhibitionComplex_fields'])) {

			if(is_array($_GET['ExhibitionComplex_fields']) && !empty($_GET['ExhibitionComplex_fields']))
				$getFields = $_GET['ExhibitionComplex_fields'];
			elseif (is_string($_GET['ExhibitionComplex_fields']))
				$getFields = explode(',', $_GET['ExhibitionComplex_fields']);
			else $getFields = array();

			foreach ($getFields as $getField)
				if (in_array($getField, $fieldsDefaults))
					$fieldsArr[] = $getField;

			Yii::app()->user->setState('ExhibitionComplex_fields', implode(',', $fieldsArr));
		}

		$selectedFields = Yii::app()->user->getState('ExhibitionComplex_fields');
		$selectedFields = $selectedFields ? explode(',', $selectedFields) : [];
		$flippedSelectedFields = array_flip($selectedFields);

		$pageSize = Yii::app()->user->getState('ExhibitionComplex_pageSize', reset($pageVariants));

		$dataProvider = ExhibitionComplexAdminService::search($selectedFields, $pageSize, $model);

		$selectedFields = [];
		foreach ($flippedSelectedFields as $selectedField => $i) {

			$description = $model->description();

			$header = $selectedField;
			if(array_key_exists($selectedField, $description) && isset($description[$selectedField]['label'])){
				$header = $description[$selectedField]['label'];
			}
			$selectedFields[$selectedField] = [
				'name' => $selectedField,
				'type' => 'raw',
				'header' => $header,
			];

			if($selectedField == 'country_name'){
                $selectedFields[$selectedField] = [
                    'name' => $selectedField,
                    'type' => 'raw',
                    'header' => $header,
                    'filter' => TbHtml::activeDropDownList(
                        $model,
                        'country_name',
                        Country::countries(),
                        array(
                            'empty' => ''
                        )
                    ),
                ];
            } elseif ($selectedField == 'statusName'){
				$selectedFields[$selectedField] = [
					'name' => $selectedField,
					'type' => 'raw',
					'header' => $header,
					'filter' => TbHtml::activeDropDownList(
						$model,
						'statusName',
						ExhibitionComplexStatus::statuses(),
						array(
							'empty' => ''
						)
					),
				];
			}  elseif ($selectedField == 'logoExists'){
				$selectedFields[$selectedField] = array(
                    'name' => 'logoExists',
                    'header' => Yii::t('AdminModule.admin', 'Is fair logo loaded'),
                    'type' => 'raw',
                    'filter' => TbHtml::activeDropDownList($model, 'logoExists', array(2 => 'Прикреплено', 1 => 'Не прикреплено'), array('empty' => 'Не выбрано')),
                    'htmlOptions' => array(
                        'style' => "vertical-align: top;"
                    ),
                );
			} elseif($selectedField == 'site'){
				$selectedFields[$selectedField]['value'] = function($data){
					if(isset($data['site'])){
						return CHtml::link($data['site'], $data['site'], array('target'=>'_blank'));
					}
					return '';
				};
			}
		}

		$this->render('admin', array(
			'model'          => $model,
			'pageVariants'   => $pageVariants,
			'pageSize'       => Yii::app()->user->getState('ExhibitionComplex_pageSize', reset($pageVariants)),
			'fields'         => $fields,
			'selectedFields' => $selectedFields,
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ExhibitionComplex the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = ExhibitionComplex::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ExhibitionComplex $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'exhibition-complex-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionGetCitiesByText($q){
		$sql = "SELECT c.id, trc.name FROM tbl_city c
				LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id AND trc.langId = :langId
                WHERE trc.name LIKE :q
                OR c.id LIKE :q
        ";

		$data = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
			':q' => "%$q%",
			':langId' => Yii::app()->language,
		]);

		echo json_encode($data);
	}

	/**
	 * @param $q
	 */
	public function actionGetAssociations($q){

		$criteria = new CDbCriteria;
		$criteria->compare('t.name', $q, true,'OR');
		$criteria->compare('t.id', $q, true,'OR');

		$association = CHtml::listData(
			Association::model()->findAll($criteria),
			'id',
			'name');

		$items = array_map(
			function($association_id, $association_name){
				return array(
					'id' => $association_id,
					'name' => $association_name
				);
			},
			array_keys($association),
			array_values($association)
		);

		echo json_encode($items);
	}

	public function actionGetCityGeoData(){
		
		if(!isset($_POST['cityId']))
		{
			echo json_encode(array('success' => FALSE,'message' => 'post didn\'t find'));
			return;
		}

		$cityId = $_POST['cityId'];
		$city = City::model()->findByPk($cityId);

		if(empty($city)){
			echo json_encode(array('success' => FALSE,'message' => 'city didn\'t find'));
			return;
		}

		echo json_encode(
			array(
				'success' => TRUE,
				'regionName' => isset($city->region->name) ? $city->region->name : '',
				'districtName' => isset($city->region->district->name) ? $city->region->district->name : '',
				'countryName' => isset($city->region->district->country->name) ? $city->region->district->country->name : '',
			)
		);
	}
}
