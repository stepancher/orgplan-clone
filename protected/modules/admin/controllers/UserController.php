<?php
Yii::import('application.modules.tz.models.Passport');

class UserController extends Controller
{
    /**
     * @var User $model
     */
    public $model = 'User';

    public $isNewRecord;

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array page variants for gridView
     */
    static $pageVariants = [10, 20, 50, 100, 200, 500, 1000, 5000];

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'save'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('getRegion'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('getCity'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
//	public function actionUpdate($id)
//	{
//		$model=$this->loadModel($id);
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['User']))
//		{
//			$model->attributes=$_POST['User'];
//			if($model->save())
//				$this->redirect(array('view','id'=>$model->id));
//		}
//
//		$this->render('update',array(
//			'model'=>$model,
//		));
//	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect('admin');
//		$dataProvider=new CActiveDataProvider('User');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {

        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        $pageVariants = array_combine(self::$pageVariants, self::$pageVariants);
        if (isset($_GET['User_pageSize'])) {
            Yii::app()->user->setState('User_pageSize', (int)$_GET['User_pageSize'] ?: reset($pageVariants));
            unset($_GET['User_pageSize']);
        } else {
            $pageSize = Yii::app()->user->getState('User_pageSize', reset($pageVariants));
            $url = $_SERVER['REQUEST_URI'] . ($_SERVER['QUERY_STRING'] ? '&' : '?') . 'User_pageSize=' . $pageSize;
            $this->redirect($url);
        }

        $model = new User('searchOnAdmin');
        $model->unsetAttributes();
        if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }

        $fields = [];
        foreach($model->description() as $key => $description) {
            if (!isset($description['relation']) && (!isset($description[0]) || $description[0] != 'pk')) {
                $fields[$key] = !empty($description['label']) ? $description['label'] : $key;
            }
        }
        $fieldsDefaults = array_keys($fields);
        $fieldsArr = [];
        if (isset($_GET['User_fields'])) {
            $getFields = explode(',', $_GET['User_fields']);
            foreach ($getFields as $getField) {
                if (in_array($getField, $fieldsDefaults)) {
                    $fieldsArr[] = $getField;
                }
            }
            Yii::app()->user->setState('User_fields', implode(',', $fieldsArr));
            unset($_GET['User_fields']);
        } else {
            $fieldsStr = Yii::app()->user->getState('User_fields', '');
            $url = $_SERVER['REQUEST_URI'] . ($_SERVER['QUERY_STRING'] ? '&' : '?') . 'User_fields=' . $fieldsStr;
            $this->redirect($url);
        }

        $selectedFields = Yii::app()->user->getState('User_fields');
        $selectedFields = $selectedFields ? explode(',', $selectedFields) : [];
        $flippedSelectedFields = array_flip($selectedFields);
        $selectedFields = [];
        foreach ($flippedSelectedFields as $selectedField => $i) {
            $selectedFields[$selectedField] = [
                'name' => $selectedField
            ];
        }
        if (isset($selectedFields['role'])) {
            $selectedFields['role']['value'] = function ($data) {
                return User::getRole($data->role);
            };
            $selectedFields['role']['filter'] = CHtml::activeDropDownList($model, 'role', User::roles(), ['empty' => '']);
        }

        if (isset($selectedFields['contact_email'])) {
            $selectedFields['contact_email']['value'] = '!empty($data->contact->email) ? $data->contact->email : ""';
        }

        if (isset($selectedFields['tariff_name'])) {
            $selectedFields['tariff_name']['value'] = '!empty($data->tariff->name) ? $data->tariff->name : ""';
        }

        if (isset($selectedFields['banned'])) {
            $selectedFields['banned']['value'] = function ($data) {
                return User::getBannedStatus($data->banned);
            };
            $selectedFields['banned']['filter'] = CHtml::activeDropDownList($model, 'banned', User::bannes(), ['empty' => '']);
        }

        if (isset($selectedFields['active'])) {
            $selectedFields['active']['value'] = function ($data) {
                return User::getActiveStatus($data->active);
            };
            $selectedFields['active']['filter'] = CHtml::activeDropDownList($model, 'active', User::actives(), ['empty' => '']);
        }

        if (isset($selectedFields['city_name'])) {
            $selectedFields['city_name']['value'] = '!empty($data->city->name) ? $data->city->name : ""';
        }

        $this->render('admin', array(
            'model'          => $model,

            'pageVariants'   => $pageVariants,
            'pageSize'       => Yii::app()->user->getState('User_pageSize', reset($pageVariants)),

            'fields'         => $fields,
            'selectedFields' => $selectedFields
        ));










/*
        $model = new User('searchOnAdmin');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
*/
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param null $id
     * @param string $return
     * Создание/редактирование пользователя
     */
    public function actionSave($id = null, $return = 'index')
    {
        if (Yii::app()->user->getState('role') != User::ROLE_ADMIN) {
            $this->redirect(Yii::app()->createUrl('/home'));
        }


        if (Yii::app()->request->isAjaxRequest) {
            $modelUser = new User('admin');
            $modelContact = new Contact();
            if (isset($_POST['User'])) {
                $modelUser->setAttributes($_POST['User']);
                $modelUser->password = isset($_POST['User']['password'])
                    && !empty($_POST['User']['password'])
                    ? password_hash($_POST['User']['password'], PASSWORD_DEFAULT)
                    :null;
            }
            if (isset($_POST['Contact'])) {
                $modelContact->setAttributes($_POST['Contact']);
            }
            $modelContact->validate();
            $modelUser->validate();
            $resultUser = array();
            if (count($modelUser->getErrors())) {
                foreach ($modelUser->getErrors() as $attribute => $errors) {
                    $resultUser[CHtml::activeId($modelUser, $attribute)] = $errors;
                }
            }
            $resultContact = array();
            if (count($modelContact->getErrors())) {
                foreach ($modelContact->getErrors() as $attribute => $errors) {
                    $resultContact[CHtml::activeId($modelContact, $attribute)] = $errors;
                }
            }
            $result = array_merge($resultUser, $resultContact);
            $errors = function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);
            echo $errors;
            return;
        } else {
            /** @var User $model
             * @var CWebApplication $app
             */
            if (null === $id || ($model = AR::model($this->model)->findByPk($id)) === null)
                $model = new $this->model();

            $model->scenario = 'admin';

            $ctrl = $this;
            $ctrl->isNewRecord = $model->isNewRecord;

            //$transaction = $app->getDb()->beginTransaction();

            Yii::import('application.modules.profile.models.LegalEntity');
            Yii::import('application.modules.profile.models.BankAccount');
            $records = AR::multiSave(
                $_POST,
                // 1 уровень для сохранения
                array(
                    function ($record) use ($model) {
                        /** @var User $user */
                        /** @var Contact $contact */
                        $user = $record['model'];

                        $user->password = $_POST['User']['password'] == '' ?
                            $model->password :
                            password_hash($_POST['User']['password'], PASSWORD_DEFAULT);

                        $contact = $record['contact'];
                        if ($user->role == User::ROLE_DEVELOPER &&
                            (!isset($_POST['Contact']['companyName']) || (isset($_POST['Contact']['companyName']) && empty($_POST['Contact']['companyName'])))
                        ) {
                            $contact->addError('companyName', Yii::t('AdminModule.admin', 'You must fill in the name of the company'));
                            return false;
                        }
                    }
                ),
                array(
                    array(
                        'name' => 'contact',
                        'class' => 'Contact',
                        'record' => $model->contact ? $model->contact : new Contact(),
                    ),
                    array(
                        'name' => 'legalEntity',
                        'class' => 'LegalEntity',
                        'record' => $model->legalEntity ? $model->legalEntity : new LegalEntity(),
                        'scenario' => $ctrl->isNewRecord ? 'insert' : 'search,insert,update',
                        'saved' => User::getSaveByRoles(isset($_POST['User']) ? $_POST['User']['role'] : null, 'exponent')
                    ),
                    array(
                        'name' => 'bankAccount',
                        'class' => 'BankAccount',
                        'record' => $model->bankAccount ? $model->bankAccount : new BankAccount(),
                        'scenario' => $ctrl->isNewRecord ? 'insert' : 'search,insert,update',
                        'saved' => User::getSaveByRoles(isset($_POST['User']) ? $_POST['User']['role'] : null, 'exponent')
                    ),
                    function ($records) use ($ctrl) {
                        /** @var User $user */
                        $user = $records['model'];

                        /** @var LegalEntity $legalEntity */
                        $legalEntity = $records['legalEntity'];

                        /** @var Contact $contact */
                        $contact = $records['contact'];

                        /** @var BankAccount $bankAccount */
                        $bankAccount = $records['bankAccount'];

                        $user->contactId = $contact->id;
                        //$user->banned = User::BANNED_OFF;
                        if (User::getSaveByRoles(isset($_POST['User']) ? $_POST['User']['role'] : null, 'exponent')) {
                            $user->legalEntityId = $legalEntity->id;
                            $user->bankAccountId = $bankAccount->id;
                        }
                        //$user->active = User::checkAdministrationRoles() ? User::ACTIVE_ON : User::ACTIVE_OFF;
                    }
                ),
                // 2
                array(
                    array(
                        'name' => 'model',
                        'class' => 'User',
                        'record' => $model,
                    ),
                    function ($records) use ($ctrl) {
                        /** @var User $user */
                        $user = $records['model'];
                        $bool = User::checkRolesForUserSetting($user->role);

                        /** @var UserSetting $userSetting
                         * @var CWebApplication $app
                         */
                        $role = $user->role;
                        $userSetting = $records['userSetting'];
                        if (!$user->userSetting) {
                            $userSetting->userId = $user->id;
                            $userSetting->dayTask = $role == User::ROLE_ADMIN || $role == User::ROLE_OPERATOR
                            || $role == User::ROLE_EXPERT || $role == User::ROLE_ORGANIZERS ? false : true;
                            $userSetting->dailyEventNotification = $role == User::ROLE_ADMIN || $role == User::ROLE_OPERATOR
                            || $role == User::ROLE_EXPERT || $role == User::ROLE_ORGANIZERS ? false : true;
                            $userSetting->administrationRequestPriseStand = $role == User::ROLE_EXPONENT ||
                            $role == User::ROLE_FREELANCER || $role == User::ROLE_DEVELOPER ? false : true;
                            $userSetting->changedToRebidding = $role == User::ROLE_EXPONENT ? false : true;
                            $userSetting->developerSelectedToFinalist = $role == User::ROLE_EXPONENT ? false : true;
                            $userSetting->regionalAuctionCreated = $role == User::ROLE_EXPONENT ? false : true;
                            $userSetting->changedToEnd = $role == User::ROLE_EXPONENT || $role == User::ROLE_FREELANCER || $role == User::ROLE_DEVELOPER ? true : false;;
                            $userSetting->chosen = $role == User::ROLE_FREELANCER || $role == User::ROLE_DEVELOPER ? false : true;
                            $userSetting->newBid = $role == User::ROLE_FREELANCER || $role == User::ROLE_DEVELOPER ? false : true;
                            $userSetting->developerTakeVictory = $role == User::ROLE_EXPONENT ||
                            $role == User::ROLE_ADMIN || $role == User::ROLE_OPERATOR ? false : true;
                            $userSetting->type = UserSetting::TYPE_ALL;
                        }

                        if (isset($_POST['UserHasOrganizer']['organizerId']) && is_array($_POST['UserHasOrganizer']['organizerId'])) {
                            User::saveOrganizer($user->id, $_POST['UserHasOrganizer']['organizerId']);
                        } else {
                            User::saveOrganizer($user->id);
                        }

                        $calendar = Calendar::model()->findByAttributes(array('userId' => $user->id));
                        if (null == $calendar) {
                            $calendar = new Calendar();
                            $calendar->userId = $user->id;
                            $calendar->save(false);
                        }

                        if ($user->role == User::ROLE_ORGANIZERS) {
                            if (isset($_POST['User']['fair']) && !empty($_POST['User']['fair'])) {
                                if (!empty($user->userHasFairs)) {
                                    foreach ($user->userHasFairs as $UHF) {
                                        $UHF->delete();
                                    }
                                }
                                foreach ($_POST['User']['fair'] as $fairId) {
                                    $UHF = new UserHasFair();
                                    $UHF->userId = $user->id;
                                    $UHF->fairId = $fairId;
                                    $UHF->save(false);
                                }
                            } else {
                                if (!empty($user->userHasFairs)) {
                                    foreach ($user->userHasFairs as $UHF) {
                                        $UHF->delete();
                                    }
                                }
                            }
                        }

                        if ($user->role == User::ROLE_DEVELOPER || $user->role == User::ROLE_FREELANCER) {
                            if (null == $user->portfolio) {
                                $portfolio = new Portfolio();
                                $portfolio->userId = $user->id;
                                $portfolio->save(false);
                            }

                        }
                        if ($user->role == User::ROLE_EXPONENT) {
                            if (null === $user->exponent) {
                                $exponent = new Exponent();
                                $exponent->userId = $user->id;
                                $exponent->save(false);
                                $user->exponent = $exponent;
                            }
                            if (isset($_POST['User']['exponent_industryId'])) {
                                $user->exponent->industryId = $_POST['User']['exponent_industryId'];
                                $user->exponent->save(false);
                            }

                            if ($user->exponent && !empty($user->exponent->geographyOfActivities)) {
                                foreach ($user->exponent->geographyOfActivities as $PGOAR) {
                                    $PGOAR->delete();
                                }
                            }
                        }
                        // Обходим список загруженных файлов
                    }
                ),
                // 3
                array(
                    array(
                        'name' => 'userSetting',
                        'class' => 'UserSetting',
                        'record' => $model->userSetting ? $model->userSetting : new UserSetting(),
                    ),
                    function ($records) use ($ctrl, $return, $model) {


                        //$transaction->commit();

                        /** @var CWebApplication $app */
                        $app = Yii::app();

                        $app->user->setFlash('success', Yii::t('AdminModule.admin', 'information is saved'));

                        if ($app->user->isGuest) {
                            /** @var User $user */
                            $user = User::model()->findByPk($records['model']->id);
                            $identity = new UserIdentity($user->login, $user->password);
                            if ($identity->authenticateByRecord($user)) {
                                if ($app->user->login($identity)) {
                                    if ($ctrl->isNewRecord) {
                                        if ($user->active == User::ACTIVE_OFF) {
                                            Yii::app()->user->setFlash('warning', Yii::t('AdminModule.admin', 'Your account is not activated'));
                                        }
                                    }
                                }
                                if (!empty($_POST['Contact']['email'])) {
                                    Yii::app()->consoleRunner->run('notification sendActivatedMessage --id="' . $user->id . '" --email="' . $_POST['Contact']['email'] . '"', true);
                                }
                                if (empty($_POST['fair'])) {
                                    if ($ctrl->isNewRecord) {
                                        /*										Notification::saveByObject(
                                                                                    1,
                                                                                    $user,
                                                                                    Notification::TYPE_NEW_FAIR_ORGANIZER,
                                                                                    array(
                                                                                        '{login}' => CHtml::link($user->login, $ctrl->createUrl('user/view', array('id' => $user->id))),
                                                                                    ),
                                                                                    null,
                                                                                    false
                                                                                );*/
                                    }
                                } else {
                                    if (!empty($model->userHasFairs)) {
                                        foreach ($model->userHasFairs as $UHF) {
                                            if ($UHF->fair->id != null && $user->id != null) {
//                                                $OCA = new OrganizersControlAccess();
//                                                $OCA->fairId = $UHF->fair->id;
//                                                $OCA->userId = $user->id;
//                                                if ($OCA->save(false)) {
                                                    /*												Notification::saveByObject(
                                                                                                        1,
                                                                                                        $user,
                                                                                                        $notification = Notification::TYPE_NEW_FAIR_ORGANIZER_CHOSE_FAIR,
                                                                                                        array(
                                                                                                            '{login}' => CHtml::link($user->login, $ctrl->createUrl('user/view', array('id' => $user->id))),
                                                                                                            '{name}' => CHtml::link($fair->name, $ctrl->createUrl('fair/view', array('id' => $fair->id)))
                                                                                                        ),
                                                                                                        null,
                                                                                                        false
                                                                                                    );*/
                                                    /*												if ($notification) {
                                                                                                        Notification::saveByObject(
                                                                                                            $user->id,
                                                                                                            $user,
                                                                                                            Notification::TYPE_FAIR_MODERATION,
                                                                                                            array(
                                                                                                                '{name}' => CHtml::link($fair->name, $ctrl->createUrl('fair/view', array('id' => $fair->id)))
                                                                                                            ),
                                                                                                            null,
                                                                                                            false
                                                                                                        );
                                                                                                    }*/

//                                                }
                                            }

                                        }

                                    }
                                }


                            }
                            $ctrl->redirect($ctrl->createUrl('profile'));
                        } else {
                            if (User::checkAdministrationRoles()) {
                                $ctrl->redirect($ctrl->createUrl('admin/user/admin'));
                            }
                        }
                    }
                )
//			function() use ($transaction) {
//				$transaction->rollback();
//			}
            );

//            $this->render('//user/save', $records);
            $this->render('update', $records);
        }
    }

    /**
     * @param $q
     */
    public function actionGetRegion($q){

        echo json_encode(Region::getRegionName($q));

    }

    public function actionGetCity($q){

        echo json_encode(City::getCityName($q));

    }

}
