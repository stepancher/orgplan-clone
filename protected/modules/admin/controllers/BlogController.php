<?php

Yii::import('application.modules.blog.models.Blog');
Yii::import('application.modules.blog.models.BlogHasTag');
Yii::import('application.modules.blog.models.BlogHasFile');
Yii::import('application.modules.blog.models.Tag');

class BlogController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';
    public $model = 'Blog';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionSave($id = null)
    {
        /** @var Blog $model */
        if (null === $id || ($model = Blog::model($this->model)->findByPk($id)) === null)
            $model = new $this->model();
        $ctrl = $this;

        if (!User::checkAdministrationRoles() && (!$model->isNewRecord && ($model->authorId !== Yii::app()->user->id))) {
            $this->redirect($this->createUrl('index'));
        }

        $records = Blog::multiSave(
            $_POST,
            array(
                function ($records) use ($model) {
                    if ($model->isNewRecord) {
                        $model->created = date('Y-m-d H:i:s', time());
                    } else {
                        $model->created = date('Y-m-d H:i:s', strtotime($model->created));
                    }
                }
            ),
            array(
                array(
                    'name' => 'model',
                    'class' => get_class($model),
                    'record' => $model
                ),
                function ($records) use ($ctrl, $id) {

                    /** @var Blog $blog */
                    $blog = $records['model'];

                    $logo = CUploadedFile::getInstances(Blog::model(), 'image');

                    if (!empty($logo)) {
                        $differentOptions = [
                            'sizes' => [
                                620 => 348,
                                300 => 168,
                            ]
                        ];
                        if ($blog->image) {
                            $path = $blog->image->getDirPath(true);
                            $objId = $blog->image->saveFile($logo[0], true, true, $differentOptions);
                            if ($objId) {
                                $BHF = new BlogHasFile();
                                $BHF->blogId = $blog->id;
                                $BHF->fileId = $objId;
                                $BHF->save();
                                unset($path);
                            }
                        } else {
                            $objFile = new ObjectFile();
                            $objFile->type = ObjectFile::TYPE_BLOG_MAIN_IMAGE;
                            $objFile->ext = ObjectFile::EXT_IMAGE;
                            $objFile->setOwnerId($blog->id);
                            $objId = $objFile->saveFile($logo[0], true, true, $differentOptions);
                            if ($objId) {
                                $BHF = new BlogHasFile();
                                $BHF->blogId = $blog->id;
                                $BHF->fileId = $objId;
                                $BHF->save();
                            }

                            $blog->imageId = $objFile->id;
                        }
                        $blog->save(false);
                    }
                    IRedactor::createItems(get_class($blog));
                    IRedactor::clearUploadFiles();
                    foreach (CUploadedFile::getInstances(Blog::model(), 'files') as $file) {
                        ObjectFile::createFile($file, $blog, ObjectFile::TYPE_BLOG_FILES, ObjectFile::EXT_FILE);
                    }
                    foreach (CUploadedFile::getInstances(Blog::model(), 'images') as $file) {
                        ObjectFile::createFile($file, $blog, ObjectFile::TYPE_BLOG_IMAGES, ObjectFile::EXT_IMAGE);
                    }

                    if (isset($_POST['Blog']['tag'])) {
                        $BHTs = BlogHasTag::model()->findAllByAttributes(array('blogId' => $blog->id));

                        /** @var Tag[] $tagRemoves */
                        $tagRemoves = [];
                        if (sizeof($BHTs)) {
                            foreach ($BHTs as $BHT) {
                                if (BlogHasTag::model()->countByAttributes(['tagId' => $BHT->tagId]) == 1) {
                                    if ($BHT->tag) {
                                        $tagRemoves[$BHT->tag->name] = $BHT->tag;
                                    }
                                }
                                $BHT->delete();
                            }
                        }

                        if (!empty($_POST['Blog']['tag'])) {
                            foreach (explode(',', $_POST['Blog']['tag']) as $tagName) {
                                $tagName = trim($tagName);
                                if (!empty($tagName)) {
                                    $tag = Tag::model()->findByAttributes(['name' => $tagName]);
                                    if (null === $tag) {
                                        $tag = new Tag();
                                        $tag->name = $tagName;
                                        $tag->save(false);
                                    }
                                    $blogHasTag = new BlogHasTag();
                                    $blogHasTag->tagId = $tag->id;
                                    $blogHasTag->blogId = $blog->id;
                                    $blogHasTag->save(false);
                                    unset($tagRemoves[$tagName]);
                                }
                            }
                        }

                        foreach ($tagRemoves as $tag) {
                            $tag->delete();
                        }
                    }
                }
            ),
            array(
                function ($records) use ($ctrl) {
                    Yii::app()->user->setFlash('success', Yii::t('AdminModule.admin', 'The information is saved.'));
                    $ctrl->redirect(Yii::app()->urlManager->createUrl('admin/blog/view', array('id' => $records['model']->id), Yii::app()->params['defaultAmpersand'], FALSE));
                }
            )
        );

        /** @var Blog $blog */
        $blog = $records['model'];
        $tags = [];
        foreach ($blog->blogHasTags as $BHT) {
            if (null !== $BHT->tag) {
                $tags[] = $BHT->tag->name;
            }
        }
        $blog->tag = implode(', ', $tags);

        /** Очистка файлов временной директории, если все модели прошли валидацию */
        IRedactor::clearUploadFiles($records);
        $this->render('save', $records);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ExhibitionComplex the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Blog::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');        }

        $model = $this->loadModel($id);
        $model->delete(false);
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionView($id)
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');        }

        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionIndex()
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        $model = new Blog('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Blog']))
            $model->attributes = $_GET['Blog'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * @param $id
     * @param $fileId
     */
    public function actionDeleteFile($id, $fileId)
    {
        /** @var Blog $model */
        if (($model = Blog::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }

        $objectFile = ObjectFile::model()->findByPk($fileId);
        if (null == $objectFile) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
        } else {
            Yii::app()->user->setFlash('success', 'Файл удален.');
            $objectFile->deleteFile($model);
        }
        $this->redirect($this->createUrl('save', array('id' => $id)));
    }

    public function actionDeleteArticle($id)
    {
        /** @var Blog $model */
        if ((User::checkAdministrationRoles() || Yii::app()->user->id == $model->authorId)
            && ($blog = Blog::model()->findByPk($id)) !== null
        ) {
            if ($blog->delete()) {
                $this->redirect(Yii::app()->createUrl('blog/index'));
            }
        }
    }
}
