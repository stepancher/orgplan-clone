<?php

class FairController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array page variants for gridView
     */
    static $pageVariants = [1, 3, 5, 10, 20, 50, 100, 200, 500, 1000, 5000, 10000, 20000, 30000];

    /**
     * @var string temporary language for saving multi language fair
     */
    protected $tempLang = '';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'index',
                    'view',
                    'admin',
                    'create',
                    'update',
                    'delete',
                    'copy',
                    'ScdTrFair',
                    'getFairs',
                    'getOtherStatisticFairs',
                    'getRelatedStatisticFairs',
                    'getStatisticFairs',
                    'getFairStatistic',
                    'separateRelatedStatisticFair',
                    'attachFairStatistic',
                    'getIndustries',
                    'getOrganizers',
                    'getAudits',
                    'getExhibitionComplexes',
                    'getAssociations',
                ),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionView($id)
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        $model = $this->loadModel($id);
        $fairInfo = FairInfo::model()->findByPk($model->infoId);

        $result = '';
        $fairAssociations = FairAdminService::getAssociationsByFairId($id);
        $countFairAssociations = count($fairAssociations);

        for ($i=0; $i < $countFairAssociations; $i++){
            if($i == $countFairAssociations - 1) {
                $result .= $fairAssociations[$i]['name'];
            } else {
                $result .= $fairAssociations[$i]['name'] . ',';
            }
        }

        if($fairInfo === NULL){
            $fairInfo = new FairInfo;
        }

        $fho = array_map(
            function($fho){
                return $fho->organizerId;
            },
            $model->fairHasOrganizer
        );

        /** @var array $organizerData */
        $organizerData = OrganizerAdminService::loadModelWithContacts($fho);

        $organizerRows = array();
        foreach ($organizerData as $key => $organizers){
            $organizerRows[$key] = array_filter(array_keys($organizers),
                function($key){
                    if($key == 'organizerName' || $key == 'companyId' || $key == 'active' || $key == 'exhibitionManagement')
                        return FALSE;
                    return TRUE;
                }
            );
        }

        foreach ($organizerRows as $key => &$organizerRow){
            array_walk($organizerRow, function (&$row) use($organizerData, $key){

                $row = array(
                    'name' => $row,
                    'type' => 'raw',
                    'label' => $row == 'id' ? Yii::t('AdminModule.admin', 'Organizer contact id') : Organizer::model()->getAttributeLabel($row),
                );

                if($row['name'] == 'id'){
                    $organizerCompanyLink = "<a target=\"_blank\" href=".Yii::app()->createUrl('admin/organizer/viewContact', array('id' => $organizerData[$key]['id']))." class=\"b-button b-button-icon f-text--tall d-text-light d-bg-warning\" data-icon=\"&#xe0ea;\" ></a>";
                    $row['value'] = $organizerData[$key]['id'] . ' ' . $organizerCompanyLink;
                }

                if($row['name'] == 'linkToTheSiteOrganizers' && isset($organizerData[$key]['linkToTheSiteOrganizers']))
                    $row['value'] = CHtml::link($organizerData[$key]['linkToTheSiteOrganizers'], $organizerData[$key]['linkToTheSiteOrganizers']);

            });
        }

        $this->render('view', array(
            'model' => $model,
            'fairAssociationsString' => $result,
            'fairInfo' => $fairInfo,
            'organizerData' => $organizerData,
            'organizerRows' => $organizerRows,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        $model = new Fair;
        $FairInfo = new FairInfo;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Fair'])) {

            $model->attributes = $_POST['Fair'];

            $newShortUrl = $model->getSeoNameForNewVersion();
            $newTrFair = TrFair::model()->findByAttributes(['shortUrl' => $newShortUrl, 'langId' => Yii::app()->language]);

            if($newTrFair !== NULL){

                Yii::app()->user->setFlash('danger', "Параметры выставки: наименование, дата начала, выставочный центр уже соответствуют выставке id = {$newTrFair->trParentId}");

            } else {

                $transaction = Yii::app()->db->beginTransaction();
                $this->tempLang = Yii::app()->language;

                try{

                    if($model->active == Fair::ACTIVE_ON && !empty($model->exhibitionComplexId) && ExhibitionComplexAdminService::geoRelationValidate($model->exhibitionComplexId) === FALSE)
                        throw new CHttpException(404, 'Нет связанных гео данных.');
                    elseif($model->active == Fair::ACTIVE_ON && !empty($model->exhibitionComplexId)){
                        $exhibitionComplex = ExhibitionComplex::model()->findByPk($model->exhibitionComplexId);
                        if(!empty($exhibitionComplex) && $exhibitionComplex->active != ExhibitionComplex::ACTIVE_ON)
                            $exhibitionComplex->active = ExhibitionComplex::ACTIVE_ON;
                    }

                    $model->save();
                    if(!empty($exhibitionComplex))
                        $exhibitionComplex->save();
                    if(!empty($model->exhibitionComplexId))
                        ExhibitionComplex::checkWithDeactivation($model->exhibitionComplexId);
                    
                    $langs = Yii::app()->params['translatedLanguages'];

                    foreach ($langs as $lang => $language){

                        Yii::app()->language = $lang;
                        $model->save();
                    }

                    if(empty($_POST['Fair']['storyId']))
                        $model->saveAttributes(array('storyId' => $model->id));

                    if (
                        isset($_POST['Fair'][Fair::INDUSTRY_VARIABLE]) &&
                        !empty($_POST['Fair'][Fair::INDUSTRY_VARIABLE])
                    ) {
                        $fairHasIndustries = explode(',',$_POST['Fair'][Fair::INDUSTRY_VARIABLE]);

                        FairHasIndustry::model()->deleteAllByAttributes(['fairId' => $model->id]);
                        foreach ($fairHasIndustries as $industryId) {
                            $modelIndustry = new FairHasIndustry();
                            $modelIndustry->fairId = $model->id;
                            $modelIndustry->industryId = $industryId;
                            $modelIndustry->save();
                        }
                    }

                    if (isset($_POST['Fair'][Fair::ORGANIZER_VARIABLE]) && !empty($_POST['Fair'][Fair::ORGANIZER_VARIABLE])) {

                        if(is_string($_POST['Fair'][Fair::ORGANIZER_VARIABLE])){
                            $fairHasOrganizers = explode(',',$_POST['Fair'][Fair::ORGANIZER_VARIABLE]);
                        } elseif(is_array($_POST['Fair'][Fair::ORGANIZER_VARIABLE])){
                            $fairHasOrganizers = $_POST['Fair'][Fair::ORGANIZER_VARIABLE];
                        } else {
                            $fairHasOrganizers = array();
                        }

                        FairHasOrganizer::model()->deleteAllByAttributes(['fairId' => $model->id]);
                        foreach ($fairHasOrganizers as $organizerId) {
                            $modelOrganizer = new FairHasOrganizer;
                            $modelOrganizer->fairId = $model->id;
                            $modelOrganizer->organizerId = $organizerId;
                            $modelOrganizer->save();
                        }
                    }

                    if(isset($_POST['fairIsForum']) && $_POST['fairIsForum'] == 1){
                        $FairInfo->forumId = $model->id;
                    }

                    if(isset($_POST['FairInfo']) && !empty(array_filter($_POST['FairInfo']))){
                        $FairInfo->attributes = $_POST['FairInfo'];
                    }

                    $FairInfo->save();

                    if(!empty($FairInfo->id))
                        $model->saveAttributes(['infoId' => $FairInfo->id]);

                    if(isset($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]) &&
                        !empty($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE])){

                        $fairAssociationIDs = explode(',', $_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]);

                        if(is_array($fairAssociationIDs) && count($fairAssociationIDs) > 0){
                            FairHasAssociation::model()->deleteAllByAttributes(['fairId' => $model->id]);

                            foreach ($fairAssociationIDs as $fairAssociationID){
                                $FHA = new FairHasAssociation;
                                $FHA->attributes = [
                                    'fairId' => $model->id,
                                    'associationId'  => $fairAssociationID,
                                ];
                                $FHA->save();
                            }
                        }
                    }

                    if(isset($_POST[get_class($model)][Fair::AUDIT_VARIABLE]) &&
                        !empty($_POST[get_class($model)][Fair::AUDIT_VARIABLE])){

                        $fairAuditIDs = explode(',', $_POST[get_class($model)][Fair::AUDIT_VARIABLE]);

                        if(is_array($fairAuditIDs) && count($fairAuditIDs) > 0){
                            FairHasAudit::model()->deleteAllByAttributes(['fairId' => $model->id]);

                            foreach ($fairAuditIDs as $fairAuditID){
                                $FHAU = new FairHasAudit;
                                $FHAU->attributes = [
                                    'fairId' => $model->id,
                                    'auditId'  => $fairAuditID,
                                ];
                                $FHAU->save();
                            }
                        }
                    }

                    if(!empty(Yii::app()->user->id))
                        $model->saveAttributes(array('userRedactorId' => Yii::app()->user->id));

                    /** Загрузка Логотипа */
                    $logo = CUploadedFile::getInstancesByName('Fair[logoId]');
                    if (isset($logo[0])) {
                        $objectFile = (new ObjectFile())->createFile($logo[0], $model, ObjectFile::TYPE_FAIR_LOGOS, ObjectFile::EXT_IMAGE);
                        if (null != $objectFile) {
                            $model->logoId = $objectFile->id;
                            $model->update('logoId');
                        }
                    }

                    /** Загрузка Логотипа для писем */
                    $logo = CUploadedFile::getInstancesByName('Fair[mailLogoId]');
                    if (isset($logo[0])) {
                        $objectFile = (new ObjectFile())->createFile($logo[0], $model, ObjectFile::TYPE_FAIR_MAIL_LOGOS, ObjectFile::EXT_IMAGE);
                        if (null != $objectFile) {
                            $model->mailLogoId = $objectFile->id;
                            $model->update('mailLogoId');
                        }
                    }

                    if(!empty($this->tempLang)){
                        Yii::app()->language = $this->tempLang;
                    }

                    $transaction->commit();

                    $this->redirect(array('view', 'id' => $model->id));

                } catch (CException $e){

                    $transaction->rollback();

                    if(!empty($this->tempLang)){
                        Yii::app()->language = $this->tempLang;
                    }

                    if(isset($e->errorInfo[2])){
                        $errorText = $e->errorInfo[2];
                    } else {
                        $errorText = $e->getMessage();
                    }
                    Yii::app()->user->setFlash('danger', "Ошибка при копировании выставки. Код: {$errorText}");
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
            'fairInfo' => $FairInfo,
        ));
    }

    /**
     * @param $id
     * @throws CDbException
     * @throws CException
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN)
            Yii::app()->createUrl('/home');

        $model = $this->loadModel($id);
        $FairInfo = FairInfo::model()->findByPk($model->infoId);

        if($FairInfo === NULL)
            $FairInfo = new FairInfo;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Fair'])) {

            $model->attributes = $_POST['Fair'];
            $transaction = Yii::app()->db->beginTransaction();

            try{
                if($model->active == Fair::ACTIVE_ON && !empty($model->exhibitionComplexId) && ExhibitionComplexAdminService::geoRelationValidate($model->exhibitionComplexId) === FALSE)
                    throw new CHttpException(404, 'Нет связанных гео данных.');
                elseif($model->active == Fair::ACTIVE_ON && !empty($model->exhibitionComplexId)){
                    $exhibitionComplex = ExhibitionComplex::model()->findByPk($model->exhibitionComplexId);
                    if(!empty($exhibitionComplex) && $exhibitionComplex->active != ExhibitionComplex::ACTIVE_ON)
                        $exhibitionComplex->active = ExhibitionComplex::ACTIVE_ON;
                }

                $newShortUrl = $model->getSeoNameForNewVersion();
                $newTrFair = TrFair::model()->findByAttributes(['shortUrl' => $newShortUrl, 'langId' => Yii::app()->language]);

                if(!empty($newTrFair) && $newTrFair->trParentId != $model->id)
                    throw new CHttpException(404, "Параметры выставки: наименование, дата начала, выставочный центр уже соответствуют выставке id = {$newTrFair->trParentId}");

                if(isset($_POST['fairIsForum']) && $_POST['fairIsForum'] == Fair::IS_FORUM)
                    $FairInfo->forumId = $model->id;
                 elseif(!isset($_POST['fairIsForum']))
                    $FairInfo->forumId = NULL;

                if(isset($_POST['FairInfo']) && !empty(array_filter($_POST['FairInfo'])))
                    $FairInfo->attributes = $_POST['FairInfo'];

                if(!empty($FairInfo->id) && $FairInfo->id != $model->infoId)
                    $model->infoId = $FairInfo->id;

                $model->userRedactorId = Yii::app()->user->id;

                if(!empty($FairInfo->id) && $FairInfo->id != $model->infoId)
                    $model->infoId = $FairInfo->id;

                $model->save();
                $FairInfo->save();
                if(!empty($exhibitionComplex))
                    $exhibitionComplex->save();
                if(!empty($model->exhibitionComplexId))
                    ExhibitionComplex::checkWithDeactivation($model->exhibitionComplexId);

                if (
                    $model->validate()
                    && isset($_POST['Fair'][Fair::INDUSTRY_VARIABLE])
                    && !empty($_POST['Fair'][Fair::INDUSTRY_VARIABLE])
                ) {
                    if(is_string($_POST['Fair'][Fair::INDUSTRY_VARIABLE]))
                        $fairHasIndustries = explode(',',$_POST['Fair'][Fair::INDUSTRY_VARIABLE]);
                     elseif(is_array($_POST['Fair'][Fair::INDUSTRY_VARIABLE]))
                        $fairHasIndustries = $_POST['Fair'][Fair::INDUSTRY_VARIABLE];
                     else $fairHasIndustries = array();

                    FairHasIndustry::model()->deleteAllByAttributes(['fairId' => $model->id]);

                    if(!empty($fairHasIndustries))
                        foreach ($fairHasIndustries as $industryId)
                            if($industryId > 0){
                                $modelIndustry = new FairHasIndustry();
                                $modelIndustry->fairId = $model->id;
                                $modelIndustry->industryId = $industryId;
                                $modelIndustry->save();
                            }

                } elseif(isset($_POST['Fair'][Fair::INDUSTRY_VARIABLE]) && empty($_POST['Fair'][Fair::INDUSTRY_VARIABLE]))
                    FairHasIndustry::model()->deleteAllByAttributes(['fairId' => $model->id]);

                if (isset($_POST['Fair'][Fair::ORGANIZER_VARIABLE]) && !empty($_POST['Fair'][Fair::ORGANIZER_VARIABLE])) {

                    if(is_string($_POST['Fair'][Fair::ORGANIZER_VARIABLE]))
                        $fairHasOrganizers = explode(',',$_POST['Fair'][Fair::ORGANIZER_VARIABLE]);
                     elseif(is_array($_POST['Fair'][Fair::ORGANIZER_VARIABLE]))
                        $fairHasOrganizers = $_POST['Fair'][Fair::ORGANIZER_VARIABLE];
                     else $fairHasOrganizers = array();

                    FairHasOrganizer::model()->deleteAllByAttributes(['fairId' => $model->id]);

                    if(!empty($fairHasOrganizers))
                        foreach ($fairHasOrganizers as $organizerId)
                            if($organizerId > 0){
                                $modelOrganizer = new FairHasOrganizer;
                                $modelOrganizer->fairId = $model->id;
                                $modelOrganizer->organizerId = $organizerId;
                                $modelOrganizer->save();
                            }

                } elseif(isset($_POST['Fair'][Fair::ORGANIZER_VARIABLE]) && empty($_POST['Fair'][Fair::ORGANIZER_VARIABLE]))
                    FairHasOrganizer::model()->deleteAllByAttributes(['fairId' => $model->id]);

                if (isset($_POST['Fair'][Fair::AUDIT_VARIABLE]) && !empty($_POST['Fair'][Fair::AUDIT_VARIABLE])){

                    if(is_string($_POST['Fair'][Fair::AUDIT_VARIABLE]))
                        $fairHasAudits = explode(',',$_POST['Fair'][Fair::AUDIT_VARIABLE]);
                     elseif(is_array($_POST['Fair'][Fair::AUDIT_VARIABLE]))
                        $fairHasAudits = $_POST['Fair'][Fair::AUDIT_VARIABLE];
                     else $fairHasAudits = array();

                    FairHasAudit::model()->deleteAllByAttributes(['fairId' => $model->id]);

                    if(!empty($fairHasAudits))
                        foreach ($fairHasAudits as $auditId)
                            if($auditId > 0){
                                $modelAudit = new FairHasAudit;
                                $modelAudit->fairId = $model->id;
                                $modelAudit->auditId = $auditId;
                                $modelAudit->save();
                            }

                } elseif(isset($_POST['Fair'][Fair::AUDIT_VARIABLE]) && empty($_POST['Fair'][Fair::AUDIT_VARIABLE]))
                    FairHasAudit::model()->deleteAllByAttributes(['fairId' => $model->id]);

                if(isset($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]) && !empty($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE])){

                    $fairAssociationIDs = explode(',', $_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]);

                    if(is_array($fairAssociationIDs) && count($fairAssociationIDs) > 0){

                        FairHasAssociation::model()->deleteAllByAttributes(['fairId' => $model->id]);

                        foreach ($fairAssociationIDs as $fairAssociationID){

                            $fairAssociation = Association::model()->findByPk($fairAssociationID);

                            if($fairAssociation !== NULL){

                                $FHA = new FairHasAssociation;
                                $FHA->attributes = [
                                    'fairId' => $model->id,
                                    'associationId'  => $fairAssociationID,
                                ];

                                $FHA->save();
                            }
                        }
                    }
                } elseif(isset($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]) && empty($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]))
                    FairHasAssociation::model()->deleteAllByAttributes(['fairId' => $model->id]);

                /** Загрузка Логотипа */
                $logo = CUploadedFile::getInstancesByName('Fair[logoId]');
                if (isset($logo[0])) {
                    $objectFile = (new ObjectFile())->createFile($logo[0], $model, ObjectFile::TYPE_FAIR_LOGOS, ObjectFile::EXT_IMAGE);
                    if (null != $objectFile) {
                        $model->logoId = $objectFile->id;
                        $model->update('logoId');
                    }
                }

                /** Загрузка Логотипа для писем */
                $logo = CUploadedFile::getInstancesByName('Fair[mailLogoId]');
                if (isset($logo[0])) {
                    $objectFile = (new ObjectFile())->createFile($logo[0], $model, ObjectFile::TYPE_FAIR_MAIL_LOGOS, ObjectFile::EXT_IMAGE);
                    if (null != $objectFile) {
                        $model->mailLogoId = $objectFile->id;
                        $model->update('mailLogoId');
                    }
                }

                $transaction->commit();
                $this->redirect(array('view', 'id' => $model->id));

            } catch(CException $e) {

                $transaction->rollback();

                if(isset($e->errorInfo[2]))
                    $errorText = $e->errorInfo[2];
                 else $errorText = $e->getMessage();

                Yii::app()->user->setFlash('danger', "Ошибка при редактировании выставки. Код: {$errorText}");
            }
        }

        $this->render('update', array(
            'model' => $model,
            'fairInfo' => $FairInfo,
        ));
    }

    /**
     * @param $id
     * @throws CDbException
     * @throws CException
     * @throws CHttpException
     */
    public function actionCopy($id)
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN)
            Yii::app()->createUrl('/home');

        $model = $this->loadModel($id);

        $fairInfoModel = FairInfo::model()->findByPk($model->infoId);
        $fairInfoModel->unsetAttributes(
            array(
                'exhibitorsLocal',
                'exhibitorsForeign',
                'countriesCount',
                'areaClosedExhibitorsLocal',
                'areaClosedExhibitorsForeign',
                'areaOpenExhibitorsLocal',
                'areaOpenExhibitorsForeign',
                'areaSpecialExpositions',
                'visitorsLocal',
                'visitorsForeign',
            )
        );

        $model->unsetAttributes(
            array(
                'infoId',
            )
        );

        if(Yii::app()->language == Yii::app()->params->defaultLanguage){
            $model->setAttributes(
                array(
                    'uniqueText' => '',
                )
            );
        }

        if($model->rating != Fair::RATING_AUDIT)
            $model->rating = Fair::RATING_DEFAULT;

        if (isset($_POST['Fair'])) {

            $copy = new Fair;
            $copy->attributes = $_POST['Fair'];

            $this->tempLang = Yii::app()->language;
            $transaction = Yii::app()->db->beginTransaction();

            try {
                ZLogRouter::sentryLogRouteDisable();

                if($copy->active == 1 && ExhibitionComplexAdminService::geoRelationValidate($copy->exhibitionComplexId) === FALSE)
                    throw new CHttpException(404, 'Нет связанных гео данных.');
                else {
                    $exhibitionComplex = ExhibitionComplex::model()->findByPk($copy->exhibitionComplexId);
                    if(!empty($exhibitionComplex) && $exhibitionComplex->active != ExhibitionComplex::ACTIVE_ON)
                        $exhibitionComplex->active = ExhibitionComplex::ACTIVE_ON;
                }

                $newShortUrl = $copy->getSeoNameForNewVersion();
                $newTrFair = TrFair::model()->findByAttributes(['shortUrl' => $newShortUrl, 'langId' => Yii::app()->language]);

                if(!empty($newTrFair) && $newTrFair->trParentId != $copy->id)
                    throw new CHttpException(404, "Параметры выставки: наименование, дата начала, выставочный центр уже соответствуют выставке id = {$newTrFair->trParentId}");

                foreach (TrFair::model()->findAllByAttributes(['trParentId' => $model->id]) as $trFair){

                    if(Yii::app()->language != $trFair->langId){
                        Yii::app()->language = $trFair->langId;
                    }

                    $copy->name = $trFair->name;
                    $copy->description = $trFair->description;
                    if(Yii::app()->language == Yii::app()->params->defaultLanguage){
                        $copy->uniqueText = '';
                    } else {
                        $copy->uniqueText = $trFair->uniqueText;
                    }
                    $copy->statistics = $trFair->statistics;
                    $copy->save();
                }

                if(!empty($exhibitionComplex))
                    $exhibitionComplex->save();

                $transaction->commit();

                if(isset($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]) && empty($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE])){
                    FairHasAssociation::model()->deleteAllByAttributes(['fairId' => $copy->id]);
                } elseif(isset($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]) && !empty($_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE])){

                    $fairAssociationIDs = explode(',', $_POST[get_class($model)][Fair::ASSOCIATION_VARIABLE]);

                    if(is_array($fairAssociationIDs) && count($fairAssociationIDs) > 0){
                        FairHasAssociation::model()->deleteAllByAttributes(['fairId' => $copy->id]);

                        foreach ($fairAssociationIDs as $fairAssociationID){
                            $FHA = new FairHasAssociation;
                            $FHA->attributes = [
                                'fairId' => $copy->id,
                                'associationId'  => $fairAssociationID,
                            ];
                            $FHA->save();
                        }
                    }
                }

                if(isset($_POST[get_class($model)][Fair::AUDIT_VARIABLE]) && empty($_POST[get_class($model)][Fair::AUDIT_VARIABLE])){
                    FairHasAudit::model()->deleteAllByAttributes(['fairId' => $copy->id]);
                } elseif(isset($_POST[get_class($model)][Fair::AUDIT_VARIABLE]) && !empty($_POST[get_class($model)][Fair::AUDIT_VARIABLE])){

                    $fairAuditIDs = explode(',', $_POST[get_class($model)][Fair::AUDIT_VARIABLE]);

                    if(is_array($fairAuditIDs) && count($fairAuditIDs) > 0){
                        FairHasAudit::model()->deleteAllByAttributes(['fairId' => $copy->id]);

                        foreach ($fairAuditIDs as $fairAuditID){
                            $FHAU = new FairHasAudit;
                            $FHAU->attributes = [
                                'fairId' => $copy->id,
                                'auditId'  => $fairAuditID,
                            ];
                            $FHAU->save();
                        }
                    }
                }

                $FairInfo = new FairInfo;

                if(isset($_POST['fairIsForum']) && $_POST['fairIsForum'] == 1)
                    $FairInfo->forumId = $copy->id;

                if(isset($_POST['FairInfo']) && !empty(array_filter($_POST['FairInfo'])))
                    $FairInfo->attributes = $_POST['FairInfo'];

                $FairInfo->save();

                if(!empty($FairInfo->id))
                    $copy->saveAttributes(['infoId' => $FairInfo->id]);

                if (isset($_POST['Fair'][Fair::INDUSTRY_VARIABLE]) && !empty($_POST['Fair'][Fair::INDUSTRY_VARIABLE])){

                    $fairHasIndustries = explode(',',$_POST['Fair'][Fair::INDUSTRY_VARIABLE]);

                    FairHasIndustry::model()->deleteAllByAttributes(['fairId' => $copy->id]);
                    foreach ($fairHasIndustries as $industryId) {
                        $modelIndustry = new FairHasIndustry();
                        $modelIndustry->fairId = $copy->id;
                        $modelIndustry->industryId = $industryId;
                        $modelIndustry->save();
                    }
                }

                if (isset($_POST['Fair'][Fair::ORGANIZER_VARIABLE]) && !empty($_POST['Fair'][Fair::ORGANIZER_VARIABLE])) {
                    
                    if(is_string($_POST['Fair'][Fair::ORGANIZER_VARIABLE]))
                        $fairHasOrganizers = explode(',',$_POST['Fair'][Fair::ORGANIZER_VARIABLE]);
                     elseif(is_array($_POST['Fair'][Fair::ORGANIZER_VARIABLE]))
                        $fairHasOrganizers = $_POST['Fair'][Fair::ORGANIZER_VARIABLE];
                     else $fairHasOrganizers = array();

                    FairHasOrganizer::model()->deleteAllByAttributes(['fairId' => $copy->id]);
                    foreach ($fairHasOrganizers as $organzierId) {
                        $modelOrganizer = new FairHasOrganizer;
                        $modelOrganizer->fairId = $copy->id;
                        $modelOrganizer->organizerId = $organzierId;
                        $modelOrganizer->save();
                    }
                }

                $copy->saveAttributes(array('userRedactorId' => Yii::app()->user->id));

                /** Загрузка Логотипа */
                $logo = CUploadedFile::getInstancesByName('Fair[logoId]');
                if (isset($logo[0])) {
                    $objectFile = (new ObjectFile())->createFile($logo[0], $copy, ObjectFile::TYPE_FAIR_LOGOS, ObjectFile::EXT_IMAGE);
                    if (null != $objectFile) {
                        $copy->logoId = $objectFile->id;
                        $copy->update('logoId');
                    }
                }

                /** Загрузка Логотипа для писем */
                $logo = CUploadedFile::getInstancesByName('Fair[mailLogoId]');
                if (isset($logo[0])) {
                    $objectFile = (new ObjectFile())->createFile($logo[0], $model, ObjectFile::TYPE_FAIR_MAIL_LOGOS, ObjectFile::EXT_IMAGE);
                    if (null != $objectFile) {
                        $model->mailLogoId = $objectFile->id;
                        $model->update('mailLogoId');
                    }
                }

                if(!empty($this->tempLang))
                    Yii::app()->language = $this->tempLang;

                if(ZLogRouter::getSentryLogRouteEnabled() == FALSE)
                    ZLogRouter::sentryLogRouteEnable();

                $this->redirect(array('view', 'id' => $copy->id));

            } catch (CException $e){

                $transaction->rollback();
                $model->attributes = $_POST['Fair'];
                $fairInfoModel->attributes = $_POST['FairInfo'];

                if(!empty($this->tempLang))
                    Yii::app()->language = $this->tempLang;

                if(isset($e->errorInfo[2]))
                    $errorText = $e->errorInfo[2];
                 else $errorText = $e->getMessage();

                Yii::app()->user->setFlash('danger', "Ошибка при копировании выставки. Код: {$errorText}");
            }
        }

        $this->render('copy', array(
            'model' => $model,
            'fairInfo' => $fairInfoModel,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        $model = $this->loadModel($id);
        $model->active = User::ACTIVE_OFF;
        $model->save(false);
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * @param $fairId
     */
    public function actionScdTrFair($fairId){

        if (Yii::app()->user->role != User::ROLE_ADMIN) {
            Yii::app()->createUrl('/home');
        }

        $model = new ScdTrFair('search');
        $dataProvider = $model->searchData($fairId);

        if(isset($_GET['ScdTrFair'])){

            $fields = $_GET['ScdTrFair'];
            $model->attributes = $_GET['ScdTrFair'];
            $dataProvider = $model->searchData($fairId, $fields);
        }

        $this->render('scdTrFair', array(
            'dataProvider' => $dataProvider,
            'model' => $model,
            'fairId' => $fairId,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (Yii::app()->user->role != User::ROLE_ADMIN)
            $this->redirect(Yii::app()->createUrl('/home'));

        $pageVariants = array_combine(self::$pageVariants, self::$pageVariants);

        if (isset($_GET['Fair_pageSize'])) {
            Yii::app()->user->setState('Fair_pageSize', (int)$_GET['Fair_pageSize'] ?: reset($pageVariants));
        }

        $fairFieldsDefaults = [
            'fairStatus',
            'active',
            'exdbId',
            'exdbFrequency',
            'exdbBusinessSectors',
            'exdbCosts',
            'exdbShowType',
            'exdbFirstYearShow',
            'canceled',
            'lastYearFair',
            'duration',
            'frequency',
            'fairIsForum',
            'forumId',
            'exhibitionComplexId',
            'organizerId',
            'organizerName',
            'organizerCompanyCount',
            'organizerContactCount',
            'site',
            'beginDate',
            'endDate',
            'organizerEmail',
            'industryName',
            'visitors',
            'members',
            'squareGross',
            'squareNet',
            'participationPrice',
            'registrationFee',
            'datetimeModified',
            'statistics',
            'rating',
            'exhibitorsLocal',
            'exhibitorsForeign',
            'countriesCount',
            'areaClosedExhibitorsLocal',
            'areaClosedExhibitorsForeign',
            'areaOpenExhibitorsLocal',
            'areaOpenExhibitorsForeign',
            'areaSpecialExpositions',
            'visitorsLocal',
            'visitorsForeign',
            'associationName',
            'auditName',
            'storyId',
            'lang',
            'fairTranslateName',
            'exhibitionComplexName',
            'cityName',
            'statisticsDate',
            'regionName',
            'districtName',
            'countryName',
            'officialComment',
            'commonStatisticFair',
            'organizerCompanyId',
            'fairEnName',
            'fairDeName',
            'fairDiscription',
            'fairUniqueText',
            'userRedactorEmail',
            'logoId',
            'logoExists',
        ];

        $fairFieldsArr = [];

        if (isset($_GET['Fair_fields'])) {

            if(is_array($_GET['Fair_fields']) && !empty($_GET['Fair_fields']))
                $getFairFields = $_GET['Fair_fields'];
             elseif (is_string($_GET['Fair_fields']))
                $getFairFields = explode(',', $_GET['Fair_fields']);
             else $getFairFields = array();

            foreach ($getFairFields as $getFairField)
                if (in_array($getFairField, $fairFieldsDefaults))
                    $fairFieldsArr[] = $getFairField;

            Yii::app()->user->setState('Fair_fields', implode(',', $fairFieldsArr));
        }

        $model = new Fair('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Fair']))
            $model->attributes = $_GET['Fair'];

        $sortColumn = NULL;

        if(isset($_GET['Fair_sort']) && !empty($_GET['Fair_sort']))
            $sortColumn = $_GET['Fair_sort'];

        if(isset($_GET['beginDateRange']) && !empty($_GET['beginDateRange']))
            $model->beginDateRange = $_GET['beginDateRange'];

        if(isset($_GET['endDateRange']) && !empty($_GET['endDateRange']))
            $model->endDateRange = $_GET['endDateRange'];

        if(!empty($_GET['datetimeModifiedRange']))
            $model->datetimeModifiedRange = $_GET['datetimeModifiedRange'];

        $fields = Yii::app()->user->getState('Fair_fields');
        $fairFields = [];

        if (!empty($fields))
            $fairFields = explode(',', $fields);

        $pageSize = Yii::app()->user->getState('Fair_pageSize', reset($pageVariants));
        $search = FairAdminService::search($fairFields, $pageSize, $sortColumn, $model);

        $this->render('admin', array(
            'fairFields'   => $fairFields,
            'search'       => $search,
            'pageVariants' => $pageVariants,
            'model'        => $model
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Fair the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Fair::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadFairInfoModel($id)
    {
        $model = FairInfo::model()->findByAttributes(['fairId' => $id]);
        if ($model === null)
            $model = new FairInfo;
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Fair $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'fair-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param $fairId
     */
    public function actionGetRelatedStatisticFairs($fairId){

        echo json_encode(FairAdminService::getRelatedStatisticFairs($fairId));
    }

    /**
     * @param $fairId
     */
    public function actionGetStatisticFairs($fairId){

        echo json_encode(FairAdminService::getRelatedStatisticFairs($fairId, FALSE));
    }

    /**
     * @param $fairId
     */
    public function actionGetFairStatistic($fairId){

        $sql = "SELECT fi.* FROM tbl_fairinfo fi
         LEFT JOIN tbl_fair f ON f.infoId = fi.id
         WHERE f.id = :fairId";
        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':fairId' => $fairId));

        echo json_encode($res);
    }

    /**
     * @throws CDbException
     */
    public function actionSeparateRelatedStatisticFair(){

        if(!isset($_POST['fairId'])){
            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => 'post didn\'t find',
                )
            );
            return;
        }

        $fairIds = $_POST['fairId'];
        $result = array();

        if(!is_array($fairIds)){
            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => 'Type error.',
                )
            );
            return;
        }

        foreach ($fairIds as $fairId){

            $fair = Fair::model()->findByPk($fairId);
            if($fair === NULL){
                $result[] = array(
                    'success' => FALSE,
                    'message' => 'fair didn\'t find',
                    'fairId' => NULL
                );
                continue;
            }
            $oldFairInfo = FairInfo::model()->findByPk($fair->infoId);
            $newFairInfo = new FairInfo;
            $newFairInfo->attributes = $oldFairInfo->attributes;

            if(!$newFairInfo->save()){
                $result[] = array(
                    'success' => FALSE,
                    'message' => 'new FairInfo didn\'t save',
                );
                continue;
            }

            $fair->saveAttributes(array('infoId' => $newFairInfo->id));
            $result[] = array(
                'success' => TRUE,
                'message' => 'new FairInfo saved',
                'fairId' => $fairId
            );
        }
        echo json_encode($result);
    }

    public function actionAttachFairStatistic(){

        if(!isset($_POST['fairId']) || !isset($_POST['infoId']) || empty($_POST['fairId']) || empty($_POST['infoId'])){
            echo json_encode(
                array(
                    'success' => FALSE,
                    'message' => 'post didn\'t find',
                )
            );
            return;
        }

        $fairIds = $_POST['fairId'];
        $infoId = $_POST['infoId'];

        foreach ($fairIds as $fairId) {

            $transaction = Yii::app()->db->beginTransaction();

            try {
                FairAdminService::saveInfoId($fairId, $infoId);
            } catch (CException $e) {
                $transaction->rollback();
                echo json_encode([
                    'status' => 'fail',
                    'message' => $e->getMessage(),
                ]);
                return;
            }
            $transaction->commit();
        }

        echo json_encode([
            'status' => 'ok',
            'message' => 'fair attached',
        ]);
    }

    /**
     * @param $q
     * @return array
     */
    public static function getFairs($q){

        $sql = "SELECT f.id, trf.name, YEAR(f.beginDate) AS yearDate, f.infoId FROM tbl_fair f
                LEFT JOIN tbl_trfair trf ON f.id = trf.trParentId
                WHERE trf.langId = :langId
                AND (trf.name LIKE :q OR f.id LIKE :q)
                ";

        $data = Yii::app()->db->createCommand($sql)->query([
            ':q' => "%$q%",
            ':langId' => Yii::app()->language,
        ]);

        return $data->readAll();
    }

    /**
     * @param $q
     */
    public function actionGetFairs($q){

        echo json_encode(self::getFairs($q));
    }

    /**
     * @param $q
     */
    public function actionGetIndustries($q){

        $criteria = new CDbCriteria;
        $criteria->with['translate'] = [
            'together' => TRUE,
        ];
        $criteria->compare('translate.name', $q, true,'OR');
        $criteria->compare('t.id', $q, true,'OR');

        $industries = CHtml::listData(
            Industry::model()->findAll($criteria),
            'id',
            function($el){
                return $el;
            });

        $items = array_map(
            function($industry_id, $industry){
                return array(
                    'id' => $industry_id,
                    'name' => $industry->name,
                );
            },
            array_keys($industries),
            array_values($industries)
        );

        echo json_encode($items);
    }

    public function actionGetAudits($q){
        $query = "SELECT DISTINCT trau.trParentId AS id, trau.name FROM tbl_traudit trau
                  WHERE trau.langId = :langId
                  AND trau.trParentId LIKE :q OR trau.name LIKE :q
        ";
        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':q' => "%$q%", ':langId' => Yii::app()->getLanguage()));
        echo json_encode($data);
    }

    /**
     * @param $q
     */
    public function actionGetOrganizers($q){

        $sql = "SELECT t.id, t.name, t.contact FROM 
                    (
                    SELECT o.id, trorg.name, GROUP_CONCAT(orgcontlist.value) AS contact FROM tbl_organizer o 
                        LEFT JOIN tbl_trorganizercompany trorg ON trorg.trParentId = o.companyId AND trorg.langId = :langId
                        LEFT JOIN tbl_organizercontactlist orgcontlist ON orgcontlist.organizerId = o.id AND (orgcontlist.type = :emailType OR orgcontlist.type = :phoneType)
                    GROUP BY o.id
                    ) t
                WHERE t.name LIKE :q 
                OR t.id LIKE :q 
                OR t.contact LIKE :q
        ";

        $data = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':q' => "%$q%",
            ':langId' => Yii::app()->language,
            ':emailType' => OrganizerContactList::EMAIL_TYPE,
            ':phoneType' => OrganizerContactList::PHONE_TYPE,
        ]);

        echo json_encode($data);
    }

    /**
     * @param $q
     */
    public function actionGetExhibitionComplexes($q){

        $criteria = new CDbCriteria;
        $criteria->with['translate'] = [
            'together' => TRUE,
            'select' => 'name',
        ];
        $criteria->compare('t.id', $q, true,'OR');
        $criteria->compare('translate.name', $q, true,'OR');

        $exhibitionComplexes = CHtml::listData(
            ExhibitionComplex::model()->findAll($criteria),
            'id',
            function($el){
                return $el;
            });

        $items = array_map(
            function($exhibitionComplex_id, $exhibitionComplex){
                return array(
                    'id' => $exhibitionComplex_id,
                    'name' => $exhibitionComplex->name,
                );
            },
            array_keys($exhibitionComplexes),
            array_values($exhibitionComplexes)
        );

        echo json_encode($items);
    }

    /**
     * @param $q
     */
    public function actionGetAssociations($q){

        $criteria = new CDbCriteria;
        $criteria->compare('t.name', $q, true,'OR');
        $criteria->compare('t.id', $q, true,'OR');

        $association = CHtml::listData(
            Association::model()->findAll($criteria),
            'id',
            'name');

        $items = array_map(
            function($association_id, $association_name){
                return array(
                    'id' => $association_id,
                    'name' => $association_name
                );
            },
            array_keys($association),
            array_values($association)
        );

        echo json_encode($items);
    }

    /**
     * @param $fairAssociations
     * @return string
     */
    public static function getFairAssociationInString ($fairAssociations){

        $result = '';
        $countFairAssociations = count($fairAssociations);

        for ($i=0; $i < $countFairAssociations; $i++){
            if($i == $countFairAssociations - 1) {
                $result .= $fairAssociations[$i]['name'];
            } else {
                $result .= $fairAssociations[$i]['name'] . ';';
            }
        }

        return $result;

    }

}
