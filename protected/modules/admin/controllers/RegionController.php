<?php

class RegionController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'getMassMedias' action
                'actions' => array('admin', 'getMassMedias'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'getMassMedias' action
                'actions' => array('admin', 'manageSponsorPosition'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'getMassMedias' action
                'actions' => array('admin', 'saveSponsorPosition'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('allow', // allow admin user to perform 'getMassMedias' action
                'actions' => array('admin', 'toggleGeneral'),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Region;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Region'])) {
            $model->attributes = $_POST['Region'];
            if ($model->save()){
                $trModel = TrRegion::model()->findByAttributes(['trParentId' => $model->id, 'langId' => Yii::app()->language]);
                $trModel->setAttribute('name', $_POST['Region']['name']);

                if(!$trModel->save()){
                    $trModel->getErrors();
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $trModel = TrRegion::model()->findByAttributes(['trParentId' => $model->id, 'langId' => Yii::app()->language]);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Region'])) {
            $model->attributes = $_POST['Region'];
            $trModel->setAttribute('name', $_POST['Region']['name']);

            if(empty($_POST[get_class($model)][Region::SMI_VARIABLE])){
                RegionHasMassMedia::model()->deleteAllByAttributes(['regionId' => $model->id]);
            }
            elseif(!empty($_POST[get_class($model)][Region::SMI_VARIABLE])){

                $regionMassMediaIDs = explode(',', $_POST[get_class($model)][Region::SMI_VARIABLE]);
                RegionHasMassMedia::model()->deleteAllByAttributes(['regionId' => $model->id]);

                foreach ($regionMassMediaIDs as $regionMassMediaId){
                    $RHM = new RegionHasMassMedia;
                    $RHM->attributes = [
                        'regionId' => $model->id,
                        'massMediaId'  => $regionMassMediaId,
                    ];
                    if(!$RHM->save()){
                        throw new CException($RHM->getErrors());
                    }
                }
            }

            if ($model->save())
                if(!$trModel->save()){
                    $trModel->getErrors();
                }
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(Yii::app()->createUrl('/admin/region/admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Region('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Region']))
            $model->attributes = $_GET['Region'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Region the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Region::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Region $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'region-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param $id
     */
    public function actionManageSponsorPosition($id){
        if(isset($id) && !empty($id) && is_numeric($id)){
            $model = Region::model()->findByPk($id);

            $sponsorPos = null;
            if(isset($model->sponsorPos) && !empty($model->sponsorPos)){
                $sponsorPos = $model->sponsorPos;
            }

            if(!empty($model)){
                $this->render('manageSponsorPosition', array(
                    'model' => $model,
                    'sponsorPos' => $sponsorPos,
                ));
            }
        }
    }
    
    public function actionSaveSponsorPosition(){
        if(Yii::app()->request->isAjaxRequest){
            if(isset($_POST['model_id']) && !empty($_POST['model_id'])){
                $id = $_POST['model_id'];
                if(isset($_POST['data']) && !empty($_POST['data'])){
                    $sponsorPos = $_POST['data'];
                    $model = Region::model()->findByPk($id);
                    if(!empty($model)){
                        $model->sponsorPos = $sponsorPos;

                        if($model->save()){
                            echo json_encode(array('success' => '1'));
                        }
                    }
                }else{
                    echo json_encode('empty data array');
                }
            }else{
                echo json_encode('empty model id');
            }
        }
    }
    
    public function actionToggleGeneral(){
        if(Yii::app()->request->isAjaxRequest){
            if(isset($_POST['model_id']) && !empty($_POST['model_id'])){
                $id = $_POST['model_id'];
                if(isset($_POST['isGeneral']) && !empty($_POST['isGeneral'])){
                    $isGeneral = $_POST['isGeneral'];
                    $model = RegionHasMassMedia::model()->findByPk($id);
                    if(!empty($model)){
                        $model->isGeneral = $isGeneral;
                        if($model->save()){
                            echo json_encode(array('success' => '1'));
                        }

                    }
                }else{
                    echo json_encode('empty data array');
                }
            }else{
                echo json_encode('empty model id');
            }
        }
    }

    /**
     * @param $q
     */
    public function actionGetMassMedias($q){

        $criteria = new CDbCriteria;
        $criteria->compare('t.name', $q, true,'OR');
        $criteria->compare('t.id', $q, true,'OR');

        $smi = CHtml::listData(
            MassMedia::model()->findAll($criteria),
            'id',
            function($el){
                return $el;
            });

        $items = array_map(
            function($region_id, $region){
                return array(
                    'id' => $region_id,
                    'name' => $region->name,
                );
            },
            array_keys($smi),
            array_values($smi)
        );

        echo json_encode($items);
        
    }
    
}
