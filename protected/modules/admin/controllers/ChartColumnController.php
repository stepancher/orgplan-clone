<?php

class ChartColumnController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';
    public $model = 'ChartColumn';

    /**
     * @return array action filters
     */
    public function filters(){
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($id = NULL){

        if($id != NULL && is_numeric($id)){
            $chartTable = ChartTable::model()->findByPk($id);

            if ($chartTable === null){
                throw new CHttpException(404, 'The requested page does not exist.');
            }

            $model = new ChartColumn('search');
            $model->tableId = $chartTable->id;

        }else{
            $model = ChartColumn::model();
        }

        $this->render('index', array(
            'model' => $model
        ));
    }

    public function actionDelete($id)
    {
        $className = get_called_class();
        $className = str_replace('Controller', '', $className);
        /** @var AR $model */
        if (($model = AR::model($className)->findByPk($id)) !== null) {
            if ($model->delete()) {
                $this->redirect(Yii::app()->createUrl(lcfirst($className) . '/index'));
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ExhibitionComplex the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ChartColumn::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionSave($id = null)
    {
        /** @var AR $model */
        if (null === $id || ($model = AR::model($this->model)->findByPk($id)) === null)
            $model = new $this->model();

        $ctrl = $this;
        $records = AR::multiSave(
            $_POST,
            array(
                array(
                    'name' => 'model',
                    'class' => get_class($model),
                    'record' => $model,
                ),
                function ($records) use ($ctrl) {

                    /** @var MassMedia $massMedia */
                    $chartColumn = $records['model'];

                    if (isset($chartColumn) && !empty($chartColumn)){
                        $chartColumn->attributes = $records['model'];
                    }

                    Yii::app()->user->setFlash('success', Yii::t('AdminModule.admin', 'The information is saved.'));

                    $ctrl->redirect($ctrl->createUrl('index', array('id' => $records['model']->tableId)));
                }
            )
        );

        //@TODO refactor (Overload standart widget css file)
        Yii::app()->clientScript->registerCss('colorpicker-css', '
            div.colorPicker-picker{
                height: 14px;
                width: 114px;
                padding: 0 !important;
                border: 1px solid #ccc;
                /* background: url(arrow.gif) no-repeat top right; */
                cursor: pointer;
                line-height: 16px;
                margin-top: 21px;
            }
        ');

        $this->render('//ar/_form', $records);
    }

    public function actionView($id)
    {
        /** @var AR $model */
        if (($model = AR::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }

        $this->render('view', array(
            'model' => $model,
        ));
    }
}
