<?php
///* @var $form CActiveForm */
$app = Yii::app();
$request = $app->getRequest();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/gridster/dist/jquery.gridster.min.js', CClientScript::POS_END);
$cs->registerCssFile(
    $am->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/css/gridster/style.css')
);
?>
<script type="text/javascript">
    $(function(){ //DOM Ready
        var gridster;
        var model_id = <?php echo $model->id;?>;
        $(function(){
            gridster = $(".gridster ul").gridster({
                widget_base_dimensions: [140, 100],
                widget_margins: [13, 13],
                draggable: {
                    stop: function(e, ui, $widget) {
                        var data = gridster.serialize();
                        $.ajax({
                            url: '/ru/admin/region/saveSponsorPosition/',
                            type: 'post',
                            dataType: 'json',
                            data: {data:JSON.stringify(data), model_id:model_id},
                            success:function(data){

                            }
                        });
                    }
                }
            }).data('gridster');
        });
    });
</script>
<?php
    echo TbHtml::linkButton('Назад', array(
        'class' => 'btn pull-right btn-warning',
        'style' => 'margin:30px 50px 0px 0px;',
        'url' => $this->createUrl('update', array('id' => $model->id)),
    ));
?>
<div class="gridster">
    <ul>
        <?php
            $row = 1; //дефолтная строка (Нужна в случае если у региона отсутствуют сохранённые позиции спонсоров ($model->sponsorPos))
            $col= 0;//дефолтная колонка (Нужна в случае если у региона отсутствуют сохранённые позиции спонсоров ($model->sponsorPos))
            $currentSponsor = 0;//Для переборки сохранённого массива позиций спонсоров
            foreach($model->regionHasMassMedia as $regionHasMassMedia){
                $col++;
                if($col >= 6){
                    $col = 1;
                    $row++;
                }
                if(!empty($sponsorPos)){
                    $positionArr = json_decode($sponsorPos);

                    $col = !empty($positionArr[$currentSponsor])?$positionArr[$currentSponsor]->col:$col;
                    $row = !empty($positionArr[$currentSponsor])?$positionArr[$currentSponsor]->row:$row;

                    $currentSponsor++;
                }
                $sizex = 1;
                $sizey = 1;
                $isGeneral = $regionHasMassMedia->isGeneral;
                if(isset($isGeneral) && !empty($isGeneral) && ($isGeneral == 2)){
                    $sizex = 2;
                    $sizey = 2;
                }
                
                $ajaxButton = '';

                if(!empty($regionHasMassMedia)){
                    $ajaxButton = TbHtml::ajaxButton(
                        Yii::t('AdminModule.admin', ($isGeneral==2)?'Обычный':'Генеральный'),
                        $this->createUrl('toggleGeneral'),
                        array(
                            'dataType'=>'json',
                            'type' => 'post',
                            'data' => array(
                                    'model_id' => $regionHasMassMedia->id,
                                    'isGeneral' => ($isGeneral==2)?1:2,
                            ),
                            'success' => 'function(res){
                                if(res.success){
                                    location.reload();
                                }
                            }',
                        ),
                        array(
                            'color' => ($isGeneral==2) ? TbHtml::BUTTON_COLOR_WARNING : TbHtml::BUTTON_COLOR_SUCCESS,
                            'size' => TbHtml::BUTTON_SIZE_MINI,
                        )
                    );
                }

                echo H::wrapGridsterElement(
                    TbHtml::image(
                        H::getImageUrl($regionHasMassMedia->massMedia->getMainImage(), 'file', 'noImage', $isGeneral==2?'300-220_':'140-100_'),
                        $regionHasMassMedia->massMedia->name,
                        array(
                            'title' => $regionHasMassMedia->massMedia->name,
                        )
                    ).$ajaxButton,$row, $col, $sizex, $sizey
                );
            }
        ?>
    </ul>
</div>
