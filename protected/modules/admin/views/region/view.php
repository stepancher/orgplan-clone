<?php
/* @var $this RegionController */
/* @var $model Region */

?>

<h1>Промотр Региона #<?php echo $model->id; ?></h1>
<?=CHtml::link('Список Регионов', Yii::app()->createUrl('/admin/region/admin'), ['class' => 'btn btn-success'])?>
<br/>
<br/>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		[
			'label' => 'Округ',
			'name' => 'district.name'
		],
		'name',
		'shortUrl',
	),
)); ?>
