<?php
use yii\helpers\ArrayHelper;
/* @var $this RegionController */
/* @var $model Region */
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/region/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

	<div class="cols-row" xmlns:float="http://www.w3.org/1999/xhtml">
		<div class="col-md-12">

			<?php

			$attribute = 'districtId';
			$name = TbHtml::resolveName($model, $attribute);

			if (substr($attribute, -1) == ']') {
				$attribute = preg_replace('#\[.*\]#', '', $attribute);
			}

			echo $this->widget('application.modules.admin.components.ListAutoComplete',
				ArrayHelper::merge(
					array(
						'name' => $name,
						'attribute' => $attribute,
					),
					array(
					'model' => $model,
					'selectedItems' => array(
						$model->districtId
					),
					'multiple' => FALSE,
					'htmlOptions' => array(
						'labelOptions' => array(
							'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
						),
						'data-input-id' => $attribute,
					),
					'itemOptions' => array(
						'data' => array('' => 'Не выбрано') + CHtml::listData(TrDistrict::model()->findAllByAttributes(['langId' => Yii::app()->language]), 'trParentId', 'name'),
					),
					'label' => NULL,
					)
				),
			true);
			?>

			<div class="form-group field-region-name">
				<label class="control-label" for="Region_name"><?= Yii::t('AdminModule.admin', 'Name') ?></label>
				<input type="text" id="Region_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Region[name]" value="<?= $model->name ?>">
				<div class="help-block"></div>
			</div>

			<div class="form-group field-region-shorturl">
				<label class="control-label" for="Region_shortUrl"><?= Yii::t('AdminModule.admin', 'Short url') ?></label>
				<input type="text" id="Region_shortUrl" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Region[shortUrl]" value="<?= $model->shortUrl ?>">
				<div class="help-block"></div>
			</div>

			<div style="margin:10px 0px;float:left;width:100%;font-weight: bold;>
                <span style="float:left;width:20px;margin-left:20px"> <?= Yii::t('AdminModule.admin','Mass media') ?>:</span>
			<?php
			$this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
			'name' => ucfirst(get_class($model))."[".Region::SMI_VARIABLE."]",
			'value' => json_encode($model->getMassMediaByRegionId($model->id)),
			'options' => array(
			'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
			'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
			'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
			'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
			'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
			'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
			'width' => '100%',
			'placeholder' => "",
			'multiple' => true,
			'minimumInputLength' => 1,
			'ajax' => array(
			'url' => $this->createUrl('getMassMedias'),
			'dataType' => 'json',
			'data' => 'js:function (term, page) { return { q: term}; }',
			'results' => 'js:function(data,page){return {results: data};}',

			),
			'formatResult'    => 'js:function(data){
			return data.id + " " + data.name;
			}',
			'formatSelection' => 'js: function(data) {
			return data.id + " " + data.name;
			}',
			'initSelection' => 'js:function (element, callback) {
			// callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
			var items = JSON.parse(element.val());
			element.val("");
			callback(items);
			}',
			),
			'htmlOptions' => array(
			'class' => '',
			'style' => 'border:none;margin-top:10px;',
			),
			));
			?>
			</div>

		<?= TbHtml::linkButton(
			'Редактировать расположение СМИ',
			array(
			'url' => $this->createUrl('manageSponsorPosition', array('id' => $model->id)),
			'style' => 'margin-bottom:30px;'
			));
		?>

		</div>
	</div>
	<div class="cols-row">
		<div class="col-md-12">
			<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Save') ?>
			</button>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('admin') ?>"
			   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Cancel') ?>
			</a>
		</div>
	</div>
</form>