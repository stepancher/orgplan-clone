<?php
/* @var $this RegionController */
/* @var $data Region */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('districtId')); ?>:</b>
	<?php echo isset($data->district) ? CHtml::encode($data->district->name) : Yii::t('AdminModule.admin', 'Not set'); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shortUrl')); ?>:</b>
	<?php echo CHtml::encode($data->shortUrl); ?>
	<br />


</div>