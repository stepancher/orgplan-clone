<?php
/* @var $this RegionController */
/* @var $model Region */
?>

<h1>Управление Регионами</h1>
<?= CHtml::link('Добавить Регион', Yii::app()->createUrl('/admin/region/create'), ['class' => 'btn btn-success']); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'region-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'district_name',
        'name',
        'shortUrl',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
    'ajaxUpdate'   => false,
)); ?>
