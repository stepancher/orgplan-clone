<?php
/**
 *
 */
?>

<h1>Редактирование шаблона письма</h1>

<form method="post">
    <div class="control-group">
        <div class="controls">
            <input class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark"
                   placeholder="Id тестового пользователя" name="userId" type="text" maxlength="20" value="<?=Yii::app()->request->getParam('userId', Yii::app()->user->id)?>">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <input class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark"
                   placeholder="Тестовая дата (dd.mm.YYYY)" name="date" type="text" maxlength="20" value="<?=Yii::app()->request->getParam('date', date('d.m.Y'))?>">
        </div>
    </div>
    <textarea class="b-form__text input-block-level" name="content" style="height: 500px"><?= $content ?></textarea>
    <input type="submit"/>
</form>
<?=$result?>