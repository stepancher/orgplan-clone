<?php
/* @var $this ExhibitionComplexController */
/* @var $model ExhibitionComplex */

$this->menu=array(
	array('label'=>'List ExhibitionComplex', 'url'=>array('index')),
	array('label'=>'Create ExhibitionComplex', 'url'=>array('create')),
	array('label'=>'View ExhibitionComplex', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ExhibitionComplex', 'url'=>array('admin')),
);
?>
<h1>Редактирование ВЦ #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>