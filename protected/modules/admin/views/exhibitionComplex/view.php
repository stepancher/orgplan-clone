<?php
/* @var $this ExhibitionComplexController */
/* @var $model ExhibitionComplex */

$this->menu=array(
	array('label'=>'List ExhibitionComplex', 'url'=>array('index')),
	array('label'=>'Create ExhibitionComplex', 'url'=>array('create')),
	array('label'=>'Update ExhibitionComplex', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ExhibitionComplex', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ExhibitionComplex', 'url'=>array('admin')),
);
?>

<h1>Просмотр ВЦ #<?php echo $model->id; ?></h1>

<?= TbHtml::linkButton('  ' .
	'Редактировать', array(
	'class' => 'btn btn-warning',
	'style' => 'margin-top:10px;',
	'url' => $this->createUrl('update', array('id' => $model->id))
));
?>

<?= TbHtml::linkButton('  ' .
	'Список', array(
	'class' => 'btn btn-success',
	'style' => 'margin-top:10px;',
	'url' => $this->createUrl('admin')
));
?>

<?= TbHtml::linkButton('  ' .
	'Просмотр на сайте', array(
	'class' => 'btn d-bg-primary-dark',
	'style' => 'margin-top:10px;',
	'url' => Yii::app()->createUrl('venue') . DIRECTORY_SEPARATOR . (isset($model->translate) ? $model->translate->shortUrl : '')
));
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'street',
		'description',
		'descriptionSnippet',
        [
            'name' => 'cityId',
            'value' => function($data){
		        if(isset($data->city))
		            return $data->city->name;
                return $data->cityId;
            },
        ],
        [
            'name' => 'regionId',
            'value' => function($data){
		        if(isset($data->city->region))
		            return $data->city->region->name;
                return $data->regionId;
            },
        ],
		'coordinates',
		'active',
//		'userId',
        [
            'name' => 'lastRedactorEmail',
            'type' => 'raw',
            'value' => function($data){
		        $res = '';
		        if(!empty($data->user->contact->email)){
		            $res = $data->user->contact->email;
                }

                return $res;
            },
        ],
        [
            'name' => 'datetimeModified',
            'type' => 'raw',
        ],
		'square',
		'number',
		'members',
		'visitors',
		'phone',
		'email',
        [
            'name' => 'site',
            'type' => 'raw',
            'value' => function($data){
		        return CHtml::link($data->site, $data->site, array('target' => '_blank'));
            }
        ],
		'postcode',
		'services',
		'FoundationYear',
		'plainHash',
		'advertisingContact',
		'standsConstructionContact',
		'cateringContact',
		'accreditationContact',
		'accreditationPrice',
		'rsva',
		[
			'name' => 'exhibitionComplexTypeId',
			'type' => 'raw',
			'value' => function($data){
				if(isset($data->exhibitionComplexType))
					return $data->exhibitionComplexType->fullNameSingle;
				return '';
			}
		],
		'uniqueText',
		[
			'name' => 'associationName',
			'type' => 'raw',
			'value' => function($data){
				$result = '';

				if(empty($data->exhibitionComplexHasAssociation))
					return $result;

				foreach ($data->exhibitionComplexHasAssociation as $ECHA)
					if(isset($ECHA->association->name))
						$result .= $ECHA->association->name . ', ';
				return trim($result,', ');
			}
		],
		[
			'name' => 'imageId',
			'type' => 'raw',
			'value' => function($data) {
				$result = '';
				$objectFiles = ObjectFile::getFileList($data, ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE);

				if (is_array($objectFiles) && count($objectFiles) > 0)
					foreach ($objectFiles as $images)
						if (isset($images['assetPath']))
							$result .= CHtml::image($images['assetPath']);
				return $result;
			},
		],
		[
			'name' => 'galleryImages',
			'label' => Yii::t('AdminModule.admin', 'Gallery images'),
			'type' => 'raw',
			'value' => function($data){
				$result = '';
				$objectFiles = ObjectFile::getFileList($data, ObjectFile::TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES);

				if(is_array($objectFiles) && count($objectFiles) > 0)
					foreach ($objectFiles as $images)
						if(isset($images['assetPath']))
							$result .= CHtml::image($images['assetPath']);
				return $result;
			},
		],
		'exdbId',
		'exdbContact',
        [
            'name' => Yii::t('AdminModule.admin', 'Is exhibitionComplex logo loaded'),
            'type' => 'raw',
            'value' => function($data){
                $result = 0;
                $objectFiles = ObjectFile::getFileList($data, ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE);
                if (is_array($objectFiles) && count($objectFiles) > 0){

                    $result =  TRUE;
                }

                return $result;
            },
        ],
//		[
//			'name' => 'exdbRaw',
//			'type' => 'raw',
//		],
	),
));
?>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'fairs-grid',
		'dataProvider' => $fairsDataProvider,
		'columns' => array(
			'id',
			'name' => [
				'name' => 'name',
				'value' => function($data){
					return CHtml::link($data->name, Yii::app()->createUrl('admin/fair/view', ['id' => $data->id]));
				},
				'type' => 'raw'
			],
		),
	)); ?>

<?= TbHtml::linkButton('  ' .
	'Редактировать', array(
	'class' => 'btn btn-warning',
	'style' => 'margin-top:10px;',
	'url' => $this->createUrl('update', array('id' => $model->id))
));
?>

<?= TbHtml::linkButton('  ' .
	'Список', array(
	'class' => 'btn btn-success',
	'style' => 'margin-top:10px;',
	'url' => $this->createUrl('admin')
));
?>

<?= TbHtml::linkButton('  ' .
	'Просмотр на сайте', array(
	'class' => 'btn d-bg-primary-dark',
	'style' => 'margin-top:10px;',
    'url' => Yii::app()->createUrl('venue') . DIRECTORY_SEPARATOR . (isset($model->translate) ? $model->translate->shortUrl : '')
));
?>
