<?php
/**
 * @var $this ExhibitionComplexController
 * @var $model ExhibitionComplex
 *
 * @var $pageVariants array
 * @var $pageSize int
 *
 * @var $fields array
 * @var $selectedFields array
 */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ExhibitionComplex-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#grid-button').on('click', function () {

    console.log('grid-button');

    var \$fields = $('#ExhibitionComplex_fields .checkbox.checked input');
    var pageSize = $('#ExhibitionComplex_pageSize .drop-list__item--selected input').val();
    var fields = \$fields.map(function() {
        return $(this).val();
    }).get();
    $.fn.yiiGridView.update('grid', {data: {
        ExhibitionComplex_fields: fields.toString(),
        ExhibitionComplex_pageSize: pageSize,
        ExhibitionComplex_page: 1
    }})
});
");
?>
<style>
    #ExhibitionComplex_pageSize .drop-list,
    #ExhibitionComplex_pageSize .slimScrollDiv,
    #ExhibitionComplex_fields .drop-list,
    #ExhibitionComplex_fields .slimScrollDiv {
        height: 220px !important;
        max-height: 220px !important;
    }

    #ExhibitionComplex_pageSize .list-auto-complete__main-list,
    #ExhibitionComplex_fields .list-auto-complete__main-list {
        height: 192px !important;
        max-height: 192px !important;
    }
</style>

<h1><?= Yii::t('AdminModule.admin', 'Exhibition complexes'); ?></h1>
<div style="height: 40px;">
    <?= TbHtml::linkButton(
        'Добавить новый выставочный центр',
        [
            'url'   => $this->createUrl('create'),
            'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
            'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;",
            'target' => '_blank',
        ]
    ); ?>

    <div style="float: right;">

        <div style="width: 180px; display: inline-block;">
            <?php $this->widget('application.modules.admin.components.ListAutoComplete', [
                'id'    => 'ExhibitionComplex_pageSize',
                'name'  => 'ExhibitionComplex_pageSize',
                'label' => 'На странице',
                'itemOptions' => [
                    'data' => $pageVariants
                ],
                'selectedItems' => [$pageSize],
                'htmlOptions' => [
                    'style' => 'margin: 0;'
                ],
                'autoComplete' => false,
                'multiple' => false
            ]); ?>
        </div>

        <div style="width: 250px; display: inline-block;">
            <?php $this->widget('application.modules.admin.components.ListAutoComplete', [
                'id'    => 'ExhibitionComplex_fields',
                'name'  => 'ExhibitionComplex_fields',
                'label' => 'Поля формы',
                'itemOptions' => [
                    'data' => $fields
                ],
                'selectedItems' => array_keys($selectedFields) ?: [],
                'htmlOptions' => [
                    'style' => 'margin: 0;'
                ],
                'autoComplete' => false
            ]); ?>
        </div>

        <div style="display: inline-block;">
            <?= TbHtml::button('Применить', [
                'id'    => 'grid-button',
                'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
                'style' => 'display: block; margin: 0;'
            ]); ?>
        </div>

    </div>
</div>

<div style="clear: both;"></div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'      => 'grid',
    'columns' => array_merge([
        'id' => [
            'name' => 'id',
            'type' => 'raw',
        ],
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}{update}',

            'buttons' => array(
                'view' => array(
                    'label' => 'Просмотреть',
                    'url' => function($data){
                        return Yii::app()->createUrl("admin/exhibitionComplex/view", array("id" => $data['id']));
                    },
                    'options' => array(
                        'target' => '_blank',
                    ),
                ),
                'update' => array(
                    'label' => 'Редактировать',
                    'url' => function($data){
                        return Yii::app()->createUrl("admin/exhibitionComplex/update", array("id" => $data['id']));
                    },
                    'options' => array(
                        'target' => '_blank',
                    ),
                ),
            ),

            'headerHtmlOptions' => array(
                'style' => "min-width: 16px;"
            ),
            'htmlOptions' => array(
                'target' => '_blank',
                'style' => "min-width: 16px;"
            ),
        ),
    ], $selectedFields),
    'dataProvider' => $dataProvider,
    'htmlOptions' => array(
        'style' => "overflow-y: auto;"
    ),
    'filter'       => $model,
    'ajaxUpdate'   => false,
    'template' => '{summary}{pager}{items}{pager}{summary}'
));