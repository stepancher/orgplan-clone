<?php
/**
 * @var $this  ExhibitionComplexController
 * @var $model ExhibitionComplex
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets.css'), false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/base.css');
?>

<div>
    <?= CHtml::decode($model->exdbRaw) ?>
</div>