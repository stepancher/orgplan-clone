<?php
/* @var $this ExhibitionComplexController */
/* @var $data ExhibitionComplex */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street')); ?>:</b>
	<?php echo CHtml::encode($data->street); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cityId')); ?>:</b>
	<?php echo CHtml::encode($data->cityId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('regionId')); ?>:</b>
	<?php echo CHtml::encode($data->regionId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coordinates')); ?>:</b>
	<?php echo CHtml::encode($data->coordinates); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('square')); ?>:</b>
	<?php echo CHtml::encode($data->square); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('number')); ?>:</b>
	<?php echo CHtml::encode($data->number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('members')); ?>:</b>
	<?php echo CHtml::encode($data->members); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visitors')); ?>:</b>
	<?php echo CHtml::encode($data->visitors); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site')); ?>:</b>
	<?php echo CHtml::encode($data->site); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('services')); ?>:</b>
	<?php echo CHtml::encode($data->services); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imageId')); ?>:</b>
	<?php echo CHtml::encode($data->imageId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activateControl')); ?>:</b>
	<?php echo CHtml::encode($data->activateControl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('originalId')); ?>:</b>
	<?php echo CHtml::encode($data->originalId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FoundationYear')); ?>:</b>
	<?php echo CHtml::encode($data->FoundationYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plainHash')); ?>:</b>
	<?php echo CHtml::encode($data->plainHash); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advertisingContact')); ?>:</b>
	<?php echo CHtml::encode($data->advertisingContact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('standsConstructionContact')); ?>:</b>
	<?php echo CHtml::encode($data->standsConstructionContact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cateringContact')); ?>:</b>
	<?php echo CHtml::encode($data->cateringContact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accreditationContact')); ?>:</b>
	<?php echo CHtml::encode($data->accreditationContact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accreditationPrice')); ?>:</b>
	<?php echo CHtml::encode($data->accreditationPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rsva')); ?>:</b>
	<?php echo CHtml::encode($data->rsva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exhibitionComplexTypeId')); ?>:</b>
	<?php echo CHtml::encode($data->exhibitionComplexTypeId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueText')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueText); ?>
	<br />

	*/ ?>

</div>