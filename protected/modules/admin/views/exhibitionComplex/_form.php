<?php
Yii::import('bootstrap.helpers.TbArray');

/**
 * @var $form CActiveForm
 * @var $model ExhibitionComplex
 * @var $this ExhibitionComplexController
 */
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/exhibitionComplex/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

	<div class="cols-row">
		<div class="col-md-12">
			<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Save') ?>
			</button>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
			   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Cancel') ?>
			</a>
			<?php if(Yii::app()->controller->action->id !== 'create'): ?>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('view', array('id' => $model->id,)) ?>"
			   class="js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'View') ?>
			</a>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group field-exhibitioncomplex-name">
		<label class="control-label" for="ExhibitionComplex_name"><?= Yii::t('AdminModule.admin', 'name') ?></label>
		<input type="text" id="ExhibitionComplex_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[name]" value="<?= $model->name ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-description">
		<label class="control-label" for="exhibitioncomplex-description"><?= Yii::t('AdminModule.admin', 'description') ?></label>
		<textarea id="exhibitioncomplex-description" class="b-form__text input-block-level" name="ExhibitionComplex[description]" row="10"><?= $model->description ?></textarea>
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-descriptionsnippet">
		<label class="control-label" for="exhibitioncomplex-descriptionsnippet"><?= Yii::t('AdminModule.admin', 'seo description') ?></label>
		<textarea id="exhibitioncomplex-descriptionsnippet" class="b-form__text input-block-level" name="ExhibitionComplex[descriptionSnippet]" row="10"><?= $model->descriptionSnippet ?></textarea>
		<div class="help-block"></div>
	</div>

	<div id="select2-widget" style="margin-bottom: 20px;"> <?= Yii::t('AdminModule.admin','City') ?>
		<?php $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
			'name' => ucfirst(get_class($model))."[".ExhibitionComplex::CITY_VARIABLE."]",
			'value' => isset($model->city) ? json_encode(array('id' => $model->city->id, 'name' => $model->city->name)) : '',
			'options' => array(
				'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
				'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
				'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
				'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
				'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
				'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
				'width' => '100%',
				'placeholder' => "Выберите город",
				'multiple' => FALSE,
				'minimumInputLength' => 1,
				'ajax' => array(
					'url' => $this->createUrl('getCitiesByText'),
					'dataType' => 'json',
					'data' => 'js:function (term, page) { return { q: term}; }',
					'results' => 'js:function(data,page){return {results: data};}',
					'update' => '#regionId',
				),
				'formatResult'    => 'js:function(data){
                                return data.id + " " + data.name;
                            }',
				'formatSelection' => "js: function(data) {
												
								$.ajax({
									url: '{$this->createUrl('getCityGeoData')}',
									method: 'POST',
									async: false,
									dataType: 'json',
									data: {'cityId': data.id},
									success: function(data){
										if(data.success){
										
										$(document).find('#regionId').val(data.regionName);
										$(document).find('#districtId').val(data.districtName);
										$(document).find('#countryId').val(data.countryName);
										
										} else {
											alert(data.message);
										}
									}
								});
								
                                return data.id + ' ' + data.name;
                            }",
				'initSelection' => 'js:function (element, callback) {
                                // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                var items = JSON.parse(element.val());
                                element.val(items.id);
                                callback(items);
                            }',
			),
			'htmlOptions' => array(
				'style' => 'border:none;margin-top:10px;',
			),
		));
		?>
	</div>

	<div style="margin-top: 10px;float:left;width:100%;font-weight: bold;"><?= Yii::t('AdminModule.admin','Region') ?> : </div>

	<div style="float:left;width:100%;font-weight: bold;">
		<label><input readonly id="regionId"></label>
	</div>

	<div style="margin-top: 10px;float:left;width:100%;font-weight: bold;"><?= Yii::t('AdminModule.admin','District') ?> : </div>

	<div style="float:left;width:100%;font-weight: bold;">
		<label><input readonly id="districtId"></label>
	</div>

	<div style="margin-top: 10px;float:left;width:100%;font-weight: bold;"><?= Yii::t('AdminModule.admin','Country') ?> : </div>

	<div style="float:left;width:100%;font-weight: bold;">
		<label><input readonly id="countryId"></label>
	</div>

	<div style="clear:both"></div>
	<br>

	<div class="form-group field-exhibitioncomplex-members">
		<label class="control-label" for="ExhibitionComplex_members"><?= Yii::t('AdminModule.admin', 'Pavilions') ?></label>
		<input type="text" id="ExhibitionComplex_members" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[members]" value="<?= $model->members ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-visitors">
		<label class="control-label" for="ExhibitionComplex_visitors"><?= Yii::t('AdminModule.admin', 'Meeting Rooms') ?></label>
		<input type="text" id="ExhibitionComplex_visitors" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[visitors]" value="<?= $model->visitors ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-number">
		<label class="control-label" for="ExhibitionComplex_number"><?= Yii::t('AdminModule.admin', 'Fairs') ?></label>
		<input type="text" id="ExhibitionComplex_number" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[number]" value="<?= $model->number ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-square">
		<label class="control-label" for="ExhibitionComplex_square"><?= Yii::t('AdminModule.admin', 'Area') ?></label>
		<input type="text" id="ExhibitionComplex_square" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[square]" value="<?= $model->square ?>">
		<div class="help-block"></div>
	</div>
	
	<div class="form-group field-exhibitioncomplex-foundationyear">
		<label class="control-label" for="ExhibitionComplex_FoundationYear"><?= Yii::t('AdminModule.admin', 'Year of foundation') ?></label>
		<input type="text" id="ExhibitionComplex_FoundationYear" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[FoundationYear]" value="<?= $model->FoundationYear ?>">
		<div class="help-block"></div>
	</div>

<?php echo
	$this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
				'model' => $model,
				'attribute' => 'uniqueText',
				'options' => array(
					'minHeight' => '250px',
					'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array('modelName' => get_class($model))),
					'lang' => 'ru',
					'iframe' => true,
					'css' => 'wym.css',
				)
			), true
		);
	?>

	<?= Yii::t('AdminModule.admin', 'Status') ?>
	<?= CHtml::activeDropDownList($model, 'statusId', ExhibitionComplexStatus::getStatuses(), array('empty' => ''))
	?>
	<br/>

	<div class="form-group field-exhibitioncomplex-exdbid">
		<label class="control-label" for="ExhibitionComplex_exdbid"><?= Yii::t('AdminModule.admin', 'Expodatabase exhibition complex id') ?></label>
		<input type="text" id="ExhibitionComplex_exdbid" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[exdbId]" value="<?= $model->exdbId ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-exdbcontact">
		<label class="control-label" for="exhibitioncomplex-exdbcontact"><?= Yii::t('AdminModule.admin', 'Expodatabase exhibition complex contact') ?></label>
		<textarea id="exhibitioncomplex-exdbcontact" class="b-form__text input-block-level" name="ExhibitionComplex[exdbContact]" row="10"><?= $model->exdbContact ?></textarea>
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-phone">
		<label class="control-label" for="ExhibitionComplex_phone"><?= Yii::t('AdminModule.admin', 'Phone number') ?></label>
		<input type="text" id="ExhibitionComplex_phone" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[phone]" value="<?= $model->phone ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-email">
		<label class="control-label" for="ExhibitionComplex_email"><?= Yii::t('AdminModule.admin', 'Email') ?></label>
		<input type="text" id="ExhibitionComplex_email" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[email]" value="<?= $model->email ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-site">
		<label class="control-label" for="ExhibitionComplex_site"><?= Yii::t('AdminModule.admin', 'exhibitionComplex Website') ?></label>
		<input type="text" id="ExhibitionComplex_site" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[site]" value="<?= $model->site ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-postcode">
		<label class="control-label" for="ExhibitionComplex_postcode"><?= Yii::t('AdminModule.admin', 'Postcode') ?></label>
		<input type="text" id="ExhibitionComplex_postcode" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[postcode]" value="<?= $model->postcode ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-advertisingContact">
		<label class="control-label" for="ExhibitionComplex_advertisingContact"><?= Yii::t('AdminModule.admin', 'Advertising contact') ?></label>
		<input type="text" id="ExhibitionComplex_advertisingContact" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[advertisingContact]" value="<?= $model->advertisingContact ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-standsConstructionContact">
		<label class="control-label" for="ExhibitionComplex_standsConstructionContact"><?= Yii::t('AdminModule.admin', 'Stands construction contact') ?></label>
		<input type="text" id="ExhibitionComplex_standsConstructionContact" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[standsConstructionContact]" value="<?= $model->standsConstructionContact ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-cateringContact">
		<label class="control-label" for="ExhibitionComplex_cateringContact"><?= Yii::t('AdminModule.admin', 'Catering contact') ?></label>
		<input type="text" id="ExhibitionComplex_cateringContact" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[cateringContact]" value="<?= $model->cateringContact ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-accreditationContact">
		<label class="control-label" for="ExhibitionComplex_accreditationContact"><?= Yii::t('AdminModule.admin', 'Accreditation contact') ?></label>
		<input type="text" id="ExhibitionComplex_accreditationContact" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[accreditationContact]" value="<?= $model->accreditationContact ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-accreditationPrice">
		<label class="control-label" for="ExhibitionComplex_accreditationPrice"><?= Yii::t('AdminModule.admin', 'Price for accreditation') ?></label>
		<input type="text" id="ExhibitionComplex_accreditationPrice" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[accreditationPrice]" value="<?= $model->accreditationPrice ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-services">
		<label class="control-label" for="ExhibitionComplex_services"><?= Yii::t('AdminModule.admin', 'exhibitionComplex Services') ?></label>
		<input type="text" id="ExhibitionComplex_services" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[services]" value="<?= $model->services ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-plainHash">
		<label class="control-label" for="ExhibitionComplex_plainHash"><?= Yii::t('AdminModule.admin', 'Panorama') ?></label>
		<input type="text" id="ExhibitionComplex_plainHash" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[plainHash]" value="<?= $model->plainHash ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-exhibitioncomplex-rsva">
		<label class="control-label" for="ExhibitionComplex_rsva"><?= Yii::t('AdminModule.admin', 'RUEF members') ?></label>
		<input type="text" id="ExhibitionComplex_rsva" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[rsva]" value="<?= $model->rsva ?>">
		<div class="help-block"></div>
	</div>

	<?= Yii::t('AdminModule.admin', 'Exhibition complex type') ?>
	<?= CHtml::activeDropDownList($model, 'exhibitionComplexTypeId', ExhibitionComplexType::getTypes(), array('empty' => ''))
	?>
	<br/>

	<div class="form-group field-exhibitioncomplex-street">
		<label class="control-label" for="ExhibitionComplex_street"><?= Yii::t('AdminModule.admin', 'Street') ?></label>
		<input type="text" id="ExhibitionComplex_street" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[street]" value="<?= $model->street ?>">
		<div class="help-block"></div>
	</div>

	<div id="exhibitioncomplex-coordinates-map" style="width: 800px; height: 400px">

		<?php
		$getClientScript = Yii::app()->getClientScript();
		$getClientScript->registerScriptFile('https://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU');
		$publicPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

		$getClientScript->registerScriptFile($publicPath . '/' . 'js/map/Map.js', CClientScript::POS_END);
		$getClientScript->registerScriptFile($publicPath . '/' . 'js/_ActiveForm/' . 'typeMap' . '.js', CClientScript::POS_END);

		$getClientScript->registerScript(
			md5('typeMap' . json_encode(
					array_replace_recursive(
						[
							'coordinates' => $model->coordinates,
							'selector' => 'exhibitioncomplex-coordinates-map',
						],
						[
							'targetCoordinates' => 'exhibitionComplex-coordinates',
							'targetStreet' => 'exhibitioncomplex-street',
						]
					)
				)
			),
			'typeMap' . '(' . json_encode(
				array_replace_recursive([
					'coordinates' => $model->coordinates,
					'selector' => 'exhibitioncomplex-coordinates-map',
				],
					[
						'targetCoordinates' => 'exhibitionComplex-coordinates',
						'targetStreet' => 'exhibitioncomplex-street',
					]
				)
			) . ')',
			CClientScript::POS_READY
			);
		?>
	</div>

	<div class="form-group field-exhibitioncomplex-coordinates">
		<label class="control-label" for="ExhibitionComplex_coordinates"><?= Yii::t('AdminModule.admin', 'Coordinates') ?></label>
		<input type="text" id="ExhibitionComplex_coordinates" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ExhibitionComplex[coordinates]" value="<?= $model->coordinates ?>">
		<div class="help-block"></div>
	</div>

	<?php echo $this->widget('application.modules.admin.components.StyledMultiFileUploader', array(
		'model' => $model,
		'attribute' => 'imageId',
		'max' => 1,
		'accept' => ObjectFile::$acceptedImageExt,
		'deleteAction' => Yii::app()->createUrl('file/objectFile/remove', array('id' => '{id}', 'cls' => 'ExhibitionComplex')),
		'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE),
		'outputAsImage' => true,
		'buttonTag' => 'button',
		'buttonOptions' => [
			'class' => 'b-button d-bg-success-dark d-bg-success--hover',
			'type' => 'button',
			'content' => Yii::t('AdminModule.admin', 'Upload main image')
		]
	), true) ?>

	<?php echo $this->widget('application.modules.admin.components.StyledMultiFileUploader', array(
		'model' => $model,
		'attribute' => 'galleryImages',
		'accept' => ObjectFile::$acceptedImageExt,
		'deleteAction' => Yii::app()->createUrl('file/objectFile/remove', array('id' => '{id}', 'cls' => 'ExhibitionComplex')),
		'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES),
		'outputAsImage' => true,
		'buttonTag' => 'button',
		'buttonOptions' => [
			'class' => 'b-button d-bg-success-dark d-bg-success--hover',
			'type' => 'button',
			'content' => Yii::t('AdminModule.admin', 'Upload gallery images')
		]
	), true) ?>

	<?php
	$getClientScript = Yii::app()->getClientScript();
	$publicPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
	$getClientScript->registerScriptFile($publicPath . '/' . 'js/dropdown/script.js', CClientScript::POS_END);

	echo
	TbHtml::activeControlGroup(
		TbHtml::INPUT_TYPE_DROPDOWNLIST,
		$model,
		'active',
		array(
			'groupOptions' => array(
				'class' => 'js-drop-down-list drop-down-list',
			),
			'labelOptions' => array(
				'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
			),
			'input' => TbHtml::dropDownList(
				'ExhibitionComplex[active]',
				$model->active,
				ExhibitionComplex::getActive()
			),
		)
	);
	?>

	<div> <?= Yii::t('AdminModule.admin', 'Fair association') ?> </div>
	<div>
		<?php
		$this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
			'name' => ucfirst(get_class($model))."[".ExhibitionComplex::ASSOCIATION_VARIABLE."]",
			'value' => json_encode(ExhibitionComplexAdminService::getAssociationsByExhibitionComplexId($model->id)),
			'options' => array(
				'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
				'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
				'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
				'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
				'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
				'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
				'width' => '100%',
				'placeholder' => "",
				'multiple' => true,
				'minimumInputLength' => 1,
				'ajax' => array(
					'url' => $this->createUrl('getAssociations'),
					'dataType' => 'json',
					'data' => 'js:function (term, page) { return { q: term}; }',
					'results' => 'js:function(data,page){return {results: data};}',

				),
				'formatResult'    => 'js:function(data){
                    return data.id + " " + data.name;
                }',
				'formatSelection' => 'js: function(data) {
                    return data.id + " " + data.name;
                }',
				'initSelection' => 'js:function (element, callback) {
                    // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                    var items = JSON.parse(element.val());
                    element.val("");
                    callback(items);
                }',
			),
			'htmlOptions' => array(
				'class' => '',
				'style' => 'border:none;margin-top:10px;',
			),
		));
		?>
	</div>

	<br>

	<div class="cols-row">
		<div class="col-md-12">
			<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Save') ?>
			</button>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
			   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Cancel') ?>
			</a>
			<?php if(Yii::app()->controller->action->id !== 'create'): ?>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('view', array('id' => $model->id,)) ?>"
			   class="js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'View') ?>
			</a>
			<?php endif; ?>
		</div>
	</div>
</form>

<?php if(Yii::app()->controller->action->id == 'update'): ?>
	<?php $this->renderPartial('_exdbRaw', array('model' => $model)) ?>
<?php endif; ?>