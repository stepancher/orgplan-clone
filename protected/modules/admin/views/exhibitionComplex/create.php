<?php
/* @var $this ExhibitionComplexController */
/* @var $model ExhibitionComplex */

$this->menu=array(
	array('label'=>'List ExhibitionComplex', 'url'=>array('index')),
	array('label'=>'Manage ExhibitionComplex', 'url'=>array('admin')),
);
?>

<h1><?= Yii::t('AdminModule.admin', 'Create exhibition complex') ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>