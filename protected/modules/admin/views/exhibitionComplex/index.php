<?php
/* @var $this ExhibitionComplexController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Create ExhibitionComplex', 'url'=>array('create')),
	array('label'=>'Manage ExhibitionComplex', 'url'=>array('admin')),
);
?>

<h1>Выставочные центры</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
