<?php

/**
 * @var CController $this
 * @var Stand $model
 * @var Wall $wall
 * @var FlooringGroup $flooringGroup
 * @var Modeling $modeling
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerCssFile(
    $am->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/css/autoComplete/style.css'));
$cs->registerScriptFile(
    $am->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/js/autoComplete/script.js'));
$cs->registerScriptFile(
    $am->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/js/historyButton/script.js'));
$ctrl = $this;
use yii\helpers\ArrayHelper;
Yii::import('application.modules.stand.models.*');
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/stand/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

<div>
    <a href="<?= $this->createUrl('admin') ?>"
       class="offset-top-8 b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
        <?= Yii::t('AdminModule.admin', 'Back to list') ?>
    </a>
    <a href="<?= $this->createUrl('view', ['id' => $model->id]) ?>"
       class="offset-top-8 b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
        <?= Yii::t('AdminModule.admin', 'View') ?>
    </a>
</div>

<div class="cols-row">
    <?php $this->renderPartial('_blocks/_b-header',
        [
            'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Form header create' : 'Form header update')
        ]
    ) ?>
    <div class="cols-row">
        <div class="col-md-4">
            
            <div class="form-group field-stand-name">
                <label class="control-label" for="Stand_name"><?= Yii::t('AdminModule.admin', 'Stand') ?></label>
                <input type="text" id="Stand_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Stand[name]" value="<?= $model->name ?>">
                <div class="help-block"></div>
            </div>

            <?=
            $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                'model' => $model,
                'attribute' => 'description',
                'options' => array(
                    'minHeight' => '250px',
                    'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array(
                            'modelName' => get_class($model)
                        )
                    ),
                    'lang' => 'ru',
                    'iframe' => true,
                    'css' => 'wym.css',
                )
            ), true
            );
            ?>
        </div>
        <div class="col-md-4">
            <?php
            $clientScript = Yii::app()->getClientScript();
            $publicPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
            $clientScript->registerScriptFile($publicPath . '/' . 'js/dropdown/script.js', CClientScript::POS_END);

            echo TbHtml::activeControlGroup(
                        TbHtml::INPUT_TYPE_DROPDOWNLIST,
                        $model,
                       'isIdeal',
                        array(
                            'groupOptions' => array(
                                'class' => 'js-drop-down-list drop-down-list',
                            ),
                            'labelOptions' => array(
                                'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                            ),
                            'input' => TbHtml::dropDownList(
                                'Stand[isIdeal]',
                                $model->isIdeal,
                                Stand::ideals()
                            ),
                        )
                    );
            ?>

            <div class="form-group field-stand-plannerhash">
                <label class="control-label" for="Stand_plannerHash"><?= Yii::t('AdminModule.admin', 'Hash Project Planner 5D') ?></label>
                <input type="text" id="Stand_plannerHash" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Stand[plannerHash]" value="<?= $model->plannerHash ?>">
                <div class="help-block"></div>
            </div>

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model,
                'plan',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Stand[isIdeal]',
                        $model->plan,
                        Stand::planTypes()
                    ),
                )
            );
            ?>

            <?php echo
            $this->widget('ext.widgets.ListAutoComplete',
                ArrayHelper::merge(
                    array(
                        'name' => 'Stand[fairs]',
                        'attribute' => 'fairs'
                    ),
                    [
                        'model' => $model,
                        'selectedItems' => $model->getObjectIds($model->standHasFairs, 'fairId'),
                        'multiple' => TRUE,
                        'htmlOptions' => array(
                            'labelOptions' => array(
                                'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                            ),
                            'data-input-id' => 'fairs'
                        ),
                        'itemOptions' => [
                            'data' => CHtml::listData(\TrFair::model()->findAll(), 'trParentId', 'name'),
                        ],
                        'label' => NULL,
                    ]
                ),
                true);
            ?>

            <?php echo
            $this->widget('ext.widgets.ListAutoComplete',
                ArrayHelper::merge(
                    array(
                        'name' => 'Stand[standTechnicalData_standTechnicalDataHasExtraServices]',
                        'attribute' => 'standTechnicalData_standTechnicalDataHasExtraServices'
                    ),
                    [
                        'model' => $model,
                        'selectedItems' => $model->getObjectIds(
                            $model->standTechnicalData ? $model->standTechnicalData->standTechnicalDataHasExtraServices : null
                            , 'extraServiceId'
                        ),
                        'multiple' => TRUE,
                        'htmlOptions' => array(
                            'labelOptions' => array(
                                'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                            ),
                            'data-input-id' => 'standTechnicalData_standTechnicalDataHasExtraServices'
                        ),
                        'itemOptions' => [
                            'data' => CHtml::listData(\ExtraService::model()->findAll(), 'id', 'name'),
                        ],
                        'label' => NULL,
                    ]
                ),
                true);
            ?>

            <?php
            echo
            $this->widget('ext.widgets.ListAutoComplete',
                ArrayHelper::merge(
                    array(
                        'name' => 'Stand[standTechnicalData_modeling_modelingHasMultimedias]',
                        'attribute' => 'standTechnicalData_modeling_modelingHasMultimedias'
                        ),
                    [
                        'model' => $model,
                        'selectedItems' => $model->getObjectIds(
                            $model->standTechnicalData && isset($model->standTechnicalData->modeling)? $model->standTechnicalData->modeling->modelingHasMultimedias : null
                            , 'multimediaId'
                        ),
                        'multiple' => TRUE,
                        'htmlOptions' => array
                        (
                            'labelOptions' => array
                            (
                                'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                            ),
                            'data-input-id' => 'standTechnicalData_modeling_modelingHasMultimedias'
                        ),
                        'itemOptions' => [
                            'data' => CHtml::listData(\Multimedia::model()->findAll(), 'id', 'name'),
                        ],
                        'label' => NULL,
                    ]
                ),
                true);
            ?>

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model,
                'typeId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Stand[typeId]',
                        $model->typeId,
                        Stand::commonType()
                    ),
                )
            );
            ?>
        </div>
        <div class="col-md-4">

            <div class="form-group field-stand-cost required">
                <label class="control-label" for="Stand_cost"><?= Yii::t('AdminModule.admin', 'Cost') ?></label>
                <input type="text" id="Stand_cost" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Stand[cost]" value="<?= $model->cost ?>">
                <div class="help-block"></div>
            </div>

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model,
                'type',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Stand[type]',
                        $model->type,
                        Stand::types()
                    ),
                )
            );
            ?>

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model,
                'locationType',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Stand[locationType]',
                        $model->locationType,
                        Stand::locationTypes()
                    ),
                )
            );
            ?>

            <div class="form-group field-stand-square">
                <label class="control-label" for="Stand_square"><?= Yii::t('AdminModule.admin', 'Area') ?></label>
                <input type="text" id="Stand_square" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Stand[square]" value="<?= $model->square ?>">
                <div class="help-block"></div>
            </div>

            <?php echo
            $this->widget('ext.widgets.ListAutoComplete',
                ArrayHelper::merge(
                    array
                    (
                        'name' => 'Stand[standTechnicalData_standTechnicalDataHasElectricities]',
                        'attribute' => 'standTechnicalData_standTechnicalDataHasElectricities'
                    ),
                    [
                        'model' => $model,
                        'selectedItems' => $model->getObjectIds(
                            $model->standTechnicalData ? $model->standTechnicalData->standTechnicalDataHasElectricities : null
                            , 'electricityId'
                        ),
                        'multiple' => TRUE,
                        'htmlOptions' => array
                        (
                            'labelOptions' => array
                            (
                                'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                            ),
                            'data-input-id' => 'standTechnicalData_standTechnicalDataHasElectricities'
                        ),
                        'itemOptions' => [
                            'data' => CHtml::listData(\Electricity::model()->findAll(), 'id', 'name'),
                        ],
                        'label' => NULL,
                    ]
                ),
                true);
            ?>

            <?php echo
            $this->widget('ext.widgets.ListAutoComplete',
                ArrayHelper::merge(
                    array
                    (
                        'name' => 'Stand[standLightings]',
                        'attribute' => 'standLightings'
                    ),
                    [
                        'model' => $model,
                        'selectedItems' => $model->getObjectIds(
                            $model->standHasLightings ? $model->standHasLightings : null
                            , 'lightingId'
                        ),
                        'multiple' => TRUE,
                        'htmlOptions' => array
                        (
                            'labelOptions' => array
                            (
                                'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                            ),
                            'data-input-id' => 'standLightings'
                        ),
                        'itemOptions' => [
                            'data' => CHtml::listData(\Lighting::model()->findAll(), 'id', 'name'),
                        ],
                        'label' => NULL,
                    ]
                ),
                true);
            ?>
            
            <?php echo
            $this->widget('ext.widgets.ListAutoComplete',
                ArrayHelper::merge(
                    array
                    (
                        'name' => 'Stand[standTechnicalData_modeling_modelingHasFurnitures]',
                        'attribute' => 'standTechnicalData_modeling_modelingHasFurnitures'
                    ),
                    [
                        'model' => $model,
                        'selectedItems' => $model->getObjectIds(
                            $model->standTechnicalData && isset($model->standTechnicalData->modeling)? $model->standTechnicalData->modeling->modelingHasFurnitures : null
                            , 'furnitureId'
                        ),
                        'multiple' => TRUE,
                        'htmlOptions' => array
                        (
                            'labelOptions' => array
                            (
                                'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                            ),
                            'data-input-id' => 'standTechnicalData_modeling_modelingHasFurnitures'
                        ),
                        'itemOptions' => [
                            'data' => CHtml::listData(\Furniture::model()->findAll(), 'id', 'name'),
                        ],
                        'label' => NULL,
                    ]
                ),
                true);
            ?>
        </div>
    </div>
    <div class="cols-row">
        <div class="col-md-6">
            <?= $ctrl->widget('application.modules.admin.components.StyledMultiFileUploader', array(
                'model' => $model,
                'attribute' => 'standImage',
                'max' => 1,
                'accept' => ObjectFile::$acceptedImageExt,
                'deleteAction' => $ctrl->createUrl('file/objectFile/remove', array('id' => '{id}', 'cls' => 'Stand')),
                'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_STAND_IMAGE),
                'outputAsImage' => true,
                'buttonTag' => 'button',
                'buttonOptions' => [
                    'class' => 'b-button d-bg-success-dark d-bg-success--hover',
                    'type' => 'button',
                    'content' => Yii::t('AdminModule.admin', 'Upload stand image')
                ]
            ), true) ?>
        </div>
        <div class="col-md-6">
            <?= $ctrl->widget('application.modules.admin.components.StyledMultiFileUploader', array(
                'model' => $model,
                'attribute' => 'standImageLayout',
                'accept' => ObjectFile::$acceptedImageExt,
                'deleteAction' => $ctrl->createUrl('file/objectFile/remove', array('id' => '{id}', 'cls' => 'Stand')),
                'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_STAND_IMAGE_LAYOUT),
                'outputAsImage' => true,
                'buttonTag' => 'button',
                'buttonOptions' => [
                    'class' => 'b-button d-bg-success-dark d-bg-success--hover',
                    'type' => 'button',
                    'content' => Yii::t('AdminModule.admin', 'Upload layout image')
                ]
            ), true) ?>
        </div>
    </div>
    <?php $this->renderPartial('_blocks/_b-header',
        [
            'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Form header create' : 'Form header update')
        ]
    ) ?>
    <div class="cols-row">
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->wall,
                'wallTypeId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Wall[wallTypeId]',
                        $model->standTechnicalData->wall->wallTypeId,
                        CHtml::listData(\WallType::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>

        </div>
        <div class="col-md-4">

            <div class="form-group field-wall-roof">
                <label class="control-label" for="Wall_roof"><?= Yii::t('AdminModule.admin', 'Roof') ?></label>
                <input type="text" id="Wall_roof" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Wall[roof]" value="<?= $model->standTechnicalData->wall->roof ?>">
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->wall,
                'decorId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Wall[decorId]',
                        $model->standTechnicalData->wall->decorId,
                        CHtml::listData(\Decor::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>
        </div>
    </div>
    <?php $this->renderPartial('_blocks/_b-header',
        [
            'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Form header create' : 'Form header update')
        ]
    ) ?>
    <div class="cols-row">
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->flooringGroup,
                'podiumId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'FlooringGroup[podiumId]',
                        $model->standTechnicalData->flooringGroup->podiumId,
                        CHtml::listData(\Podium::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>

        </div>
        <div class="col-md-4">

            <div class="form-group field-flooringgroup-square">
                <label class="control-label" for="FlooringGroup_square"><?= Yii::t('AdminModule.admin', 'Area') ?></label>
                <input type="text" id="FlooringGroup_square" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="FlooringGroup[square]" value="<?= $model->standTechnicalData->flooringGroup->square ?>">
                <div class="help-block"></div>
            </div>

        </div>
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->flooringGroup,
                'flooringId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'FlooringGroup[flooringId]',
                        $model->standTechnicalData->flooringGroup->flooringId,
                        CHtml::listData(\Flooring::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>

        </div>
    </div>
    <?php $this->renderPartial('_blocks/_b-header',
        [
            'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Form header create' : 'Form header update')
        ]
    ) ?>
    <div class="cols-row">
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->modeling,
                'elementId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Modeling[elementId]',
                        $model->standTechnicalData->modeling->elementId,
                        CHtml::listData(\Element::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>

        </div>
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->modeling,
                'showcaseId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Modeling[showcaseId]',
                        $model->standTechnicalData->modeling->showcaseId,
                        CHtml::listData(\Showcase::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>

        </div>
        <div class="col-md-4">

            <?php
            echo TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_DROPDOWNLIST,
                $model->standTechnicalData->modeling,
                'technicId',
                array(
                    'groupOptions' => array(
                        'class' => 'js-drop-down-list drop-down-list',
                    ),
                    'labelOptions' => array(
                        'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                    ),
                    'input' => TbHtml::dropDownList(
                        'Modeling[technicId]',
                        $model->standTechnicalData->modeling->technicId,
                        CHtml::listData(\Technic::model()->findAll(), 'id', 'name')
                    ),
                )
            );
            ?>

        </div>
    </div>
    <div class="cols-row">
        <div class="col-md-4">
            <div class="cols-row">
                <div class="col-md-12">

                    <?php
                    echo TbHtml::activeControlGroup(
                        TbHtml::INPUT_TYPE_DROPDOWNLIST,
                        $model,
                        'active',
                        array(
                            'groupOptions' => array(
                                'class' => 'js-drop-down-list drop-down-list',
                            ),
                            'labelOptions' => array(
                                'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
                            ),
                            'input' => TbHtml::dropDownList(
                                'Stand[active]',
                                $model->active,
                                \Stand::getActiveList()
                            ),
                        )
                    );
                    ?>

                </div>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>
    <div class="cols-row">
        <div class="col-md-4">
            <div class="cols-row">
                <div class="col-md-6">
                    <button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
                        <?= Yii::t('AdminModule.admin', 'Save') ?>
                    </button>
                </div>
                <div class="col-md-6">
                    <a href="<?= $this->createUrl('admin') ?>"
                       class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
                        <?= Yii::t('AdminModule.admin', 'Cancel') ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>
</div>

</form>