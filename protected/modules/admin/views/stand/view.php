<?php
/**
 * @var CController $this
 * @var Stand $model
 * @var CActiveDataProvider $dataProviderConcept
 * @var string $requestUrlConcept
 * @var CWebApplication $app
 * @var string $requestUrl
 */
$app = Yii::app();
$cs = $app->clientScript;
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

// Slick
$next = "<button type='button' class='slick-next'></button><span class='arrow-next-img'></span>";
$prev = "<button type='button' class='slick-prev'></button><span class='arrow-prev-img'></span>";
$publishUrl4Slick = $am->publish($app->basePath . '/vendor/bower/slick-carousel/slick');
$cs->registerScriptFile($publishUrl4Slick . '/slick.min.js');
$cs->registerScript('Slick',
    '$(".responsive").slick({
		infinite: false,
		slidesToShow: 3,
		autoplay: false,
		nextArrow: "' . $next . '",
		prevArrow: "' . $prev . '",
		autoplaySpeed: 5000,
		speed: 300,
		arrows: true,
		centerMode: false,
		variableWidth: true
	});' .
    '$(".responsive").on("beforeChange", function(event, slick, currentSlide, nextSlide){
		var slideCount = slick.slideCount;
		var listWidth = slick.listWidth;
		var widthOfImages = 0;
		$.each(slick.$slides, function(k, v){
			if(k >= nextSlide ){
				widthOfImages = +widthOfImages + +v.clientWidth;
			}
		});

		if(widthOfImages <= listWidth){
			slick.$nextArrow.hide();
		}else{
			slick.$nextArrow.show();
		}
	});',
    CClientScript::POS_READY
);
$cs->registerCssFile($publishUrl4Slick . '/slick.css');
$cs->registerCssFile($publishUrl4Slick . '/slick-theme.css');

$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');
$cs->registerScriptFile($publishUrl . '/js/requestPrice/requestPrice.js');
// FancyBox
$publishUrl4FancyBox = $am->publish($app->basePath . '/vendor/bower/fancybox/source');
$cs->registerScriptFile($publishUrl4FancyBox . '/jquery.fancybox.pack.js');
$cs->registerScript('fancyBox', '$(".img-gall, .item").fancybox({padding: 5});', CClientScript::POS_READY);
$cs->registerCssFile($publishUrl4FancyBox . '/jquery.fancybox.css');

$cs->registerCssFile($am->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/css/stand/view.css'));
$cs->registerCssFile($publishUrl . '/css/stand/view.css');

$userId = Yii::app()->user->id;
$portfolioUserId = null;

Yii::import('application.modules.stand.models.Portfolio');
if ($model->portfolio) {
    $portfolioUserId = $model->portfolio->userId;
}

$this->widget('application.modules.admin.components.SimpleTooltip', [
    'coordinates' => ['top' => 0, 'left' => '170px', 'width' => '180%'],
    'items' => [
        [
            'target' => 'planner-5d',
            'content' => '
				<h2>Полезная информация</h2>
				Появляется при наведении курсора на соответствующие поле с возможностью копирования текста.
				 Возможно будет выбор из предложенных вариантов.'
        ]
    ]
]);
?>

    <div class="page-stand-view-wrapper">
        <?= CHtml::activeHiddenField($model, 'id'); ?>
        <div class="cols-row">
            <div class="col-md-8">
                <?= $this->renderPartial('_blocks/_b-header', array(
                    'header' => Yii::t('AdminModule.admin', 'Viewing stand'),
                    'description' => $model->name,
                )) ?>
            </div>
            <div class="col-md-4 page-stand-view__header-button">
                <?php
                //@TODO refactor

                $dataToggle = '';
                $dataTarget = '';
                if(Yii::app()->user->isGuest){
                    $dataToggle = 'modal';
                    $dataTarget = '#modal-sign-up';
                }
                if(!Yii::app()->user->isGuest && !User::checkTariff(1)){
                    $dataToggle = 'modal';
                    $dataTarget = '#modal-tariff-insufficient';
                }
                if(!Yii::app()->user->isGuest && User::checkTariff(1)){
                    $dataToggle = '';
                    $dataTarget = '';
                }
                ?>
            </div>
        </div>
        <div class="cols-row">

            <div class="col-md-6 page-stand__gallery">
                <div class="cols-row">
                    <div class="col-md-12 first-image">
                    </div>
                </div>
                <div class="cols-row">
                    <div class="col-md-12 page-stand__gallery--last-images responsive">


                    </div>
                </div>
                <?php if (null != $model->plannerHash): ?>
                    <div class="cols-row page-stand__planner-5d">
                        <div class="col-md-4">
                            <?= CHtml::link(
                                '5D planner',
                                $this->createUrl('stand/planner5d', array('hash' => $model->plannerHash)),
                                array(
                                    'class' => 'b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn',
                                    'data-toggle-tooltip' => 'planner-5d'
                                )
                            ) ?>
                        </div>
                        <div class="col-md-8"></div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-md-6 page-stand__parameters">
                <h2><?= Yii::t('AdminModule.admin', 'Brief description and specifications') ?></h2>
                <?php
                if (null !== $model) {
                    $this->widget(
                        'StandDetailView',
                        array(
                            'data' => $model,
                            'htmlOptions' => array(
                                'class' => 'b-detail-view__table--simple',
                            ),
                        )
                    );
                } else {
                    echo Yii::t('AdminModule.admin', 'Information not available');
                }; ?>
                <h2>Оформление</h2>
                <?php if (isset($model->standDecor)) {
                    $this->widget(
                        'application.widgets.StandDecorDetailView',
                        array(
                            'data' => $model->standDecor,
                            'htmlOptions' => array(
                                'class' => 'b-detail-view__table--simple',
                            ),
                        )
                    );
                } else {
                    echo Yii::t('AdminModule.admin', 'Information not available');
                }
                ?>
            </div>
        </div>

        <div class="cols-row page-stand-view__actions">
            <div class="col-md-2">
                <?=
                $app->user->getState('role') == User::ROLE_ADMIN
                || $userId == $portfolioUserId ? TbHtml::linkButton('  ' .
                    Yii::t('AdminModule.admin', 'Edit'), array(
                    'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
                    'url' => $this->createUrl('admin/stand/save', array('id' => $model->id))
                )) : null ?>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>
            <div class="col-md-2">
                <?= TbHtml::linkButton('  ' .
                    Yii::t('AdminModule.admin', 'Check rates'),
                    array(
                        'class' => 'b-button d-bg-info-dark d-bg-info--hover d-text-light btn',
                        'data-toggle' => 'modal',
                        'data-target' => '#request-prise',
                        'url' => '#')
                ); ?>
            </div>
            <div class="col-md-2">
                <?= Yii::app()->user->getState('role') == User::ROLE_FREELANCER || Yii::app()->user->getState('role') == User::ROLE_DEVELOPER ? null :
                    TbHtml::linkButton('  ' .
                        Yii::t('AdminModule.admin', 'Create TK'),
                        array(
                            'class' => 'b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn',
                            'url' => $this->createUrl('tz/Tz/create'))
                    ) ?>
            </div>
        </div>
        <hr />
    </div>

<?php
$this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'request-prise',
    'closeText' => '<i class="icon icon--white-close"></i>',
    'show' => isset($_GET['modal']) && $_GET['modal'] == 'show',
    'htmlOptions' => array(
        'class' => 'modal--sign-up'
    ),
    'header' => $this->renderPartial('application.modules.stand.views.default.partial._requestPriceHeader', array(), true),
    'content' => $this->renderPartial('application.modules.stand.views.default.partial._requestPriceContent', array('publishUrl' => $publishUrl, 'model' => $model), true),
));
?>
    <button id="after-request-price-button" class="hidden" data-toggle="modal"
            data-target="#after-request-price"></button>
<?php
$this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'after-request-price',
    'closeText' => '<i class="icon icon--white-close"></i>',
    'htmlOptions' => array(
        'class' => 'modal--sign-up'
    ),
    'header' => $this->renderPartial('application.modules.stand.views.default.partial._afterRequestPriceHeader', array(), true),
    'content' => $this->renderPartial('application.modules.stand.views.default.partial._afterRequestPriceContent', array('publishUrl' => $publishUrl, 'model' => $model), true),
));