<?php
Yii::import('application.modules.fair.FairModule');
/**
 * Created by PhpStorm.
 * User: Rendol
 * Date: 22.04.2015
 * Time: 9:21
 */

/**
 * @var CController $this
 * @var string $requestUrl
 * @var string $itemView
 * @var string $switcherType
 * @var string $name
 * @var array $sortableAttributes
 * @var bool $enableNextPageButton
 * @var CWebApplication $app
 * @var bool $notFoundVisible
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/raty/raty.js', CClientScript::POS_END);

if (!isset($notFoundVisible)) {
    $notFoundVisible = true;
}
if (empty($switcherType)) {
    $switcherType = 'block';
}
if (empty($name)) {
    $name = 'default';
}
if (empty($sortableAttributes)) {
    $sortableAttributes = [];
}
if (empty($queryParams)) {
    $queryParams = [];
}

$curPage = $dataProvider->getPagination()->getCurrentPage(false);
$nextPage = $curPage + 1;
$nextPageUrl = '';
if (empty($nextPageExist)) {
    $nextPageExist = $nextPage * $dataProvider->getPagination()->getPageSize() < $dataProvider->totalItemCount;
}

if (!isset($enableNextPageButton)) {
    $enableNextPageButton = true;
}
$url = parse_url($requestUrl);
parse_str(isset($url['query']) ? $url['query'] : '', $params);
$nextPageUrl = $url['path'] . '?' . http_build_query(['page' => $nextPage] + $params);
?>

<div class="b-switch-list-view" data-request='<?= json_encode(['page' => $curPage] + $queryParams) ?>'
     data-ctrl='<?= Yii::app()->controller->id ?>'>
    <div class="b-search-loader-block">
        <?= TbHtml::image($publishUrl . '/img/loader/ajax-loader.gif', 'Загрузка', ['class' => 'js-page-search__loader', 'title' => 'Загрузка']) ?>
    </div>
    <?php
    if ($dataProvider->getTotalItemCount() != 0) : ?>

        <?php if (isset($searches) && !empty($searches)): ?>
            <div class="b-switch-list-view__items">
                <?php foreach ($searches as $searchCategory => $search): ?>
                    <h2 class="f-h3 f-head-search-result-2">
                        <span itemscope itemtype="http://schema.org/Service">
                            <span itemprop="description">
                        <?php
                        if (!empty($searchCategory)) {
                            echo $searchCategory;
                        }

                        ?>
                            </span>
                            </span>
                    </h2>
                    <?php
                    $this->widget('application.modules.fair.components.SearchListView', [
                        'dataProvider' => $dataProvider,
                        'afterAjaxUpdate' => 'js:function() { FairMatch(); }',
                        'htmlOptions' => [
                            'class' => 'js-updated-block b-switch-list-view js-switch-list-view-' . $name . ' b-switch-list-view--as-' . $switcherType . ' lasss-' . md5($searchCategory),
                            'data-switcher' => 'js-switch-list-view-' . $name,
                        ],
                        'sorterCssClass' => 'b-switch-list-view__sorter',
                        'sortableAttributes' => $sortableAttributes + [''],
                        'itemView' => $itemView,
                        'template' => "{sorter}\n{items}",
                    ]);
                    ?>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <?php
            $this->widget('application.modules.fair.components.SearchListView', [
                'dataProvider' => $dataProvider,
                'afterAjaxUpdate' => 'js:function() { FairMatch(); }',
                'htmlOptions' => [
                    'class' => 'b-switch-list-view__items js-updated-block b-switch-list-view js-switch-list-view-' . $name . ' b-switch-list-view--as-' . $switcherType,
                    'data-switcher' => 'js-switch-list-view-' . $name,
                ],
                'sorterCssClass' => 'b-switch-list-view__sorter',
                'sortableAttributes' => $sortableAttributes + [''],
                'itemView' => $itemView,
                'template' => "{sorter}\n{items}",
                'viewData' => [
                    'fairMatches'  => isset($fairMatches) ? $fairMatches : [],
                ],
            ]);
            ?>
        <?php endif; ?>


        <?php if ($enableNextPageButton) : ?>
            <div class="b-switch-list-view__add-items-wrap">
                <?php if ($nextPageExist) : ?>
                    <?= CHtml::button(
                        Yii::t('FairModule.fair', 'Search result more'),
                        [
                            'data-url' => $nextPageUrl,
                            'class' => 'b-switch-list-view__add-items b-button d-bg-base d-bg-bordered d-bg-input--hover d-text-dark btn'
                        ]
                    ) ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php elseif ($notFoundVisible == true) : ?>
        <div class="not-found">
                <h3 class="text-center"><?= Yii::t('FairModule.fair', 'Unfortunately, upon request') ?>
                    <?= isset($_GET['query']) ? '«' . $_GET['query'] . '»' : null ?> <?= Yii::t('FairModule.fair', 'there is nothing') ?>
                    .<br/>
                    <?= Yii::t('FairModule.fair', 'Enter into the search box or try a new query filters') ?>.
                </h3>
        </div>
    <?php endif; ?>
</div>