<?php
///* @var $this ProductController */
///* @var $model Product */
///* @var $form CActiveForm */
//
//
//
//$app = Yii::app();
//$am = $app->getAssetManager();
//$cs = $app->getClientScript();
//$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
//
//$cs->registerScriptFile($publishUrl . '/js/blog/confirm-delete-article.js', CClientScript::POS_END);
//?>
<!---->
<?php
	echo TbHtml::linkButton('Назад', array(
		'class' => 'btn pull-right btn-warning',
		'style' => 'margin:10px 50px 0px 0px;',
		'url' => $this->createUrl('index'),
	));
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'MassMedia-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'action' => $this->createUrl('MassMedia/save'),
));

?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>85,'maxlength'=>500, 'style' => 'width:400px;')); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'industryId'); ?>
		<?php echo $form->textArea($model,'industryId', array('style'=>'width:400px;height:140px;')); ?>
		<?php echo $form->error($model,'industryId'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'massMediaId'); ?>
		<?php echo $form->textField($model,'massMediaId'); ?>
		<?php echo $form->error($model,'massMediaId'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'isGeneral'); ?>
		<?php echo $form->textField($model,'isGeneral'); ?>
		<?php echo $form->error($model,'isGeneral'); ?>
	</div>

	<div style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>


