<?php
/**
 * @var MassMediaController $this
 * @var MassMedia $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

//$cs->registerScriptFile($publishUrl . '/js/blog/confirm-delete-article.js', CClientScript::POS_END);
?>

<?php
echo TbHtml::linkButton('Назад', array(
	'class' => 'btn pull-right btn-warning',
	'style' => 'margin:10px 50px 0px 0px;',
	'url' => $this->createUrl('MassMedia/index'),
));
?>

<div class="col-md-11">
	<?php
	$formClass = get_class($model) . 'Form';
	Yii::import('application.modules.admin.components.' . $formClass);
	$ctrl = $this;
	$ctrl->widget('application.modules.admin.components.FieldSetView', array(
		'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Form header create' : 'Form header update'),
		'items' => array(
			array(
				'application.modules.admin.components.FormView',
				'form' => new $formClass(
					array(
						'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
						'enctype' => 'multipart/form-data',
						//'elementGroupName' => '[]'
					),
					$model, $ctrl
				),
				'items' => array(
					array(
						'application.modules.admin.components.HtmlView',
						'content' => function ($owner) use ($model) {
							if ($model->image && $model->image->getFileUrl()) {
								echo TbHtml::thumbnails(
									H::getThumbnailsArray(
										array($model), $model->id, 'image', null,
										TbHtml::linkButton(
											Yii::t('AdminModule.admin', 'delete'),
											array(
												'color' => TbHtml::BUTTON_COLOR_DANGER,
												'size' => TbHtml::BUTTON_SIZE_MINI,
												'url' => $owner->createUrl('deleteMainImage', array('id' => $model->id, 'fileId' => '{fileId}'))
											)
										)
									)
								);
							} else {
								$owner->widget('application.modules.admin.components.FieldSetView', array(
										'header' => Yii::t('AdminModule.admin', 'download Image (title picture)'),
										'items' => array(
											array(
												'CMultiFileUpload',
												'model' => $model,
												'htmlOptions' => array(
													'style' => 'margin-left:80px'
												),
												'max' => 1,
												'attribute' => 'image',
												'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
												'accept' => 'gif|jpg|jpeg|png',
												'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
											)
										)
									)
								);
							}
						},
					),

					array(
						'application.modules.admin.components.ActionsView',
						'items' => array(
							'submit' => array(
								'type' => TbHtml::BUTTON_TYPE_SUBMIT,
								'label' => Yii::t('AdminModule.admin', 'Применить'),
								'attributes' => array(
									'color' => TbHtml::BUTTON_COLOR_SUCCESS
								)
							),
							'cancel' => array(
								'type' => TbHtml::BUTTON_TYPE_LINK,
								'label' => Yii::t('AdminModule.admin', 'Отмена'),
								'attributes' => array(
									'url' => $this->createUrl('index'),
									'color' => TbHtml::BUTTON_COLOR_DANGER
								)
							),
							'delete' => array(
								'type' => TbHtml::BUTTON_TYPE_LINK,
								'label' => Yii::t('AdminModule.admin', 'Удалить'),
								//'visible' => !$model->isNewRecord,
								'attributes' => array(
									'url' => $this->createUrl('deleteMassMedia', ['id' => $model->id]),
									'color' => TbHtml::BUTTON_COLOR_WARNING,
									'class' => 'delete-article',
								)
							)
						)
					),
				),
			),
		)
	));
	?>
</div>

