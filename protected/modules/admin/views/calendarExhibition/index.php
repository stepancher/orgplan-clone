<?php
/**
 * @var CalendarExhibitionController $this
 * @var CalendarExhibition $model
 */
?>
<div class="cols-row">
    <div class="col-md-12">
        <fieldset>
            <legend>Управление базовыми задачами</legend>
            <div class="cols-row">
                <div class="col-md-2">
                    <?=CHtml::link(
                        'Создать',
                        Yii::app()->createUrl('admin/calendarExhibition/save'),
                        ['class' => 'btn b-button d-bg-success-dark d-bg-success--hover d-text-light'])?>
                </div>
                <div class="col-md-10"></div>
            </div>
            <div class="cols-row">
                <div class="col-md-12">
                    <?php
                    $sort = new CSort();
                    $sort->attributes = array(
                        'defaultOrder'=>'t.date DESC',
                        'id'=>array(
                            'asc'=>'t.id',
                            'desc'=>'t.id DESC',
                        ),
                        'name'=>array(
                            'asc'=>'t.name',
                            'desc'=>'t.name DESC',
                        ),
                        'pageTitle'=>array(
                            'asc'=>'page.title',
                            'desc'=>'page.title desc',
                        ),
                    );

                    $dataProvider = new CActiveDataProvider('CalendarExhibition', [
                        'sort' => [
                            'attributes' => [
                                'name',
                                'count',
                                'term',
                                'links',
                                'fairId',
                            ],
                        ],
                    ]);

                    $this->widget('zii.widgets.grid.CGridView', array(
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns'=>array(
                            'name',
                            [
                                'name' => 'beginDate',
                                'filter' => CalendarExhibition::date(),
                                'value' => function($data) {
                                    return !empty($data->beginDate) && isset(CalendarExhibition::date()[$data->beginDate])
                                        ? CalendarExhibition::date()[$data->beginDate] : null;
                                }
                            ],
                            'count',
                            'term',
                            'links',
                            [
                                'name' => 'fairId',
                                'value' => function($data) {
                                    return !empty($data->fairId) ? Fair::model()->findByPk($data->fairId)->name : null;
                                },
//                                'filter' => Fair::model()->findAll(),
                            ],
                            array(
                                'class'=>'CButtonColumn',
                                'template' => '{view} {update} {delete}',
                                'buttons' => [
                                    'update' => [
                                        'url' => function($data) {
                                            return Yii::app()->createUrl('admin/calendarExhibition/save', ['id' => $data->id]);
                                        }
                                    ]
                                ]
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </fieldset>
    </div>
</div>


