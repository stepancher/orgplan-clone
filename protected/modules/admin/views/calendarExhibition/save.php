<?php
/**
 * @var CalendarExhibitionController $this
 * @var CalendarExhibition $model
 */
?>
<div class="cols-row">
    <div class="col-md-12">
        <fieldset>
            <legend><?= $model->isNewRecord ? 'Добавление ' : 'Редактирование ' ?>базовой задачи <?=$model->isNewRecord ? '' : '#'.$model->id;?></legend>
            <?= CHtml::beginForm('', '',
                [
                    'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                    'class' => 'form-horizontal dropdown-end'
                ]
            ); ?>
            <?= TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_TEXT,
                $model,
                'name',
                [
                    'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark'
                ]
            ); ?>

            <div class="control-group">
                <label class="control-label" for="CalendarExhibition_beginDate">
                    <?=$model->getAttributeLabel('beginDate')?>
                </label>
                <div class="controls">
                    <?= $this->widget('application.modules.admin.components.ListAutoComplete', [
                        'model' => $model,
                        'attribute' => 'beginDate',
                        'selectedItems' => [$model->beginDate],
                        'multiple' => false,
                        'autoComplete' => false,
                        'ajaxAutoComplete' => false,
                        'showNotEmptyLabel' => false,
                        'showLabel' => false,
                        'label' => 'Выбрать',
                        'htmlOptions' => [
                            'labelOptions' => [
                                'class' => 'd-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark'
                            ],
                        ],
                        'itemOptions' => [
                            'data' => CalendarExhibition::date(),
                            'text' => 'name',
                        ]
                    ], true)?>
                </div>
            </div>
            <?= TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_NUMBER,
                $model,
                'count',
                [
                    'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark'
                ]
            ); ?>
            <?= TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_NUMBER,
                $model,
                'term',
                [
                    'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark'
                ]
            ); ?>
            <?= TbHtml::activeControlGroup(
                TbHtml::INPUT_TYPE_TEXT,
                $model,
                'links',
                [
                    'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark'
                ]
            ); ?>

            <div class="control-group">
                <label class="control-label" for="CalendarExhibition_beginDate">
                    <?=$model->getAttributeLabel('fairId')?>
                </label>
                <div class="controls">
                    <?= $this->widget('ext.widgets.ListAutoComplete', [
                        'model' => $model,
                        'attribute' => 'fairId',
                        'selectedItems' => [$model->fairId],
                        'multiple' => false,
                        'autoComplete' => true,
                        'ajaxAutoComplete' => false,
                        'showNotEmptyLabel' => false,
                        'showLabel' => false,
                        'label' => 'Выбрать',
                        'htmlOptions' => [
                            'labelOptions' => [
                                'class' => 'd-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark'
                            ],
                        ],
                        'itemOptions' => [
                            'data' => ['' => '-= Для любой выставки =-'] + CHtml::listData(Fair::model()->findAll(), 'id', 'name'),
                            'text' => 'name',
                        ]
                    ], true)?>
                </div>
            </div>
                <div class="controls">
                    <div class="cols-row">
                        <div class="col-md-2">
                            <button type="submit" class="btn b-button d-bg-success-dark d-bg-success--hover d-text-light">
                                Сохранить
                            </button>
                        </div>
                        <div class="col-md-2">
                            <?=CHtml::link(
                                'Отмена',
                                Yii::app()->createUrl('admin/calendarExhibition/index'),
                                [
                                    'class' => 'b-button b-button--large-offset d-bg-primary-dark d-bg-danger--hover d-text-light btn'
                                ]
                            )?>
                        </div>
                    </div>
                </div>
            </div>
            <?= CHtml::endForm(); ?>
        </fieldset>
    </div>
</div>