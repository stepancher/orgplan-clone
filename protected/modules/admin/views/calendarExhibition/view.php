<?php
/**
 * @var CalendarExhibitionController $this
 * @var CalendarExhibition $model
 */
?>
<div class="cols-row">
    <div class="col-md-12">
        <fieldset>
            <legend>Просмотр базовой задачи #<?=$model->id?></legend>
            <div class="cols-row">
                <div class="col-md-12">
                    <?=CHtml::link('Назад', Yii::app()->createUrl('admin/calendarExhibition/index'),
                        [
                            'class' => 'b-button b-button--large-offset d-bg-primary-dark d-bg-danger--hover d-text-light btn'
                        ]
                    )?>
                </div>
            </div>
            <div class="cols-row">
                <div class="col-md-12">
                    <?php $this->widget('zii.widgets.CDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'name',
                            [
                                'name' => 'beginDate',
                                'value' => !empty($model->beginDate) && isset(CalendarExhibition::date()[$model->beginDate])
                                    ? CalendarExhibition::date()[$model->beginDate] : null
                            ],
                            'count',
                            'term',
                            'links',
                            [
                                'name' => 'fair',
                                'value' => function($data) {
                                    return !empty($data->fair) ? $data->fair->name : null;
                                }
                            ],
                        ),
                    )); ?>
                </div>
            </div>
        </fieldset>
    </div>
</div>

