<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
use yii\helpers\ArrayHelper;
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/city/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

    <?php
echo $this->widget('application.modules.admin.components.ListAutoComplete',
        ArrayHelper::merge(
            [
                'name' => 'City[regionId]',
                'attribute' => 'regionId',
            ],
            [
                'model' => $model,
                'selectedItems' => [$model->regionId],
                'multiple' => FALSE,
                'htmlOptions' => array(
                    'labelOptions' => array(
                        'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
                    ),
                    'data-input-id' => 'regionId',
                ),
                'itemOptions' => [
                    'data' => array('' => 'Не выбрано') + CHtml::listData(Region::model()->findAll(), 'id', 'name'),
                ],
                'label' => NULL,
            ]
        ),
        true);
    ?>

    <div class="form-group field-city-name">
        <label class="control-label" for="City_name"><?= Yii::t('AdminModule.admin', 'name') ?></label>
        <input type="text" id="City_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="City[name]" value="<?= $model->name ?>">
        <div class="help-block"></div>
    </div>

    <div class="form-group field-city-shorturl">
        <label class="control-label" for="City_shortUrl"><?= Yii::t('AdminModule.admin', 'short url') ?></label>
        <input type="text" id="City_shortUrl" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="City[shortUrl]" value="<?= $model->shortUrl ?>">
        <div class="help-block"></div>
    </div>

    <div class="cols-row">
        <div class="col-md-12">
            <button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
               class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Cancel') ?>
            </a>
        </div>
    </div>
</form>