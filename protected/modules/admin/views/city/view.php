<?php
/* @var $this CityController */
/* @var $model City */
?>

<?= CHtml::link('К списку', Yii::app()->createUrl('admin/city/admin'), ['class' => 'btn btn-success']) ?>

<fieldset>
    <legend><?= Yii::t('AdminModule.admin', 'View city #') . $model->id; ?></legend>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            [
                'label' => 'Регион',
                'name' => 'region.name'
            ],
            'name',
            'shortUrl',
        ),
    )); ?>
</fieldset>