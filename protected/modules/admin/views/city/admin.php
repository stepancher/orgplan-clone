<?php
/* @var $this CityController */
/* @var $model City */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#city-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?= Yii::t('AdminModule.admin', 'Manage City')?></h1>
<?=CHtml::link('Создать', Yii::app()->createUrl('admin/city/create'), ['class' => 'btn btn-success'])?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'city-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'region_name' => [
            'name' => 'region_name',
            'value' => function ($data) {
                if ($data->region) {
                    return $data->region->name;
                }
            }
        ],
        'name' => [
            'name' => 'name',
            'value' => function($data){
                return $data->name;
            }
        ],
        'shortUrl',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
