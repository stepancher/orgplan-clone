<?php
/**
 * @var CController $this
 * @var AR $model
 * @var CWebApplication $app
 */
$ctrl = $this;
$table = ChartTable::model()->findByPk($model->tableId);

$back = array(
    TbHtml::BUTTON_TYPE_LINK,
    Yii::t('AdminModule.admin', 'К списку таблиц всех отраслей'),
    array(
        'url' => $this->createUrl('admin/chartTable/index'),
        'color' => TbHtml::BUTTON_COLOR_DANGER
    )
);

if(!empty($table)){
    $industry = Industry::model()->findByPk($table->industryId);

    $back = array(
        TbHtml::BUTTON_TYPE_LINK,
        Yii::t('AdminModule.admin', 'К списку таблиц отрасли'),
        array(
            'url' => $this->createUrl('admin/chartTable/index', array('id' =>$industry->id)),
            'color' => TbHtml::BUTTON_COLOR_DANGER
        )
    );
}

$ctrl->widget('application.modules.admin.components.FieldSetView', array(
    'header' => Yii::t('AdminModule.admin', 'Колонки таблиц отраслей'),
    'items' => array(
        array(
            'application.modules.admin.components.ActionsView',
            'items' => array(
                'add' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('AdminModule.admin', 'добавить колонку'),
                    array(
                        'url' => $this->createUrl('admin/ChartColumn/save', array('tableId' => $model->tableId)),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS
                    )
                ),
                'back' => $back,
            )
        ),
        array(
            'application.modules.admin.components.ChartColumnGridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{show}&#160;{edit}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'show' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('view', array('id' => $data->id));
                            },
                            'options' => array('title' => 'Информация', 'class' => 'btn btn-small btn-success'),
                        ),
                        'edit' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('save', array('id' => $data->id));
                            },
                            'options' => array('title' => 'Редактирование', 'class' => 'btn btn-small btn-primary'),
                        ),
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('delete', array('id' => $data->id));
                            },
                            'options' => array('title' => 'Удалить', 'class' => 'btn btn-small btn-danger'),
                        ),
                    )
                )
            )
        ),
    )
));
