<?php
/**
 * @var CController $this
 * @var Task $model
 */
$ctrl = $this;
$fileIds = array(0);
$ctrl->widget('application.modules.admin.components.ObjectFileGridView', array(
	'model' => ObjectFile::model(),
	'compare' => array(
		array('t.id', $fileIds),
		array('t.type', ObjectFile::TYPE_TASK_FILES)
	),
	'columnsAppend' => array(
		'buttons' => array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
			'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
			'buttons' => array(
				'delete' => array(
					'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
					'url' => function ($data) use ($ctrl, $model) {
							return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
						},
					'options' => array('title' => 'Информация', 'class' => 'btn btn-small btn-danger'),
				),
			)
		)
	)
));
