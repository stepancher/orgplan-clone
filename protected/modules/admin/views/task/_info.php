<?php
/**
 * @var CController $this
 * @var Task $model
 */
$this->widget(
	'application.widgets.TaskDetailView',
	array(
		'data' => $model,
	)
);