<?php
/**
 * @var CController $this
 * @var Event $model
 */
$this->widget(
	'application.modules.admin.components.FieldSetView',
	array(
		'header' => Yii::t('AdminModule.admin', 'Detail header'),
		'items' => array(
			array(
				'application.modules.admin.components.ActionsView',
				'items' => array(
					'cancel' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('AdminModule.admin', 'Detail action cancel'),
						'attributes' => array(
							'url' => $this->createUrl('calendar/view', array('id' => $model->calendarId)),
							'color' => TbHtml::BUTTON_COLOR_WARNING
						)
					),
					'edit' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('AdminModule.admin', 'Detail action edit'),
						'attributes' => array(
							'url' => $this->createUrl('save', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_PRIMARY
						)
					)
				)
			),
			array(
				'bootstrap.widgets.TbTabs',
				'tabs' => array(
					array(
						'label' => 'Данные о задаче',
						'content' => $this->renderPartial('_info', array('model' => $model), true),
						'active' => true
					),
					array(
						'label' => 'Файлы',
						'content' => $this->renderPartial('_file', array('model' => $model), true),
					),
				)
			)
		)
	)
);