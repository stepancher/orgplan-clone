<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login')); ?>:</b>
	<?php echo CHtml::encode($data->login); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middleName')); ?>:</b>
	<?php echo CHtml::encode($data->middleName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<?php echo CHtml::encode($data->gender); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo CHtml::encode($data->dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logoId')); ?>:</b>
	<?php echo CHtml::encode($data->logoId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cityId')); ?>:</b>
	<?php echo CHtml::encode($data->cityId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactId')); ?>:</b>
	<?php echo CHtml::encode($data->contactId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?>:</b>
	<?php echo CHtml::encode($data->modified); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balance')); ?>:</b>
	<?php echo CHtml::encode($data->balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('legalEntityId')); ?>:</b>
	<?php echo CHtml::encode($data->legalEntityId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bankAccountId')); ?>:</b>
	<?php echo CHtml::encode($data->bankAccountId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('banned')); ?>:</b>
	<?php echo CHtml::encode($data->banned); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastEntry')); ?>:</b>
	<?php echo CHtml::encode($data->lastEntry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tariffId')); ?>:</b>
	<?php echo CHtml::encode($data->tariffId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calculatorData')); ?>:</b>
	<?php echo CHtml::encode($data->calculatorData); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post')); ?>:</b>
	<?php echo CHtml::encode($data->post); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('googleAPI')); ?>:</b>
	<?php echo CHtml::encode($data->googleAPI); ?>
	<br />

	*/ ?>

</div>