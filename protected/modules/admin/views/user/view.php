<?php
/* @var $this UserController */
/* @var $model User */
?>

<h1>Просмотр данных пользователя №<?php echo $model->id; ?></h1>

<?=CHtml::link('Вернуться к списку', Yii::app()->createUrl('admin/user/admin'), ['class' => 'btn btn-success'])?>
&nbsp;
<?=CHtml::link('Редактировать', Yii::app()->createUrl('admin/user/save', ['id' => $model->id]), ['class' => 'btn btn-warning'])?>
<br/>
<br/>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'login',
        'password',
        'firstName',
        'lastName',
        'middleName',
        [
            'name' => 'gender',
            'value' => function ($data) {
                return User::getGender($data->gender);
            }
        ],
        [
            'name' => 'dob',
			'value'=> strtotime('1970-01-01 00:00:00') < strtotime($model->dob) ? $model->dob : ' ',
        ],
        [
            'name' => 'logoId',
            'value' => isset($model->logo) ? CHtml::image($model->logo->getFileUrl(), '', ['style' => 'height: 200px;']) : Yii::t('AdminModule.admin', 'Not set'),
            'type' => 'html'
        ],
        [
            'name' => 'role',
            'value' => User::getRole($model->role)
        ],
        [
            'name' => 'cityId',
            'value' => isset($model->city) ? $model->city->name : ''
        ],
        [
            'name' => 'contactId',
            'value' => isset($model->contact) ? $this->renderPartial('_contactView', ['model' => $model->contact], true) : Yii::t('AdminModule.admin', 'Not set'),
            'type' => 'html'
        ],
        [
            'name' => 'active',
            'value' => User::getActiveStatus($model->active)
        ],
        'modified',
        'created',
        'description',
        'balance',
        [
            'name' => 'legalEntityId',
            'value' => isset($model->legalEntity) ? $model->legalEntity->shortName : ''
        ],
        'bankAccountId',
//        [
//            'name' => 'bankAccountId',
//            'value' => isset($model->bankAccount) ? $this->renderPartial('_bankAccountView', ['model' => $model->bankAccount], true) : Yii::t('AdminModule.admin', 'Not set'),
//            'type' => 'html'
//        ],
        [
            'name' => 'banned',
            'value' => User::getBannedStatus($model->banned)
        ],
        'lastEntry',
        [
            'name' => 'tariffId',
            'value' => isset($model->tariff) ? $model->tariff->name : Yii::t('AdminModule.admin', 'Not set')
        ],
        'calculatorData',
        'post',
        'googleAPI',
    ),
)); ?>
