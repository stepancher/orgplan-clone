<?php
/**
 * @var $this UserController
 * @var $model User
 *
 * @var $pageVariants array
 * @var $pageSize int
 *
 * @var $fields array
 * @var $selectedFields array
 */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#User-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#grid-button').on('click', function () {

    console.log('grid-button');

    var \$fields = $('#User_fields .checkbox.checked input');
    var pageSize = $('#User_pageSize .drop-list__item--selected input').val();
    var fields = \$fields.map(function() {
        return $(this).val();
    }).get();
    $.fn.yiiGridView.update('grid', {data: {
        User_fields: fields.toString(),
        User_pageSize: pageSize,
        User_page: 1
    }})
});
");
?>
<style>
	#User_pageSize .drop-list,
	#User_pageSize .slimScrollDiv,
	#User_fields .drop-list,
	#User_fields .slimScrollDiv {
		height: 220px !important;
		max-height: 220px !important;
	}

	#User_pageSize .list-auto-complete__main-list,
	#User_fields .list-auto-complete__main-list {
		height: 192px !important;
		max-height: 192px !important;
	}
</style>

<h1>Управление пользователями</h1>
<div style="height: 40px;">
	<?= TbHtml::linkButton(
		'Добавить нового пользователя',
		[
			'url'   => $this->createUrl('save'),
			'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
			'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;"
		]
	); ?>

	<div style="float: right;">

		<div style="width: 180px; display: inline-block;">
			<?php $this->widget('application.modules.admin.components.ListAutoComplete', [
				'id'    => 'User_pageSize',
				'name'  => 'User_pageSize',
				'label' => 'На странице',
				'itemOptions' => [
					'data' => $pageVariants
				],
				'selectedItems' => [$pageSize],
				'htmlOptions' => [
					'style' => 'margin: 0;'
				],
				'autoComplete' => false,
				'multiple' => false
			]); ?>
		</div>

		<div style="width: 250px; display: inline-block;">
			<?php $this->widget('application.modules.admin.components.ListAutoComplete', [
				'id'    => 'User_fields',
				'name'  => 'User_fields',
				'label' => 'Поля формы',
				'itemOptions' => [
					'data' => $fields
				],
				'selectedItems' => array_keys($selectedFields) ?: [],
				'htmlOptions' => [
					'style' => 'margin: 0;'
				],
				'autoComplete' => false
			]); ?>
		</div>

		<div style="display: inline-block;">
			<?= TbHtml::button('Применить', [
				'id'    => 'grid-button',
				'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
				'style' => 'display: block; margin: 0;'
			]); ?>
		</div>

	</div>
</div>

<div style="clear: both;"></div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'      => 'grid',
	'columns' => array_merge([
		'id',
		[
			'class' => 'CButtonColumn',
			'headerHtmlOptions' => array(
				'style' => "width: 16px;"
			),
			'htmlOptions' => array(
				'style' => "width: 16px;"
			),
			'buttons' => [
				'update' => [
					'url' => 'Yii::app()->createUrl("admin/user/save", ["id" => $data->id])'
				]
			]
		]
	], $selectedFields),
	'dataProvider' => $model->searchOnAdmin($selectedFields, $pageSize),
	'filter'       => $model,
	'ajaxUpdate'   => false
)); ?>



<?php /*
<h1>Управление пользователями</h1>

<?= CHtml::link('Добавить пользователя', Yii::app()->createUrl('admin/user/save'), ['class' => 'btn btn-success'])?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->adminSearch(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'login',
		//'password',
		'firstName',
		'lastName',
		'middleName',
//		'gender',
//		'dob',
//		'logoId',
		[
			'name' => 'role',
			'value' => function ($data) {
				return User::getRole($data->role);
			},
			'filter' => CHtml::activeDropDownList($model, 'role', User::roles(), ['empty' => ''])
		],
		[
			'name' => 'cityId',
			'value' => function ($data) {
				return isset($data->city) ? $data->city->name : Yii::t('AdminModule.admin', 'Not set');
			}
		],
//		'contactId',
		[
			'name' => 'active',
			'value' => function ($data) {
				return User::getActiveStatus($data->active);
			},
			'filter' => CHtml::activeDropDownList($model, 'active', User::actives(), ['empty' => ''])
		],
//		'modified',
//		'created',
//		'description',
		'balance',
//		'legalEntityId',
//		'bankAccountId',
		[
			'name' => 'banned',
			'value' => function ($data) {
				return User::getBannedStatus($data->banned);
			},
			'filter' => CHtml::activeDropDownList($model, 'banned', User::bannes(), ['empty' => ''])
		],
//		'lastEntry',
		[
			'name' => 'tariffId',
			'value' => function ($data) {
				return isset($data->tariff) ? $data->tariff->name : '';
			},
		],
//		'calculatorData',
//		'post',
//		'googleAPI',
		array(
			'class'=>'CButtonColumn',
			'buttons' => [
				'update' => [
					'url' => 'Yii::app()->createUrl("admin/user/save", ["id" => $data->id])'
					]
			]
		),
	),
)); */ ?>
