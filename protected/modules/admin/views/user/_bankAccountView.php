<?php
/**
 * @var BankAccount $model
 */
?>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        //'id',
        'paymentAccount',
        'bank',
        'bankBik',
        'correspondentAccount',
    ),
)); ?>
