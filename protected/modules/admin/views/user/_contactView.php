<?php
/**
 * @var Contact $model
 */
?>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        //'id',
        'companyName',
        'post',
        'email',
        'phone',
        'workPhone',
    ),
)); ?>
