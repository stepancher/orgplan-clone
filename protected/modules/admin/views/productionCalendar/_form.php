<?php
/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendarDay */
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/ProductionCalendar/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

	<div class="form-group">
		<label class="control-label" for="ProductionCalendarDay_newHoliday"><?= Yii::t('AdminModule.admin', 'The name of the holiday') ?></label>
		<input type="text" id="ProductionCalendarDay_newHoliday" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ProductionCalendarDay[newHoliday]"
			   value="<?php echo $holidayName
				   = (!$model->isNewRecord && $model->productionCalendarId >= 1 && isset($model->productionCalendar))
				   ? $model->productionCalendar->holiday
				   : $model->newHoliday?>">
		<div class="help-block"></div>

		<?= $model->hasErrors('newHoliday') ? $model->getError('newHoliday') : '';?>

		<?php if (!$model->isNewRecord):?>
			<?=CHtml::activeHiddenField($model, 'productionCalendarId')?>
		<?php endif;?>
	</div>

	<div class="form-group">
		<?=CHtml::activeLabel($model, 'type', ['class' => 'control-label'])?>
		<?=CHtml::activeDropDownList($model, 'type', ProductionCalendarDay::$types, ['empty' => '', 'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark'])?>
		<div class="help-block">
			<?= $model->hasErrors('type') ? $model->getError('type') : '';?>
		</div>
	</div>

	<div class="form-group">
		<?=CHtml::activeLabel($model, 'day', ['class' => 'control-label'])?>
		<?=CHtml::activeDateField($model, 'day', ['class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark'])?>
		<div class="help-block">
			<?= $model->hasErrors('day') ? $model->getError('day') : '';?>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12">
			<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Save') ?>
			</button>
			<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
			   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
				<?= Yii::t('AdminModule.admin', 'Cancel') ?>
			</a>
		</div>
	</div>
</form>


