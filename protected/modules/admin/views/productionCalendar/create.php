<?php
/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendar */

$this->menu=array(
	array('label'=>'List ProductionCalendar', 'url'=>array('index')),
	array('label'=>'Manage ProductionCalendar', 'url'=>array('admin')),
);
?>

<h1>Создание даты производственного календаря</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>