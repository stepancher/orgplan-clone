<?php
/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendar */

$this->menu = array(
    array('label' => 'List ProductionCalendar', 'url' => array('index')),
    array('label' => 'Create ProductionCalendar', 'url' => array('create')),
    array('label' => 'Update ProductionCalendar', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete ProductionCalendar', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage ProductionCalendar', 'url' => array('admin')),
);
?>
    <br/>
<?= CHtml::link('Назад', Yii::app()->createUrl('admin/productionCalendar/admin'), ['class' => 'btn btn-danger']) ?>

    <h1>Просмотр даты производственного календаря #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'productionCalendar.holiday',
        [
            'name' => 'type',
            'value' => function ($data) {
                if($data->type === NULL){
                    return '';
                }
                return ProductionCalendarDay::$types[$data->type];
            },
            'filter' => ProductionCalendarDay::$types
        ],
        'day',
    ),
));