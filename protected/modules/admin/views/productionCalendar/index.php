<?php
/* @var $this ProductionCalendarController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Create ProductionCalendar', 'url'=>array('create')),
	array('label'=>'Manage ProductionCalendar', 'url'=>array('admin')),
);
?>

<h1>Производственный календарь</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
