<?php
/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendarDay */
/* @var $dataProvider CActiveDataProvider */

$this->menu = array(
    array('label' => 'List ProductionCalendar', 'url' => array('index')),
    array('label' => 'Create ProductionCalendar', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#production-calendar-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Даты производственного календаря</h1>

<?= CHtml::link('Создать', Yii::app()->createUrl('admin/productionCalendar/create'), ['class' => 'btn btn-success']) ?>
<?php //CHtml::link('Просмотр дат без праздников', Yii::app()->createUrl('admin/productionCalendar/notHolidayView'), ['class' => 'btn btn-success']) ?>

<div class="search-form" style="display:none; margin-left: 30px">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'production-calendar-grid',
    'dataProvider' => $dataProvider,
    'filter' => $model,
    'columns' => array(
        [
            'name' => 'productionCalendar.holiday',
            'header' => ProductionCalendar::model()->getAttributeLabel('holiday'),
            'value' => function ($data) {
                return isset($data->productionCalendar) ? $data->productionCalendar->holiday : '';
            },
            'filter' => CHtml::activeTextField(
                $model,
                'newHoliday',
                [
                    'value' => isset($_GET['ProductionCalendarDay']['newHoliday'])
                        ? $_GET['ProductionCalendarDay']['newHoliday']
                        : ''
                ])
        ],
        [
            'name' => 'type',
            'value' => function ($data) {
                if($data->type === NULL){
                    return '';
                }
                return ProductionCalendarDay::$types[$data->type];
            },
            'filter' => ProductionCalendarDay::$types
        ],
        [
            'name' => 'day',
            'filter' => CHtml::activeDateField($model, 'day')
        ],
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
