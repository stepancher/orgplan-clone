<?php

$app = Yii::app();
$isAdmin = $app->user->getState('role') == User::ROLE_ADMIN;
?>
<div style="height:1100px;">
    <h1>Управление</h1>
    <div class="container-fluid" style="margin-top:40px;margin-left:-40px;">
        <div class="row-fluid">
            <div class="col-md-3">
                <div class="col-sm-3 col-xs-12 bs-docs-sidebar">
                    <?php if ($isAdmin) {
                        echo $this->widget('bootstrap.widgets.TbNav', array(
                            'type' => TbHtml::NAV_TYPE_TABS,
                            'stacked' => true,
                            'encodeLabel' => false,
                            'htmlOptions' => array(
                                'class' => 'nav nav-list bs-docs-sidenav',
                            ),
                            'items' => array(
                                array(
                                    'label' => 'Выставки',
                                    'url' => $app->createUrl('/admin/fair/admin'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Организаторы',
                                    'url' => $app->createUrl('/admin/organizer/admin'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Площадки',
                                    'url' => $app->createUrl('/admin/exhibitionComplex/admin'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Создание записи в блоге',
                                    'url' => $this->createUrl('/admin/blog/save'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Отчет РСВЯ',
                                    'url' => $this->createUrl('/RUEF/districtReport/report'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - письмо',
                                    'url' => $app->createUrl('/admin/notification'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Список блогов',
                                    'url' => $app->createUrl('/admin/blog/index'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Цены',
                                    'url' => $app->createUrl('/priceAdmin/product/'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),

                                array(
                                    'label' => 'Адм - СМИ',
                                    'url' => $app->createUrl('/admin/MassMedia/index'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Отрасли',
                                    'url' => $app->createUrl('/admin/industry'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Градотека',
                                    'url' => $app->createUrl('/gradoteka/GObjectsHasGStatTypes'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Стенды',
                                    'url' => $app->createUrl('/admin/stand/admin'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - список базовых задач для участия в выставке',
                                    'url' => $app->createUrl('calendar/admin'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Производствен. Календарь',
                                    'url' => $app->createUrl('/admin/productionCalendar/admin'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Регионы',
                                    'url' => $app->createUrl('/admin/region/index'),
                                    'active' => $this->id == 'region' && $this->getAction()->id == 'index',
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Информация о регионах',
                                    'url' => $app->createUrl('/admin/regionInformation/admin'),
                                    'active' => $this->id == 'regionInformation' && $this->getAction()->id == 'index',
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - SEO',
                                    'url' => $app->createUrl('admin/seo'),
                                    'active' => $this->id == 'seo' && $this->getAction()->id == 'index',
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Адм - Города',
                                    'url' => $app->createUrl('/admin/city/admin'),
                                    'active' => $this->id == 'city' && $this->getAction()->id == 'index',
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Шаблон анкеты',
                                    'url' => $app->createUrl('/admin/questionnaire/questionnaire'),
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Регион',
                                    'url' => $app->createUrl('admin/regionInformation/index'),
                                    'active' => $this->id == 'regionInformation' && $this->getAction()->id == 'index',
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                                array(
                                    'label' => 'Идеальные стенды',
                                    'url' => $app->createUrl('stand/default/index'),
                                    'active' => $this->id == 'stand' && $this->getAction()->id == 'index',
                                    'htmlOptions' => array(
                                        'class' => 'nav-list__item'
                                    ),
                                    'visible' => $isAdmin,
                                ),
                            )
                        ), true, true);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>