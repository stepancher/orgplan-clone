<?php
/**
 * @var CController $this
 * @var Event $model
 */
$this->widget(
	'application.modules.admin.components.FieldSetView',
	array(
		'header' => Yii::t('AdminModule.admin', 'Detail header'),
		'items' => array(
			array(
				'application.modules.admin.components.ActionsView',
				'items' => array(
					'cancel' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('AdminModule.admin', 'Detail action cancel'),
						'attributes' => array(
							'url' => $this->createUrl('industry/index'),
							'color' => TbHtml::BUTTON_COLOR_WARNING
						)
					),
					'edit' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('AdminModule.admin', 'Detail action edit'),
						'attributes' => array(
							'url' => $this->createUrl('save', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_PRIMARY
						)
					)
				)
			),
		)
	)
);
$this->widget(
	'application.modules.admin.components.IndustryDetailView',
	array(
		'data' => $model,
	)
);