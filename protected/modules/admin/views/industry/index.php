<?php
/**
 * @var CController $this
 * @var AR $model
 * @var CWebApplication $app
 */
?>
<div style="margin-top: 40px;">
<?= TbHtml::linkButton(
	'Добавить новую отрасль',
	[
		'url'   => $this->createUrl('save'),
		'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
		'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;"
	]
); ?>
</div>

<?php
$ctrl = $this;
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'      => 'industry-grid',
	'columns' => array_merge(
		array_values($industryFields),
		array(
			array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '<nobr>{show}&#160;{edit}&#160;{delete}</nobr>',
				'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
				'buttons' => array(
					'show' => array(
						'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
						'url' => function ($data) use ($ctrl) {
							return $ctrl->createUrl('view', array('id' => $data->id));
						},
						'options' => array('title' => 'Информация', 'class' => 'btn btn-small btn-success'),
					),
					'edit' => array(
						'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
						'url' => function ($data) use ($ctrl) {
							return $ctrl->createUrl('save', array('id' => $data->id));
						},
						'options' => array('title' => 'Редактирование', 'class' => 'btn btn-small btn-primary'),
					),
					'delete' => array(
						'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
						'url' => function ($data) use ($ctrl) {
							return $ctrl->createUrl('delete', array('id' => $data->id));
						},
						'options' => array('title' => 'Удалить', 'class' => 'btn btn-small btn-danger'),
					),
				),
				'htmlOptions' => array(
					'target' => '_blank',
					'style' => "min-width: 16px;"
				),
			)
		)
	),
	'dataProvider' => $search,
	'filter'       => $model,
	'ajaxUpdate'   => false
));
?>