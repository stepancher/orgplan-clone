<?php
/* @var $this BlogController */
/* @var $model Blog */
?>

<h1>Просмотр записи блога #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        'theme',
        'created',
        'authorId',
		'status',
		'statistics',
		array(
            'name' => 'text',
            'type' =>'raw'
        ),
	),
)); ?>

<?=
	TbHtml::linkButton('  ' .
		'Редактировать', array(
		'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
		'style' => 'margin-top:10px;',
		'url' => Yii::app()->createUrl('admin/blog/save', array('id' => $model->id))
	));
?>
<?=
	TbHtml::linkButton('  ' .
		'Список', array(
		'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
		'style' => 'margin-top:10px;',
		'url' => Yii::app()->createUrl('admin/blog/index', array('id' => $model->id))
	));
?>
<?php
	if($model->status == 2){
		echo TbHtml::linkButton('  ' .
			'Просмотр на сайте', array(
			'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
			'style' => 'margin-top:10px;',
			'url' => Yii::app()->urlManager->createUrl('blog/default/viewArticle', array('url' => $model->shortUrl), Yii::app()->params['defaultAmpersand'], FALSE)
		));
	}
?>
