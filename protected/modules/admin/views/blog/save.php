<?php
/**
 * @var BlogController $this
 * @var Blog $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/blog/confirm-delete-article.js', CClientScript::POS_BEGIN);
?>

<style>
    .control-group{
        float: left;
    }
</style>

<div class="col-md-11">
    <?php
    $formClass = get_class($model) . 'Form';
    Yii::import('application.modules.admin.components.' . $formClass);
    $ctrl = $this;
    $ctrl->widget('application.modules.admin.components.FieldSetView', array(
        'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Form header create' : 'Form header update'),
        'items' => array(
            array(
                'application.modules.admin.components.FormView',
                'form' => new $formClass(
                    array(
                        'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                        'enctype' => 'multipart/form-data',
                        //'elementGroupName' => '[]'
                    ),
                    $model, $ctrl
                ),
                'items' => array(
                    array(
                        'application.modules.admin.components.HtmlView',
                        'content' => function ($owner) use ($model) {
                            if ($model->image && $model->image->getFileUrl()) {
                                echo TbHtml::thumbnails(
                                    H::getThumbnailsArray(
                                        array($model), $model->id, 'image', null,
                                        TbHtml::linkButton(
                                            Yii::t('AdminModule.admin', 'delete'),
                                            array(
                                                'color' => TbHtml::BUTTON_COLOR_DANGER,
                                                'size' => TbHtml::BUTTON_SIZE_MINI,
                                                'url' => $owner->createUrl('deleteFile', array('id' => $model->id, 'fileId' => '{fileId}'))
                                            )
                                        )
                                    )
                                );
                            } else {
                                $owner->widget('application.modules.admin.components.FieldSetView', array(
                                        'header' => Yii::t('AdminModule.admin', 'download Image (title picture)'),
                                        'items' => array(
                                            array(
                                                'CMultiFileUpload',
                                                'model' => $model,
                                                'htmlOptions' => array(
                                                    'style' => 'margin-left:80px'
                                                ),
                                                'max' => 1,
                                                'attribute' => 'image',
                                                'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
                                                'accept' => 'gif|jpg|jpeg|png',
                                                'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
                                            )
                                        )
                                    )
                                );
                            }
                        },
                    ),
                    array(
                        'application.modules.admin.components.HtmlView',
                        'content' => function ($owner) use ($model, $ctrl) {
                            $owner->widget('application.modules.admin.components.FieldSetView', array(
                                    'header' => Yii::t('AdminModule.admin', 'download Image'),
                                    'items' => array(
                                        array(
                                            'CMultiFileUpload',
                                            'model' => Blog::model(),
                                            'htmlOptions' => array(
                                                'style' => 'margin-left:80px'
                                            ),
                                            'attribute' => 'images',
                                            'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
                                            'accept' => 'gif|jpg|jpeg|png',
                                            'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
                                        )
                                    )
                                )
                            );
                            echo TbHtml::thumbnails(
                                H::getThumbnailsArray(
                                    $model->blogHasFiles, $model->id, 'file', ObjectFile::TYPE_BLOG_IMAGES,
                                    TbHtml::linkButton(
                                        Yii::t('AdminModule.admin', 'delete'),
                                        array(
                                            'color' => TbHtml::BUTTON_COLOR_DANGER,
                                            'size' => TbHtml::BUTTON_SIZE_MINI,
                                            'url' => $owner->createUrl('deleteFile', array('id' => $model->id, 'fileId' => '{fileId}'))
                                        )
                                    )
                                )
                            );
                            $owner->widget('application.modules.admin.components.FieldSetView', array(
                                    'header' => Yii::t('AdminModule.admin', 'form to attach files'),
                                    'items' => array(
                                        array(
                                            'CMultiFileUpload',
                                            'model' => Blog::model(),
                                            'htmlOptions' => array(
                                                'style' => 'margin-left:80px',
                                            ),
                                            'attribute' => 'files',
                                            'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
                                            'accept' => 'gif|jpg|jpeg|png',
                                            'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
                                        ),
                                    )
                                )
                            );
                            $compare = array(0);
                            if (!empty($model->blogHasFiles)) {
                                $compare = CHtml::listData($model->blogHasFiles, 'id', 'fileId');
                            }

                            if (!$model->isNewRecord && !empty($model->blogHasFiles)) {
                                $owner->widget('application.modules.admin.components.ObjectFileGridView', array(
                                    'model' => ObjectFile::model(),
                                    'compare' => array(
                                        array('t.id', $compare),
                                        array('t.type', ObjectFile::TYPE_BLOG_FILES)
                                    ),
                                    'columnsAppend' => array(
                                        'buttons' => array(
                                            'class' => 'bootstrap.widgets.TbButtonColumn',
                                            'template' => '{delete}',
                                            'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                                            'buttons' => array(
                                                'delete' => array(
                                                    'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
                                                    'url' => function ($data) use ($ctrl, $model) {
                                                        return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
                                                    },
                                                    'options' => array('title' => Yii::t('AdminModule.admin', 'information'), 'class' => 'btn btn-small btn-danger'),
                                                ),
                                            )
                                        )
                                    )
                                ));
                            }

                        },
                    ),
                    array(
                        'application.modules.admin.components.ActionsView',
                        'items' => array(
                            'submit' => array(
                                'type' => TbHtml::BUTTON_TYPE_SUBMIT,
                                'label' => Yii::t('AdminModule.admin', 'Form action save'),
                                'attributes' => array(
                                    'color' => TbHtml::BUTTON_COLOR_SUCCESS
                                )
                            ),
                            'cancel' => array(
                                'type' => TbHtml::BUTTON_TYPE_LINK,
                                'label' => Yii::t('AdminModule.admin', 'Form action cancel'),
                                'attributes' => array(
                                    'url' => $this->createUrl('index'),
                                    'color' => TbHtml::BUTTON_COLOR_DANGER
                                )
                            ),
                            'delete' => array(
                                'type' => TbHtml::BUTTON_TYPE_LINK,
                                'label' => Yii::t('AdminModule.admin', 'delete article'),
                                //'visible' => !$model->isNewRecord,
                                'attributes' => array(
                                    'url' => $this->createUrl('admin/blog/DeleteArticle', ['id' => $model->id]),
                                    'color' => TbHtml::BUTTON_COLOR_WARNING,
                                    'class' => 'delete-article',
                                )
                            )
                        )
                    ),
                ),
            ),
        )
    ));
    ?>
</div>
<script type="text/javascript">
    new ConfirmDelete({
        confirmText: '<?=Yii::t('AdminModule.admin', 'Are you sure you want to delete an article or material?')?>'
    });
</script>
