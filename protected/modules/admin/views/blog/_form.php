<?php
/**
 * @var $model Blog
 * @var $this BlogController
 */
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/blog/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

    <div class="cols-row">
        <div class="col-md-11">
            <?php
            $formClass = get_class($model) . 'Form';
            Yii::import('application.modules.admin.components.' . $formClass);
            $ctrl = $this;
            $ctrl->widget('application.modules.admin.components.FieldSetView', array(
                'header' => Yii::t('AdminModule.admin', $model->isNewRecord ? 'Создание' : 'Редактирование'),
                'htmlOptions' => [
                    'class' => 'form-horizontal',

                ],
                'items' => array(
                    array(
                        'application.modules.admin.components.FormView',
                        'form' => new $formClass(
                            array(
                                'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                                'enctype' => 'multipart/form-data',
                                //'elementGroupName' => '[]'
                            ),
                            $model, $ctrl
                        ),
                        'items' => array(
                            array(
                                'application.modules.admin.components.HtmlView',
                                'content' => function ($owner) use ($model) {
                                    if ($model->image && $model->image->getFileUrl()) {
                                        echo TbHtml::thumbnails(
                                            H::getThumbnailsArray(
                                                array($model), $model->id, 'image', null,
                                                TbHtml::linkButton(
                                                    Yii::t('AdminModule.admin', 'delete'),
                                                    array(
                                                        'color' => TbHtml::BUTTON_COLOR_DANGER,
                                                        'size' => TbHtml::BUTTON_SIZE_MINI,
                                                        'url' => $owner->createUrl('blog/deleteFile', array(
                                                            'id' => $model->id,
                                                            'fileId' => '{fileId}',
                                                            'action' => Yii::app()->controller->action->id)
                                                        )
                                                    )
                                                )
                                            )
                                        );
                                    } else {
                                        $owner->widget('application.modules.admin.components.FieldSetView', array(
                                                'header' => Yii::t('AdminModule.admin', 'download Image (title picture)'),
                                                'items' => array(
                                                    array(
                                                        'CMultiFileUpload',
                                                        'model' => $model,
                                                        'htmlOptions' => array(
                                                            'style' => 'margin-left:80px'
                                                        ),
                                                        'max' => 1,
                                                        'attribute' => 'image',
                                                        'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
                                                        'accept' => 'gif|jpg|jpeg|png',
                                                        'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
                                                    )
                                                )
                                            )
                                        );
                                    }
                                },
                            ),
                            array(
                                'application.modules.admin.components.HtmlView',
                                'content' => function ($owner) use ($model, $ctrl) {
                                    $owner->widget('application.modules.admin.components.FieldSetView', array(
                                            'header' => Yii::t('AdminModule.admin', 'download Image'),
                                            'items' => array(
                                                array(
                                                    'CMultiFileUpload',
                                                    'model' => Blog::model(),
                                                    'htmlOptions' => array(
                                                        'style' => 'margin-left:80px'
                                                    ),
                                                    'attribute' => 'images',
                                                    'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
                                                    'accept' => 'gif|jpg|jpeg|png',
                                                    'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
                                                )
                                            )
                                        )
                                    );
                                    echo TbHtml::thumbnails(
                                        H::getThumbnailsArray(
                                            $model->blogHasFiles, $model->id, 'file', ObjectFile::TYPE_BLOG_IMAGES,
                                            TbHtml::linkButton(
                                                Yii::t('AdminModule.admin', 'delete'),
                                                array(
                                                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                                                    'size' => TbHtml::BUTTON_SIZE_MINI,
                                                    'url' => $owner->createUrl('blog/deleteFile', array(
                                                            'id' => $model->id,
                                                            'fileId' => '{fileId}',
                                                            'action' => Yii::app()->controller->action->id)
                                                    )
                                                )
                                            )
                                        )
                                    );
                                    $owner->widget('application.modules.admin.components.FieldSetView', array(
                                            'header' => Yii::t('AdminModule.admin', 'form to attach files'),
                                            'items' => array(
                                                array(
                                                    'CMultiFileUpload',
                                                    'model' => Blog::model(),
                                                    'htmlOptions' => array(
                                                        'style' => 'margin-left:80px',
                                                    ),
                                                    'attribute' => 'files',
                                                    'accept' => 'gif|jpg|jpeg|png',
                                                    'denied' => Yii::t('AdminModule.admin', 'Invalid file type'),
                                                    'duplicate' => Yii::t('AdminModule.admin', 'this file already exists!'),
                                                ),
                                            )
                                        )
                                    );
                                    $compare = array(0);
                                    if (!empty($model->blogHasFiles)) {
                                        $compare = CHtml::listData($model->blogHasFiles, 'id', 'fileId');
                                    }

                                    if (!$model->isNewRecord && !empty($model->blogHasFiles)) {
                                        $owner->widget('application.modules.admin.components.ObjectFileGridView', array(
                                            'model' => ObjectFile::model(),
                                            'compare' => array(
                                                array('t.id', $compare),
                                                array('t.type', ObjectFile::TYPE_BLOG_FILES)
                                            ),
                                            'columnsAppend' => array(
                                                'buttons' => array(
                                                    'class' => 'bootstrap.widgets.TbButtonColumn',
                                                    'template' => '{delete}',
                                                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                                                    'buttons' => array(
                                                        'delete' => array(
                                                            'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
                                                            'url' => function ($data) use ($ctrl, $model) {
                                                                return $ctrl->createUrl('blog/deleteFile', array('id' => $model->id, 'fileId' => $data->id));
                                                            },
                                                            'options' => array('title' => Yii::t('AdminModule.admin', 'information'), 'class' => 'btn btn-small btn-danger'),
                                                        ),
                                                    )
                                                )
                                            )
                                        ));
                                    }

                                },
                            ),
                        ),
                    ),
                )
            ));
            ?>
            <button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
               class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Cancel') ?>
            </a>
        </div>
    </div>
</form>