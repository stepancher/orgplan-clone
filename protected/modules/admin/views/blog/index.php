<?php
/* @var $this FairController */
/* @var Blog $model */

$this->menu = array(
    array('label' => 'Create Fair', 'url' => array('create')),
    array('label' => 'Manage Fair', 'url' => array('admin')),
);
?>

<h1>Блоги</h1>
<?php echo TbHtml::linkButton('Добавить новую статью', array('url' => $this->createUrl('save'))); ?>

<br>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'theme',
        'created',
        'authorId' => array(
            'name' => 'author_firstName',
            'filter' => CHtml::listData(User::model()->findAllByAttributes(['role' => User::ROLE_EXPERT]), 'firstName', 'firstName'),
            'value' => function ($data) {
                if ($data->author) {
                    return $data->author->firstName;
                }
                return null;
            }
        ),
        array(
            'name' => 'status',
            'filter' => Blog::status(),
            'value' => function ($data) {
                if (isset(Blog::status()[$data->status])) {
                    return Blog::status()[$data->status];
                }
                return null;
            }
        ),
        array(
            'name' => 'statistics',
            'filter' => [1 => 'Добавлено', 0 => 'Не Добавлено'],
            'type' => 'raw',
            'value' => function ($data) {
                return TbHtml::activeCheckBox($data, 'statistics', ['checked' => $data->statistics == 1, 'disabled' => true]);
            }
        ),
        array(
            'class' => 'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                'update' => array
                (
                    'url'=>'Yii::app()->createUrl("admin/blog/save", array("id"=>$data->id))',
                ),
            ),
        ),
    )
))?>
