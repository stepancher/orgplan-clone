<?php
/**
 * @var $this CController
 * @var $app CWebApplication
 * @var $model Questionnaire
 * @var $dataArray array
 */
$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/questionnaire/style.css');
$cs->registerCssFile($publishUrl . '/css/questionnaire/icons.css');
$cs->registerScriptFile($publishUrl . '/js/questionnaire/script.js', CClientScript::POS_BEGIN);
$data = $dataArray + array(
        'labels' => array(
            'Contact' => array(
                1 => 'Компания',
                2 => 'ФИО',
                3 => 'Должность',
                4 => 'Отдел',
                5 => 'Телефон',
                6 => 'Факс',
                7 => 'E-mail',
                8 => 'Страна, индекс',
                9 => 'Город, улица, дом',
                10 => 'URL',
            ),
            'Questions' => array(
                1 => 'Тип посетителя:',
                2 => 'Профиль деятельности:',
                3 => 'Мотив посещения:',
                4 => 'Какой товар представляет интерес:',
                5 => 'Сфера применения товара:',
                6 => 'Степень интереса:',
                7 => 'Тема переговоров:',
                8 => 'Какой материал передали:',
                9 => 'Какой материал отправить после followup:',
                10 => 'В какой отдел передается контакт для дальнейшей работы:',
                11 => 'Комментарии:',
            ),
        ),
        'fields' => array(
            'Contact' => array(
                3 => array(
                    1 => 'Менеджер',
                    2 => 'Руководитель',
                    3 => 'Собственник',
                ),
                4 => array(
                    1 => 'Продажи',
                    2 => 'Маркетинг',
                    3 => 'Производство',
                    4 => 'Закупки',
                    5 => 'Управление',
                ),
            ),
            'Questions' => array(
                1 => array(
                    1 => 'Существующий клиент',
                    2 => 'Новый клиент',
                    3 => 'Конкурент',
                    4 => 'СМИ',
                ),
                2 => array(
                    1 => 'Дистрибутор',
                    2 => 'Оптовая компания',
                    3 => 'Магазин',
                    4 => 'Производитель',
                ),
                3 => array(
                    1 => 'Приглашение',
                    2 => 'Выставочный каталог',
                    3 => 'Реклама (какая)',
                    4 => 'По рекомендации',
                    5 => 'Случайно',
                    6 => 'Анализ конкурента',
                ),
                6 => array(
                    1 => 'Очень заинтересован',
                    2 => 'Заинтересован',
                    3 => 'Нейтрален',
                    4 => 'Негативен',
                ),
                5 => array(
                    1 => 'Сувенирная продукция',
                    2 => 'Прайс-лист',
                    3 => 'Каталог',
                ),
                9 => array(
                    1 => 'Брошюра',
                    2 => 'Образец продукции',
                    3 => 'Прайс-лист',
                    4 => 'Каталог',
                ),
                10 => array(
                    1 => 'Отдел продаж',
                    2 => 'Производство',
                    3 => 'Отдел маркетинга',
                ),
            ),
        ),
    );
?>

<div class="profile-questionnaire__content">
    <?=
    $this->renderPartial('_blocks/_b-header', array(
        'header' => Yii::t('questionnaire', 'Templates'),
        'description' => Yii::t('questionnaire', 'Profile exhibition visitor'),
    ))
    ?>
    <?= TbHtml::beginForm('', 'post', array(
        'id' => 'questionnaire-form-logo',
        'enctype' => 'multipart/form-data',
    )) ?>
    <div class="profile-questionnaire__header-settings">
        <div class="profile-questionnaire__upload_logo">
            <?php
            if ($model->logo) {
                echo TbHtml::image(H::getImageUrl($model, 'logo'));
            }
            ?>
        </div>
    </div>
    <div style="padding: 20px;">
        <span class="text-size-34">LOGO</span>

        <div class="styled-upload-button">
            <?= TbHtml::button('Загрузить', array('class' => 'styled-uploader__styled-button b-button d-bg-success-dark d-bg-success--hover d-text-light btn')) ?>
            <?= TbHtml::createInput(TbHtml::INPUT_TYPE_FILE, 'Questionnaire[logo]', '', array('class' => 'styled-uploader__main-hidden-input')) ?>
            <div class="styled-upload-button__file-name"></div>
        </div>
    </div>
    <?= TbHtml::endForm() ?>

    <?= TbHtml::beginForm('', 'post', array(
        'id' => 'questionnaire-form',
        'enctype' => 'multipart/form-data',
    )) ?>
    <div class="profile-questionnaire__colors">
        <div class="profile-questionnaire__colors-label">
            <span class="text-size-16">Выбор цвета:</span>
        </div>
        <div class="profile-questionnaire__colors-checkboxes">
        </div>
    </div>

    <div class="profile-questionnaire__header--center">
        <h2><span class="text-size-30"><?= Yii::t('questionnaire', 'Profile exhibition visitor') ?></span></h2>
    </div>
    <div class="profile-questionnaire">
        <div>
            <table>
                <tr>
                    <th>КОНТАКТЫ</th>
                    <th></th>
                </tr>
                <?php
                foreach ($data['labels']['Contact'] as $k => $label) {
                    ?>
                    <tr>
                        <td>
                            <span class="question-label"><?= $label ?></span>
                            <input type="hidden" data-index="<?= $k ?>" value="<?= $label ?>"
                                   name="Questionnaire[data][labels][Contact][<?= $k ?>]"/>
                        </td>
                        <td>
                            <div>
                                <?php
                                foreach ($data['fields']['Contact'] as $labelKey => $box) {
                                    if ($labelKey == $k) {
                                        foreach ($box as $name) {
                                            echo '<div class="input-block">';
                                            echo TbHtml::checkBoxControlGroup("Questionnaire[unchecked]", false, array(
                                                'label' => '<label>' . $name . '</label>',
                                            ));
                                            echo TbHtml::hiddenField("Questionnaire[data][fields][Contact][$labelKey][]", $name);
                                            echo '</div>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div>
                                <i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>
                                <?= TbHtml::textField('Add[additional_field]', '', array('data-col-name' => 'Contact')) ?>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>
                        <div>

                        </div>
                        <div>
                            <i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>
                            <?= TbHtml::textField('Add[additional_label]', '', array('data-col-name' => 'Contact')) ?>
                        </div>

                    </td>
                    <td>
                        <div>

                        </div>
                        <div>
                            <i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>
                            <?= TbHtml::textField('Add[additional_field]', '', array('data-col-name' => 'Contact')) ?>
                        </div>

                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <th>ВОПРОСЫ</th>
                    <th></th>
                </tr>
                <?php
                foreach ($data['labels']['Questions'] as $k => $label) {
                    ?>
                    <tr>
                        <td>
                            <span class="question-label"><?= $label ?></span>
                            <input type="hidden" data-index="<?= $k ?>" value="<?= $label ?>"
                                   name="Questionnaire[data][labels][Questions][<?= $k ?>]"/>
                        </td>
                        <td>
                            <div>
                                <?php
                                foreach ($data['fields']['Questions'] as $labelKey => $box) {
                                    if ($labelKey == $k) {
                                        foreach ($box as $name) {
                                            echo '<div class="input-block">';
                                            echo TbHtml::checkBoxControlGroup("Questionnaire[unchecked]", false, array(
                                                'label' => '<label>' . $name . '</label>',
                                            ));
                                            echo TbHtml::hiddenField("Questionnaire[data][fields][Questions][$labelKey][]", $name);
                                            echo '</div>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                            <div>
                                <i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>
                                <?= TbHtml::textField('Add[additional_field]', '', array('data-col-name' => 'Questions')) ?>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>
                        <div>

                        </div>
                        <div>
                            <i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>
                            <?= TbHtml::textField('Add[additional_label]', '', array('data-col-name' => 'Questions')) ?>
                        </div>

                    </td>
                    <td>
                        <div>

                        </div>
                        <div>
                            <i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>
                            <?= TbHtml::textField('Add[additional_field]', '', array('data-col-name' => 'Questions')) ?>
                        </div>

                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/html" id="option-checkbox-template">
        <div class="input-block">
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" value="1" name="Questionnaire[unchecked]"
                               id="Questionnaire_{id}">
                        {label}
                    </label>
                </div>
            </div>
            <input type="hidden" value="{label}" name="Questionnaire[data][fields]{name}"
                   id="Questionnaire_data_fields_{id}">
        </div>
    </script>
    <?= TbHtml::hiddenField('Questionnaire[generate]', '0', array('class' => 'input-save-mode')) ?>
    <?= TbHtml::endForm() ?>
    <?= TbHtml::submitButton('Сохранить', array('class' => 'b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn')) ?>
    <?= TbHtml::submitButton('Сгенерировать', array(
        'class' => 'b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn data-generate',
    )) ?>
</div>
