<?php
/**
 * @var array $data
 * @var Questionnaire $model
 * @var CWebApplication $app
 */
$app = Yii::app();
$checkboxImagePath = Yii::getPathOfAlias('application.modules.admin.assets') . '/img/checkbox/single-checkbox-empty.png';
$checkboxImage = CHtml::image($checkboxImagePath, '', ['class' => 'pdf'])."&nbsp;";
?>
<style>
	img.pdf {
		width: 7px;
		height: 7px;
		margin: 2px 5px 0 0;
	}
</style>
<table>
	<tr>
		<td style="width: 60%;">
			<?= TbHtml::image(
                Yii::getPathOfAlias('application.modules.admin.assets') . '/img/logos/header_logo.png', '',
				array('style' => 'width: 100px; padding-left: 30px; display:block')
			) ?>
		</td>
		<td valign="bottom">Я помогаю участвовать в выставках. Orgplan.ru</td>
	</tr>
</table>

<table cellpadding="3">
	<tr>
		<td width="25%" style="border: 3px solid #385d89;">
			<?= TbHtml::image(H::getImageUrl($model, 'logo')) ?>
		</td>
		<td width="75%"
			style="background-color: <?= $model->getColorCode() ?>; border: 3px solid #385d89; padding: 4px">

		</td>
	</tr>
</table>
<h1 style="text-align: center">Анкета посетителя выставки</h1>
<style>
	table {
		border-collapse: collapse;
	}
</style>
<table style="border: 1px solid black">
	<tr>
		<td style="width:25%; border: 1px solid black">Дата</td>
		<td style="width:25%; border: 1px solid black">Время</td>
		<td style="width:50%; border: 1px solid black">Сотрудник, заполнивший анкету</td>
	</tr>
	<tr>
		<td style="border: 1px solid black"></td>
		<td style="border: 1px solid black"></td>
		<td style="border: 1px solid black"></td>
	</tr>
</table>
<br/>
<br/>
<table width="100%" style="padding: 0;margin: 0">
	<tr style="padding: 0;margin: 0">
		<td style="width:45%; padding: 0; margin: 0">
			<table cellpadding="3" style="border: 1px solid black;">
				<tr style="background-color: #bebebe">
					<td style="width:30%; border: 1px solid black;">КОНТАКТЫ</td>
					<td style="width:70%; border: 1px solid black;"></td>
				</tr>
				<?php foreach ($data['data']['labels']['Contact'] as $k => $label) { ?>
					<tr>
						<td style="border: 1px solid black;"><?= $label ?></td>
						<td style="border: 1px solid black;">
							<?php
							foreach ($data['data']['fields']['Contact'] as $labelKey => $checkboxes) {
								if ($labelKey == $k) {
									foreach ($checkboxes as $v) {
										echo $checkboxImage.$v . '<br>';
									}
								}
							}
							?>
						</td>
					</tr>
				<?php } ?>
			</table>
		</td>
		<td style="width: 5%"></td>
		<td style="width:50%; padding: 0;margin: 0">
			<table cellpadding="3" style="border: 1px solid black;">
				<tr style="background-color: #bebebe">
					<td style="width:30%; border: 1px solid black;">ВОПРОСЫ</td>
					<td style="width:70%;border: 1px solid black;"></td>
				</tr>
				<?php foreach ($data['data']['labels']['Questions'] as $k => $label) { ?>
					<tr>
						<td style="border: 1px solid black;"><?= $label ?></td>
						<td style="border: 1px solid black;">
							<?php
							foreach ($data['data']['fields']['Questions'] as $labelKey => $checkboxes) {
								if ($labelKey == $k) {
									foreach ($checkboxes as $v) {
										echo $checkboxImage.$v . '<br>';
									}
								}
							}
							?>
						</td>
					</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
</table>
