<?php
/* @var $this RegionInformationController */
/* @var $data RegionInformation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('regionId')); ?>:</b>
	<?php echo isset($data->region) ? CHtml::encode($data->region->name) : Yii::t('AdminModule.admin', 'Not set'); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo $data->text; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imageId')); ?>:</b>
	<?php echo CHtml::encode($data->imageId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('analytics')); ?>:</b>
	<?php echo CHtml::encode($data->analytics); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area')); ?>:</b>
	<?php echo CHtml::encode($data->area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('population')); ?>:</b>
	<?php echo CHtml::encode($data->population); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fair')); ?>:</b>
	<?php echo CHtml::encode($data->fair); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('regionalProduct')); ?>:</b>
	<?php echo CHtml::encode($data->regionalProduct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tradeTurnover')); ?>:</b>
	<?php echo CHtml::encode($data->tradeTurnover); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sourceUrl')); ?>:</b>
	<?php echo CHtml::encode($data->sourceUrl); ?>
	<br />

	*/ ?>

</div>