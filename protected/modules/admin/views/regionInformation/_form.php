<?php

/* @var $this RegionInformationController */
/* @var $model RegionInformation */

use yii\helpers\ArrayHelper;

$publishUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
Yii::app()->getClientScript()->registerScriptFile($publishUrl . '/js/dropdown/script.js');
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/regionInformation/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

<div class="cols-row">
	<div class="col-md-12">

		<?=
		$this->widget('application.modules.admin.components.ListAutoComplete',
			ArrayHelper::merge(
				array(
					'name' => 'RegionInformation[regionId]',
					'attribute' => 'regionId',
				),
				array(
					'model' => $model,
					'selectedItems' => array($model->regionId),
					'multiple' => FALSE,
					'htmlOptions' => array(
						'labelOptions' => array(
							'class' => 'd-bg-tab d-bg-tooltip--hover d-bg-tooltip--focus d-text-dark'
						),
						'data-input-id' => 'regionId',
					),
					'itemOptions' => array(
						'data' => array('' => 'Не выбрано') + CHtml::listData(TrRegion::model()->findAllByAttributes(['langId' => Yii::app()->language]), 'trParentId', 'name'),
					),
					'label' => NULL,
				)
			), true);
		?>

        <?php if ($model->hasErrors('regionId')){
            echo '<div class="control-group error"><p class="help-block">'.$model->getError('regionId'). '</p></div>';
        }?>

		<div class="form-group field-regioninformation-name">
			<label class="control-label" for="RegionInformation_name"><?= Yii::t('AdminModule.admin', 'Name') ?></label>
			<input type="text" id="RegionInformation_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[name]" value="<?= $model->name ?>">
			<div class="help-block"></div>
		</div>

		<?php

		if (method_exists($model, 'getAttribute')) {
			$select = $model->getAttribute('type');
		} else {
			$select = NULL;
		}

		echo TbHtml::activeControlGroup(
			TbHtml::INPUT_TYPE_DROPDOWNLIST,
			$model,
			'type',
			array(
				'groupOptions' => array(
					'class' => 'js-drop-down-list drop-down-list',
				),
				'labelOptions' => array(
					'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
				),
			)
		);
		?>

		<label><?= RegionInformation::model()->getAttributeLabel('text')?></label>

		<?= $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
				'model' => $model,
				'attribute' => 'text',
				'options' => array(
					'minHeight' => '250px',
					'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array('modelName' => get_class($model))),
					'lang' => 'ru',
					'iframe' => TRUE,
					'css' => 'wym.css',
				)
			), TRUE
		);
		?>

		<div class="form-group field-regioninformation-rating">
			<label class="control-label" for="RegionInformation_rating"><?= Yii::t('AdminModule.admin', 'Rating') ?></label>
			<input type="text" id="RegionInformation_rating" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[rating]" value="<?= $model->rating ?>">
			<div class="help-block"></div>
		</div>

		<?php

		if (method_exists($model, 'getAttribute')) {
			$select = $model->getAttribute('active');
		} else {
			$select = NULL;
		}

		echo TbHtml::activeControlGroup(
			TbHtml::INPUT_TYPE_DROPDOWNLIST,
			$model,
			'active',
			array(
				'groupOptions' => array(
					'class' => 'js-drop-down-list drop-down-list',
				),
				'labelOptions' => array(
					'class' => 'js-drop-button drop-button d-bg-tab d-bg-tooltip--hover d-text-dark',
				),
				'input' => TbHtml::dropDownList(
					'RegionInformation[active]',
					$select,
					RegionInformation::getActive()
				),
			)
		);
		?>
		
		<label><?= RegionInformation::model()->getAttributeLabel('analytics')?></label>


		<?= $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
			'model' => $model,
			'attribute' => 'analytics',
			'options' => array(
				'minHeight' => '250px',
				'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array('modelName' => get_class($model))),
				'lang' => 'ru',
				'iframe' => TRUE,
				'css' => 'wym.css',
			)
		), TRUE
		);
		?>

		<div class="form-group field-regioninformation-area">
			<label class="control-label" for="RegionInformation_area"><?= Yii::t('AdminModule.admin', 'Area') ?></label>
			<input type="text" id="RegionInformation_area" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[area]" value="<?= $model->area ?>">
			<div class="help-block"></div>
		</div>

		<div class="form-group field-regioninformation-population">
			<label class="control-label" for="RegionInformation_population"><?= Yii::t('AdminModule.admin', 'Population') ?></label>
			<input type="text" id="RegionInformation_population" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[population]" value="<?= $model->population ?>">
			<div class="help-block"></div>
		</div>

		<div class="form-group field-regioninformation-regionalProduct">
			<label class="control-label" for="RegionInformation_regionalProduct"><?= Yii::t('AdminModule.admin', 'Gross regional product') ?></label>
			<input type="text" id="RegionInformation_regionalProduct" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[regionalProduct]" value="<?= $model->regionalProduct ?>">
			<div class="help-block"></div>
		</div>

		<div class="form-group field-regioninformation-tradeTurnover">
			<label class="control-label" for="RegionInformation_tradeTurnover"><?= Yii::t('AdminModule.admin', 'Retail trade turnover') ?></label>
			<input type="text" id="RegionInformation_tradeTurnover" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[tradeTurnover]" value="<?= $model->tradeTurnover ?>">
			<div class="help-block"></div>
		</div>

		<div class="form-group field-regioninformation-sourceUrl">
			<label class="control-label" for="RegionInformation_sourceUrl"><?= Yii::t('AdminModule.admin', 'Source') ?></label>
			<input type="text" id="RegionInformation_sourceUrl" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="RegionInformation[sourceUrl]" value="<?= $model->sourceUrl ?>">
			<div class="help-block"></div>
		</div>

		<label><?= RegionInformation::model()->getAttributeLabel('logo')?></label>

		<?php echo
		$this->widget('application.modules.admin.components.StyledMultiFileUploader',
			TbArray::merge(
				array(
					'model' => $model,
					'attribute' => 'logo',
					'max' => 4,
					'accept' => ObjectFile::getDefaultAllowedExt(),
					'maxMessage' => Yii::t('AdminModule.admin', 'Limit of files is exceeded'),
					'duplicate' => Yii::t('AdminModule.admin', 'This file is already available'),
					'deleteAction' => $this->createUrl('file/fieldHasFile/remove', ['id' => '{id}']),
					'fileData' => FieldHasFile::getFieldFiles($model, 'logo'),
				),
				array(
					'max' => 1,
					'accept' => ObjectFile::$acceptedImageExt,
					'outputAsImage' => true,
					'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE),
					'deleteAction' => $this->createUrl('file/objectFile/remove',
						array('id' => '{id}', 'cls' => 'RegionInformation')),
					'buttonTag' => 'button',
					'buttonOptions' => array(
						'class' => 'b-button d-bg-success-dark d-bg-success--hover',
						'type' => 'button',
						'content' => Yii::t('AdminModule.admin', 'Select files')
					)
				)
			), true
		);
		?>

	</div>
</div>

<div class="cols-row">
	<div class="col-md-12">
		<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
			<?= Yii::t('AdminModule.admin', 'Save') ?>
		</button>
		<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('admin') ?>"
		   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
			<?= Yii::t('AdminModule.admin', 'Cancel') ?>
		</a>
	</div>
</div>

</form>


