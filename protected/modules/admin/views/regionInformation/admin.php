<?php
/* @var $this RegionInformationController */
/* @var $model RegionInformation */

?>

<h1>Список Информация о регионе</h1>
<?= CHtml::link('Добвить', Yii::app()->createUrl('admin/regionInformation/create'), ['class' => 'btn btn-success']) ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'region-information-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        [
            'header' => 'Регион',
            'name' => 'region_name',
            'value' => function($data){
                if ($data->region){
                    return $data->region->name;
                }
            }
        ],
        'rating',
        [
            'name' => 'active',
            'filter' => [1 => 'Активно', 0 => 'Не активно'],
            'value' => function ($data) {
                return !empty($data->active) || $data->active != null
                    ? RegionInformation::getActive($data->active)
                    : Yii::t('AdminModule.admin', 'Not set');
            }
        ],
        'area',
        'population',
        'regionalProduct',
        'tradeTurnover',
        'sourceUrl',

        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
