<?php
/* @var $this RegionInformationController */
/* @var $model RegionInformation */
?>

<h1>Создание Информации о регионе</h1>
<?=CHtml::link('Вернуться к списку', Yii::app()->createUrl('/admin/regionInformation/admin'), ['class' => 'btn btn-success'])?>
    <br/>
    <br/>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>