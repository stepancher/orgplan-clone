<?php
/* @var $this RegionInformationController */
/* @var $model RegionInformation */
?>

<h1>Просмотр Информация о регионе #<?php echo $model->id; ?></h1>
<?= CHtml::link('Вернуться к списку', Yii::app()->createUrl('/admin/regionInformation/admin'), ['class' => 'btn btn-success']) ?>
<br/>
<br/>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        [
            'name' => 'regionId',
            'value' => $model->region ? $model->region->name : '',
        ],
        'text',
        'rating',
        [
            'name' => 'active',
            'value' => RegionInformation::getActive()[$model->active]
        ],
        'analytics',
        'area',
        'population',
        'regionalProduct',
        'tradeTurnover',
        'sourceUrl',
        [
            'name' => 'logo',
            'value' => $model->getLogo(),
            'type' => 'html'
        ],
    ),
)); ?>
