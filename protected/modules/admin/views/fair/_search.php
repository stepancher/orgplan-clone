<?php
/* @var $this FairController */
/* @var $model Fair */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'logoId'); ?>
		<?php echo $form->textField($model,'logoId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rating'); ?>
		<?php echo $form->textField($model,'rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'participationPrice'); ?>
		<?php echo $form->textField($model,'participationPrice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'registrationFee'); ?>
		<?php echo $form->textField($model,'registrationFee'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organizerId'); ?>
		<?php echo $form->textField($model,'organizerId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exhibitionComplexId'); ?>
		<?php echo $form->textField($model,'exhibitionComplexId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'beginDate'); ?>
		<?php echo $form->textField($model,'beginDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'endDate'); ?>
		<?php echo $form->textField($model,'endDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'beginMountingDate'); ?>
		<?php echo $form->textField($model,'beginMountingDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'endMountingDate'); ?>
		<?php echo $form->textField($model,'endMountingDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'beginDemountingDate'); ?>
		<?php echo $form->textField($model,'beginDemountingDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'endDemountingDate'); ?>
		<?php echo $form->textField($model,'endDemountingDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'squareNet'); ?>
		<?php echo $form->textField($model,'squareNet'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'members'); ?>
		<?php echo $form->textField($model,'members'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'visitors'); ?>
		<?php echo $form->textField($model,'visitors'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activateControl'); ?>
		<?php echo $form->textField($model,'activateControl'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'originalId'); ?>
		<?php echo $form->textField($model,'originalId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keyword'); ?>
		<?php echo $form->textArea($model,'keyword',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uniqueText'); ?>
		<?php echo $form->textArea($model,'uniqueText',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'statistics'); ?>
		<?php echo $form->textArea($model,'statistics',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'currencyId'); ?>
		<?php echo $form->textField($model,'currencyId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->