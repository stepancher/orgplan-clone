<?php
/* @var $this FairController */
/* @var $data Fair */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logoId')); ?>:</b>
	<?php echo CHtml::encode($data->logoId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('participationPrice')); ?>:</b>
	<?php echo CHtml::encode($data->participationPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('registrationFee')); ?>:</b>
	<?php echo CHtml::encode($data->registrationFee); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exhibitionComplexId')); ?>:</b>
	<?php echo CHtml::encode($data->exhibitionComplexId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beginDate')); ?>:</b>
	<?php echo CHtml::encode($data->beginDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endDate')); ?>:</b>
	<?php echo CHtml::encode($data->endDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beginMountingDate')); ?>:</b>
	<?php echo CHtml::encode($data->beginMountingDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endMountingDate')); ?>:</b>
	<?php echo CHtml::encode($data->endMountingDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beginDemountingDate')); ?>:</b>
	<?php echo CHtml::encode($data->beginDemountingDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endDemountingDate')); ?>:</b>
	<?php echo CHtml::encode($data->endDemountingDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('squere')); ?>:</b>
	<?php echo CHtml::encode($data->squere); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('members')); ?>:</b>
	<?php echo CHtml::encode($data->members); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visitors')); ?>:</b>
	<?php echo CHtml::encode($data->visitors); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activateControl')); ?>:</b>
	<?php echo CHtml::encode($data->activateControl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('originalId')); ?>:</b>
	<?php echo CHtml::encode($data->originalId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword')); ?>:</b>
	<?php echo CHtml::encode($data->keyword); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueText')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueText); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('statistics')); ?>:</b>
	<?php echo CHtml::encode($data->statistics); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currencyId')); ?>:</b>
	<?php echo CHtml::encode($data->currencyId); ?>
	<br />

	*/ ?>

</div>