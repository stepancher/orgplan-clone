<?php
/* @var $this FairController */
/* @var $model Fair */
/* @var $fairInfo FairInfo */

$this->menu=array(
	array('label'=>'List Fair', 'url'=>array('index')),
	array('label'=>'Create Fair', 'url'=>array('create')),
	array('label'=>'View Fair', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Fair', 'url'=>array('admin')),
);
?>

<h1>Редактирование выставки <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'fairInfo'=>$fairInfo)); ?>