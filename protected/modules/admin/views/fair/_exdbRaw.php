<?php
/**
 * @var $this  FairController
 * @var $model Fair
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets.css'), false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/base.css');
?>

<div style="border: 5px solid red">
    <?= CHtml::decode($model->exdbRaw) ?>
</div>