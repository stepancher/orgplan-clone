<?php
/**
 * @var FairController  $this
 * @var Fair            $fairId
 */
?>

    <h1>История изменения имен выставок</h1>

    <div style="float: left;">

        <?= TbHtml::linkButton(
            'Вернуться к списку',
            [
                'url'   => $this->createUrl('admin'),
                'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
                'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;"
            ]
        ); ?>

        <?= TbHtml::linkButton(
            'Редактировать выставку',
            [
                'url'   => $this->createUrl('update', array("id" => $fairId)),
                'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
                'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;"
            ]
        ); ?>
    </div>

    <div style="clear: both;"></div>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'      => 'fair-grid',
    'htmlOptions' => array(
        'style' => "overflow-y: auto;"
    ),
    'columns' => array(
        'scdId',
        'id' => array(
            'name' => 'id',
            'header' => 'id trFair',
            'type' => 'raw',
        ),
        'trParentId' => array(
            'name' => 'trParentId',
            'header' => 'id Выставки',
            'type' => 'raw',
        ),
        'langId' => array(
            'name' => 'langId',
            'header' => 'Язык',
            'type' => 'raw',
        ),
        'shortUrl' => array(
            'name' => 'shortUrl',
            'header' => 'Короткая ссылка',
            'type' => 'raw',
        ),
        'name' => array(
            'name' => 'name',
            'header' => 'Наименование',
            'type' => 'raw',
        ),
        'description' => array(
            'name' => 'description',
            'header' => 'Описание',
            'type' => 'raw',
        ),
        'manipulateDatetime' => array(
            'name' => 'manipulateDatetime',
            'header' => 'Дата изменения',
            'type' => 'raw',
        ),
        'manipulateUser' => array(
            'name' => 'manipulateUser',
            'header' => 'Изменивший пользователь',
            'type' => 'raw',
        ),
    ),
    'dataProvider' => $dataProvider,
    'filter'       => $model,
    'ajaxUpdate'   => false,
));