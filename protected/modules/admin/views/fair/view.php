<?php
/* @var $this FairController */
/* @var $model Fair */
/**
 * @var array   $organizerData
 * @var array   $organizerRows
 * @var string   $organizerCompanyLink
 * @var string   $fairAssociationsString
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();

$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/css/page-fair.css');
?>
</br>
<?= CHtml::link('К списку', Yii::app()->createUrl('admin/fair/admin'), ['class' => 'btn btn-success']) ?>&nbsp;
<?= CHtml::link('Редактировать', Yii::app()->createUrl('admin/fair/update', ['id' => $model->id]), ['class' => 'btn btn-warning']) ?>&nbsp;

<?php if (isset($model->translate)) : ?>
    <?php
    echo TbHtml::linkButton('  ' .
        'Просмотр на сайте', array(
        'class' => 'btn d-bg-primary-dark',
        'url' => Yii::app()->createUrl('fair/html/view', array('urlFairName' => $model->translate->shortUrl))
    ));
    ?>
<?php endif; ?>

<h1>Просмотр выставки #<?php echo $model->id; ?></h1>
<?php
$rating = '<div title="' . Fair::getTitleRatingIcon($model) . '"
         class="b-switch-list-view__cell rating-logo rating-logo-type-' . $model->rating . '-' . $model->getCountryStyle() . '">
        <div style="width: 30px; height: 30px;"></div>
    </div>';
?>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array_merge(array(
        'id',
        'storyId',
        'name',
        [
            'name' => 'fairIsForum',
            'type' => 'raw',
            'value' => function($data){
                /**@var Fair    $data */
                if(!isset($data->fairInfo))
                    return 0;
                if($data->fairInfo->forumId == $data->id)
                    return 1;
                return 0;
            }
        ],
        [
            'name' => 'forumId',
            'label' => Yii::t('AdminModule.admin', 'Fair forum'),
            'type' => 'raw',
            'value' => function($data){
                return FairAdminService::getForumByFairId($data->id);
            }
        ],
        'description',
        [
            'name' => 'organizerId',
            'type' => 'html',
            'value' => function($data){

                $result = '';

                if(isset($data->fairHasOrganizer)){
                    foreach ($data->fairHasOrganizer as $fairHasorganizer){
                        if(isset($fairHasorganizer->organizer) && isset($fairHasorganizer->organizer->company)){
                            $link = CHtml::link(
                                $fairHasorganizer->organizer->id . ' ' . $fairHasorganizer->organizer->company->name,
                                Yii::app()->createUrl('admin/organizer/update', array('id' => $fairHasorganizer->organizer->company->id))
                            );
                            $result .= $link . ', ';
                        }
                    }
                }
                return rtrim($result, ', ');
            }
        ],
        [
            'name' => 'exhibitionComplexId',
            'value' => isset($model->exhibitionComplex) && isset($model->exhibitionComplex->translate) ?
                CHtml::link(
                    $model->exhibitionComplex->name,
                    Yii::app()->createUrl('admin/exhibitionComplex/view', array('id' => $model->exhibitionComplex->id)),
                    array(
                        'target' => '_blank',
                    )
                ) : null,
            'type' => 'raw'
        ],
        [
            'label' => 'Отрасль',
            'value' => function($data) {
                if(isset($data->fairHasIndustries))
                    return FairAdminService::getIndustriesInString($data);
                return '';
            }
        ],
        [
            'name' => 'rating',
            'value' => $rating,
            'type' => 'html'
        ],
        [
            'label' => Yii::t('AdminModule.admin', 'Fair association'),
            'value' => $fairAssociationsString,
        ],
        [
            'name' => 'audit',
            'label' => Yii::t('AdminModule.admin', 'Audit'),
            'value' => function($data){
                $result = '';
                if(empty($data->fairHasAudit))
                    return $result;
                foreach ($data->fairHasAudit as $fhau)
                    if(isset($fhau->audit->name))
                        $result .= $fhau->audit->name . ', ';
                return rtrim($result, ', ');
            }
        ],
        'beginDate',
        'endDate',
        'beginMountingDate',
        'endMountingDate',
        'beginDemountingDate',
        'endDemountingDate',
        [
            'name' => 'duration',
            'value' => isset($model->duration) ? $model->duration . ' дн.' : '',
            'type' => 'raw'
        ],
        [
            'name' => 'squareGross',
            'label' => Yii::t('AdminModule.admin','Square gross'),
            'type' => 'raw',
            'value' => $fairInfo->squareGross,
        ],
        [
            'name' => 'squareNet',
            'label' => Yii::t('AdminModule.admin','Square net'),
            'type' => 'raw',
            'value' => $fairInfo->squareNet,
        ],
        [
            'name' => 'members',
            'label' => Yii::t('AdminModule.admin','Members'),
            'type' => 'raw',
            'value' => $fairInfo->members,
        ],
        [
            'name' => 'visitors',
            'label' => Yii::t('AdminModule.admin','Visitors'),
            'type' => 'raw',
            'value' => $fairInfo->visitors,
        ],
        [
            'label' => Yii::t('AdminModule.admin','common statistic fair'),
            'type' => 'raw',
            'value' => function($data){
                return FairAdminService::getStatisticRelatedFairs($data->id);
            }
        ],
        [
            'name' => 'statistics',
            'label' => Yii::t('AdminModule.admin','statistics'),
            'type' => 'raw',
            'value' => isset($model->translate) ? $model->translate->statistics : '',
        ],
        [
            'name' => 'statisticsDate',
            'label' => Yii::t('AdminModule.admin','statisticsDate'),
            'type' => 'raw',
            'value' => $fairInfo->statisticsDate,
        ],
        [
            'name' => 'frequency',
            'value' => isset($model::$frequencies[$model->frequency]) ? $model::$frequencies[$model->frequency] : null,
            'type' => 'raw'
        ],
        'participationPrice',
        'registrationFee',
        [
            'name' => 'currencyId',
            'value' => isset($model->currency) ? $model->currency->name : null
        ],
        [
            'name' => 'site',
            'value' => CHtml::link($model->site, CHtml::normalizeUrl($model->site), array('target'=>'_blank')),
            'type' => 'raw',
            'label' => Yii::t('AdminModule.admin', 'Site')
        ],
        [
            'name' => 'uniqueText',
            'value' => $model->uniqueText,
            'type' => 'html'
        ],
        'keyword',
        [
            'name' => 'active',
            'value' => Fair::getRead($model->active)
        ],
        [
            'name' => 'canceled',
            'value' => Fair::getCancelStatus($model->canceled)
        ],
        ),
        array(
            'datetimeModified',
        ),
        array(
            [
                'label' => Yii::t('AdminModule.admin','user redactor email'),
                'type' => 'raw',
                'visible' => TRUE,
                'value' => function($data){
                    if(isset($data->userRedactor->contact))
                        return $data->userRedactor->contact->email;
                    return '';
                }
            ]
        ),
        array('officialComment'),
        array('lang'),
        array(
            [
                'name' => Yii::t('AdminModule.admin', 'Logo'),
                'type' => 'raw',
                'value' => function($data){
                    $result = '';
                    $objectFiles = ObjectFile::getFileList($data, ObjectFile::TYPE_FAIR_LOGOS, true);
                    if(is_array($objectFiles) && count($objectFiles) > 0)
                        foreach ($objectFiles as $images)
                            if(isset($images['assetPath']))
                                $result .= CHtml::image($images['assetPath']);
                    return $result;
                },
            ]
        ),
        array(
            [
                'name' => Yii::t('AdminModule.admin', 'Mail logo'),
                'type' => 'raw',
                'value' => function($data){
                    $result = '';
                    $objectFiles = ObjectFile::getFileList($data, ObjectFile::TYPE_FAIR_MAIL_LOGOS, true);
                    if(is_array($objectFiles) && count($objectFiles) > 0)
                        foreach ($objectFiles as $images)
                            if(isset($images['assetPath']))
                                $result .= CHtml::image($images['assetPath']);
                    return $result;
                },
            ]
        ),array(
            [
                'name' => Yii::t('AdminModule.admin', 'Is fair logo loaded'),
                'type' => 'raw',
                'value' => function($data){
                    $result = 0;
                    $objectFiles = ObjectFile::getFileList($data, ObjectFile::TYPE_FAIR_LOGOS, true);
                    if(is_array($objectFiles) && count($objectFiles) > 0){

                        $result =  TRUE;
                    }

                    return $result;
                },
            ],
        )
    ),
)); ?>

<!--</br>-->
<!--<div style="font-size: 24px">-->
<!--    --><?//= Yii::t('AdminModule.admin', 'Expodatabase rows') ?>
<!--</div>-->
<!---->
<?php //$this->widget('zii.widgets.CDetailView', array(
//    'data' => $model,
//    'attributes' => array(
//            'exdbId',
//            'exdbFrequency',
//            'exdbBusinessSectors',
//            'exdbCosts',
//            'exdbShowType',
//            'exdbFirstYearShow',
//        )
//    )
//); ?>
<br>
<div>
    <?php if(!empty($organizerData) && !empty($organizerRows)) : ?>
            <?php foreach ($organizerData as $key => $organizers) : ?>
            <div style="float: left">
                <?php if(isset($organizers['companyId'])) : ?>
                    <?php $organizerCompanyLink = "<a target=\"_blank\" href=".Yii::app()->createUrl('admin/organizer/update', array('id' => $organizers['companyId']))." class=\"b-button b-button-icon f-text--tall d-text-light d-bg-warning\" data-icon=\"&#xe0ea;\" ></a>"; ?>
                <?php else : ?>
                    <?php $organizerCompanyLink = '' ?>
                <?php endif; ?>
                <div style="font-size: 24px"><?= $organizers['companyId'] . ' ' . $organizers['organizerName'] . ' ' . $organizerCompanyLink ?></div>
                <br>
                    <?php $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $organizers,
                        'attributes' => $organizerRows[$key],
                    )); ?>
            </div>
            <?php endforeach; ?>
    <?php endif;?>
</div>

<div style="clear:both"></div>
<br>
<?= CHtml::link('К списку', Yii::app()->createUrl('admin/fair/admin'), ['class' => 'btn btn-success']) ?>&nbsp;
<?= CHtml::link('Редактировать', Yii::app()->createUrl('admin/fair/update', ['id' => $model->id]), ['class' => 'btn btn-warning']) ?>&nbsp;

<?php if (isset($model->translate)) : ?>
<?php
echo TbHtml::linkButton('  ' .
    'Просмотр на сайте', array(
    'class' => 'btn d-bg-primary-dark',
    'url' => Yii::app()->createUrl('fair/html/view', array('urlFairName' => $model->translate->shortUrl))
));
?>
<?php endif; ?>
</br>
</br>
