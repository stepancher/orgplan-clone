<?php

/**
 * @var $this       FairController
 * @var $model      Fair
 * @var $fairInfo   FairInfo
 * @var $form       CActiveForm
 */

?>

<?php $action = $this->action->id; ?>

<form method="POST" enctype="multipart/form-data"
      action="<?= Yii::app()->createUrl('admin/fair/' . $action, !empty($model->id) ? array('id' => $model->id) : array()) ?>"
      xmlns:float="http://www.w3.org/1999/xhtml">

    <div class="cols-row">
        <div class="col-md-12">
            <button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <a href="<?= Yii::app()->createUrl('admin/fair/admin')?>"
               class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Cancel') ?>
            </a>
            <?php if($action !== 'create') : ?>
            <a href="<?= $this->createUrl('view', array('id' => $model->id,)) ?>"
               class="js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
                Просмотр
            </a>
            <a href="<?= $this->createUrl('copy', array('id' => $model->id,)) ?>"
               class="js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
                Копировать
            </a>
            <?php
            echo TbHtml::linkButton('  ' .
                'Просмотр на сайте', array(
                'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
                'url' => Yii::app()->urlManager->createUrl('fair/default/view', array('id' => $model->id), Yii::app()->params['defaultAmpersand'], FALSE)
            ));
            ?>
            <?php endif; ?>
        </div>
    </div>

    <style>
        .redactor-toolbar {
            z-index: 10;
        }
    </style>

            <div class="form-group field-fair-name">
                <label class="control-label" for="Fair_name"><?= Yii::t('AdminModule.admin', 'Name') ?></label>
                <input type="text" id="Fair_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Fair[name]" value="<?= $model->name ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-fair-description">
                <label class="control-label" for="Fair_description"><?= Yii::t('AdminModule.admin', 'Description') ?></label>
                <textarea id="Fair_description" class="b-form__text input-block-level" name="Fair[description]" rows="10"><?=$model->description?></textarea>
                <div class="help-block"></div>
            </div>

            <div id="organizer-widget" style="margin-bottom: 20px;"> <?= Yii::t('AdminModule.admin','Organizer') ?>
                <?php $create = '<div id=\"create-link\" class=\"select2-result-label\"><a style=\"display: block\" target=\"_blank\" href=\"'.Yii::app()->createUrl('admin/organizer/create').'\">'.Yii::t('AdminModule.admin','Organizer didn\'t find. Create new organizer').'</a></div>'; ?>
                <?php $organizers = FairAdminService::getOrganizersByFairId($model->id);
                $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                    'name' => ucfirst(get_class($model))."[".Fair::ORGANIZER_VARIABLE."]",
                    'value' => !empty($organizers) ? json_encode($organizers) : '',
                    'options' => array(
                        'formatNoMatches' => 'js:function(){return "' . $create . '";}',
                        'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                        'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                        'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                        'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                        'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                        'width' => '100%',
                        'placeholder' => "Выберите организатора",
                        'multiple' => true,
                        'minimumInputLength' => 1,
                        'ajax' => array(
                            'url' => $this->createUrl('getOrganizers'),
                            'dataType' => 'json',
                            'data' => 'js:function (term, page) { return { q: term}; }',
                            'results' => 'js:function(data,page){return {results: data};}',
                        ),
                        'formatResult'    => 'js:function(data){
                                return data.id + " " + data.name;
                            }',
                        'formatSelection' => 'js: function(data) {
                                return data.id + " " + data.name;
                            }',
                        'initSelection' => 'js:function (element, callback) {
                                // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                var items = JSON.parse(element.val());
                                element.val(items.id);
                                callback(items);
                            }',
                    ),
                    'htmlOptions' => array(
                        'class' => '',
                        'style' => 'border:none;margin-top:10px;',
                    ),
                ));
                ?>
            </div>

            <div style="margin-bottom: 20px" id="s2id_industry"><?= Yii::t('AdminModule.admin','Industries')?>
                <?php $value = FairAdminService::getIndustriesByFairId($model->id);
                $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                        'name' => ucfirst(get_class($model))."[".Fair::INDUSTRY_VARIABLE."]",
                        'value' => !empty($value[0]['id']) ? json_encode($value) : '',
                        'options' => array(
                            'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                            'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                            'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                            'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                            'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                            'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                            'width' => '100%',
                            'placeholder' => "",
                            'multiple' => true,
                            'minimumInputLength' => 1,
                            'ajax' => array(
                                'url' => $this->createUrl('getIndustries'),
                                'dataType' => 'json',
                                'data' => 'js:function (term, page) { return { q: term}; }',
                                'results' => 'js:function(data,page){return {results: data};}',
                            ),
                            'formatResult'    => 'js:function(data){
                                    return data.id + " " + data.name;
                                }',
                            'formatSelection' => 'js: function(data) {
                                    return data.id + " " + data.name;
                                }',
                            'initSelection' => 'js:function (element, callback) {
                                    // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                    var items = JSON.parse(element.val());
                                    element.val("");
                                    callback(items);
                                }',
                        ),
                        'htmlOptions' => array(
                            'class' => '',
                            'style' => 'border:none;margin-top:10px;',
                        ),
                    ));
                ?>
            </div>

             <?php echo CHtml::checkBox('fairIsForum', FairAdminService::isForum($model), array('id' => 'is-forum','style'=>'float: left')) ?>
                <label for="is-forum"><?=Yii::t('AdminModule.admin','Forum')?></label>

            <style>
                .remove-related-fair{
                    cursor: pointer;
                    font-weight: bold;
                }
            </style>

    <div class="row-fluid statistic-plugin">
        <div class="col-md-5" style="margin-top: 68px">
            <select name="from[]" id="multiselect_to_2" style="width: 100%" class="multiselect form-control" size="<?php echo Fair::MULTISELECT_SIZE ?>" multiple="multiple"
                    data-right="#multiselect_to_1"
                    data-right-all="#right_All_1"
                    data-right-selected="#right_Selected_1"
                    data-left-all="#left_All_1"
                    data-left-selected="#left_Selected_1"">

                <?php if($model->id !== NULL && $model->infoId !== NULL) : ?>
                    <?php foreach (FairAdminService::getRelatedStatisticFairs($model->id) as $relatedFair) : ?>
                    <option id="select-left-option-id-<?= $relatedFair['id']?>" value="<?= $relatedFair['id'] ?>"
                            <?php if($relatedFair['forumId'] == $relatedFair['id']) : ?>style="font-weight: bold"<?php endif; ?>>
                        <?php $association = '';
                                if(!empty($relatedFair['associationName']))
                                    $association = $relatedFair['associationName'] . ', ';
                            ?>
                        <?= $relatedFair['id'].' '.$relatedFair['name'].' '.$relatedFair['beginDate'].' ('.
                        $association .'рейтинг: '.$relatedFair['rating'].')' ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
                
            </select>
        </div>

        <div class="col-md-1" style="margin-top: 68px">
            <button type="button" id="right_All_1" class="btn btn-block"><i class="icon-black icon-forward"></i></button>
            <button type="button" id="right_Selected_1" class="btn btn-block"><i class="icon-black icon-chevron-right"></i></button>
            <button type="button" id="left_Selected_1" class="btn btn-block"><i class="icon-black icon-chevron-left"></i></button>
            <button type="button" id="left_All_1" class="btn btn-block"><i class="icon-black icon-backward"></i></button>
            <button type="button" id="devide_selected_1" class="btn btn-block"><i class="icon-black icon-remove"></i></button>
        </div>

        <div class="col-md-5">

            <div id="common-statistic-widget" style="margin-bottom: 10px;">
                <?php $this->widget('application.modules.admin.components.reselect2.RESelect2', array(
                    'name' => ucfirst(get_class($model))."[".Fair::INFO_VARIABLE."]",
                    'value' => FairAdminService::getCommonStatisticFair($model),
                    'options' => array(
                        'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                        'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                        'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                        'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                        'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                        'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                        'width' => '100%',
                        'placeholder' => "Выберите выставку",
                        'multiple' => FALSE,
                        'minimumInputLength' => 1,
                        'ajax' => array(
                            'url' => $this->createUrl('getFairs'),
                            'dataType' => 'json',
                            'data' => 'js:function (term, page) { return { q: term}; }',
                            'results' => 'js:function(data,page){
                                return {results: data};
                            }',

                        ),
                        'formatResult'    => 'js:function(data){
                                return data.id + " " + data.name + " " + data.yearDate;
                            }',
                        'formatSelection' => "js: function(data) {
                                return '<span id=\"common-statistic-select\" data-common-info-id=\"'+data.infoId+'\">' + data.id + ' ' + data.name + ' ' + data.yearDate + '</span>';
                            }",
                    ),
                    'htmlOptions' => array(
                        'class' => '',
                        'id' => 'fairInfoDropDown',
                        'style' => 'border:none;margin-top:10px;',
                    ),
                    'script' => "function(e) {
                        $.ajax({
                            url:'".$this->createUrl('GetStatisticFairs')."',
                            dataType:'json',
                            data:{'fairId': e.val},
                            success:function(data){
                                var res = '';
                                var boldFlag = '';
                                
                                for(i in data){
                                
                                boldFlag = '';
                                
                                if(data[i]['forumId'] == data[i]['id']){
                                    boldFlag = 'style=\"font-weight:bold\"';
                                }
                                    var association = '';
                                    
                                    if(data[i]['association'] != '' && data[i]['associationName'] != undefined){
                                        association = data[i]['associationName'] + ', ';
                                    }
                                    
                                    res += '<option id=\"select-right-option-id-'+data[i]['id']+'\" value=\"' + data[i]['id'] + '\" '+boldFlag+'>' + 
                                        data[i]['id'] + ' ' + data[i]['name'] + ' ' + data[i]['beginDate'] + ' (' + association + 'рейтинг: ' + data[i]['rating'] + ')' + '</option>';
                                }
                                $('#multiselect_to_1').html(res);
                            }
                        });
                    }"
                ));
                ?>
            </div>

            <select name="to[]" style="width: 100%" id="multiselect_to_1" class="form-control" size="<?= Fair::MULTISELECT_SIZE+1 ?>" multiple="multiple">
            </select>
        </div>
    </div>

    <?php
    /** @var CWebApplication $app */
        $app = Yii::app();
    /** @var CClientScript $cs */
        $cs = $app->getClientScript();
    /** @var ZAssetManager $am */
        $am = $app->getAssetManager();

        $publishUrl = $am->publish(Yii::getPathOfAlias('ext.multiselect.dist.js'), false, -1, YII_DEBUG);
        $cs->registerScriptFile($publishUrl.'/multiselect.min.js', $cs::POS_HEAD);
    ?>

    <script type="text/javascript">
        jQuery(document).ready(function($) {

            <?php if(Yii::app()->controller->action->id == 'create' ||
                        Yii::app()->controller->action->id == 'copy') : ?>
            $(".statistic-plugin").find("input, select, button").each(function (i, el)
            {
                $(el).prop("disabled", true);
            });

            <?php endif; ?>

            <?php if(Yii::app()->controller->action->id == 'update') : ?>

            var multy = $('.multiselect').multiselect({
                'beforeMoveToLeft' : function(left,right,options){

                    if(right.val() === null){

                        var fairIds = [];

                        $.each(right.children(),function(index,element){
                            fairIds.push($(element).val());
                        });

                    } else {
                        fairIds = right.val();
                    }

                    var res = false;

                    $.ajax({
                        url: '<?php echo $this->createUrl('attachFairStatistic') ?>',
                        method: 'POST',
                        async: false,
                        dataType: 'json',
                        data: {'fairId': fairIds, 'infoId': <?= $model->infoId ?>},
                        success: function(data){

                            if(data.status == 'ok'){
                                res = true;
                            } else{
                                alert("Fairs statistic didn\'t attach!" + ' ' + data.message);
                            }
                        }

                    });
                    return res;
                },
                'beforeMoveToRight' : function(left,right,options){

                    if(left.val() === null){

                        var fairIds = [];

                        $.each(left.children(),function(index,element){
                            fairIds.push($(element).val());
                        });

                    } else {
                        fairIds = left.val();
                    }

                    var res = false;
                    var infoId = $('#common-statistic-select').data('commonInfoId');

                    $.ajax({
                        url: '<?php echo $this->createUrl('attachFairStatistic') ?>',
                        method: 'POST',
                        async: false,
                        dataType: 'json',
                        data: {'fairId': fairIds, 'infoId': infoId},
                        success: function(data){

                            if(data.status == 'ok'){
                                res = true;
                            } else{
                                alert('Fairs statistic didn\'t attahed!');
                            }
                        }

                    });
                    return res;
                }
            });

            <?php endif; ?>
        });

        $(function(e){
            $('#devide_selected_1').click(function(e){
                var fairIds = $('#multiselect_to_2').val();
                $.ajax({
                    url: '<?php echo $this->createUrl('SeparateRelatedStatisticFair') ?>',
                    method: 'POST',
                    dataType: 'json',
                    data: {'fairId': fairIds},
                    success:function(data){

                        $.each(data,function (index, value) {
                            if(value.success == true){
                                $('#select-left-option-id-'+value.fairId).remove();
                            }
                        });
                    }
                });

            });
        });
    </script>

            <div style="float:left;width:200px;" class="form-group field-fair-visitors">
                <label class="control-label" for="FairInfo_visitors"><?= Yii::t('AdminModule.admin', 'Visitors') ?></label>
                <input type="text" id="FairInfo_visitors" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="FairInfo[visitors]" value="<?= $fairInfo->visitors ?>">
                <div class="help-block"></div>
            </div>

            <div style="float:left;width:200px;margin-left:20px;" class="form-group field-fair-members">
                <label class="control-label" for="FairInfo_members"><?= Yii::t('AdminModule.admin', 'Participants') ?></label>
                <input type="text" id="FairInfo_members" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="FairInfo[members]" value="<?= $fairInfo->members ?>">
                <div class="help-block"></div>
            </div>

            <div style="float:left;width:200px;margin-left:20px;" class="form-group field-fair-squareNet">
                <label class="control-label" for="FairInfo_squareNet"><?= Yii::t('AdminModule.admin', 'Square net') ?></label>
                <input type="text" id="FairInfo_squareNet" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="FairInfo[squareNet]" value="<?= $fairInfo->squareNet ?>">
                <div class="help-block"></div>
            </div>

            <div style="float:left;width:200px;margin-left:20px;" class="form-group field-fair-squareGross">
                <label class="control-label" for="FairInfo_squareGross"><?= Yii::t('AdminModule.admin', 'Square gross') ?></label>
                <input type="text" id="FairInfo_squareGross" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="FairInfo[squareGross]" value="<?= $fairInfo->squareGross ?>">
                <div class="help-block"></div>
            </div>

            <div style="clear:both"></div>

            <div id="btn-detail-button" style="cursor: pointer;font-weight: bold;">Детализация <span id="btn-detail-icon">+</span></div>

            <div id="detail-box" style="display: none;">

                <table cellpadding="10">
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'areaClosedExhibitorsLocal');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'areaClosedExhibitorsLocal');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'areaClosedExhibitorsForeign');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'areaClosedExhibitorsForeign');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'areaOpenExhibitorsLocal');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'areaOpenExhibitorsLocal');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'areaOpenExhibitorsForeign');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'areaOpenExhibitorsForeign');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'areaSpecialExpositions');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'areaSpecialExpositions');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'exhibitorsLocal');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'exhibitorsLocal');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'exhibitorsForeign');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'exhibitorsForeign');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'countriesCount');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'countriesCount');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'visitorsLocal');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'visitorsLocal');?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeTextField($fairInfo, 'visitorsForeign');?></td>
                        <td><?php echo CHtml::activeLabel($fairInfo, 'visitorsForeign');?></td>
                    </tr>
                </table>
            </div>
            <br/>

            <div class="clearfix"></div>

            <div class="form-group field-fair-statistics">
                <label class="control-label" for="FairInfo_statistics"><?= Yii::t('AdminModule.admin', 'Statistics') ?></label>
                <textarea id="FairInfo_statistics" class="b-form__text input-block-level" name="Fair[statistics]" rows="10"><?= isset($model->translate) ? $model->translate->statistics : ''?></textarea>
                <div class="help-block"></div>
            </div>


            <div style="float:left;width:200px;">
                Год статистики
                <?= CHtml::activeDropDownList($fairInfo, 'statisticsDate', FairAdminService::getStatisticDates(),
                    array(
                        'empty' => 'Не задано'
                    ))
                ?>
            </div>
            <div class="clearfix"></div>
            <br/>

            <div style="float:left;width:200px;">
                <?= Yii::t('AdminModule.admin', 'Fair frequency') ?>
                <?= CHtml::activeDropDownList($model, 'frequency', Fair::$frequencies,
                    array(
                        'empty' => 'Не задано'
                    ))
                ?>
            </div>

            <div class="clearfix"></div>
            <br/>
            <div id="exhibition-widget" style="margin-bottom: 20px;"> <?= Yii::t('AdminModule.admin','Exhibition Complex') ?>
                <?php
                $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                    'name' => ucfirst(get_class($model))."[".Fair::EXHIBITION_VARIABLE."]",
                    'value' => isset($model->exhibitionComplex) ? json_encode(array('id' => $model->exhibitionComplex->id, 'name' => $model->exhibitionComplex->name)) : NULL,
                    'options' => array(
                        'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                        'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                        'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                        'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                        'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                        'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                        'width' => '100%',
                        'placeholder' => "Выберите выставочный центр",
                        'minimumInputLength' => 1,
                        'ajax' => array(
                            'url' => $this->createUrl('getExhibitionComplexes'),
                            'dataType' => 'json',
                            'data' => 'js:function (term, page) { return { q: term}; }',
                            'results' => 'js:function(data,page){return {results: data};}',

                        ),
                        'formatResult'    => 'js:function(data){
                                            return data.id + " " + data.name;
                                        }',
                        'formatSelection' => 'js: function(data) {
                                            return data.id + " " + data.name;
                                        }',
                        'initSelection' => 'js:function (element, callback) {
                                            // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                            var items = JSON.parse(element.val());
                                            element.val(items.id);
                                            callback(items);
                                        }',
                    ),
                    'htmlOptions' => array(
                        'class' => '',
                        'style' => 'border:none;margin-top:10px;',
                    ),
                ));
                ?>
            </div>

            <div class="form-group field-fair-price">
                <label class="control-label" for="Fair_price"><?= Yii::t('AdminModule.admin', 'Cost of accreditation') ?></label>
                <input type="text" id="Fair_price" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Fair[price]" value="<?= $model->price ?>">
                <div class="help-block"></div>
            </div>

            <div style="width:200px;float:left;">
                <div class="control-group">
                    <label class="control-label required" for="Fair_beginDate">
                        <?= Yii::t('AdminModule.admin', 'Start date')?>
                        <span class="required">*</span>
                    </label>

                    <div class="controls">
                        <?php

                        $input = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
                            'model' => $model,
                            'attribute' => 'beginDate',
                            'htmlOptions' => array(
                                'type' => 'text',
                                'placeholder' => \Yii::t('AdminModule.admin', 'Choose'),
                                'value' => $model->beginDate ? date('d.m.Y', strtotime($model->beginDate)) : date('d.m.Y', time()),
                                'class' => 'b-form__string input-block-level grd-white',
                            ),
                            'pluginOptions' => array(
                                'format' => 'dd.mm.yyyy',
                                'language' => Yii::app()->language,
                                'weekStart' => 1,
                            ),
                        ), true);

                        echo TbHtml::activeControlGroup(
                            TbHtml::INPUT_TYPE_TEXT,
                            $model,
                            '',
                            [
                                'input' => $input,
                            ]
                        );

                        ?>
                    </div>
                </div>
            </div>

            <div style="width:200px;float:left;margin-left:20px;">
                <div class="control-group">
                    <label class="control-label required" for="Fair_endDate">
                        <?= Yii::t('AdminModule.admin', 'Expiration date')?>
                    </label>
            
                    <div class="controls">
                        <?php
            
                        $input = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
                            'model' => $model,
                            'attribute' => 'endDate',
                            'htmlOptions' => array(
                                'type' => 'text',
                                'placeholder' => \Yii::t('AdminModule.admin', 'Choose'),
                                'value' => $model->endDate ? date('d.m.Y', strtotime($model->endDate)) : date('d.m.Y', time()),
                                'class' => 'b-form__string input-block-level grd-white',
                            ),
                            'pluginOptions' => array(
                                'format' => 'dd.mm.yyyy',
                                'language' => Yii::app()->language,
                                'weekStart' => 1,
                            ),
                        ), true);
            
                        echo TbHtml::activeControlGroup(
                            TbHtml::INPUT_TYPE_TEXT,
                            $model,
                            '',
                            [
                                'input' => $input,
                            ]
                        );
            
                        ?>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div style="width:200px;float:left;">
                <div class="control-group">
                    <label class="control-label required" for="Fair_beginMountingDate">
                        <?= Yii::t('AdminModule.admin', 'Date of commencement of installation')?>
                    </label>
            
                    <div class="controls">
                        <?php
            
                        $input = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
                            'model' => $model,
                            'attribute' => 'beginMountingDate',
                            'htmlOptions' => array(
                                'type' => 'text',
                                'placeholder' => \Yii::t('AdminModule.admin', 'Choose'),
                                'value' => $model->beginMountingDate ? date('d.m.Y', strtotime($model->beginMountingDate)) : date('d.m.Y', time()),
                                'class' => 'b-form__string input-block-level grd-white',
                            ),
                            'pluginOptions' => array(
                                'format' => 'dd.mm.yyyy',
                                'language' => Yii::app()->language,
                                'weekStart' => 1,
                            ),
                        ), true);
            
                        echo TbHtml::activeControlGroup(
                            TbHtml::INPUT_TYPE_TEXT,
                            $model,
                            '',
                            [
                                'input' => $input,
                            ]
                        );
            
                        ?>
                    </div>
                </div>
            </div>

            <div style="width:200px;float:left;margin-left:20px;">
                <div class="control-group">
                    <label class="control-label required" for="Fair_endMountingDate">
                        <?= Yii::t('AdminModule.admin', 'End date of installation')?>
                    </label>
            
                    <div class="controls">
                        <?php
            
                        $input = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
                            'model' => $model,
                            'attribute' => 'endMountingDate',
                            'htmlOptions' => array(
                                'type' => 'text',
                                'placeholder' => \Yii::t('AdminModule.admin', 'Choose'),
                                'value' => $model->endMountingDate ? date('d.m.Y', strtotime($model->endMountingDate)) : date('d.m.Y', time()),
                                'class' => 'b-form__string input-block-level grd-white',
                            ),
                            'pluginOptions' => array(
                                'format' => 'dd.mm.yyyy',
                                'language' => Yii::app()->language,
                                'weekStart' => 1,
                            ),
                        ), true);
            
                        echo TbHtml::activeControlGroup(
                            TbHtml::INPUT_TYPE_TEXT,
                            $model,
                            '',
                            [
                                'input' => $input,
                            ]
                        );
            
                        ?>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div style="width:200px;float:left;">
                <div class="control-group">
                    <label class="control-label required" for="Fair_beginDemountingDate">
                        <?= Yii::t('AdminModule.admin', 'Date of commencement of dismantling')?>
                    </label>
            
                    <div class="controls">
                        <?php
            
                        $input = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
                            'model' => $model,
                            'attribute' => 'beginDemountingDate',
                            'htmlOptions' => array(
                                'type' => 'text',
                                'placeholder' => \Yii::t('AdminModule.admin', 'Choose'),
                                'value' => $model->beginDemountingDate ? date('d.m.Y', strtotime($model->beginDemountingDate)) : date('d.m.Y', time()),
                                'class' => 'b-form__string input-block-level grd-white',
                            ),
                            'pluginOptions' => array(
                                'format' => 'dd.mm.yyyy',
                                'language' => Yii::app()->language,
                                'weekStart' => 1,
                            ),
                        ), true);
            
                        echo TbHtml::activeControlGroup(
                            TbHtml::INPUT_TYPE_TEXT,
                            $model,
                            '',
                            [
                                'input' => $input,
                            ]
                        );
            
                        ?>
                    </div>
                </div>
            </div>

            <div style="width:200px;float:left;margin-left:20px;">
                <div class="control-group">
                    <label class="control-label required" for="Fair_endDemountingDate">
                        <?= Yii::t('AdminModule.admin', 'Date of completion of dismantling')?>
                    </label>
            
                    <div class="controls">
                        <?php
            
                        $input = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
                            'model' => $model,
                            'attribute' => 'endDemountingDate',
                            'htmlOptions' => array(
                                'type' => 'text',
                                'placeholder' => \Yii::t('AdminModule.admin', 'Choose'),
                                'value' => $model->endDemountingDate ? date('d.m.Y', strtotime($model->endDemountingDate)) : date('d.m.Y', time()),
                                'class' => 'b-form__string input-block-level grd-white',
                            ),
                            'pluginOptions' => array(
                                'format' => 'dd.mm.yyyy',
                                'language' => Yii::app()->language,
                                'weekStart' => 1,
                            ),
                        ), true);
            
                        echo TbHtml::activeControlGroup(
                            TbHtml::INPUT_TYPE_TEXT,
                            $model,
                            '',
                            [
                                'input' => $input,
                            ]
                        );
            
                        ?>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="form-group field-fair-participationPrice">
                <label class="control-label" for="Fair_participationPrice"><?= Yii::t('AdminModule.admin', 'Cost of participation') ?></label>
                <input type="text" id="Fair_participationPrice" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Fair[participationPrice]" value="<?= $model->participationPrice ?>">
                <div class="help-block"></div>
            </div>

            <?=
                CHtml::activeDropDownList(
                    $model,
                    'currencyId',
                    array('' => Yii::t('AdminModule.admin', 'Currency')) + CHtml::listData(\Currency::model()->findAll(), 'id', 'name')
                );
            ?>

            <div class="form-group field-fair-registrationFee">
                <label class="control-label" for="Fair_registrationFee"><?= Yii::t('AdminModule.admin', 'Registration fee') ?></label>
                <input type="text" id="Fair_registrationFee" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Fair[registrationFee]" value="<?= $model->registrationFee ?>">
                <div class="help-block"></div>
            </div>

            <?=
            $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
                    'model' => $model,
                    'attribute' => 'uniqueText',
                    'options' => array(
                        'minHeight' => '250px',
                        'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array(
                                'modelName' => get_class($model)
                            )
                        ),
                        'lang' => 'ru',
                        'iframe' => true,
                        'css' => 'wym.css',
                    )
                ), true
            );
            ?>

            <div class="form-group field-fair-site">
                <label class="control-label" for="Fair_site"><?= Yii::t('AdminModule.admin', 'Site') ?></label>
                <input type="text" id="Fair_site" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Fair[site]" value="<?= $model->site ?>">
                <div class="help-block"></div>
            </div>

            <?= Yii::t('AdminModule.admin','Rating') ?>
            <?=CHtml::activedropDownList($model,
                'rating',
                ['' => 'Выбрать'] + \FairAdminService::getRatingName()
            );?>

            <div style="margin:20px 0px;width:100%;font-weight: bold;">
                <span style="float:left;width:20px;margin-left:20px"> <?= Yii::t('AdminModule.admin','Fair association') ?>:</span>
            <?php
            $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                'name' => ucfirst(get_class($model))."[".Fair::ASSOCIATION_VARIABLE."]",
                'value' => $this->action->id == 'copy' ? '' : json_encode(FairAdminService::getAssociationsByFairId($model->id)),
                'options' => array(
                    'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                    'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                    'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                    'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                    'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                    'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                    'width' => '100%',
                    'placeholder' => "",
                    'multiple' => true,
                    'minimumInputLength' => 1,
                    'ajax' => array(
                        'url' => $this->createUrl('getAssociations'),
                        'dataType' => 'json',
                        'data' => 'js:function (term, page) { return { q: term}; }',
                        'results' => 'js:function(data,page){return {results: data};}',

                    ),
                    'formatResult'    => 'js:function(data){
                    return data.id + " " + data.name;
                }',
                    'formatSelection' => 'js: function(data) {
                    return data.id + " " + data.name;
                }',
                    'initSelection' => 'js:function (element, callback) {
                    // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                    var items = JSON.parse(element.val());
                    element.val("");
                    callback(items);
                }',
                ),
                'htmlOptions' => array(
                    'class' => '',
                    'style' => 'border:none;margin-top:10px;',
                ),
            ));
            ?>
            </div>

            <div style="margin-bottom: 20px" id="s2id_audit"><?= Yii::t('AdminModule.admin', 'Audit')?>
                <?php
                $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                    'name' => ucfirst(get_class($model))."[".Fair::AUDIT_VARIABLE."]",
                    'value' => $this->action->id == 'copy' ? '' : json_encode(FairAdminService::getAuditsByFairId($model->id)),
                    'options' => array(
                        'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                        'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                        'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                        'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                        'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                        'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                        'width' => '100%',
                        'placeholder' => "",
                        'multiple' => true,
                        'minimumInputLength' => 1,
                        'ajax' => array(
                            'url' => $this->createUrl('getAudits'),
                            'dataType' => 'json',
                            'data' => 'js:function (term, page) { return { q: term}; }',
                            'results' => 'js:function(data,page){return {results: data};}',
                        ),
                        'formatResult'    => 'js:function(data){
                                            return data.id + " " + data.name;
                                        }',
                        'formatSelection' => 'js: function(data) {
                                            return data.id + " " + data.name;
                                        }',
                        'initSelection' => 'js:function (element, callback) {
                                            // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                            var items = JSON.parse(element.val());
                                            element.val("");
                                            callback(items);
                                        }',
                    ),
                    'htmlOptions' => array(
                        'class' => '',
                        'style' => 'border:none;margin-top:10px;',
                    ),
                ));
                ?>
            </div>
    
            <br/>
            <?= Yii::t('AdminModule.admin', 'Active') ?>
            <?=CHtml::activeDropDownList($model, 'active', array(0 => 'Не активный', 1 => 'Активный'));?>

            <br/>
            <?= Yii::t('AdminModule.admin', 'Canceled') ?>
            <?=CHtml::activeDropDownList($model, 'canceled', array(0 => 'Не отменена', 1 => 'Отменена'));?>

            <div class="form-group field-fair-keyword">
                <label class="control-label" for="Fair_keyword"><?= Yii::t('AdminModule.admin', 'keyword') ?></label>
                <textarea id="Fair_keyword" class="b-form__text input-block-level" name="Fair[keyword]" rows="10"><?=$model->keyword?></textarea>
                <div class="help-block"></div>
            </div>

            <?= CHtml::activeDropDownList($model, 'statusId', ['0' => 'Статус отсутствует'] + FairStatus::getFairStatuses(), array('empty' => ''))
            ?>
            <br/>
            <?= Yii::t('AdminModule.admin', 'Fair lang') ?>
            <br/>
            <?=
                //@TODO move it into application component (get languages list())
                CHtml::activeDropDownList($model,
                    'lang',
                    array_combine(Fair::getLanguageList(), Fair::getLanguageList())
                )
            ?>
            <br/>
            <?php echo CHtml::activeLabel($model, 'storyId');?>
            <?php echo CHtml::activeTextField($model, 'storyId');?>
            <br/>

            <div class="form-group field-fair-officialComment">
                <label class="control-label" for="Fair_officialComment"><?= Yii::t('AdminModule.admin', 'official comment') ?></label>
                <textarea id="Fair_officialComment" class="b-form__text input-block-level" name="Fair[officialComment]" rows="10"><?=$model->officialComment?></textarea>
                <div class="help-block"></div>
            </div>

            <?= $this->widget('application.modules.admin.components.StyledMultiFileUploader', array(
                'model' => $model,
                'attribute' => 'logoId',
                'max' => 1,
                'accept' => ObjectFile::$acceptedImageExt,
                'deleteAction' => Yii::app()->createUrl('file/objectFile/remove', array('id' => '{id}', 'cls' => 'Fair')),
                'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_FAIR_LOGOS, true),
                'outputAsImage' => true,
                'buttonTag' => 'button',
                'buttonOptions' => [
                    'class' => 'b-button d-bg-success-dark d-bg-success--hover',
                    'type' => 'button',
                    'content' => Yii::t('AdminModule.admin', 'Logo')
                ]
            ), true) ?>
            <?= $this->widget('application.modules.admin.components.StyledMultiFileUploader', array(
                'model' => $model,
                'attribute' => 'mailLogoId',
                'max' => 1,
                'accept' => ObjectFile::$acceptedImageExt,
                'deleteAction' => Yii::app()->createUrl('file/objectFile/remove', array('id' => '{id}', 'cls' => 'Fair')),
                'fileData' => ObjectFile::getFileList($model, ObjectFile::TYPE_FAIR_MAIL_LOGOS, true),
                'outputAsImage' => true,
                'buttonTag' => 'button',
                'buttonOptions' => [
                    'class' => 'b-button d-bg-success-dark d-bg-success--hover',
                    'type' => 'button',
                    'content' => Yii::t('AdminModule.admin', 'Mail logo')
                ]
            ), true) ?>

            </br>

            <div id="btn-exdb-button" style="cursor: pointer;font-weight: bold;"><?= Yii::t('AdminModule.admin', 'Expodatabase information') ?> <span id="btn-exdb-icon">+</span></div>

            <div id="exdb-box" style="display: none;">
                <div class="form-group field-fair-exdbId">
                    <label class="control-label" for="Fair_exdbId"><?= Yii::t('AdminModule.admin', 'Expodatabase fair id') ?></label>
                    <input type="text" id="Fair_exdbId" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Fair[exdbId]" value="<?= $model->exdbId ?>">
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-fair-exdbFrequency">
                    <label class="control-label" for="Fair_exdbFrequency"><?= Yii::t('AdminModule.admin', 'Expodatabase fair frequency') ?></label>
                    <textarea id="Fair_exdbFrequency" class="b-form__text input-block-level" name="Fair[exdbFrequency]" rows="10"><?= $model->exdbFrequency ?></textarea>
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-fair-exdbBusinessSectors">
                    <label class="control-label" for="Fair_exdbBusinessSectors"><?= Yii::t('AdminModule.admin', 'Expodatabase fair business sectors') ?></label>
                    <textarea id="Fair_exdbBusinessSectors" class="b-form__text input-block-level" name="Fair[exdbBusinessSectors]" rows="10"><?= $model->exdbBusinessSectors ?></textarea>
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-fair-exdbCosts">
                    <label class="control-label" for="Fair_exdbCosts"><?= Yii::t('AdminModule.admin', 'Expodatabase fair costs') ?></label>
                    <textarea id="Fair_exdbCosts" class="b-form__text input-block-level" name="Fair[exdbCosts]" rows="10"><?= $model->exdbCosts ?></textarea>
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-fair-exdbShowType">
                    <label class="control-label" for="Fair_exdbShowType"><?= Yii::t('AdminModule.admin', 'Expodatabase fair show type') ?></label>
                    <textarea id="Fair_exdbShowType" class="b-form__text input-block-level" name="Fair[exdbShowType]" rows="10"><?= $model->exdbShowType ?></textarea>
                    <div class="help-block"></div>
                </div>

                <div class="form-group field-fair-exdbFirstYearShow">
                    <label class="control-label" for="Fair_exdbFirstYearShow"><?= Yii::t('AdminModule.admin', 'Expodatabase fair first year show') ?></label>
                    <textarea id="Fair_exdbFirstYearShow" class="b-form__text input-block-level" name="Fair[exdbFirstYearShow]" rows="10"><?= $model->exdbFirstYearShow ?></textarea>
                    <div class="help-block"></div>
                </div>
            </div>

            </br>

    <div class="cols-row">
        <div class="col-md-12" style="margin-left: 20px">
            <button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <a href="<?= Yii::app()->createUrl('admin/fair/admin') ?>"
               class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
                <?= Yii::t('AdminModule.admin', 'Cancel') ?>
            </a>
            <?php if($action !== 'create') : ?>
            <a href="<?= $this->createUrl('view', array('id' => $model->id,)) ?>"
               class="js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
                Просмотр
            </a>
            <a href="<?= $this->createUrl('copy', array('id' => $model->id,)) ?>"
               class="js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
                Копировать
            </a>
            <?php
                echo TbHtml::linkButton('  ' .
                    'Просмотр на сайте', array(
                    'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
                    'url' => Yii::app()->urlManager->createUrl('fair/default/view', array('id' => $model->id), Yii::app()->params['defaultAmpersand'], FALSE)
                ));
            ?>
            <?php endif; ?>
        </div>
    </div>

</form>

<?php if(Yii::app()->controller->action->id == 'update'): ?>
    <?php $this->renderPartial('_exdbRaw', array('model' => $model)) ?>
<?php endif; ?>

<script>
    $(function(e){
        $('#btn-exdb-button').click(function (e) {
            if($('#exdb-box').css('display') == 'none'){
                $('#btn-exdb-icon').text('-');
                $('#exdb-box').slideDown();
            }else{
                $('#btn-exdb-icon').text('+');
                $('#exdb-box').slideUp();
            }
        });
    })
</script>

<script>
    $(function(e){
        $('#btn-detail-button').click(function(e){
            if($('#detail-box').css('display') == 'none'){
                $('#btn-detail-icon').text('-');
                $('#detail-box').slideDown();
            }else{
                $('#btn-detail-icon').text('+');
                $('#detail-box').slideUp();
            }
        });
    });
</script>

<script>
    (function ($)
    {
        $(document).delegate('#FairInfo_visitorsForeign, #FairInfo_visitorsLocal', 'input', function ()
        {
            $('#FairInfo_visitors').val(1 * $('#FairInfo_visitorsForeign').val() + 1 * $('#FairInfo_visitorsLocal').val());
        });

    })($);

    (function ($)
    {
        $(document).delegate('#FairInfo_exhibitorsLocal, #FairInfo_exhibitorsForeign', 'input', function ()
        {
            $('#FairInfo_members').val(1 * $('#FairInfo_exhibitorsLocal').val() + 1 * $('#FairInfo_exhibitorsForeign').val());
        });

    })($);

    (function ($)
    {
        var squareNetInputs = ('#FairInfo_areaClosedExhibitorsLocal, #FairInfo_areaClosedExhibitorsForeign, #FairInfo_areaOpenExhibitorsLocal, #FairInfo_areaOpenExhibitorsForeign, #FairInfo_areaSpecialExpositions');

        $(document).delegate(squareNetInputs, 'input', function ()
        {
            $('#FairInfo_squareNet').val(summaryInputs($(squareNetInputs)));
        });

        function summaryInputs(squareNetInputs)
        {
            var result = 0;

            squareNetInputs.each(function (i, item)
            {
                result = result + $(item).val() * 1;
            });

            return result;
        }
    })($);
</script>