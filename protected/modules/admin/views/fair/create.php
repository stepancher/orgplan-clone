<?php
/* @var $this FairController */
/* @var $model Fair */

$this->menu=array(
	array('label'=>'List Fair', 'url'=>array('index')),
	array('label'=>'Manage Fair', 'url'=>array('admin')),
);
?>

<h1>Создание выставки</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'id' => isset($id) ? $id:null, 'fairInfo' => $fairInfo)); ?>