<?php
/* @var $this FairController */
/* @var $model Fair */
/* @var $fairFields array */
/* @var $search CActiveDataProvider */
/* @var $pageVariants array */

$this->menu = array(
    array('label' => 'List Fair', 'url' => array('index')),
    array('label' => 'Create Fair', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fair-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$('#fair-grid-button').on('click', function () {
    var \$fields = $('#Fair_fields .checkbox.checked input');
    var pageSize = $('#Fair_pageSize .drop-list__item--selected input').val();
    var fields = \$fields.map(function() {
        return $(this).val();
    }).get();
    $.fn.yiiGridView.update('fair-grid', {data: {
        Fair_fields: fields.toString(),
        Fair_pageSize: pageSize,
        Fair_page: 1
    }})
});
");

$langId = Yii::app()->language;

?>
<h1>Список выставок</h1>
<div style="height: 40px;">
    <?= TbHtml::linkButton(
        'Добавить новую выставку',
        [
            'url'   => $this->createUrl('create'),
            'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
            'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;",
            'target' => '_blank',
        ]
    ); ?>

    <div style="float: right;">

        <div style="width: 180px; display: inline-block;">
            <?php  $this->widget('application.modules.admin.components.ListAutoComplete', [
                'id'    => 'Fair_pageSize',
                'name'  => 'Fair_pageSize',
                'label' => 'На странице',
                'itemOptions' => [
                    'data' => $pageVariants
                ],
                'selectedItems' => [$search->pagination->pageSize],
                'htmlOptions' => [
                    'style' => 'margin: 0;'
                ],
                'autoComplete' => false,
                'multiple' => false
            ]); ?>
        </div>

        <?php
        $columns = array(
            'fairStatus' => array(
                'name' => 'fairStatus',
                'header' => Yii::t('AdminModule.admin', 'Status'),
                'type' => 'raw',
                'value' => function($data){
                    $fairStatus = $data['fairStatus'];
                    $fairStatusColor = $data['fairStatusColor'];

                    if($data['fairStatus'] === NULL)
                        return 'Статус отсутствует';
                    return '<div style="width: 20px;height: 20px;float: left;margin-right: 5px;background: #' . $fairStatusColor . '"></div>' . $fairStatus;
                },
                'filter' => TbHtml::activeDropDownList($model, 'fairStatus', ['0' => 'Статус отсутствует'] + FairStatus::getFairStatuses(), array('empty' => '')),
                'htmlOptions' => array(
                    'style' => 'min-width: 90px !important; vertical-align: top;',
                )
            ),
            'active' => array(
                'name' => 'active',
                'header' => Yii::t('AdminModule.admin', 'active'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'active', Fair::reads(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'logoExists' => array(
                'name' => 'logoExists',
                'header' => Yii::t('AdminModule.admin', 'Is fair logo loaded'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'logoExists', array(2=>'Прикреплено',1=>'Не прикреплено'), array('empty' => 'Не выбрано')),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exdbId' => array(
                'name' => 'exdbId',
                'header' => Yii::t('AdminModule.admin', 'Expodatabase fair id'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exdbFrequency' => array(
                'name' => 'exdbFrequency',
                'header' => Yii::t('AdminModule.admin', 'Expodatabase fair frequency'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exdbBusinessSectors' => array(
                'name' => 'exdbBusinessSectors',
                'header' => Yii::t('AdminModule.admin', 'Expodatabase fair business sectors'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exdbCosts' => array(
                'name' => 'exdbCosts',
                'header' => Yii::t('AdminModule.admin', 'Expodatabase fair costs'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exdbShowType' => array(
                'name' => 'exdbShowType',
                'header' => Yii::t('AdminModule.admin', 'Expodatabase fair show type'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'exdbShowType', FairAdminService::getExdbShowTypes(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exdbFirstYearShow' => array(
                'name' => 'exdbFirstYearShow',
                'header' => Yii::t('AdminModule.admin', 'Expodatabase fair first year show'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'exdbFirstYearShow', FairAdminService::getExdbFirstYearShows(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'canceled' => array(
                'name' => 'canceled',
                'header' => Yii::t('AdminModule.admin', 'Canceled'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'canceled', Fair::cancelStatus(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'lastYearFair' => array(
                'name' => 'lastYearFair',
                'header' => Yii::t('AdminModule.admin', 'Future exhibitions'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'lastYearFair', FairAdminService::getLastYearFair(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'storyId' =>array(
                'name' => 'storyId',
                'header' => 'storyId',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'fairTranslateName' => array(
                'name'  => 'fairTranslateName',
                'header' => Yii::t('AdminModule.admin', 'Name'),
                'type'  => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'fairEnName' => array(
                'name'  => 'fairEnName',
                'header' => Yii::t('AdminModule.admin', 'en name'),
                'type'  => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'fairDeName' => array(
                'name'  => 'fairDeName',
                'header' => Yii::t('AdminModule.admin', 'de name'),
                'type'  => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'frequency' => array(
                'name' => 'frequency',
                'header' => Yii::t('AdminModule.admin', 'Fair frequency'),
                'value' => function($data) {
                    return isset(Fair::$frequencies[$data['frequency']]) ? Fair::$frequencies[$data['frequency']] : null;
                },
                'filter' => TbHtml::activeDropDownList(
                    $model,
                    'frequency',
                    Fair::$frequencies,
                    array(
                        'empty' => ''
                    )
                ),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => 'min-width: 90px !important; vertical-align: top;'
                )
            ),
            'fairIsForum' => array(
                'name' => 'fairIsForum',
                'header' => Yii::t('AdminModule.admin', 'Fair is forum'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'fairIsForum', FairAdminService::getFairIsForum(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'forumId' => array(
                'name' => 'forumId',
                'header' => Yii::t('AdminModule.admin', 'Fair forum'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'rating' => array(
                'name' => 'rating',
                'header' => 'Рейтинг',
                'filter' => TbHtml::activeDropDownList(
                    $model,
                    'rating',
                    Fair::$ratings,
                    array(
                        'empty' => ''
                    )
                ),
                'value' => function($data){
                    $res = 'Отсутствует рейтинг';
                    if(!empty(FairAdminService::getRatingName($data['rating'])) && !is_array(FairAdminService::getRatingName($data['rating']))){
                        $res = '<div title="'.Fair::getTitleRatingIconByRatingId($data['rating']).'"
                                     class="b-switch-list-view__cell rating-logo rating-logo-type-'.$data['rating'].'-'.Fair::model()->getCountryStyle().'">
                                    <div style="width: 30px; height: 30px;"></div>
                                </div>';
                    }

                    return $res;
                },
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'associationName' => array(
                'name' => 'associationName',
                'header' => Yii::t('AdminModule.admin', 'Fair association'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'associationName', FairAdminService::getAssociations(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'auditName' => array(
                'name' => 'auditName',
                'header' => Yii::t('AdminModule.admin', 'Audit'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'auditName', FairAdminService::getAudits(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'organizerId' => array(
                'name' => 'organizerId',
                'header' => 'ID организатора',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'organizerCompanyId' => array(
                'name' => 'organizerCompanyId',
                'header' => 'ID компании организатора',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'organizerCompanyCount' => array(
                'name' => 'organizerCompanyCount',
                'header' => Yii::t('AdminModule.admin', 'Count of organizers'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'organizerContactCount' => array(
                'name' => 'organizerContactCount',
                'header' => Yii::t('AdminModule.admin', 'Count of organizer contacts'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'organizerName' => array(
                'name' => 'organizerName',
                'header' => Yii::t('AdminModule.admin', 'Organizer'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'organizerEmail' => array(
                'name' => 'organizerEmail',
                'header' => 'email организатора',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exhibitionComplexId' => array(
                'name' => 'exhibitionComplexId',
                'header' => 'ID площадки',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exhibitionComplexName' => array(
                'name' => 'exhibitionComplexName',
                'header' => Yii::t('AdminModule.admin', 'Exhibition Complex'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'site' => array(
                'type' => 'raw',
                'name' => 'site',
                'value' => function ($data) {
                    return $data['site'] ?
                        '<a style="word-break:break-all;" target="_blank" href="'.$data['site'].'">'.$data['site'].'</a>'
                        : null;
                },
                'htmlOptions' => array(
                    'style' => "width: 100px; vertical-align: top;",
                ),
                'header' => 'Сайт выставки'
            ),
            'beginDate' => array(
                'name' => 'beginDate',
                'header' => Yii::t('AdminModule.admin', 'Start date'),
                'value' => function($data){
                    return $data['beginDate'];
                },
                'filter' => TbHtml::textField('beginDateRange', $model->beginDateRange, array(
                    'title' => 'Формат: "ГГГГ-ММ-ДД - ГГГГ-ММ-ДД"',
                )),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'endDate' => array(
                'name' => 'endDate',
                'header' => "<div title='Формат: \"ГГГГ-ММ-ДД - ГГГГ-ММ-ДД\"'>".Yii::t('AdminModule.admin', 'End date')."</div>",
                'value' => function($data){
                    return $data['endDate'];
                },
                'filter' => TbHtml::textField('endDateRange', $model->endDateRange, array(
                    'title' => 'Формат: "ГГГГ-ММ-ДД - ГГГГ-ММ-ДД"',
                )),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'duration' => array(
                'name' => 'duration',
                'header' => Yii::t('AdminModule.admin', 'Duration'),
                'value' => function($data) {
                    if(!empty($data['duration']) && $data['duration'] >= 0)
                        return $data['duration'] . ' д.';
                    return '';
                },
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'duration', FairAdminService::getDurations(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'cityName' => array(
                'name' => 'cityName',
                'header' => Yii::t('AdminModule.admin', 'City'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'cityName', FairAdminService::getCities(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'regionName' => array(
                'name' => 'regionName',
                'header' => Yii::t('AdminModule.admin', 'Region'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'regionName', FairAdminService::getRegions(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'districtName' => array(
                'name' => 'districtName',
                'header' => Yii::t('AdminModule.admin', 'District'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'districtName', FairAdminService::getDistricts(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'countryName' => array(
                'name' => 'countryName',
                'header' => Yii::t('AdminModule.admin', 'Country'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'countryName', FairAdminService::getCountries(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'industryName' => array(
                'name' => 'industryName',
                'header' => Yii::t('AdminModule.admin', 'Industries'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'industryName', FairAdminService::getIndustries(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'visitorsLocal' => array(
                'type' => 'raw',
                'name' => 'visitorsLocal',
                'header' => Yii::t('AdminModule.admin', 'visitorsLocal'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'visitorsForeign' => array(
                'type' => 'raw',
                'name' => 'visitorsForeign',
                'header' => Yii::t('AdminModule.admin', 'visitorsForeign'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'visitors' => array(
                'name' => 'visitors',
                'header' => Yii::t('AdminModule.admin', 'Visitors'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exhibitorsLocal' => array(
                'type' => 'raw',
                'name' => 'exhibitorsLocal',
                'header' => Yii::t('AdminModule.admin', 'exhibitorsLocal'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'exhibitorsForeign' => array(
                'type' => 'raw',
                'name' => 'exhibitorsForeign',
                'header' => Yii::t('AdminModule.admin', 'exhibitorsForeign'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'members' => array(
                'name' => 'members',
                'header' => Yii::t('AdminModule.admin', 'Participants'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'areaClosedExhibitorsLocal' => array(
                'type' => 'raw',
                'name' => 'areaClosedExhibitorsLocal',
                'header' => Yii::t('AdminModule.admin', 'areaClosedExhibitorsLocal'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'areaClosedExhibitorsForeign' => array(
                'type' => 'raw',
                'name' => 'areaClosedExhibitorsForeign',
                'header' => Yii::t('AdminModule.admin', 'areaClosedExhibitorsForeign'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'areaOpenExhibitorsLocal' => array(
                'type' => 'raw',
                'name' => 'areaOpenExhibitorsLocal',
                'header' => Yii::t('AdminModule.admin', 'areaOpenExhibitorsLocal'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'areaOpenExhibitorsForeign' => array(
                'type' => 'raw',
                'name' => 'areaOpenExhibitorsForeign',
                'header' => Yii::t('AdminModule.admin', 'areaOpenExhibitorsForeign'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'areaSpecialExpositions' => array(
                'type' => 'raw',
                'name' => 'areaSpecialExpositions',
                'header' => Yii::t('AdminModule.admin', 'areaSpecialExpositions'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'squareNet' => array(
                'name' => 'squareNet',
                'header' => Yii::t('AdminModule.admin', 'Square net'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'squareGross' => array(
                'name' => 'squareGross',
                'header' => Yii::t('AdminModule.admin', 'Square gross'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'countriesCount' => array(
                'type' => 'raw',
                'name' => 'countriesCount',
                'header' => Yii::t('AdminModule.admin', 'countriesCount'),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'statisticsDate' => array(
                'name' => 'statisticsDate',
                'header' => 'Год статистики',
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'statisticsDate', FairAdminService::getStatisticDates(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'statistics' => array(
                'name' => 'statistics',
                'header' => 'Статистика',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'registrationFee' => array(
                'name' => 'registrationFee',
                'header' => Yii::t('AdminModule.admin', 'Registration fee'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'participationPrice' =>array(
                'name' => 'participationPrice',
                'header' => Yii::t('AdminModule.admin', 'Cost of participation'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'officialComment' =>array(
                'name' => 'officialComment',
                'header' => Yii::t('AdminModule.admin','official comment'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'lang' => array(
                'name' => 'lang',
                'header' => Yii::t('AdminModule.admin','Fair lang'),
                'type' => 'raw',
                'filter' => TbHtml::activeDropDownList($model, 'lang', FairAdminService::getLanguages(), ['empty' => '']),
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'datetimeModified' => array(
                'name' => 'datetimeModified',
                'header' => Yii::t('AdminModule.admin', 'datetimeModified'),
                'filter' => TbHtml::textField(
                    'datetimeModifiedRange',
                    is_array($model->datetimeModifiedRange) ?  '' : $model->datetimeModifiedRange,
                    array(
                        'title' => 'Формат: "ГГГГ-ММ-ДД - ГГГГ-ММ-ДД"',
                    )
                ),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'commonStatisticFair' => array(
                'name' => 'commonStatisticFair',
                'type' => 'raw',
                'header' => Yii::t('AdminModule.admin', 'common statistic fair'),
                'value' => function($data) use($langId){

                    $fairs = explode(';',$data['commonStatisticFair']);
                    $result = array();
                    $fairAssociation = array();
                    $fairRating = array();
                    $rating = '';

                    if(!empty($data['commonStatisticFairAssociation'])){

                        $commonStatisticFairAssociations = explode(';',$data['commonStatisticFairAssociation']);

                        if(!empty($commonStatisticFairAssociations) && is_array($commonStatisticFairAssociations)){

                            foreach ($commonStatisticFairAssociations as $commonStatisticFairAssociation){

                                $fairAssociationFields = explode('::', $commonStatisticFairAssociation);

                                if(count($fairAssociationFields) == 3){
                                    list($fairId, $associationName, $rating) = $fairAssociationFields;

                                    if(!isset($fairAssociation[$fairId])){
                                        $fairAssociation[$fairId] = $associationName;
                                    } else {
                                        $fairAssociation[$fairId] = $fairAssociation[$fairId] . ',' . $associationName;
                                    }

                                    $fairRating[$fairId] = $rating;
                                }
                            }
                        }
                    }

                    foreach ($fairs as $fair){

                        $fairFields = explode('::', $fair);

                        if(count($fairFields) == 3){
                            list($id, $name, $year) = $fairFields;

                            if(isset($fairAssociation[$id]) && isset($fairRating[$id])){

                                if(isset(Fair::$ratings[$fairRating[$id]])){
                                    $rating = 'рейтинг : ' . Fair::$ratings[$fairRating[$id]];
                                }
                                $result[] = CHtml::link($id . ' ' . $name . ' ' . $year . " ({$fairAssociation[$id]}, {$rating})", Yii::app()->createUrl('admin/fair/view', array('id' => $id, 'langId' => $langId)));
                            }else {
                                $result[] = CHtml::link($id . ' ' . $name . ' ' . $year, Yii::app()->createUrl('admin/fair/view', array('id' => $id, 'langId' => $langId)));
                            }
                        } elseif(isset($fairFields[0]) && isset($fairFields[1])) {
                            list($id, $name) = $fairFields;

                            if(isset($fairAssociation[$id]) && isset($fairRating[$id])){

                                if(isset(Fair::$ratings[$fairRating[$id]])){
                                    $rating = ', рейтинг : ' . Fair::$ratings[$fairRating[$id]];
                                }
                                $result[] = CHtml::link($id . ' ' . $name . " ({$fairAssociation[$id]}, {$rating})", Yii::app()->createUrl('admin/fair/view', array('id' => $id, 'langId' => $langId)));
                            }else {
                                $result[] = CHtml::link($id . ' ' . $name, Yii::app()->createUrl('admin/fair/view', array('id' => $id, 'langId' => $langId)));
                            }
                        } elseif(isset($fairFields[0]) && isset($fairFields[2])){
                            list($id, $year) = $fairFields;

                            if(isset($fairAssociation[$id]) && isset($fairRating[$id])){

                                if(isset(Fair::$ratings[$fairRating[$id]])){
                                    $rating = ', рейтинг : ' . Fair::$ratings[$fairRating[$id]];
                                }
                                $result[] = CHtml::link($id . ' ' . $year . " ({$fairAssociation[$id]}, {$rating})", Yii::app()->createUrl('admin/fair/view', array('id' => $id, 'langId' => $langId)));
                            }else {
                                $result[] = CHtml::link($id . ' ' . $year, Yii::app()->createUrl('admin/fair/view', array('id' => $id, 'langId' => $langId)));
                            }
                        }
                    }
                    return implode('</br></br>', $result);
                },
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'fairDiscription' => array(
                'name' => 'fairDiscription',
                'header' => Yii::t('AdminModule.admin', 'Description'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'fairUniqueText' => array(
                'name' => 'fairUniqueText',
                'header' => Yii::t('AdminModule.admin', 'Unique text'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
            'userRedactorEmail' => array(
                'name' => 'userRedactorEmail',
                'header' => Yii::t('AdminModule.admin', 'user redactor email'),
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ),
        );
        ?>

        <div style="width: 250px; display: inline-block;">
            <?php $this->widget('application.modules.admin.components.ListAutoComplete', [
                'id'    => 'Fair_fields',
                'name'  => 'Fair_fields',
                'label' => 'Поля формы',
                'itemOptions' => [
                    'data' => array_map(
                        function($column) use($model){
                            if (is_array($column)) {
                                $label = isset($column['header']) ? $column['header'] : $model->getAttributeLabel($column['name']);
                            } else {
                                $label =  $model->getAttributeLabel($column);
                            }

                            return $label;
                        },
                        $columns
                    )
                ],
                'selectedItems' => $fairFields,
                'htmlOptions' => [
                    'style' => 'margin: 0;'
                ],
                'autoComplete' => false
            ]); ?>
        </div>

        <div style="display: inline-block;">
            <?= TbHtml::button('Применить', [
                'id'    => 'fair-grid-button',
                'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
                'style' => 'display: block; margin: 0;'
            ]); ?>
        </div>

    </div>
</div>

<div style="clear: both;"></div>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'      => 'fair-grid',
    'columns' => array_merge(
        array(
            'id' => [
                'name' => 'id',
                'header' => '#',
                'type' => 'raw',
                'htmlOptions' => array(
                    'style' => "vertical-align: top;"
                ),
            ],
            array(
                'class' => 'CButtonColumn',
                'template'=>'{update}{view}{showOnSite}{copy}{showHistory}',

                'buttons' => array(
                    'update' => array(
                        'label' => 'Редактировать',
                        'url' => function($data) use ($langId){
                            return Yii::app()->createUrl("admin/fair/update", array("id" => $data['id'], 'langId' => $langId));
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    ),
                    'view' => array(
                        'label' => 'Просмотреть',
                        'url' => function($data) use ($langId){
                            return Yii::app()->createUrl("admin/fair/view", array("id" => $data['id'], 'langId' => $langId));
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    ),
                    'copy' => array(
                        'label'=>'Копировать выставку',
                        'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/copy.png'),
                        'url' => function($data) use ($langId){
                            return Yii::app()->createUrl("admin/fair/copy", array("id"=>$data['id'], 'langId' => $langId));
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    ),
                    'showOnSite' => array(
                        'label' => Yii::t('AdminModule.admin', 'show on site'),
                        'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/chfavicon.png'),
                        'url' => function($data) use ($langId){
                            return Yii::app()->createUrl("fair/html/view", ["urlFairName" => $data['fairShortUrl'], 'langId' => $langId]);
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    ),
                    'showHistory' => array(
                        'label' => 'Просмотр истории',
                        'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/history.png'),
                        'url' => function($data) use ($langId){
                            return Yii::app()->createUrl("admin/fair/ScdTrFair", array("fairId"=>$data['id'], 'langId' => $langId));
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    )
                ),
                'headerHtmlOptions' => array(
                    'style' => "min-width: 16px;"
                ),
                'htmlOptions' => array(
                    'target' => '_blank',
                    'style' => "min-width: 16px; vertical-align: top;"
                ),
            ),
        ),
        array_intersect_key($columns, array_flip($fairFields))
    ),
    'htmlOptions' => array(
        'style' => "overflow-y: auto;"
    ),
    'dataProvider' => $search,
    'filter'       => $model,
    'ajaxUpdate'   => false,
    'template' => '{summary}{pager}{items}{pager}{summary}'
));
?>
