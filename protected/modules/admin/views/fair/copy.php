<?php
/* @var $this FairController */
/* @var $model Fair */

$this->menu=array(
    array('label'=>'List Fair', 'url'=>array('index')),
    array('label'=>'Create Fair', 'url'=>array('create')),
    array('label'=>'View Fair', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage Fair', 'url'=>array('admin')),
);
?>

    <h1>Копирование выставки <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'fairInfo'=>$fairInfo)); ?>