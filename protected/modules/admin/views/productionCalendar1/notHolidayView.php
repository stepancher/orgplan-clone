<?php
/**
 * @var $this ProductionCalendarController
 * @var $model ProductionCalendarDay
 */

$this->menu = array(
    array('label' => 'List ProductionCalendar', 'url' => array('index')),
    array('label' => 'Create ProductionCalendar', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#production-calendar-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
    <br/>
<?= CHtml::link('Назад', Yii::app()->createUrl('admin/productionCalendar/admin'), ['class' => 'btn btn-danger']) ?>

    <h1>Просмотр дат без праздников в производственном календаре #<?php echo $model->id; ?></h1>

    <div class="search-form" style="display:none; margin-left: 30px">
        <?php $this->renderPartial('_searchNotHoliday', array(
            'model' => $model,
        )); ?>
    </div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'production-calendar-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'productionCalendarId' =>
            [
                'name' => 'productionCalendar',
                'value' => function ($data) {
                    return $data->getHolidayDay();
                },
            ],
        'type',
        'day',
        'region',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>