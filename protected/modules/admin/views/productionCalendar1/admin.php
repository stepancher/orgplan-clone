<?php
/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendar */

$this->menu = array(
    array('label' => 'List ProductionCalendar', 'url' => array('index')),
    array('label' => 'Create ProductionCalendar', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#production-calendar-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Производственный календарь</h1>

<?= CHtml::link('Создать', Yii::app()->createUrl('admin/productionCalendar/create'), ['class' => 'btn btn-success']) ?>
<?php //CHtml::link('Просмотр дат без праздников', Yii::app()->createUrl('admin/productionCalendar/notHolidayView'), ['class' => 'btn btn-success']) ?>

<div class="search-form" style="display:none; margin-left: 30px">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'production-calendar-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'holiday',
        'year',
        [
            'name' => 'rangeDate',
            'value' => function ($data) {
                return $data->getProductionCalendarRangeDays();
            },
        ],
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
