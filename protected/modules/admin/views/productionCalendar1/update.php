<?php
/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendar */


$this->menu=array(
	array('label'=>'List ProductionCalendar', 'url'=>array('index')),
	array('label'=>'Create ProductionCalendar', 'url'=>array('create')),
	array('label'=>'View ProductionCalendar', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductionCalendar', 'url'=>array('admin')),
);
?>

<h1>Редактирование  производственный календаря #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>