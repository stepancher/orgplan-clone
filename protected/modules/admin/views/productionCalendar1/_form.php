<?php


/* @var $this ProductionCalendarController */
/* @var $model ProductionCalendar */
/* @var $form CActiveForm */
?>

<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/ProductionCalendarTemp/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

	<div class="form-group field-productioncalendar-year">
		<label class="control-label" for="ProductionCalendar_year"><?= Yii::t('AdminModule.admin', 'Year') ?></label>
		<input type="text" id="ProductionCalendar_year" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ProductionCalendar[year]" value="<?= $model->year ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-productioncalendar-holidayid">
		<label class="control-label" for="ProductionCalendar_holidayId"><?= Yii::t('AdminModule.admin', 'ID output') ?></label>
		<input type="text" id="ProductionCalendar_holidayId" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="ProductionCalendar[holidayId]" value="<?= $model->holidayId ?>">
		<div class="help-block"></div>
	</div>

	<div class="form-group field-productioncalendar-holiday">
		<label class="control-label" for="productioncalendar-holiday"><?= Yii::t('AdminModule.admin', 'Year') ?></label>
		<textarea id="productioncalendar-holiday" class="b-form__text input-block-level" name="ProductionCalendar[holiday]" row="10"><?php echo $model->holiday ?></textarea>
		<div class="help-block"></div>
	</div>

	<div class="col-md-12">
		<button type="submit" class="b-button d-bg-primary-dark d-bg-danger--hover d-text-light btn">
			<?= Yii::t('AdminModule.admin', 'Save') ?>
		</button>
		<a href="<?= Yii::app()->request->urlReferrer ?: $this->createUrl('index') ?>"
		   class="js-btn-back-history b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn">
			<?= Yii::t('AdminModule.admin', 'Cancel') ?>
		</a>
	</div>
</form>