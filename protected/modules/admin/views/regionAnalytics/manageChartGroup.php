<?php
/**
 * @var CController $this
 * @var AR $model
 */
$ctrl = $this;
$ctrl->widget('application.modules.admin.components.FieldSetView', array(
    'header' => Yii::t('AdminModule.admin', 'Grid header'),
    'items' => array(
        array(
            'application.modules.admin.components.ActionsView',
            'items' => array(
                'add' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('AdminModule.admin', 'Add chart Group'),
                    array(
                        'url' => $this->createUrl('admin/regionAnalytics/saveChartGroup'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS
                    )
                ),
                'manageRegionAnalytics' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('AdminModule.admin', 'Список аналитики по регионам'),
                    array(
                        'url' => $this->createUrl('admin/regionAnalytics/manageIndustryAnalyticsInformation'),
                        'color' => TbHtml::BUTTON_COLOR_WARNING
                    )
                ),
                'manageChartGroups' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('AdminModule.admin', 'Список группы чартов'),
                    array(
                        'url' => $this->createUrl('manageChartGroup'),
                        'color' => TbHtml::BUTTON_COLOR_WARNING
                    )
                ),
                'manageCharts' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('AdminModule.admin', 'Список чартов'),
                    array(
                        'url' => $this->createUrl('admin/regionAnalytics/manageChart'),
                        'color' => TbHtml::BUTTON_COLOR_WARNING
                    )
                ),
            )
        ),
        array(
            'application.widgets.' . get_class($model) . 'GridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{edit}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'edit' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array()),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('admin/regionAnalytics/saveChartGroup', array("id" => $data->id));
                            },
                            'options' => array('title' => 'Редактирование', 'class' => 'btn btn-small btn-primary'),
                        ),
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array()),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('admin/regionAnalytics/deleteChartGroup', array("id" => $data->id));
                            },
                            'options' => array('title' => 'Удаление', 'class' => 'btn btn-small btn-danger'),
                        ),
                    )
                )
            )
        ),
    )
));
