<?php echo CHtml::tag('h1', [], 'Создать группу чартов');?>

<?php
    $params = array();
    if(isset($_GET['id']) && !empty($_GET['id'])){
        $params = array('id' => $_GET['id']);
    }
?>

<?php echo TbHtml::beginFormTb('vertical',$this->createUrl('admin/regionAnalytics/saveChartGroup', $params), 'post');?>

<?php echo TbHtml::activeLabel($model, 'header');?>
<?php echo TbHtml::activeTextField($model, 'header');?>

<?php echo TbHtml::activeLabel($model, 'type');?>
<?php
    echo TbHtml::activeDropDownList($model,'type',
        ChartGroup::getChartsGroups(),
        array(
            'empty' => ' - ',
        )
    );
?>

<?php echo TbHtml::activeLabel($model, 'industryId');?>
<?php $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
    'model'=>$model,
    'attribute'=> 'industryId',
    'data' =>CHtml::listData(IndustryAnalyticsInformation::model()->findAll(), 'id', 'header'),
    'htmlOptions'=>array(
        'options' => array($model->industryId => array('selected' => TRUE)),
        'empty' => '-',
        'style' => 'width:60%;',
    ),
));?>

    <div class="clear"></div>

    <?php echo TbHtml::submitButton('Сохранить', array('style' => 'margin-top:20px;'));?>
<?php echo TbHtml::endForm();?>


