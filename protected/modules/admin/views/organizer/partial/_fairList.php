<?php
/**
 * @var OrganizerController $this
 * @var CArrayDataProvider  $fairsDataProvider
 */
?>

<div class="col-md-12" style="margin-top: 10px; padding: 0;">

    </br>

    <div style="margin-left: 30px; font-size: large">
        <?= Yii::t('AdminModule.admin', 'Fairs') ?>
    </div>

    <div class="col-md-2">

        <?php $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'fairs-grid',
            'dataProvider' => $fairsDataProvider,
            'columns' => array(
                'id',
                [
                    'class' => 'CButtonColumn',
                    'template'=>'{view}{showOnSite}',
                    'buttons' => array(
                        'view' => array(
                            'label' => 'Просмотреть',
                            'url' => function($data){
                                return Yii::app()->createUrl("admin/fair/view", ['id' => $data->id]);
                            },
                            'options' => array(
                                'target' => '_blank',
                            ),
                        ),
                        'showOnSite' => array(
                            'label' => Yii::t('AdminModule.admin', 'show on site'),
                            'imageUrl' => Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/chfavicon.png'),
                            'url' => function($data){
                                return Yii::app()->createUrl("fair/html/view", ["urlFairName" => $data->shortUrl]);
                            },
                            'options' => array(
                                'target' => '_blank',
                            ),
                        ),
                    ),
                    'headerHtmlOptions' => array(
                        'style' => "min-width: 16px;"
                    ),
                    'htmlOptions' => array(
                        'style' => "min-width: 16px;"
                    ),
                ],
                'name',
            ),
            'htmlOptions' => array(
                'style' => "overflow-y: auto;"
            ),
        )); ?>

    </div>
</div>