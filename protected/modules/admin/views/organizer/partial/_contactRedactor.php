<?php
/**
 * @var OrganizerController $this
 * @var array               $data
 * @var array               $rows
 * @var CSqlDataProvider    $dataProvider
 */
?>

<div class="col-md-9" >
    <?php $this->widget('application.modules.admin.components.OrganizerContactView', array(
        'data' => $data,
        'attributes' => $rows,
        'itemCssClass' => array('organizer-row'),
    )); ?>

    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'id'      => 'contact-grid',
        'columns' => array(
            [
                'name' => 'type',
                'type' => 'raw',
                'value' => function($data){
                    $createImg = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/add.png');
                    $updateImg = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/Pencil-icon.png');
                    $deleteImg = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/delete.png');
                    $createLink = "<a style=\"float: left;\" title=\"" . Yii::t('AdminModule.admin','Create contact') . "\" class='add-contact'><img src=\"{$createImg}\" alt=\"" . Yii::t('AdminModule.admin','Create contact') . "\"></a>";
                    $updateLink = "<a style=\"float: right; opacity: 0\" title=\"" . Yii::t('AdminModule.admin','Update contact') . "\" class='update-contact'><img src=\"{$updateImg}\" alt=\"" . Yii::t('AdminModule.admin','Update contact') . "\"></a>";
                    $deleteLink = "<a style=\"float: right; opacity: 0\" data-contact-id=\"{$data['id']}\" title=\"" . Yii::t('AdminModule.admin','Delete contact') . "\" class='delete-contact'><img src=\"{$deleteImg}\" alt=\"" . Yii::t('AdminModule.admin','Delete contact') . "\"></a>";

                    if(isset($data['type'])){

                        if($this->firstRow != $data['type']){
                            $rowVal = $createLink . Organizer::model()->getAttributeLabel(OrganizerContactList::getType($data['type'])) . $deleteLink . $updateLink;
                            $this->firstRow = $data['type'];
                        } else {
                            $rowVal = $deleteLink . $updateLink;
                        }
                        return $rowVal;
                    }
                    return $createLink . $deleteLink . $updateLink;
                }
            ],
            [
                'name' => 'countryId',
                'type' => 'raw',
                'value' => function($data){
                    return "<div class='country-drop-down' style='display: none'>" . CHtml::dropDownList('countryId', $data['countryId'], ['0' => ''] + Country::getCountries()) . "</div>";
                }
            ],
            [
                'name' => 'countryTelephoneCode',
                'type' => 'raw',
                'value' => function($data){

                    if(isset($data['countryId'])){
                        $country = Country::model()->findByPk($data['countryId']);

                        if(!empty($country))
                            return "<div class='country-telephone-code'>" . $country->telephoneCode . "</div>";
                    }
                    return "<div class='country-telephone-code'></div>";
                }
            ],
            [
                'name' => 'value',
                'type' => 'raw',
                'value' => function($data){
                    if(isset($data['value']))
                        return "<input style='border: none' disabled class='value-input' value='{$data['value']}'>";
                    return '';
                }
            ],
            [
                'name' => 'additionalContactInformation',
                'type' => 'raw',
                'value' => function($data){
                    if(isset($data['additionalContactInformation']))
                        return "<input style='border: none' disabled class='additional-info-input' value='{$data['additionalContactInformation']}'>";
                    return "<input style='border: none' disabled class='additional-info-input' value=''>";
                }
            ],
            [
                'name' => 'saveButton',
                'type' => 'raw',
                'value' => function($data){
                    return "<button style='display: none' class='save-button' data-type=\"{$data['type']}\" data-contact-id=\"{$data['id']}\" data-organizer-Id=\"{$data['organizerId']}\">".Yii::t('AdminModule.admin','Save')."</button>";
                }
            ],
            [
                'name' => 'cancelButton',
                'type' => 'raw',
                'value' => function($data){
                    return "<button style='display: none' class='cancel-button' data-type=\"{$data['type']}\" data-contact-id=\"{$data['id']}\" data-organizer-Id=\"{$data['organizerId']}\">".Yii::t('AdminModule.admin','Cancel')."</button>";
                }
            ],
        ),
        'dataProvider' => $dataProvider,
        'hideHeader' => TRUE,
        'template' => '{items}',
        'rowCssClass' => array('contact-row'),
    ));
    ?>
</div>

<script>
    $(function(e){

        var updateDump;
        var isActive = false;
        var text = '';
        var KEYCODE_BACKSPACE = 8;
        var KEYCODE_DELETE = 46;
        var KEYCODE_ENTER = 13;
        var EMAIL_TYPE = 2;

        $(document).click(function (e) {

            var elem = e.target;
            var button = elem.parentNode;
            var cell = button.parentNode;
            var row = cell.parentNode;
            var table = row.parentNode;
            var contactId = elem.dataset.contactId;
            var contactOrganizerId;
            var contactType;
            var contactValue;

            if(button.className == 'update-contact' && !isActive){

                updateDump = row.cloneNode(true);

                if($(row).find('.save-button')[0].dataset.type != EMAIL_TYPE){
                    $(row).find('.country-drop-down').css('display', 'block');

                    if($(row).find('.additional-info-input').val() == '')
                        $(row).find('.additional-info-input').attr('placeholder', 'доп.');
                }

                $(row).find('.save-button').css('display', 'block');
                $(row).find('.cancel-button').css('display', 'block');
                $(row).find('.value-input')[0].removeAttribute("style");
                $(row).find('.value-input')[0].removeAttribute("disabled");
                $(row).find('.additional-info-input')[0].removeAttribute("style");
                $(row).find('.additional-info-input')[0].removeAttribute("disabled");
                isActive = true;

            } else if(button.className == 'update-organizer' && !isActive){

                updateDump = row.cloneNode(true);
                $(row).find(".organizer-input").attr("style", 'border-width: 2px;');
                $(row).find('.organizer-save-button').css('display', 'block');
                $(row).find('.organizer-cancel-button').css('display', 'block');
                isActive = true;

            } else if(elem.className == 'organizer-save-button'){

                contactOrganizerId = elem.dataset.organizerId;
                contactType = button.dataset.attributeName;
                contactValue = $(button).find(".organizer-input").val();

                $.ajax({
                    url: '<?php echo $this->createUrl('organizerAttributesSave') ?>',
                    method: 'POST',
                    async: false,
                    dataType: 'json',
                    data: {'organizerId': contactOrganizerId, 'type': contactType, 'value': contactValue},
                    success: function(data){
                        if(data.success){
                            $(cell).find(".organizer-save-button").css('display', 'none');
                            $(cell).find(".organizer-cancel-button").css('display', 'none');
                            $(cell).find(".organizer-input").attr('style', 'border: none');
                            isActive = false;
                        } else alert(data.message);
                    }
                });

            } else if(elem.className == 'organizer-cancel-button'){

                row.replaceChild(updateDump, cell);
                isActive = false;

            } else if(elem.className == 'save-button'){

                var selectedCountry = $(button.parentNode).find(".country-drop-down option:selected").text();
                var contactAdditionalValue = $(button.parentNode).find(".additional-info-input").val();
                contactValue = $(button.parentNode).find(".value-input").val();
                contactOrganizerId = elem.dataset.organizerId;
                contactType = elem.dataset.type;

                $.ajax({
                    url: '<?php echo $this->createUrl('saveContact') ?>',
                    method: 'POST',
                    async: false,
                    dataType: 'json',
                    data: {'contactId': contactId,
                        'organizerId': contactOrganizerId,
                        'type': contactType,
                        'value': contactValue,
                        'countryName': selectedCountry,
                        'contactAdditionalValue' : contactAdditionalValue
                    },
                    success: function(data){

                        if(data.success){

                            if(elem.dataset.contactId == 0){
                                var deleteButton = $(row).find(".delete-contact")[0];
                                deleteButton.dataset.contactId = data.contactId;
                                cell.childNodes[1].innerHTML = deleteButton.outerHTML + cell.childNodes[1].innerHTML;
                            }

                            $(cell).find(".country-drop-down").css('display', 'none');
                            $(cell).find(".save-button").css('display', 'none');
                            $(cell).find(".cancel-button").css('display', 'none');
                            $(cell).find(".value-input").attr('style', 'border: none');
                            $(cell).find(".value-input").attr('disabled', 'true');
                            $(cell).find(".additional-info-input").attr('style', 'border: none');
                            $(cell).find(".additional-info-input").attr('disabled', 'true');
                            $(cell).find(".delete-contact").css('opacity', '1');

                            $(cell).find(".country-telephone-code").html(data.countryTelephoneCode);
                            elem.dataset.contactId = data.contactId;
                            isActive = false;
                        } else alert(data.message);
                    }
                });

            } else if(elem.className == 'cancel-button'){

                if(elem.dataset.contactId == 0)
                    row.removeChild(cell);
                 else
                    row.replaceChild(updateDump, cell);

                isActive = false;
            } else if(button.className == 'add-contact' && !isActive){

                var trClone = row.cloneNode(true);
                var nextTr = row.nextElementSibling;
                var updateButton = $(row).find(".update-contact")[0].outerHTML;

                if($(row).find('.save-button')[0].dataset.type != EMAIL_TYPE)
                    $(trClone).find(".country-drop-down").css('display', 'block');

                $(trClone).find(".save-button").css('display', 'block');
                $(trClone).find(".cancel-button").css('display', 'block');

                $(trClone).find('.value-input')[0].removeAttribute("style");
                $(trClone).find('.value-input')[0].removeAttribute("disabled");
                $(trClone).find('.additional-info-input')[0].removeAttribute("style");
                $(trClone).find('.additional-info-input')[0].removeAttribute("disabled");

                $(trClone).find(".country-drop-down").val('0').prop('selected', true);
                $(trClone).find("#countryId")[0].selectedIndex = -1;
                $(trClone).find(".value-input").val('');
                $(trClone).find(".additional-info-input").val('');

                if($(row).find('.save-button')[0].dataset.type != EMAIL_TYPE)
                    $(trClone).find('.additional-info-input').attr('placeholder', 'доп.');

                $(trClone).find(".country-telephone-code").html('');
                trClone.childNodes[1].innerHTML = updateButton;

                $(trClone).find(".delete-contact").css('opacity','0');
                $(trClone).find(".save-button")[0].dataset.contactId = 0;
                $(trClone).find(".cancel-button")[0].dataset.contactId = 0;

                table.insertBefore(trClone, nextTr);
                isActive = true;
            } else if(button.className == 'delete-contact'){

                var toDelete = confirm("Вы уверены, что хотите удалить этот контакт?");
                contactId = elem.parentNode.dataset.contactId;

                if(toDelete){
                    $.ajax({
                        url: '<?php echo $this->createUrl('deleteContact') ?>',
                        method: 'POST',
                        async: false,
                        dataType: 'json',
                        data: {'contactId': contactId},
                        success: function(data){

                            if(data.success){

                                var row = elem.parentNode.parentNode.parentNode;
                                var table = row.parentNode;

                                if($(row).find('.add-contact')[0] == undefined){
                                    table.removeChild(row);
                                } else{

                                    if(isActive){
                                        $(row).find('.country-telephone-code').html('');
                                        $(row).find('.value-input').val('');
                                        $(row).find('.additional-info-input').val('');
                                        $(row).find('.additional-info-input')[0].removeAttribute("placeholder");
                                        $(row).find('#countryId')[0].selectedIndex = -1;
                                        $(row).find('.save-button').click();
                                        isActive = false;
                                    }

                                    $(row).find('.country-telephone-code').html('');
                                    $(row).find('.value-input').val('');
                                    $(row).find('.additional-info-input').val('');
                                }

                            } else alert(data.message);
                        }
                    });
                    isActive = false;
                }
            } else if(button.className == 'select-result'){

                $(row).find('input').val(elem.innerHTML);
                $(row).find('input').focus();
                text = elem.innerHTML;

                var results = $(document).find(".select-results");
                if(results !== undefined)
                    $(document).find(".select-results").remove();
                text = '';
            }
        });

        function getChar(event) {
            if (event.which == null) { // IE
                if (event.keyCode < 32) return null; // спец. символ
                return String.fromCharCode(event.keyCode)
            }

            if (event.which != 0 && event.charCode != 0) { // все кроме IE
                if (event.which < 32) return null; // спец. символ
                return String.fromCharCode(event.which); // остальные
            }

            return null; // спец. символ
        }

        $(document).keypress(function (e) {

            var elem = e.target;
            var elemParent = elem.parentNode;

            if(elemParent.className !== 'organizer-val'){

                var row = elem.parentNode.parentNode;
                var data = $(row).find(".save-button")[0].dataset;

                text += getChar(e);

                $(document).find(".select-results").remove();

                $.ajax({
                    url: '<?php echo $this->createUrl('getContactValue') ?>',
                    method: 'POST',
                    async: false,
                    dataType: 'json',
                    data: {'organizerId': data.organizerId, 'value': text, 'type': data.type},
                    success: function(data){

                        if(data.success == false){
                            alert(data.message);
                        } else {

                            var ul = document.createElement("ul");
                            ul.className = 'select-results';

                            ul.style.position = 'absolute';
                            ul.style.zIndex = 2;
                            ul.style.background = '#FFFFFF';
                            ul.style.width = 'inherit';
                            var li;
                            var div;

                            $.each(data, function (index, value) {

                                li = document.createElement("li");
                                div = document.createElement("div");

                                li.className = 'select-result';
                                li.style.listStyleType = 'none';
                                li.style.border = '1px solid black';
                                li.style.width = '183px';
                                div.className = 'select-result-label';
                                div.innerHTML = value;
                                li.innerHTML = div.outerHTML;

                                if(li !== ''){
                                    ul.appendChild(li);
                                }
                            });

                            if(ul.innerHTML !== '')
                                elemParent.appendChild(ul);
                        }
                    }
                });
            }
        });

        $(document).keyup(function (e) {

            var elem = e.target;
            var elemParent = elem.parentNode;
            var row = elemParent.parentNode;
            var value = $(elemParent).find('input').val();

            if (e.keyCode == KEYCODE_BACKSPACE || e.keyCode == KEYCODE_DELETE){

                if(value !== '')
                    text = value.substring(0, value.length);
                 else
                    text = '';

                var results = $(document).find(".select-results");
                if(results !== undefined)
                    $(document).find(".select-results").remove();
            } else if (e.keyCode == KEYCODE_ENTER){
                if(elem.className == 'value-input' || elem.className == 'additional-info-input')
                    $(row).find('.save-button').click();
            }
        });

        $('.contact-row').mouseenter(function (e) {
            var elem = e.target;
            $(elem).find('.delete-contact').css('opacity', '1');
            $(elem).find('.update-contact').css('opacity', '1');
        });

        $('.contact-row').mouseleave(function (e) {
            var elem = e.target;
            $(elem).find('.delete-contact').css('opacity', '0');
            $(elem).find('.update-contact').css('opacity', '0');
        });

        $('.organizer-row').mouseenter(function (e) {
            var elem = e.target;
            $(elem.parentNode).find(".update-organizer").css('opacity','1');
        });

        $('.organizer-row').mouseleave(function (e) {
            var elem = e.target;
            $(elem.parentNode).find(".update-organizer").css('opacity','0');
        });
    });
</script>