<?php
/**
 * @var $this OrganizerController
 * @var $model Organizer
 * @var $contactSelectedFields array
 * @var $contactFields array
 * @var $fields array
 * @var $dataProvider CSqlDataProvider
 * @var integer $organizerCompanyId
 */
?>

<?php
Yii::app()->clientScript->registerScript('search', "

    $('#organizer-contact-grid-button').on('click', function () {
    
        var \$fields = $('#OrganizerCompany_fields .checkbox.checked input');
        
        var fields = \$fields.map(function() {
            return $(this).val();
        }).get();
        
        $.fn.yiiGridView.update('organizer-contact-grid', {data: {
            OrganizerCompany_fields: fields.toString(),
            Fair_page: 1
        }})
    });
    
");
?>

<div style="float: right;">

    <div style="width: 250px; display: inline-block;">
        <?php $this->widget('application.modules.admin.components.ListAutoComplete', [
            'id'    => 'OrganizerCompany_fields',
            'name'  => 'OrganizerCompany_fields',
            'label' => 'Поля формы',
            'itemOptions' => [
                'data' => $contactFields,
            ],
            'selectedItems' => $contactSelectedFields,
            'htmlOptions' => [
                'style' => 'margin: 0;'
            ],
            'autoComplete' => false
        ]); ?>
    </div>

    <div style="display: inline-block;">
        <?= TbHtml::button('Применить', [
            'id'    => 'organizer-contact-grid-button',
            'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
            'style' => 'display: block; margin: 0;'
        ]); ?>
    </div>

</div>

<div style="clear: both"></div>

</br>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'      => 'organizer-contact-grid',
	'columns' => array_merge([
		[
			'name' => 'id',
			'type' => 'raw',
			'value' => function($data){
				if(isset($data['id']) && !empty($data['id'])){
					return CHtml::link($data['id'], Yii::app()->createUrl("admin/organizer/viewContact", array("id" => $data['id'])));
				}
				return '';
			}
		],
		[
			'class' => 'CButtonColumn',
			'template'=>'{delete}',
			'buttons' => array(
				'delete' => array(
					'label' => 'Удалить',
					'url' => function($data) use($organizerCompanyId){
						return Yii::app()->createUrl("admin/organizer/cancel", array('organizerId' => $data['id']));
					},
				),
			),
			'headerHtmlOptions' => array(
				'style' => "min-width: 16px;"
			),
			'htmlOptions' => array(
				'target' => '_blank',
				'style' => "min-width: 16px;"
			),
		]
	], $fields),
	'dataProvider' => $dataProvider,
	'filter'       => $model,
	'ajaxUpdate'   => false,
    'htmlOptions' => array(
        'style' => "overflow-y: auto;"
    ),
)); ?>
