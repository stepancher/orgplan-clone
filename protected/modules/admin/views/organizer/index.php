<?php
/* @var $this OrganizerController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Create Organizer', 'url'=>array('create')),
	array('label'=>'Manage Organizer', 'url'=>array('admin')),
);
?>

<h1>Organizers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
