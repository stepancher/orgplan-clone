<?php
/* @var $this OrganizerController */
/* @var $model Organizer */

?>

<h1>Редактирование организатора <?= $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>