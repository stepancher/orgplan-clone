<?php
/**
 * @var $this   OrganizerController
 * @var array   $data
 * @var array   $userErrors
 */
?>
<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/organizer/'.$this->action->id, !empty(isset($data['id']) ? $data['id'] : '') ? array('id' => isset($data['id']) ? $data['id'] : '') : array()) ?>">
    <div class="cols-row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-success">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <?php if(isset($data['id']) && !empty($data['id'])) : ?>
            <?=CHtml::link('Просмотр', Yii::app()->createUrl('admin/organizer/viewOrganizer', array('id' => $data['id'])), ['class' => 'btn btn-info'])?>
            <?php endif; ?>
            <?=CHtml::link('Вернуться к списку', Yii::app()->createUrl('admin/organizer/adminOrganizer'), ['class' => 'btn btn-primary'])?>
        </div>
    </div>

    <div class="cols-row">
        <div class="col-md-12">

            </br>

            <div id="organizer-widget" style="margin-bottom: 20px;"> <?= Yii::t('AdminModule.admin','Organizer') ?>
                <?php
                $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                    'name' => 'Organizer'."[".Organizer::ORGANIZER_COMPANY_ID."]",
                    'value' => isset($data['companyId']) && isset($data['organizerName']) ? json_encode(array('id' => $data['companyId'], 'name' => $data['organizerName'])) : NULL,
                    'options' => array(
                        'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                        'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                        'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                        'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                        'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                        'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                        'width' => '100%',
                        'placeholder' => "Выберите организатора",
                        'minimumInputLength' => 1,
                        'ajax' => array(
                            'url' => $this->createUrl('getOrganizers'),
                            'dataType' => 'json',
                            'data' => 'js:function (term, page) { return { q: term}; }',
                            'results' => 'js:function(data,page){return {results: data};}',

                        ),
                        'formatResult'    => 'js:function(data){
                                return data.id + " " + data.name;
                            }',
                        'formatSelection' => 'js: function(data) {
                                return data.id + " " + data.name;
                            }',
                        'initSelection' => 'js:function (element, callback) {
                                // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                var items = JSON.parse(element.val());
                                element.val(items.id);
                                callback(items);
                            }',
                    ),
                    'htmlOptions' => array(
                        'class' => '',
                        'style' => 'border:none;margin-top:10px;',
                    ),
                ));
                ?>
            </div>

            <?php if(isset($userErrors['linkToTheSiteOrganizers'])) : ?>
            <div style="color: red" class="errorMessage">
                <?= $userErrors['linkToTheSiteOrganizers'] ?>
            </div>
            <?php endif; ?>

            <div class="form-group field-contact-phone">
                <label class="control-label" for="contact_phone"><?= Yii::t('AdminModule.admin', 'Directorate phone') ?></label>
                <input type="text" id="contact_phone" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[phone]" value="<?= isset($data['phone']) ? $data['phone'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <?php if(isset($userErrors['phone'])) : ?>
                <div style="color: red" class="errorMessage">
                    <?= $userErrors['phone'] ?>
                </div>
            <?php endif; ?>

            <div class="form-group field-contact-email">
                <label class="control-label" for="contact_email"><?= Yii::t('AdminModule.admin', 'Management of e-mail') ?></label>
                <input type="text" id="contact_email" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[email]" value="<?= isset($data['email']) ? $data['email'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <?php if(isset($userErrors['email'])) : ?>
                <div style="color: red" class="errorMessage">
                    <?= $userErrors['email'] ?>
                </div>
            <?php endif; ?>

            <div class="form-group field-contact-exhibitionManagement">
                <label class="control-label" for="contact_exhibitionManagement"><?= Yii::t('AdminModule.admin', 'Exhibition Management') ?></label>
                <input type="text" id="contact_exhibitionManagement" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[exhibitionManagement]" value="<?= isset($data['exhibitionManagement']) ? $data['exhibitionManagement'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-contact-servicesBuilding">
                <label class="control-label" for="contact_servicesBuilding"><?= Yii::t('AdminModule.admin', 'Services building') ?></label>
                <input type="text" id="contact_servicesBuilding" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[servicesBuilding]" value="<?= isset($data['servicesBuilding']) ? $data['servicesBuilding'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-contact-advertisingServicesTheFair">
                <label class="control-label" for="contact_advertisingServicesTheFair"><?= Yii::t('AdminModule.admin', 'Services in advertising on exhibition') ?></label>
                <input type="text" id="contact_advertisingServicesTheFair" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[advertisingServicesTheFair]" value="<?= isset($data['advertisingServicesTheFair']) ? $data['advertisingServicesTheFair'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-contact-servicesLoadingAndUnloading">
                <label class="control-label" for="contact_servicesLoadingAndUnloading"><?= Yii::t('AdminModule.admin', 'Services for loading and unloading') ?></label>
                <input type="text" id="contact_servicesLoadingAndUnloading" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[servicesLoadingAndUnloading]" value="<?= isset($data['servicesLoadingAndUnloading']) ? $data['servicesLoadingAndUnloading'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-contact-foodServices">
                <label class="control-label" for="contact_foodServices"><?= Yii::t('AdminModule.admin', 'Catering services') ?></label>
                <input type="text" id="contact_foodServices" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Contact[foodServices]" value="<?= isset($data['foodServices']) ? $data['foodServices'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-organizer-proposalEmail">
                <label class="control-label" for="organizer_proposalEmail"><?= Yii::t('AdminModule.admin', 'Proposal Email') ?></label>
                <input type="text" id="organizer_proposalEmail" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="Organizer[proposalEmail]" value="<?= isset($data['proposalEmail']) ? $data['proposalEmail'] : '' ?>">
                <div class="help-block"></div>
            </div>

            <br/>
            <?= Yii::t('AdminModule.admin','Active') ?>
            <?=CHtml::dropDownList('Organizer[active]', isset($data['active']) ? $data['active'] : '', array(0 => 'Не активный', 1 => 'Активный'))?>
        </div>
    </div>
    <div class="cols-row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-success">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <?php if(isset($data['id']) && !empty($data['id'])) : ?>
                <?=CHtml::link('Просмотр', Yii::app()->createUrl('admin/organizer/viewOrganizer', array('id' => $data['id'])), ['class' => 'btn btn-info'])?>
            <?php endif; ?>
            <?=CHtml::link('Вернуться к списку', Yii::app()->createUrl('admin/organizer/adminOrganizer'), ['class' => 'btn btn-primary'])?>
        </div>
    </div>
</form>