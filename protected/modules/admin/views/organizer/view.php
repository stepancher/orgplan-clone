<?php
/**
 * @var $this OrganizerController
 * @var $model OrganizerCompany
 * @var $modelSearch Organizer
 * @var $pageVariants array
 * @var $pageSize integer
 * @var $fields array
 * @var $contactSelectedFields array
 * @var $contactFields array
 * @var $dataProvider CSqlDataProvider
 */
?>
    <legend><?= Yii::t('AdminModule.admin', 'View organizer #') . $model->id; ?></legend>

<div class="col-md-12" style="margin-top: 10px; padding: 0;">
    <?= CHtml::link(
        'Редактировать',
        Yii::app()->createUrl(
            'admin/organizer/update',
            ['id' => $model->id]
        ),
        ['class' => 'btn btn-warning']
    ); ?>

    <?= CHtml::link(
        'Вернуться к списку',
        Yii::app()->createUrl('admin/organizer/admin'),
        ['class' => 'btn btn-primary']
    ); ?>
</div>

    </br>
    </br>
    </br>

<fieldset>

    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'name',
            'active',
            [
                'name' => 'linkToTheSiteOrganizers',
                'type' => 'raw',
                'value' => function($data){

                    if(!empty($data->linkToTheSiteOrganizers)){
                        return CHtml::link($data->linkToTheSiteOrganizers, $data->linkToTheSiteOrganizers);
                    }

                    return '';
                }
            ],
            'email',
            'phone',
            [
                'name' => 'cityName',
                'type' => 'raw',
                'value' => function($data){

                    if(isset($data->city->translate)){
                        return $data->city->name;
                    }
                    return $data->cityId;
                }
            ],
            [
                'name' => 'regionName',
                'type' => 'raw',
                'value' => function($data){

                    if(isset($data->city->region->translate)){
                        return $data->city->region->name;
                    }
                    return '';
                }
            ],
            [
                'name' => 'districtName',
                'type' => 'raw',
                'value' => function($data){

                    if(isset($data->city->region->district->translate)){
                        return $data->city->region->district->name;
                    }
                    return '';
                }
            ],
            [
                'name' => 'countryName',
                'type' => 'raw',
                'value' => function($data){

                    if(isset($data->city->region->district->country->translate)){
                        return $data->city->region->district->country->name;
                    }
                    return '';
                }
            ],
            'street',
            'coordinates',
            [
                'name' => 'associationName',
                'label' => Yii::t('AdminModule.admin', 'Organizer association'),
                'type' => 'raw',
                'value' => function($data){

                    $result = '';

                    if(isset($data->organizerCompanyHasAssociations) &&
                        is_array($data->organizerCompanyHasAssociations) &&
                        !empty($data->organizerCompanyHasAssociations))
                    {
                        foreach ($data->organizerCompanyHasAssociations as $association){
                            $result .= $association->association->name . ', ';
                        }
                    }
                    return rtrim($result, ', ');
                }
            ],
            'exdbId',
        ),
    )); ?>
</fieldset>

    </br>
    </br>

<h2>
    <?= Yii::t('AdminModule.admin', 'organizer contacts') ?>
</h2>

<?= CHtml::link(
    'Создать новый контакт',
    Yii::app()->createUrl('admin/organizer/createContact', array('organizerCompanyId' => $model->id)),
    ['class' => 'btn btn-success']
); ?>

<?php $this->renderPartial('partial/_contacts', array(
    'model'                 => $modelSearch,
    'contactSelectedFields' => $contactSelectedFields,
    'contactFields'         => $contactFields,
    'fields'                => $fields,
    'dataProvider'          => $dataProvider,
    'organizerCompanyId'    => $model->id,
)) ?>