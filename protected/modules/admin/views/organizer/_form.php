<?php
/**
 * @var $this OrganizerController
 * @var $model Organizer
 */
?>
<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('admin/organizer/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

    <div class="cols-row">
        </br>
        <div class="col-md-12">

            <div class="form-group field-organizerCompany-name">
                <label class="control-label" for="organizerCompany_name"><?= Yii::t('AdminModule.admin', 'fair organizer') ?></label>
                <input type="text" id="organizerCompany_name" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[name]" value="<?= $model->name ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-organizerCompany-linktothesiteorganizers required">
                <label class="control-label" for="organizerCompany_LinkToTheSiteOrganizers"><?= Yii::t('AdminModule.admin', 'Link to the website of the organizers') ?></label>
                <input type="text" id="organizerCompany_LinkToTheSiteOrganizers" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[linkToTheSiteOrganizers]" value="<?= $model->linkToTheSiteOrganizers ?>">
                <div class="help-block"></div>
            </div>
            <?= CHtml::error($model, 'linkToTheSiteOrganizers', array('style' => 'color: red')) ?>

            <div class="form-group field-organizerCompany-email">
                <label class="control-label" for="organizerCompany_email"><?= Yii::t('AdminModule.admin', 'email') ?></label>
                <input type="text" id="organizerCompany_email" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[email]" value="<?= $model->email ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-organizerCompany-phone">
                <label class="control-label" for="organizerCompany_phone"><?= Yii::t('AdminModule.admin', 'phone') ?></label>
                <input type="text" id="organizerCompany_phone" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[phone]" value="<?= $model->phone ?>">
                <div class="help-block"></div>
            </div>

            <div id="select2-widget" style="margin-bottom: 20px;"> <?= Yii::t('AdminModule.admin','City') ?>
                <?php $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                    'name' => ucfirst(get_class($model))."[".OrganizerCompany::CITY_VARIABLE."]",
                    'value' => isset($model->city) ? json_encode(array('id' => $model->city->id, 'name' => $model->city->name)) : '',
                    'options' => array(
                        'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                        'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                        'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                        'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                        'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                        'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                        'width' => '100%',
                        'placeholder' => "Выберите город",
                        'multiple' => FALSE,
                        'minimumInputLength' => 1,
                        'ajax' => array(
                            'url' => $this->createUrl('getCitiesByText'),
                            'dataType' => 'json',
                            'data' => 'js:function (term, page) { return { q: term}; }',
                            'results' => 'js:function(data,page){return {results: data};}',
                            'update' => '#regionId',
                        ),
                        'formatResult'    => 'js:function(data){
                                return data.id + " " + data.name;
                            }',
                        'formatSelection' => "js: function(data) {
												
								$.ajax({
									url: '{$this->createUrl('getCityGeoData')}',
									method: 'POST',
									async: false,
									dataType: 'json',
									data: {'cityId': data.id},
									success: function(data){
										if(data.success){
										
										$(document).find('#regionId').val(data.regionName);
										$(document).find('#districtId').val(data.districtName);
										$(document).find('#countryId').val(data.countryName);
										
										} else {
											alert(data.message);
										}
									}
								});
								
                                return data.id + ' ' + data.name;
                            }",
                        'initSelection' => 'js:function (element, callback) {
                                // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                var items = JSON.parse(element.val());
                                element.val(items.id);
                                callback(items);
                            }',
                    ),
                    'htmlOptions' => array(
                        'style' => 'border:none;margin-top:10px;',
                    ),
                ));
                ?>
            </div>

            <div style="margin-top: 10px;float:left;width:100%;font-weight: bold;"><?= Yii::t('AdminModule.admin','Region') ?> : </div>

            <div style="float:left;width:100%;font-weight: bold;">
                <label><input readonly id="regionId"></label>
            </div>

            <div style="margin-top: 10px;float:left;width:100%;font-weight: bold;"><?= Yii::t('AdminModule.admin','District') ?> : </div>

            <div style="float:left;width:100%;font-weight: bold;">
                <label><input readonly id="districtId"></label>
            </div>

            <div style="margin-top: 10px;float:left;width:100%;font-weight: bold;"><?= Yii::t('AdminModule.admin','Country') ?> : </div>

            <div style="float:left;width:100%;font-weight: bold;">
                <label><input readonly id="countryId"></label>
            </div>

            <div style="clear:both"></div>
            <br>

            <div class="form-group field-organizerCompany-street">
                <label class="control-label" for="organizerCompany_street"><?= Yii::t('AdminModule.admin', 'Street') ?></label>
                <input type="text" id="organizerCompany_street" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[street]" value="<?= $model->street ?>">
                <div class="help-block"></div>
            </div>

            <div class="form-group field-organizerCompany-coordinates">
                <label class="control-label" for="organizerCompany_coordinates"><?= Yii::t('AdminModule.admin', 'Coordinates') ?></label>
                <input type="text" id="organizerCompany_coordinates" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[coordinates]" value="<?= $model->coordinates ?>">
                <div class="help-block"></div>
            </div>

            <div id="organizerCompany-coordinates-map" style="width: 800px; height: 400px">

                <?php
                $getClientScript = Yii::app()->getClientScript();
                $getClientScript->registerScriptFile('https://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU');
                $publicPath = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

                $getClientScript->registerScriptFile($publicPath . '/' . 'js/map/Map.js', CClientScript::POS_END);
                $getClientScript->registerScriptFile($publicPath . '/' . 'js/_ActiveForm/' . 'typeMap' . '.js', CClientScript::POS_END);

                $getClientScript->registerScript(
                    md5('typeMap' . json_encode(
                            array_replace_recursive(
                                [
                                    'coordinates' => $model->coordinates,
                                    'selector' => 'organizerCompany-coordinates-map',
                                ],
                                [
                                    'targetCoordinates' => 'organizerCompany-coordinates',
                                    'targetStreet' => 'organizerCompany-street',
                                ]
                            )
                        )
                    ),
                    'typeMap' . '(' . json_encode(
                        array_replace_recursive([
                            'coordinates' => $model->coordinates,
                            'selector' => 'organizerCompany-coordinates-map',
                        ],
                            [
                                'targetCoordinates' => 'organizerCompany-coordinates',
                                'targetStreet' => 'organizerCompany-street',
                            ]
                        )
                    ) . ')',
                    CClientScript::POS_READY
                );
                ?>
            </div>

            <br/>
            <br/>

            <?= Yii::t('AdminModule.admin','Active') ?>
            <?=CHtml::activeDropDownList($model, 'active', array(0 => 'Не активный', 1 => 'Активный'));?>

            <br/>
            <?= Yii::t('AdminModule.admin', 'Status') ?>
            <?= CHtml::activeDropDownList($model, 'statusId', OrganizerCompanyStatus::getStatuses(), array('empty' => '')) ?>
            <br/>

            <div class="form-group field-organizerCompany-exdbId">
                <label class="control-label" for="organizerCompany_exdbId"><?= Yii::t('AdminModule.admin', 'Expodatabase organizer id') ?></label>
                <input type="text" id="organizerCompany_exdbId" class="input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" name="OrganizerCompany[exdbId]" value="<?= $model->exdbId ?>">
                <div class="help-block"></div>
            </div>

            <div style="margin:20px 0px;float:left;width:100%;font-weight: bold;>
                <span style="float:left;width:20px;margin-left:20px"> <?= Yii::t('AdminModule.admin','Organizer association') ?>:</span>
            <?php
            $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
                'name' => ucfirst(get_class($model))."[".OrganizerCompany::ASSOCIATION_VARIABLE."]",
                'value' => json_encode(OrganizerCompanyAdminService::getAssociationsByOrganizerCompanyId($model->id)),
                'options' => array(
                    'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
                    'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
                    'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
                    'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
                    'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
                    'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
                    'width' => '100%',
                    'placeholder' => "",
                    'multiple' => true,
                    'minimumInputLength' => 1,
                    'ajax' => array(
                        'url' => $this->createUrl('getAssociations'),
                        'dataType' => 'json',
                        'data' => 'js:function (term, page) { return { q: term}; }',
                        'results' => 'js:function(data,page){return {results: data};}',

                    ),
                    'formatResult'    => 'js:function(data){
                    return data.id + " " + data.name;
                }',
                    'formatSelection' => 'js: function(data) {
                    return data.id + " " + data.name;
                }',
                    'initSelection' => 'js:function (element, callback) {
                    // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                    var items = JSON.parse(element.val());
                    element.val("");
                    callback(items);
                }',
                ),
                'htmlOptions' => array(
                    'class' => '',
                    'style' => 'border:none;margin-top:10px;',
                ),
            ));
            ?>
        </div>

        </div>
    </div>
    <div class="cols-row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-success">
                <?= Yii::t('AdminModule.admin', 'Save') ?>
            </button>
            <?php if(!empty($model->id)) : ?>
                <?=CHtml::link('Просмотр', Yii::app()->createUrl('admin/organizer/view', array('id' => $model->id)), ['class' => 'btn btn-info'])?>
            <?php endif; ?>
            <?=CHtml::link('Вернуться к списку', Yii::app()->createUrl('admin/organizer/admin'), ['class' => 'btn btn-primary'])?>
        </div>
    </div>
</form>

<div>
    <?= CHtml::decode($model->exdbContactsRaw) ?>
</div>