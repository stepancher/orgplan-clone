<?php
/**
 * @var array               $pageVariants
 * @var int                 $pageSize
 * @var array               $fields
 * @var array               $selectedFields
 * @var Controller          $this
 * @var CSqlDataProvider    $dataProvider
 * @var OrganizerCompany    $model
 * @var array               $columns
 */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$('#grid-button').on('click', function () {
    var \$fields = $('#OrganizerCompany_fields .checkbox.checked input');
    var pageSize = $('#OrganizerCompany_pageSize .drop-list__item--selected input').val();
    var fields = \$fields.map(function() {
        return $(this).val();
    }).get();
    
    $.fn.yiiGridView.update('grid', {data: {
        OrganizerCompany_fields: fields.toString(),
        OrganizerCompany_pageSize: pageSize,
        OrganizerCompany_page: 1
    }})
});
");

?>
<h1><?= Yii::t('AdminModule.admin', 'Manage organizer companies'); ?></h1>

<div style="height: 50px;">
    <?= TbHtml::linkButton(
        'Добавить новую компанию организатора',
        [
            'url'   => $this->createUrl('create'),
            'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
            'style' => "float: left; white-space: nowrap; width: auto; padding: 10px;"
        ]
    ); ?>

    <div style="float: right;">
        <div style="width: 180px; display: inline-block;">
            <?php $this->widget('application.modules.admin.components.ListAutoComplete', [
                'id'    => 'OrganizerCompany_pageSize',
                'name'  => 'OrganizerCompany_pageSize',
                'label' => 'На странице',
                'itemOptions' => [
                    'data' => $pageVariants
                ],
                'selectedItems' => [$pageSize],
                'htmlOptions' => [
                    'style' => 'margin: 0px;'
                ],
                'autoComplete' => false,
                'multiple' => false
            ]); ?>
        </div>

        <div style="width: 250px; display: inline-block;">
            <?php
            $this->widget('application.modules.admin.components.ListAutoComplete', [
                'id'    => 'OrganizerCompany_fields',
                'name'  => 'OrganizerCompany_fields',
                'label' => 'Поля формы',
                'itemOptions' => [
                    'data' => array_map(
                        function($field) use($model)
                        {
                            return $model->getAttributeLabel($field);
                        },
                        $fields
                    ),
                ],
                'selectedItems' => $selectedFields,
                'htmlOptions' => [
                    'style' => 'margin: 0;'
                ],
                'autoComplete' => false
            ]); ?>
        </div>

        <div style="display: inline-block;">
            <?= TbHtml::button('Применить', [
                'id'    => 'grid-button',
                'class' => 'b-button d-bg-primary-dark d-bg-primary--hover d-text-light btn',
                'style' => 'display: block; margin: 0px;'
            ]); ?>
        </div>
    </div>
</div>

<div style="clear: both;"></div>

<?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id'           => 'grid',
        'dataProvider' => $dataProvider,
        'filter'       => $model,
        'ajaxUpdate'   => false,
        'columns'      => array_merge([
            'id' => [
                'name' => 'id',
                'header' => 'id',
                'type' => 'raw',
            ],
            [
                'class' => 'CButtonColumn',
                'template'=>'{update}{view}',
                'buttons' => array(
                    'update' => array(
                        'label' => 'Редактировать',
                        'url' => function($data){
                            return Yii::app()->createUrl("admin/organizer/update", array("id" => $data['id']));
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    ),
                    'view' => array(
                        'label' => 'Просмотреть',
                        'url' => function($data){
                            return Yii::app()->createUrl("admin/organizer/view", array("id" => $data['id']));
                        },
                        'options' => array(
                            'target' => '_blank',
                        ),
                    ),
                ),
                'headerHtmlOptions' => array(
                    'style' => "min-width: 16px;"
                ),
                'htmlOptions' => array(
                    'target' => '_blank',
                    'style' => "min-width: 16px;"
                ),
            ]
        ],
            $columns
        ),
        'htmlOptions' => array(
            'style' => "overflow-y: auto;"
        ),
        'template' => '{summary}{pager}{items}{pager}{summary}'
    ));
?>