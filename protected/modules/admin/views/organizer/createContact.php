<?php
/**
 * @var OrganizerController $this
 * @var array               $data
 * @var array               $rows
 * @var integer             $organizerCompanyId
 * @var CSqlDataProvider    $dataProvider
 */
?>

<h1><?= Yii::t('AdminModule.admin', 'Create organizer contact'); ?></h1>

<div class="col-md-12" style="margin-top: 10px; padding: 0;">
    <?= CHtml::link(
        'Вернуться к просмотру организатора',
        Yii::app()->createUrl('admin/organizer/view', array('id' => $organizerCompanyId)),
        ['class' => 'btn btn-primary']
    ); ?>

    <?= CHtml::link(
        'Отмена',
        Yii::app()->createUrl('admin/organizer/cancel', array('organizerId' => $data['id'])),
        ['class' => 'btn btn-danger']
    ); ?>
</div>

</br>
</br>
</br>

<?php $this->renderPartial('partial/_contactRedactor', array('data' => $data, 'rows' => $rows, 'dataProvider' => $dataProvider)); ?>

