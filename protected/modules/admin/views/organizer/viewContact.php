<?php
/**
 * @var OrganizerController $this
 * @var array               $data
 * @var array               $rows
 * @var CArrayDataProvider  $fairsDataProvider
 * @var CSqlDataProvider    $dataProvider
 */
?>

<h1><?= Yii::t('AdminModule.admin', 'View organizer contact') . ' #' . $data['id']; ?></h1>

<div class="col-md-12" style="margin-top: 10px; padding: 0;">
    <?= CHtml::link(
        'Вернуться к просмотру организатора',
        Yii::app()->createUrl('admin/organizer/view', array('id' => $data['companyId'])),
        ['class' => 'btn btn-primary']
    ); ?>
    <?= CHtml::link(
        'Удалить контакт организатора',
        Yii::app()->createUrl("admin/organizer/cancelOrganizerWithRedirect", array('organizerId' => $data['id'])),
        ['class' => 'btn btn-danger']
    ); ?>
</div>

</br>
</br>
</br>

<?php $this->renderPartial('partial/_contactRedactor', array('data' => $data, 'rows' => $rows, 'dataProvider' => $dataProvider)); ?>

<div style="clear: both"></div>
</br>

<div id="fair-widget" style="margin-bottom: 5px;">

    <b>
        <?= Yii::t('AdminModule.admin','Attach the exhibit to contact the organizer') ?>
    </b>


    <div style="clear: both"></div>

    <?php
    $this->widget( 'ext.anggiaj.eselect2.ESelect2', array(
        'name' => 'OrganizerContact'."[".Organizer::FAIR_VARIABLE."]",
        'value' => NULL,
        'options' => array(
            'formatNoMatches' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'No matches found') . '";}',
            'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
            'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('AdminModule.admin', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
            'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('AdminModule.admin', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
            'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('AdminModule.admin', 'Loading more results...') . '";}',
            'formatSearching' => 'js:function(){return "' . Yii::t('AdminModule.admin', 'Searching...') . '";}',
            'width' => '40%',
            'placeholder' => "Выберите выставку",
            'minimumInputLength' => 1,
            'ajax' => array(
                'url' => $this->createUrl('getFairs'),
                'dataType' => 'json',
                'data' => 'js:function (term, page) { return { q: term}; }',
                'results' => 'js:function(data,page){return {results: data};}',

            ),
            'formatResult'    => 'js:function(data){
                                return data.id + " " + data.fairName + " " + data.beginYear + " " + data.cityName;
                            }',
            'formatSelection' => 'js: function(data) {
                                return data.id + " " + data.fairName + " " + data.beginYear + " " + data.cityName;
                            }',
            'initSelection' => 'js:function (element, callback) {
                                // callback() accepts array of objects like: [{"id":1,"name":"Item123"},{"id":2,"name":"Item124"}]
                                var items = JSON.parse(element.val());
                                element.val(items.id);
                                callback(items);
                            }',
        ),
        'htmlOptions' => array(
            'class' => '',
            'style' => 'border:none; margin-top:10px;',
            'id' => "eselect-id",
        ),
    ));
    ?>
</div>

<div style="display: inline-block;">
    <?= TbHtml::button('Прикрепить', [
        'id'    => 'fair-attach-button',
        'class' => 'btn btn-success',
        'style' => 'display: block; margin: 0;'
    ]); ?>
</div>

</br>

<?php $this->renderPartial('partial/_fairList', array('fairsDataProvider' => $fairsDataProvider)); ?>

<script>

    $('#fair-attach-button').click(function () {

        var select = $('#eselect-id');
        var fairId = null;

        if(select.prop("tagName") == 'INPUT'){
            fairId = select.val();
        }

        $.ajax({
            url: '<?php echo $this->createUrl('fairAttach') ?>',
            method: 'POST',
            async: false,
            dataType: 'json',
            data: {'fairId': fairId, 'organizerId': '<?= $data['id'] ?>'},
            success: function(data){

                if(data.success == 'ok'){
                    location.reload();
                } else {
                    alert(data.message);
                }
            }
        });
    });

</script>