function ConfirmDelete(params){
    return ({
        confirmText: '',
        run: function (params) {
            var me = this;
            if('confirmText' in params) {
                me.confirmText = params.confirmText;
            }
            $('.delete-article').on('click', function(e){
                if (confirm(me.confirmText)){
                    return true;
                } else {
                    e.preventDefault();
                    return false;
                }
            });
        }
    }).run(params);
};