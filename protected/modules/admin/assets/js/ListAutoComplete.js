function ListAutoComplete(params) {
    params = $.extend({
        data: [],
        selector: null,
        event: 'keyup',
        ajax: false,
        showNotEmptyLabel: true
    }, params);
    return ({
        $element: null,
        $list: null,
        $noMatches: null,
        value: null,
        haveChecked: false,
        oldData: [],
        params: {
            alwaysCount: false,
            multiple: true,
            runSynonym: false,
        },
        init: function (params) {
            this.params = $.extend(this.params, params);
            this.$element = $(this.params.selector);
            this.$list = this.$element.find('.js-drop-list');
            this.$noMatches = this.$list.find('.js-drop-list__item--empty');

            this.haveChecked = this.$list.find('input[type=checkbox]:checked').length > 0;

            return this;
        },
        reset: function () {
            $(params.selector).find('.drop-list__item--selected').removeClass('drop-list__item--selected');
            $(params.selector).find('.list-auto-complete__drop-button').text('');
        },
        run: function () {
            var me = this;

            $(document).click(function (e) {
                var showBlock = $(e.target).closest('.js-list-auto-complete').get(0);
                $('.js-list-auto-complete').each(function (k) {
                    if (this != showBlock) {
                        $(this).removeClass('active');
                    }
                });
            });

            $(params.selector).on('reset', function () {
                me.reset();
            });

            $(window).on('resize', function () {
                me.$list.width(me.$element.width());
            });

            var $label = me.$element.find('.js-list-auto-complete__drop-button');

            if (me.haveChecked) {
                if ($label.hasClass('d-bg-tab')) {
                    $label.removeClass('d-bg-tab');
                    $label.addClass('d-bg-tooltip');
                }
            } else {
                if ($label.hasClass('d-bg-tooltip')) {
                    $label.removeClass('d-bg-tooltip');
                    $label.addClass('d-bg-tab');
                }
            }

            me.$list.delegate('input[type=checkbox]', 'change', function () {
                var $checkbox = $(this);
                if (!me.params.multiple) {
                    var $parent = $checkbox.parent();
                    me.$list.find('input[type=checkbox]').each(function () {
                        var $elm = $(this);
                        $elm.prop('checked', false);
                        $elm.parent().removeClass('drop-list__item--selected');
                    });
                    $checkbox.prop('checked', true);
                    $parent.addClass('drop-list__item--selected');
                    me.$element.removeClass('active');
                    me.value = $($checkbox).val();
                }
                me.reCalculate($label, $checkbox);

                if (me.haveChecked) {
                    if ($label.hasClass('d-bg-tab')) {
                        $label.removeClass('d-bg-tab');
                        $label.addClass('d-bg-tooltip');
                    }
                } else {
                    if ($label.hasClass('d-bg-tooltip')) {
                        $label.removeClass('d-bg-tooltip');
                        $label.addClass('d-bg-tab');
                    }
                }
            });


            var $input = me.$element.find('.js-list-auto-complete___match-input input');
            $label.click(function () {
                me.resizeWidth();
                $input.addClass('input-block-level d-bg-tab d-bg-tab--hover d-bg-tab--focus d-text-dark');
                me.$element.toggleClass('active');
                if (me.$element.hasClass('active')) {
                    /* SlimScroll*/
                    $('.js-list-auto-complete__main-list').slimScroll({
                        color: '#000',
                        size: '5px',
                        height: '350px',
                        alwaysVisible: true
                    });
                    $input.focus();
                }
            });

            $input.bind(me.params.event, function () {
                me.matcher(this.value);
            });

            return this;
        },

        /** resize toggle block by width of input[type=text] */
        resizeWidth: function () {
            if ($(this.$element).children('label').hasClass('f-contest-list')) {
                this.$list.width(this.$element.width() - 2);
            } else {
                this.$list.width(this.$element.width() - 3);
            }
        },
        reCalculate: function ($label, $checkbox) {
            var me = this;
            if (this.params.multiple) {
                var count = this.$list.find('input[type=checkbox]:checked').length;
                if (count == 0) {
                    me.haveChecked = false
                }
                if ($label.text().match(/\((.+)\)/)) {
                    $label.text($label.text().replace(/\((.+)\)/, me.params.alwaysCount ? ' (' + count + ')' : (count ? ' (' + count + ')' : ''))).addClass('selected');
                } else {
                    if (+count != 0) {
                        $label.text($label.text() + ' (' + count + ')').addClass('selected');
                        me.haveChecked = true;
                    }
                }
            } else {
                var text = $checkbox.parent().text();
                if ($label.text().match(/\((.+)\)/)) {
                    $label.text($label.text().replace(/\((.+)\)/, ' (' + text + ')')).addClass('selected');
                } else {
                    if (me.params.showNotEmptyLabel) {
                        $label.text($label.text() + (text != '' ? ' (' + text + ')' : '')).addClass('selected');
                    } else {
                        $label.text(text != '' ? text : '').addClass('selected');
                    }
                }
            }
        },
        getNewData: function (value) {
            var me = this;
            var $mainList = me.$element.find('.js-list-auto-complete__main-list');
            if (me.oldData.length == 0) {
                var val = '';
                var name = '';
                $.each($mainList.find('label'), function () {
                    val = $(this).find('input[type=checkbox]').val();
                    name = $(this).text();
                    me.oldData[val] = name;
                });
            }
            if (value.length < 1) {
                $mainList.find('label').remove();
                var newElm = '';
                $.each(me.oldData, function (key, value) {
                    if (key !== 0 && value != 'undefined') {
                        newElm = '<label ' +
                            'class="js-drop-list__item drop-list__item d-bg-base d-bg-input--hover hidden-checkbox" ' +
                            'style="display: block;"><input value="' + key + '" type="checkbox" ' +
                            'name="TZ[fairId]">' + value + '</label>';
                        $mainList.append(newElm);
                    }
                });
            } else {
                $.get('/ru/fair/getFairList', {filterData: value}, function (data) {
                    $mainList.find('label').remove();
                    if (data.status == 1) {
                        $mainList.find('label').remove();
                        var newElm = '';
                        $.each(data.fairsData, function (key, value) {
                            newElm = '<label ' +
                                'class="js-drop-list__item drop-list__item d-bg-base d-bg-input--hover hidden-checkbox" ' +
                                'style="display: block;"><input value="' + key + '" type="checkbox" ' +
                                'name="TZ[fairId]">' + value + '</label>';
                            $mainList.append(newElm);
                        });
                    } else if (data.status == 0) {
                        var text = '<label class="js-drop-list__item drop-list__item d-bg-base d-bg-input--hover hidden-checkbox">'
                            +me.params.emptyText+
                            '</label>';
                        $mainList.append(text);
                    }
                }, 'json');
            }
        },
        /** matching by input[type=text] */
        matcher: function (value) {
            var me = this;
            if (me.params.ajax) {
                me.getNewData(value);
            } else {
                $('.js-drop-list__item', me.$list).each(function (i, elm) {
                    var $elm = $(elm);
                    if (value != '') {
                        if ($elm.text().match(new RegExp(value, 'gi'))
                            || (me.params.runSynonym && typeof $elm.data('synonyms') != 'undefined'
                                && $elm.data('synonyms').match(new RegExp(value, 'gi'))
                            )
                        ) {
                            $elm.show();
                        } else {
                            $elm.hide();
                        }
                    } else {
                        $elm.show();
                    }
                });
                if (!$('.js-drop-list__item:visible', me.$list).length) {
                    me.$noMatches.show();
                } else {
                    me.$noMatches.hide();
                }
            }
        }
    })
        .init(params)
        .run();
}