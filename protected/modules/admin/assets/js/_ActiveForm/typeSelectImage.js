function typeSelectImage(opts) {
	var $target = $(opts.target);
	var multiple = opts.multiple;
	$target.click(function () {
		var $elm = $(this);

		if (!multiple) {
			$target.removeClass('active');
			$elm.parent().find('input[type=checkbox]').prop('checked', false);
		}

		$elm.toggleClass('active');
		$elm.find('input[type=checkbox]').prop('checked', $elm.hasClass('active')).trigger('change');
	});
}