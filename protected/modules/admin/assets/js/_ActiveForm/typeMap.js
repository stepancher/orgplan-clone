function typeMap(opts) {
	ymaps.ready(function(){
		new Map({
			selector: opts.selector,
			enableMapObjects: false,
			draggablePlacemark: true,
			placemarkCoordsTargetSelector: opts.targetCoordinates,
			placemarkNameTargetSelector:  opts.targetStreet,
			coordinates: opts.coordinates,
			hintContent: "Перетащите маркер в нужное место"
		});
	})
};