function Map(params) {
	return ({
		hotelCluster: null,
		landmarkCluster: null,
		cafeCluster: null,
		loader: null,
		checkboxEvent: false,
		map: null,
		params: {
			selector: "map",
			zoom: 16,
			coordinates: null,
			url: null,
			placemarkCoordsTargetSelector: null,
			placemarkNameTargetSelector: null,
			hintContent: null,
			draggablePlacemark: false,
			enableMapObjects: true
		},
		init: function (params) {
			var me = this;

			$.extend(me.params, params);

			var map, placemark;

			if (me.params.coordinates != null) {
				me.params.coordinates = me.params.coordinates.split(',');
			} else {
				me.params.coordinates = "55.76, 37.64".split(','); // Москва
			}

			map = new ymaps.Map(me.params.selector, {
				center: me.params.coordinates,
				zoom: me.params.zoom
			});

			placemark = new ymaps.Placemark(me.params.coordinates, {
				hintContent: me.params.hintContent
			}, {
				draggable: me.params.draggablePlacemark
			});

			if (me.params.draggablePlacemark) {
				placemark.events.add(['dragend'], function (e) {
					var marker = e.get('target');
					var coordinates = marker.geometry.getCoordinates();
					if (me.params.placemarkCoordsTargetSelector) {
						$(me.params.placemarkCoordsTargetSelector).val(coordinates);
					}
					ymaps.geocode(coordinates, {
						kind: 'house',
						results: 1
					}).then(function (res) {
						if (me.params.placemarkNameTargetSelector) {
							$(me.params.placemarkNameTargetSelector).val(res.geoObjects.get(0).properties.get('name'));
						}
					});

				});
			}

			if (me.params.enableMapObjects) {
				me.loader = $('.ajax-loader');
				me.hotelCluster = new ymaps.Clusterer({
					clusterDisableClickZoom: true,
					preset: 'twirl#invertedVioletClusterIcons',
					openBalloonOnClick: true
				});
				me.landmarkCluster = new ymaps.Clusterer({
					clusterDisableClickZoom: true,
					preset: 'twirl#invertedDarkgreenClusterIcons',
					openBalloonOnClick: true
				});
				me.cafeCluster = new ymaps.Clusterer({
					clusterDisableClickZoom: true,
					preset: 'twirl#invertedDarkorangeClusterIcons',
					openBalloonOnClick: true
				});
			}

			map.controls.add(
				new ymaps.control.ZoomControl()
			);
			map.behaviors.enable('scrollZoom');
			map.geoObjects.add(placemark);

			me.map = map;

			return me;
		},
		run: function () {
			var me = this;

			if (me.params.enableMapObjects) {
				me.map.events.add('actionend', function () {
					if (me.map.getZoom() >= me.params.zoom) {
						me.send();
					}
				});

				$('.map-request').change(function () {
					if (this.checked && me.map.getZoom() >= me.params.zoom) {
						me.checkboxEvent = true;
						me.send(this);
					} else {
						if (this.name == 'hotel') {
							me.hotelCluster.removeAll();
						} else if (this.name == 'landmark') {
							me.landmarkCluster.removeAll();
						} else if (this.name == 'cafe') {
							me.cafeCluster.removeAll();
						}
					}
				});
			}
		},
		send: function (elm) {
			var me = this;
			if (me.checkboxEvent && typeof elm !== 'undefined') {
				me.getResults(elm);
			} else {
				$('.map-request').each(function () {
					if (this.checked) {
						me.getResults(this);
					}
				});
			}
			me.checkboxEvent = false;
		},
		getResults: function (elm) {
			var me = this, $elm = $(elm);
			$elm.attr('disabled', 'disabled');
			me.loader.fadeIn(300);
			var bounds = me.map.getBounds();
			$.get(me.params.url, {
				top: String(bounds[1][0]).substr(0, 7),
				right: String(bounds[1][1]).substr(0, 7),
				bottom: String(bounds[0][0]).substr(0, 7),
				left: String(bounds[0][1]).substr(0, 7),
				request: elm.name
			}, function (data) {
				if (data) {
					var json = JSON && JSON.parse(data) || $.parseJSON(data);
					var myGeoObjects = [], color;

					if (elm.name == 'hotel') {
						me.hotelCluster.removeAll();
						color = 'violetDotIcon';
					} else if (elm.name == 'landmark') {
						me.landmarkCluster.removeAll();
						color = 'greenDotIcon';
					} else if (elm.name == 'cafe') {
						me.cafeCluster.removeAll();
						color = 'darkorangeDotIcon';
					}

					$.each(json.data, function (i, obj) {
						var information = '';
						if (obj.tags_ru) {
							$.each(obj.tags_ru, function (name, value) {
								if (value) {
									information = information + name + ': ' + value + '<br />';
								}
							});
						}
						myGeoObjects[i] = new ymaps.GeoObject({
								geometry: {
									type: "Point",
									coordinates: [obj.lat, obj.lon]
								},
								properties: {
									hintContent: obj.index_name,
									clusterCaption: obj.class_ru + ' <b>' + obj.name_ru + '</b>',
									balloonContentBody: information
								}
							}, {
								preset: "twirl#" + color
							}
						);
					});

					if (elm.name == 'hotel') {
						me.hotelCluster.add(myGeoObjects);
						me.map.geoObjects.add(me.hotelCluster);
						$elm.parent().addClass('checked');
					} else if (elm.name == 'landmark') {
						me.landmarkCluster.add(myGeoObjects);
						me.map.geoObjects.add(me.landmarkCluster);
						$elm.parent().addClass('checked');
					} else if (elm.name == 'cafe') {
						me.cafeCluster.add(myGeoObjects);
						me.map.geoObjects.add(me.cafeCluster);
						$elm.parent().addClass('checked');
					}

					$elm.removeAttr('disabled');
					me.loader.fadeOut(300);
				}
			}, 'html');
		}
	}).init(params).run();
}