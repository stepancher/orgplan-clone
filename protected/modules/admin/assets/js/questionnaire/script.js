function Questionnaire (params) {
    return ({
        confirmText: '',
        init: function(params){
            var me = this;
            if('confirmText' in params) {
                me.confirmText = params.confirmText;
            }
        },
        run: function(params){
            var me = this;
            me.init(params);
            $(document).delegate('.js-profile-questionnaire__add-option', 'click', function () {
                $(this).next().toggleClass('active')
            });

            $('.profile-questionnaire__colors-checkboxes input[type=radio]').click(function () {
                var $elm = $(this);
                $('.profile-questionnaire__colors-checkboxes input[type=radio]').each(function () {
                    $(this).parent().removeClass('checked');
                });
                $elm.parent().addClass('checked');
                changeBG();
            });
            changeBG();

            $(document).delegate('#questionnaire-form .profile-questionnaire label.checkbox input:checkbox', 'click', function () {
                var question = me.confirmText+' "' + $(this).next().text() + '"?';
                if (confirm(question)) {
                    $(this).parent().closest('.input-block').remove();
                }else {
                    $(this).prop( "checked", false )
                }
            });

            $(document).delegate('#questionnaire-form .profile-questionnaire label.checkbox label', 'click', function () {
                var question = me.confirmText+' "' + $(this).text() + '"?';
                if (confirm(question)) {
                    $(this).parent().closest('.input-block').remove();
                }
            });

            $(document).delegate('.styled-uploader__styled-button', 'click', function () {
                $(this).parent().find('.styled-uploader__main-hidden-input').trigger('click');
            });

            $(document).delegate('span.question-label', 'click', function () {
                var question = me.confirmText+' "' + $(this).text() + '"?';
                if (confirm(question)) {
                    $(this).parent().closest('tr').remove();
                }
            });


            $('#questionnaire-form-logo').find(':input').change(function () {
                var $form = $('#questionnaire-form-logo');
                $form.submit();
            });

            $('button[type=submit]').click(function () {
                var $form = $('#questionnaire-form');
                var $input = $form.find('.input-save-mode');
                if ($(this).hasClass('data-generate')) {
                    $input.val(1);
                } else {
                    $input.val(0);
                }
                $form.submit();
            });

            var $template = $('#option-checkbox-template');

            $(document).delegate('.profile-questionnaire input[type=text]', 'keyup', function (e) {
                if (e.keyCode == 13) {
                    var $elm = $(this), $tr = $elm.closest('tr'), $newTr, index;

                    if ($elm.val() != '') {
                        if ($elm.attr('name') == 'Add[additional_label]') {
                            $newTr = $tr.clone();
                            index = +$tr.prev().find('td:first-child input[type=hidden]').data('index') + 1;
                            $newTr.find('td:first-child').html('<span class="question-label">' + $elm.val() + '</span>' + '<input type="hidden" value="' + $elm.val() +
                            '" name="Questionnaire[data][labels][' + $elm.data('col-name') + '][' + index + ']" data-index="' + index + '" />');

                            $newTr.insertBefore($tr);
                            var content = '<div></div><div>' +
                                '<i class="icon icon-small--add-plus icon--cursored-icon profile-questionnaire__add-option js-profile-questionnaire__add-option"></i>' +
                                '<input data-col-name="Contact" type="text" value="" name="Add[additional_field]" id="Add_additional_field" class="">' +
                                '</div>';
                            $tr.find('td:last-child div').remove().html(content);
                            $tr.find('td:last-child').html(content);
                        } else if ($elm.attr('name') == 'Add[additional_field]') {
                            var $prevTr = $tr.prev().find('td:first-child input[type=hidden]');
                            if ($prevTr.length) {
                                index = +$tr.prev().find('td:first-child input[type=hidden]').data('index') + 1;
                            } else {
                                index = 1;
                            }

                            $tr.find('td:last-child > div:first-child').append(
                                $template.html().strtr(
                                    {
                                        '{label}': '<label>' + $elm.val() + '</label>',
                                        '{name}': '[' + $elm.data('col-name') + '][' + index + '][]',
                                        '{id}': index
                                    }
                                )
                            );
                        }
                    }
                    $tr.find('input[type=text]').removeClass('active').val('');
                }
            });
        }
    }).run(params);
}



function changeBG() {
    var $elm = $('.profile-questionnaire__colors-checkboxes input[type=radio]:checked');
    var block = document.querySelector(".profile-questionnaire__header-settings");
    if ($elm.val() == 1) {
        block.style.backgroundColor = "#F5A701";
    } else if ($elm.val() == 2) {
        block.style.backgroundColor = "#EF8B01";
    } else if ($elm.val() == 3) {
        block.style.backgroundColor = "#E8523D";
    } else if ($elm.val() == 4) {
        block.style.backgroundColor = "#44ACE3";
    } else if ($elm.val() == 5) {
        block.style.backgroundColor = "#487CC7";
    } else if ($elm.val() == 6) {
        block.style.backgroundColor = "#3DB571";
    } else if ($elm.val() == 7) {
        block.style.backgroundColor = "#D1D1D1";
    }
}

String.prototype.strtr = function (replacePairs) {
    "use strict";
    var str = this.toString(), key, re;
    for (key in replacePairs) {
        if (replacePairs.hasOwnProperty(key)) {
            re = new RegExp(key, "g");
            str = str.replace(re, replacePairs[key]);
        }
    }
    return str;
};