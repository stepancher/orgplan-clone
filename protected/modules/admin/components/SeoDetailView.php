<?php
/**
 * File SeoDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class SeoDetailView
 *
 * @property Seo $data
 */
class SeoDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		
		parent::init();
	}
}