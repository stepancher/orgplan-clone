<?php
/**
* File SeoForm.php
*/
Yii::import('ext.helpers.ARForm');

/**
* Class Seo*/
class SeoForm extends TbForm
{
	/**
	* Префикс в аттрибут name (например: "[]") для обработки полей формы с name=Seo[]['login']
	* @var array
	*/
	public $elementGroupName = '';

	/**
	* Инициализация формы
	*/
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();
		/** @var Seo $model */
		$model = $this->getModel();

		$this->elements = CMap::mergeArray(
			ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
			$this->initElements()
		);

		parent::init();
	}

	/**
	* Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=Seo[]['login']
	* @param string $name
	* @param TbFormInputElement $element
	* @param bool $forButtons
	*/
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	* Инициализация элементов формы
	* @return array
	*/
	public function initElements()
	{
		return array(
			'id' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'title' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'description' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'keywords' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'url' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'route' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'params' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'refId' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
            'lastmode' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
            ),
            'sitemapPriority' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
            ),
		);
	}
}