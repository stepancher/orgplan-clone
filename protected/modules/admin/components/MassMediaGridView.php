<?php
/**
 * File MassMediaGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class MassMediaGridView
 *
 * @property MassMedia $model
 */
class MassMediaGridView extends TbGridView
{
    /**
     * Модель MassMedia::model() or new MassMedia('scenario')
     * @var MassMedia	 */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns(){
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'name' => array(
                'name' => 'name',
            ),
            'description' => array(
                'value' => 'substr(strip_tags($data->description),0,160)',
            ),
            'imageId' => array(
                'name' => 'imageId',
                'value' => function($data){
                    echo TbHtml::image(
                        H::getImageUrl($data->getMainImage(), 'file', 'noImage'),
                        $data->name,
                        array(
                            'title' => $data->name,
                            'style' => 'max-width:150px;',
                        )
                    );
                },
                'htmlOptions' => array(
                    'style' => 'width:200px;'
                ),
            ),
            'url' => array(
                'name' => 'url',
            ),
            'active' => array(
                'name' => 'active',
            ),
        );
        return $columns;
    }
}