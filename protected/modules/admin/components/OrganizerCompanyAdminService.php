<?php

class OrganizerCompanyAdminService
{

    /**
     * @param array $selectedFields
     * @param int $pageSize
     * @param OrganizerCompany $model
     * @return CSqlDataProvider
     */
    public static function search($selectedFields = [], $pageSize = 10, &$model)
    {
        $sql = "SELECT * FROM (SELECT oc.id, 
                       oc.active,
                       troc.name,
                       troc.street,
                       oc.linkToTheSiteOrganizers,
                       oc.phone,
                       oc.email,
                       oc.cityId,
                       oc.coordinates,
                       oc.exdbId,
                       oc.exdbContacts,
                       oc.exdbContactsRaw,
                       trc.name AS cityName,
                       trr.name AS regionName,
                       trd.name AS districtName,
                       trcn.name AS countryName,
                       GROUP_CONCAT(DISTINCT ass.name SEPARATOR ',') AS associationName,
                       COUNT(f.id) AS fairsCount,
                       ocs.name AS statusName
                       
                   FROM tbl_organizercompany oc
                       LEFT JOIN tbl_trorganizercompany troc ON troc.trParentId = oc.id AND troc.langId = :langId
                       LEFT JOIN tbl_organizercompanystatus ocs ON ocs.id = oc.statusId
                       LEFT JOIN tbl_city c ON oc.cityId = c.id
                       LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id AND trc.langId = :langId
                       LEFT JOIN tbl_region r ON c.regionId = r.id
                       LEFT JOIN tbl_trregion trr ON trr.trParentId = r.id AND trr.langId = :langId
                       LEFT JOIN tbl_district d ON r.districtId = d.id
                       LEFT JOIN tbl_trdistrict trd ON trd.trParentId = d.id AND trd.langId = :langId
                       LEFT JOIN tbl_country cn ON d.countryId = cn.id
                       LEFT JOIN tbl_trcountry trcn ON trcn.trParentId = cn.id AND trcn.langId = :langId
                       LEFT JOIN tbl_organizercompanyhasassociation ocha ON ocha.organizerCompanyId = oc.id
                       LEFT JOIN tbl_association ass ON ass.id = ocha.associationId
                       LEFT JOIN tbl_organizer o ON o.companyId = oc.id
                       LEFT JOIN tbl_fairhasorganizer fho ON fho.organizerId = o.id
                       LEFT JOIN tbl_fair f ON fho.fairId = f.id
                       GROUP BY oc.id
                   ) query WHERE 1 = 1
        ";
        $description = $model->description();
        $sort = array();

        foreach ($selectedFields as $field){

            if(!empty($model->{$field})){

                if($field == 'countryName'){
                    $additionalQuery = "SELECT trParentId FROM tbl_trcountry WHERE name = :countryName GROUP BY trParentId";
                    $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':countryName' => $model->{$field}));

                    if(!empty($data) && isset($data['trParentId'])){
                        $countryName = TrCountry::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                        if(isset($countryName->name) && $model->{$field} != $countryName->name)
                            $model->{$field} = $countryName->name;
                    }
                } elseif ($field == 'regionName'){
                    $additionalQuery = "SELECT trParentId FROM tbl_trregion WHERE name = :regionName GROUP BY trParentId";
                    $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':regionName' => $model->{$field}));

                    if(!empty($data) && isset($data['trParentId'])){
                        $regionName = TrRegion::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                        if(isset($regionName->name) && $model->{$field} != $regionName->name)
                            $model->{$field} = $regionName->name;
                    }
                } elseif ($field == 'districtName'){
                    $additionalQuery = "SELECT trParentId FROM tbl_trdistrict WHERE name = :districtName GROUP BY trParentId";
                    $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':districtName' => $model->{$field}));

                    if(!empty($data) && isset($data['trParentId'])){
                        $districtName = TrDistrict::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                        if(isset($districtName->name) && $model->{$field} != $districtName->name)
                            $model->{$field} = $districtName->name;
                    }
                } elseif($field == 'cityName'){
                    $additionalQuery = "SELECT trParentId FROM tbl_trcity WHERE name = :cityName GROUP BY trParentId";
                    $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':cityName' => $model->{$field}));

                    if(!empty($data) && isset($data['trParentId'])){
                        $cityName = TrCity::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                        if(isset($cityName->name) && $model->{$field} != $cityName->name)
                            $model->{$field} = $cityName->name;
                    }
                }

                if(array_search('string', $description[$field]) !== FALSE){
                    $sql .= "AND query.{$field} LIKE '%{$model->{$field}}%'
                    ";
                } else {
                    $sql .= "AND query.{$field} = '{$model->{$field}}'
                    ";
                }

            }

            $sort[$field] = array(
                'asc' => "query.{$field}",
                'desc' => "query.{$field} DESC",
            );
        }

        $totalItemCountSql = Yii::app()->db->createCommand("SELECT COUNT(*) AS count FROM ({$sql}) t")->query(
            array(
                ':langId' => Yii::app()->language
            )
        );
        $totalItem = $totalItemCountSql->read();

        if(isset($totalItem['count']))
            $totalItemCount = $totalItem['count'];

        return new CSqlDataProvider($sql, array(
            'totalItemCount' => isset($totalItemCount) ? (int)$totalItemCount : $pageSize,
            'params' => array(
                ':langId' => Yii::app()->language,
            ),
            'pagination' => array(
                'pageSize' => $pageSize,
            ),
            'sort' => array(
                'attributes' => $sort,
            ),
        ));
    }

    /**
     * @param $organizerCompanyId
     * @return array|string
     */
    public static function getAssociationsByOrganizerCompanyId($organizerCompanyId){

        if(empty($organizerCompanyId)){
            return '';
        }

        $criteria = new CDbCriteria;
        $criteria->with['association'] = [
            'together' => TRUE,
        ];
        $criteria->addCondition("t.organizerCompanyId = :organizerCompanyId");
        $criteria->params[':organizerCompanyId'] = $organizerCompanyId;

        $association = CHtml::listData(
            OrganizerCompanyHasAssociation::model()->findAll($criteria),
            'associationId',
            function($el){
                return isset($el->association->name) ? $el->association->name : '';
            }
        );

        $items = array_map(
            function($associationId, $association) {
                return array(
                    'id'   => $associationId,
                    'name' => $association,
                );
            },
            array_keys($association),
            array_values($association)
        );

        return $items;
    }
}