<?php
/**
 * File SeoGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class SeoGridView
 *
 * @property Seo $model
 */
class SeoGridView extends TbGridView
{
	/**
	 * Модель Seo::model() or new Seo('scenario')
     * @var Seo
     */
	public $model;

	/**
	 * Двумерный массив аргументов для создания условий CDbCriteria::compare
	 * @see CDbCriteria::compare
	 * @var array
	 */
	public $compare = array(
		array()
	);
	/**
	 * Для добавления колонок в начало таблицы
	 * @var array
	 */
	public $columnsPrepend = array();

	/**
	 * Для добавления колонок в конец таблицы
	 * @var array
	 */
	public $columnsAppend = array();

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate {@link columns} objects.
	 */
	public function init()
	{
		// Включаем фильтрацию по сценарию search
		$this->model->setScenario('search');
		$this->filter = ARGridView::createFilter($this->model, true);

		// Создаем колонки таблицы
		$this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

		// Создаем провайдер данных для таблицы
		$this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

		// Инициализация
		parent::init();
	}

	/**
	 * Колонки таблицы
	 * @return array
	 */
	public function getColumns()
	{
		$columns = array(
			'id' => array(
				'name' => 'id',
			),
			'title' => array(
				'name' => 'title',
			),
			'description' => array(
				'name' => 'description',
			),
			'keywords' => array(
				'name' => 'keywords',
			),
			'header' => array(
				'name' => 'header',
			),
			'url' => array(
				'name' => 'url',
			),
			'route' => array(
				'name' => 'route',
			),
			'params' => array(
				'name' => 'params',
			),
			'refId' => array(
				'name' => 'refId',
            ),
            'lastmode' => array(
                'name' => 'lastmode',
            ),
            'sitemapPriority' => array(
                'name' => 'sitemapPriority',
			),
		);
		return $columns;
	}
}