<?php
/**
 * @var CWebApplication $app
 * File BlogForm.php
 */
Yii::import('ext.helpers.ARForm');
$app = Yii::app();
//Yii::app()->bootstrap->register();
$cs = $app->getClientScript();
$am = $app->getAssetManager();

$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/css/WhRedactor/whRedactor.css');


/**
 * Class Blog*/
class BlogForm extends TbForm
{
	/**
	 * Префикс в аттрибут name (например: "[]") для обработки полей формы с name=Blog[]['login']
	 * @var array
	 */
	public $elementGroupName = '';

	/**
	 * Инициализация формы
	 */
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();
		/** @var Blog $model */
		$model = $this->getModel();

        $this->elements = CMap::mergeArray(
            array_flip(array_keys($this->initElements())),
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );
		parent::init();
	}

	/**
	 * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=Blog[]['login']
	 * @param string $name
	 * @param TbFormInputElement $element
	 * @param bool $forButtons
	 */
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	 * Инициализация элементов формы
	 * @return array
	 */
	public function initElements()
	{
		$ctrl = $this->getOwner();

		$textInput = Yii::app()->controller->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
				'model' => $this->getModel(),
				'attribute' => 'text',
				'options' => array(
					'minHeight' => '250px',
					'imageUpload' => Yii::app()->createUrl('file/default/imageUpload', array('modelName' => get_class($this->getModel()))),
					'lang' => 'ru',
					'iframe' => true,
					'css' => 'wym.css',
				)
			), true
		);

		$seoDescriptionInput =
			'<div class="form-group field-blog-seodescription">
				<textarea id="blog-seodescription" class="b-form__text input-block-level" name="Blog[seoDescription]" row="10">' .
					$this->model->seoDescription .
				'</textarea>
				<div class="help-block"></div>
			</div>';

		$statisticValue = $this->model->statistics == 1 ? 'checked' : '';
		$statisticLabelText = Yii::t('blogModule.blog', 'add popular');
		$statisticsInput =
		"<div class=\"form-group field-blog-statistics\">
			<input type=\"hidden\" name=\"Blog[statistics]\" value=\"0\">
				<label>
					<input type=\"checkbox\" id=\"blog-statistics\" name=\"Blog[statistics]\" value=\"1\" $statisticValue >
					$statisticLabelText
				</label>
				<div class=\"help-block\"></div>
		</div>";

		return array(
			'id' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
				'visible' => false
			),
			'theme' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'text' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
				'input' => $textInput
			),
			'seoDescription' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
				'input' => $seoDescriptionInput
			),
            'authorId' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
                'items' => CHtml::listData(User::model()->findAllByAttributes(['role' => User::ROLE_EXPERT]), 'id', 'firstName'),
                'type' => TbHtml::INPUT_TYPE_DROPDOWNLIST,
                'visible' => true
            ),
			'status' => array(
				'groupOptions' => array(
                'class' => 'span12',
                ),
                'type' => TbHtml::INPUT_TYPE_DROPDOWNLIST,
                'items' => Blog::status(),
				'visible' => true
			),
            'created' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
                'value' => 'date("d.m.Y H:i:s",strtotime($data->created))',
                'visible' => true
            ),
            'tag' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
                'type' => TbHtml::INPUT_TYPE_TEXT,
                'visible' => true
            ),
            'statistics' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
                'input' => $statisticsInput
            ),
			'viewCounter' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
        );
	}
}