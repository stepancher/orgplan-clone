<?php

class RESelect2 extends ESelect2{

    public $script = '';

    public function init(){

        parent::init();
    }

    
    public function run()
    {
        if ($this->selector == null) {
            list($this->name, $this->id) = $this->resolveNameId();
            $this->selector = '#' . $this->id;

            if (isset($this->htmlOptions['placeholder']))
                $this->options['placeholder'] = $this->htmlOptions['placeholder'];

            if (!isset($this->htmlOptions['multiple'])) {
                $data = array();
                if (isset($this->options['placeholder']))
                    $data[''] = '';
                $this->data = $data + $this->data;
            }


            if ($this->hasModel())
            {
                if (isset($this->options['ajax']))
                    echo CHtml::activeHiddenField($this->model, $this->attribute, $this->htmlOptions);
                else
                    echo CHtml::activeDropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);

            }
            elseif (!isset($this->options['ajax']))
            {
                $this->htmlOptions['id'] = $this->id;
                echo CHtml::dropDownList($this->name, $this->value, $this->data, $this->htmlOptions);
            }
            else
            {
                echo CHtml::hiddenField($this->name, $this->value, $this->htmlOptions);
            }
        }

        $bu = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/');
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($bu . '/select2.css');

        if (YII_DEBUG)
            $cs->registerScriptFile($bu . '/select2.js');
        else
            $cs->registerScriptFile($bu . '/select2.min.js');

        if ($this->sortable) {
            $cs->registerCoreScript('jquery.ui');
        }

        $options = CJavaScript::encode(CMap::mergeArray($this->defaultOptions, $this->options));
        ob_start();
        echo "jQuery('{$this->selector}').select2({$options})";
        foreach ($this->events as $event => $handler)
            echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";
        echo ';';
        if ($this->sortable) {
            echo <<<JavaScript
jQuery('{$this->selector}').select2("container").find("ul.select2-choices").sortable({
	containment: 'parent',
	start: function() { jQuery('{$this->selector}').select2("onSortStart"); },
	update: function() { jQuery('{$this->selector}').select2("onSortEnd"); }
});
JavaScript;
        }

        echo "

            $('{$this->selector}').on('select2-selecting', {$this->script});
        
        ";

        $cs->registerScript(__CLASS__ . '#' . $this->id, ob_get_clean());

    }
}