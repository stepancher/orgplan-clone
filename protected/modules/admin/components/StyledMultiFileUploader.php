<?php
/**
 * File StyledMultiFileUploader.php
 */

/**
 * Class StyledMultiFileUploader
 */
class StyledMultiFileUploader extends CInputWidget
{
	/**
	 * @var null|string
	 */
	private $_list = null;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $buttonOptions;

	/**
	 * @var array|string
	 */
	public $input;

	/**
	 * @var string
	 */
	public $elementGroupName;

	/**
	 * @var string
	 */
	public $accept;

	/**
	 * @var string
	 */
	public $duplicate;

	/**
	 * @var string
	 */
	public $denied;

	/**
	 * @var integer
	 */
	public $max;

	/**
	 * @var string
	 */
	public $maxMessage;

	/**
	 * @var string
	 */
	public $deleteAction;

	/**
	 * @var string
	 */
	public $buttonTag = 'div';

	/**
	 * @var array
	 */
	public $fileData = array();

	/**
	 * @var array
	 */
	public $options = array();

	/**
	 * @var string
	 */
	public $template = '{button}{list}';

	/**
	 * @var bool
	 */
	public $outputAsImage = false;

	/**
	 * @var string
	 */
	public $removeButton = '
	<div data-uploaded="false" data-index="" class="styled-uploader__file-item">
		<span></span><a style="display:none;" data-icon="&#xe3c1" href=""></a>
	</div>
	';

	/**
	 * Инициализация
	 */
	public function init()
	{
		parent::init();

		if (is_bool($this->outputAsImage)) {
			$this->options['outputAsImage'] = $this->outputAsImage;
		}

		if (is_string($this->removeButton)) {
			$this->options['removeButton'] = $this->removeButton;
		}

		if (!isset($this->options['validations'])) {
			$this->options['validations'] = array();
		}

		if (!isset($this->options['messages'])) {
			$this->options['messages'] = array();
		}

		if (is_integer($this->max)) {
			if (!$this->maxMessage) {
				$this->maxMessage = Yii::t('AdminModule.admin', 'Limit of files is exceeded');
			}
			$this->options['validations']['max'] = $this->max;
			$this->options['messages']['max'] = $this->maxMessage;
		}

		if (!$this->duplicate) {
			$this->duplicate = Yii::t('AdminModule.admin', 'This image is already available');
		}
		$this->options['messages']['duplicate'] = $this->duplicate;

		if ($this->accept) {
			if (!$this->denied) {
				$this->denied = Yii::t('AdminModule.admin', 'Invalid file type');
			}

			$this->options['messages']['denied'] = $this->denied;
			$this->options['validations']['accept'] = $this->accept;
		}

		if (!$this->deleteAction) {
			$this->deleteAction = false;
		}

		$this->options['deleteAction'] = rawurldecode($this->deleteAction);

		if (!isset($this->options['listSelector'])) {
			$this->options['listSelector'] = '#' . $this->id . '-list-selector';
		}

		$listOptions = TbArray::popValue('listOptions', $this->htmlOptions, '');
		TbHtml::addCssClass('styled-uploader__file-list', $listOptions);
		if(!isset($listOptions['data-selector'])) {
			$listOptions['data-selector'] = $this->options['listSelector'];
		}
		if($this->outputAsImage) {
			TbHtml::addCssClass('styled-uploader__file-list--as-image', $listOptions);
		}

		$this->_list = TbHtml::tag('div', $listOptions, '');

		if (!isset($this->options['blockSelector'])) {
			$this->options['blockSelector'] = '#' . $this->id . '-multi-uploader';
		}

		if (!empty($this->fileData)) {
			$this->options['fileData'] = $this->fileData;
		}

		$this->registerClientScript();
	}

	/**
	 * Запуск отрисовки
	 */
	public function run()
	{
		$value = null;
		if ($this->hasModel()) {
			$attribute = $this->elementGroupName . $this->attribute;
			$value = $this->model->{$this->attribute};
			$name = TbHtml::resolveName($this->model, $attribute);
		} else {
			$name = $this->elementGroupName . $this->name;
		}
		$fileInput = TbHtml::fileField($name . '[]', '', array(
			'class' => 'styled-uploader__hidden-input styled-uploader__main-hidden-input'
		));
		if (empty($this->buttonOptions)) {
			$this->buttonOptions = array(
				'data-icon' => '',
                'class' => 'b-button b-button-icon b-input-icon d-bg-tooltip d-bg-info--hover'
			);
		}
		TbHtml::addCssClass('styled-uploader__styled-button', $this->buttonOptions);
		$content = TbArray::popValue('content', $this->buttonOptions, '');
		$styledButton = TbHtml::tag($this->buttonTag, $this->buttonOptions, $content);

		$inputList = TbHtml::tag('div', array('class' => 'styled-uploader__input-list'), $fileInput);

		$block = TbHtml::tag('div', array('class' => 'styled-uploader', 'data-selector' => $this->options['blockSelector']),
			strtr($this->template, array('{button}' => $styledButton, '{list}' => $this->_list)) . $inputList
		);

		if (is_array($this->input)) {
			$type = TbArray::popValue('type', $this->input, TbHtml::INPUT_TYPE_TEXT);
			$input = TbHtml::createInput($type, $name, $value, $this->input) . $block;
		} else {
			$input = $block;
		}

		echo $input;
	}

	private function registerClientScript()
	{
		/**
		 * @var CWebApplication $app
		 */
		$app = Yii::app();
		$am = $app->getAssetManager();
		$cs = $app->getClientScript();
		$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

		$options = CJavaScript::encode($this->options);

		$cs->registerCssFile($publishUrl . '/css/styled-multi-file-upload/style.css');
		$cs->registerScriptFile($publishUrl . '/js/styled-multi-file-upload/StyledMultiFile.js', CClientScript::POS_END);
		$cs->registerScript($this->id . '#StyledMultiFile', "new StyledMultiFile({$options});", CClientScript::POS_READY);
	}
}