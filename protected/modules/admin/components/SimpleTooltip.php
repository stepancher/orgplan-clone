<?php
/**
 * File SimpleTooltip.php
 */

/**
 * Class SimpleTooltip
 */
class SimpleTooltip extends CWidget
{
    /**
     * Tooltip items
     *    [
     *        ['content' => 'example', 'target' => 'attribute of input']
     *    ]
     *
     * @var array
     */
    public $items = [];

    /**
     * ['top' => n, 'right' => n, 'bottom' => n, 'left' => n]
     *
     * @var array
     */
    public $coordinates = [];

    /**
     * Фиксировать только при клике.
     * @var bool
     */
    public $holdOnClick = false;

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @var array
     */
    private $options = [];

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        if(empty($this->items) || !is_array($this->items)) {
            throw new Exception("Property of class Tooltip::items is empty");
        }

        if(empty($this->htmlOptions) || !is_array($this->htmlOptions)) {
            $this->htmlOptions = [];
        }

        if(empty($this->coordinates) || !is_array($this->coordinates)
            || (
                !isset($this->coordinates['top']) &&
                !isset($this->coordinates['right']) &&
                !isset($this->coordinates['bottom']) &&
                !isset($this->coordinates['left'])
            )
        ) {
            throw new Exception("Property of class Tooltip::coordinates should have at least one direction");
        }
        $this->options['coordinates'] = $this->coordinates;
        $this->options['holdOnClick'] = $this->holdOnClick;

        foreach ($this->items as $item) {
            $this->options['items'][] = $item['target'];
        }

        $this->registerClientScript();
    }

    /**
     * Запуск отрисовки
     */
    public function run()
    {
        $tooltips = '';
        foreach ($this->items as $item) {
            $tooltips .= TbHtml::tag('div', array(
                        'style' => 'position: absolute; display: none;',
                        'data-tooltip-target' => $item['target'],
                    ) + $this->htmlOptions, $item['content']) . PHP_EOL;
        }
        echo $tooltips;
    }

    private function registerClientScript()
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        $am = $app->getAssetManager();
        $cs = $app->getClientScript();
        $publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);

        $options = CJavaScript::encode($this->options);

        $cs->registerScriptFile($publishUrl . '/js/simple-tooltip/SimpleTooltip.js', CClientScript::POS_END);
        $cs->registerScript('#SimpleTooltip-' . $this->getId(), "new SimpleTooltip({$options});", CClientScript::POS_READY);
    }

}