<?php
/**
* File IndustryForm.php
*/
Yii::import('ext.helpers.ARForm');

/**
* Class Industry*/
class IndustryForm extends TbForm
{
	/**
	* Префикс в аттрибут name (например: "[]") для обработки полей формы с name=Industry[]['login']
	* @var array
	*/
	public $elementGroupName = '';
	/**
	* Инициализация формы
	*/
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();
		/** @var Industry $model */
		$model = $this->getModel();

		$this->elements = CMap::mergeArray(
			ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
			$this->initElements()
		);

		parent::init();
	}

	/**
	* Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=Industry[]['login']
	* @param string $name
	* @param TbFormInputElement $element
	* @param bool $forButtons
	*/
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	* Инициализация элементов формы
	* @return array
	*/
	public function initElements(){

		$model = $this->getModel();
		$ctrl = $this->getOwner();

		return array(
			'id' => array(
                'visible' => false,
            ),
			'massMedias' => array(
				'type' => TbHtml::INPUT_TYPE_TEXT,
				'input' => $ctrl->widget('vendor.anggiaj.eselect2.ESelect2', array(
					'model'=>$model,
					'attribute'=> 'massMedias',
					'data' => MassMedia::getMassMediaList(),
					'htmlOptions'=>array(
						'options' =>$model->getSelectedMassMedia(),
						'multiple'=>'multiple',
						'style' => 'width:30%;',
					)), true)
			),
			!empty($model->id)?TbHtml::linkButton(
				'Редактировать расположение СМИ',
				array(
					'url' => $ctrl->createUrl('manageSponsorPosition', array('id' => $model->id)),
					'style' => 'margin-left:250px;'
				)
			):'',
			'sponsorPos' => array(
				'visible'=>false,
			),
			'auctionId' => array(),
			'type' => array(),
			'<div style="clear:both;display:block;"></div>',
			TbHtml::linkButton(
				'Редактировать Аналитику по данной отрасли',
				array(
					'url' => $ctrl->createUrl('admin/chartTable/index', array('id' => $model->id)),
					'style' => 'margin:20px 0 0 250px;clear:both;'
				)
			),
//			'statTypes' => array(
//				'type' => TbHtml::INPUT_TYPE_TEXT,
//				'input' => $ctrl->widget('vendor.anggiaj.eselect2.ESelect2', array(
//					'model'=>$model,
//					'attribute'=> 'statTypes',
////					'data' => GStatTypes::getStatTypesList(),
//					'htmlOptions'=>array(
////						'options' =>$model->getSelectedStatTypes(),
//						'multiple'=>'multiple',
//						'style' => 'width:30%;',
//					)), true)
//			),
		);
	}
}