<?php

class ExhibitionComplexAdminService
{
    /**
     * @param $id
     * @return bool
     */
    public static function geoRelationValidate($id){

        if(empty($id)){
            return FALSE;
        }

        $params = array(
            ':id' => $id,
            ':langId' => Yii::app()->getLanguage(),
        );

        $query = "
            SELECT count(0) `count`                        
                 /*trcn.name AS countryName,                      
                    trd.name AS districtName,
                trr.name AS regionName,   
                trc.name AS cityName,
                trec.name AS exhibitionComplexName,    
                ec.coordinates*/               
            FROM tbl_country cn      
                LEFT JOIN tbl_trcountry trcn ON trcn.trParentId = cn.id AND trcn.langId = :langId                    
                LEFT JOIN tbl_district d ON d.countryId = cn.id                    
                LEFT JOIN tbl_trdistrict trd ON trd.trParentId = d.id AND trd.langId = :langId                    
                LEFT JOIN tbl_region r ON r.districtId = d.id                    
                LEFT JOIN tbl_trregion trr ON trr.trParentId = r.id AND trr.langId = :langId                    
                LEFT JOIN tbl_city c ON c.regionId = r.id                    
                LEFT JOIN tbl_trcity trc ON trc.trParentId = d.id AND trc.langId = :langId                    
                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id                    
                LEFT JOIN tbl_trexhibitioncomplex trec ON trec.trPArentId = ec.id AND trec.langId = :langId   
                               
            WHERE ec.id = :id        
        ";

        $geoNames = (int)Yii::app()->db->createCommand($query)->queryScalar($params);
        $r = true;

        if(!$geoNames){
            $r = FALSE;
        }

        return $r;
    }

    /**
     * @param array $fields
     * @param int $pageSize
     * @param ExhibitionComplex $model
     * @return CSqlDataProvider
     */
    public static function search($fields = [], $pageSize = 10, &$model) {

        $fields = array_keys($fields);

        $criteria = new CDbCriteria;
        $criteria->select = ['t.*'];
        $criteria->compare('t.id', $model->id);
        $sort = ['*'];
        $sort['id'] = [
            'asc'   => "t.id",
            'desc'  => "t.id DESC"
        ];

        foreach($model->description() as $key => $description) {

            if (!isset($description['relation']) && !in_array('pk', $description) && in_array($key, $fields)) {
                if ($key == 'city_name' || $key == 'region_name' || $key == 'district_name' || $key == 'country_name') {

                    if(!strpos($criteria->join, 'AS city ON')){
                        $criteria->join .= "
                            LEFT JOIN tbl_city AS city ON t.cityId = city.id
                            LEFT JOIN tbl_trcity AS city_name ON city_name.trParentId = city.id AND city_name.langId = :langId
                            LEFT JOIN tbl_region region ON city.regionId = region.id
                            LEFT JOIN tbl_trregion region_name ON region_name.trParentId = region.id AND region_name.langId = :langId
                            LEFT JOIN tbl_district district ON district.id = region.districtId
                            LEFT JOIN tbl_trdistrict district_name ON district_name.trParentId = district.id AND district_name.langId = :langId
                            LEFT JOIN tbl_trcountry country_name ON country_name.trParentId = district.countryId AND country_name.langId = :langId
                        ";
                    }

                    $criteria->select[] = "{$key}.name AS {$key}";

                    if($key == 'country_name'){
                        $additionalQuery = "SELECT trParentId FROM tbl_trcountry WHERE name = :countryName GROUP BY trParentId";
                        $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':countryName' => $model->{$key}));

                        if(!empty($data) && isset($data['trParentId'])){
                            $countryName = TrCountry::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                            if(isset($countryName->name) && $model->{$key} != $countryName->name)
                                $model->{$key} = $countryName->name;
                        }
                    } elseif ($key == 'region_name'){
                        $additionalQuery = "SELECT trParentId FROM tbl_trregion WHERE name = :regionName GROUP BY trParentId";
                        $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':regionName' => $model->{$key}));

                        if(!empty($data) && isset($data['trParentId'])){
                            $regionName = TrRegion::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                            if(isset($regionName->name) && $model->{$key} != $regionName->name)
                                $model->{$key} = $regionName->name;
                        }
                    } elseif ($key == 'district_name'){
                        $additionalQuery = "SELECT trParentId FROM tbl_trdistrict WHERE name = :districtName GROUP BY trParentId";
                        $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':districtName' => $model->{$key}));

                        if(!empty($data) && isset($data['trParentId'])){
                            $districtName = TrDistrict::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                            if(isset($districtName->name) && $model->{$key} != $districtName->name)
                                $model->{$key} = $districtName->name;
                        }
                    } elseif($key == 'city_name'){
                        $additionalQuery = "SELECT trParentId FROM tbl_trcity WHERE name = :cityName GROUP BY trParentId";
                        $data = Yii::app()->db->createCommand($additionalQuery)->queryRow(TRUE, array(':cityName' => $model->{$key}));

                        if(!empty($data) && isset($data['trParentId'])){
                            $cityName = TrCity::model()->findByAttributes(['trParentId' => $data['trParentId'], 'langId' => Yii::app()->language]);

                            if(isset($cityName->name) && $model->{$key} != $cityName->name)
                                $model->{$key} = $cityName->name;
                        }
                    }

                    $criteria->compare("{$key}.name", $model->{$key}, true);
                    $sort[$key] = [
                        'asc' => "`{$key}`.`name`",
                        'desc' => "`{$key}`.`name` DESC"
                    ];

                } elseif ($key == 'name' || $key == 'street' || $key == 'uniqueText' || $key == 'descriptionSnippet' || $key == 'shortUrl') {

                    $criteria->select[] = "name_translate.{$key} AS {$key}";

                    if(!strpos($criteria->join, 'AS name_translate ON')){

                        $criteria->join .= "
                            LEFT JOIN tbl_trexhibitioncomplex AS name_translate ON name_translate.trParentId = t.id 
                                AND name_translate.langId = :langId
                        ";
                    }

                    $criteria->compare("name_translate.{$key}", $model->{$key}, true);
                    $sort[$key] = [
                        'asc' => "`name_translate`.`{$key}`",
                        'desc' => "`name_translate`.`{$key}` DESC"
                    ];
                } elseif($key == 'fairsCount'){

                    $criteria->select[] = "COUNT(fair.id) AS {$key}";

                    if(!strpos($criteria->join, 'AS fair ON')){
                        $criteria->join .= "
                            LEFT JOIN tbl_fair AS fair ON fair.exhibitionComplexId = t.id
                        ";
                    }

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE){
                        $criteria->group .= ', t.id';
                    } elseif(empty($criteria->group)){
                        $criteria->group = 't.id';
                    }

                    if(!empty($model->{$key}) || $model->{$key} == '0'){
                        if(empty($criteria->having)){
                            $criteria->having = "COUNT(fair.id) = :fairsCount
                            ";
                        } else {
                            $criteria->having .= "AND COUNT(fair.id) = :fairsCount
                            ";
                        }
                        $criteria->params[':fairsCount'] = $model->{$key};
                    }

                    $sort[$key] = [
                        'asc' => "COUNT(fair.id)",
                        'desc' => "COUNT(fair.id) DESC"
                    ];
                } elseif($key == 'associationName'){
                    $criteria->select[] = "GROUP_CONCAT(DISTINCT association.name) AS {$key}";

                    if(!strpos($criteria->join, 'AS association ON')){
                        $criteria->join .= "
                            LEFT JOIN tbl_exhibitioncomplexhasassociation AS echa ON echa.exhibitionComplexId = t.id
                            LEFT JOIN tbl_association association ON echa.associationId = association.id
                        ";
                    }

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE){
                        $criteria->group .= ', t.id';
                    } elseif(empty($criteria->group)){
                        $criteria->group = 't.id';
                    }

                    if(!empty($model->{$key})){
                        if(empty($criteria->having)){
                            $criteria->having = "GROUP_CONCAT(DISTINCT association.name) LIKE :associationName
                            ";
                        } else {
                            $criteria->having .= "AND GROUP_CONCAT(DISTINCT association.name) LIKE :associationName
                            ";
                        }
                        $criteria->params[':associationName'] = '%' . $model->{$key} . '%';
                    }

                    $sort[$key] = [
                        'asc' => "GROUP_CONCAT(DISTINCT association.name)",
                        'desc' => "GROUP_CONCAT(DISTINCT association.name) DESC"
                    ];
                } elseif($key == 'statusName'){
                    $criteria->select[] = "status.name AS {$key}";

                    if(!strpos($criteria->join, 'AS status ON')){
                        $criteria->join .= "
                            LEFT JOIN tbl_exhibitioncomplexstatus AS status ON status.id = t.statusId
                        ";
                    }
                    $criteria->compare("status.name", $model->{$key});
                    $sort[$key] = [
                        'asc' => "status.name",
                        'desc' => "status.name DESC"
                    ];
                }  elseif($key == 'logoExists'){
                    $criteria->select[] = "CASE WHEN of.id IS NOT NULL THEN 1 ELSE 0 END AS {$key}";

                    $criteria->join .= "
                        LEFT JOIN tbl_exhibitioncomplexhasfile AS echf ON echf.exhibitionComplexId = t.id
                        LEFT JOIN tbl_objectfile AS of ON of.id = echf.fileId AND of.type = :fileType
                    ";
                    $criteria->params[':fileType'] = ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE;

                    if($model->{$key} == 2){
                        $criteria->addCondition("(of.id IS NOT NULL)");
                    }elseif($model->{$key} == 1){
                        $criteria->addCondition("(of.id IS NULL)");
                    }

                    $sort[$key] = [
                        'asc' => "logoExists",
                        'desc' => "logoExists DESC"
                    ];
                }  elseif($key == 'datetimeModified'){

                    $criteria->params[':datetimeModified'] = $model->{$key};
                    $criteria->compare('t.' . $key, $model->{$key}, TRUE);

                    $sort[$key] = [
                        'asc' => "datetimeModified",
                        'desc' => "datetimeModified DESC"
                    ];
                }   elseif($key == 'lastRedactorEmail'){

                    $criteria->select[] = "contact.email AS lastRedactorEmail";

                    $criteria->join .= "
                        LEFT JOIN tbl_user AS user ON user.id = t.userId
                        LEFT JOIN tbl_contact AS contact ON contact.id = user.contactId
                    ";

                    $criteria->compare( 'contact.email', $model->{$key}, TRUE);

                    $sort[$key] = [
                        'asc' => "contact.email",
                        'desc' => "contact.email DESC"
                    ];
                } else {

                    $sort[$key] = [
                        'asc' => "t.{$key}",
                        'desc' => "t.{$key} DESC"
                    ];
                    $criteria->compare('t.' . $key, $model->{$key}, in_array('string', $description));
                }
            }
        }

        if((bool)strpos($criteria->join, ':langId') && !empty($criteria->params)){
            $criteria->params += [
                ':langId' => Yii::app()->language
            ];
        } elseif ((bool)strpos($criteria->join, ':langId') && empty($criteria->params)) {
            $criteria->params = [
                ':langId' => Yii::app()->language
            ];
        }

        $query = ExhibitionComplex::model()->getCommandBuilder()->createFindCommand(ExhibitionComplex::model()->getTableSchema(),$criteria)->getText();
        $count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ({$query}) t")->queryScalar($criteria->params);

        return new CSqlDataProvider(
            $query,
            array(
                'totalItemCount' => $count,
                'sort' => array(
                    'attributes' => $sort,
                ),
                'pagination' => [
                    'pageSize' => $pageSize
                ],
                'params' => $criteria->params,
            )
        );
    }

    public static function getAssociationsByExhibitionComplexId($exhibitionComplexId){
    $query = "SELECT a.id, a.name FROM tbl_exhibitioncomplexhasassociation echa 
                  LEFT JOIN tbl_association a ON echa.associationId = a.id
                WHERE echa.exhibitionComplexId = :exhibitionComplexId
    ";
    $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':exhibitionComplexId' => $exhibitionComplexId));
    return $data;
}
}