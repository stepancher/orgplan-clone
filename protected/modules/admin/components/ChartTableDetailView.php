<?php
/**
 * File ChartTableDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class ChartTableDetailView
 *
 * @property ChartTable $data
 */
class ChartTableDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());

        parent::init();
	}
}