<?php

class OrganizerAdminService
{
    const COUNT_NULL = 0;

    /**
     * @param array $fields
     * @param int $pageSize
     * @param Organizer $model
     * @param integer $organizerId
     * @return CSqlDataProvider
     */
    public static function search($fields = [], $pageSize = 10, $model, $organizerId)
    {
        $selectPart = "SELECT o.id AS id
                              ,o.active AS active 
                              ,o.proposalEmail AS proposalEmail
	    ";
        $fromPart = "FROM tbl_organizer o
                     LEFT JOIN tbl_organizercompany oc ON o.companyId = oc.id
        ";
        $conditionPart = "WHERE oc.id = :organizerId
	    ";
        $groupPart = "
        ";
        $params = array(
            ':organizerId' => $organizerId,
        );
        $sort['id'] = array(
            'ASC' => 'o.id',
            'DESC' => 'o.id DESC',
        );

        if(isset($model->id) && $model->id !== ''){
            $conditionPart .= "AND o.id = {$model->id}";
        }

        preg_match_all('/,(\w+).(\w+)\sAS\s(\w+)/', $selectPart, $matches);

        if(isset($matches[0])){

            $selectPartFields = $matches[0];

            foreach ($selectPartFields as $key => $selectPartField){

                if(isset($matches[1][$key]) && isset($matches[2][$key]) && isset($matches[3][$key])){

                    $tbl = $matches[1][$key];
                    $column = $matches[2][$key];
                    $property = $matches[3][$key];

                    $sort[$property] = array(
                        'ASC'  => "{$tbl}.{$column}",
                        'DESC' => "{$tbl}.{$column} DESC",
                    );

                    if(isset($model->{$property}) && $model->{$property} !== ''){

                        $filteredValue = $model->{$property};
                        $conditionPart .= "AND {$tbl}.{$column} LIKE '%{$filteredValue}%'
                        ";
                    }
                }
            }
        }

        $contactListFields = array_intersect(OrganizerContactList::$types, $fields);

        if(count($contactListFields) != 0){

            $fromPart .= 'LEFT JOIN tbl_organizercontactlist ocl ON ocl.organizerId = o.id
            ';
            $groupPart .= "GROUP BY o.id
            ";

            foreach ($contactListFields as $key => $type){

                $groupExpr = "GROUP_CONCAT(CASE WHEN (ocl.type = '{$key}') THEN ocl.value END SEPARATOR ',')";
                $selectPart .= ",{$groupExpr} AS {$type}
	            ";
                $sort[$type] = array(
                    'ASC' => "{$groupExpr}",
                    'DESC' => "{$groupExpr} DESC",
                );

                if(isset($model->{$type}) && $model->{$type} !== ''){

                    $filteredValue = $model->{$type};

                    if(strpos($groupPart, 'HAVING') === FALSE){
                        $groupPart .= "HAVING {$groupExpr} LIKE '%{$filteredValue}%'
                        ";
                    } else {
                        $groupPart .= "AND {$groupExpr} LIKE '%{$filteredValue}%'
                        ";
                    }

                }
            }
        }

        $query = $selectPart .
            $fromPart .
            $conditionPart .
            $groupPart;

        $countQuery = "SELECT COUNT(*) AS count FROM ({$query}) t";
        $totalItemCount = Yii::app()->db->createCommand($countQuery)->query($params)->read();

        return new CSqlDataProvider($query, array(
            'params' => $params,
            'sort' => array(
                'attributes' => $sort,
            ),
            'totalItemCount' => isset($totalItemCount['count']) ? $totalItemCount['count'] : self::COUNT_NULL,
            'pagination' => array(
                'pageSize' => $pageSize,
            ),
        ));
    }

    /**
     * @param array $id
     * @param bool $onlyFirst
     * @return array|CDbDataReader|mixed
     */
    public static function loadModelWithContacts($id, $onlyFirst = FALSE){

        $criteria = new CDbCriteria;
        $criteria->select = "t.id AS id
                              ,troc.name AS organizerName 
                              ,oc.linkToTheSiteOrganizers AS linkToTheSiteOrganizers
                              ,t.active AS active 
                              ,t.companyId AS companyId 
        ";
        foreach (OrganizerContactList::$types as $key => $type){
            $criteria->select .= ",GROUP_CONCAT(CASE WHEN (ocl.type = '{$key}') THEN ocl.value END SEPARATOR ',') AS {$type}
            ";
        }
        $criteria->select .= ",t.proposalEmail AS proposalEmail
        ";
        $criteria->join = "LEFT JOIN tbl_organizercompany oc ON oc.id = t.companyId
                        LEFT JOIN tbl_trorganizercompany troc ON troc.trParentId = oc.id AND troc.langId = :langId
                        LEFT JOIN tbl_organizercontactlist ocl ON ocl.organizerId = t.id
        ";
        $criteria->addInCondition('t.id', $id);
        $criteria->params += array(
            ':langId' => Yii::app()->getLanguage(),
        );
        $criteria->group = "t.id";

        $query = Organizer::model()->getCommandBuilder()->createFindCommand(Organizer::model()->getTableSchema(),$criteria)->getText();
        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, $criteria->params);
        
        if($onlyFirst){
            return array_shift($data);
        }
        return $data;
    }
    
    public static function contactRedactorData($id){

        $query = "SELECT ocl.id, ocl.type, ocl.countryId, ocl.value AS value, ocl.additionalContactInformation, ocl.organizerId
                FROM tbl_organizercontactlist ocl WHERE `organizerId` = :organizerId ORDER BY ocl.type
        ";
        return new CSqlDataProvider($query, array('params' => array(':organizerId' => $id),'pagination' => FALSE));
    }

    public static function contactTypeCountById($organizerId){
        $query = "SELECT COUNT(DISTINCT type) AS cnt FROM tbl_organizercontactlist ocl WHERE `organizerId` = :organizerId";
        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':organizerId' => $organizerId));
        $data = array_shift($data);
        return array_shift($data);
    }
}