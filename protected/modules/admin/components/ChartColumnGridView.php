<?php
/**
 * File ChartColumnGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');
Yii::import('ext.ZLineChart.ZLineChart');
Yii::import('application.modules.gradoteka.models.GStatTypes');

/**
 * Class ChartColumnGridView
 *
 * @property ChartColumn $model
 */
class ChartColumnGridView extends TbGridView
{
    /**
     * Модель ChartColumn::model() or new ChartColumn('scenario')
     * @var ChartColumn	 */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        $this->dataProvider->getPagination()->pageSize = 1000;

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns(){
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'statId' => array(
                'name' => 'statId',
                'value' => function($data){
                    $stat = GStatTypes::model()->findByAttributes(array('gId' => $data->statId));

                    if(!empty($stat)){
                        return $stat->name.'/'.$data->statId;
                    }

                    return $data->statId;
                },
            ),
            'header' => array(
                'name' => 'header',
            ),
            'widgetType' => array(
                'name' => 'widgetType',
                'value' => function($data){
                    $widgetTypes = ZLineChart::getwidgetTypes();

                    if(isset($widgetTypes[$data->widgetType]) && !empty($widgetTypes[$data->widgetType])){
                        return ZLineChart::getwidgetTypes()[$data->widgetType];
                    }

                    return '-';
                }
            ),
            'tableId' => array(
                'name' => 'tableId',
                'value' => function($data){
                    $table = ChartTable::model()->findByPk($data->tableId);

                    if(!empty($table)){
                        return $table->name;
                    }

                    return $data->tableId;
                }
            ),
            'color' => array(
                'name' => 'color',
            ),
            'position' => array(
                'name' => 'position',
            ),
            'related' => array(
                'name' => 'related',
            ),
        );
        return $columns;
    }
}