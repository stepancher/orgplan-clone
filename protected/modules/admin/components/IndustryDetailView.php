<?php
/**
 * File IndustryDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class IndustryDetailView
 *
 * @property Industry $data
 */
class IndustryDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());

        $this->attributes['id']['visible'] = false;
        $this->attributes['sponsorPos']['visible'] = false;


        parent::init();
	}
}