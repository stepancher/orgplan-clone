<?php

class FairAdminService
{
    const LANGUAGE_DE = 'de';
    const LANGUAGE_EN = 'en';
    const EMPTY_FILTER = "<''";
    const NOT_EMPTY_FILTER = ">''";

    public static $fairGeoInfoTables = array(
        'exhibitioncomplex' => 'exhibitioncomplex',
        'city'=>'city',
        'region'=>'region',
        'district'=>'district',
        'country'=>'country',
    );

    /**
     * @param array $fields
     * @param int $pageSize
     * @param null $sortColumn
     * @param Fair $model
     * @return CSqlDataProvider
     */
    public static function search($fields = [], $pageSize = 10, $sortColumn = NULL, $model)
    {
        $criteria = new CDbCriteria;
        $criteria->select = ['t.id','t.datetimeModified','fairTranslate.shortUrl AS fairShortUrl', 't.infoId', 'CASE WHEN t.logoId IS NOT NULL THEN 1 ELSE 0 END AS logoExists'];
        $criteria->join = "
            LEFT JOIN tbl_trfair AS fairTranslate ON fairTranslate.trParentId = t.id AND fairTranslate.langId = :langId
        ";
        $criteria->compare('t.id', $model->id);
        $sort = ['*'];
        $sort['id'] = [
            'asc'   => "t.id",
            'desc'  => "t.id DESC"
        ];

        $fairGeoInfoTables = self::$fairGeoInfoTables;

        foreach ($fields as $field){

            if(array_key_exists($field, $model->getAttributes())){

                if($field == 'beginDate'  && !empty($model->beginDateRange)) {

                    if(!array_search("t.beginDate", $criteria->select))
                        $criteria->select[] = "t.beginDate";

                    $dates = explode(' - ', $model->beginDateRange);

                    if($model->beginDateRange == self::EMPTY_FILTER)
                        $criteria->addCondition("(t.beginDate IS NULL OR BINARY t.beginDate = '' OR BINARY t.beginDate = '\n')");
                    elseif ($model->beginDateRange == self::NOT_EMPTY_FILTER)
                        $criteria->addCondition("(t.beginDate IS NOT NULL AND BINARY t.beginDate != '' AND BINARY t.beginDate != '\n')");
                    elseif (is_array($dates) && !empty($dates[0]) && !empty($dates[1]))
                        $criteria->addBetweenCondition('t.beginDate', $dates[0], $dates[1]);

                } elseif($field == 'endDate' && !empty($model->endDateRange)){

                    if(!array_search("t.endDate", $criteria->select))
                        $criteria->select[] = "t.endDate";

                    $dates = explode(' - ', $model->endDateRange);

                    if($model->endDateRange == self::EMPTY_FILTER)
                        $criteria->addCondition("(t.endDate IS NULL OR BINARY t.endDate = '' OR BINARY t.endDate = '\n')");
                    elseif ($model->endDateRange == self::NOT_EMPTY_FILTER)
                        $criteria->addCondition("(t.endDate IS NOT NULL AND t.endDate != '' AND BINARY t.endDate != '\n')");
                    elseif (is_array($dates) && !empty($dates[0]) && !empty($dates[1]))
                        $criteria->addBetweenCondition('t.endDate', $dates[0], $dates[1]);

                }  elseif($field === 'datetimeModified' && !empty($model->datetimeModifiedRange)){

                    if(!array_search("t.datetimeModified", $criteria->select))
                        $criteria->select[] = "t.datetimeModified";

                    $dates = explode(' - ', $model->datetimeModifiedRange);

                    if($model->datetimeModifiedRange == self::EMPTY_FILTER)
                        $criteria->addCondition("(t.datetimeModified IS NULL OR BINARY t.datetimeModified = '' OR BINARY t.datetimeModified = '\n')");
                    elseif ($model->datetimeModifiedRange == self::NOT_EMPTY_FILTER)
                        $criteria->addCondition("(t.datetimeModified IS NOT NULL AND BINARY t.datetimeModified != '' AND BINARY t.datetimeModified != '\n')");
                    elseif (is_array($dates) && !empty($dates[0]) && !empty($dates[1]))
                        $criteria->addBetweenCondition('t.datetimeModified', $dates[0], $dates[1]);

                } else {

                    if(!array_search("t.$field", $criteria->select))
                        $criteria->select[] = "t.$field";

                    if($model->{$field} == self::EMPTY_FILTER)
                        $criteria->addCondition("(t.$field IS NULL OR BINARY t.$field = '' OR BINARY t.$field = '\n')");
                    elseif ($model->{$field} == self::NOT_EMPTY_FILTER)
                        $criteria->addCondition("(t.$field IS NOT NULL AND BINARY t.$field != '' AND BINARY t.$field != '\n')");
                    elseif(isset($model->description()[$field][0]) && $model->description()[$field][0] == 'integer')
                        $criteria->compare("t.$field", $model->{$field});
                    elseif(isset($model->description()[$field][0]) && $model->description()[$field][0] != 'integer')
                        $criteria->compare("t.$field", $model->{$field}, TRUE);

                }

                $sort[$field] = [
                    'asc'   => "t.{$field}",
                    'desc'  => "t.{$field} DESC"
                ];
            } elseif($field === 'fairStatus'){

                $criteria->select[] = 'status.status AS fairStatus';
                $criteria->select[] = 'status.color AS fairStatusColor';

                if(!strpos($criteria->join, 'AS status ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairstatus AS status ON status.id = t.statusId
                    ";

                if($model->fairStatus == self::EMPTY_FILTER)
                    $criteria->addCondition("(t.statusId IS NULL OR BINARY t.statusId = '' OR BINARY t.statusId = '\n')");
                elseif ($model->fairStatus == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(t.statusId IS NOT NULL AND BINARY t.statusId != '' AND BINARY t.statusId != '\n')");
                elseif(!empty($model->fairStatus))
                    $criteria->addCondition("t.statusId = '$model->fairStatus'");
                elseif ($model->fairStatus == '0')
                    $criteria->addCondition('t.statusId = 0');

                $sort['fairStatus'] = [
                    'asc'   => '`status`.`status`',
                    'desc'  => '`status`.`status` DESC'
                ];

            } elseif($field === 'fairDiscription') {

                $criteria->select[] = 'fairTranslate.description AS fairDiscription';

                if(!strpos($criteria->join, 'AS fairTranslate ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_trfair AS fairTranslate ON fairTranslate.trParentId = t.id AND fairTranslate.langId = :langId
                    ";

                if($model->fairDiscription == self::EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.description IS NULL OR BINARY fairTranslate.description = '' OR BINARY fairTranslate.description = '\n')");
                elseif ($model->fairDiscription == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.description IS NOT NULL AND BINARY fairTranslate.description != '' AND BINARY fairTranslate.description != '\n')");
                elseif(!empty($model->fairDiscription))
                    $criteria->compare('fairTranslate.description', $model->fairDiscription, TRUE);

                $sort['fairDiscription'] = [
                    'asc'   => '`fairTranslate`.`description`',
                    'desc'  => '`fairTranslate`.`description` DESC'
                ];

            } elseif($field === 'fairUniqueText') {

                $criteria->select[] = 'fairTranslate.uniqueText AS fairUniqueText';

                if(!strpos($criteria->join, 'AS fairTranslate ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_trfair AS fairTranslate ON fairTranslate.trParentId = t.id AND fairTranslate.langId = :langId
                    ";

                if($model->fairUniqueText == self::EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.uniqueText IS NULL OR BINARY fairTranslate.uniqueText = '' OR BINARY fairTranslate.uniqueText = '\n')");
                elseif ($model->fairUniqueText == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.uniqueText IS NOT NULL AND BINARY fairTranslate.uniqueText != '' AND BINARY fairTranslate.uniqueText != '\n')");
                elseif(!empty($model->fairUniqueText))
                    $criteria->compare('fairTranslate.uniqueText', $model->fairUniqueText, TRUE);
                
                $sort['fairUniqueText'] = [
                    'asc'   => '`fairTranslate`.`uniqueText`',
                    'desc'  => '`fairTranslate`.`uniqueText` DESC'
                ];

            } elseif ($field === 'statistics'){
                
                $criteria->select[] = 'fairTranslate.statistics AS statistics';
                
                if(!strpos($criteria->join, 'AS fairTranslate ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_trfair AS fairTranslate ON fairTranslate.trParentId = t.id AND fairTranslate.langId = :langId
                    ";

                if($model->statistics == self::EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.statistics IS NULL OR BINARY fairTranslate.statistics = '' OR BINARY fairTranslate.statistics = '\n')");
                elseif ($model->statistics == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.statistics IS NOT NULL AND BINARY fairTranslate.statistics != '' AND BINARY fairTranslate.statistics != '\n')");
                elseif(!empty($model->statistics))
                    $criteria->compare('fairTranslate.statistics', $model->statistics, TRUE);

                $sort['statistics'] = [
                    'asc'   => '`fairTranslate`.`statistics`',
                    'desc'  => '`fairTranslate`.`statistics` DESC'
                ];
                
            } elseif ($field === 'userRedactorEmail'){

                $criteria->select[] = 'contact.email AS userRedactorEmail';

                if(!strpos($criteria->join, 'AS user ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_user AS user ON t.userRedactorId = user.id
                        LEFT JOIN tbl_contact contact ON user.contactId = contact.id
                    ";

                if($model->userRedactorEmail == self::EMPTY_FILTER)
                    $criteria->addCondition("(contact.email IS NULL OR BINARY contact.email = '' OR BINARY contact.email = '\n')");
                elseif ($model->userRedactorEmail == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(contact.email IS NOT NULL AND BINARY contact.email != '' AND BINARY contact.email != '\n')");
                elseif(!empty($model->userRedactorEmail))
                    $criteria->compare('contact.email', $model->userRedactorEmail, TRUE);

                $sort['userRedactorEmail'] = [
                    'asc'   => '`contact`.`email`',
                    'desc'  => '`contact`.`email` DESC'
                ];

            } elseif($field === 'fairTranslateName') {

                $criteria->select[] = 'fairTranslate.name AS fairTranslateName';

                if(!strpos($criteria->join, 'AS fairTranslate ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_trfair AS fairTranslate ON fairTranslate.trParentId = t.id AND fairTranslate.langId = :langId
                    ";

                if($model->fairTranslateName == self::EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.name IS NULL OR BINARY fairTranslate.name = '' OR BINARY fairTranslate.name = '\n')");
                elseif ($model->fairTranslateName == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(fairTranslate.name IS NOT NULL AND BINARY fairTranslate.name != '' AND BINARY fairTranslate.name != '\n')");
                elseif(!empty($model->fairTranslateName))
                    $criteria->compare('fairTranslate.name', $model->fairTranslateName, TRUE);

                $sort['fairTranslateName'] = [
                    'asc'   => '`fairTranslate`.`name`',
                    'desc'  => '`fairTranslate`.`name` DESC'
                ];

            } elseif($field === 'duration') {

                if(!array_search('t.beginDate', $criteria->select))
                    $criteria->select[] = 't.beginDate';

                if(!array_search('t.endDate', $criteria->select))
                    $criteria->select[] = 't.endDate';

                $criteria->select[] = 'CASE WHEN TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) IS NOT NULL AND TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) > 0
                                            THEN TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) + 1 
                                            ELSE NULL END duration';

                if (!empty($model->duration))
                    $criteria->compare('CASE WHEN TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) IS NOT NULL AND TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) > 0
                                             THEN TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) + 1 
                                             ELSE NULL END', $model->duration);

                $sort['duration'] = [
                    'asc' => '`duration`',
                    'desc' => '`duration` DESC'
                ];

            } elseif(array_key_exists($field, FairInfo::model()->getAttributes())) {

                $attribute = $field;

                if($field == 'forumId')
                    $criteria->select[] = 'trForumName.name AS forumId';
                else
                    $criteria->select[] = 'fairInfo.' . $attribute . ' AS ' . $attribute;

                if(!strpos($criteria->join, 'AS fairInfo ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairinfo AS fairInfo ON fairInfo.id = t.infoId
                    ";

                if(!strpos($criteria->join, 'AS trForumName ON') && $field == 'forumId'){

                    $criteria->join .= "
                        LEFT JOIN tbl_fair AS forum ON forum.id = fairInfo.forumId
                        LEFT JOIN tbl_trfair AS trForumName ON trForumName.trParentId = forum.id AND trForumName.langId = :langId
                    ";

                    if($model->{$field} == self::EMPTY_FILTER)
                        $criteria->addCondition("(trForumName.name IS NULL OR BINARY trForumName.name = '' OR BINARY trForumName.name = '\n')");
                    elseif ($model->{$field} == self::NOT_EMPTY_FILTER)
                        $criteria->addCondition("(trForumName.name IS NOT NULL AND BINARY trForumName.name != '' AND BINARY trForumName.name != '\n')");
                    elseif (!empty($model->{$field}))
                        $criteria->compare('trForumName.name', $model->{$field}, TRUE);
                } else {

                    if($model->{$field} == self::EMPTY_FILTER)
                        $criteria->addCondition("(fairInfo.{$attribute} IS NULL OR BINARY fairInfo.{$attribute} = '' OR BINARY fairInfo.{$attribute} = '\n')");
                    elseif ($model->{$field} == self::NOT_EMPTY_FILTER)
                        $criteria->addCondition("(fairInfo.{$attribute} IS NOT NULL AND BINARY fairInfo.{$attribute} != '' AND BINARY fairInfo.{$attribute} != '\n')");
                    elseif (!empty($model->{$field}))
                        $criteria->compare('fairInfo.' . $attribute, $model->{$field}, TRUE);
                }

                $sort[$field] = [
                    'asc'   => "`fairInfo`.`{$attribute}`",
                    'desc'  => "`fairInfo`.`{$attribute}` DESC"
                ];

            } elseif(strpos($field, 'organizer') !== FALSE) {

                $attribute = lcfirst(str_replace('organizer','',$field));

                if(!strpos($criteria->join, 'AS organizer ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairhasorganizer fho ON fho.fairId = t.id
                        LEFT JOIN tbl_organizer AS organizer ON organizer.id = fho.organizerId
                    ";

                if($attribute == 'name'){

                    $criteria->select[] = 'GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR \', \')' . ' AS ' . 'organizerName';

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                        $criteria->group .= ', t.id';
                     elseif(empty($criteria->group))
                        $criteria->group = 't.id';

                    if(!strpos($criteria->join, 'AS trOrganizerCompany ON'))
                        $criteria->join .= "
                            LEFT JOIN tbl_trorganizercompany AS trOrganizerCompany ON trOrganizerCompany.trParentId = organizer.companyId AND trOrganizerCompany.langId = :langId
                        ";

                    if($model->{$field} == self::EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') = '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') = '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif ($model->{$field} == self::NOT_EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') != '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') != '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif (!empty($model->{$field})){

                        if(empty($criteria->having))
                            $criteria->having = "GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') LIKE :organizerCompanyName
                            ";
                         else
                            $criteria->having .= "AND GROUP_CONCAT(DISTINCT trOrganizerCompany.name SEPARATOR ', ') LIKE :organizerCompanyName
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';
                    }

                    $sort[$field] = [
                        'asc'   => "`trOrganizerCompany`.`name`",
                        'desc'  => "`trOrganizerCompany`.`name` DESC"
                    ];
                } elseif(($organizerContactType = array_search($attribute, OrganizerContactList::$types)) !== FALSE) {

                    $criteria->select[] = 'GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR \', \')' . ' AS ' . 'organizer' . ucfirst($attribute);

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                        $criteria->group .= ', t.id';
                     elseif(empty($criteria->group))
                        $criteria->group = 't.id';

                    if(!strpos($criteria->join, 'AS orgContList ON'))
                        $criteria->join .= "
                            LEFT JOIN tbl_organizercontactlist AS orgContList ON orgContList.organizerId = organizer.id AND orgContList.type = :contactType
                        ";

                    if($model->{$field} == self::EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') = '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') = '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif ($model->{$field} == self::NOT_EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') != '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') != '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif(!empty($model->{$field})){

                        if(empty($criteria->having))
                            $criteria->having = "GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') LIKE :organizerContactValue
                            ";
                         else 
                            $criteria->having .= "AND GROUP_CONCAT(DISTINCT orgContList.value SEPARATOR ', ') LIKE :organizerContactValue
                            ";
                        
                        $criteria->params[':organizerContactValue'] = '%' . $model->{$field} . '%';
                    }

                    $criteria->params[':contactType'] = $organizerContactType;

                    $sort[$field] = [
                        'asc'   => "`orgContList`.`value`",
                        'desc'  => "`orgContList`.`value` DESC"
                    ];

                } elseif ($attribute == 'companyId') {

                    $criteria->select[] = 'GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR \', \')' . ' AS ' . 'organizerCompanyId';

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                        $criteria->group .= ', t.id';
                     elseif(empty($criteria->group))
                        $criteria->group = 't.id';

                    if($model->{$field} == self::EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') = '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') = '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif ($model->{$field} == self::NOT_EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') != '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') != '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif(!empty($model->{$field})){
                        
                        if(empty($criteria->having))
                            $criteria->having = "GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') LIKE :organizerCompanyId
                            ";
                         else 
                            $criteria->having .= "AND GROUP_CONCAT(DISTINCT organizer.companyId SEPARATOR ', ') LIKE :organizerCompanyId
                            ";
                        
                        $criteria->params[':organizerCompanyId'] = '%' . $model->{$field} . '%';
                    }

                    $sort[$field] = [
                        'asc'   => "`organizer`.`companyId`",
                        'desc'  => "`organizer`.`companyId` DESC"
                    ];

                } elseif($attribute == 'id') {

                    $criteria->select[] = 'GROUP_CONCAT(DISTINCT organizer.id SEPARATOR \', \')' . ' AS ' . 'organizerId';

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                        $criteria->group .= ', t.id';
                     elseif(empty($criteria->group))
                        $criteria->group = 't.id';

                    if($model->{$field} == self::EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') = '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') IS NULL OR BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') = '' OR BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') = '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif ($model->{$field} == self::NOT_EMPTY_FILTER){

                        if(empty($criteria->having))
                            $criteria->having = "(GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') != '\n')
                            ";
                        else
                            $criteria->having .= "AND (GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ')  IS NOT NULL AND BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') != '' AND BINARY GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') != '\n')
                            ";

                        $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                    } elseif(!empty($model->{$field})){
                        
                        if(empty($criteria->having))
                            $criteria->having = "GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') LIKE :organizerId
                            ";
                        else
                            $criteria->having .= "AND GROUP_CONCAT(DISTINCT organizer.id SEPARATOR ', ') LIKE :organizerId
                            ";
                        
                        $criteria->params[':organizerId'] = '%' . $model->{$field} . '%';
                    }

                    $sort[$field] = [
                        'asc'   => "`organizer`.`id`",
                        'desc'  => "`organizer`.`id` DESC"
                    ];

                } elseif ($attribute == 'companyCount'){

                    $criteria->select[] = 'COUNT(DISTINCT organizer.companyId)' . ' AS ' . 'organizerCompanyCount';

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                        $criteria->group .= ', t.id';
                     elseif(empty($criteria->group))
                        $criteria->group = 't.id';

                    if(!empty($model->{$field})){

                        if (substr($model->{$field},0,2) == '<=')
                            $sign = '<=';
                        elseif (substr($model->{$field},0,2) == '>=')
                            $sign = '>=';
                        elseif (substr($model->{$field},0,2) == '<>')
                            $sign = '<>';
                        elseif(substr($model->{$field},0,1) == '<')
                            $sign = '<';
                        elseif (substr($model->{$field},0,1) == '>')
                            $sign = '>';
                        else
                            $sign = '=';

                        $abs = str_replace($sign, '', $model->{$field});

                        if(empty($criteria->having))
                            $criteria->having = "COUNT(DISTINCT organizer.companyId) {$sign} :companyCount
                            ";
                        else
                            $criteria->having .= "AND COUNT(DISTINCT organizer.companyId) {$sign} :companyCount
                            ";

                        $criteria->params[':companyCount'] = $abs;

                    } elseif($model->{$field} === '0'){

                        if(empty($criteria->having))
                            $criteria->having = "COUNT(DISTINCT organizer.companyId) = :companyCount
                            ";
                        else
                            $criteria->having .= "AND COUNT(DISTINCT organizer.companyId) = :companyCount
                            ";

                        $criteria->params[':companyCount'] = $model->{$field};
                    }

                    $sort[$field] = [
                        'asc'   => "COUNT(DISTINCT organizer.companyId)",
                        'desc'  => "COUNT(DISTINCT organizer.companyId) DESC"
                    ];

                } elseif ($attribute == 'contactCount'){

                    $criteria->select[] = 'COUNT(DISTINCT organizer.id)' . ' AS ' . 'organizerContactCount';

                    if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                        $criteria->group .= ', t.id';
                     elseif(empty($criteria->group))
                        $criteria->group = 't.id';

                    if(!empty($model->{$field})){

                        if (substr($model->{$field},0,2) == '<=')
                            $sign = '<=';
                        elseif (substr($model->{$field},0,2) == '>=')
                            $sign = '>=';
                        elseif (substr($model->{$field},0,2) == '<>')
                            $sign = '<>';
                        elseif(substr($model->{$field},0,1) == '<')
                            $sign = '<';
                        elseif (substr($model->{$field},0,1) == '>')
                            $sign = '>';
                        else
                            $sign = '=';

                        $abs = str_replace($sign, '', $model->{$field});

                        if(empty($criteria->having))
                            $criteria->having = "COUNT(DISTINCT organizer.id) {$sign} :contactCount
                            ";
                         else
                            $criteria->having .= "AND COUNT(DISTINCT organizer.id) {$sign} :contactCount
                            ";

                        $criteria->params[':contactCount'] = $abs;
                    } elseif($model->{$field} === '0') {

                        if(empty($criteria->having))
                            $criteria->having = "COUNT(DISTINCT organizer.id) = :contactCount
                            ";
                        else
                            $criteria->having .= "AND COUNT(DISTINCT organizer.id) = :contactCount
                            ";

                        $criteria->params[':contactCount'] = $model->{$field};
                    }

                    $sort[$field] = [
                        'asc'   => "COUNT(DISTINCT organizer.id)",
                        'desc'  => "COUNT(DISTINCT organizer.id) DESC"
                    ];
                }

            } elseif(strpos($field, 'exhibitionComplex') !== FALSE
                || strpos($field, 'city') !== FALSE
                || strpos($field, 'region') !== FALSE
                || strpos($field, 'district') !== FALSE
                || strpos($field, 'country') !== FALSE
            ) {

                if(strpos($field, 'exhibitionComplex') !== FALSE){
                    $attribute = lcfirst(str_replace('exhibitionComplex','',$field));
                    $modelName = 'exhibitionComplex';
                    $ModelName = ucfirst($modelName);
                } elseif(strpos($field, 'city') !== FALSE){
                    $attribute = lcfirst(str_replace('city','',$field));
                    $modelName = 'city';
                    $ModelName = ucfirst($modelName);
                } elseif(strpos($field, 'region') !== FALSE){
                    $attribute = lcfirst(str_replace('region','',$field));
                    $modelName = 'region';
                    $ModelName = ucfirst($modelName);
                } elseif(strpos($field, 'district') !== FALSE){
                    $attribute = lcfirst(str_replace('district','',$field));
                    $modelName = 'district';
                    $ModelName = ucfirst($modelName);
                } elseif(strpos($field, 'country') !== FALSE){
                    $attribute = lcfirst(str_replace('country','',$field));
                    $modelName = 'country';
                    $ModelName = ucfirst($modelName);
                }

                if($attribute == 'name')
                    $criteria->select[] = $modelName . 'Name.' . $attribute . ' AS ' . $modelName . ucfirst($attribute);
                 else
                    $criteria->select[] = $modelName . $attribute . ' AS ' . $modelName . ucfirst($attribute);

                if(in_array(strtolower($ModelName), $fairGeoInfoTables) &&
                    !strpos($criteria->join, 'AS exhibitionComplex ON'))
                {
                    $criteria->join .= "
                        LEFT JOIN tbl_exhibitioncomplex AS exhibitionComplex ON exhibitionComplex.id = t.exhibitionComplexId
                        LEFT JOIN tbl_trexhibitioncomplex AS exhibitionComplexName ON exhibitionComplexName.trParentId = exhibitionComplex.id AND exhibitionComplexName.langId = :langId
                    ";
                    unset($fairGeoInfoTables['exhibitioncomplex']);
                }

                if(in_array(strtolower($ModelName), $fairGeoInfoTables) &&
                    !strpos($criteria->join, 'AS city ON'))
                {
                    $criteria->join .= "
                        LEFT JOIN tbl_city AS city ON city.id = exhibitionComplex.cityId
                        LEFT JOIN tbl_trcity AS cityName ON cityName.trParentId = city.id AND cityName.langId = :langId
                    ";
                    unset($fairGeoInfoTables['city']);
                }

                if(in_array(strtolower($ModelName), $fairGeoInfoTables) &&
                    !strpos($criteria->join, 'AS region ON'))
                {
                    $criteria->join .= "
                        LEFT JOIN tbl_region AS region ON region.id = city.regionId
                        LEFT JOIN tbl_trregion AS regionName ON regionName.trParentId = region.id AND regionName.langId = :langId
                    ";
                    unset($fairGeoInfoTables['region']);
                }

                if(in_array(strtolower($ModelName), $fairGeoInfoTables) &&
                    !strpos($criteria->join, 'AS district ON'))
                {
                    $criteria->join .= "
                        LEFT JOIN tbl_district AS district ON district.id = region.districtId
                        LEFT JOIN tbl_trdistrict AS districtName ON districtName.trParentId = district.id AND districtName.langId = :langId
                    ";
                    unset($fairGeoInfoTables['district']);
                }

                if(in_array(strtolower($ModelName), $fairGeoInfoTables) &&
                    !strpos($criteria->join, 'AS country ON'))
                {
                    $criteria->join .= "
                        LEFT JOIN tbl_country AS country ON country.id = district.countryId
                        LEFT JOIN tbl_trcountry AS countryName ON countryName.trParentId = country.id AND countryName.langId = :langId
                    ";
                    unset($fairGeoInfoTables['country']);
                }

                if($attribute == 'name')
                    $criteria->compare($modelName.'Name.'.$attribute, $model->{$field}, TRUE);
                 elseif($attribute !== 'name')
                    $criteria->compare($modelName.'.'.$attribute, $model->{$field}, TRUE);

                if($attribute == 'name')
                    $sort[$field] = [
                        'asc'   => "`$modelName" . "Name`.`{$attribute}`",
                        'desc'  => "`$modelName" . "Name`.`{$attribute}` DESC"
                    ];
                 else
                    $sort[$field] = [
                        'asc'   => "`$modelName`.`{$attribute}`",
                        'desc'  => "`$modelName`.`{$attribute}` DESC"
                    ];

            } elseif(strpos($field, 'industry') !== FALSE &&
                (array_key_exists(lcfirst(str_replace('industry','',$field)), Industry::model()->getAttributes())
                    || array_key_exists(lcfirst(str_replace('industry','',$field)), TrIndustry::model()->getAttributes()))
            ) {

                $attribute = lcfirst(str_replace('industry','',$field));

                $criteria->select[] = 'GROUP_CONCAT(DISTINCT industryName.name ORDER BY industryName.name ASC SEPARATOR "; ") AS industryName';

                if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                    $criteria->group .= ', t.id';
                 elseif(empty($criteria->group))
                    $criteria->group = 't.id';

                if(!strpos($criteria->join, 'AS fairHasIndustries ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairhasindustry AS fairHasIndustries ON fairHasIndustries.fairId = t.id
                        LEFT JOIN tbl_industry AS industry ON industry.id = fairHasIndustries.industryId
                        LEFT JOIN tbl_trindustry AS industryName ON industryName.trParentId = industry.id AND industryName.langId = :langId
                    ";


                if(!empty($model->{$field}))
                    if(empty($criteria->having))
                        $criteria->having = "GROUP_CONCAT(DISTINCT industryName.name ORDER BY industryName.name ASC SEPARATOR \"; \") LIKE '%{$model->{$field}}%'
                        ";
                     else
                        $criteria->having .= "AND GROUP_CONCAT(DISTINCT industryName.name ORDER BY industryName.name ASC SEPARATOR \"; \") LIKE '%{$model->{$field}}%'
                        ";

                if($attribute == 'name')
                    $sort[$field] = [
                        'asc' => '`industryName`.`name`',
                        'desc' => '`industryName`.`name` DESC'
                    ];
                 else
                    $sort[$field] = [
                        'asc' => "`industry`.`{$attribute}`",
                        'desc' => "`industry`.`{$attribute}` DESC"
                    ];

            } elseif(strpos($field, 'association') !== FALSE && array_key_exists(lcfirst(str_replace('association','',$field)), Association::model()->getAttributes())){

                $attribute = lcfirst(str_replace('association','',$field));

                $criteria->select[] = "GROUP_CONCAT(DISTINCT association.$attribute ORDER BY association.$attribute ASC SEPARATOR '; ') AS $field";

                if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                    $criteria->group .= ', t.id';
                 elseif(empty($criteria->group))
                    $criteria->group = 't.id';

                if(!strpos($criteria->join, 'AS fairHasAssociations ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairhasassociation AS fairHasAssociations ON fairHasAssociations.fairId = t.id
                        LEFT JOIN tbl_association AS association ON association.id = fairHasAssociations.associationId
                    ";


                $criteria->compare('association.'.$attribute, $model->{$field}, TRUE);

                $sort[$field] = [
                    'asc' => "`association`.`{$attribute}`",
                    'desc' => "`association`.`{$attribute}` DESC"
                ];

            } elseif(strpos($field, 'audit') !== FALSE){

                $criteria->select[] = "GROUP_CONCAT(DISTINCT traudit.name) AS auditName";

                if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                    $criteria->group .= ', t.id';
                 elseif(empty($criteria->group))
                    $criteria->group = 't.id';

                if(!strpos($criteria->join, 'AS fairHasAudit ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairhasaudit AS fairHasAudit ON fairHasAudit.fairId = t.id
                        LEFT JOIN tbl_audit AS audit ON audit.id = fairHasAudit.auditId
                        LEFT JOIN tbl_traudit AS traudit ON traudit.trParentId = audit.id AND traudit.langId = :langId
                    ";

                if(!empty($model->{$field}))
                    if(empty($criteria->having))
                        $criteria->having = "GROUP_CONCAT(DISTINCT traudit.name) LIKE '%{$model->{$field}}%'
                        ";
                     else
                        $criteria->having .= "AND GROUP_CONCAT(DISTINCT traudit.name) LIKE '%{$model->{$field}}%'
                        ";

                $sort[$field] = [
                    'asc' => "GROUP_CONCAT(DISTINCT traudit.name)",
                    'desc' => "GROUP_CONCAT(DISTINCT traudit.name) DESC"
                ];

            } elseif(strpos($field, 'fairIsForum') !== FALSE){

                $criteria->select[] = "CASE WHEN (fairInfo.forumId = t.id) THEN 1 ELSE 0 END AS fairIsForum";

                if(!strpos($criteria->join, 'AS fairInfo ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairinfo AS fairInfo ON fairInfo.id = t.infoId
                    ";

                $criteria->compare('CASE WHEN (fairInfo.forumId = t.id) THEN 1 ELSE 0 END', $model->{$field});

                $sort[$field] = [
                    'asc' => "CASE WHEN (fairInfo.forumId = t.id) THEN 1 ELSE 0 END",
                    'desc' => "CASE WHEN (fairInfo.forumId = t.id) THEN 1 ELSE 0 END DESC"
                ];
            }
            elseif(strpos($field, 'commonStatisticFair') !== FALSE){

                $criteria->select[] = "GROUP_CONCAT(DISTINCT fair.id, '::', trCommonStatisticFair.name, '::', YEAR(fair.beginDate) SEPARATOR ';') AS commonStatisticFair";
                $criteria->select[] = "GROUP_CONCAT(DISTINCT fair.id, '::', commonStatisticFairAssociation.name, '::', fair.rating SEPARATOR ';') AS commonStatisticFairAssociation";

                if(!strpos($criteria->join, 'AS fairInfo ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fairinfo AS fairInfo ON fairInfo.id = t.infoId
                    ";

                if(!strpos($criteria->join, 'AS trCommonStatisticFair ON'))
                    $criteria->join .= "
                        LEFT JOIN tbl_fair AS fair ON fair.infoId = fairInfo.id
                        LEFT JOIN tbl_trfair AS trCommonStatisticFair ON trCommonStatisticFair.trParentId = fair.id AND trCommonStatisticFair.langId = :langId
                        LEFT JOIN tbl_fairhasassociation AS commonStatisticFHA ON commonStatisticFHA.fairId = fair.id
                        LEFT JOIN tbl_association AS commonStatisticFairAssociation ON commonStatisticFairAssociation.id = commonStatisticFHA.associationId
                    ";

                if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                    $criteria->group .= ', t.id';
                 elseif(empty($criteria->group))
                    $criteria->group = 't.id';

                if($model->{$field} == self::EMPTY_FILTER){

                    if(empty($criteria->having))
                        $criteria->having = "(GROUP_CONCAT(trCommonStatisticFair.name) IS NULL OR BINARY GROUP_CONCAT(trCommonStatisticFair.name) = '' OR BINARY GROUP_CONCAT(trCommonStatisticFair.name) = '\n')
                            ";
                    else
                        $criteria->having .= "AND (GROUP_CONCAT(trCommonStatisticFair.name) IS NULL OR BINARY GROUP_CONCAT(trCommonStatisticFair.name) = '' OR BINARY GROUP_CONCAT(trCommonStatisticFair.name) = '\n')
                            ";

                    $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                } elseif ($model->{$field} == self::NOT_EMPTY_FILTER){

                    if(empty($criteria->having))
                        $criteria->having = "(GROUP_CONCAT(trCommonStatisticFair.name)  IS NOT NULL AND BINARY GROUP_CONCAT(trCommonStatisticFair.name) != '' AND BINARY GROUP_CONCAT(trCommonStatisticFair.name) != '\n')
                            ";
                    else
                        $criteria->having .= "AND (GROUP_CONCAT(trCommonStatisticFair.name)  IS NOT NULL AND BINARY GROUP_CONCAT(trCommonStatisticFair.name) != '' AND BINARY GROUP_CONCAT(trCommonStatisticFair.name) != '\n')
                            ";

                    $criteria->params[':organizerCompanyName'] = '%' . $model->{$field} . '%';

                } elseif(!empty($model->{$field}))
                    if(empty($criteria->having))
                        $criteria->having = "GROUP_CONCAT(trCommonStatisticFair.name) LIKE '%{$model->{$field}}%'
                        ";
                     else
                        $criteria->having .= "AND GROUP_CONCAT(trCommonStatisticFair.name) LIKE '%{$model->{$field}}%'
                        ";

                $sort[$field] = [
                    'asc'   => "`trCommonStatisticFair`.`name`",
                    'desc'  => "`trCommonStatisticFair`.`name` DESC"
                ];

            }
            elseif (strpos($field, 'lastYearFair') !== FALSE){

                $criteria->select[] = "CASE WHEN (nextYearFair.id IS NULL AND t.beginDate < :today) THEN 0 ELSE 1 END AS lastYearFair";

                if(!strpos($criteria->join, 'AS nextYearFair ON')){

                    $criteria->join .= "
                        LEFT JOIN tbl_fair AS nextYearFair ON nextYearFair.storyId = t.storyId 
                                                           AND YEAR(nextYearFair.beginDate) > YEAR(t.beginDate)
                                                           AND t.active = :active
                                                           AND (t.statusId != :cancelStatus OR t.statusId IS NULL)
                    ";

                    $criteria->params += [
                        ':cancelStatus' => FairStatus::STATUS_CANCEL,
                        ':active' => Fair::ACTIVE_ON,
                        ':today' => $model->today,
                    ];
                }

                $criteria->compare('CASE WHEN (nextYearFair.id IS NULL AND t.beginDate < :today) THEN 0 ELSE 1 END', $model->{$field});
                $sort['lastYearFair'] = [
                    'asc' => "CASE WHEN (nextYearFair.id IS NULL AND t.beginDate < :today) THEN 0 ELSE 1 END",
                    'desc' => "CASE WHEN (nextYearFair.id IS NULL AND t.beginDate < :today) THEN 0 ELSE 1 END DESC"
                ];

                if(!empty($criteria->group) && strpos($criteria->group, 't.id') === FALSE)
                    $criteria->group .= ', t.id';
                 elseif(empty($criteria->group))
                    $criteria->group = 't.id';

            } elseif (strpos($field, 'fairEnName') !== FALSE){

                $criteria->select[] = 'enTrFair.name AS fairEnName';

                if(!strpos($criteria->join, 'AS enTrFair ON')){

                    $criteria->join .= "
                        LEFT JOIN tbl_trfair AS enTrFair ON enTrFair.trParentId = t.id AND enTrFair.langId = :enLangId
                    ";

                    $criteria->params += [
                        ':enLangId' => self::LANGUAGE_EN,
                    ];
                }

                if($model->{$field} == self::EMPTY_FILTER)
                    $criteria->addCondition("(enTrFair.name IS NULL OR BINARY enTrFair.name = '' OR BINARY enTrFair.name = '\n')");
                elseif ($model->{$field} == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(enTrFair.name IS NOT NULL AND BINARY enTrFair.name != '' AND BINARY enTrFair.name != '\n')");
                elseif (!empty($model->{$field}))
                    $criteria->compare('enTrFair.name', $model->{$field}, TRUE);

                $sort['fairEnName'] = [
                    'asc' => 'enTrFair.name',
                    'desc' => 'enTrFair.name DESC',
                ];

            } elseif (strpos($field, 'fairDeName') !== FALSE){

                $criteria->select[] = 'deTrFair.name AS fairDeName';

                if(!strpos($criteria->join, 'AS deTrFair ON')){

                    $criteria->join .= "
                        LEFT JOIN tbl_trfair AS deTrFair ON deTrFair.trParentId = t.id AND deTrFair.langId = :deLangId
                    ";

                    $criteria->params += [
                        ':deLangId' => self::LANGUAGE_DE,
                    ];
                }

                if($model->{$field} == self::EMPTY_FILTER)
                    $criteria->addCondition("(deTrFair.name IS NULL OR BINARY deTrFair.name = '' OR BINARY deTrFair.name = '\n')");
                elseif ($model->{$field} == self::NOT_EMPTY_FILTER)
                    $criteria->addCondition("(deTrFair.name IS NOT NULL AND BINARY deTrFair.name != '' AND BINARY deTrFair.name != '\n')");
                elseif (!empty($model->{$field}))
                    $criteria->compare('deTrFair.name', $model->{$field}, TRUE);

                $sort['fairDeName'] = [
                    'asc' => 'deTrFair.name',
                    'desc' => 'deTrFair.name DESC',
                ];
            } elseif (strpos($field, 'logoExists') || strpos($field, 'logoExists') == 0){
                if($model->logoExists == 2){
                    $criteria->addCondition("(t.logoId IS NOT NULL)");
                }elseif($model->logoExists == 1){
                    $criteria->addCondition("(t.logoId IS NULL)");
                }

                $sort['logoExists'] = [
                    'asc' => 'logoExists',
                    'desc' => 'logoExists DESC',
                ];
            }
        }

        if((bool)strpos($criteria->join, ':langId') && !empty($criteria->params)){
            $criteria->params += [
                ':langId' => Yii::app()->language
            ];
        } elseif ((bool)strpos($criteria->join, ':langId') && empty($criteria->params)) {
            $criteria->params = [
                ':langId' => Yii::app()->language
            ];
        }

        $query = Fair::model()->getCommandBuilder()->createFindCommand(Fair::model()->getTableSchema(),$criteria)->getText();
        $count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ({$query}) t")->queryScalar($criteria->params);

        return new CSqlDataProvider(
            $query,
            array(
                'totalItemCount' => $count,
                'sort' => array(
                    'attributes' => $sort,
                ),
                'pagination' => [
                    'pageSize' => $pageSize
                ],
                'params' => $criteria->params,
            )
        );
    }

    public static function getStatisticDates(){
        $keys = $values = range(2011, date('Y') + 4);

        foreach($keys as $k => $v){
            $keys[$k] = $v.'-01-01';
        }

        return array_combine($keys, $values);
    }

    /**
     * @param $fairId
     * @return array|string
     */
    public static function getAssociationsByFairId($fairId){

        if(empty($fairId)){
            return '';
        }

        $criteria = new CDbCriteria;
        $criteria->with['association'] = [
            'together' => TRUE,
        ];
        $criteria->addCondition("t.fairId = :fairId");
        $criteria->params[':fairId'] = $fairId;

        $association = CHtml::listData(
            FairHasAssociation::model()->findAll($criteria),
            'associationId',
            function($el){
                return isset($el->association->name) ? $el->association->name : '';
            }
        );

        $items = array_map(
            function($associationId, $association) {
                return array(
                    'id'   => $associationId,
                    'name' => $association,
                );
            },
            array_keys($association),
            array_values($association)
        );

        return $items;
    }

    public static function getOrganizersByFairId($fairId){
        
        if(empty($fairId)){
            return NULL;
        }

        $query = "SELECT o.id, troc.name
                    FROM tbl_fairhasorganizer fho 
                    LEFT JOIN tbl_organizer o ON fho.organizerId = o.id
                    LEFT JOIN tbl_organizercompany oc ON o.companyId = oc.id
                    LEFT JOIN tbl_trorganizercompany troc ON troc.trParentId = oc.id AND troc.langId = :langId
                  WHERE fho.fairId = :fairId AND o.id IS NOT NULL";
        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':fairId' => $fairId, ':langId' => Yii::app()->getLanguage()));
        return $data;
    }

    /**
     * @param $fairId
     * @return array|null
     */
    public static function getIndustriesByFairId($fairId){

        if(empty($fairId)){
            return NULL;
        }

        $criteria = new CDbCriteria;
        $criteria->with['industry'] = [
            'together' => TRUE,
            'select' => FALSE,
            'with' => [
                'translate' => [
                    'together' => TRUE,
                    'select' => 'name',
                ],
            ],
        ];
        $criteria->addCondition("t.fairId = $fairId");

        $industries = CHtml::listData(
            FairHasIndustry::model()->findAll($criteria),
            'industryId',
            function($el){
                if(isset($el->industry->translate->name))
                    return $el->industry->translate->name;
                return '';

            }
        );

        $items = array_map(
            function($industryId, $name) {
                return array(
                    'id'   => $industryId,
                    'name' => $name,
                );
            },
            array_keys($industries),
            array_values($industries)
        );

        return $items;
    }

    /**
     * @param   Fair $model
     * @return string
     */
    public static function getCommonStatisticFair($model){

        $sql = "SELECT f.id AS id, trf.name AS name, f.beginDate AS yearDate FROM tbl_fair f
                LEFT JOIN tbl_trfair trf ON trf.trParentId = f.id AND trf.langId = :langId
                WHERE f.infoId = :infoId AND f.id != :id LIMIT 1";

        $res = Yii::app()->db->createCommand($sql)->queryAll(
            TRUE,
            array(
                ':infoId' => $model->infoId,
                ':id' => $model->id,
                ':langId' => Yii::app()->language
            )
        );
        if(!empty($res))
            return json_encode($res);
        return "";
    }

    public static function getIndustriesInString($model)
    {
        $result = '';
        if (isset($model->fairHasIndustries)) {
            $industriesCount = count($model->fairHasIndustries);
            for ($i = 0; $i < $industriesCount; $i++) {
                if (
                    isset($model->fairHasIndustries[$i], $model->fairHasIndustries[$i]->industry)
                    && !empty($model->fairHasIndustries[$i]->industry->name)
                ) {
                    if ($i == ($industriesCount - 1)) {
                        $result .= $model->fairHasIndustries[$i]->industry->name;
                    } else {
                        $result .= $model->fairHasIndustries[$i]->industry->name . '; ';
                    }
                }
            }
        } else {
            $result = Yii::t('system', 'Not set');
        }

        return $result;
    }

    public static function getRatingName($rating = null)
    {
        if (null != $rating) {
            $data = self::getRatingName();
            if (isset($data[$rating])) {
                return $data[$rating];
            }
        }

        return [
            1 => Yii::t('AdminModule.admin', 'Statistics confirmed exhibition rating.'),
            2 => Yii::t('AdminModule.admin', 'Statistics confirmed exhibition audit.'),
            3 => Yii::t('AdminModule.admin', 'Statistics confirmed only organizer of the exhibition.')
        ];
    }

    /**
     * @param $fairId
     * @param $infoId
     * @throws CException
     */
    public static function saveInfoId($fairId, $infoId){

        $fair = Fair::model()->findByPk($fairId);

        if($fair === NULL){
            throw new CException("fair {$fairId} didn't found");
        }

        if(Fair::model()->countByAttributes(['infoId' => $fair->infoId]) <= 1){
            FairInfo::model()->deleteByPk($fair->infoId);
        }

        $fairInfo = FairInfo::model()->findByPk($fair->infoId);

        if($fairInfo !== NULL && $fairInfo->forumId == $fair->id){
            $fairInfo->saveAttributes(array('forumId' => NULL));
        }

        $newFairInfo = FairInfo::model()->findByPk($infoId);

        if($newFairInfo === NULL){
            throw new CException("new fairInfo didn't found");
        }

        $fair->infoId = $infoId;

        if(!$fair->save()){
            throw new CException("fair $fairId didn't save");
        }
    }

    /**
     * @param $fairId
     * @param bool $withoutSelf
     * @return array|CDbDataReader
     */
    public static function getRelatedStatisticFairs($fairId, $withoutSelf = TRUE){

        $sql = "SELECT f2.id, trf2.name, f2.beginDate, fi.forumId, GROUP_CONCAT(a2.name SEPARATOR ';') AS associationName, fr2.name AS rating
                FROM tbl_fairinfo fi
                    LEFT JOIN tbl_fair f ON fi.id = f.infoId
                    LEFT JOIN tbl_fair f2 ON fi.id = f2.infoId 
                    LEFT JOIN tbl_fairrating fr2 ON fr2.id = f2.rating
                    LEFT JOIN tbl_trfair trf2 ON trf2.trParentId = f2.id AND trf2.langId = :langId
                    LEFT JOIN tbl_fairhasassociation fha2 ON fha2.fairId = f2.id
                    LEFT JOIN tbl_association a2 ON a2.id = fha2.associationId
                WHERE f.id = :fairId";

        if($withoutSelf){
            $sql .= ' AND f2.id != :fairId';
        }

        $sql .= ' GROUP BY f2.id';

        $res = Yii::app()->db->createCommand($sql)->queryAll(
            TRUE,
            array(
                ':langId' => Yii::app()->language,
                ':fairId' => $fairId,
            )
        );

        return $res;
    }

    /**
     * @param $fairId
     * @return string
     */
    public static function getForumByFairId($fairId){

        $sql = "SELECT fi.forumId, trf.name FROM tbl_fair f
            LEFT JOIN tbl_fairinfo fi ON fi.id = f.infoId
            LEFT JOIN tbl_trfair trf ON trf.trParentId = fi.forumId AND trf.langId = :langId
            WHERE f.id = :fairId AND fi.forumId != :fairId";

        $data = Yii::app()->db->createCommand($sql)->queryRow(
            TRUE,
            array(
                ':langId' => Yii::app()->language,
                ':fairId' => $fairId,
            )
        );

        return CHtml::link($data['forumId'] . ' ' . $data['name'], Yii::app()->createUrl('admin/fair/view', array('id' =>$data['forumId'])));
    }

    public static function getStatisticRelatedFairs($fairId){

        $sql = "SELECT f2.id, trf2.name, YEAR(f2.beginDate) AS beginDate FROM tbl_fairinfo fi
                LEFT JOIN tbl_fair f ON fi.id = f.infoId
                LEFT JOIN tbl_fair f2 ON fi.id = f2.infoId 
                LEFT JOIN tbl_trfair trf2 ON trf2.trParentId = f2.id AND trf2.langId = :langId
                WHERE f.id = :fairId";

        $data = Yii::app()->db->createCommand($sql)->queryAll(
            TRUE,
            array(
                ':langId' => Yii::app()->language,
                ':fairId' => $fairId,
            )
        );
        $res = '';

        for ($i=0;$i<count($data);$i++){
            $res .= CHtml::link($data[$i]['id'] . ' ' . $data[$i]['name'] . ' ' . $data[$i]['beginDate'], Yii::app()->createUrl('admin/fair/view', array('id' =>$data[$i]['id']))) . '<br>';
        }

        return $res;
    }

    /**
     * @param Fair $model
     * @return bool
     */
    public static function isForum($model){

        $fairInfo = FairInfo::model()->findByPk($model->infoId);
        if(isset($model->id) && isset($fairInfo->forumId) && $fairInfo->forumId == $model->id)
            return true;
        return false;
    }

    public static function getAuditsByFairId($fairId){
        $query = "SELECT au.id, trau.name FROM tbl_fairhasaudit fhau 
                  LEFT JOIN tbl_audit au ON fhau.auditId = au.id
                  LEFT JOIN tbl_traudit trau ON trau.trParentId = au.id AND trau.langId = :langId
                WHERE fairId = :fairId
        ";
        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':fairId' => $fairId, ':langId' => Yii::app()->getLanguage()));
        return $data;
    }

    public static function getExdbShowTypes(){
        $query = "SELECT DISTINCT f.exdbShowType FROM tbl_fair f WHERE f.exdbShowType IS NOT NULL AND f.exdbShowType != '' ORDER BY f.exdbShowType
        ";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE);
        $data = array();
        foreach ($queryData as $row){
            $data[$row['exdbShowType']] = $row['exdbShowType'];
        }
        return $data;
    }

    public static function getExdbFirstYearShows(){
        $query = "SELECT DISTINCT f.exdbFirstYearShow FROM tbl_fair f WHERE f.exdbFirstYearShow IS NOT NULL AND f.exdbFirstYearShow != '' ORDER BY f.exdbFirstYearShow DESC
        ";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE);
        $data = array();
        foreach ($queryData as $row){
            $data[$row['exdbFirstYearShow']] = $row['exdbFirstYearShow'];
        }
        return $data;
    }

    public static function getLastYearFair(){
        return array(
            Yii::t('AdminModule.admin', 'Last year fair'),
            Yii::t('AdminModule.admin', 'Not last year fair'),
        );
    }

    public static function getFairIsForum(){
        return array(
            Yii::t('AdminModule.admin', 'Fair is not forum'),
            Yii::t('AdminModule.admin', 'Fair is forum'),
        );
    }

    public static function getAssociations(){
        $query = "SELECT name FROM tbl_association ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll();
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getAudits(){
        $query = "SELECT name FROM tbl_traudit WHERE langId = :langId ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getDurations(){
        $query = "SELECT d.duration FROM (SELECT DISTINCT CASE WHEN TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) IS NOT NULL AND TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) > 0
                                            THEN TIMESTAMPDIFF(DAY, t.beginDate, t.endDate) + 1 
                                            ELSE NULL END duration FROM tbl_fair t) d WHERE d.duration > 0
                                            ORDER BY d.duration";
        $queryData = Yii::app()->db->createCommand($query)->queryAll();
        $data = array();
        foreach ($queryData as $row){
            $data[$row['duration']] = $row['duration'];
        }
        return $data;
    }

    public static function getCities(){
        $query = "SELECT name FROM tbl_trcity WHERE langId = :langId ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getRegions(){
        $query = "SELECT name FROM tbl_trregion WHERE langId = :langId ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getDistricts(){
        $query = "SELECT name FROM tbl_trdistrict WHERE langId = :langId ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getCountries(){
        $query = "SELECT name FROM tbl_trcountry WHERE langId = :langId ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getIndustries(){
        $query = "SELECT name FROM tbl_trindustry WHERE langId = :langId ORDER BY name";
        $queryData = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        $data = array();
        foreach ($queryData as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public static function getLanguages(){
        $query = "SELECT DISTINCT lang FROM tbl_fair WHERE lang IS NOT NULL AND lang != '' ORDER BY lang";
        $queryData = Yii::app()->db->createCommand($query)->queryAll();
        $data = array();
        foreach ($queryData as $row){
            $data[$row['lang']] = $row['lang'];
        }
        return $data;
    }
}