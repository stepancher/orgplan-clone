<?php

class OrganizerContactView extends TbDetailView
{
    public $organizerItemTemplate = "<tr class=\"{class}\"><th>{label}{updateButton}</th><td style='display: inline-block' class='organizer-val' data-organizer-id='{organizerId}' data-attribute-name='{attributeName}'><input style='float: left; border: none' class='organizer-input' value=\"{value}\">{cancelButton}{saveButton}</td></tr>\n";
    public $updateButtonTemplate = "<a style=\"float: right; opacity: 0\" title=\"{updateTitle}\" class='update-organizer'><img src=\"{updateImgPath}\" alt=\"{updateTitle}\"></a>";
    public $saveButtonTemplate =   "<button style='display: none; float: right' class='organizer-save-button' data-organizer-id=\"{organizerId}\">{saveButtonText}</button>";
    public $cancelButtonTemplate = "<button style='display: none; float: right' class='organizer-cancel-button' data-organizer-id=\"{organizerId}\">{cancelButtonText}</button>";

    const UPDATE_BUTTON_TITLE = 'Редактировать контакт';

    public function run(){

        $formatter=$this->getFormatter();
        if ($this->tagName!==null)
            echo CHtml::openTag($this->tagName,$this->htmlOptions);

        $i=0;
        $n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;

        foreach($this->attributes as $attribute)
        {
            if(is_string($attribute))
            {
                if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
                    throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
                $attribute=array(
                    'name'=>$matches[1],
                    'type'=>isset($matches[3]) ? $matches[3] : 'text',
                );
                if(isset($matches[5]))
                    $attribute['label']=$matches[5];
            }

            if(isset($attribute['visible']) && !$attribute['visible'])
                continue;

            $tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
            if(isset($attribute['cssClass']))
                $tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

            if(isset($attribute['label']))
                $tr['{label}']=$attribute['label'];
            elseif(isset($attribute['name']))
            {
                if($this->data instanceof CModel)
                    $tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
                else
                    $tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
            }

            if(isset($this->data['id'])){
                $tr['{organizerId}'] = $this->data['id'];
            } else {
                $tr['{organizerId}'] = 0;
            }

            if(isset($attribute['name']) && array_search($attribute['name'], OrganizerContactList::$types) !== FALSE){
                $tr['{type}'] = array_search($attribute['name'], OrganizerContactList::$types);
            } else {
                $tr['{type}'] = 0;
            }

            if($attribute['name'] == 'active'){
                $tr['{attributeName}'] = 'active';
            }

            if($attribute['name'] == 'companyId'){
                $tr['{attributeName}'] = 'companyId';
            }

            if($attribute['name'] == 'proposalEmail'){
                $tr['{attributeName}'] = 'proposalEmail';
            }

            $tr['{updateTitle}'] = self::UPDATE_BUTTON_TITLE;
            $tr['{updateImgPath}'] = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.admin.assets') . '/img/Pencil-icon.png');
            $tr['{saveButtonText}'] = Yii::t('AdminModule.admin','Save');
            $tr['{cancelButtonText}'] = Yii::t('AdminModule.admin','Cancel');

            if(!isset($attribute['type']))
                $attribute['type']='text';
            if(isset($attribute['value']))
                $value=is_object($attribute['value']) && get_class($attribute['value']) === 'Closure' ? call_user_func($attribute['value'],$this->data) : $attribute['value'];
            elseif(isset($attribute['name']))
                $value=CHtml::value($this->data,$attribute['name']);
            else
                $value=null;

            $tr['{value}']=$value===null ? '' : $formatter->format($value,$attribute['type']);

            $this->renderItem($attribute, $tr);

            $i++;
        }

        if ($this->tagName!==null)
            echo CHtml::closeTag($this->tagName);
    }

    public function renderItem($options, $templateData){

        if ($options['name'] == 'active' || $options['name'] == 'proposalEmail' || $options['name'] == 'companyId'){

            $template = $this->organizerItemTemplate;
            $template = str_replace('{updateButton}', $this->updateButtonTemplate, $template);
            $template = str_replace('{saveButton}', $this->saveButtonTemplate, $template);
            $template = str_replace('{cancelButton}', $this->cancelButtonTemplate, $template);

            echo strtr(isset($options['template']) ? $options['template'] : $template, $templateData);
        } else {
            echo strtr(isset($options['template']) ? $options['template'] : $this->itemTemplate, $templateData);
        }
    }
}