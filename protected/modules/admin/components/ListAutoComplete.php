<?php
/**
 * File ListAutoComplete.php
 */

/**
 * Class ListAutoComplete
 */
class ListAutoComplete extends CInputWidget
{
    /**
     * @var string
     */
    public $class;

    /**
     * @var bool
     */
    public $multiple = true;

    /**
     * @var bool
     */
    public $autoComplete = true;

    public $alwaysCount = false;
    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $placeholder;

    /**
     * @var array
     */
    public $itemOptions;

    /**
     * @var array
     */
    public $selectedItems = array();

    /**
     * @var bool
     */
    public $hiddenField = true;

    /**
     * @var bool
     */
    public $runSynonym = false;

    /**
     * @var string
     */
    public $selector;

    /**
     * For JS
     *
     * @var array
     */
    public $options;

    /**
     * Получение результатов из БД
     * @var bool
     */
    public $ajaxAutoComplete = false;

    /**
     * Показывать или не показывать label если что-то выбрано
     * @var bool
     */
    public $showNotEmptyLabel = true;

    /**
     * Показывать или не показывать label
     * @var bool
     */
    public $showLabel = true;

    /**
     * @var string
     */
    static $synonymAttribute = 'synonyms';

    public $proto;

    /**
     * @var string
     */
    static $assetsFileRegistered = false;

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        $this->options['proto'] = $this->proto;


        $this->options['alwaysCount'] = $this->alwaysCount;
        if(!$this->selector) {
            $this->selector = '#' . $this->id . '_list-auto-complete';
        }

        if(!isset($this->options['emptyText'])) {
            $this->options['emptyText'] = Yii::t('AdminModule.admin', 'No matches');
        }

        if(isset($this->options['label'])) {
            $this->label = $this->options['label'];
        }

        $this->options['multiple'] = $this->multiple;

        if($this->runSynonym === true) {
            if(isset($this->itemOptions['synonymAttribute'])) {
                $this->synonymAttribute = $this->itemOptions['synonymAttribute'];
            }
        }
        if($this->hasModel()) {
            if(!$this->label) {
                $this->label = $this->model->getAttributeLabel($this->attribute);
            }
            if(!$this->name) {
                $this->name = TbHtml::resolveName($this->model, $this->attribute) . ($this->multiple ? '[]' : '');
            }
        }

        if(!$this->name) {
            throw new CException('Property of class ' . __CLASS__ . '::$name, must be a string');
        }

        if(!isset($this->htmlOptions['labelOptions']) || !is_array($this->htmlOptions['labelOptions'])) {
            $this->htmlOptions['labelOptions'] = array();
        }
        TbHtml::addCssClass('list-auto-complete__drop-button js-list-auto-complete__drop-button', $this->htmlOptions['labelOptions']);

        if(!isset($this->htmlOptions['toggleListOptions']) || !is_array($this->htmlOptions['toggleListOptions'])) {
            $this->htmlOptions['toggleListOptions'] = array();
        }

        $this->addAttribute($this->htmlOptions['toggleListOptions'], 'id', TbHtml::getIdByName($this->name));
        TbHtml::addCssClass('js-drop-list drop-list controls', $this->htmlOptions['toggleListOptions']);

        if(!$this->itemOptions || !is_array($this->itemOptions)) {
            throw new CException('Property of class ' . __CLASS__ . '::$itemOptions, must be an array');
        }

        if(!$this->hasModel()) {
            if(!$this->label) {
                throw new CException('Property of class ' . __CLASS__ . '::$label, must be a string, if widget has no model');
            }
        }

        $this->addAttribute($this->htmlOptions, 'data-text');

        $this->registerClientScript();
    }

    /**
     * Запуск отрисовки
     */
    public function run()
    {
        /** @var array $data */
        $data = array();
        $toggleListOptions = TbArray::popValue('toggleListOptions', $this->htmlOptions, array());

        if($this->hasModel()) {
            $inputName = $this->id . '_' . get_class($this->model) . '_text';
        } else {
            $inputName = $this->id . '_' . $this->name . '_text';
        }
        $input = $this->autoComplete ? TbHtml::tag(
            'div', array('class' => 'list-auto-complete__match-input js-list-auto-complete___match-input'),
            TbHtml::textField('', '', array('placeholder' => $this->placeholder ?: Yii::t('AdminModule.admin', 'Enter')))
        ) : null;

        $infoText = 0;
        $items = static::getItems(
            $this->name,
            $this->itemOptions['data'],
            $this->selectedItems,
            $this->id,
            isset($this->itemOptions['text']) ? $this->itemOptions['text'] : '',
            $this->multiple,
            $this->runSynonym,
            $infoText
        );

        $button = TbHtml::label(
            ((!$this->showNotEmptyLabel && empty($infoText)) || $this->showLabel ? $this->label : '')
            . ($this->alwaysCount
                ? ($this->showNotEmptyLabel ? ' (' : '')
                . $infoText . ($this->showNotEmptyLabel ? ')' : '')
                : ($infoText ? ($this->showNotEmptyLabel ? ' (' : '')
                    . $infoText . ($this->showNotEmptyLabel ? ')' : '') : '')),
            false,
            $this->htmlOptions['labelOptions']);

        $items .= TbHtml::tag('div', array('class' => 'js-drop-list__item--empty'), $this->options['emptyText']);

        TbHtml::addCssClass(
            'list-auto-complete js-list-auto-complete ' . $this->class .
            (!$this->multiple ? ' list-auto-complete--single js-list-auto-complete--single' : ''),
            $this->htmlOptions
        );
        $this->htmlOptions['id'] = str_replace('#', '', $this->selector);

        foreach ($this->htmlOptions as $k => $val) {
            if(is_array($val)) {
                unset($this->htmlOptions[$k]);
            }
        }

        echo TbHtml::tag('div', $this->htmlOptions,
            ($this->hiddenField
                ? TbHtml::hiddenField(str_replace('[]', '', $this->name), '', array('id' => $this->id . '_hidden_input'))
                : null
            )
            . $button . TbHtml::tag(
                'span', $toggleListOptions, $input
                . TbHtml::tag('div', array('class' => 'list-auto-complete__main-list js-list-auto-complete__main-list'), $items))
        );
    }

    private function registerClientScript()
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        $am = $app->getAssetManager();
        $cs = $app->getClientScript();

        if (!static::$assetsFileRegistered) {

            $publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.admin.assets'), false, -1, YII_DEBUG);
            $cs->registerScriptFile($publishUrl . '/js/' . 'ListAutoComplete.js');
            $cs->registerCssFile($publishUrl . '/css/page-fair.css');
            $cs->registerCssFile($publishUrl . '/css/auto-complete/style.css');

            $publishUrl4SlimScroll = $am->publish($app->basePath . '/extensions/slimscroll');
            $cs->registerScriptFile($publishUrl4SlimScroll . '/jquery.slimscroll.js');
            static::$assetsFileRegistered = true;
        }

        $clientScript = $app->getClientScript();

        $this->options['selector'] = $this->selector;

        if(!isset($this->options['event'])) {
            $this->options['event'] = 'keyup';
            $this->options['ajax'] = $this->ajaxAutoComplete;
            $this->options['showNotEmptyLabel'] = $this->showNotEmptyLabel;
        }

        $this->options['runSynonym'] = $this->runSynonym;

        if(empty($this->proto)){
            $clientScript
                ->registerScript(
                    $this->id,
                    'new ListAutoComplete(' . CJavaScript::encode($this->options) . ');',
                    CClientScript::POS_READY
                );
        }else{
            $clientScript
                ->registerScript(
                    $this->id,
                    'if (window.lists == undefined) {
                        window.lists = []; 
                    }
                    window.lists.push('.$this->proto.' = new ListAutoComplete(' . CJavaScript::encode($this->options) . '));',
                    CClientScript::POS_READY
                );

        }

    }

    /**
     * Добавление атрибутов к елементу
     *
     * @param $options
     * @param $attribute
     * @param $value
     * @return array
     */
    private function addAttribute(&$options, $attribute, $value = '')
    {
        if(!!$value && isset($options[$attribute]) && !stristr($options[$attribute], $value)) {
            $options[$attribute] = $options[$attribute] . ' ' . $value;
        } else {
            $options[$attribute] = $value;
        }
    }

    /**
     * @param array $options
     * @param EActiveRecord $object
     */
    static function resolveSynonymsByModel($object, &$options)
    {
        if($object->hasAttribute(static::$synonymAttribute) && !empty($object->{static::$synonymAttribute})) {
            $options['data-synonyms'] = $object->{static::$synonymAttribute};
        }
    }

    static function getItems($name, $data, $selectedItems = [], $id = '', $textField = null, $multiple = false, $runSynonym = false, &$infoText)
    {
        $i = 0;
        $items = '';
        foreach ($data as $key => $object) {
            $labelOptions = array();
            $text = null;
            if(is_object($object)) {
                $checkKey = $object->id;
                if($runSynonym) {
                    static::resolveSynonymsByModel($object, $labelOptions);
                }
                $text = $object->{$textField};
            } else {
                $checkKey = $key;
                $text = $object;
            }
            $checked = in_array($checkKey, $selectedItems);
            $label = TbHtml::checkBox($name, $checked, array(
                'value' => $checkKey,
                'id' => $id . '_' . TbHtml::getIdByName($name) . '_' . $i
            ));
            TbHtml::addCssClass('js-drop-list__item drop-list__item d-bg-base d-bg-input--hover', $labelOptions);
            if($multiple) {
                if($checked) {
                    $infoText++;
                }
                TbHtml::addCssClass('checkbox', $labelOptions);
            } else {
                if($checked) {
                    $infoText = $text;
                    TbHtml::addCssClass('drop-list__item--selected', $labelOptions);
                }
                TbHtml::addCssClass('hidden-checkbox', $labelOptions);
            }
            $items .= TbHtml::label($label . $text, false, $labelOptions);
            $i++;
        }

        return $items;
    }
}