<div class='custom_progress_bar' title="<?=$epoch?>">
    <?php //TODO rafactor immediately hotfix with margin-top in double line layout?>
    <h3 class="panel-title"><?=$name?></h3>
    <div class="stat-info" style="margin-top: -14px;">
        <h3 class="first_value">
            <div><?=$valueType?></div>
            <div class="digit">
                <?php if(isset($lockValues) && !empty($lockValues) && $lockValues):?>
                    <a class="lock lock-modal"  data-target="#modal-tariff-insufficient" data-toggle="modal"></a>
                <?php else:?>
                    <?=$percent?>
                <?php endif;?>
                <span>
                %
                </span>
            </div>
        </h3>
        <h3 class="second_value">
            <div><?=$second_valueType?></div>
            <div class="digit">
                <?php if(isset($lockValues) && !empty($lockValues) && $lockValues):?>
                    <a class="lock lock-modal"  data-target="#modal-tariff-insufficient" data-toggle="modal"></a>
                <?php else:?>
                    <?=$second_percent?>
                <?php endif;?>
                <span>
                    %
                </span>
            </div>
        </h3>
    </div>
    <div class="progress <?=$options['progress']?>">
        <div class="progress-bar" style="width: <?=$percent?>%;background:<?=$color?>;"></div>
        <div class="progress-bar" style="width: <?=$second_percent?>%;background:<?=$colorSecond?>;"></div>
    </div>
</div>