<div class='custom_progress_bar simple-progress' title="<?=$epoch?>">
    <div class="progress <?=$options['progress']?>">
        <div class="progress-bar" style="width: <?=$percent?>%;background:<?=$color?>;"></div>
        <div class="progress-bar" style="width: <?=$second_percent?>%;background:<?=$colorSecond?>;"></div>
    </div>
</div>