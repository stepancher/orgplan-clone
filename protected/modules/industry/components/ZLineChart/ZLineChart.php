<?php

/**
 * Class ZLineChart
 */
class ZLineChart extends CWidget{

    public $options = array();
    public $type;
    public $percent = 0;
    public $second_percent = 0;
    public $name = '';
    public $value = 0;
    public $second_valueType = '';
    public $valueType = 'не указан';
    public $ratingPlace = 0;
    public $color = '#a0d468';
    public $colorSecond = '#fa694c';
    public $backUrl;

    public $lockValues = FALSE;

    const COLOR_GREEN = '#a0d468';
    const COLOR_LIGHT_GREEN = '#4ec882';
    const COLOR_RED = '#fa694c';
    const COLOR_BLUE = '#43ade3';
    const COLOR_AZURE =  '#2b98d5';
    const COLOR_LIGHT_AZURE = '#42c0b4';
    const COLOR_ORANGE ='#f6a800';

    public $epoch;

    const LINE_LAYOUT = 'line_layout';
    const DOUBLE_LINE_LAYOUT = 'double_line_layout';
    const SIMPLE_LINE_LAYOUT = 'simple_line_layout';
    const SIMPLE_DOUBLE_LINE_LAYOUT = 'simple_double_line_layout';
    const SIMPLE_SMALL_LINE_LAYOUT = 'simple_small_line_layout';

    const TYPE_DEFAULT = 0;
    public static $TYPE_DEFAULT_PARAMS = array(
        'progress' => 'progress-sm',
        'layout' => self::LINE_LAYOUT,
    );

    const TYPE_STRIPED = 1;
    public static $PARAMS_TYPE_STRIPED = array(
        'progress' => 'progress-md progress-striped',
        'layout' => self::LINE_LAYOUT,
    );

    const TYPE_BASIC = 2;
    public static $PARAMS_TYPE_BASIC = array(
        'progress' => 'progress-md',
        'layout' => self::LINE_LAYOUT,
    );

    const TYPE_STACKED = 3;
    public static $PARAMS_TYPE_STACKED = array(
        'progress' => 'progress-lg',
        'layout' => self::DOUBLE_LINE_LAYOUT,
    );

    const TYPE_SIMPLE_BASIC = 4;
    public static $PARAMS_TYPE_SIMPLE_BASIC = array(
        'progress' => 'progress-md',
        'layout' => self::SIMPLE_LINE_LAYOUT,
    );

    const TYPE_SIMPLE_STACKED = 5;
    public static $PARAMS_TYPE_SIMPLE_STACKED = array(
        'progress' => 'progress-md',
        'layout' => self::SIMPLE_DOUBLE_LINE_LAYOUT,
    );
    const TYPE_SIMPLE_SMALL = 6;
    public static $PARAMS_TYPE_SIMPLE_SMALL = array(
        'progress' => 'progress-sm',
        'layout' => self::SIMPLE_SMALL_LINE_LAYOUT,
    );

    public function init(){
        if (!isset($this->type) || empty($this->type))
            $this->options = self::$TYPE_DEFAULT_PARAMS;

        switch($this->type){
            case self::TYPE_STRIPED:
                $params = self::$TYPE_DEFAULT_PARAMS;
                $this->options = $params;
                break;

            case self::TYPE_BASIC:
                $params = self::$PARAMS_TYPE_BASIC;
                $this->options = $params;
                break;

            case self::TYPE_STACKED:
                $params = self::$PARAMS_TYPE_STACKED;
                $this->options = $params;
                break;

            case self::TYPE_SIMPLE_BASIC:
                $params = self::$PARAMS_TYPE_SIMPLE_BASIC;
                $this->options = $params;
                break;

            case self::TYPE_SIMPLE_STACKED:
                $params = self::$PARAMS_TYPE_SIMPLE_STACKED;
                $this->options = $params;
                break;

            case self::TYPE_SIMPLE_SMALL:
                $params = self::$PARAMS_TYPE_SIMPLE_SMALL;
                $this->options = $params;
                break;

            default:{
                $params = self::$TYPE_DEFAULT_PARAMS;
                $this->options = $params;
                break;
            }
        }

        if(empty($this->backUrl)){
            $this->backUrl = Yii::app()->createUrl('profile/default', ['to' => Yii::app()->getRequest()->requestUri, 'tab' => 'tariffs']);
        }

        parent::init();

        $appPath = realpath(__DIR__ . DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR;
        Yii::setPathOfAlias('ZLineChart', $appPath );

        $this->registerAssets();
    }

    public function run(){

        echo $this->controller->renderPartial('ZLineChart.views.'.$this->options['layout'], array(
            'options' => $this->options,
            'percent' => $this->percent,
            'second_percent' => $this->second_percent,
            'name' => $this->name,
            'value' => $this->lockValues?
                '<a class="lock lock-modal" data-target="#modal-tariff-insufficient" data-toggle="modal"></a>'
                :
                $this->value,
            'second_valueType' => $this->second_valueType,
            'valueType' => $this->valueType,
            'ratingPlace' => $this->ratingPlace,
            'color' => $this->color,
            'colorSecond' => $this->colorSecond,
            'epoch' => $this->epoch,
            'lockValues' => $this->lockValues,
            'backUrl' => $this->backUrl,
        ));
    }

    protected function registerAssets(){
        $app = Yii::app();
        $cs = $app->getClientScript();
        $am = $app->getAssetManager();
        $publishUrl = $am->publish(Yii::getPathOfAlias('ZLineChart.assets') , false, -1, YII_DEBUG);

		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.industry.extensions.ZLineChart.assets.line_chart') . '.css')
		);
    }

    /**
     * @param $id
     * @return array
     */
    public static function getWidgetType($id){

        $widgets = self::getWidgetTypes();

        if(isset($widgets[$id])){
            return $widgets[$id];
        }

        return array();
    }

    /**
     * @param $id
     * @return array
     */
    public static function getWidgetTypes(){

        return array(
            self::TYPE_BASIC => 'Стандартный',
            self::TYPE_STRIPED => 'С полосами',
            self::TYPE_STACKED => 'Составной',
            self::TYPE_SIMPLE_BASIC => 'Табличный стандартный',
            self::TYPE_SIMPLE_STACKED => 'Табличный составной',
            self::TYPE_SIMPLE_SMALL => 'Табличный маленький со значением',
        );

    }

    /**
     * @return array
     */
    public static function getColors(){

        return array(
            self::COLOR_GREEN,
            self::COLOR_LIGHT_GREEN,
            self::COLOR_RED,
            self::COLOR_BLUE,
            self::COLOR_AZURE,
            self::COLOR_LIGHT_AZURE,
            self::COLOR_ORANGE,
        );
    }
}
