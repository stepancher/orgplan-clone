<?php

Class TrUrl extends CComponent{

    public function init(){

    }

    public function getTrUrl($route, $params){

        if(empty($params['urlIndustryName'])){
            return NULL;
        }

        $trIndustry = TrIndustry::model()->findByAttributes(array('shortNameUrl' => $params['urlIndustryName']));

        if(empty($trIndustry)){
            return NULL;
        }

        $tr = TrIndustry::model()->findByAttributes(array('trParentId' => $trIndustry->trParentId, 'langId' => $params['langId']));

        if(empty($tr)){
            return NULL;
        }

        $params['urlIndustryName'] = $tr->shortNameUrl;

        return Yii::app()->createUrl($route, $params);
    }
}