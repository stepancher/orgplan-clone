<?php

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    public $header = '';

    /**
     * @var Industry $model
     */
    public $model = 'Industry';

    /**
     * @param string $langId
     * @param string $urlIndustryName
     * @throws CHttpException
     */
    public function actionView($langId, $urlIndustryName)
    {
        if(Yii::app()->user->isGuest){
            Yii::app()->request->redirect(Yii::app()->createUrl('auth/default/auth'));
        }

        if(is_string($urlIndustryName)){
            $urlIndustryName = strtolower($urlIndustryName);
        }

        $trindustry = TrIndustry::model()->findByAttributes([
            'langId' => $langId,
            'shortNameUrl' => $urlIndustryName,
        ]);

        if($trindustry === NULL){
            throw new CHttpException(404, 'Industry not found');
        }

        $id = $trindustry->trParentId;

        $model = Industry::model()->findByPk($id);

        if($model == NULL){
            throw new CHttpException(404, 'Industry not found');
        }


        $this->header = $model->name;//@TODO refactor getting header h1 for page;
        $this->pageTitle = $model->name;


        $this->render('view', array(
            'model' => $model,
        ));
    }
}