<?php

class JsonController extends Controller
{

    protected $dump = FALSE;

    public function actionDump($langId){
        $this->dump = TRUE;
        $this->actionGetList($langId);
    }

    public function actionGetList($langId)
    {
        $json = array(
            "user" => array(
                "auth" => !Yii::app()->user->isGuest,
            ),
            "header"=> Yii::t('IndustryModule.industry', 'Fairs of all countries'),
            "topIndustriesList"=> array(
                "header"=> Yii::t('IndustryModule.industry', 'Popular industries'),
                "industries"=> $this->getTopIndustries([11,1,2,3,5,13,14,37,8]),
            ),
            "industriesList"=> array(
                "header"=> Yii::t('IndustryModule.industry', 'Industries list'),
                "industries"=> $this->getIndustries(),
            )
        );

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    public function getIndustries(){

        $sql = '
            SELECT
                i.id industryId,
                tri.name industryName,
                count(fhi.fairId) fairsCount
                
            FROM tbl_industry i
            
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.industryId = i.id
            LEFT JOIN tbl_trindustry tri ON fhi.industryId = tri.trParentId AND langId = :langId
            LEFT JOIN tbl_fair f ON f.id = fhi.fairId
            
            WHERE 
            f.active = 1 AND 
            YEAR(f.beginDate) >= YEAR(NOW())
            
            GROUP BY fhi.industryId
            
            ORDER BY industryName ASC
        ';

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':langId' => Yii::app()->language]);

        $industries = array();
        foreach($res as $row){
            $industries[] = array(
                "id"    => $row['industryId'],
                "name"  => $row['industryName'],
                "value" => $row['fairsCount'],
            );
        }
        
        return $industries;
    }

    public function getTopIndustries($industryIds){

        $industriesIds = implode(', ', $industryIds);

        $sql = "
            SELECT
                i.id industryId,
                tri.name industryName,
                count(fhi.fairId) fairsCount
                
            FROM tbl_industry i
            
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.industryId = i.id
            LEFT JOIN tbl_trindustry tri ON fhi.industryId = tri.trParentId AND langId = :langId
            LEFT JOIN tbl_fair f ON f.id = fhi.fairId
            
            WHERE 
            f.active = 1 AND 
            fhi.industryId IN ({$industriesIds}) AND
            YEAR(f.beginDate) >= YEAR(NOW())
            
            GROUP BY fhi.industryId
            
            ORDER BY industryName ASC
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':langId' => Yii::app()->language]);

        $industries = array();
        foreach($res as $row){
            $industries[] = array(
                "id"    => $row['industryId'],
                "name"  => $row['industryName'],
                "value" => $row['fairsCount'],
            );
        }

        return $industries;
    }

    public function actionGetAnalyticsList($searchQuery = NULL){

        $json = array(
            "header"=> Yii::t('IndustryModule.industry', 'Industry catalog'),
            "models"=> $this->getIndustriesList($searchQuery, [11,1,2,3,5,13,14,37,8]),
            "searchPlaceholder"=> Yii::t('IndustryModule.industry', 'Search in name'),
        );

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }


    public function getIndustriesList($searchQuery = NULL, $industryIds){

        $searchQueryCondition = '';
        $q = Yii::t('IndustryModule.industry', 'Founded');
        $params = [
            ':langId' => Yii::app()->language,
            ':fileType' => ObjectFile::TYPE_ANALYTICS_IMAGE,
        ];

        if(!empty($searchQuery)){
            $searchQueryCondition = "tri.name LIKE :searchQuery AND";
            $q = Yii::t('IndustryModule.industry', 'On search')." \"{$searchQuery}\" ".Yii::t('IndustryModule.industry', 'founded');
            $params[':searchQuery'] = "%{$searchQuery}%";
        }

        $sql = "
            SELECT
                tri.name AS industryName,
                i.id AS id,
                a.id as imgId,
                objf.name AS fileName,
                objf.label AS fileLabel,
                tri.shortNameUrl

            FROM tbl_industry i

            LEFT JOIN tbl_trindustry tri ON (i.id = tri.trParentId AND tri.langId = :langId)
            LEFT JOIN tbl_analytics a ON (a.industryId = i.id)
            LEFT JOIN tbl_objectfile objf ON (objf.id = a.imageId)

            WHERE
                {$searchQueryCondition}
                i.id IN (".implode(',', $industryIds).") AND
                objf.type = :fileType

            GROUP BY i.id
        ";

        $industries = Yii::app()->db->createCommand($sql)->queryAll(TRUE, $params);

        $data = array();
        foreach($industries as $industry){

            $filePath = Yii::app()->getBaseUrl(true).'/frontend/empty.png';

            if(!empty($industry['fileName'])){
                $filePath = Yii::app()->getBaseUrl(true).'/uploads/'.str_replace('{id}',$industry['imgId'], ObjectFile::$paths[ObjectFile::TYPE_ANALYTICS_IMAGE]).'/'.$industry['fileName'];
            }

            $data[] = array(
                "id"=> $industry['id'],
                "name"=> mb_convert_case($industry['industryName'], MB_CASE_LOWER, "UTF-8"),
                "link" => Yii::app()->getBaseUrl(true).Yii::app()->createUrl(
                        'industry/default/view',
                        array(
                            'langId' => Yii::app()->language,
                            'urlIndustryName' => $industry['shortNameUrl'],
                        )
                    ),
                "image"=> $filePath,
            );
        }

        $result = array(
            "searchResultLabel"=> $q." ".count($industries)." ".Yii::t('IndustryModule.industry', 'industries'),
            'list' => $data,
        );

        return $result;
    }
}