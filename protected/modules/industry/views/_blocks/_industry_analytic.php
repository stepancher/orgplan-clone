<?php
/**
 * @var Controller  $this
 */

?>

<?php
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.industry.components.ZGridView.assets.zgridview') . '.css')
    );
    Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.industry.assets.css.style') . '.css')
    );
?>
    <div class="col-md-12" style="margin-top:20px;">

        <?php if($industry->id == 11):?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','SALES VOLUME OF AGRICULTURAL INDUSTRY') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_agro_map2.png' alt='' title=''>
                </div>
            </div>
        <?php elseif($industry->id == 2):?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_building_map2.png' alt='' title=''>
                </div>
            </div>
        <?php endif;?>
        <?php if($industry->id == 1):?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_furniture_map.png' alt='' title=''>
                </div>
            </div>
        <?php elseif($industry->id == 5):?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_timber_map.png' alt='' title=''>
                </div>
            </div>
        <?php endif;?>

        <?php if($industry->id == 8) :?>
        <div class="grid-table">
            <div class="analytic-table-head">
                <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_energy_map.png' alt='' title=''>
            </div>
        </div>
        <?php endif;?>

        <?php if($industry->id == 13) :?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_foodequipment_map.png' alt='' title=''>
                </div>
            </div>
        <?php endif;?>

        <?php if($industry->id == 14) :?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_food_map.png' alt='' title=''>
                </div>
            </div>
        <?php endif;?>

        <?php if($industry->id == 3) :?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_metal_and_equipment_map.png' alt='' title=''>
                </div>
            </div>
        <?php endif;?>

        <?php if($industry->id == 37) :?>
            <div class="grid-table">
                <div class="analytic-table-head">
                    <?= Yii::t('IndustryModule.industry','INDUSTRY HIGHLIGHTS') ?>
                    <div class="header-line" style="position:relative;margin-top:13px;margin-bottom: 20px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <img style="display:block;clear:both;margin: 0px auto 30px auto;" src='<?php echo Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.industry.assets'));?>/img/analytics/analytics_industry_oil_map.png' alt='' title=''>
                </div>
            </div>
        <?php endif;?>

        <?php
        $this->renderPartial('application.modules.industry.views._blocks._tooltip');
        ?>

        <?php $first=1; foreach (ChartTableGroup::model()->findAllByAttributes(
            array(
                'industryId' => $industry->id,
                'visible'    => 1,
            ),
            array(
                'order'=>'position'
            )
        ) as $chartTableGroup) : ?>

            <div class="chartTableGroup-dropdown industry-dropdown" id="<?php echo $first ? 'first-block' : ''?>" target="chartTableGroup-<?=$chartTableGroup->id?>" style="background: <?=$chartTableGroup->color?>">
                <div class="industry-name"><?=$chartTableGroup->name ?></div>
                <div class="chartTableGroup-wrap-arrow">
                    <div class="chartTableGroup-arrow-down">

                    </div>
                </div>
                <div class="chartTableGroup-dropdown-arrow" >
                </div>
            </div>

            <script>
                $(function(e){
                    var t = $('.chartTableGroup-cards-container').parent().find('.chartTableGroup-cards-container:first-child');
                });
            </script>


            <div style="clear:both;"></div>

            <?php foreach(ChartTable::model()->findAllByAttributes(
                array(
                    'groupId' => $chartTableGroup->id,
                    'visible'    => 1,
                ),
                array(
                    'order'=>'position'
                )
            ) as $table):?>
            <div class="chartTableGroup-cards-container chartTableGroup-<?=$chartTableGroup->id?>  " data-target="chartTableGroup-<?=$chartTableGroup->id?>">
                <div class="grid-table">
                    <div class="analytic-table-head">
                        <?php echo $table->name ?>
                        <div class="header-line">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>

                    <?php if($table->area == 'country') : ?>
                        <?php $this->widget('application.modules.industry.extensions.ZGridView.ZGridView', array(
                            'id' => "analytics-grid-{$table->id}",
                            'dataProvider' => AnalyticsService::getCountryColumnDataProvider(ChartColumn::getByTableId($table->id), $table->industryId),
                            'htmlOptions' => array(
                                'class' => 'analytics-table-container table-type-'.$table->type
                            ),
                            'itemsCssClass' => 'analytic-table',
                            'chartColumns' => ChartColumn::getByTableId($table->id),
                            'area' => $table->area,
                            'lockValues' => !$statsAccess,
                            'template' => '{items}',
                            'ajaxVar' => 'analytics-grid-request',
                        ));?>
                    <?php else : ?>
                        <?php $this->widget('application.modules.industry.extensions.ZGridView.ZGridView', array(
                            'id' => "analytics-grid-{$table->id}",
                            'dataProvider' => AnalyticsService::getColumnsDataProvider(ChartColumn::getByTableId($table->id), $table->area),
                            'htmlOptions' => array(
                                'class' => 'analytics-table-container table-type-'.$table->type
                            ),
                            'itemsCssClass' => 'analytic-table',
                            'chartColumns' => ChartColumn::getByTableId($table->id),
                            'area' => $table->area,
                            'lockValues' => !$statsAccess,
                            'template' => '{items}',
                            'ajaxVar' => 'analytics-grid-request',
                        ));?>
                    <?php endif;?>

                    <?php if($table->area == ChartTable::TYPE_REGION):?>
                        <div class="footer-more-button"><?=Yii::t('IndustryModule.industry', 'Show all list');?><!--<div class="arrow-open"></div>--></div>
                    <?php endif;?>
                </div>
            </div>
            <?php endforeach;?>

        <div class="chartTableGroup-cards-container chartTableGroup-<?=$chartTableGroup->id?>">
            <div class="chartTableGroup-name industry-dropdown" target="chartTableGroup-<?=$chartTableGroup->id?>" style="background: #C0C0C0">
                <div class="industry-name"><?= Yii::t('GradotekaModule.gradoteka', 'roll up') ?></div>
            </div>
        </div>

        <?php $first=0; endforeach;?>

        <p class="source">
            <?= Yii::t('IndustryModule.industry', 'Source: Gradoteka') ?> /
            <?php
            echo CHtml::link('www.gradoteka.ru', 'http://www.gradoteka.ru', ['class' => 'default-link', 'rel' => 'nofollow', 'target' => '_blank']);
            ?>
        </p>

        <?php
        Yii::app()->clientScript->registerScript('industry_dropdown', "
                    $('.chartTableGroup-dropdown').click(function(){
                        var target = $(this).attr('target');
                        $('.' + target).slideToggle('slow');
                    });
                ");
        ?>

        <?php
        Yii::app()->clientScript->registerScript('industry_dropdown_lower', "
                    $('.chartTableGroup-name').click(function(){
                        var target = $(this).attr('target');
                        $('.' + target).slideToggle('slow');
                    });
                ");
        ?>
    </div>

<?php
//@TODO refactor modal window script. include once.
Yii::app()->clientScript->registerScript('lock-modal', "
    $(window).ready(function(){
        $('.lock-modal').click(function(){
            var url = $(this).attr('href');
            if(url === '' || url === 0 || url === null || url === undefined || url === 'undefined'){
                url = '/ru/profile/default/index/?to='+encodeURIComponent(window.location.href)+'&tab=tariffs';
            }
            $('#go-to').attr({'href':url});
        });
    });
");
?>

<script type="text/javascript">
    var openClass = 'analytic-table--opened';
    $('.footer-more-button').click(function(){
        var container = $(this).parent('.grid-table').children('.analytics-table-container');
        var tableHeight = container.children('.analytic-table').height();
        var table = $(this).parent('.grid-table');

        if(table.hasClass(openClass)){
            $(this).html('<?=Yii::t('IndustryModule.industry', 'Show all list');?>');
            table.animate({height: get_closed_height($(this)) + 52}, 600, 'swing').removeClass(openClass);
        }else{
            $(this).html('Скрыть весь список');
            table.css({height: tableHeight + 103}).addClass(openClass);
        }
    });

    $('.chartTableGroup-name').click(function(){
        var container = $(this).parent('.grid-table').children('.analytics-table-container');
        var tableHeight = container.children('.analytic-table').height();
        var table = $(this).parent('.grid-table');

        if(table.hasClass(openClass)){
            $(this).html('<?=Yii::t('IndustryModule.industry', 'Show all list');?>');
            table.animate({height: get_closed_height($(this)) + 52}, 600, 'swing').removeClass(openClass);
        }else{
            table.css({height: tableHeight + 103}).addClass(openClass);
        }
    });

    function get_closed_height(obj){
        var parent = obj.parent('.grid-table');
        var table = parent.children('.analytics-table-container').children('.analytic-table');
        var height = parent.children('.analytic-table-head').outerHeight();

        height = height + table.children('thead').outerHeight();
        var trs = table.children('tbody').children('tr');

        for(var i = 0;i <= 9; i++){
            height = height + trs.eq(i).outerHeight();
        }

        return height;
    }

    function set_height(){
        $('.footer-more-button').each(function(){
            $(this).parent('.grid-table').height(get_closed_height($(this)) + 52);
            $(this).parent('.grid-table').parent('.chartTableGroup-cards-container').css({"display": "none"});
        });
    }

    $(window).load(function(){
        set_height();
        $('.chartTableGroup-cards-container').css({"display": "none"});
        $('#first-block').click();
    });

    <?php if(Yii::app()->request->isAjaxRequest):?>
    setTimeout(
        function()
        {
            set_height()
        }, 300);
    <?php endif;?>
</script>