<?php
/**
 * @var Analytics $data
 */

	$name = '';
	if ($data->industryId && $data->industry) {
		$name = $data->industry->name;
	}
	$modal = '';
	if(Yii::app()->user->isGuest){
		$modal = 'data-toggle="modal" data-target="#modal-sign-up"';
	}
?>
<div class="b-switch-list-view__item">
	<div class="b-switch-list-view__cell b-switch-list-view__cell--name" <?=$modal;?>>
		<?= TbHtml::link($name, Yii::app()->createUrl('industry/default/view', array('id' => $data->industryId))) ?>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--type b-switch-list-view__cell--hide-as-row">
		<span class="b-switch-list-view__item--type-text"><?=Yii::t('IndustryModule.industry', 'sphere')?></span>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--background b-switch-list-view__cell--hide-as-row" <?=$modal;?>>
		<a href="<?=Yii::app()->createUrl('industry/default/view', array('id' => $data->industryId));?>">
			<?=TbHtml::image(H::getImageUrl($data, 'image'))?>
		</a>
	</div>
</div>