<?php
/**
 * @var UsefulInformationController $this
 * @var UsefulInformation $model
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerScriptFile(
	$am->publish($app->theme->basePath . '/assets' . '/js/usefulInformation.js')
);
Yii::import('application.widgets.UsefulInformationForm');
$ctrl = $this;
$ctrl->widget('application.modules.industry.components.FieldSetView', array(
	'header' => Yii::t('IndustryModule.industry', $model->isNewRecord ? 'Form header create' : 'Form header update'),
	'items' => array(
		array(
			'application.modules.industry.components.FormView',
			'form' => new UsefulInformationForm(
					array(
						'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
						'enctype' => 'multipart/form-data',
						//'elementGroupName' => '[]'
					),
					$model, $ctrl
				),
			//'partialMode' => true,
			'items' => array(
				array(
					'application.modules.industry.components.HtmlView',
					'content' => function ($owner) use ($model, $ctrl) {
							/** @var ARController $owner */
							if (!$model->isNewRecord && $model->image) {
								$model->image->setOwnerId($model->id);
								if ($model->image->getFileUrl()) {
									$owner->widget('application.modules.industry.components.FieldSetView', array(
										'header' => 'Заглавное изображение'
									));
									$images[] = array(
										'image' => $model->image->getFileUrl(),
										'caption' => TbHtml::linkButton(
												'Удалить',
												array(
													'color' => TbHtml::BUTTON_COLOR_DANGER,
													'size' => TbHtml::BUTTON_SIZE_MINI,
													'url' => $owner->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $model->imageId))
												)
											)
									);
									echo TbHtml::thumbnails($images);

								}
							} else {
								$owner->widget('application.modules.industry.components.FieldSetView', array(
										'header' => 'Загрузить заглавное изображение',
										'items' => array(
											array(
												'CMultiFileUpload',
												'model' => UsefulInformation::model(),
												'htmlOptions' => array(
													'style' => 'margin-left:80px'
												),
												'max' => 1,
												'attribute' => 'image',
												'duplicate' => 'Такой файл уже имеется!',
												'accept' => 'gif|jpg|jpeg|png',
												'denied' => 'Недопустимый тип файла',
											)
										)
									)
								);
							}

							$owner->widget('application.modules.industry.components.FieldSetView', array(
									'header' => 'Загрузить изображения',
									'items' => array(
										array(
											'CMultiFileUpload',
											'model' => UsefulInformation::model(),
											'htmlOptions' => array(
												'style' => 'margin-left:80px'
											),
											'attribute' => 'images',
											'duplicate' => 'Такой файл уже имеется!',
											'accept' => 'gif|jpg|jpeg|png',
											'denied' => 'Недопустимый тип файла',
										)
									)
								)
							);

							if (!empty($model->usefulInformationHasFiles)) {
								$images = array();
								$owner->widget('application.modules.industry.components.FieldSetView', array(
									'header' => 'Изображения'
								));

								foreach ($model->usefulInformationHasFiles as $image) {
									$image->file->setOwnerId($model->id);
									if ($image->file->type == ObjectFile::TYPE_USEFUL_INFORMATION_IMAGES && $image->file->getFileUrl()) {
										$images[] = array(
											'image' => $image->file->getFileUrl(),
											'caption' => TbHtml::linkButton(
													'Удалить',
													array(
														'color' => TbHtml::BUTTON_COLOR_DANGER,
														'size' => TbHtml::BUTTON_SIZE_MINI,
														'url' => $owner->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $image->fileId))
													)
												)
										);
									}
								}
								echo TbHtml::thumbnails($images);

							}

							$owner->widget('application.modules.industry.components.FieldSetView', array(
									'header' => 'Загрузить файлы',
									'items' => array(
										array(
											'CMultiFileUpload',
											'model' => UsefulInformation::model(),
											'htmlOptions' => array(
												'style' => 'margin-left:80px'
											),
											'attribute' => 'files',
											'duplicate' => 'Такой файл уже имеется!',
											'denied' => 'Недопустимый тип файла',
										)
									)
								)
							);

							if (!$model->isNewRecord && !empty($model->usefulInformationHasFiles)) {
								$owner->widget('application.modules.industry.components.FieldSetView', array(
									'header' => 'Список файлов',
									'items' => array(
										array(
											'application.modules.industry.components.ObjectFileGridView',
											'model' => ObjectFile::model(),
											'compare' => array(
												array('t.id', CHtml::listData($model->usefulInformationHasFiles, 'id', 'fileId')),
												array('t.type', ObjectFile::TYPE_USEFUL_INFORMATION_FILES)
											),
											'columnsAppend' => array(
												'buttons' => array(
													'class' => 'bootstrap.widgets.TbButtonColumn',
													'template' => '{delete}',
													'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
													'buttons' => array(
														'delete' => array(
															'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
															'url' => function ($data) use ($ctrl, $model) {
																	return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
																},
															'options' => array('title' => 'Информация', 'class' => 'btn btn-small btn-danger'),
														),
													)
												)
											)
										)
									)
								));
							}
						}
				),
				array(
					'application.modules.industry.components.ActionsView',
					'items' => array(
						'submit' => array(
							'type' => TbHtml::BUTTON_TYPE_SUBMIT,
							'label' => Yii::t('IndustryModule.industry', 'Form action save'),
							'attributes' => array(
								'color' => TbHtml::BUTTON_COLOR_SUCCESS
							)
						),
						'cancel' => array(
							'type' => TbHtml::BUTTON_TYPE_LINK,
							'label' => Yii::t('IndustryModule.industry', 'Form action cancel'),
							'attributes' => array(
								'url' => $this->createUrl('index'),
								'color' => TbHtml::BUTTON_COLOR_DANGER
							)
						)
					)
				)
			)

		),
	)
));