<?php
/**
 * @var CController $this
 * @var Analytics $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/less/global.css');

if(isset($model) && !empty($model)){
    $industry = $model;
}

if(!empty($industry) && $industry->checkAccess()){
    $statsAccess = User::checkTariff(1);
    $pageParams = array();
    $pageParams['analytics-grid-request'] = isset($_GET['analytics-grid-request'])?$_GET['analytics-grid-request']:'';
    $pageParams['sort'] = isset($_GET['sort'])?$_GET['sort']:'';
    $pageParams['industry'] = $industry->id;
    $pageParams['open'] = $statsAccess;
    $pageParams['lang'] = Yii::app()->language;
?>
<div style="max-width:960px;">
    <div id="industry_tab_container">
        <div class="col-md-12">
            <?php //@TODO refactor separated h1_header?>
            <?=$this->renderPartial('application.modules.industry.views._blocks._h1_header', array(
                'model' => $model,
            ), true)?>
        </div>
        <?php //@TODO create methods or renderPartial for checking rights and displaying message ?>
        <?php if(!Yii::app()->user->isGuest && !User::checkTariff(1)):?>
            <div class="login-access-notice">
                <span class="p-icon"></span>
                Внимание!
                <br/>
                Отраслевые показатели доступны только после выбора тарифа.
                <a class="sign-up" style="color: #fa694c !important;" href="<?=Yii::app()->createUrl('profile/default', array('tab' => 'tariffs', 'to' => Yii::app()->request->requestUri));?>">
                    Выберите тариф
                </a>
            </div>
        <?php endif;?>
        <div class="cols-row">
            <?php
                if($this->beginCache(sha1(serialize($pageParams)), ['duration' => 3600 * 24 * 365])){

                    $this->renderPartial('application.modules.industry.views._blocks._industry_analytic', array(
                        'industry' => $industry,
                        'statsAccess' => $statsAccess,
                        'model' => $model,
                    ));

                    $this->endCache();
                }
            ?>
        </div>
    </div>
</div>
<?php }?>

