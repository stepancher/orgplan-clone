<?php
///* @var $form CActiveForm */
$app = Yii::app();
$request = $app->getRequest();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/gridster/dist/jquery.gridster.min.js', CClientScript::POS_END);

?>
<style type="text/css">
    .gridster{
        width:100%;
        margin-top:100px;
    }
    .gridster ul{
        width:100%;
        position: relative;
        /*background-color: #EFEFEF;*/
    }
    .gridster ul li{
        border:1px solid black;
        list-style: none;
        background:rgba(0,0,0,0.3);
        position:absolute;
        font-size: 1em;
        font-weight: bold;
        text-align: center;
        line-height: 100%;
    }
    .gridster .gs-w {
        background: #DDD;
        cursor: pointer;
    }

    .gridster .player {
        background: #BBB;
    }

    .gridster .preview-holder {
        border: none!important;
        background: orange!important;
    }

    .gridster {
        position:relative;
    }

    .gridster > * {
        -webkit-transition: height .4s, width .4s;
        -moz-transition: height .4s, width .4s;
        -o-transition: height .4s, width .4s;
        -ms-transition: height .4s, width .4s;
        transition: height .4s, width .4s;
    }
    .gridster .gs-w {
        z-index: 2;
        position: absolute;
    }
    .ready .gs-w:not(.preview-holder) {
        -webkit-transition: opacity .3s, left .3s, top .3s;
        -moz-transition: opacity .3s, left .3s, top .3s;
        -o-transition: opacity .3s, left .3s, top .3s;
        transition: opacity .3s, left .3s, top .3s;
    }
    .ready .gs-w:not(.preview-holder),
    .ready .resize-preview-holder {
        -webkit-transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
        -moz-transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
        -o-transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
        transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
    }
    .gridster .preview-holder {
        z-index: 1;
        position: absolute;
        background-color: #fff;
        border-color: #fff;
        opacity: 0.3;
    }
    .gridster .player-revert {
        z-index: 10!important;
        -webkit-transition: left .3s, top .3s!important;
        -moz-transition: left .3s, top .3s!important;
        -o-transition: left .3s, top .3s!important;
        transition:  left .3s, top .3s!important;
    }
    .gridster .dragging,
    .gridster .resizing {
        z-index: 10!important;
        -webkit-transition: all 0s !important;
        -moz-transition: all 0s !important;
        -o-transition: all 0s !important;
        transition: all 0s !important;
    }
    .gs-resize-handle {
        position: absolute;
        z-index: 1;
    }
    .gs-resize-handle-both {
        width: 20px;
        height: 20px;
        bottom: -8px;
        right: -8px;
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pg08IS0tIEdlbmVyYXRvcjogQWRvYmUgRmlyZXdvcmtzIENTNiwgRXhwb3J0IFNWRyBFeHRlbnNpb24gYnkgQWFyb24gQmVhbGwgKGh0dHA6Ly9maXJld29ya3MuYWJlYWxsLmNvbSkgLiBWZXJzaW9uOiAwLjYuMSAgLS0+DTwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DTxzdmcgaWQ9IlVudGl0bGVkLVBhZ2UlMjAxIiB2aWV3Qm94PSIwIDAgNiA2IiBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjojZmZmZmZmMDAiIHZlcnNpb249IjEuMSINCXhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbDpzcGFjZT0icHJlc2VydmUiDQl4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjZweCIgaGVpZ2h0PSI2cHgiDT4NCTxnIG9wYWNpdHk9IjAuMzAyIj4NCQk8cGF0aCBkPSJNIDYgNiBMIDAgNiBMIDAgNC4yIEwgNCA0LjIgTCA0LjIgNC4yIEwgNC4yIDAgTCA2IDAgTCA2IDYgTCA2IDYgWiIgZmlsbD0iIzAwMDAwMCIvPg0JPC9nPg08L3N2Zz4=');
        background-position: top left;
        background-repeat: no-repeat;
        cursor: se-resize;
        z-index: 20;
    }
    .gs-resize-handle-x {
        top: 0;
        bottom: 13px;
        right: -5px;
        width: 10px;
        cursor: e-resize;
    }
    .gs-resize-handle-y {
        left: 0;
        right: 13px;
        bottom: -5px;
        height: 10px;
        cursor: s-resize;
    }
    .gs-w:hover .gs-resize-handle,
    .resizing .gs-resize-handle {
        opacity: 1;
    }
    .gs-resize-handle,
    .gs-w.dragging .gs-resize-handle {
        opacity: 0;
    }
    .gs-resize-disabled .gs-resize-handle {
        display: none!important;
    }
    [data-max-sizex="1"] .gs-resize-handle-x,
    [data-max-sizey="1"] .gs-resize-handle-y,
    [data-max-sizey="1"][data-max-sizex="1"] .gs-resize-handle {
        display: none !important;
    }
    .gridster ul li button{
        position:absolute;
        bottom:0px;
        left:0px;
    }
    .gridster ul li img{
        width:100%;
    }
</style>
<script type="text/javascript">
    $(function(){ //DOM Ready
        var gridster;
        var model_id = <?php echo $model->id;?>;
        $(function(){
            gridster = $(".gridster ul").gridster({
                widget_base_dimensions: [200, 110],
                widget_margins: [10, 10],
                draggable: {
                    stop: function(e, ui, $widget) {
                        var data = gridster.serialize();
                        $.ajax({
                            url: '/ru/admin/industry/saveSponsorPosition/',
                            type: 'post',
                            dataType: 'json',
                            data: {data:JSON.stringify(data), model_id:model_id},
                            success:function(data){

                            }
                        });
                    }
                }
            }).data('gridster');
        });
    });
</script>
<?php
echo TbHtml::linkButton('Назад', array(
    'class' => 'btn pull-right btn-warning',
    'style' => 'margin:30px 50px 0px 0px;',
    'url' => $this->createUrl('industry/default/save', array('id' => $model->id)),
));
?>
<div class="gridster">
    <ul>
        <?php
            $row = 1; //дефолтная строка (Нужна в случае если у индустрии отсутствуют сохранённые позиции спонсоров ($model->sponsorPos))
            $col= 0;//дефолтная колонка (Нужна в случае если у индустрии отсутствуют сохранённые позиции спонсоров ($model->sponsorPos))
            $currentSponsor = 0;//Для переборки сохранённого массива позиций спонсоров
            foreach($model->industryHasMassMedia as $industryHasMassMedia){
                $col++;
                if($col % 6 == 0){
                    $col = 1;
                    $row++;
                }
                if(!empty($sponsorPos)){
                    $positionArr = json_decode($sponsorPos);

                    $col = $positionArr[$currentSponsor]->col;
                    $row = $positionArr[$currentSponsor]->row;

                    $currentSponsor++;
                }
                $sizex = 1;
                $sizey = 1;
                $isGeneral = $industryHasMassMedia->isGeneral;
                if(isset($isGeneral) && !empty($isGeneral) && ($isGeneral == 2)){
                    $sizex = 2;
                    $sizey = 2;
                }

                $ajaxButton = '';

                if(!empty($industryHasMassMedia)){
                    $ajaxButton = TbHtml::ajaxButton(
                        Yii::t('IndustryModule.industry', ($isGeneral==2)?'Обычный':'Генеральный'),
                        $this->createUrl('toggleGeneral'),
                        array(
                            'dataType'=>'json',
                            'type' => 'post',
                            'data' => array(
                                    'model_id' => $industryHasMassMedia->id,
                                    'isGeneral' => ($isGeneral==2)?1:2,
                            ),
                            'success' => 'function(res){
                                if(res.success){
                                    location.reload();
                                }
                            }',
                        ),
                        array(
                            'color' => ($isGeneral==2)?TbHtml::BUTTON_COLOR_WARNING:TbHtml::BUTTON_COLOR_SUCCESS,
                            'size' => TbHtml::BUTTON_SIZE_MINI,
                        )
                    );
                }

                echo H::wrapGridsterElement(
                    TbHtml::image(
                        H::getImageUrl($industryHasMassMedia->massMedia->getMainImage(), 'file', 'noImage', '300-168_'),
                        $industryHasMassMedia->massMedia->name,
                        array(
                            'title' => $industryHasMassMedia->massMedia->name,
                        )
                    ).$ajaxButton,$row, $col, $sizex, $sizey
                );
            }
        ?>
    </ul>
</div>
