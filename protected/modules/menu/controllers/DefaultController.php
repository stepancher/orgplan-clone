<?php
/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    
    public function actionSetCollapsed($status)
    {
        Yii::$app->session->set('layout-collapsed', $status);
    }
}