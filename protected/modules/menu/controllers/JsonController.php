<?php

class JsonController extends Controller
{

    protected $dump = FALSE;

    public function actionDump($langId){
        $this->dump = TRUE;
        $this->actionGetData($langId);
    }

    public function actionGetLanguage($url){

        $request = new ZHttpRequest();
        $request->setRequestUri($url);

        $res = array();
        foreach(Yii::app()->params['translatedLanguages'] as $lang => $value) {
            $res[$lang] = Yii::app()->urlManager->switchLang($request, $lang);
        }

        echo $this->addCallback($res);
    }

    public function actionGetData($langId)
    {
        $json = array(
            'header'=> array(
                'nav'=> array(
                    'header'=> Yii::t('leftMenu', 'Catalog'),
                    'links'=> [
                        'fairs' => Yii::t('search','Fairs'),
                        'venues'=> Yii::t('search','Exhibition Complexes'),
                        'industries'=> Yii::t('search','Industries'),
                        'regions'=> Yii::t('search','Regions'),
                        'prices'=> Yii::t('search','Prices'),
                    ]
                ),
                'lang'=> array(
                    'header' => "RUS",
                    'list' => [
                        array(
                            'name'=> "Russian",
                            'shortCode'=> "ru",
                            'shortName'=> "RUS"
                        ),
                        array(
                            'name'=> "English",
                            'shortCode'=> "en",
                            'shortName'=> "ENG"
                        )
                    ]
                ),
                'user' => $this->getUserLinks(),
                'blogLabel'=> Yii::t('leftMenu', 'Blog'),
                'blogLink'=> Yii::app()->createUrl('blog'),
                'profileLabel'=> Yii::t('leftMenu', 'profile'),
                'profileLink'=> $this->getProfileLink(),
                'loginLabel'=> Yii::t('leftMenu', 'Enter'),
                'logoutLabel'=> Yii::t('leftMenu', 'logout'),
            ),
            'sidebar'=> $this->getUserCabinetLinks(),
            'defaultModal' => array(
                'header' => Yii::t('leftMenu', 'Authorization'),
                'text' => Yii::t('leftMenu', 'Authorization needed'),
                'ok' => Yii::t('leftMenu', 'Enter'),
                'cancel' => Yii::t('leftMenu', 'Cancel'),
            ),
        );

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo $this->addCallback($json);
    }

    public function getProfileLink(){
        $link = Yii::app()->createUrl('/profile');

        if(
            Yii::app()->user->role == User::ROLE_ORGANIZERS
        ){
            $link = '/acamar/'.Yii::app()->language.'/organizer';
        }

        return $link;
    }

    public function getUserLinks(){

        if(Yii::app()->user->isGuest){
            $links = array(
                'auth'=> FALSE,
                'profileLabel'=> Yii::t('system','Log in'),
                'exitLabel'=> Yii::t('system','Registration'),
            );
        }else{
            $links = array(
                'auth'=> TRUE,
                'profileLabel'=> Yii::t('site','profile'),
                'exitLabel'=> Yii::t('system','Exit'),
            );
        }

        return $links;
    }

    public function getUserCabinetLinks(){

        $links = array();

        $isExponent = Yii::app()->user->role == User::ROLE_EXPONENT;
        $isOrganizer = Yii::app()->user->role == User::ROLE_ORGANIZERS;
        $isExponentSimple = Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE;
        $isAdmin = Yii::app()->user->GetState('role') == User::ROLE_ADMIN;
        $isExponentSimple = Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE;
        $isRUEF = Yii::app()->user->role == USER::ROLE_RUEF;

        $office = 'workspace';

        if (User::ROLE_ORGANIZERS == Yii::app()->user->getRole()) {
            $office = 'backoffice';
        }

        $userCabinetUrl = '';
        if(Yii::app()->user->role == User::ROLE_ORGANIZERS){
            $userCabinetUrl = Yii::app()->createUrl('myFairs');
        }

        if(Yii::app()->user->role == User::ROLE_EXPONENT){
            $userCabinetUrl = Yii::app()->createUrl('myFairs');
        }

        if(Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE){
            $userCabinetUrl = Yii::app()->createUrl('myFairs');
        }

        if(
            !Yii::app()->user->isGuest &&
            (
                $isExponent ||
                $isOrganizer
            )
        ){
            $links[] = array(
                'name'=> Yii::t('leftMenu','My fairs'),
                'link'=> $userCabinetUrl,
                'type'=> "big"
            );
        }

        if(
            (
                isset(Yii::$app->session['myFairViewId']) &&
                !empty($fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId']))
            ) &&
            (
                (
                    Yii::app()->controller->id == 'workspace' &&
                    Yii::app()->controller->action->id == 'index'
                ) ||
                $isExponent ||
                $isOrganizer ||
                $isAdmin ||
                $isExponentSimple
            )
        ){
            $link = Yii::app()->createUrl($office.'/panel/panel' , ['fairId' => $fair->id]);
            if(!empty($fair->proposalVersion)){
                $link = '/'.$fair->proposalVersion.Yii::app()->createUrl($office.'/panel/panel' , ['fairId' => $fair->id]);
            }

            $links[] = array(
                'name' => $fair->loadName(),
                'link'=> $link,
                'type'=> "focused",
            );
        }

        if(Yii::$app->session['myFairViewId']){

            $fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId']);

            $proposalVersion = '';
            if(!empty($fair->proposalVersion)){
                $proposalVersion = '/'.$fair->proposalVersion.'/'.Yii::app()->language.'/';
            }

            $links[] = array(
                'name'=> Yii::t('leftMenu','About fair'),
                'link' => Yii::app()->createUrl('fair/html/view', array('urlFairName' => $fair->shortUrl, 'langId' => Yii::app()->language)),
                'type'=> "inner"
            );
        }

        if(
            (
                !Yii::app()->user->isGuest &&
                Yii::app()->user->role != User::ROLE_USER &&
                Yii::app()->user->role != User::ROLE_EXPONENT_SIMPLE &&
                isset(Yii::$app->session['myFairViewId']) &&
                MyFair::isExponentFair(Yii::$app->session['myFairViewId']) &&
                Yii::app()->user->role == User::ROLE_EXPONENT
            ) || Yii::app()->user->role == User::ROLE_ORGANIZERS
        ){
            if(
                !empty($fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId'])) &&
                !empty($fair->proposalVersion)
            ){
                $links[] = array(
                    'name'=> Yii::t('leftMenu','Calendar'),
                    'link'=> '/'.$fair->proposalVersion.Yii::app()->createUrl('calendar', ['fairId' => Yii::$app->session['myFairViewId']]),
                    'type'=> "inner",
                );
            }
        }

        if(
            !Yii::app()->user->isGuest &&
            Yii::app()->user->role == User::ROLE_ORGANIZERS &&
            isset(Yii::$app->session['myFairViewId']) &&
            MyFair::isOrganizerOfFair(Yii::$app->session['myFairViewId'])
        ){
            $allExponentsLink = Yii::app()->createUrl('organizer/exponent');
            if(
                !empty($fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId'])) &&
                !empty($fair->proposalVersion)
            ){
                $allExponentsLink = '/'.$fair->proposalVersion.Yii::app()->createUrl('organizer/exponent');
            }

            $links[] = array(
                'name'=> Yii::t('leftMenu','all exponents'),
                'link'=> $allExponentsLink,
                'type'=> "inner"
            );
        }

        if(
            isset(Yii::$app->session['myFairViewId']) &&
            (
                Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE ||
                (
                    Yii::app()->user->role == User::ROLE_EXPONENT &&
                    !MyFair::isExponentFair(Yii::$app->session['myFairViewId'])
                )
            )
        ){
            $links[] = array(
                'name'=> Yii::t('leftMenu','Calendar'),
                'link'=> Yii::app()->createUrl('calendarExponent', ['fairId' => Yii::$app->session['myFairViewId']]),
                'type'=> "inner"
            );
        }

        if( isset(Yii::$app->session['myFairViewId']) &&
            (
                (
                    Yii::app()->user->role == User::ROLE_EXPONENT &&
                    MyFair::isExponentFair(Yii::$app->session['myFairViewId'])
                ) ||
                Yii::app()->user->role == User::ROLE_ORGANIZERS
            )
        ){
            $proposalsLink = Yii::app()->createUrl($office . '/list/requests', ['fairId' => Yii::$app->session['myFairViewId']]);

            if(
                !empty($fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId'])) &&
                !empty($fair->proposalVersion)
            ){
                $proposalsLink = '/'.$fair->proposalVersion.Yii::app()->createUrl($office . '/list/requests', ['fairId' => Yii::$app->session['myFairViewId']]);
            }

            $links[] = array(
                'name'=> Yii::t('leftMenu','Proposals'),
                'link'=> $proposalsLink,
                'type'=> "inner"
            );
        }

        if(
            isset(Yii::$app->session['myFairViewId']) &&
            Yii::app()->user->role == User::ROLE_ORGANIZERS
        ){
            $reportsLink = Yii::app()->createUrl('backoffice/list/reports', ['id' => Yii::$app->session['myFairViewId']]);

            if(
                !empty($fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId'])) &&
                !empty($fair->proposalVersion)
            ){
                $reportsLink = '/'.$fair->proposalVersion.Yii::app()->createUrl('backoffice/list/reports', ['id' => Yii::$app->session['myFairViewId']]);
            }

            $links[] = array(
                'name'=> Yii::t('leftMenu','Reports'),
                'link'=> $reportsLink,
                'type'=> "inner"
            );
        }

        if(
            isset(Yii::$app->session['myFairViewId']) &&
            $office == 'workspace' &&
            Yii::app()->user->role != User::ROLE_ORGANIZERS &&
            !MyFair::isExponentFair(Yii::$app->session['myFairViewId'])
        ){
            $links[] = array(
                'name'=> Yii::t('leftMenu','Orgplan'),
                'link'=> Yii::app()->createUrl('/exponentPrepare', ['fairId' => Yii::$app->session['myFairViewId']]),
                'type'=> "inner"
            );
        }

        if(
            (
                Yii::app()->controller->id == 'workspace' &&
                Yii::app()->controller->action->id == 'index'
            ) ||
            isset(Yii::$app->session['myFairViewId']) &&
            $office == 'workspace'
        ){

            $documentsLink = Yii::app()->createUrl($office . '/list/documents', ['id' => Yii::$app->session['myFairViewId']]);
            $photoreportLink = Yii::app()->createUrl('workspace/photo/index', array('fairId' => Yii::$app->session['myFairViewId']));
            if(
                !empty($fair = Fair::model()->findByPk(Yii::$app->session['myFairViewId'])) &&
                !empty($fair->proposalVersion)
            ){
                $documentsLink = '/'.$fair->proposalVersion.Yii::app()->createUrl($office . '/list/documents', ['id' => Yii::$app->session['myFairViewId']]);
                $photoreportLink = '/'.$fair->proposalVersion.Yii::app()->createUrl('workspace/photo/index', array('fairId' => Yii::$app->session['myFairViewId']));
            }

            $links[] = array(
                'name'=> Yii::t('leftMenu','My documents'),
                'link'=> $documentsLink,
                'type'=> "inner"
            );

            if(
                MyFair::isExponentFair(Yii::$app->session['myFairViewId']) &&
                Yii::$app->session['myFairViewId'] == 184
            ){
                $links[] = array(
                    'name'=> Yii::t('leftMenu','Photoreport'),
                    'link'=> $photoreportLink,
                    'type'=> "inner"
                );
            }
        }

        if(
            !Yii::app()->user->isGuest && (Yii::app()->user->role == User::ROLE_EXPONENT || Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE)
        ){
            $links[] = array(
                'name'=> Yii::t('leftMenu','Calendar'),
                'link'=> Yii::app()->createUrl('/calendarExponent'),
                'type'=> "big"
            );
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $links[] = array(
                'name'=> Yii::t('leftMenu','My checklists'),
                'link'=> Yii::app()->params['expoplannerUrl'].'/lists',
                'type'=> "big"
            );
        }

        if($isRUEF){

            $links[] = array(
                'name' => Yii::t('leftMenu', 'rus fairs'),
                'link' => Yii::app()->createUrl('/RUEF/fairReport/report'),
                'type' => 'inner',
            );

            $links[] = array(
                'name' => Yii::t('leftMenu', 'rus industries'),
                'link' => Yii::app()->createUrl('/RUEF/industryReport/report'),
                'type' => 'inner',
            );

            $links[] = array(
                'name' => Yii::t('leftMenu', 'rus districts'),
                'link' => Yii::app()->createUrl('/RUEF/districtReport/report'),
                'type' => 'inner',
            );

            $links[] = array(
                'name' => Yii::t('leftMenu', 'rus regions'),
                'link' => Yii::app()->createUrl('/RUEF/regionReport/report'),
                'type' => 'inner',
            );

            $links[] = array(
                'name' => Yii::t('leftMenu', 'cis fairs'),
                'link' => Yii::app()->createUrl('/RUEF/fairReport/CISReport'),
                'type' => 'inner',
            );

            $links[] = array(
                'name' => Yii::t('leftMenu', 'cis industries'),
                'link' => Yii::app()->createUrl('/RUEF/industryReport/CISReport'),
                'type' => 'inner',
            );
        }
        
        return $links;
    }

    private function addCallback($json)
    {
        return (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }
}