<?php
/**
 * @var MassMediaController $this
 * @var MassMedia $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

//$cs->registerScriptFile($publishUrl . '/js/blog/confirm-delete-article.js', CClientScript::POS_END);
?>

<?php
echo TbHtml::linkButton('Назад', array(
	'class' => 'btn pull-right btn-warning',
	'style' => 'margin:10px 50px 0px 0px;',
	'url' => $this->createUrl('MassMedia/index'),
));
?>

<div class="col-md-11">
	<?php
	$formClass = get_class($model) . 'Form';
	Yii::import('massMediaAdmin.widgets.' . $formClass);
	$ctrl = $this;
	$ctrl->widget('ext.widgets.FieldSetView', array(
		'header' => Yii::t($ctrl->getId(), $model->isNewRecord ? 'Form header create' : 'Form header update'),
		'items' => array(
			array(
				'ext.widgets.FormView',
				'form' => new $formClass(
					array(
						'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
						'enctype' => 'multipart/form-data',
						//'elementGroupName' => '[]'
					),
					$model, $ctrl
				),
				'items' => array(
					array(
						'ext.widgets.HtmlView',
						'content' => function ($owner) use ($model) {
							if ($model->image && $model->image->getFileUrl()) {
								echo TbHtml::thumbnails(
									H::getThumbnailsArray(
										array($model), $model->id, 'image', null,
										TbHtml::linkButton(
											Yii::t($this->getId(), 'delete'),
											array(
												'color' => TbHtml::BUTTON_COLOR_DANGER,
												'size' => TbHtml::BUTTON_SIZE_MINI,
												'url' => $owner->createUrl('deleteMainImage', array('id' => $model->id, 'fileId' => '{fileId}'))
											)
										)
									)
								);
							} else {
								$owner->widget('ext.widgets.FieldSetView', array(
										'header' => Yii::t($this->getId(), 'download Image (title picture)'),
										'items' => array(
											array(
												'CMultiFileUpload',
												'model' => $model,
												'htmlOptions' => array(
													'style' => 'margin-left:80px'
												),
												'max' => 1,
												'attribute' => 'image',
												'duplicate' => Yii::t($this->getId(), 'this file already exists!'),
												'accept' => 'gif|jpg|jpeg|png',
												'denied' => Yii::t($this->getId(), 'Invalid file type'),
											)
										)
									)
								);
							}
						},
					),

					array(
						'ext.widgets.ActionsView',
						'items' => array(
							'submit' => array(
								'type' => TbHtml::BUTTON_TYPE_SUBMIT,
								'label' => Yii::t($this->getId(), 'Применить'),
								'attributes' => array(
									'color' => TbHtml::BUTTON_COLOR_SUCCESS
								)
							),
							'cancel' => array(
								'type' => TbHtml::BUTTON_TYPE_LINK,
								'label' => Yii::t($this->getId(), 'Отмена'),
								'attributes' => array(
									'url' => $this->createUrl('index'),
									'color' => TbHtml::BUTTON_COLOR_DANGER
								)
							),
							'delete' => array(
								'type' => TbHtml::BUTTON_TYPE_LINK,
								'label' => Yii::t($this->getId(), 'Удалить'),
								//'visible' => !$model->isNewRecord,
								'attributes' => array(
									'url' => $this->createUrl('deleteMassMedia', ['id' => $model->id]),
									'color' => TbHtml::BUTTON_COLOR_WARNING,
									'class' => 'delete-article',
								)
							)
						)
					),
				),
			),
		)
	));
	?>
</div>

