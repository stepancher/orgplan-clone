<?php
///* @var $this ProductController */
///* @var $model Product */
///* @var $form CActiveForm */
//
//
//
//$app = Yii::app();
//$am = $app->getAssetManager();
//$cs = $app->getClientScript();
//$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
//
//$cs->registerScriptFile($publishUrl . '/js/blog/confirm-delete-article.js', CClientScript::POS_END);
//?>
<!---->
<?php
	echo TbHtml::linkButton('Назад', array(
		'class' => 'btn pull-right btn-warning',
		'style' => 'margin:10px 50px 0px 0px;',
		'url' => $this->createUrl('index'),
	));
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'MassMedia-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'action' => $this->createUrl('MassMedia/save'),
));

?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>85,'maxlength'=>500, 'style' => 'width:400px;')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description', array('style'=>'width:400px;height:140px;')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url'); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<?php
		echo TbHtml::activeLabel($model, 'active');
		$this->widget('vendor.anggiaj.eselect2.ESelect2',array(
			'model'=>$model,
			'attribute'=> 'active',
			'data' =>array('0'=>'Выкл.', '1' => 'Вкл.'),
			'htmlOptions'=>array(
				'style' => 'width:100px;margin-bottom:20px;',
			),
		));
	?>

	<?php
		if ($model->image && $model->image->getFileUrl()) {
			echo TbHtml::thumbnails(
				H::getThumbnailsArray(
					array($model), $model->id, 'image', null,
					TbHtml::linkButton(
						Yii::t($this->getId(), 'delete'),
						array(
							'color' => TbHtml::BUTTON_COLOR_DANGER,
							'size' => TbHtml::BUTTON_SIZE_MINI,
							'url' => $this->createUrl('delete', array('id' => $model->id, 'fileId' => '{fileId}'))
						)
					)
				)
			);
		} else {
			$this->widget('ext.widgets.FieldSetView', array(
					'header' => Yii::t($this->getId(), 'download Image (title picture)'),
					'items' => array(
						array(
							'CMultiFileUpload',
							'model' => $model,
							'htmlOptions' => array(
								'style' => 'margin-left:80px'
							),
							'max' => 1,
							'attribute' => 'image',
							'duplicate' => Yii::t($this->getId(), 'this file already exists!'),
							'accept' => 'gif|jpg|jpeg|png',
							'denied' => Yii::t($this->getId(), 'Invalid file type'),
						)
					)
				)
			);
		}
	?>
	<div style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>


