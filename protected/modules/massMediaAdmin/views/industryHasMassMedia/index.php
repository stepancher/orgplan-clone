<?php
$ctrl = $this;
$ctrl->widget('ext.widgets.FieldSetView', array(
    'header' => Yii::t($this->getId(), 'Категории'),
    'items' => array(
        array(
            'ext.widgets.ActionsView',
            'items' => array(
                'massMedia' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'СМИ'),
                    array(
                        'url' => $this->createUrl('MassMedia/index'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                        'style' => 'color:white !important;',
                    )
                ),
                'productsCreate' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Создание СМИ'),
                    array(
                        'url' => $this->createUrl('MassMedia/save'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                        'style' => 'color:white !important;',
                    )
                ),
            ), 
        ),
        array(
            'massMediaAdmin.widgets.' . get_class($model) . 'GridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{update}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'update' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('save', array("id" => $data->id));
                            },
                            'options' => array('title' => 'редактировать', 'class' => 'btn btn-small btn-info'),
                        ),
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('deleteIndustryHasMM', array("id" => $data->id));
                            },
                            'options' => array('title' => 'удалить', 'class' => 'btn btn-small btn-danger'),
                        ),
                    )
                )
            )
        ),
    )
));
?>