<?php
/**
 * @var CWebApplication $app
 * File MassMediaForm.php
 */
Yii::import('ext.helpers.ARForm');
$app = Yii::app();
//Yii::app()->bootstrap->register();
$cs = $app->getClientScript();
$am = $app->getAssetManager();

$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/css/WhRedactor/whRedactor.css');


/**
 * Class MassMedia*/
class MassMediaForm extends TbForm
{
	/**
	 * Префикс в аттрибут name (например: "[]") для обработки полей формы с name=MassMedia[]['login']
	 * @var array
	 */
	public $elementGroupName = '';

	/**
	 * Инициализация формы
	 */
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();
		/** @var MassMedia $model */
		$model = $this->getModel();

        $this->elements = CMap::mergeArray(
            array_flip(array_keys($this->initElements())),
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );
		parent::init();
	}

	/**
	 * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=MassMedia[]['login']
	 * @param string $name
	 * @param TbFormInputElement $element
	 * @param bool $forButtons
	 */
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	 * Инициализация элементов формы
	 * @return array
	 */
	public function initElements()
	{
		$ctrl = $this->getOwner();
		$fields = \frontend\widgets\MassMedia\ActiveForm::fields($this->getModel());

		return array(
			'id' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
				'visible' => false
			),
			'name' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'description' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
				'input' => $fields->fieldDescription()
			),
            'url' => array(
                'groupOptions' => array(
                    'class' => 'span12',
                ),
            ),
			'active' => array(
				'groupOptions' => array(
                'class' => 'span12',
                ),
                'type' => TbHtml::INPUT_TYPE_DROPDOWNLIST,
                'items' => H::switcherArray(),
				'visible' => true
			),
		);
	}
}