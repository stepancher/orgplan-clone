<?php

class MassMediaAdminModule extends CWebModule
{
    public function init(){

        $this->setImport(array(
            'vendor.anggiaj.eselect2.ESelect2.*',
            'massMediaAdmin.widgets.*',
        ));
    }
}