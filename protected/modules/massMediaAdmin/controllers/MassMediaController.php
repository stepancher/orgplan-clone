<?php
class MassMediaController extends Controller{

    /**
     * @var MassMedia $model
     */
    public $model = 'MassMedia';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){

        $model = MassMedia::model();

        $this->render('index', array('model'=> $model));
    }

    /**
     * @param $id
     * @throws CDbException
     */
    public function actionDeleteMassMedia($id){
        /** @var MassMedia $massMedia */
        if (($massMedia = MassMedia::model()->findByPk($id)) !== null){
            $this->deleteFiles($id);

            if ($massMedia->delete()) {
                $this->redirect($this->createUrl('index'), array('model' => MassMedia::model()));
            }
        }
    }

    public function actionCreate(){

        $model=new MassMedia();

        if(isset($_POST['MassMedia'])){
            $this->save();
            $model=new MassMedia();
            $model->attributes=$_POST['MassMedia'];

            if($model->validate()){

                $model->save();
                $this->redirect(array('index'));
            }
        }

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    /**
     * @TODO Вынести удаление всех файлов и папок в ObjectFile
     * @param $id
     */
    public function deleteFiles($id){

        if (($model = MassMedia::model()->findByPk($id)) !== null) {
            $files = MassMediaHasFile::model()->findAllByAttributes(array('massMediaId' => $model->id));
            foreach ($files as $file) {
                $objectFile = ObjectFile::model()->findByPk($file->fileId);
                if (null !== $objectFile){
                    foreach(MassMedia::getSizes() as $x => $y){
                        $prefix = $x.'-'.$y.'_';
                        if(file_exists($objectFile->getFilePath(false, $prefix))){
                            unlink($objectFile->getFilePath(false, $prefix));
                        }
                    }
                    if(file_exists($objectFile->getFilePath())){
                        unlink($objectFile->getFilePath());
                    }
//                    $directory = $objectFile->getDirPath(true);
//                    if(is_dir($directory)){
//                        rmdir($directory);
//                    }
//                    $sub_directory = str_replace('/mainImage', '',$objectFile->getDirPath(true));
//
//                    if(is_dir($sub_directory)){
//                        rmdir($sub_directory);
//                    }
                    $objectFile->deleteFile($model);
                }
            }
        }
    }

    /**
     * @param $id
     * @param $fileId
     */
    public function actionDeleteMainImage($id, $fileId){

        /** @var MassMedia $model */
        if (($model = MassMedia::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }
        $model->imageId = NULL;
        $model->save();

        $objectFile = ObjectFile::model()->findByPk($fileId);
        if (null == $objectFile) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
        } else {
            Yii::app()->user->setFlash('success', 'Файл удален.');

//            $objectFile->deleteWithDimensions();
            $objectFile->deleteFile($model);
        }
        $this->redirect($this->createUrl('save', array('id' => $id)));
    }

    public function actionSave($id = null)
    {
        /** @var MassMedia $model */
        if (null === $id || ($model = MassMedia::model($this->model)->findByPk($id)) === null){
            $model = new $this->model();
        }

        $ctrl = $this;

        $records = MassMedia::multiSave(
            $_POST,
            array(
                array(
                    'name' => 'model',
                    'class' => get_class($model),
                    'record' => $model
                ),
                function ($records) use ($ctrl, $id) {

                    /** @var MassMedia $massMedia */
                    $massMedia = $records['model'];

                    $logo = CUploadedFile::getInstances(MassMedia::model(), 'image');

                    if (!empty($logo)) {
                        $differentOptions = [
                            'sizes' => MassMedia::getSizes()
                        ];
                        if ($massMedia->image) {
                            $path = $massMedia->image->getDirPath(true);
                            $objId = $massMedia->image->saveFile($logo[0], true, true, $differentOptions);
                            if ($objId) {
                                $MMHF = new MassMediaHasFile();
                                $MMHF->massMediaId = $massMedia->id;
                                $MMHF->fileId = $objId;
                                $MMHF->save();
                                unset($path);
                            }
                        } else {
                            $objFile = new ObjectFile();
                            $objFile->type = ObjectFile::TYPE_MASS_MEDIA_IMAGE;
                            $objFile->setOwnerId($massMedia->id);
                            $objId = $objFile->saveFile($logo[0], true, true, $differentOptions);
                            if ($objId) {
                                $MMHF = new MassMediaHasFile();
                                $MMHF->massMediaId = $massMedia->id;
                                $MMHF->fileId = $objId;
                                $MMHF->save();
                            }

                            $massMedia->imageId = $objFile->id;
                        }
                        $massMedia->save(false);
                    }

                    IRedactor::createItems(get_class($massMedia));
                    IRedactor::clearUploadFiles();

                    foreach (CUploadedFile::getInstances(MassMedia::model(), 'images') as $file) {
                        ObjectFile::createFile($file, $massMedia, ObjectFile::TYPE_MASS_MEDIA_IMAGES, ObjectFile::EXT_IMAGE);
                    }
                }
            ),
            array(
                function ($records) use ($ctrl) {
                    Yii::app()->user->setFlash('success', 'Информация сохранена.');
                    $ctrl->redirect($ctrl->createUrl('save', array('id' => $records['model']->id)));
                }
            )
        );

        /** Очистка файлов временной директории, если все модели прошли валидацию */
        IRedactor::clearUploadFiles($records);
        $this->render('create', $records);
    }

}