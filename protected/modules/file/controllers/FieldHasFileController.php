<?php

/**
 * Class FieldHasFileController
 */
class FieldHasFileController extends Controller
{
    /**
     * @var FieldHasFile $model
     */
    public $model = 'FieldHasFile';

    /**
     * Удаление файла
     * @param $id
     */
    public function actionRemove($id)
    {
        $model = FieldHasFile::model()->findByAttributes(array('fileId' => $id));
        if ($model && $model->deleteWithFile()) {
            echo 0;
        } else {
            echo 1;
        }
    }

    public function actionDownloadFile($id)
    {
        $model = FieldHasFile::model()->findByAttributes(array('fileId' => $id));
        if ($model && $model->file) {
            Yii::app()->getRequest()->sendFile($model->file->label, file_get_contents($model->getFilePath()));
        }
        if (!Yii::app()->getRequest()->isAjaxRequest) {
            $this->redirect(Yii::app()->createUrl(Yii::app()->getRequest()->urlReferrer));
        }
    }
}