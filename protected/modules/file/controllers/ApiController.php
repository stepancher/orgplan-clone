<?php

class ApiController extends Controller {

    protected $authenticated = false;

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function actionAuth($SystemId, $UserName, $Password) {

        return true;
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        echo json_encode(true);
    }

    /**
     * @param integer $userId
     * @param string $type
     * @param string $ifFalse
     * @return string
     * @soap
     */
   public function actionGetImageUrl($userId, $type, $ifFalse){
       echo json_encode(H::getImageUrl(User::model()->findByPk($userId), $type, $ifFalse));
   }

    /**
     * @return array
     * @soap
     */
    public function actionGetConstants(){
        $oClass = new ReflectionClass('ObjectFile');
        echo json_encode($oClass->getConstants());
    }

    /**
     * @param integer $objectFileId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetFileProperties($objectFileId, array $properties){

        if(!empty($file = ObjectFile::model()->findByPk($objectFileId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($file->{$name}) &&
                    !empty($file->{$name})
                ){
                    $res[$name] = $file->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }
}