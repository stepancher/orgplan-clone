<?php
/**
 * Class DefaultController
 */
class DefaultController extends Controller
{

    /**
     * @param $modelName
     * @throws CException
     */
    public function actionImageUpload($modelName)
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        $image = CUploadedFile::getInstanceByName('file');

        $filename = 'description_image_' . md5(time()) . '.' . $image->extensionName;

        $image->saveAs(IRedactor::getPathToTemp() . $filename);

        IRedactor::setUploadFiles(IRedactor::getPathToTemp(), $filename);

        /**
         * @var CImageComponent $CIComponent
         */
        $CIComponent = $app->image;
        $image_open = $CIComponent->load(IRedactor::getPathToTemp() . $filename);
        if (isset($image_open)) {
            if ($image_open->width > $image_open->height) $dim = Image::HEIGHT;
            else $dim = Image::WIDTH;
            $image_open->resize(100, 100, $dim)->crop(100, 100);
        }
        $array = array(
            'filelink' => $this->createUrl('getFileContent',
                array(
                    'modelName' => $modelName,
                    'fileName' => $filename,
                )
            ),
            'filename' => $filename
        );
        echo stripslashes(json_encode($array));
    }

    /**
     * @param $modelName
     * @param $fileName
     */
    public function actionGetFileContent($modelName, $fileName)
    {
        $modelName = basename($modelName);
        $fileName = basename($fileName);

        header('Content-Type: image/jpeg');

        $filePath = IRedactor::getPathToTemp() . $fileName;

        if (!is_file($filePath)) {
            $filePath = IRedactor::getPathToUpload($modelName) . $fileName;
        }

        if(is_file($filePath)) {
            echo file_get_contents($filePath);
        }
    }

    public function actionDownloadFile($id)
    {
        $model = ObjectFile::model()->findByPk($id);
        if ($model && $model->getFilePath() && is_file($model->getFilePath(true, DIRECTORY_SEPARATOR))) {
            Yii::app()->getRequest()->sendFile($model->label, file_get_contents($model->getFilePath(true, DIRECTORY_SEPARATOR)));
        }
        if (!Yii::app()->getRequest()->isAjaxRequest) {
            $this->redirect(Yii::app()->getRequest()->urlReferrer);
        }
    }

    public function actionDownloadFieldFile($id)
    {
        $model = FieldHasFile::model()->findByAttributes(array('fileId' => $id));
        if ($model && $model->file && is_file($model->getFilePath())) {
            Yii::app()->getRequest()->sendFile($model->file->label, file_get_contents($model->getFilePath()));
        }
        if (!Yii::app()->getRequest()->isAjaxRequest) {
            $this->redirect(Yii::app()->getRequest()->urlReferrer);
        }
    }
}