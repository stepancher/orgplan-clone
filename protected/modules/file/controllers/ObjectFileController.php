<?php

/**
 * Class ObjectFileController
 */
class ObjectFileController extends Controller
{
    /**
     * @var ObjectFile $model
     */
    public $model = 'ObjectFile';

    /**
     * Стандартные actions перенаправляют на 404.
     */
    public function actionIndex()
    {
        throw new CHttpException(404, Yii::t('FileModule.file','Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }

    public function actionRemove($id, $cls)
    {
        /** @var StandHasFile|AR $relation */
        if (($relation = AR::model($cls . 'HasFile')->findByAttributes(['fileId' => $id])) &&
            is_file($relation->file->getFilePath(true, DIRECTORY_SEPARATOR)) &&
            unlink($relation->file->getFilePath(true, DIRECTORY_SEPARATOR)) &&
            $relation->file->delete() &&
            $relation->delete()
        ) {
            echo 0;
        } else {
            echo 1;
        }
    }
}