<?php

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @var RegionInformation $model
     */
    public $model = 'RegionInformation';

    /**
     * @param $countryId
     * Подборка Округов по Стране
     */
    public function actionGetDistrictsByCountry($countryId)
    {
        /** @var CWebApplication $app */
        $districts = District::model()->findAllByAttributes(array('countryId' => $countryId));
        if ($countryId) {
            $data = array();
            $data[0]['id'] = '';
            $data[0]['name'] = 'Select';

            foreach ($districts as $key => $districtsByCounty) {
                $data[$key + 1]['id'] = $districtsByCounty->id;
                $data[$key + 1]['name'] = $districtsByCounty->name;
            };
            echo json_encode($data);
        }
    }

    /**
     * @param $districtId
     * Подборка регионов по Округу
     */
    public function actionGetRegionsByDistrict($districtId)
    {
        /** @var CWebApplication $app */
        $regions = Region::model()->findAllByAttributes(array('districtId' => $districtId));

        if ($districtId) {
            $data = array();
            foreach ($regions as $key => $regionsByDistrict) {
                $data[$key]['id'] = $regionsByDistrict->id;
                $data[$key]['name'] = $regionsByDistrict->name;
            };
            echo json_encode($data);
        }
    }

    /**
     * @param null $id
     * Создание/редактирование региона
     */
    public function actionSave($id = null)
    {
        if (Yii::app()->user->getState('role') != User::ROLE_ADMIN) {
            $this->redirect(Yii::app()->request->getBaseUrl(true));
        }
        /** @var RegionInformation $model */
        /** @var CWebApplication $app */
        $app = Yii::app();
        if (null === $id || ($model = RegionInformation::model()->findByPk($id)) === null)
            $model = new $this->model();
        $ctrl = $this;
        $records = RegionInformation::multiSave(
            $_POST,
            array(
                array(
                    'name' => 'model',
                    'class' => get_class($model),
                    'record' => $model,
                ),
                function ($records) use ($ctrl, $model, $app) {
                    /** @var RegionInformation $regionInformation */

                    $regionInformation = $records['model'];

                    IRedactor::createItems(get_class($regionInformation));
                    IRedactor::clearUploadFiles();
                    $image = CUploadedFile::getInstances(RegionInformation::model(), 'logo');
                    if (!empty($image)) {
                        ObjectFile::createFile($image[0], $regionInformation, ObjectFile::TYPE_REGION_INFORMATION_LOGO, ObjectFile::EXT_IMAGE);
                    }
                    $logo = CUploadedFile::getInstances(RegionInformation::model(), 'mainImage');
                    if (!empty($logo)) {
                        ObjectFile::createFile($logo[0], $regionInformation, ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE, ObjectFile::EXT_IMAGE);
                    }


                    //Yii::app()->consoleRunner->run('RegionInformation --regionName="' . rawurlencode($regionInformation->region->name) . '"', true);

                    Yii::app()->user->setFlash('success', Yii::t('RegionInformationModule.regionInformation', 'The information is saved.'));
                    $ctrl->redirect($ctrl->createUrl('view', array('id' => $records['model']->id, 'langId' => Yii::app()->language)));
                }
            )
        );
        /** Очистка файлов временной директории, если все модели прошли валидацию */
        IRedactor::clearUploadFiles($records);
        $this->render('save', $records);
    }

    /**
     * Стандартные actions перенаправляют на 404.
     */
    public function actionIndex()
    {
        throw new CHttpException(404, Yii::t('RegionInformationModule.regionInformation', 'Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }

    public function actionViewRegion($urlRegionName){

        $sql = "
            SELECT 
                ri.id
            
            FROM tbl_regioninformation ri
            LEFT JOIN tbl_region r ON (ri.regionId = r.id)
            LEFT JOIN tbl_trregion trr ON (trr.trParentId = r.id AND trr.langId = :langId)
            
            WHERE
              trr.shortUrl = :regionShortUrl AND
              trr.langId = :langId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':langId' => Yii::app()->language,
            ':regionShortUrl' => $urlRegionName,
        ]);

        $region = $dataReader->read();

        if(empty($region['id'])){
            $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
        }

        $this->view($region['id']);
    }

    public function view($id){

        /** @var RegionInformation $model */
        if (($model = RegionInformation::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
        }

        $this->_pageTitle = $model->name;

        $pageSize = RegionInformation::PAGE_SIZE;

        $sql = "SELECT ri.id, f.id as fairId, trf.name AS fairName, trf.shortUrl AS fairShortUrl, f.rating AS fairRating,
                f.beginDate as fairBeginDate, f.endDate AS fairEndDate, GROUP_CONCAT(tri.name SEPARATOR '; ') AS industriesList,
                trr.name AS regionName, ri.id AS regionInformationId, trc.name AS cityName,
                IF(fi.squareNet IS NOT NULL, fi.squareNet, fi.squareGross) square
                
                FROM tbl_regioninformation ri
                LEFT JOIN tbl_region r ON r.id = ri.regionId
                LEFT JOIN tbl_trregion trr ON trr.trParentId = r.id AND trr.langId = :langId
                LEFT JOIN tbl_city c ON c.regionId = r.id
                LEFT JOIN tbl_trcity AS trc ON trc.trParentId = c.id AND trc.langId = :langId
                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                LEFT JOIN tbl_fair f ON f.exhibitionComplexId = ec.id
                LEFT JOIN tbl_trfair trf ON trf.trParentId = f.id AND trf.langId = :langId
                LEFT JOIN tbl_fairinfo fi ON fi.id = f.infoId
                LEFT JOIN tbl_fairhasindustry fhi ON fhi.fairId = f.id
                LEFT JOIN tbl_trindustry AS tri ON tri.trParentId = fhi.industryId AND tri.langId = :langId
                WHERE ri.id = :regionInformationId
                AND f.id IS NOT NULL
                AND f.active = :active
                AND YEAR(f.beginDate) = :YEAR
                GROUP BY f.id
                ORDER BY f.rating ASC, square DESC";

        $count = 0;
        $totalCountSql = "SELECT COUNT(*) AS count FROM ({$sql}) t";
        $totalCountDataProvider = new CSqlDataProvider(
            $totalCountSql,
            array(
                'params' => array(
                    ':regionInformationId' => $id,
                    ':langId' => Yii::app()->language,
                    ':active' => Fair::ACTIVE_ON,
                    ':YEAR' => Fair::getThisYear(),
                )
            )
        );

        $totalCount = $totalCountDataProvider->getData();

        if(isset($totalCount[0]['count'])){
            $count = $totalCount[0]['count'];
        }

        $dataProvider = new CSqlDataProvider($sql,
            array(
                'params' => array(
                    ':regionInformationId' => $id,
                    ':langId' => Yii::app()->language,
                    ':active' => Fair::ACTIVE_ON,
                    ':YEAR' => Fair::getThisYear(),
                ),
                'totalItemCount' => $count,
                'pagination'=>array(
                    'pageSize' => $pageSize,
                ),
            )
        );

        $this->render('view', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'pageSize' => $pageSize,
        ));
    }

    /**
     * @param $id
     * Просмотр информации о регионе
     */
    public function actionView($id)
    {
        $region = Region::model()->findByPk($id);

        if (empty($region)){
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
        }

        $this->redirect(Yii::app()->createUrl('regionInformation/default/viewRegion', array('urlRegionName' => $region->shortUrl)));
    }

    /**
     * @param $id
     * @param $fileId
     * Удаление файлов
     */
    public function actionDeleteFile($id, $fileId)
    {
        /** @var RegionInformation $model */
        if (($model = RegionInformation::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
        }

        $objectFile = ObjectFile::model()->findByPk($fileId);
        if (null == $objectFile) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
        } else {
            Yii::app()->user->setFlash('success', 'Файл удален.');
            $objectFile->deleteFile($model);
        }
        $this->redirect($this->createUrl('save', array(
            'id' => $id,
            'langId' => Yii::app()->language
        )));
    }

    /**
     * @param $id
     * @param null $class
     * Изменение статуса Активация/деактивация
     */
    public function actionActive($id, $class = null)
    {
        /**
         * @var RegionInformation $model
         * @var CWebApplication $app
         */
        $app = Yii::app();
        if (($model = RegionInformation::model()->findByPk($id)) !== null) {
            if ($model->type == UsefulInformation::STATUS_HIDDEN || $model->type == UsefulInformation::STATUS_ARCHIVE) {
                $model->type = UsefulInformation::STATUS_PUBLISHED;
                $app->user->setFlash('success', 'Обьект опубликован');
            } elseif ($model->type == UsefulInformation::STATUS_PUBLISHED || $model->type == UsefulInformation::STATUS_ARCHIVE) {
                $model->type = UsefulInformation::STATUS_HIDDEN;
                $app->user->setFlash('success', 'Обьект скрыт');
            }
            $model->save(false);
        }
        $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
    }

    /**
     * @param $id
     * Статус добавить в архив
     */
    public function actionArchive($id)
    {
        /**
         * @var RegionInformation $model
         * @var CWebApplication $app
         */
        $app = Yii::app();
        if (($model = RegionInformation::model()->findByPk($id)) !== null) {
            if ($model->type == UsefulInformation::STATUS_HIDDEN || $model->type == UsefulInformation::STATUS_PUBLISHED) {
                $model->type = UsefulInformation::STATUS_ARCHIVE;
                $app->user->setFlash('success', 'Обьект добален в архив');
                $model->save(false);
            }
        }
        $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
    }

    /**
     * @param $id
     * @throws CDbException
     * Удаление информации о регионе
     */
    public function actionDelete($id)
    {
        /**
         * @var RegionInformation $model
         */
        if (($model = RegionInformation::model()->findByPk($id)) !== null) {
            $model->delete();
        }
    }


    /**
     * Загрузка информации о регионе в XML
     */
    public function actionAddRegionInformation()

    {
        $file = CUploadedFile::getInstances(RegionInformation::model(), 'regionInformationXML');
        if (!empty($file)) {
            $content = file_get_contents($file[0]->tempName);
            $xml = new SimpleXMLElement($content);
            $item = array();
            $keys = array();
            $i = 0;
            foreach ($xml->Worksheet->Table->Row as $row) {
                if (!empty($row->Cell->Data)) {
                    $j = 0;
                    foreach ($row as $cell) {
                        if ($i == 0) {
                            $keys[] = (string)$cell->Data;
                        } else {
                            $item[$keys[$j]] = (string)$cell->Data;
                        }
                        $j++;
                    }
                    if (!empty($item)) {
                        $countryName = $item['countryId'];
                        $regionName = $item['regionId'];
                        $districtName = $item['districtId'];
                        if (Region::model()->findByAttributes(array('name' => $regionName))) {
                            $regionId = Region::model()->findByAttributes(array('name' => $regionName))->id;
                        } else {
                            Yii::app()->user->setFlash('success', 'Ошибка: Опечатка в названии региона, либо не добавлен в базу данных один из регионов которые вы указали');
                            $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
                        }
                        $countryId = Country::model()->findByAttributes(array('name' => $countryName))->id;
                        $districtId = District::model()->findByAttributes(array('name' => $districtName))->id;
                        $regionInformation = RegionInformation::model()->findByAttributes(array('regionId' => $regionId));
                        if ($regionInformation == null) {
                            $regionInformation = new RegionInformation();
                        }
                        foreach ($item as $k => $value) {
                            if (empty($value)) {
                                $regionInformation->{$k} = null;
                            } else {
                                $regionInformation->{$k} = $value;
                            }
                        }
                        $regionInformation->countryId = $countryId;
                        $regionInformation->districtId = $districtId;
                        $regionInformation->regionId = $regionId;
                        if ($regionInformation && $regionInformation->rating == null || $regionInformation->rating == 1) {
                            $regionInformation->rating = 0;
                        }
                        $regionInformation->save(false);
                        Yii::app()->consoleRunner->run('RegionInformation --regionName="' . rawurlencode($regionInformation->region->name) . '"', true);
                    }
                    $i++;
                }
            }
            Yii::app()->user->setFlash('success', Yii::t('RegionInformationModule.regionInformation', 'The information is saved.'));
            $this->redirect($this->createUrl('index', array('langId' => Yii::app()->language)));
        }
    }

}