<?php
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->assetManager->publish(Yii::app()->theme->basePath . '/assets/css/industry/industry.css')
    );

    Yii::import('ext.ZLineChart.ZLineChart');
    Yii::import('application.modules.gradoteka.extensions.ZEasyPieChart.ZEasyPieChart');
    Yii::import('application.modules.gradoteka.extensions.ZGridView.ZGridView');

    if (!isset($tab)) {
        $tab = false;
    }

?>
<?php
/**
 * @var RegionInformation   $model
 */
if(isset($model->regionId) && !empty($model->regionId) && is_numeric($model->regionId)):?>
    <?php
    /**
     * @var Region  $region
     */
    $region = Region::model()->findByPk($model->regionId);?>
    <?php if(!empty($region) && isset($region->id) && !empty($region->id)):?>
        <?php
            //@TODO refactor into method or something
            $pageParams = array();
            $pageParams['open'] = User::checkTariff(1);
            $pageParams['regionId'] = $region->id;
            $pageParams['lang'] = Yii::app()->language;
        ?>
        <?php if ($this->beginCache(sha1(serialize($pageParams)), ['duration' => 3600 * 24 * 365])) {?>
            <?php //@TODO create methods or renderPartial for checking rights and displaying message ?>
            <?php if(!Yii::app()->user->isGuest && !User::checkTariff(1)):?>
                <div class="login-access-notice" style="margin-bottom:20px;">
                    <span class="p-icon"></span>
            <?= Yii::t('RegionInformationModule.regionInformation', 'Attention') . '!' ?>
                    <br/>
            <?= Yii::t('RegionInformationModule.regionInformation', 'Industry figures are available only after the tariff selection') ?>
                    <a class="sign-up" style="color: #fa694c !important;" href="<?=Yii::app()->createUrl('profile/default', array('tab' => 'tariffs', 'to' => Yii::app()->request->requestUri.'#analytics'));?>">
            <?= Yii::t('RegionInformationModule.regionInformation', 'Select the rate') ?>
                    </a>
                </div>
            <?php endif;?>
                <?php foreach(IndustryAnalyticsInformation::model()->findAll(array('order' => 'priority ASC')) as $industry):?>
                    <?php
                        if(!in_array($industry->id,Industry::getAllowedIndustries())){
                            continue;
                        }
                    ?>
                    <div class="industry-dropdown" target="industry-<?=$industry->id?>" style="background: <?php echo $industry->headerColor;?>">
                        <div class="industry-image">
                            <?php echo $industry->icon?>
                        </div>
                        <div class="industry-name">
                            <?php echo $industry->header?>
                        </div>
                        <div class="industry-wrap-arrow">
                            <div class="industry-arrow-down">

                            </div>
                        </div>
                        <div class="industry-dropdown-arrow" >
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="industry-cards-container industry-<?=$industry->id?>" style="display: none;" data-target="industry-<?=$industry->id?>">
                        
                        <?php if($model->regionId == 63 && $industry->id == 11):?>
                            <div class="industry-card-wide" style="padding-bottom:20px;">
                                <div class="cards-header" style="padding-bottom:0px;margin-bottom:0px;border:none;line-height: inherit;">
                                    <?= Yii::t('RegionInformationModule.regionInformation', 'In St. Petersburg region does not include rural areas, only urban') . '.' ?>
                                    <?= Yii::t('RegionInformationModule.regionInformation', 'You can view data for the industry "Agriculture" in the region') ?> <a href="<?php echo Yii::app()->createUrl('regionInformation/default/view', array('id'=>'30'));?>">Ленинградская область</a>.
                                </div>
                            </div>
                        <?php else:?>
                            <?php foreach(AnalyticsService::widgets($industry->id) as $group):?>
                                <?php if($group->type == 1):?>
                                    <?= $this->renderPartial('application.modules.regionInformation.views.default._a', array('model'=>$model, 'group'=>$group), true) ?>
                                <?php elseif($group->type == 2):?>
                                    <?= $this->renderPartial('application.modules.regionInformation.views.default._b', array('model'=>$model, 'group'=>$group), true) ?>
                                <?php elseif($group->type == 3):?>
                                    <?= $this->renderPartial('application.modules.regionInformation.views.default._c', array('model'=>$model, 'group'=>$group), true) ?>
                                <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>

                        <div class="industry-dropdown-hide">
                            <div class="industry-image">

                            </div>
                            <div class="industry-name">
                                <?php echo $industry->header?>
                            </div>
                            <div class="industry-wrap-arrow">
                                <div class="industry-arrow-up">

                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            <script>
                $('.industry-dropdown').click(function () {
                    var wrap = $(this).children('.industry-wrap-arrow');


                    if ($(wrap).children().hasClass('industry-arrow-down')) {
                        $(wrap).children().removeClass('industry-arrow-down');
                        $(wrap).children().addClass('industry-arrow-up');
                    } else {
                        $(wrap).children().removeClass('industry-arrow-up');
                        $(wrap).children().addClass('industry-arrow-down');
                    }
                });

                $('.industry-dropdown-hide').click(function() {
                    var target = $(this).parent().attr('data-target');
                    $('.' + target).slideToggle('slow');

                    var wrap = $('[target="' + target + '"]').children('.industry-wrap-arrow');

                    $(wrap).children().removeClass('industry-arrow-up industry-arrow-down');
                    $(wrap).children().addClass('industry-arrow-down');

                });

            </script>
            <div style="clear:both;"></div>
            <p class="source">
                <?= Yii::t('RegionInformationModule.regionInformation', 'Source: Gradoteka') ?> /
                <?php
                echo CHtml::link('www.gradoteka.ru', 'http://www.gradoteka.ru', ['class' => 'default-link', 'rel' => 'nofollow', 'target' => '_blank']);
                ?>
            </p>
            <?php
                Yii::app()->clientScript->registerScript('industry_dropdown', "
                    $('.industry-dropdown').click(function(){
                        var target = $(this).attr('target');
                        $('.' + target).slideToggle('slow');
                    });
                ");
            ?>
            <?php
                //@TODO refactor modal window script. include once.
                Yii::app()->clientScript->registerScript('lock-modal', "
                    $(window).ready(function(){
                        $('.lock-modal').click(function(){
                            var url = $(this).attr('href');
                            if(url === '' || url === 0 || url === null || url === undefined || url === 'undefined'){
                                url = '/ru/profile/default/index/?to='+encodeURIComponent(window.location.href)+'&tab=tariffs';
                            }
                            $('#go-to').attr({'href':url});
                        });
                    });
                ");
            ?>
        <?php $this->endCache();}?>
    <?php endif;?>
<?php endif;?>
