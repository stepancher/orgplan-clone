<?php
Yii::import('application.modules.regionInformation.RegionInformationModule');
/**
 * @var CController $this
 * @var bool $tab
 * @var RegionInformation $model
 * @var string $imagePath
 * @var CSqlDataProvider $dataProvider
 * @var $pageSize
 */

$app = Yii::app();
$request = $app->getRequest();

$ctrl = $this;
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

if (!isset($tab)) {
    $tab = false;
}

if ($tab == true && $model->region) {
    ?>
    <div class="cols-row">
        <div class="col-md-12">
            <span class="h1-header"><?= $model->region->name ?></span>
        </div>
    </div>
<?php
}
?>
<div class="cols-row page-regionInformation-view">
    <div class="col-md-12">
        <div class="p-pull-left page-regionInformation-view_image">
            <?php
            echo TbHtml::image($imagePath, $model->region ? $model->region->name : '',
                array(
                    'title' => $model->region ? $model->region->name : ''
                ))
            ?>
            <div class="shadow-block">&#160;</div>
        </div>
        <div class="p-pull-left page-regionInformation-view_information-block">
            <?php
                $title = '';
                if (isset(RegionInformation::$ratings[$model->rating])) {
                    $title = RegionInformation::$ratings[$model->rating];
                }
                if ($model->region):
            ?>
                <div class="page-regionInformation-view__rating" title="<?= $title ?>">
                    <?= $model->rating
                        ? TbHtml::link(
                            Yii::t('RegionInformationModule.regionInformation', 'Investment rating') . $model->rating . '. ' .
                            Yii::t('RegionInformationModule.regionInformation', 'Source: "Expert RA" (RAEX)'),
                            $model->region && $model->region->raexpert
                                ? $model->region->raexpert->regionLink
                                : '#',
                            array('target' => '_blank', 'rel' => 'nofollow', 'class' => 'default-link')
                        )
                        : Yii::t('RegionInformationModule.regionInformation', 'Information not available') ?>
                </div>
            <?php else:?>
                <div class="page-regionInformation-view__rating">
                    <?= $model->rating ? Yii::t('RegionInformationModule.regionInformation', 'Investment rating') . $model->rating . '. ' .
                        Yii::t('RegionInformationModule.regionInformation', 'Source: "Expert RA" (RAEX)')
                        : Yii::t('RegionInformationModule.regionInformation', 'Information not available') ?>
                </div>
            <?php endif;?>

            <div class="page-regionInformation-view__row">
                <div class="p-pull-left">
                    <div class="page-regionInformation-view__icon">
                        <i data-icon="&#xe3d2;"></i>
                    </div>
                    <div class="page-regionInformation-view__info">
                        <div><?= Yii::t('RegionInformationModule.regionInformation', 'Area') ?></div>
                        <div>
                            <h2 class="p-no-tb-indent">
                                <?php
                                    $square = $model->getStatisticObject(RegionInformation::SQUARE_STAT);
                                    if(!empty($square)){
                                        echo number_format($square->value, 1, ',', ' ') . ' ' . Yii::t('RegionInformationModule.regionInformation', 'thousand km') ;
                                    }else{
                                        echo Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                                    }
                                ?>
                            </h2>
                        </div>
                        <div class="page-regionInformation-view_information-block--place">
                            <?= isset($square) ?
                                $square->getRatingPoint() . ' ' . Yii::t('RegionInformationModule.regionInformation', 'place Russia') :
                                Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="p-pull-left">
                    <div class="page-regionInformation-view__icon">
                        <i data-icon="&#xe045;"></i>
                    </div>
                    <div class="page-regionInformation-view__info">
                        <div><?= Yii::t('RegionInformationModule.regionInformation', 'Gross regional product') ?> </div>
                        <div>
                            <h2 class="p-no-tb-indent">
                                <?php
                                    $regionalProduct = $model->getStatisticObject(RegionInformation::REGIONAL_PRODUCT_STAT);
                                    if(!empty($regionalProduct)){
                                        echo number_format($regionalProduct->value, 0, ',', ' ') . ' ' . Yii::t('RegionInformationModule.regionInformation', 'rubs') ;
                                    }else{
                                        echo Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                                    }
                                ?>
                            </h2>
                        </div>
                        <div class="page-regionInformation-view_information-block--place">
                            <?= isset($regionalProduct) ?
                                $regionalProduct->getRatingPoint() . ' ' . Yii::t('RegionInformationModule.regionInformation', 'place Russia') :
                                Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-regionInformation-view__row">
                <div class="p-pull-left">
                    <div class="p-pull-left page-regionInformation-view__icon">
                        <i data-icon="&#xe06c;"></i>
                    </div>
                    <div class="p-pull-left page-regionInformation-view__info">
                        <div><?= Yii::t('RegionInformationModule.regionInformation', 'Population') ?></div>
                        <div>
                            <h2 class="p-no-tb-indent">
                                <?php
                                    $population = $model->getStatisticObject(RegionInformation::PEOPLE_COUNT_STAT);
                                    if(!empty($population)){
                                        echo number_format($population->value, 0, ',', ' ') . ' ' . Yii::t('RegionInformationModule.regionInformation', 'people') ;
                                    }else{
                                        echo Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                                    }
                                ?>
                            </h2>
                        </div>
                        <div class="page-regionInformation-view_information-block--place">
                            <?= isset($population) ?
                                $population->getRatingPoint() . ' ' . Yii::t('RegionInformationModule.regionInformation', 'place Russia') :
                                Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="p-pull-left">
                    <div class="page-regionInformation-view__icon">
                        <i data-icon="&#xe1c6;"></i>
                    </div>
                    <div class="page-regionInformation-view__info">
                        <div><?= Yii::t('RegionInformationModule.regionInformation', 'Retail trade turnover') ?></div>
                        <div>
                            <h2 class="p-no-tb-indent">
                                <?php
                                    $tradeTurnover = $model->getStatisticObject(RegionInformation::TRADE_TURNOVER_STAT);
                                    if(!empty($tradeTurnover)){
                                        echo number_format($tradeTurnover->value, 0, ',', ' ') . ' ' . Yii::t('RegionInformationModule.regionInformation', 'million rubs') ;
                                    }else{
                                        echo Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                                    }
                                ?>
                            </h2>
                        </div>
                        <div class="page-regionInformation-view_information-block--place">
                            <?= isset($tradeTurnover) ?
                                $tradeTurnover->getRatingPoint() . ' ' . Yii::t('RegionInformationModule.regionInformation', 'place Russia') :
                                Yii::t('RegionInformationModule.regionInformation', 'Information not available');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cols-row p-offset-small-t-2">
    <div class="col-md-12">
            <?php
            $this->widget('application.modules.regionInformation.components.SimpleTabs', array(
                'base' => 'region-information',
                'enableAjax' => true,
                'htmlOptions' => [
                    'class' => 'b-tabs--bordered'
                ],
                'tabs' => array_filter(array(
                        array(
                            'active' => true,
                            'label' => Yii::t('RegionInformationModule.regionInformation', 'Description'),
                            'target' => 'description',
                            'url' => '#description'
                        ),
                        Yii::app()->user->isGuest?
                            array(
                                'label' => Yii::t('RegionInformationModule.regionInformation', 'Analytics'),
                                'target' => 'analytics',
                                'url' => '#analytics',
                                'requestAuthorization'=>TRUE,
                            ) :
                            array(
                                'label' => Yii::t('RegionInformationModule.regionInformation', 'Analytics'),
                                'target' => 'analytics',
                                'url' => '#analytics',
                            ),
                    )
                )));
            ?>
            <div data-tab-base="region-information" data-tab-target="description" class="active">
                <?= $this->renderPartial($tab ? 'application.modules.regionInformation.views.default._description' : '_description', ['model' => $model], true) ?>

                <?php

                if ($dataProvider->getTotalItemCount() !== 0) : ?>
                    <h2 class="f-h2">
                        <?= Yii::t('RegionInformationModule.regionInformation', 'TOP-fair by region') ?>
                        <br/>
                        <?= $model->regionId && $model->region ? $model->region->name : null ?>
                    </h2>

                    <div class="region-cards">
                        <?php

                        $this->renderPartial('application.modules.regionInformation.views._blocks._b-switch-list-view', [
                            'dataProvider' => $dataProvider,
                            'enableNextPageButton' => false,
                            'requestUrl' => Yii::app()->createUrl('/search'),
                            'itemView' => 'application.modules.regionInformation.views._blocks._b-card._fair',
                            'name' => 'fair',
                        ]);

                        if ($dataProvider->getTotalItemCount() > $pageSize) :?>
                            <div class="b-switch-list-view__add-items-wrap">
                                <?= CHtml::link(
                                    Yii::t('RegionInformationModule.regionInformation', 'Search result more'),
                                    Yii::app()->createAbsoluteUrl('/catalog/region/'.$model->region->id),
                                    [
                                        'class' => 'b-switch-list-view__add-items b-button d-bg-base d-bg-bordered d-bg-input--hover d-text-dark btn'
                                    ]
                                );
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif;?>
            </div>
            <?php if(!Yii::app()->user->isGuest):?>
                <div data-tab-base="region-information" data-tab-target="analytics">
                    <?= $this->renderPartial($tab ? 'application.modules.regionInformation.views.default._analytics' : '_analytics', ['model' => $model, 'tab' => $tab], true) ?>
                </div>
            <?php endif;?>
    </div>
</div>

<div style="height: 130px"></div>
