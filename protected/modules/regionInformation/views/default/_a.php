<?php
/**
 * @var ChartGroup $group
 */
?>

<div class="industry-card">

    <?php if(!empty($group->header)):?>
        <div class="cards-header">
            <?php echo $group->header?>
        </div>
    <?php endif;?>

    <?php $delimiter = 0;?>
    <?php foreach(AnalyticsService::loadWidgetsByGroup($group->id) as $chart):?>

        <?php if($delimiter):?>
            <div class="industry-card-delimiter"></div>
        <?php endif;?>

        <?php
            $this->widget(
                $chart['widget'],
                array_merge(
                    AnalyticsService::loadParams($chart, $model->regionId, AnalyticsService::DEFAULT_EPOCH),
                    array(
                        'lockValues' => !User::checkTariff(1),
                        'backUrl' => Yii::app()->createUrl('profile/default', ['to' => Yii::app()->getRequest()->requestUri.'#analytics', 'tab' => 'tariffs'])
                    )
                )
            );
        ?>

        <?php $delimiter++;?>
    <?php endforeach;?>

</div>