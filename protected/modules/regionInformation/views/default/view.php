<?php
/**
 * @var RegionInformation $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$cs = $app->clientScript;
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$linkAssets = null;
if(isset($model->regionInformationHasFiles[1]) && $model->regionInformationHasFiles[1]->file->type == ObjectFile::TYPE_REGION_INFORMATION_LOGO) {
    $linkAssets = H::getImageUrl($model->regionInformationHasFiles[1], 'file');
} elseif(isset($model->regionInformationHasFiles[0]) && $model->regionInformationHasFiles[0]->file->type == ObjectFile::TYPE_REGION_INFORMATION_LOGO) {
    $linkAssets = H::getImageUrl($model->regionInformationHasFiles[0], 'file');
} else {
    $linkAssets = H::getImageUrl(null, 'file');
}

$assets = strstr($linkAssets, 'assets', true);
$newLinkAssets = dirname($linkAssets) . DIRECTORY_SEPARATOR . 'regionInformation-resized-' . basename($linkAssets);
$newAbsoluteImagePath = ($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $newLinkAssets;
if(!file_exists($newAbsoluteImagePath)) {
    $image = new Image(($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $linkAssets);
    $image->resize(null, 350);
    $image->save($newAbsoluteImagePath);
}

?>
<div class="cols-row">
    <div class="col-md-7">
        <?php if(Yii::app()->user->getState('role') == User::ROLE_ADMIN): ?>
            <a href="<?= Yii::app()->createUrl('regionInformation/save', array('id' => $model->id)) ?>"
               class="b-button b-button-icon f-text--tall d-text-light d-bg-warning button-edit"
               data-icon="&#xe0ea;"></a>
        <?php endif; ?>

        <?= $this->renderPartial('/_blocks/_b-header', array(
            'header' => $model->region ? $model->region->name : '',
            'description' => Yii::t('RegionInformationModule.regionInformation', 'description')
        )) ?>
    </div>

    <div class="col-md-5 offset-small-t-2">
        <div>
            <?= CHtml::link(
                Yii::t('RegionInformationModule.regionInformation', 'Back to search'),
                $this->createUrl('/regions'),
                array(
                    'class' => 'js-b-back-history b-button b-button d-bg-base d-bg-bordered d-bg-input--hover d-text-dark btn button-back'
                )
            ) ?>
        </div>
    </div>
</div>
<?= $this->renderPartial('_template', array('model' => $model, 'imagePath' => $newLinkAssets, 'dataProvider' => $dataProvider, 'pageSize' => $pageSize)) ?>


