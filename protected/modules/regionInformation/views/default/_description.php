<?php
/**
 * @var CController $this
 * @var RegionInformation $model
 */
?>
<div class="cols-row">
    <div class="col-md-8">
        <?php
        if ($model->text) {
            echo '<span itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="description">' . $model->text . '</span></span>';
        } else {
            echo Yii::t('RegionInformationModule.regionInformation', 'Information not available');
        }
        ?>
        <p class="source">
            <?= '<span itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="name">' . Yii::t('RegionInformationModule.regionInformation', 'source rosstat') . '</span></span>' ?>
            <?php
            if (!empty($model->sourceUrl)) {
                $links = $model->sourceUrl;
                $link = explode(',', $links);
                $count = count($link);
                for ($i = 0; $i < $count; $i++) {
                    $link[$i] = trim($link[$i]);
                    if ($count - 1 > $i) {
                        echo '<span itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="name">' . CHtml::link($link[$i], 'http://' . $link[$i], ['class' => 'default-link', 'rel' => 'nofollow', 'itemprop' => 'url', 'target' => '_blank']) . ', ' . '</span></span>';
                    } else {
                        echo '<span itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="name">' . CHtml::link($link[$i], 'http://' . $link[$i], ['class' => 'default-link', 'rel' => 'nofollow', 'itemprop' => 'url', 'target' => '_blank']) . '</span></span>';
                    }
                }
            }
            ?>
        </p>
    </div>
</div>