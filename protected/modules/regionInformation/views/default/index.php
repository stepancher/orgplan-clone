<?php
/**
 * @var CController $this
 * @var CActiveDataProvider $dataProvider
 * @var CWebApplication $app
 * @var RegionInformation $regionInformation
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerCssFile($am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets') . '/css/main.css'));
$cs->registerCssFile($am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets') . '/css/autoComplete/style.css'));
$cs->registerScriptFile($am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets') . '/js/autoComplete/script.js'));
$cs->registerScriptFile($am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets') . '/js/regionInformation/script.js'));

$am = $app->getAssetManager();

$array = array();

foreach ($dataProvider->getData() as $regionInformation) {
	$fairsCount = 0;

	$model = Fair::model();
	$conn = $model->getDbConnection();
	$models = Fair::model()->with(array('exhibitionComplex', 'exhibitionComplex.city'))->findAll(
		$conn->quoteColumnName('city.regionId') . ' = ' . $conn->quoteValue($regionInformation->region->id)
	);
	if (!empty($models)) {
		$fairsCount = $regionInformation->fair;
	}
	$array[] = array(
		'image' => H::getImageUrl($regionInformation, 'image'),
		'caption' => '<b style="font-size: 18px; color: #112931; bottom: 20px; position: absolute">' . $regionInformation->region->name . '</b>
			<div style="position: absolute; top: 0; left: 0; margin-left: 9px;margin-top: 5px;" >
				<img src="' . $am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets') . '/img/regionRatingStar.png') . '"> </img>'
			. '<b style ="font-weight: 600">' . $regionInformation->rating . '</b>' .
			'</div>'
//			'<div style="bottom: 0px; position: absolute">
//				<img src="' . $am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets') . '/img/regionRatingFairsImg.png') . '"> </img>'
//			. '<i style ="font-size: 12px">' . H::getCountText($fairsCount, array('выставок', 'выставка', 'выставки')) . '</i>' .
//			'</div>',
	, 'url' => $this->createUrl('regionInformation/default/view', array('id' => $regionInformation->id)),
		'htmlOptions' => array('target' => '_blank', 'style' => 'position: relative; height: 228px; width: 284px; margin-left: -20px; margin-bottom: 10px; background-size: cover'),
		'imageOptions' => array('style' => 'width: 280px; height: 160px; margin-top: 21px;')
	);
}
$this->widget('zii.widgets.CBreadcrumbs', array(
	'separator' => ' / ',
	'links' => array(
		'Список регионов',
	),
	'htmlOptions' => array(
		'class' => 'breadcrumbs'
	)
));
?>

<?=
$app->user->checkAccess('regionInformationActionSave') ? TbHtml::linkButton('Добавить', array(
	'class' => 'btn-success',
	'url' => $this->createUrl('regionInformation/save'),
)) : null ?>
&nbsp;
<?=
$app->user->checkAccess('regionActionGenerateXml') ? TbHtml::linkButton('Выгрузить в формате XML', array(
	'class' => 'btn-warning',
	'url' => $this->createUrl('region/default/generateXml'),
)) : null ?>
<style>
	.auto-complete {
		margin-top: 10px;
		float: right;
	}

	.widget-UserForm .auto-complete {
		margin-top: 0;
		float: none;
	}

	.widget-UserForm .enter-text-input {
		width: auto;
	}

	.enter-text-input {
		width: 270px;
	}
</style>
<script type="text/javascript">
	$(function () {
		new AutoComplete({
			data: <?=json_encode(CHtml::listData(Region::model()->findAll(), 'id', 'name'))?>,
			element: $('.auto-complete.input-name'),
			emptyValue: 'Все регионы...',
			init: function (e) {
				$('.drop-box', e.config.element).hide();

				setTimeout(function () {
					$('.drop-box div', e.config.element).click(function () {
						$('.auto-complete-input', e.config.element).val($(this).text());
						$('.input-hidden', e.config.element).val($(this).data('id'));
					});
				}, 100);

				$('.auto-complete-input', e.config.element).focus(function () {
					$('.drop-box', e.config.element).show();
				});

				$('.auto-complete-input', e.config.element).blur(function () {
					setTimeout(function () {
						$('.drop-box', e.config.element).hide();
					}, 200)
				});

				e.config.element.find('.input-arrow').click(function () {
					$('.auto-complete-input', e.config.element).focus();
				});

			}
		});

	});
</script>

<div>
	<div class="auto-complete input-name">
		<div class="controls">
			<div class="enter-text-input">
				<input class="auto-complete-input" type="text">
				<input id="RegionInformation_regionId" class="input-hidden" value="" type="hidden">

				<div class="input-arrow">
					<div class="auto-complete-arrow"></div>
				</div>
			</div>
			<div class="drop-box"></div>
		</div>
	</div>
	<h1><?= Yii::t('RegionInformationModule.regionInformation', 'Regions') ?></h1>
</div>

<p class="tudayinportal">
	<b style="font-weight: 100">Сегодня на портале:</b> <b style="font-weight: 600; color: #112931">
		<?php echo H::getCountText(count(Region::model()->findAll()), array('регионов', 'регион', 'региона')) ?>
	</b>
</p>
<?php
if (($page = Page::model()->findByAttributes(array('type' => Page::TYPE_INFO_ABOUT_REGION_INFO, 'status' => Page::STATUS_PUBLISHED))) !== null
) {
	?>
	<div style="clear: both"></div>
	<div class="global-description">
		<div class="page-name" style="font-size: 24px">
			<?= $page->name ?: '' ?>
		</div> <?php

		?>
		<div class="page-description">
			<?= $page->description ?: '' ?>
		</div>
	</div> <?php
};

?>
<div id="region-thumbnails">
	<?php echo TbHtml::thumbnails($array);
	$this->widget('TbPager', array(
		'pages' => $dataProvider->getPagination(),
	))
	?>
</div>
<style type="text/css">

	.thumbnail:after {
		visibility: hidden;
		display: block;
		content: "";
		clear: both;
		height: 0;
	}

	.tudayinportal {
		font-weight: normal;
		font-size: 26px;
	}

	ul li:before {
		background: none;
	}

	a:hover {
		text-decoration: none;
	}

</style>