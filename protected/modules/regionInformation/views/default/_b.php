<?php
/**
 * @var ChartGroup $group
 */
?>

<div class="industry-card-wide">

    <?php if(!empty($group->header)):?>
        <div class="cards-header">
            <?php echo $group->header?>
        </div>
    <?php endif;?>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>

    <?php foreach(AnalyticsService::loadWidgetsByGroup($group->id) as $chart):?>
        <td align="center" valign="middle">
        <?php if(isset($chart['widget'])):?>

            <!--<div class="small-card ch-col-2">-->
                <?php
                    $this->widget($chart['widget'],
                        array_merge(
                            AnalyticsService::loadParams($chart, $model->regionId, AnalyticsService::DEFAULT_EPOCH),
                            array(
                                'lockValues' => !User::checkTariff(1),
                                'backUrl' => Yii::app()->createUrl('profile/default', ['to' => Yii::app()->getRequest()->requestUri.'#analytics', 'tab' => 'tariffs'])
                            )
                        )
                    );
                ?>
            <!--</div>-->

        <?php elseif(isset($chart['widgets']) && is_array($chart['widgets']) && !empty($chart['widgets'])):?>

            <?php foreach($chart['widgets'] as $group):?>

            <!--<div class="small-card ch-col-4">-->
                    <?php foreach($group as $chart):?>
                        <?php if(!isset($chart['widget'])){continue;}?>
                        <?php
                            $this->widget(
                                $chart['widget'],
                                array_merge(
                                    AnalyticsService::loadParams($chart, $model->regionId, AnalyticsService::DEFAULT_EPOCH),
                                    array(
                                        'lockValues' => !User::checkTariff(1),
                                        'backUrl' => Yii::app()->createUrl('profile/default', ['to' => Yii::app()->getRequest()->requestUri.'#analytics', 'tab' => 'tariffs'])
                                    )
                                )
                            );
                        ?>
                    <?php endforeach;?>
            <!--</div>-->

            <?php endforeach;?>
        <?php endif;?>
                    </td>
    <?php endforeach;?>
    </tr>
 </table>
 </div>