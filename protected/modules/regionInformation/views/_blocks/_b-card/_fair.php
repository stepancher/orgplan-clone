<?php
/**
 * @var Fair $data
 * @var bool $enableButton
 */

if (Yii::app()->request->getParam('ctrl')) {
    $isSearchPage = Yii::app()->request->getParam('ctrl') == 'search';
} else {
    $isSearchPage = Yii::app()->controller->id == 'search';
}

$publishUrl = Yii::app()->assetManager->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
Yii::app()->getClientScript()->registerCssFile($publishUrl . '/less/global.css');

$module = NULL;
if(isset(Yii::app()->controller->getModule()->id) && !empty(Yii::app()->controller->getModule())){
    $module = Yii::app()->controller->getModule()->id;
}

$isMyFairsPage = $module == 'workspace' && in_array(Yii::app()->controller->action->id, ['index', 'archive']);
$isMyFairsPageArchive = $module == 'workspace' && Yii::app()->controller->action->id == 'archive';
$isBackoffice = $module == 'backoffice';

if(!isset($fairMatches)){
    $fairMatches = [];
}

$matches = Yii::app()->session->get('matches', []);

if (Yii::app()->user->isGuest) {
    $checked = isset($matches[$data['fairId']]);
} else {
    $checked = in_array($data['fairId'], $fairMatches);
}

$url = Yii::app()->createUrl('fair/default/view', array('id' => $data['fairId']));

/**
$url = Yii::app()->createUrl('fair/html/view', [
    'urlFairName' => $data['fairShortUrl'],
    'langId' => Yii::app()->language,
]);
**/

if ($isMyFairsPage) {
    if ($isMyFairsPageArchive) {
        $url = 'javascript://';
    }
    else {
        if(MyFair::isExponentFair($data['fairId'])){
            $url = Yii::app()->createUrl('/workspace/panel/panel', array('fairId' => $data['fairId']));
        }else{
            $url = Yii::app()->createUrl('workspace/panel/exponentFair', ['fairId' => $data['fairId']]);
        }
    }
}
if ($isBackoffice) {
    $url = Yii::app()->createUrl('/backoffice/panel/panel', array('fairId' => $data['fairId']));
}
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.fair.assets'), false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/css/main.css');
?>

<div id='remove-button-<?=$data['fairId']?>' class="b-switch-list-view__item b-switch-list-view__item-fair" itemscope itemtype="http://schema.org/Service">

    <div class="b-switch-list-view__cell b-switch-list-view__cell--fair-name upper-case <?= $isMyFairsPage ? 'my-fairs-link' : '' ?>" itemprop="name">
        <?php echo TbHtml::link(
            $data['fairName'],
            $url,
            [
                'title' => $data['fairName'],
                'class' => 'trim-clamp',
                'data-max-line' => 4,
                'data-clamping' => 0,
                'data-fair-id' => $data['fairId'],
                'itemprop' => 'url'
            ]
        );?>
    </div>

    <div
        class="b-switch-list-view__cell b-switch-list-view__cell--type b-switch-list-view__cell--hide-as-row">
        <?= Yii::t('RegionInformationModule.regionInformation', 'fair') ?>
    </div>

    <div title="<?= Fair::getTitleRatingIconByRatingId($data['fairRating']) ?>"
         class="b-switch-list-view__cell rating-logo rating-logo-type-<?= $data['fairRating'] ?>-<?='rus'?> <?= !$isSearchPage ? 'rating-logo-pick-up' : '' ?>">
        <div></div>
    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--date">
        <span itemprop="hoursAvailable" content="'<?= date('d.m.Y', strtotime($data['fairBeginDate'])) ?>'">
            <?= Fair::getDateRange($data['fairBeginDate'], $data['fairEndDate']) ?>
        </span>
    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--industry b-switch-list-view__cell--hide-as-block">
        <?php echo TbHtml::tag('span', ['title' => $data['industriesList']], $data['industriesList'] ? : Yii::t('RegionInformationModule.regionInformation', 'Not set')) ?>
    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--fair-region">

        <?php if(!empty($data['regionName']) ? $data['regionName'] : '') :?>
            <span itemprop="name">
                <span itemprop="serviceArea">
                    <a itemprop="url" title="<?= $data['regionName'] ?>"
                       href="
                            <?=
                                !empty($regionInformationId = $data['regionInformationId']) ?
                                    Yii::app()->createUrl('regionInformation/default/view', array('id' => $regionInformationId))
                                :
                                    '#'
                            ;?>
                        ">
                        <span class="b-switch-list-view__content--hide-as-row"><?= $data['cityName'] ?> / </span> <?= $data['regionName'] ?>
                    </a>
                </span>
            </span>
        <?php endif;?>

    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--fair-city b-switch-list-view__cell--hide-as-block">
        <a title="<?php echo !empty($data['cityName']) ? $data['cityName'] : '' ?>"
           href="<?=Yii::app()->createUrl('/catalog', array(
               'langId' => Yii::app()->language,
           ))?>">
            <?php echo !empty($data['cityName']) ? $data['cityName'] : '' ?>
        </a>
    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--actions">
        <?php if ($isSearchPage) :?>

            <?php echo TbHtml::linkButton('', [
                'url' => 'javascript://',
                'class' => implode(' ',[
                    'b-button',
                    'js-add-to-mach',
                    'd-bg-secondary-icon',
                    'f-text--tall',
                    'd-text-light',
                    'b-list-active-button-1',
                    'd-bg-success--hover',
                    !$checked ? '' : ' compare-checked d-bg-success',
                ]),
                'data-icon' => '&#xe3d3;',
                'data-id' => $data['fairId'],
                'encode' => false,
                'title' => !$checked ? Yii::t('RegionInformationModule.regionInformation', 'compare') : Yii::t('RegionInformationModule.regionInformation', 'remove from the comparison'),
            ]);?>

        <?php elseif ($isMyFairsPage): ?>

            <?php echo TbHtml::ajaxLink('',
                Yii::app()->createUrl('/workspace/panel/removeFair', array('id' => $data['fairId'])),
                [
                    'success'=>'function(html){ $("#remove-button-'.$data['fairId'].'").fadeOut(200); }'
                ],
                [
                    'confirm' => 'Вы уверены что хотите удалить выставку?',
                    'url' => 'javascript://',
                    'class' => implode(' ', [
                        'b-button',
                        'd-bg-secondary-icon',
                        'f-text--tall',
                        'd-text-light',
                        'b-list-active-button-2',
                        'd-bg-danger--hover',
                        'js-remove-item',
                    ]),
                    'data-icon' => '&#xe3c1;',
                    'data-id' => $data['fairId'],
                    'data-target' => '#modal-remove',
                    'encode' => false,
                    'title' => Yii::t('RegionInformationModule.regionInformation', 'Delete'),
            ]);?>

        <?php endif;?>

        <?php if(strtotime($data['fairBeginDate']) > time()):?>
            <?php $this->renderPartial('application.modules.regionInformation.views._blocks._addFair', ['fairId' => $data['fairId']]); ?>
        <?php endif;?>
        <div class="<?= IS_MODE_DEV  && !$isBackoffice ? Fair::getBottomButtonClass($data['fairId']) : 'orange-block' ?>">

            <?php if (IS_MODE_DEV && !$isBackoffice):?>

                <?php $textIconTime = Fair::getDataForBottomButton($data['fairId'], $data['fairBeginDate']);?>

                <div class="b-switch-list-view__cell--bottom-button-text">
                    <div class="uplifting-text"><?= $textIconTime['text'] ?></div>
                </div>
                <div class="b-switch-list-view__cell--bottom-button-icon">
                    <?= $textIconTime['icon-time'] ?>
                </div>

            <?php endif; ?>

        </div>
    </div>
</div>

<?php if(!IS_MODE_DEV) : ?>
    <style>
        .b-switch-list-view .b-switch-list-view--as-block .b-switch-list-view__item.b-switch-list-view__item-fair{
            height: 205px !important;
        }
    </style>
<?php endif; ?>