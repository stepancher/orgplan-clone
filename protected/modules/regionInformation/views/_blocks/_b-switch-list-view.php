<?php
/**
 * @var CController $this
 * @var string $requestUrl
 * @var string $itemView
 * @var string $switcherType
 * @var string $name
 * @var array $sortableAttributes
 * @var bool $enableNextPageButton
 * @var CWebApplication $app
 * @var bool $notFoundVisible
 */
Yii::import('application.modules.region.RegionModule');
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/raty/raty.js', CClientScript::POS_END);

if (!isset($notFoundVisible)) {
    $notFoundVisible = true;
}
if (empty($switcherType)) {
    $switcherType = 'block';
}
if (empty($name)) {
    $name = 'default';
}
if (empty($sortableAttributes)) {
    $sortableAttributes = [];
}
if (empty($queryParams)) {
    $queryParams = [];
}

$curPage = $dataProvider->getPagination()->getCurrentPage(false);
$nextPage = $curPage + 1;
$nextPageUrl = '';
if (empty($nextPageExist)) {
    $nextPageExist = $nextPage * $dataProvider->getPagination()->getPageSize() < $dataProvider->totalItemCount;
}

if (!isset($enableNextPageButton)) {
    $enableNextPageButton = true;
}

?>

<div class="b-switch-list-view" data-request='<?= json_encode(['page' => $curPage] + $queryParams) ?>'
     data-ctrl='<?= Yii::app()->controller->id ?>'>
    <div class="b-search-loader-block">
        <?= TbHtml::image($publishUrl . '/img/loader/ajax-loader.gif', 'Загрузка', ['class' => 'js-page-search__loader', 'title' => 'Загрузка']) ?>
    </div>
    <?php
    if ($dataProvider->getTotalItemCount() != 0) : ?>

        <div class="b-switch-list-view__items">
                <?php

                $this->widget('application.modules.regionInformation.components.SearchListView', [
                    'dataProvider' => $dataProvider,
                    'afterAjaxUpdate' => 'js:function() { FairMatch(); }',
                    'htmlOptions' => [
                        'class' => 'js-updated-block b-switch-list-view js-switch-list-view-' . $name . ' b-switch-list-view--as-' . $switcherType,
                        'data-switcher' => 'js-switch-list-view-' . $name,
                    ],
                    'sorterCssClass' => 'b-switch-list-view__sorter',
                    'sortableAttributes' => $sortableAttributes + [''],
                    'itemView' => $itemView,
                    'template' => "{sorter}\n{items}",
                ]);
                ?>
        </div>
        
    <?php elseif ($notFoundVisible == true) : ?>
        <div class="not-found">
            <h3 class="text-center"><?= Yii::t('RegionModule.region', 'Unfortunately, upon request') ?>
                <?= isset($_GET['query']) ? '«' . $_GET['query'] . '»' : null ?> <?= Yii::t('RegionModule.region', 'there is nothing') ?>
                .<br/>
                <?= Yii::t('RegionModule.region', 'Enter into the search box or try a new query filters') ?>.
            </h3>
        </div>
    <?php endif; ?>
</div>