function AutoComplete(config) {
	config = $.extend({
		data: [],
		element: null,
		event: 'keyup',
		emptyValue: '',
		selectedValue: null,
		init: null
	}, config);
	return ({
		config: {},
		init: function (config) {
			this.config = config;
			var html = '';
			if (typeof config.init == 'function') {
				config.init(this);
			}
			if (this.config.emptyValue) {
				html = '<div class="empty-text" data-id="">' + this.config.emptyValue + '</div>';
			}

			var me = this;
			$.each(me.config.data, function (id, name) {
				if (me.config.selectedValue == id) {
					$('.auto-complete-input', me.config.element).val(name);
					$('.input-hidden', me.config.element).val(id);
				}
				html += '<div data-id="' + id + '">' + name + '</div>';
			});

			$('.drop-box', this.config.element).html(html);
			this.run();
			return this;
		},
		run: function () {
			var me = this;
			$('.empty-text', me.config.element).click(function () {
				me.mather('')
			});
			$('.auto-complete-input', me.config.element).bind(me.config.event, function () {
				me.mather(this.value);
			});
		},
		mather: function (value) {
			$('.drop-box div', this.config.element).each(function (i, elm) {
				if (value != '') {
					if ($(elm).text().match(new RegExp(value, 'gi'))) {
						$(elm).show();
					} else {
						$(elm).not('.empty-text').hide();
					}
				} else {
					$(elm).show();
				}

			});
		}
	}).init(config);
}