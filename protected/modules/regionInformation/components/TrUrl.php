<?php

Class TrUrl extends CComponent{

    public function init(){

    }

    public function getTrUrl($route, $params){

        if(empty($params['urlRegionName'])){
            return NULL;
        }

        $trRegion = TrRegion::model()->findByAttributes(array('shortUrl' => $params['urlRegionName']));

        if(empty($trRegion)){
            return NULL;
        }

        $tr = TrRegion::model()->findByAttributes(array('trParentId' => $trRegion->trParentId, 'langId' => $params['langId']));

        if(empty($tr)){
            return NULL;
        }

        $params['urlRegionName'] = $tr->shortUrl;

        return Yii::app()->createUrl($route, $params);
    }
}