<?php
/**
 * Created by PhpStorm.
 * User: SoundGoodizer
 * Date: 27.04.2015
 * Time: 16:50
 */
Yii::import('zii.widgets.CListView');

class SearchListView extends CListView
{
    /**
     * Renders the sorter.
     */
    public function renderSorter()
    {
        if ($this->dataProvider->getItemCount() <= 0)
            return;
        echo CHtml::openTag('div', array('class' => $this->sorterCssClass)) . "\n";
        echo "<ul>\n";
        foreach ($this->sortableAttributes as $name => $label) {
            echo "<li>";
            if (is_string($name)) {
                echo $this->getLink($name);
            }
            echo "</li>\n";
        }
        echo "</ul>";
        echo $this->sorterFooter;
        echo CHtml::closeTag('div');
    }

    public function getLink($name)
    {
        $options = ['class' => 'switch-list-view__sorter-item', 'rel' => 'nofollow'];
        $get = $_GET;
        $d = null;
        if (isset($get['sort']) && ltrim($get['sort'], '-') == $name) {
            TbHtml::addCssClass('active', $options);

            if (strncmp($get['sort'], '-', 1) === 0) {
                $d = '';
                TbHtml::addCssClass('desc', $options);
            } else {
                $d = '-';
            }
        }

        $get['sort'] = $d . $name;

        if (isset($get['page'])) {
            unset($get['page']);
        }

//        return CHtml::link(Yii::t('FairModule.fair', $name), Yii::app()->controller->createUrl('/search', $get), $options);
        return '<span class="' . $options['class']
            . '"data-url="' . Yii::app()->controller->createUrl('/search', $get) . '">'
            . Yii::t('RegionInformationModule.regionInformation', $name) . '</span>';
    }

}