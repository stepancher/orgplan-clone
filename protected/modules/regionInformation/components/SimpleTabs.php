<?php
/**
 * File SimpleTabs.php
 */

/**
 * Class SimpleTabs
 */
class SimpleTabs extends CWidget
{
    private $options = [];

    /**
     * @var array
     */
    public $tabs;

    /**
     * @var string
     */
    public $base;

    /**
     * @var bool
     */
    public $fixed;

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @var array
     */
    public $afterUpdate = null;

    /**
     * @var array
     */
    public $onClick = null;

    /**
     * @var array
     */
    public $handlers = null;

    /**
     * @var bool
     */
    public $enableAjax = false;

    /**
     * @var string
     */
    public $contentType = 'html';

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();
        $this->options['baseName'] = $this->base;
        if($this->afterUpdate) {
            $this->options['afterUpdate'] = $this->afterUpdate;
        }
        if($this->onClick) {
            $this->options['onClick'] = $this->onClick;
        }
        if($this->handlers) {
            $this->options['handlers'] = $this->handlers;
        }
        if(empty($this->fixed)){
            $this->fixed = false;
        }
        $this->options['enableAjax'] = $this->enableAjax;
        $this->options['contentType'] = $this->contentType;
        $this->registerClientScript();
    }

    /**
     * Запуск отрисовки
     */
    public function run()
    {
        $tabsOutput = '';
        foreach ($this->tabs as $tab) {
            $active = isset($tab['active']) && $tab['active'] == true ? ' active' : '';
            $content = CHtml::link($tab['label'], isset($tab['url']) ? $tab['url'] : '#');
            $extraParams = [];
            if(isset($tab['alwaysAjax']) && $tab['alwaysAjax']) {
                $extraParams['data-always-ajax'] = 1;
            }
            if(isset($tab['pageDescription']) && $tab['pageDescription']) {
                $extraParams['data-page-description'] = $tab['pageDescription'];
            }
            if(isset($tab['pageTitle']) && $tab['pageTitle']) {
                $extraParams['data-page-title'] = $tab['pageTitle'];
            }
            if(isset($tab['requestAuthorization']) && $tab['requestAuthorization']) {
                $extraParams['data-target'] = '#modal-sign-up';
                $extraParams['data-toggle'] = 'modal';
            }
            $class = '';
            if(isset($tab['class']) && $tab['class']) {
                $class = $tab['class'];
            }
            $tabsOutput .= TbHtml::tag('div', [
                'class' => 'b-tabs__tab' . $active.' '.$class,
                'data-content-target' => $tab['target']
            ]+$extraParams, $content);
        }
        TbHtml::addCssClass('b-tabs', $this->htmlOptions);
        if($this->fixed) {
             TbHtml::addCssClass('b-tabs-fixed', $this->htmlOptions);
        }
        
        $this->htmlOptions['data-base'] = $this->base;
        echo TbHtml::tag('div', $this->htmlOptions, $tabsOutput);
    }

    private function registerClientScript()
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        $am = $app->getAssetManager();
        $cs = $app->getClientScript();
        $publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.regionInformation.assets'), false, -1, YII_DEBUG);

        $cs->registerCssFile($publishUrl . '/css/b-tabs.css');
        $cs->registerScriptFile($publishUrl . '/js/tabs/SimpleTabs.js', CClientScript::POS_END);

        $cs->registerScript(
            '#SimpleTabs-' . $this->id,
            'new SimpleTabs(' . CJavaScript::encode($this->options) . ')',
            CClientScript::POS_END
        );
    }

}