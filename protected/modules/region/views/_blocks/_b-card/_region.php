<?php
/**
 * @var RegionInformation $data
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$name = null;
if ($data->regionId && $data->region) {
	$name = $data->region->name;
}
$title = '';
if (isset(RegionInformation::$ratings[$data->rating])) {
	$title = RegionInformation::$ratings[$data->rating];
}

?>
<div class="b-switch-list-view__item">
	<div class="b-switch-list-view__cell b-switch-list-view__cell--regions-name">
		<div class="p-pull-left b-switch-list-view__cell--name-region"
			 itemscope itemtype="http://schema.org/PostalAddress">
			<span itemprop="addressLocality"><?= TbHtml::link($name, Yii::app()->createUrl('regionInformation/default/view', array('id' => $data->id)),array('itemprop' => 'url')) ?></span>
		</div>
		<div class="p-pull-right region-rating" title="<?= $title ?>">
			<span itemscope itemtype="https://schema.org/Rating">
			<?= TbHtml::image($publishUrl . '/img/raty/star-on.png', $data->rating, ['style' => 'margin-top: -4px;', 'itemprop'=>'image']) ?>
			<span itemprop="name"><?= $data->rating ?></span>
			</span>

		</div>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--type__regions b-switch-list-view__cell--hide-as-row">
		<span class="b-switch-list-view__item--type-text"><?=Yii::t('RegionModule.region', 'region')?></span>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--background__regions b-switch-list-view__cell--hide-as-row">
			<?php
			if (isset($data->regionInformationHasFiles[1]) && $data->regionInformationHasFiles[1]->file->type == ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE) {
				echo TbHtml::image(H::getImageUrl($data->regionInformationHasFiles[1], 'file'), $name, [
					'title' => $name,
				]);
			} elseif (isset($data->regionInformationHasFiles[0]) && $data->regionInformationHasFiles[0]->file->type == ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE) {
				echo TbHtml::image(H::getImageUrl($data->regionInformationHasFiles[0], 'file'), $name, [
					'title' => $name,
				]);
			} else {
				echo TbHtml::image(H::getImageUrl(null, 'file'), $name, [
					'title' => $name,
				]);
			}
			?>

		<?php
		echo Chtml::link('<div></div>',
			Yii::app()->createUrl('regionInformation/default/view', array('id' => $data->id)),
			[
				'title' => $name
			]
		);
		?>
	</div>
</div>