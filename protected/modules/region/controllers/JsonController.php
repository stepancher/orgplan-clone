<?php

class JsonController extends Controller
{

    protected $dump = FALSE;

    public function actionDump($langId = NULL){
        $this->dump = TRUE;
        $this->actionGetList();
    }

    public function actionGetList($searchQuery = NULL)
    {

        $json = array(
            "header"=> Yii::t('RegionModule.region', 'Regions catalog'),
            "models"=> $this->getRegions($searchQuery),
            "searchPlaceholder"=> Yii::t('RegionModule.region', 'Search in name'),
        );

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    public function getRegions($searchQuery = NULL){

        $searchQueryCondition = '';
        $q = Yii::t('RegionModule.region', 'Founded');
        $params = [
            ':langId' => Yii::app()->language,
            ':fileType' => ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE,
        ];

        if(!empty($searchQuery)){
            $searchQueryCondition = "trr.name LIKE :searchQuery AND";
            $q = Yii::t('RegionModule.region', 'On search')." \"{$searchQuery}\" ".Yii::t('RegionModule.region', 'founded');
            $params[':searchQuery'] = "%{$searchQuery}%";
        }

        $sql = "
            SELECT 
                trr.name AS regionName,
                ri.id AS id,
                objf.name AS fileName,
                objf.label AS fileLabel,
                trr.shortUrl AS shortUrl
                
            FROM tbl_regioninformation ri
            
            LEFT JOIN tbl_region r ON (r.id = ri.regionId)
            LEFT JOIN tbl_trregion trr ON (r.id = trr.trParentId AND trr.langId = :langId)
            LEFT JOIN tbl_regioninformationhasfile rihf ON (rihf.regionInformationId = ri.id)
            LEFT JOIN tbl_objectfile objf ON (objf.id = rihf.fileId)

            WHERE
                {$searchQueryCondition}
                ri.active = 1 AND 
                objf.type = :fileType
        ";

        $regions = Yii::app()->db->createCommand($sql)->queryAll(TRUE, $params);

        $data = array();
        foreach($regions as $region){

            $filePath = Yii::app()->getBaseUrl(true).'/frontend/empty.png';

            if(!empty($region['fileName'])){
                $filePath = Yii::app()->getBaseUrl(true).'/uploads/'.str_replace('{id}',$region['id'], ObjectFile::$paths[ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE]).'/'.$region['fileName'];
            }

            $data[] = array(
                "id"=> $region['id'],
                "name"=> mb_convert_case($region['regionName'], MB_CASE_LOWER, "UTF-8"),
                "link" => Yii::app()->getBaseUrl(true).Yii::app()->createUrl('regionInformation/default/viewRegion', array('urlRegionName' => $region['shortUrl'])),
                "image"=> $filePath,
            );
        }

        $result = array(
            "searchResultLabel"=> $q." ".count($regions)." ".Yii::t('RegionModule.region', 'regions'),
            'list' => $data,
        );

        return $result;
    }
}