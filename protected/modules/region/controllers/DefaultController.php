<?php

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @var Region $model
     */
    public $model = 'Region';

    /**
     * Стандартные actions перенаправляют на 404.
     */
    public function actionIndex()
    {
        throw new CHttpException(404, Yii::t('RegionModule.region', 'Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }

    /**
     * Парсинг регионов
     */
    public function actionParsing()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        $ctrl = $this;
        $app->consoleRunner->run('raexpert run', true);
        $app->user->setFlash('success', 'Информация обрабатывается.');
        $ctrl->redirect($ctrl->createUrl('index', array('langId' => Yii::app()->language)));
    }

    /**
     * Генерация XML
     */
    public function actionGenerateXml()
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;

        $root = $dom->createElement('document');
        $dom->appendChild($root);

        $model = Raexpert::model();
        $this->addChildrenItems(
            $root,
            'raexpert',
            $model->findAll()
        );
        Yii::app()->getRequest()->sendFile('raexpert.xml', $dom->saveXML(), 'text/xml');
    }

    /**
     * @param $root DOMElement
     * @param $nodeName string
     * @param AR[]|array[] $models
     * Каждый объект обворачивается тегами <item>
     */
    public function addChildrenItems($root, $nodeName, $models)
    {
        $items = new DOMElement($nodeName);
        $root->appendChild($items);

        foreach ($models as $data) {
            $this->addChildrenData(
                $items->appendChild(new DOMElement('item')),
                $data
            );
        }
    }

    /**
     * @param $node DOMNode|DOMElement
     * @param $data array
     * Заполнение Item записями из БД
     */
    public function addChildrenData($node, $data)
    {
        foreach ($data as $name => $value) {
            if ($name != 'id') {
                if (is_array($value)) {
                    // массив атрибутов моделей
                    if (isset($value[0])) {
                        $this->addChildrenItems($node, $name, $value);
                    } // атрибуты модели
                    else {
                        if (!empty($value)) {
                            $this->addChildrenData($node->appendChild(new DOMElement($name)), $value);
                        }
                    }
                } // Строка
                else {
                    $node->appendChild(new DOMElement($name, $value == null ? $value = 'null' : str_replace('&', ' ', $value)));
                }
            }
        }
    }

    /**
     * @param $model AR|array
     * @return array
     * Читаем атрибуты моделей
     */
    public function getModelAttributes($model)
    {
        $attributes = array();

        // код для обработки связи один ко многим
        if (is_array($model)) {
            foreach ($model as $k => $m) {
                $attributes[$k] = $this->getModelAttributes($m);
            }
            return $attributes;
        }

        // основной код
        if ($model instanceof AR) {
            $model->setScenario('export');

            $safeAttributes = $model->getSafeAttributeNames();

            // читаем атрибуты модели
            $attributes = $model->getAttributes($safeAttributes);
            $attributes['@class'] = get_class($model);

            // обрабатываем связи
            foreach ($model->description() as $field => $params) {
                if (in_array($field, $safeAttributes)) {
                    if (isset($params['relation']) && !isset($params[0]) && !strripos($field, 'With')) {
                        $attributes[$field] = $this->getModelAttributes(
                            $model->{$field}
                        );
                    }
                }
            }
        }
        return $attributes;
    }
}