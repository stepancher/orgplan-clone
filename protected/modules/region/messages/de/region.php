<?php 
return array(
  'region' => 'регион',
  'In no tabs.' => 'Es gibt keine Regionen in den Lesezeichen. Speichere die Regionen, vergleiche die Kennwerte. Halte sie griffbereit, das ist bequem.',
  'Enter into the search box or try a new query filters' => 'Geben Sie neue Kriterien ins Suchfenster oder benutzen Sie Filter',
  'Search result more' => 'mehr zeigen',
  'Unfortunately, upon request' => 'Leider haben wir keine Objekte zu Ihrer Anfrage',
  'there is nothing' => 'gefunden',
  'Unable to resolve the request "{route}".' => 'Konnte den Request "{route}" nicht auflösen.',
    'Regions catalog' => 'Regionenverzeichnis',
    'Search in name' => 'Suche nach Namen',
    'Founded' => 'gefunden',
    'founded' => 'gefunden',
    'On search' => 'Auf Anfrage',
    'regions' => 'Regionen',
);