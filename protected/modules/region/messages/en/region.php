<?php 
return array(
  'region' => 'Region',
  'In no tabs.' => 'There are no regions in your bookmarks. Bookmark regions, compare key features, keep them within your reach - it\'s convenient!',
  'Enter into the search box or try a new query filters' => 'Enter a new query in the "Search" field or use filters',
  'Search result more' => 'Show more',
  'Unfortunately, upon request' => 'Unfortunately, the information you have requested',
  'there is nothing' => 'is not available at this time',
  'Unable to resolve the request "{route}".' => '',
    'Regions catalog' => 'Catalog of the regions',
    'Search in name' => 'Search by name',
    'Founded' => 'Found',
    'founded' => 'found',
    'On search' => 'On request',
    'regions' => 'regions',
);