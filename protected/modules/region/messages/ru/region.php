<?php 
return array(
    'region' => 'регион',
    'In no tabs.' => 'В закладках регионов нет. Сохраняй регионы, сравнивай показатели. Держи их под рукой, это удобно',
    'Enter into the search box or try a new query filters' => 'Введи в поисковое окно новый запрос или воспользуйся фильтрами',
    'Search result more' => 'Показать еще',
    'Unfortunately, upon request' => 'К сожалению, по запросу',
    'there is nothing' => 'ничего нет',
    'Unable to resolve the request "{route}".' => 'Невозможно обработать запрос "{route}".',
    'Regions catalog' => 'Каталог регионов',
    'Search in name' => 'Поиск по названию',
    'Founded' => 'Найдено',
    'founded' => 'найдено',
    'On search' => 'По запросу',
    'regions' => 'регионов',
);