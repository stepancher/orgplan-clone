<?php
Yii::import('ext.yii-mail.YiiMailMessage');
Yii::import('application.modules.mail.MailModule');

/**
 * Системные уведомления для пользователей
 * Class NotificationClickButtonCommand
 */
class NotificationClickButtonCommand extends CConsoleCommand
{
	public $baseUrl;

	/**
	 * @param $email
	 * @param $content
	 * @param null $subject
	 * @return int
	 */
	public function sendEmail($email, $content, $subject = null)
	{
		/**
		 * @var YiiMailMessage|Swift_Message $message
		 */
		$message = new YiiMailMessage;
		$message->setBody($content, 'text/html');
		$message->addTo($email);
		$message->subject = $subject;

		$message->addFrom('info@orgplan.ru');
		/** @var YiiMail $mailer
		 * @var CWebApplication $app
		 */
		$app = Yii::app();
		$mailer = $app->mail;
		return $mailer->send($message);
	}

	/**
	 * получения базового пути
	 * @return CUrlManager
	 */
	public function getUrlManager()
	{
		$config = require(Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'config/web.php');
		Yii::app()->setComponent(
			'urlManager',
			array_merge(
				$config['components']['urlManager'],
				array(
					'baseUrl' => $this->baseUrl
				)
			)
		);
		return Yii::app()->urlManager;
	}

	/**
	 * Создание пути
	 * @param $route
	 * @param array $params
	 * @return string
	 */
	public function createUrl($route, $params = array())
	{
		return $this->getUrlManager()->createUrl($route, $params);
	}

}