<?php
Yii::import('ext.yii-mail.YiiMailMessage');
Yii::import('application.modules.tz.models.Auction');
Yii::import('application.modules.mail.MailModule');
Yii::import('application.modules.auth.models.RecoverPassword');
Yii::import('application.modules.notification.models.Notification');

/**
 * Системные уведомления для пользователей
 * Class NotificationCommand
 */
class NotificationCommand extends CConsoleCommand
{
    public $baseUrl;
    public $adminEmail;
    public $layout = '_layout';

    /**
     * @param $email
     * @param $content
     * @param $view
     * @param null $subject
     * @param null $data
     * @param null $fileName
     * @param null $contentType
     * @param array $content
     * @return int
     */
    //@TODO refactor logo sending
    public function sendEmail($email, $content, $subject = null, $data = null, $fileName = null, $contentType = null, $view = '', $logo = NULL)
    {
        /**
         * @var YiiMailMessage|Swift_Message $message
         */
        $message = new YiiMailMessage;
        $controller = new CController('YiiMail');
        Yii::app()->mail->viewPath = 'application.modules.mail.views';
        $viewPath = Yii::getPathOfAlias(Yii::app()->mail->viewPath . '.' . $view) . '.php';

        $message->view = $this->layout;
        $message->addTo($email);
        $message->setFrom(['info@protoplan.pro' => 'Protoplan']);
        $message->setSubject($subject);
        $message->setBody([
            'controller' => $controller,
            'content' => $content,
            'logo' => $logo,
            'viewPath' => $viewPath,
            'view' => $view,
            'mailViewPath' => Yii::app()->mail->viewPath,
        ], 'text/html');

        $sentMessage = new SentMessage();

        $sentMessage->viewLayout = $this->layout;
        $sentMessage->from = 'info@protoplan.pro';
        $sentMessage->to = $email;
        $sentMessage->subject = $subject;
//        $sentMessage->content = $content;
        $sentMessage->logo = $logo;
        $sentMessage->viewPath = $viewPath;
        $sentMessage->view = $view;
        $sentMessage->mailViewPath = Yii::app()->mail->viewPath;

        if ($data && $fileName) {
            $swiftAttachment = Swift_Attachment::newInstance($data, $fileName, $contentType);
            $message->attach($swiftAttachment);

            $sentMessage->attachmentData = $data;
            $sentMessage->attachmentFileName = $fileName;
            $sentMessage->attachmentContentType = $contentType;
        }

        $sentMessage->raw = $message->getBody();
        $sentMessage->messageId = $message->getId();

        $sentMessage->save();

        /** @var YiiMail $mailer */
        $mailer = Yii::app()->mail;
        return $mailer->send($message);
    }

    /**
     * Прикрипление .ics к сообщению
     * @param $fairId
     * @param $email
     * @param $userId
     * @param $content
     */
    public function actionAttachCalendarEmail($email, $userId, $content, $fairId = null)
    {
        /**
         * @var MyFair $myFair
         */

        if ($fairId) {
            $myFair = MyFair::model()->findAllByAttributes([
                'userId' => $userId,
                'fairId' => $fairId,
                'status' => MyFair::STATUS_ACTIVE
            ]);
        } else {
            $myFair = MyFair::model()->findAllByAttributes([
                'userId' => $userId,
                'status' => MyFair::STATUS_ACTIVE
            ]);
        }
        if ($myFair) {
            $this->sendEmail(
                $email,
                'К этому сообщению прикреплен .ics файл календаря.' . '</br>' . rawurldecode($content),
                'PROTOPLAN.PRO - Календарь',
                $this->getCalendarICSFormat($myFair),
                'calendar.ics', 'text/calendar'
            );
        }
    }

    /**
     * Форматирование даты в .ics календаре
     * @param $timestamp
     * @return bool|string
     */
    public function dateToCal($timestamp)
    {
        return date('Ymd\THis\Z', strtotime($timestamp));
    }

    /**
     * @param MyFair $myFair
     * @return string ics format
     */
    public function getCalendarICSFormat($myFair)
    {
        $ics = 'BEGIN:VCALENDAR' . PHP_EOL;
        $ics .= 'VERSION:2.0' . PHP_EOL;
        $ics .= 'PRODID:-//svs-okno//SVS Calendar//EN' . PHP_EOL;
        $ics .= 'CALSCALE:GREGORIAN' . PHP_EOL;

        $tasks = array();

        if (is_array($myFair)) {
            $myFairs = $myFair;
            foreach ($myFairs as $myFair) {
                if ($myFair->tasks) {
                    $tasks = array_merge($tasks, $myFair->tasks);
                }
            }
        } else {
            $tasks = $myFair->tasks;
        }

        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $ics .= 'BEGIN:VEVENT' . PHP_EOL;
                $ics .= 'UID:' . md5($task->id) . '@' . $this->baseUrl . PHP_EOL;
                $ics .= 'DTSTAMP:' . gmdate('Ymd') . 'T' . gmdate('His') . 'Z' . PHP_EOL;
                $ics .= 'DTSTART:' . $this->dateToCal($task->startDate) . PHP_EOL;
                $ics .= 'DTEND:' . $this->dateToCal($task->endDate) . PHP_EOL;
                $ics .= 'SUMMARY:' . $task->name . PHP_EOL;
                $ics .= 'DESCRIPTION:' . $task->description . PHP_EOL;
                $ics .= 'END:VEVENT' . PHP_EOL;
            }
        }

        $ics .= 'END:VCALENDAR' . PHP_EOL;

        return $ics;
    }

    /**
     * Определение времени записи
     * @param $count
     * @param array $arrayString
     * @return string
     */
    public function getTimeText($count, array $arrayString)
    {
        if ($count == 0) {
            return null;
        } elseif (($length = strlen($count)) > 1 && ($lastNumb = substr($count, $length - 2, 2)) > 10 && $lastNumb < 15) {
            return $count . ' ' . $arrayString[0];
        } elseif ($count % 10 == 1) {
            return $count . ' ' . $arrayString[1];
        } elseif ($count % 10 > 1 && $count % 10 < 5) {
            return $count . ' ' . $arrayString[2];
        } else {
            return $count . ' ' . $arrayString[0];
        }
    }

    /**
     * Проверка даты финала аукциона
     * @throws CDbException
     */
    public function actionCheckFinalDate()
    {
        echo 'Start action CheckFinalDate' . PHP_EOL;

        /** @var Auction[] $auctions */
        $model = Auction::model();
        $conn = $model->getDbConnection();
        $auctions = $model->with('auctionTechnicalData')->findAllByAttributes(
            array('step' => Auction::STEP_BEGIN),
            $conn->quoteColumnName('auctionTechnicalData.finalPickDate') . ' < ' . $conn->quoteValue(date('Y-m-d H:i:s', time() - 3600 * 24))
        );
        foreach ($auctions as $auction) {
            if (!empty($auction->bids)) {
                $this->sendByStatus($auction, Bid::STATUS_FINALIST, Notification::TYPE_NOT_SELECTED_FINALIST, 'Необходимо выбрать финалиста!');
            }
        }
    }

    /**
     * Проверка даты окончания аукциона
     * @throws CDbException
     */
    public function actionCheckEndDate()
    {
        echo 'Start action CheckEndDate' . PHP_EOL;

        /** @var Auction[] $auctions */
        $model = Auction::model();
        $conn = $model->getDbConnection();
        $auctions = $model->with('auctionTechnicalData')->findAllByAttributes(
            array('step' => Auction::STEP_FINAL),
            $conn->quoteColumnName('auctionTechnicalData.endDate') . ' < ' . $conn->quoteValue(date('Y-m-d H:i:s', time() - 3600 * 24))
        );
        foreach ($auctions as $auction) {
            if (!empty($auction->bids)) {
                $this->sendByStatus($auction, Bid::STATUS_WINNER, Notification::TYPE_NOT_SELECTED_WINNER, 'Необходимо выбрать победителя!');
            }
        }
    }

    /**
     * оповещение о необходимости выполнить задачу
     * @throws CDbException
     */
    public function actionDayTask()
    {

        /** @var Task[] $tasks */
        /*if (!isset($_SERVER['REQUEST_METHOD'])) {
            $model = Task::model();
            $tasks = $model->findAllByAttributes(
                array('status' => Task::STATUS_OPEN)
            );
            if (!empty($tasks)) {
                foreach ($tasks as $task) {
                    $date = new DateTime($task->endDate);
                    $now = new DateTime('now');
                    $interval = $date->diff($now);
                    $string = $this->getTimeText($interval->d, array('Дней', 'День', 'Дня')) . ' ' . $this->getTimeText($interval->h, array('Часов', 'Час', 'Часа'));
                    if ($task->calendar->user->userSetting->dayTask == true && $task->calendar->user->userSetting->type == UserSetting::TYPE_ALL) {
                        if ($task->responsibleId) {
                            $resultResponsible = Notification::saveByObject(
                                $task->responsible->id, // Получатель
                                $task->calendar, // Обьект
                                Notification::TYPE_DAY_TASK, // Тип уведомления
                                array( // Шаблон, в зависимости от типа уведомления, если нужно
                                    '{name}' => CHtml::link($task->name, $this->createUrl('calendar/view', array('id' => $task->id))),
                                    '{date}' => $string
                                ),
                                null,
                                false
                            );
                            if ($resultResponsible) {
                                echo 'Notification created. id: ' . $resultResponsible->id . PHP_EOL;
                                $this->sendEmail($task->responsible->email, $resultResponsible->content, 'Необходимо выполнить задачу!');
                                echo 'System notification is send to the user-ID: "' . $task->responsibleId . '" by: "' . $task->name . PHP_EOL;
                            }
                        }
                        $result = Notification::saveByObject(
                            $task->calendar->userId, // Получатель
                            $task->calendar, // Обьект
                            Notification::TYPE_DAY_TASK, // Тип уведомления
                            array( // Шаблон, в зависимости от типа уведомления, если нужно
                                '{name}' => CHtml::link($task->name, $this->createUrl('calendar/view', array('id' => $task->id))),
                                '{date}' => $string
                            ),
                            null,
                            false
                        );
                        if ($result) {
                            echo 'Notification created. id: ' . $result->id . PHP_EOL;
                            $this->sendEmail($task->calendar->user->contact->email, $result->content, 'Необходимо выполнить задачу!');
                            echo 'System notification is send to the user-ID: "' . $task->calendar->userId . '" by: "' . $task->name . PHP_EOL;
                        }
                    } elseif ($task->calendar->user->userSetting->dayTask == true && $task->calendar->user->userSetting->type == UserSetting::TYPE_TO_SITE) {
                        $result = Notification::saveByObject(
                            $task->calendar->userId, // Получатель
                            $task->calendar, // Обьект
                            Notification::TYPE_DAY_TASK, // Тип уведомления
                            array( // Шаблон, в зависимости от типа уведомления, если нужно
                                '{name}' => CHtml::link($task->name, $this->createUrl('calendar/view', array('id' => $task->id))),
                                '{date}' => $string
                            ),
                            null,
                            false
                        );
                        if ($task->responsibleId) {
                            $resultResponsible = Notification::saveByObject(
                                $task->responsible->id, // Получатель
                                $task->calendar, // Обьект
                                Notification::TYPE_DAY_TASK, // Тип уведомления
                                array( // Шаблон, в зависимости от типа уведомления, если нужно
                                    '{name}' => CHtml::link($task->name, $this->createUrl('calendar/view', array('id' => $task->id))),
                                    '{date}' => $string
                                ),
                                null,
                                false
                            );
                            if ($resultResponsible) {
                                echo 'Notification created. id: ' . $resultResponsible->id . PHP_EOL;
                                echo 'System notification is send to the user-ID: "' . $task->responsibleId . '" by: "' . $task->name . PHP_EOL;
                            }
                        }
                        if ($result) {
                            echo 'Notification created. id: ' . $result->id . PHP_EOL;
                            echo 'System notification is send to the user-ID: "' . $task->calendar->userId . '" by: "' . $task->name . PHP_EOL;

                        }
                    } elseif ($task->calendar->user->userSetting->dayTask == true && $task->calendar->user->userSetting->type == UserSetting::TYPE_TO_EMAIL) {
                        $content = strtr(
                            Notification::types()[Notification::TYPE_DAY_TASK],
                            array( // Шаблон, в зависимости от типа уведомления, если нужно
                                '{name}' => CHtml::link($task->name, $this->createUrl('calendar/view', array('id' => $task->id))),
                                '{date}' => $string
                            )
                        );
                        if ($task->responsibleId) {
                            $content = strtr(
                                Notification::types()[Notification::TYPE_DAY_TASK],
                                array( // Шаблон, в зависимости от типа уведомления, если нужно
                                    '{name}' => CHtml::link($task->name, $this->createUrl('calendar/view', array('id' => $task->id))),
                                    '{date}' => $string
                                )
                            );
                            $this->sendEmail($task->responsible->email, $content, 'Необходимо выполнить задачу!');
                            echo 'System notification is send to the user-ID: "' . $task->responsibleId . '" by: "' . $task->name . PHP_EOL;
                        }

                        $this->sendEmail($task->calendar->user->contact->email, $content, 'Необходимо выполнить задачу!');
                        echo 'System notification is send to the email: "' . $task->calendar->user->contact->email . PHP_EOL;

                    }
                }
            }
        }*/
    }

    /**
     * @param $id
     * Отправка задачи на Email
     */
    public function actionTaskSendEmail($id)
    {
        /** @var Task $model */
      /*  if (($model = Task::model()->findByPk($id)) !== null && $model->responsibleId && $model->responsible) {
            $content = strtr(
                Notification::types()[Notification::TYPE_TASK],
                array( // Шаблон, в зависимости от типа уведомления, если нужно
                    '{name}' => $model->name,
                    '{description}' => $model->description,
                    '{duration}' => $model->duration,
                    '{beginDate}' => $model->startDate,
                    '{endDate}' => $model->endDate,
                )
            );
            $this->sendEmail(
                $model->responsible->email,
                $content,
                'Вас выбрали ответственным исполнителем',
                $this->getTaskICSFormat($model->id),
                'calendar.ics', 'text/calendar'
            );
        }*/
    }

    /**
     * Отправка активации на @mail
     * @param $id
     * @param $email
     * @param $lang
     */
    public function actionSendActivatedMessage($id, $email, $lang = 'ru')
    {
        if(!empty($lang)){
            Yii::app()->setLanguage($lang);
        }

        /** @var User $model */
        $model = User::model()->findByPk($id);
        if ($model) {
            $url = $this->createUrl('/profile/default', array(
                'id' => $model->id,
                'activated' => md5($model->password)
            ));
            $content = strtr(
                Notification::types()[Notification::TYPE_ACTIVATED],
                array( // Шаблон, в зависимости от типа уведомления, если нужно
                    '{firstName}' => $model->firstName,
                    '{link}' => TbHtml::link($url, $url)
                )
            );
            $this->sendEmail(
                $email,
                $content,
                $subject = $model->firstName . ', '. Yii::t('MailModule.mail', 'hi orgplan')
            );
        }
    }

    /**
     * Формирование задачи в .ics форамте
     * @param $id
     * @return null|string
     */
    public function getTaskICSFormat($id)
    {
        /** @var Task $model */
       /* if (($model = Task::model()->findByPk($id)) !== null) {
            $ics = 'BEGIN:VCALENDAR' . PHP_EOL;
            $ics .= 'VERSION:2.0' . PHP_EOL;
            $ics .= 'PRODID:-//svs-okno//SVS Calendar//EN' . PHP_EOL;
            $ics .= 'CALSCALE:GREGORIAN' . PHP_EOL;

            $ics .= 'BEGIN:VEVENT' . PHP_EOL;
            $ics .= 'UID:' . md5($model->id) . '@svs-okno.gmail.com' . PHP_EOL;
            $ics .= 'DTSTAMP:' . gmdate('Ymd') . 'T' . gmdate('His') . 'Z' . PHP_EOL;
            $ics .= 'DTSTART:' . $this->dateToCal($model->startDate) . PHP_EOL;
            $ics .= 'DTEND:' . $this->dateToCal($model->endDate) . PHP_EOL;
            $ics .= 'SUMMARY:' . $model->name . PHP_EOL;
            $ics .= 'DESCRIPTION:' . $model->description . PHP_EOL;
            $ics .= 'END:VEVENT' . PHP_EOL;

            $ics .= 'END:VCALENDAR' . PHP_EOL;

            return $ics;
        }
        return null;*/
    }

    /**
     * получения базового пути
     * @return CUrlManager
     */
    public function getUrlManager()
    {
        $config = require(Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'config/web.php');
        Yii::app()->setComponent(
            'urlManager',
            array_merge(
                $config['components']['urlManager'],
                array(
                    'baseUrl' => 'https://protoplan.pro'
                )
            )
        );
        return Yii::app()->urlManager;
    }

    /**
     * Создание пути
     * @param $route
     * @param array $params
     * @return string
     */
    public function createUrl($route, $params = array())
    {
        return $this->getUrlManager()->createUrl($route, $params);
    }

    /**
     * Проверка окончании даты выставки
     * @throws CDbException
     */
    public function actionFairCheckEndDate()
    {
        /** @var Fair[] $fairs */
        $model = Fair::model();
        $conn = $model->getDbConnection();
        $fairs = $model->findAll(
            $conn->quoteColumnName('endDate') . ' < ' . $conn->quoteValue(date('d.m.Y', time()))
        );
        if (!empty($fairs)) {
            foreach ($fairs as $fair) {
                if ($fair->user->userSetting->type == UserSetting::TYPE_TO_SITE || $fair->user->userSetting->type == UserSetting::TYPE_ALL) {
                    $result = Notification::saveByObject(
                        $fair->userId,
                        $fair,
                        Notification::TYPE_FAIR_CHECK_END_DATE,
                        array(
                            '{name}' => CHtml::link($fair->name, $this->createUrl('fair/default/view', array('id' => $fair->id))),
                        ),
                        null,
                        false
                    );
                    if ($result) {
                        echo 'Notification created. id: ' . $result->id . PHP_EOL;
                        echo 'System notification is send to the user-ID: "' . $fair->userId . '" by: "' . $fair->name . '".' . PHP_EOL;
                    }
                }
                if ($fair->user->userSetting->type == UserSetting::TYPE_TO_EMAIL || $fair->user->userSetting->type == UserSetting::TYPE_ALL) {
                    $content = strtr(
                        Notification::types()[Notification::TYPE_FAIR_CHECK_END_DATE],
                        array(
                            '{name}' => CHtml::link($fair->name, $this->createUrl('fair/view', array('id' => $fair->id))),
                        )
                    );
                    $this->sendEmail($fair->user->contact->email, $content, 'Дата окончания Вашей выставки');
                    echo 'System notification is send to the email: "' . $fair->user->contact->email . PHP_EOL;

                }
            }
        }
    }

    public function actionRecoverPassword($langId, $id)
    {
        if(!empty($langId)){
            Yii::app()->setLanguage($langId);
        }

        /** @var User $user */

        $user = User::model()->findByPk($id);
        if ($user !== null) {
            $hash = md5($id . microtime());
            $recoverPassword = new RecoverPassword;
            $recoverPassword->userId = $id;
            $recoverPassword->hash = $hash;
            $recoverPassword->save(false);
            $url = $this->createUrl(
                '/auth/default/resetPassword', array('hash' => $hash)
            );
            $result = Notification::saveByObject(
                $user->id, $user, Notification::TYPE_RECOVER_PASSWORD, array(
                '{text}' => $user->firstName == '' ?
                    Yii::t('MailModule.mail', 'For change password click on link ') :
                    $user->firstName . Yii::t('MailModule.mail', ', for change password click on link '),
                '{link}' => CHtml::link(Yii::t('MailModule.mail', 'link'), $url),
            ), null, false);
            if ($result) {
                $this->sendEmail(
                    $user->contact->email,
                    Yii::t('MailModule.mail', 'For change password click on link ').' '.CHtml::link($url, $url),
                    $subject = Yii::t('MailModule.mail', 'recovery password orgplan'));
                echo 'System notification is send to the email: "' . $user->contact->email . PHP_EOL;
            }
        }
    }

    public function actionRequestSendEmail($notificationId, $standId)
    {
        $notification = Notification::model()->findByPk($notificationId);
        if ($notification) {
            $admins = User::model()->findAllByAttributes(['role' => User::ROLE_ADMIN]);
            foreach ($admins as $admin) {
                $subject = 'Запрос цены для стенда $stand->name ';
                $this->sendEmail($admin->contact->email, $notification->content, $subject);
            }
        }
    }

    public function actionPriceRequestSendEmail($notificationId, $cityId, $categoryId, $productName)
    {
        $notification = Notification::model()->findByPk($notificationId);
        $city = City::model()->findByPk($cityId);
        $category = ProductCategory::model()->findByPk($categoryId);
        if ($notification && $city && $category) {
            $admins = User::model()->findAllByAttributes(['role' => User::ROLE_ADMIN]);
            foreach ($admins as $admin) {
                $subject = 'Запрос цены для города "' . $city->name . '"'.' категория услуг: "'.$category->name.'" услуга: "'.$productName.'"';
                $this->sendEmail($admin->contact->email, $notification->content, $subject);
            }
        }
    }

    public function actionRequestDemoSendEmail($firstName, $lastName, $email, $company)
    {
        $managers = User::model()->findAllByAttributes(['role' => User::ROLE_MANAGER]);

        foreach ($managers as $manager) {

            if(empty($manager->contact)){
                continue;
            }

            $subject = 'Запрос демо доступа в Protoplan.';
            $content = 'Пользователь "' . $firstName . ' ' . $lastName . '" email: ('.$email.')'.' из компании: "'.$company.'" запросил доступ в тестовый кабинет';
            $this->sendEmail($manager->contact->email, $content, $subject);
        }
    }

    /**
     * @param $email
     * @param $logo
     * @param $lang
     * @param $fairId
     * @param $password
     */
    public function actionCreatedExponentSendEmail($notificationId, $email, $password, $logo, $lang = NULL, $fairId)
    {
        if(!empty($lang)){
            Yii::app()->setLanguage($lang);
        }

        $content = ['email' => $email, 'password' => $password, 'logo' => $logo, 'lang' => $lang, 'fairId' => $fairId];

        $this->sendEmail($email, $content, Yii::t('MailModule.mail','Welcome to Protoplan!'), NULL, NULL, NULL, 'exponent-create');
    }

    /**
     * @param $email
     * @param $ecLabel
     * @param $elId
     * @param $company
     * @param $ecPluralLabel
     * @param $logo
     * @param $notificationId
     * @param $langId
     */
    public function actionSendProposal($langId = 'ru', $email, $ecLabel, $elId, $company, $ecPluralLabel, $notificationId, $logo, $applicationPath = NULL)
    {
        if(!empty($langId)){
            Yii::app()->setLanguage($langId);
        }

        $notification = Notification::model()->findByPk($notificationId);

        $files = [];

        if ($notification !== NULL) {
            $data = json_decode($notification->data);

            foreach ($data->files as $file) {


                if (file_exists($file->path)) {
                    $data = file_get_contents($file->path);
                }

                array_push($files, ['name' => $file->name, 'data' => $data]);
            }

            $proposalPath = Yii::getPathOfAlias('application.uploads') . '/proposals/' . $elId . '.html';

            if(!empty($applicationPath)){
                $proposalPath = $applicationPath . '/proposals/' . $elId . '.html';
            }

            $proposalFile = NULL;
            $proposalFileName = NULL;

            if (file_exists($proposalPath)) {
                $proposalFile = file_get_contents($proposalPath);
                $proposalFileName = 'proposal.html';
                array_push($files, ['name' => $proposalFileName, 'data' => $proposalFile]);
            }
        }

        $this->send(
            $email,
            [
                'ec' => $ecLabel,
                'el' => $elId,
                'company' => $company,
                'name' => $ecPluralLabel,
                'logo' => $logo,
            ],
            Yii::t('MailModule.mail', 'Exponent ').'"' . $company . '"'.  Yii::t('MailModule.mail', ' was sent proposal ')  . $ecLabel . ' ' . $ecPluralLabel,
            $files,
            'sendProposal'
        );
    }

    /**
     * @param $email
     * @param $ecLabel
     * @param $elId
     * @param $logo
     * @param $company
     * @param $header
     * @param $language
     * @param $ecPluralLabel
     * @param $notificationId
     */
    public function actionSendProposalToExponent($email, $ecLabel, $elId, $company, $ecPluralLabel, $notificationId, $logo, $header, $language = NULL, $applicationPath = NULL)
    {

        if(!empty($language)){
            Yii::app()->setLanguage($language);
        }

        $notification = Notification::model()->findByPk($notificationId);

        $files = [];

        if ($notification !== NULL) {
            $data = json_decode($notification->data);

            foreach ($data->files as $file) {

                if (file_exists($file->path)) {
                    $data = file_get_contents($file->path);
                }

                array_push($files, ['name' => $file->name, 'data' => $data]);
            }

            $proposalPath = Yii::getPathOfAlias('application.uploads') . '/proposals/' . $elId . '.html';

            if(!empty($applicationPath)){
                $proposalPath = $applicationPath . '/proposals/' . $elId . '.html';
            }

            $proposalFile = NULL;
            $proposalFileName = NULL;

            if (file_exists($proposalPath)) {
                $proposalFile = file_get_contents($proposalPath);
                $proposalFileName = 'proposal.html';
                array_push($files, ['name' => $proposalFileName, 'data' => $proposalFile]);
            }
        }

        $this->send(
            $email,
            [
                'ec' => $ecLabel,
                'el' => $elId,
                'company' => $company,
                'name' => $ecPluralLabel,
                'logo' => $logo,
            ],
            $header,
            $files,
            'sendProposalToExponent'
        );
    }

    /**
     * @param $email
     * @param $notificationId
     * @param $header
     * @param $language
     */
    public function actionSendAcceptingProposalToExponent($email, $notificationId, $header, $language = NULL, $logo) {

        if(!empty($language)){
            Yii::app()->setLanguage($language);
        }

        $notification = Notification::model()->findByPk($notificationId);
        if ($notification) {
            $this->sendEmail(
                $email,
                $notification->content,
                $header,
                NULL,
                NULL,
                NULL,
                NULL,
                $logo
            );
        }
    }

    /**
     * @param $email
     * @param $notificationId
     * @param $header
     * @param $language
     */
    public function actionSendRejectingProposalToExponent($email, $notificationId, $header, $language = NULL, $logo) {

        if(!empty($language)){
            Yii::app()->setLanguage($language);
        }

        $notification = Notification::model()->findByPk($notificationId);
        if ($notification) {
            $this->sendEmail(
                $email,
                $notification->content,
                $header,
                NULL,
                NULL,
                NULL,
                NULL,
                $logo
            );
        }
    }

    /**
     * @param $email
     * @param $notificationId
     * @param string $header
     * @param null $language
     * @param $logo
     */
    public function actionSendDeletingProposalToExponent($email, $notificationId, $header = '', $language = NULL, $logo) {

        $notification = Notification::model()->findByPk($notificationId);
        if ($notification) {
            $this->sendEmail(
                $email,
                $notification->content,
                $header,
                NULL,
                NULL,
                NULL,
                NULL,
                $logo
            );
        }
    }

    /**
     * @param int $taskId
     */
    public function actionCalendarEditTask($taskId)
    {

        $usersTasks = Yii::app()->db->createCommand()
            ->select([
                'users_tasks.id',
                'tasks.name',
                'parent_users_tasks.completeDate as parent_complete',
                'tasks.duration as t_duration',
                'tasks.date as t_date',
                'users_tasks.duration as ut_duration',
                'users_tasks.date as ut_date',
                'fair.id as fair_id',
                'IFNULL(trfair.name, fair.name) as fair',
                'user.firstName as username',
                'contact.email'
            ])
            ->from('{{calendarfairtasks}} tasks')
            ->join('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid AND users_tasks.completeDate IS NULL')
            ->leftJoin('{{calendarfairuserstasks}} parent_users_tasks', 'tasks.parent = parent_users_tasks.uuid AND users_tasks.userId = parent_users_tasks.userId AND parent_users_tasks.completeDate IS NOT NULL')
            ->join('{{fair}} fair', 'tasks.fairId = fair.id')
            ->leftJoin('{{trfair}} trfair', 'trfair.trParentId = tasks.fairId AND trfair.langId = :langId', [':langId' => Yii::app()->language])
            ->join('{{user}} user', 'users_tasks.userId = user.id')
            ->join('{{contact}} contact', 'user.contactId = contact.id')
            ->where('tasks.id = :taskId', [':taskId' => $taskId])
            ->queryAll();

        foreach ($usersTasks as $usersTask) {

            $duration = $usersTask['ut_duration'] ?: $usersTask['t_duration'];
            $date = $usersTask['ut_date'] ?: $usersTask['t_date'];
            $startDate = date('Y-m-d H:i:s');
            $endDate = date('Y-m-d H:i:s');

            if (!$duration) {
                if ($usersTask['parent_complete']) {
                    $startDate = $usersTask['parent_complete'];
                }
                $endDate = $date;
            }
            if ($duration > 0) {
                if ($date) {
                    $startDate = $date;
                } elseif ($usersTask['parent_complete']) {
                    $startDate = $usersTask['parent_complete'];
                }
                $endDate = date('Y-m-d H:i:s', strtotime($startDate . ' +' . $duration . ' days'));
            }

            $usersTask['email'] = 'andrey.diveev@orgplan.pro';
            $fair = $usersTask['fair'] . ' - ' . date('Y');
            $this->sendEmail($usersTask['email'], [
                'username' => $usersTask['username'],
                'tasks' => [
                    [
                        'fairId' => $usersTask['fair_id'],
                        'fair' => $fair,
                        'task' => $usersTask['name'],
                        'startDate' => $startDate,
                        'endDate' => $endDate
                    ]
                ],
                'text' => 'Организатор изменил задачу:'
            ], 'Организатор изменил задачу (' . $fair . ')', null, null, null, 'calendar.organizerChangeTask');
        }
    }

    /**
     * @param int $taskId
     * @param int $userId
     */
    public function actionCalendarAddTask($taskId, $userId) {

        $usersTask = Yii::app()->db->createCommand()
            ->select([
                'tasks.id',
                'tasks.name',
                'parent_users_tasks.completeDate as parent_complete',
                'tasks.duration as t_duration',
                'tasks.date as t_date',
                'users_tasks.duration as ut_duration',
                'users_tasks.date as ut_date',
                'fair.id as fair_id',
                'IFNULL(trfair.name, fair.name) as fair',
                'user.firstName as username',
                'contact.email'
            ])
            ->from('{{calendarfairtasks}} tasks')
            ->join('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid AND users_tasks.completeDate IS NULL')
            ->leftJoin('{{calendarfairuserstasks}} parent_users_tasks', 'tasks.parent = parent_users_tasks.uuid AND users_tasks.userId = parent_users_tasks.userId AND parent_users_tasks.completeDate IS NOT NULL')
            ->join('{{fair}} fair', 'tasks.fairId = fair.id')
            ->leftJoin('{{trfair}} trfair', 'trfair.trParentId = tasks.fairId AND trfair.langId = :langId', [':langId' => Yii::app()->language])
            ->join('{{user}} user', 'users_tasks.userId = user.id')
            ->join('{{contact}} contact', 'user.contactId = contact.id')
            ->where('tasks.id = :taskId', [':taskId' => $taskId])
            ->andWhere('users_tasks.userId = :userId', [':userId' => $userId])
            ->queryRow();

        $duration = $usersTask['ut_duration'] ?: $usersTask['t_duration'];
        $date = $usersTask['ut_date'] ?: $usersTask['t_date'];
        $startDate = date('Y-m-d H:i:s');
        $endDate = date('Y-m-d H:i:s');

        if (!$duration) {
            if ($usersTask['parent_complete']) {
                $startDate = $usersTask['parent_complete'];
            }
            $endDate = $date;
        }
        if ($duration > 0) {
            if ($date) {
                $startDate = $date;
            } elseif ($usersTask['parent_complete']) {
                $startDate = $usersTask['parent_complete'];
            }
            $endDate = date('Y-m-d H:i:s', strtotime($startDate . ' +' . $duration . ' days'));
        }

        // @todo remove after testing
        $usersTask['email'] = 'andrey.diveev@orgplan.pro';

        $fair = $usersTask['fair'] . ' - ' . date('Y');
        $this->sendEmail($usersTask['email'], [
            'username' => $usersTask['username'],
            'tasks' => [
                [
                    'fairId' => $usersTask['fair_id'],
                    'fair' => $fair,
                    'task' => $usersTask['name'],
                    'startDate' => $startDate,
                    'endDate' => $endDate
                ]
            ],
            'text' => 'У вас новая задача:'
        ], 'У вас новая задача (' . $fair . ')', null, null, null, 'calendar.organizerChangeTask');
    }

    public function actionCalendarDayTasks() {

        $fairTasks = Yii::app()->db->createCommand()
            ->select([
                'tasks.name',
                'tasks.duration as t_duration',
                'tasks.date as t_date',

                'parent_users_tasks.completeDate as parent_complete',

                'users_tasks.duration as ut_duration',
                'users_tasks.date as ut_date',

                'fair.id as fair_id',
                'IFNULL(trfair.name, fair.name) as fair',

                'user.id as user_id',
                'user.firstName as username',
                'contact.email',
                'user_setting.dailyEventNotification as notification',
                'user_setting.notificationBeforeDay as before_day'
            ])
            ->from('{{calendarfairtasks}} tasks')

            ->join('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid AND users_tasks.completeDate IS NULL')
            ->leftJoin('{{calendarfairuserstasks}} parent_users_tasks', 'tasks.parent = parent_users_tasks.uuid AND users_tasks.userId = parent_users_tasks.userId AND parent_users_tasks.completeDate IS NOT NULL')
            ->join('{{fair}} fair', 'tasks.fairId = fair.id')
            ->leftJoin('{{trfair}} trfair', 'trfair.trParentId = tasks.fairId AND trfair.langId = :langId', [':langId' => Yii::app()->language])

            ->join('{{user}} user', 'users_tasks.userId = user.id')
            ->join('{{contact}} contact', 'user.contactId = contact.id')
            ->join('{{usersetting}} user_setting', 'user_setting.userId = user.id')

            ->where('user_setting.notificationTime = :notificationTime', [':notificationTime' => date('G')])

            // @todo remove after testing
            ->andWhere('tasks.fairId = :fairId', [':fairId' => 184])

            ->order('user.id, fair.id, tasks.id')

            ->queryAll();


        $results = [];
        foreach ($fairTasks as $key => $usersTask) {

            $duration = $usersTask['ut_duration'] ?: $usersTask['t_duration'];
            $date = $usersTask['ut_date'] ?: $usersTask['t_date'];
            $startDate = date('Y-m-d H:i:s');
            $endDate = date('Y-m-d H:i:s');

            if (!$duration) {
                if ($usersTask['parent_complete']) {
                    $startDate = $usersTask['parent_complete'];
                }
                $endDate = $date;
            }
            if ($duration > 0) {
                if ($date) {
                    $startDate = $date;
                } elseif ($usersTask['parent_complete']) {
                    $startDate = $usersTask['parent_complete'];
                }
                $endDate = date('Y-m-d H:i:s', strtotime($startDate . ' +' . $duration . ' days'));
            }

            $now = time();
            $before = strtotime(' +' . $usersTask['before_day'] . ' days');

            if (
                ($now <= strtotime($endDate) && strtotime($endDate) <= $before) ||
                ($usersTask['notification'] && strtotime($startDate) <= $now && $now <= strtotime($endDate))
            ) {

                $id = $usersTask['user_id'];

                $results[$id]['user'] = [
                    'id' => $usersTask['user_id'],
                    'name' => $usersTask['username'],
                    'email' => $usersTask['email'],
                    'notification' => $usersTask['notification']
                ];

                $fair_id = $usersTask['fair_id'];

                $results[$id]['fairs'][$fair_id]['id'] = $usersTask['fair_id'];
                $results[$id]['fairs'][$fair_id]['name'] = $usersTask['fair'] . ' - ' . date('Y');

                $results[$id]['fairs'][$fair_id]['tasks'][] = [
                    'task' => $usersTask['name'],
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'personal' => 0
                ];
            }
        }

        $usersTasks = Yii::app()->db->createCommand()
            ->select([
                'user_task.name',

                'user_task.startDate',
                'user_task.endDate',

                'fair.id as fair_id',
                'IFNULL(trfair.name, fair.name) as fair',

                'user.id as user_id',
                'user.firstName as username',
                'contact.email',
                'user_setting.dailyEventNotification as notification',
                'user_setting.notificationBeforeDay as before_day'
            ])
            ->from('{{calendaruserstasks}} user_task')

            ->leftJoin('{{fair}} fair', 'user_task.fairId = fair.id')
            ->leftJoin('{{trfair}} trfair', 'trfair.trParentId = user_task.fairId AND trfair.langId = :langId', [':langId' => Yii::app()->language])

            ->join('{{user}} user', 'user_task.userId = user.id')
            ->join('{{contact}} contact', 'user.contactId = contact.id')
            ->join('{{usersetting}} user_setting', 'user_setting.userId = user.id')

            ->where('user_setting.notificationTime = :notificationTime', [':notificationTime' => date('G')])

            // @todo remove after testing
            ->andWhere('user_task.fairId = :fairId', [':fairId' => 184])

            ->order('user.id, fair.id')
            ->queryAll();

        foreach ($usersTasks as $i => $usersTask) {

            $now = time();
            $before = strtotime(' +' . $usersTask['before_day'] . ' days');
            $startDate = $usersTask['startDate'];
            $endDate = $usersTask['endDate'];

            if (
                ($now <= strtotime($endDate) && strtotime($endDate) <= $before) ||
                ($usersTask['notification'] && strtotime($startDate) <= $now && $now <= strtotime($endDate))
            ) {

                $id = $usersTask['user_id'];

                $results[$id]['user'] = [
                    'id' => $usersTask['user_id'],
                    'name' => $usersTask['username'],
                    'email' => $usersTask['email'],
                    'notification' => $usersTask['notification']
                ];

                $fair_id = $usersTask['fair_id'];

                $results[$id]['fairs'][$fair_id]['id'] = $usersTask['fair_id'];
                $results[$id]['fairs'][$fair_id]['name'] = $usersTask['fair'] . ' - ' . date('Y');

                $results[$id]['fairs'][$fair_id]['tasks'][] = [
                    'task' => $usersTask['name'],
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'personal' => 1
                ];
            }
        }

        foreach ($results as $result) {
            $user['email'] = 'andrey.diveev@orgplan.pro'; //$result['user']['email'];
            $text = 'Ваш список задач на день';
            $message = 'send day tasks for userId: ';
            if (!$result['user']['notification']) {
                $text = 'Сегодня последний день для выполнения задач';
                $message = 'send deadline tasks for userId: ';
            }
            $this->sendEmail($user['email'], [
                'username' => $result['user']['name'],
                'fairs' => $result['fairs'],
                'text' => $text . ':'
            ], $text, null, null, null, 'calendar.dayTasks');
            echo date('Y-m-d H:i:s') . ' ' . $message . $result['user']['id'];
            sleep(5);
        }
    }

    public function actionCalendarUpdateTasks($fairId) {

        Yii::import('app.modules.calendar.models.CalendarFairTempTasks');
        $updatedTasks = Yii::app()->db->createCommand()
            ->select([
                'fair.id as fair_id',
                'IFNULL(trfair.name, fair.name) as fair',
                'user.id as user_id',
                'user.firstName as username',
                'contact.email',
                'temp_task.uuid',
                'temp_task.action',
                'task.name as task_name',
                'task.duration as task_duration',
                'task.date as task_date',
                'temp_task.name as temp_name',
                'temp_task.duration as temp_duration',
                'temp_task.date as temp_date',
                'parent_user_task.completeDate as parent_complete'
            ])
            ->from('{{user}} user')
            ->join('{{contact}} contact', 'user.contactId = contact.id')

            ->join('{{myfair}} myfair', 'user.id = myfair.userId')
            ->join('{{fair}} fair', 'fair.id = myfair.fairId')
            ->leftJoin('{{trfair}} trfair', 'trfair.trParentId = fair.id AND trfair.langId = :langId', [':langId' => Yii::app()->language])

            ->join('{{calendarfairtemptasks}} temp_task', 'temp_task.fairId = myfair.fairId')
            ->leftJoin('{{calendarfairtasks}} task', 'temp_task.uuid = task.uuid')
            ->leftJoin('{{calendarfairuserstasks}} user_task', 'temp_task.uuid = user_task.uuid AND user_task.userId = user.id')

            ->leftJoin('{{calendarfairuserstasks}} parent_user_task', 'temp_task.parent = parent_user_task.uuid AND parent_user_task.userId = user.id')

            ->where('user.role = :roleExponent', [':roleExponent' => User::ROLE_EXPONENT])
            ->andWhere('myfair.fairId = :fairId', [':fairId' => $fairId])

            // @todo remove after testing
            ->andWhere('user.id = 612')

            ->andWhere('
                (temp_task.action = :actionInsert AND temp_task.parent IS NULL AND temp_task.first = 1) OR
                (temp_task.action = :actionInsert AND parent_user_task.completeDate IS NOT NULL) OR
                (temp_task.action IN (:actionUpdate,:actionDelete) AND user_task.uuid IS NOT NULL AND user_task.completeDate IS NULL)',
            [
                ':actionInsert' => CalendarFairTempTasks::ACTION_INSERT,
                ':actionUpdate' => CalendarFairTempTasks::ACTION_UPDATE,
                ':actionDelete' => CalendarFairTempTasks::ACTION_DELETE
            ])

            ->order('fair_id, user_id, action DESC')
            ->queryAll();

        $results = [];
        foreach ($updatedTasks as $updatedTask) {

            $oldName = $updatedTask['task_name'];
            $newName = $updatedTask['temp_name'];
            if ($updatedTask['action'] == CalendarFairTempTasks::ACTION_DELETE) {
                $newName = '';
            }

            $oldDates = CalendarFairTempTasks::calculateDates($updatedTask['task_date'], $updatedTask['task_duration']);
            $newDates = CalendarFairTempTasks::calculateDates($updatedTask['temp_date'], $updatedTask['temp_duration']);
            if ($updatedTask['action'] == CalendarFairTempTasks::ACTION_INSERT) {
                $oldDates = [];
            }
            if ($updatedTask['action'] == CalendarFairTempTasks::ACTION_DELETE) {
                $newDates = [];
            }

            if ($oldName != $newName || $oldDates != $newDates) {
                $userId = $updatedTask['user_id'];
                if (!isset($results[$userId])) {
                    $results[$userId]['user'] = [
                        'id' => $updatedTask['user_id'],
                        'name' => $updatedTask['username'],
                        'email' => $updatedTask['email']
                    ];
                }

                $fairId = $updatedTask['fair_id'];
                if (!isset($results[$userId]['fairs'][$fairId])) {
                    $results[$userId]['fairs'][$fairId] = [
                        'id' => $updatedTask['fair_id'],
                        'name' => $updatedTask['fair'] . ' - ' . date('Y')
                    ];
                }

                $results[$userId]['fairs'][$fairId]['tasks'][] = [
                    'action'   => $updatedTask['action'],
                    'oldName'  => $oldName,
                    'newName'  => $newName,
                    'oldDates' => $oldDates,
                    'newDates' => $newDates
                ];
            }
        }

        foreach ($results as $result) {
            $user['email'] = 'andrey.diveev@orgplan.pro'; //$result['user']['email'];
            $text = 'Организатор обновил задачи';
            $message = 'send updated tasks for userId: ';
            $this->sendEmail($user['email'], [
                'username' => $result['user']['name'],
                'fairs' => $result['fairs'],
                'text' => $text . ':'
            ], $text, null, null, null, 'calendar.updatedTasks');
            print_r([date('Y-m-d H:i:s') . ' ' . $message . $result['user']['id']]);
            sleep(5);
        }
    }

    public function send($email, $content, $subject = null, $files = [], $view = '')
    {
        $message = new YiiMailMessage;
        $controller = new CController('YiiMail');
        Yii::app()->mail->viewPath = 'application.modules.mail.views';
        $viewPath = Yii::getPathOfAlias(Yii::app()->mail->viewPath.'.'.$view).'.php';

        $message->view = $this->layout;
        $message->addTo($email);
        $message->setFrom(['info@protoplan.pro' => 'Protoplan']);
        $message->setSubject($subject);
        $message->setBody([
            'controller' => $controller,
            'content' => $content,
            'viewPath' => $viewPath,
            'view' => $view,
            'mailViewPath' => Yii::app()->mail->viewPath,
        ], 'text/html');

        foreach ($files as $file) {
            $swiftAttachment = Swift_Attachment::newInstance($file['data'], $file['name'], NULL);
            $message->attach($swiftAttachment);
        }

        $sentMessage = new SentMessage();

        $sentMessage->viewLayout = $this->layout;
        $sentMessage->from = 'info@protoplan.pro';
        $sentMessage->to = $email;
        $sentMessage->subject = $subject;
//        $sentMessage->content = $content;
        $sentMessage->viewPath = $viewPath;
        $sentMessage->view = $view;
        $sentMessage->mailViewPath = Yii::app()->mail->viewPath;

        $sentMessage->raw = $message->getBody();
        $sentMessage->messageId = $message->getId();

        $sentMessage->save();

        /** @var YiiMail $mailer */
        $mailer = Yii::app()->mail;
        return $mailer->send($message);
    }
}