<?php
/**
 * @var $content
 * @var $controller CController
 * @var $mailViewPath
 */
echo $controller->renderInternal(Yii::getPathOfAlias($mailViewPath.'.'.'_second_logotype').'.php', true);
?>
<strong>Здравствуйте<?= $content['username'] ? ', ' . $content['username'] : '' ?>!</strong>
<br>
<?php $fairId = false; ?>
<?php $title = false; ?>

<?php foreach ($content['fairs'] as $f_id => $fair): ?>

    <?php if (!$fairId || $fairId != $f_id): ?>
        <p style="font-size: 15px; text-transform: uppercase; background-color: #f08c00; color: #fff; padding: 19px; border-radius: 4px; font-weight: 600; line-height: 12px; margin: 54px 0 0;">
            <?= $fair['name'] ?>
        </p>
        <?php $fairId = $f_id; ?>
    <?php endif; ?>

    <?php $tasks = $fair['tasks']; ?>

    <?php for ($i = 0; $i <= 3; $i++): ?>

        <?php $title = false; ?>

        <?php foreach ($tasks as $t_id => $task): ?>

            <?php if ($i > 3 || date('Y-m-d', strtotime('+ ' .$i. ' days')) == date('Y-m-d', strtotime($task['endDate']))): ?>

                <?php if (!$title): ?>
                    <strong><?php
                        switch ($i) {
                            case 0:
                                echo "Задачи, которые завершаются сегодня:";
                                break;
                            case 1:
                                echo "Задачи, которые завершаются завтра:";
                                break;
                            case 2:
                                echo "Задачи, которые завершаются послезавтра:";
                                break;
                            default:
                                echo "Задачи на сегодня:";
                        }
                        ?></strong>
                    <?php $title = true; ?>
                <?php endif; ?>

                <table style="font-size: 13px; width: 100%; border-bottom: 1px solid #d2d2d2; margin-top: 30px;">
                    <tr>
                        <th style="text-transform: uppercase; padding-bottom: 10px; text-align: left; width: 30%;">Задача</th>
                        <th style="text-transform: uppercase; padding-bottom: 10px; text-align: left;<?= (date('Y-m-d') == date('Y-m-d', strtotime($task['endDate']))) ? ' color: red;' : ''; ?>">
                            <?= $task['task'] ?>
                        </th>
                    </tr>
                    <tr>
                        <td>Дата старта</td>
                        <td><?= $task['startDate'] ?></td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 10px;">Дата окончания</td>
                        <td style="padding-bottom: 10px;"><?= $task['endDate'] ?></td>
                    </tr>
                </table>
                <?php unset($tasks[$t_id]); ?>

            <?php endif; ?>

        <?php endforeach; ?>

    <?php endfor; ?>

<?php endforeach; ?>