<?php
/**
 * @var $content
 * @var $controller CController
 * @var $mailViewPath
 */
echo $controller->renderInternal(Yii::getPathOfAlias($mailViewPath.'.'.'_second_logotype').'.php', true);
?>
    <strong>Здравствуйте<?= $content['username'] ? ', ' . $content['username'] : '' ?>!</strong>
    <br>
    <p><?= $content['text'] ?></p>
<?php $fairId = false; ?>

<?php foreach ($content['fairs'] as $f_id => $fair): ?>

    <?php if (!$fairId || $fairId != $f_id): ?>
        <p style="font-size: 15px; text-transform: uppercase; background-color: #f08c00; color: #fff; padding: 19px; border-radius: 4px; font-weight: 600; line-height: 12px; margin: 54px 0 0;">
            <?= $fair['name'] ?>
        </p>
        <?php $fairId = $f_id; ?>
    <?php endif; ?>

    <?php $tasks = $fair['tasks']; ?>

    <?php foreach ($tasks as $t_id => $task): ?>
        <?php $color = ''; ?>
        <table style="font-size: 13px; width: 100%; border-bottom: 1px solid #d2d2d2; margin-top: 30px;">
            <tr>
                <th style="text-transform: uppercase; padding-bottom: 10px; text-align: left; width: 30%;">Задача <?php
                    switch ($task['action']) {
                        case CalendarFairTempTasks::ACTION_INSERT:
                            $color = 'green';
                            echo "добавлена";
                            break;
                        case CalendarFairTempTasks::ACTION_UPDATE:
                            echo "изменена";
                            break;
                        case CalendarFairTempTasks::ACTION_DELETE:
                            $color = 'red';
                            echo "удалена";
                            break;
                    }
                    ?></th>
                <th style="text-transform: uppercase; padding-bottom: 10px; text-align: left;<?= !empty($color) ? ' color: ' . $color . ';' : '' ?>">
                <?php if (empty($task['oldName']) || empty($task['newName']) || $task['oldName'] != $task['newName']): ?>
                    <?= empty($task['oldName']) ? '' : '<s>' . $task['oldName'] . '</s>'; ?>
                    <?= (empty($task['oldName']) || empty($task['newName'])) ? '' : ' ⟶ '; ?>
                    <?= empty($task['newName']) ? '' : $task['newName']; ?>
                <?php else: ?>
                    <?= $task['oldName']; ?>
                <?php endif; ?>
                </th>
            </tr>
            <tr>
                <td>Дата старта</td>
                <td>
                <?php if (empty($task['oldDates'][0]) || empty($task['newDates'][0]) || $task['oldDates'][0] != $task['newDates'][0]): ?>
                    <?= empty($task['oldDates'][0]) ? '' : '<s>' . $task['oldDates'][0] . '</s>'; ?>
                    <?= (empty($task['oldDates'][0]) || empty($task['newDates'][0])) ? '' : ' ⟶ '; ?>
                    <?= empty($task['newDates'][0]) ? '' : $task['newDates'][0]; ?>
                <?php else: ?>
                    <?= $task['oldDates'][0]; ?>
                <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td style="padding-bottom: 10px;">Дата окончания</td>
                <td style="padding-bottom: 10px;">
                <?php if (empty($task['oldDates'][1]) || empty($task['newDates'][1]) || $task['oldDates'][1] != $task['newDates'][1]): ?>
                    <?= empty($task['oldDates'][1]) ? '' : '<s>' . $task['oldDates'][1] . '</s>'; ?>
                    <?= (empty($task['oldDates'][1]) || empty($task['newDates'][1])) ? '' : ' ⟶ '; ?>
                    <?= empty($task['newDates'][1]) ? '' : $task['newDates'][1]; ?>
                <?php else: ?>
                    <?= $task['oldDates'][1]; ?>
                <?php endif; ?>
                </td>
            </tr>
        </table>

    <?php endforeach; ?>

<?php endforeach; ?>