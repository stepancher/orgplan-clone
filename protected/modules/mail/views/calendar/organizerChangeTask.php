<?php
/**
 * @var $content
 * @var $controller CController
 * @var $mailViewPath
 */
echo $controller->renderInternal(Yii::getPathOfAlias($mailViewPath.'.'.'_second_logotype').'.php', true);
?>
<strong>Здравствуйте<?= $content['username'] ? ', ' . $content['username'] : '' ?>!</strong>
<br>
<?= $content['text'] ?>
<?php $fairId = 0; ?>
<?php foreach ($content['tasks'] as $task): ?>
    <?php if (!$fairId || $fairId != $task['fairId']): ?>
<p style="font-size: 15px; text-transform: uppercase; background-color: #f08c00; color: #fff; padding: 19px; border-radius: 4px; font-weight: 600; line-height: 12px; margin: 54px 0 0;">
    <?= $task['fair'] ?>
</p>
        <?php $fairId = $task['fairId']; ?>
    <?php endif; ?>

<table style="font-size: 13px; width: 100%; border-bottom: 1px solid #d2d2d2; margin-top: 30px;">
    <tr>
        <th style="text-transform: uppercase; padding-bottom: 10px; text-align: left; width: 30%;">Задача</th>
        <th style="text-transform: uppercase; padding-bottom: 10px; text-align: left;<?= (date('Y-m-d') == date('Y-m-d', strtotime($task['endDate']))) ? ' color: red;' : ''; ?>">
            <?= $task['task'] ?>
        </th>
    </tr>
    <tr>
        <td>Дата старта</td>
        <td><?= $task['startDate'] ?></td>
    </tr>
    <tr>
        <td style="padding-bottom: 10px;">Дата окончания</td>
        <td style="padding-bottom: 10px;"><?= $task['endDate'] ?></td>
    </tr>
</table>
<?php endforeach; ?>