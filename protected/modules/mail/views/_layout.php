<?php
/**
 * @var $content
 * @var $controller
 */
Yii::import('application.modules.admin.AdminModule');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset="utf-8" />
</head>
<body style="background-color: rgb(210, 210, 210); color: #505055;">
<table  style="font-family: arial,sans-serif; font-size: 13px;line-height: 20px; margin: 0 auto;">
    <tbody>
    <tr>
        <td width="20px"></td>
        <td style="padding: 20px 0;">
            <div style="display: block; float: left;">
                <div style="width: 520px; background: #fff; border-top-left-radius: 12px; border-top-right-radius: 12px; padding: 20px 50px 60px 50px; height: 100%;">
                    <a href="https://protoplan.pro/">
                        <img src="https://protoplan.pro/static/protoplan-block-logo.png" alt="" style="width: 170px; height: 30px;">
                    </a>
                    <div style="float: right;font-size: 16px;margin-top: 9px;text-transform: uppercase;margin-right: 2px;"><?= Yii::t('AdminModule.admin', 'description') ?></div>
                    <div style="margin-top: 15px;">
                        <?php //@TODO refactor logo sending!!!?>
                        <?php if(isset($logo) && !empty($logo)):?>
                            <hr style="border-top: 0px; margin-bottom: 30px;">
                            <div style="width: 100%; float: left; margin-bottom: 68px;">
                                <img src="<?=$logo?>" alt="" style="" />
                            </div>
                        <?php endif;?>
                        <?php if (is_array($content)) {
                            echo $controller->renderInternal($viewPath, [
                                'content' => $content,
                                'mailViewPath' => $mailViewPath,
                                'controller' => $controller,
                            ], true);
                        } else {
                            echo $content;
                        }
                        ?>

                    </div>
                </div>

                <div style="width: 520px; float: left; background: #F0F0F0;padding: 60px 50px;  border-bottom-left-radius: 12px;border-bottom-right-radius: 12px;">
                    <div style="font-size: 12px;">
                        <a style="padding: 0px 14px 0 0;text-decoration:none; color: #000;" href="https://protoplan.pro/"><?= Yii::t('AdminModule.admin', 'go-to-site') ?></a> |
                        <a style="padding: 0 14px;      text-decoration:none; color: #000;" href="https://protoplan.pro/help/confidentiality/"><?= Yii::t('AdminModule.admin', 'confidentiality') ?></a> |
                        <a style="padding: 0 0 0 14px;  text-decoration:none; color: #000;" href="mailto:info@protoplan.pro"><?= Yii::t('AdminModule.admin', 'connect') ?></a>
                    </div>

                    <div style="display: block; float: left; width: 100%; margin-top: 31px;">
                        <table style="float: left; margin-top: 31px;">
                            <tbody>
                            <tr>
                                <td style="display: block; float: left; width: 24px; height: 24px; font-family: 'retinaicon-font'; font-size: 17px;">
                                    <img src="https://protoplan.pro/static/mail/phone.png" alt="">
                                </td>
                                <td style="display: block; float: left; height: 24px;">
                                    <?= Yii::t('MailModule.mail', 'phone_rus');?>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: block; float: left; width: 24px; height: 24px; font-family: 'retinaicon-font'; font-size: 17px;">
                                    <img src="https://protoplan.pro/static/mail/phone.png" alt="">
                                </td>
                                <td style="display: block; float: left; height: 24px;">
                                    <?= Yii::t('MailModule.mail', 'phone_rus_ru');?>
                                </td>
                            </tr>
                            <tr>
                                <td style="display: block; float: left; width: 24px; height: 24px; font-family: 'retinaicon-font'; font-size: 17px;">
                                    <img src="https://protoplan.pro/static/mail/phone.png" alt="">
                                </td>
                                <td style="display: block; float: left; height: 24px;">
                                    <?= Yii::t('MailModule.mail', 'phone_de');?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div style="display: block; float: left; width: 100%;">
                        <div style="display: block; float: left; width: 24px; height: 24px; font-family: 'retinaicon-font'; font-size: 17px;">
                            <img src="https://protoplan.pro/static/mail/mail.png" alt="">
                        </div>
                        <div style="display: block; float: left; height: 24px;">info@protoplan.pro</div>
                    </div>

                    <div style="display: block; float: left; width: 100%; margin-top: 20px;">
                        <a href="https://www.instagram.com/protoplan.pro/"><img src="https://protoplan.pro/static/mail/icon_social_instagram.png" alt=""></a>
                        <a href="https://vk.com/protoplan"><img src="https://protoplan.pro/static/mail/icon_social_vkontakte.png" alt=""></a>
                        <a href="https://www.facebook.com/protoplan.pro/"><img src="https://protoplan.pro/static/mail/icon_social_facebook.png" alt=""></a>
                        <a href="https://twitter.com/InfoProtoplan"><img src="https://protoplan.pro/static/mail/icon_social_twitter.png" alt=""></a>
                        <a href="https://www.youtube.com/channel/UCwkqCKo7lnipR6tM36701Dg"><img src="https://protoplan.pro/static/mail/icon_social_youtube.png" alt=""></a>
                        <?php /*
                    <a style="color: #000; text-decoration:none; border: 1px solid #D2D2D2;border-radius: 3px;float: left;padding: 1px 34px;display:inline-block;margin-right: 12px;" href="<?= $url ?>"><?= Yii::t('AdminModule.admin', 'web-version') ?></a>
                    <a style="color: #000;text-decoration:none; border: 1px solid #D2D2D2;border-radius: 3px;float: left;padding: 1px 34px;display:inline-block" href="<?= $url ?>"><?= Yii::t('AdminModule.admin', 'unsubscribe') ?></a>
                    */?>
                    </div>
                </div>
            </div>
        </td>
        <td width="20px"></td>
    </tr>
    </tbody>
</table>
</body>
</html>
