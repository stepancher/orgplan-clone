<?php

class JsonController extends Controller
{
    public function actionDeleteMyFair(){

        $json = array(
            'success' => FALSE,
            'message' => 'myfair id didn\'t find in post',
        );
        echo (isset($_POST['callback'])?$_POST['callback']:'') . ' (' . json_encode($json) . ');';
        exit;
        if(!isset($_POST['id']) && empty(json_decode(file_get_contents('php://input'), FILE_USE_INCLUDE_PATH))){
            $json = array(
                'success' => FALSE,
                'message' => 'post didn\'t find',
            );
            echo (isset($_POST['callback'])?$_POST['callback']:'') . ' (' . json_encode($json) . ');';
            return;
        }

        if(isset($_POST['id'])){
            $id = $_POST['id'];
        }

        if(!empty(json_decode(file_get_contents('php://input'), FILE_USE_INCLUDE_PATH))){
            $post = json_decode(file_get_contents('php://input'), FILE_USE_INCLUDE_PATH);

            if(isset($post['id'])){
                $id = $post['id'];
            } else {
                $json = array(
                    'success' => FALSE,
                    'message' => 'myfair id didn\'t find in post',
                );
                echo (isset($_POST['callback'])?$_POST['callback']:'') . ' (' . json_encode($json) . ');';
                return;
            }
        }

        $fair = MyFair::model()->findByAttributes(['fairId' => $id, 'userId' => Yii::app()->user->id]);

        if(empty($fair)){
            $json = array(
                'success' => FALSE,
                'message' => 'fair didn\'t find',
            );
            echo (isset($_POST['callback'])?$_POST['callback']:'') . ' (' . json_encode($json) . ');';
            return;
        }

        $transaction = Yii::app()->db->beginTransaction();

        try{

            $fair->delete();
            $transaction->commit();

            $json = array(
                'success' => 'ok',
                'message' => 'fair deleted',
            );
            echo (isset($_POST['callback'])?$_POST['callback']:'') . ' (' . json_encode($json) . ');';

        } catch (CException $e){

            $transaction->rollback();
            
            $json = array(
                'success' => FALSE,
                'message' => 'fair didn\'t deleted',
            );
            echo (isset($_POST['callback'])?$_POST['callback']:'') . ' (' . json_encode($json) . ');';
        }
    }

    public function actionGetMyFairs()
    {
        if(Yii::app()->user->isGuest){

            echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode(array('userId' => NULL)) . ');';
        }else{

            $data = $this->getMyFairsData();

            $json = array(
                "header" => Yii::t('WorkspaceModule.workspace', 'My fairs'),
                "models" => array_map(
                    function ($data){
                        $office = Yii::app()->user->role == User::ROLE_ORGANIZERS ? 'backoffice' : 'workspace';
                        $panelPath = $office . '/panel/panel/';

                        $url = Yii::app()->createUrl("fair/html/view", ["urlFairName" => $data['fairShortUrl'], 'langId' => Yii::app()->language]);
                        if(!empty($data['version']) && $data['version'] != ''){
                            $url = $data['version'].Yii::app()->createUrl($panelPath, array('fairId' => $data['id']));
                        }

                        return array(
                            "id" => $data['id'],
                            "name" => $data['name'],
                            "date" => Fair::getDateRange($data['beginDate'], $data['endDate']),
                            "city" => $data['cityName'] . ' / ' . $data['regionName'],
                            "cityPath" => Yii::app()->createUrl('regionInformation/default/viewRegion', array('urlRegionName' => $data['regionUrl'])),
                            "panelPath" => $url,
                            "rating" => $data['rating'],
                        );
                    },
                    $data
                ),
            );

            echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
        }

    }

    public function getMyFairsData(){

        $sql = "SELECT  fairId AS id, 
                        trf.name AS name, 
                        trf.shortUrl AS fairShortUrl, 
                        f.beginDate, 
                        f.endDate, 
                        f.proposalVersion version,
                        trc.name AS cityName,
                        trr.name AS regionName,
                        trr.shortUrl AS regionUrl,
                        f.rating
                FROM tbl_myfair mf
                    LEFT JOIN tbl_fair f ON mf.fairId = f.id
                    LEFT JOIN tbl_trfair trf ON trf.trParentId = f.id AND trf.langId = :langId 
                    LEFT JOIN tbl_exhibitioncomplex ec ON f.exhibitionComplexId = ec.id
                    LEFT JOIN tbl_city c ON ec.cityId = c.id
                    LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id AND trc.langId = :langId
                    LEFT JOIN tbl_trregion trr ON trr.trParentId = c.regionId AND trr.langId = :langId
                WHERE mf.`userId` = :userId AND mf.`status` = :status";

        $data = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(
            ':userId' => Yii::app()->user->id,
            ':status' => MyFair::STATUS_ACTIVE,
            ':langId' => Yii::app()->getLanguage(),
        ));

        return $data;
    }
}