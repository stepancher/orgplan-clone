<?php

/**
 * Class Notification
 */
class Notification extends \AR
{
    public static $_description = NULL;

    const TYPE_NOT_SELECTED_FINALIST = 1;
    const TYPE_DEVELOPER_SELECTED_TO_FINALIST = 2;
    const TYPE_DEVELOPER_TAKE_VICTORY = 3;
    const TYPE_REGIONAL_AUCTION_CREATED = 4;
    const TYPE_NEW_BID = 5;
    const TYPE_NOT_SELECTED_WINNER = 6;
    const TYPE_CHANGED_TO_END = 7;
    const TYPE_CHANGED_TO_REBIDDING = 8;
    const TYPE_ADMINISTRATION_REQUEST_PRISE_STAND = 10;
    const TYPE_DAILY_EVENT_NOTIFICATION = 11;
    const TYPE_DAY_TASK = 12;
    const TYPE_NEW_FAIR_ORGANIZER_CHOSE_FAIR = 14;
    const TYPE_NEW_FAIR_ORGANIZER = 15;
    const TYPE_NEW_EXHIBITION_COMPLEX_ORGANIZER = 16;
    const TYPE_NEW_EXHIBITION_COMPLEX_ORGANIZER_CHOSE_EC = 17;
    const TYPE_NEW_FAIR = 18;
    const TYPE_NEW_EXHIBITION_COMPLEX = 19;
    const TYPE_FAIR_MODERATION = 20;
    const TYPE_SUCCESS_MODERATION = 21;
    const TYPE_DENIED_MODERATION = 22;
    const TYPE_EXHIBITION_MODERATION = 23;
    const TYPE_SUCCESS_EXC_MODERATION = 24;
    const TYPE_DENIED_EXC_MODERATION = 25;
    const TYPE_TASK = 26;
    const TYPE_FAIR_END_DATE = 27;
    const TYPE_FAIR_CHECK_END_DATE = 28;
    const TYPE_ACTIVATED = 29;
    const TYPE_FAIR_ACTIVATED = 30;
    const TYPE_FAIR_ACTIVATED_TRUE = 31;
    const TYPE_FAIR_ACTIVATED_FALSE = 32;
    const TYPE_EXHIBITION_COMPLEX_ACTIVATED = 33;
    const TYPE_EXHIBITION_COMPLEX_ACTIVATED_TRUE = 34;
    const TYPE_EXHIBITION_COMPLEX_ACTIVATED_FALSE = 35;
    const TYPE_SPY_DB_OPERATION = 36;
    const TYPE_RECOVER_PASSWORD = 37;
    const TYPE_BEFORE_EVENT = 38;
    const TYPE_ORDER_PRICE = 39;
    const TYPE_EXPONENT_REGISTRATION = 40;
    const TYPE_PROPOSAL_SEND_ORGANIZER = 41;
    const TYPE_PROPOSAL_SEND_EXPONENT = 42;
    const TYPE_PROPOSAL_ACCEPTED_EXPONENT = 43;
    const TYPE_PROPOSAL_DELETED_EXPONENT = 44;
    const TYPE_PROPOSAL_REJECTED_EXPONENT = 45;


    public static function types()
    {
        return array(
            self::TYPE_NOT_SELECTED_FINALIST => Yii::t('notification', "The tender {auction}, you need to select the finalists, the term of the winners choice {date}"),
            self::TYPE_NOT_SELECTED_WINNER => Yii::t('notification', '{date} The tender {auction}, you must select the winner'),
            self::TYPE_DEVELOPER_SELECTED_TO_FINALIST => Yii::t('notification', 'You selected a finalist in the tender - {auction}'),
            self::TYPE_DEVELOPER_TAKE_VICTORY => Yii::t('notification', 'Congratulations, you have chosen the winner of the tender - {auction}'),
            self::TYPE_REGIONAL_AUCTION_CREATED => Yii::t('notification', 'A new tender - {auction}'),
            self::TYPE_NEW_BID => Yii::t('notification', 'There is a new proposal from the builder {login}, tender {auction}'),
            self::TYPE_CHANGED_TO_END => Yii::t('notification', 'Tender {auction} Change status to {status}'),
            self::TYPE_CHANGED_TO_REBIDDING => Yii::t('notification', 'Customer Retendering announced a tender {auction}'),
            self::TYPE_ADMINISTRATION_REQUEST_PRISE_STAND => Yii::t('notification', 'User {login} Quote stand {name}, c area {square}, the city {city}, the fair {fair}'),
            self::TYPE_DAILY_EVENT_NOTIFICATION => Yii::t('notification', 'Event {name} will occur in {date}'),
            self::TYPE_DAY_TASK => Yii::t('notification', 'You must perform the following tasks {name} end date by {date}'),
            self::TYPE_NEW_FAIR_ORGANIZER_CHOSE_FAIR => Yii::t('notification', 'Register a new organizer of exhibitions {login} c show {name}'),
            self::TYPE_NEW_FAIR_ORGANIZER => Yii::t('notification', 'Register a new organizer of exhibitions {login}'),
            self::TYPE_NEW_EXHIBITION_COMPLEX_ORGANIZER => Yii::t('notification', 'Register a new organizer fairgrounds {login}'),
            self::TYPE_NEW_EXHIBITION_COMPLEX_ORGANIZER_CHOSE_EC => Yii::t('notification', 'Register a new organizer fairgrounds {login} c fairgrounds {name}'),
            self::TYPE_NEW_FAIR => Yii::t('notification', 'Organizer {login} has created an exhibition {name}'),
            self::TYPE_NEW_EXHIBITION_COMPLEX => Yii::t('notification', 'The organizer of the exhibition complex {login} created exhibition complex {name}'),
            self::TYPE_FAIR_MODERATION => Yii::t('notification', 'Your exhibition {name} is on moderation'),
            self::TYPE_SUCCESS_MODERATION => Yii::t('notification', 'Your exhibition {name} has passed moderation and received the status of "Approved"'),
            self::TYPE_SUCCESS_EXC_MODERATION => Yii::t('notification', 'Your exhibition complex {name} was moderated and received the status of "Approved"'),
            self::TYPE_DENIED_MODERATION => Yii::t('notification', 'Your exhibition {name} has passed moderation and received the status of "forbidden"'),
            self::TYPE_DENIED_EXC_MODERATION => Yii::t('notification', 'Your exhibition complex {name} was moderated and received the status of "forbidden"'),
            self::TYPE_EXHIBITION_MODERATION => Yii::t('notification', 'Your exhibition complex {name} is on moderation'),
            self::TYPE_FAIR_END_DATE => Yii::t('notification', 'End date of the exhibition {name}, {date}'),
            self::TYPE_TASK => Yii::t('notification', 'You have chosen a responsible executive tasks {name} <br> Description {description} <br> duration of the task {duration} <br> Start Date: {beginDate}
                <br> Date oknchaniya: {endDate}'),
            self::TYPE_ACTIVATED => Yii::t('notification', 'Registration {firstName} {link}'),
            self::TYPE_FAIR_ACTIVATED => Yii::t('notification', '{Login} Organizer edited exhibition {name} and expects confirmation'),
            self::TYPE_EXHIBITION_COMPLEX_ACTIVATED => Yii::t('notification', '{Login} Organizer edited exhibition complex {name} and waits for confirmation'),
            self::TYPE_FAIR_ACTIVATED_TRUE => Yii::t('notification', 'Approved by the Administrator of the exhibition editing {name}'),
            self::TYPE_EXHIBITION_COMPLEX_ACTIVATED_TRUE => Yii::t('notification', 'Approved by the Administrator to edit the fairgrounds {name}'),
            self::TYPE_FAIR_ACTIVATED_FALSE => Yii::t('notification', 'The administrator has rejected editing exhibition {name}'),
            self::TYPE_EXHIBITION_COMPLEX_ACTIVATED_FALSE => Yii::t('notification', 'The administrator has rejected editing fairgrounds {name}'),
            self::TYPE_FAIR_CHECK_END_DATE => Yii::t('notification', 'The end date of your exhibition {name} has already passed, update data about the exhibition.'),
            self::TYPE_RECOVER_PASSWORD => Yii::t('notification', '{text} {link}'),
            self::TYPE_ORDER_PRICE => Yii::t('notification', 'Requesting price from user: {login} in city: {city} and category: {productCategory} on product: {productName}'),
            self::TYPE_EXPONENT_REGISTRATION => Yii::t('notification', '{email} added as exhibitor of the {fairName} fair'),
            self::TYPE_PROPOSAL_SEND_ORGANIZER => Yii::t('notification', 'Proposal was sent to the organizer'),//'Заявка отправлена организатору',
            self::TYPE_PROPOSAL_SEND_EXPONENT => Yii::t('notification', 'Proposal was send to the exponent'),//'Заявка отправлена экспоненту',
            self::TYPE_PROPOSAL_ACCEPTED_EXPONENT => Yii::t('notification', 'Congratulations! Your proposal {label} {pluralLabel} was accepted by organizer'),//'Поздравляем! Ваша заявка {label} {pluralLabel} одобрена Организатором.',
            self::TYPE_PROPOSAL_DELETED_EXPONENT => Yii::t('notification', 'Your proposal {label} {pluralLabel} was deleted by organizer. <br/><br/>Reason: <b><i>{comment}</i></b>.'),
            self::TYPE_PROPOSAL_REJECTED_EXPONENT => Yii::t('notification', 'Your proposal {label} {pluralLabel} was rejected by organizer. <br/><br/>Reason: <b><i>{comment}</i></b>.'),
        );
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'recipientId' => array(
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'recipientId' => 'id',
                    ),
                ),
                'label' => Yii::t('notification', 'Sender'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'content' => array(
                'string',
                'label' => Yii::t('notification', 'Text'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'data' => array(
                'label' => Yii::t('notification', 'Text'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'senderId' => array(
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'senderId' => 'id',
                    ),
                ),
                'label' => Yii::t('notification', 'User'),
            ),
            'type' => array(
                'integer',
                'label' => Yii::t('notification', 'Type'),
            ),
            'exhibitionComplexHasNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'ExhibitionComplexHasNotification',
                    array(
                        'notificationId' => 'id',
                    ),
                ),
            ),
            'standHasNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'StandHasNotification',
                    array(
                        'notificationId' => 'id',
                    ),
                ),
            ),
            'trNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrNotification',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'auctionHasNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'AuctionHasNotification',
                    array(
                        'notificationId' => 'id',
                    ),
                ),
            ),
            'calendarHasNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'CalendarHasNotification',
                    array(
                        'notificationId' => 'id',
                    ),
                ),
            ),
        );
    }

    /**
     * @param null $criteria
     * @param array $ignoreCompareFields
     * @return CActiveDataProvider
     * Установка сортировки
     */
    public function search($criteria = null, $ignoreCompareFields = array())
    {
        $dataProvider = parent::search($criteria, $ignoreCompareFields);
        $dataProvider->setSort(array(
            'defaultOrder' => 't.id DESC',
            'attributes' => array(
                '*',
            ),
        ));

        return $dataProvider;
    }

    /**
     * @param $recipientId
     * @param $object
     * @param $type
     * @param array $content
     * @param null $senderId
     * @param bool $hasSend
     * @param array $data
     * @return bool|Notification
     */
    static function saveByObject($recipientId, $object, $type, $content = array(), $senderId = null, $hasSend = true, $data = [])
    {
        $notification = new self();
        $notification->type = $type;
        $notification->recipientId = $recipientId;
        $notification->senderId = $senderId;
        $notification->content = is_array($content)
            ? strtr(Notification::types()[$type], $content)
            : $content;

        if (!empty($data)) {
            $notification->data = json_encode($data);
            $notification->content = NULL;
        }

        if ($hasSend && $notification->hasSend($object)) {
            return false;
        }
        $result = $notification->save(false);
        if ($result) {
            $clsName = get_class($object);
            $relationCls = $clsName . 'HasNotification';
            $cls = new $relationCls;
            $cls->{lcfirst($clsName) . 'Id'} = $object->id;
            $cls->notificationId = $notification->id;
            if ($cls->save(false)) {
                return $notification;
            }
        }
        return false;
    }

    public function hasSend($object)
    {
        $clsName = get_class($object);
        $objectId = lcfirst($clsName) . 'Id';
        $relModel = AR::model($clsName . 'HasNotification');
        $conn = $relModel->getDbConnection();

        $senderValue = null !== $this->senderId ? ' = ' . $conn->quoteValue($this->senderId) : 'IS NULL';

        return null !== $relModel->with('notification')->findByAttributes(
            array($objectId => $object->id),
            $conn->quoteColumnName('notification.senderId') . $senderValue . ' AND ' .
            $conn->quoteColumnName('notification.recipientId') . ' = ' . $conn->quoteValue($this->recipientId) . ' AND ' .
            $conn->quoteColumnName('notification.type') . ' = ' . $conn->quoteValue($this->type)
        );
    }

    /**
     * @param string $recipientId
     * @param string $objectClass
     * @param string $objectId
     * @param integer $type
     * @param array $content
     * @param string $senderId
     * @param bool $hasSend
     * @param string $data
     * @return bool|integer
     */
    static function saveByObjectApi($recipientId, $objectClass, $objectId, $type, $content = array(), $senderId = null, $hasSend = true, $data = '[]')
    {
        $notification = new self();
        $notification->type = $type;
        $notification->recipientId = $recipientId;
        $notification->senderId = $senderId;
        $notification->content = is_array($content)
            ? strtr(Notification::types()[$type], $content)
            : $content;

        if (!empty($data)) {
            $notification->data = $data;
            $notification->content = NULL;
        }

        if ($hasSend && $notification->hasSendApi($objectClass, $objectId)) {
            return false;
        }

        $result = $notification->save(false);

        if ($result && !empty($notification)) {
            return $notification->id;
        }

        return false;
    }


    /**
     * @param $objectClass
     * @param $objectId
     * @return bool
     */
    public function hasSendApi($objectClass, $objectId)
    {
        $clsName = $objectClass;
        $objectId = lcfirst($clsName) . 'Id';
        $relModel = AR::model($clsName . 'HasNotification');
        $conn = $relModel->getDbConnection();

        $senderValue = null !== $this->senderId ? ' = ' . $conn->quoteValue($this->senderId) : 'IS NULL';

        return null !== $relModel->with('notification')->findByAttributes(
            array($objectId => $objectId),
            $conn->quoteColumnName('notification.senderId') . $senderValue . ' AND ' .
            $conn->quoteColumnName('notification.recipientId') . ' = ' . $conn->quoteValue($this->recipientId) . ' AND ' .
            $conn->quoteColumnName('notification.type') . ' = ' . $conn->quoteValue($this->type)
        );
    }
}