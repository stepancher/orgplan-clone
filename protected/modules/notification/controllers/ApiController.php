<?php

class ApiController extends Controller {

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function actionAuth($SystemId, $UserName, $Password) {

        return true;
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        echo json_encode(true);
    }

    /**
     * @param integer $notificationId
     * @return bool
     * @soap
     */
    public function actionGetNotificationExists($notificationId){
        $result = FALSE;

        if(!empty($fair = Notification::model()->findByPk($notificationId))){
            $result = TRUE;
        }

        echo json_encode($result);
    }

    /**
     * @param integer $notificationId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetNotificationProperties($notificationId, array $properties){

        if(!empty($fair = Notification::model()->findByPk($notificationId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($fair->{$name}) &&
                    !empty($fair->{$name})
                ){
                    $res[$name] = $fair->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @return array
     * @soap
     */
    public function actionGetConstants(){
        $oClass = new ReflectionClass('Notification');
        echo json_encode($oClass->getConstants());
    }

    /**
     * @param string $recipientId
     * @param string $objectClass
     * @param string $objectId
     * @param integer $type
     * @param array $content
     * @param string $senderId
     * @param bool $hasSend
     * @param string $data
     * @return integer|bool
     * @soap
     */
    public function actionSaveByObject($recipientId = NULL, $objectClass, $objectId, $type, array $content, $senderId = NULL, $hasSend, $data = NULL){

        echo json_encode(Notification::saveByObjectApi($recipientId, $objectClass, $objectId, $type, $content, $senderId, $hasSend, $data));
    }

    /**
     * @param string $commandString
     * @param bool $background
     * @return string
     * @soap
     */
    public function actionConsole($commandString, $background = FALSE){
        echo json_encode(Yii::app()->consoleRunner->run(
            $commandString,
            $background
        ));
    }
}