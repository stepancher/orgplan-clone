<?php

class NotificationModule extends CWebModule {

	public function init() {

		$this->setImport(array(
			'notification.models.*',
		));
	}
}
