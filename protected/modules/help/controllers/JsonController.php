<?php

class JsonController extends Controller
{
    public function actionGetTermsOfUse()
    {
        $termsOfUseHtml = $this->renderPartial('/_blocks_' . Yii::app()->language . '/_termsOfUse', NULL, TRUE);

        $json = array(
            'title' => '',
            'description' => '',
            'header' => Yii::t('helpModule.help', 'Support'),
            'subheader' => Yii::t('helpModule.help', 'general questions'),
            'content' => $termsOfUseHtml,
        );

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . json_encode($json) . ');';
    }

    public function actionGetConfidentiality()
    {
        $confidentialityHtml = $this->renderPartial('/_blocks_' . Yii::app()->language . '/_confidentiality', NULL, TRUE);

        $json = array(
            'title' => '',
            'description' => '',
            'header' => Yii::t('helpModule.help', 'Support'),
            'subheader' => Yii::t('helpModule.help', 'Privacy'),
            'content' => $confidentialityHtml,
        );

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . json_encode($json) . ');';
    }

    public function actionGetImpressum()
    {
        $confidentialityHtml = $this->renderPartial('/_blocks_' . Yii::app()->language . '/_impressum', NULL, TRUE);

        $json = array(
            'title' => Yii::t('helpModule.help', 'Impressum'),
            'description' => '',
            'header' => Yii::t('helpModule.help', 'Impressum') . ':',
            'subheader' => null,
            'content' => $confidentialityHtml,
        );

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . json_encode($json) . ');';
    }
}