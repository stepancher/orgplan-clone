<?php

class DefaultController extends Controller
{
    /**
     * Страница помощи
     */
    public function actionIndex()
    {
        $this->render('helpPage');
    }

    public function actionConfidentiality(){

        $this->render('confidentiality');
    }
}