<?php
/**
 * @var Controller $this
 */
?>

<div class="cols-row"><?= $this->renderPartial('/_blocks/_b-header', [
        'header' => Yii::t('helpModule.help', 'Support'),
        'description' => Yii::t('helpModule.help', 'general questions'),
    ], true); ?>
</div>

<?= $this->renderPartial('/_blocks/_termsOfUse') ?>