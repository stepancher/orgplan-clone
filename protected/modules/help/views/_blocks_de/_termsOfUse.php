<style>
    .indent-t-3 {
        margin-top: 2em;
    }
</style>
<div>
    <div class="indent-t-1">
        <span class="p-h2 f-h2">AGB PROTOPLAN</span>
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">1. Geltungsbereich dieser Bedingungen</span>
    </div>

    <div class="indent-t-2">
        (1) Kubex GmbH (nachfolgend: PROTOPLAN) ist Betreiber der Messeplattform www.protoplan.pro (nachfolgend Plattform genannt) und bietet für Unternehmen (also Messeveranstalter, Aussteller und Anbieter von Messe-Dienstleistungen - gemeinsam nachfolgend als Nutzer benannt) die Nutzung der PROTOPLAN Plattform an. Darüber hinaus kann auf Antrag ein kostenfreier und zeitlich begrenzter Gast-Zugang zu der Plattform eingerichtet werden.
        <br><br>
        (2) Diese Allgemeinen Geschäftsbedingungen (nachfolgend benannt als „AGB“ oder „Nutzungsbedingungen“) gelten für alle Nutzer der Plattform mit dem ersten Zugriff auf die von der Plattform bereitgestellten Dienste. Diese AGB werden durch die Preisliste und Datenschutzhinweise sowie durch andere Nutzungsbedingungen auf der Plattform ergänzt.
        <br><br>
        (3) Die Gültigkeit Allgemeiner Geschäftsbedingungen der Nutzer ist ausdrücklich ausgeschlossen, soweit sie mit diesen Allgemeinen Geschäftsbedingungen nicht übereinstimmen. Bei Aufträgen für Werbeschaltungen, die sich auf Online-Medien und andere Medien beziehen, gelten die Allgemeinen Geschäftsbedingungen „Werbung“. Individuelle Vereinbarungen bleiben hiervon unberührt.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">2. Leistungen von PROTOPLAN</span>
    </div>

    <div class="indent-t-2">
        (1) Die Plattform ermöglicht es den Nutzern, sich zu präsentieren, Kooperationspartner zu finden, Dokumente und Informationen auszutauschen, Aufträge zu vergeben und das gesamte Messegeschehen des Unternehmens auf einer einzigen Plattform zu koordinieren.
        <br><br>
        (2) Zu den auf der Plattform angebotenen Leistungen gehören insbesondere:
        Die Nutzungsmöglichkeit der Plattform nach Bereitstellung der Zugangsmöglichkeit
        Ein zeitlich begrenzter, kostenfreier Demozugang, um die Plattform und deren Funktionsfähigkeit zu testen;
        Das Einrichtung der Online-Formulare und Berichte für den persönlichen Nutzerbereich
        Beratung zur Nutzung der Plattform und Technischer Support
        <br><br>
        Veröffentlichung von Unternehmensprofilen, Informationsmaterialien und Werbung im persönlichen Nutzerbereich;
        Bereithaltung eines Unternehmensverzeichnisses für das Suchen und Finden von Geschäfts- und Kooperationspartnern;
        Bereithaltung einer individualisierten Benutzeroberfläche zur Bedienung und Nutzung der Plattform und zum Austausch messerelevanter Daten zwischen den Nutzern;
        Nutzung eines umfangreichen Informations- und Vergleichsportals zu Messen ausgewählter Staaten und Gebietszusammenschlüsse.
        <br><br>
        (3)  Protoplan behält sich das Recht vor, nach eigenem Ermessen Leistungen Dritter in Anspruch zu nehmen, um seinen Vertragsverpflichtungen nachkommen zu können.
    </div>





    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">3. Registrierung</span>
    </div>

    <div class="indent-t-2">
        (1) Die vollständige Nutzung der Plattform ist erst nach Abschluss eines Nutzungsvertrages möglich. Die Registrierung als Nutzer ist nur Unternehmern und Unternehmen gestattet – also ausschließlich natürlichen oder juristischen Personen oder rechtsfähigen Personengesellschaften, die bei der Inanspruchnahme der auf der Plattform angebotenen Dienste in Ausübung ihrer gewerblichen oder selbständigen beruflichen Tätigkeit handeln. Die Registrierung als Nutzer gilt als Vertragsangebot des jeweiligen Unternehmers / Unternehmens. Ein Anspruch auf Zulassung zur Nutzung der Plattform besteht nicht.
        <br><br>
        (2) Der Vertrag kommt durch die im Regelfall per E-Mail versandte Annahmeerklärung von PROTOPLAN zustande.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">4. Kosten und Zahlung</span>
    </div>

    <div class="indent-t-2">
        (1) Die Nutzung der momentan auf der Plattform zur Verfügung gestellten Services ist bis auf Weiteres kostenfrei.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">5. Vertragslaufzeit und Kündigung</span>
    </div>

    <div class="indent-t-2">
        (1) Der Nutzungsvertrag beginnt mit der von PROTOPLAN erklärten Vertragsannahme und wird für die Dauer von einem Jahr geschlossen. Der Vertrag verlängert sich stillschweigend um ein weiteres Jahr, sofern er nicht von einer Vertragspartei mit einer Frist von 3 Monaten zum Ende der Laufzeit gekündigt wird. Das Recht beider Parteien zu einer außerordentlichen Kündigung (Kündigung aus wichtigem Grund) bleibt hiervon unberührt. Der Gastzugang endet automatisch nach vier Wochen.
        <br><br>
        (2) PROTOPLAN behält das Recht, die Zugriffsmöglichkeiten des Nutzers auf die Plattform ganz oder teilweise zu entziehen.
        <br><br>
        (3) Jede Kündigung muss entweder schriftlich (Brief, Fax) oder in Textform (E-Mail an: Emailadresse) erfolgen.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">6. Vertragsabwicklung zwischen Nutzern auf der Plattform</span>
    </div>
    <div class="indent-t-2">
        (1) Das über die Plattform abgewickelte Vertragsgeschehen zwischen Ausstellern und Veranstaltern und / oder Dienstleistern, d.h. die dort zwischen den Nutzern angebahnten oder abgeschlossenen Verträge sind alleinige Angelegenheit der jeweiligen Nutzer. PROTOPLAN übernimmt für diese Verträge weder eine Erfüllungsgarantie noch haftet PROTOPLAN für Sach- oder Rechtsmängel der gehandelten Waren und Leistungen. PROTOPLAN ist nicht verpflichtet, die Erfüllung der zwischen den Nutzern zustande gekommenen Verträge zu gewährleisten. Die Angaben der Nutzer stellen auch keine von PROTOPLAN zugesicherten Eigenschaften dar.
        <br><br>
        (2) PROTOPLAN übernimmt zudem keine Gewähr für die wahre Identität und die Verfügungsbefugnis der Nutzer. Die jeweiligen Vertragsparteien verpflichten sich, sich in geeigneter Weise über die wahre Identität die ggf. erforderliche Verfügungsbefugnis des jeweils anderen Teils zu informieren.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">7. Pflichten des Nutzers</span>
    </div>

    <div class="indent-t-2">
        (1)  Der Nutzer wird PROTOPLAN alle Informationen und Daten, welche zur Erbringung der vertraglichen Leistungen erforderlich sind, innerhalb von 5 (fünf) Arbeitstagen nach Unterzeichnung des Vertrages, in jedem Fall jedoch vor Beginn der Ausführung der Leistungen über die von der Plattform zur Verfügung gestellten Hilfsmittel oder per E-Mail übermitteln. Dies gilt auch für sämtliche später vereinbarten Änderungen der Leistungen oder Inhalte des persönlichen Nutzerbereich während der Vertragslaufzeit.
        <br><br>
        (2)  Im Fall einer verspäteten Übermittlung von Informationen hat PROTOPLAN das Recht, die Erbringung von Dienstleistungen nicht zu beginnen oder die vereinbarten Ausführungsfristen zu verlängern.
        <br><br>
        (3)  Der Nutzer verpflichtet sich, PROTOPLAN von Ansprüchen  Dritter freizustellen, sofern diese wegen der vom Nutzer übermittelten oder eigenständig eingestellten Daten erhoben werden. Der Nutzer haftet für die Echtheit der von ihm übermittelten Daten sowie deren Konformität mit gesetzlichen Vorgaben.
        <br><br>
        (4) Der Nutzer ist verpflichtet, rechtswidrige Handlungen und Missbrauch der Zugriffsmöglichkeiten auf die auf der Plattform bereitgehaltenen Internetdienste zu unterlassen oder durch geeignete Maßnahmen sicherzustellen, dass Erfüllungsgehilfen oder Mitarbeiter diese Verpflichtungen ebenfalls einhalten. Sofern nicht vereinbart, wird der Nutzer sein Nutzungsrecht und Rechte aus diesem Vertrag nicht verkaufen, ein Revers Engineering der Plattform oder ihrer Komponenten durchführen; identische oder ähnliche Dienste zur Nutzung vergleichbarer Plattformen schaffen oder den Zugang zu diesen vermitteln, die Plattform in jedweder Verbindung  mit verbotenen Inhalten nutzen.
        <br><br>
        (5) Der Nutzer verpflichtet sich, seine Unternehmensdaten fortwährend auf Richtigkeit zu überprüfen und gegebenenfalls zu aktualisieren. Der Nutzer hat seine Dateien auf Freiheit von Viren zu überprüfen, bevor er diese auf die Plattform stellt. Nutzer stellen PROTOPLAN von sämtlichen Ansprüchen Dritter hinsichtlich der vom Nutzer überlassenen Daten frei.
        <br><br>
        (6)  Nutzer sind gegenüber PROTOPLAN berechtigt und vor Einleitung eines gerichtlichen Verfahrens gegen PROTOPLAN verpflichtet, die Sperrung oder Entfernung von eingestellten Inhalten oder Daten zu verlangen, deren sachliche Richtigkeit zweifelhaft ist, die gegen gesetzliche oder behördliche Vorschriften oder gegen die guten Sitten verstoßen sowie den Nutzer in seinen eigenen Rechten verletzen.
        <br><br>
        (7) Verstößt der Nutzer gegen eine der vorstehenden Pflichten ist PROTOPLAN berechtigt, die entsprechenden Daten zu löschen bzw. die Zugriffsmöglichkeit auf die Plattform ganz oder teilweise zu entziehen. Das gilt auch für schwerwiegende Vertragsverletzungen des Nutzers.
        <br><br>
        (8) Wird PROTOPLAN aufgrund eines von einem Nutzer zu verantwortenden Verstoßes von Dritten oder einem anderen Nutzer in Anspruch genommen, verpflichtet sich der für den Verstoß verantwortliche Nutzer, PROTOPLAN von jeglichen Ansprüchen und Aufwendungen freizustellen, die PROTOPLAN wegen der Inanspruchnahme durch den Dritten entstehen. Die Geltendmachung darüber hinausgehender Schäden bleibt ausdrücklich vorbehalten.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">8. Pflichten von PROTOPLAN / Abnahme</span>
    </div>
    <div class="indent-t-2">
        (1) PROTOPLAN verpflichtet sich, auf Beschwerden von Nutzern über Verstöße und rechtswidrige Inhalte auf der Plattform zu reagieren und über geeignete Maßnahmen zu entscheiden.
        <br><br>
        (2)  PROTOPLAN behält sich vor, Inhalte und Dateien, deren sachliche Richtigkeit zweifelhaft sind, die gegen gesetzliche oder behördliche Vorschriften, gegen die Rechte Dritter, gegen die guten Sitten verstoßen oder von Viren befallen sind, nach Kenntniserlangung und je nach Schwere der im Raum stehenden Verletzung auch ohne vorherige Anhörung und Ankündigung zu sperren oder zu entfernen. Ansprüche wegen der Entfernung solcher Informationen oder Dateien sind ausgeschlossen.
        <br><br>
        (3) PROTOPLAN behält sich das Recht vor, Leistungen jederzeit einzustellen, einzuschränken, zu erweitern, zu ergänzen oder zu verbessern. Bei einer Reduktion des Leistungsumfanges hat der Nutzer ein außerordentliches Kündigungsrecht, sofern es sich nicht um eine kostenlos zu Verfügung gestellte Leistung handelt.
        <br><br>
        (4) Leistungen von PROTOPLAN im Zusammenhang mit der Einrichtung des persönlichen Nutzerbereichs wie die Einrichtung oder Anpassung der Online-Formulare und Berichte gelten als erbracht, sobald der Nutzer die Möglichkeit zur Nutzung sämtlicher vereinbarter Funktionen zur Verfügung gestellt bekommt und über die Möglichkeit der Nutzungsaufnahme durch PROTOPLAN per Email informiert wurde. Über die Ausführung der Leistungen erstellt PROTOPLAN ein Protokoll, welches dem Nutzer zur Prüfung übersandt wird. Sofern der Nutzer der Richtigkeit der Inhalte des Protokolls nicht innerhalb von 5 Werktagen widerspricht, gelten die Leistungen von PROTOPLAN als mangelfrei abgenommen.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">9. Verfügbarkeit der Plattform / Support</span>
    </div>
    <div class="indent-t-2">
        (1) Die Plattform und die über diese Plattform angebotenen Leistungen werden ohne jegliche Zusicherung in Bezug auf Verfügbarkeit bereitgestellt. Sofern kostenpflichtige Leistungen nicht genutzt werden können, erfolgt für den Fall, dass die Leistungen zu einem Umfang  von mehr als 5% nicht verfügbar sind eine Rückerstattung des Nutzungsentgelts in der entsprechenden Höhe.
        <br><br>
        (2) Davon ausgenommen sind planmäßige Wartungsarbeiten und eine Nichtverfügbarkeit, auf die PROTOPLAN keinen Einfluss hat – insbesondere bei einem Verschulden Dritter oder höherer Gewalt.
        <br><br>
        (3) Der technische Support wird zwischen 9:00 bis 18:00 Uhr MEZ gewährt. Anfragen des Nutzers für den technischen Support sind ausschließlich an die E-Mail-Adresse des Supportdienstes von PROTOPLAN contact@protoplan.pro zu richten. PROTOPLAN versucht Supportanfragen spätestens nach 4 Stunden zu bearbeiten.
        <br><br>
        (4) Der technische Support beinhaltet keine Beratungsleistungen zur Verwendung der Software oder zur Ausstattung des persönlichen Nutzerbereichs. Die Erbringung solcher Dienstleistungen erfolgt auf Grundlage gesonderter Dienstleistungsaufträge.
        <br><br>
        (5) Der Auftragnehmer kann andere Regeln zur Gewährung des technischen Supports festlegen, wenn er den Nutzer hiervon spätestens 14 Tage vorab per E-Mail darüber informiert und der Nutzer diesen Änderungen nicht widerspricht.
    </div>
    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">10. Gewährleistung und Haftung</span>
    </div>
    <div class="indent-t-2">
        (1)  Aufgrund der technischen Besonderheiten des Services übernimmt PROTOPLAN keine Gewähr für einen möglichen Verlust der Daten.
        <br><br>
        (2) Die Haftung von PROTOPLAN  für leicht fahrlässig verursachte Verletzungen oder von Verletzungen vertragswesentlicher Verpflichtungen ist auf den vorhersehbaren und vertragstypischen Schaden beschränkt. Diese Beschränkung gilt auch für Handlungen von Mitarbeitern oder Erfüllungsgehilfen, die für PROTOPLAN im Rahmen dieses Nutzungsvertrages tätig werden.
        <br><br>
        (3) Die Haftung für fremde Inhalte ist ausgeschlossen. Für Links mit Zugang zu anderen Websites und dort erreichbaren fremden Inhalte dieser Websites haftet PROTOPLAN nicht. PROTOPLAN macht sich die fremden Inhalte nicht zu Eigen. PROTOPLAN wird den Link zu solchen Inhalten unverzüglich beseitigen, sobald es Kenntnis von rechtswidrigen Inhalten solcher fremden Websites erlangt.
        <br><br>
        (4) PROTOPLAN haftet weder für den Inhalt der Daten oder die Virenfreiheit von Dateien, die durch Nutzer  auf der Plattform eingestellt werden. Jeder Nutzer hat die Möglichkeit Inhalte, die gegen gesetzliche oder behördliche Vorschriften oder gegen die guten Sitten verstoßen und den Nutzer in seinen eigenen Rechten verletzen durch PROTOPLAN entfernen zu lassen.
        <br><br>
        (5) PROTOPLAN übernimmt keine Gewähr für die Aktualität der Informationen, die auf dem Informationsportal zu internationalen Messen und anderen Veranstaltungen (Katalog) abrufbar sind. Auch insofern handelt es sich um fremde Inhalte.
    </div>

    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">11. Datenschutz</span>
    </div>
    <div class="indent-t-2">
        (1) Der Nutzer ist damit einverstanden, dass seine im Rahmen der Geschäftsbeziehungen zugehenden personenbezogenen Daten zur Abwicklung des Geschäftsverhältnisses gespeichert und automatisiert verarbeitet werden. PROTOPLAN verpflichtet sich, die bei der Registrierung und bei der Nutzung gespeicherten Daten lediglich zu Zwecken der Anbahnung oder der Erfüllung der Nutzungsverhältnisse betreffend die Plattform zu nutzen und diese nicht an Dritte weiterzugeben, es sei denn diese Dritte sind an der Leistungserbringung beteiligt oder hierzu eine behördlich angeordnete Verpflichtung besteht. Nutzer sind berechtigt, jederzeit die zu ihrer Person gespeicherten Daten unentgeltlich bei PROTOPLAN abzufragen.
        <br><br>
        (2) PROTOPLAN darf das Nutzerverhalten von Nutzern beobachten und aufzeichnen, soweit dies zur Gewährleistung eines ordnungsgemäßen Plattformbetriebes oder zur Bekämpfung eines Missbrauchs der Plattform erforderlich ist.
        <br><br>
        (3) PROTOPLAN ist bei der Registrierung der Nutzer berechtigt, für die Zwecke von PROTOPLAN Bonitätsinformationen auf der Basis mathematisch-statistischer Verfahren von sog. Auskunfteien abzurufen und aktualisierte Auskünfte zu erhalten.
        <br><br>
        (4) Nutzer sind verpflichtet, etwaige Vertragspartner von der Übermittlung von personenbezogenen Daten in Kenntnis zu setzen, soweit es sich um Daten dieser Vertragspartner handelt. Insofern sichert der Nutzer zu, dass er berechtigt ist, solche personenbezogene Daten von Vertragspartnern auf der Plattform einzustellen.
    </div>
    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">12. Urheber- und Schutzrechte</span>
    </div>
    <div class="indent-t-2">
        (1) Die Nutzer beachten die Eigentumsrechte und Urheberrechte von PROTOPLAN an den Inhalten der Plattform, an welchen Nutzungsrechte nur vergeben werden, soweit und solange dies für die Nutzung der Plattform erforderlich ist.
        <br><br>
        (2) Die auf Nutzerdateien und Inhalte von Nutzern bezogenen Rechte verbleiben bei den jeweiligen Nutzern. Soweit erforderlich, räumt der Nutzer PROTOPLAN ein einfaches Nutzungsrecht an solchen Dateien und Inhalten ein. PROTOPLAN macht sich die Inhalte der Nutzerdateien in keinem Fall zu Eigen.
    </div>
    <div class="indent-t-3">
        <span id="sub_terms_of_use" class="p-h2 f-h2">13. Sonstiges</span>
    </div>
    <div class="indent-t-2">
        (1) Es gilt ausschließlich das Recht der Bundesrepublik Deutschland. Vertragssprache ist deutsch. Der ausschließliche Gerichtsstand ist München.
        <br><br>
        (2) Die Unwirksamkeit einer oder mehrerer Bestimmungen dieses Vertrages berührt nicht die Wirksamkeit dieses Vertrages im Übrigen.
        <br><br>
        (3) Diese Nutzungsbedingungen treten an die Stelle früherer Nutzungsbedingungen. Zukünftige Änderungen dieser Bedingungen werden dem Nutzer per E-Mail übersandt und gelten als vereinbart, sofern der Nutzer diesen Änderungen nicht innerhalb von 14 Tagen nach Zugang der Mitteilung widerspricht. Auf das Widerspruchsrecht und die Rechtsfolgen des Schweigens wird der Nutzer im Falle der Änderung dieser AGB jeweils hingewiesen.
        <br><br><br>
        März 2017	Ende der Nutzungsbedingungen
    </div>
</div>