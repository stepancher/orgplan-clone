<div>
    <div class="indent-t-2">
        Kubex GmbH<br>
        Karl-Liebknecht-Straße 153-155,<br>
        04277 Leipzig<br><br><br>
    </div>

    <div class="indent-t-3">
        <span id="general_provisions" class="p-h2 f-h2"><b class="help__bold">Geschäftsführung<br><br></b></span>
    </div>

    <div class="indent-t-2">
        Andrei Kurilov<br><br><br>
    </div>

    <div class="indent-t-2">
        <b class="help__bold">Kontakt:<br><br></b>
    </div>

    <div class="indent-t-2">
        Telefon: +49 (0) 341 59 09 30 79<br><br>
        E-Mail: <a href="mailto:contact@protoplan.pro">contact@protoplan.pro<br><br></a>
    </div>

    <div class="indent-t-2">
        <b class="help__bold">Als Autor nach §55 Abs. 2 RStV verantwortlich:<br><br></b>
    </div>

    <div class="indent-t-3">
        <span id="general_provisions" class="p-h2 f-h2">Andrei Kurilov<br><br></span>
    </div>

    <div class="indent-t-2">
        <b class="help__bold">Registergericht<br><br></b>
        <div class="indent-t-2">
            Amtsgericht Leipzig - Registernummer: HRB 29722<br><br>
            Umsatzsteuer-ID: DE292133183<br><br>
        </div>
    </div>

    <div class="indent-t-2 ">
        <b class="help__bold "><br>Online-Streitbeilegung</b>
    </div>

    <div class="indent-t-2 ">
        <br> Gemäß der Verordnung (EU) Nr. 524/2013 des Europäischen Parlaments und des Rates vom 21. Mai 2013 über die Online-Beilegung verbraucherrechtlicher Streitigkeiten richtet die EU-Kommission eine Internetplattform zur Online-Beilegung von Streitigkeiten („OS-Plattform“) zwischen Unternehmern und Verbrauchern ein. Diese ist unter folgendem Link erreichbar: <a href="http://ec.europa.eu/consumers/odr" target="_blank">http://ec.europa.eu/consumers/odr</a><br><br>
    </div>
    <div class="indent-t-2 ">
        Das Angebot auf diese Website richtet sich ausschließlich an Gewerbetreibende. Die Kubex GmbH ist deshalb grundsätzlich nicht verpflichtet und nicht bereit, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.
    </div>