<?php

class DefaultController extends Controller
{

    public function actionAuth($fairId = NULL)
    {
        $this->layout = FALSE;
        if (Yii::app()->user->isGuest) {
            $this->render('auth', array('fairId' => $fairId));
        } else {
            $this->redirect(Yii::app()->createUrl('home'));
        }
    }

    public function actionRecovery()
    {
        $this->layout = FALSE;
        if (Yii::app()->user->isGuest) {
            $this->render('recovery');
        } else {
            $this->redirect(Yii::app()->createUrl('home'));
        }
    }

    public function actionReg()
    {
        $this->layout = FALSE;
        if (Yii::app()->user->isGuest) {
            $this->render('reg');
        } else {
            $this->redirect(Yii::app()->createUrl('home'));
        }
    }

    public function actionChangePassword($hash)
    {
        $recoverPass = RecoverPassword::model()->findByAttributes(['hash' => $hash]);
        $user = '';

        if(isset($_POST['ResetPassword']) && !empty($recoverPass)){

            $user = User::model()->findByPk($recoverPass->userId);
            $form = $_POST['ResetPassword'];

            if(!empty(array_filter($form))){

                if (isset($form['changePassword']) &&
                    isset($form['repeatChangePassword']) &&
                    $form['changePassword'] == $form['repeatChangePassword'])
                {
                    $user->password = password_hash($form['changePassword'], PASSWORD_DEFAULT);
                    if ($user->save(false)) {
                        RecoverPassword::model()->findByAttributes(['hash' => $hash])->delete();
                    }
                    Yii::app()->user->setFlash('warning', Yii::t('site', 'password success'));

                    $user->password = $form['changePassword'];
                    $user->email = $user->contact->email;
                    $user->login();

                    if(
                        Yii::app()->user->role == User::ROLE_EXPONENT ||
                        Yii::app()->user->role == User::ROLE_ORGANIZERS
                    ){
                        $this->redirect(Yii::app()->createUrl('/myFairs', array('langId' => Yii::app()->language)));
                    }else{

                        $this->redirect(Yii::app()->createUrl('site/index', array('langId' => Yii::app()->language)));
                    }

                } else {
                    $user->addError('password', Yii::t('user', 'Passwords do not match'));
                }
            } else {
                $user->addError('password', Yii::t('user', 'Password cannot be blank'));
            }
        }

        $this->layout = FALSE;
        $this->render('changePassword', array('hash' => $hash, 'user' => $user));
    }

    /**
     * Сброс (смена) пароля
     * @param $hash
     * @return string|void
     */
    public function actionResetPassword($hash)
    {
        $this->layout = FALSE;
        if ($rp = RecoverPassword::model()->findByAttributes(array('hash' => $hash))) {
            $this->render('changePassword', array('hash' => $hash));
        } else {
            $this->redirect(Yii::app()->createUrl('site/index', array('langId' => Yii::app()->language)));
        }
    }

    /**
     * Восстановление пароля - отправка письма
     */
    public function actionRecoverPassword()
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['Contact'])) {
            $model = new Contact('recoverPassword');
            $model->setAttributes($_POST['Contact']);
            $result = array();

            $user = NULL;
            $contact = Contact::model()->findByAttributes(['email' => $model->email]);

            if ($contact !== NULL) {
                $criteria = new CDbCriteria();
                $criteria->addCondition('contactId = :contactId');
                $criteria->params[':contactId'] = $contact->id;

                $user = User::model()->find($criteria);
            }

            if ($user === NULL || $user->banned == 1) {
                $result[CHtml::activeId($model, 'email')] = array(Yii::t('site', 'user not found'));
            } else {
                Yii::app()->consoleRunner->run('notification RecoverPassword --langId='.Yii::app()->language.' --id=' . $user->id, true);
            }

            echo json_encode($result);
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createUrl('/home'));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = new User('login');
            // collect user input data
            if (isset($_POST['User'])) {
                $model->setAttributes($_POST['User']);
            }

            $data = $model->login();
            $result = array();
            if (count($model->getErrors())) {
                foreach ($model->getErrors() as $attribute => $errors) {
                    $result[CHtml::activeId($model, $attribute)] = $errors;
                }
            }

            if (isset($data['matches']) && $data['matches']) {
                $result['matches'] = $data['matches'];
            }

            if (isset($_POST['backUrlButton']) && !empty($_POST['backUrlButton']) && is_string($_POST['backUrlButton'])) {
                Yii::$app->session->set('clickButton', $_POST['backUrlButton']);
            }

            if (isset($_POST['redirect-url']) && !empty($_POST['redirect-url'])) {
                $result['redirectUrl'] = $_POST['redirect-url'];
            }

            if (empty($result['redirectUrl']) && !empty($_GET['to'])) {
                $result['redirectUrl'] = $_GET['to'];
            }

            if(empty($result['redirectUrl'])){
                $result['redirectUrl'] = '/'.Yii::app()->language.'/';
            }

            if (isset($_GET['callback'])) {
                $result = $_GET['callback'] . ' (' . $result . ');';
            }

            if(!empty($result['redirectUrl']) && strpos($result['redirectUrl'], 'landing')){
                $result['redirectUrl'] = '/'.Yii::app()->language.'/industriesList';
            }

            if(Yii::app()->user->role == User::ROLE_ORGANIZERS){
                $result['redirectUrl'] = Yii::app()->createUrl('/myFairs');
            }

            if(Yii::app()->user->role == User::ROLE_EXPONENT){
                $result['redirectUrl'] = Yii::app()->createUrl('/myFairs');
            }

            $result = function_exists('json_encode') ? json_encode($result) : CJSON::encode($result);

            echo $result;
        } else {
            $this->redirect(Yii::app()->createUrl('home'));
        }
    }

    public function actionRegistrationLeftMenu()
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['User'])) {
            $user = new User('registration-left-menu');
            $user->setAttributes($_POST['User']);
            echo json_encode($user->register());
        }
    }

    public function actionRegistrationMainPage()
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['User'])) {
            $user = new User('registration-main');
            $user->setAttributes($_POST['User']);
            echo json_encode($user->register());
        }
    }

    public function actionRegistration()
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['User'])) {
            $user = new User('registration');
            $user->setAttributes($_POST['User']);
            echo json_encode($user->register());
        }
    }

    public function createUserSocialAuth($identity, $email, $eauth)
    {
        /**
         * /* @var CWebApplication $app
         * /* @var $eauth EAuthServiceBase
         * /* @var $identity SocialUserIdentity
         */

        $app = Yii::app();

        $socialAuth = SocialAuth::model()->findByAttributes(array(
            'socialName' => $identity->getState('service'),
            'socialId' => $identity->getId()
        ));

        if (Yii::app()->user->isGuest) {
            if (null == $socialAuth) {
                $user = new User();
                if ($identity->getState('first_name') || $identity->getState('last_name')) {
                    $user->firstName = $identity->getState('first_name');
                    $user->lastName = $identity->getState('last_name');
                } else {
                    $user->firstName = $identity->getName();
                }
                if ($gender = $identity->getState('gender')) {
                    if ($gender == 'male' || $gender == 2 || $gender == 'M') {
                        $user->gender = User::GENDER_MALE;
                    } elseif ($gender == 'female' || $gender == 1 || $gender == 'F') {
                        $user->gender = User::GENDER_FEMALE;
                    }
                }

                $contact = new Contact();
                $contact->email = $email;
                $contact->save(false);

                $user->role = User::ROLE_USER;
                $user->contactId = $contact->id;
                $user->save(false);

                $socialAuth = new SocialAuth();
                $socialAuth->socialName = $identity->getState('service');
                $socialAuth->userName = $identity->getName();
                $socialAuth->socialId = $identity->getId();
                $socialAuth->userId = $user->id;
                $socialAuth->save(false);
            } else {
                $user = User::model()->findByPk($socialAuth->userId);
            }
            unset (Yii::app()->session['email']);
            unset (Yii::app()->session['identity']);
            unset (Yii::app()->session['socialName']);
            unset (Yii::app()->session['eauth']);

            $identity->setState('id', $user->id);
            $app->user->login($identity);
            $app->consoleRunner->run(
                'notification sendActivatedMessage --id=' . $user->id . ' --email=' . $email,
                true
            );

        } else {
            $user = User::model()->findByPk(Yii::app()->user->id);
            $socialAuth = new SocialAuth();
            $socialAuth->socialName = $identity->getState('service');
            $socialAuth->userName = $identity->getName();
            $socialAuth->socialId = $identity->getId();
            $socialAuth->userId = $user->id;
            $socialAuth->save(false);
        }
        // special redirect with closing popup window
        $eauth->redirect();
    }

    public function actionGetSocialEmail()
    {
        $result = array('success' => false);
        if (isset($_POST['Contact'])) {
            $model = new Contact('getSocialEmail');
            $model->setAttributes($_POST['Contact']);
            $result = array();
            if (!$model->validate()) {
                foreach ($model->getErrors() as $attribute => $errors) {
                    $result[CHtml::activeId($model, $attribute)] = $errors;
                }
                echo json_encode($result);
                return;
            } else {
                $contact = Contact::model()->findByAttributes(array('email' => $model->email));
                if ($contact) {
                    $result[CHtml::activeId($model, 'email')] = array(Yii::t('site', 'User with this mail is already registered.'));
                    echo json_encode($result);
                    return;
                } else {
                    Yii::app()->session['email'] = $model->email;
                    $result = array('success' => true);
                    if (!Yii::app()->request->isAjaxRequest) {
                        $this->redirect(Yii::app()->createUrl('auth/default/socialAuth', array('langId' => Yii::app()->language)));
                    }
                }
            }
        }
        echo json_encode($result);
    }

    public function actionSocialAuth()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        $serviceName = $app->request->getQuery('service');
        if (!isset(Yii::app()->session['redirectUrl'])) {
            Yii::app()->session['redirectUrl'] = Yii::app()->request->urlReferrer;
        }
        if (!isset($serviceName) && isset(Yii::app()->session['socialName'])) {
            $serviceName = Yii::app()->session['socialName'];
        }
        if (isset($serviceName)) {
            /** @var $eauth EAuthServiceBase */
            $eauth = $app->eauth->getIdentity($serviceName);
            $eauth->redirectUrl = Yii::app()->session['redirectUrl'];

            try {
                if (isset(Yii::app()->session['identity']) && isset(Yii::app()->session['email']) && isset(Yii::app()->session['eauth'])) {
                    $identity = Yii::app()->session['identity'];
                    $email = Yii::app()->session['email'];
                    $eauth = Yii::app()->session['eauth'];
                    $this->createUserSocialAuth($identity, $email, $eauth);
                } else {
                    if ($eauth->authenticate()) {
                        $identity = new SocialUserIdentity($eauth);
                        // successful authentication
                        if ($identity->authenticate()) {
                            $email = $identity->getState('email');
                            if ($email == null) {
                                $socialAuth = SocialAuth::model()->findByAttributes(array(
                                    'socialName' => $identity->getState('service'),
                                    'socialId' => $identity->getId()
                                ));
                                if ($socialAuth != null) {
                                    $user = User::model()->findByPk($socialAuth->userId);
                                    if ($user != null && $user->contact) {
                                        $email = $user->contact->email;
                                    }
                                }
                            }

                            if (null == $email && !Yii::app()->user->isGuest) {
                                $email = User::model()->findByPk(Yii::app()->user->id)->contact->email;
                            }

                            if ($email != null) {
                                $this->createUserSocialAuth($identity, $email, $eauth);
                            } else {
                                Yii::app()->session['identity'] = $identity;
                                Yii::app()->session['eauth'] = $eauth;
                                Yii::app()->session['socialName'] = $identity->getState('service');
                                $this->createUserSocialAuth($identity, $email, $eauth);
                            }
                        }
                    }
                }

                // Something went wrong, redirect to login page
                $this->redirect(Yii::app()->createUrl('auth/default/login', array('langId' => Yii::app()->language)));
            } catch (EAuthException $e) {
//                $this->redirect(array('/'));
                // save authentication error to session
//                $app->user->setFlash('error', 'Ошибка входа. Пожалуйста, перезагрузите страницу и попробуйте снова.');
                throw $e;
            }
        }
    }

    public function actionRequestDemo(){

        $this->layout = FALSE;
        $requestSuccess = FALSE;

        if(
            !empty($_POST) &&
            !empty($_POST['firstName']) &&
            !empty($_POST['lastName']) &&
            !empty($_POST['email']) &&
            !empty($_POST['company'])
        ){

            Yii::app()->consoleRunner->run(
                'notification requestDemoSendEmail --firstName=' . $_POST['firstName'] . ' --lastName=' . $_POST['lastName'].' --email='. $_POST['email'].' --company="'.$_POST['company'].'"',
                true);
            $requestSuccess = TRUE;

        }elseif(
            !empty($_POST) &&
            (
                empty($_POST['firstName']) ||
                empty($_POST['lastName']) ||
                empty($_POST['email']) ||
                empty($_POST['company'])
            )
        ){
            Yii::app()->user->setFlash('error', Yii::t('requestDemo', 'All fields are required for write.'));
            $this->refresh();
        }

        if($requestSuccess){
            $this->render('requestDemoSuccess');
        }else{
            $this->render('request');
        }

    }
}