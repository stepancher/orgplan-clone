<?php

class JsonController extends Controller
{
    private $requestParams = null;

    public function actionLogin()
    {
        $model = new User('login');
        $params = $this->getParams();

        if (isset($params['User'])) {
            $model->setAttributes($params['User']);
        }

        $data = $model->login();

        $result = array();
        if (count($model->getErrors())) {
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[CHtml::activeId($model, $attribute)] = $errors;
            }
        }

        $this->jsonCallback(['status' => $data, 'errors' => $result]);
    }

    public function actionGetUserData()
    {
        $result = [];
        /** @var $model User */
        $model = Yii::app()->user->getModel();

        $result['logined'] = true;

        if ($model == NULL) {
            $model = new User;
            $result['logined'] = false;
        }


        $result['name'] = $model->firstName;
        $result['lastname'] = $model->lastName;
        $result['image'] = substr(H::getImageUrl(User::model()->findByPk(Yii::app()->user->id), 'logo', 'noAvatar'), 1);

        $this->jsonCallback($result);
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->jsonCallback(['status' => true]);
    }

    public function actionRegistration()
    {
        $params = $this->getParams();

        if (isset($params['User'])) {
            $user = new User('registration');
            $user->setAttributes($params['User']);

            $result = $user->register();
            $json = $result;
            $json['status'] = true;

            if (count($result) !== 0) {
                $json['status'] = false;
                $user->login();
            }

            $this->jsonCallback($json);
        }
    }

    public function actionRecoverPassword()
    {
        $params = $this->getParams();
        $model = new Contact('recoverPassword');
        $model->setAttributes($params['Contact']);
        $result = array();

        $user = NULL;
        $contact = Contact::model()->findByAttributes(['email' => $model->email]);

        if ($contact !== NULL) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('contactId = :contactId');
            $criteria->params[':contactId'] = $contact->id;

            $user = User::model()->find($criteria);
        }

        if ($user === NULL || $user->banned == 1) {
            $result[CHtml::activeId($model, 'email')] = array(Yii::t('site', 'user not found'));
        } else {
            Yii::app()->consoleRunner->run('notification RecoverPassword --langId='.Yii::app()->language.' --id=' . $user->id, true);
        }

        $json = $result;
        $json['status'] = true;

        if (count($result) !== 0) {
            $json['status'] = false;
        }

        $this->jsonCallback($json);
    }

    private function jsonCallback($json)
    {
        echo (isset($_GET['callback'])?$_GET['callback']:'') . '(' . json_encode($json) . ');';
    }

    private function getParams()
    {
        if ($this->requestParams === null) {
            $this->requestParams = json_decode($_GET['params'], 1);
        }

        return $this->requestParams;
    }
}