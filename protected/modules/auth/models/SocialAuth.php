<?php
/**
 * Class SocialAuth
 */
class SocialAuth extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'userId' => array(
				'label' => 'Пользователь',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'User',
					array(
						'userId' => 'id',
					),
				),
			),
			'userName' => array(
				'string',
				'label' => 'Имя пользователя',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'socialId' => array(
				'label' => 'Social Id',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'socialName' => array(
				'label' => 'Social Name',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}