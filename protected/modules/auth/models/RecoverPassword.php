<?php
/**
 * Class RecoverPassword
 */
class RecoverPassword extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'userId' => array(
				'integer',
				'label' => 'User',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'hash' => array(
				'string',
				'label' => 'Hash',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'keywords' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'url' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'linkId' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'linkType' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'linkData' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}