<?php 
return array(
    'Changing password' => 'Смена пароля',
    'New password' => 'Новый пароль',
    'Repeat new password' => 'Повторите новый пароль',
    'Repeat password' => 'Повторите пароль',
    'Save' => 'Сохранить',
);