<?php
/**
 * FacebookOAuthServiceCustom class file.
 */

/**
 * Facebook provider class.
 */
class FacebookOAuthServiceCustom extends FacebookOAuthService
{

    protected function fetchAttributes()
    {
        $info = (object)$this->makeSignedRequest('https://graph.facebook.com/me');

        if(isset($info->id))
            $this->attributes['id'] = $info->id;

        if(isset($info->first_name))
            $this->attributes['first_name'] = $info->first_name;

        if(isset($info->last_name))
            $this->attributes['last_name'] = $info->last_name;

        if(isset($info->gender))
            $this->attributes['gender'] = $info->gender;

        if(isset($info->link))
            $this->attributes['url'] = $info->link;
    }
}
