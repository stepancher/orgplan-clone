<?php

class SocialUserIdentity extends EAuthUserIdentity
{
    /**
     * Authenticates a user based on {@link service}.
     * This method is required by {@link IUserIdentity}.
     *
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        if ($this->service->isAuthenticated) {
            $this->id = $this->service->id;
            $this->name = $this->service->getAttribute('name');

            $this->setState('id', $this->id);
            $this->setState('name', $this->name);
            $this->setState('service', $this->service->serviceName);

            if (!empty($this->service->getAttribute('email'))) {
                $this->setState('email', $this->service->getAttribute('email'));
            }
            if (!empty($this->service->getAttribute('first_name'))) {
                $this->setState('first_name', $this->service->getAttribute('first_name'));
            }
            if (!empty($this->service->getAttribute('last_name'))) {
                $this->setState('last_name', $this->service->getAttribute('last_name'));
            }
            if (!empty($this->service->getAttribute('gender'))) {
                $this->setState('gender', $this->service->getAttribute('gender'));
            }

            // You can save all given attributes in session.
            //$attributes = $this->service->getAttributes();
            //$session = Yii::app()->session;
            //$session['eauth_attributes'][$this->service->serviceName] = $attributes;

            $this->errorCode = self::ERROR_NONE;
        } else {
            $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
        }
        return !$this->errorCode;
    }

}