<?php
/**
 * VKontakteOAuthServiceCustom class file.
 */

/**
 * VKontakte provider class.
 */
class VKontakteOAuthServiceCustom extends VKontakteOAuthService
{
    protected function fetchAttributes()
    {
        $info = (array)$this->makeSignedRequest('https://api.vk.com/method/users.get.json', array(
            'query' => array(
                'uids' => $this->uid,
                'fields' => 'sex',
            ),
        ));

        $info = $info['response'][0];

        $this->attributes['id'] = $info->uid;
        $this->attributes['first_name'] = $info->first_name;
        $this->attributes['last_name'] = $info->last_name;
        $this->attributes['gender'] = $info->sex;
        $this->attributes['url'] = 'http://vk.com/id' . $info->uid;
    }

}
