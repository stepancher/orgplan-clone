<?php
/**
 * GoogleOAuthServiceCustom class file.
 */

/**
 * Google provider class.
 */
class GoogleOAuthServiceCustom extends GoogleOAuthService
{
    protected function fetchAttributes()
    {
        $info = (array)$this->makeSignedRequest('https://www.googleapis.com/oauth2/v1/userinfo');

        $this->attributes['info'] = $info;
        $this->attributes['id'] = $info['id'];
        $this->attributes['first_name'] = $info['given_name'];
        $this->attributes['last_name'] = $info['family_name'];
        $this->attributes['name'] = $info['name'];

        if (!empty($info['link'])) {
            $this->attributes['url'] = $info['link'];
        }
        if (!empty($info['email'])) {
            $this->attributes['email'] = $info['email'];
        }
        if (!empty($info['gender']))
            $this->attributes['gender'] = $info['gender'] == 'male' ? 'M' : 'F';
    }
}
