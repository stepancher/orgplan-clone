<?php

/**
 * Class FormGen
 */
Yii::import('application.modules.proposal.ProposalModule');
class FormGen extends CInputWidget
{

    public $formName;

    public $model;

    public $form;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        foreach ($this->form as $input) {
            echo '<div class="control-group">' .
            $this->renderLabel($input) .
            '<div class="controls col-md-8">';

            if (!isset($input['type'])) {
                echo $this->renderInput($input);
            } else {
                $method = 'render' . ucfirst($input['type']);
                echo $this->$method($input);
            }

            echo '</div></div>';
        }
    }

    /**
     * @param $input
     * @return string
     */
    private function renderLabel($input)
    {
        $return = '<label class="control-label col-md-4" for="">';

        if(is_string($input)) {
            $return = $return . $this->model->getAttributeLabel($input);
        } elseif (isset($input['label'])) {
            $return = $return . $input['label'];
        } else {
            $return = $return . $this->model->getAttributeLabel($input['name']);
        }

        $return = $return . '</label>';

        return $return;
    }

    /**
     * @param $input
     * @param string $type
     * @return string
     */
    private function renderInput($input, $type = 'text')
    {
        $return = '<input type="' . $type . '" name="';
        $return = $return . $this->formName;
        $return = $return . '[';
        if (is_string($input)) {
            $return = $return . $input;
        }  else {
            $return = $return . $input['name'];
        }

        if ((is_string($input) && $input == 'login') || $type == 'password'){
            $return = $return . ']"';
            $return = $return . ' readonly onmouseover="this.removeAttribute(\'readonly\')"';
            $return = $return . ' autocomplete="off"';
            $return = $return . ' value="';
        } else {
            $return = $return . ']" value="';
        }

        if (is_string($input)) {
            $return = $return . $this->model->{$input};
        } elseif (isset($input['value'])) {
            $return = $return . $input['value'];
        } elseif (isset($this->model->{$input['name']})) {
            //@TODO don't send objects in widget, refactior it
            if (is_object($this->model->{$input['name']})) {
                $return = $return . $this->model->{$input['name']}->name;
            } else {
                $return = $return . $this->model->{$input['name']};
            }
        } else {
            $return = $return . '';
        }

        $return = $return . '"';

        if (isset($input['placeholder'])) {
            $return = $return . ' placeholder="' . $input['placeholder'] . '"';
        }

        if (isset($input['htmlOptions'])) {
            $return = $return . $this->htmlOptions($input['htmlOptions']);
        }

        $return = $return . '>';

        if(isset($input['passErrorUser']) && !empty($input['passErrorUser'])){
            $user = $input['passErrorUser'];
            $error = CHtml::error($user, 'password', array('style' => 'color: red'));
            return $return . $error;
        }

        return $return;
    }

    private function renderPasswordInput($input)
    {
        return $this->renderInput($input, 'password');
    }

    /**
     * @param $input
     * @return mixed
     * @throws Exception
     */
    private function renderSelect($input)
    {
        if (!isset($input['htmlOptions'])) {
            $input['htmlOptions'] = NULL;
        }

        return $this->widget('ext.widgets.ListAutoComplete', [
            'name' => $this->formName . '[' . $input['name'] . ']',
            'selectedItems' => [$this->model->{$input['name']}],
            'multiple' => false,
            'autoComplete' => false,
            'ajaxAutoComplete' => false,
            'showNotEmptyLabel' => false,
            'showLabel' => false,
            'label' => 'Выбрать',
            'htmlOptions' => $input['htmlOptions'],
            'itemOptions' => [
                'data' => $input['options'],
                'text' => 'name',
            ]
        ], true);
    }

    /**
     * @param $input
     * @return mixed
     * @throws Exception
     */
    private function renderDatePicker($input)
    {
        return $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
            'name' => $this->formName . '[' . $input['name'] . ']',
            'htmlOptions' => array(
                'type' => 'text',
                'value' => stristr($this->model->dob, '1970') || $this->model->dob == ''? '' : date('d.m.Y', strtotime($this->model->dob)),
                'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark'
            ),
            'pluginOptions' => array(
                'format' => 'dd.mm.yyyy',
                'language' => 'ru'
            ),
        ), true);
    }

    /**
     * @param $header
     * @param $type
     * @return string
     */
    private function renderHeader($header, $type) {
        $return = '<' . $type;

        if (isset($header['htmlOptions'])) {
            $return = $return . $this->htmlOptions($header['htmlOptions']);
        }

        $return = $return . '>';

        return $return . $header['value'] . '</' . $type . '>';
    }

    /**
     * @param $input
     * @return string
     */
    private function renderH2($input)
    {
        return $this->renderHeader($input, 'h2');
    }

    /**
     * @param $input
     * @return string
     */
    private function renderH3($input)
    {
        return $this->renderHeader($input, 'h3');
    }

    //@TODO Refactor it
    /**
     * @param $input
     * @return mixed|string
     */
    private function renderOrganizer($input) {
        $user = $this->model;
        $user_organizers = UserHasOrganizer::model()->findAllByAttributes(['userId' => $user->id]);
        $organizers = Organizer::model()->findAll();
        $data = [];
        $selectedItems = [];

        if (empty($user_organizers)) {
            $user_organizers  = [];
        }

        foreach ($organizers as $key => $value) {
            $data[$value->id] = $value->name;
        }

        foreach ($user_organizers as $key => $value) {
            $selectedItems[$value->organizerId] = ['selected' => true];
        }

        return $this->getOwner()->widget('vendor.anggiaj.eselect2.ESelect2', array(
            'name' => 'UserHasOrganizer[organizerId]',
            'data' => $data,
            'htmlOptions' => array(
                'options' => $selectedItems,
                'multiple' => 'multiple',
                'style' => 'width:100%;',
                'data-role' => '7',
            ),
        ), true);
    }

    /**
     * @param $options
     * @return string
     */
    private function htmlOptions($options) {
        $return = '';
        foreach ($options as $option => $value) {
            $return = $return . ' ' . $option . '="' . $value . '"';
        }

        return $return;
    }

    /**
     * @param $input
     * @return mixed|string
     */
    private function renderDropDownList($input)
    {

        if (!isset($input['htmlOptions'])) {
            $input['htmlOptions'] = NULL;
        }

        return $this->getOwner()->widget( 'ext.anggiaj.eselect2.ESelect2', array(
            'name' => $input['name'],
            'options' => array(
//                'formatNoMatches' => 'js:function(){return "' . Yii::t('proposalModule.proposal', 'No matches found') . '";}',
//                'formatInputTooShort' => 'js:function(input,min){return "' . Yii::t('proposalModule.proposal', 'Please enter {chars} more characters', array('{chars}' => '"+(min-input.length)+"')) . '";}',
//                'formatInputTooLong' => 'js:function(input,max){return "' . Yii::t('proposalModule.proposal', 'Please enter {chars} less characters', array('{chars}' => '"+(input.length-max)+"')) . '";}',
//                'formatSelectionTooBig' => 'js:function(limit){return "' . Yii::t('proposalModule.proposal', 'You can only select {count} items', array('{count}' => '"+limit+"')) . '";}',
//                'formatLoadMore' => 'js:function(pageNumber){return "' . Yii::t('proposalModule.proposal', 'Loading more results...') . '";}',
//                'formatSearching' => 'js:function(){return "' . Yii::t('proposalModule.proposal', 'Searching...') . '";}',
                'width' => '100%',
                'placeholder' => "",
                'minimumInputLength' => 1,
                'ajax' => array(
                    'url' => $this->getOwner()->createUrl('get'.$input['getMethodName']),
                    'dataType' => 'json',
                    'data' => 'js:function (term, page) { return { q: term}; }',
                    'results' => 'js:function(data,page){return {results: data};}',

                ),
                'formatResult'    => 'js:function(data){
                                return data.name;
                            }',
                'formatSelection' => 'js: function(data) {
                                return data.name;
                            }',
            ),
            'htmlOptions' => $input['htmlOptions'],
        ), true);
    }
}

