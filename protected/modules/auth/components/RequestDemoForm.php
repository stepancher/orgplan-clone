<?php

/**
 * File RequestDemoForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class RequestDemoForm
 */
class RequestDemoForm extends TbForm
{
    /**
     * @var string
     */
    public $elementGroupName = '';

    /**
     * @var string
     */
    public $template = '
    <div class="cols-row offset-small-l-5 offset-small-r-5 top-width-input-block-reg control-group">
			<div class="col-md-12 control">
				<div class="cols-row">
					<div class="col-md-12">
						{firstName}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{lastName}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{email}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{company}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{submit}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						<input type="hidden" name="redirect-url" value="{redirectUrl}">
					</div>
				</div>
			</div>
		</div>
    ';

    public function renderElements()
    {

        $parts = array(
            '{firstName}' => TbHtml::textField(
                'firstName',
                NULL,
                array(
                    'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                    'placeholder' => Yii::t('requestDemo', 'First name'),
                    'required'
                )
            ),
            '{lastName}' => TbHtml::textField(
                'lastName',
                NULL,
                array(
                    'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                    'placeholder' => Yii::t('requestDemo', 'Last name'),
                )
            ),
            '{email}' => TbHtml::textField(
                'email',
                NULL,
                array(
                    'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                    'placeholder' => Yii::t('requestDemo', 'E-mail'),
                )
            ),
            '{company}' => TbHtml::textField(
                'company',
                NULL,
                array(
                    'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                    'placeholder' => Yii::t('requestDemo', 'Company'),
                )
            ),
        );

        $content = '';

        foreach ($this->getElements() as $element) {
            $parts['{' . $element->name . '}'] = $this->renderElement($element);
        }

        $content = '';
        $buttonLabel = Yii::t('requestDemo', 'Send');
        $parts['{submit}'] = TbHtml::submitButton($buttonLabel, [
            'class' => 'button-180 b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize button-180-inside-text',
            'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200)',
        ]);

        $parts['{content}'] = $content;
        $parts['{redirectUrl}'] = Yii::app()->request->getUrlReferrer();

        if(
            Yii::app()->request->getUrlReferrer() == 'https://protoplan.pro/landing'.Yii::app()->language.'/index.html'
        ){
            $parts['{redirectUrl}'] = Yii::app()->createUrl('/industriesList');
        }


        return strtr($this->template, $parts);
    }

    /**
     * Инициализация формы
     */
    public function init()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();

        /** @var User $model */
        $model = $this->getModel();

        parent::init();
    }

    /**
     * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=User[]['login']
     * @param string $name
     * @param TbFormInputElement $element
     * @param bool $forButtons
     */
    public function addedElement($name, $element, $forButtons)
    {
        $element->name = $this->elementGroupName . $name;
    }

}