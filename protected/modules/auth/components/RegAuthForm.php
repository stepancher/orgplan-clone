<?php

/**
 * File RegAuthForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class RegAuthForm
 */
class RegAuthForm extends TbForm
{
    /**
     * @var string
     */
    public $elementGroupName = '';

    /**
     * @var string
     */
    public $template = '
    <div class="cols-row offset-small-l-5 offset-small-r-5 top-width-input-block-reg">
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-12">
						{firstName}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{email}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{password}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{repeatPassword}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{content}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{termsOfUse}
					</div>
				</div>
    			<div class="cols-row">
					<div class="col-md-12">
						{rememberMe}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{submit}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						<input type="hidden" name="redirect-url" id="redirectUrl" value="{redirectUrl}">
					</div>
				</div>
			</div>
		</div>
    ';

    public function renderElements()
    {
        $content = '';
        $parts = array();
        foreach ($this->getElements() as $element) {
            $parts['{' . $element->name . '}'] = $this->renderElement($element);
        }

        if ($this->getModel()->getScenario() == 'login') {
            $buttonLabel = Yii::t('system', 'Sign In');
            $parts['{submit}'] = TbHtml::submitButton($buttonLabel, [
                'class' => 'button-140 b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize',
                'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200)',
            ]);
            $content = '<div class="create-your-org-plan">
				<a href="' . Yii::app()->createUrl('auth/default/reg') . '">' . Yii::t('system', 'Registration') . '</a>
				<a style="margin: 100px 0 0 74px;" href="' . Yii::app()->createUrl('auth/default/recovery') . '">' . Yii::t('system', 'Forgot your password?') . '</a>
			</div>';
        } else {
            $content = '<div class="terms-of-use terms-of-use-recast">
				<a href="' . Yii::app()->createUrl('/help/default/termsOfUse') . '">' . Yii::t('system', 'Terms of use') . '</a></br>
			    <a href="' . Yii::app()->createUrl('/help/default/confidentiality') . '">' . Yii::t('system', 'Privacy') . '</a>
            </div>';
            $buttonLabel = Yii::t('system', 'Registration');
            $parts['{submit}'] = TbHtml::submitButton($buttonLabel, [
                'class' => 'button-180 b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize button-180-inside-text',
                'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200)',
            ]);
        }

        $parts['{content}'] = $content;

        $parts['{redirectUrl}'] = Yii::app()->request->getUrlReferrer();

        if(
            Yii::app()->request->requestUri == Yii::app()->createUrl('/login').'/' ||
            Yii::app()->request->requestUri == Yii::app()->createUrl('/registration').'/'
        ){
            $parts['{redirectUrl}'] = Yii::app()->createUrl('/catalog');
        }

        if(
            Yii::app()->request->getUrlReferrer() == 'https://protoplan.pro/landing'.Yii::app()->language.'/index.html'
        ){
            $parts['{redirectUrl}'] = Yii::app()->createUrl('/catalog');
        }

        return strtr($this->template, $parts);
    }

    /**
     * Инициализация формы
     */
    public function init()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();

        /** @var User $model */
        $model = $this->getModel();

        $this->elements = CMap::mergeArray(
            array_flip(array_keys($this->initElements())),
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );

        parent::init();
    }

    /**
     * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=User[]['login']
     * @param string $name
     * @param TbFormInputElement $element
     * @param bool $forButtons
     */
    public function addedElement($name, $element, $forButtons)
    {
        $element->name = $this->elementGroupName . $name;
    }

    /**
     * Инициализация элементов формы
     * @return array
     */
    public function initElements()
    {
        $success = '<i data-icon="&#xe426;" class="b-button b-button-icon b-input-icon d-bg-success"></i>';
        return array(
            'firstName' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'firstName', array(
                        'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('firstName'),
                    )) . $success
            ),
            'email' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'email', array(
                        'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                        'placeholder' => Contact::model()->getAttributeLabel('email'),
                    )) . $success
            ),
            'password' => array(
                'input' => TbHtml::activePasswordField($this->getModel(), 'password', array(
                        'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('password'),
                    )) . $success
            ),
            'repeatPassword' => array(
                'input' => TbHtml::activePasswordField($this->getModel(), 'repeatPassword', array(
                        'class' => 'input-block-level d-bg-tab--focus d-text-dark',
                        'placeholder' => Yii::t('user', 'repeatPassword'),
                    )) . $success
            ),
            'termsOfUse' => array(
                'type' => TbHtml::INPUT_TYPE_CHECKBOX,
                'placeholder' => $this->getModel()->getAttributeLabel('termsOfUse'),
                'groupOptions' => array(
                    'class' => 'terms-of-use bottom-checkbox checkbox'
                )
            ),
            'rememberMe' => array(
                'type' => TbHtml::INPUT_TYPE_CHECKBOX,
                'placeholder' => User::model()->getAttributeLabel('rememberMe'),
                'groupOptions' => array(
                    'class' => 'rememberMe'
                )
            ),
        );
    }
}