<?php
/**
 * @var CWebApplication $app
 * @var ARController $this
 * @var string $content
 *
 * @var $this CController
 * @var $publishUrl string
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

Yii::app()->bootstrap->register();
$publishUrl = $am->publish(Yii::getPathOfAlias('auth.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/social-icon.css');
$cs->registerCssFile($publishUrl . '/css/main.css');
?>

<!DOCTYPE html>
<html lang="en" class="-webkit-">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <META http-equiv="X-UA-Compatible" content="IE=11">
    <meta name='wmail-verification' content='bb4b28284d3e19c4829a70a24fb95f27'/>
    <meta name='yandex-verification' content='6d0d4f68b645c3ab'/>
    <meta name="google-site-verification" content="JUqR23YuoOgnXhXwZZgy08my4kh97mh6L1hpeOvT5_c"/>
    <meta name="msvalidate.01" content="93AB6A3241E35BFF516ED236B92D4035"/>
    <title><?= CHtml::decode(strip_tags($this->pageTitle)); ?></title>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="language" content="<?= CHtml::encode($this->language); ?>"/>
    <?php endif; ?>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="description" content="<?= str_replace('"', '&quot;', strip_tags($this->description)); ?>"/>
    <?php endif; ?>
    <link rel="shortcut icon" href="<?= $app->request->baseUrl . '/favicon.ico'; ?>">
    <script src="https://cdn.ravenjs.com/2.1.0/raven.min.js"></script>
</head>
<?= TbHtml::openTag('body') ?>
<div class="clear flash-clear">
    <div class="b-flash">
        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'alerts' => implode(' ', array(
                TbHtml::ALERT_COLOR_INFO,
                TbHtml::ALERT_COLOR_SUCCESS,
                TbHtml::ALERT_COLOR_WARNING,
                TbHtml::ALERT_COLOR_ERROR,
                TbHtml::ALERT_COLOR_DANGER,
            ))
        ));
        ?>
    </div>
</div>

<div class="wrapper">

    <a style="display: block;" href="/landing/<?=Yii::app()->language?>/index.html" class="orange-block-logo"></a>
    <?php if(Yii::app()->language == 'ru'):?>
        <!--        <div class="agrosalon-block-logo"></div>-->
    <?php endif;?>
    <div class="word">
        <?=Yii::t('requestDemo', 'Demo requesting')?>
    </div>
    <div style="margin-bottom:10px;">
        <?=Yii::t('requestDemo', 'Requesting successful')?>
    </div>
    <a
        href="<?=Yii::app()->createUrl('/home');?>"
        class="button-180 b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize button-180-inside-text"
        style="
            display: block;
            padding-top: 10px;
            padding-bottom: 10px;
            width: 156px;
            margin: 0px auto;
        "
    >
        <?=Yii::t('requestDemo', 'Back');?>
    </a>
</div>

<?php $this->renderPartial('//layouts/counters/_carriotQuest');?>
<?php $this->renderPartial('//layouts/counters/_ramblerTop100');?>
<?php $this->renderPartial('//layouts/counters/_yandexMetrika');?>
<?php $this->renderPartial('//layouts/counters/_googleTagManager');?>
<?php $this->renderPartial('//layouts/counters/_ratingMailRu');?>

<?= TbHtml::closeTag('body') ?>
</html>

