<?php
/**
 * @var CWebApplication $app
 * @var ARController $this
 * @var string $content
 *
 * @var $this CController
 * @var $publishUrl string
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

Yii::app()->bootstrap->register();
$publishUrl = $am->publish(Yii::getPathOfAlias('auth.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/social-icon.css');
$cs->registerCssFile($publishUrl . '/css/main.css');
?>

<!DOCTYPE html>
<html lang="en" class="-webkit-">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <META http-equiv="X-UA-Compatible" content="IE=11">
    <meta name='wmail-verification' content='bb4b28284d3e19c4829a70a24fb95f27'/>
    <meta name='yandex-verification' content='6d0d4f68b645c3ab'/>
    <meta name="google-site-verification" content="JUqR23YuoOgnXhXwZZgy08my4kh97mh6L1hpeOvT5_c"/>
    <meta name="msvalidate.01" content="93AB6A3241E35BFF516ED236B92D4035"/>
    <title><?= CHtml::decode(strip_tags($this->pageTitle)); ?></title>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="language" content="<?= CHtml::encode($this->language); ?>"/>
    <?php endif; ?>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="description" content="<?= str_replace('"', '&quot;', strip_tags($this->description)); ?>"/>
    <?php endif; ?>
    <link rel="shortcut icon" href="<?= $app->request->baseUrl . '/favicon.ico'; ?>">
    <script src="https://cdn.ravenjs.com/2.1.0/raven.min.js"></script>
</head>
<?= TbHtml::openTag('body') ?>

<div class="wrapper">

    <a style="display: block;" href="/<?=Yii::app()->language?>/" class="orange-block-logo"></a>
    <?php if(Yii::app()->language == 'ru'):?>
        <!--        <div class="agrosalon-block-logo"></div>-->
    <?php endif;?>
    <div class="word"><?=Yii::t('site', 'Password recovery');?></div>

    <div class="control-group"><label class="control-label required" for="User_firstName">E-mail <span class="required">*</span></label><div class="controls"><input class="input-block-level d-bg-tab--focus d-text-dark" placeholder="E-mail" name="User[firstName]" id="User_firstName" type="text"><i data-icon="" class="b-button b-button-icon b-input-icon d-bg-success"></i><p id="User_firstName_em_" style="display:none" class="help-block"></p></div></div>
    <div class="cols-row">
        <div class="col-md-12">
            <span class="msg" style="display: block; text-align: center; margin-bottom: 10px"></span>
            <button class="button-180 b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize button-180-inside-text" type="submit" name="yt0"><?=Yii::t('site', 'Recover password');?></button>
        </div>
    </div>

    <script>
        (function ()
        {
            var input = $('.input-block-level');

            function afterClick()
            {
                var url = '/<?=Yii::app()->language?>/auth/default/recoverPassword/';
                var params = {
                    Contact: {
                        email: input.val()
                    }
                };

                $.post(url, params).done(afterLoad);
            }

            function afterLoad(data)
            {
                var msg = JSON.parse(data);

                $('.msg').html('');

                if (msg.Contact_email != undefined && msg.Contact_email[0] != undefined) {
                    $('.msg').html(msg.Contact_email[0]);
                }

                if (msg.length == 0) {
                    $('.msg').html('<?=Yii::t('site', 'Mail with link send on email');?>');
                    input.val('');
                }
            }

            $('.button-180').click(afterClick);
        }
        )();
    </script>

<!--    <a style="text-align: center; display: block; width: 100%; text-decoration: underline;" href="/ru/login">--><?//= Yii::t('site', 'already_have') ?><!--</a>-->
</div>

<?php $this->renderPartial('//layouts/counters/_carriotQuest');?>
<?php $this->renderPartial('//layouts/counters/_ramblerTop100');?>
<?php $this->renderPartial('//layouts/counters/_yandexMetrika');?>
<?php $this->renderPartial('//layouts/counters/_googleTagManager');?>
<?php $this->renderPartial('//layouts/counters/_ratingMailRu');?>

<?= TbHtml::closeTag('body') ?>
</html>

