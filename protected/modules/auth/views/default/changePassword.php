<?php
/**
 * @var CWebApplication $app
 * @var ARController $this
 * @var string $content
 * @var	User $user
 * @var $this CController
 * @var $publishUrl string
 * @var	string	$hash
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
Yii::app()->bootstrap->register();
$cs->registerCssFile($publishUrl . '/less/global.css');

$publishUrl = $am->publish(Yii::getPathOfAlias('auth.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/social-icon.css');
$cs->registerCssFile($publishUrl . '/css/main.css');
?>

<!DOCTYPE html>
<html lang="en" class="-webkit-">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="ru"/>
	<META http-equiv="X-UA-Compatible" content="IE=11">
	<meta name='wmail-verification' content='bb4b28284d3e19c4829a70a24fb95f27'/>
	<meta name='yandex-verification' content='6d0d4f68b645c3ab'/>
	<meta name="google-site-verification" content="JUqR23YuoOgnXhXwZZgy08my4kh97mh6L1hpeOvT5_c"/>
	<meta name="msvalidate.01" content="93AB6A3241E35BFF516ED236B92D4035"/>
	<title><?= CHtml::decode(strip_tags($this->pageTitle)); ?></title>
	<?php if (property_exists($this, 'language')) : ?>
		<meta name="language" content="<?= CHtml::encode($this->language); ?>"/>
	<?php endif; ?>
	<?php if (property_exists($this, 'language')) : ?>
		<meta name="description" content="<?= str_replace('"', '&quot;', strip_tags($this->description)); ?>"/>
	<?php endif; ?>
	<link rel="shortcut icon" href="<?= $app->request->baseUrl . '/favicon.ico'; ?>">
	<script src="https://cdn.ravenjs.com/2.1.0/raven.min.js"></script>
</head>
<?= TbHtml::openTag('body') ?>

<?php if (!IS_MODE_DEV) : ?>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function () {
				try {
					w.yaCounter31914033 = new Ya.Metrika({
						id: 31914033,
						clickmap: true,
						trackLinks: true,
						accurateTrackBounce: true,
						webvisor: true
					});
				} catch (e) {
				}
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () {
					n.parentNode.insertBefore(s, n);
				};
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/31914033" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

<?php if (stristr($_SERVER['HTTP_HOST'], 'orgplan.pro') || stristr($_SERVER['HTTP_HOST'], 'orgplan.ru')): ?>
	<!-- BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
		(function () {
			var widget_id = 'bzKSgGjm4U';
			var s = document.createElement('script');
			s.type = 'text/javascript';
			s.async = true;
			s.src = '//code.jivosite.com/script/widget/' + widget_id;
			var ss = document.getElementsByTagName('script')[0];
			ss.parentNode.insertBefore(s, ss);
		})();</script>
	<!-- {/literal} END JIVOSITE CODE -->
<?php endif; ?>
<?php endif;?>


<div class="wrapper">

	<a style="display: block;" href="/<?=Yii::app()->language?>/" class="orange-block-logo"></a>
<!--	<div class="agrosalon-block-logo"></div>-->
	<div class="word"><?=Yii::t('AuthModule.auth', 'Changing password');?></div>

	<div class="orgplan">
		<div class="no-layout-form">
			<div class="col-md-8">
				<form action="<?=$this->createUrl('changePassword', array('hash' => $hash))?>" method="post" class="orgplan-form">
					<?= $this->widget('application.modules.auth.components.FormGen', [
						'formName' => 'ResetPassword',
						'form' => [
							[
								'name' => 'changePassword',
								'label' => Yii::t('AuthModule.auth', 'New password'),
								'value' => '',
								'placeholder' => Yii::t('AuthModule.auth', 'New password'),
								'type' => 'passwordInput'
							],
							[
								'name' => 'repeatChangePassword',
								'label' => Yii::t('AuthModule.auth', 'Repeat new password'),
								'value' => '',
								'placeholder' => Yii::t('AuthModule.auth', 'Repeat password'),
								'type' => 'passwordInput',
								'passErrorUser' => !empty($user) ? $user : '',
							],
						],
					], true); ?>
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-green" value="<?=Yii::t('AuthModule.auth', 'Save');?>">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?= TbHtml::closeTag('body') ?>
</html>