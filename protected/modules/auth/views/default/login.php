<?php
/* @var $this SiteController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="row">
	<div class="span6 offset3">
		<div class="well">
			<fieldset>
				<?php echo $form = new TbForm(array(
					'activeForm' => array(
						'class'=>'TbActiveForm',
						'enableAjaxValidation'=> true,
						'enableClientValidation'=> false,
						'clientOptions'=>array(
							'validateOnSubmit' => true,
							'validateOnType' => false,
							'validateOnChange' => false
						),
					),
					'action' => Yii::app()->createUrl('auth/default/login'),
					'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
					'elements' => array(
						'login' => array(
							'type' => TbHtml::INPUT_TYPE_TEXT,
						),
						'password' => array(
							'type' => TbHtml::INPUT_TYPE_PASSWORD,
						),
					),
					'buttons' => array(
						'submit' => array(
							'type' => TbHtml::BUTTON_TYPE_SUBMIT,
							'label' => 'Войти',
							'attributes' => array('color' => TbHtml::BUTTON_COLOR_SUCCESS),
						),
					),
				), $model); ?>
				<?php
				?>
			</fieldset>
		</div>
	</div>
</div>
