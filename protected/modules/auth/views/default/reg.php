<?php
/**
 * @var CWebApplication $app
 * @var ARController $this
 * @var string $content
 *
 * @var $this CController
 * @var $publishUrl string
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

Yii::app()->bootstrap->register();
$publishUrl = $am->publish(Yii::getPathOfAlias('auth.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/social-icon.css');
$cs->registerCssFile($publishUrl . '/css/main.css');
?>

<!DOCTYPE html>
<html lang="en" class="-webkit-">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <META http-equiv="X-UA-Compatible" content="IE=11">
    <meta name='wmail-verification' content='bb4b28284d3e19c4829a70a24fb95f27'/>
    <meta name='yandex-verification' content='6d0d4f68b645c3ab'/>
    <meta name="google-site-verification" content="JUqR23YuoOgnXhXwZZgy08my4kh97mh6L1hpeOvT5_c"/>
    <meta name="msvalidate.01" content="93AB6A3241E35BFF516ED236B92D4035"/>
    <title><?= CHtml::decode(strip_tags($this->pageTitle)); ?></title>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="language" content="<?= CHtml::encode($this->language); ?>"/>
    <?php endif; ?>
    <?php if (property_exists($this, 'language')) : ?>
        <meta name="description" content="<?= str_replace('"', '&quot;', strip_tags($this->description)); ?>"/>
    <?php endif; ?>
    <link rel="shortcut icon" href="<?= $app->request->baseUrl . '/favicon.ico'; ?>">
    <script src="https://cdn.ravenjs.com/2.1.0/raven.min.js"></script>
</head>
<?= TbHtml::openTag('body') ?>

<div class="wrapper">

    <a style="display: block;" href="/<?=Yii::app()->language?>/" class="orange-block-logo"></a>
    <?php if(Yii::app()->language == 'ru'):?>
<!--        <div class="agrosalon-block-logo"></div>-->
    <?php endif;?>
    <div class="word"><?=Yii::t('system', 'Registration')?></div>

    <div class="social-links-block"  style="width: auto; margin: inherit; display: none">
        <?php
        $this->widget('ZAuthWidget', array('action' => '/auth/default/socialAuth'));
        ?>
    </div>
    <?php
    $model = new User('registration');

    Yii::import('application.modules.auth.components.RegAuthForm');
    $this->widget('ext.widgets.FormView', array(
        'form' => new RegAuthForm(
            array(
                'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
                'action' => Yii::app()->createUrl('auth/default/registration'),
                'activeForm' => array(
                    'class' => 'TbActiveForm',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnType' => false,
                        'validateOnChange' => false,
                        'afterValidate' => 'js:function(form, data, hasError){
                            if(!hasError) {
                                try {
                                    yaCounter31914033.reachGoal("registracia");
                                } catch (e) {
                                    console.log(e);
                                    if (Raven != undefined) {
                                        Raven.config("http://6c0aa533332643918320060f986ea813@sentry.orgplan.ru/4").install();
                                        Raven.captureException(e);
                                    }
                                }
                                window.location = document.getElementById("redirectUrl").value;
                            }

                        }'
                    ),
                ),
            ),
            $model, $this
        ),
        'htmlOptions' => array(),
    ));
    ?>
    <a style="text-align: center; display: block; width: 100%; text-decoration: underline;" href="/<?=Yii::app()->language?>/login"><?= Yii::t('site', 'already_have') ?></a>
</div>

<?php $this->renderPartial('//layouts/counters/_carriotQuest');?>
<?php $this->renderPartial('//layouts/counters/_ramblerTop100');?>
<?php $this->renderPartial('//layouts/counters/_yandexMetrika');?>
<?php $this->renderPartial('//layouts/counters/_googleTagManager');?>
<?php $this->renderPartial('//layouts/counters/_ratingMailRu');?>

<?= TbHtml::closeTag('body') ?>
</html>

