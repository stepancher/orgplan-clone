<?php
/**
 * @var array $services
 */

$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();

Yii::app()->bootstrap->register();

$publishUrl = $am->publish(Yii::getPathOfAlias('auth.assets'), false, -1, YII_DEBUG);
$cs->registerCssFile($publishUrl . '/css/social-icon.css');
$cs->registerCssFile($publishUrl . '/css/main.css');
?>
<div class="services">
	<ul class="auth-services clear">
		<?php

		foreach ($services as $name => $service) {
			echo '<li class="auth-service ' . $service->id . '">';
			//$html = '<span class="auth-icon ' . $service->id . '"><i></i></span>';
			//$html .= '<span class="auth-title">' . $service->title . '</span>';
			$html = CHtml::link('', array($action, 'service' => $name), array(
				'class' => 'social-icon-' . $service->id . (in_array($service->id, $socialConnected) ? ' social-icon-' . $service->id . '--active ' : ''),
			));
			echo $html;
			echo '</li>';
		}
		?>
	</ul>
</div>
