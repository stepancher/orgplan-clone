<?php

class UserModule extends CWebModule {

	public function init() {

		$this->setImport(array(
			'user.models.*',
		));
	}
}
