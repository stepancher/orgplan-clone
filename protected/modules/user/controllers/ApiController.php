<?php

class ApiController extends Controller {

    const STATUS_NOT_AUTHORIZED = '403 Forbidden';

    protected $authenticated = false;

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function actionAuth($SystemId, $UserName, $Password) {

        return true;
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        echo json_encode(true);
    }

    /**
     * @param integer $userId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetUserProperties($userId, array $properties){

        if(!empty($user = User::model()->findByPk($userId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($user->{$name}) &&
                    !empty($user->{$name})
                ){
                    $res[$name] = $user->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param integer $userId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetUserContactProperties($userId, array $properties){

        if(
            !empty($user = User::model()->findByPk($userId)) &&
            !empty($contact = $user->contact)
        ){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($contact->{$name}) &&
                    !empty($contact->{$name})
                ){
                    $res[$name] = $contact->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param int $tariff
     * @return bool
     * @soap
     */
    public function actionCheckTariff($tariffId = 1)
    {
        echo json_encode(User::checkTariff($tariffId));
    }

    /**
     * @return array
     * @soap
     */
    public function actionGetConstants(){
        $oClass = new ReflectionClass('User');
        echo json_encode($oClass->getConstants());
    }

    /**
     * @param array $attributes
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetUHOPropertiesByAttributes(array $attributes, array $properties){

        if(!empty($user = UserHasOrganizer::model()->findByAttributes($attributes))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($user->{$name}) &&
                    !empty($user->{$name})
                ){
                    $res[$name] = $user->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param array $attributes
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetUHOsPropertiesByAttributes(array $attributes, array $properties){
        if(!empty($users = UserHasOrganizer::model()->findAllByAttributes($attributes))){

            foreach($users as $user){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($user->{$name}) &&
                        !empty($user->{$name})
                    ){
                        $res[$name] = $user->{$name};
                    }
                }

                $properties[] = $res;
            }
        }

        echo json_encode($properties);
    }

    /**
     * @param array $attributes
     * @return bool
     * @soap
     */
    public function actionGetUHOExistsByAttributes(array $attributes){
        $res = FALSE;

        if(!empty(UserHasOrganizer::model()->findByAttributes($attributes))){
            $res = TRUE;
        }

        echo json_encode($res);
    }

    /**
     * @param integer $userId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetContactPropertiesByUser($userId, array $properties){
        $user = User::model()->findByPk($userId);

        if(empty($user) || empty($user->contact)){
            return $properties;
        };

        $contact = $user->contact;

        $res = [];

        foreach($properties as $k => $name){
            $res[$name] = NULL;
            if(
                isset($contact->{$name}) &&
                !empty($contact->{$name})
            ){
                $res[$name] = $contact->{$name};
            }
        }

        $properties = $res;


        return $properties;
    }

    /**
     * @param integer $userId
     * @return bool
     * @soap
     */
    public function actionGetUserExists($userId){

        $result = FALSE;

        $user = User::model()->findByPk($userId);
        if(!empty($user)){
            $result = TRUE;
        }

        echo json_encode($result);
    }

    /**
     * @param array $attributes
     * @param array $condition
     * @soap
     */
    public function actionCountContactByAttributes(array $attributes, array $condition = array()){
        echo json_encode(Contact::model()->countByAttributes($attributes, $condition));
    }


    /**
     * @param integer $userId
     * @param array $organizerIds
     * @param array $properties
     */
    public function actionGetCurrentUHOPropertiesByAttributes($userId, array $organizerIds, array $properties){

        foreach($organizerIds as $organizerId){

            if(!empty($user = UserHasOrganizer::model()->findByAttributes(array(
                'userId' => $userId,
                'organizerId' => $organizerId,
            )))){
                $res = [];

                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($user->{$name}) &&
                        !empty($user->{$name})
                    ){
                        $res[$name] = $user->{$name};
                    }
                }

                $properties = $res;
            }
        }

        echo json_encode($properties);
    }
}