<?php

class PriceAdminModule extends CWebModule
{
    public function init(){

        $this->setImport(array(
            'application.models.Product',
            'application.models.base.Product',
            'application.models.ProductCategory',
            'application.models.base.ProductCategory',
            'vendor.anggiaj.eselect2.ESelect2.*',
        ));
    }
}