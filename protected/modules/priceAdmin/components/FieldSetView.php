<?php
/**
 * File FieldSetView.php
 */
Yii::import('ext.widgets.RowView');

/**
 * Class FieldSetView
 */
class FieldSetView extends RowView {

	public $tagName = 'fieldset';
	public $widgetCss = 'widget-field-set-view';
	public $htmlOptions=array();

	public $header;
	public $headerTagName = 'legend';
	public $headerOptions=array();

	public function renderContent()
	{
		if ($this->header) {
			$this->renderHeader();
		}
		$this->renderItems();
	}

	public function renderHeader()
	{
		echo CHtml::openTag($this->headerTagName, $this->headerOptions)."\n";
		echo $this->header;
		echo CHtml::closeTag($this->headerTagName);
	}
}