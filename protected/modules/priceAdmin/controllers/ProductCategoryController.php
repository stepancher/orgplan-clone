<?php
Yii::import('application.models.ProductCategory');

class ProductCategoryController extends Controller{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->render('index', array(
            'model' => ProductCategory::model()
        ));
    }

    public function actionCreate(){

        $model = new ProductCategory();
        $trModel = new TrProductCategory();

        if(isset($_POST['ProductCategory'])){

            if($model->validate()){
                $model->save();

                $trModel->setAttribute('trParentId', $model->id);
                $trModel->setAttribute('langId', Yii::app()->language);
                $trModel->setAttribute('name', $_POST['ProductCategory']['name']);
                $trModel->setAttribute('description', $_POST['ProductCategory']['description']);

                if(!$trModel->save())
                    $trModel->getErrors();

                $this->redirect(array('index'));
            }else{
                $model->getErrors();
            }

        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * @param $id
     */
    public function actionUpdate($id)
    {
        $model = ProductCategory::model()->findByPk($id);
        $trModel = TrProductCategory::model()->findByAttributes(array('trParentId' => $id, 'langId' => Yii::app()->language));

        if(isset($_POST['ProductCategory'])){

            if($model->validate()){
                $model->save();

                $trModel->setAttribute('name', $_POST['ProductCategory']['name']);
                $trModel->setAttribute('description', $_POST['ProductCategory']['description']);

                if(!$trModel->save())
                    $trModel->getErrors();

                $this->redirect(array('index'));
            }else{
               $model->getErrors();
            }

        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }
    /**
     * @param $id
     * @throws CDbException
     */
    public function actionDelete($id){
        if(isset($id) && !empty($id) && is_numeric($id)){
            if (($model = ProductCategory::model()->findByPk($id)) !== null) {
                $model->delete();
            }
        }
    }

}