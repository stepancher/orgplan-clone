<?php

Yii::import('application.models.Product');

class ProductController extends Controller{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){

        $model = Product::model();

        $this->render('index', array('model'=> $model));
    }

    /**
     * @param $id
     * @throws CDbException
     */
    public function actionDelete($id){

        if(isset($id) && !empty($id) && is_numeric($id)){
            if (($model = Product::model()->findByPk($id)) !== null) {
                $model->delete();
            }
        }
    }

    public function actionCreate(){

        $model=new Product();

        if(isset($_POST['Product'])){

            $model=new Product();
            $trModel = new TrProduct();

            $model->attributes=$_POST['Product'];

            if($model->validate()){
                $model->save();

                $trModel->setAttribute('name', $_POST['Product']['name']);
                $trModel->setAttribute('trParentId', $model->id);
                $trModel->setAttribute('param', $_POST['Product']['param']);
                $trModel->setAttribute('langId', Yii::app()->language);

                if(!$trModel->save()){
                    $trModel->getErrors();
                }

                $this->redirect(array('index'));
            }else{
                $model->getErrors();
            }

        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * @param $id
     */
    public function actionUpdate($id)
    {
        $model = Product::model()->findByPk($id);
        $trModel = TrProduct::model()->findByAttributes(['trParentId' => $id, 'langId' => Yii::app()->language]);

        if(isset($_POST['Product'])){

            if($model->validate()){
                $model->save();

                $trModel->setAttribute('name', $_POST['ProductCategory']['name']);
                $trModel->setAttribute('description', $_POST['ProductCategory']['param']);

                if(!$trModel->save())
                    $trModel->getErrors();

                $this->redirect(array('index'));
            }else{
                $model->getErrors();
            }

        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }
}