<?php
class ProductPriceController extends Controller{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){

        $model = ProductPrice::model();

        $this->render('index', array('model'=> $model));
    }

    /**
     * @param $id
     * @throws CDbException
     */
    public function actionDelete($id){

        if(isset($id) && !empty($id) && is_numeric($id)){
            if (($model = ProductPrice::model()->findByPk($id)) !== null) {
                $model->delete();
            }
        }
    }

    public function actionCreate(){

        $model=new ProductPrice();

        if(isset($_POST['ProductPrice'])){

            $model=new ProductPrice();
            $model->attributes=$_POST['ProductPrice'];

            if($model->validate()){
                $model->save();

                $this->redirect(array('index'));
            }else{
                $model->getErrors();
            }

        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * @param $id
     */
    public function actionUpdate($id)
    {
        $model=ProductPrice::model()->findByPk($id);

        if(isset($_POST['ProductPrice'])){

            $model->attributes=$_POST['ProductPrice'];

            if($model->validate()){
                $model->update();

                $this->redirect(array('index'));
            }else{
                $model->getErrors();
            }

        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }
}