<?php
/**
 * This is the configuration for generating message translations
 * for the Yii framework. It is used by the 'yiic message' command.
 */
return array(
    'sourcePath'=>dirname(__FILE__),
    'messagePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'messages',
    'languages'=>array(
        'de',
        'ru',
        'en',
        // 'fi',
        // 'zh_cn',
        // 'zh_tw',
        // 'ca',
        // 'el',
        // 'es',
        // 'sv',
        // 'he',
        // 'nl',
        // 'pt',
        // 'pt_br',
        // 'it',
        // 'fr',
        // 'ja',
        // 'pl',
        // 'hu',
        // 'ro',
        // 'id',
        // 'vi',
        // 'bg',
        // 'lv',
        // 'sk',
        // 'uk',
        // 'ko_kr',
        // 'kk',
        // 'cs',
        // 'da',
    ),
    'fileTypes'=>array('php'),
    'overwrite'=>true,
    'exclude'=>array(
    ),
);