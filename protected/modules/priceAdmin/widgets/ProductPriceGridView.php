<?php
/**
 * File ProductGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');
Yii::import('application.models.Product');

/**
 * Class ProductPriceGridView
 *
 * @property ProductPrice $model
 */
class ProductPriceGridView extends TbGridView
{
    /**
     * Модель ProductPrice::model() or new ProductPrice('scenario')
     * @var ProductPrice	 */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'cityId' => array(
                'name' => 'cityId',
                'value' => '$data->getCityName()',
                'filter' => TbHtml::dropDownList(
                    'ProductPrice[cityId]',
                    $this->model->cityId,
                    ['' => '- все -'] + CHtml::listData($this->model->findAll(['with'=>['city'],'group'=>'city.id']), 'city.id', 'city.name')
                )
            ),
            'productId' => array(
                'name' => 'productId',
                'value' => '$data->getProductName()',
                'filter' => TbHtml::dropDownList(
                    'ProductPrice[productId]',
                    $this->model->productId,
                    ['' => '- все -'] + CHtml::listData(Product::model()->findAll(), 'id', 'name')
                )
            ),
            'cost' => array(
                'name' => 'cost',
            ),
        );
        return $columns;
    }
}