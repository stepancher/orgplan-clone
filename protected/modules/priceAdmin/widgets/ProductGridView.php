<?php
/**
 * File ProductGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');
Yii::import('application.models.ProductCategory');

/**
 * Class ProductGridView
 *
 * @property Product $model
 */
class ProductGridView extends TbGridView
{
    /**
     * Модель Product::model() or new Product('scenario')
     * @var Product	 */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'name' => array(
                'name' => 'name',
            ),
            'description' => array(
                'name' => 'description',
            ),
            'param' => array(
                'name' => 'param',
            ),
            'costType' => array(
                'name' => 'costType',
                'value' => 'Product::getCostTypeById($data->costType)',
                'filter' => TbHtml::dropDownList(
                    'Product[costType]',
                    $this->model->costType,
                    ['' => '- все -'] + Product::getCostTypes()
                )
            ),
            'categoryId' => array(
                'name' => 'categoryId',
                'value' => '$data->category->name',
                'filter' => TbHtml::dropDownList(
                    'Product[categoryId]',
                    $this->model->categoryId,
                    ['' => '- все -'] + CHtml::listData(ProductCategory::model()->findAll(), 'id', 'name')
                )
            ),
            'currency' => array(
                'name' => 'currency',
                'value' => 'Product::getCurrencyById($data->currency)',
                'filter' => TbHtml::dropDownList(
                    'Product[currency]',
                    $this->model->currency,
                    ['' => '- все -'] + Product::getCurrencies()
                )
            ),
        );
        return $columns;
    }
}