<?php
$ctrl = $this;
$ctrl->widget('application.modules.priceAdmin.components.FieldSetView', array(
    'header' => Yii::t('PriceAdminModule.priceAdmin', 'Категории продуктов'),
    'items' => array(
        array(
            'application.modules.priceAdmin.components.ActionsView',
            'items' => array(
                'products' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('PriceAdminModule.priceAdmin', 'Карточки продуктов'),
                    array(
                        'url' => $this->createUrl('product/index'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                        'style' => 'color:white !important;',
                    )
                ),
                'productsCreate' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('PriceAdminModule.priceAdmin', 'Создание карточки продукта'),
                    array(
                        'url' => $this->createUrl('product/create'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                        'style' => 'color:white !important;',
                    )
                ),
                'productPrices' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('PriceAdminModule.priceAdmin', 'Продукты'),
                    array(
                        'url' => $this->createUrl('productPrice/index'),
                        'color' => TbHtml::BUTTON_COLOR_INFO,
                        'style' => 'color:white !important;',
                    )
                ),
                'productPricesCreate' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('PriceAdminModule.priceAdmin', 'Создание продукта'),
                    array(
                        'url' => $this->createUrl('productPrice/create'),
                        'color' => TbHtml::BUTTON_COLOR_INFO,
                        'style' => 'color:white !important;',
                    )
                ),
                'categories' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('PriceAdminModule.priceAdmin', 'Категории'),
                    array(
                        'url' => $this->createUrl('productCategory/index'),
                        'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                        'style' => 'color:white !important;',
                        'class' => 'active',
                    )
                ),
                'categoryCreate' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t('PriceAdminModule.priceAdmin', 'Создание категории'),
                    array(
                        'url' => $this->createUrl('productCategory/create'),
                        'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                        'style' => 'color:white !important;',
                    )
                ),
            ),
        ),
        array(
            'priceAdmin.widgets.' . get_class($model) . 'GridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{update}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'update' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('update', array("id" => $data->id));
                            },
                            'options' => array('title' => 'редактировать', 'class' => 'btn btn-small btn-info'),
                        ),
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('delete', array("id" => $data->id));
                            },
                            'options' => array('title' => 'удалить', 'class' => 'btn btn-small btn-danger'),
                        ),
                    )
                )
            )
        ),
    )
));