<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */

Yii::import('application.models.ProductCategory');

?>

<?php
	echo TbHtml::linkButton('Назад', array(
		'class' => 'btn pull-right btn-warning',
		'style' => 'margin:10px 50px 0px 0px;',
		'url' => $this->createUrl('product/index'),
	));
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'Product-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description', array('style'=>'width:500px;height:140px;')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'param'); ?>
		<?php echo $form->textArea($model,'param', array('style'=>'width:500px;height:140px;')); ?>
		<?php echo $form->error($model,'param'); ?>
	</div>

	<?php

		echo TbHtml::activeLabel($model, 'costType');
		$this->widget('vendor.anggiaj.eselect2.ESelect2',array(
			'model'=>$model,
			'attribute'=> 'costType',
			'data' => $model::getCostTypes(),
			'htmlOptions'=>array(
				'style' => 'width:300px;',
			),
		));

		echo TbHtml::activeLabel($model, 'currency');
		$this->widget('vendor.anggiaj.eselect2.ESelect2',array(
			'model'=>$model,
			'attribute'=> 'currency',
			'data' =>$model::getCurrencies(),
			'htmlOptions'=>array(
				'style' => 'width:300px;',
			),
		));
		echo TbHtml::activeLabel($model, 'categoryId');
		$this->widget('vendor.anggiaj.eselect2.ESelect2',array(
			'model'=>$model,
			'attribute'=> 'categoryId',
			'data' => ProductCategory::getCategoriesList(),
			'options' => array(
				'placeholder'=>' -- Выберите категорию -- ', // add this line
			),
			'htmlOptions'=>array(
				'style' => 'width:300px;',
			),
		));
	?>

	<div style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
