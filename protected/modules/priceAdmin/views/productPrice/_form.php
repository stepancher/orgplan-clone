<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */

Yii::import('application.models.Product');

?>

<?php
	echo TbHtml::linkButton('Назад', array(
		'class' => 'btn pull-right btn-warning',
		'style' => 'margin:10px 50px 0px 0px;',
		'url' => $this->createUrl('product/index'),
	));
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ProductPrice-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<?php

	echo TbHtml::activeLabel($model, 'productId');
	$this->widget('vendor.anggiaj.eselect2.ESelect2',array(
		'model'=>$model,
		'attribute'=> 'productId',
		'data' => CHtml::listData(Product::model()->findAll(), 'id', 'name'),
		'options' => array(
			'placeholder'=>' -- Выберите продукт -- ', // add this line
		),
		'htmlOptions'=>array(
			'style' => 'width:300px;',
		),
	));

	echo TbHtml::activeLabel($model, 'cityId');
	$this->widget('vendor.anggiaj.eselect2.ESelect2',array(
		'model'=>$model,
		'attribute'=> 'cityId',
		'data' =>CHtml::listData(
			City::model()->findAll(array('order'=>'id')),
			'id',
			function($el){
				return isset($el->translate->name) ? $el->translate->name : '';
			}
		),
		'options' => array(
			'placeholder'=>' -- Выберите город -- ', // add this line
		),
		'htmlOptions'=>array(
			'style' => 'width:300px;',
		),
	));
	?>

	<div style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
