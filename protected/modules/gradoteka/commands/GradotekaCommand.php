<?php
Yii::import('app.modules.gradoteka.models.*');

class GradotekaCommand extends CConsoleCommand{

//    protected $access_token = 'Refgs13ab0000001';
//    protected $access_token = 'cubtok1498UkoPyz';

    protected $httpOptions = array(
        'http' => array(                // use key 'http' even if you send the request to https://...
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
        )
    );

    const URL = 'http://api.gradoteka.ru/v2/object/access?access-token=cubtok1498UkoPyz';
    const OBJECTS_URL = 'http://api.gradoteka.ru/v2/object/index?access-token=cubtok1498UkoPyz';
    const STATS_URL = 'http://api.gradoteka.ru/v2/stat-type/index?access-token=cubtok1498UkoPyz';
    const VALUES_URL = 'http://api.gradoteka.ru/v2/stats/index?access-token=cubtok1498UkoPyz';
    const FIRST_YEAR = 2014;
    const AUTO_UPDATE_ON = 1;
    const AUTO_UPDATE_OFF = 0;

    const TYPE_YEAR = 'YEAR';
    const TYPE_HALF_YEAR = 'HALF_YEAR';
    const TYPE_QUARTET = 'QUARTET';
    const TYPE_QUARTET_INC = 'QUARTET_INC';
    const TYPE_MONTH = 'MONTH';
    const TYPE_MONTH_INC = 'MONTH_INC';
    const TYPE_DAY = 'DAY';
    const TYPE_WEEK = 'WEEK';

    const OBJECT_LIMIT = 1000;
    const STATS_LIMIT = 500;

    public $pregPatterns = [
        [
            'type' => self::TYPE_YEAR,
            'pattern' => '/^\d{4}$/',
        ],
        [
            'type' => self::TYPE_HALF_YEAR,
            'pattern' => '/^\d{4}\/[12]$/',
        ],
        [
            'type' => self::TYPE_QUARTET,
            'pattern' => '/^\d{4}\.[1-4]$/',
        ],
        [
            'type' => self::TYPE_QUARTET_INC,
            'pattern' => '/^\d{4}\:[1-4]$/',
        ],
        [
            'type' => self::TYPE_MONTH,
            'pattern' => '/^\d{4}\.(0[1-9]|1[0-2])$/',
        ],
        [
            'type' => self::TYPE_MONTH_INC,
            'pattern' => '/^\d{4}\:(0[1-9]|1[0-2])$/',
        ],
        [
            'type' => self::TYPE_WEEK,
            'pattern' => '/^\d{4}w(0[1-9]|1[0-2])\.(0[1-9]|1[0-9]|2[0-9]|3[01])$/',
        ],
        [
            'type' => self::TYPE_DAY,
            'pattern' => '/^\d{4}\.(0[1-9]|1[0-2])\.(0[1-9]|1[0-9]|2[0-9]|3[01])$/',
        ],
    ];

    public $percentVariates = array(
        1 => '%',
        2 => 'процент',
        3 => 'Процент',
    );

    /**
     * @param null $objectId    gId объекта
     * @param null $statId      gId стата
     * @param null $name        regexp фильтр по полю name
     * @param null $parent_name regexp фильтр по полю parent_name
     * @param int $limit        ограничение количества запрошенных показателей
     * @param int $raw          Флаг. Если 1, вывести сырые данные.
     * @param int $export       Флаг для Эскспорта. Если 1, колонки будут табулированы, удобно копировать в Excel. 0 - для просмотра.
     */
    public function actionPullStat(
        $objectId = NULL,
        $statId = NULL,
        $name = NULL,
        $parent_name = NULL,
        $limit = self::STATS_LIMIT,
        $raw = 0,
        $export = 0
    ) {

        if (isset($objectId) && isset($statId)){

            $data = array(
                "type"=> "$statId",
                "codes"=> ["$objectId",],
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            $context  = stream_context_create($options);
            $result = file_get_contents(self::VALUES_URL, false, $context);

            $stat = json_decode($result);

            var_dump($stat);

        }
//        elseif(isset($statId)){
//
//            echo "Searching...\n";
//            $i = 0;
//            do{
//                $data = array(
//                    "limit" => $limit,
//                    "offset" => $i*$limit,
//                );
//
//                $this->httpOptions['http']['content'] = http_build_query($data);
//                $context  = stream_context_create($this->httpOptions);
//                $result = file_get_contents(self::STATS_URL, false, $context);
//                $stats = json_decode($result);
//
//                $stats = array_filter(
//                    $stats->data,
//                    function($data) use($statId){
//                        if($data->_id !== "$statId")
//                            return FALSE;
//                        return TRUE;
//                    }
//                );
//
//                $i++;
//                $howMany = round(100*$i*$limit/170000, 4);
//
//                echo "\rViewed $howMany% stats...";
//
//            } while (empty($stats) && $howMany <= 100);
//
//            if(!empty($stats)){
//
//                if($raw == 1){
//                    var_dump($stats); exit;
//                }
//
//                $stats = array_values($stats);
//
//                $gId = $stats[0]->_id;
//                $gParentName = $stats[0]->parent_name;
//                $gName = $stats[0]->name;
//                $gUnit = $stats[0]->unit;
//
//                echo "\tgId = \"$gId\"\n";
//                echo "\tgName = \"$gName\"\n";
//                echo "\tgParentName = \"$gParentName\"\n";
//                echo "\tgUnit = \"$gUnit\"\n";
//                echo "............................................................................\n";
//            } else {
//
//                echo "Gradoteka haven't this stat!\n";
//            }
//
//        }
        elseif(isset($name) || isset($parent_name) || isset($statId)){

            $data = array(
                "limit" => $limit,
                "name" => $name,
                "parent_name" => $parent_name,
                "_id" => $statId,
            );

            $this->httpOptions['http']['content'] = http_build_query($data);

            $context  = stream_context_create($this->httpOptions);
            $result = file_get_contents(self::STATS_URL, false, $context);
            $stats = json_decode($result);

            if($raw == 1){
                var_dump($stats);exit;
            }

            foreach ($stats->data as $objectOne){

                if($export == 1){
                    echo "id=\"$objectOne->_id\"\t";
                    echo "name=\"$objectOne->name\"\t";
                    echo "parentName=\"$objectOne->parent_name\"\t";
                    echo "unit=\"$objectOne->unit\"\n";
                } else {
                    echo "\tgId = \"$objectOne->_id\n";
                    echo "\tgName = \"$objectOne->name\"\n";
                    echo "\tgParentName = \"$objectOne->parent_name\"\n";
                    echo "\tgUnit = \"$objectOne->unit\"\n";
                    echo "............................................................................\n";
                }
            }
        } else{

            echo "Pulling all objects...\n";

            $gObjects = $this->pullAvailableObjects();

            foreach ($gObjects as $object){

                $data = array(
                    "type" => "$object",
                    "limit" => $limit,
                );

                $this->httpOptions['http']['content'] = http_build_query($data);

                $context = stream_context_create($this->httpOptions);
                $result = file_get_contents(self::OBJECTS_URL, false, $context);

                $objects = json_decode($result);

                foreach ($objects->data as $objectOne){
                    echo "\ttype=\"$objectOne->type\"\n\t";
                    echo "code=\"$objectOne->code\"\n\t";
                    echo "name=\"$objectOne->name\"\n\t";
                    echo "display_name=\"$objectOne->display_name\"\n";
                    echo "...................................................................................\n";
                }
            }
        }
    }

    public function actionUpdate(){

        $gObjectTypes = $this->pullAvailableObjects();

        foreach ($gObjectTypes as $objectType){

            /**
             * @var string  $object
             */
            $this->pullObjects($objectType);
        }

        $this->pullObjectStatValues();

        $this->calcDerivatives();
    }

    /**
     * @return array
     */
    public function pullAvailableObjects(){

        echo "\nPulling all available objects...\n";

        $gObjects = array();

        $context  = stream_context_create($this->httpOptions);
        $result = file_get_contents(self::URL, false, $context);

        $objects = json_decode($result);

        if($objects->error !== 0){
            echo "Available objects received with errors! Command interrupted.\n";
            exit;
        } else {
            foreach ($objects->data as $object){
                $gObjects[] = $object;
            }
        }
        echo "Done.\n";
        return $gObjects;
    }

    /**
     * @param   string  $objectType
     */
    public function pullObjects($objectType){

        echo "\nPulling $objectType objects...\n";

        $data = array(
            "type" => $objectType,
            "limit" => self::OBJECT_LIMIT,
        );

        $this->httpOptions['http']['content'] = http_build_query($data);

        $context = stream_context_create($this->httpOptions);
        $result = file_get_contents(self::OBJECTS_URL, false, $context);
        $objects = json_decode($result);

        if ($objects->error != 0) {
            echo "Objects $objectType received with errors! Command interrupted.\n";
            exit;
        } else {

            foreach ($objects->data as $object) {

                $gObject = GObjects::model()->findByAttributes(array(
                    'gId' => (string)$object->code,
                    'gType' => $object->type
                ));

                if(empty($object->display_name)){
                    $objectName = $object->name;
                }else {
                    $objectName = $object->display_name;
                }

//                if ($gObject !== NULL && $gObject['name'] != $objectName) { // Updating Gradoteka object name in db
//
//                    $prevName = $gObject['name'];
//                    $gObject->setAttribute('name', $objectName);
//
//                    if (!$gObject->save()) {
//
//                        echo "gObject \"$objectName\" didn't update in GObjects!\n";
//                        echo "\n";
//
//                    } else {
//
//                        echo "gObject name = \"$prevName\" replaced to \"$objectName\" in GObjects!\n";
//                        echo "\n";
//
//                    }
//
//                } elseif ($gObject === NULL) { // Adding Gradoteka object to db
                if ($gObject === NULL) { // Adding Gradoteka object to db

                    if ($objectType == 'state') {

                        $gObject = new GObjects;
                        $gObject->attributes = array(
                            'gType' => $objectType,
                            'name' => $objectName,
                            'gId' => (string)$object->code,
                        );

                        if (!$gObject->save()) {
                            echo "GObject $objectType doesn't save in GObjects!\n";
                        }

                    } elseif ($objectType == 'region') {


                        $gObject = new GObjects;
                        $gObject->attributes = array(
                            'gType' => $objectType,
                            'parentType' => 'state',
                            'name' => $objectName,
                            'gId' => (string)$object->code,
                        );

                        $trRegion = TrRegion::model()->findByAttributes(
                            [
                                'langId' => Yii::app()->language,
                                'name' => $objectName
                            ]
                        );

                        if($trRegion === NULL){
                            echo "Added new GObject \"$objectName\" region. Please, add row to GObjectHasRegion.\n";
                        }
                        $gObject->setAttribute(
                            'parentId',
                            isset($trRegion->trParent) ? $trRegion->trParent->districtId : NULL
                        );

                        if (!$gObject->save()) {
                            echo "GObject $objectType doesn't save in GObjects!\n";
                        } else {

                            $OHR = new GObjectHasRegion;
                            $OHR->attributes = array(
                                'objId' => $gObject->id,
                                'regionId' => isset($trRegion->trParent) ? $trRegion->trParent->districtId : NULL,
                            );

                            if (!$OHR->save()) {
                                echo "There is new region object in gradoteka. Object added to GObjects, but didn't add to GObjectHasRegion!";
                            }
                        }

                    } elseif($objectType == 'country'){

                        $gObject = new GObjects;
                        $gObject->attributes = array(
                            'gType' => $objectType,
                            'name' => $object->name,
                            'gId' => (string)$object->code,
                        );

                        if (!$gObject->save()) {
                            echo "GObject $objectType doesn't save in GObjects!\n";
                        }
                    } else {
                        echo "Gradoteka added new $objectType objectType witch doesn't process Gradoteka command.";
                    }
                }
            }
            echo "Done.\n";
        }
    }

    public function actionPullAvailableStats(){

        $i = 0;

        for($i = 0; $i <= 170000; $i = $i + 1000) {

            echo "\nPulling stats $i...\n";

            $data = array(
                "limit" => 1000,
                "offset" => $i,
            );

            $this->httpOptions['http']['content'] = http_build_query($data);

            $context = stream_context_create($this->httpOptions);
            $result = file_get_contents(self::STATS_URL, false, $context);
            $stats = json_decode($result);

            if ($stats->error != 0) {
                echo "Stats received with errors! Command interrupted.\n";
                exit;
            } else {

                NewGStatTypes::model()->deleteAll();

                foreach ($stats->data as $stat) {

                    $newGStatType = NewGStatTypes::model()->findByAttributes(['gId' => $stat->_id]);
                    if ($newGStatType === NULL) {

                        $newGStatType = new NewGStatTypes;
                        $newGStatType->attributes = array(
                            'gId' => $stat->_id,
                            'name' => $stat->name,
                            'parentName' => $stat->parent_name,
                            'type' => $stat->type,
                            'description' => $stat->description,
                            'unit' => $stat->unit,
                        );
                    }

                    if (!$newGStatType->save()) {

                        echo "Don't save!\n";
                        exit;
                    } else {
                        echo "Save!";
                    }

                }
            }
        }
    }

    public function pullObjectStatValues(){

$n = 1;
        $gStatTypes = GStatTypes::model()->findAllByAttributes(
            [
                'type' => 'stat-type',
                'autoUpdate' => self::AUTO_UPDATE_ON,
            ]
        );

        foreach ($gStatTypes as $gStatType){

            echo "#$n Pulling stat \"$gStatType->gId\"...\n";
$n++;
            $gObjects = GObjects::model()->findAll();

            //создаем набор дескрипторов cURL
            $mh = curl_multi_init();
            $ch = array();
            $gObjectGid = array();

            foreach ($gObjects as $k => $gObject){

                $ch[$k] = curl_init();

                if($gObject->gType == 'state'){
                    $gObjectGType = sprintf("%'.02d", $gObject->gId);
                } elseif ($gObject->gType == 'region'){
                    $gObjectGType = sprintf("%'.08d", $gObject->gId);
                } elseif ($gObject->gType == 'country'){
                    $gObjectGType = $gObject->gId;
                } else{
                    $gObjectGType = NULL;
                }

                $data[$k] = array(
                    "type" => "$gStatType->gId",
                    "codes" => [$gObjectGType],
                );

                curl_setopt($ch[$k], CURLOPT_URL, self::VALUES_URL);
                curl_setopt($ch[$k], CURLOPT_POST, TRUE);
                curl_setopt($ch[$k], CURLOPT_HEADER, 0);
                curl_setopt($ch[$k], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch[$k], CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($ch[$k], CURLOPT_POSTFIELDS, http_build_query($data[$k]));

                //добавляем два дескриптора
                curl_multi_add_handle($mh, $ch[$k]);

                $gObjectGid[$k] = $gObjectGType;
            }

            do { curl_multi_exec($mh, $active); } while ($active);

            foreach ($ch as $k => $channel){

                $objectStats = json_decode(curl_multi_getcontent($channel));

                if($objectStats === NULL)
                    continue;

                if($objectStats->error != 0){
                    echo "Response error code = $objectStats->error. Please check access to stat gId = $gStatType->gId and API availability!\n";
                    continue 2;
                }

                if(empty($objectStats->data)){
//                echo "There is no stat \"$gStatType->gId\" for \"$gObjectGid[$k]\" object!\n";
                    continue;
                }

                $objectGid = $objectStats->data[0]->code;

                $objectDataValues = array_filter($objectStats->data[0]->values,
                    function($item){

                        $itemEpoch = substr($item->epoch, 0, 4);

                        if($itemEpoch < self::FIRST_YEAR - 1){
                            return false;
                        }
                        return true;
                    }
                );

                if(empty($objectDataValues))
                    continue;

                $gObject = GObjects::model()->findByAttributes(['gId' => $objectGid]);
                $GObjectsHasGStatTypes = GObjectsHasGStatTypes::model()->findByAttributes(
                    [
                        'objId' => $gObject->id,
                        'statId' => $gStatType->gId
                    ]
                );

                if($GObjectsHasGStatTypes === NULL){

                    $GObjectsHasGStatTypes = $this->saveGObjectHasGStatType($gObject->id, $gStatType->gId, $gObject->gType);
                }

                $this->prepareGValues($objectDataValues, $GObjectsHasGStatTypes->id, $gStatType->unit);

                curl_multi_remove_handle($mh, $channel);
                curl_close($channel);

            }
            curl_multi_close($mh);

            $gStatType = GStatTypes::model()->findByAttributes(['id' => $gStatType->id]);
            $gStatType->setAttribute('autoUpdate', self::AUTO_UPDATE_OFF);
            if(!$gStatType->save()){
                echo "AutoUpdate=0 for gStatType $gStatType->id didn't save!\n";
                var_dump($gStatType->getErrors()); echo "\n";
            }
        }
    }

    /**
     * @param $objectDataValues
     * @param $GObjectsHasGStatTypesId
     * @param $unit
     */
    public function prepareGValues($objectDataValues, $GObjectsHasGStatTypesId, $unit){

        $patternType = array();
        $dataValues = array();

        foreach ($this->pregPatterns as $pattern){

            $dataValues = array_filter(
                $objectDataValues,
                function($objectValue) use($pattern){

                    if(preg_match($pattern['pattern'], $objectValue->epoch))
                        return true;
                    return false;

                }
            );

            if(!empty($dataValues)){
                $patternType[] = $pattern['type'];
                $patternType[] = $pattern['pattern'];
                break;
            }

        }

        if(!empty($dataValues)){

            if($patternType[0] == self::TYPE_YEAR){

                $dataValues = array_filter(
                    $dataValues,
                    function ($dataValue) use ($GObjectsHasGStatTypesId) {

                        $gValue = GValue::model()->findByAttributes(
                            array(
                                'setId' => $GObjectsHasGStatTypesId,
                                'epoch' => $dataValue->epoch,
                            )
                        );

                        if($gValue === NULL || $gValue->value != $dataValue->value)
                            return TRUE;
                        return FALSE;
                    }
                );

                if(!empty($dataValues)){

                    GValue::model()->deleteAllByAttributes(['setId' => $GObjectsHasGStatTypesId]);

                    foreach ($dataValues as $dataValue){
                        $dataValue = (array)$dataValue;
                        $this->saveGValues($dataValue, $GObjectsHasGStatTypesId);
                    }
                }

            } else{

                $num = array();
                $values = array_reduce(
                    $dataValues,
                    function ($result, $item) use($patternType, &$num){

                        $itemEpoch = substr($item->epoch, 0, 4);
                        if(!isset($result[$itemEpoch])){
                            $result[$itemEpoch] = NULL;
                        }

                        $result[$itemEpoch] += $item->value;

                        if(!isset($num[$itemEpoch])){
                            $num[$itemEpoch] = 0;
                        }
                        $num[$itemEpoch] += 1;

                        return $result;
                    },
                    array()
                );

                if(array_search($unit, $this->percentVariates)){
                    foreach ($values as $k => &$value){

                        $value = $value/$num[$k];
                    }
                }

                $dataValues = array_map(
                    function($k, $v){
                        return [
                            'epoch' => $k,
                            'value' => $v,
                        ];
                    },
                    array_keys($values),
                    array_values($values)
                );

                $dataValues = array_filter(
                    $dataValues,
                    function ($dataValue) use ($GObjectsHasGStatTypesId) {

                        $gValue = GValue::model()->findByAttributes(
                            array(
                                'setId' => $GObjectsHasGStatTypesId,
                                'epoch' => $dataValue['epoch'],
                            )
                        );

                        if($gValue === NULL || $gValue->value != $dataValue['value'])
                            return TRUE;
                        return FALSE;
                    }
                );

                if(!empty($dataValues)){

                    GValue::model()->deleteAllByAttributes(['setId' => $GObjectsHasGStatTypesId]);

                    foreach ($dataValues as $dataValue){
                        $this->saveGValues($dataValue, $GObjectsHasGStatTypesId);
                    }
                }
            }
        }
    }

    /**
     * @param $objectStat
     * @param $GObjectsHasGStatTypesId
     */
    public function saveGValues($objectStat, $GObjectsHasGStatTypesId){

            $gValue = new GValue;
            $gValue->attributes = array(
                'setId' => $GObjectsHasGStatTypesId,
                'epoch' => $objectStat['epoch'],
                'value' => $objectStat['value'],
            );

            $showEpoch = $objectStat['epoch'];
            if(!$gValue->save()){
                echo "GValue setId = $GObjectsHasGStatTypesId and epoch=$showEpoch didn't save!\n";
                var_dump($gValue->getErrors()); echo "\n";
            }
    }

    /**
     * Расчет дополнительных показателей из основных. Список и правила в переменной $additionalStats
     */
    public function calcDerivatives()
    {

    echo "Saving additional stats...\n";

        $gStatTypes = GStatTypes::model()->findAllByAttributes(
            [
                'type' => 'derivative',
                'autoUpdate' => self::AUTO_UPDATE_ON,
            ]
        );

    foreach ($gStatTypes as $derivativeGStatType) {

        foreach (GStatTypeHasFormula::model()->findAllByAttributes(['gId' => $derivativeGStatType->gId]) as $statRow) {
            echo "formula id=$statRow->id...\n";
            $derivatives = array();

            $calcStats = GStatTypeFormulaHasStat::model()->findAllByAttributes(['formulaId' => $statRow->id]);

            foreach ($calcStats as $calcStat) {

                $gObjects = GObjects::model()->findAll();

                //создаем набор дескрипторов cURL
                $mh = curl_multi_init();
                $ch = array();
                $gObjectGid = array();

                foreach ($gObjects as $k => $gObject){

                    $ch[$k] = curl_init();

                    if($gObject->gType == 'state'){
                        $gObjectGType = sprintf("%'.02d", $gObject->gId);
                    } elseif ($gObject->gType == 'region'){
                        $gObjectGType = sprintf("%'.08d", $gObject->gId);
                    } elseif ($gObject->gType == 'country'){
                        $gObjectGType = $gObject->gId;
                    } else{
                        $gObjectGType = NULL;
                    }

                    $data[$k] = array(
                        "type" => "$calcStat->statId",
                        "codes" => [$gObjectGType],
                    );

                    curl_setopt($ch[$k], CURLOPT_URL, self::VALUES_URL);
                    curl_setopt($ch[$k], CURLOPT_POST, TRUE);
                    curl_setopt($ch[$k], CURLOPT_HEADER, 0);
                    curl_setopt($ch[$k], CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch[$k], CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($ch[$k], CURLOPT_POSTFIELDS, http_build_query($data[$k]));

                    //добавляем два дескриптора
                    curl_multi_add_handle($mh, $ch[$k]);

                    $gObjectGid[$k] = $gObjectGType;
                }

                do {
                    curl_multi_exec($mh, $active);
                } while ($active);

                foreach ($ch as $ch_key => $channel) {

                    $objectStats = json_decode(curl_multi_getcontent($channel));

                    if ($objectStats === NULL)
                        continue;

                    if ($objectStats->error != 0) {
                        echo "Response error code = $objectStats->error. Please check access to stat gId = \"$calcStat->statId\" and API availability!\n";
                        continue 2;
                    }

                    if (empty($objectStats->data)) {
//                echo "There is no stat \"$gStatType->gId\" for \"$gObjectGid[$ch_key]\" object!\n";
                        continue;
                    }

                    $objectGid = $objectStats->data[0]->code;

                    $objectDataValues = array_filter(
                        $objectStats->data[0]->values,
                        function ($item) {

                            $itemEpoch = substr($item->epoch, 0, 4);

                            if ($itemEpoch < self::FIRST_YEAR - 1) {
                                return false;
                            }
                            return true;
                        }
                    );

                    if (empty($objectDataValues)) {
                        continue;
                    }

                    $patternType = array();
                    $dataValues = array();
                    foreach ($this->pregPatterns as $pattern) {

                        $dataValues = array_filter(
                            $objectDataValues,
                            function ($objectValue) use ($pattern) {

                                if (preg_match($pattern['pattern'], $objectValue->epoch))
                                    return true;
                                return false;

                            }
                        );

                        if (!empty($dataValues)) {
                            $patternType[] = $pattern['type'];
                            $patternType[] = $pattern['pattern'];
                            break;
                        }

                    }

                    if (!empty($dataValues)) {

                        if ($patternType[0] == self::TYPE_YEAR) {

                            foreach ($dataValues as $dataValue) {

                                $dataValue = (array)$dataValue;
                                $derivatives[$objectGid][$dataValue['epoch']][$calcStat->key] = $dataValue['value'];
                            }

                        } else {

                            $values = array_reduce(
                                $dataValues,
                                function ($result, $item) use ($patternType) {

                                    $itemEpoch = substr($item->epoch, 0, 4);
                                    if (!isset($result[$itemEpoch])) {
                                        $result[$itemEpoch] = NULL;
                                    }

                                    $result[$itemEpoch] += $item->value;
                                    return $result;
                                },
                                array()
                            );

                            $dataValues = array_map(
                                function ($k, $v) {
                                    return [
                                        'epoch' => $k,
                                        'value' => $v,
                                    ];
                                },
                                array_keys($values),
                                array_values($values)
                            );

                            foreach ($dataValues as $dataValue) {

                                $derivatives[$objectGid][$dataValue['epoch']][$calcStat->key] = $dataValue['value'];
                            }
                        }
                    }

                    curl_multi_remove_handle($mh, $channel);
                    curl_close($channel);

                }

                curl_multi_close($mh);
            }

            foreach ($derivatives as $objectGid => $derivative) {

                foreach ($derivative as $epoch => $derivativeStats) {

                    if ($statRow->wholeFlag && count($calcStats) != count($derivativeStats)) {
                        continue 1;
                    }

                    if ($statRow->formulaType == 'custom') {

                        $formula = $statRow->formula;

                        foreach ($derivativeStats as $statKey => $statValue) {

                            $formula = str_replace("[$statKey]", $statValue, $formula);
                        }

                        $formula = "return $formula;";
                        $value = eval($formula);
                        
                    } elseif ($statRow->formulaType == 'aggregatable' && $statRow->formula == 'SUM') {

                        $value = array_sum($derivativeStats);
                    }

                    $gObject = GObjects::model()->findByAttributes(['gId' => (string)$objectGid]);

                    if ($gObject === NULL) {
                        echo "GObject gId=$objectGid don't exist in GObjects table!\n";
                        continue 2;
                    }

                    $gObjectHasGStatType = GObjectsHasGStatTypes::model()->findByAttributes(
                        array(
                            'objId' => $gObject->id,
                            'statId' => $statRow->gId,
                        )
                    );

                    if ($gObjectHasGStatType === NULL) {

                        $gObjectHasGStatType = $this->saveGObjectHasGStatType($gObject->id, $statRow->gId, $gObject->gType);

                    }

                    $gValue = GValue::model()->findByAttributes(
                        [
                            'setId' => $gObjectHasGStatType->id,
                            'epoch' => $epoch,
                        ]
                    );
                    if ($gValue === NULL) {

                        $this->saveGValues(
                            array(
                                'epoch' => $epoch,
                                'value' => $value,
                            ),
                            $gObjectHasGStatType->id
                        );
                    } elseif ($gValue->value != $value) {

                        $gValue->setAttribute('value', $value);
                        $gValue->save();
                    }
                }

            }
        }

            $gStatType = GStatTypes::model()->findByAttributes(['id' => $derivativeGStatType->id]);
            $gStatType->setAttribute('autoUpdate', self::AUTO_UPDATE_OFF);
            if(!$gStatType->save()){
                echo "AutoUpdate=0 for gStatType $gStatType->id didn't save!\n";
                var_dump($gStatType->getErrors()); echo "\n";
            }
        }
        echo "Done.\n";
    }

    /**
     * @param $gObjectId
     * @param $statId
     * @param $gObjectType
     * @return GObjectsHasGStatTypes
     */
    public function saveGObjectHasGStatType($gObjectId, $statId, $gObjectType){

        $gObjectHasGStatType = new GObjectsHasGStatTypes;

        $gObjectHasGStatType->setAttribute('objId', $gObjectId);
        $gObjectHasGStatType->setAttribute('statId', $statId);
        $gObjectHasGStatType->setAttribute('gType', $gObjectType);

        if(!$gObjectHasGStatType->save()){

            echo "GObjectsHasGStatTypes objId = $gObjectId statId=$statId didn't save!\n";
            var_dump($gObjectHasGStatType->getErrors()); echo "\n";
        }

        return $gObjectHasGStatType;
    }
}

