<?php

class GradotekaModule extends CWebModule
{
    public function init(){

        $this->setImport(array(
            'gradoteka.components.*',
            'gradoteka.models.*',
//            'gradoteka.models.base.*',
            'vendor.anggiaj.eselect2.ESelect2.*',
            'gradoteka.widgets.*'
        ));
    }
}