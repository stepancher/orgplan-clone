<?php

class m150814_110220_create_gradoteka_tables extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateGObjects();
		$sql .= $this->getCreateGStatTypes();
		$sql .= $this->getCreateGObjectsHasGStatTypes();
		$sql .= $this->getCreateGValue();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{gvalue}};
			DROP TABLE {{gobjectshasgstattypes}};
			DROP TABLE {{gobjects}};
			DROP TABLE {{gstattypes}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/

	public function getCreateGObjects(){
		return '
			CREATE TABLE {{gobjects}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `gType` varchar(45) DEFAULT NULL,
			  `parentType` varchar(45) DEFAULT NULL,
			  `parentId` int(11) DEFAULT NULL,
			  `name` varchar(100) DEFAULT NULL,
			  `gId` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `index2` (`gId`,`name`)
			) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
		';
	}

	public function getCreateGStatTypes(){
		return '
			CREATE TABLE {{gstattypes}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `gId` int(11) DEFAULT NULL,
			  `gType` varchar(45) DEFAULT NULL,
			  `name` varchar(150) DEFAULT NULL,
			  `description` varchar(2000) DEFAULT NULL,
			  `units` varchar(45) DEFAULT NULL,
			  `epochType` varchar(45) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `g_id_UNIQUE` (`gId`)
			) ENGINE=InnoDB AUTO_INCREMENT=2557 DEFAULT CHARSET=utf8;
		';
	}

	public function getCreateGObjectsHasGStatTypes(){
		return '
			CREATE TABLE {{gobjectshasgstattypes}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `objId` int(11) NOT NULL,
			  `statId` int(11) NOT NULL,
			  `groupId` int(11) DEFAULT NULL,
			  `groupName` varchar(100) DEFAULT NULL,
			  `frequency` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `objId_UNIQUE` (`objId`,`statId`),
			  KEY `fk_tbl_gobjectshasgstattypes_2_idx` (`statId`),
			  CONSTRAINT `fk_tbl_gobjectshasgstattypes_1` FOREIGN KEY (`objId`) REFERENCES `tbl_gobjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
			  CONSTRAINT `fk_tbl_gobjectshasgstattypes_2` FOREIGN KEY (`statId`) REFERENCES `tbl_gstattypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

		';
	}

	public function getCreateGValue(){
		return '
			CREATE TABLE {{gvalue}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `setId` int(11) DEFAULT NULL,
			  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  `value` varchar(45) DEFAULT NULL,
			  `epoch` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `fk_tbl_gvalue_1_idx` (`setId`,`epoch`),
			  CONSTRAINT `fk_tbl_gvalue_1` FOREIGN KEY (`setId`) REFERENCES `tbl_gobjectshasgstattypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=938 DEFAULT CHARSET=utf8;
		';
	}
}