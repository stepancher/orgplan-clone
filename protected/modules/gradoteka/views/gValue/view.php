<?php
$ctrl = $this;

$ctrl->widget('application.modules.gradoteka.components.FieldSetView', array(
    'header' => Yii::t($this->getId(), 'Данные по объекту'),
    'items' => array(
        array(
            'application.modules.gradoteka.components.ActionsView',
            'items' => array(
                'add' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Добавить набор'),
                    array(
                        'url' => $this->createUrl('GObjectsHasGStatTypes/create'),
                        'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                        'style' => 'color:white !important;',
                    )
                ),
                'updateValues' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Обновить данные'),
                    array(
                        'url' => $this->createUrl('GObjectsHasGStatTypes/collectValues'),
                        'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                        'style' => 'color:white !important;',
                    )
                ),
                'updateRegions' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Обновить объекты'),
                    array(
                        'url' => $this->createUrl('GObjectsHasGStatTypes/pullObjects'),
                        'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                        'style' => 'color:white !important;',
                    )
                ),
                'updateStats' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Обновить показатели'),
                    array(
                        'url' => $this->createUrl('GObjectsHasGStatTypes/pullStats'),
                        'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                        'style' => 'color:white !important;',
                    )
                ),
                'viewStat' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Все показатели'),
                    array(
                        'url' => $this->createUrl('GStatTypes/index'),
                        'color' => TbHtml::BUTTON_COLOR_INFO,
                        'style' => 'color:white !important;',
                    )
                ),
                'viewObjects' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Все объекты'),
                    array(
                        'url' => $this->createUrl('GObjects/index'),
                        'color' => TbHtml::BUTTON_COLOR_INFO,
                        'style' => 'color:white !important;',
                    )
                ),
                'viewValues' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Все значения'),
                    array(
                        'url' => $this->createUrl('GValue/index'),
                        'color' => TbHtml::BUTTON_COLOR_INFO,
                        'style' => 'color:white !important;',
                    )
                ),
                'back' => array(
                    TbHtml::BUTTON_TYPE_LINK,
                    Yii::t($this->getId(), 'Все наборы'),
                    array(
                        'url' => $this->createUrl('GObjectsHasGStatTypes/index'),
                        'color' => TbHtml::BUTTON_COLOR_WARNING,
                        'style' => 'color:white !important;',
                        'class' => 'pull-right',
                    )
                ),
            ),
        ),
        array(
            'gradoteka.widgets.'.get_class($model).'GridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{update}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('delete', array("id" => $data->id));
                            },
                            'options' => array('title' => 'удалить', 'class' => 'btn btn-small btn-danger'),
                        ),
                        'update' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('update', array("id" => $data->id));
                            },
                            'options' => array('title' => 'редактировать', 'class' => 'btn btn-small btn-info'),
                        ),
                    )
                )
            )
        ),
    )
));
?>