<?php
/* @var $this GvalueController */
/* @var $model Gvalue */
/* @var $form CActiveForm */
?>

<?php
	echo TbHtml::linkButton('Назад', array(
		'class' => 'btn pull-right btn-warning',
		'style' => 'margin:10px 50px 0px 0px;',
		'url' => $this->createUrl('GValue/index'),
	));
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gvalue-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<h4>
			<?php echo $form->labelEx($model,'setId', array('style' => 'font-size:20px;')); ?>
			<?php echo  $model->set->obj->name.": ".$model->set->stat->name;?>
		</h4>
	</div>

	<div>
		<h3>
			<?php echo $form->labelEx($model,'timestamp', array('style' => 'font-size:20px;')); ?>
			<?php echo  $model->timestamp;?>
		</h3>
	</div>

	<div>
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'epoch'); ?>
		<?php echo $form->textField($model,'epoch'); ?>
		<?php echo $form->error($model,'epoch'); ?>
	</div>

	<div>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
