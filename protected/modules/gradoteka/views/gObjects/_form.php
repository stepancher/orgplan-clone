<?php
/* @var $this GvalueController */
/* @var $model Gvalue */
/* @var $form CActiveForm */
?>

<?php
echo TbHtml::linkButton('Назад', array(
	'class' => 'btn pull-right btn-warning',
	'style' => 'margin:10px 50px 0px 0px;',
	'url' => $this->createUrl('GObjects/index'),
));
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gvalue-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'gType'); ?>
		<?php echo $form->textField($model,'gType'); ?>
		<?php echo $form->error($model,'gType'); ?>
	</div>
<!--	<div>-->
<!--		--><?php //echo $form->labelEx($model,'parentType'); ?>
<!--		--><?php //echo $form->textField($model,'parentType'); ?>
<!--		--><?php //echo $form->error($model,'parentType'); ?>
<!--	</div>-->
<!--	<div>-->
<!--		--><?php //echo $form->labelEx($model,'parentId'); ?>
<!--		--><?php //echo $form->textField($model,'parentId'); ?>
<!--		--><?php //echo $form->error($model,'parentId'); ?>
<!--	</div>-->
	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textArea($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div>
		<?php echo $form->labelEx($model,'gId'); ?>
		<?php echo $form->textField($model,'gId'); ?>
		<?php echo $form->error($model,'gId'); ?>
	</div>

	<div>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
