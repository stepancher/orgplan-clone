<?php
$ctrl = $this;
$ctrl->widget('application.modules.gradoteka.components.FieldSetView', array(
    'header' => Yii::t($this->getId(), 'Все показатели'),
    'items' => array(
        array(
            'application.modules.gradoteka.components.ActionsView',
            'items' => $this->getMenuItems(),
        ),
        array(
            'gradoteka.widgets.' . get_class($model) . 'GridView',
            'model' => $model,
            'columnsAppend' => array(
                'buttons' => array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '<nobr>{update}&#160;{delete}</nobr>',
                    'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                    'buttons' => array(
                        'update' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('update', array("id" => $data->id));
                            },
                            'options' => array('title' => 'редактировать', 'class' => 'btn btn-small btn-info'),
                        ),
                        'delete' => array(
                            'label' => TbHtml::icon(TbHtml::ICON_PENCIL, array('color' => TbHtml::ICON_COLOR_WHITE)),
                            'url' => function ($data) use ($ctrl) {
                                return $ctrl->createUrl('delete', array("id" => $data->id));
                            },
                            'options' => array('title' => 'удалить', 'class' => 'btn btn-small btn-danger'),
                        ),
                    )
                )
            )
        ),
    )
));
?>