<?php
/* @var $this GvalueController */
/* @var $model Gvalue */
/* @var $form CActiveForm */
?>

<?php
	echo TbHtml::linkButton('Назад', array(
		'class' => 'btn pull-right btn-warning',
		'style' => 'margin:10px 50px 0px 0px;',
		'url' => $this->createUrl('GStatTypes/index'),
	));
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gvalue-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'gId'); ?>
		<?php echo $form->textField($model,'gId'); ?>
		<?php echo $form->error($model,'gId'); ?>
	</div>
<!--	<div>-->
<!--		--><?php //echo $form->labelEx($model,'gType'); ?>
<!--		--><?php //echo $form->textField($model,'gType'); ?>
<!--		--><?php //echo $form->error($model,'gType'); ?>
<!--	</div>-->
	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
<!--	<div>-->
<!--		--><?php //echo $form->labelEx($model,'type'); ?>
<!--		--><?php //echo $form->textField($model,'type'); ?>
<!--		--><?php //echo $form->error($model,'type'); ?>
<!--	</div>-->
	<div>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description', array('style'=>'width:500px;height:140px;')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	<div>
		<?php echo $form->labelEx($model,'units'); ?>
		<?php echo $form->textField($model,'units'); ?>
		<?php echo $form->error($model,'units'); ?>
	</div>
	<div>
		<?php echo $form->labelEx($model,'epochType'); ?>
		<?php echo $form->textField($model,'epochType'); ?>
		<?php echo $form->error($model,'epochType'); ?>
	</div>

	<div>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
