<?php
/**
 * @var GStatTypes $statTypes GStatTypes model instance
 */

echo TbHtml::linkButton('Назад', array(
    'class' => 'btn pull-right btn-warning',
    'style' => 'margin:10px 50px 0px 0px;',
    'url' => $this->createUrl('GObjectsHasGStatTypes/index'),
));
echo CHtml::tag('h1', [], 'Создать набор');
echo TbHtml::beginFormTb('vertical',$this->createUrl('saveGroup'), 'post');
    echo TbHtml::activeLabel($objectsToStats, 'objId');

    $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
        'model'=>$objectsToStats,
        'attribute'=> 'objId',
        'data' =>CHtml::listData($objects->findAll(array('order'=>'name')), 'id', 'name'),
        'htmlOptions'=>array(
            'multiple'=>'multiple',
            'style' => 'width:60%;',
        ),
    ));

    echo TbHtml::activeLabel($objectsToStats, 'statId');
    // Multiple data

    $this->widget('vendor.anggiaj.eselect2.ESelect2',array(
        'model'=>$objectsToStats,
        'attribute'=> 'statId',
        'data' => CHtml::listData(
            GStatTypes::model()->findAll(),
            'id',
            function($el){
                return '('.$el->gType.')'.$el->name.(!empty($el->description)?' ('.ZHtml::cutString($el->description, 100).')':'').' | gId:'.$el->gId;
            }
        ),
        'htmlOptions'=>array(
            'multiple'=>'multiple',
            'style' => 'width:60%;',
        ),
    ));

    echo TbHtml::activeLabel($objectsToStats, 'groupName');
    echo TbHtml::activeTextField($objectsToStats, 'groupName', array());
    echo TbHtml::submitButton('Сохранить',
        array(
            'color' => TbHtml::BUTTON_COLOR_SUCCESS,
            'style' => 'display:block;width:100px;',
        )
    );

echo TbHtml::endForm();

?>

