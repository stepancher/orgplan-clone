<?php
class ZHtml
{
    /**
     * @param $str
     * @param int $len
     * @param string $suffix
     * @param string $charset
     * @return string
     */
    public static function cutString($str, $len = 100, $suffix = '...', $charset = 'UTF-8'){
        return mb_substr($str, 0, $len, $charset).(strlen($str)>$len?$suffix:'');
    }
}