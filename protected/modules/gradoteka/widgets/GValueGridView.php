<?php
/**
 * File GValueGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class GValueGridView
 *
 * @property GValue $model
 */
class GValueGridView extends TbGridView
{
    /**
     * Модель GValue::model()
     * @var GValue	 */
    public $model;

    const PAGE_SIZE = 100;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);
        $this->dataProvider->getPagination()->pageSize = self::PAGE_SIZE;

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        $ctrl = $this->getOwner();

        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'gId' => array(
                'type' => 'raw',
                'name' => 'set_stat_gId',
                'value' => function ($data) {
                   return $data->set->stat->gId;
                }
            ),
            'setId' => array(
                'name' => 'setId',
//                'value' => '$data->valHasStats->obj->name.": ".$data->valHasStats->stat->name',
                'value' => '$data->setId',
                'type' => 'html',
            ),
            'set_obj_name' => array(
                'name' => 'set_obj_name',
//                'value' => '$data->valHasStats->obj->name.": ".$data->valHasStats->stat->name',
                'value' => '$data->set->obj->name',
                'type' => 'html',
//                'filter'=>CHtml::activeTextField(GObjects::model(), 'name'),
            ),
            'set_stat_name' => array(
                'name' => 'set_stat_name',
//                'value' => '$data->valHasStats->obj->name.": ".$data->valHasStats->stat->name',
                'value' => '$data->set->stat->name',
                'type' => 'html',
                'htmlOptions'=>array('width'=>'90px'),
            ),
            'timestamp' => array(
                'name' => 'timestamp',
            ),
            'value' => array(
                'name' => 'value',
                'value' => '$data->value." (".$data->set->stat->units.")"',
            ),
            'epoch' => array(
                'name' => 'epoch',
            ),
        );
        return $columns;
    }
}