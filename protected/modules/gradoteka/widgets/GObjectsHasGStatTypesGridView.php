<?php
/**
 * File GObjectsHasGStatTypesGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class GObjectsHasGStatTypesGridView
 *
 * @property GObjectsHasGStatTypes $model
 */
class GObjectsHasGStatTypesGridView extends TbGridView
{
    /**
     * Модель GObjectsHasGStatTypes::model() or new GObjectsHasGStatTypes('scenario')
     * @var GObjectsHasGStatTypes	 */
    public $model;

    const PAGE_SIZE = 100;
    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);
        $this->dataProvider->getPagination()->pageSize = self::PAGE_SIZE;

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
            'obj_name' => array(
                'type' => 'raw',
                'name' => 'obj_name',
                'value' => function ($data) {
                    return $data->obj->name;
                }
            ),
            'gId' => array(
                'type' => 'raw',
                'name' => 'stat_gId',
                'value' => function ($data) {
                    return $data->stat->gId;
                }
            ),
            'stat_name' => array(
                'type' => 'raw',
                'name' => 'stat_name',
                'value' => function ($data) {
                    return "<span title=\"{$data->stat->description}\">{$data->stat->name}</span>";
                }
            ),
//            'objId' => array(
//                'header' => 'Объект',
//                'name' => 'objId',
//                'value' => '$data->obj->name',
//            ),
//            'statId' => array(
//                'header' => 'Показатель',
//                'name' => 'statId',
//                'value' => '"<span title=\"{$data->stat->description}\">{$data->stat->name}</span>"',
//                'type' => 'html',
//            ),
//            'groupId' => array(
//                'name' => 'groupId',
//            ),
//            'frequency' => array(
//                'name' => 'frequency',
//            ),
//            'groupName' => array(
//                'name' => 'groupName',
//            ),
        );
        return $columns;
    }
}