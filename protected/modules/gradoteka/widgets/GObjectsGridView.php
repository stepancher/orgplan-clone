<?php
/**
 * File GObjectsGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class GObjectsGridView
 *
 * @property GObjects $model
 */
class GObjectsGridView extends TbGridView
{
    /**
     * Модель GObjects::model()
     * @var GObjects	 */
    public $model;

    const PAGE_SIZE = 100;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = ARGridView::createFilter($this->model, true);

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);
        $this->dataProvider->getPagination()->pageSize = self::PAGE_SIZE;

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        $columns = array(
            'id' => array(
                'name' => 'id',
            ),
//            'gType' => array(
//                'name' => 'gType',
//            ),
//            'parentType' => array(
//                'name' => 'parentType',
//            ),
//            'parentId' => array(
//                'name' => 'parentId',
//            ),
            'gId' => array(
                'name' => 'gId',
            ),
            'name' => array(
                'name' => 'name',
            ),
        );

        return $columns;
    }
}