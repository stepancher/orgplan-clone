<div class="<?php echo $params['class'];?>" title="<?php echo $params['epoch'];?>">

    <div class='pie-header'>
            <table width='100%' height='100%' cellpadding='0' cellspacing='0'>
                <tr>
                    <td align='center' valign='middle'>
                        <div>
                            <?php echo $params['header'];?>
                        </div>
                    </td>
                </tr>
            </table>
          </div>
    <div <?php echo ZHtml::buildAttributes(CMap::mergeArray(['class' => 'easy-pie-chart'], $params['htmlOptions']));?>>
        <div class="pie-value">
            <div class='inner-value'>
                <?php echo $params['innerValueDigit'];?>
                <div style='display:block;'>
                    <?php echo $params['innerValueName'];?>
                </div>
            </div>
        </div>
        <div class='canvas' data-percent='<?php echo $params['htmlOptions']['data-percent'];?>'></div>
        <div class='pie-label-digit'>
            <?php echo $params['labelDigit'];?>
        </div>
        <div class='pie-label-name'>
            <?php echo $params['labelName'];?>
        </div>
    </div>
</div>