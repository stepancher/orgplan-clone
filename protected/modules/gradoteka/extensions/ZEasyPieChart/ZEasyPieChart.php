<?php
/**
 * Class ZEasyPieChart
 */
Yii::import('application.extensions.chervand.yii-easy-pie-chart.EasyPieChart');

class ZEasyPieChart extends EasyPieChart
{
    public $barColor = '#ef1e25';
    public $trackColor = '#f2f2f2';
    public $scaleColor = '#dfe0e0';
    public $lineCap = 'butt'; //Defines how the ending of the bar line looks like. Possible values are: butt, round and square.
    public $lineWidth = '5'; //Width of the bar line in px.
    public $innerValue;
    public $lockValues = FALSE;
    public $backUrl;

    public $idPrefix = 'ch';

    public $header;
    public $labelDigit;
    public $labelName;
    public $innerValueDigit;
    public $innerValueName;

    public $epoch;

    public $type;

    const BOLD_LINE = 12;
    const SLIM_LINE = 6;
    const BIG_SIZE = 140;
    const SMALL_SIZE = 74;
    const TRACK_COLOR = '#e1e1e1';

    const BAR_COLOR_GREEN = '#a0d468';
    const BAR_COLOR_LIGHT_GREEN = '#4ec882';
    const BAR_COLOR_RED = '#fa694c';
    const BAR_COLOR_BLUE = '#4b77be';
    const BAR_COLOR_AZURE = '#2b98d5';
    const BAR_COLOR_LIGHT_AZURE = '#42c0b4';
    const BAR_COLOR_ORANGE = '#f6a800';

    const TYPE_DEFAULT = 0;
    public $TYPE_DEFAULT_PARAMS = array(
        'options' => array(
            'barColor'   => self::BAR_COLOR_GREEN,
            'trackColor' => self::TRACK_COLOR,
            'size'       => self::SMALL_SIZE,
            'lineCap' => 'butt',
            'lineWidth' => self::BOLD_LINE,
            'scaleColor'=>false,
        ),
        'class' => 'pie-small'
    );

    const SMALL_BOLD = 1;
    public $PARAMS_SMALL_BOLD = array(
        'options' => array(
            'barColor'   => self::BAR_COLOR_GREEN,
            'trackColor' => self::TRACK_COLOR,
            'size'       => self::SMALL_SIZE,
            'lineCap' => 'butt',
            'lineWidth' => self::BOLD_LINE,
            'scaleColor'=>false,
        ),
        'class' => 'pie-small'
    );

    const SMALL_SLIM = 2;
    public $PARAMS_SMALL_SLIM = array(
        'options' => array(
            'barColor'   => self::BAR_COLOR_GREEN,
            'trackColor' => self::TRACK_COLOR,
            'size'       => self::SMALL_SIZE,
            'lineCap' => 'butt',
            'lineWidth' => self::SLIM_LINE,
            'scaleColor'=>false,
        ),
        'class' => 'pie-small'
    );

    const BIG_BOLD = 3;
    public $PARAMS_BIG_BOLD = array(
        'options' => array(
            'barColor'   => self::BAR_COLOR_GREEN,
            'trackColor' => self::TRACK_COLOR,
            'size'       => self::BIG_SIZE,
            'lineCap' => 'butt',
            'lineWidth' => self::BOLD_LINE,
            'scaleColor'=>false,
        ),
        'class' => 'pie-big'
    );

    const BIG_SLIM = 4;
    public $PARAMS_BIG_SLIM = array(
        'options' => array(
            'barColor'   => self::BAR_COLOR_GREEN,
            'trackColor' => self::TRACK_COLOR,
            'size'       => self::BIG_SIZE,
            'lineCap' => 'butt',
            'lineWidth' => self::SLIM_LINE,
            'scaleColor'=>false,
        ),
        'class' => 'pie-big'
    );

    public function init(){


        $appPath = realpath(__DIR__ . DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR;
        Yii::setPathOfAlias('ZEasyPieChart', $appPath);


        if (empty($this->type)){
            $this->type = self::TYPE_DEFAULT;
        }

        switch($this->type){
            case self::SMALL_BOLD:
                $params = $this->PARAMS_SMALL_BOLD;
                $this->options = $params['options'];
                break;

            case self::SMALL_SLIM:
                $params = $this->PARAMS_SMALL_SLIM;
                $this->options = $params['options'];
                break;

            case self::BIG_BOLD:
                $params = $this->PARAMS_BIG_BOLD;
                $this->options = $params['options'];
                break;

            case self::BIG_SLIM:
                $params = $this->PARAMS_BIG_SLIM;
                $this->options = $params['options'];
                break;

            default:{
                $params = $this->TYPE_DEFAULT_PARAMS;
                $this->options = $params['options'];
                break;
            }
        }

        if(isset($this->barColor) && !empty($this->barColor)){
            $this->options['barColor'] = $this->barColor;
        }

        $this->id .= '-'.$this->idPrefix;

        if(empty($this->backUrl)){
            $this->backUrl = Yii::app()->createUrl('profile/default', ['to' => Yii::app()->getRequest()->requestUri, 'tab' => 'tariffs']);
        }

        parent::init();

    }

    public function run(){

        $params = array(
            'htmlOptions'       => $this->htmlOptions,
            'options'           => $this->options,
            'header'            => $this->header,
            'labelDigit'        => $this->lockValues?'<a class="lock lock-modal" data-target="#modal-tariff-insufficient" data-toggle="modal"></a>'
                                   :$this->labelDigit,

            'labelName'         => $this->labelName,
            'innerValueDigit'   => $this->lockValues?'<a class="lock lock-modal" data-target="#modal-tariff-insufficient" data-toggle="modal"></a>'
                                   :$this->innerValueDigit,

            'innerValueName'    => $this->innerValueName,
            'epoch'             => $this->epoch,
        );

        switch($this->type){

            case self::SMALL_BOLD:
                $params = array_merge($params, $this->PARAMS_SMALL_BOLD);
                break;

            case self::SMALL_SLIM:
                $params = array_merge($params, $this->PARAMS_SMALL_SLIM);
                break;

            case self::BIG_BOLD:
                $params = array_merge($params, $this->PARAMS_BIG_BOLD);
                break;

            case self::BIG_SLIM:
                $params = array_merge($params, $this->PARAMS_BIG_SLIM);
                break;

            default:{
                $params = array_merge($params, $this->TYPE_DEFAULT_PARAMS);
                break;
            }
        }

        echo $this->controller->renderPartial('ZEasyPieChart.views.pie_chart', array(
            'params' => $params,
        ));
    }

    protected function registerEasyPieChart(){
        $app = Yii::app();
        $clientScript = $app->getClientScript();
        $am = $app->getAssetManager();
        $publishUrl = $am->publish(Yii::getPathOfAlias('ZEasyPieChart.assets') , false, -1, YII_DEBUG);

        Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.gradoteka.extensions.ZEasyPieChart.assets.pie_chart') . '.css')
        );

        $options = CJavaScript::encode($this->options);
        $assetsPath = Yii::getPathOfAlias('ext.chervand.yii-easy-pie-chart.assets') . '/' . self::PACKAGE_ID;
        $assetsUrl = Yii::app()->getAssetManager()->publish($assetsPath);

        $clientScript->registerScriptFile($assetsUrl.'/dist/jquery.easypiechart.min.js');

        $clientScript
            ->registerCss(
                self::PACKAGE_ID,
                '.easy-pie-chart{display:inline-block;text-align:center;vertical-align:middle;position:relative;}.easy-pie-chart>.value{position:absolute;width:100%;height:100%;margin:auto;}')
            ->registerScript(
                $this->id,
                'jQuery("#' . $this->id . '").find(".canvas").easyPieChart(' . $options . ');',
                CClientScript::POS_READY
            );
    }
}
