<?php
/**
 * Class ZGridView
 */
Yii::import('zii.widgets.grid.CGridView');
Yii::import('application.modules.gradoteka.GradotekaModule');


class ZGridView extends CGridView{

    public $headers;

    /** @var array $chartColumns Array of ChartColumn objects */
    public $chartColumns;
    public $enableSorting = TRUE;
    public $area;
    public $backUrl;
    public static $percentValues;

    public  $lockValues = FALSE;
    protected static $_lockValues;
    protected static $_backUrl;

    const PAGE_SIZE = 100;
    const COLUMN_PREFIX = 'column_';

    public function init(){
        
        $this->dataProvider->getPagination()->pageSize = self::PAGE_SIZE;
        $this->dataProvider->getSort()->params = array_merge(
            $_GET,
            array(
                'analytics-grid-request' => $this->id,
            )
        );

        self::$_lockValues = $this->lockValues;


        if(empty($this->backUrl)){
           $this->backUrl = Yii::app()->createUrl('profile/default', ['to' => Yii::app()->getRequest()->requestUri, 'tab' => 'tariffs']);
        }
        self::$_backUrl = $this->backUrl;

        if($this->area == ChartTable::TYPE_REGION){
            $this->htmlOptions['class'] .= ' type-region';
        }

        $sqlData = AnalyticsService::getColumnsDataProvider($this->chartColumns, $this->area, TRUE);
        $this::$percentValues = $sqlData->getData();
        $this->columns = self::getColumns($this->chartColumns, $this->area);
        parent::init();
    }

    public function run(){
        parent::run();

        $appPath = realpath(__DIR__ . DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR;
        Yii::setPathOfAlias('ZGridView', $appPath);

        $this->registerAssets();
    }

    protected function registerAssets(){
        $app = Yii::app();
        $cs = $app->getClientScript();
        $am = $app->getAssetManager();
        $publishUrl = $am->publish(Yii::getPathOfAlias('ZGridView.assets') , false, -1, YII_DEBUG);

        Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.gradoteka.extensions.ZGridView.assets.zgridview') . '.css')
        );
        //@TODO refactor prevent default for header sort link when user clicking on lock in table header.
        if($this->lockValues){
            $cs->registerScript(
                'GridViewLink',
                '
                $(".sort-link").click(function(e){
                    e.stopPropagation();
                    e.preventDefault();
                    $("#modal-tariff-insufficient").modal();
                });
            ',
                CClientScript::POS_READY
            );
        }
    }

    /**
     * @param $chartColumns
     * @param $area
     * @return array
     */
    public static function getColumns($chartColumns, $area){
        return array_merge(
            array(
                array(
                    'value' => ' ',
                    'htmlOptions' => array(
                        'style' => 'width:20px;padding:0px !important;',
                    ),
                ),
                array(
                    'type' => 'raw',
                    'value' => function($data, $pos){

                        $index = $pos + 1;

                        return $index;
                    },
                    'footerHtmlOptions'=>array(
                        'style' => 'display:none;'
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:30px;',
                    ),
                ),
                array(
                    'value' => function($data) use($chartColumns, $area){

                        echo "<div style='float: left'>{$data['name']}</div>";

                        if($area == 'country'){

                            $uniqId = md5(microtime());

                            echo "<div class=\"tooltipster\" id=\"stat-{$uniqId}\"></div>";

                            $hintGroup = explode(';',$data["column_1Hint"]);

                            $hintGroup = '<ul>' . implode('',
                                array_map(
                                    function($hint){

                                        return '<li style="margin-bottom: 10px">'.$hint.'</li>';
                                    },
                                    $hintGroup
                                )
                                ) . '</ul>';

                            echo "
                                                <script>
                                                    $(function(e){
                                                        $('#stat-".$uniqId."').tooltipster({
                                                            content: $('<div class=\"tooltip-wrap\"><button class=\"close\"></button>' + '" . $hintGroup . "'+'</div>'),
                                                            trigger: 'click',
                                                            delay: 0,
                                                            speed: 0,
                                                            position: 'bottom'
                                                        });
                                                    });
                                                </script>
                                            ";
                        }

                    },
                    'name' => 'name',
                    'header' => Yii::t('GradotekaModule.gradoteka', 'Russian Federation'),
                    'footerHtmlOptions'=>array(
                        'style' => 'display:none;'
                    ),
                ),
                $area == 'country' ?
                array(
                    'type' => 'raw',
                    'name' => 'TNVED',
                    'header' => '',
                    'value' => function(){
                        return Yii::t('GradotekaModule.gradoteka', 'TNVED');
                    },
                ) : array(
                    'name' => ''
                ),
            ),
            array_map(
                function($column) use ($chartColumns, $area){

                    isset($column->unit) ? $unit = $column->unit : $unit = NULL;

                    if($area == 'country'){

                        $header = self::getHeader(
                            array(
                                'name'  => $column->header,
                                'count' => GStatTypeGroup::model()->findByAttributes(['groupId' => $column->statId])->unit,
                                'value' => $unit == '%' ?
                                    number_format(AnalyticsService::getCountryHeader($column)[self::COLUMN_PREFIX.$column->position],1,',',' ') :
                                    number_format(AnalyticsService::getCountryHeader($column)[self::COLUMN_PREFIX.$column->position],0,',',' '),
                            ),
                            $area
                        );
                    } elseif ($area == 'state' || $area == 'region'){

                        $header = empty($column->header) ? '' : self::getHeader(
                            array(
                                'name'  => $column->header,
                                'count' => isset($column->stat, $column->unit) ? AnalyticsService::filterUnits($column->unit) : '-',
                                'value' => $unit == '%' ?
                                    AnalyticsService::getStateHeader($column->statId, $column->epoch, $column->accessory, 1) :
                                    AnalyticsService::getStateHeader($column->statId, $column->epoch, $column->accessory, 0),
                            ),
                            $area
                        );
                    }
                    else{

                        $header = empty($column->header) ? '' : self::getHeader(
                            array(
                                'name'  => $column->header,
                                'count' => isset($column->stat, $unit) ? AnalyticsService::filterUnits($unit) : '-',
                                'value' => isset($column->stat) ? AnalyticsService::getPercentSumByStat($column->statId, $column->epoch, $area) : '',
                            ),
                            $area
                        );
                    }

                    /** @var ChartColumn $column */
                    return array(
                        'type' => 'raw',
                        'name' => self::COLUMN_PREFIX.'_'.$column->position,
                        'header' => $header,
                        'value' => function($data) use($column, $chartColumns, $area){

                            if(!empty($column->widgetType)){

                                if(!isset($data[self::COLUMN_PREFIX.$column->position])){
                                    echo '<span title="Нет данных">';
                                    return '-';
                                }
                                return self::getWidget($data, $column, $data[self::COLUMN_PREFIX.$column->position."Year"]);
                            }else{
                                if(self::$_lockValues){
                                    return '
                                        <a class="lock lock-modal" data-target="#modal-tariff-insufficient" data-toggle="modal"">
                                        </a>
                                    ';
                                }else{
                                    isset($column->unit) ? $unit = $column->unit : $unit = NULL;
                                    if(isset($data[self::COLUMN_PREFIX.$column->position])){
                                        echo '<span title="'.str_replace('$epoch', $data[self::COLUMN_PREFIX.$column->position."Year"], Yii::t('GradotekaModule.gradoteka','Data for the year')).'">';
                                        if(($unit == '%')){
                                            return number_format($data[self::COLUMN_PREFIX.$column->position], 1, ',', ' ');
                                        }else{
                                            return number_format($data[self::COLUMN_PREFIX.$column->position], 0, ',', ' ');
                                        }
                                    }else{
                                        echo '<span title="Нет данных">';
                                        return '-';
                                    }
                                }
                            }
                        },
                        'footerHtmlOptions'=>array(
                            'style' => 'display:none;'
                        ),
                    );
                },
                $chartColumns
            ),
            array(
                array(
                    'value' => ' ',
                    'htmlOptions' => array(
                        'style' => 'width:20px;padding:0px !important;'
                    ),
                    'footerHtmlOptions'=>array(
                        'style' => 'display:none;'
                    ),
                ),
            )
        );
    }

    /**
     * @param $data
     * @param $column
     * @param $year
     * @return mixed|string
     */
    public static function getWidget($data, $column, $year){
        return Yii::app()->getController()->widget('applications.extensions.ZLineChart.ZLineChart', self::getWidgetParams($data, $column, $year), true);
    }

    /**
     * @param $data
     * @param $column
     * @param $year
     * @return array
     */
    public static function getWidgetParams($data, $column, $year){

        $data = $data[self::COLUMN_PREFIX.$column->position];

        switch($column->widgetType){
            case ZLineChart::TYPE_SIMPLE_BASIC : {
                return array(
                    'percent' =>  self::getPercent($data, $column),
                    'color' => $column->color,
                    'type' => $column->widgetType,
                    'epoch' => $year,
                    'lockValues' => self::$_lockValues,
                );
                break;
            }
            case ZLineChart::TYPE_SIMPLE_SMALL : {
                return array(
                    'percent' =>  self::getPercent($data, $column),
                    'color' => $column->color,
                    'type' => $column->widgetType,
                    'value' => ($column->unit == '%') ? number_format($data, 1,',',' ') : number_format($data, 0,',',' '),
                    'epoch' => $year,
                    'lockValues' => self::$_lockValues,
                );
                break;
            }
            case ZLineChart::TYPE_SIMPLE_STACKED : {
                return array(
                    'name' => Yii::t('GradotekaModule.gradoteka', 'Number of organization'),
                    'type' => ZLineChart::TYPE_SIMPLE_STACKED,
                    'valueType'=> 'раст-во',
                    'second_valueType' => 'живот-во',
                    'color' => ZLineChart::COLOR_GREEN,
                    'colorSecond' => ZLineChart::COLOR_RED,
                    'percent' => $data,
                    'epoch' => $year,
                    'lockValues' => self::$_lockValues,
                    'second_percent' => isset($column->rel->statId,
                            GValue::getByGIds(
                                $column->statId,
                                $column->stat->gId,
                                $column->epoch
                            )->value)?
                            GValue::getByGIds(
                                $column->statId,
                                $column->stat->gId,
                                $column->epoch
                            )->value:
                            '-',
                );
                break;
            }
        }

        return array();
    }

    /**
     * @param $data
     * @param $column
     * @return float|int
     */
    public static function getPercent($data, $column){

        $columnPosition = isset($column->position) ? ZGridView::COLUMN_PREFIX . $column->position : ZGridView::COLUMN_PREFIX . '1';
        $getSqlData = self::$percentValues;
        $maxValue = $getSqlData[0][$columnPosition];

        if($maxValue == 0){
            return 0;
        }

        return 100 * $data/($maxValue);
    }

    /**
     * @param $data
     * @return string
     */
    public static function getHeader($data, $area){

        if(empty($data) || !is_array($data)) {
            return '';
        }

        if(isset($data['name'])){
            $name = $data['name'];
        }
        if(isset($data['count'])){
            $count = $data['count'];
        }
        if(isset($data['value'])){
            $value = $data['value'];
        }

        //@TODO refactor checking for user tariff
        if(self::$_lockValues){
            $value =  '
                <div class="lock grid_header_lock lock-modal" data-target="#modal-tariff-insufficient" data-toggle="modal">
                </div>
            ';
        }

        if($area == 'country'){

            return "<div class='help-table-container' style='width: 150px'>
                    <div class='header-help-table'>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-category'>
                                    {$name}
                                </div>
                            </div>
                        </div>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-type'>
                                    {$count}
                                </div>
                            </div>
                        </div>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-count'>
                                    {$value}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
        } else {

            return "<div class='help-table-container'>
                    <div class='header-help-table'>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-category'>
                                    {$name}
                                </div>
                            </div>
                        </div>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-type'>
                                    {$count}
                                </div>
                            </div>
                        </div>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-count'>
                                    {$value}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
        }

    }
}
