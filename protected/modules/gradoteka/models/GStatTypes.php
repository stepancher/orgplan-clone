<?php
/**
 * Class GStatTypes
 */

class GStatTypes extends \AR
{
    public static $_description = NULL;

    protected static $_uniqRecords = array();

    const GTYPE_REGION = 'region';
    const GTYPE_STATE  = 'state';

    /**
     * @return array
     */
    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'gId' => array(
                'string',
                'label' => Yii::t('gstattypes', 'Глобальный ID'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('required'),
                    ),
                    'insert' => array(
                        array('safe'),
                        array('required'),
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('gstattypes', 'name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'parentName' => array(
                'string',
                'label' => Yii::t('gstattypes', 'parentName'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'type' => array(
                'string',
                'label' => Yii::t('gstattypes', 'type'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'description' => array(
                'string',
                'label' => Yii::t('gstattypes', 'description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'unit' => array(
                'string',
                'label' => Yii::t('gstattypes', 'unit'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'autoUpdate' => array(
                'integer',
                'label' => Yii::t('gstattypes', 'autoUpdate'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'gObjectHasGStatTypes' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'GObjectsHasGStatTypes',
                    array(
                        'gId' => 'statId',
                    ),
                ),
            ),
        );
    }

    /**
     * @param $gtype
     * @return static[]
     */
    public static function getListByType($gtype){

        $criteria = new CDbCriteria();
        $criteria->with['gObjectHasGStatTypes'] = [
            'together' => TRUE,
            'select' => FALSE,
        ];
        $criteria->addCondition('gObjectHasGStatTypes.gType = :gtype');
        $criteria->params[':gtype'] = $gtype;
        $criteria->order = 'name ASC';

        return self::model()->findAll($criteria);
    }

    /**
     * @return array
     */
    public function filterGType(){
        return CHtml::listData(
            self::model()->findAll(array(
                'select'=>'t.gType',
                'distinct'=>true,
            )),
            'gType',
            'gType'
        );
    }

    /**
     * @return array
     */
    public static function getStatTypesList(){
        return CHtml::listData(GStatTypes::model()->findAll(),'id','name');
    }
}