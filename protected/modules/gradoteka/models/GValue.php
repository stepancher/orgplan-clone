<?php
/**
 * Class GValue
 *
 * @property integer id
 * @property integer set_stat_gid
 * @property string timestamp
 * @property integer value
 * @property integer epoch
 */

class GValue extends \AR
{
    public static $_description = NULL;

    protected static $_uniqRecords = array();
    public static $topValue = array();

    /**
     * @return array
     */
    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'set_stat_gId' => array(
                'label' => 'Глобальный ID показателя',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'set_stat_name' => array(
                'label' => 'Показатель',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'set_obj_name' => array(
                'label' => 'Объект',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'setId' => array(
                'integer',
                'label' => Yii::t('GValue', 'Набор'),
                'relation' => array(
                    'CBelongsToRelation',
                    'GObjectsHasGStatTypes',
                    array(
                        'setId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,update' => array(
                        array('safe'),
                        array('required'),
                    ),
                    'insert' => array(
                        array('uniqueValidation'),
                        array('safe'),
                        array('required'),
                    ),
                ),
            ),
            'timestamp' => array(
                'timestamp',
                'label' => Yii::t('GValue', 'Время добавления'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'value' => array(
                'double',
                'label' => Yii::t('GValue', 'Значение'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
//                        array('required'),
                    ),
                ),
            ),
            'epoch' => array(
                'integer',
                'label' => Yii::t('GValue', 'Дата'),
                'rules' => array(
                    'search,update' => array(
                        array('safe'),
                    ),
                    'insert' => array(
                        array('safe'),
                        array('uniqueValidation'),
                    ),
                ),
            ),
        );
    }

    /**
     * TODO unique validation for insert & update
     * @param $attribute
     */
    public function uniqueValidation($attribute){

        $uniqKey = $this->attributes['setId'].":".$this->attributes['epoch'];

        if(!isset(self::$_uniqRecords[$uniqKey])){

            $criteria = new CDbCriteria();
            $criteria->addCondition('setId = :setId');
            $criteria->addCondition('epoch = :epoch');
            $criteria->params = array(
                ':setId' => $this->attributes['setId'],
                ':epoch' => $this->attributes['epoch'],
            );

            if(self::$_uniqRecords[$uniqKey] = parent::model()->exists($criteria)){
                $this->addError($attribute,'uniqueValidation','unique key constraint violation');
            }
        }
    }

    /**
     * @return bool
     * Сохранение коректной даты
     */
    public function beforeSave()
    {
        if(parent::beforeSave()){
            if (empty($this->timestamp)) {
                $this->timestamp = new CDbExpression('NOW()');
            }
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getRatingPoint(){

        $criteria = new CDbCriteria();
        $criteria->with = array('set','set.stat','set.obj.translate');
        $criteria->order = 't.value DESC, translate.name';
        $criteria->addCondition('
            stat.gid = :stat_gid and
            obj.gType = :gType and
            epoch = :epoch'
        );
        $criteria->params = array(
            ':stat_gid' =>  $this->set->stat->gId,
            ':gType' => $this->set->obj->gType,
            ':epoch' => $this->epoch,
        );

        $els = CHtml::listData(self::model()->findAll($criteria), 'id', 'value');

        return array_search($this->id, array_keys($els))+1;
    }

    /**
     * @return int
     */
    public function getTopValue(){
        $statGid = $this->set->stat->gId;

        $criteria = new CDbCriteria();
        $criteria->with = array('set','set.stat','set.obj');
        $criteria->addCondition('
            stat.gid = :stat_gid and
            obj.gType = :gType and
            epoch = :epoch'
        );
        $criteria->params = array(
            ':stat_gid' =>  $statGid,
            ':epoch' => $this->epoch,
            ':gType' => $this->set->obj->gType,
        );
        $criteria->order = 't.value DESC';

        if(!isset(self::$topValue[$statGid.'_'.$this->epoch])){

            $gValue = self::model()->find($criteria);
            if($gValue !== NULL){
                self::$topValue[$statGid.'_'.$this->epoch] = $gValue->value;
            } else {
                self::$topValue[$statGid.'_'.$this->epoch] = NULL;
            }
        }

        return self::$topValue[$statGid.'_'.$this->epoch];
    }


    /**
     * @param $gTypeId
     * @param $objId
     * @param $epoch
     * @return static
     */
    public static function getByGIds($gTypeId, $objId, $epoch){

        Yii::import('app.modules.gradoteka.models.*');

        /**
         * set relation -> to GObjectsHasGStatTypes model
         * obj relation -> to GObjects model
         * stat relation -> to GStatTypes model
         */
        $criteria = new CDbCriteria();
        $criteria->with = array('set','set.obj', 'set.stat');
        $criteria->addCondition('
            obj.id = :obj_gid and
            stat.gid = :stat_gid and
            epoch = :epoch'
        );
        $criteria->params = array(
            ':obj_gid' => $objId,
            ':stat_gid' => $gTypeId,
            ':epoch' => $epoch,
        );

        return self::model()->find($criteria);
    }

    /**
     * @return null|string
     */
    public function getUnits(){
        if(!isset($this->set)){
            return NULL;
        }

        if(!isset($this->set->stat)){
            return NULL;
        }

        if(!isset($this->set->stat->units)){
            return NULL;
        }

        return $this->set->stat->units;
    }

    /**
     * @param $statId
     * @param $epoch
     * @param $object
     * @return array|mixed|null
     */
    public static function getValuesByStat($statId, $epoch, $object){

        $criteria = new CDbCriteria();
        $criteria->with = [
            'set' => [
                'together' => TRUE,
            ]
        ];
        $criteria->addCondition('set.statId = :statId');
        $criteria->addCondition('t.epoch = :epoch');
        $criteria->addCondition('set.gType = :object');

        $criteria->params = array(
            ':statId' =>  $statId,
            ':epoch' => $epoch,
            ':object' => $object,
        );
        $criteria->order = 't.value DESC';

        return self::model()->findAll($criteria);
    }
}