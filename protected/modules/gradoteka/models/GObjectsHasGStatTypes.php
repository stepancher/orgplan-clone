<?php

/**
 * This is the model class for table "{{gobjectshasgstattypes}}".
 *
 * The followings are the available columns in table '{{gobjectshasgstattypes}}':
 * @property integer $id
 * @property integer $objId
 * @property integer $statId
 * @property integer $groupId
 * @property string  $groupName
 * @property integer $frequency
 *
 * The followings are the available model relations:
 * @property Gobjects $obj
 * @property Gstattypes $stat
 */
class GObjectsHasGStatTypes extends AR
{
	public static $_description = NULL;

	protected static $_uniqRecords = array();

	public function description()
	{
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'obj_name' => array(
				'string',
				'label' => GObjects::model()->getAttributeLabel('name'),
				'rules' => array(
					'search' => array(
						array('safe')
					)
				)
			),
			'objId' => array(
				'integer',
				'label' =>'Объекты',
				'rules' => array(
					'search,update' => array(
						array('safe'),
						array('numerical'),
						array('required'),
					),
					'insert' => array(
						array('safe'),
						array('numerical'),
						array('required'),
						array('uniqueValidation'),
					)
				),
				'relation' => array(
					'CBelongsToRelation',
					'GObjects',
					array(
						'objId' => 'id',
					),
				),
			),
			'stat_name' => array(
				'string',
				'label' => GStatTypes::model()->getAttributeLabel('name'),
				'rules' => array(
					'search' => array(
						array('safe')
					)
				)
			),
			'stat_gId' => array(
				'label' => GStatTypes::model()->getAttributeLabel('gId'),
				'rules' => array(
					'search' => array(
						array('safe')
					)
				)
			),
			'statId' => array(
				'integer',
				'label' =>'Показатели',
				'rules' => array(
					'search,update' => array(
						array('safe'),
						array('required'),
					),
					'insert' => array(
						array('safe'),
						array('required'),
						array('uniqueValidation'),
					)
				),
				'relation' => array(
					'CBelongsToRelation',
					'GStatTypes',
					array(
						'statId' => 'gId',
					),
				),
			),
			'groupId' => array(
				'integer',
				'label' =>'id группы',
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
						array('numerical'),
					)
				),
			),
			'frequency' => array(
				'integer',
				'label' =>'частота',
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
						array('numerical'),
					)
				),
			),
			'groupName' => array(
				'string',
				'label' =>'Название группы',
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					)
				),
			),
		);
	}


	/**
	 * TODO unique validation for insert & update
	 * @param $attribute
	 */
	public function uniqueValidation($attribute){

		$uniqKey = $this->attributes['objId'].":".$this->attributes['statId'];

		if(!isset(self::$_uniqRecords[$uniqKey])){

			$criteria = new CDbCriteria();
			$criteria->addCondition('objId = :objId');
			$criteria->addCondition('statId = :statId');
			$criteria->params = array(
				':objId' => $this->attributes['objId'],
				':statId' => $this->attributes['statId'],
			);

			if(self::$_uniqRecords[$uniqKey] = parent::model()->exists($criteria)){
				$this->addError($attribute,'uniqueValidation','unique key constraint violation');
			}
		}
	}
}
