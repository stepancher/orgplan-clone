<?php

/**
 * Class GStatTypeHasFormula
 *
 */
class GStatTypeHasFormula extends \AR{

    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'gId' => array(
                'string',
                'label' => 'gId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'formulaType' => array(
                'string',
                'label' => 'formulaType',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'formula' => array(
                'string',
                'label' => 'formula',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
        );
    }
}