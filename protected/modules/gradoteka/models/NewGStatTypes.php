<?php
/**
 * Class NewGStatTypes
 * @property int id
 * @property string gId
 */

class NewGStatTypes extends \AR
{
    public static $_description = NULL;

    /**
     * @return array
     */
    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'gId' => array(
                'string',
                'label' => Yii::t('gstattypes', 'Глобальный ID'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('required'),
                    ),
                    'insert' => array(
                        array('safe'),
                        array('required'),
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('gstattypes', 'name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'parentName' => array(
                'string',
                'label' => Yii::t('gstattypes', 'parentName'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'type' => array(
                'string',
                'label' => Yii::t('gstattypes', 'type'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'description' => array(
                'string',
                'label' => Yii::t('gstattypes', 'description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'unit' => array(
                'string',
                'label' => Yii::t('gstattypes', 'unit'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
        );
    }
}