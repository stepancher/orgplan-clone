<?php
/**
 * Class Gobjects
 */

class GObjects extends \AR
{
    public static $_description = NULL;
    
    const RUSSIA_COUNTRY_ID = 379;

    protected $_name = NULL;

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    /**
     * @return array
     */
    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'objHasStats' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'GObjectsHasGStatTypes',
                    array(
                        'objId' => 'id',
                    ),
                ),
            ),
            'gType' => array(
                'string',
                'label' => Yii::t('GObjects', 'Тип объекта'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('required'),
                    )
                ),
            ),
            'parentType' => array(
                'string',
                'label' => Yii::t('GObjects', 'parentType'),
                'rules' => array(
                    'search,insert' => array(
                        array('safe'),
                    ),
                    'update' => array(
                        array('safe'),
                    )
                ),
            ),
            'parentId' => array(
                'integer',
                'label' => Yii::t('GObjects', 'parentId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('numerical'),
                    )
                ),
            ),
            'gId' => array(
                'string',
                'label' => Yii::t('GObjects', 'Глобальный ID'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('numerical'),
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrGObjects',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }

    /**
     * @return array
     */
    public static function getDataArray($gType = NULL){

        $criteria = new CDbCriteria();
        if($gType !== NULL){
            $criteria->addCondition('objHasStats.gType = :gType AND objHasStats.statId NOT IN (500000, 500001, 500002, 500003, 500004, 500005, 500006, 500007)');
            $criteria->params = array(':gType' => $gType);
        }

        $models = GObjects::model()->with(array('objHasStats', 'objHasStats.stat'))->findAll($criteria);

        $data_array = array();
        if(!empty($models) && is_array($models)){
            foreach($models as $object){
                foreach($object->objHasStats as $stats){

                    if(!isset($stats->id) || empty($stats->id)){
                        continue;
                    }

                    $data_array[$object->gId][$stats->stat->gId] = $stats->id;
                }
            }
        }

        return $data_array;
    }
}