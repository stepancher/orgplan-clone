<?php

/**
 * Class GStatTypeFormulaHasStat
 *
 */
class GStatTypeFormulaHasStat extends \AR{

    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'formulaId' => array(
                'integer',
                'label' => 'formulaId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'key' => array(
                'string',
                'label' => 'key',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'statId' => array(
                'string',
                'label' => 'statId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
        );
    }
}