<?php

Yii::import('app.modules.gradoteka.commands.GradotekaCommand');

class GObjectsHasGStatTypesController extends Controller{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){
        $this->render('index', array('model'=> GObjectsHasGStatTypes::model()));
    }

    public function actionCreate(){

        $objects = GObjects::model();
        $objectsToStats = GObjectsHasGStatTypes::model();

        $this->render('create', array(
            'objects'=>$objects,
            'objectsToStats'=>$objectsToStats,
        ));
    }

    public function actionSaveGroup(){

        $data = $_POST['GObjectsHasGStatTypes'];
        if(isset($data) && isset($data['objId']) && isset($data['statId'])){
            $objIds = $data['objId'];
            $statIds = $data['statId'];
            $groupName = $data['groupName'];

            foreach($objIds as $objId){
                foreach($statIds as $statId){
                    if(empty($data['objId']) || empty($data['statId'])){
                        continue;
                    }
                    $model = new GObjectsHasGStatTypes;
                    $model->objId = $objId;
                    $model->gType = GObjects::model()->findByPk($objId)->gType;
                    $model->statId = $statId;
                    $model->groupName = $groupName;
                    $model->save();
                }
            }
        }

        Yii::app()->user->setFlash('success', 'Информация сохранена.');
        $this->redirect($this->createUrl('index'));
    }

    public function actionCollectValues(){

        $command = new GradotekaCommand('GradotekaCommand', '');
        $command->run(array('collectValues'));

        Yii::app()->user->setFlash('success', 'Данные обновлены');
        $this->redirect($this->createUrl('index'));
    }

    public function actionPullObjects(){

        $command = new GradotekaCommand('GradotekaCommand', '');
        $command->run(array('pullObjects'));

        Yii::app()->user->setFlash('success', 'Данные обновлены');
        $this->redirect($this->createUrl('index'));
    }

    public function actionPullStats(){

        $command = new GradotekaCommand('GradotekaCommand', '');
        $command->run(array('pullStats'));

        Yii::app()->user->setFlash('success', 'Данные обновлены');
        $this->redirect($this->createUrl('index'));
    }

    /**
     * @param $id
     * @throws CDbException
     */
    public function actionDelete($id){
        if (($gObjectsHasGStatTypes = GObjectsHasGStatTypes::model()->findByPk($id)) !== null){
            if($gObjectsHasGStatTypes->delete()){
                GValue::model()->deleteAll('setId = :id', array(':id'=>$id));
            }
        }
    }

    public function actionDeleteAll(){

        if(GObjectsHasGStatTypes::model()->deleteAll()){
            GValue::model()->deleteAll();
        }

        $this->render('index',  array('model'=> GObjectsHasGStatTypes::model()));
    }

    public function getMenuItems(){
        return array(
            'add' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Добавить набор'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/create'),
                    'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                    'style' => 'color:white !important;',

                )
            ),
            'updateValues' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Обновить данные'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/collectValues'),
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'style' => 'color:white !important;',
                )
            ),
            'updateRegions' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Обновить объекты'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/pullObjects'),
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'style' => 'color:white !important;',
                )
            ),
            'updateStats' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Обновить показатели'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/pullStats'),
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'style' => 'color:white !important;',
                )
            ),
            'viewStat' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все показатели'),
                array(
                    'url' => $this->createUrl('GStatTypes/index'),
                    'color' => TbHtml::BUTTON_COLOR_INFO,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gStatTypes/index'?'active':'',
                )
            ),
            'viewObjects' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все объекты'),
                array(
                    'url' => $this->createUrl('GObjects/index'),
                    'color' => TbHtml::BUTTON_COLOR_INFO,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gObjects/index'?'active':'',
                )
            ),
            'viewValues' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все значения'),
                array(
                    'url' => $this->createUrl('GValue/index'),
                    'color' => TbHtml::BUTTON_COLOR_INFO,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gValue/index'?'active':'',
                )
            ),
            'deleteValues' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить значения'),
                array(
                    'url' => $this->createUrl('GValue/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все значения?',
                )
            ),
            'deleteObjects' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить объекты'),
                array(
                    'url' => $this->createUrl('GObjects/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все объекты?',
                )
            ),
            'deleteStats' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить показатели'),
                array(
                    'url' => $this->createUrl('GStatTypes/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все показатели?',
                )
            ),
            'deleteSets' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить наборы'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все наборы?',
                )
            ),
            'back' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все наборы'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/index'),
                    'color' => TbHtml::BUTTON_COLOR_WARNING,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gObjectsHasGStatTypes/index'?'active':'',
                )
            ),
        );
    }
}