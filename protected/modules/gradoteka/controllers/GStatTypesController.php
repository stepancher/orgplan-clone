<?php
class GStatTypesController extends Controller{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array_unique(array_map(
                    function($el){
                        return strstr($el, 'action') ? preg_replace(array('/actions/', '/action/'), '', $el) : '';
                    },
                    get_class_methods(__CLASS__)
                )),
                'roles' => array(User::ROLE_ADMIN),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex(){

        $model = GStatTypes::model();

        $this->render('index', array('model'=> $model));
    }

    /**
     * @param $id
     * @throws CDbException
     */
    public function actionDelete($id){

        if(isset($id) && !empty($id) && is_numeric($id)){
            if (($model = GStatTypes::model()->findByPk($id)) !== null) {
                $model->delete();
            }
        }
    }

    /**
     * @param $id
     */
    public function actionUpdate($id)
    {
        $model=GStatTypes::model()->findByPk($id);

        if(isset($_POST['GStatTypes'])){

            $model->attributes=$_POST['GStatTypes'];

            if($model->validate()){
                $model->save();

                $this->redirect(array('index'));
            }else{
                CVarDumper::dump($model->getErrors(),10, 1);exit;
            }

        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDeleteAll(){

        if(GStatTypes::model()->deleteAll()){
            GValue::model()->deleteAll();
        }

        $this->render('index',  array('model'=> GStatTypes::model()));
    }

    public function getMenuItems(){
        return array(
            'add' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Добавить набор'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/create'),
                    'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                    'style' => 'color:white !important;',

                )
            ),
            'updateValues' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Обновить данные'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/collectValues'),
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'style' => 'color:white !important;',
                )
            ),
            'updateRegions' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Обновить объекты'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/pullObjects'),
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'style' => 'color:white !important;',
                )
            ),
            'updateStats' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Обновить показатели'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/pullStats'),
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'style' => 'color:white !important;',
                )
            ),
            'viewStat' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все показатели'),
                array(
                    'url' => $this->createUrl('GStatTypes/index'),
                    'color' => TbHtml::BUTTON_COLOR_INFO,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gStatTypes/index'?'active':'',
                )
            ),
            'viewObjects' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все объекты'),
                array(
                    'url' => $this->createUrl('GObjects/index'),
                    'color' => TbHtml::BUTTON_COLOR_INFO,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gObjects/index'?'active':'',
                )
            ),
            'viewValues' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все значения'),
                array(
                    'url' => $this->createUrl('GValue/index'),
                    'color' => TbHtml::BUTTON_COLOR_INFO,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gValue/index'?'active':'',
                )
            ),
            'deleteValues' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить значения'),
                array(
                    'url' => $this->createUrl('GValue/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все значения?',
                )
            ),
            'deleteObjects' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить объекты'),
                array(
                    'url' => $this->createUrl('GObjects/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все объекты?',
                )
            ),
            'deleteStats' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить показатели'),
                array(
                    'url' => $this->createUrl('GStatTypes/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все показатели?',
                )
            ),
            'deleteSets' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Удалить наборы'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/deleteAll'),
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'style' => 'color:white !important;',
                    'confirm' => 'Вы уверены что хотите удалить все наборы?',
                )
            ),
            'back' => array(
                TbHtml::BUTTON_TYPE_LINK,
                Yii::t($this->getId(), 'Все наборы'),
                array(
                    'url' => $this->createUrl('GObjectsHasGStatTypes/index'),
                    'color' => TbHtml::BUTTON_COLOR_WARNING,
                    'style' => 'color:white !important;',
                    'class' => $this->route == 'gradoteka/gObjectsHasGStatTypes/index'?'active':'',
                )
            ),
        );
    }
}