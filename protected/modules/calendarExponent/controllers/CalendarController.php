<?php

class CalendarController extends Controller {

	public static function getFair($fairId) {

		if (Yii::app()->user->role == User::ROLE_ORGANIZERS) {
			$fair = Fair::model()->findByPk($fairId);
			if ($fair) {

				$fairHasOrganizers = FairHasOrganizer::model()->findAllByAttributes(['fairId' => $fairId]);
				$userHasOrganizers = array();

				foreach ($fairHasOrganizers as $fairHasOrganizer){

					$userHasOrganizer = UserHasOrganizer::model()->findByAttributes([
						'userId' => Yii::app()->user->id,
						'organizerId' => $fairHasOrganizer->organizerId,
					]);

					if(!empty($userHasOrganizer)){
						$userHasOrganizers[] = $userHasOrganizer;
					}
				}
			}
			if (empty($userHasOrganizers)) {
				$fair = [];
			}
		} else {
			$fair = [];
			$myFair = MyFair::model()->findByAttributes([
				'userId' => Yii::app()->user->id,
				'fairId' => $fairId,
				'status' => 1
			]);
			if ($myFair) {
				$fair = Fair::model()->findByPk($fairId);
			}
		}

		return $fair;
	}

	public function actionIndex($fairId = null) {

		if (!in_array(Yii::app()->user->role, [User::ROLE_EXPONENT, User::ROLE_EXPONENT_SIMPLE, User::ROLE_ORGANIZERS, User::ROLE_ADMIN])) {
			$this->redirect(Yii::app()->createUrl('home'));
		}

		$fair = self::getFair($fairId);
		$this->pageTitle = Yii::t('CalendarExponentModule.calendarExponent', 'Calendar view');
		if ($fair) {
			$this->pageTitle .= ' - ' . $fair->name;
		}
		$this->pageTitle .= ' | ' . Yii::app()->name;

		$this->render('index', [
			'fair' => self::getFair($fairId)
		]);
	}

	public function actionAll($fairId = null) {

		if ($fair = self::getFair($fairId)) {
			$attributes = ['fairId' => [$fair->id]];
		} else {
			if (Yii::app()->user->role == User::ROLE_ORGANIZERS) {
				$userHasOrganizers = UserHasOrganizer::model()->findAllByAttributes([
					'userId' => Yii::app()->user->id
				]);
				$organizers =[];
				foreach ($userHasOrganizers as $userHasOrganizer) {
					$organizers[] = $userHasOrganizer->organizerId;
				}

				$organizerFairs = FairHasOrganizer::model()->findAllByAttributes(['organizerId' => $organizers]);
				$fairs = [];
				foreach ($organizerFairs as $organizerFair) {
					$fairs[] = $organizerFair->fairId;
				}
				$attributes = ['fairId' => $fairs];
			} else {
				$myFairs = MyFair::model()->findAllByAttributes([
					'userId' => Yii::app()->user->id,
					'status' => 1
				]);
				$fairs = [];
				foreach ($myFairs as $myFair) {
					$fairs[] = $myFair->fairId;
				}
				$attributes = ['fairId' => $fairs];
			}
		}

//		$calendarTasks = CalendarFairDays::model()->findAllByAttributes(
//			$attributes,
//			['order' => 'startDate']
//		);

        $res = array();
        $i=0;
        foreach($attributes["fairId"] as $fairId){
            if(
                /*in_array($fairId, array(184,10556,11699)) &&*/
                MyFair::isExponentFair($fairId)
            ){
                continue;
            }

            $fair = Fair::model()->findByPk($fairId);

            $res[] = array(
                "id"=> $i++,
                "fairId"=> $fair->id,
                "name"=> Yii::t('CalendarExponentModule.calendarExponent', 'Dates of fair running ').$fair->name,
                "startDate"=> $fair->beginDate,
                "endDate"=> date('Y-m-d', strtotime($fair->endDate.' + 1 days')),
                "cssClass"=> "red",
            );
        }

		echo json_encode($res);
	}
}