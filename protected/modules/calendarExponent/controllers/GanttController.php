<?php

class GanttController extends Controller {

	public static function getFair($fairId) {

		if (Yii::app()->user->role == User::ROLE_ORGANIZERS) {
			$fair = Fair::model()->findByPk($fairId);
			if ($fair) {
				
				$fairHasOrganizers = FairHasOrganizer::model()->findAllByAttributes(['fairId' => $fairId]);
				$userHasOrganizers = array();

				foreach ($fairHasOrganizers as $fairHasOrganizer){

					$userHasOrganizer = UserHasOrganizer::model()->findByAttributes([
						'userId' => Yii::app()->user->id,
						'organizerId' => $fairHasOrganizer->organizerId,
					]);

					if(!empty($userHasOrganizer)){
						$userHasOrganizers[] = $userHasOrganizer;
					}
				}
			}
			if (empty($userHasOrganizers)) {
				$fair = [];
			}
		} else {
			$fair = [];
			$myFair = MyFair::model()->findByAttributes([
				'userId' => Yii::app()->user->id,
				'fairId' => $fairId,
				'status' => 1
			]);
			if ($myFair) {
				$fair = Fair::model()->findByPk($fairId);
			}
		}

		return $fair;
	}

	public function beforeAction($action) {

		$this->check();

		return true;
	}

	public function actionIndex($fairId = null) {

		if (!in_array(Yii::app()->user->role, [User::ROLE_EXPONENT_SIMPLE, User::ROLE_EXPONENT])) {
			$this->redirect(Yii::app()->createUrl('home'));
		}

		$fair = self::getFair($fairId);
		$this->pageTitle = Yii::t('CalendarExponentModule.calendarExponent', 'Gantt view');
		if ($fair) {
			$this->pageTitle .= ' - ' . $fair->name;
		}

        $myFairs = MyFair::getFairsId(Yii::app()->user->id);
        foreach($myFairs as $myFair){
            if(
                /*in_array($myFair, array(184,10556,11699)) &&*/
                MyFair::isExponentFair($myFair) ||
                empty($mfair = Fair::model()->findByPk($myFair))
            ){
                continue;
            }
            $cft = CalendarFairTasks::model()->findByAttributes(array('fairId' => $myFair));
            if(empty($cft)){
                foreach(CalendarFairTasksCommon::model()->findAll() as $cftCommon){

                    $uuid = md5(microtime());

                    $cft = new CalendarFairTasks();

                    $cft->fairId = $myFair;
                    $cft->uuid = $uuid;
                    $cft->group = $uuid;
                    $cft->duration = $cftCommon->duration;
                    if(
                        empty($cftCommon->before)
                    ){
                        $cft->date = date('Y-m-d H:i:s', strtotime($mfair->endDate.' + '.$cftCommon->after.' days'));
                    }else{
                        $cft->date = date('Y-m-d H:i:s', strtotime($mfair->beginDate.' - '.$cftCommon->before.' days'));
                    }

                    $cft->color = $cftCommon->color;
                    $cft->completeButton = $cftCommon->completeButton;
                    $cft->first = $cftCommon->first;

                    $cft->save();

                    $langs = array(
                        'ru',
                        'de',
                        'en',
                    );

                    foreach($langs as $lang){

                        $trcftCommon = TrCalendarFairTasksCommon::model()->findByAttributes(array('trParentId' => $cftCommon->uuid, 'langId' => $lang));

                        if(empty($trcftCommon)){
                            continue;
                        }

                        $trcft = new TrCalendarFairTasks();

                        $trcft->trParentId = $uuid;
                        $trcft->langId = $lang;
                        $trcft->name = $trcftCommon->name;
                        $trcft->desc = $trcftCommon->desc;

                        $trcft->save();
                    }
                }
            }
        }



		$this->pageTitle .= ' | ' . Yii::app()->params['titleName'];

		$this->render('exponent', [
			'fair' => self::getFair($fairId)
		]);
	}

	public function actionAll($fairId = null) {
			$this->exponentTasks($fairId);
	}

	public function organizerTasks($fairId) {

		$select = [
			"IFNULL(temp_tasks.id, tasks.id) as id",
			'IF(temp_tasks.id IS NULL, tasks.uuid, temp_tasks.uuid) as uuid',
			'IF(temp_tasks.id IS NULL, tasks.parent, temp_tasks.parent) as parent',
			'IF(temp_tasks.id IS NULL, tasks.group, temp_tasks.group) as `group`',
			'IF(temp_tasks.id IS NULL, tasks.fairId, temp_tasks.fairId) as fairId',
			'trfair.name as fairName',
			'IF(temp_tasks.id IS NULL, trtasks.name, temp_tasks.name) as name',
			'IF(temp_tasks.id IS NULL, trtasks.desc, temp_tasks.desc) as `desc`',
			'IF(temp_tasks.id IS NULL, tasks.duration, temp_tasks.duration) as duration',
			'IF(temp_tasks.id IS NULL, tasks.date, temp_tasks.date) as date',
			'IF(temp_tasks.id IS NULL, tasks.color, temp_tasks.color) as color',
			'IF(temp_tasks.id IS NULL, tasks.completeButton, temp_tasks.completeButton) as completeButton',
			'IF(temp_tasks.id IS NULL, tasks.first, temp_tasks.first) as first',
			'temp_tasks.action',
			'temp_tasks.editDate'
		];

		$commandLeft = Yii::app()->db->createCommand()
			->select($select)
			->from('{{calendarfairtasks}} tasks')
			->leftJoin('{{trcalendarfairtasks}} trtasks', 'tasks.uuid = trtasks.trParentId and trtasks.langId = :langId', array(':langId' => Yii::app()->language))
			->leftJoin('{{calendarfairtemptasks}} temp_tasks', 'temp_tasks.uuid = tasks.uuid')
			->leftJoin('{{trfair}} trfair', "trfair.trParentId = IFNULL(temp_tasks.fairId, tasks.fairId) AND trfair.langId = '" . Yii::app()->language ."'")
			->join('{{fair}} fair', 'IFNULL(temp_tasks.fairId, tasks.fairId) = fair.id');

		$commandRight = Yii::app()->db->createCommand()
			->select($select)
			->from('{{calendarfairtasks}} tasks')
			->rightJoin('{{calendarfairtemptasks}} temp_tasks', 'temp_tasks.uuid = tasks.uuid')
			->leftJoin('{{trcalendarfairtasks}} trtasks', 'tasks.uuid = trtasks.trParentId and trtasks.langId = :langId', array(':langId' => Yii::app()->language))
			->leftJoin('{{trfair}} trfair', "trfair.trParentId = IFNULL(temp_tasks.fairId, tasks.fairId) AND trfair.langId = '" . Yii::app()->language ."'")
			->join('{{fair}} fair', 'IFNULL(temp_tasks.fairId, tasks.fairId) = fair.id');

		$fair = self::getFair($fairId);
		if ($fair) {
			$commandLeft->where('IFNULL(temp_tasks.fairId, tasks.fairId) = ' . $fair->id);
			$commandRight->where('IFNULL(temp_tasks.fairId, tasks.fairId) = ' . $fair->id);
		} else {
			$userHasOrganizers = UserHasOrganizer::model()->findAllByAttributes([
				'userId' => Yii::app()->user->id
			]);
			$organizers =[];
			foreach ($userHasOrganizers as $userHasOrganizer) {
				$organizers[] = $userHasOrganizer->organizerId;
			}
			$organizerFairs = FairHasOrganizer::model()->findAllByAttributes(['organizerId' => $organizers]);
			$fairs = [];
			foreach ($organizerFairs as $organizerFair) {
				$fairs[] = $organizerFair->fairId;
			}
			$commandLeft->where(['in', 'IFNULL(temp_tasks.fairId, tasks.fairId)', $fairs]);
			$commandRight->where(['in', 'IFNULL(temp_tasks.fairId, tasks.fairId)', $fairs]);
		}

		echo json_encode($commandLeft->union($commandRight->text)->order('group, parent, uuid')->queryAll());
	}

	public function exponentTasks($fairId) {

		$command = Yii::app()->db->createCommand()
			->select([
				'tasks.id',
				'tasks.uuid',
				'tasks.parent',
				'tasks.fairId',
				'trfair.name as fairName',
				'trtasks.name as name',
				'trtasks.desc as desc',
				'IFNULL(users_tasks.date, tasks.date) as date',
				'IFNULL(users_tasks.duration, tasks.duration) as duration',
				'users_tasks.completeDate as complete',
				'IFNULL(users_tasks.color, tasks.color) as color',
				'tasks.completeButton'
			])
			->from('{{calendarfairtasks}} tasks')
			->join('{{fair}} fair', 'tasks.fairId = fair.id')
			->leftJoin('{{trfair}} trfair', 'trfair.trParentId = tasks.fairId AND trfair.langId = :langId', [':langId' => Yii::app()->language])
			->join('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid')
			->leftJoin('{{trcalendarfairtasks}} trtasks', 'tasks.uuid = trtasks.trParentId and trtasks.langId = :langId', [':langId' => Yii::app()->language])
			->where('users_tasks.userId = :userId', [':userId' => Yii::app()->user->id])
			->order('tasks.date');

		$fair = self::getFair($fairId);

		if ($fair) {
			$command->andWhere('tasks.fairId = :fairId', [':fairId' => $fair->id]);
		}else{
            $command->andWhere(array('NOT IN','tasks.fairId', Fair::getWorkFairsIds()));
        }

//        if ($fair) {
//            $myFairs = MyFair::getFairsId(Yii::app()->user->id);
//
//            $command->andWhere(array('IN','tasks.fairId',array_keys($myFairs)));
//            $command->andWhere(array('NOT IN','tasks.fairId',array(184, 11556, 10943)));
//
//        }

		echo json_encode($command->queryAll());
	}

	public function actionGetUserTasks($fairId = null) {

		$command = Yii::app()->db->createCommand()
			->select([
				'users_tasks.id',
				'users_tasks.parent',
				'users_tasks.fairId',
				'trfair.name as fairName',
				'users_tasks.name',
				'users_tasks.desc',
				'users_tasks.startDate',
				'users_tasks.endDate',
				'users_tasks.completeDate as completeDate'
			])
			->from('{{calendaruserstasks}} users_tasks')
			->leftJoin('{{fair}} fair', 'users_tasks.fairId = fair.id')
			->leftJoin('{{trfair}} trfair', 'trfair.trParentId = users_tasks.fairId')
			->where('trfair.langId = :langId OR trfair.langId IS NULL', [':langId' => Yii::app()->language])
			->andWhere('users_tasks.userId = :userId', [':userId' => Yii::app()->user->id]);

		$fair = self::getFair($fairId);
		if ($fair) {
			$command->andWhere('users_tasks.fairId = :fairId', [':fairId' => $fair->id]);
		}

		echo json_encode($command->queryAll());
	}

	public function actionGetTempTasks($fairId = null) {

		$command = Yii::app()->db->createCommand()
			->select([
				'temp_tasks.uuid',
				'temp_tasks.fairId',
				'trfair.name as fairName',
				'temp_tasks.parent as temp_parent',
				'tasks.parent as task_parent',
				'temp_tasks.group as temp_group',
				'tasks.group as task_group',
				'temp_tasks.name as temp_name',
				'trtasks.name as task_name',
				'temp_tasks.desc as temp_desc',
				'trtasks.desc as task_desc',
				'temp_tasks.duration as temp_duration',
				'tasks.duration as task_duration',
				'temp_tasks.date as temp_date',
				'tasks.date as task_date',
				'temp_tasks.color as temp_color',
				'tasks.color as task_color',
				'temp_tasks.completeButton as temp_completeButton',
				'tasks.completeButton as task_completeButton',
				'temp_tasks.first as temp_first',
				'tasks.first as task_first',
				'temp_tasks.action',
				'temp_tasks.editDate'
			])
			->from('{{calendarfairtemptasks}} temp_tasks')
			->leftJoin('{{calendarfairtasks}} tasks', 'temp_tasks.uuid = tasks.uuid')
			->leftJoin('{{trcalendarfairtasks}} trtasks', 'tasks.uuid = trtasks.trParentId and trtasks.langId = :langId', [':langId' => Yii::app()->language])
			->join('{{fair}} fair', 'temp_tasks.fairId = fair.id')
			->leftJoin('{{trfair}} trfair', 'trfair.trParentId = temp_tasks.fairId AND trfair.langId = :langId', [':langId' => Yii::app()->language]);

		$fair = self::getFair($fairId);
		if ($fair) {
			$command->andWhere('temp_tasks.fairId = :fairId', [':fairId' => $fair->id]);
		}

		echo json_encode($command->queryAll(), true);
	}

	public function actionEditTask() {

		$tempTask = new CalendarFairTempTasks();
		$tempTask['action'] = CalendarFairTempTasks::ACTION_INSERT;

		if ($_POST['id']) {
			$oldTask = CalendarFairTempTasks::model()->findByAttributes([
				'uuid' => $_POST['id']
			]);
			if ($oldTask) {
				$tempTask = $oldTask;
			} else {
				$fairTask = CalendarFairTasks::model()->findByAttributes([
					'uuid' => $_POST['id']
				]);
				if ($fairTask) {
					$tempTask->setAttributes($fairTask->attributes);
					$tempTask['action'] = CalendarFairTempTasks::ACTION_UPDATE;
				}
			}
		} else {
			$tempTask['uuid'] = Yii::app()->db->createCommand("SELECT UUID_SHORT();")->queryScalar();
		}

		// parent
		if (!empty($_POST['parent'])) {
			if (strpos($_POST['parent'], '-fair') !== false) {
				$tempTask['fairId'] = (int)$_POST['parent'];
				$tempTask['parent'] = null;
			} else {
				$parentTask = CalendarFairTasks::model()->findByAttributes([
					'uuid' => $_POST['parent']
				]);
				if (!$parentTask) {
					$parentTask = CalendarFairTempTasks::model()->findByAttributes([
						'uuid' => $_POST['parent']
					]);
				}
				if ($parentTask) {
					$tempTask['fairId'] = $parentTask['fairId'];
					$tempTask['parent'] = $parentTask['uuid'];
				}
			}
		}

		// group
		$tempTask['group'] = $tempTask['uuid'];
		if (!empty($_POST['group'])) {
			$groupTask = CalendarFairTasks::model()->findByAttributes([
				'uuid' => $_POST['group']
			]);
			if ($groupTask) {
				$tempTask['group'] = $groupTask['group'];
			}
		}

		// name
		if (isset($_POST['name'])) {
			$tempTask['name'] = $_POST['name'];
		}

		// desc
		if (isset($_POST['desc'])) {
			$tempTask['desc'] = $_POST['desc'];
		}

		// duration
		$_POST['duration'] .= '';
		if (ctype_digit($_POST['duration'])) {
			$tempTask['duration'] = (int)$_POST['duration'];
		}

		// date
		if (!empty($_POST['date'])) {
			$tempTask['date'] = $_POST['date'];
		}

		// color
		if (isset($_POST['color'])) {
			$tempTask['color'] = $_POST['color'];
		}

		// completeButton
		$tempTask['completeButton'] = isset($_POST['completeButton']) ? 1 : 0;

		// first
		$tempTask['first'] = isset($_POST['first']) ? 1 : 0;

		// editDate
		$tempTask['editDate'] = date('Y-m-d H:i:s');

		if ($tempTask->save()) {
			die(json_encode(['id' => $tempTask['uuid']]));
		}

		die(json_encode(['id' => 0]));
	}

	/**
	 * @param $id CalendarFairTempTasks[uuid]
	 */
	public function actionDeleteTask($id) {

		$tempTask = CalendarFairTempTasks::model()->findByAttributes([
			'uuid' => $id
		]);
		if (!$tempTask) {
			$tempTask = new CalendarFairTempTasks();
		}
		$tempTask['action'] = CalendarFairTempTasks::ACTION_DELETE;
		$tempTask['editDate'] = date('Y-m-d H:i:s');

		$fairTask = CalendarFairTasks::model()->findByAttributes([
			'uuid' => $id
		]);
		if ($fairTask) {
			$tempTask->setAttributes($fairTask->attributes);
			$tempTask->save();
		} else {
			$tempTask->delete();
			$childTasks = CalendarFairTempTasks::model()->findAllByAttributes([
				'parent' => $tempTask->uuid
			]);
			foreach ($childTasks as $childTask) {
				$this->actionDeleteTask($childTask['uuid']);
			}
		}
	}

	/**
	 * @param $fairId
	 */
	public function actionUpdateTasks($fairId) {

		$fair = self::getFair($fairId);
		if (!$fair) {
			die('error');
		}

		$tempTasks = CalendarFairTempTasks::model()->findAllByAttributes([
			'fairId' => $fairId
		]);

		Yii::app()->consoleRunner->run(
			'notification calendarUpdateTasks --fairId="' . $fairId . '"',
			true
		);
		sleep(1);

		foreach ($tempTasks as $tempTask) {
			if ($tempTask['action'] == -1) {
				$task = CalendarFairTasks::model()->findByAttributes([
					'uuid' => $tempTask['uuid']
				]);
				if (!$task) {
					echo 'error';
					continue;
				}
				if ($task->delete()) {
					$tempTask->delete();
					CalendarFairUsersTasks::model()->deleteAllByAttributes([
						'uuid' => $task['uuid']
					]);
					$this->deleteChildTasks($task['uuid']);
				}
			} else {
				$task = new CalendarFairTasks();
				if ($tempTask['action'] == 0) {
					$task = CalendarFairTasks::model()->findByAttributes([
						'uuid' => $tempTask['uuid']
					]);
					if (!$task) {
						echo 'error';
						continue;
					}
				}
				$task->setAttributes($tempTask->attributes);
				if ($task->save()) {
					$tempTask->delete();
				} else {
					print_r($task->errors);
				}
			}
		}
	}

	/**
	 * @param $parent CalendarFairTasks[parent]
	 */
	public function deleteChildTasks($parent) {

		$tasks = CalendarFairTasks::model()->findAllByAttributes([
			'parent' => $parent
		]);
		foreach ($tasks as $task) {
			$task->delete();
			CalendarFairUsersTasks::model()->deleteAllByAttributes([
				'uuid' => $task['uuid']
			]);
			$this->deleteChildTasks($task['uuid']);
		}
	}

	/**
	 * Insert or update userTask
	 */
	public function actionEditUserTask() {

		$userTask = new CalendarUsersTasks();
		$userTask->userId = Yii::app()->user->id;
		$userTask->parent = null;
		$userTask->name = '';
		$userTask->startDate = null;
		$userTask->endDate = null;
		$userTask->desc = '';

		if ($id = intval($_POST['id'])) {
			$userTask = CalendarUsersTasks::model()->findByAttributes([
				'id' => $id,
				'userId' => Yii::app()->user->id
			]);

			if (!$userTask) {
				json_encode(['id' => 0]);
				die();
			}
		}

		if (isset($_POST['parent'])) {
			$parent = $_POST['parent'];

			if ($parent) {
				if (strpos($parent, '-ut') !== false) {
					$parentTask = CalendarUsersTasks::model()->findByPk(substr($parent, 0, -3));
					$userTask->fairId = $parentTask->fairId;
				} elseif (strpos($parent, '-fair') !== false) {
					$userTask->fairId = substr($parent, 0, -5);
				} else {
					$parentTask = CalendarFairTasks::model()->findByAttributes([
						'uuid' => $parent
					]);
					$userTask->fairId = $parentTask->fairId;
				}
			}
			$userTask->parent = $parent;
			if (!$parent) {
				$userTask->parent = null;
				$userTask->fairId = null;
			}
		}
		if (!empty($_POST['name'])) {
			$userTask->name = $_POST['name'];
		}
		if (!empty($_POST['start_date'])) {
			$userTask->startDate = $_POST['start_date'];
		}
		if (!empty($_POST['end_date'])) {
			$userTask->endDate = $_POST['end_date'];
		}
		if (isset($_POST['desc'])) {
			$userTask->desc = $_POST['desc'];
		}

		if ($userTask->save()) {
			$this->saveUserTask($userTask);

			echo json_encode(['id' => $userTask->id]);
			die();
		}

		echo json_encode(['id' => 0]);
		die();
	}

	public function saveUserTask($userTask) {

		$childTasks = CalendarUsersTasks::model()->findAllByAttributes([
			'parent' => $userTask->id . '-ut'
		]);
		foreach ($childTasks as $childTask) {
			$childTask->fairId = $userTask->fairId;
			if ($childTask->save()) {
				$this->saveUserTask($childTask);
			}
		}
	}

	public function actionDeleteUserTask($id) {

		$userTask = CalendarUsersTasks::model()->findByPk(intval($id));
		if ($userTask) {
			$userTask->delete();
			$childTasks = CalendarUsersTasks::model()->findAllByAttributes([
				'parent' => $userTask->id . '-ut'
			]);
			foreach ($childTasks as $childTask) {
				$this->actionDeleteUserTask($childTask->id);
			}
		}
	}

	/**
	 * @param integer $id
	 * @param $complete bool
	 */
	public function actionAdd($id, $complete = false) {

		$task = CalendarFairTasks::model()->findByPk($id);
		if ($task) {

            $myFair = MyFair::model()->findByAttributes([
                'userId' => Yii::app()->user->id,
                'fairId' => $task->fairId,
                'status' => 1
            ]);

			if ($myFair) {
				$userTask = CalendarFairUsersTasks::model()->findByAttributes([
					'uuid' => $task['uuid'],
					'userId' => Yii::app()->user->id
				]);

				if (!$userTask) {
					$userTask = new CalendarFairUsersTasks;
					$userTask['uuid'] = $task['uuid'];
					$userTask['userId'] = Yii::app()->user->id;

					if ($task->date && $task->duration) {
						/*if ($task->date < date('Y-m-d H:i:s')) {*/
							if ($userTask->save() && $complete) {
								Yii::app()->consoleRunner->run(
									'notification calendarAddTask --taskId="' . $task['id'] . '" --userId="' . Yii::app()->user->id . '"',
									true
								);
							}
						/*}*/
					} else {
						if ($userTask->save() && $complete) {
							Yii::app()->consoleRunner->run(
								'notification calendarAddTask --taskId="' . $task['id'] . '" --userId="' . Yii::app()->user->id . '"',
								true
							);
						}
					}
				}
			}
		}
	}

	/**
	 * @param $id
	 * @param bool|false $complete
	 * @param $userId
	 */
	public function addByUserId($id, $complete = false, $userId) {

		$task = CalendarFairTasks::model()->findByPk($id);
		if ($task) {
			$myFair = MyFair::model()->findByAttributes([
				'userId' => $userId,
				'fairId' => $task->fairId,
				'status' => 1
			]);
			if ($myFair) {
				$userTask = CalendarFairUsersTasks::model()->findByAttributes([
					'uuid' => $task['uuid'],
					'userId' => $userId
				]);

				if (!$userTask) {
					$userTask = new CalendarFairUsersTasks;
					$userTask['uuid'] = $task['uuid'];
					$userTask['userId'] = $userId;

					if ($task->date && $task->duration) {
						if ($task->date < date('Y-m-d H:i:s')) {
							if ($userTask->save() && $complete) {
								Yii::app()->consoleRunner->run(
									'notification calendarAddTask --taskId="' . $task['id'] . '" --userId="' . $userId . '"',
									true
								);
							}
						}
					} else {
						if ($userTask->save() && $complete) {
							Yii::app()->consoleRunner->run(
								'notification calendarAddTask --taskId="' . $task['id'] . '" --userId="' . $userId . '"',
								true
							);
						}
					}
				}
			}
		}
	}

	/**
	 * @param $id CalendarFairTasks[id]
	 */
	public function actionComplete($id) {

		$task = CalendarFairTasks::model()->findByPk($id);

		if ($task) {
			$userTask = CalendarFairUsersTasks::model()->findByAttributes([
				'uuid'         => $task['uuid'],
				'userId'       => Yii::app()->user->id,
				'completeDate' => null
			]);

			if ($userTask) {
				$userTask['completeDate'] = date('Y-m-d H:i:s');
				$userTask->save();
				$tasks = CalendarFairTasks::model()->findAllByAttributes([
					'parent' => $task['uuid']
				]);
				foreach ($tasks as $task) {
					$this->actionAdd($task['id']);
				}
			}
		}
	}

	/**
	 * @param $id CalendarFairTasks[id]
	 * @throws CDbException
	 */
	public function actionDelete($id) {

		$task = CalendarFairTasks::model()->findByPk($id);
		if ($task) {
			$userTask = CalendarFairUsersTasks::model()->findByAttributes([
				'uuid'         => $task['uuid'],
				'userId'       => Yii::app()->user->id,
				'completeDate' => NULL
			]);
			if ($userTask !== NULL) {
				$userTask->delete();
			}

			if ($task['group']) {
				$groupTask = Yii::app()->db->createCommand()
					->select([
						'tasks.id',
						'IF(tasks.duration IS NULL, NOW(), IF(tasks.duration > 0, tasks.date, DATE_ADD(tasks.date, INTERVAL tasks.duration DAY))) as startDate',
						'IF(tasks.duration IS NULL, tasks.date, IF(tasks.duration < 0, tasks.date, DATE_ADD(tasks.date, INTERVAL tasks.duration DAY))) as endDate',
					])
					->from('{{calendarfairtasks}} tasks')
					->where('tasks.fairId = :fairId', [':fairId' => $task['fairId']])
					->andWhere('tasks.group = :group', [':group' => $task['group']])
					->having('NOW() BETWEEN startDate AND endDate')
					->queryRow();

				if ($groupTask) {
					$this->actionAdd($groupTask['id']);
				}
			}
		}
	}

	/**
	 * @param $id CalendarFairTasks[id]
	 * @throws CDbException
	 */
	public function deleteByUserId($id, $userId) {

		$task = CalendarFairTasks::model()->findByPk($id);

		if ($task) {
			$userTask = CalendarFairUsersTasks::model()->findByAttributes([
				'uuid'         => $task['uuid'],
				'userId'       => $userId,
			]);

			if ($userTask !== NULL) {
				$userTask->delete();
			}

			if ($task['group']) {
				$groupTask = Yii::app()->db->createCommand()
					->select([
						'tasks.id',
						'IF(tasks.duration IS NULL, NOW(), IF(tasks.duration > 0, tasks.date, DATE_ADD(tasks.date, INTERVAL tasks.duration DAY))) as startDate',
						'IF(tasks.duration IS NULL, tasks.date, IF(tasks.duration < 0, tasks.date, DATE_ADD(tasks.date, INTERVAL tasks.duration DAY))) as endDate',
					])
					->from('{{calendarfairtasks}} tasks')
					->where('tasks.fairId = :fairId', [':fairId' => $task['fairId']])
					->andWhere('tasks.group = :group', [':group' => $task['group']])
					->having('NOW() BETWEEN startDate AND endDate')
					->queryRow();

				if ($groupTask) {
					$this->addByUserId($groupTask['id'], FALSE, $userId);
				}
			}
		}
	}

	/**
	 * @param $token
	 * @throws CDbException
	 */
	public function actionDelAll($token = '') {

		if ($token == 'protoplan') {
			$criteria = new CDbCriteria;
			$criteria->addCondition('userId', Yii::app()->user->id);
			CalendarFairUsersTasks::model()->deleteAll($criteria);
			$this->redirect(Yii::app()->createUrl('/calendar/', array('langId' => Yii::app()->language)));
		}
	}

	public function check() {

		if (Yii::app()->user->role != User::ROLE_ORGANIZERS) {
			$firstTasks = Yii::app()->db->createCommand()
				->select(['tasks.id'])
				->from('{{calendarfairtasks}} tasks')
				->join('{{myfair}} myfair', 'myfair.fairId = tasks.fairId')
				->leftJoin('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid AND users_tasks.uuid IS NULL')
				->where('myfair.userId = :userId', [':userId' => Yii::app()->user->id])
				->andWhere('tasks.first = 1')
				->queryAll();

			foreach($firstTasks as $firstTask) {
				$this->actionAdd($firstTask['id']);
			}

			$completeTasks = Yii::app()->db->createCommand()
				->select(['tasks.id'])
				->from('{{calendarfairtasks}} tasks')
				->join('{{calendarfairtasks}} complete_tasks', 'tasks.parent = complete_tasks.uuid')
				->join('{{calendarfairuserstasks}} complete_users_tasks', 'complete_tasks.uuid = complete_users_tasks.uuid')
				->join('{{myfair}} myfair', 'myfair.fairId = tasks.fairId')
				->where('myfair.userId = :userId', [':userId' => Yii::app()->user->id])
				->andWhere('complete_users_tasks.completeDate IS NOT NULL')
				->andWhere('complete_users_tasks.userId = :userId', [':userId' => Yii::app()->user->id])
				->queryAll();

			foreach ($completeTasks as $completeTask) {
				$this->actionAdd($completeTask['id']);
			}

//			$tasks = Yii::app()->db->createCommand()
//				->select([
//					'tasks.id',
//					'IF(IFNULL(users_tasks.duration, tasks.duration) IS NULL,
//						tasks.date,
//						IF(tasks.duration < 0,
//							tasks.date,
//							DATE_ADD(tasks.date, INTERVAL tasks.duration DAY)
//						)
//					) as endDate',
//				])
//				->from('{{calendarfairtasks}} tasks')
//				->join('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid')
//				->where('users_tasks.userId = :userId', [':userId' => Yii::app()->user->id])
//				->andWhere('users_tasks.completeDate IS NULL')
//				->having('endDate < NOW()')
//				->queryAll();
//
//			foreach ($tasks as $task) {
//				$this->actionDelete($task['id']);
//			}

			$task1 = Yii::app()->db->createCommand()
				->select([
					'tasks.id',
				])
				->from('{{calendarfairtasks}} tasks')
				->join('{{calendarfairtasks}} task_1', 'tasks.group = task_1.uuid')
				->join('{{calendarfairuserstasks}} users_tasks', 'tasks.uuid = users_tasks.uuid')
				->where('users_tasks.userId = :userId', [':userId' => Yii::app()->user->id])
				->andWhere('users_tasks.completeDate IS NULL')
				->andWhere('task_1.id = 1')
				->queryRow();

			if ($task1) {
				$this->actionComplete($task1['id']);
			}
		}
	}

	public function actionGcal() {

		$client_id = '1011254887421-tc2qv28p897hrmhfpqkf4g4ac7s18fkh.apps.googleusercontent.com';
		$client_secret = 'ZjovNlf9deioXwyuw5LjHVdc';
		$redirect_uri = 'https://protoplan.pro/calendar/gantt/gcal/';

		$client = new Google_Client();

		$client->setApplicationName("Client_Library_Examples");
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->setAccessType("offline");

		// Gets us our refreshtoken

		$client->addScope(Google_Service_Calendar::CALENDAR);

		// For loging out.
		if (isset($_GET['logout'])) {
			unset($_SESSION['token']);
		}

		// The user accepted your access now you need to exchange it.
		if (isset($_GET['code'])) {
			$client->authenticate($_GET['code']);
			$_SESSION['token'] = $client->getAccessToken();
			$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
			header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
		}

		// The user has not authenticated we give them a link to login
		if (!isset($_SESSION['token'])) {
			$authUrl = $client->createAuthUrl();
			print "<a class='login' href='$authUrl'>Connect Me!</a>";
		}

		// We have access we can now create our service
		if (isset($_SESSION['token'])) {

			$client->setAccessToken($_SESSION['token']);
			$authUrl = $client->createAuthUrl();
			print "<a class='logout' href='$authUrl?logout=1'>LogOut</a><br>";
			$service = new Google_Service_Calendar($client);

			if (!isset($_SESSION['calendar_id'])) {
				$calendar = new Google_Service_Calendar_Calendar();
				$calendar->setSummary('Agrosalon');
				$calendar->setTimeZone('Europe/Moscow');
				$createdCalendar = $service->calendars->insert($calendar);
				$_SESSION['calendar_id'] = $createdCalendar->getId();
			} else {
				echo '<pre>';
				print_r($_SESSION['calendar_id']);
				echo '</pre>';
			}
		}
	}

	public function actionGcal2() {

		$client = new Google_Client();
		$client->setApplicationName("AppName");

		if (isset($_SESSION['token'])) {
			$client->setAccessToken($_SESSION['token']);
		}

		$credentials = new Google_Auth_AssertionCredentials(
			'protoplan-1289@appspot.gserviceaccount.com',
			[Google_Service_Calendar::CALENDAR],
			file_get_contents('ProtoPlan-c197d0ff130c.p12')
		);

		$client->setAssertionCredentials($credentials);

		if ($client->getAuth()->isAccessTokenExpired()) {
			$client->getAuth()->refreshTokenWithAssertion();
		}

		if ($_SESSION['token'] = $client->getAccessToken()) {
			$service = new Google_Service_Calendar($client);

			if (!isset($_SESSION['calendar_id'])) {
				$calendar = new Google_Service_Calendar_Calendar();
				$calendar->setSummary('Agrosalon');
				$calendar->setTimeZone('Europe/Moscow');
				$createdCalendar = $service->calendars->insert($calendar);
				$_SESSION['calendar_id'] = $createdCalendar->getId();
			} else {
				H::dump($_SESSION['calendar_id']);
			}
			H::dump($service->calendarList->listCalendarList()->getItems());

		} else {
			die('failed');
		}

	}

	/**
	 * @param $id
	 * @param $userId
	 */
	public function actionReject($id, $userId){
		$this->deleteByUserId($id, $userId);
	}
}