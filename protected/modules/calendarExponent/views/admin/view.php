<?php
/* @var $this AdminController */
/* @var $model CalendarFairTasks */

$this->breadcrumbs=array(
	'Calendar Fair Tasks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List CalendarFairTasks', 'url'=>array('index')),
	array('label'=>'Create CalendarFairTasks', 'url'=>array('create')),
	array('label'=>'Update CalendarFairTasks', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CalendarFairTasks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CalendarFairTasks', 'url'=>array('admin')),
);
?>

<h1>View CalendarFairTasks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parent',
		'group',
		'fairId',
		'name',
		'desc',
		'duration',
		'date',
		'color',
		'completeButton',
		'first',
	),
)); ?>
