<?php
/* @var $this DefaultController */

$app = Yii::app();
$assetManager = $app->getAssetManager();
$clientScript = $app->getClientScript();

$publishUrl = $assetManager->publish(Yii::getPathOfAlias('application.modules.calendarExponent.assets'), false, -1, YII_DEBUG);
$clientScript->registerCssFile($publishUrl . '/less/global.css');
$clientScript->registerCssFile($publishUrl . '/css/style.css');

$scheduler = $assetManager->publish($app->extensionPath . '/dhtmlxscheduler');
$clientScript->registerCssFile($scheduler . '/dhtmlxscheduler_flat.css');
$clientScript->registerScriptFile($scheduler . '/dhtmlxscheduler.js');
$clientScript->registerScriptFile($scheduler . '/ext/dhtmlxscheduler_year_view.js');
$clientScript->registerScriptFile($scheduler . '/locale/locale_' . $app->language . '.js');

$periodPicker = $assetManager->publish($app->extensionPath . '/periodPicker/build');
$clientScript->registerCssFile($periodPicker . '/jquery.periodpicker.min.css');
$clientScript->registerCssFile($periodPicker . '/jquery.timepicker.min.css');
$clientScript->registerScriptFile($periodPicker . '/jquery.periodpicker.full.min.js');
?>

<div class="orgplan" style="margin: 0; padding: 0; width: 100%; height: 100%; position: inherit;">

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<?= $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('CalendarExponentModule.calendarExponent', 'Gantt view') . (Yii::app()->user->role == User::ROLE_ORGANIZERS ? ' ('.Yii::t('CalendarExponentModule.calendarExponent', 'Organizer').')' : ''),
				'description' => empty($fair) ? Yii::t('CalendarExponentModule.calendarExponent', 'all fairs') : $fair->name
			)); ?>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12 calendar-buttons">

			<div class="pull-left">
				<a class="b-button gantt-button" href="<?= Yii::app()->createUrl('calendarExponent', $fair ? ['fairId' => $fair->id] : []); ?>"><?php echo Yii::t('CalendarExponentModule.calendarExponent','Gantt view'); ?></a>
				<span class="b-button d-bg-warning d-text-light calendar-button"><?php echo Yii::t('CalendarExponentModule.calendarExponent','calendar'); ?></span>
				<!--
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				-->
			</div>

			<div class="pull-right">
				<a class="b-button d-bg-success d-text-light create-button" data-icon="" href="#"><?php echo Yii::t('CalendarExponentModule.calendarExponent','create task'); ?></a>
			</div>

			<div style="clear: both;"></div>
		</div>
	</div>

	<div id="scheduler_here" class="dhx_cal_container">
		<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_cal_tab" name="day_tab" data-tab="day" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" data-tab="week" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" data-tab="month" style="right:76px;"></div>
			<div class="dhx_cal_tab" name="year_tab" data-tab="year" style="right:280px;"></div>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>
	</div>

</div>

<div id="modal-edit-task" class="modal--sign-up modal hide fade" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header js-fair-name"></span>
				<span class="text-size-20 js-task-text"></span>
			</div>
		</h3>
	</div>
	<div class="modal-body">
		<div class="row-fluid modal__modal-content">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<div class="cols-row offset-small-l-3 offset-small-r-3">
					<div class="col-md-12">
						<div class="js-start-date" style="font-size: 20px; margin: 10px 0;">
							<?=Yii::t('CalendarExponentModule.calendarExponent', 'Beginning date');?>: <span style="float: right;"></span>
						</div>
						<div class="js-end-date" style="font-size: 20px; margin: 10px 0">
							<?=Yii::t('CalendarExponentModule.calendarExponent', 'Deadline');?>: <span style="float: right;"></span>
						</div>
						<div class="js-complete-date" style="font-size: 20px; margin: 10px 0">
							<?=Yii::t('CalendarExponentModule.calendarExponent', 'Completed');?>: <span style="float: right;"></span>
						</div>
						<div class="terms-of-use js-desc"></div>
						<div style="text-align: center;">
							<button class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-complete-button" type="button" name="complete"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Done');?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-user-task" class="modal--sign-up modal hide fade" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header">
					<?=Yii::t('CalendarExponentModule.calendarExponent', 'Private task');?>
				</span>
				<span class="text-size-20 js-task-text" data-add-text="<?=Yii::t('CalendarExponentModule.calendarExponent', 'Adding new task');?>">
					<?=Yii::t('CalendarExponentModule.calendarExponent', 'Adding new task');?>
				</span>
			</div>
		</h3>
	</div>
	<div class="modal-body" style="padding: 15px !important;">
		<div class="row-fluid">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<form id="task">
					<div class="cols-row">
						<div class="col-md-12">

							<div class="control-group">
								<label class="task-label control-label" for="parent"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Parent task');?></label>
								<div class="controls">
									<select id="parent" name="parent"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="name"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Task name');?> <span class="required">*</span></label>
								<div class="controls">
									<input class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="<?=Yii::t('CalendarExponentModule.calendarExponent', 'Enter task name');?>" name="name" id="name" type="text">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="start_date"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Period');?> <span class="required">*</span></label>
								<div class="controls js-start-date-wrapper">
									<input id="start_date" name="start_date" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="text">
									<input id="end_date" name="end_date" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="text">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="desc"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Task caption');?></label>
								<div class="controls">
									<textarea style="resize: none; padding: 9px; width: 100%;" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="<?=Yii::t('CalendarExponentModule.calendarExponent', 'Enter task description');?>" name="desc" id="desc"></textarea>
								</div>
							</div>

							<div style="text-align: center; margin: 20px 0 0;">
								<button style="margin: 0; display: none;" class="b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn js-complete-button" type="button" name="complete"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Done');?></button>
							</div>

							<input type="hidden" name="id" id="id" value="">

							<div style="margin: 20px 0 0;">
								<button style="margin: 0; float: left;" class="b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn js-cancel-button" type="button" data-edit-text="Удалить" data-add-text="<?=Yii::t('CalendarExponentModule.calendarExponent', 'Cancel');?>"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Cancel');?></button>
								<button style="margin: 0; float: right;" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-submit-button" type="button" data-edit-text="Сохранить" data-add-text="<?=Yii::t('CalendarExponentModule.calendarExponent', 'Add');?>"><?=Yii::t('CalendarExponentModule.calendarExponent', 'Add');?></button>
								<div style="clear: both;"></div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	$app = Yii::app();
	$am = $app->getAssetManager();
	$cs = $app->getClientScript();
	$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.calendarExponent.assets'), false, -1, YII_DEBUG);
?>
<script type="text/javascript" src="<?=$publishUrl?>/js/jquery.multiselect.js"></script>

<script type="text/javascript">

	var fair = <?= $fair ? $fair->id : 0 ?>;

	var res = {
		data: [],
		fairs: [],
		colored: []
	};

	function init(date, tab) {

		res = {
			data: [],
			fairs: [],
			colored: []
		};

		var request = $.ajax({
			url: "<?= Yii::app()->createUrl('calendarExponent/calendar/all', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		});

		request.done(function(data) {
			for (var i in data) {
				res.data[i] = {
					id: data[i].id + '-cal',
					start_date: data[i].startDate,
					end_date: data[i].endDate,
					text: data[i].name,
					cssClass: data[i].cssClass,
					type: 'calendarDate'
				};
				if (data[i].cssClass) {
					var msInDay = 24 * 60 * 60 * 1000;
					var startDate = new Date(data[i].startDate + ' 00:00:00');
					var endDate = new Date(data[i].endDate + ' 00:00:00');
					res.colored[i] = {
						date: startDate,
						cssClass: data[i].cssClass
					};
					if (endDate - startDate > msInDay) {
						var end = (endDate - startDate) / msInDay;
						for (var j = 1; j < end; j++) {
							res.colored[i+'.'+j] = {
								date: new Date(startDate.getTime() + j * msInDay),
								cssClass: data[i].cssClass
							};
						}
					}
				}
			}
		});

		var organizerTaskRequest = $.ajax({
			url: "<?= Yii::app()->createUrl('calendarExponent/gantt/all', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		});

		organizerTaskRequest.done(function(data) {

			var resData = [];
			var fairs = {};

			for (var i in data) {

				var iData = data[i];
				var id = iData.id;
				var startDate = new Date(Date.parse(iData.date));
				var endDate = new Date(Date.parse(iData.date));

				resData[id] = iData;

				if (!iData.date && resData[iData.parent]) {
					startDate = new Date(resData[iData.parent].endDate.getTime() + 1000);
				}

				if (iData.duration > 0) {
					endDate = new Date(startDate.getTime() + (iData.duration * 24 * 60 * 60 * 1000) - 1000);
				} else if (iData.duration < 0) {
					endDate = startDate;
					startDate = new Date(endDate.getTime() + (iData.duration * 24 * 60 * 60 * 1000) + 1000);
				} else {
					var now = new Date();
					if (startDate.getTime() > now.getTime()) {
						startDate = now;
					}
					if (resData[iData.parent]) {
						startDate = new Date(resData[iData.parent].endDate.getTime() + 1000);
					}
				}

				var text = iData.name;
				if (iData.complete) {
					text = '<del>' + text + '</del>';
					startDate = new Date(Date.parse(iData.complete));
					startDate.setHours(0);
					startDate.setMinutes(0);
					startDate.setSeconds(0);
					endDate = new Date(startDate.getTime() + 24 * 60 * 60 * 1000);
				}

				resData[id].startDate = startDate;
				resData[id].endDate = endDate;

				fairs[iData.fairId] = {
					id: iData.fairId + '-fair',
					parent: 0,
					text: iData.fairName
				};

				if (!iData.parent) {
					iData.parent = iData.fairId + '-fair';
				}

				res.data.push({
					id: iData.id,
					parent: iData.parent,
					fair_name: iData.fairName,
					complete_btn: iData.completeButton,
					start_date: startDate,
					end_date: endDate,
					complete: iData.complete,
					text: text,
					desc: iData.desc,
					cssClass: '',
					type: 'task',
					color: iData.color ? iData.color : '#43ade3'
				});
			}

			for (i in fairs) {
				res.fairs.push({
					id: fairs[i].id,
					parent: fairs[i].parent,
					text: fairs[i].text
				});
			}
		});

		var userTaskRequest = $.ajax({
			url: "<?= Yii::app()->createUrl('calendarExponent/gantt/getUserTasks', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		});

		userTaskRequest.done(function (data) {
			for (var i in data) {
				var iData = data[i];

				var startDate = new Date(Date.parse(iData.startDate));
				var endDate = new Date(Date.parse(iData.endDate));
				var text = iData.name;
				if (iData.complete) {
					text = '<del>' + text + '</del>';
					startDate = new Date(Date.parse(iData.complete));
					startDate.setHours(0);
					startDate.setMinutes(0);
					startDate.setSeconds(0);
					endDate = new Date(startDate.getTime() + 24 * 60 * 60 * 1000);
				}

				res.data.push({
					id: iData.id + '-ut',
					parent: iData.parent,
					start_date: new Date(Date.parse(iData.startDate)),
					end_date: endDate,
					text: text,
					desc: iData.desc,
					cssClass: '',
					type: 'userTask',
					color: '#8cc152'
				});
			}
		});

		scheduler.templates.month_date_class = function(date, today) {
			var cssClass = '';
			for (var i in res.colored) {
				if (res.colored[i].date.getTime() == date.getTime()) {
					cssClass = res.colored[i].cssClass;
				}
			}
			return cssClass;
		};

		scheduler.templates.event_class = function(start, end, event) {
			if (event.color) {
				event.textColor = '#fff';
			}
			if (!event.type) {
				event.color = '#8cc152';
			}

			return event.cssClass ? event.cssClass : '';
		};

		scheduler.templates.tooltip_text = function(start,end,event) {
			return event.text;
		};

		scheduler.config.xml_date = "%Y-%m-%d %H:%i";
		scheduler.config.multi_day = true;
		scheduler.config.icons_select = ['icon_details', 'icon_edit'];
		scheduler.config.icons_edit = ['icon_save', 'icon_cancel'];

		scheduler.init('scheduler_here', date, tab);
		scheduler.parse(res.data, "json");
	}

	init(new Date(), 'year');



	function createPeriodPicker(ppStartDate, ppEndDate) {

		var startDate = new Date(ppStartDate);
		var endDate = new Date(ppEndDate);

		if (!ppStartDate) {
			startDate = new Date();
			startDate.setHours(0);
			startDate.setMinutes(0);
			startDate.setSeconds(0);
		}

		if (!ppEndDate) {
			endDate = new Date();
			endDate.setHours(23);
			endDate.setMinutes(55);
			endDate.setSeconds(0);
		}

		var $oldStartDate = $('#start_date');
		var $newStartDate = $oldStartDate.clone();
		$oldStartDate.remove();
		$('.period_picker_input').remove();

		$newStartDate.val(moment(startDate).format('YYYY-MM-DD HH:mm:ss'));

		$('.js-start-date-wrapper').append($newStartDate);

		$('#end_date').val(moment(endDate).format('YYYY-MM-DD HH:mm:ss'));

		$newStartDate.periodpicker({
			end: '#end_date',
			lang: 'ru',
			formatDateTime: 'YYYY-MM-DD HH:mm:ss',
			timepicker: true,
			minDate: moment(new Date()).format('YYYY-MM-DD'),
			defaultEndTime: moment(endDate).format('HH:mm'),
			timepickerOptions: {
				hours: true,
				minutes: true,
				seconds: false,
				ampm: false,
				defaultTime: moment(startDate).format('HH:mm'),
				twelveHoursFormat: false,
				steps:[1,5,2,1]
			}
		});

		// $('#end_date').val(moment(endDate).format('YYYY-MM-DD HH:mm:ss'));
		$newStartDate.periodpicker('change');
	}

	function schedulerReload() {
		var minDate = scheduler._min_date;
		var maxDate = scheduler._max_date;
		var mode = scheduler._mode;
		var date = new Date(minDate.getTime() + ((maxDate.getTime() - minDate.getTime()) / 2));
		scheduler.clearAll();
		init(date, mode);
	}

	function fStructureRes(notId) {
		if (!fair) {
			$parent.html('<option value=""><?=Yii::t('CalendarExponentModule.calendarExponent', 'Empty');?></option>');
		}
		structureRes(0, 0, notId);
		$parent.multiselect('reload');
	}

	function structureRes(parent, depth, notId) {
		var tab = '- ';
		var pref = '';
		for (var j = 0; j < depth; j++) {
			pref += tab;
		}
		var i = 0;

		for (i in res.fairs) {
			if (res.fairs[i].parent == parent) {
				var text = pref + strip(res.fairs[i].text);
				$parent.append($("<option></option>").attr("value", res.fairs[i].id).text(text));
				structureRes(res.fairs[i].id, (depth + 1 - 0), notId);
			}
		}

		for (i in res.data) {
			if (res.data[i].parent !== undefined && res.data[i].parent == parent && res.data[i].id != notId) {
				var text = pref + strip(res.data[i].text);
				$parent.append($("<option></option>").attr("value", res.data[i].id).text(text));
				structureRes(res.data[i].id, (depth + 1 - 0), notId);
			}
		}
	}

	function strip(html) {
		var tmp = document.createElement("DIV");
		tmp.innerHTML = html;
		return tmp.textContent || tmp.innerText || "";
	}

	var deleteEvent = null;

	// добавление личной задачи через двойной клик на дату календаря
	scheduler.attachEvent("onBeforeLightbox", function(id) {
		var event = scheduler.getEvent(id);

		if (!event.type && event.end_date.getTime() < new Date().getTime()) {
			scheduler.deleteEvent(event.id);
			return false;
		}

		var $modal, task;

		if (event.type == 'task') {

			task = event;
			$modal = $('#modal-edit-task');
			var $startDate = $modal.find('.js-start-date');
			var $endDate = $modal.find('.js-end-date');
			var $completeBtn = $modal.find('.js-complete-button');
			var $completeDate = $modal.find('.js-complete-date');

			$modal.find('.js-fair-name').text(task.fair_name);
			$modal.find('.js-task-text').html(task.text);

			var options = {
				year: 'numeric',
				month: 'long',
				day: 'numeric',
				timezone: 'UTC',
				hour: 'numeric',
				minute: 'numeric',
				second: 'numeric'
			};

			$modal.find('.js-start-date span').text(task.start_date.toLocaleString("<?=Yii::app()->language?>", options));
			$endDate.find('span').text(task.end_date.toLocaleString("<?=Yii::app()->language?>", options));
			$modal.find('.js-desc').text('').show();
			if (task.desc) {
				$modal.find('.js-desc').text(task.desc);
			}

			$startDate.show();
			$endDate.show();
			$completeBtn.hide();
			$completeDate.hide();
			if (task.complete) {
				$completeDate.find('span').text(task.complete.toLocaleString("<?=Yii::app()->language?>", options));
				$completeDate.show();
				$endDate.hide();
				$startDate.hide();
				$modal.find('.js-desc').hide();
			} else if (task.complete_btn == '1') {
				$completeBtn.attr('data-id', id);
				$completeBtn.data('id', id);
				$completeBtn.show();
			}

			$modal.modal('show');
		}

		if (event.type == 'userTask') {

			task = event;
			$modal = $('#modal-user-task');
			var $parent = $modal.find('#parent');
			var $name = $modal.find('#name');
			var $desc = $modal.find('#desc');
			var $id = $modal.find('#id');

			$name.val(task.text);

			$('.js-task-text').text(task.text);

			createPeriodPicker(task.start_date, task.end_date);

			$desc.text(task.desc);

			var $cancelBtn = $('.js-cancel-button');
			$cancelBtn.text($cancelBtn.attr('data-edit-text'));

			var $submitBtn = $('.js-submit-button');
			$submitBtn.text($submitBtn.attr('data-edit-text'));

			$id.val(parseInt(task.id));
			fStructureRes(task.id);
			$parent.val(task.parent);
			$parent.multiselect('reload');
			$modal.modal('show');
		}

		if (!event.type) {
			deleteEvent = event.id;
			fStructureRes(null);

			var startDate = event.start_date;
			var endDate = new Date(event.start_date);
			endDate.setHours(23);
			endDate.setMinutes(55);
			endDate.setSeconds(0);

			createPeriodPicker(startDate, endDate);
			$('#modal-user-task').modal('show');
		}

		return false;
	});

	var $modalUserTask = $('#modal-user-task');

	$modalUserTask.on('hidden.bs.modal', function() {

		if (deleteEvent) {
			scheduler.deleteEvent(deleteEvent);
		}

		var $form = $modalUserTask.find("#task");
		$form.trigger('reset');
		$form.find("#desc").text('');
		$form.find("#id").val('');
		$("#parent").multiselect('reload');

		var $cancelBtn = $('.js-cancel-button');
		$cancelBtn.text($cancelBtn.attr('data-add-text'));

		var $submitBtn = $('.js-submit-button');
		$submitBtn.text($submitBtn.attr('data-add-text'));

		var $taskText = $modalUserTask.find('.js-task-text');
		$taskText.text($taskText.attr('data-add-text'));

		$form.find('.control-group.error').removeClass('error');

		//$('.js-complete-button').css('display', 'none');
	});

	$('.js-cancel-button').on('click', function() {
		var id = $("#id").val();
		var task = null;
		if (id) {
			task = scheduler.getEvent(id + '-ut');
		}
		if (task) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendarExponent/gantt/deleteUserTask'); ?>",
				type: "GET",
				data: {id: id},
				dataType: 'json'
			});
			schedulerReload();
		}
		$('#modal-user-task').modal('hide');
	});

	scheduler.attachEvent("onBeforeDrag", function (id, mode, e){
		//any custom logic here
		var event = scheduler.getEvent(id);

		if (!event) {
			return true;
		}

		if (event.type == 'userTask') {
			return true;
		}

		return false;
	});

	// добавление события в дне (нажатие на галочку)
	scheduler.attachEvent("onEventAdded", function(id, e){
		var event = scheduler.getEvent(id);

		if (event.end_date.getTime() < new Date().getTime()) {
			scheduler.deleteEvent(event.id);
			return false;
		}

		editUserTask({
			id: '',
			parent: '184-fair',
			start_date: moment(event.start_date).format('YYYY-MM-DD HH:mm:ss'),
			end_date: moment(event.end_date).format('YYYY-MM-DD HH:mm:ss'),
			name: event.text
		});
		deleteEvent = null;
	});

	// изменение события в дне (перетаскивание)
	scheduler.attachEvent("onEventChanged", function(id, e){
		var event = scheduler.getEvent(id);

		if (event.type != 'userTask') {
			return false;
		}

		if (event.end_date.getTime() < new Date().getTime()) {
			schedulerReload();
			return false;
		}

		editUserTask({
			id: event.id,
			parent: event.parent,
			start_date: moment(event.start_date).format('YYYY-MM-DD HH:mm:ss'),
			end_date: moment(event.end_date).format('YYYY-MM-DD HH:mm:ss'),
			name: event.text
		});
		deleteEvent = null;
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на 'Создать задачу'
	$('.create-button').on('click', function() {

		var $taskText = $('.js-task-text');
		$taskText.text($taskText.attr('data-add-text'));
		fStructureRes(0);
		createPeriodPicker(0, 0);
		$('#modal-user-task').modal('show');
	});

	$('#modal-edit-task').on('click', '.js-complete-button', function() {

		var id = $(this).attr('data-id');
		$.ajax({
			url: "<?= Yii::app()->createUrl('calendarExponent/gantt/complete'); ?>",
			async: false,
			data: {id: id},
			success: function() {
				$('#modal-edit-task').modal('hide');
				schedulerReload();
			}
		});
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие 'Добавить' или 'Сохранить'
	$('.js-submit-button').on('click', function() {

		var $userTaskModal = $('#modal-user-task');
		var $userTaskForm = $userTaskModal.find("#task");
		var $name = $userTaskForm.find('#name');
		var $startDate = $userTaskForm.find('#start_date');

		var post = {};

		var errors = false;

		$.each($userTaskForm.serializeArray(), function(i, field) {
			post[field.name] = field.value;
		});

		if (!post.name) {
			$name.closest('.control-group').addClass('error');
			errors = true;
		} else {
			$name.closest('.control-group').removeClass('error');
		}

		if (!post.start_date || !post.end_date) {
			$startDate.closest('.control-group').addClass('error');
			errors = true;
		} else {
			$startDate.closest('.control-group').removeClass('error');
		}
		if (!errors) {
			editUserTask(post);
			$userTaskModal.modal('hide');
		}
	});


	function editUserTask(post) {

		$.ajax({
			url: "<?= Yii::app()->createUrl('calendarExponent/gantt/editUserTask'); ?>",
			type: "POST",
			data: post,
			success: function (data) {
				if (data.id) {
					schedulerReload();
				}
			},
			dataType: 'json'
		});
	}







	var $parent = $('#parent');

	$parent.multiselect({
		placeholder: '<?=Yii::t('CalendarExponentModule.calendarExponent', 'Choose parent task');?>',
		showCheckbox: false,
		maxHeight: 250,
		search: true,
		searchOptions: {
			'default': 'Поиск'
		},
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
		}
	});

	$("a[href='#']").on('click', function() {
		return false;
	});

</script>