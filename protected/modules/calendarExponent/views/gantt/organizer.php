<?php
/**
 * @var $this GanttController
 */

$app = Yii::app();
$assetManager = $app->getAssetManager();
$clientScript = $app->getClientScript();

$publishUrl = $assetManager->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$clientScript->registerCssFile($publishUrl . '/less/global.css');

$scheduler = $assetManager->publish($app->extensionPath . '/dhtmlxgantt');
$periodPicker = $assetManager->publish($app->extensionPath . '/periodPicker/build');

$clientScript->registerCssFile($scheduler . '/dhtmlxgantt.css');
$clientScript->registerScriptFile($scheduler . '/dhtmlxgantt.js');
$clientScript->registerScriptFile($scheduler . '/ext/dhtmlxgantt_marker.js');
$clientScript->registerScriptFile($scheduler . '/locale/locale_' . $app->language . '.js');

$clientScript->registerCssFile($periodPicker . '/jquery.periodpicker.min.css');
$clientScript->registerCssFile($periodPicker . '/jquery.timepicker.min.css');
$clientScript->registerScriptFile($periodPicker . '/jquery.periodpicker.full.min.js');
?>

<style>
	html, body {
		height: 100%;
	}

	.container-fluid {
		height: calc(100% - 50px);
		position: relative;
	}

	.nav-list__side-left {
		height: calc(100% - 50px) !important;
	}

	.gantt_task_content {
		cursor: pointer;
	}


	.dhx_cal_navline {
		position: relative;
		height: 59px;
		left: 0;
		top: 0;
	}

	.dhx_cal_navline .dhx_cal_tab {
		color: #5780AD;
		font-size: 13px;
		font-weight: bolder;
		padding-top: 0;
		text-decoration: none;
		width: 60px;
		position: absolute;
		font-family: "Segoe UI", Arial;
		top: 14px;
		white-space: nowrap;
		border-radius: 0;
		border: none;
		height: 30px;
		line-height: 30px;
		background: 0 0;
		text-align: center;
		cursor: pointer;
	}

	.dhx_cal_navline .dhx_cal_tab.year {
		width: 70px;
	}

	.dhx_cal_navline .dhx_cal_tab:hover {
		text-decoration: underline;
	}

	.dhx_cal_navline .dhx_cal_tab.active {
		background-color: #5780AD;
		border: none;
		color: #FFF;
		font-weight: lighter;
		text-decoration: none;
		cursor: default;
	}

	.dhx_cal_navline .dhx_cal_tab input[type=radio] {
		display: none;
	}



	.calendar-buttons {
		height: 40px;
	}

	.calendar-buttons .b-button {
		text-decoration: none;
		text-align: center;
		height: 40px;
		float: left;
		margin: 0;
	}

	.calendar-buttons .calendar-button {
		border: 1px solid #d1d1d1;
		border-left: none;
		margin-left: 0;
		color: #505055;
	}

	.calendar-buttons .gantt-button {
		margin-right: 0;
		cursor: default;
	}

	.calendar-buttons .b-button-icon {
		margin-right: 10px;
	}

	.calendar-buttons .create-button {
		width: 140px;
		margin-right: 40px;
		text-align: center;
	}

	.calendar-buttons .create-button:hover {
		text-decoration: none;
	}

	.calendar-buttons .create-button:before {
		display: block;
		font-size: 24px;
		height: 30px;
		width: 40px;
		position: absolute;
		right: 10px;
		top: 0;
		padding-top: 10px;
		background: #8cc152;
	}


	.calendar-buttons .update-button {
		width: 140px;
		margin-right: 50px;
		text-align: center;
		position: relative;
	}

	.calendar-buttons .update-button:hover {
		text-decoration: none;
	}

	.calendar-buttons .update-button:before {
		display: block;
		font-size: 24px;
		height: 30px;
		width: 40px;
		position: absolute;
		right: -40px;
		top: 0;
		padding-top: 10px;
		background: #2b98d5;
	}











	.gantt_marker .gantt_marker_content {
		padding: 2px 5px;
		bottom: 0;
		left: 2px;
	}

	.gantt_add,
	.gantt_grid_head_add {
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAJElEQVR42mNgoBXo2R/0HxmPGkDAAHTFxOLhZMBoOqCRAcQCAMmU+oEi72HWAAAAAElFTkSuQmCC');
	}

	.gantt_task_drag.task_right {
		cursor: w-resize;
	}

	.gantt_task_drag.task_left {
		cursor: e-resize;
	}

	.gantt_task_progress_drag {
		cursor: move;
	}

	.gantt_tree_content {
		cursor: default;
	}

	/*
	.gantt_task_line:hover .gantt_link_control div {
		display: none;
	}
	*/
	.gantt_task_progress_drag {
		display: none !important;
	}





	.ms-options-wrap,
	.ms-options-wrap * {
		box-sizing: border-box;
	}

	.ms-options-wrap:focus,
	.ms-options-wrap {
		width: 100%;
		text-align: left;
		border: none;
		outline: none;
		white-space: nowrap;
		color: #515056;
		background-color: #efefef;
		padding: 0 10px;
		margin: 0;
		cursor: pointer;
		position: relative;
		font-size: 16px;
		letter-spacing: 0;
	}

	.ms-options-wrap:hover {
		color: #515056;
		background: #e0e0e0;
	}

	.ms-options-wrap:after {
		content: '';
		display: block;
		position: absolute;
		top: 43%;
		right: 15px;
		width: 9px;
		height: 6px;
		overflow: hidden;
		background: url('http://orgplan/assets/24bde9c4/img/dropdown/caret-black.png') no-repeat;
	}

	.ms-options-wrap > button {
		overflow: hidden;
		width: 100%;
		text-align: left;
		border: none;
		outline: none;
		white-space: nowrap;
		color: #515056;
		background-color: inherit;
		margin: 0;
		padding: 10px 0;
		cursor: pointer;
		position: relative;
		font-size: 16px;
		letter-spacing: 0;
	}

	.ms-options-wrap > .ms-options {
		position: absolute;
		left: 0;
		width: 100%;
		margin-top: 1px;
		margin-bottom: 20px;
		background: white;
		z-index: 2000;
		border: 1px solid #aaa;
		min-height: 1px !important;
		max-height: 200px;
	}

	.ms-options-wrap > .ms-options:before {
		/*
		display: block;
		position: absolute;
		left: 17px;
		top: 43px;
		overflow: auto;
		content: '';
		z-index: 2;
		background: url('http://orgplan/assets/cecb2559/img/dropdown/mark.png');
		width: 14px;
		height: 8px;
		*/
	}

	.ms-options-wrap > .ms-options > .ms-search {
		padding: 10px 10px 0;
		position: relative;
	}

	.ms-options-wrap > .ms-options > .ms-search:before {
		content: '';
		display: block;
		position: absolute;
		top: 19px;
		left: 18px;
		width: 21px;
		height: 21px;
		background: url('http://orgplan/assets/cecb2559/img/sprites/icon-sprites-white-21px.png') 0 -148px no-repeat;
	}

	.ms-options-wrap > .ms-options > .ms-search input {
		width: 100%;
		border: none;
		border-radius: 0;
		-o-border-radius: 0;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		line-height: 22px;
		box-shadow: none;
		font-size: 16px;
		color: #505055;
		display: block;
		height: 40px;
		padding-left: 40px;
		background: #e0e0e0;
		margin: 0;
	}

	.ms-options-wrap > .ms-options > .ms-search input::-webkit-input-placeholder {
		line-height: 25px;
		font-size: 14px;
		color: #afafaf;
	}

	.ms-options-wrap > .ms-options > .ms-search input::-moz-placeholder {
		line-height: 25px;
		font-size: 14px;
		color: #afafaf;
	}

	.ms-options-wrap > .ms-options .ms-selectall {
		display: inline-block;
		font-size: .9em;
		text-transform: lowercase;
		text-decoration: none;
	}
	.ms-options-wrap > .ms-options .ms-selectall:hover {
		text-decoration: underline;
	}

	.ms-options-wrap > .ms-options > .ms-selectall.global {
		margin: 4px 5px;
	}

	.ms-options-wrap > .ms-options > ul {
		margin: 0;
		padding: 10px;
		list-style-type: none;
	}

	.ms-options-wrap > .ms-options > ul > li.optgroup {
		padding: 5px;
	}
	.ms-options-wrap > .ms-options > ul > li.optgroup + li.optgroup {
		border-top: 1px solid #aaa;
	}

	.ms-options-wrap > .ms-options > ul > li.optgroup .label {
		display: block;
		padding: 5px 0 0 0;
		font-weight: bold;
	}

	.ms-options-wrap > .ms-options > ul label {
		line-height: 18px;
		font-size: 16px;
		position: relative;
		display: block;
		margin: 0;
		padding: 3px 0 3px 15px;
		cursor: pointer;
		color: #515056;
		text-align: left;
	}

	.ms-options-wrap > .ms-options > ul li.selected label,
	.ms-options-wrap > .ms-options > ul label:hover {
		color: #515056;
		background: #efefef;
	}

	.ms-options-wrap > .ms-options > ul input[type="checkbox"] {
		margin-right: 5px;
		position: absolute;
		left: 4px;
		top: 7px;
	}


	.task-label {
		margin: 0;
	}

	.task-input {
		width: 100%;
		margin: 0 !important;
		background: #efefef;
	}


	.period_picker_input {
		width: 59% !important;
	}

	.period_picker_input,
	.period_picker_input:before {
		width: 100%;
		text-align: left;
		outline: none;
		white-space: nowrap;
		color: #515056;
		background-color: #efefef;
		margin: 0;
		cursor: pointer;
		position: relative;
		font-size: 16px;
		letter-spacing: 0;
		border: none;
		border-radius: 0;
		-o-border-radius: 0;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		padding: 9px;
		height: 40px;
		line-height: 22px;
		box-shadow: none;
		font-family: MyriadPro-Light;
	}

	.period_picker_input:hover {
		color: #515056;
		background: #e0e0e0;
	}

	.period_picker_input:before {
		display: none;
	}

	.period_picker_input .icon_calendar {
		display: none;
	}

	.period_picker_input .period_button_text {
		padding: 0;
	}

	.icon_clear {
		margin-top: 3px;
		float: right;
	}
	#date {
		width: 59%;
	}
	#duration {
		float: right;
		width: 39%;
	}

	.modal--sign-up .modal-body {
		max-height: none;
	}

	.color-label {
		float: left;
		margin-right: 10px;
		width: 18px;
		height: 18px;
	}

	.control-group.error input, .control-group.error .period_picker_input {
		background: #fbc2b6 !important;
		box-shadow: none;
	}

	#modal-user-task .control-group>label {
		display: inline-block;
	}
</style>

<div class="orgplan" style="margin: 0; padding: 0; width: 100%; height: 100%; position: inherit;">

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<?= $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('CalendarExponentModule.calendarExponent', 'Gantt view') . ' (Организатор)',
				'description' => empty($fair) ? Yii::t('CalendarExponentModule.calendarExponent', 'all fairs') : $fair->name
			)); ?>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12 calendar-buttons">

			<div class="pull-left">
				<span class="b-button d-bg-warning d-text-light gantt-button">График Ганта</span>
				<a class="b-button calendar-button" href="<?= Yii::app()->createUrl('calendar/calendar', $fair ? ['fairId' => $fair->id] : []); ?>">Календарь</a>
				<!--
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				-->
			</div>

			<div class="pull-right">
				<a class="b-button d-bg-info d-text-light update-button" style="display: none;" data-icon="" href="#">Обновить задачи</a>
				<a class="b-button d-bg-success d-text-light create-button" data-icon="" href="#">Создать задачу</a>
			</div>

			<div style="clear: both;"></div>
		</div>
	</div>

	<div class="dhx_cal_navline js-zoom-task">
		<label class="dhx_cal_tab" style="left: 14px;">
			<input name="scales" type="radio" value="day">
			День
		</label>
		<label class="dhx_cal_tab" style="left: 75px;">
			<input name="scales" type="radio" value="week">
			Неделя
		</label>
		<label class="dhx_cal_tab" style="left: 136px;">
			<input name="scales" type="radio" value="month">
			Месяц
		</label>
		<label class="dhx_cal_tab year active" style="left: 211px;">
			<input name="scales" type="radio" checked value="year">
			Год
		</label>
	</div>

	<div id="gantt_here" style="width: 100%; height: calc(100% - 170px);"></div>

</div>

<div class="modal--sign-up modal hide fade" id="modal-edit-task" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header">
					<?= empty($fair) ? Yii::t('CalendarExponentModule.calendarExponent', 'all fairs') : $fair->name ?>
				</span>
				<span class="text-size-20 js-task-text" data-add-text="Добавление новой задачи">
					Добавление новой задачи
				</span>
			</div>
		</h3>
	</div>
	<div class="modal-body" style="padding: 15px !important;">
		<div class="row-fluid">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<form id="task">
					<div class="cols-row">
						<div class="col-md-12">

							<div class="control-group">
								<label class="task-label control-label" for="parent">Родительская задача</label>
								<div class="controls">
									<select id="parent" name="parent"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="group">Является продолжением задачи</label>
								<div class="controls">
									<select id="group" name="group"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="name">Название задачи <span class="required">*</span></label>
								<div class="controls">
									<input class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="Введите название задачи" name="name" id="name" type="text">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="start_date">Дата и/или длительность <span class="required">*</span></label>
								<div class="controls js-start-date-wrapper">
									<input id="date" name="date" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="text">
									<input id="duration" name="duration" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="number" min="1" step="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Длительность">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="parent">Цвет</label>
								<div class="controls">
									<select id="color" name="color">
										<option value="#43ade3">Синий</option>
										<option value="#f6a800">Желтый</option>
										<option value="#f96a0e">Оранжевый</option>
									</select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="desc">Описание задачи</label>
								<div class="controls">
									<textarea style="resize: none; padding: 9px;" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="Введите описание задачи" name="desc" id="desc"></textarea>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label checkbox" style="padding-top: 6px;">
									<?= TbHtml::checkBox('completeButton'); ?>
									Завершается пользователем вручную
								</label>
							</div>

							<div class="control-group">
								<label class="control-label checkbox" style="padding-top: 6px;">
									<?= TbHtml::checkBox('first'); ?>
									Активна по умолчанию
								</label>
							</div>

							<div style="text-align: center; margin: 20px 0 0;">
								<button style="margin: 0; display: none;" class="b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn js-complete-button" type="button" name="complete">Завершить</button>
							</div>

							<input type="hidden" name="id" id="id" value="">

							<div style="margin: 20px 0 0;">
								<button style="margin: 0; float: left;" class="b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn js-cancel-button" type="button" data-edit-text="Удалить" data-add-text="Отменить">Отменить</button>
								<button style="margin: 0; float: right;" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-submit-button" type="button" data-edit-text="Сохранить" data-add-text="Добавить">Добавить</button>
								<div style="clear: both;"></div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal--sign-up modal hide fade" id="modal-update-task" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header">
					<?= empty($fair) ? Yii::t('CalendarExponentModule.calendarExponent', 'all fairs') : $fair->name ?>
				</span>
				<span class="text-size-20">
					Список задач для обновления
				</span>
			</div>
		</h3>
	</div>
	<div class="modal-body" style="padding: 15px !important;">
		<div class="row-fluid">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<form id="task">
					<div class="cols-row">

						<div class="col-md-12 js-content"></div>

						<div class="col-md-12">
							<div style="text-align: center; margin: 20px 0 0;">
								<button style="margin: 0; display: none;" class="b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn js-complete-button" type="button" name="complete">Завершить</button>
							</div>

							<div style="margin: 20px 0 0;">
								<button style="margin: 0; float: left;" class="b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn js-cancel-button" type="button">Отменить</button>
								<button style="margin: 0; float: right;" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-submit-button" type="button">Обновить</button>
								<div style="clear: both;"></div>
							</div>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.calendarExponent.assets'), false, -1, YII_DEBUG);
?>
<script type="text/javascript" src="<?=$publishUrl?>/js/jquery.multiselect.js"></script>

<script type="text/javascript">

	gantt.attachEvent("onTaskLoading", function (task) {
		if (!task.color) {
			task.color = '#43ade3';
		}
		if (task.complete) {
			task.progress = 0.9;
			task.textColor = '#454545';
			task.color = '#efefef';
		}
		if (task.type == gantt.config.types.project) {
			task.color = '#378882';
		}

		task.readonly = true;
		/*
		if (task.data && task.data.action == -1) {
			task.readonly = true;
		}
		*/
		return true;
	});

	var fair = <?= $fair ? $fair->id : 0 ?>;
	var res = {
		data: [],
		links: [],
		colored: []
	};

	var $update = $('.update-button');

	function init(zoom) {

		$update.hide();

		res = {
			data: [],
			links: [],
			colored: []
		};

		var request = $.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/all', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		});

		request.done(function (data) {
			var res_data = [];
			var fairs = [];
			for (var i in data) {
				var iData = data[i];
				var id = iData.uuid;
				var text = iData.name;

				res_data[id] = {
					id: id,
					parent: iData.parent,
					text: text,
					desc: iData.desc,
					fair_name: iData.fairName,
					start_date: new Date(Date.parse(iData.date)),
					end_date: new Date(Date.parse(iData.date)),
					complete_date: new Date(Date.parse(iData.complete)),
					complete_btn: iData.completeButton,
					first: iData.first,
					color: iData.color,
					group: iData.group,
					data: iData,
					open: true
				};

				if (!iData.date && res_data[iData.parent]) {
					res_data[id].start_date = new Date(res_data[iData.parent].end_date.getTime());
				}

				if (iData.duration > 0) {
					res_data[id].end_date = new Date(res_data[id].start_date.getTime() + (iData.duration * 24 * 60 * 60 * 1000));
				} else if (iData.duration < 0) {
					res_data[id].end_date = res_data[id].start_date;
					res_data[id].start_date = new Date(res_data[id].end_date.getTime() + (iData.duration * 24 * 60 * 60 * 1000));
				} else {
					var now = new Date();
					if (res_data[id].start_date.getTime() > now.getTime()) {
						res_data[id].start_date = now;
					}
					if (res_data[iData.parent]) {
						res_data[id].start_date = new Date(res_data[iData.parent].end_date.getTime());
						if (res_data[id].end_date.getTime() < res_data[iData.parent].end_date.getTime()) {
							res_data[id].start_date = res_data[iData.parent].start_date;
						}
					}
				}

				if (iData.complete) {
					res_data[id].end_date = new Date(Date.parse(iData.complete));
					res_data[id].text = '<del>' + res_data[id].text + '</del>';
					if (res_data[id].start_date.getTime() > res_data[id].end_date.getTime()) {
						res_data[id].end_date = res_data[id].start_date;
					}
				}

				if (iData.parent) {
					if (res_data[iData.parent] && res_data[iData.parent].data.action == -1) {
						res_data[id].data.action = -1;
					}
					res.links.push({
						id: iData.parent + '-' + id,
						source: iData.parent,
						target: id,
						type: 0
					});
				} else {
					res_data[id].parent = iData.fairId + '-fair';
				}

				if (iData.group != iData.uuid) {
					res.links.push({
						id: iData.group + '-' + iData.uuid,
						source: iData.group,
						target: iData.uuid,
						type: 1,
						color: '#43ade3'
					});
				}

				if (iData.action !== null) {
					res_data[id].text = '<b>' + res_data[id].text + '</b>';
					if (iData.action == -1) {
						res_data[id].text = '<del>' + res_data[id].text + '</del>';
					}
					$update.show();
				}

				fairs[iData.fairId] = {
					id: iData.fairId + '-fair',
					parent: 0,
					text: iData.fairName,
					type: gantt.config.types.project,
					progress: 0,
					open: true
				};

				res.data.push(res_data[id]);
			}
			for (var j in fairs) {
				res.data.push(fairs[j]);
			}
		});

		gantt.config.xml_date = "%Y-%m-%d %H:%i";
		gantt.config.details_on_create = true;
		gantt.config.grid_width = 500;
		gantt.config.sort = true;

		gantt.config.columns = [
			{name: "text", width: "*", tree: true},
			{name: "start_date", width: "81"},
			{name: "end_date", label: "Окончание", width: "81"},
			{name: "add", label: "", width: 36}
		];

		var today = new Date();
		gantt.addMarker({start_date: today, css: "today", text: "Сегодня", title: "Сейчас"});

		gantt.init("gantt_here");
		gantt.parse(res);
		zoom_tasks(zoom);
	}
	init('year');

	function hideColumns() {
		gantt.config.columns = [];
		gantt.render();
	}

	function showColumns() {
		gantt.config.columns = [
			{name: "text", width: "*", tree: true},
			{name: "start_date", width: "81"},
			{name: "end_date", label: "Окончание", width: "81"},
			{name: "add", label: "", width: 36}
		];
		gantt.render();
	}

	function zoom_tasks(node) {
		switch (node) {
			case "day":
				gantt.config.min_column_width = 20;
				gantt.config.scale_unit = "day";
				gantt.config.date_scale = "%j %F";
				gantt.config.subscales = [
					{unit: "hour", step: 1, date: "%H"}
				];
				gantt.config.scale_height = 40;
				break;

			case "week":
				//gantt.config.min_column_width = 20;
				gantt.config.scale_unit = "week";
				gantt.config.date_scale = "#%W неделя";
				gantt.config.subscales = [
					{unit: "day", step: 1, date: "%j %M"}
				];
				gantt.config.scale_height = 40;
				break;

			case "month":
				gantt.config.min_column_width = 30;
				gantt.config.scale_unit = "month";
				gantt.config.date_scale = "%F";
				gantt.config.subscales = [
					{unit: "week", step: 1, date: "#%W"}
				];
				gantt.config.scale_height = 40;
				break;

			case "year":
				gantt.config.scale_unit = "year";
				gantt.config.date_scale = "%Y год";
				gantt.config.subscales = [
					{unit: "month", step: 1, date: "%F"}
				];
				gantt.config.scale_height = 40;
				break;
		}
		gantt.render();
	}

	var $taskModal = $('#modal-edit-task');

	$('.js-zoom-task input[type=radio]').on('change', function() {
		var $this = $(this);
		$('.js-zoom-task').find('.dhx_cal_tab.active').removeClass('active');
		$this.closest('.dhx_cal_tab').addClass('active');
		zoom_tasks($this.val());
	});

	$("a[href='#']").on('click', function() {
		return false;
	});

	var $parent = $("#parent");

	function fStructureRes(notId) {
		$parent.html('');
		structureRes(0, [], 0, notId);
		$parent.multiselect('reload');
	}

	function structureRes(parent, result, depth, notId) {
		var tab = '- ';
		var pref = '';
		for (var i = 0; i < depth; i++) {
			pref += tab;
		}
		for (var i in res.data) {
			if (res.data[i].parent == parent && res.data[i].id != notId && (!res.data[i].data || res.data[i].data.action != -1)) {
				var text = pref + strip(res.data[i].text);
				$parent.append($("<option></option>").attr("value", res.data[i].id).text(text));
				structureRes(res.data[i].id, result, (depth + 1 - 0), notId);
			}
		}
	}

	function strip(html) {
		var tmp = document.createElement("DIV");
		tmp.innerHTML = html;
		return tmp.textContent || tmp.innerText || "";
	}

	$parent.multiselect({
		placeholder: 'Выберите родительскую задачу',
		showCheckbox: false,
		maxHeight: 250,
		search: true,
		searchOptions: {
			'default': 'Поиск'
		},
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
		}
	});

	var $group = $("#group");
	function fGroupRes(notId) {
		$group.html('');
		groupRes(0, [], 0, notId);
		$group.multiselect('reload');
	}
	function groupRes(parent, result, depth, notId) {
		var tab = '- ';
		var pref = '';
		var i;
		for (i = 0; i < depth; i++) {
			pref += tab;
		}
		for (i in res.data) {
			var iData = res.data[i];
			if (iData.parent == parent && iData.id != notId && (!res.data[i].data || res.data[i].data.action != -1)) {
				var text = pref + strip(iData.text);
				$group.append($("<option></option>").attr("value", iData.id).text(text));
				groupRes(iData.id, result, (depth + 1 - 0), notId);
			}
		}
	}
	$group.multiselect({
		placeholder: 'Выберите задачу',
		showCheckbox: false,
		maxHeight: 250,
		search: true,
		searchOptions: {
			'default': 'Поиск'
		},
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
		}
	});



	var $color = $('#color');
	$color.multiselect({
		placeholder: 'Выберите цвет',
		showCheckbox: false,
		maxHeight: 250,
		search: false,
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
			addColorLabel();
		}
	});

	function addColorLabel() {
		$color.closest('.control-group').find('.ms-options-wrap ul li').each(function (indx, element) {
			var $input = $(element).find('input');
			$input.after("<div class='color-label' style='background-color: " + $input.val() + "'>&nbsp;</div>");
		});
	}

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие 'Добавить' или 'Сохранить'
	$taskModal.on('click', '.js-submit-button', function() {

		var $taskForm = $taskModal.find("#task");
		var $name = $taskForm.find('#name');
		var $date = $taskForm.find('#date');

		var post = {};

		var errors = false;

		$.each($taskForm.serializeArray(), function(i, field) {
			post[field.name] = field.value;
		});

		var $completeButton = $taskForm.find('#completeButton');
		if ($completeButton.attr('checked')) {
			post['completeButton'] = $completeButton.val();
		}

		var $first = $taskForm.find('#first');
		if ($first.attr('checked')) {
			post['first'] = $first.val();
		}

		if (!post.name) {
			$name.closest('.control-group').addClass('error');
			errors = true;
		} else {
			$name.closest('.control-group').removeClass('error');
		}

		if (post.date || post.duration > 0) {
			$date.closest('.control-group').removeClass('error');
		} else {
			$date.closest('.control-group').addClass('error');
			errors = true;
		}

		if (!errors) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/editTask'); ?>",
				type: "POST",
				data: post,
				success: function (data) {
					console.log(data);
					if (data.id) {
						gantt.clearAll();
						init($('.js-zoom-task .active input[type=radio]').val());
						$taskModal.modal('hide');
					}
				},
				dataType: 'json'
			});
		}
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на 'Создать задачу'
	$('.create-button').on('click', function() {
		fStructureRes(0);
		fGroupRes(0);
		createPeriodPicker(0);
		addColorLabel();
		$taskModal.modal('show');
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на '+'
	gantt.attachEvent("onTaskCreated", function(task) {
		fStructureRes(0);
		var $parent = $taskModal.find('#parent');
		$parent.val(task.parent);
		$parent.multiselect('reload');
		fGroupRes(0);
		createPeriodPicker(0);
		addColorLabel();
		$taskModal.modal('show');

		return false;
	});

	// ЗАДАЧИ ЭКСПОНЕНТА закрытие модального окна
	$taskModal.on('hidden.bs.modal', function() {

		var $modal = $(this);
		var $form = $modal.find("#task");
		$form.trigger('reset');
		$form.find("#desc").text('');
		$form.find("#id").val('');
		$form.find('.control-group.error').removeClass('error');

		$parent.multiselect('reload');
		$color.multiselect('reload');

		$('#completeButton').removeAttr('checked');
		$('#first').removeAttr('checked');
		$('.checkbox.checked').removeClass('checked');

		var $cancelBtn = $taskModal.find('.js-cancel-button');
		$cancelBtn.text($cancelBtn.attr('data-add-text'));

		var $submitBtn = $taskModal.find('.js-submit-button');
		$submitBtn.text($submitBtn.attr('data-add-text'));
	});

	var taskDragOriginal = null;
	gantt.attachEvent("onBeforeTaskDrag", function(id, mode, e) {
		var task = gantt.getTask(id);
		taskDragOriginal = {
			start_date: task.start_date,
			end_date: task.end_date
		};
		return true;
	});

	var taskDrag = null;
	gantt.attachEvent("onTaskDrag", function(id, mode, task, original) {
		taskDrag = task;
	});

	gantt.attachEvent("onAfterTaskDrag", function(id, mode, e) {

		var task = gantt.getTask(id);

		console.log(mode);
		console.log([
			taskDragOriginal.start_date,
			taskDragOriginal.end_date
		]);
		console.log([
			taskDrag.start_date,
			taskDrag.end_date
		]);

		if (task.data.date && task.data.duration) {
			task.start_date = taskDrag.start_date;
			task.start_date.setHours(taskDragOriginal.start_date.getHours());
			task.start_date.setMinutes(taskDragOriginal.start_date.getMinutes());
			task.start_date.setSeconds(0);

			task.duration = task.data.duration;
			if (mode == 'resize') {
				var endDate = taskDrag.end_date;
				endDate.setHours(taskDragOriginal.end_date.getHours());
				endDate.setMinutes(taskDragOriginal.end_date.getMinutes());
				endDate.setSeconds(0);

				console.log(task.start_date);
				console.log(endDate);

				task.duration = moment(moment(endDate).format('YYYY-MM-DD')).diff(moment(moment(task.start_date).format('YYYY-MM-DD')), 'days');
			}
			task.end_date = new Date(task.start_date.getTime() + (task.duration * 24 * 60 * 60 * 1000));

			console.log(task);

			gantt.updateTask(task.id);
		}

		if (!task.data.date && taskDragOriginal.start_date != taskDrag.start_date && confirm('Вы уверены, что хотите задать дату?')) {
			task.start_date = taskDrag.start_date;
			task.start_date.setHours(0);
			task.start_date.setMinutes(0);
			task.start_date.setSeconds(0);

			task.end_date = taskDrag.end_date;
			task.end_date.setHours(0);
			task.end_date.setMinutes(0);
			task.end_date.setSeconds(0);
		}
		//gantt.updateTask(task.id);
/*
		console.log([
			moment(task.start_date).format('YYYY-MM-DD HH:mm:ss'),
			moment(task.end_date).format('YYYY-MM-DD HH:mm:ss')
		]);
*/
/*
		if (task.type == "userTask") {
			var post = {
				id: parseInt(task.id),
				start_date: moment(task.start_date).format('YYYY-MM-DD HH:mm:ss'),
				end_date: moment(task.end_date).format('YYYY-MM-DD HH:mm:ss')
			};
			$.ajax({*/
//				url: "<?//=''// Yii::app()->createUrl('calendar/gantt/editUserTask'); ?>//",
				/*type: "POST",
				data: post,
				dataType: 'json'
			});
			gantt.updateTask(task.id);
		}
*/
	});

	gantt.attachEvent("onTaskDblClick", function (id, e) {

		var task = null;
		if (id) {
			task = gantt.getTask(id);
			console.log(task);
		}

		if (task && task.type != gantt.config.types.project && (!task.data || task.data.action != -1)) {
			var $parent = $('#parent');
			var $name = $('#name');
			var $desc = $('#desc');
			var $completeButton = $('#completeButton');
			var $first = $('#first');
			var $id = $('#id');

			$name.val(task.data.name);

			$('.js-task-text').html(task.text);

			createPeriodPicker(task.data.date);

			$desc.text(task.data.desc);

			var $cancelBtn = $taskModal.find('.js-cancel-button');
			$cancelBtn.text($cancelBtn.attr('data-edit-text'));

			var $submitBtn = $taskModal.find('.js-submit-button');
			$submitBtn.text($submitBtn.attr('data-edit-text'));

			//$('.js-complete-button').css('display', '');

			$id.val(task.id);

			fStructureRes(task.id);
			$parent.val(task.parent);
			$parent.multiselect('reload');

			fGroupRes(0);
			$group.val(task.group);
			$group.multiselect('reload');

			$('#duration').val(task.data.duration ? task.data.duration : '');

			$color.val(task.color);
			$color.multiselect('reload');
			addColorLabel();

			if (task.complete_btn == 1) {
				$completeButton.attr('checked', 'checked');
				$completeButton.closest('.checkbox').addClass('checked');
			}

			if (task.first == 1) {
				$first.attr('checked', 'checked');
				$first.closest('.checkbox').addClass('checked');
			}

			$taskModal.modal('show');
		}
	});

	gantt.attachEvent("onBeforeLinkAdd", function(id,link) {
		return false;
	});

	function createPeriodPicker(ppStartDate) {
		var $oldStartDate = $taskModal.find('#date');
		var $newStartDate = $oldStartDate.clone();
		$oldStartDate.remove();
		$('.period_picker_input').remove();

		if (!ppStartDate) {
			$newStartDate.val('');
			$('.js-start-date-wrapper').append($newStartDate);
			$newStartDate.periodpicker({
				lang: 'ru',
				formatDateTime: 'YYYY-MM-DD HH:mm:ss',
				timepicker: true,
				clearButtonInButton: true,
				norange: true,
				cells: [1, 1],
				timepickerOptions: {
					hours: true,
					minutes: true,
					seconds: false,
					ampm: false,
					defaultTime: '00:00',
					twelveHoursFormat: false,
					steps:[1,5,2,1]
				}
			});
		} else {
			var startDate = new Date(ppStartDate);

			$newStartDate.val(moment(startDate).format('YYYY-MM-DD HH:mm:ss'));
			$('.js-start-date-wrapper').append($newStartDate);
			$newStartDate.periodpicker({
				lang: 'ru',
				formatDateTime: 'YYYY-MM-DD HH:mm:ss',
				timepicker: true,
				clearButtonInButton: true,
				norange: true,
				cells: [1, 1],
				timepickerOptions: {
					hours: true,
					minutes: true,
					seconds: false,
					ampm: false,
					defaultTime: moment(startDate).format('HH:mm'),
					twelveHoursFormat: false,
					steps:[1,5,2,1]
				}
			});

			$newStartDate.periodpicker('change');
		}
	}

	$('.js-cancel-button').on('click', function() {
		var id = $("#id").val();
		var task = null;
		if (id) {
			task = gantt.getTask(id);
		}
		if (task) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/deleteTask'); ?>",
				type: "GET",
				data: {id: id},
				success: function (data) {
					console.log(data);
				},
				dataType: 'json'
			});
			gantt.clearAll();
			init($('.js-zoom-task .active input[type=radio]').val());
		}
		$taskModal.modal('hide');
	});

	var $modalUpdate = $('#modal-update-task');

	$('.update-button').on('click', function() {

		$modalUpdate.find('.js-content').html('');

		//var $taskText = $('.js-text');
		//$taskText.text($taskText.attr('data-add-text'));

		$.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/getTempTasks', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		}).done(function (data) {

			var i;
			for (i in data) {
				var iData = data[i];
				console.log(iData);

				if (iData.action == -1) {
					$modalUpdate.find('.js-content').append('<p style="color: red; font-weight: bold;">' + iData.temp_name + '</p>');
				} else if (iData.action == 1) {
					$modalUpdate.find('.js-content').append('<p style="color: green; font-weight: bold;">' + iData.temp_name + '</p>');
				} else {
					$modalUpdate.find('.js-content').append('<p style="font-weight: bold;">' + iData.temp_name + '</p>');
					if (iData.temp_parent != iData.task_parent) {
						$modalUpdate.find('.js-content').append('<p>Родительская задача:<br>' + iData.task_parent + ' -> ' + iData.temp_parent + '</p>');
					}
					if (iData.temp_group != iData.task_group) {
						$modalUpdate.find('.js-content').append('<p>Является продолжением задачи:<br>' + iData.task_group + ' -> ' + iData.temp_group + '</p>');
					}
					if (iData.temp_name != iData.task_name) {
						$modalUpdate.find('.js-content').append('<p>Название задачи:<br>' + iData.task_name + ' -> ' + iData.temp_name + '</p>');
					}
					if (iData.temp_date != iData.task_date) {
						$modalUpdate.find('.js-content').append('<p>Дата:<br>' + iData.task_date + ' -> ' + iData.temp_date + '</p>');
					}
					if (iData.temp_duration != iData.task_duration) {
						$modalUpdate.find('.js-content').append('<p>Длительность:<br>' + iData.task_duration + ' -> ' + iData.temp_duration + '</p>');
					}
					if (iData.temp_color != iData.task_color) {
						$modalUpdate.find('.js-content').append('<p>Цвет:<br>' + iData.task_color + ' -> ' + iData.temp_color + '</p>');
					}
					if (iData.temp_desc != iData.task_desc) {
						$modalUpdate.find('.js-content').append('<p>Описание:<br>' + iData.task_desc + ' -> ' + iData.temp_desc + '</p>');
					}
					if (iData.temp_completeButton != iData.task_completeButton) {
						$modalUpdate.find('.js-content').append('<p>Завершается пользователем вручную:<br>' + iData.task_completeButton + ' -> ' + iData.temp_completeButton + '</p>');
					}
					if (iData.temp_first != iData.task_first) {
						$modalUpdate.find('.js-content').append('<p>Активна по умолчанию:<br>' + iData.task_first + ' -> ' + iData.temp_first + '</p>');
					}
				}

				$modalUpdate.find('.js-content').append('<hr>');
			}

		});

		$modalUpdate.modal('show');
	});

	$modalUpdate.on('click', '.js-cancel-button', function () {
		$modalUpdate.modal('hide');
	});

	$modalUpdate.on('click', '.js-submit-button', function () {
		$.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/updateTasks', $fair ? ['fairId' => $fair->id] : []); ?>",
			//dataType: "json",
			async: false
		}).done(function (data) {
			console.log(data);
			gantt.clearAll();
			init($('.js-zoom-task .active input[type=radio]').val());
			$modalUpdate.modal('hide');
		});
	});

	$('.checkbox').on('click', function(e) {
		var $checkbox = $(this);
		if ($checkbox.hasClass('checked')) {
			$checkbox.removeClass('checked');
			$checkbox.find('input[type=checkbox]').removeAttr('checked');
		} else {
			$checkbox.find('input[type=checkbox]').attr('checked', 'checked');
			$checkbox.addClass('checked');
		}
		e.preventDefault();
	});

</script>