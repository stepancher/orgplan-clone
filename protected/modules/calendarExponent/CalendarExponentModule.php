<?php

class CalendarExponentModule extends CWebModule {

	public $defaultController = 'gantt';

	public function init() {

		$this->setImport(array(
			'calendarExponent.models.*',
			'calendarExponent.components.*',
		));
	}

	public function beforeControllerAction($controller, $action) {

		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}
