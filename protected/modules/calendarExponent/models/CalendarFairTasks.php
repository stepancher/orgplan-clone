<?php

/**
 * This is the model class for table "{{calendarfairtasks}}".
 *
 * The followings are the available columns in table '{{calendarfairtasks}}':
 * @property integer $id
 * @property integer $fairId
 * @property string $uuid
 * @property string $parent
 * @property string $group
 * @property string $name
 * @property string $desc
 * @property integer $duration
 * @property string $date
 * @property string $color
 * @property integer $completeButton
 * @property integer $first
 */
class CalendarFairTasks extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {

		return '{{calendarfairtasks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uuid', 'required'),
			array('fairId, duration, completeButton, first', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>245),
			array('color', 'length', 'max'=>45),
			array('uuid, parent, group', 'length', 'max'=>36),
			array('desc, date', 'safe'),
			array('id, uuid, parent, group, fairId, name, desc, duration, date, color, completeButton, first', 'safe', 'on'=>'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {

		return array(
			'UsersTasks' => array(self::HAS_MANY, 'CalendarFairUsersTasks', 'uuid'),
            'translate' => array(
				self::HAS_ONE,
				'TrCalendarFairTasks',
				'trParentId',
				'on' => 'translate.langId = "'.Yii::app()->language.'"',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {

		return array(
			'id' => 'ID',
			'uuid' => 'UUID',
			'parent' => 'Родительская задача',
			'group' => 'Группа',
			'fairId' => 'ID выставки',
			'name' => 'Заголовок',
			'desc' => 'Описание',
			'duration' => 'Продолжительность',
			'date' => 'Дата',
			'color' => 'Цвет',
			'completeButton' => 'Кнопка завершить',
			'first' => 'Загружается первый'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {

		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('uuid', $this->id);
		$criteria->compare('parent', $this->parent);
		$criteria->compare('group', $this->group);
		$criteria->compare('fairId', $this->fairId);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('desc', $this->desc, true);
		$criteria->compare('duration', $this->duration);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('color', $this->color, true);
		$criteria->compare('completeButton', $this->completeButton);
		$criteria->compare('first', $this->first);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Calendarfairtasks the static model class
	 */
	public static function model($className=__CLASS__) {

		return parent::model($className);
	}

	/**
	 * @return string
	 */
	public function getName(){

		$trString = '';

		if(!empty($this->translate->name)){
			$trString = $this->translate->name;
		}

		return $trString;
	}

	/**
	 * @return string
	 */
	public function getDesc(){

		$trString = '';

		if(!empty($this->translate->desc)){
			$trString = $this->translate->desc;
		}

		return $trString;
	}
}
