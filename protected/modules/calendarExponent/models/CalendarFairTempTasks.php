<?php

/**
 * This is the model class for table "{{calendarfairtemptasks}}".
 *
 * The followings are the available columns in table '{{calendarfairtemptasks}}':
 * @property integer $id
 * @property integer $fairId
 * @property string $uuid
 * @property string $parent
 * @property string $group
 * @property string $name
 * @property string $desc
 * @property integer $duration
 * @property string $date
 * @property string $color
 * @property integer $completeButton
 * @property integer $first
 * @property integer $action
 * @property string $editDate
 */
class CalendarFairTempTasks extends CActiveRecord {

	const ACTION_DELETE = -1;
	const ACTION_UPDATE = 0;
	const ACTION_INSERT = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {

		return '{{calendarfairtemptasks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('fairId, uuid, name, group', 'required'),
			array('fairId, duration, completeButton, first, action', 'numerical', 'integerOnly'=>true),
			array('uuid, parent, group', 'length', 'max'=>36),
			array('name', 'length', 'max'=>245),
			array('color', 'length', 'max'=>45),
			array('desc, date', 'safe'),
			array('id, fairId, uuid, parent, group, name, desc, duration, date, color, completeButton, first, action, editDate', 'safe', 'on'=>'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {

		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fairId' => 'Fair',
			'uuid' => 'Uuid',
			'parent' => 'Parent',
			'group' => 'Group',
			'name' => 'Name',
			'desc' => 'Desc',
			'duration' => 'Duration',
			'date' => 'Date',
			'color' => 'Color',
			'completeButton' => 'Complete Button',
			'first' => 'First',
			'action' => 'Action',
			'editDate' => 'Edit Date'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('fairId', $this->fairId);
		$criteria->compare('uuid', $this->uuid, true);
		$criteria->compare('parent', $this->parent, true);
		$criteria->compare('group', $this->group, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('desc', $this->desc, true);
		$criteria->compare('duration', $this->duration);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('color', $this->color, true);
		$criteria->compare('completeButton', $this->completeButton);
		$criteria->compare('first', $this->first);
		$criteria->compare('action', $this->action);
		$criteria->compare('editDate', $this->editDate, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CalendarFairTempTasks the static model class
	 */
	public static function model($className=__CLASS__) {

		return parent::model($className);
	}

	/**
	 * @param DateTime $date
	 * @param int $duration
	 * @param DateTime $parentComplete
	 * @return array
	 */
	public static function calculateDates($date = null, $duration = null, $parentComplete = null) {

		$now = date('Y-m-d H:i:s');
		$startDate = $now;
		$endDate = $now;
		if (!$duration) {
			if ($parentComplete) {
				$startDate = $parentComplete;
			}
			$endDate = $date;
		}
		if ($duration > 0) {
			if ($date) {
				$startDate = $date;
			} elseif ($parentComplete) {
				$startDate = $parentComplete;
			}
			$endDate = date('Y-m-d H:i:s', strtotime($startDate . ' +' . $duration . ' days'));
		}

		return [$startDate, $endDate];
	}
}
