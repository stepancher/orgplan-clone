<?php
/**
 * Class TrCalendarFairTasksCommon
 *
 * @property integer $id
 * @property integer $trParentId
 * @property string $langId
 * @property string $name
 * @property string $desc
 */
class TrCalendarFairTasksCommon extends \AR
{
    public function description()
    {
        return array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'trParentId' => array(
                'label' => 'trParentId',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'langId' => array(
                'label' => 'Язык',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => 'name',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'desc' => array(
                'string',
                'label' => 'desc',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }
}