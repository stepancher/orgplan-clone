<?php

/**
 * This is the model class for table "{{calendaruserstasks}}".
 *
 * The followings are the available columns in table '{{calendaruserstasks}}':
 * @property integer $id
 * @property integer $userId
 * @property integer $fairId
 * @property string $parent
 * @property string $name
 * @property string $startDate
 * @property string $endDate
 * @property string $completeDate
 * @property string $desc
 */
class CalendarUsersTasks extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{calendaruserstasks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, name, startDate, endDate', 'required'),
			array('userId, fairId', 'numerical', 'integerOnly'=>true),
			array('parent', 'length', 'max'=>45),
			array('name, desc', 'length', 'max'=>245),
			array('completeDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userId, fairId, parent, name, startDate, endDate, completeDate, desc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'fairId' => 'Fair',
			'parent' => 'Parent',
			'name' => 'Name',
			'startDate' => 'Start Date',
			'endDate' => 'End Date',
			'completeDate' => 'Complete Date',
			'desc' => 'Desc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('fairId',$this->fairId);
		$criteria->compare('parent',$this->parent,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('startDate',$this->startDate,true);
		$criteria->compare('endDate',$this->endDate,true);
		$criteria->compare('completeDate',$this->completeDate,true);
		$criteria->compare('desc',$this->desc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CalendarUsersTasks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
