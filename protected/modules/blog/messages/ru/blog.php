<?php return array(
	'Grid header'=>'Список последних постов',
	'Grid action add'=>'Добавить запись в блог',
	'Form header create'=>'Создание записи в блоге',
	'Form header update'=>'Редактирование записи блога',
	'Form action save'=>'Сохранить',
	'Form action cancel'=>'Отмена',
	'Detail header'=>'Просмотр информации о записи блога',
	'Detail action cancel'=>'Отмена',
	'Detail action edit'=>'Редактирование',
	'On page provides information about '=>'На странице представлена информация про ',
	'Blogs'=>'Блоги',
	'Blog'=>'Блог',
	'leave a comment'=>'Оставить комментарий',
	'list of comments'=>'Список комментариев',
	'information'=>'Информация',
	'delete'=>'Удалить',
	'publish'=>'Опубликовать',
	'hide'=>'Скрыть',
	'favourites image'=>'Избранное изображение',
	'image'=>'Изображение',
	'no images'=>'Изображения отсутствуют',
	'files'=>'Файлы',
	'curator of the exhibition'=>'КУРАТОР ВЫСТАВКИ',
	'an expert in the field of contemporary art'=>'Эксперт в области современного искусства',
	'post'=>'поста',
	'blog'=>'Блог',
	'today on portal:'=>'Сегодня на портале:',
	'main'=>'Главная',
	'posts'=>'постов',
	'post1'=>'пост',
	'the post'=>'поста',
	'and'=>'и',
	'experts'=>'экспертов',
	'expert'=>'эксперт',
	'the expert'=>'эксперта',
	'there are no data available on request'=>'Данные по запросу отсутствуют',
	'show more'=>'Показать еще',
	'the experts'=>'ЭКСПЕРТЫ',
	'about this blog'=>'О БЛОГЕ',
	'subscribe to newsletter'=>'Подписка на рассылку',
	'categories'=>'Категории',
	'search experts'=>'Поиск по экспертам',
	'editing'=>'Редактирование',
	'show'=>'Скрыть',
	'download Image (title picture)'=>'Загрузить изображение( Заглавное изображение )',
	'this file already exists!'=>'Такой файл уже имеется!',
	'Invalid file type'=>'Недопустимый тип файла',
	'download Image'=>'Загрузить изображение',
	'form to attach files'=>'Форма для прикрепления файлов',
	'data on the blog'=>'Данные о блоге',
	'list of uploaded files'=>'Список загруженных файлов',
	'New'=>'Новое',
	'Private'=>'Скрыто',
	'Published'=>'Опубликовано',
	'Author'=>'Автор',
	'Title image'=>'Заглавное изображение',
	'Date of creation'=>'Дата создания',
	'Theme'=>'Тема',
	'Description'=>'Описание',
	'SEO Description'=>'SEO Описание',
	'Status'=>'Статус',
	'View Counter'=>'Количество просмотров',
	'subscribe'=>'Подписка на рассылку!',
	'popular'=>'Популярное',
	'add popular'=>'Добавить в популярное',
	'fresh blog'=>'Свежие статьи',
	'review and comment'=>'Отзывы и комментарии',
	'rubric'=>'рубрика',
	'tags'=>'Тэги',
	'our experts'=>'Наши эксперты',
	'Leave a comment'=>'Оставить отзыв или комментарий',
	'delete article'=>'Удалить статью',
	'Sign up and get fresh and relevant advice from the leading experts of exhibition activity'=>'Подпишись и получай свежие и актуальные советы от ведущих экспертов выставочной деятельности',
	'articles'=>'статей',
	'article'=>'статья',
	'articles '=>'статьи',
	'Unable to resolve the request "{route}"' => 'Невозможно обработать запрос "{route}".',
	'The information is saved'=>'Информация сохранена.',
	'Search result more'=>'Показать еще',
	'In no tabs' => 'Нет во вкладках',
	'Unfortunately, upon request'=>'К сожалению, по запросу',
	'There is nothing'=>'ничего нет',
	'Enter into the search box or try a new query filters'=>'Введи в поисковое окно новый запрос или воспользуйся фильтрами',
	'Are you sure you want to delete an article or material?'=>'Вы точно хотите удалить статью или материал?',
	'No comments yet. Be the first!'=>'Комментариев пока нет. Будь первым!',
	'rate this article: 5 stars - maximum rating'=>'оценить статью: 5 звездочек - максимальная оценка',
	'Register'=>'Зарегистрируйся',
	'or'=>'или',
	'Sign In'=>'Войти',
	'To leave a comment' => 'Оставить комментарий',
	'You must fill in the "Leave a comment".'=>'Необходимо заполнить поле "Оставить отзыв".',
  'Send' => 'Отправить',
  'User deleted' => 'Пользователь удалён',
);