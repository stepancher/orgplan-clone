<?php 
return array(
	'Grid header'=>'Liste der letzten Einträge',
	'Grid action add'=>'Blogeintrag hinzufügen',
	'Form header create'=>'Blogeintrag erstellen',
	'Form header update'=>'Blogeintrag bearbeiten',
	'Form action save'=>'speichern',
	'Form action cancel'=>'abbrechen',
	'Detail header'=>'Information über Blogeintrag einsehen',
	'Detail action cancel'=>'abbrechen',
	'Detail action edit'=>'Bearbeitung',
	'On page provides information about '=>'KEY_NOT_FOUND',
	'Blogs'=>'Blogs',
	'Blog'=>'Blog',
	'leave a comment'=>'Kommentar hinterlassen',
	'list of comments'=>'Liste der Kommentare',
	'information'=>'Information',
	'delete'=>'löschen',
	'publish'=>'veröffentlichen',
	'hide'=>'verbergen',
	'favourites image'=>'Ausgewählte Abbildung',
	'image'=>'Abbildung',
	'no images'=>'Abbildungen entfallen',
	'files'=>'Dateien',
	'curator of the exhibition'=>'MESSEBETREUER',
	'an expert in the field of contemporary art'=>'Experte im Bereich der modernen Kunst',
	'post'=>'Eintrag',
	'blog'=>'Blog',
	'today on portal:'=>'Heute auf dem Webportal:',
	'main'=>'Hauptseite',
	'posts'=>'Einträge',
	'post1'=>'Eintrag',
	'the post'=>'Eintrag',
	'and'=>'und',
	'experts'=>'Experten',
	'expert'=>'Experte',
	'the expert'=>'Experte',
	'there are no data available on request'=>'Angaben auf Anfrage entfallen',
	'show more'=>'Mehr zeigen',
	'the experts'=>'EXPERTEN',
	'about this blog'=>'ÜBER BLOG',
	'subscribe to newsletter'=>'Newsletter bestellen',
	'categories'=>'Kategorien',
	'search experts'=>'Suche nach Experten',
	'editing'=>'Bearbeitung',
	'show'=>'verbergen',
	'download Image (title picture)'=>'Abbildung hochladen (Kopfbild)',
	'this file already exists!'=>'Die Datei ist schon vorhanden!',
	'Invalid file type'=>'Unerlaubter Dateityp',
	'download Image'=>'Abbildung hochladen',
	'form to attach files'=>'Datei-Anhänge-Form',
	'data on the blog'=>'Angaben zum Blog',
	'list of uploaded files'=>'Liste von heruntergeladeten Dateien',
	'New'=>'Neues',
	'Private'=>'verborgen',
	'Published'=>'veröffentlicht',
	'Author'=>'Verfasser',
	'Title image'=>'Kopfbild',
	'Date of creation'=>'Erstellungsdatum',
	'Theme'=>'Thema',
	'Description'=>'Beschreibung',
	'SEO Description'=>'KEY_NOT_FOUND',
	'Status'=>'Status',
	'View Counter'=>'KEY_NOT_FOUND',
	'subscribe'=>'Newsletter bestellen!',
	'popular'=>'Populäres',
	'add popular'=>'Zum Populäres hinzufügen',
	'fresh blog'=>'Frische Artikel',
	'review and comment'=>'Bewertungen und Kommentare',
	'rubric'=>'Rubrik',
	'tags'=>'Tags',
	'our experts'=>'Unsere Experten',
	'Leave a comment'=>'Bewertung oder Kommentar hinterlassen',
	'delete article'=>'Artikel löschen',
	'Sign up and get fresh and relevant advice from the leading experts of exhibition activity'=>'Bestellen Sie unsere Newsletter und bekommen Sie frische und aktuelle Ratschläge von den führenden Experten der Messewirtschaft. ',
	'articles'=>'Artikel',
	'article'=>'Artikel',
	'articles '=>'Artikel',
	'Unable to resolve the request "{route}"' => 'Невозможно обработать запрос "{route}".',
	'The information is saved'=>'Информация сохранена.',
	'Search result more'=>'Показать еще',
	'In no tabs' => 'Нет во вкладках',
	'Unfortunately, upon request'=>'К сожалению, по запросу',
	'There is nothing'=>'ничего нет',
	'Enter into the search box or try a new query filters'=>'Введи в поисковое окно новый запрос или воспользуйся фильтрами',
	'Are you sure you want to delete an article or material?'=>'Вы точно хотите удалить статью или материал?',
	'No comments yet. Be the first!'=>'Комментариев пока нет. Будь первым!',
	'rate this article: 5 stars - maximum rating'=>'оценить статью: 5 звездочек - максимальная оценка',
	'Register'=>'Зарегистрируйся',
	'or'=>'или',
	'Sign In'=>'Войти',
	'To leave a comment' => 'Оставить комментарий',
	'You must fill in the "Leave a comment".'=>'Необходимо заполнить поле "Оставить отзыв".',
  'Send' => 'senden',
  'User deleted' => 'Пользователь удалён',
);