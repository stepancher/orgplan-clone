$(document).ready(function(){
	$('.comment-detail').on('click', function(e){
		e.preventDefault();
		var $elm = $(this);
		var target = $elm.data('target');
		var show = $elm.data('show');
		var $spanAll = $('span.all-'+target);
		var $spanShort = $('span.short-'+target);
		if(show == 1){
			$elm.data('show', 2);
			$elm.text('Свернуть');
			$spanShort.addClass('hide');
			$spanAll.slideDown(300);
		} else if(show == 2){
			$elm.data('show', 1);
			$elm.text('Читать полностью');
			$spanAll.addClass('hide').slideUp(300);
			$spanShort.removeClass('hide');
		}
	});
});

