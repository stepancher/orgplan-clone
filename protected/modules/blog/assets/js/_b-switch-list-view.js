function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function getChangesFromUrl(changes, url) {
    var elm = document.createElement('a');
    if (~url.indexOf('/coming/') && ~url.indexOf('sort=') == 0) {
        url = url + '?sort=date'
    }
    elm.setAttribute('href', url);
    $.each(elm.search.replace("?", "").split('&').map(function (v) {
        return v.split('=')
    }), function (k, v) {
        changes[v[0]] = v[1];
    });

    return changes;
}

function changePositionDateRegion() {
    var $elms = $('.b-switch-list-view--as-block .b-switch-list-view__item');
    $.each($elms, function (i, v) {
        if ($(this).find('.b-switch-list-view__cell--fair-region').height() > 20) {
            $(this).find('.b-switch-list-view__cell--fair-region').css('top', '154px');
            $(this).find('.b-switch-list-view__cell--date').css('top', '126px');
        }
    });
}
changePositionDateRegion();
var locationHistory = history.length;
$(function () {
    $('.b-switch-list-view')
        .filter(':not(.b-switch-list-view__items)')
        .each(function () {
            var self = this;

            self.filterData = $('form.b-search-form').length
                ? $('form.b-search-form').data('params')
                : JSON.parse(self.getAttribute('data-request'));

            self.filterUrl = self.getAttribute('data-request-url');

            if (self.filterData) {
                var $self = $(self);
                var $loader = $('.js-page-search__loader');

                self.filterChange = function (changes, afterChange) {
                    if (!afterChange) {
                        $.each(changes, function (k, v) {
                            self.filterData[k] = v;
                        });
                    }
                    self.isChanged = true;
                    self.load(function () {
                        if (afterChange) {
                            $.each(changes, function (k, v) {
                                self.filterData[k] = v;
                            });
                        }
                    });
                };

                self.load = debounce(function (callback) {
                    if (callback instanceof Function) {
                        callback();
                    }
                    if (!self.loading) {
                        self._load();
                    }
                }, 500);

                self._load = function () {
                    if (self.isChanged) {
                        self.loading = true;
                        self.isChanged = false;
                        var requestUrl = null;
                        var requestData = null;
                        var ctrl = self.getAttribute('data-ctrl');

                        if ($('.b-search__query').length) {
                            var lang = $('.b-search__query').data('lang');
                            requestUrl = '/'+lang+'/search/';
                            var filterData = {};
                            $.each(self.filterData, function (k, v) {
                                if (!(v == undefined || v.length == 0)) {
                                    filterData[k] = self.filterData[k];
                                }
                            });
                            self.filterData = filterData;
                            requestData = $.extend(self.filterData, {ctrl: self.getAttribute('ctrl')});
                        }
                        else {
                            requestUrl = $self.find('.b-switch-list-view__add-items').data('url');
                            self.filterData = [];
                        }

                        if (requestUrl) {

                            if (~requestUrl.indexOf('?')) {
                                requestUrl += '&ctrl=' + ctrl;
                            }
                            else {
                                requestUrl += '?ctrl=' + ctrl;
                            }
                            $loader.show();

                            $
                                .getJSON(requestUrl, requestData, function (data) {
                                    var $response = ('content' in data) ? $(data.content) : data;
                                    var header = data.sectorName + ((
                                                    'header' in data
                                                    && 'sectorName' in data
                                                    && 'defaultHeader' in data
                                                    && data.header != ''
                                                    && data.header != data.defaultHeader
                                                )
                                                    ? data.header
                                                    : (
                                                        'defaultHeader' in data && data.defaultHeader != ''
                                                            ? data.defaultHeader : ' '
                                                    )
                                            )
                                        ;
                                    var title = ('title' in data && 'sectorName' in data) ? data.sectorName + data.title : '';
                                    var description = ('description' in data) ? data.description : '';
                                    var $itemsBlock = $self.find('.b-switch-list-view__items');
                                    var $items = $self.find('.b-switch-list-view__items').find('.items');

                                    if (title || title == '') {
                                        $('title').text(title)
                                    }
                                    $('meta[name="description"]').attr("content", description);
                                    if (header) {
                                        $('.f-head-search-result').text(header)
                                    }

                                    if ($self.find('.not-found').length || $response.find('.not-found').length) {
                                        var $parent = $self.parent();
                                        $parent.empty().append(
                                            $response.find('.b-search-result__items').children()
                                        );
                                        $self = $parent.find('.b-switch-list-view');
                                    }
                                    else if (self.filterData.page < 1) {
                                        $itemsBlock.empty().append(
                                            $response.find('.b-switch-list-view__items').children()
                                        );
                                    }
                                    else if ($response.length) {
                                        $items.append(
                                            $response.find('.b-switch-list-view__items').find('.items').children()
                                        );
                                    }

                                    var $responseMore = $response.find('.b-switch-list-view__add-items');

                                    // Скрытие кнопки "Показать ЕЩЁ"
                                    var $more = $self.find('.b-switch-list-view__add-items');
                                    if ($responseMore.length) {
                                        if (!$more.length) {
                                            $more = $($responseMore.parent().html()).appendTo($self.find('.b-switch-list-view__add-items-wrap'));
                                        }
                                        $more.show().data('url', $responseMore.data('url'));
                                    }
                                    else {
                                        $more.hide();
                                    }

                                    self.afterRequest(true, data);
                                })
                                .fail(function () {
                                    $self.find('.b-switch-list-view__add-items').hide();

                                    self.afterRequest(false);
                                })
                            ;
                        }
                    }
                };

                // Обработчк действий после загрузки данных, которые не связаны с самим списком данных
                self.afterRequest = function (success, data, firstLoad) {

                    // отмечаем, что загрузка завершена
                    self.loading = false;
                    // Проверяем есть ли измененные фильтры и повторяем запрос на сервер
                    self.load();

                    // Прячем loader
                    $loader.hide();

                    // Обновляем адресную строку
                    if (success && 'url' in data) {

                        self.filterUrl = data.url;

                        if ($('.b-search').length && data.page == 0) {
                            if ('pushState' in window.history) {
                                if (locationHistory >= history.length) {
                                    window.history.pushState(self.filterData, document.title, data.url);
                                    locationHistory = history.length;
                                }
                            }
                        }
                    }

                    // Обновляем звездочки для табличного вида
                    $self.find(".raty-cell.raty-icons").each(function () {
                        var $elm = $(this),
                            target = $elm.data("target"),
                            score = $elm.data("score"),
                            id = $elm.attr('id');

                        jQuery('#' + id).raty({
                            readOnly: true,
                            path: $elm.data("asset-path"),
                            width: '200px',
                            score: score,
                            target: '#' + target
                        });
                        jQuery('#' + target).hide();
                    });

                    // Кнопка сравнения выставок
                    if (typeof FairMatch != 'undefined') {
                        FairMatch();
                    }

                    // Длинные текста ".trim-clamp:visible"
                    if (!firstLoad && typeof trimmingString != 'undefined') {
                        trimmingString();
                    }

                    // Изменение позиции полей в карточке выставки, если регион не влез в одну строку
                    if (typeof changePositionDateRegion != 'undefined') {
                        changePositionDateRegion();
                    }

                    // Обновление фильтров
                    if (success && 'filters' in data) {
                        var $filters = $('.b-search-filters__items').find('.js-list-auto-complete');
                        var $filtersData = $(data.filters);

                        $filters
                        //.filter(':not(.active)')
                            .each(function () {
                                var $filter = $(this);
                                var $list = $filter.find('.js-drop-list');
                                var $filterItems = $list.find('.js-list-auto-complete__main-list');
                                var $filterDataItems = $filtersData
                                    .find('#' + $list.attr('id'))
                                    .find('.js-list-auto-complete__main-list');

                                var loadFlag = false;

                                if (self.$lastActiveFilter && (self.$lastActiveFilter.get(0) == $filter.get(0))) {
                                    if ($filterDataItems.children().length > $filterItems.children().length) {
                                        loadFlag = true;
                                    }
                                }
                                else {
                                    loadFlag = true;
                                }

                                if (loadFlag) {
                                    $filterItems.empty().html($filterDataItems.html());
                                    var count = $filterItems.find(':checked').length;
                                    var $label = $filter.find('.js-list-auto-complete__drop-button');
                                    var label = $label.text().replace(/\(.*/, '');
                                    if (count) {
                                        label += '(' + count + ')';
                                    }
                                    $label.text(label);
                                }

                                $list.find('input').each(function () {
                                    jsCheckboxes($(this));
                                });
                            });
                    }

                    // Обновление "НАЙДЕНО:"
                    if (success && 'counter' in data) {
                        var $cntBlock = $('.b-search-counters');
                        if (!$cntBlock.length) {
                            $('.js-b-search-counter').html(data.counter);
                        }
                        else {
                            $cntBlock.show().html($(data.counter).html());
                        }
                    }

                };

                // Показать ЕЩЁ
                $self.on('click', '.b-switch-list-view__add-items', function () {
                    if (!self.loading) {
                        if ($('form.b-search-form').length) {
                            self.filterChange({
                                page: (self.filterData.page || 0) + 1
                            }, true);
                        }
                        else {
                            var changes = getChangesFromUrl({}, this.getAttribute('data-url'));
                            self.filterChange(changes);
                        }
                    }
                    return false;
                });

                // Блок сортировки
                $('.b-search-sorter').each(function () {
                    var $me = $(this);
                    var $sortField = $me.find('.b-search-sorter__field');
                    var $sortDirect = $me.find('.b-search-sorter__direction');

                    var sort = function (changeDirection) {
                        var sort;
                        var fieldName = $sortField.find('option:selected').data('field');
                        if (fieldName) {

                            var $direct = $sortDirect.find('.js-b-search-sorter__action');
                            var direction = 0;

                            direction += +$direct.hasClass('b-search-sorter__direction-item--asc');
                            direction -= +$direct.hasClass('b-search-sorter__direction-item--desc');

                            if (changeDirection) {
                                direction *= -1;
                                $direct.toggleClass('b-search-sorter__direction-item--asc', direction > 0);
                                $direct.toggleClass('b-search-sorter__direction-item--desc', direction < 0);
                            }

                            sort = (direction > 0 ? '' : '-') + fieldName;
                        }
                        self.filterChange({
                            page: 0,
                            sort: sort
                        });
                    };

                    $sortField.on('change', function () {
                        sort();
                    });

                    $sortDirect.on('click', function () {
                        sort(true);
                    });
                });

                // Сортировка в табличном виде
                $self.on('click', '.switch-list-view__sorter-item', function () {
                    var changes = getChangesFromUrl({
                        page: 0
                    }, this.getAttribute('data-url'));
                    self.filterChange(changes);
                    return false;
                });

                // Фильтр типов планировке в каталоге стендов
                $self.on('change', '.b-typeSelectImage__checkbox', function () {
                    if (this.checked) {
                        var changes = {
                            page: 0,
                            type: this.value
                        };
                        self.filterChange(changes);
                    }
                    return false;
                });

                // Переключение между карточным и табличным видом
                $(document).on('click', '.list-switcher > div:not(.list-switcher--active)', function () {
                    var $switcher = $(this),
                        $switcherBlock = $switcher.closest('.list-switcher'),
                        $activeSwitcher = $switcherBlock.find('.list-switcher--active');
                    $switcher.addClass('list-switcher--active');
                    $activeSwitcher.removeClass('list-switcher--active');
                    $('.b-switch-list-view[data-switcher=' + $switcherBlock.data('switcher-name') + ']')
                        .toggleClass('b-switch-list-view--as-block b-switch-list-view--as-row')
                    ;
                    changePositionDateRegion();
                });

                // Динамическое обновление по выбранным фильтрам
                $('.b-search-filters__items').on('change', 'input[type=checkbox]', function () {
                    console.log(1);
                    var name = this.name.replace(/\w+\[(\w+)].+/i, '$1');
                    var value = this.value;
                    var filter = self.filterData;
                    var data = (name in filter && (filter[name].toString().indexOf('_') != -1 || filter[name].toString().length))
                        ? filter[name].toString().split('_') : [];

                    if (!~this.name.indexOf('[]')) {
                        data = [];
                    }

                    if (this.checked) {
                        if (this.value) {
                            data.push(this.value);
                        }
                    }
                    else {
                        data = data.filter(function (v) {
                            return v != value
                        });
                    }

                    var changes = {
                        page: 0
                    };
                    changes[name] = data.join('_');
                    var filterInput = $('div[data-input-id="' + name + '"] .list-auto-complete__match-input > input');
                    if (filterInput.length && filterInput.val() != '') {
                        changes['inputValue'] = filterInput.val();
                        changes['inputName'] = name;
                    }

                    self.$lastActiveFilter = $('.b-search-filters__items').find('.js-list-auto-complete.active');
                    self.filterChange(changes);
                });

                self.afterRequest(true, {}, true);
            }
        });

    $(window).on('popstate', function (event) {
        if (event.state) {
            locationHistory--;
            location.reload();
        }
    }, false);

    $(window).load(function () {
        trimmingString();
    });
});