/**
 * Created by Евгений on 25.04.2015.
 */
$(function () {
	$(document).delegate('.js-pagination-button', 'click', function () {
		var $button = $(this),
			$list = $button.closest('.js-block-list'),
			$block = $list.find('.b-switch-list-view--as-block');

		$.get($button.attr('href'), function (data) {
			var $newList = $(data),
				$newItems = $newList.find('.b-switch-list-view__item');

			if ($newItems.length) {
				$newItems.each(function () {
					$block.append($(this))
				});
				$button.parent().html(
					$newList.find('.b-switch-list-view__add-items-wrap').html()
				);
			}
		});
		return false
	});
});