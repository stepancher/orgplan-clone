function alignMaxSizeBlog() {
	var $thumbnails = $('.blog-thumbnails .thumbnails li .thumbnail');
	var max = 0;

	$thumbnails.each(function (i, elm) {
		if (max < $(elm).height()) {
			max = $(elm).height();
		}
	});

	$thumbnails.height(max);
}

function alignMaxSizeExpert() {
	var $thumbnails = $('.expert-thumbnails .thumbnails li .thumbnail');
	var max = 0;

	$thumbnails.each(function (i, elm) {
		if (max < $(elm).height()) {
			max = $(elm).height();
		}
	});

	$thumbnails.height(max);
}

$(document).on('click', '.show-more-blog-items div', function () {
	$.get('?', {
		count: $('.show-more-blog-items div').data('count'),
		'Blog[id]': $('#Blog_id').val()
	}, function (html) {
		$('.blog-thumbnails').html(
			$(html).find('.blog-thumbnails .thumbnails')
		);
		alignMaxSizeBlog();
	});
});

$(document).on('click', '.show-more-expert-items div', function () {
	$.get('?', {
		count: $('.show-more-expert-items div').data('count'),
		'Expert[id]': $('#Expert_id').val()
	}, function (html) {
		$('.expert-thumbnails').html(
			$(html).find('.expert-thumbnails .thumbnails')
		);
		alignMaxSizeExpert();
	});
});

$(function () {
	alignMaxSizeBlog();

	$('body').delegate('.show-more-blog-items div', 'click', function () {
		$(this).hide();
	});
});$(function () {
	alignMaxSizeExpert();

	$('body').delegate('.show-more-expert-items div', 'click', function () {
		$(this).hide();
	});
});