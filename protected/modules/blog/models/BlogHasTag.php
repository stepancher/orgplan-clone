<?php
/**
 * Class BlogHasTag
 */
class BlogHasTag extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'blogId' => array(
				'integer',
				'label' => 'Блог',
				'relation' => array(
					'CBelongsToRelation',
					'Blog',
					array(
						'blogId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'tagId' => array(
				'integer',
				'label' => 'Тэг',
				'relation' => array(
					'CBelongsToRelation',
					'Tag',
					array(
						'tagId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);

	}
}