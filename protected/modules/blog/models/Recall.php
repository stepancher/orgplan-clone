<?php

/**
 * Class Recall
 */
class Recall extends \AR
{
    public static $_description = NULL;

    const TYPE_FAIR = 1;
    const TYPE_EXHIBITION_CENTER = 2;
    const TYPE_BLOG = 3;
    const TYPE_BID = 4;
    const TYPE_USER = 5;

    const STATUS_ACTIVE_OFF = 0;
    const STATUS_ACTIVE_ON = 1;


    public static function getTypeRouteView($type)
    {
        return \yii\helpers\ArrayHelper::getValue(
            [
                self::TYPE_FAIR => 'fair/fairComment',
                self::TYPE_EXHIBITION_CENTER => 'exhibitionComplex/html/view',
                self::TYPE_BLOG =>  'blog/view',
                self::TYPE_BID => 'bid/view',
                self::TYPE_USER => 'user/view',
            ],
            $type
        );
    }


    public static function getTypeClass($type)
    {
        return \yii\helpers\ArrayHelper::getValue(
            [
                self::TYPE_FAIR => 'Fair',
                self::TYPE_EXHIBITION_CENTER => 'ExhibitionComplex',
                self::TYPE_BLOG =>  'Blog',
                self::TYPE_BID => 'Bid',
                self::TYPE_USER => 'User',
            ],
            $type
        );
    }

    public static function types()
    {
        return array(
            self::TYPE_FAIR => Yii::t('recall', 'Exhibition'),
            self::TYPE_EXHIBITION_CENTER => Yii::t('recall', 'Exhibition Center'),
            self::TYPE_BLOG => Yii::t('recall', 'Blog'),
            self::TYPE_BID => Yii::t('recall', 'Bid'),
            self::TYPE_USER => Yii::t('recall', 'User'),
        );
    }

    public static function statuses()
    {
        return array(
            self::STATUS_ACTIVE_OFF => Yii::t('recall', 'off'),
            self::STATUS_ACTIVE_ON => Yii::t('recall', 'on'),
        );
    }


    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'comment' => array(
                'text',
                'label' => Yii::t('recall', 'Comment'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('required')
                    )
                ),
            ),
            'rating' => array(
                'integer',
                'label' => Yii::t('recall', 'Rating'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'integer',
                'label' => Yii::t('recall', 'Type'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'objectId' => array(
                'integer',
                'label' => Yii::t('recall', 'Object'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'authorId' => array(
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'authorId' => 'id',
                    ),
                ),
                'label' => Yii::t('recall', 'Author'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'date' => array(
                'datetime',
                'label' => Yii::t('recall', 'Date of creation'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'bool',
                'label' => Yii::t('recall', 'Activation'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'author_firstName' => array(
                'label' => User::model()->getAttributeLabel('firstName'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'author_contact_email' => array(
                'label' => Contact::model()->getAttributeLabel('email'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
        );
    }

    /**
     * Получение комментария в нужном виде
     * @param int $length
     * @return string
     */
    public function getComment($length = 300)
    {
        $showDetail = '';
        $link = '';
        if (strlen($this->comment) > $length) {
            $link = TbHtml::link(Yii::t('exhibitionComplex', 'Read completely'),
                Yii::app()->createUrl('#'),
                array(
                    'data-target' => 'comment-detail-' . $this->id,
                    'data-show' => 1,
                    'class' => 'comment-detail default-link',
                ));
            $text = mb_substr($this->comment, 0, $length, 'UTF-8');
            $text = '<span class="short-comment-detail-' . $this->id . '">' . $this->removeResidue($text) . '...> </span>';
            $showDetail = '<span class="all-comment-detail-' . $this->id . ' hide">' . $this->comment . '</span>';

        } else {
            $text = $this->comment;
        }
        return $text . ' ' . $showDetail . ' ' . $link;
    }

    /**
     * Удаление последнего (некрасивого) слова
     * @param $text
     * @return string
     */
    public function removeResidue($text)
    {
        $words = explode(' ', $text);
        $count = count($words);
        unset($words[$count - 1]);
        return implode(' ', $words);
    }
}