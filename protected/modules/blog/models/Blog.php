<?php
Yii::import('application.modules.blog.BlogModule');
/**
 * Class Blog
 *
 * @property string $shortUrl
 * @property string $theme
 * @property string $seoDescription
 * @property string $title
 * @property string $description
 * @property string $keywords
 */
class Blog extends \AR
{
    public static $_description = NULL;

    const STATUS_NEW = 0;
    const STATUS_HIDDEN = 1;
    const STATUS_POSTED = 2;

    public static function status()
    {
        return array(
            self::STATUS_NEW => Yii::t('BlogModule.blog', 'New'),
            self::STATUS_HIDDEN => Yii::t('BlogModule.blog', 'Private'),
            self::STATUS_POSTED => Yii::t('BlogModule.blog', 'Published'),
        );
    }

    /**
     * Поля-исключения, в которых не нужно делать проверку purify
     * @var array
     */
    public $notPurifyAttributes = [
        'text'
    ];

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'created' => array(
                'datetime',
                'label' => Yii::t('BlogModule.blog', 'Date of creation'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'theme' => array(
                'string',
                'label' => Yii::t('BlogModule.blog', 'Theme'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('required'),
                        array('unique'),
                        array('uniqueUrl'),
                    )
                ),
            ),
            'shortUrl' => array(
                'string',
                'label' => Yii::t('BlogModule.blog', 'shortUrl'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('unique'),
                        array('uniqueUrl'),
                    )
                ),
            ),
            'text' => array(
                'text',
                'label' => Yii::t('BlogModule.blog', 'Description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('required')
                    )
                ),
            ),
            'status' => array(
                'integer',
                'label' => Yii::t('BlogModule.blog', 'Status'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
            ),
            'authorId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'authorId' => 'id',
                    ),
                ),
            ),
            'imageId' => array(
                'label' => Yii::t('BlogModule.blog', 'Title image'),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'statistics' => array(
                'bool',
                'label' => Yii::t('BlogModule.blog', 'add popular'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'viewCounter' => array(
                'integer',
                'label' => Yii::t('BlogModule.blog', 'View Counter'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'seoDescription' => array(
                'text',
                'label' => Yii::t('BlogModule.blog', 'SEO Description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exhibitionComplexHasBlogs' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'ExhibitionComplexHasBlog',
                    array(
                        'blogId' => 'id',
                    ),
                ),
            ),
            'blogHasComments' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'BlogHasComment',
                    array(
                        'blogId' => 'id',
                    ),
                ),
            ),
            'blogHasFiles' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'BlogHasFile',
                    array(
                        'blogId' => 'id',
                    ),
                ),
            ),
            'blogHasTags' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'BlogHasTag',
                    array(
                        'blogId' => 'id',
                    ),
                ),
            ),
            'tag' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'author' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'author_firstName' => array(
                'label' => 'Автор',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'trBlogs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrBlog',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'title' => array(
                'label' => 'title',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'description' => array(
                'label' => 'description',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'keywords' => array(
                'label' => 'keywords',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }

    public function uniqueUrl()
    {

    }

    public function createShortUrl(){
        return ToTransliteration::getInstance()->replace($this->theme);
    }

    public function beforeSave()
    {
        $this->shortUrl = $this->createShortUrl();

        return parent::beforeSave();
    }

    /**
     * @inheritdoc
     */
    public function afterSave()
    {
        parent::afterSave();

        /** @var CWebApplication $app */
        $app = \Yii::app();
        if ($app instanceof CWebApplication) {
            $route = 'blog/default/viewArticle';
            $params = ['id' => (string)intval($this->id)];
            $attributes = [
                'route' => 'blog/default/viewArticle',
                'params' => json_encode($params)
            ];
            
            Yii::app()->urlManager->createUrl($route, $params, $app->params['defaultAmpersand'], FALSE);
        }
    }

    static function getFreshBlog()
    {
        $criteria = new CDbCriteria();

        $criteria->condition = 'DATE(created) <= DATE(CURRENT_DATE)';
        $criteria->order = 'created DESC';
        $criteria->limit = 3;
        $criteria->addNotInCondition('t.status', [Blog::STATUS_HIDDEN, Blog::STATUS_NEW]);
        $blog = Blog::model()->findAll($criteria);
        if ($blog) {
            return $blog;
        }
        return null;
    }


    static function getPopularBlog()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'viewCounter DESC';
        $criteria->limit = 3;
        //$criteria->compare('statistics', 1);
        $blog = Blog::model()->findAll($criteria);
        return $blog;
    }

    public function upViewCounter()
    {
        return $this->updateByPk($this->id, [
            'viewCounter' => new CDbExpression(
                $this->getDbConnection()->quoteColumnName('viewCounter') . ' + 1'
            )
        ]);
    }

    public function getSeoName()
    {
        $nameParts = [];
        $nameParts[] = 'blog';
        $nameParts[] = ToTransliteration::getInstance()->replace($this->theme);
        return implode('/', $nameParts);
    }

    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    public function getMainImage()
    {
        $result = false;
        if ($this->blogHasFiles) {
            foreach ($this->blogHasFiles as $file) {
                if ($file->file && $file->file->type == ObjectFile::TYPE_BLOG_MAIN_IMAGE) {
                    $result = $file;
                }
            }
        }
        return $result;
    }

    /**
     * Получение тегов блога
     * @return string
     */
    public function getBlogTags()
    {
        $result = '';
        if ($BHTs = $this->blogHasTags) {
            foreach ($BHTs as $bht) {
                if ($bht->tag) {
                    $result .= $bht->tag->name . ' ';
                }
            }
        }

        return $result;
    }

    /**
     * @param array $params
     * @return CActiveDataProvider
     * @throws Exception
     */
    static function dataProvider($params = [], &$model = null)
    {
        $page = \yii\helpers\ArrayHelper::getValue($params, 'page', 0);
        $url = \yii\helpers\ArrayHelper::getValue($params, 'url', null);

        $dataProvider = new CActiveDataProvider('Blog', [
            'pagination' => [
                'pageSize' => 6,
                'currentPage' => $page,
            ],
        ]);

        $tagModel = Tag::model()->findByAttributes(['shortUrl' => $url]);

        if($tagModel === null && $url !== null){

            throw new CHttpException(404, 'Page not found');
        }
        elseif ($tagModel !== null && $url !== null)
        {

            if (null === $tagModel || !$tagModel->blogHasTags) {
                throw new \Exception('Tag not found', 1);
            }
            $dataProvider->criteria->with = ['blogHasTags' => [
                'together' => true
            ]];
            $dataProvider->criteria->condition = 'DATE(created) <= DATE(CURRENT_DATE)';
            $dataProvider->criteria->order = 'created DESC';
            $dataProvider->criteria->compare('blogHasTags.tagId', $tagModel->id);
            $dataProvider->criteria->addNotInCondition('t.status', [Blog::STATUS_HIDDEN, Blog::STATUS_NEW]);
            $model = Blog::model()->find($dataProvider->criteria);
            if (null !== $model) {
                $dataProvider->criteria->addNotInCondition('t.id', [$model->id]);
            } else {
                throw new \Exception('Blog not found', 2);
            }
        } else {
            $criteria = new CDbCriteria();
            $criteria->order = 'created DESC';
            $criteria->addNotInCondition('t.status', [Blog::STATUS_HIDDEN, Blog::STATUS_NEW]);
            $model = Blog::model()->find($criteria);
            $dataProvider->criteria = $criteria;
            $dataProvider->criteria->addNotInCondition('t.status', [Blog::STATUS_HIDDEN, Blog::STATUS_NEW]);
            if (null !== $model) {
                $dataProvider->criteria->addNotInCondition('t.id', [$model->id]);
            }
        }

        return $dataProvider;
    }
}