<?php

/**
 * Class Tag
 * @property string shortUrl
 * @property string name
 */
class Tag extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => 'Наименование',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'shortUrl' => array(
                'string',
                'label' => 'shortUrl',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'blogHasTags' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'BlogHasTag',
                    array(
                        'tagId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }

    /**
     * Получение название тега по его идентификатору
     * @param $id
     * @return string
     */
    public static function getTagName($id)
    {
        $model = static::model()->findByPk($id);

        return null != $model ? $model->name : null;
    }

    public static function getTagNameByShortUrl($tagUrl){

        $model = static::model()->findByAttributes(['shortUrl' => $tagUrl]);

        return null != $model ? $model->name : null;
    }

    /**
     *
     * @param bool $link
     * @return string
     */
    public static function getTags($link = false)
    {
        $criteria = new CDbCriteria();
        $criteria->with = ['blogHasTags', 'blogHasTags.blog'];
        $criteria->addCondition([
            't.name IS NOT NULL AND t.name != \'\'',
            'blog.status = ' . Blog::STATUS_POSTED,
        ]);
        $modelTags = Tag::model()->findAll($criteria);
        $result = '';
        if(null !== $modelTags) {
            foreach ($modelTags as $tag) {
                if($link) {
                    $result .= CHtml::link($tag->name,
                        Yii::app()->createUrl('blog/default/view', ['url' => isset($tag->shortUrl) ? $tag->shortUrl : '']),
                        ['class' => 'b-button d-bg-secondary-button d-bg-tab--hover d-text-dark btn fixed-auto-width offset-small-r p-centralize-p p-fill-r-2 p-fill-l-2 inner-button', 'itemprop'=>'url']
                    ). ' ';
                } else {
                    $result .= $tag->name . ' ';
                }
            }
        }
        return $result;
    }
}