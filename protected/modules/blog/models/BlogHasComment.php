<?php
/**
 * Class BlogHasComment
 */
class BlogHasComment extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'commentId' => array(
				'integer',
				'label' => 'Комментарий',
				'relation' => array(
					'CBelongsToRelation',
					'Comment',
					array(
						'commentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'blogId' => array(
				'label' => 'Блог',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'Blog',
					array(
						'blogId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}