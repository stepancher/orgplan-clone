<?php

/**
 * Class BlogController
 */
class DefaultController extends Controller
{

    public $defaultAction = 'exit';
    public $seoDescription = '';
    public $title = '';
    public $ogMetaUrl = '';
    public $ogMetaImage = '';

    /**
     * @var Blog $model
     */
    public $model = 'Blog';

    public $authorId = array();

    public function actionExit(){
        $this->redirect(Yii::app()->createUrl('/blog', array('langId' => Yii::app()->language)));
    }

    /**
     * Список записей блога
     * @param int $page
     * @param null $url
     * @throws CHttpException
     */
    public function actionIndex($page = 0, $url = null)
    {
        $model = null;

        $dataProvider = Blog::dataProvider([
            'page' => $page,
            'url' => $url
        ], $model);

        if(isset($url)){
            $requestUrl = $this->createUrl('index') . DIRECTORY_SEPARATOR . $url;
        } else {
            $requestUrl = $this->createUrl('index');
        }

        if ($dataProvider->totalItemCount > 0) {
            if ($page * $dataProvider->pagination->pageSize >= $dataProvider->totalItemCount) {
                throw new CHttpException(404, Yii::t('BlogModule.blog', 'Unable to resolve the request "{route}"',
                    array('{route}' => $_SERVER['REQUEST_URI'])));
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo json_encode([
                'content' => $this->renderPartial('_list', [
                    'dataProvider' => $dataProvider,
                    'requestUrl' => $requestUrl,
                    'itemView' => '_card'
                ], true)
            ]);
        } else {

            /**@var Blog    $model */
            if(isset($model)){
                $this->pageTitle = $model->title;
            }

            if (null != $url) {
                $this->pageTitle = Tag::getTagNameByShortUrl($url);
            }

            $this->render('view', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'requestUrl' => $requestUrl,
            ]);
        }
    }

    /**
     * @param $id
     * Блоги пользователей
     */
    public function actionUserBlogs($id)
    {
        $this->render('userBlogs', array(
            'model' => Blog::model(),
        ));
    }

    /**
     * @param null $url
     * @param int $page
     * @return null
     * @throws CException
     * @throws CHttpException
     * @throws Exception
     */
    public function actionView($url = null, $page = 0)
    {
        $model = Blog::model()->findByAttributes(['shortUrl' => $url]);

        /** @var Blog $model */
        if ($model === null) {

            $dataProvider = Blog::dataProvider([
                'page' => $page,
                'url' => $url
            ], $model);

            if(isset($url)){
                $requestUrl = $this->createUrl('index') . DIRECTORY_SEPARATOR . $url;
            } else {
                $requestUrl = $this->createUrl('index');
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo json_encode([
                    'content' => $this->renderPartial('_list', [
                        'dataProvider' => $dataProvider,
                        'requestUrl' => $requestUrl,
                        'itemView' => '_card'
                    ], true)
                ]);

                return null;
            } else {

                /**@var Blog    $model */
                if(isset($model)){
                    $this->pageTitle = $model->title;
                }

                if (null != $url) {
                    $this->pageTitle = Tag::getTagNameByShortUrl($url);
                }
            }

            $header = Yii::t('BlogModule.blog', 'Blog');
            $description = Tag::getTagNameByShortUrl($url);
        } else {

            $ctrl = $this;
            $records = AR::multiSave(
                $_POST,
                array(
                    function ($records) use ($model) {
                        /** @var Recall $recall */
                        $recall = $records['recall'];
                        $recall->date = date('Y-m-d H:i:s', time());
                        $recall->authorId = Yii::app()->user->id;
                        $recall->type = Recall::TYPE_BLOG;
                        $recall->objectId = $model->id;
                        $recall->active = User::ACTIVE_ON;
                    }
                ),
                array(
                    array(
                        'name' => 'recall',
                        'class' => 'Recall',
                        'record' => new Recall()
                    ),
                    function ($records) use ($ctrl, $model) {
                        /** @var BlogHasComment $BHC */
                        $BHC = $records['blogHasComment'];
                        $BHC->blogId = $model->id;
                        $BHC->commentId = $records['recall']->id;
                    },
                ),
                array(
                    array(
                        'name' => 'blogHasComment',
                        'class' => 'BlogHasComment',
                        'record' => new BlogHasComment()
                    ),
                    function ($records) use ($ctrl) {
                        IRedactor::createItems(get_class($records['recall']));
                        IRedactor::clearUploadFiles();

                        Yii::app()->user->setFlash('success', Yii::t('BlogModule.blog', 'The information is saved'));
                        $this->refresh();
                    }
                )
            );
            /** Очистка файлов временной директории, если все модели прошли валидацию */
            IRedactor::clearUploadFiles($records);

            $this->pageTitle = $model->theme;

            $model->upViewCounter();

            $header = $model->theme;
            $description = '';
        }

        if(isset($model->seoDescription) && !empty($model->seoDescription))
            $this->seoDescription = $model->seoDescription;
        if(isset($model->theme) && !empty($model->theme))
            $this->title = $model->theme;
        if(isset($model->shortUrl) && !empty($model->shortUrl))
            $this->ogMetaUrl = Yii::app()->createAbsoluteUrl('blog/default/view', array('url' => $model->shortUrl));
        if(isset($model->blogHasFiles) && !empty($model->blogHasFiles) && isset(Yii::app()->request->hostInfo))
            $this->ogMetaImage = Yii::app()->request->hostInfo . H::getImageUrl($model->getMainImage(), 'file', 'noImage', '620-348_');

        $this->render('view',
            CMap::mergeArray(array(
                'model' => $model,
                'dataProvider' => isset($dataProvider) ? $dataProvider : null,
                'requestUrl' => isset($requestUrl) ? $requestUrl : null,
                'header' => $header,
                'description' => $description,
            ), isset($records) ? $records : [])
        );
    }
}