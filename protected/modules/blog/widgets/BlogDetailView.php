<?php
/**
 * File BlogDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class BlogDetailView
 *
 * @property Blog $data
 */
class BlogDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());

		$this->attributes['id']['visible'] = false;
		$this->attributes['status']['visible'] = false;
		$this->attributes['imageId']['visible'] = false;
		$this->attributes['authorId']['value'] = $this->data->author->login;
		$this->attributes['text']['type'] = 'html';

		parent::init();
	}
}