<?php
/**
* File BlogHasTagForm.php
*/
Yii::import('ext.helpers.ARForm');

/**
* Class BlogHasTag*/
class BlogHasTagForm extends TbForm
{
	/**
	* Префикс в аттрибут name (например: "[]") для обработки полей формы с name=BlogHasTag[]['login']
	* @var array
	*/
	public $elementGroupName = '';

	/**
	* Инициализация формы
	*/
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();
		/** @var BlogHasTag $model */
		$model = $this->getModel();

		$this->elements = CMap::mergeArray(
			ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
			$this->initElements()
		);

		parent::init();
	}

	/**
	* Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=BlogHasTag[]['login']
	* @param string $name
	* @param TbFormInputElement $element
	* @param bool $forButtons
	*/
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	* Инициализация элементов формы
	* @return array
	*/
	public function initElements()
	{
		return array(
			'id' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'blogId' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'tagId' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
		);
	}
}