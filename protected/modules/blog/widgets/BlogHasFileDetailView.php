<?php
/**
 * File BlogHasFileDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class BlogHasFileDetailView
 *
 * @property BlogHasFile $data
 */
class BlogHasFileDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		
		$this->attributes['blogId']['value'] = $this->data->blogId ? CHtml::link(
			$this->data->blog->theme,
			$this->getOwner()->createUrl(
				'/blog/view',
				array('id' => $this->data->blogId)
			)
		) : null;
		$this->attributes['blogId']['type'] = 'raw';

		$this->attributes['fileId']['value'] = $this->data->fileId ? CHtml::link(
			$this->data->file->name,
			$this->getOwner()->createUrl(
				'/file/objectFile/view',
				array('id' => $this->data->fileId)
			)
		) : null;
		$this->attributes['fileId']['type'] = 'raw';

		parent::init();
	}
}