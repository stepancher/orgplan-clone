<?php
/**
 * File BlogHasTagDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class BlogHasTagDetailView
 *
 * @property BlogHasTag $data
 */
class BlogHasTagDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		
		parent::init();
	}
}