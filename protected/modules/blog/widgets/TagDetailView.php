<?php
/**
 * File TagDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class TagDetailView
 *
 * @property Tag $data
 */
class TagDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		
		parent::init();
	}
}