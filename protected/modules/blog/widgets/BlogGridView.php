<?php
/**
 * File BlogGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class BlogGridView
 *
 * @property Blog $model
 */
class BlogGridView extends TbGridView
{
	/**
	 * Модель Blog::model() or new Blog('scenario')
	 * @var Blog
	 */
	public $model;

	/**
	 * Двумерный массив аргументов для создания условий CDbCriteria::compare
	 * @see CDbCriteria::compare
	 * @var array
	 */
	public $compare = array(
		array()
	);
	/**
	 * Для добавления колонок в начало таблицы
	 * @var array
	 */
	public $columnsPrepend = array();

	/**
	 * Для добавления колонок в конец таблицы
	 * @var array
	 */
	public $columnsAppend = array();

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate {@link columns} objects.
	 */
	public function init()
	{
		// Включаем фильтрацию по сценарию search
		$this->model->setScenario('search');
		$this->filter = ARGridView::createFilter($this->model, true);

		// Создаем колонки таблицы
		$this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

		// Создаем провайдер данных для таблицы
		$this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

		// Инициализация
		parent::init();
	}

	/**
	 * Колонки таблицы
	 * @return array
	 */
	public function getColumns()
	{
		$columns = array(
			'imageId' => array(
				'name' => 'imageId',
				'type' => 'html',
				'htmlOptions' => array(
					'width' => '150px'
				),
				'value' => function ($data) {
						/**@var Blog $data */
						return TbHtml::image(H::getImageUrl($data, 'image'));
					}
			),
			'created' => array(
				'name' => 'created',
				'value' => 'date("d.m.Y H:i:s",strtotime($data->created))'
			),
			'authorId' => array(
				'name' => 'authorId',
				'value' => '$data->authorId ? $data->author->login : null',
			),
			'theme' => array(
				'name' => 'theme',
				'type' => 'raw',
				'value' => 'CHtml::link($data->theme, array("blog/view", "id" => $data->id))'
			),
			'text' => array(
				'name' => 'text',
				'value' => 'substr(strip_tags($data->text),0,160)',
				'type' => 'html'
			),
			'status' => array(
				'name' => 'status',
				'visible' => false
			)
		);
		return $columns;
	}
}