<?php
/**
* File BlogHasCommentForm.php
*/
Yii::import('ext.helpers.ARForm');

/**
* Class BlogHasComment*/
class BlogHasCommentForm extends TbForm
{
	/**
	* Префикс в аттрибут name (например: "[]") для обработки полей формы с name=BlogHasComment[]['login']
	* @var array
	*/
	public $elementGroupName = '';

	/**
	* Инициализация формы
	*/
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();
		/** @var BlogHasComment $model */
		$model = $this->getModel();

		$this->elements = CMap::mergeArray(
			ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
			$this->initElements()
		);

		parent::init();
	}

	/**
	* Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=BlogHasComment[]['login']
	* @param string $name
	* @param TbFormInputElement $element
	* @param bool $forButtons
	*/
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	* Инициализация элементов формы
	* @return array
	*/
	public function initElements()
	{
		return array(
			'id' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'commentId' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
			),
			'blogId' => array(
				'groupOptions' => array(
					'class' => 'span12',
				),
				'items' => CHtml::listData(Blog::model()->findAll(), 'id', 'theme'),
			),
		);
	}
}