<?php
/**
 * File BlogHasFileGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class BlogHasFileGridView
 *
 * @property BlogHasFile $model
 */
class BlogHasFileGridView extends TbGridView
{
	/**
	 * Модель BlogHasFile::model() or new BlogHasFile('scenario')
	 * @var BlogHasFile	 */
	public $model;

	/**
	 * Двумерный массив аргументов для создания условий CDbCriteria::compare
	 * @see CDbCriteria::compare
	 * @var array
	 */
	public $compare = array(
		array()
	);
	/**
	 * Для добавления колонок в начало таблицы
	 * @var array
	 */
	public $columnsPrepend = array();

	/**
	 * Для добавления колонок в конец таблицы
	 * @var array
	 */
	public $columnsAppend = array();

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate {@link columns} objects.
	 */
	public function init()
	{
		// Включаем фильтрацию по сценарию search
		$this->model->setScenario('search');
		$this->filter = ARGridView::createFilter($this->model, true);

		// Создаем колонки таблицы
		$this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

		// Создаем провайдер данных для таблицы
		$this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

		// Инициализация
		parent::init();
	}

	/**
	 * Колонки таблицы
	 * @return array
	 */
	public function getColumns()
	{
		$columns = array(
			'id' => array(
				'name' => 'id',
			),
			'blogId' => array(
				'name' => 'blogId',
				'filter' => CHtml::listData(Blog::model()->findAll(), 'id', 'theme'),
				'value' => '$data->blogId ? $data->blog->theme : null',
			),
			'fileId' => array(
				'name' => 'fileId',
				'filter' => CHtml::listData(ObjectFile::model()->findAll(), 'id', 'name'),
				'value' => '$data->fileId ? $data->file->name : null',
			),
		);
		return $columns;
	}
}