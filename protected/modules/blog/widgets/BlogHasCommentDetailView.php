<?php
/**
 * File BlogHasCommentDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class BlogHasCommentDetailView
 *
 * @property BlogHasComment $data
 */
class BlogHasCommentDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		
		$this->attributes['blogId']['value'] = $this->data->blogId ? CHtml::link(
			$this->data->blog->theme,
			$this->getOwner()->createUrl(
				'/blog/view',
				array('id' => $this->data->blogId)
			)
		) : null;
		$this->attributes['blogId']['type'] = 'raw';

		parent::init();
	}
}