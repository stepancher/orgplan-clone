<?php
/**
 * File RecallForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class Recall*/
class RecallForm extends TbForm
{
    /**
     * Префикс в аттрибут name (например: "[]") для обработки полей формы с name=Recall[]['login']
     * @var array
     */
    public $elementGroupName = '';
    public $hints = [];
    public $raty = true;
    public $template = '
		<div class="cols-row">
			<div class="col-md-12">
			{comment}
			</div>
		</div>
		<div class="cols-row">
			<div class="col-md-12">
			{raty}
			</div>
		</div>
		<div class="cols-row">
			<div class="col-md-12">
					{submit}
			</div>
		</div>
';

    public function renderElements()
    {
        $parts = array();
        $parts['{raty}'] = $this->raty == true ? $this->renderRaty() : null;
        $parts['{submit}'] = TbHtml::submitButton(
            Yii::t('BlogModule.blog', 'Send'),
            array(
                'class' => 'submit-recall-button b-button d-bg-success-dark d-bg-success--hover d-text-light p-pull-right fixed-width-2 btn',
            )
        );

        foreach ($this->getElements() as $element) {
            $parts['{' . $element->name . '}'] = $this->renderElement($element);
        }
        return strtr($this->template, $parts);
    }

    public function renderRaty()
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        $am = $app->getAssetManager();
        $publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);
        return $this->getOwner()->widget('ext.dzRaty.DzRaty', array(
            'model' => $this->getModel(),
            'attribute' => 'rating',
            'options' => array(
                'path' => $publishUrl . '/img/raty',
                'starOff' => 'star-off.png',
                'starOn' => 'star-on.png',
                'width' => '155px',
            ),
            'htmlOptions' => array(
                'style' => 'float: right; margin-bottom: 9px'
            ),
            'hints' => $this->hints,
            'data' => ['1', '2', '3', '4', '5']
        ), true);
    }

    /**
     * Инициализация формы
     */
    public function init()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();
        /** @var Recall $model */
        $model = $this->getModel();

        $this->elements = CMap::mergeArray(
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );
        CHtml::$afterRequiredLabel = '';
        $this->registerClientScript();

        parent::init();
    }

    protected function registerClientScript()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        $am = $app->getAssetManager();
        $cs = $app->getClientScript();
        $publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);

        $cs->registerScriptFile($publishUrl.'/js/recall/checkRecall.js');
    }

    /**
     * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=Recall[]['login']
     * @param string $name
     * @param TbFormInputElement $element
     * @param bool $forButtons
     */
    public function addedElement($name, $element, $forButtons)
    {
        $element->name = $this->elementGroupName . $name;
    }

    /**
     * Инициализация элементов формы
     * @return array
     */
    public function initElements()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();

        return array(
            'id' => array(
                'visible' => false
            ),
            'comment' => array(
                'type' => TbHtml::INPUT_TYPE_TEXTAREA,
                'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark f-text--base',
                'attributes' => array(
                    'label' => '',
                ),
                'placeholder' => Yii::t('BlogModule.blog', 'Leave a comment'),
            ),
            'rating' => array(
                'groupOptions' => array(
                    'class' => 'input-block-level',
                ),
                'placeholder' => 'Введите рейтинг от 0 до 5.',
                'type' => TbHtml::INPUT_TYPE_NUMBER,
            ),
            'type' => array(
                'visible' => false
            ),
            'objectId' => array(
                'visible' => false
            ),
            'authorId' => array(
                'visible' => false
            ),
            'date' => array(
                'visible' => false
            ),
        );
    }
}