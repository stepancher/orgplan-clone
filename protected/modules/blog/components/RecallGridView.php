<?php
/**
 * File RecallGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class RecallGridView
 *
 * @property Recall $model
 */
class RecallGridView extends TbGridView
{
    public $raty = true;
    /**
     * Модель Recall::model() or new Recall('scenario')
     * @var Recall
     */
    public $model;

    /**
     * Опция. Если true, ссылки ведут на профиль пользователя. В обратном случае, ссылки ведут на страницу 404 ошибки.
     * @var bool
     */
    public $onLink = true;

    /**
     * @var string
     */
    public $enableFilter;

    /**
     * @var bool
     */
    public $hideOnEmpty = false;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = $this->enableFilter ? ARGridView::createFilter($this->model, true) : null;

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);
        if ($this->hideOnEmpty && $this->dataProvider->itemCount == 0) {
            $this->htmlOptions['style'] = 'display: none;';
        }
        $this->dataProvider->pagination = false;


        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        /** @var Controller $ctrl */
        $ctrl = $this->getOwner();
        $columns = array(
            'date' => array(
                'value' => function ($data) use ($ctrl) {
                    /**
                     * @var CWebApplication $app
                     */
                    $app = Yii::app();
                    $am = $app->getAssetManager();
                    $publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);


                    /** @var Recall $data */
                    $link = $ctrl->createUrl('error/default/error');
                    ?>
                    <?php
                    $user = User::model()->findByPk($data->authorId);
                    if ($this->onLink && null != $user) {
                        $result = $user
                            ? TbHtml::link(
                                $user->firstName . ' ' . $user->lastName . '<br />',
                                $ctrl->createUrl('user/view', array('id' => $user->id)),
                                array(
                                    'class' => 'default-link',
                                    'itemprop' => 'creator'
                                )
                            )
                            : null;
                    } elseif(!empty($user)) {
                        $result = '<span class="default-link" itemprop="creator">' . $user->firstName . ' ' . $user->lastName . '</span>' . '<br />';
                    }else{
                        $result = '<span class="default-link" itemprop="creator">' . Yii::t('user', 'User deleted') . '</span>' . '<br />';
                    }
                    $result .= $this->raty == true ? '<span itemprop="datePublished" content="'. $data->date.'">' . date('d.m.Y', strtotime($data->date)) . '</span>' . '<br />'
                        : '<span itemprop="datePublished" content="'. $data->date.'">' . date('d.m.Y', strtotime($data->date)) . ' / ' . date('H:i:s', strtotime($data->date)) . '</span>';
                    ?>
                    <div itemscope itemtype="http://schema.org/Comment">
                        <?php
                        $result .= $this->raty == true ? $ctrl->widget('ext.dzRaty.DzRaty', array(
                            'model' => $data,
                            'attribute' => 'rating',
                            'htmlOptions' => array(
                                "id" => 'rating-' . $data->id,
                                "data-score" => $data->rating,
                                "class" => "raty-cell"
                            ),
                            'options' => array(
                                'readOnly' => true,
                                'path' => $publishUrl . '/img/raty',
                                'starOff' => 'star-off.png',
                                'starOn' => 'star-on.png',
                                'width' => '230px',
                            ),
                        ), true) : null;
                        return $result;
                        ?>
                    </div>
                    <?php
                },
                'type' => 'raw',
                'htmlOptions' => array(
                    'width' => '230px',
                ),

            ),
            'comment' => array(
                'name' => '',
                'value' => function ($data) {

                    return $data->getComment(200) ? '<div itemscope itemtype="http://schema.org/Comment"><span itemprop="comment">' . $data->getComment(200) . '</span></div>' : null;
                },
                'type' => 'raw',
            )
        );
        return $columns;
    }
}