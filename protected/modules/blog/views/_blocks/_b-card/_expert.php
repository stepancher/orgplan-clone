<?php
/**
 * @var User[] $models
 */
Yii::import('application.modules.blog.BlogModule');
?>
<div itemscope itemtype="http://schema.org/Blog">
    <h2 class="offset-top" itemprop="description"><span
            itemprop="name"><?= Yii::t('blogModule.blog', 'our experts') . ':' ?></span></h2>
</div>
<div class="items-blog">
    <?php
    foreach ($models as $model) {
        $linkAssets = H::getImageUrl($model, 'logo', 'noUserImage');
        $assets = strstr($linkAssets, 'assets', true);
        $newLinkAssets = dirname($linkAssets) . DIRECTORY_SEPARATOR . 'user-expert-logo-resized-' . basename($linkAssets);
        $newAbsoluteImagePath = ($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $newLinkAssets;
        if (!file_exists($newAbsoluteImagePath)) {
            $image = new Image(($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $linkAssets);
            $image->resize(80, 80, Image::AUTO);
            $image->save($newAbsoluteImagePath);
        }
        $imageHeight = getimagesize(($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $newLinkAssets)[1];
        ?>
        <div class="expert-card">
            <div itemscope itemtype="http://schema.org/Blog">
                <div class="p-pull-left">
                    <div class="left-part-logo">
                        <?php
                        if ($imageHeight < 80) {
                            $offset = (80 - $imageHeight) / 2;
                            echo TbHtml::image($newLinkAssets, $model->firstName . ' ' . $model->lastName, ['style' => 'margin-top:' . $offset . 'px; margin-bottom:' . $offset . 'px'],
                                array(
                                    'title' => $model->firstName . ' ' . $model->lastName,
                                    'itemprope' => 'image'
                                )
                            );
                        } else {
                            echo TbHtml::image($newLinkAssets, $model->firstName . ' ' . $model->lastName,
                                array(
                                    'title' => $model->firstName . ' ' . $model->lastName,
                                    'itemprope' => 'image'
                                )
                            );
                        }
                        ?>
                    </div>
                    <div class="left-part-count-blog">
                        <?= H::getCountText(count(Blog::model()->findAllByAttributes(['authorId' => $model->id, 'status' => Blog::STATUS_POSTED])), [Yii::t('blogModule.blog', 'articles'), Yii::t('blogModule.blog', 'article'), Yii::t('blogModule.blog', 'articles ')]) ?>
                    </div>
                </div>
                <div class="right-part">
                    <div class="right-part-name" itemprop="description">
                        <span itemprop="name"><?= $model->firstName . '<br/>' . $model->lastName ?></span>
                    </div>
                    <div itemscope itemtype="http://schema.org/Person">
                        <div class="right-part-post" itemprop="colleague">
                            <?= $model->contact ? $model->contact->post : null ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>