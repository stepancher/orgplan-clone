<?php
/**
 * @var ARController $this
 * @var string $header
 * @var string $description
 * @var string $class
 */
if (empty($header) && isset($model)) {
    $header = $model->theme;
}
?>
<h1 <?= isset($class) ? 'class="' . $class . '"' : ''; ?>>
    <?= $this->getId() == 'fair' || $this->getId() == 'exhibitionComplex' ? '<class="upper-case">' : '' ?>
    <?php

    if ($this->getId() == 'blog') {
        echo '<div itemscope itemtype="http://schema.org/Blog"><div itemprop="description"><span itemprop="name">' . $header . '</span></div></div>';
    } else {
        echo '<span itemscope itemtype="http://schema.org/Organization">
                <span itemprop="name">' . $header . '</span>
                <span itemprop="address">&nbsp;</span>
                <span itemprop="telephone">&nbsp;</span>
              </span>';
    }

    ?>
    <?php if (isset($description) && !empty($description)): ?>
        <span class="p-slash">/</span>
        <span class="f-header-desc"><?= $description; ?></span>
    <?php endif; ?>
</h1>