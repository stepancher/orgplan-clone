<?php
/**
 * @var Blog $model
 * @var CActiveDataProvider $dataProvider
 * @var Recall $recall
 * @var string $imagePath
 * @var string $requestUrl
 * @var CController $this
 */
?>
<div class="cols-row blog-page" style="max-width: 960px">
    <div class="col-md-8">
        <?php if (Yii::app()->user->getState('role') == User::ROLE_ADMIN && !isset($dataProvider)): ?>
            <a href="<?= Yii::app()->createUrl('admin/blog/save', array('id' => $model->id)) ?>"
               class="b-button b-button-icon f-text--tall d-text-light d-bg-warning button-edit"
               data-icon="&#xe0ea;"></a>
        <?php endif; ?>

        <?php

        if(!isset($header)){
            $header = Yii::t('BlogModule.blog', 'Blog');
        }

        if(!isset($description)){
            $description = '';
        }

        if (empty($description)) {

            $this->renderPartial('/_blocks/_b-header',
                ['header' => $header, 'description' => $description, 'model' => $model, 'class' => 'blog-view-h1']
            );
        } else {
            ?>
            <div class="f-h1 p-h1 h1-header">
                    <span><?= $header ?></span>
                <?php if (!empty($description)): ?>
                    <span class="p-slash">/</span>
                    <h1 class="f-header-desc"><?= $description; ?></h1>
                <?php endif; ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div class="cols-row blog-page" style="max-width: 960px">

    <?php if(!isset($dataProvider)) : ?>
    <div class="col-md-8">
        <?php
        $this->renderPartial('_blog/_view',
            [
                'model' => $model,
                'recall' => $recall,
                'imagePath' => $imagePath,
            ]
        );
        ?>
    </div>
    <div class="col-md-4">
        <?php $this->renderPartial('rightColumn',
            ['action' => 'view', 'model' => $model]) ?>
    </div>

    <?php else : ?>
    <div class="col-md-8">
        <?php
        $this->renderPartial('_blog/_index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'imagePath' => $imagePath,
            'requestUrl' => $requestUrl,
        ]);
        ?>
    </div>
    <div class="col-md-4">
        <?php $this->renderPartial('rightColumn', ['action' => 'view']) ?>
    </div>
    <?php endif; ?>
</div>