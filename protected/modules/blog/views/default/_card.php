<?php
/**
 * @var Blog $data
 * @var CWebApplication $app
 * @var CController $this
 */
$mainImage = $data->getMainImage();
?>
<div class="b-switch-list-view-blog b-switch-list-view__item">
    <div itemscope itemtype="http://schema.org/Blog">
        <div
            class="b-switch-list-view__cell b-switch-list-view__cell--name fixed-width-blog-cards p-bottom-2 b-switch-list-view__cell--name-blog" itemprop="description">
            <?= TbHtml::link($data->theme, $this->createUrl('view', ['url' => $data->shortUrl]), array('itemprop' => 'url')) ?>
        </div>
        <div
            class="b-switch-list-view__cell b-switch-list-view__cell--background b-switch-list-view__cell--background-blog">
            <?= TbHtml::link(
                TbHtml::image(
                    (
                    $mainImage
                        ? H::getImageUrl($mainImage, 'file', 'noImage', '300-168_')
                        : H::getImageUrl($data, 'image')
                    ),
                    $data->theme, ['title' => $data->theme, 'itemprop' => 'image']
                ) . '<div></div>',
                $this->createUrl('view', ['url' => $data->shortUrl]), array('itemprop' => 'url')
            ) ?>

        </div>
    <div
        class="b-switch-list-view__cell b-switch-list-view__cell--region fixed-width-blog-cards b-switch-list-view__cell--region-blog" itemprop="description">

            <div class="p-pull-right">
                <span itemprop="datePublished" content="<?= $data->created ?>">
                <?= $data->created
                    ? date('j', strtotime($data->created)) . ' ' . Fair::getMonthData(date('n', strtotime($data->created))) . ' ' . date('Y', strtotime($data->created))
                    : ''
                ?>
                </span>
            </div>
            <div class="p-pull-left" itemprop="author">
                <?= $data->authorId && $data->author ? $data->author->firstName . ' ' . $data->author->lastName : null ?>
            </div>
        </div>
    </div>
</div>
