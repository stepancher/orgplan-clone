<?php
/**
 * @var Blog $model
 * @var string $image
 * @var string $action
 * @var integer $pageSize
 * @var CController $this
 */
?>
<div class="header-right-column">
    <h2 title="<?= Yii::t('BlogModule.blog', 'Sign up and get fresh and relevant advice from the leading experts of exhibition activity') ?>"
        class="p-no-t-margin p-no-t-padding"><?= Yii::t('BlogModule.blog', 'subscribe') ?></h2>
</div>

<?php
$blogs = Blog::model()->findAll();
?>

<div class="cols-row popular-fresh">
    <div itemscope itemtype="http://schema.org/Blog">
        <div class="col-md-12">
            <div class="popular-block d-bg-bordered">

                <div class="popular-block-inner">
                    <h2 itemprop="name"><?= \Yii::t('BlogModule.blog', 'popular') ?></h2>
                    <?php
                    if (\Blog::getPopularBlog() !== null) {
                        foreach (\Blog::getPopularBlog() as $blog) {
                            ?>
                            <div class="b-items__item-offset b-items__item-offset--dot">
                                <?= TbHtml::link($blog->theme, $this->createUrl('view', ['url' => $blog->shortUrl]), array('itemprop' => 'url')) ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="fresh-block d-bg-bordered">

                <div class="fresh-block-inner">
                    <h2 itemprop="name"><?= \Yii::t('BlogModule.blog', 'fresh blog') ?></h2>
                    <?php
                    if (\Blog::getFreshBlog() !== null) {
                        foreach (\Blog::getFreshBlog() as $blog) {
                            ?>
                            <div class="b-items__item-offset b-items__item-offset--dot">
                                <?= TbHtml::link($blog->theme, $this->createUrl('view', ['url' => $blog->shortUrl]), array('itemprop' => 'url')) ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="tags">
                <h2 itemprop="description"><span itemprop="name"><?= \Yii::t('BlogModule.blog', 'tags') ?></span></h2>
                <?= Tag::getTags(true) ?>
            </div>
        </div>
    </div>
</div>