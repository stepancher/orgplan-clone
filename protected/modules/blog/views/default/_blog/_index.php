<?php
/**
 * @var CController $this
 * @var CWebApplication $app
 * @var string $imagePath
 * @var string $requestUrl
 * @var integer $imageWidth
 * @var CActiveDataProvider $dataProvider
 * @var Blog $model
 */

$mainImage = $model->getMainImage();

?>
    <div class="b-switch-list-view">
        <div class="b-switch-list-view--as-block b-switch-list-view--as-block--big">
            <div class="p-pull-clear p-inline b-switch-list-view__item b-switch-list-view__item">
                <div itemscope itemtype="http://schema.org/Blog">
                    <div class="b-switch-list-view__cell b-switch-list-view__cell--background">
                        <?php
                        $check = $mainImage
                            ? H::getImageUrl($mainImage, 'file', 'noImage', '620-348_')
                            : $imagePath;
                        echo TbHtml::link(
                            TbHtml::image($check, $model->theme,
                                array(
                                    'title' => $model->theme,
                                    'itemprop' => 'image',
                                )
                            ) . '<div></div>',
                            $this->createUrl('view', ['url' => $model->shortUrl]), array('itemprop' => 'url')
                        );
                        ?>
                    </div>
                    <div class="b-switch-list-view__cell b-switch-list-view__cell--name">
                        <span class="h1-page-blog" itemprop="description">
                            <?= TbHtml::link($model->theme, $this->createUrl('view', ['url' => $model->shortUrl]), array('itemprop' => 'url')) ?>
                        </span>
                    </div>
                    <div class="b-switch-list-view__cell b-switch-list-view__cell--region">
                        <div class="p-pull-right">
                            <span itemprop="datePublished" content="
                            <?= $model->created ?>">
                            <?= $model->created
                                ? date('j', strtotime($model->created)) . ' ' . Fair::getMonthData(date('n', strtotime($model->created))) . ' ' . date('Y', strtotime($model->created))
                                : ''
                            ?>
                            </span>
                        </div>
                        <div class="p-pull-left" itemprop="author">
                            <?= $model->authorId && $model->author ? $model->author->firstName . ' ' . $model->author->lastName : null; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$this->renderPartial('_list',
    [
        'dataProvider' => $dataProvider,
        'requestUrl' => $requestUrl,
        'itemView' => '_card',
        'notFoundVisible' => false
    ]
);
$this->renderPartial('/_blocks/_b-card/_expert',
    [
        'models' => User::model()->findAllByAttributes(['role' => User::ROLE_EXPERT]),
    ]
);


