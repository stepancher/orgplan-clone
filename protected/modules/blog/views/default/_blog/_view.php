<?php
/**
 * @var Recall $recall
 */

Yii::import('application.modules.blog.components.RecallForm');

/**
 * @var Blog $model
 * @var CController $this
 * @var CWebApplication $app
 * @var string $imagePath
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/raty/raty.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/user/developer-view-show-comment.js');

$mainImage = $model->getMainImage();
?>

<div class="f-pull-center">
    <div itemscope itemtype="http://schema.org/Blog">
        <div class="p-inline">
            <div class="blog-main-img" itemprop="description">
                <?php
                $check = $mainImage
                    ? H::getImageUrl($mainImage, 'file', 'noImage', '620-348_')
                    : $imagePath;
                echo TbHtml::image($check, $model->theme,
                        array(
                            'title' => $model->theme,
                            'itemprop' => 'image'
                        )
                    ) . '<div></div>' ?>
            </div>
        </div>
        <div class="offset-top-8 fixed-auto-width">
            <div class="p-pull-left no-letter-s">
                <span itemprop="author">
                    <?= $model->authorId && $model->author ? Yii::t('BlogModule.blog', 'Author') . " / " . $model->author->firstName . ' ' . $model->author->lastName : '' ?>
                </span>
            </div>
            <div class="p-pull-right" itemprop="description">
                <span itemprop="datePublished" content="<?= $model->created ?>">
                <?= $model->created
                    ? date('j', strtotime($model->created)) . ' ' . Fair::getMonthData(date('n', strtotime($model->created))) . ' ' . date('Y', strtotime($model->created))
                    : '' ?>
                    </span>
            </div>
        </div>
    </div>
</div>
<hr class="line-after-author"/>
<p><?= $model->text ?></p>
<div class="cols-row">
    <div class="col-md-12 page-blog__share-button">
        <script type="text/javascript">(function (w, doc) {
                if (!w.__utlWdgt) {
                    w.__utlWdgt = true;
                    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })(window, document);
        </script>
        <div data-background-alpha="0.0" data-buttons-color="#ffffff" data-counter-background-color="#ffffff"
             data-share-counter-size="12" data-top-button="false" data-share-counter-type="disable" data-share-style="1"
             data-mode="share" data-like-text-enable="false" data-mobile-view="false" data-icon-color="#ffffff"
             data-orientation="horizontal" data-text-color="#000000" data-share-shape="round-rectangle"
             data-sn-ids="fb.tw.gp.vk.ln." data-share-size="30" data-background-color="#ffffff"
             data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.gp." data-pid="1391266"
             data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true"
             data-selection-enable="false" class="uptolike-buttons"></div>
    </div>
</div>
<hr/>

<h2 class="comment-header"><?= Yii::t('BlogModule.blog', 'review and comment') ?></h2>
<div class="comment-block">

    <?php
    $ctrl = $this;
    $ctrl->widget('application.modules.blog.components.RecallGridView', array(
        'compare' => [
            ['t.objectId', $model->id],
            ['t.type', Recall::TYPE_BLOG],
            ['t.active', User::ACTIVE_ON]
        ],
        'model' => Recall::model(),
        'raty' => false,
        'template' => "{items}\n<div class=\"row-fluid\"><div class=\"span6\">{summary}</div></div>",
        'hideOnEmpty' => true,
        'onLink' => false,
        'summaryText' => '',
        'hideHeader' => true,
        'afterAjaxUpdate' => 'js:function() { ratyUpdate(); }',
        'htmlOptions' => array(
            'class' => 'grid-view--simple grid-view--comment'
        )
    ));
    ?>
    <?php
    $ctrl = $this;
    $count = count(Recall::model()->findAllByAttributes(array(
            'active' => User::ACTIVE_ON,
            'type' => Recall::TYPE_BLOG,
            'objectId' => $model->id)
    )); ?>

    <?php if (!$count): ?>
        <div class="indent-bottom-blog"><?= Yii::t('BlogModule.blog', 'No comments yet. Be the first!') ?></div>
    <?php endif; ?>

    <?php if (!Yii::app()->user->isGuest) { ?>
        <div class="cols-row offset-l-23">
            <?php
            $this->widget('application.modules.blog.components.FieldSetView', array(
                'items' => array(
                    array(
                        'application.modules.blog.components.FormView',
                        'form' => new RecallForm(
                            array(
                                'raty' => false,
                                'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
                                'hints' => [
                                    Yii::t('BlogModule.blog', 'rate this article: 5 stars - maximum rating'),
                                    Yii::t('BlogModule.blog', 'rate this article: 5 stars - maximum rating'),
                                    Yii::t('BlogModule.blog', 'rate this article: 5 stars - maximum rating'),
                                    Yii::t('BlogModule.blog', 'rate this article: 5 stars - maximum rating'),
                                    Yii::t('BlogModule.blog', 'rate this article: 5 stars - maximum rating'),
                                ]
                                //'elementGroupName' => '[]'
                            ),
                            (isset($recall) && $recall->hasErrors() ? $recall : new Recall()), $this
                        ),
                    ),
                )
            ));
            ?>
        </div>
        <?php
    } else {
        ?>
        <a data-toggle="modal" data-target="#modal-registration"
           href="javascript://"><u><?= Yii::t('BlogModule.blog', 'Register') ?></u></a> <?= Yii::t('BlogModule.blog', 'or') ?>
        <a data-toggle="modal" data-target="#modal-sign-up"
           href="javascript://"><u><?= Yii::t('BlogModule.blog', 'sign in') ?></u></a>, <?= Yii::t('BlogModule.blog', 'To leave a comment') ?>.
        <?php
    } ?>
</div>

<script type="text/javascript">

    function SubmitRecallButton(params) {
        return ({
            errorText: '',
            init: function (params) {
                var me = this;
                if('errorText' in params) {
                    me.errorText = params.errorText;
                }
            },
            run: function (params) {
                var me = this;
                me.init(params);
                $('.submit-recall-button').on('click', function (event) {
                    var $texarea = $('#Recall_comment');
                    var value = $texarea.val();
                    if (value.length > 0) {
                        return true;
                    } else {
                        event.preventDefault();
                        if ($('.widget-RecallForm p.help-block').length == 0) {
                            $texarea.after('<p class="help-block">' + me.errorText + '</p>');
                        }
                    }
                });
            }
        }).run(params);
    };

    new SubmitRecallButton(
        {
            errorText: '<?=Yii::t('BlogModule.blog', 'You must fill in the "Leave a comment".')?>'
        }
    );
</script>