<?php
/**
 * @var BlogController $this
 * @var Blog $model
 * @var CWebApplication $app
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/blog/confirm-delete-article.js', CClientScript::POS_BEGIN);
?>


<div class="col-md-11">
    <?php
    $formClass = get_class($model) . 'Form';
    Yii::import('application.modules.blog.widgets.' . $formClass);
    $ctrl = $this;
    $ctrl->widget('application.modules.blog.components.FieldSetView', array(
        'header' => Yii::t('BlogModule.blog', $model->isNewRecord ? 'Form header create123' : 'Form header update'),
        'items' => array(
            array(
                'application.modules.blog.components.FormView',
                'form' => new $formClass(
                    array(
                        'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
                        'enctype' => 'multipart/form-data',
                        //'elementGroupName' => '[]'
                    ),
                    $model, $ctrl
                ),
                'items' => array(
                    array(
                        'application.modules.blog.components.HtmlView',
                        'content' => function ($owner) use ($model) {
                            if ($model->image && $model->image->getFileUrl()) {
                                echo TbHtml::thumbnails(
                                    H::getThumbnailsArray(
                                        array($model), $model->id, 'image', null,
                                        TbHtml::linkButton(
                                            Yii::t('BlogModule.blog', 'delete'),
                                            array(
                                                'color' => TbHtml::BUTTON_COLOR_DANGER,
                                                'size' => TbHtml::BUTTON_SIZE_MINI,
                                                'url' => $owner->createUrl('deleteFile', array('id' => $model->id, 'fileId' => '{fileId}'))
                                            )
                                        )
                                    )
                                );
                            } else {
                                $owner->widget('application.modules.blog.components.FieldSetView', array(
                                        'header' => Yii::t('BlogModule.blog', 'download Image (title picture)'),
                                        'items' => array(
                                            array(
                                                'CMultiFileUpload',
                                                'model' => $model,
                                                'htmlOptions' => array(
                                                    'style' => 'margin-left:80px'
                                                ),
                                                'max' => 1,
                                                'attribute' => 'image',
                                                'duplicate' => Yii::t('BlogModule.blog', 'this file already exists!'),
                                                'accept' => 'gif|jpg|jpeg|png',
                                                'denied' => Yii::t('BlogModule.blog', 'Invalid file type'),
                                            )
                                        )
                                    )
                                );
                            }
                        },
                    ),
                    array(
                        'application.modules.blog.components.HtmlView',
                        'content' => function ($owner) use ($model, $ctrl) {
                            $owner->widget('application.modules.blog.components.FieldSetView', array(
                                    'header' => Yii::t('BlogModule.blog', 'download Image'),
                                    'items' => array(
                                        array(
                                            'CMultiFileUpload',
                                            'model' => Blog::model(),
                                            'htmlOptions' => array(
                                                'style' => 'margin-left:80px'
                                            ),
                                            'attribute' => 'images',
                                            'duplicate' => Yii::t('BlogModule.blog', 'this file already exists!'),
                                            'accept' => 'gif|jpg|jpeg|png',
                                            'denied' => Yii::t('BlogModule.blog', 'Invalid file type'),
                                        )
                                    )
                                )
                            );
                            echo TbHtml::thumbnails(
                                H::getThumbnailsArray(
                                    $model->blogHasFiles, $model->id, 'file', ObjectFile::TYPE_BLOG_IMAGES,
                                    TbHtml::linkButton(
                                        Yii::t('BlogModule.blog', 'delete'),
                                        array(
                                            'color' => TbHtml::BUTTON_COLOR_DANGER,
                                            'size' => TbHtml::BUTTON_SIZE_MINI,
                                            'url' => $owner->createUrl('deleteFile', array('id' => $model->id, 'fileId' => '{fileId}'))
                                        )
                                    )
                                )
                            );
                            $owner->widget('application.modules.blog.components.FieldSetView', array(
                                    'header' => Yii::t('BlogModule.blog', 'form to attach files'),
                                    'items' => array(
                                        array(
                                            'CMultiFileUpload',
                                            'model' => Blog::model(),
                                            'htmlOptions' => array(
                                                'style' => 'margin-left:80px',
                                            ),
                                            'attribute' => 'files',
                                            'duplicate' => Yii::t('BlogModule.blog', 'this file already exists!'),
                                            'accept' => 'gif|jpg|jpeg|png',
                                            'denied' => Yii::t('BlogModule.blog', 'Invalid file type'),
                                        ),
                                    )
                                )
                            );
                            $compare = array(0);
                            if (!empty($model->blogHasFiles)) {
                                $compare = CHtml::listData($model->blogHasFiles, 'id', 'fileId');
                            }

                            if (!$model->isNewRecord && !empty($model->blogHasFiles)) {
                                $owner->widget('application.modules.blog.components.ObjectFileGridView', array(
                                    'model' => ObjectFile::model(),
                                    'compare' => array(
                                        array('t.id', $compare),
                                        array('t.type', ObjectFile::TYPE_BLOG_FILES)
                                    ),
                                    'columnsAppend' => array(
                                        'buttons' => array(
                                            'class' => 'bootstrap.widgets.TbButtonColumn',
                                            'template' => '{delete}',
                                            'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
                                            'buttons' => array(
                                                'delete' => array(
                                                    'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
                                                    'url' => function ($data) use ($ctrl, $model) {
                                                        return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
                                                    },
                                                    'options' => array('title' => Yii::t('BlogModule.blog', 'information'), 'class' => 'btn btn-small btn-danger'),
                                                ),
                                            )
                                        )
                                    )
                                ));
                            }

                        },
                    ),
                    array(
                        'application.modules.blog.components.ActionsView',
                        'items' => array(
                            'submit' => array(
                                'type' => TbHtml::BUTTON_TYPE_SUBMIT,
                                'label' => Yii::t('BlogModule.blog', 'Form action save'),
                                'attributes' => array(
                                    'color' => TbHtml::BUTTON_COLOR_SUCCESS
                                )
                            ),
                            'cancel' => array(
                                'type' => TbHtml::BUTTON_TYPE_LINK,
                                'label' => Yii::t('BlogModule.blog', 'Form action cancel'),
                                'attributes' => array(
                                    'url' => $this->createUrl('index'),
                                    'color' => TbHtml::BUTTON_COLOR_DANGER
                                )
                            ),
                            'delete' => array(
                                'type' => TbHtml::BUTTON_TYPE_LINK,
                                'label' => Yii::t('BlogModule.blog', 'delete article'),
                                //'visible' => !$model->isNewRecord,
                                'attributes' => array(
                                    'url' => $this->createUrl('DeleteArticle', ['id' => $model->id]),
                                    'color' => TbHtml::BUTTON_COLOR_WARNING,
                                    'class' => 'delete-article',
                                )
                            )
                        )
                    ),
                ),
            ),
        )
    ));
    ?>
</div>
<script type="text/javascript">
    new ConfirmDelete({
        confirmText: '<?=Yii::t('BlogModule.blog', 'Are you sure you want to delete an article or material?')?>'
    });
</script>
