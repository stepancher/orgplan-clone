<?php
/**
 * @var CController $this
 * @var Blog $model
 * @var Recall $recall
 * @var CActiveDataProvider $dataProvider
 * @var string $requestUrl
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/social-icon.css');
$cs->registerCssFile($publishUrl . '/css/main.css');

if ($model) {
    $linkAssets = H::getImageUrl($model, 'image');
    $assets = strstr($linkAssets, 'assets', true);
    $newLinkAssets = dirname($linkAssets) . DIRECTORY_SEPARATOR . 'blog-resized-' . basename($linkAssets);
    $newAbsoluteImagePath = ($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $newLinkAssets;
    if (!file_exists($newAbsoluteImagePath)) {
        $image = new Image(($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $linkAssets);
        $image->resize(null, 348);
        $image->save($newAbsoluteImagePath);
    }
    $this->renderPartial('_template', [
            'model' => $model,
            'recall' => isset($recall) && $recall->hasErrors() ? $recall : new Recall(),
            'imagePath' => $newLinkAssets,
            'header' => isset($header) ? $header : NULL,
            'description' => isset($description) ? $description : NULL,
        ] + (isset($dataProvider) ? ['dataProvider' => $dataProvider] : [])
        + (isset($dataProvider) ? ['requestUrl' => $requestUrl] : [])
    );
}