<?php
/**
 * Created by PhpStorm.
 * User: Rendol
 * Date: 22.04.2015
 * Time: 9:21
 */

/**
 * @var CController $this
 * @var string $requestUrl
 * @var string $itemView
 * @var string $switcherType
 * @var bool $enableNextPageButton
 * @var CWebApplication $app
 * @var bool $notFoundVisible
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.blog.assets'), false, -1, YII_DEBUG);

$cs->registerScriptFile($publishUrl . '/js/_b-switch-list-view.js', CClientScript::POS_END);

if (!isset($notFoundVisible)) {
    $notFoundVisible = true;
}
if (empty($queryParams)) {
    $queryParams = [];
}

$curPage = $dataProvider->getPagination()->getCurrentPage(false);
$nextPage = $curPage + 1;
$nextPageUrl = '';
if (empty($nextPageExist)) {
    $nextPageExist = $nextPage * $dataProvider->getPagination()->getPageSize() < $dataProvider->totalItemCount;
}

if (!isset($enableNextPageButton)) {
    $enableNextPageButton = true;
}
$url = parse_url($requestUrl);
parse_str(isset($url['query']) ? $url['query'] : '', $params);
$nextPageUrl = $url['path'] . '?' . http_build_query(['page' => $nextPage] + $params);
?>

<div class="b-switch-list-view" data-request='<?= json_encode(['page' => $curPage] + $queryParams) ?>'
     data-ctrl='<?= Yii::app()->controller->id ?>'>
    <div class="b-search-loader-block">
        <?= TbHtml::image($publishUrl . '/img/loader/ajax-loader.gif', 'Загрузка', ['class' => 'js-page-search__loader', 'title' => 'Загрузка']) ?>
    </div>
    <?php if ($dataProvider->getTotalItemCount() != 0) : ?>

        <?php
        $this->widget('zii.widgets.CListView', [
            'dataProvider' => $dataProvider,
            'htmlOptions' => [
                'class' => 'b-switch-list-view__items js-updated-block b-switch-list-view js-switch-list-view-default b-switch-list-view--as-block',
            ],
            'template' => "{items}",
            'itemView' => $itemView,
        ]);
        ?>

        <?php if ($enableNextPageButton) : ?>
            <div class="b-switch-list-view__add-items-wrap">
                <?php if ($nextPageExist) : ?>
                    <?= CHtml::button(
                        Yii::t('BlogModule.blog', 'Search result more'),
                        [
                            'data-url' => $nextPageUrl,
                            'class' => 'b-switch-list-view__add-items b-button d-bg-base d-bg-bordered d-bg-input--hover d-text-dark btn'
                        ]
                    ) ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php elseif ($notFoundVisible == true) : ?>
        <div class="not-found">
                <h3 class="text-center"><?= Yii::t('BlogModule.blog', 'Unfortunately, upon request') ?>
                    <?= isset($_GET['query']) ? '«' . $_GET['query'] . '»' : null ?> <?= Yii::t('BlogModule.blog', 'There is nothing') ?>
                    .<br/>
                    <?= Yii::t('BlogModule.blog', 'Enter into the search box or try a new query filters') ?>.
                </h3>
        </div>
    <?php endif; ?>
</div>