<?php

class JsonController extends Controller
{

    protected $dump = FALSE;

    const GVALUE_ROUND_PRECISION = 2;

    public function actionDump($langId, $year = NULL, $industryId = NULL, $countryId = NULL){
        $this->dump = TRUE;
        $this->actionGetData($langId, $year, $industryId, $countryId);
    }

    public function actionGetData($langId, $year = NULL, $industryId = NULL, $countryId = NULL, $regionId = NULL, $searchQuery = NULL){

        if(empty($year)){
            $year = date('Y');
        }

        $country  = null;
        $region   = null;
        $industry = null;
        $link     = '';

        if (is_numeric($countryId)) {
            $countryId = $countryId * 1;
            $country = Country::model()->findByPk($countryId);
        }

        if (is_numeric($regionId)) {
            $regionId = $regionId * 1;
            $region = Region::model()->findByPk($regionId);
        }

        if (is_numeric($industryId)) {
            $industryId = $industryId * 1;
            $industry = Industry::model()->findByPk($industryId);
        }

        if ($country != null) {
            $link = $link . 'country/' . $country->shortUrl . '/';
        }

        if ($country == null && $countryId != null && $countryId != 0) {
            $link = $link . 'country/' . $countryId . '/';
        }

        if (is_string($countryId)) {
            $link = $link . 'country/' . $countryId . '/';
        }

        if ($region != null) {
            $link = $link . 'region/' . $region->shortUrl . '/';
        }

        if ($region == null && $regionId != null && $countryId != 0) {
            $link = $link . 'region/' . $regionId . '/';
        }

        if ($industry != null) {
            $link = $link . 'industry/' . Industry::getShortUrl($industry->id) . '/';
        }

        if ($industry == null && $industryId != null) {
            $link = $link . 'industry/' . $industryId . '/';
        }

        if ($year != null) {
            $link = $link . 'year/' . $year . '/';
        }

        if ($link != '' && (is_numeric($countryId) || is_numeric($regionId) || is_numeric($industryId))) {
            $json = [
                'status' => 'redirect',
                'code' => 301,
                'link' => $link
            ];

            echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
            return;
        }

        if ($countryId != null) {
            $country = Country::model()->findByAttributes(['shortUrl' => $countryId]);
        }

        if ($country != null) {
            $countryId = $country->id;
        }

        if ($regionId != null) {
            $region = TrRegion::model()->findByAttributes(['shortUrl' => $regionId]);

            if (isset($region->trParentId)) {
                $region = Region::model()->findByPk($region->trParentId);
            } else {
                $region = null;
            }
        }

        if ($region != null) {
            $regionId = $region->id;
        }

        $trIndustry = null;

        if ($industryId != null) {
            $trIndustry = TrIndustry::model()->findByAttributes(['shortNameUrl' => $industryId]);
        }

        if ($trIndustry != null) {
            $industry = Industry::model()->findByPk($trIndustry->trParentId);
        }

        if ($industry != null) {
            $industryId = $industry->id;
        }

        $industryHeader = $this->getIndustryHeader($industryId);

        $headerPart = '';

        if(!empty($industryHeader)){
            $headerPart = ' '.Yii::t('CatalogModule.catalog', 'on industry').' '.$industryHeader;
        }

        if(!empty($countryName = $this->getCountryName($countryId)))
            $mapHeader = Yii::t('CatalogModule.catalog', 'Fairs')." ".$industryHeader." ".Yii::t('CatalogModule.catalog', 'in')." ".$countryName;
        else
            $mapHeader = Yii::t('CatalogModule.catalog','Exhibitions of the world');

        $regionName = isset($region->name) ? $region->name : '';
        $year = $year != null ? $year : '';

        $json = array(
            "user" => array(
                "auth" => !Yii::app()->user->isGuest,
            ),
            "header"=> $this->getHeader($industryHeader, $countryName, $regionName, $year),
            "map" => array(
                "yearLabel" => Yii::t('CatalogModule.catalog', 'Year'),
                "header" => $mapHeader,
                "searchLink" => "/ru/search/etc",
                "industryId" => isset($trIndustry->shortNameUrl) ? $trIndustry->shortNameUrl : null,
                "industriesLabel" => Yii::t('CatalogModule.catalog', 'Industry'),
                "industriesAllLabel" => Yii::t('CatalogModule.catalog', 'All industries'),
                "industries"=> $this->getAllIndustries(),
                "countryId"=> isset($country->shortUrl) ? $country->shortUrl : null,
                "countriesLabel"=> Yii::t('CatalogModule.catalog', 'Country'),
                "countries"=> $this->getCountries(),
                "regionId" => isset($region->shortUrl) ? $region->shortUrl : null,
                "regionsLabel" => Yii::t('CatalogModule.catalog', 'Region'),
                "data" => $this->getMapData($industryId, $countryId, $year),
                "years"=> Fair::getFairYears(),
                "regions"=> $this->linksListRegionWithId($countryId, 100),
                "placeholder"=> Yii::t('CatalogModule.catalog', 'placeholder'),
                "template" => Yii::t('CatalogModule.catalog', 'template'),
                "isDisabled" => $countryId == NULL ? true : false,
            ),
            "fairsList" => array(
                "header"=> Yii::t('CatalogModule.catalog', 'TOP fairs'),
                "compareLink"=> array(
                    "text"=> Yii::t('CatalogModule.catalog', 'Compare fairs'),
                    "link"=> "/fairs/matchView/"
                ),
                "compareBtn"=> array(
                    "text"=> Yii::t('CatalogModule.catalog', 'Add to compare'),
                ),
                "matchFairsCount" => $this->getMatchFairsCount(),
                "compareDeleteBtn"=> array(
                    "text"=> Yii::t('CatalogModule.catalog', 'Remove from compare'),
                ),
                "fairs"=> $this->getFairsList($year, $industryId, $countryId, $regionId, $searchQuery),
                "compareLabel" => Yii::t('CatalogModule.catalog', 'compare'),
            ),
            "analytics"=> array(),//$this->getAnalyticsData($industryId, $countryId),
            "ratings" => $this->getRatings(),
            "ads"=> $this->advertisingText(),
            "title" => $this->getHeader($industryHeader, $countryName, $regionName, $year),
            "description" => $this->getDescription($industryHeader, $countryName, $regionName, $year),
            "queryDesc" => $this->getQueryDesc($industryId, $countryId, $regionId, $year),
        );

        $topBlocksStatArray = $this->getTopBlocksStat($year, $industryId, $countryId);

        $json = array_merge($json, $topBlocksStatArray);

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    public function advertisingText(){
        return array(
            "header"=> Yii::t('CatalogModule.catalog', 'Advertising'),
            "text1" => Yii::t('CatalogModule.catalog', 'PROTOPLAN ADVERTISEMENT'),
            "text2" => Yii::t('CatalogModule.catalog', 'EXPOPLANNER'),
            "text3" => Yii::t('CatalogModule.catalog', 'WILL HELP TO DO ALL-TIME'),
            "text4" => Yii::t('CatalogModule.catalog', 'Exhibitors waste more than'),
            "text5" => Yii::t('CatalogModule.catalog', 'TRY FOR FREE'),
            "text6" => Yii::t('CatalogModule.catalog', 'ACTION PLAN'),
            "text7" => Yii::t('CatalogModule.catalog', 'Create your own plan'),
            "text8" => Yii::t('CatalogModule.catalog', 'MANAGEMENT'),
            "text9" => Yii::t('CatalogModule.catalog', 'Save the exhibitions in your dashboard'),
            "text10" => Yii::t('CatalogModule.catalog', 'EXHIBITION CALENDAR'),
            "text11" => Yii::t('CatalogModule.catalog', 'Create your exhibition calendar, print it out or mail it'),
            "text12" => Yii::t('CatalogModule.catalog', 'CLICK TO LEARN MORE'),
            "text12domain" => Yii::app()->params['expoplannerUrl'],
            "lang" => Yii::app()->getLanguage(),
        );
    }

    public function getTopBlocksStat($year, $industryId = NULL, $countryId = NULL){

        $result = array();
        if(
            empty($industryId) &&
            empty($countryId)
        ){
            $result = array(
                "industryFairs" => $this->getAllWorldIndustries(),
                "countryFairs" => $this->getAllWorldFairs($year),
                "industryCounters" => $this->getCountryFairs($countryId, $year),
                "regionFairs" => $this->getAllWorldVenues(),
            );
        }

        if(
            empty($industryId) &&
            !empty($countryId)
        ){
            $result = array(
                "industryFairs" => $this->getAllWorldFairs($year),
                "countryFairs" => $this->getCountAllIndustriesByCountry($countryId),
                "industryCounters" => $this->getCountryFairs($countryId, $year),
                "regionFairs" => $this->getVenuesByCountry($countryId),
            );
        }

        if(
            !empty($industryId) &&
            empty($countryId)
        ){
            $result = array(
                "industryFairs" => $this->getAllWorldFairsByIndustry($industryId, $year),
                "countryFairs" => $this->getAllWorldIndustries(),
                "industryCounters" => $this->getFairsByIndustryAndCountry($industryId, $countryId, $year),
                "regionFairs" => $this->getAllWorldVenues(),
            );
        }

        if(
            !empty($industryId) &&
            !empty($countryId)
        ){
            $result = array(
                "industryFairs" => $this->getAllWorldFairsByIndustry($industryId, $year),
                "countryFairs" => $this->getCountAllIndustriesByCountry($countryId),
                "industryCounters" => $this->getCountryFairs($countryId, $year),
                "regionFairs" => $this->getVenuesByCountry($countryId),
            );
        }

        return $result;
    }

    /**** TOP BLOCK STATS FUNCTIONS ****/

    public function getAllWorldFairsByIndustry($industryId, $year){

        $sql = "
            SELECT 
                COUNT(f.id) cnt,
                tri.name industryName
            
            FROM tbl_fairhasindustry fhi
            
            LEFT JOIN tbl_fair f ON (f.id = fhi.fairId)
            LEFT JOIN tbl_trindustry tri ON (fhi.industryId = tri.trParentId AND tri.langId = :langId)
                              
            WHERE 
                fhi.industryId = :industryId AND 
                f.active = 1 AND 
                YEAR(f.beginDate) = :year;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':industryId' => $industryId,
            ':year' => $year,
            ':langId' => Yii::app()->language
        ]);

        $data = [];
        foreach($res as $row){
            $data = array(
                'name' => Yii::t('CatalogModule.catalog', 'Fairs')." ".$row['industryName'].' / '.$year,
                'value' => $row['cnt'],
            );
        }

        return $data;
    }

    public function getAllWorldFairs($year){

        $sql = "
            SELECT 
                COUNT(0) cnt
                
            FROM tbl_fair f 
            
            WHERE f.active = 1 AND YEAR(f.beginDate) = :year
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':year' => $year,
        ]);

        $res = $dataReader->read();

        $fairsCountry = array(
            "name" => Yii::t('CatalogModule.catalog', 'Fairs of world')." / ".$year,
            "value" => $res['cnt'],
        );

        return $fairsCountry;
    }

    public function getAllWorldIndustries(){

        $sql = '
            SELECT 
                COUNT(i.id) cnt
                
            FROM tbl_industry i
        ';

        $dataReader = Yii::app()->db->createCommand($sql)->query();

        $industries = $dataReader->read();

        $data = array(
            'name' => Yii::t('CatalogModule.catalog', 'Industries count'),
            'value' => $industries['cnt'],
        );

        return $data;
    }

    public function getAllWorldVenues(){

        $sql = '
            SELECT 
                COUNT(ec.id) cnt
                
            FROM tbl_exhibitioncomplex ec
            WHERE ec.active = 1;
        ';

        $dataReader = Yii::app()->db->createCommand($sql)->query();

        $industries = $dataReader->read();

        $data = array(
            'name' => Yii::t('CatalogModule.catalog', 'Exhibition complex world count'),
            'value' => $industries['cnt'],
        );

        return $data;
    }

    public function getCountryFairs($countryId, $year){

        if(empty($countryId)){
            $countryId = 1;
        }

        $sql = "
            SELECT 
                COUNT(0) cnt,
                trct.name countryName
                
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c ON ec.cityId = c.id
            LEFT JOIN tbl_region r ON c.regionId = r.id
            LEFT JOIN tbl_district d ON r.districtId = d.id
            LEFT JOIN tbl_country ct ON ct.id = d.countryId
            LEFT JOIN tbl_trcountry trct ON ct.id = trct.trParentId AND trct.langId = :langId
            
            WHERE f.active = 1 AND ct.id = :countryId AND YEAR(f.beginDate) = :year
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':countryId' => $countryId,
            ':year' => $year,
            ':langId' => Yii::app()->language
        ]);

        $res = $dataReader->read();

        $fairsCountry = array(
            "name" => Yii::t('CatalogModule.catalog', 'Fairs')." ".ucfirst($res['countryName'])." /  ".$year,
            "value" => $res['cnt'],
        );

        return $fairsCountry;
    }

    public function getCountAllIndustriesByCountry($countryId){

        $sql = '
            SELECT 
                COUNT(DISTINCT i.id) cnt,
                trcntry.name countryName
                
            FROM tbl_industry i
            
            LEFT JOIN tbl_fairhasindustry fhi       ON (fhi.industryId = i.id)
            LEFT JOIN tbl_fair f                    ON (f.id = fhi.fairId)
            LEFT JOIN tbl_exhibitioncomplex ec      ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c                    ON c.id = ec.cityId
            LEFT JOIN tbl_region r                  ON r.id = c.regionId
            LEFT JOIN tbl_district d                ON d.id = r.districtId
            LEFT JOIN tbl_country cntry             ON cntry.id = d.countryId
            LEFT JOIN tbl_trcountry trcntry         ON (trcntry.trParentId = cntry.id AND trcntry.langId = :langId)
            
            WHERE cntry.id = :countryId
        ';

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':countryId' => $countryId,
            ':langId' => Yii::app()->language,
        ]);

        $industries = $dataReader->read();

        $data = array(
            'name' => Yii::t('CatalogModule.catalog', 'Industries count').' / '.$industries['countryName'],
            'value' => $industries['cnt'],
        );

        return $data;
    }

    public function getFairsByIndustryAndCountry($industryId, $countryId, $year){

        if(empty($countryId)){
            $countryId = 1;
        }

        $sql = "
            SELECT 
                COUNT(fhi.fairId) cnt,
                tri.name industryName,
                trcntry.name countryName
                
            FROM tbl_fairhasindustry fhi
            
            LEFT JOIN tbl_trindustry tri ON tri.trParentId =fhi.industryId AND tri.langId = :langId
            LEFT JOIN tbl_fair f ON fhi.fairId = f.id
            LEFT JOIN tbl_exhibitioncomplex ec     ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c                   ON c.id = ec.cityId
            LEFT JOIN tbl_region r                 ON r.id = c.regionId
            LEFT JOIN tbl_district d               ON d.id = r.districtId
            LEFT JOIN tbl_country cntry            ON cntry.id = d.countryId
            LEFT JOIN tbl_trcountry trcntry        ON trcntry.trParentId = cntry.id AND trcntry.langId = :langId
            
            WHERE fhi.industryId = :industryId AND 
                  cntry.id = :countryId AND
                  f.active = 1 AND 
                  YEAR(f.beginDate) = :year 
                  
            GROUP BY fhi.industryId;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':industryId' => $industryId,
            ':countryId' => $countryId,
            ':year' => $year,
            ':langId' => Yii::app()->language,
        ]);

        $data = [];
        foreach($res as $row){
            $data = array(
                'name' => Yii::t('CatalogModule.catalog', 'Fairs')." ".$row['industryName'].' '.$row['countryName'].' / '.$year,
                'value' => $row['cnt'],
            );
        }

        return $data;
    }

    public function getVenuesByCountry($countryId){

        $sql = "
            SELECT 
                COUNT(ec.id) cnt,
                trct.name countryName
                
            FROM tbl_exhibitioncomplex ec 
            LEFT JOIN tbl_city c ON ec.cityId = c.id
            LEFT JOIN tbl_region r ON c.regionId = r.id
            LEFT JOIN tbl_district d ON r.districtId = d.id
            LEFT JOIN tbl_country ct ON ct.id = d.countryId
            LEFT JOIN tbl_trcountry trct ON ct.id = trct.trParentId AND trct.langId = :langId
            
            WHERE ct.id = :countryId AND
                  ec.active = 1;
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':countryId' => $countryId,
            ':langId' => Yii::app()->language,
        ]);

        $res = $dataReader->read();

        $fairsCountry = array(
            "name" => Yii::t('CatalogModule.catalog', 'Exhibition complex')." /  ".ucfirst($res['countryName']),
            "value" => $res['cnt'],
        );

        return $fairsCountry;
    }

    /**** END TOP BLOCK STATS FUNCTIONS ****/


    public function getRatings(){

        $ratings = array(
            array(
                "key" => 1,
                "value" => Yii::t('CatalogModule.catalog', 'Statistics confirmed exhibition rating.'),
            ),
            array(
                "key" => 2,
                "value" => Yii::t('CatalogModule.catalog', 'Statistics confirmed exhibition audit.'),
            ),
            array(
                "key" => 3,
                "value" => Yii::t('CatalogModule.catalog', 'Statistics confirmed only organizer of the exhibition.'),
            ),
        );

        return $ratings;
    }

    public function getMapData($industryId, $countryId, $year){

        $params = array();

        $yearCondition = "YEAR(f.beginDate) = :year AND";
        if($year == NULL){
            $year = date('Y');
        }

        $industryCondition = '';
        if($industryId != NULL){
            $industryCondition = 'fhi.industryId = :industryId AND';
            $params[':industryId']= $industryId;
        }

        $countryCondition = '';
        if($countryId != NULL){
            $countryCondition = 'd.countryId = :countryId AND';
            $params[':countryId'] = $countryId;
        }

        $params[':year'] = $year;

        $sql = '
            SELECT 
                r.code code,
                COUNT(f.id) cnt ,
                trr.name regionName
            
            FROM tbl_region r
            LEFT JOIN tbl_trregion trr ON trr.trParentId = r.id AND trr.langId = :langId
            LEFT JOIN tbl_city c ON c.regionId = r.id
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
            LEFT JOIN tbl_fair f ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_district d ON d.id = r.districtId
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.fairId = f.id
            
            WHERE                
                '.$yearCondition.'
                '.$industryCondition.'
                '.$countryCondition.'
                f.active = 1 
                          
            GROUP BY r.id
            
        ';

        $params[':langId'] = Yii::app()->language;
        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, $params);

        $data = array();
        foreach($res as $region){
            $data[] = array(
                'key' => $region['code'],
                'value' => $region['cnt'],
                'fullName' => $region['regionName'],
            );
        }

        return $data;
    }

    public function getWorldMapData($industryId = 0, $year = 0)
    {
        if ($year == 0) {
            $year = date('YYYY');
        }

        $yearCondition = 'and f.beginDate > "' . $year . '-00-00"';
        $industryCondition = '';
        $industryGroup = '';

        $industryId = 3;

        if ($industryId != 0) {
            $industryCondition = 'and i.id = ' . $industryId;
            $industryGroup = ', tri.name';
        }

        $sql = '
            SELECT trcn.name AS countryName, tri.id as industryId, COUNT(DISTINCT f.id) AS fairsCount FROM orgplan.tbl_fair f
            left join tbl_fairhasindustry fhi on fhi.fairId = f.id
            left join tbl_industry i on fhi.industryId = i.id
            left join tbl_trindustry tri on tri.trParentId = i.id AND tri.langId = \'' . Yii::app()->language . '\'
            left join tbl_exhibitioncomplex ec on f.exhibitionComplexId = ec.id
            left join tbl_city c on ec.cityId = c.id
            left join tbl_region r on c.regionId = r.id
            left join tbl_district d on r.districtId = d.id
            left join tbl_country cn on d.countryId = cn.id
            left join tbl_trcountry trcn on trcn.trParentId = cn.id AND trcn.langId = \'' . Yii::app()->language . '\'
            WHERE f.active = 1
            ' . $industryCondition . ' ' . $yearCondition . '
            GROUP BY trcn.name ' . $industryGroup . '
        ';

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE);
        CVarDumper::dump($res, 10, 1); exit;
    }

    public function getMatchFairsCount(){

        $sql = "
            SELECT 
                COUNT(fm.id) cnt
                
            FROM tbl_fairmatches fm
            
            WHERE fm.userId = :userId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':userId' => Yii::app()->user->id,
        ]);

        $res = $dataReader->read();

        return $res['cnt'];
    }

    /**
     * @deprecated
     * @return array
     */
    public function linksListIndustry(){

        $sql = "
            SELECT 
                DISTINCT ct.industryId cnt
                ,tri.name industryName
                , i.id id
                
            FROM tbl_chartcolumn cc
            LEFT JOIN tbl_charttable ct ON ct.id = cc.tableId
            LEFT JOIN tbl_industry i ON i.id = ct.industryId
            LEFT JOIN tbl_trindustry tri ON tri.trParentId = i.id AND tri.langId = :langId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':langId' => Yii::app()->language]);

        $res = $dataReader->readAll();

        $data = array();
        foreach($res as $ind){
            $data[] = array(
                'name' => $ind['industryName'],
                'value' => Yii::app()->createAbsoluteUrl('industry/default/view', array('id' => $ind['id']))
            );
        }

        return $data;
    }

    /**
     * @deprecated
     * @param $countryId
     * @param int $limit
     * @return array
     */
    public function linksListRegion($countryId, $limit = 10){

        $sql = "
            SELECT 
                r.id id,
                trr.name regionName,
                ri.id regionInformationId
               
            FROM tbl_region r 
            LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId AND trr.langId = :langId
            LEFT JOIN tbl_district d ON r.districtId = d.id
            LEFT JOIN tbl_regioninformation ri ON r.id = ri.regionId
            
            WHERE d.countryId = :countryId AND
                  ri.rating != 0
            
            ORDER BY ri.rating ASC
            
            LIMIT 10
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':langId' => Yii::app()->language,
            ':countryId' => $countryId,
        ]);

        $res = $dataReader->readAll();

        $data = array();
        foreach($res as $reg){
            $data[] = array(
                'name' => $reg['regionName'],
                'value' => Yii::app()->createAbsoluteUrl('regionInformation/default/view', array('id' => $reg['regionInformationId'])).'#analytics'
            );
        }

        return $data;
    }

    public function linksListRegionWithId($countryId, $limit = 10){

        $sql = "
            SELECT DISTINCT
                trr.shortUrl id,
                trr.name regionName,
                ri.id regionInformationId,
                r.code code
               
            FROM tbl_region r 
                LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId AND trr.langId = :langId
                LEFT JOIN tbl_district d ON r.districtId = d.id
                LEFT JOIN tbl_regioninformation ri ON r.id = ri.regionId
                LEFT JOIN tbl_city c ON c.regionId = r.id
                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                LEFT JOIN tbl_fair f ON f.exhibitionComplexId = ec.id
            WHERE d.countryId = :countryId AND f.active = :active
            ORDER BY trr.name ASC
            LIMIT {$limit}
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':langId' => Yii::app()->language,
            ':countryId' => $countryId,
            ':active' => Fair::ACTIVE_ON,
        ]);

        $res = $dataReader->readAll();

        $data = array();
        foreach($res as $reg){
            $data[] = array(
                'name' => $reg['regionName'],
                'code' => $reg['code'],
                'id' => $reg['id'],
                'value' => Yii::app()->createAbsoluteUrl('regionInformation/default/view', array('id' => $reg['regionInformationId'])).'#analytics'
            );
        }

        return array_merge([['id' => 0, 'name' => Yii::t('CatalogModule.catalog','All'), 'code' => 'all']] , $data);
    }

    public function getCountryName($countryId){

        $sql = "
            SELECT 
                trcntry.name name
                
            FROM tbl_trcountry trcntry 
                     
            WHERE trcntry.trParentId = :countryId AND langId = :langId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':countryId' => $countryId,
            ':langId' => Yii::app()->language,
        ]);
        $country = $dataReader->read();

        return $country['name'];
    }

    public function getIndustryHeader($industryId){

        $sql = "
            SELECT 
                tri.name name
            FROM tbl_trindustry tri
            WHERE tri.trParentId = :industryId AND langId = :langId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':industryId' => $industryId,
            ':langId' => Yii::app()->language,
        ]);

        $industry = $dataReader->read();

        return $industry['name'];
    }

    public function getAllIndustries(){

        $sql = '
            SELECT 
                tri.name         name,
                tri.shortNameUrl id,
                tri.shortNameUrl shortUrl
             
            FROM tbl_trindustry tri
            
            WHERE tri.langId = :langId
            ORDER BY name ASC
        ';

        $industries = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':langId' => Yii::app()->language]);

        return $industries;
    }

    public function getCountAllIndustries(){

        $sql = '
            SELECT 
                COUNT(i.id) cnt
                
            FROM tbl_industry i
        ';


        $dataReader = Yii::app()->db->createCommand($sql)->query([':langId' => Yii::app()->language]);

        $industries = $dataReader->read();

        $data = array(
            'name' => Yii::t('CatalogModule.catalog', 'Industries count'),
            'value' => $industries['cnt'],
        );

        return $data;
    }

    public function getCountries(){

        $sql = '
            SELECT DISTINCT
                trcn.name name,
                cn.shortUrl id,
                cn.code code
            FROM tbl_trcountry trcn
                LEFT JOIN tbl_country cn ON cn.id = trcn.trParentId
                LEFT JOIN tbl_district d ON d.countryId = cn.id
                LEFT JOIN tbl_region r ON r.districtId = d.id
                LEFT JOIN tbl_city c ON c.regionId = r.id
                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                LEFT JOIN tbl_fair f ON f.exhibitionComplexId = ec.id
            WHERE trcn.langId = :langId AND f.active = :active ORDER BY trcn.name ASC
        ';

        $countries = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->language, ':active' => Fair::ACTIVE_ON));

        return array_merge([['id' => 0, 'name' => Yii::t('CatalogModule.catalog','All'), 'code' => 'all']], $countries);
    }

    public function getRegionCodes(){

        $sql = '
            SELECT 
                trr.name name,
                trr.trParentId id,
                r.code code
             
            FROM tbl_trregion trr
            LEFT JOIN tbl_region r ON r.id = trr.trParentId
            
            WHERE trr.langId = :langId
            
            ORDER BY name ASC
        ';

        $regions = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->language));

        return $regions;
    }

    public function getFairsList($year, $industryId, $countryId, $regionId, $searchQuery = NULL){

        $conditions = array();
        $additionalParams = array();
        $conditions[] = " YEAR(f.beginDate) = :year AND ";
        if($year == NULL){
            $year = date('Y');
        }
        $additionalParams[':year'] = $year;

        if($countryId != NULL){
            $conditions[] = " cntry.id = :countryId AND ";
            $additionalParams[':countryId'] = $countryId;
        }

        if($regionId != NULL){
            $conditions[] = " r.id = :regionId AND ";
            $additionalParams[':regionId'] = $regionId;
        }

        if($industryId != NULL){
            $conditions[] = ' tri.trParentId = :industryId AND ';
            $additionalParams[':industryId'] = $industryId;
        }

        $order = "fi.members DESC";
        if(!empty($searchQuery)){
            $order = "f.beginDate DESC";

            $conditions = array();
            $additionalParams = array();
            $conditions[] = " trf.name LIKE :searchQuery AND ";
            $additionalParams = [':searchQuery' => "%{$searchQuery}%"];
            $q = "\"{$searchQuery}\" ";
        }

        $params = array_merge($additionalParams, [
            ':langId' => Yii::app()->language,
            ':userId' => Yii::app()->user->id,
        ]);

        $sql = [
            "SELECT" => "
                f.id id,
                trf.name `name`,
                f.beginDate beginDate,
                trf.uniqueText uniqueText,
                YEAR(f.beginDate) fairYear,
                f.endDate endDate,
                trc.name cityName,
                trr.name regionName,
                f.rating rating,
                trcntry.name countryName,
                trf.shortUrl url,
                IF(fm.id IS NULL, 0, 1) inMatches,
                IF(fi.squareNet IS NOT NULL, fi.squareNet, fi.squareGross) square
            ",
            "FROM" => "tbl_fair f",
            "JOIN" => "
                LEFT JOIN tbl_trfair trf               ON trf.trParentId = f.id AND trf.langId = :langId
                LEFT JOIN tbl_fairinfo fi              ON fi.id = f.infoId
                LEFT JOIN tbl_exhibitioncomplex ec     ON ec.id = f.exhibitionComplexId
                LEFT JOIN tbl_city c                   ON c.id = ec.cityId
                LEFT JOIN tbl_region r                 ON r.id = c.regionId
                LEFT JOIN tbl_district d               ON d.id = r.districtId
                LEFT JOIN tbl_country cntry            ON cntry.id = d.countryId
                LEFT JOIN tbl_trcountry trcntry        ON trcntry.trParentId = cntry.id AND trcntry.langId = :langId
                LEFT JOIN tbl_trcity trc               ON c.id = trc.trParentId AND trc.langId = :langId
                LEFT JOIN tbl_trregion trr             ON trr.trParentId = c.regionId AND trr.langId = :langId
                LEFT JOIN tbl_fairhasindustry fhi      ON fhi.fairId = f.id
                LEFT JOIN tbl_trindustry tri           ON tri.trParentId = fhi.industryId AND tri.langId = :langId
                LEFT JOIN tbl_fairmatches fm           ON fm.fairId = f.id AND fm.userId = :userId
            ",
            "WHERE" => implode('', $conditions) . " f.active = 1",
            "GROUP" => "f.id",
            "ORDER" => $order,
            "LIMIT" => 200,
        ];

        $fairs = Yii::app()->db->createCommand($sql)->queryAll(TRUE, $params);

        $data = array();
        foreach($fairs as $fair){

            $data[] = array(
                "id" => $fair['id'],
                "name" => $fair['name'],
                "date" => Fair::getDateRange($fair['beginDate'], $fair['endDate']),
                "region" => $fair['cityName']." / ".$fair['regionName'],
                "link" => $fair['url'],
                "rating" => $fair['rating'],
                "inMatches" => $fair['inMatches'],
            );
        }

        return $data;
    }

    public function getFairsByIndustry($industryId, $countryId, $year)
    {

        if(empty($industryId)){
            return $this->getCountAllIndustries();
        }

        $sql = "
            SELECT 
                COUNT(fhi.fairId) cnt,
                tri.name industryName
                
            FROM tbl_fairhasindustry fhi
            LEFT JOIN tbl_trindustry tri ON tri.trParentId =fhi.industryId AND tri.langId = :langId
            LEFT JOIN tbl_fair f ON fhi.fairId = f.id
            LEFT JOIN tbl_exhibitioncomplex ec     ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c                   ON c.id = ec.cityId
            LEFT JOIN tbl_region r                 ON r.id = c.regionId
            LEFT JOIN tbl_district d               ON d.id = r.districtId
            LEFT JOIN tbl_country cntry            ON cntry.id = d.countryId
            
            WHERE fhi.industryId = :industryId AND 
                  cntry.id = :countryId AND
                  f.active = 1 AND 
                  YEAR(f.beginDate) = :year 
                  
            GROUP BY fhi.industryId;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':industryId' => $industryId,
            ':countryId' => $countryId,
            ':year' => $year,
            ':langId' => Yii::app()->language,
        ]);

        $data = [];
        foreach($res as $row){
            $data = array(
                'name' => Yii::t('CatalogModule.catalog', 'Fairs')." ".$row['industryName'].' / '.$year,
                'value' => $row['cnt'],
            );
        }

        return $data;
    }

    public function getFairsByCountry($countryId, $year)
    {
        $sql = "
            SELECT 
                COUNT(0) cnt,
                trct.name countryName
                
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c ON ec.cityId = c.id
            LEFT JOIN tbl_region r ON c.regionId = r.id
            LEFT JOIN tbl_district d ON r.districtId = d.id
            LEFT JOIN tbl_country ct ON ct.id = d.countryId
            LEFT JOIN tbl_trcountry trct ON ct.id = trct.trParentId AND trct.langId = :langId
            
            WHERE f.active = 1 AND ct.id = :countryId AND YEAR(f.beginDate) = :year
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':countryId' => $countryId,
            ':year' => $year,
            ':langId' => Yii::app()->language
        ]);

        $res = $dataReader->read();

        $fairsCountry = array(
            "name" => Yii::t('CatalogModule.catalog', 'Fairs')." ".ucfirst($res['countryName'])." /  ".$year,
            "value" => $res['cnt'],
        );

        return $fairsCountry;
    }

    public function getRegionCountByCountry($countryId){

        $sql = "
            SELECT 
                COUNT(r.id) cnt
                
            FROM tbl_region r 
            LEFT JOIN tbl_district d               ON d.id = r.districtId
            LEFT JOIN tbl_country cntry            ON cntry.id = d.countryId
            
            WHERE cntry.id = :countryId AND r.id NOT IN (88, 89, 93)
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':countryId' => $countryId]);

        $res = $dataReader->read();

        $regionsCount = array(
            "name" => Yii::t('CatalogModule.catalog', 'Regions count'),
            "value" => $res['cnt'],
        );

        return $regionsCount;
    }

    public function getCountersIndustry($industryId){

        if(empty($industryId)){
            return $this->getAllCountersIndustry();
        }
        $sql = "
            SELECT 
                COUNT(DISTINCT cc.statId) cnt
                ,tri.name industryName
                
            FROM tbl_chartcolumn cc
            LEFT JOIN tbl_charttable ct ON ct.id = cc.tableId
            LEFT JOIN tbl_industry i ON i.id = ct.industryId
            LEFT JOIN tbl_trindustry tri ON tri.trParentId = i.id AND tri.langId = :langId
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.industryId = i.id
            LEFT JOIN tbl_fair f ON fhi.fairId = f.id
            
            WHERE fhi.id IS NOT NULL AND
                  fhi.fairId IS NOT NULL AND 
                  fhi.industryId = :industryId AND 
                  f.active = 1 AND 
                  YEAR(f.beginDate) >= YEAR(NOW())
                  
            GROUP BY i.id
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':industryId' => $industryId, ':langId' => Yii::app()->language]);

        $res = $dataReader->readAll();

        $data = array();
        foreach($res as $ind){
            $data = array(
                'name' => Yii::t('CatalogModule.catalog', 'Counters').' '.$ind['industryName'],
                'value' => $ind['cnt'],
            );
        }

        return $data;
    }

    public function getAllCountersIndustry(){

        $sql = "
            SELECT 
                COUNT(DISTINCT cc.statId) cnt
                
            FROM tbl_chartcolumn cc
            LEFT JOIN tbl_charttable ct ON ct.id = cc.tableId;
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query();

        $res = $dataReader->readAll();

        $data = array();
        foreach($res as $ind){
            $data = array(
                'name' => Yii::t('CatalogModule.catalog', 'Counters on all industries'),
                'value' => $ind['cnt'],
            );
        }

        return $data;
    }

    public function getFairCountByRegion($regionId)
    {

        $sql = "
            SELECT 
                COUNT(0) cnt,
                trr.name regionName,
                YEAR(NOW()) as year
            
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c ON ec.cityId = c.id
            LEFT JOIN tbl_region r ON c.regionId = r.id
            LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId AND trr.langId = :langId
            WHERE f.active = 1 AND r.id = :regionId AND YEAR(f.beginDate) >= YEAR(NOW())
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':regionId' => $regionId, ':langId' => Yii::app()->language]);

        $res = $dataReader->read();

        $fairsRegion = array(
            'name' => Yii::t('CatalogModule.catalog', 'Fairs').' '.$res['regionName'].' / '.$res['year'],
            'value' => $res['cnt'],
        );

        return $fairsRegion;
    }

    /**
     * @deprecated
     * @param $countryId
     * @return array
     */
    public function getFairUsefulLinks($countryId)
    {

        $links = array(
            'nearestFairsByCountry' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Nearest fairs in country'),
                'value' => Yii::app()->createUrl('catalog/html/view/', array('year'=>date('Y'), 'country' => $countryId)),
            ),
            'nearestFairsInMoscow' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Nearest fairs in Moscow'),
                'value' => '#'
            ),
            'nearestFairsInPiter' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Nearest fairs in Saint-Petersburg'),
                'value' => '#'
            ),
            'pricesInMoscow' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Prices in Moscow'),
                'value' => Yii::app()->createAbsoluteUrl('prices/moscow/'),
            ),
            'regions' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Regions RF'),
                'value' => Yii::app()->createAbsoluteUrl('regions/'),
            ),
            'venues' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Exhibition complexes'),
                'value' => Yii::app()->createAbsoluteUrl('venues/'),
            ),
            'venuesMoscow' => array(
                'name' => Yii::t('CatalogModule.catalog', 'Exhibition complexes in Moscow'),
                'value' => Yii::app()->createAbsoluteUrl('venues/moscow/'),
            ),
        );

        return $links;
    }

    public function getAnalyticsData($fairId){
        Yii::import('application.modules.gradoteka.models.*');

        $sql = '
            SELECT
              fhi.industryId,
              tri.name,
              trr.name regionName,
              trctry.name countryName
              
            FROM tbl_fair f
            LEFT JOIN tbl_exhibitioncomplex ec ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_city c ON c.id = ec.cityId
            LEFT JOIN tbl_region r ON r.id = c.regionId
            LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId AND trr.langId = :langId
            LEFT JOIN tbl_district d ON d.id = r.districtId
            LEFT JOIN tbl_country ctry ON ctry.id = d.countryId
            LEFT JOIN tbl_trcountry trctry on trctry.trParentId = ctry.id AND trctry.langId = :langId
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.fairid = f.id
            LEFT JOIN tbl_industry i ON fhi.industryId = i.id
            LEFT JOIN tbl_trindustry tri ON tri.trParentId = i.id AND tri.langId = :langId
            WHERE f.id = :fairId
        ';

        $industriesIds = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':fairId'=>$fairId, ':langId' => Yii::app()->language]);

        $analyticsData = array();
        foreach($industriesIds as $industryId){
            $circles = array();
            foreach ($this->getIndustryCirclesData($fairId, $industryId['industryId']) as $k => $r){

                $gvalue = new GValue();
                $gvalue->id = $r['gValId'];
                $gvalue->setId = $r['gValSetId'];
                $gvalue->epoch = $r['gValEpoch'];

                $circles[] = [
                    'percent'     => round($r['gvalue']/($gvalue->getTopValue()/100), 2),
                    'header'      => $r['header'],
                    'ratingPlace' => $gvalue->getRatingPoint(),
                    'ratingLabel' => Yii::t('CatalogModule.catalog', 'Place in Russia'),
                    'statValue'   => number_format(round($r['gvalue'], self::GVALUE_ROUND_PRECISION),self::GVALUE_ROUND_PRECISION,'.',' '),
                    'statUnit'    => $r['unit'],
                ];
            }

            $ind = [
                'header'  => $industryId['name'],
                'table'   => array(
                    "rows"=> $this->getFederalStats($fairId, $industryId['industryId']),
                ),
                'circles' => $circles,
                'regionName' => $industryId['regionName'],
                'countryName' => $industryId['countryName'],
            ];

            $analyticsData[] = $ind;
        }

        return $analyticsData;
    }

    public function getFederalStats($fairId, $industryId){

        $sql = "
            SELECT 
                gval.id gValId
                ,gval.setId gValSetId
                ,IFNULL(acp.header,stat.name) header
                ,IFNULL(acp.measure,stat.unit) unit,
                MAX(
                    CASE WHEN (gval.epoch = YEAR(NOW())) THEN gval.value 
                         WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value 
                         WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value 
                         END
                ) gvalue,
                MAX(
                    CASE WHEN (gval.epoch = YEAR(NOW())) THEN gval.epoch 
                         WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.epoch 
                         WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.epoch 
                         END
                ) gValEpoch
                
            FROM tbl_fair f
            LEFT JOIN tbl_exhibitioncomplex ec       ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_city c                     ON c.id = ec.cityId
            LEFT JOIN tbl_region r                   ON r.id = c.regionId
            LEFT JOIN tbl_district d                   ON d.id = r.districtId
            LEFT JOIN tbl_gobjecthascountry gohc      ON gohc.countryId = d.countryId
            LEFT JOIN tbl_gobjects obj               ON obj.id = gohc.objId
            LEFT JOIN tbl_gobjectshasgstattypes gset ON gset.objId = obj.id
            LEFT JOIN tbl_gstattypes stat            ON stat.gId = gset.statId
            LEFT JOIN tbl_analyticschartpreview acp  ON acp.stat = gset.statId
            LEFT JOIN tbl_gvalue gval                ON gval.setId = gset.id
            
            WHERE f.id = :fairId AND 
                  acp.id IS NOT NULL AND 
                  acp.industryId = :industryId AND
                  acp.gType = obj.gType
            GROUP BY gval.setId;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':fairId'=>$fairId,
            ':industryId' => $industryId,
        ]);

        $rawData = array();
        foreach($res as $row){
            $rawData[] = array(
                $row['header'],
                $row['unit'],
                strpos(round($row['gvalue'],2), '.')
                    ? number_format(round($row['gvalue'],2), 2, '.', ' ')
                    : number_format($row['gvalue'], 0, '.', ' ')
            );
        }

        return $rawData;
    }

    public function getIndustryCirclesData($fairId, $industryId){

        $sql = '
            SELECT 
                gval.setId gValSetId
                ,IFNULL(acp.header,stat.name) header
                ,IFNULL(acp.measure,stat.unit) unit,
                MAX(
                    CASE WHEN (gval.epoch = YEAR(NOW())) THEN gval.value 
                         WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value 
                         WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value 
                         END
                ) gvalue,
                MAX(
                    CASE WHEN (gval.epoch = YEAR(NOW())) THEN gval.epoch 
                         WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.epoch 
                         WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.epoch 
                         END
                ) gValEpoch,
                MAX(
                    CASE WHEN (gval.epoch = YEAR(NOW())) THEN gval.id 
                         WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.id 
                         WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.id 
                         END
                ) gValId
                
            FROM tbl_fair f
            LEFT JOIN tbl_exhibitioncomplex ec       ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_city c                     ON c.id = ec.cityId
            LEFT JOIN tbl_region r                   ON r.id = c.regionId
            LEFT JOIN tbl_gobjecthasregion gohr      ON gohr.regionId = r.id
            LEFT JOIN tbl_gobjects obj               ON obj.id = gohr.objId
            LEFT JOIN tbl_gobjectshasgstattypes gset ON gset.objId = obj.id
            LEFT JOIN tbl_gstattypes stat            ON stat.gId = gset.statId
            LEFT JOIN tbl_analyticschartpreview acp  ON acp.stat = gset.statId
            LEFT JOIN tbl_gvalue gval                ON gval.setId = gset.id
            
            WHERE f.id = :fairId AND 
                  acp.id IS NOT NULL AND 
                  acp.industryId = :industryId AND
	              acp.gType = obj.gType
            GROUP BY gval.setId;
        ';

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':fairId'=>$fairId,
            ':industryId' => $industryId,
        ]);

        return $res;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getAds(){
        return Yii::t('CatalogModule.catalog', 'ads');
    }

    /**
     * @param $countryId
     * @param $regionId
     * @param $industryId
     * @return string
     */
    private function conrevertUrl($countryId, $regionId, $industryId)
    {
        $country  = null;
        $region   = null;
        $industry = null;
        $link     = '';

        if (is_numeric($countryId)) {
            $country = Country::model()->findByPk($countryId);
        }

        if (is_numeric($regionId)) {
            $region = Region::model()->findByPk($regionId);
        }

        if (is_numeric($industryId)) {
            $industry = Industry::model()->findByPk($industryId);
        }

        if ($country != null) {
            $link = $link . '/country/' . $country->shortUrl;
        }

        if ($country == null && is_numeric($countryId)) {
            $country = new Country;
            $link = $link . '/country/' . $countryId;
        }

        if ($region != null) {
            $link = $link . '/region/' . $region->shortUrl;
        }

        if ($region == null && is_numeric($regionId)) {
            $region = new Region;
            $link = $link . '/region/' . $regionId;
        }

        if ($industry != null) {
            $link = $link . '/industry/' . $industry->shortUrl;
        }

        if ($industry == null && is_numeric($industryId)) {
            $industry = new Industry;
            $link = $link . '/industry/' . $industryId;
        }

        return ['link' => $link, 'country' => $country, 'region' => $region, 'industry' => $industry];
    }

    /**
     * @param string $industryName
     * @param string $countryName
     * @param string $regionName
     * @param string $year
     * @return string
     */
    private function getHeader($industryName = '', $countryName = '', $regionName = '', $year = ''){

        $result = [
            $countryName == '' ? Yii::t('CatalogModule.catalog', 'Fairs of world') : Yii::t('CatalogModule.catalog', 'Fairs'),
            $industryName,
            $countryName,
            $regionName,
            $year
        ];

        $title = implode(' ', $result);

        if(
            $year == date('Y') &&
            empty($industryName) &&
            empty($countryName)
        ){
            $title = Yii::t('CatalogModule.catalog', 'Protoplan fairs');
        }

        return $title;
    }

    /**
     * @param string $industryName
     * @param string $countryName
     * @param string $regionName
     * @param string $year
     * @return string
     */
    private function getDescription($industryName = '', $countryName = '', $regionName = '', $year = ''){

        $result = [
            $countryName == '' ? Yii::t('CatalogModule.catalog', 'Fairs of world') : Yii::t('CatalogModule.catalog', 'Fairs'),
            $industryName,
            $countryName,
            $regionName,
            $year
        ];

        $title = implode(' ', $result);

        if(
            $year == date('Y') &&
            empty($industryName) &&
            empty($countryName)
        ){
            $title = Yii::t('CatalogModule.catalog', 'Protoplan default catalog descirption');
        }

        return $title;
    }

    private function getQueryDesc($industryId = null, $countryId = null, $regionId = null, $year = null)
    {
        $params = [];

        if ($industryId != null) {
            $params['industryId'] = $industryId;
        }

        if ($countryId != null) {
            $params['countryId'] = $countryId;
        }

        if ($regionId != null) {
            $params['regionId'] = $regionId;
        }

        if ($year != null) {
            $params['year'] = $year;
        }

        $model = CatalogDescription::model()->findByAttributes($params);

        if ($model == null) {
            unset($params['year']);
            $model = CatalogDescription::model()->findByAttributes($params);
        }

        if (count($params) == 0) {
            $model = null;
        }

        $result = [];

        if (isset($model->header)) {
            $result['header'] = $model->header;
        }

        if (isset($model->text)) {
            $result['content'] = $model->text;
        }

        if (empty($result['header']) || empty($result['content'])) {
            $result = null;
        }

        if ($model == null) {
            $result = null;
        }

        return $result;
    }
}