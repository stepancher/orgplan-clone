<?php

class ApiController extends Controller {

    const STATUS_NOT_AUTHORIZED = '403 Forbidden';

    protected $authenticated = false;

    public function actions() {
        return array(
            'quote' => array(
                'class' => 'CWebServiceAction',
            ),
        );
    }

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function auth($SystemId, $UserName, $Password) {

        return true;
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        return true;
    }

    /**
     * @param array $rds the stock price
     * @return array the stock price
     * @soap
     */
    public function getTest($rds){


        return array('asd'=>'asd');
    }

    /**
     * @param integer $cityId
     * @param array $properties
     * @return array
     * @soap
     */
    public function getCityProperties($cityId, $properties){

        if(!empty($city = City::model()->findByPk($cityId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($city->{$name}) &&
                    !empty($city->{$name})
                ){
                    $res[$name] = $city->{$name};
                }
            }

            $properties = $res;
        }

        return $properties;
    }

    /**
     * @param integer $regionId
     * @param array $properties
     * @return array
     * @soap
     */
    public function getRegionProperties($regionId, $properties){

        if(!empty($region = Region::model()->findByPk($regionId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($region->{$name}) &&
                    !empty($region->{$name})
                ){
                    $res[$name] = $region->{$name};
                }
            }

            $properties = $res;
        }

        return $properties;
    }

    /**
     * @param integer $districtId
     * @param array $properties
     * @return array
     * @soap
     */
    public function getDistrictProperties($districtId, $properties){

        if(!empty($district = District::model()->findByPk($districtId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($district->{$name}) &&
                    !empty($district->{$name})
                ){
                    $res[$name] = $district->{$name};
                }
            }

            $properties = $res;
        }

        return $properties;
    }

    /**
     * @param integer $countryId
     * @param array $properties
     * @return array
     * @soap
     */
    public function getCountryProperties($countryId, $properties){

        if(!empty($country = Country::model()->findByPk($countryId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($country->{$name}) &&
                    !empty($country->{$name})
                ){
                    $res[$name] = $country->{$name};
                }
            }

            $properties = $res;
        }

        return $properties;
    }

    /**
     * @return array
     * @soap
     */
    public function getConstants(){
        $oClass = new ReflectionClass('User');
        return $oClass->getConstants();
    }
}