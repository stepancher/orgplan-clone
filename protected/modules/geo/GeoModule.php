<?php

class GeoModule extends CWebModule {

	public function init() {

		$this->setImport(array(
			'geo.models.*',
		));
	}
}
