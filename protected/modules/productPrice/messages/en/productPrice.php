<?php 
return array(
  'Category' => 'Category',
  'City' => 'City',
  'Fair not found' => '',
  'Query cost of services' => 'Query cost of services',
  'Service' => 'Service',
  'Greetings!' => 'Hello!',
  'I Org Plan.' => 'I am Protoplan',
  'I help to participate in exhibitions.' => 'I help in exhibition planning and participation',
  'Send' => 'Send',
  'Enter' => 'Enter',
  'No matches' => 'No matches',
);