<?php 
return array(
  'Category' => 'Категория',
  'City' => 'Город',
  'Fair not found' => '',
  'Query cost of services' => 'Запрос стоимости услуг',
  'Service' => 'Услуга',
  'Greetings!' => 'Приветствую Вас!',
  'I Org Plan.' => 'Я Protoplan.',
  'I help to participate in exhibitions.' => 'Я помогаю участвовать в выставках.',
  'Send' => 'Отправить',
  'Enter' => 'Введите',
  'No matches' => 'Нет совпадений',
);