<?php 
return array(
  'Category' => 'Категория',
  'City' => 'Город',
  'Fair not found' => '',
  'Query cost of services' => 'Anfrage des Preises für Dienstleistungen',
  'Service' => 'Услуга',
  'Greetings!' => 'Ich begrüße Sie herzlich!',
  'I Org Plan.' => 'Ich bin Orgplan.',
  'I help to participate in exhibitions.' => 'Ich helfe an den Messen teilzunehmen.',
  'Send' => 'senden',
  'Enter' => 'Geben Sie ein',
  'No matches' => 'Keine Übereinstimmung gefunden',
);