<?php
class DefaultController extends Controller{

    /**
     * @param null $categoryId
     * @param null $cityId
     * @param null $productName
     * Запрос цены на новую услугу
     */
    public function actionOrderPrice($cityId = NULL, $categoryId = NULL, $productName = NULL){

        $result = array('success' => false);

        if (!Yii::app()->user->isGuest && User::checkTariff(1)) {
            $city = City::model()->findByPk($cityId);
            $category = ProductCategory::model()->findByPk($categoryId);

            Yii::import('application.modules.notification.models.Notification');
            $notification = Notification::saveByObject(
                1,
                $category,
                Notification::TYPE_ORDER_PRICE,
                array(
                    '{login}' => CHtml::link(Yii::app()->user->getName(), $this->createUrl('/user/view', array('id' => Yii::app()->user->id))),
                    '{productName}' => $productName,
                    '{city}' => $city ? $city->name : Yii::t('ProductPriceModule.productPrice', 'City not found'),
                    '{productCategory}' => $category ? $category->name : Yii::t('ProductPriceModule.productPrice', 'Fair not found')
                ),
                Yii::app()->user->id,
                false
            );

            Yii::app()->consoleRunner->run(
                'notification priceRequestSendEmail --notificationId=' . $notification->id . ' --cityId=' . $city->id.' --categoryId='.$category->id.' --productName="'.$productName.'"',
                true);

//            $request = new Request();
//            $request->userId = Yii::app()->user->id;
//            $request->date = date('Y-m-d');
//            $request->save(false);

            $result['success'] = true;
        }

        echo json_encode($result);
    }
}