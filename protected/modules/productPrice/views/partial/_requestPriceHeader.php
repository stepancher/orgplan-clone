<?php
Yii::import('application.modules.productPrice.ProductPriceModule');
?>
<div class="modal__modal-header">
    <span class="header-offset h1-header"><?= Yii::t('ProductPriceModule.productPrice','Greetings!')?></span>
    <span class="text-size-20"><?= Yii::t('ProductPriceModule.productPrice','I Org Plan.')?></span><br />
    <span class="text-size-20"><?= Yii::t('ProductPriceModule.productPrice','I help to participate in exhibitions.')?></span>
</div>