<?php
/**
 * @var $this CController
 * @var $publishUrl string
 * @var Stand $model
 */
?>
<div class="row-fluid modal__modal-content mini-registration">
    <div class="cols-row page-stand__modal--after-request-price-content">
        <p>
            Благодарим за доверие и работу в Оргплане!
        </p>
        <div class="cols-row page-stand__modal--after-request-price-button">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button class="b-button d-bg-success d-bg-success--hover d-text-light btn" data-dismiss="modal">Ok</button>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div>