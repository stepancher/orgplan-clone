<?php
/**
 * @var $this CController
 * @var $publishUrl string
 * @var Stand $model
 */
?>
<div class="row-fluid modal__modal-content mini-registration">
    <img alt="Загрузка" title="Загрузка" class="ajax-loader js-ajax-loader" src="<?= $publishUrl ?>\img\loader\ajax-loader.gif">

    <h3 class="text-center">
        <?= Yii::t('ProductPriceModule.productPrice', 'Query cost of services') ?>
    </h3>

    <form action="" method="post" class="form-vertical" id="requestPrise">
        <div class="cols-row offset-small-l-5 offset-small-r-5">
            <div class="col-md-12">
                <div class="cols-row">
                    <div class="col-md-12">
                        <?= Yii::app()->controller->widget('application.modules.productPrice.components.ListAutoComplete', array(
                            'id' => 'city',
                            'name' => 'ProductPrice[cityId]',
                            'label' => /*ProductPrice::model()->getAttributeLabel('city'),*/Yii::t('ProductPriceModule.productPrice', 'City'),
                            'multiple' => false,
                            'htmlOptions' => array(
                                'labelOptions' => array(
                                    'class' => 'd-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark fixed-width-3'
                                )
                            ),
                            'itemOptions' => array(
                                'data' => \yii\helpers\ArrayHelper::map(\City::model()->findAll(), 'id', 'name'),
                            )
                        ), true) ?>
                    </div>
                </div>
                <div class="cols-row">
                    <div class="col-md-12">
                        <?= Yii::app()->controller->widget('application.modules.productPrice.components.ListAutoComplete', array(
                            'name' => 'ProductCategory[id]',
                            'id' => 'category',
                            'label' => /*ProductCategory::model()->getAttributeLabel('name'),*/Yii::t('ProductPriceModule.productPrice', 'Category'),
                            'multiple' => false,
                            'htmlOptions' => array(
                                'labelOptions' => array(
                                    'class' => 'd-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark fixed-width-3'
                                )
                            ),
                            'itemOptions' => array(
                                'data' => \yii\helpers\ArrayHelper::map(\ProductCategory::model()->findAll(), 'id', 'name'),
                            )
                        ), true) ?>
                    </div>
                </div>
                <div class="cols-row">
                    <div class="col-md-12">
                        <?= H::controlGroup(TbHtml::textField('Product[name]', '', [
                            'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark fixed-width-3',
                            'placeholder' => Yii::t('ProductPriceModule.productPrice', 'Service'),
                        ])) ?>
                    </div>
                </div>
                <div class="cols-row page-stand__modal--request-pricse-button">
                    <div class="col-md-12">
                        <?= TbHtml::button(Yii::t('ProductPriceModule.productPrice', 'Send'), [
                            'class' => 'request-price-button b-button d-bg-success-dark d-bg-success d-text-light btn fixed-width-1 p-pull-right offset-small-r',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>