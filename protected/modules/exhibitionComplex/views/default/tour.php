<?php
/**
 * @var ExhibitionComplexController $this
 * @var ExhibitionComplex $model
 */

?>
<div class="cols-row">

		<div class="col-md-10">
			<?=$this->renderPartial('/_blocks/_b-header', array(
				'header' => $model->name,
				'description' => Yii::t('ExhibitionComplexModule.exhibitionComplex', '3D Tour'),
			))?>
		</div>

	<div class="cols-row panorama-indent">
		<div class="col-md-12">
			<iframe class="tour" src="/themes/panorama/<?=$model->plainHash;?>/tour.html"></iframe>
		</div>
	</div>
</div>
