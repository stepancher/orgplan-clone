<?php
/**
 * @var CController $this
 * @var CActiveDataProvider $dataProvider
 * @var CWebApplication $app
 * @var ExhibitionComplex[] $ECAll
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerCssFile($am->publish($app->theme->basePath . '/assets' . '/css/main.css'));
$cs->registerCssFile($am->publish($app->theme->basePath . '/assets' . '/css/exhibition/main.css'));
$cs->registerCssFile($am->publish($app->theme->basePath . '/assets' . '/css/autoComplete/style.css'));
$cs->registerScriptFile($am->publish($app->theme->basePath . '/assets' . '/js/exhibitionComplex/main.js'));
$cs->registerScriptFile($am->publish($app->theme->basePath . '/assets' . '/js/autoComplete/script.js'));

$app->clientScript->registerScript('list-items', "
");
$ctrl = $this;
$thumbnails = array();
foreach ($dataProvider->getData() as $exhibitionComplex) {

	$geo = array();
	if ($exhibitionComplex->city) {
		$geo[] = $exhibitionComplex->city->name;
		if ($exhibitionComplex->city->region) {
			$geo[] = $exhibitionComplex->city->region->name;
		}
	}
	$location = empty($geo) ? Yii::t('ExhibitionComplexModule.exhibitionComplex', 'no Information') : implode(', ', $geo);
	$area = empty($exhibitionComplex->square) || $exhibitionComplex->square === null ? Yii::t('ExhibitionComplexModule.exhibitionComplex', 'no Information') : $exhibitionComplex->square . Yii::t('ExhibitionComplexModule.exhibitionComplex', 'm<sup>2</sup>');

	$thumbnails[] = array(
		'image' => H::getImageUrl($exhibitionComplex, 'image'),
		'caption' => '
			<div class="fair-name">
				<span><b>' . $exhibitionComplex->name . '</b>  </span>
			</div>
			<div class="fair-count">
				<img src="' . $am->publish($app->theme->basePath . '/assets' . '/img/exhibition/fair-image.png') . '"> </img>'
			. '<span><i>' . H::getCountText($exhibitionComplex->number, array(
				Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibitions'),
				Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibition'),
				Yii::t('ExhibitionComplexModule.exhibitionComplex', 'the exhibition'))) . '</i></span>
				<img style="margin-left: 16px" src="' . $am->publish($app->theme->basePath . '/assets' . '/img/exhibition/area-image.png') . '"> </img>
				<i style ="font-size: 12px">' . $area . '</i>
			' .
			'</div>' .
			'<div class="fair-location">
				<img src="' . $am->publish($app->theme->basePath . '/assets' . '/img/exhibition/location-image.png') . '"> </img>'
			. '<i style ="font-size: 12px">' . $location . '</i>' .
			'</div>',
		'url' => $this->createUrl('exhibitionComplex/html/view', array('urlVenueName' => $exhibitionComplex->id)),
		'htmlOptions' => array('target' => '_blank', 'style' => 'position: relative;width: 284px; margin-left: -20px; margin-bottom: 10px;'),
	);
}

echo $this->widget('zii.widgets.CBreadcrumbs', array(
	'separator' => ' / ',
	'links' => array(
		Yii::t('ExhibitionComplexModule.exhibitionComplex', 'list exhibition centers'),
	),
	'htmlOptions' => array(
		'class' => 'breadcrumbs'
	)
), true);
?>
<div>
<h1><?= Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibition centers') ?></h1>
<?php
if ($app->user->checkAccess('exhibitionComplexActionAddExhibitionComplex')) {
	echo TbHtml::form('addExhibitionComplex', 'post', array('enctype' => 'multipart/form-data'));
	$this->widget('application.modules.exhibitionComplex.components.FieldSetView', array(
			'header' => Yii::t('ExhibitionComplexModule.exhibitionComplex', 'download data in XML format'),
			'items' => array(
				array(
					'CMultiFileUpload',
					'model' => ExhibitionComplex::model(),
					'max' => 1,
					'attribute' => 'exhibitionComplexXML',
					'duplicate' => Yii::t('ExhibitionComplexModule.exhibitionComplex', 'This file already exists!'),
					'accept' => 'xml',
					'denied' => Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Invalid file type'),
				)
			),
		)
	);
	echo TbHtml::submitButton(Yii::t('ExhibitionComplexModule.exhibitionComplex', 'create'),
		array(
			'color' => TbHtml::BUTTON_COLOR_WARNING,
		)
	);
	echo TbHtml::endForm();
	echo '<hr />';
}
if ($app->user->checkAccess('exhibitionComplexActionSave')) {
	echo TbHtml::linkButton(Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Grid action add'), array(
		'url' => $this->createUrl('save'),
		'color' => TbHtml::BUTTON_COLOR_SUCCESS,
	));
}
?>
<legend style="padding-bottom: 13px">
	Сегодня на портале:
	<b><?= H::getCountText(count($ECAll), array(
			Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibition centers'),
			Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibition centre'),
			Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibition center'))) ?></b>
	<?=
	TbHtml::linkButton('Фильтр', array(
		'color' => TbHtml::BUTTON_COLOR_SUCCESS,
		'class' => 'filter-toggle-button',
		'style' => 'float: right'
	));
	?>
</legend>
<?php

if (($page = Page::model()->findByAttributes(array('type' => Page::TYPE_INFO_ABOUT_EXHIBITION_COMPLEX,
		'status' => Page::STATUS_PUBLISHED))) !== null
) {
	?>
	<div style="clear: both"></div>
	<div class="global-description">
		<div class="page-name" style="font-size: 24px">
			<?= $page->name ?: '' ?>
		</div>
		<div class="page-description">
			<?= $page->description ?: '' ?>
		</div>
	</div> <?php
};


?>

<div class="filter-block">
	<div class="filter-input span3 control-group auto-complete input-name">
		<div class="controls">
			<div class="enter-text-input">
				<input class="auto-complete-input" type="text">
				<input id="ExhibitionComplex_id" class="input-hidden" value="" type="hidden">

				<div class="input-arrow">
					<div class="auto-complete-arrow"></div>
				</div>
			</div>
			<div class="drop-box" style="max-height: 300px; overflow: auto"></div>
		</div>
	</div>
	<div class="filter-input span3 control-group auto-complete input-region">
		<div class="controls">
			<div class="enter-text-input">
				<input class="auto-complete-input" type="text">
				<input id="ExhibitionComplex_regionId" class="input-hidden" value="" type="hidden">

				<div class="input-arrow">
					<div class="auto-complete-arrow"></div>
				</div>
			</div>
			<div class="drop-box" style="max-height: 300px; overflow: auto"></div>
		</div>
	</div>
	<div class="filter-input span3 control-group auto-complete input-city">
		<div class="controls">
			<div class="enter-text-input">
				<input class="auto-complete-input" type="text">
				<input id="ExhibitionComplex_cityId" class="input-hidden" value="" type="hidden">

				<div class="input-arrow">
					<div class="auto-complete-arrow"></div>
				</div>
			</div>
			<div class="drop-box" style="max-height: 300px; overflow: auto"></div>
		</div>
	</div>
</div>
<div style="clear:both"></div>
<script type="text/javascript">
	$(function () {
		new AutoComplete({
			data: <?=json_encode(CHtml::listData($ECAll, 'id', 'name'))?>,
			element: $('.auto-complete.input-name'),
			emptyValue: '<?=Yii::t('ExhibitionComplexModule.exhibitionComplex','all exhibition centers')?>',
			init: function (e) {
				$('.drop-box', e.config.element).hide();

				setTimeout(function () {
					$('.drop-box div', e.config.element).click(function () {
						$('.auto-complete-input', e.config.element).val($(this).text());
						$('.input-hidden', e.config.element).val($(this).data('id'));
					});
				}, 100);

				$('.auto-complete-input', e.config.element).focus(function () {
					$('.drop-box', e.config.element).show();
				});

				$('.auto-complete-input', e.config.element).blur(function () {
					setTimeout(function () {
						$('.drop-box', e.config.element).hide();
					}, 200)
				});

				e.config.element.find('.input-arrow').click(function () {
					$('.auto-complete-input', e.config.element).focus();
				});

			}
		});

		new AutoComplete({
			data: <?=json_encode(CHtml::listData(Region::model()->findAll(), 'id', 'name'))?>,
			element: $('.auto-complete.input-region'),
			emptyValue: '<?=Yii::t('ExhibitionComplexModule.exhibitionComplex','all regions')?>',
			init: function (e) {
				$('.drop-box', e.config.element).hide();

				setTimeout(function () {
					$('.drop-box div', e.config.element).click(function () {
						$('.auto-complete-input', e.config.element).val($(this).text());
						$('.input-hidden', e.config.element).val($(this).data('id'));
					});
				}, 100);

				$('.auto-complete-input', e.config.element).focus(function () {
					$('.drop-box', e.config.element).show();
				});

				$('.auto-complete-input', e.config.element).blur(function () {
					setTimeout(function () {
						$('.drop-box', e.config.element).hide();
					}, 200)
				});

				e.config.element.find('.input-arrow').click(function () {
					$('.auto-complete-input', e.config.element).focus();
				});

			}
		});

		new AutoComplete({
			data: <?=json_encode(CHtml::listData(City::model()->findAll(), 'id', 'name'))?>,
			element: $('.auto-complete.input-city'),
			emptyValue: '<?=Yii::t('ExhibitionComplexModule.exhibitionComplex','all cities')?>',
			init: function (e) {
				$('.drop-box', e.config.element).hide();

				setTimeout(function () {
					$('.drop-box div', e.config.element).click(function () {
						$('.auto-complete-input', e.config.element).val($(this).text());
						$('.input-hidden', e.config.element).val($(this).data('id'));
					});
				}, 100);

				$('.auto-complete-input', e.config.element).focus(function () {
					$('.drop-box', e.config.element).show();
				});

				$('.auto-complete-input', e.config.element).blur(function () {
					setTimeout(function () {
						$('.drop-box', e.config.element).hide();
					}, 200)
				});

				e.config.element.find('.input-arrow').click(function () {
					$('.auto-complete-input', e.config.element).focus();
				});

			}
		});
	});
</script>
<div id="fair-thumbnails">
	<?= !empty($thumbnails) ? TbHtml::thumbnails($thumbnails) : Yii::t('ExhibitionComplexModule.exhibitionComplex', 'There are no data available on request') ?>
	<div class="show-more-items">
		<div data-count="<?= count($dataProvider->getData()) ?>">
			<?= TbHtml::image($am->publish($app->theme->basePath . '/assets' . '/img/exhibition/more-image.png')) ?>
			<span><?= Yii::t('ExhibitionComplexModule.exhibitionComplex', 'show more') ?></span>
		</div>
	</div>
</div>
</div>