<?php
Yii::import('application.widgets.DropZone');

/**
 * Class DefaultController
 * @var DefaultController $this
 */
class DefaultController extends Controller
{
    /**
     * @var ExhibitionComplex $model
     */
    public $model = 'ExhibitionComplex';
    public $isNewRecord;

    /**
     * Стандартные actions перенаправляют на 404.
     */
    public function actionIndex()
    {
        throw new CHttpException(404, Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }

    public function actionView($id, $partial = '_description')
    {
        $this->redirect(Yii::app()->createUrl('exhibitionComplex/html/view', array('urlVenueName' => $id)));
    }

    /**
     * @param $top
     * @param $right
     * @param $bottom
     * @param $left
     * @param $request
     * составление запроса для получения маркеров карты
     */
    public function actionGetMapData($top, $right, $bottom, $left, $request)
    {
        $nClass = null;
        if (preg_match('/landmark/', $request)) {
            $nClass = 'sights%2Cartwork%2Cviewpoint%2Ccity_gate%2Cattraction%2Ccastle%2Czoo%2Cruins%2Clighthouse%2Cboundary_stone%2Cmemorial%2Cwreck%2Cmonument%2Cmuseum%2Ctheme_park%2Cbattlefield%2Carchaeological_site%2Crune_stone%2Cship%2Cfountain%2Cfort&';
        } elseif (preg_match('/hotel/', $request)) {
            $nClass = 'hotel';
        } elseif (preg_match('/cafe/', $request)) {
            $nClass = 'cafe';
        }
        $url = 'http://openstreetmap.ru/api/poi?action=getpoibbox&nclass=' . $nClass . '&t=' . $top . '&r=' . $right . '&b=' . $bottom . '&l=' . $left;

        try{
            $data = file_get_contents($url);
        }catch(Exception $e){
            $data = NULL;
        }

        echo $data;
    }

    /**
     * 3D tour
     * @param $id
     * @throws CHttpException
     */
    public function actionTour($id)
    {
        $model = ExhibitionComplex::model()->findByPk($id);
        if ($model) {
            if ($model->plainHash) {
                if (is_dir(Yii::app()->getBasePath() . '/../themes/panorama/' . $model->plainHash)) {
                    $this->render('tour', array('model' => $model));
                    return;
                }
            }
        }
        throw new CHttpException(404, Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }
}