<?php

class ApiController extends Controller {

    const STATUS_NOT_AUTHORIZED = '403 Forbidden';

    protected $authenticated = false;

    public function actions() {
        return array(
            'quote' => array(
                'class' => 'CWebServiceAction',
            ),
        );
    }

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function auth($SystemId, $UserName, $Password) {

        return true;
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        return true;
    }

    /**
     * @param integer $exhibitionComplexId
     * @return bool
     * @soap
     */
    public function getExhibitionComplexExists($exhibitionComplexId){
        $result = FALSE;

        if(!empty($exhibitionComplex = ExhibitionComplex::model()->findByPk($exhibitionComplexId))){
            $result = TRUE;
        }

        return $result;
    }

    /**
     * @param integer $exhibitionComplexId
     * @param array $properties
     * @return array
     * @soap
     */
    public function getExhibitionComplexProperties($exhibitionComplexId, $properties){

        if(!empty($exhibitionComplex = ExhibitionComplex::model()->findByPk($exhibitionComplexId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($exhibitionComplex->{$name}) &&
                    !empty($exhibitionComplex->{$name})
                ){
                    $res[$name] = $exhibitionComplex->{$name};
                }
            }

            $properties = $res;
        }

        return $properties;
    }

    /**
     * @return array
     * @soap
     */
    public function getConstants(){
        $oClass = new ReflectionClass('ExhibitionComplex');
        return $oClass->getConstants();
    }

}