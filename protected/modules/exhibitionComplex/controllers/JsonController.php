<?php

class JsonController extends Controller
{

    protected $dump = FALSE;
    protected $list_limit = 20;

    public function actionDump($langId, $urlExhibitionComplexName){
        $this->dump = TRUE;
        $this->actionList($langId, $urlExhibitionComplexName);
    }

    public function actionList($categoryId = NULL, $cityId = NULL, $regionId = NULL, $countryId = NULL, $searchQuery = NULL, $page = 1)
    {

        $json = array(
            "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Exhibition complexes catalog'),
            "countries"=> array(
                "placeholder"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Country'),
                "list"=> $this->getCountries(),
            ),
            "regions"=> array(
                "placeholder"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Region'),
                "list"=> $this->getRegions($countryId),
            ),
            "cities"=> array(
                "placeholder"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'City'),
                "list"=> $this->getCities($countryId, $regionId),
            ),
            "categories"=> array(
                "placeholder"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Category'),
                "list"=> $this->getExhibitionComplexCategories(),
            ),
            "models"=> $this->getExhibitionComplexes($categoryId, $cityId, $regionId, $countryId, $searchQuery, $page),
            "searchLabel"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Filters'),
            "searchPlaceholder"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Search in name'),
            "categoryLabel"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Category'),
            "placeLabel"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Place'),
        );

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    public function getExhibitionComplexes($categoryId = NULL, $cityId = NULL, $regionId = NULL, $countryId = NULL, $searchQuery = NULL, $page = 1){

        $params = array(
            ':langId' => Yii::app()->language,
            ':fileType' => ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE,
        );

        $filterParams = [];
        $additionalConditions = [];

        if(!empty($categoryId)){
            $filterParams[':categoryId'] = $categoryId;
            $additionalConditions[] = "ect.id = :categoryId AND ";
        }

        if(!empty($cityId)){
            $filterParams[':cityId'] = $cityId;
            $additionalConditions[] = "c.id = :cityId AND ";
        }

        if(!empty($regionId)){
            $filterParams[':regionId'] = $regionId;
            $additionalConditions[] = "r.id = :regionId AND ";
        }

        if(!empty($countryId)){
            $filterParams[':countryId'] = $countryId;
            $additionalConditions[] = "cntry.id = :countryId AND";
        }

        $q = '';
        if(!empty($searchQuery)){
            $additionalConditions = ["trec.name LIKE :searchQuery AND"];
            $filterParams = [':searchQuery' => "%{$searchQuery}%"];
            $q = "\"{$searchQuery}\" ";
        }

        $sql = [
            "SELECT" => " 
                trec.name AS name,
                trr.name AS region,
                trc.name AS city,
                ec.id AS id,
                objf.name AS fileName,
                objf.label AS fileLabel,
                trect.name AS exhibitionComplexTypeName,
                trec.shortUrl AS shortUrl
            ",
            "FROM" => "tbl_exhibitioncomplex ec",
            "JOIN" => "
                LEFT JOIN tbl_trexhibitioncomplex trec ON (trec.trParentId = ec.id AND trec.langId = :langId)
                LEFT JOIN tbl_trexhibitioncomplextype trect ON (trect.id = ec.exhibitionComplexTypeId AND trec.langId = :langId)
                LEFT JOIN tbl_exhibitioncomplextype ect ON ect.id = trect.trParentId
                LEFT JOIN tbl_city c ON (ec.cityId = c.id)
                LEFT JOIN tbl_trcity trc ON (trc.trParentId = c.id AND trc.langId = :langId)
                LEFT JOIN tbl_region r ON (r.id = c.regionId)
                LEFT JOIN tbl_trregion trr ON (r.id = trr.trParentId AND trr.langId = :langId)
                LEFT JOIN tbl_district d ON (d.id = r.districtId)
                LEFT JOIN tbl_country cntry ON (cntry.id = d.countryId)
                LEFT JOIN tbl_exhibitioncomplexhasfile echf ON (echf.exhibitionComplexId = ec.id)
                LEFT JOIN tbl_objectfile objf ON (objf.id = echf.fileId AND objf.type = :fileType)
            ",
            "WHERE" => implode('', $additionalConditions) . " ec.active = 1",
            "GROUP" => "ec.id",
            "ORDER" => "ec.square DESC",
            "LIMIT" => $this->list_limit,
        ];

        if(!empty($page)){
            $sql['OFFSET'] = $this->list_limit * $page - $this->list_limit;
        }

        $countSql = $sql;
        $countSql['SELECT'] = 'COUNT(0)';
        unset($countSql['GROUP']);

        $resultsCount = Yii::app()->db->createCommand($countSql)->queryScalar(array_merge($params, $filterParams));
        $exhibitionComplexes = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array_merge($params, $filterParams));


        $data = array();
        foreach($exhibitionComplexes as $exhibitionComplex){

            $filePath = Yii::app()->getBaseUrl(true).'/frontend/empty.png';

            if(!empty($exhibitionComplex['fileName'])){
                $filePath = Yii::app()->getBaseUrl(true).'/uploads/'.str_replace('{id}',$exhibitionComplex['id'], ObjectFile::$paths[ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE]).'/'.$exhibitionComplex['fileName'];
            }

            $data[] = array(
                "id" => $exhibitionComplex['shortUrl'],
                "name"=> mb_convert_case($exhibitionComplex['name'], MB_CASE_LOWER, "UTF-8"),
                "desc"=> !empty($exhibitionComplex['city'])?$exhibitionComplex['city']:'',
                "image"=> $filePath,
            );
        }

        $result = array(
            "searchResultLabel"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'On search')." ".Yii::t('ExhibitionComplexModule.exhibitionComplex', 'founded')." ".$resultsCount." ".Yii::t('ExhibitionComplexModule.exhibitionComplex', 'exhibitionComplexes'),
            'list' => $data,
        );

        return $result;
    }

    public function getExhibitionComplexCategories(){

        $sql = '
            SELECT 
                trect.name name,
                trect.trParentId id
             
            FROM tbl_trexhibitioncomplextype trect
            LEFT JOIN tbl_exhibitioncomplextype ect ON ect.id = trect.trParentId
            
            WHERE trect.langId = :langId
            
            ORDER BY name ASC
        ';

        $categories = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->language));

        return $categories;
    }

    public function getCities($countryId, $regionId){

        $countryCondition = '';
        if(!empty($countryId)){
            $countryCondition = "d.countryId = {$countryId} AND";
        }

        $regionCondition = '';
        if(!empty($regionId)){
            $regionCondition = "c.regionId = {$regionId} AND";
        }

        $sql = "
            SELECT 
                trc.name name,
                trc.trParentId id
             
            FROM tbl_trcity trc
            LEFT JOIN tbl_city c ON c.id = trc.trParentId
            LEFT JOIN tbl_region r ON r.id = c.regionId
            LEFT JOIN tbl_district d ON d.id = r.districtId
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
            
            WHERE 
                {$regionCondition}
                {$countryCondition}
                trc.langId = :langId AND
                ec.id IS NOT NULL AND
                ec.active = :active

            GROUP BY trc.id
            ORDER BY name ASC
        ";

        $cities = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->language, ':active' => ExhibitionComplex::ACTIVE_ON));

        return $cities;
    }

    public function getRegions($countryId = NULL){

        $params = array(
            ':langId' => Yii::app()->language
        );

        $countryCondition = '';
        if(!empty($countryId)){
            $countryCondition = "d.countryId = :countryId AND";
            $params[':countryId'] = $countryId;
        }

        $sql = "
            SELECT 
                trr.name name,
                trr.trParentId id
             
            FROM tbl_trregion trr
            LEFT JOIN tbl_region r ON r.id = trr.trParentId
            LEFT JOIN tbl_district d ON d.id = r.districtId
            LEFT JOIN tbl_city c ON c.regionId = r.id
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
            
            WHERE 
                {$countryCondition}
                trr.langId = :langId AND
                ec.id IS NOT NULL AND
                ec.active = :active

            GROUP BY r.id
            ORDER BY name ASC
        ";
        $params[':active'] = ExhibitionComplex::ACTIVE_ON;

        $regions = Yii::app()->db->createCommand($sql)->queryAll(TRUE, $params);

        return $regions;
    }

    public function getCountries(){

        $sql = '
            SELECT 
                trc.name name,
                trc.trParentId id
             
            FROM tbl_trcountry trc
            LEFT JOIN tbl_country cntry ON cntry.id = trc.trParentId
            LEFT JOIN tbl_district d ON d.countryId = cntry.id
            LEFT JOIN tbl_region r ON r.districtId = d.id
            LEFT JOIN tbl_city c ON c.regionId = r.id
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
            
            WHERE trc.langId = :langId AND
                  ec.id IS NOT NULL AND
                  ec.active = :active

            GROUP BY trc.trParentId
            ORDER BY name ASC
        ';

        $countries = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->language, ':active' => ExhibitionComplex::ACTIVE_ON));

        return $countries;
    }

    public function actionGetExhibitionComplexes($categoryId = NULL, $cityId = NULL, $regionId = NULL, $countryId = NULL, $searchQuery = NULL, $page = 1){

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($this->getExhibitionComplexes($categoryId, $cityId, $regionId, $countryId, $searchQuery, $page)) . ');';
    }

    public function actionGetData($urlVenueName){

        $digitUrlType = ctype_digit($urlVenueName);

        if($digitUrlType){

            $venue = TrExhibitionComplex::model()->findByAttributes([
                'trParentId' => (int)$urlVenueName,
                'langId' => Yii::app()->language,
            ]);

            if($venue !== NULL){

                echo (isset($_GET['callback']) ? $_GET['callback'] : '') .
                    ' (' .
                    json_encode(
                        array(
                            'status' => 'redirect',
                            'code' => 301,
                            'link' => $venue->shortUrl,
                        )
                    ) .
                    ');';
                exit;
            }
        }

        $trExhibitionComplex = TrExhibitionComplex::model()->findByAttributes(
            array(
                'shortUrl' => $urlVenueName,
                'langId' => Yii::app()->language,
            )
        );

        if($trExhibitionComplex === NULL){
            throw new CHttpException(404, 'Venue not found');
        }

        $params = array(
            ':langId' => Yii::app()->language,
            ':fileType' => ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE,
            ':urlVenueName' => $urlVenueName,
            ':fairActive' => Fair::ACTIVE_ON,
            ':fairCanceled' => Fair::CANCELED_NO,
            ':fairYear' => DATE('Y'),
        );

        $sql = "
            SELECT 
                ec.id                                     AS id,
                trec.name                                 AS name,
                ec.visitors                               AS conferenceRooms,
                ec.members                                AS pavilions,
                COUNT(f.id)                               AS fairs,
                ec.square                                 AS square,
                ec.accreditationPrice                     AS accreditationPrice,
                trec.description                          AS organization,
                CONCAT(trcntry.name, ', ', trec.street)   AS address,
                ec.site                                   AS site,
                ec.phone                                  AS phone,
                ec.email                                  AS email,
                ec.standsConstructionContact              AS standsConstructionContact,
                ec.cateringContact                        AS cateringContact,
                ec.accreditationContact                   AS accreditationContact,
                ec.advertisingContact                     AS advertisingContact,
                SUBSTRING_INDEX(ec.coordinates, ',', 1)   AS latitude,
                SUBSTRING_INDEX(ec.coordinates, ',', -1)  AS longitude,
                objf.name                                 AS fileName,
                objf.label                                AS fileLabel,
                trec.uniqueText                           AS description,
                ecst.name                                 AS shortType,
                trec.descriptionSnippet                   AS descriptionSnippet,
                trectype.fullNameSingle                   AS ecFullSingleTypeName,
                trc.name                                  AS cityName
            
            FROM tbl_exhibitioncomplex                ec
            
            LEFT JOIN tbl_trexhibitioncomplex         trec      ON (ec.id = trec.trParentId AND trec.langId = :langId)
            LEFT JOIN tbl_trexhibitioncomplextype     trectype  ON (ec.exhibitionComplexTypeId = trectype.trParentId AND trectype.langId = :langId)
            LEFT JOIN tbl_exhibitioncomplexshorttype  ecst      ON (ecst.id = ec.exhibitionComplexShortTypeId)
            LEFT JOIN tbl_city                        c         ON (ec.cityId = c.id)
            LEFT JOIN tbl_trcity                      trc       ON (trc.trParentId = c.id AND trc.langId = :langId)
            LEFT JOIN tbl_region                      r         ON (r.id = c.regionId)
            LEFT JOIN tbl_district                    d         ON (d.id = r.districtId)
            LEFT JOIN tbl_country                     cntry     ON (cntry.id = d.countryId)
            LEFT JOIN tbl_trcountry                   trcntry   ON (trcntry.trParentId = cntry.id AND trcntry.langId = :langId)
            LEFT JOIN tbl_exhibitioncomplexhasfile    echf      ON (echf.exhibitionComplexId = ec.id)
            LEFT JOIN tbl_objectfile                  objf      ON (objf.id = echf.fileId AND objf.type = :fileType)
            LEFT JOIN tbl_fair                        f         ON (f.exhibitionComplexId = ec.id AND f.active = :fairActive AND f.canceled = :fairCanceled AND YEAR(f.beginDate) = :fairYear)
            
            WHERE trec.shortUrl = :urlVenueName;
            GROUP BY ec.id
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query($params);

        $res = $dataReader->read();

        $filePath = '';

        if(!empty($res['fileName'])){
            $filePath = Yii::app()->getBaseUrl(true).'/uploads/'.str_replace('{id}',$res['id'], ObjectFile::$paths[ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE]).'/'.$res['fileName'];
        }

        $exhibitionComplex = array(
            "description" => $res['descriptionSnippet'],
            "title" => $res['ecFullSingleTypeName'].' '. '"'.$res['name'].'"',
            "header"=> Yii::app()->language == Yii::app()->params['defaultLanguage'] ? $res['shortType'] . ' ' . $res['name'] : $res['name'],
            "halls"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Conference rooms'),
                "value"=> $res['conferenceRooms'],
            ),
            "pavilions"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Pavilions'),
                "value"=> $res['pavilions']
            ),
            "fairs"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Fairs'),
                "value"=> $res['fairs'],
                "year" => DATE('Y'),
            ),
            "square"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Swuare'),
                "value"=> $res['square'],
                "unit"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'm2')
            ),
            "accreditationPrice"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Price for accreditation '),
                "value"=> $res['accreditationPrice'],
                "unit"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', ' rub')
            ),
            "contacts"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Contacts'),
                "rows"=> [
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Organization'),
                        "value"=> $res['name'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'City'),
                        "value"=> $res['cityName'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Address'),
                        "value"=> $res['address'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Site'),
                        "value"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'go to the website'),
                        "link"=> $res['site'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Phone'),
                        "value"=> $res['phone'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Email'),
                        "value"=>  $res['email'],
                        "link"=> "mailto:{$res['email']}",
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Build stands'),
                        "value"=>  $res['standsConstructionContact'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Catering'),
                        "value"=> $res['cateringContact'],
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Accreditation of builders'),
                        "value"=>  $res['accreditationContact']
                    ),
                    array(
                        "name"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Advertising'),
                        "value"=>  $res['advertisingContact'],
                    )
                ]
            ),
            "map"=> array(
                "header"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'Map'),
                "coordinats"=> array(
                    "latitude"=>  $res['latitude'],
                    "longitude"=>  $res['longitude'],
                ),
                "hint"=> Yii::t('ExhibitionComplexModule.exhibitionComplex', 'hint')
            ),
            "image"=> $filePath,
            "desc"=>  $res['description'],
            "user"=> array(
                "auth"=> !Yii::app()->user->isGuest,
            )
        );

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($exhibitionComplex) . ');';
    }

}