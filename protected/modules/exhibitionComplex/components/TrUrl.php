<?php

Class TrUrl extends CComponent{

    public function init(){

    }

    public function getTrUrl($route, $params){

        if(empty($params['urlVenueName'])){
            return NULL;
        }

        $trExhibitionComplex = TrExhibitionComplex::model()->findByAttributes(array('shortUrl' => $params['urlVenueName']));

        if(empty($trExhibitionComplex)){
            return NULL;
        }

        $tr = TrExhibitionComplex::model()->findByAttributes(array('trParentId' => $trExhibitionComplex->trParentId, 'langId' => $params['langId']));

        if(empty($tr)){
            return NULL;
        }

        $params['urlVenueName'] = $tr->shortUrl;

        return Yii::app()->createUrl($route, $params);
    }
}