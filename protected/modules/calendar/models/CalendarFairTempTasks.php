<?php

/**
 * This is the model class for table "{{calendarfairtemptasks}}".
 *
 * The followings are the available columns in table '{{calendarfairtemptasks}}':
 * @property integer $id
 * @property integer $fairId
 * @property string $uuid
 * @property string $parent
 * @property string $group
 * @property string $name
 * @property string $desc
 * @property integer $duration
 * @property string $date
 * @property string $color
 * @property integer $completeButton
 * @property integer $first
 * @property integer $action
 * @property string $editDate
 */
class CalendarFairTempTasks extends ShardedAR {

	const ACTION_DELETE = -1;
	const ACTION_UPDATE = 0;
	const ACTION_INSERT = 1;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('fairId, uuid, name, group', 'required'),
			array('fairId, duration, completeButton, first, action', 'numerical', 'integerOnly'=>true),
			array('uuid, parent, group', 'length', 'max'=>36),
			array('name', 'length', 'max'=>245),
			array('color', 'length', 'max'=>45),
			array('desc, date', 'safe'),
			array('id, fairId, uuid, parent, group, name, desc, duration, date, color, completeButton, first, action, editDate', 'safe', 'on'=>'search')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fairId' => 'Fair',
			'uuid' => 'Uuid',
			'parent' => 'Parent',
			'group' => 'Group',
			'name' => 'Name',
			'desc' => 'Desc',
			'duration' => 'Duration',
			'date' => 'Date',
			'color' => 'Color',
			'completeButton' => 'Complete Button',
			'first' => 'First',
			'action' => 'Action',
			'editDate' => 'Edit Date'
		);
	}

	/**
	 * @param DateTime $date
	 * @param int $duration
	 * @param DateTime $parentComplete
	 * @return array
	 */
	public static function calculateDates($date = null, $duration = null, $parentComplete = null) {

		$now = date('Y-m-d H:i:s');
		$startDate = $now;
		$endDate = $now;
		if (!$duration) {
			if ($parentComplete) {
				$startDate = $parentComplete;
			}
			$endDate = $date;
		}
		if ($duration > 0) {
			if ($date) {
				$startDate = $date;
			} elseif ($parentComplete) {
				$startDate = $parentComplete;
			}
			$endDate = date('Y-m-d H:i:s', strtotime($startDate . ' +' . $duration . ' days'));
		}

		return [$startDate, $endDate];
	}
}
