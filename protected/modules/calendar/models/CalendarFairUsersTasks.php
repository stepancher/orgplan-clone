<?php

/**
 * This is the model class for table "{{calendarfairuserstasks}}".
 *
 * The followings are the available columns in table '{{calendarfairuserstasks}}':
 * @property string $uuid
 * @property integer $userId
 * @property string $date
 * @property string $completeDate
 * @property string $color
 * @property integer $duration
 * 
 * @property CalendarFairTasks $FairTask
 */
class CalendarFairUsersTasks extends ShardedAR{

	public function primaryKey() {

		return array('uuid', 'userId');
	}

    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'uuid' => array(
                'string',
                'label' => 'uuid',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'userId' => array(
                'integer',
                'label' => 'userId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'taskId' => array(
                'integer',
                'label' => 'userId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'date' => array(
                'datetime',
                'label' => 'date',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'completeDate' => array(
                'datetime',
                'label' => 'completeDate',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'color' => array(
                'string',
                'label' => 'color',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'duration' => array(
                'integer',
                'label' => 'duration',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'FairTask' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'CalendarFairTasks',
                    array(
                        'uuid' => 'id',
                    ),
                ),
            ),
        );
    }

    public function beforeSave()
    {
        if(empty($this->completeDate)){
            $this->completeDate = new CDbExpression('NULL');
        }

        if(empty($this->date)){
            $this->date = new CDbExpression('NULL');
        }

        return parent::beforeSave();
    }

}
