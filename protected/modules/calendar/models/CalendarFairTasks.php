<?php

/**
 * This is the model class for table "{{calendarfairtasks}}".
 *
 * The followings are the available columns in table '{{calendarfairtasks}}':
 * @property integer $id
 * @property integer $fairId
 * @property string $uuid
 * @property string $parent
 * @property string $group
 * @property string $name
 * @property string $desc
 * @property integer $duration
 * @property string $date
 * @property string $color
 * @property integer $completeButton
 * @property integer $first
 */
class CalendarFairTasks extends ShardedAR {

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, uuid', 'required'),
			array('fairId, duration, completeButton, first', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>245),
			array('color', 'length', 'max'=>45),
			array('uuid, parent, group', 'length', 'max'=>36),
			array('desc, date', 'safe'),
			array('id, uuid, parent, group, fairId, name, desc, duration, date, color, completeButton, first', 'safe', 'on'=>'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {

		return array(
			'UsersTasks' => array(self::HAS_MANY, 'CalendarFairUsersTasks', 'uuid'),
            'translate' => array(
				self::HAS_ONE,
				'TrCalendarFairTasks',
				'trParentId',
				'on' => 'translate.langId = "'.Yii::app()->language.'"',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {

		return array(
			'id' => 'ID',
			'uuid' => 'UUID',
			'parent' => 'Родительская задача',
			'group' => 'Группа',
			'fairId' => 'ID выставки',
			'name' => 'Заголовок',
			'desc' => 'Описание',
			'duration' => 'Продолжительность',
			'date' => 'Дата',
			'color' => 'Цвет',
			'completeButton' => 'Кнопка завершить',
			'first' => 'Загружается первый'
		);
	}

	/**
	 * @return string
	 */
	public function getName(){

		$trString = '';

		if(!empty($this->translate->name)){
			$trString = $this->translate->name;
		}

		return $trString;
	}

	/**
	 * @return string
	 */
	public function getDesc(){

		$trString = '';

		if(!empty($this->translate->desc)){
			$trString = $this->translate->desc;
		}

		return $trString;
	}
}
