<?php

/**
 * This is the model class for table "{{calendaruserstasks}}".
 *
 * The followings are the available columns in table '{{calendaruserstasks}}':
 * @property integer $id
 * @property integer $userId
 * @property integer $fairId
 * @property string $parent
 * @property string $name
 * @property string $startDate
 * @property string $endDate
 * @property string $completeDate
 * @property string $desc
 */
class CalendarUsersTasks extends ShardedAR {

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, name, startDate, endDate', 'required'),
			array('userId, fairId', 'numerical', 'integerOnly'=>true),
			array('parent', 'length', 'max'=>45),
			array('name, desc', 'length', 'max'=>245),
			array('completeDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userId, fairId, parent, name, startDate, endDate, completeDate, desc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'fairId' => 'Fair',
			'parent' => 'Parent',
			'name' => 'Name',
			'startDate' => 'Start Date',
			'endDate' => 'End Date',
			'completeDate' => 'Complete Date',
			'desc' => 'Desc',
		);
	}
}
