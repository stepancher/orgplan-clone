<?php

/**
 * This is the model class for table "{{calendarfairdays}}".
 *
 * The followings are the available columns in table '{{calendarfairdays}}':
 * @property integer $id
 * @property integer $fairId
 * @property string $name
 * @property string $startDate
 * @property string $endDate
 * @property string $cssClass
 */
class CalendarFairDays extends ShardedAR {

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('fairId, startDate, endDate', 'required'),
			array('fairId', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>245),
			array('cssClass', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fairId, name, startDate, endDate, cssClass', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {

		return array(
			'id' => 'ID',
			'fairId' => 'Fair',
			'name' => 'Name',
			'startDate' => 'Start Date',
			'endDate' => 'End Date',
			'cssClass' => 'Css Class'
		);
	}
}
