<?php

class CalendarController extends Controller {

	public static function getFair($fairId) {

		if (Yii::app()->user->role == User::ROLE_ORGANIZERS) {
			$fair = Fair::model()->findByPk($fairId);
			if ($fair) {
				$userHasOrganizer = UserHasOrganizer::model()->findByAttributes([
					'userId' => Yii::app()->user->id,
					'organizerId' => $fair->organizerId
				]);
			}
			if (empty($userHasOrganizer)) {
				$fair = [];
			}
		} else {
			$fair = [];
			$myFair = MyFair::model()->findByAttributes([
				'userId' => Yii::app()->user->id,
				'fairId' => $fairId,
				'status' => 1
			]);
			if ($myFair) {
				$fair = Fair::model()->findByPk($fairId);
			}
		}

		return $fair;
	}

	public function actionIndex($fairId = null) {

		if (!in_array(Yii::app()->user->role, [User::ROLE_EXPONENT, User::ROLE_ORGANIZERS, User::ROLE_ADMIN])) {
			$this->redirect(Yii::app()->createUrl('home'));
		}

		$fair = self::getFair($fairId);
		$this->pageTitle = Yii::t('CalendarModule.calendar', 'Calendar view');
		if ($fair) {
			$this->pageTitle .= ' - ' . $fair->name;
		}
		$this->pageTitle .= ' | ' . Yii::app()->name;

		$this->render('index', [
			'fair' => self::getFair($fairId)
		]);
	}

	public function actionAll($fairId = null) {

		if ($fair = self::getFair($fairId)) {
			$attributes = ['fairId' => $fair->id];
		} else {
			if (Yii::app()->user->role == User::ROLE_ORGANIZERS) {
				$userHasOrganizers = UserHasOrganizer::model()->findAllByAttributes([
					'userId' => Yii::app()->user->id
				]);
				$organizers =[];
				foreach ($userHasOrganizers as $userHasOrganizer) {
					$organizers[] = $userHasOrganizer->organizerId;
				}
				$organizerFairs = Fair::model()->findAllByAttributes([
					'organizerId' => $organizers
				]);
				$fairs = [];
				foreach ($organizerFairs as $organizerFair) {
					$fairs[] = $organizerFair->id;
				}
				$attributes = ['fairId' => $fairs];
			} else {
				$myFairs = MyFair::model()->findAllByAttributes([
					'userId' => Yii::app()->user->id,
					'status' => 1
				]);
				$fairs = [];
				foreach ($myFairs as $myFair) {
					$fairs[] = $myFair->fairId;
				}
				$attributes = ['fairId' => $fairs];
			}
		}

//		$calendarTasks = CalendarFairDays::model()->findAllByAttributes(
//			$attributes,
//			['order' => 'startDate']
//		);

        $res = array();
        $i=0;

        if(!empty($attributes["fairId"])){

            $fair = Fair::model()->findByPk($fairId);
            $res[] = array(
                "id"=> $i++,
                "fairId"=> $fair->id,
                "name"=> Yii::t('CalendarModule.calendar', 'Dates of fair running ').$fair->name,
                "startDate"=> $fair->beginDate,
                "endDate"=> date('Y-m-d', strtotime($fair->endDate.' + 1 days')),
                "cssClass"=> "red",
            );

        }

        echo json_encode($res);
	}
}