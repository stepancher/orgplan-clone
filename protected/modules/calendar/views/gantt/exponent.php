<?php
/**
 * @var $this GanttController
 */

$app = Yii::app();
$assetManager = $app->getAssetManager();
$clientScript = $app->getClientScript();

$publishUrl = $assetManager->publish(Yii::getPathOfAlias('application.modules.calendar.assets'), false, -1, YII_DEBUG);
$clientScript->registerCssFile($publishUrl . '/less/global.css');
$clientScript->registerCssFile($publishUrl . '/css/style.css');

$scheduler = $assetManager->publish($app->extensionPath . '/dhtmlxgantt');
$periodPicker = $assetManager->publish($app->extensionPath . '/periodPicker/build');

$clientScript->registerCssFile($scheduler . '/dhtmlxgantt.css');
$clientScript->registerScriptFile($scheduler . '/dhtmlxgantt.js');
$clientScript->registerScriptFile($scheduler . '/ext/dhtmlxgantt_marker.js');
$clientScript->registerScriptFile($scheduler . '/locale/locale_' . $app->language . '.js');

$clientScript->registerCssFile($periodPicker . '/jquery.periodpicker.min.css');
$clientScript->registerCssFile($periodPicker . '/jquery.timepicker.min.css');
$clientScript->registerScriptFile($periodPicker . '/jquery.periodpicker.full.min.js');


?>

<style>
	html, body {
		height: 100%;
	}

	.container-fluid {
		height: calc(100% - 50px);
		position: relative;
	}

	.nav-list__side-left {
		height: calc(100% - 50px) !important;
	}

	.gantt_task_content {
		cursor: pointer;
	}


	.dhx_cal_navline {
		position: relative;
		height: 59px;
		left: 0;
		top: 0;
	}

	.dhx_cal_navline .dhx_cal_tab {
		color: #5780AD;
		font-size: 13px;
		font-weight: bolder;
		padding-top: 0;
		text-decoration: none;
		width: 60px;
		position: absolute;
		font-family: "Segoe UI", Arial;
		top: 14px;
		white-space: nowrap;
		border-radius: 0;
		border: none;
		height: 30px;
		line-height: 30px;
		background: 0 0;
		text-align: center;
		cursor: pointer;
	}

	.dhx_cal_navline .dhx_cal_tab.year {
		width: 70px;
	}

	.dhx_cal_navline .dhx_cal_tab:hover {
		text-decoration: underline;
	}

	.dhx_cal_navline .dhx_cal_tab.active {
		background-color: #5780AD;
		border: none;
		color: #FFF;
		font-weight: lighter;
		text-decoration: none;
		cursor: default;
	}

	.dhx_cal_navline .dhx_cal_tab input[type=radio] {
		display: none;
	}



	.calendar-buttons {
		height: 40px;
	}

	.calendar-buttons .b-button {
		text-decoration: none;
		text-align: center;
		height: 40px;
		float: left;
		margin: 0;
	}

	.calendar-buttons .calendar-button {
		border: 1px solid #d1d1d1;
		border-left: none;
		margin-left: 0;
		color: #505055;
	}

	.calendar-buttons .gantt-button {
		margin-right: 0;
		cursor: default;
	}

	.calendar-buttons .b-button-icon {
		margin-right: 10px;
	}

	.calendar-buttons .create-button {
		width: 140px;
		margin-right: 40px;
		text-align: center;
	}

	.calendar-buttons .create-button:hover {
		text-decoration: none;
	}

	.calendar-buttons .create-button:before {
		display: block;
		font-size: 24px;
		height: 30px;
		width: 40px;
		position: absolute;
		right: 10px;
		top: 0;
		padding-top: 10px;
		background: #8cc152;
	}

	.gantt_marker .gantt_marker_content {
		padding: 2px 5px;
		bottom: 0;
		left: 2px;
	}

	.gantt_add,
	.gantt_grid_head_add {
		background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAJElEQVR42mNgoBXo2R/0HxmPGkDAAHTFxOLhZMBoOqCRAcQCAMmU+oEi72HWAAAAAElFTkSuQmCC');
	}

	.gantt_task_drag.task_right {
		cursor: w-resize;
	}

	.gantt_task_drag.task_left {
		cursor: e-resize;
	}

	.gantt_task_progress_drag {
		cursor: move;
	}

	.gantt_tree_content {
		cursor: default;
	}

	.gantt_task_line:hover .gantt_link_control div {
		display: none;
	}





	.ms-options-wrap,
	.ms-options-wrap * {
		box-sizing: border-box;
	}

	.ms-options-wrap > button:focus,
	.ms-options-wrap > button {
		width: 100%;
		text-align: left;
		border: none;
		outline: none;
		white-space: nowrap;
		color: #515056;
		background-color: #efefef;
		padding: 10px;
		margin: 0;
		cursor: pointer;
		position: relative;
		font-size: 16px;
		letter-spacing: 0;
	}

	.ms-options-wrap > button:hover {
		color: #515056;
		background: #e0e0e0;
	}

	.ms-options-wrap > button:after {
		content: '';
		display: block;
		position: absolute;
		top: 43%;
		right: 15px;
		width: 9px;
		height: 6px;
		overflow: hidden;
		background: url('http://orgplan/assets/cecb2559/img/dropdown/caret-black.png') no-repeat;
	}

	.ms-options-wrap > .ms-options {
		position: absolute;
		left: 0;
		width: 100%;
		margin-top: 1px;
		margin-bottom: 20px;
		background: white;
		z-index: 2000;
		border: 1px solid #aaa;
	}

	.ms-options-wrap > .ms-options:before {
		/*
		display: block;
		position: absolute;
		left: 17px;
		top: 43px;
		overflow: auto;
		content: '';
		z-index: 2;
		background: url('http://orgplan/assets/cecb2559/img/dropdown/mark.png');
		width: 14px;
		height: 8px;
		*/
	}

	.ms-options-wrap > .ms-options > .ms-search {
		padding: 10px;
		position: relative;
	}

	.ms-options-wrap > .ms-options > .ms-search:before {
		content: '';
		display: block;
		position: absolute;
		top: 19px;
		left: 18px;
		width: 21px;
		height: 21px;
		background: url('http://orgplan/assets/cecb2559/img/sprites/icon-sprites-white-21px.png') 0 -148px no-repeat;
	}

	.ms-options-wrap > .ms-options > .ms-search input {
		width: 100%;
		border: none;
		border-radius: 0;
		-o-border-radius: 0;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		line-height: 22px;
		box-shadow: none;
		font-size: 16px;
		color: #505055;
		display: block;
		height: 40px;
		padding-left: 40px;
		background: #e0e0e0;
		margin: 0;
	}

	.ms-options-wrap > .ms-options > .ms-search input::-webkit-input-placeholder {
		line-height: 25px;
		font-size: 14px;
		color: #afafaf;
	}

	.ms-options-wrap > .ms-options > .ms-search input::-moz-placeholder {
		line-height: 25px;
		font-size: 14px;
		color: #afafaf;
	}

	.ms-options-wrap > .ms-options .ms-selectall {
		display: inline-block;
		font-size: .9em;
		text-transform: lowercase;
		text-decoration: none;
	}
	.ms-options-wrap > .ms-options .ms-selectall:hover {
		text-decoration: underline;
	}

	.ms-options-wrap > .ms-options > .ms-selectall.global {
		margin: 4px 5px;
	}

	.ms-options-wrap > .ms-options > ul {
		margin: 0;
		padding: 0 10px 10px;
		list-style-type: none;
	}

	.ms-options-wrap > .ms-options > ul > li.optgroup {
		padding: 5px;
	}
	.ms-options-wrap > .ms-options > ul > li.optgroup + li.optgroup {
		border-top: 1px solid #aaa;
	}

	.ms-options-wrap > .ms-options > ul > li.optgroup .label {
		display: block;
		padding: 5px 0 0 0;
		font-weight: bold;
	}

	.ms-options-wrap > .ms-options > ul label {
		line-height: 18px;
		font-size: 16px;
		position: relative;
		display: block;
		margin: 0;
		padding: 3px 0 3px 15px;
		cursor: pointer;
		color: #515056;
		text-align: left;
	}

	.ms-options-wrap > .ms-options > ul li.selected label,
	.ms-options-wrap > .ms-options > ul label:hover {
		color: #515056;
		background: #efefef;
	}

	.ms-options-wrap > .ms-options > ul input[type="checkbox"] {
		margin-right: 5px;
		position: absolute;
		left: 4px;
		top: 7px;
	}


	.task-label {
		margin: 0;
	}

	.task-input {
		width: 100%;
		margin: 0 !important;
		background: #efefef;
	}




	.period_picker_input,
	.period_picker_input:before {
		width: 100%;
		text-align: left;
		outline: none;
		white-space: nowrap;
		color: #515056;
		background-color: #efefef;
		margin: 0;
		cursor: pointer;
		position: relative;
		font-size: 16px;
		letter-spacing: 0;
		border: none;
		border-radius: 0;
		-o-border-radius: 0;
		-moz-border-radius: 0;
		-webkit-border-radius: 0;
		padding: 9px;
		height: 40px;
		line-height: 22px;
		box-shadow: none;
		font-family: MyriadPro-Light;
	}

	.period_picker_input:hover {
		color: #515056;
		background: #e0e0e0;
	}

	.period_picker_input:before {
		display: none;
	}

	.period_picker_input .icon_calendar {
		display: none;
	}

	.period_picker_input .period_button_text {
		padding: 0;
	}
</style>

<div class="orgplan" style="margin: 0; padding: 0; width: 100%; height: 100%; position: inherit;">

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<?= $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('CalendarModule.calendar', 'Gantt view') . (Yii::app()->user->role == User::ROLE_ORGANIZERS ? ' ('.Yii::t('CalendarModule.calendar', 'Organizer').')' : ''),
				'description' => empty($fair) ? Yii::t('CalendarModule.calendar', 'all fairs') : $fair->name
			)); ?>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12 calendar-buttons">

			<div class="pull-left">
				<span class="b-button d-bg-warning d-text-light gantt-button">
					<?=Yii::t('CalendarModule.calendar', 'Grafic Gantt');?>
				</span>
				<a class="b-button calendar-button" href="<?= Yii::app()->createUrl('calendar/calendar', $fair ? ['fairId' => $fair->id] : []); ?>">
					<?=Yii::t('CalendarModule.calendar', 'Calendar');?>
				</a>
				<!--
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				-->
			</div>

			<div class="pull-right">
				<a class="b-button d-bg-success d-text-light create-button" data-icon="" href="#"><?=Yii::t('CalendarModule.calendar', 'create task');?></a>
			</div>

			<div style="clear: both;"></div>
		</div>
	</div>

	<div class="dhx_cal_navline js-zoom-task">
		<label class="dhx_cal_tab" style="left: 14px;">
			<input name="scales" type="radio" value="day">
			<?=Yii::t('CalendarModule.calendar', 'Day');?>
		</label>
		<label class="dhx_cal_tab" style="left: 75px;">
			<input name="scales" type="radio" value="week">
			<?=Yii::t('CalendarModule.calendar', 'Week');?>
		</label>
		<label class="dhx_cal_tab" style="left: 136px;">
			<input name="scales" type="radio" value="month">
			<?=Yii::t('CalendarModule.calendar', 'Month');?>
		</label>
		<label class="dhx_cal_tab year active" style="left: 211px;">
			<input name="scales" type="radio" checked value="year">
			<?=Yii::t('CalendarModule.calendar', 'Year');?>
		</label>
	</div>

	<div id="gantt_here" style="width: 100%; height: calc(100% - 170px);"></div>

</div>

<div class="modal--sign-up modal hide fade" id="modal-edit-task" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header js-fair-name"></span>
				<span class="text-size-20 js-task-text"></span>
			</div>
		</h3>
	</div>
	<div class="modal-body">
		<div class="row-fluid modal__modal-content">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<div class="cols-row offset-small-l-3 offset-small-r-3">
					<div class="col-md-12">
						<div class="js-start-date" style="font-size: 20px; margin: 10px 0;">
							<?=Yii::t('CalendarModule.calendar', 'Beginning date');?>: <span style="float: right;"></span>
						</div>
						<div class="js-end-date" style="font-size: 20px; margin: 10px 0">
							<?=Yii::t('CalendarModule.calendar', 'Deadline');?>: <span style="float: right;"></span>
						</div>
						<div class="js-complete-date" style="font-size: 20px; margin: 10px 0">
							<?=Yii::t('CalendarModule.calendar', 'Completed');?>: <span style="float: right;"></span>
						</div>
						<div class="terms-of-use js-desc"></div>
						<div style="text-align: center;">
							<button class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-complete-button" type="button" name="complete"><?=Yii::t('CalendarModule.calendar', 'Done');?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal--sign-up modal hide fade" id="modal-user-task" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header">
					<?=Yii::t('CalendarModule.calendar', 'Private task');?>
				</span>
				<span class="text-size-20 js-task-text" data-add-text="<?=Yii::t('CalendarModule.calendar', 'Adding new task');?>">
					<?=Yii::t('CalendarModule.calendar', 'Adding new task');?>
				</span>
			</div>
		</h3>
	</div>
	<div class="modal-body" style="padding: 15px !important;">
		<div class="row-fluid">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<form id="task">
					<div class="cols-row">
						<div class="col-md-12">

							<div class="control-group">
								<label class="task-label control-label" for="parent"><?=Yii::t('CalendarModule.calendar', 'Parent task');?></label>
								<div class="controls">
									<select id="parent" name="parent"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="name"><?=Yii::t('CalendarModule.calendar', 'Task name');?> <span class="required">*</span></label>
								<div class="controls">
									<input class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="<?=Yii::t('CalendarModule.calendar', 'Enter task name');?>" name="name" id="name" type="text">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="start_date"><?=Yii::t('CalendarModule.calendar', 'Period');?> <span class="required">*</span></label>
								<div class="controls js-start-date-wrapper">
									<input id="start_date" name="start_date" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="text">
									<input id="end_date" name="end_date" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="text">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="desc"><?=Yii::t('CalendarModule.calendar', 'Task caption');?></label>
								<div class="controls">
									<textarea style="resize: none; padding: 9px;" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="<?=Yii::t('CalendarModule.calendar', 'Enter task description');?>" name="desc" id="desc"></textarea>
								</div>
							</div>

							<div style="text-align: center; margin: 20px 0 0;">
								<button style="margin: 0; display: none;" class="b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn js-complete-button" type="button" name="complete"><?=Yii::t('CalendarModule.calendar', 'Done');?></button>
							</div>

							<input type="hidden" name="id" id="id" value="">

							<div style="margin: 20px 0 0;">
								<button style="margin: 0; float: left;" class="b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn js-cancel-button" type="button" data-edit-text="<?=Yii::t('CalendarModule.calendar', 'remove');?>" data-add-text="<?=Yii::t('CalendarModule.calendar', 'Cancel');?>"><?=Yii::t('CalendarModule.calendar', 'Cancel');?></button>
								<button style="margin: 0; float: right;" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-submit-button" type="button" data-edit-text="<?=Yii::t('CalendarModule.calendar', 'save');?>" data-add-text="<?=Yii::t('CalendarModule.calendar', 'Add');?>"><?=Yii::t('CalendarModule.calendar', 'Add');?></button>
								<div style="clear: both;"></div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.calendar.assets'), false, -1, YII_DEBUG);
?>
<script type="text/javascript" src="<?=$publishUrl?>/js/jquery.multiselect.js"></script>

<script type="text/javascript">

	gantt.attachEvent("onTaskLoading", function (task) {
		if (!task.color) {
			task.color = '#43ade3';
		}
		if (task.complete) {
			task.progress = 0.9;
			task.textColor = '#454545';
			task.color = '#efefef';
		}
		if (task.type == gantt.config.types.project) {
			task.color = '#378882';
		}

		if (task.type != 'userTask') {
			task.readonly = true;
		}
		return true;
	});

	var organizer = <?= Yii::app()->user->role == User::ROLE_ORGANIZERS ? 1 : 0 ?>;
	var fair = <?= $fair ? $fair->id : 0 ?>;
	var res = {
		data: [],
		links: [],
		colored: []
	};

	function init(zoom) {

		res = {
			data: [],
			links: [],
			colored: []
		};

		var request = $.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/all', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		});

		request.done(function (data) {
			var res_data = [];
			var fairs = [];
			for (var i in data) {
				var iData = data[i];
				var id = iData.uuid;
				var text = iData.name;
				res_data[id] = {
					id: id,
					parent: iData.parent,
					text: text,
					desc: iData.desc,
					fair_name: iData.fairName,
					start_date: new Date(Date.parse(iData.date)),
					end_date: new Date(Date.parse(iData.date)),
					complete_date: new Date(Date.parse(iData.complete)),
					complete_btn: iData.completeButton,
					color: iData.color,
					data: iData,
					open: true
				};

				if (!iData.date && res_data[iData.parent]) {
					res_data[id].start_date = new Date(res_data[iData.parent].end_date.getTime() + 1000);
				}

				if (iData.duration > 0) {
					res_data[id].end_date = new Date(res_data[id].start_date.getTime() + (iData.duration * 24 * 60 * 60 * 1000) - 1000);
				} else if (iData.duration < 0) {
					res_data[id].end_date = res_data[id].start_date;
					res_data[id].start_date = new Date(res_data[id].end_date.getTime() + (iData.duration * 24 * 60 * 60 * 1000) + 1000);
				} else {
					var now = new Date();
					if (res_data[id].start_date.getTime() > now.getTime()) {
						res_data[id].start_date = now;
					}
					if (res_data[iData.parent]) {
						res_data[id].start_date = new Date(res_data[iData.parent].end_date.getTime() + 1000);
						if (res_data[id].end_date.getTime() < res_data[iData.parent].end_date.getTime()) {
							res_data[id].start_date = res_data[iData.parent].start_date;
						}
					}
				}

				if (iData.complete) {
					res_data[id].end_date = new Date(Date.parse(iData.complete));
					res_data[id].text = '<del>' + res_data[id].text + '</del>';
					if (res_data[id].start_date.getTime() > res_data[id].end_date.getTime()) {
						res_data[id].end_date = res_data[id].start_date;
					}
				}

				if (iData.parent) {
					res.links.push({
						id: iData.parent + '-' + id,
						source: iData.parent,
						target: id,
						type: 0
					});
				} else {
					res_data[id].parent = iData.fairId + '-fair';
				}

				fairs[iData.fairId] = {
					id: iData.fairId + '-fair',
					parent: 0,
					text: iData.fairName,
					type: gantt.config.types.project,
					progress: 0,
					open: true
				};

				res.data.push(res_data[id]);
			}
			for (var j in fairs) {
				res.data.push(fairs[j]);
			}
		});

		if (!organizer) {
			var userTasksRequest = $.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/getUserTasks', $fair ? ['fairId' => $fair->id] : []); ?>",
				dataType: "json",
				async: false
			});

			userTasksRequest.done(function (data) {

				for (var i in data) {
					var iData = data[i];
					var id = iData.id + '-ut';

					res.data.push({
						id: id,
						parent: iData.parent,
						text: iData.name,
						desc: iData.desc,
						fair_name: iData.fairName,
						start_date: new Date(Date.parse(iData.startDate)),
						end_date: new Date(Date.parse(iData.endDate)),
						complete_date: new Date(Date.parse(iData.complete)),
						complete_btn: 1,
						color: '#8cc152',
						type: 'userTask',
						open: true
					});
				}
			});
		}

		gantt.config.xml_date = "%Y-%m-%d %H:%i";
		gantt.config.details_on_create = true;
		gantt.config.grid_width = 500;
		gantt.config.sort = true;

		gantt.config.columns = [
			{name: "text", width: "*", tree: true},
			{name: "start_date", width: "81"},
			{name: "end_date", label: "<?=Yii::t('CalendarModule.calendar', 'ending');?>", width: "81"},
			{name: "add", label: "", width: 36}
		];

		var today = new Date();
		gantt.addMarker({start_date: today, css: "today", text: "<?=Yii::t('CalendarModule.calendar', 'Today');?>", title: "<?=Yii::t('CalendarModule.calendar', 'Now');?>"});

		gantt.init("gantt_here");
		gantt.parse(res);
		zoom_tasks(zoom);
	}
	init('year');

	function hideColumns() {
		gantt.config.columns = [];
		gantt.render();
	}

	function showColumns() {
		gantt.config.columns = [
			{name: "text", width: "*", tree: true},
			{name: "start_date", width: "81"},
			{name: "end_date", label: "<?=Yii::t('CalendarModule.calendar', 'ending');?>", width: "81"},
			{name: "add", label: "", width: 36}
		];
		gantt.render();
	}

	function zoom_tasks(node) {
		switch (node) {
			case "day":
				gantt.config.min_column_width = 20;
				gantt.config.scale_unit = "day";
				gantt.config.date_scale = "%j %F";
				gantt.config.subscales = [
					{unit: "hour", step: 1, date: "%H"}
				];
				gantt.config.scale_height = 40;
				break;

			case "week":
				//gantt.config.min_column_width = 20;
				gantt.config.scale_unit = "week";
				gantt.config.date_scale = "#%W <?=Yii::t('CalendarModule.calendar', 'week');?>";
				gantt.config.subscales = [
					{unit: "day", step: 1, date: "%j %M"}
				];
				gantt.config.scale_height = 40;
				break;

			case "month":
				gantt.config.min_column_width = 30;
				gantt.config.scale_unit = "month";
				gantt.config.date_scale = "%F";
				gantt.config.subscales = [
					{unit: "week", step: 1, date: "#%W"}
				];
				gantt.config.scale_height = 40;
				break;

			case "year":
				gantt.config.scale_unit = "year";
				gantt.config.date_scale = "%Y <?=Yii::t('CalendarModule.calendar', 'year');?>";
				gantt.config.subscales = [
					{unit: "month", step: 1, date: "%F"}
				];
				gantt.config.scale_height = 40;
				break;
		}
		gantt.render();
	}

	$('#modal-edit-task').on('click', '.js-complete-button', function () {
		var id = $(this).attr('data-id');
		var task = gantt.getTask(id);
		$.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/complete'); ?>",
			async: false,
			data: {id: task.data.id},
			success: function() {
				$('#modal-edit-task').modal('hide');
				gantt.clearAll();
				init($('.js-zoom-task .active input[type=radio]').val());
			}
		});
	});

	$('.js-zoom-task input[type=radio]').on('change', function() {
		var $this = $(this);
		$('.js-zoom-task').find('.dhx_cal_tab.active').removeClass('active');
		$this.closest('.dhx_cal_tab').addClass('active');
		zoom_tasks($this.val());
	});

	$("a[href='#']").on('click', function() {
		return false;
	});

	var $parent = $("#parent");

	function fStructureRes(notId) {
		$parent.html(fair ? '' : '<option value=""><?=Yii::t('CalendarModule.calendar', 'Empty');?></option>');
		structureRes(0, [], 0, notId);
		$parent.multiselect('reload');
	}

	function structureRes(parent, result, depth, notId) {
		var tab = '- ';
		var pref = '';
		for (var i = 0; i < depth; i++) {
			pref += tab;
		}
		for (var i in res.data) {
			if (res.data[i].parent == parent && res.data[i].id != notId) {
				var text = pref + strip(res.data[i].text);
				$parent.append($("<option></option>").attr("value", res.data[i].id).text(text));
				structureRes(res.data[i].id, result, (depth + 1 - 0), notId);
			}
		}
	}

	function strip(html) {
		var tmp = document.createElement("DIV");
		tmp.innerHTML = html;
		return tmp.textContent || tmp.innerText || "";
	}

	$parent.multiselect({
		placeholder: '<?=Yii::t('CalendarModule.calendar', 'Choose parent task');?>',
		showCheckbox: false,
		maxHeight: 250,
		search: true,
		searchOptions: {
			'default': '<?=Yii::t('CalendarModule.calendar', 'Search');?>'
		},
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
		}
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие 'Добавить' или 'Сохранить'
	$('.js-submit-button').on('click', function() {

		var $userTaskModal = $('#modal-user-task');
		var $userTaskForm = $userTaskModal.find("#task");
		var $name = $userTaskForm.find('#name');
		var $startDate = $userTaskForm.find('#start_date');

		var post = {};

		var errors = false;

		$.each($userTaskForm.serializeArray(), function(i, field) {
			post[field.name] = field.value;
		});

		if (!post.name) {
			$name.closest('.control-group').addClass('error');
			errors = true;
		} else {
			$name.closest('.control-group').removeClass('error');
		}

		if (!post.start_date || !post.end_date) {
			$startDate.closest('.control-group').addClass('error');
			errors = true;
		} else {
			$startDate.closest('.control-group').removeClass('error');
		}

		if (!errors) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/editUserTask'); ?>",
				type: "POST",
				data: post,
				success: function (data) {
					if (data.id) {
						if (gantt.isTaskExists(data.id + '-ut')) {
							gantt.clearAll();
							init($('.js-zoom-task .active input[type=radio]').val());
						} else {
							gantt.addTask({
								id: data.id,
								text: post.name,
								start_date: post.start_date,
								end_date: post.end_date,
								desc: post.desc,
								type: 'userTask',
								complete_btn: 1,
								color: '#8cc152'
							}, post.parent);
						}
						$userTaskModal.modal('hide');
					}
				},
				dataType: 'json'
			});
		}
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на 'Создать задачу'
	$('.create-button').on('click', function() {
		if (!organizer) {

			var $taskText = $('.js-task-text');
			$taskText.text($taskText.attr('data-add-text'));

			fStructureRes(0);

			createPeriodPicker(0, 0);

			$('#modal-user-task').modal('show');
		} else {
			alert('<?=Yii::t('CalendarModule.calendar', 'Adding task by organizer is in progress.');?>');
		}
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на '+'
	gantt.attachEvent("onTaskCreated", function(task) {

		if (!organizer) {
			var $userTask = $('#modal-user-task');

			var $taskText = $userTask.find('.js-task-text');
			$taskText.text($taskText.attr('data-add-text'));

			fStructureRes(0);
			var $parent = $userTask.find('#parent');
			$parent.val(task.parent);
			$parent.multiselect('reload');

			createPeriodPicker(0, 0);

			$userTask.modal('show');
		} else {
			alert('<?=Yii::t('CalendarModule.calendar', 'Adding task by organizer is in progress.');?>');
		}

		return false;
	});

	// ЗАДАЧИ ЭКСПОНЕНТА закрытие модального окна
	$('#modal-user-task').on('hidden.bs.modal', function() {

		var $modal = $(this);
		var $form = $modal.find("#task");
		$form.trigger('reset');
		$form.find("#desc").text('');
		$form.find("#id").val('');
		$parent.multiselect('reload');

		var $cancelBtn = $('.js-cancel-button');
		$cancelBtn.text($cancelBtn.attr('data-add-text'));

		var $submitBtn = $('.js-submit-button');
		$submitBtn.text($submitBtn.attr('data-add-text'));

		$form.find('.control-group.error').removeClass('error');

		//$('.js-complete-button').css('display', 'none');
	});

	var taskDrag = null;
	gantt.attachEvent("onTaskDrag", function(id, mode, task, original) {
		taskDrag = task;
	});

	gantt.attachEvent("onAfterTaskDrag", function(id, mode, e) {
		var task = gantt.getTask(id);
		task.start_date = taskDrag.start_date;
		task.end_date = taskDrag.end_date;
		updateUserTask(task);
	});

	// ЗАДАЧИ ЭКСПОНЕТА обновление задачи
	function updateUserTask(task) {

		if (task.type == "userTask") {
			var post = {
				id: parseInt(task.id),
				start_date: moment(task.start_date).format('YYYY-MM-DD HH:mm:ss'),
				end_date: moment(task.end_date).format('YYYY-MM-DD HH:mm:ss')
			};
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/editUserTask'); ?>",
				type: "POST",
				data: post,
				dataType: 'json'
			});
			gantt.updateTask(task.id);
		}
	}

	gantt.attachEvent("onTaskDblClick", function (id, e) {

		var task = null;
		if (id) {
			task = gantt.getTask(id);
		}

		if (task) {
			if (task.type == 'userTask') {

				if (!organizer) {
					var $parent = $('#parent');
					var $name = $('#name');
					var $desc = $('#desc');
					var $id = $('#id');

					$name.val(task.text);

					$('.js-task-text').text(task.text);

					createPeriodPicker(task.start_date, task.end_date);

					$desc.text(task.desc);

					var $cancelBtn = $('.js-cancel-button');
					$cancelBtn.text($cancelBtn.attr('data-edit-text'));

					var $submitBtn = $('.js-submit-button');
					$submitBtn.text($submitBtn.attr('data-edit-text'));

					//$('.js-complete-button').css('display', '');
					$id.val(parseInt(task.id));
					fStructureRes(task.id);
					$parent.val(task.parent);
					$parent.multiselect('reload');
					$('#modal-user-task').modal('show');
				} else {
					alert('<?=Yii::t('CalendarModule.calendar', 'Adding task by organizer is in progress.');?>');
				}

			} else if (task.type != gantt.config.types.project) {

				var $modal = $('#modal-edit-task');
				var $endDate = $modal.find('.js-end-date');
				var $completeBtn = $modal.find('.js-complete-button');
				var $completeDate = $modal.find('.js-complete-date');

				$modal.find('.js-fair-name').text(task.fair_name);
				$modal.find('.js-task-text').html(task.text);

				var options = {
					year: 'numeric',
					month: 'long',
					day: 'numeric',
					timezone: 'UTC',
					hour: 'numeric',
					minute: 'numeric',
					second: 'numeric'
				};

				$modal.find('.js-start-date span').text(task.start_date.toLocaleString("<?=Yii::app()->language?>", options));
				$endDate.find('span').text(task.end_date.toLocaleString("<?=Yii::app()->language?>", options));
				$modal.find('.js-desc').text('').show();
				if (task.desc) {
					$modal.find('.js-desc').text(task.desc);
				}

				$endDate.show();
				$completeBtn.hide();
				$completeDate.hide();
				if (task.complete) {
					$completeDate.find('span').text(task.complete.toLocaleString("<?=Yii::app()->language?>", options));
					$completeDate.show();
					$endDate.hide();
					$modal.find('.js-desc').hide();
				} else if (task.complete_btn == '1' && !organizer) {
					$completeBtn.attr('data-id', id);
					$completeBtn.data('id', id);
					$completeBtn.show();
				}

				$modal.modal('show');
			}
		}
	});

	gantt.attachEvent("onBeforeLinkAdd", function(id,link) {
		return false;
	});

	function createPeriodPicker(ppStartDate, ppEndDate) {

		var startDate = new Date(ppStartDate);
		var endDate = new Date(ppEndDate);

		if (!ppStartDate) {
			startDate = new Date();
			startDate.setHours(0);
			startDate.setMinutes(0);
			startDate.setSeconds(0);
		}

		if (!ppEndDate) {
			endDate = new Date();
			endDate.setHours(23);
			endDate.setMinutes(55);
			endDate.setSeconds(0);
		}

		var $oldStartDate = $('#start_date');
		var $newStartDate = $oldStartDate.clone();
		$oldStartDate.remove();
		$('.period_picker_input').remove();

		$newStartDate.val(moment(startDate).format('YYYY-MM-DD HH:mm:ss'));

		$('.js-start-date-wrapper').append($newStartDate);

		$newStartDate.periodpicker({
			end: '#end_date',
			lang: 'ru',
			formatDateTime: 'YYYY-MM-DD HH:mm:ss',
			timepicker: true,
			minDate: moment(new Date()).format('YYYY-MM-DD'),
			defaultEndTime: moment(endDate).format('HH:mm'),
			timepickerOptions: {
				hours: true,
				minutes: true,
				seconds: false,
				ampm: false,
				defaultTime: moment(startDate).format('HH:mm'),
				twelveHoursFormat: false,
				steps:[1,5,2,1]
			}
		});

		$('#end_date').val(moment(endDate).format('YYYY-MM-DD HH:mm:ss'));
		$newStartDate.periodpicker('change');
	}

	$('.js-cancel-button').on('click', function() {
		var id = $("#id").val();
		var task = null;
		if (id) {
			task = gantt.getTask(id + '-ut');
		}
		if (task) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/deleteUserTask'); ?>",
				type: "GET",
				data: {id: id},
				dataType: 'json'
			});
			gantt.clearAll();
			init($('.js-zoom-task .active input[type=radio]').val());
		}
		$('#modal-user-task').modal('hide');
	});

</script>