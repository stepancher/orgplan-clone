<?php
/**
 * @var $this GanttController
 */

$app = Yii::app();
$assetManager = $app->getAssetManager();
$clientScript = $app->getClientScript();

$publishUrl = $assetManager->publish(Yii::getPathOfAlias('application.modules.calendar.assets'), false, -1, YII_DEBUG);
$clientScript->registerCssFile($publishUrl . '/less/global.css');
$clientScript->registerCssFile($publishUrl . '/css/gantt/style.css');

$scheduler = $assetManager->publish($app->extensionPath . '/dhtmlxgantt');
$periodPicker = $assetManager->publish($app->extensionPath . '/periodPicker/build');

$clientScript->registerCssFile($scheduler . '/dhtmlxgantt.css');
$clientScript->registerScriptFile($scheduler . '/dhtmlxgantt.js');
$clientScript->registerScriptFile($scheduler . '/ext/dhtmlxgantt_marker.js');
$clientScript->registerScriptFile($scheduler . '/locale/locale_' . $app->language . '.js');

$clientScript->registerCssFile($periodPicker . '/jquery.periodpicker.min.css');
$clientScript->registerCssFile($periodPicker . '/jquery.timepicker.min.css');
$clientScript->registerScriptFile($periodPicker . '/jquery.periodpicker.full.min.js');
?>

<div class="orgplan" style="margin: 0; padding: 0; width: 100%; height: 100%; position: inherit;">

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<?= $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('CalendarModule.calendar', 'Gantt view') . ' (' . Yii::t('CalendarModule.calendar', 'organizer') . ')',
				'description' => empty($fair) ? Yii::t('CalendarModule.calendar', 'all fairs') : $fair->name
			)); ?>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12 calendar-buttons">

			<div class="pull-left">
				<span class="b-button d-bg-warning d-text-light gantt-button"><?=Yii::t('CalendarModule.calendar', 'Gantt view')?></span>
				<a class="b-button calendar-button" href="<?= Yii::app()->createUrl('calendar/calendar', $fair ? ['fairId' => $fair->id] : []); ?>"><?=Yii::t('CalendarModule.calendar', 'Calendar view')?></a>
				<!--
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				<a class="icon b-button b-button-icon d-bg-info d-text-light" data-icon="" href="#"></a>
				-->
			</div>

			<div class="pull-right">
				<a class="b-button d-bg-info d-text-light update-button" style="display: none;" data-icon="" href="#">
					<?=Yii::t('CalendarModule.calendar', 'update tasks')?>
				</a>
				<a class="b-button d-bg-success d-text-light create-button" data-icon="" href="#">
					<?=Yii::t('CalendarModule.calendar', 'create task')?>
				</a>
			</div>

			<div style="clear: both;"></div>
		</div>
	</div>

	<div class="dhx_cal_navline js-zoom-task">
		<label class="dhx_cal_tab" style="left: 14px;">
			<input name="scales" type="radio" value="day">
			<?=Yii::t('CalendarModule.calendar', 'day')?>
		</label>
		<label class="dhx_cal_tab" style="left: 75px;">
			<input name="scales" type="radio" value="week">
			<?=Yii::t('CalendarModule.calendar', 'week')?>
		</label>
		<label class="dhx_cal_tab" style="left: 136px;">
			<input name="scales" type="radio" value="month">
			<?=Yii::t('CalendarModule.calendar', 'month')?>
		</label>
		<label class="dhx_cal_tab year active" style="left: 211px;">
			<input name="scales" type="radio" checked value="year">
			<?=Yii::t('CalendarModule.calendar', 'year')?>
		</label>
	</div>

	<div id="gantt_here" style="width: 100%; height: calc(100% - 170px);"></div>

</div>

<div class="modal--sign-up modal hide fade" id="modal-edit-task" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header">
					<?= empty($fair) ? Yii::t('CalendarModule.calendar', 'all fairs') : $fair->name ?>
				</span>
				<span class="text-size-20 js-task-text" data-add-text="<?=Yii::t('CalendarModule.calendar', 'Adding new task')?>">
					<?=Yii::t('CalendarModule.calendar', 'Adding new task')?>
				</span>
			</div>
		</h3>
	</div>
	<div class="modal-body" style="padding: 15px !important;">
		<div class="row-fluid">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<form id="task">
					<div class="cols-row">
						<div class="col-md-12">

							<div class="control-group">
								<label class="task-label control-label" for="parent"><?=Yii::t('CalendarModule.calendar', 'Parent task')?></label>
								<div class="controls">
									<select id="parent" name="parent"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="group"><?=Yii::t('CalendarModule.calendar', 'from')?></label>
								<div class="controls">
									<select id="group" name="group"></select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="name"><?=Yii::t('CalendarModule.calendar', 'Task name')?> <span class="required">*</span></label>
								<div class="controls">
									<input class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="<?=Yii::t('CalendarModule.calendar', 'input task name')?>" name="name" id="name" type="text">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label required" for="start_date"><?=Yii::t('CalendarModule.calendar', 'date')?> <span class="required">*</span></label>
								<div class="controls js-start-date-wrapper">
									<input id="date" name="date" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="text">
									<input id="duration" name="duration" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" type="number" min="1" step="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="<?=Yii::t('CalendarModule.calendar', 'time')?>">
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="parent"><?=Yii::t('CalendarModule.calendar', 'color')?></label>
								<div class="controls">
									<select id="color" name="color">
										<option value="#43ade3"><?=Yii::t('CalendarModule.calendar', 'blue')?></option>
										<option value="#f6a800"><?=Yii::t('CalendarModule.calendar', 'yellow')?></option>
										<option value="#f96a0e"><?=Yii::t('CalendarModule.calendar', 'orange')?></option>
									</select>
								</div>
							</div>

							<div class="control-group">
								<label class="task-label control-label" for="desc"><?=Yii::t('CalendarModule.calendar', 'desc')?></label>
								<div class="controls">
									<textarea style="resize: none; padding: 9px;" class="task-input d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark" placeholder="<?=Yii::t('CalendarModule.calendar', 'input desc')?>" name="desc" id="desc"></textarea>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label checkbox" style="padding-top: 6px;">
									<?= TbHtml::checkBox('completeButton'); ?>
									<?=Yii::t('CalendarModule.calendar', 'manual')?>
								</label>
							</div>

							<div class="control-group">
								<label class="control-label checkbox" style="padding-top: 6px;">
									<?= TbHtml::checkBox('first'); ?>
									<?=Yii::t('CalendarModule.calendar', 'active')?>
								</label>
							</div>

							<div style="text-align: center; margin: 20px 0 0;">
								<button style="margin: 0; display: none;" class="b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn js-complete-button" type="button" name="complete"><?=Yii::t('CalendarModule.calendar', 'close')?></button>
							</div>

							<input type="hidden" name="id" id="id" value="">

							<div style="margin: 20px 0 0;">
								<button style="margin: 0; float: left;" class="b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn js-cancel-button" type="button" data-edit-text="<?=Yii::t('CalendarModule.calendar', 'remove')?>" data-add-text="<?=Yii::t('CalendarModule.calendar', 'cancel')?>"><?=Yii::t('CalendarModule.calendar', 'cancel')?></button>
								<button style="margin: 0; float: right;" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-submit-button" type="button" data-edit-text="<?=Yii::t('CalendarModule.calendar', 'save')?>" data-add-text="<?=Yii::t('CalendarModule.calendar', 'add')?>"><?=Yii::t('CalendarModule.calendar', 'add')?></button>
								<div style="clear: both;"></div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal--sign-up modal hide fade" id="modal-update-task" style="position: absolute;" role="dialog" tabindex="-1">
	<div class="modal-header">
		<button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button>
		<h3>
			<div class="modal__modal-header">
				<span class="header-offset h1-header">
					<?= empty($fair) ? Yii::t('CalendarModule.calendar', 'all fairs') : $fair->name ?>
				</span>
				<span class="text-size-20">
					<?=Yii::t('CalendarModule.calendar', 'list')?>
				</span>
			</div>
		</h3>
	</div>
	<div class="modal-body" style="padding: 15px !important;">
		<div class="row-fluid">
			<div class="widget-form-view widget-FormView widget-SocialEmailForm">
				<form id="task">
					<div class="cols-row">

						<div class="col-md-12 js-content"></div>

						<div class="col-md-12">
							<div style="text-align: center; margin: 20px 0 0;">
								<button style="margin: 0; display: none;" class="b-button d-bg-warning-dark d-bg-warning--hover d-text-light btn js-complete-button" type="button" name="complete"><?=Yii::t('CalendarModule.calendar', 'close')?></button>
							</div>

							<div style="margin: 20px 0 0;">
								<button style="margin: 0; float: left;" class="b-button d-bg-danger-dark d-bg-danger--hover d-text-light btn js-cancel-button" type="button"><?=Yii::t('CalendarModule.calendar', 'cancel')?></button>
								<button style="margin: 0; float: right;" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn js-submit-button" type="button"><?=Yii::t('CalendarModule.calendar', 'update')?></button>
								<div style="clear: both;"></div>
							</div>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.calendar.assets'), false, -1, YII_DEBUG);
?>
<script type="text/javascript" src="<?=$publishUrl?>/js/jquery.multiselect.js"></script>

<script type="text/javascript">

	gantt.attachEvent("onTaskLoading", function (task) {
		if (!task.color) {
			task.color = '#43ade3';
		}
		if (task.complete) {
			task.progress = 0.9;
			task.textColor = '#454545';
			task.color = '#efefef';
		}
		if (task.type == gantt.config.types.project) {
			task.color = '#378882';
		}

		task.readonly = true;
		/*
		if (task.data && task.data.action == -1) {
			task.readonly = true;
		}
		*/
		return true;
	});

	var fair = <?= $fair ? $fair->id : 0 ?>;
	var res = {
		data: [],
		links: [],
		colored: []
	};

	var $update = $('.update-button');

	function init(zoom) {

		$update.hide();

		res = {
			data: [],
			links: [],
			colored: []
		};

		var request = $.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/all', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		});

		request.done(function (data) {
			var res_data = [];
			var fairs = [];
			for (var i in data) {
				var iData = data[i];
				var id = iData.uuid;
				var text = iData.name;

				res_data[id] = {
					id: id,
					parent: iData.parent,
					text: text,
					desc: iData.desc,
					fair_name: iData.fairName,
					start_date: new Date(Date.parse(iData.date)),
					end_date: new Date(Date.parse(iData.date)),
					complete_date: new Date(Date.parse(iData.complete)),
					complete_btn: iData.completeButton,
					first: iData.first,
					color: iData.color,
					group: iData.group,
					data: iData,
					open: true
				};

				if (!iData.date && res_data[iData.parent]) {
					res_data[id].start_date = new Date(res_data[iData.parent].end_date.getTime());
				}

				if (iData.duration > 0) {
					res_data[id].end_date = new Date(res_data[id].start_date.getTime() + (iData.duration * 24 * 60 * 60 * 1000));
				} else if (iData.duration < 0) {
					res_data[id].end_date = res_data[id].start_date;
					res_data[id].start_date = new Date(res_data[id].end_date.getTime() + (iData.duration * 24 * 60 * 60 * 1000));
				} else {
					var now = new Date();
					if (res_data[id].start_date.getTime() > now.getTime()) {
						res_data[id].start_date = now;
					}
					if (res_data[iData.parent]) {
						res_data[id].start_date = new Date(res_data[iData.parent].end_date.getTime());
						if (res_data[id].end_date.getTime() < res_data[iData.parent].end_date.getTime()) {
							res_data[id].start_date = res_data[iData.parent].start_date;
						}
					}
				}

				if (iData.complete) {
					res_data[id].end_date = new Date(Date.parse(iData.complete));
					res_data[id].text = '<del>' + res_data[id].text + '</del>';
					if (res_data[id].start_date.getTime() > res_data[id].end_date.getTime()) {
						res_data[id].end_date = res_data[id].start_date;
					}
				}

				if (iData.parent) {
					if (res_data[iData.parent] && res_data[iData.parent].data.action == -1) {
						res_data[id].data.action = -1;
					}
					res.links.push({
						id: iData.parent + '-' + id,
						source: iData.parent,
						target: id,
						type: 0
					});
				} else {
					res_data[id].parent = iData.fairId + '-fair';
				}

				if (iData.group != iData.uuid) {
					res.links.push({
						id: iData.group + '-' + iData.uuid,
						source: iData.group,
						target: iData.uuid,
						type: 1,
						color: '#43ade3'
					});
				}

				if (iData.action !== null) {
					res_data[id].text = '<b>' + res_data[id].text + '</b>';
					if (iData.action == -1) {
						res_data[id].text = '<del>' + res_data[id].text + '</del>';
					}
					$update.show();
				}

				fairs[iData.fairId] = {
					id: iData.fairId + '-fair',
					parent: 0,
					text: iData.fairName,
					type: gantt.config.types.project,
					progress: 0,
					open: true
				};

				res.data.push(res_data[id]);
			}
			for (var j in fairs) {
				res.data.push(fairs[j]);
			}
		});

		gantt.config.xml_date = "%Y-%m-%d %H:%i";
		gantt.config.details_on_create = true;
		gantt.config.grid_width = 500;
		gantt.config.sort = true;

		gantt.config.columns = [
			{name: "text", width: "*", tree: true},
			{name: "start_date", width: "81"},
			{name: "end_date", label: "<?=Yii::t('CalendarModule.calendar', 'end')?>", width: "81"},
			{name: "add", label: "", width: 36}
		];

		var today = new Date();
		gantt.addMarker({start_date: today, css: "today", text: "<?=Yii::t('CalendarModule.calendar', 'Today')?>", title: "<?=Yii::t('CalendarModule.calendar', 'now')?>"});

		gantt.init("gantt_here");
		gantt.parse(res);
		zoom_tasks(zoom);
	}
	init('year');

	function hideColumns() {
		gantt.config.columns = [];
		gantt.render();
	}

	function showColumns() {
		gantt.config.columns = [
			{name: "text", width: "*", tree: true},
			{name: "start_date", width: "81"},
			{name: "end_date", label: "<?=Yii::t('CalendarModule.calendar', 'end')?>", width: "81"},
			{name: "add", label: "", width: 36}
		];
		gantt.render();
	}

	function zoom_tasks(node) {
		switch (node) {
			case "day":
				gantt.config.min_column_width = 20;
				gantt.config.scale_unit = "day";
				gantt.config.date_scale = "%j %F";
				gantt.config.subscales = [
					{unit: "hour", step: 1, date: "%H"}
				];
				gantt.config.scale_height = 40;
				break;

			case "week":
				//gantt.config.min_column_width = 20;
				gantt.config.scale_unit = "week";
				gantt.config.date_scale = "#%W <?=Yii::t('CalendarModule.calendar', 'week2')?>";
				gantt.config.subscales = [
					{unit: "day", step: 1, date: "%j %M"}
				];
				gantt.config.scale_height = 40;
				break;

			case "month":
				gantt.config.min_column_width = 30;
				gantt.config.scale_unit = "month";
				gantt.config.date_scale = "%F";
				gantt.config.subscales = [
					{unit: "week", step: 1, date: "#%W"}
				];
				gantt.config.scale_height = 40;
				break;

			case "year":
				gantt.config.scale_unit = "year";
				gantt.config.date_scale = "%Y <?=Yii::t('CalendarModule.calendar', 'year2')?>";
				gantt.config.subscales = [
					{unit: "month", step: 1, date: "%F"}
				];
				gantt.config.scale_height = 40;
				break;
		}
		gantt.render();
	}

	var $taskModal = $('#modal-edit-task');

	$('.js-zoom-task input[type=radio]').on('change', function() {
		var $this = $(this);
		$('.js-zoom-task').find('.dhx_cal_tab.active').removeClass('active');
		$this.closest('.dhx_cal_tab').addClass('active');
		zoom_tasks($this.val());
	});

	$("a[href='#']").on('click', function() {
		return false;
	});

	var $parent = $("#parent");

	function fStructureRes(notId) {
		$parent.html('');
		structureRes(0, [], 0, notId);
		$parent.multiselect('reload');
	}

	function structureRes(parent, result, depth, notId) {
		var tab = '- ';
		var pref = '';
		for (var i = 0; i < depth; i++) {
			pref += tab;
		}
		for (var i in res.data) {
			if (res.data[i].parent == parent && res.data[i].id != notId && (!res.data[i].data || res.data[i].data.action != -1)) {
				var text = pref + strip(res.data[i].text);
				$parent.append($("<option></option>").attr("value", res.data[i].id).text(text));
				structureRes(res.data[i].id, result, (depth + 1 - 0), notId);
			}
		}
	}

	function strip(html) {
		var tmp = document.createElement("DIV");
		tmp.innerHTML = html;
		return tmp.textContent || tmp.innerText || "";
	}

	$parent.multiselect({
		placeholder: '<?=Yii::t('CalendarModule.calendar', 'choose')?>',
		showCheckbox: false,
		maxHeight: 250,
		search: true,
		searchOptions: {
			'default': '<?=Yii::t('CalendarModule.calendar', 'search')?>'
		},
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
		}
	});

	var $group = $("#group");
	function fGroupRes(notId) {
		$group.html('');
		groupRes(0, [], 0, notId);
		$group.multiselect('reload');
	}
	function groupRes(parent, result, depth, notId) {
		var tab = '- ';
		var pref = '';
		var i;
		for (i = 0; i < depth; i++) {
			pref += tab;
		}
		for (i in res.data) {
			var iData = res.data[i];
			if (iData.parent == parent && iData.id != notId && (!res.data[i].data || res.data[i].data.action != -1)) {
				var text = pref + strip(iData.text);
				$group.append($("<option></option>").attr("value", iData.id).text(text));
				groupRes(iData.id, result, (depth + 1 - 0), notId);
			}
		}
	}
	$group.multiselect({
		placeholder: '<?=Yii::t('CalendarModule.calendar', 'choose2')?>',
		showCheckbox: false,
		maxHeight: 250,
		search: true,
		searchOptions: {
			'default': '<?=Yii::t('CalendarModule.calendar', 'search')?>'
		},
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
		}
	});



	var $color = $('#color');
	$color.multiselect({
		placeholder: '<?=Yii::t('CalendarModule.calendar', 'choose3')?>',
		showCheckbox: false,
		maxHeight: 250,
		search: false,
		onOptionClick: function(element, option) {
			$('.ms-options-wrap > .ms-options:visible').hide();
			$(element).multiselect('reload');
			addColorLabel();
		}
	});

	function addColorLabel() {
		$color.closest('.control-group').find('.ms-options-wrap ul li').each(function (indx, element) {
			var $input = $(element).find('input');
			$input.after("<div class='color-label' style='background-color: " + $input.val() + "'>&nbsp;</div>");
		});
	}

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие 'Добавить' или 'Сохранить'
	$taskModal.on('click', '.js-submit-button', function() {

		var $taskForm = $taskModal.find("#task");
		var $name = $taskForm.find('#name');
		var $date = $taskForm.find('#date');

		var post = {};

		var errors = false;

		$.each($taskForm.serializeArray(), function(i, field) {
			post[field.name] = field.value;
		});

		var $completeButton = $taskForm.find('#completeButton');
		if ($completeButton.attr('checked')) {
			post['completeButton'] = $completeButton.val();
		}

		var $first = $taskForm.find('#first');
		if ($first.attr('checked')) {
			post['first'] = $first.val();
		}

		if (!post.name) {
			$name.closest('.control-group').addClass('error');
			errors = true;
		} else {
			$name.closest('.control-group').removeClass('error');
		}

		if (post.date || post.duration > 0) {
			$date.closest('.control-group').removeClass('error');
		} else {
			$date.closest('.control-group').addClass('error');
			errors = true;
		}

		if (!errors) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/editTask'); ?>",
				type: "POST",
				data: post,
				success: function (data) {
					console.log(data);
					if (data.id) {
						gantt.clearAll();
						init($('.js-zoom-task .active input[type=radio]').val());
						$taskModal.modal('hide');
					}
				},
				dataType: 'json'
			});
		}
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на 'Создать задачу'
	$('.create-button').on('click', function() {
		fStructureRes(0);
		fGroupRes(0);
		createPeriodPicker(0);
		addColorLabel();
		$taskModal.modal('show');
	});

	// ЗАДАЧИ ЭКСПОНЕНТА нажатие на '+'
	gantt.attachEvent("onTaskCreated", function(task) {
		fStructureRes(0);
		var $parent = $taskModal.find('#parent');
		$parent.val(task.parent);
		$parent.multiselect('reload');
		fGroupRes(0);
		createPeriodPicker(0);
		addColorLabel();
		$taskModal.modal('show');

		return false;
	});

	// ЗАДАЧИ ЭКСПОНЕНТА закрытие модального окна
	$taskModal.on('hidden.bs.modal', function() {

		var $modal = $(this);
		var $form = $modal.find("#task");
		$form.trigger('reset');
		$form.find("#desc").text('');
		$form.find("#id").val('');
		$form.find('.control-group.error').removeClass('error');

		$parent.multiselect('reload');
		$color.multiselect('reload');

		$('#completeButton').removeAttr('checked');
		$('#first').removeAttr('checked');
		$('.checkbox.checked').removeClass('checked');

		var $cancelBtn = $taskModal.find('.js-cancel-button');
		$cancelBtn.text($cancelBtn.attr('data-add-text'));

		var $submitBtn = $taskModal.find('.js-submit-button');
		$submitBtn.text($submitBtn.attr('data-add-text'));
	});

	var taskDragOriginal = null;
	gantt.attachEvent("onBeforeTaskDrag", function(id, mode, e) {
		var task = gantt.getTask(id);
		taskDragOriginal = {
			start_date: task.start_date,
			end_date: task.end_date
		};
		return true;
	});

	var taskDrag = null;
	gantt.attachEvent("onTaskDrag", function(id, mode, task, original) {
		taskDrag = task;
	});

	gantt.attachEvent("onAfterTaskDrag", function(id, mode, e) {

		var task = gantt.getTask(id);

		console.log(mode);
		console.log([
			taskDragOriginal.start_date,
			taskDragOriginal.end_date
		]);
		console.log([
			taskDrag.start_date,
			taskDrag.end_date
		]);

		if (task.data.date && task.data.duration) {
			task.start_date = taskDrag.start_date;
			task.start_date.setHours(taskDragOriginal.start_date.getHours());
			task.start_date.setMinutes(taskDragOriginal.start_date.getMinutes());
			task.start_date.setSeconds(0);

			task.duration = task.data.duration;
			if (mode == 'resize') {
				var endDate = taskDrag.end_date;
				endDate.setHours(taskDragOriginal.end_date.getHours());
				endDate.setMinutes(taskDragOriginal.end_date.getMinutes());
				endDate.setSeconds(0);

				console.log(task.start_date);
				console.log(endDate);

				task.duration = moment(moment(endDate).format('YYYY-MM-DD')).diff(moment(moment(task.start_date).format('YYYY-MM-DD')), 'days');
			}
			task.end_date = new Date(task.start_date.getTime() + (task.duration * 24 * 60 * 60 * 1000));

			console.log(task);

			gantt.updateTask(task.id);
		}

		if (!task.data.date && taskDragOriginal.start_date != taskDrag.start_date && confirm('<?=Yii::t('agree', 'choose agree')?>')) {
			task.start_date = taskDrag.start_date;
			task.start_date.setHours(0);
			task.start_date.setMinutes(0);
			task.start_date.setSeconds(0);

			task.end_date = taskDrag.end_date;
			task.end_date.setHours(0);
			task.end_date.setMinutes(0);
			task.end_date.setSeconds(0);
		}
		//gantt.updateTask(task.id);
/*
		console.log([
			moment(task.start_date).format('YYYY-MM-DD HH:mm:ss'),
			moment(task.end_date).format('YYYY-MM-DD HH:mm:ss')
		]);
*/
/*
		if (task.type == "userTask") {
			var post = {
				id: parseInt(task.id),
				start_date: moment(task.start_date).format('YYYY-MM-DD HH:mm:ss'),
				end_date: moment(task.end_date).format('YYYY-MM-DD HH:mm:ss')
			};
			$.ajax({
 				// Yii::app()->createUrl('calendar/gantt/editUserTask');
				url: "",
				type: "POST",
				data: post,
				dataType: 'json'
			});
			gantt.updateTask(task.id);
		}
*/
	});

	gantt.attachEvent("onTaskDblClick", function (id, e) {

		var task = null;
		if (id) {
			task = gantt.getTask(id);
			console.log(task);
		}

		if (task && task.type != gantt.config.types.project && (!task.data || task.data.action != -1)) {
			var $parent = $('#parent');
			var $name = $('#name');
			var $desc = $('#desc');
			var $completeButton = $('#completeButton');
			var $first = $('#first');
			var $id = $('#id');

			$name.val(task.data.name);

			$('.js-task-text').html(task.text);

			createPeriodPicker(task.data.date);

			$desc.text(task.data.desc);

			var $cancelBtn = $taskModal.find('.js-cancel-button');
			$cancelBtn.text($cancelBtn.attr('data-edit-text'));

			var $submitBtn = $taskModal.find('.js-submit-button');
			$submitBtn.text($submitBtn.attr('data-edit-text'));

			//$('.js-complete-button').css('display', '');

			$id.val(task.id);

			fStructureRes(task.id);
			$parent.val(task.parent);
			$parent.multiselect('reload');

			fGroupRes(0);
			$group.val(task.group);
			$group.multiselect('reload');

			$('#duration').val(task.data.duration ? task.data.duration : '');

			$color.val(task.color);
			$color.multiselect('reload');
			addColorLabel();

			if (task.complete_btn == 1) {
				$completeButton.attr('checked', 'checked');
				$completeButton.closest('.checkbox').addClass('checked');
			}

			if (task.first == 1) {
				$first.attr('checked', 'checked');
				$first.closest('.checkbox').addClass('checked');
			}

			$taskModal.modal('show');
		}
	});

	gantt.attachEvent("onBeforeLinkAdd", function(id,link) {
		return false;
	});

	function createPeriodPicker(ppStartDate) {
		var $oldStartDate = $taskModal.find('#date');
		var $newStartDate = $oldStartDate.clone();
		$oldStartDate.remove();
		$('.period_picker_input').remove();

		if (!ppStartDate) {
			$newStartDate.val('');
			$('.js-start-date-wrapper').append($newStartDate);
			$newStartDate.periodpicker({
				lang: '<?=Yii::app()->language;?>',
				formatDateTime: 'YYYY-MM-DD HH:mm:ss',
				timepicker: true,
				clearButtonInButton: true,
				norange: true,
				cells: [1, 1],
				timepickerOptions: {
					hours: true,
					minutes: true,
					seconds: false,
					ampm: false,
					defaultTime: '00:00',
					twelveHoursFormat: false,
					steps:[1,5,2,1]
				}
			});
		} else {
			var startDate = new Date(ppStartDate);

			$newStartDate.val(moment(startDate).format('YYYY-MM-DD HH:mm:ss'));
			$('.js-start-date-wrapper').append($newStartDate);
			$newStartDate.periodpicker({
				lang: 'ru',
				formatDateTime: 'YYYY-MM-DD HH:mm:ss',
				timepicker: true,
				clearButtonInButton: true,
				norange: true,
				cells: [1, 1],
				timepickerOptions: {
					hours: true,
					minutes: true,
					seconds: false,
					ampm: false,
					defaultTime: moment(startDate).format('HH:mm'),
					twelveHoursFormat: false,
					steps:[1,5,2,1]
				}
			});

			$newStartDate.periodpicker('change');
		}
	}

	$('.js-cancel-button').on('click', function() {
		var id = $("#id").val();
		var task = null;
		if (id) {
			task = gantt.getTask(id);
		}
		if (task) {
			$.ajax({
				url: "<?= Yii::app()->createUrl('calendar/gantt/deleteTask'); ?>",
				type: "GET",
				data: {id: id},
				success: function (data) {
					console.log(data);
				},
				dataType: 'json'
			});
			gantt.clearAll();
			init($('.js-zoom-task .active input[type=radio]').val());
		}
		$taskModal.modal('hide');
	});

	var $modalUpdate = $('#modal-update-task');

	$('.update-button').on('click', function() {

		$modalUpdate.find('.js-content').html('');

		//var $taskText = $('.js-text');
		//$taskText.text($taskText.attr('data-add-text'));

		$.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/getTempTasks', $fair ? ['fairId' => $fair->id] : []); ?>",
			dataType: "json",
			async: false
		}).done(function (data) {

			var i;
			for (i in data) {
				var iData = data[i];
				console.log(iData);

				if (iData.action == -1) {
					$modalUpdate.find('.js-content').append('<p style="color: red; font-weight: bold;">' + iData.temp_name + '</p>');
				} else if (iData.action == 1) {
					$modalUpdate.find('.js-content').append('<p style="color: green; font-weight: bold;">' + iData.temp_name + '</p>');
				} else {
					$modalUpdate.find('.js-content').append('<p style="font-weight: bold;">' + iData.temp_name + '</p>');
					if (iData.temp_parent != iData.task_parent) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'Parent task')?>:<br>' + iData.task_parent + ' -> ' + iData.temp_parent + '</p>');
					}
					if (iData.temp_group != iData.task_group) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'from')?>:<br>' + iData.task_group + ' -> ' + iData.temp_group + '</p>');
					}
					if (iData.temp_name != iData.task_name) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'Task name')?>:<br>' + iData.task_name + ' -> ' + iData.temp_name + '</p>');
					}
					if (iData.temp_date != iData.task_date) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'date2')?>:<br>' + iData.task_date + ' -> ' + iData.temp_date + '</p>');
					}
					if (iData.temp_duration != iData.task_duration) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'time')?>:<br>' + iData.task_duration + ' -> ' + iData.temp_duration + '</p>');
					}
					if (iData.temp_color != iData.task_color) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'color')?>:<br>' + iData.task_color + ' -> ' + iData.temp_color + '</p>');
					}
					if (iData.temp_desc != iData.task_desc) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'desc2')?>:<br>' + iData.task_desc + ' -> ' + iData.temp_desc + '</p>');
					}
					if (iData.temp_completeButton != iData.task_completeButton) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'manual')?>:<br>' + iData.task_completeButton + ' -> ' + iData.temp_completeButton + '</p>');
					}
					if (iData.temp_first != iData.task_first) {
						$modalUpdate.find('.js-content').append('<p><?=Yii::t('CalendarModule.calendar', 'active')?>:<br>' + iData.task_first + ' -> ' + iData.temp_first + '</p>');
					}
				}

				$modalUpdate.find('.js-content').append('<hr>');
			}

		});

		$modalUpdate.modal('show');
	});

	$modalUpdate.on('click', '.js-cancel-button', function () {
		$modalUpdate.modal('hide');
	});

	$modalUpdate.on('click', '.js-submit-button', function () {
		$.ajax({
			url: "<?= Yii::app()->createUrl('calendar/gantt/updateTasks', $fair ? ['fairId' => $fair->id] : []); ?>",
			//dataType: "json",
			async: false
		}).done(function (data) {
			console.log(data);
			gantt.clearAll();
			init($('.js-zoom-task .active input[type=radio]').val());
			$modalUpdate.modal('hide');
		});
	});

	$('.checkbox').on('click', function(e) {
		var $checkbox = $(this);
		if ($checkbox.hasClass('checked')) {
			$checkbox.removeClass('checked');
			$checkbox.find('input[type=checkbox]').removeAttr('checked');
		} else {
			$checkbox.find('input[type=checkbox]').attr('checked', 'checked');
			$checkbox.addClass('checked');
		}
		e.preventDefault();
	});

</script>