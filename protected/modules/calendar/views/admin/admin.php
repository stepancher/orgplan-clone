<?php
/* @var $this AdminController */
/* @var $model CalendarFairTasks */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#calendar-fair-tasks-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>
	Задачи выставки
	<?= TbHtml::linkButton(
		'Добавить новую задачу',
		[
			'url'   => $this->createUrl('create'),
			'class' => 'js-btn-back-history b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
			'style' => "float: right; white-space: nowrap; width: auto; padding: 10px;"
		]
	); ?>
</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'calendar-fair-tasks-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'parent',
		'group',
		'fairId',
		'name',
		'desc',
		'duration',
		'date',
		'color',
		'completeButton',
		'first',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
