<?php
/* @var $this AdminController */
/* @var $model CalendarFairTasks */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'calendar-fair-tasks-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'parent'); ?>
		<?php echo $form->numberField($model,'parent'); ?>
		<?php echo $form->error($model,'parent'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'group'); ?>
		<?php echo $form->numberField($model,'group'); ?>
		<?php echo $form->error($model,'group'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'fairId'); ?>
		<?php echo $form->numberField($model,'fairId'); ?>
		<?php echo $form->error($model,'fairId'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>245)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'desc'); ?>
		<?php echo $form->textArea($model,'desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'desc'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'duration'); ?>
		<?php echo $form->numberField($model,'duration'); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'color'); ?>
		<?php echo $form->textField($model,'color',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'color'); ?>
	</div>

	<div>
		<?php echo CHtml::activeCheckBox($model,'completeButton'); ?>
		<?php echo CHtml::activeLabel($model,'completeButton'); ?>
		<?php echo $form->error($model,'completeButton'); ?>
	</div>

	<div>
		<?php echo CHtml::activeCheckBox($model,'first'); ?>
		<?php echo CHtml::activeLabel($model,'first'); ?>
		<?php echo $form->error($model,'first'); ?>
	</div>

	<div class="buttons">
		<?= CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
		<?= CHtml::linkButton('Отменить', ['href' => '/calendar/admin/']); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->