<?php

class CalendarModule extends CWebModule {

	public $defaultController = 'gantt';

	public function init() {

            $this->setImport(array(
			'calendar.models.*',
			'calendar.components.*',
            'proposal.components.ShardedAR',
		));
	}

	public function beforeControllerAction($controller, $action) {

		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}
