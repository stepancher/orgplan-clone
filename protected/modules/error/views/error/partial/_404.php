<?php
$app = Yii::app();
$am = $app->getAssetManager();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.error.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/main.css');

$errors = [
    '400', '401', '402',
    '403', '404', '405',
    '406', '407', '408',
    '500', '501', '502',
    '503', '504', '505',
    '506', '507', '508', '509'
];
$this->pageTitle = $code  . ' - Страница не найдена | '.Yii::app()->params['titleName'];
?>

<div class="error-img">
    <img alt="<?= in_array($code, $errors)? $code: '' ?> - Страница не найдена" title="<?= in_array($code, $errors)? $code: '' ?> - Страница не найдена" src="<?= $publishUrl . '/source.gif' ?>">
</div>
<div class="error-text"><h1 class="error-text"><?= in_array($code, $errors)? $code: '' ?></h1></div>
<div class="wrap-center-button">
    <ul class="error-button">
        <li class="error-button-red"><?= CHtml::link('', Yii::app()->createUrl('search/index')) ?></li>
        <li class="error-button-blue"><?= CHtml::link('', Yii::app()->createUrl('search/index')) ?></li>
    </ul>
</div>
