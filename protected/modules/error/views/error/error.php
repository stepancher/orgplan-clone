<?php
/** @var $this SiteController */
/** @var $error array */
/** @var integer $code */

$renderError = $this->renderPartial('partial/_404', array('code' => $code), true);
$errors = [
    '400', '401', '402',
    '403', '404', '405',
    '406', '407', '408',
    '500', '501', '502',
    '503', '504', '505',
    '506', '507', '508', '509'
];
?>
<h2>
    <?php if (in_array($code, $errors)):
        echo $renderError;
        ?>
    <?php else: ?>
        <?= Yii::t('system', 'Error') ?>&nbsp;
        <?= $code; ?>
    <?php endif ?>
</h2>


