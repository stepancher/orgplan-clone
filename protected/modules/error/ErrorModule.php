<?php

class ErrorModule extends CWebModule {

    public function init() {

        $this->setImport(array(
            'application.error.views.*',
        ));
    }
}
