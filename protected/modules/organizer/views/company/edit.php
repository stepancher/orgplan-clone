<?php
/**
 * @var OrganizerInfo $model
 * @var string $image
 */

$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.organizer.assets'), false, -1, YII_DEBUG);

//$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
    'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
    CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');

$cs->registerCssFile($publishUrl . '/less/global.css');
?>

<?php Yii::app()->request->isAjaxRequest ? '' : $this->renderPartial('/_tabs') ?>

<div>
    <div class="orgplan organizer-profile">
        <div class="cols-row">
            <div class="col-md-8 col-sm-12">
                <?= $this->renderPartial('//layouts/partial/_header', array(
                    'header' => Yii::t('OrganizerModule.organizer', 'Profile'),
                    'description' => [
                        CHtml::link(
                            Yii::t('OrganizerModule.organizer', 'Company'),
                            Yii::app()->createUrl('organizer/company')
                        ),
                        Yii::t('OrganizerModule.organizer', 'Company edit')
                    ]
                )) ;?>
            </div>
        </div>
    </div>

    <div class="cols-row orgplan">
        <div class="col-md-8 col-sm-12">
            <form class="orgplan-form" action="<?= $this->createUrl('/organizer/company/edit') ?>" method="post" enctype="multipart/form-data">

                <h2><?= Yii::t('OrganizerModule.organizer', 'Information about company:'); ?></h2>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Name'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[name]" value="<?= $model->name ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Full name'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[fullName]" value="<?= $model->fullName ?>">
                    </div>
                </div>

                <h2><?= Yii::t('OrganizerModule.organizer', 'Post address:'); ?></h2>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Postcode'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postPostcode]" value="<?= $model->postPostcode ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Country'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postCountry]" value="<?= $model->postCountry ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Region'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postRegion]" value="<?= $model->postRegion ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'City'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postCity]" value="<?= $model->postCity ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Street'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postStreet]" value="<?= $model->postStreet ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Phone'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postPhone]" value="<?= $model->postPhone ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Site'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[postSite]" value="<?= $model->postSite ?>">
                    </div>
                </div>

                <h2><?= Yii::t('OrganizerModule.organizer', 'Legal address:'); ?></h2>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Postcode'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[legalPostcode]" value="<?= $model->legalPostcode ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Country'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[legalCountry]" value="<?= $model->legalCountry ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Region'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[legalRegion]" value="<?= $model->legalRegion ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'City'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[legalCity]" value="<?= $model->legalCity ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Street'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[legalStreet]" value="<?= $model->legalStreet ?>">
                    </div>
                </div>

                <h2><?= Yii::t('OrganizerModule.organizer', 'Requisites:'); ?></h2>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Settlement account RUR'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesSettlementAccountRUR]" value="<?= $model->requisitesSettlementAccountRUR ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Settlement account EUR'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesSettlementAccountEUR]" value="<?= $model->requisitesSettlementAccountEUR ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Settlement account USD'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesSettlementAccountUSD]" value="<?= $model->requisitesSettlementAccountUSD ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Bank'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesBank]" value="<?= $model->requisitesBank ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'BIC'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesBic]" value="<?= $model->requisitesBic ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'ITN'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesItn]" value="<?= $model->requisitesItn ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'IEC'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesIec]" value="<?= $model->requisitesIec ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Correspondent account'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[requisitesCorrespondentAccount]"  value="<?= $model->requisitesCorrespondentAccount ?>">
                    </div>
                </div>

                <h2><?= Yii::t('OrganizerModule.organizer', 'Sign rules:'); ?></h2>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Full name'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[signRulesName]" value="<?= $model->signRulesName ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Position'); ?></label>
                    <div class="controls col-md-8">
                        <input type="text" name="OrganizerInfo[signRulesPosition]" value="<?= $model->signRulesPosition ?>">
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <?= CHtml::link(
                            Yii::t('OrganizerModule.organizer', 'Back'),
                            Yii::app()->createUrl('organizer/company'),
                            [
                                'class' => 'btn btn-orange profile-back-button'
                            ]
                        ); ?>
                        <input type="submit" class="btn btn-green" value="<?=Yii::t('OrganizerModule.organizer', 'save')?>">
                    </div>
                </div>
                <?php
                //@TODO create iframe here and ajax loading picture
                ?>
                <!--<iframe src="<?= $this->createUrl('/site/image', ['img' => time()]) ?>" frameborder="0"></iframe>-->
                <input id="file" type="file" name="OrganizerInfo[file][]" style="display: none;" />
            </form>
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="row">
                <div class="picture logotype">
                    <img src="<?= $image ?>" alt="">
                    <i class="btn btn-green upload-picture"></i></div>
            </div>
        </div>

    </div> <!-- .cols-row -->
</div>

<script>
    $('.upload-picture').click(function () {
        $('#file').trigger('click');
    });
</script>