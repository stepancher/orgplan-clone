<?php
/**
 * @var OrganizerInfo $model
 * @var string $image
 */

$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.organizer.assets'), false, -1, YII_DEBUG);

//$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
    'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
    CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');

$cs->registerCssFile($publishUrl . '/less/global.css');
?>

<?php Yii::app()->request->isAjaxRequest ? '' : $this->renderPartial('/_tabs') ?>

<div class="orgplan organizer-profile">
    <div class="cols-row">
        <div class="col-md-8 col-sm-12">
            <?= $this->renderPartial('//layouts/partial/_header', array(
                'header' => Yii::t('OrganizerModule.organizer', 'Profile'),
                'description' => Yii::t('OrganizerModule.organizer', 'Company')
            )) ;?>
        </div>

        <div class="col-md-4 col-sm-12">
            <a href="<?= Yii::app()->createUrl('organizer/company/edit'); ?>" class="btn btn-orange pull-right" data-content-target="company-edit">
                <?= Yii::t('OrganizerModule.organizer', 'Edit profile'); ?>
            </a>
        </div>
    </div>
<div>

<div class="orgplan">
    <div class="cols-row">
        <div class="col-md-8">
            <div class="col-md-12">
                <h2><?= Yii::t('OrganizerModule.organizer', 'Information about company:'); ?></h2>
                <table class="table two-cols">
                    <tbody>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Name'); ?></td>
                        <td><?= $model->name ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Full name'); ?></td>
                        <td><?= $model->fullName ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <h2><?= Yii::t('OrganizerModule.organizer', 'Post address:'); ?></h2>

                <table class="table two-cols">
                    <tbody>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Postcode'); ?></td>
                        <td><?= $model->postPostcode ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Country'); ?></td>
                        <td><?= $model->postCountry ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Region'); ?></td>
                        <td><?= $model->postRegion ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'City'); ?></td>
                        <td><?= $model->postCity ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Street'); ?></td>
                        <td><?= $model->postStreet ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Phone'); ?></td>
                        <td><?= $model->postPhone ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Site'); ?></td>
                        <td><?= $model->postSite ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <h2><?= Yii::t('OrganizerModule.organizer', 'Legal address:'); ?></h2>

                <table class="table two-cols">
                    <tbody>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Postcode'); ?></td>
                        <td><?= $model->legalPostcode ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Country'); ?></td>
                        <td><?= $model->legalCountry ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Region'); ?></td>
                        <td><?= $model->legalRegion ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'City'); ?></td>
                        <td><?= $model->legalCity ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Street'); ?></td>
                        <td><?= $model->legalStreet ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <h2><?= Yii::t('OrganizerModule.organizer', 'Requisites:'); ?></h2>

                <table class="table two-cols">
                    <tbody>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Settlement account RUR'); ?></td>
                        <td><?= $model->requisitesSettlementAccountRUR ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Settlement account EUR'); ?></td>
                        <td><?= $model->requisitesSettlementAccountEUR ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Settlement account USD'); ?></td>
                        <td><?= $model->requisitesSettlementAccountUSD ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Bank'); ?></td>
                        <td><?= $model->requisitesBank ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'BIC'); ?></td>
                        <td><?= $model->requisitesBic ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'ITN'); ?></td>
                        <td><?= $model->requisitesItn ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'IEC'); ?></td>
                        <td><?= $model->requisitesIec ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">

                <h2><?= Yii::t('OrganizerModule.organizer', 'Permissions to sign:'); ?></h2>

                <table class="table two-cols">
                    <tbody>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Full name'); ?></td>
                        <td><?= $model->signRulesName ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('OrganizerModule.organizer', 'Position'); ?></td>
                        <td><?= $model->signRulesPosition ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

        <div class="col-md-4">
            <div class="row">
                <img src="<?= $image ?>" class="picture logotype pull-right">
            </div>
        </div>
    </div>
</div>