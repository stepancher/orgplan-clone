<?php $this->widget('application.modules.organizer.components.SimpleTabs', array(
    'base' => 'fair-redactor',
    'fixed' => true,
    'enableAjax' => false,
    'contentType' => 'json',
    'onClick' => 'js:function(o) {
        if("$tab" in o && !o.$tab.hasClass("active")) {
            $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").show();
        } else if ("$tab" in o && o.$tab.hasClass("active")){
        
        } else {
            $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").show();
        }
    }',
    'afterUpdate' => 'js:function(o) {
        changePositionDateRegion();
        $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").hide();
    }',
    'tabs' => array_filter(array(
            array(
                'active' => Yii::app()->controller->id == 'company' ? true : false,
                'label' => Yii::t('OrganizerModule.organizer', 'Company'),
                'target' => 'company',
                'url' => Yii::app()->createUrl('/organizer/company'),
                'pageTitle' => $this->pageTitle,
            ),
            array(
                'active' => Yii::app()->controller->id == 'contact' ? true : false,
                'label' => Yii::t('OrganizerModule.organizer', 'Team'),
                'target' => 'aboutMe',
                'url' => Yii::app()->createUrl('/organizer/contact'),
                'pageTitle' => $this->pageTitle,
            ),
            array(
                'active' => Yii::app()->controller->id == 'exponent' ? true : false,
                'label' => Yii::t('OrganizerModule.organizer', 'Exponent'),
                'target' => 'aboutMe',
                'url' => Yii::app()->createUrl('/organizer/exponent'),
                'pageTitle' => $this->pageTitle,
            ),
        )
    )));
?>