<?php
/**
 * @var OrganizerContact $model
 */

$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.organizer.assets'), false, -1, YII_DEBUG);

//$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
    'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
    CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');

$cs->registerCssFile($publishUrl . '/less/global.css');
?>

<?php Yii::app()->request->isAjaxRequest ? '' : $this->renderPartial('/_tabs') ?>

<div>

    <div class="orgplan organizer-profile">

        <div class="cols-row">
            <div class="col-md-8 col-sm-12">
                <?= $this->renderPartial('//layouts/partial/_header', array(
                    'header' => Yii::t('OrganizerModule.organizer', 'Profile'),
                    'description' => [
                        CHtml::link(
                            Yii::t('OrganizerModule.organizer', 'Team'),
                            Yii::app()->createUrl('organizer/contact')
                        ),
                        Yii::t('OrganizerModule.organizer', $model->isNewRecord ? 'New worker' : 'Edit worker')
                    ]
                )) ;?>
            </div>
        </div>

        <div class="cols-row">
            <div class="col-md-8 col-sm-12">
                <form class="orgplan-form" method="post" action="<?= $this->createUrl('/organizer/contact/edit', ['contactId' => $model->id]) ?>" enctype="multipart/form-data">

                    <div class="control-group">
                        <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Position'); ?></label>
                        <div class="controls col-md-8">
                            <input type="text" name="OrganizerContact[position]" value="<?= $model->position ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Surname'); ?></label>
                        <div class="controls col-md-8">
                            <input type="text" name="OrganizerContact[surname]" value="<?= $model->surname ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'First name'); ?></label>
                        <div class="controls col-md-8">
                            <input type="text" name="OrganizerContact[name]" value="<?= $model->name ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Patronymic'); ?></label>
                        <div class="controls col-md-8">
                            <input type="text" name="OrganizerContact[patronymic]" value="<?= $model->patronymic ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Phone'); ?></label>
                        <div class="controls col-md-8">
                            <input type="text" name="OrganizerContact[phone]" value="<?= $model->phone ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Email'); ?></label>
                        <div class="controls col-md-8">
                            <input type="text" name="OrganizerContact[email]" value="<?= $model->email ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls col-md-offset-4 col-md-8">
                            <label for="OrganizerContact_administrator" class="checkbox">
                                <input type="checkbox" id="OrganizerContact_administrator" class="map-request" name="OrganizerContact[administrator]" <?= $model['administrator'] ? 'checked' : '' ?> value="1">
                                <?= Yii::t('OrganizerModule.organizer', 'Administrator'); ?>
                            </label>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <?= CHtml::link(
                                Yii::t('OrganizerModule.organizer', 'Back'),
                                Yii::app()->createUrl('organizer/contact'),
                                [
                                    'class' => 'btn btn-orange profile-back-button'
                                ]
                            ); ?>
                            <input type="submit" class="btn btn-green" value="<?=Yii::t('OrganizerModule.organizer', 'save')?>">
                        </div>
                    </div>
                    <input id="file" type="file" name="OrganizerContact[file][]" accept="image/*" style="display: none;" />
                </form>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="row">

                </div>
                <div class="row">
                    <div class="picture avatar">
                        <img class="js-avatar" src="<?= $image ?>" data-default="<?= $image ?>" alt="avatar">
                        <i class="btn btn-green upload-picture"></i></div>
                </div>
            </div>

        </div> <!-- .cols-row -->
    </div>

    <script>
        $('.upload-picture').click(function () {
            $('#file').trigger('click');
        });

        $('input[type=submit]').on('click', function() {
            var $name = $('input[name="OrganizerContact[name]"]');
            if ($name.val() == '') {
                $name.closest('.control-group').addClass('error');
                return false;
            }
        });

        $('input[name="OrganizerContact[name]"]').on('change', function () {
            var $this = $(this);
            var $controlGroup = $(this).closest('.control-group');
            if ($this.val() == '') {
                $controlGroup.addClass('error');
            } else {
                $controlGroup.removeClass('error');
            }
        });

        $('#file').on('change', function() {
            var $img = $('.js-avatar');
            if (window.FileReader) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $img.attr("src", e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            } else {
                $img.attr("src", $img.attr('data-default'));
            }
        });
    </script>
</div>