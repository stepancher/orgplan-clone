<?php
/**
 * @var OrganizerContact $model
 */

$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.organizer.assets'), false, -1, YII_DEBUG);

//$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
    'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
    CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/remove.js');

$cs->registerCssFile($publishUrl . '/less/global.css');
?>

<?php Yii::app()->request->isAjaxRequest ? '' : $this->renderPartial('/_tabs') ?>

<div>

    <div class="orgplan organizer-profile">
        <div class="cols-row">
            <div class="col-md-8 col-sm-12">
                <?= $this->renderPartial('//layouts/partial/_header', array(
                    'header' => Yii::t('OrganizerModule.organizer', 'Profile'),
                    'description' => Yii::t('OrganizerModule.organizer', 'Team')
                )) ;?>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="row">
                    <a href="<?= Yii::app()->createUrl('organizer/contact/edit'); ?>" class="btn btn-green pull-right" data-content-target="team-edit">
                        <?= Yii::t('OrganizerModule.organizer', 'Add worker'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="cols-row orgplan">
        <div class="col-md-8">
            <div class="row"></div>
            <ul class="contacts list-unstyled">
                <?php foreach ($models as $i => $model): ?>
                    <li class="row" data-remove="<?= $model->id ?>">
                        <div class="col-xs-1 avatar" style="background-image: url('<?= $model->getImage($publishUrl); ?>');"></div>
                        <div class="col-xs-6">
                            <span class="name"><?= $model->name ?></span><span class="surname"><?= $model->surname ?></span>
                            <span class="position"><?= $model->position ?></span>
                        </div>
                        <div class="col-xs-4">
                            <span class="phone"><?= $model->phone ?></span>
                            <span class="email"><?= $model->email ?></span>
                        </div>
                        <div class="col-xs-1">
                            <div class="wrap">
                                <a href="<?= Yii::app()->createUrl('/organizer/contact/edit', ['contactId' => $model->id]); ?>" class="btn edit" data-icon=""></a>
                                <a href="javascript://" class="btn remove js-remove-item" data-remove-target="<?= $model->id ?>" data-icon=""></a>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php
        $this->widget('bootstrap.widgets.TbModal', array(
            'id' => 'modal-remove',
            'closeText' => '<i class="icon icon--white-close"></i>',
            'htmlOptions' => array(
                'class' => 'modal--sign-up modal-danger'
            ),
            'header' => '<span class="header-offset h1-header">' . Yii::t('OrganizerModule.organizer', 'warning') . '</span>',
            'content' => '
                    <span>' . Yii::t('OrganizerModule.organizer', 'remove') . '</span>
                    <button class="btn btn-orange js-modal-ok">' . Yii::t('OrganizerModule.organizer', 'ok') . '</button>
                    <button class="btn btn-orange js-modal-cancel">' . Yii::t('OrganizerModule.organizer', 'cancel') . '</button>
                    '
        ));
        ?>

    </div>

<script type="text/javascript">
    new removeItem({
        lang: '<?=Yii::app()->getLanguage();?>',
        route: "<?= Yii::app()->createUrl('/organizer/contact/delete'); ?>",
        key: 'contactId'
    });
</script>

</div>