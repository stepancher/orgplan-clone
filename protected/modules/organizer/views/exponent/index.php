<?php
/**
 * @var array $models
 * @var OrganizerExponent $model
 */

$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.organizer.assets'), false, -1, YII_DEBUG);

//$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
	'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
	CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/remove.js');

$cs->registerCssFile($publishUrl . '/less/global.css');
?>

<?php Yii::app()->request->isAjaxRequest ? '' : $this->renderPartial('/_tabs') ?>

<div class="orgplan organizer-profile">
	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<?= $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('OrganizerModule.organizer', 'Profile'),
				'description' => Yii::t('OrganizerModule.organizer', 'Exponent')
			)) ;?>
		</div>

		<div class="col-md-4 col-sm-12">
			<div class="row">
				<a target="_blank" href="<?= Yii::app()->createUrl('organizer/exponent/edit'); ?>" class="btn btn-green pull-right" data-content-target="team-edit">
					<?= Yii::t('OrganizerModule.organizer', 'Add exponent'); ?>
				</a>
				<a href="<?=Yii::app()->createUrl('proposal/proposal/downloadExcel', array('reportId' => Report::REPORT_EXPONENTS_PROPOSAL))?>" class="btn btn-orange pull-right" style="margin-top:20px;">
					<?=Yii::t('OrganizerModule.organizer', 'download');?>
				</a>
			</div>
		</div>
	</div>
</div>

	<div class="orgplan cols-row" style="margin: 0;">
		<div class="col-md-12" style="padding: 0;">
			<h2><?= Yii::t('OrganizerModule.organizer', 'Exponents:'); ?></h2>

			<?php $this->widget('bootstrap.widgets.TbGridView', array(
				'id' => 'grid',
				'rowHtmlOptionsExpression' => 'array("data-remove"=>$data->id)',
				'columns' => array(
					'id',
					'email' => [
						'name'   => 'email',
						'header' => Yii::t('OrganizerModule.organizer', 'post')
					],
					'name' => [
						'name'   => 'name',
						'header' => Yii::t('OrganizerModule.organizer', 'Company name')
					],
					'bill' => [
						'name'   => 'bill',
						'header' => Yii::t('OrganizerModule.organizer', '№ bill')
					],
					'stand' => [
						'name'   => 'stand',
						'header' => Yii::t('OrganizerModule.organizer', '№ stand')
					],
					'square' => [
						'name'   => 'square',
						'header' => Yii::t('OrganizerModule.organizer', 'square')
					],
					'user.contact.phone' => [
						'name'   => 'user.contact.phone',
						'header' => Yii::t('OrganizerModule.organizer', 'phone')
					],
					'user.contact.post' => [
						'name'   => 'user.contact.post',
						'header' => Yii::t('OrganizerModule.organizer', 'position')
					],
					'user.fullName' => array(
						'name' => 'user.fullName',
						'header' => Yii::t('OrganizerModule.organizer', 'fullName')
					),

					array(
						'type' => 'raw',
						'headerHtmlOptions' => [
							'style' => "min-width: 70px;"
						],
						'htmlOptions' => [
							'style' => "padding: 0;"
						],
						'value' => function($data) {
							return '<a target="_blank" class="btn edit" style="float: left;" data-icon="" title="" href="' . Yii::app()->createUrl("organizer/exponent/edit", ["exponentId"=>$data->id]) . '"></a>
							<a class="btn remove js-remove-item" style="float: left;" data-remove-target="' . $data->id . '" data-icon="" title="" ></a>';
						}
					),
				),
				'dataProvider' => $model->searchOrganizerExponent(),
				'filter' => $model,
				'ajaxUpdate' => false
			)); ?>

		<?php
			$this->widget('bootstrap.widgets.TbModal', array(
				'id' => 'modal-remove',
				'closeText' => '<i class="icon icon--white-close"></i>',
				'htmlOptions' => array(
					'class' => 'modal--sign-up modal-danger'
				),
				'header' => '<span class="header-offset h1-header">' . Yii::t('OrganizerModule.organizer', 'warning') . '</span>',
				'content' => '
					<span>' . Yii::t('OrganizerModule.organizer', 'remove') . '</span>
					<button class="btn btn-default js-modal-ok">' . Yii::t('OrganizerModule.organizer', 'accept deleting') . '</button>
					<button class="btn btn-orange js-modal-cancel">' . Yii::t('OrganizerModule.organizer', 'cancel') . '</button>
				'
			));
		?>

	</div>



<script type="text/javascript">
	new removeItem({
		lang: '<?=Yii::app()->getLanguage();?>',
		route: "<?= Yii::app()->createUrl('/organizer/exponent/delete'); ?>",
		key: 'exponentId'
	});
</script>