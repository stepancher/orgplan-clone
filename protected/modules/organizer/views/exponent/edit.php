<?php
/**
 * @var OrganizerExponent $model
 */

$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.organizer.assets'), false, -1, YII_DEBUG);

//$cs->registerCssFile($publishUrl . '/css/tabs/style.css');
$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
	'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
	CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/remove.js');

$cs->registerCssFile($publishUrl . '/less/global.css');
?>

<?php Yii::app()->request->isAjaxRequest ? '' : $this->renderPartial('/_tabs') ?>

<div class="orgplan organizer-profile">
	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<?= $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('OrganizerModule.organizer', 'Profile'),
				'description' => [
					CHtml::link(
						Yii::t('OrganizerModule.organizer', 'Exponent'),
						Yii::app()->createUrl('organizer/exponent')
					),
					Yii::t('OrganizerModule.organizer', $model->isNewRecord ? 'New exponent' : 'Edit exponent')
				]
			)) ;?>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<form class="orgplan-form" method="post" action="">

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Email'); ?></label>
					<div class="controls col-md-8">
						<input class="js-email-validation" type="text" placeholder="<?= Yii::t('OrganizerModule.organizer', 'Enter exponents email'); ?>" name="OrganizerExponent[email]" value="<?= $model->email ?>">
						<p class="error"></p>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Manager'); ?></label>
					<div class="controls col-md-8">
						<?= Yii::app()->controller->widget('application.modules.organizer.components.ListAutoComplete', [
							'name' => 'OrganizerExponent[managerId]',
							'model' => $model,
							'attribute' => 'managerId',
							'selectedItems' => [$model->managerId],
							'multiple' => false,
							'autoComplete' => false,
							'ajaxAutoComplete' => false,
							'showNotEmptyLabel' => false,
							'showLabel' => false,
							'label' => \Yii::t('OrganizerModule.organizer', 'Choose'),
							'htmlOptions' => [
								'labelOptions' => [
									'class' => 'd-bg-input d-bg-tab--focus d-bg-tab--hover d-text-dark'
								],
							],
							'itemOptions' => [
								//@TODO find managers by id of organizer!!! (instead of exponent->organizerId)
								'data' => CHtml::listData(OrganizerContact::model()->findAllByAttributes(array('organizerId' => $organizerId, 'administrator' => 0)), 'id', function($el){
									return $el->getFullName().' ('.$el->email.')';
								}),
								'text' => 'name',
							]
						], true);?>
						<p class="error"></p>
					</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'exponent language'); ?></label>
					<div class="controls col-md-8">
						<?= Yii::app()->controller->widget('application.modules.organizer.components.ListAutoComplete', [
							'name' => 'OrganizerExponent[lang]',
							'model' => $model,
							'attribute' => 'lang',
							'selectedItems' => [$model->lang],
							'multiple' => false,
							'autoComplete' => false,
							'ajaxAutoComplete' => false,
							'showNotEmptyLabel' => false,
							'showLabel' => false,
							'label' => \Yii::t('OrganizerModule.organizer', 'Choose'),
							'htmlOptions' => [
								'labelOptions' => [
									'class' => 'd-bg-input d-bg-tab--focus d-bg-tab--hover d-text-dark'
								],
							],
							'itemOptions' => [
								//@TODO move it into application component (get languages list())
								'data' => array_combine(Fair::getLanguageList(), Fair::getLanguageList()),
								'text' => 'name',
							]
						], true);?>
						<p class="error"></p>
					</div>
				</div>

				<h2><?= Yii::t('OrganizerModule.organizer', 'Company information:'); ?></h2>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Company name'); ?></label>
					<div class="controls col-md-8">
						<input class="js-require-validation" type="text" name="OrganizerExponent[name]" value="<?= CHtml::encode($model->name); ?>">
						<p class="error"></p>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', '№ of bill'); ?></label>
					<div class="controls col-md-8">
						<input class="js-require-validation" type="text" name="OrganizerExponent[bill]" value="<?= $model->bill ?>">
						<p class="error"></p>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', '№ of stand'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[stand]" value="<?= $model->stand ?>">
						<p class="error"></p>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('OrganizerModule.organizer', 'Stand square'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[square]" value="<?= $model->square ?>">
						<p class="error"></p>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<?= CHtml::link(
							Yii::t('OrganizerModule.organizer', 'Back'),
							Yii::app()->createUrl('organizer/exponent'),
							[
								'class' => 'btn btn-orange profile-back-button'
							]
						); ?>
						<input type="submit" class="btn btn-green" value="<?=Yii::t('OrganizerModule.organizer', 'save')?>">
					</div>
				</div>
			</form>
		</div>

	</div> <!-- .cols-row -->

	<script>
		$('input[type=submit]').on('click', function() {
			var $form = $('form.orgplan-form');
			$form.find('.js-require-validation').trigger('change');
			$form.find('.js-email-validation').trigger('change');

			var $errors = $form.find('.control-group.error');
			if ($errors.length > 0) {
				return false;
			}
		});

		$('.js-require-validation').on('change', function () {
			var $this = $(this);
			var $controlGroup = $(this).closest('.control-group');
			var $error = $controlGroup.find('.error');

			if ($this.val() == '') {
				$controlGroup.addClass('error');
				$error.text('<?=Yii::t('OrganizerModule.organizer', 'require')?>');
			} else {
				$controlGroup.removeClass('error');
				$error.text('');
			}
		});

		$('.js-email-validation').on('change', function () {
			var $this = $(this);
			var $controlGroup = $(this).closest('.control-group');
			var $error = $controlGroup.find('.error');

			if ($this.val() == '') {
				$controlGroup.addClass('error');
				$error.text('<?=Yii::t('OrganizerModule.organizer', 'require')?>');
			} else if (!validateEmail($this.val())) {
				$controlGroup.addClass('error');
				$error.text('<?=Yii::t('OrganizerModule.organizer', 'invalid')?>');
			} else if (!validateEmailOnServer($this.val())) {
				$controlGroup.addClass('error');
				$error.text('<?=Yii::t('OrganizerModule.organizer', 'already_use')?>');
			} else {
				$controlGroup.removeClass('error');
				$error.text('');
			}
		});

		function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		function validateEmailOnServer(email) {

			var result = false;

			$.ajax({
				type: 'POST',
				url: '<?= Yii::app()->createUrl('organizer/exponent/ajaxCheckEmail', ['exponentId'=>$model->id]); ?>',
				data: {email: email},
				success: function(data) {
					result = data.result;
				},
				error: function(error) {
					console.log('error', error);
				},
				async: !1,
				dataType: 'json'
			});

			return result;
		}
	</script>
</div>


