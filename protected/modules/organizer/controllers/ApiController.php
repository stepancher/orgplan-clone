<?php

class ApiController extends Controller {

    protected $authenticated = false;

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function actionAuth($SystemId, $UserName, $Password) {

        return true;
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        return true;
    }

    /**
     * @param integer $organizerId
     * @return bool
     * @soap
     */
    public function actionGetOrganizerExists($organizerId){
        $result = FALSE;

        if(
            is_numeric($organizerId) &&
            !empty($organizerId) &&
            !empty($organizer = Organizer::model()->findByPk($organizerId))
        ){
            $result = TRUE;
        }

        echo json_encode($result);
    }

    /**
     * @param integer $organizerId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetOrganizerProperties($organizerId, array $properties){

        if(!empty($organizer = Organizer::model()->findByPk($organizerId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($organizer->{$name}) &&
                    !empty($organizer->{$name})
                ){
                    $res[$name] = $organizer->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param integer $organizerId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetOrganizerAdminsProperties($organizerId, array $properties){

        $organizerAdmins = OrganizerContact::model()->findAllByAttributes(array(
            'organizerId' => $organizerId,
            'administrator' => 1,
        ));

        $result = array();
        if(!empty($organizerAdmins)){
            foreach($organizerAdmins as $organizerAdmin){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($organizerAdmin->{$name}) &&
                        !empty($organizerAdmin->{$name})
                    ){
                        $res[$name] = $organizerAdmin->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    /**
     * @param integer $contactId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetContactProperties($contactId, array $properties){

        if(!empty($organizerContact = OrganizerContact::model()->findByPk($contactId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($organizerContact->{$name}) &&
                    !empty($organizerContact->{$name})
                ){
                    $res[$name] = $organizerContact->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param integer $organizerContactId
     * @param string $publishUrl
     * @return mixed
     * @soap
     */
    public function actionGetImage($organizerContactId, $publishUrl){

        $organizerContact = OrganizerContact::model()->findByPk($organizerContactId);

        if(!empty($organizerContact)){
            $result = $organizerContact->getImage($publishUrl);
        }else{
            $result = '';
        }

        echo json_encode($result);
    }

    /**
     * @param integer $contactId
     * @return bool
     * @soap
     */
    public function actionGetContactExists($contactId){
        echo json_encode(!empty(OrganizerContact::model()->findByPk($contactId)));
    }

    /**
     * @param integer $organizerId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetOrganizerTeamsContactProperties($organizerId, array $properties){

        $criteriaTeam = new CDbCriteria;
        $criteriaTeam->addCondition('organizerId = ' . $organizerId);
        $criteriaTeam->limit = 3;

        $contactTeams = OrganizerContact::model()->findAll($criteriaTeam);

        $result = array();
        if(!empty($contactTeams)){

            foreach($contactTeams as $contactTeam){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($contactTeam->{$name}) &&
                        !empty($contactTeam->{$name})
                    ){
                        $res[$name] = $contactTeam->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    /**
     * @param integer $fairId
     * @param integer $exponentId
     * @param array $postData
     * @return array
     * @soap
     */
    public function actionAddNewExponent($fairId, $exponentId = NULL, array $postData = array()){

        if(
            Yii::app()->user->isGuest ||
            Yii::app()->user->role != User::ROLE_ORGANIZERS ||
            !MyFair::isOrganizerOfFair($fairId)
        ){
            echo json_encode('fail');
        }

        $fair = Fair::model()->findByPk($fairId);

        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fair->id));

        $FHO = FairHasOrganizer::model()->findByAttributes(array('fairId' => $fairId));

        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(array(
            'userId' => Yii::app()->user->getId(),
            'organizerId' => $FHO->organizerId,
        ));

        if (empty($userHasOrganizer)) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
            echo json_encode('empty user has organizer');
            exit;
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = NULL;

        if (is_numeric($exponentId) && !empty($exponentId)) {
            $model = OrganizerExponent::model()->findByAttributes([
                'organizerId' => $userHasOrganizer->organizerId,
                'id' => $exponentId,
            ]);
        }


        if ($model === NULL) {
            $model = new OrganizerExponent();
            $user = new User;
            $contact = new Contact;
            $myFair = new MyFair;
        } else {
            $user = User::model()->findByPk($model->userId);
            if(!empty($user)){
                $contact = Contact::model()->findByPk($user->contactId);
                $myFair = MyFair::model()->findByAttributes(['userId' => $user->id]);
            }
        }

        if ($user === NULL) {
            $user = new User();
        }

        if (empty($contact)) {
            $contact = new Contact();
        }

        if (empty($myFair)) {
            $myFair = new MyFair();
        }

        if (!empty($postData)) {

            $fairName = '';

            if (!empty($fair) && !empty($fn = $fair->getTrNameByLang(Yii::app()->language))) {
                $fairName = $fn;
            }

            $transaction = Yii::app()->db->beginTransaction();

            $contact->companyName = $postData['name'];
            $contact->email = $postData['email'];

            $isNewContact = $contact->isNewRecord;

            if (!$contact->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'User contacts not saved'));
                echo json_encode('Failed to save contact');
            }

            $user->role = User::ROLE_EXPONENT;
            $user->login = NULL;
            $user->password = $user->password;
            $user->contactId = $contact->id;
            $user->tariffId = 1;
            $user->active = 1;
            $user->banned = 0;

            if ($isNewContact) {
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                $count = mb_strlen($chars);

                for ($i = 0, $result = ''; $i < 10; $i++) {
                    $index = rand(0, $count - 1);
                    $result .= mb_substr($chars, $index, 1);
                }

                $password = $result;
                $user->password = password_hash($password, PASSWORD_DEFAULT);
            }

            if (!$user->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'User not saved'));
                echo json_encode('failed to save user');
            }

            $model->attributes = $postData;
            $model->organizerId = $userHasOrganizer->organizerId;
            $model->userId = $user->id;

            if (!$model->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Exponent not saved'));
                echo json_encode('fairled to save oe');
            }

            $myFair->userId = $user->id;
            $myFair->fairId = $fair->id;
            $myFair->status = MyFair::STATUS_ACTIVE;

            if (!isset($_GET['exponentId'])) {
                $myFair->createdAt = date('Y-m-d H:i:s');
                $myFair->updatedAt = date('Y-m-d H:i:s');
            } else {
                $myFair->updatedAt = date('Y-m-d H:i:s');
            }

            if (!$myFair->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Fair not moved in My fairs'));
                echo json_encode('failed to save my fair');
            }

            $transaction->commit();
            if ($isNewContact) {
                $currentLanguage = Yii::app()->language;
                if(!empty($model->lang)){
                    Yii::app()->setLanguage($model->lang);
                }
                Yii::import('application.modules.notification.models.Notification');

                $notification = Notification::saveByObject(
                    $userHasOrganizer->organizerId,
                    $model,
                    Notification::TYPE_EXPONENT_REGISTRATION,
                    array(
                        '{email}' => $contact->email,
                        '{fairName}' => $fairName,
                    ),
                    Yii::app()->user->id,
                    false,
                    [
                        'email' => $contact->email,
                        'password' => '******',
                    ]
                );

                $logo = $fair->getMailLogoPath();

                Yii::app()->consoleRunner->run(
                    'notification createdExponentSendEmail --email=' .
                    $contact->email .
                    ' --password=' . $password .
                    ' --notificationId=' . $notification->id .
                    ' --logo='.$logo .
                    ' --lang='.Yii::app()->language.' --fairId='.$fair->id,
                    true
                );

                if(!empty($model->lang)){
                    Yii::app()->setLanguage($currentLanguage);
                }
            }

        }

        echo json_encode(array(
            'modelId' => $model->id,
            'organizerId' => $userHasOrganizer->organizerId,
            'contactId' => $contact->id,
            'myFairId' => $myFair->id,
        ));
    }


    /**
     * @param integer $fairId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetOrganizersPropertiesByFair($fairId, array $properties){
        $fhos = FairHasOrganizer::model()->findAllByAttributes(array('fairId' => $fairId));

        $organizersProperties = array();
        foreach($fhos as $fho){

            if(!empty($organizer = Organizer::model()->findByPk($fho->organizerId))){
                $res = [];

                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($organizer->{$name}) &&
                        !empty($organizer->{$name})
                    ){
                        $res[$name] = $organizer->{$name};
                    }
                }

                $properties = $res;
            }

            $organizersProperties[] = $properties;
        }

        echo json_encode($organizersProperties);
    }

}