<?php

/**
 * Class ContactController
 */

class ExponentController extends Controller
{
    public function actionIndex() {

        if(!empty(Yii::app()->session->get('myFairViewId'))){
            $fair = Fair::model()->findByPk(Yii::app()->session->get('myFairViewId'));

            $this->redirect('/'.$fair->proposalVersion.Yii::app()->createUrl('organizer/exponent'));
        }else{
            $this->redirect(Yii::app()->createUrl('/home'));
        }

        /** @var Fair $fair */
        if(empty($fair = Fair::model()->findByPk(Yii::app()->session->get('myFairViewId')))){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fair->id));

        Yii::import('application.modules.proposal.models.Report');
        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes([
            'userId' => Yii::app()->user->id
        ]);
        if ($userHasOrganizer === NULL) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = new OrganizerExponent('searchOrganizerExponent');
        if (isset($_GET['OrganizerExponent'])) {
            $model->attributes = $_GET['OrganizerExponent'];
        }

        $this->render('index', array(
            'model' => $model
        ));

    }

    public function actionEdit($exponentId = NULL) {

        /** @var Fair $fair */
        if(empty($fair = Fair::model()->findByPk(Yii::app()->session->get('myFairViewId')))){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fair->id));

        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(['userId' => Yii::app()->user->getId()]);

        if ($userHasOrganizer === NULL) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = NULL;

        if (is_numeric($exponentId)) {
            $model = OrganizerExponent::model()->findByAttributes([
                'organizerId' => $userHasOrganizer->organizerId,
                'id' => $_GET['exponentId'],
            ]);
        }

        if ($model === NULL) {
            $model = new OrganizerExponent;
            $user = new User;
            $contact = new Contact;
            $myFair = new MyFair;
        } else {
            $user = User::model()->findByPk($model->userId);
            if(!empty($user)){
                $contact = Contact::model()->findByPk($user->contactId);
                $myFair = MyFair::model()->findByAttributes(['userId' => $user->id]);
            }
        }

        if ($user === NULL) {
            $user = new User;
        }

        if (empty($contact)) {
            $contact = new Contact;
        }

        if (empty($myFair)) {
            $myFair = new MyFair;
        }

        if (isset($_POST['OrganizerExponent'])) {

            $fairName = '';

            if (!empty($fair) && !empty($fn = $fair->getTrNameByLang(Yii::app()->language))) {
                $fairName = $fn;
            }

            $transaction = Yii::app()->db->beginTransaction();

            $contact->companyName = $_POST['OrganizerExponent']['name'];
            $contact->email = $_POST['OrganizerExponent']['email'];

            $isNewContact = $contact->isNewRecord;

            if (!$contact->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'User contacts not saved'));
                $this->redirect(Yii::app()->createUrl('/organizer/exponent', array('langId' => Yii::app()->language)));
            }

            $user->role = User::ROLE_EXPONENT;
            $user->login = NULL;
            $user->password = $user->password;
            $user->contactId = $contact->id;
            $user->tariffId = 1;
            $user->active = 1;
            $user->banned = 0;

            if ($isNewContact) {
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                $count = mb_strlen($chars);

                for ($i = 0, $result = ''; $i < 10; $i++) {
                    $index = rand(0, $count - 1);
                    $result .= mb_substr($chars, $index, 1);
                }

                $password = $result;
                $user->password = password_hash($password, PASSWORD_DEFAULT);
            }

            if (!$user->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'User not saved'));
                $this->redirect(Yii::app()->createUrl('/organizer/exponent', array('langId' => Yii::app()->language)));
            }

            $model->attributes = $_POST['OrganizerExponent'];
            $model->organizerId = $userHasOrganizer->organizerId;
            $model->userId = $user->id;

            if (!$model->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Exponent not saved'));
                $this->redirect(Yii::app()->createUrl('/organizer/exponent', array('langId' => Yii::app()->language)));
            }

            $myFair->userId = $user->id;
            $myFair->fairId = $fair->id;
            $myFair->status = MyFair::STATUS_ACTIVE;

            if (!isset($_GET['exponentId'])) {
                $myFair->createdAt = date('Y-m-d H:i:s');
                $myFair->updatedAt = date('Y-m-d H:i:s');
            } else {
                $myFair->updatedAt = date('Y-m-d H:i:s');
            }

            if (!$myFair->save()) {
                $transaction->rollback();
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Fair not moved in My fairs'));
                $this->redirect(Yii::app()->createUrl('/organizer/exponent', array('langId' => Yii::app()->language)));
            }

            $transaction->commit();
            if ($isNewContact) {
                $currentLanguage = Yii::app()->language;
                if(!empty($model->lang)){
                    Yii::app()->setLanguage($model->lang);
                }
                Yii::import('application.modules.notification.models.Notification');

                $notification = Notification::saveByObject(
                    $userHasOrganizer->organizerId,
                    $model,
                    Notification::TYPE_EXPONENT_REGISTRATION,
                    array(
                        '{email}' => $contact->email,
                        '{fairName}' => $fairName,
                    ),
                    Yii::app()->user->id,
                    false,
                    [
                        'email' => $contact->email,
                        'password' => '******',
                    ]
                );

                $logo = Yii::app()->getBaseUrl(true).$fair->getMailLogoPath();

                Yii::app()->consoleRunner->run(
                    'notification createdExponentSendEmail --email=' .
                    $contact->email .
                    ' --password=' . $password .
                    ' --notificationId=' . $notification->id .
                    ' --logo='.$logo .
                    ' --lang='.Yii::app()->language.' --fairId='.$fair->id,
                    true
                );

                if(!empty($model->lang)){
                    Yii::app()->setLanguage($currentLanguage);
                }
            }

            $this->redirect(Yii::app()->createUrl('/organizer/exponent', array('langId' => Yii::app()->language)));
        }
        
        $this->render('edit', [
            'model' => $model,
            'organizerId' => $userHasOrganizer->organizerId,
            'contact' => $contact,
            'myFair' => $myFair,
        ]);
    }

    function actionDelete($exponentId) {

        /** @var Fair $fair */
        if(empty($fair = Fair::model()->findByPk(Yii::app()->session->get('myFairViewId')))){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fair->id));

        OrganizerExponent::model()->deleteByPk($exponentId);
        $this->redirect(Yii::app()->createUrl('/organizer/exponent', array('langId' => Yii::app()->language)));
    }

    public function actionAjaxGetList() {

        /** @var Fair $fair */
        if(empty($fair = Fair::model()->findByPk(Yii::app()->session->get('myFairViewId')))){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fair->id));

        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException('404');
        }

        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes([
            'userId' => Yii::app()->user->id
        ]);

        if (!$userHasOrganizer) {
            throw new CHttpException('403');
        }

        $result['page'] = 1;
        if (!empty($_POST['page']) && intval($_POST['page']) > 1) {
            $result['page'] = intval($_POST['page']);
        }

        $result['onPage'] = 30;
        if (!empty($_POST['onPage']) && intval($_POST['onPage'])) {
            $result['onPage'] = intval($_POST['onPage']);
        }

        $attributes['organizerId'] = $userHasOrganizer->organizerId;
        $result['count'] = intval(OrganizerExponent::model()->countByAttributes($attributes));

        $organizerExponents = OrganizerExponent::model()->findAllByAttributes($attributes, [
            'limit'  => $result['onPage'],
            'offset' => ($result['page'] - 1) * $result['onPage'],
            'with' => ['user'],
        ]);

        $result['exponents'] = [];

        $result['exponents'] = array_map(function($organizerExponent){
            return array(
                'email' => $organizerExponent->email,
                'name' => $organizerExponent->name,
                'bill' => $organizerExponent->bill,
                'id' => $organizerExponent->id,
                'userId' => $organizerExponent->userId,
                'organizerId' => $organizerExponent->organizerId,
                'stand' => $organizerExponent->stand,
                'square' => $organizerExponent->square,
                'managerId' => $organizerExponent->managerId,
                'user' => array(
                    'contact' => array(
                        'phone' => !empty($organizerExponent->user->contact->phone)?$organizerExponent->user->contact->phone:'',
                        'post' => !empty($organizerExponent->user->contact->post)?$organizerExponent->user->contact->post:'',
                    ),
                    'fullName' => !empty($organizerExponent->user->fullName)?$organizerExponent->user->fullName:'',
                ),
            );
        }, $organizerExponents);

        echo json_encode($result);
    }

    public function actionAjaxCheckEmail() {

        /** @var Fair $fair */
        if(empty($fair = Fair::model()->findByPk(Yii::app()->session->get('myFairViewId')))){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fair->id));

        if (empty($_POST['email'])) {
            throw new CHttpException('404');
        }
        $email = $_POST['email'];

        $condition = [];
        if (!empty($_GET['exponentId']) && $exponentId = intval($_GET['exponentId'])) {
            $exponent = OrganizerExponent::model()->findByPk($exponentId);
            if ($exponent) {
                $user = User::model()->findByPk($exponent->userId);
                if ($user) {
                    $condition = [
                        'condition' => 'id != :id',
                        'params' => [':id' => $user->contactId]
                    ];
                }
            }
        }
        $count = Contact::model()->countByAttributes(['email' => $email], $condition);

        echo json_encode(['result' => $count == 0]);
    }
}
