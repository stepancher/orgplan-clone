<?php

/**
 * Class CompanyController
 */

class CompanyController extends Controller
{
    public function actionIndex() {
        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(['userId' => Yii::app()->user->getId()]);

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        if ($userHasOrganizer === NULL) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
            return $this->renderPartial('');
        }

        $model = OrganizerInfo::model()->findByAttributes(['organizerId' => $userHasOrganizer->organizerId]);

        if ($model === NULL) {
//            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Не найдена информация об организаторе'));
            $model = new OrganizerInfo;
        }

        $publishUrl = Yii::app()->getAssetManager()->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
        $image = $model->getImage($publishUrl, '240');

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('index', ['model' => $model, 'image' => $image]);
        } else {
            $this->render('index', ['model' => $model, 'image' => $image]);
        }
    }

    public function actionEdit() {
        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(['userId' => Yii::app()->user->getId()]);

        if ($userHasOrganizer === NULL) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = OrganizerInfo::model()->findByAttributes(['organizerId' => $userHasOrganizer->organizerId]);

        if ($model === NULL) {
//            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Не найдена информация об организаторе'));
            $model = new OrganizerInfo;
        }

        $publishUrl = Yii::app()->getAssetManager()->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
        $image = $model->getImage($publishUrl, '240');

        if (isset($_POST['OrganizerInfo']) && !empty($_POST)) {
            $model->attributes = $_POST['OrganizerInfo'];
            $model->organizerId = $userHasOrganizer->organizerId;

            if ($model->save()) {
                $image = CUploadedFile::getInstancesByName('OrganizerInfo[file]');
                $file = new ObjectFile;

                $file->setFileNamePrefix('240');
                $file->type = ObjectFile::TYPE_ORGANIZER_INFO_LOGOS;
                $file->ext = ObjectFile::EXT_IMAGE;
                $file->setOwnerId($model->id);

                if (isset($image[0])) {
                    $file->resizeAndSaveImage($image[0], $model, 240);
                }

            } else {
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Organizer info not saved'));
            }

            $this->redirect($this->createUrl('/organizer', array('langId' => Yii::app()->language)));
        }

        $this->render('edit', ['model' => $model, 'image' => $image]);
    }
}
