<?php

/**
 * Class ContactController
 */

class ContactController extends Controller
{
    public function actionIndex() {
        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(['userId' => Yii::app()->user->getId()]);

        if ($userHasOrganizer === NULL) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $criteria = new CDbCriteria;
        $criteria->addCondition('organizerId = ' . $userHasOrganizer->organizerId);
        $criteria->order = 'administrator DESC';
        $OrganizerContacts = OrganizerContact::model()->findAll($criteria);

        if (empty($OrganizerContacts)) {
//            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Не найдены контакты организатора'));
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('index', ['models' => $OrganizerContacts]);
        } else {
            $this->render('index', ['models' => $OrganizerContacts]);
        }
    }

    public function actionEdit() {
        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(['userId' => Yii::app()->user->getId()]);

        if ($userHasOrganizer === NULL) {
            Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Отсутствует связанный организатор'));
        }

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        $model = NULL;

        if (isset($_GET['contactId']) && is_numeric($_GET['contactId'])) {
            $model = OrganizerContact::model()->findByAttributes([
                'organizerId' => $userHasOrganizer->organizerId,
                'id' => $_GET['contactId'],
            ]);
        }
        if ($model === NULL) {
            $model = new OrganizerContact;
        }

        $publishUrl = Yii::app()->getAssetManager()->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
        $image = $model->getImage($publishUrl);

        if (isset($_POST['OrganizerContact'])) {
            $model->attributes = $_POST['OrganizerContact'];
            $model->organizerId = $userHasOrganizer->organizerId;
            if (empty($_POST['OrganizerContact']['administrator'])) {
                $model->administrator = 0;
            }

            if ($model->save()) {
                $image = CUploadedFile::getInstancesByName('OrganizerContact[file]');

                if (isset($image[0])) {
                    ObjectFile::createFile(
                        $image[0],
                        $model,
                        ObjectFile::TYPE_ORGANIZER_CONTACT_AVATARS,
                        ObjectFile::EXT_IMAGE,
                        true,
                        true,
                        [
                            'sizes' => [
                                36 => 36
                            ],
                            'prefix' => '36',
                        ]);
                }
            } else {
                Yii::app()->user->setFlash('warning', Yii::t('OrganizerModule.organizer', 'Contact not saved'));
            }

            $this->redirect(Yii::app()->createUrl('/organizer/contact', array('langId' => Yii::app()->language)));
        }

        $this->render('edit', ['model' => $model, 'image' => $image]);
    }

    public function actionDelete($contactId) {

        if(Yii::app()->user->role != User::ROLE_ORGANIZERS){
            $this->redirect(Yii::app()->createUrl('home'));
        }

        OrganizerContact::model()->deleteByPk($contactId);
        $this->redirect(Yii::app()->createUrl('/organizer/contact', array('langId' => Yii::app()->language)));
    }
}
