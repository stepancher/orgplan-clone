<?php
/**
 * Class OrganizerInfoHasFile
 */
class OrganizerInfoHasFile extends \AR
{
    public static $_description = NULL;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerInfoId' => array(
                'integer',
                'label' => 'Календарь подготовки',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'OrganizerInfo',
                    array(
                        'organizerInfoId' => 'id',
                    ),
                ),
            ),
            'fileId' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
        );
    }
}