<?php
/**
 * Class OrganizerContactHasFile
 */
class OrganizerContactHasFile extends \AR
{
    public static $_description = NULL;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerContactId' => array(
                'integer',
                'label' => 'Контакт',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'OrganizerContact',
                    array(
                        'organizerContactId' => 'id',
                    ),
                ),
            ),
            'fileId' => array(
                'label' => 'Файл',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }
}