<?php

/**
 * Class OrganizerExponent
 *
 * @property integer $id
 * @property integer $organizerId
 * @property string $email
 * @property string $name
 * @property string $bill
 * @property string $number
 * @property string $stand
 * @property string $square
 */

class OrganizerExponent extends ShardedAR
{
    public static $_description = NULL;

    /**
     * Поля-исключения, в которых не нужно делать проверку purify
     * @var array
     */
    public $notPurifyAttributes = [
        'name'
    ];

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,searchOrganizerExponent' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerId' => array(
                'integer',
                'label' => 'Организатор',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'lang' => array(
                'string',
                'label' => 'Язык экспонента',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'userId' => array(
                'integer',
                'label' => 'Пользователь',
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'userId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'email' => array(
                'string',
                'label' => 'Email',
                'rules' => array(
                    'search,insert,update,searchOrganizerExponent' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'label' => 'Название компании',
                'rules' => array(
                    'search,insert,update,searchOrganizerExponent' => array(
                        array('safe')
                    )
                ),
            ),
            'bill' => array(
                'string',
                'label' => '№ договора',
                'rules' => array(
                    'search,insert,update,searchOrganizerExponent' => array(
                        array('safe')
                    )
                ),
            ),
            'stand' => array(
                'string',
                'label' => '№ стенда',
                'rules' => array(
                    'search,insert,update,searchOrganizerExponent' => array(
                        array('safe')
                    )
                ),
            ),
            'square' => array(
                'string',
                'label' => 'Площадь стенда',
                'rules' => array(
                    'search,insert,update,searchOrganizerExponent' => array(
                        array('safe')
                    )
                ),
            ),
            'managerId' => array(
                'integer',
                'label' => 'Менеджер',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),

            'user_lastName' => array(
                'label' => '',
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                )
            ),
            'user_phone' => array(
                'label' => '',
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                )
            ),

        );
    }

    public function searchOrganizerExponent() {

        $criteria = new CDbCriteria;
        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes([
            'userId' => Yii::app()->user->id
        ]);
        if ($userHasOrganizer) {
            $criteria->addCondition('organizerId = ' . $userHasOrganizer->organizerId);
        }
//        $criteria->with['user'] = array(
//            'together' => FALSE,
//        );
        $criteria->compare('t.id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('bill', $this->bill, true);
        $criteria->compare('stand', $this->stand, true);
        $criteria->compare('square', $this->square);

        //$criteria->compare('user.lastName', $this->user_lastName, true);

        return new CActiveDataProvider('OrganizerExponent', [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 30
            ],
        ]);
    }
}