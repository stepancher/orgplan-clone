<?php
Yii::import('application.modules.organizer.OrganizerModule');
/**
 * Class OrganizerContact
 *
 * @property integer $id
 * @property integer $organizerId
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $surname
 * @property string $patronymic
 * @property string $position
 */
class OrganizerContact extends \AR
{
    public static $_description = NULL;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerId' => array(
                'label' => 'Организатор',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => 'Имя',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'phone' => array(
                'string',
                'label' => 'Телефон',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'email' => array(
                'string',
                'label' => 'Email',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'surname' => array(
                'string',
                'label' => 'Фамилия',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'patronymic' => array(
                'string',
                'label' => 'Отчество',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'position' => array(
                'string',
                'label' => 'Должность',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerContactHasFiles' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'OrganizerContactHasFile',
                    array(
                        'organizerContactId' => 'id',
                    ),
                ),
            ),
            'administrator' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Administrator'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
        );
    }

    public function getImage($publishUrl, $size = '')
    {
        $result = NULL;
        if ($this->organizerContactHasFiles) {
            foreach ($this->organizerContactHasFiles as $file) {
                if ($file->file && $file->file->type == ObjectFile::TYPE_ORGANIZER_CONTACT_AVATARS) {
                    $result = $file;
                }
            }
        }

        if ($size != '') {
            $prefix = $size . '_';
        } else {
            $prefix = '';
        }

        if ($result !== NULL) {
            $result = '/uploads/organizer/contact/' . $this->id . '/' . $prefix . $file->file->name;
        } else {
            $result = $publishUrl . '/img/user/no-avatar.png';
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getFullName(){
        return $this->surname.' '.$this->name.' '.$this->patronymic;
    }
}