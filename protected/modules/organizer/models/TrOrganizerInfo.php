<?php

/**
 * Class TrOrganizerInfo
 *
 * @property integer $id
 * @property integer $trParentId
 * @property string $langId
 * @property string $name
 * @property string $fullName
 * @property string $scope
 * @property string $territoryOfActivity
 * @property string $postCountry
 * @property string $postRegion
 * @property string $postCity
 * @property string $postStreet
 * @property string $legalCountry
 * @property string $legalRegion
 * @property string $legalCity
 * @property string $legalStreet
 * @property string $requisitesBank
 * @property string $signRulesName
 * @property string $signRulesPosition
 */
class TrOrganizerInfo extends \AR
{
    public static $_description = NULL;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'organizerId' => array(
                'label' => 'Организатор',
                'integer',
            ),
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'trParentId' => array(
                'label' => '',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'langId' => array(
                'label' => '',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fullName' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Full name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'scope' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Scope'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'territoryOfActivity' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Territory of activity'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postCountry' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Country'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postRegion' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postCity' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postStreet' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Street'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalCountry' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Country'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalRegion' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalCity' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalStreet' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Street'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesBank' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Bank'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'signRulesName' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Full name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'signRulesPosition' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Position'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerInfoHasFiles' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'OrganizerInfoHasFile',
                    array(
                        'organizerInfoId' => 'id',
                    ),
                ),
            ),
        );
    }
}