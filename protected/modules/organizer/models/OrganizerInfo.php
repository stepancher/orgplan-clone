<?php
Yii::import('application.modules.organizer.OrganizerModule');
/**
 * Class OrganizerInfo
 *
 * @property integer $id
 * @property integer $organizerId
 * @property \Organizer $organizer
 * @property string $name
 * @property string $fullName
 * @property string $scope
 * @property string $territoryOfActivity
 * @property string $postPostcode
 * @property integer $postCountry
 * @property \Country $postCountryRel
 * @property integer $postRegion
 * @property \Region $postRegionRel
 * @property integer $postCity
 * @property \City $postCityRel
 * @property string $postStreet
 * @property string $postPhone
 * @property string $postSite
 * @property string $legalPostcode
 * @property integer $legalCountry
 * @property \Country $legalCountryRel
 * @property integer $legalRegion
 * @property \Region $legalRegionRel
 * @property integer $legalCity
 * @property \City $legalCityRel
 * @property string $legalStreet
 * @property string $requisitesSettlementAccountRUR
 * @property string $requisitesSettlementAccountEUR
 * @property string $requisitesSettlementAccountUSD
 * @property string $requisitesBank
 * @property string $requisitesBic
 * @property string $requisitesItn
 * @property string $requisitesIec
 * @property string $requisitesCorrespondentAccount
 * @property string $signRulesName
 * @property string $signRulesPosition
 * @property TrOrganizerInfo $translate
 * @property \OrganizerInfoHasFile $organizerInfoHasFiles
 */
class OrganizerInfo extends \AR
{
    public static $_description = NULL;

    protected $_name                = NULL;
    protected $_fullName            = NULL;
    protected $_legalCountry        = NULL;
    protected $_postCountry         = NULL;
    protected $_postRegion          = NULL;
    protected $_postCity            = NULL;
    protected $_postStreet          = NULL;
    protected $_legalRegion         = NULL;
    protected $_legalCity           = NULL;
    protected $_legalStreet         = NULL;
    protected $_requisitesBank      = NULL;
    protected $_signRulesName       = NULL;
    protected $_signRulesPosition   = NULL;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fullName' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Full name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerId' => array(
                'integer',
                'label' => 'Организатор',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerIdRel' => array(
                'label' => 'Организатор',
                'relation' => array(
                    'CBelongsToRelation',
                    'Organizer',
                    array(
                        'organizerId' => 'id',
                    ),
                ),
            ),
            'scope' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Scope'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'territoryOfActivity' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Territory of activity'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postPostcode' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Postcode'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postCountry' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Country'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postCountryRel' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Country'),
                'relation' => array(
                    'CBelongsToRelation',
                    'Country',
                    array(
                        'postCountry' => 'id',
                    ),
                ),
            ),
            'postRegion' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postRegionRel' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Region'),
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'postRegion' => 'id',
                    ),
                ),
            ),
            'postCity' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postCityRel' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'City'),
                'relation' => array(
                    'CBelongsToRelation',
                    'City',
                    array(
                        'postCity' => 'id',
                    ),
                ),
            ),
            'postSite' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Site'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postStreet' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Street'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'postPhone' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Phone'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalPostcode' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Postcode'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),

            ),
            'legalCountry' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Country'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalCountryRel' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Country'),
                'relation' => array(
                    'CBelongsToRelation',
                    'Country',
                    array(
                        'legalCountry' => 'id',
                    ),
                ),
            ),
            'legalRegion' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalRegionRel' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'Region'),
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'legalRegion' => 'id',
                    ),
                ),
            ),
            'legalCity' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalCityRel' => array(
                'label' => Yii::t('OrganizerModule.organizer', 'OrganizerCity'),
                'relation' => array(
                    'CBelongsToRelation',
                    'City',
                    array(
                        'legalCity' => 'id',
                    ),
                ),
            ),
            'legalStreet' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Street'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesBank' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Bank'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesSettlementAccountRUR' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Settlement account RUR'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesSettlementAccountEUR' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Settlement account EUR'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesSettlementAccountUSD' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Settlement account USD'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesBic' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'BIC'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesItn' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'ITN'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesIec' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'IEC'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'requisitesCorrespondentAccount' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Correspondent account'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerInfoHasFiles' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'OrganizerInfoHasFile',
                    array(
                        'organizerInfoId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'signRulesName' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Full name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'signRulesPosition' => array(
                'string',
                'label' => Yii::t('OrganizerModule.organizer', 'Position'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrOrganizerInfo',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
//                    'condition' => 'translate.langId = :langId OR translate.langId IS NULL',
//                    'params' => array(':langId' => Yii::app()->language),
                ),
            ),
        );
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            ),
        );
    }


    /**
     * @param $publishUrl
     * @param string $size
     * @return mixed|null|string
     */
    public function getImage($publishUrl, $size = '')
    {
        $result = NULL;
        if ($this->organizerInfoHasFiles) {
            foreach ($this->organizerInfoHasFiles as $file) {
                if ($file->file && $file->file->type == ObjectFile::TYPE_ORGANIZER_INFO_LOGOS) {
                    $result = $file;
                }
            }
        }

        if ($size != '') {
            $prefix = $size . '_';
        } else {
            $prefix = '';
        }

        if ($result !== NULL) {
            $result = '/uploads/organizer/company/' . $this->id . '/' . $prefix . $file->file->name;
        } else {
            $result = $publishUrl . '/img/empty.gif';
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_name !== NULL){
            $value = $this->_name;
        }elseif(isset($translate->name)) {
            $this->_name = $translate->name;
            $value = $this->_name;
        }

        return $value;
    }

    public function setName($value){
        $this->_name = $value;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_fullName !== NULL){
            $value = $this->_fullName;
        }elseif(isset($translate->fullName)) {
            $this->_fullName = $translate->fullName;
            $value = $this->_fullName;
        }

        return $value;
    }

    public function setFullName($value){
        $this->_fullName = $value;
    }

    /**
     * @return string
     */
    public function getLegalCountry()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_legalCountry !== NULL){
            $value = $this->_legalCountry;
        }elseif(isset($translate->legalCountry)) {
            $this->_legalCountry = $translate->legalCountry;
            $value = $this->_legalCountry;
        }

        return $value;
    }

    public function setLegalCountry($value){
        $this->_legalCountry = $value;
    }

    /**
     * @return string
     */
    public function getLegalCity()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_legalCity !== NULL){
            $value = $this->_legalCity;
        }elseif(isset($translate->legalCity)) {
            $this->_legalCity = $translate->legalCity;
            $value = $this->_legalCity;
        }

        return $value;
    }

    public function setLegalCity($value){
        $this->_legalCity = $value;
    }

    /**
     * @return string
     */
    public function getLegalStreet()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_legalStreet !== NULL){
            $value = $this->_legalStreet;
        }elseif(isset($translate->legalStreet)) {
            $this->_legalStreet = $translate->legalStreet;
            $value = $this->_legalStreet;
        }

        return $value;
    }

    public function setLegalStreet($value){
        $this->_legalStreet = $value;
    }


    /**
     * @return string
     */
    public function getRequisitesBank()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_requisitesBank !== NULL){
            $value = $this->_requisitesBank;
        }elseif(isset($translate->requisitesBank)) {
            $this->_requisitesBank = $translate->requisitesBank;
            $value = $this->_requisitesBank;
        }

        return $value;
    }

    public function setRequisitesBank($value){
        $this->_requisitesBank = $value;
    }

    /**
     * @return string
     */
    public function getPostCountry()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_postCountry !== NULL){
            $value = $this->_postCountry;
        }elseif(isset($translate->postCountry)) {
            $this->_postCountry = $translate->postCountry;
            $value = $this->_postCountry;
        }

        return $value;
    }

    public function setPostCountry($value){
        $this->_postCountry = $value;
    }

    /**
     * @return string
     */
    public function getPostRegion()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_postRegion !== NULL){
            $value = $this->_postRegion;
        }elseif(isset($translate->postRegion)) {
            $this->_postRegion = $translate->postRegion;
            $value = $this->_postRegion;
        }

        return $value;
    }

    public function setPostRegion($value){
        $this->_postRegion = $value;
    }

    /**
     * @return string
     */
    public function getPostCity()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_postCity !== NULL){
            $value = $this->_postCity;
        }elseif(isset($translate->postCity)) {
            $this->_postCity = $translate->postCity;
            $value = $this->_postCity;
        }

        return $value;
    }

    public function setPostCity($value){
        $this->_postCity = $value;
    }

    /**
     * @return string
     */
    public function getPostStreet()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_postStreet !== NULL){
            $value = $this->_postStreet;
        }elseif(isset($translate->postStreet)) {
            $this->_postStreet = $translate->postStreet;
            $value = $this->_postStreet;
        }

        return $value;
    }

    public function setPostStreet($value){
        $this->_postStreet = $value;
    }

    /**
     * @return string
     */
    public function getLegalRegion()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_legalRegion !== NULL){
            $value = $this->_legalRegion;
        }elseif(isset($translate->legalRegion)) {
            $this->_legalRegion = $translate->legalRegion;
            $value = $this->_legalRegion;
        }

        return $value;
    }

    public function setLegalRegion($value){
        $this->_legalRegion = $value;
    }

    /**
     * @return string
     */
    public function getSignRulesName()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_signRulesName !== NULL){
            $value = $this->_signRulesName;
        }elseif(isset($translate->signRulesName)) {
            $this->_signRulesName = $translate->signRulesName;
            $value = $this->_signRulesName;
        }

        return $value;
    }

    public function setSignRulesName($value){
        $this->_signRulesName = $value;
    }

    /**
     * @return string
     */
    public function getSignRulesPosition()
    {
        $value = '';
        $translate = $this->translate;
        if($this->_signRulesPosition !== NULL){
            $value = $this->_signRulesPosition;
        }elseif(isset($translate->signRulesPosition)) {
            $this->_signRulesPosition = $translate->signRulesPosition;
            $value = $this->_signRulesPosition;
        }

        return $value;
    }

    public function setSignRulesPosition($value){
        $this->_signRulesPosition = $value;
    }

    public function trAttributeNames(){
        return [
            'name',
            'fullName',
            'postCountry',
            'postRegion',
            'postCity',
            'postStreet',
            'legalCountry',
            'legalRegion',
            'legalCity',
            'legalStreet',
            'requisitesBank',
            'signRulesName',
            'signRulesPosition',
        ];
    }
}