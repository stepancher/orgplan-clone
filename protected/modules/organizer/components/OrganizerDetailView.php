<?php
/**
 * File OrganizerDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class OrganizerDetailView
 *
 * @property Organizer $data
 */
class OrganizerDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		$organizerCompanyLink = '';

		if(!empty($this->data->companyId))
			$organizerCompanyLink = "<a href=".Yii::app()->createUrl('admin/organizer/updateOrganizerCompany',array('id' => $this->data->companyId))." class=\"b-button b-button-icon f-text--tall d-text-light d-bg-warning\" data-icon=\"&#xe0ea;\" ></a>";

		//$this->attributes['id']['visible'] = false;
		$this->attributes['linkToTheSiteOrganizers']['value'] = CHtml::link(
			$this->data->linkToTheSiteOrganizers,
			$this->data->linkToTheSiteOrganizers,
			array('target' => '_blank')
		);
		$this->attributes['linkToTheSiteOrganizers']['type'] = 'raw';

		$this->attributes['phone']['value'] = $this->data->phoneWithEmail;

		$this->attributes['organizerName']['type'] = 'raw';

		$this->attributes['organizerName']['value'] = (isset($this->data->company) ? $this->data->companyId . ' ' . $this->data->company->name : '') . ' ' . $organizerCompanyLink;




		parent::init();
	}
}