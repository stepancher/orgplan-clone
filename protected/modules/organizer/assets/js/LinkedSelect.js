var LinkedSelect = {
	url: null,
	blockingBox: '<div class="blocking-block"></div>',
	init: function(url) {
		var me = this;

		if(typeof url !== 'undefined') {
			me.url = url;
		}

		return me;
	},
	run: function() {
		var me = this;

		$('[data-related]:not(.js-first-filter-list)').each(function () {
			var $elm = $(this);
			$elm.append(me.blockingBox);
		});
		me.events();
	},
	events: function() {
		var me = this;

		$(document).delegate('[data-child-related] .js-drop-item', 'click', function () {
			var $item = $(this);

			me.update($item.closest('[data-related]'), $item.data('value'));
		});
	},
	update: function ($list, selected) {
		var me = this,
			name = $list.data('related'),
			relName = $list.data('child-related'),
			$childList = typeof relName !== 'undefined' ? $('[data-related$="' + relName + '"]') : null,
			values = [];

		if ($childList === null || !$childList.length) {
			return;
		}

		$list.find('.js-drop-item').each(function () {
			var $item = $(this);

			if (!selected && $item.data('selected')) {
				selected = $item.data('value');
			}
			values.push($item.data('value'));
		});

		var $loader = $childList.find('.blocking-block').show();

		$.get(me.url, {
			ids: selected ? [selected] : values,
			name: name.replace(/#.+$/, '')
		}, function (json) {
			if (typeof json === 'object') {
				var $items = $childList.find('.js-drop-box'),
					$select = $childList.find('select'),
					$button = $childList.find('.js-drop-button');

				$items.find('.js-drop-item').each(function() {
					$(this).remove();
				});
				$button.text($button.text().replace(/\(.+\)$/, '').trim());
				$select.find('option').each(function() {
					$(this).remove();
				});
				var i = 0;
				$.each(json, function (id, text) {
					var $option = $('<option/>', {
							value: id,
							text: text
						}),
						$dropItem = $('<div/>', {
							"data-value": id,
							"data-selected": false,
							class: 'js-drop-item drop-item ' + (i % 2 ? 'even' : 'odd'),
							text: text
						});

					$select.append($option);
					$items.append($dropItem);
				});
			}
			me.update($childList);
			$loader.hide();
		}, 'json');
	}
};