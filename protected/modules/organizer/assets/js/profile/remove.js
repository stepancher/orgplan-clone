/**
 * Created by lain on 10.03.16.
 */

'use strict';

var orgplan = {};

function removeItem(params) {
    return ({
        lang: 'ru',
        route: '', //'/workspace/removeFair/'
        key: '',
        run: function(params){
                $('.js-remove-item').click(function () {
                    $('#modal-remove').modal('show');

                    var target = $(this).attr('data-remove-target');

                    $('.js-modal-ok').click(function (e) {
                        e.stopPropagation();
                        var attributes = {};
                        attributes[params.key] = target;
                        $.ajax({
                            'url' : params.route,
                            'type' : 'get',
                            'data' : attributes
                        });
                        $('#modal-remove').modal('hide');
                        $('[data-remove="' + target + '"]').fadeOut('slow');
                    });

                    $('.js-modal-cancel').click(function (e) {
                        e.stopPropagation();
                        $('#modal-remove').modal('hide');
                    });
                });

        }
    }).run(params);
};