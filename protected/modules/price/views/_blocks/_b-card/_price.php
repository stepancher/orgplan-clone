<?php
/**
 * @var ProductPrice $data
 * @var bool $enableButton
 * @var array $_data_
 */
?>
<?php
if (isset($data->product)):?>
    <div class="b-switch-list-view__item b-switch-list-view__item-price">
        <div itemscope itemType="http://schema.org/Service">
            <div class="b-switch-list-view__cell b-switch-list-view__cell--price-name">
                <div itemprop="name">
                    <div title="<?= $data->getProductName() ?>">
                        <?= $data->getProductName() ?>
                    </div>
                </div>
            </div>
            <div class="b-switch-list-view__cell b-switch-list-view__cell--price-description">
                <div itemprop="description">
                    <?= $data->product->param ? $data->product->param : '' ?>
                </div>
            </div>
            <div class="b-switch-list-view__cell b-switch-list-view__cell--price-city">
                <?= $data->getCityName() ?>
            </div>
            <div class="b-switch-list-view__cell b-switch-list-view__cell--price-category">
                <?= $data->product->getCategoryName() ?>
            </div>
        </div>
        <div class="b-switch-list-view__cell b-switch-list-view__cell--price">
            <span class="price-desc">
                <span itemscope itemtype="http://schema.org/PriceSpecification">
                <span content="RUB" itemprop="priceCurrency">
                    <?= $data->product->getCurrencyById($data->product->currency) ?>
                </span>
            </span>

                <?php if (!empty($data->product->getCostTypeById($data->product->costType)) && !empty($data->product->getCurrencyById($data->product->currency))): ?>
                    /
                <?php endif; ?>

                <?= $data->product->getCostTypeById($data->product->costType) ?>
            </span>
                <span class="price">
                    <?php if (Yii::app()->user->isGuest): ?>
                        <span class="lock" data-toggle="modal" data-target="#modal-sign-up">
                        </span>
                    <?php else: ?>
                        <span itemscope itemtype="http://schema.org/PriceSpecification">
                        <span itemprop="price"><?= !empty($data->cost) ? $data->cost : '' ?></span>
                        </span>
                    <?php endif; ?>
                </span>
            </span>
        </div>
    </div>

    <?php /**Для создания заявки**/
    ?>
    <?php if ($_data_['index'] == ($_data_['widget']->dataProvider->totalItemCount - 1)): ?>

        <?php
        if (Yii::app()->user->isGuest) {
            $modalClass = 'modal-sign-up';
        } elseif (User::checkTariff(1)) {
            $modalClass = 'modal-request-price';
        } else {
            $modalClass = 'modal-tariff-insufficient';
        }
        ?>

        <div class="b-switch-list-view__item b-switch-list-view__item-price price-request"
             data-toggle="modal"
             data-target="#<?= $modalClass; ?>"
             category="<?= $data->getCategoryId(); ?>"
             city="<?= $data->cityId; ?>">

            <div class="getPrice">
                <div class="plus"></div>
                <div style="float:right;">
                    <?= Yii::t('product', 'Price request') ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>