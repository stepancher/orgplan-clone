<?php

class JsonController extends Controller
{

    protected $dump = FALSE;

    public function actionDump($langId)
    {
        $this->dump = TRUE;
        $this->actionGetData($langId);
    }

    public function actionGetData($cityId = NULL)
    {
        if($cityId == NULL){
            $cityId = 'moscow';
        }

        $city = City::model()->findByAttributes(['shortUrl' => $cityId]);

        if($city === NULL){
            $city = City::model()->findByAttributes(['id' => $cityId]);
        }

        $description = '';

        if (method_exists(ProductPrice::model(), 'getCustomSeoDescription')) {
            $description = ProductPrice::getCustomSeoDescription($cityId);
        }

        $header = '';
        $title = 'Каталог цен';

        if(!empty($city) && !empty($city->name)){
            $header = Yii::t('PriceModule.price', 'Prices in city').' '.$city->name;
            $title = $title. ' ' .$city->name;
        }

        $json = array(
            'title' => $title,
            'description' => $description,
            'allLabel' => Yii::t('PriceModule.price', 'All prices'),
            'header' => $header,
            'link' => "/ru/prices/moscow",
            'list' => $this->getCityPrices($cityId),
        );

        if ($this->dump) {
            CVarDumper::dump($json, 10, 1);
            exit;
        }

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . json_encode($json) . ');';
    }

    public function getCityPrices($cityId){

        $sql = "
            SELECT
                cost price,
                p.costType costType,
                p.currency currency,
                trpc.name categoryName,
                trp.name productName,
                trp.param productParams,
                trpc.description categoryDescription

            FROM tbl_productprice pp
            LEFT JOIN tbl_city c ON (c.id = pp.cityId)
            LEFT JOIN tbl_trcity trc ON (c.id = trc.trParentId AND trc.langId = :langId)
            LEFT JOIN tbl_product p ON (p.id = pp.productId)
            LEFT JOIN tbl_trproduct trp ON (p.id = trp.trParentId AND trp.langId = :langId)
            LEFT JOIN tbl_productcategory pc ON (pc.id = p.categoryId)
            LEFT JOIN tbl_trproductcategory trpc ON (trpc.trParentId = pc.id AND trpc.langId = :langId)

            WHERE
                c.shortUrl = :cityId

            ORDER BY trpc.name, trp.name ASC
        ";

        $prices = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(
            ':langId' => Yii::app()->language,
            ':cityId' => $cityId,
        ));

        $gdata = array();
        $result = array();
        $prev = '';
        $i = 0;
        foreach($prices as $price){
            $i++;
            $curr = $price['categoryName'];

            $costType = $price['costType']?Product::getCostTypeById($price['costType']).' / ':'';

            if($prev == $curr){
                $gdata['list'][] = array(
                    'description' => $price['productParams'],
                    'header' => $price['productName'],
                    'unit' => $costType.Product::getCurrencyById($price['currency']),
                    'value' => $price['price'],
                );
            }else{
                if(!empty($gdata)){
                    $result[] = $gdata;
                }


                unset($gdata);
                $gdata['header'] = $price['categoryName'];
                $gdata['list'][] = array(
                    'description' => $price['productParams'],
                    'header' => $price['productName'],
                    'unit' => $costType.Product::getCurrencyById($price['currency']),
                    'value' => $price['price'],
                );
            }

            if($i == count($prices)){
                $result[] = $gdata;
            }

            $prev = $curr;
        }

        return $result;
    }

    public function actionGetCitiesList(){

        $json = array(
            "header"=> Yii::t('PriceModule.price', 'Price catalog for products and services'),
            "models"=> array(
                "header"=> Yii::t('PriceModule.price', 'Cities list'),
                "list"=> $this->getCities(),
            )
        );

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . json_encode($json) . ');';
    }

    public function getCities(){

        $sql = "
            SELECT
                trc.name name,
                c.shortUrl id

            FROM tbl_productprice pp

            LEFT JOIN tbl_city c ON c.id = pp.cityId
            LEFT JOIN tbl_trcity trc ON c.id = trc.trParentId AND trc.langId = :langId

            GROUP BY pp.cityId
            ORDER BY trc.name ASC
        ";

        $cities = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->language));

        return $cities;
    }
}