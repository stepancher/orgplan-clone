<?php

class PriceModule extends CWebModule {

	public $defaultController = 'json';

	public function init() {

		$this->setImport(array(
			'price.models.*',
		));
	}
}