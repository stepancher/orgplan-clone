<?php 
    return array(
        'All prices' => 'Все цены',
        'Prices in city' => 'Цены на услуги в городе',
        'Price catalog for products and services' => 'Каталог цен на товары и услуги',
        'Cities list' => 'Список городов',
        'Catalog price' => 'Каталог цен',
    );