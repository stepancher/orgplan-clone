<?php 
return array(
    'All prices' => 'Все цены',
    'Prices in city' => 'Цены на услуги в городе',
    'Price catalog for products and services' => 'PREISLISTE FÜR WAREN UND DIENSTLEISTUNGEN',
    'Cities list' => 'Список городов',
    'Catalog price' => 'Каталог цен',
);