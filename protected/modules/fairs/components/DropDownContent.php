<?php
/**
 * File DropDownContent.php
 */

/**
 * Class DropDownContent
 */
class DropDownContent extends CWidget
{
	/**
	 * Содержимое вида, может быть передана функция
	 * @var string
	 */
	public $content;

	/**
	 * Атрибуты елементов
	 * @var array
	 */
	public $htmlOptions;

	/**
	 * Название кнопки
	 * @var string
	 */
	public $label = 'Click me';

	/**
	 * Запуск отрисовки
	 */
	public function run()
	{
		static::register();

		if (is_callable($this->content)) {
			ob_start();
			ob_implicit_flush(false);

			echo call_user_func($this->content, $this->getOwner());

			$content = ob_get_clean();
		} else {
			$content = $this->content;
		}

		$htmlOptions = ['class' => 'drop-down-content js-drop-down-content'];
		$labelOptions = ['class' => 'drop-down-content__label js-drop-down-content__label'];
		$contentOptions = ['class' => 'drop-down-content__drop-block js-drop-down-content__drop-block'];
		foreach ($this->htmlOptions as $optionName => $option) {
			if (is_array($option)) {
				if ($optionName == 'labelOptions') {
					foreach ($option as $lOptionName => $lOption) {
						$labelOptions = $this->addAttribute($labelOptions, $lOptionName, $lOption);
					}
				} elseif ($optionName == 'contentOptions') {
					foreach ($option as $COptionName => $COption) {
						$contentOptions = $this->addAttribute($contentOptions, $COptionName, $COption);
					}
				} else {
					throw new CException('Undefined option "' . $optionName . '"');
				}
			} else {
				$htmlOptions = $this->addAttribute($htmlOptions, $optionName, $option);
			}
		}

		$label = CHtml::tag('div', $labelOptions, $this->label);
		$output = CHtml::tag('div', $contentOptions, CHtml::tag('div', array(), $content));

		echo CHtml::tag('div', $htmlOptions, $label . $output);
	}

	static function register()
	{

		/**
		 * @var CWebApplication $app
		 */
		$app = Yii::app();
		$am = $app->getAssetManager();
		$cs = $app->getClientScript();
		$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.fair.assets'), false, -1, YII_DEBUG);

		$cs->registerCssFile($publishUrl . '/css/drop-down-content/style.css');
		$cs->registerScriptFile($publishUrl . '/js/drop-down-content/DropDownContent.js', CClientScript::POS_END);

		$publishUrl4SlimScroll = $am->publish($app->basePath . '/extensions/slimscroll');
		$cs->registerScriptFile($publishUrl4SlimScroll . '/jquery.slimscroll.js');
	}

	/**
	 * Добавление атрибутов к елементу
	 *
	 * @param $array
	 * @param $key
	 * @param $value
	 * @return array
	 */
	private function addAttribute($array, $key, $value)
	{
		if (isset($array[$key])) {
			$array[$key] = $array[$key] . ' ' . $value;
		} else {
			$array[$key] = $value;
		}
		return $array;
	}
}