<?php
/**
 * File FairGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class FairGridView
 *
 * @property Fair $model
 */
class MatchFairGridView extends TbGridView
{
    /**
     * Модель Fair::model() or new Fair('scenario')
     * @var Fair
     */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /** @var bool $enableFilter */
    public $enableFilter = true;

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = $this->enableFilter ? ARGridView::createFilter($this->model, true) : null;

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();
        $columns = array(
            'name' => array(
                'name' => 'name',
                'header' => Yii::t('FairsModule.fairs', 'Exhibition name'),
                'type' => 'raw',
            ),
            'rating' => array(
                'name' => 'rating',
                'header' => '',
                'type' => 'html',
                'value' => function ($data) {
                    $rating = $data['rating'];
                    /**
                     * @var Fair $data
                     * @var CWebApplication $app
                     */
                    $app = Yii::app();
                    $am = $app->getAssetManager();
                    $basePath = $app->theme->basePath . '/assets';
                    if ($rating !== null) {
                        $fullPath = TbHtml::image($am->publish($basePath . "/img/fair/ratingImage/rus/expo-rating-{$rating}.png"), '', ['title' => Fair::getTitleRatingIcon(Fair::model()->findByPk($data['id']))]);
                        return $fullPath;
                    } else {
                        return TbHtml::image(ObjectFile::noImage(), '', array('style' => 'width:20px;height:20px'));
                    }
                },
                'htmlOptions' => array(
                    'style' => 'width: 40px;',
                    'class' => 'rating-block'
                ),
            ),
            'beginDate' => array(
                'name' => 'beginDate',
                'header' => Yii::t('FairsModule.fairs', 'fair begin date'),
            ),
            'endDate' => array(
                'name' => 'endDate',
                'header' => Yii::t('FairsModule.fairs', 'fair end date'),
            ),
            'square' => array(
                'name' => 'square',
                'header' => Yii::t('FairsModule.fairs', 'Square net'),
                'value' => function($data){

                    if($data['squareType'] == 'squareGross')
                        return $data['square'] . Yii::t('FairsModule.fairs', '(Square gross)');
                    return $data['square'];
                }
            ),
            'members' => array(
                'name' => 'members',
                'header' => Yii::t('FairsModule.fairs', 'members'),
            ),
            'visitors' => array(
                'name' => 'visitors',
                'header' => Yii::t('FairsModule.fairs', 'visitors'),

            ),

        );
        return $columns;
    }
}