<?php
Yii::import('application.widgets.DropZone');

/**
 * Class FairController
 */
class DefaultController extends Controller
{
    /**
     * @TODO refactor
     */
    const SEE_ALSO_RU_CITY_ID = 1; // Moscow.
    const SEE_ALSO_DE_CITY_ID = 114; // Berlin.

    /**
     * @var Fair $model
     */
    public $model = 'Fair';
    public $id = null;
    public $isNewRecord;

    /**
     * Стандартные actions перенаправляют на 404.
     */
    public function actionIndex()
    {
        throw new CHttpException(404, Yii::t('FairsModule.fairs', 'Unable to resolve the request "{route}".',
            array('{route}' => $_SERVER['REQUEST_URI'])));
    }

    /**
     * Список ТЗ
     */
    public function actionTZFair()
    {
        $fairIds = array();
        $app = Yii::app();
        $userId = $app->user->id;
        $Ids = TZ::model()->findAllByAttributes(array('userId' => $userId));
        if ($Ids) {
            foreach ($Ids as $id) {
                $fairIds[] = $id->fairId;
            }
        }
        $this->render('tZFair', array(
            'model' => TZ::model(),
            'fairIds' => $fairIds
        ));
    }

    /**
     * @param $id
     * @param $fileId
     * Удаленее файлов
     */
    public function actionDeleteFile($id, $fileId)
    {
        /** @var AR $model */
        if (($model = AR::model($this->model)->findByPk($id)) === null) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
            $this->redirect($this->createUrl('index'));
        }

        $objectFile = ObjectFile::model()->findByPk($fileId);
        if (null == $objectFile) {
            Yii::app()->user->setFlash('danger', 'Запрашиваемая информация отсутствует.');
        } else {
            Yii::app()->user->setFlash('success', 'Файл удален.');
            $objectFile->deleteFile($model);
        }
        $this->redirect($this->createUrl('save', array('id' => $id)));
    }

//    public function actionView($langId, $trFairName){
//        echo "lang:{$langId}, trFairName:{$trFairName}";
//    }

    /**
     * Календарь подготовки
     */
    public function actionMyFairCalendar()
    {
        /** @var Fair $model */
        $this->render('myFairCalendar', array(
            'model' => Fair::model($this->model),
        ));
    }

    /**
     * TODO nikborovskiy Не нашел где используется - Артефакт?
     * @param $id
     * Изменение комментария
     */
    public function actionChangeCommentStatus($id)
    {
        /** @var Fair $model */
        if (($comment = Comment::model()->findByPk($id)) !== null) {
            if ($comment->type == Comment::STATUS_NEW || $comment->type == Comment::STATUS_HIDDEN) {
                $comment->type = Comment::STATUS_PUBLISHED;
                Yii::app()->user->setFlash('success', 'Комментарий опубликован');
            } elseif ($comment->type == Comment::STATUS_PUBLISHED) {
                $comment->type = Comment::STATUS_HIDDEN;
                Yii::app()->user->setFlash('success', 'Комментарий скрыт');
            }
            $comment->save(false);
            $this->redirect($this->createUrl('view', array('id' => $comment->fairHasComment->fair->id)));
        }
    }

    /**
     * @param $id
     * @param null $class
     * Активация деактивация/объекта
     */
    public function actionActive($id, $class = null)
    {
        /** @var Fair $model */
        if (($fair = Fair::model()->findByPk($id)) !== null) {
            if ($fair->active == User::ACTIVE_ON) {
                $fair->active = User::ACTIVE_OFF;
                Yii::app()->user->setFlash('success', 'Обьект деактивирован');
            } else {
                $fair->active = User::ACTIVE_ON;
                Yii::app()->user->setFlash('success', 'Обьект активирован');
            }
            $fair->save(false);

        }
        $this->redirect($this->createUrl('fair/index'));
    }

    /**
     * @param $id
     * @param null $usefulCBId
     * @throws CDbException
     * Сохранение Выставок в избранное
     */
    public function actionSaveMyFairViewData($id, $usefulCBId = null)
    {
        /** @var MyFairViewData $model */

        if (($fair = Fair::model()->findByPk($id)) !== null) {
            if (($myFairViewData = MyFairViewData::model()->findByAttributes(array(
                    'fairId' => $id,
                    'userId' => Yii::app()->user->id,
                ))) == null
            ) {
                $myFairViewData = new MyFairViewData();
                $myFairViewData->fairId = $id;
                $myFairViewData->userId = Yii::app()->user->id;
                $myFairViewData->usefulCBId = $usefulCBId;
                $myFairViewData->save(false);
            } else {
                $myFairViewData->delete();
                $myFairViewData = new MyFairViewData();
                $myFairViewData->fairId = $id;
                $myFairViewData->userId = Yii::app()->user->id;
                $myFairViewData->usefulCBId = $usefulCBId;
                $myFairViewData->save(false);

            }
        }
    }

    /**
     * @param $key1
     * @param $key2
     * @param $key3
     * @param $data
     * @return null
     * Проверка на заполнение полей в калькуляторе
     */
    public function checkCalculatorData($key1, $key2, $key3, $data)
    {
        return isset($data[$key2], $data[$key2][$key1], $data[$key2][$key1][$key3]) ? $data[$key2][$key1][$key3] : null;
    }

    /**
     * Просмотр Избранного
     */
    public function actionMatchView()
    {

        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->request->urlReferrer);
        }

        $userId = Yii::app()->user->id;
        $lang = Yii::app()->language;
        $sql = "SELECT f.id AS id, trf.name AS name, f.beginDate, f.endDate, 
                CASE WHEN(fi.squareNet IS NULL) THEN fi.squareGross
                ELSE fi.squareNet END square,
                CASE WHEN(fi.squareNet IS NULL) THEN 'squareGross'
                ELSE 'squareNet' END squareType,
                fi.members, fi.visitors, f.rating
                FROM tbl_fair f
                LEFT JOIN tbl_fairinfo fi ON fi.id = f.infoId
                JOIN tbl_fairmatches fm ON fm.fairId = f.id
                LEFT JOIN tbl_trfair trf ON trf.trParentId = f.id AND trf.langId = '{$lang}'
                WHERE fm.userId = {$userId}";

        $sort = new CSort;
        $sort->defaultOrder = 'name DESC';

        $sort->attributes = array(
            'name' => array(
                'asc' => 'trf.name',
                'desc' => 'trf.name DESC',
            ),
            'beginDate' => array(
                'asc' => 'f.beginDate',
                'desc' => 'f.beginDate DESC',
            ),
            'endDate' => array(
                'asc' => 'f.endDate',
                'desc' => 'f.endDate DESC',
            ),
            'square' => array(
                'asc' => 'CASE WHEN(fi.squareNet IS NULL) THEN fi.squareGross
                ELSE fi.squareNet END',
                'desc' => 'CASE WHEN(fi.squareNet IS NULL) THEN fi.squareGross
                ELSE fi.squareNet END DESC',
            ),
            'members' => array(
                'asc' => 'fi.members',
                'desc' => 'fi.members DESC',
            ),
            'visitors' => array(
                'asc' => 'fi.visitors',
                'desc' => 'fi.visitors DESC',
            ),
            'rating' => array(
                'asc' => 'f.rating',
                'desc' => 'f.rating DESC',
            ),
        );

        $dataProvider = new CSqlDataProvider($sql,array(
            'keyField' => 'name',
            'sort' => $sort,
        ));

        $this->render('matchView', array(
            'model' => AR::model($this->model),
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * @throws CDbException
     */
    public function actionMatchClear()
    {
        $criteria = new CDbCriteria();
        $criteria->compare('userId', Yii::app()->user->id);
        FairMatches::model()->deleteAll($criteria);

        /** @var CWebApplication $app */
        $app = Yii::app();
        /** @var CHttpRequest $request */
        $request = $app->getRequest();
        if (!$request->isAjaxRequest) {
            $this->redirect(
                $app->user->getReturnUrl(
                    $this->createUrl('/search', ['sector' => 'fair'])
                )
            );
        }
    }

    /**
     * @param $id
     * @param bool $remove
     * @throws CDbException
     */
    public function actionMatchChange($id, $remove = false)
    {
        $result = [
            'count' => 0,
            'checked' => false
        ];
        $fair = Fair::model()->findByPk($id);
        if (null !== $fair && $fair->active) {
            if (Yii::app()->user->isGuest) {
                $matches = Yii::app()->session->get('matches', []);
                if ($remove) {
                    unset($matches[$id]);
                } else {
                    $matches[$id] = true;
                }
                $result['count'] = sizeof($matches);
                $result['checked'] = !$remove;
                Yii::app()->session->add('matches', $matches);
            } else {
                $match = FairMatches::model()->findByAttributes([
                    'fairId' => $id,
                    'userId' => Yii::app()->user->id
                ]);
                if ($remove) {
                    if (null !== $match) {
                        $match->delete();
                    }
                    $result['checked'] = false;
                } else {
                    if (null === $match) {
                        $match = new FairMatches();
                        $match->fairId = $id;
                        $match->userId = Yii::app()->user->id;
                        if ($match->save(false)) {
                            $result['checked'] = true;
                        }
                    }
                }
                $result['count'] = FairMatches::model()->countByAttributes([
                    'userId' => Yii::app()->user->id
                ]);
            }
        }
        echo json_encode($result);
    }

    /**
     * @param $id
     * @throws CDbException
     * Удаленее коментария
     */
    public function actionDeleteComment($id)
    {
        /** @var Blog $model */
        if ((User::checkAdministrationRoles() || Yii::app()->user->id == $model->authorId)
            && ($comment = Comment::model()->findByPk($id)) !== null
        ) {
            $comment->delete();
        }
    }

    /**
     * @param $pathsArray
     * @param $folderImagePath
     * @param $fileImagePath
     * @param $deleteIconPath
     * @param $loaderPath
     * @return array
     * Формирования массива миниатюр
     */
    public function getThumbnailsArray($pathsArray, $folderImagePath, $fileImagePath, $deleteIconPath, $loaderPath)
    {
        $thumbnailsArray = array();

        if (is_array($pathsArray)) {
            $i = 0;
            foreach ($pathsArray as $url) {
                if (is_string($url)) {
                    $arr = explode(DIRECTORY_SEPARATOR, $url);
                    $name = array_pop($arr);
                    if (preg_match('/.+\.(\w+)$/xis', $url)) {
                        $thumbnailsArray[$i]['image'] = $fileImagePath;
                        $thumbnailsArray[$i]['class'] = 'folder-image';
                        $thumbnailsArray[$i]['itemOptions'] = array(
                            'class' => 'add-folder-span',
                        );
                    } else {
                        $html = '<div style="position: relative; display:none" class="rename-folder">' .
                            "<input type='text' value='$name' name='rename-folder' class='rename-folder-input'/>
							<a onclick='renameFolder(this,\"$name\")' class='btn-small btn-warning rename-button' style='display: inline;'>ok</a>
						</div>";
                        $cut = $name;
                        if (mb_strlen($name) > 18) {
                            $cut = substr($name, 0, 18) . '...';
                        }
                        $thumbnailsArray[$i]['image'] = $folderImagePath;
                        $thumbnailsArray[$i]['class'] = 'folder-image folder';
                        $thumbnailsArray[$i]['caption'] = "<span class='add-folder'><span title='$name'>$cut</span> $html</span>" . TbHtml::image(
                                $deleteIconPath, '', array(
                                    'class' => 'delete-folder',
                                )
                            ) .
                            TbHtml::image($loaderPath, '', array(
                                'style' => 'display: none; position:absolute; left: 10px; bottom: 30px',
                                'class' => 'ajax-loader'
                            ));;
                        $thumbnailsArray[$i]['itemOptions'] = array(
                            'class' => 'add-folder-span'
                        );
                    }
                }
                $i++;
            }
        }

        return $thumbnailsArray;
    }

    /**
     * @param $path
     * Перенос папки/файла
     */
    public function removeDir($path)
    {
        if ($objects = glob($path . "/*")) {
            foreach ($objects as $obj) {
                is_dir($obj) ? $this->removeDir($obj) : unlink($obj);
            }
        }
        rmdir($path);
    }

    /**
     * @param $path
     * @return mixed
     * Конвертация пути
     */
    public function convertPath($path)
    {
        return preg_match('/\|+/', $path) ? str_replace('|', DIRECTORY_SEPARATOR,
            $path) : str_replace(DIRECTORY_SEPARATOR, '|', $path);
    }

    public function actionGetFairList($filterData)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $result = [
                'status' => 0,
                'data' => []
            ];
            $criteria = new CDbCriteria;
            $criteria->compare('name', $filterData, true);
            $models = Fair::model()->findAll($criteria);
            if (null != $models) {
                $result = [
                    'status' => 1,
                    'fairsData' => CHtml::listData($models, 'id', 'name')
                ];
            }

            echo json_encode($result);
        }

    }
}