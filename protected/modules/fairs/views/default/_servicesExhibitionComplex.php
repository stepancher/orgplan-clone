<?php
/**
 * @var CController $this
 * @var ExhibitionComplex $model
 */
$this->widget(
	'application.modules.fairs.components.FieldSetView',
	array(
		'header' => Yii::t('FairsModule.fairs', 'Contact Information'),
		'items' => array(
			array(
				'application.widgets.ExhibitionComplexDetailView',
				'data' => $model,
			),
		)
	)
);