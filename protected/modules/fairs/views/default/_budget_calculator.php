<?php
/**
 * @var Fair $model
 * @var CWebApplication $app
 * @var array $data
 * @var FairController $this
 */
$app = Yii::app();
$cs = $app->getClientScript();
$am = $app->getAssetManager();
$cs->registerScriptFile($am->publish($app->theme->basePath . '/assets' . '/js/budgetCalculator/script.js'));
$cs->registerCssFile($am->publish($app->theme->basePath . '/assets' . '/css/budgetCalculator/main.css'));

$array = array();
$objectivesConcept = ObjectivesConcept::model()->findAll();
if (!empty($objectivesConcept)) {
	$array[Yii::t('FairsModule.fairs', 'PURPOSE. CONCEPT')] = $objectivesConcept;
}
$servicesStandMakingWork = ServicesStandMakingWork::model()->findAll();
if (!empty($servicesStandMakingWork)) {
	$array[Yii::t('FairsModule.fairs', 'SERVICES STAND CONSTRUCTION AND MAKING IT WORK')] = $servicesStandMakingWork;
}
$programAndRentalSpace = ProgramAndRentalSpace::model()->findAll();
if (!empty($programAndRentalSpace)) {
	$array[Yii::t('FairsModule.fairs', 'PROGRAM PARTICIPATION AND SPACE FOR RENT')] = $programAndRentalSpace;
}
$choosingContractor = ChoosingContractor::model()->findAll();
if (!empty($choosingContractor)) {
	$array[Yii::t('FairsModule.fairs', 'SERVICES FOR EXHIBITIONS \ SELECTION OF CONTRACTORS')] = $choosingContractor;
}
$organizationOfArrival = OrganizationOfArrival::model()->findAll();
if (!empty($organizationOfArrival)) {
	$array[Yii::t('FairsModule.fairs', 'ARRANGE CHECK \ DEPARTURE TO THE EXHIBITION AND LOGISTICS')] = $organizationOfArrival;
}
$organizationOfMissions = OrganizationOfMissions::model()->findAll();
if (!empty($organizationOfMissions)) {
	$array[Yii::t('FairsModule.fairs', 'ORGANIZATION OF MISSIONS')] = $organizationOfMissions;
}
$postExhibitionOfWork = PostExhibitionOfWork::model()->findAll();
if (!empty($postExhibitionOfWork)) {
	$array[Yii::t('FairsModule.fairs', 'POST EXHIBITION WORK')] = $postExhibitionOfWork;
}

?>

<?= TbHtml::submitButton(Yii::t('FairsModule.fairs', 'Save results'), array('color' => 'info', 'class' => 'calculator-save-button')) ?>
<table id="fair-calculator" border="1">
	<?php foreach ($array as $label => $models) { ?>
		<tbody class="calc-block">
		<tr class="header">
			<td><?= $label ?></td>
			<td><?=Yii::t('FairsModule.fairs', 'PLAN')?></td>
			<td><?=Yii::t('FairsModule.fairs', 'FACT')?></td>
			<td><?=Yii::t('FairsModule.fairs', 'DEVIATIONS')?></td>
			<td><?=Yii::t('FairsModule.fairs', 'DEVIATIONS in')?> %</td>
			<td><?=Yii::t('FairsModule.fairs', 'VARIANT')?> 1</td>
			<td><?=Yii::t('FairsModule.fairs', 'COMMENTS')?></td>
		</tr>
		<?php foreach ($models as $k => $model) {
			$name = get_class($model);
			?>
			<tr class="process-block calc-row" data-object="<?= $name ?>" data-index="<?= $k ?>">
				<td class="field-name"><?= $model->name ?></td>
				<td>
					<input name="Calculator[<?= $name ?>][<?= $k ?>][]"
						   class="calculator-value calc-value calc-plan"
						   value="<?= $this->checkCalculatorData($k, $name, 0, $data) ?>"
						   type="text">
				</td>
				<td>
					<input name="Calculator[<?= $name ?>][<?= $k ?>][]"
						   class="calculator-value calc-value"
						   value="<?= $this->checkCalculatorData($k, $name, 1, $data) ?>" type="text">
				</td>
				<td class="result calc-row-result"></td>
				<td class="result calc-row-result-percent"></td>
				<td>
					<input name="Calculator[<?= $name ?>][<?= $k ?>][]" data-changed="false"
						   class="calculator-value calc-value calc-variant"
						   value="<?= $this->checkCalculatorData($k, $name, 2, $data) ?>"
						   type="text">
				</td>
				<td><?= $model->comment ?></td>
			</tr>
		<?php } ?>
		<tr class="calculator-results" data-object-results="<?= get_class($models[0]) ?>">
			<td><?=Yii::t('FairsModule.fairs', 'TOTAL')?>:</td>
			<td class="result-plan"></td>
			<td class="result-fact"></td>
			<td class="result-deviation"></td>
			<td class="result-deviation-percent"></td>
			<td class="result-variant"></td>
			<td></td>
		</tr>
		</tbody>
	<?php } ?>
	<tbody class="calc-block">
	<tr class="other-costs process-block calc-row" data-object="OtherCosts" data-index="0">
		<td class="field-name"><?=Yii::t('FairsModule.fairs', 'Other costs')?></td>
		<td>
			<input name="Calculator[OtherCosts][0][]" class="calculator-value calc-value"
				   value="<?= $this->checkCalculatorData(0, 'OtherCosts', 0, $data) ?>" type="text">
		</td>
		<td>
			<input name="Calculator[OtherCosts][0][]" class="calculator-value calc-value"
				   value="<?= $this->checkCalculatorData(0, 'OtherCosts', 1, $data) ?>" type="text">
		</td>
		<td class="result calc-row-result"></td>
		<td class="result calc-row-result-percent"></td>
		<td>
			<input name="Calculator[OtherCosts][0][]" data-changed="false" class="calculator-value calc-value"
				   value="<?= $this->checkCalculatorData(0, 'OtherCosts', 2, $data) ?>" type="text">
		</td>
		<td></td>
	</tr>
	</tbody>
	<tr class="calculator-all-results all-results">
		<td><?=Yii::t('FairsModule.fairs', 'TOTAL BUDGET FOR THE EXHIBITION')?>:</td>
		<td class="result-plan"></td>
		<td class="result-fact"></td>
		<td class="result-deviation"></td>
		<td class="result-deviation-percent"></td>
		<td class="result-variant"></td>
		<td></td>
	</tr>

</table>

<br>

<script type="text/javascript">
	ROICalculator.objectNames.push({name: 'DesiredLevelOfEfficiency'});
	ROICalculator.objectNames.push({name: 'CostOfGoodsSold'});
	ROICalculator.objectNames.push({name: 'IncomeFromExhibition'});
	ROICalculator.objectNames.push({name: 'ROI'});
</script>
<table id="oe-calculator" border="1">
	<tr class="header">
		<td><?=Yii::t('FairsModule.fairs', 'PERFORMANCE EVALUATION OF PARTICIPATION')?>:</td>
		<td><?=Yii::t('FairsModule.fairs', 'Amount')?></td>
		<td><?=Yii::t('FairsModule.fairs', 'Cost')?></td>
		<td><?=Yii::t('FairsModule.fairs', 'Total fact')?></td>
		<td><?=Yii::t('FairsModule.fairs', 'Total variant')?> 1</td>
	</tr>
	<tr class="process-block">
		<td class="field-name"><?=Yii::t('FairsModule.fairs', 'min. level of efficiency \ breakeven point')?></td>
		<td>
		</td>
		<td>
		</td>
		<td>
			<input class="calculator-fact" value="" type="text">
		</td>
		<td>
			<input class="calculator-variant" value="" type="text">
		</td>
	</tr>
	<tr class="process-block" data-object="DesiredLevelOfEfficiency" data-index="0">
		<td class="field-name"><?=Yii::t('FairsModule.fairs', 'the desired level of efficiency')?></td>
		<td>
			<input name="Calculator[DesiredLevelOfEfficiency][0][]" class="calculator-count"
				   value="<?= $this->checkCalculatorData(0, 'DesiredLevelOfEfficiency', 0, $data) ?>" type="text">
		</td>
		<td>
			<input name="Calculator[DesiredLevelOfEfficiency][0][]" class="calculator-price"
				   value="<?= $this->checkCalculatorData(0, 'DesiredLevelOfEfficiency', 1, $data) ?>" type="text">
		</td>
		<td class="result calculator-results"></td>
		<td class="result calculator-results"></td>
	</tr>
	<tr class="process-block" data-object="CostOfGoodsSold" data-index="1">
		<td class="field-name"><?=Yii::t('FairsModule.fairs', 'the cost of production \ purchase')?></td>
		<td>
			<input name="Calculator[CostOfGoodsSold][0][]" class="calculator-count"
				   value="<?= $this->checkCalculatorData(0, 'CostOfGoodsSold', 0, $data) ?>" type="text">
		</td>
		<td>
			<input name="Calculator[CostOfGoodsSold][0][]" class="calculator-price"
				   value="<?= $this->checkCalculatorData(0, 'CostOfGoodsSold', 1, $data) ?>" type="text">
		</td>
		<td class="result calculator-results"></td>
		<td class="result calculator-results"></td>
	</tr>
	<tr class="process-block calculator-all-results" data-object="IncomeFromExhibition" data-index="2">
		<td class="field-name"><?=Yii::t('FairsModule.fairs', 'INCOME FROM SHOWS')?>:</td>
		<td></td>
		<td></td>
		<td class="result-number"></td>
		<td class="result-number"></td>
	</tr>
	<tr class="process-block calculator-all-results" data-object="ROI" data-index="3">
		<td class="field-name"><?=Yii::t('FairsModule.fairs', 'ROI \ Effective Exhibiting')?></td>
		<td></td>
		<td></td>
		<td class="result-percent result-fact"></td>
		<td class="result-percent result-variant"></td>
	</tr>
</table>
<?= TbHtml::endForm() ?>
<script type="text/javascript">
$(function () {
	window['calc'] = (
	{
		isNum: function (value) { // Вхождение только цыфр и плавающей точки
			return !/^\.|\d+\..*\.|[^\d\.{1}]/.test(value);
		},
		table: function ($elm, me) {
			var table = {
				$elm: $elm,
				blocks: [],
				data: {
					plan: 0,
					fact: 0,
					variant: 0,
					deviation: 0
				},
				calculate: function () {
					table.data = {
						plan: 0,
						fact: 0,
						variant: 0,
						deviation: 0
					};
					$.each(table.blocks, function (i, block) {
						table.data.plan += +block.data.plan;
						table.data.fact += +block.data.fact;
						table.data.variant += +block.data.variant;
					});
					table.data.deviation = table.data.plan - table.data.fact;

					var $oe = $('#oe-calculator');
					$('input.calculator-variant', $oe).val(table.data.variant);
					$('input.calculator-fact', $oe).val(table.data.fact);

					$elm.find('.all-results .result-plan').text(+table.data.plan);
					$elm.find('.all-results .result-fact').text(+table.data.fact);
					$elm.find('.all-results .result-variant').text(+table.data.variant);
					$elm.find('.all-results .result-deviation').text(+table.data.deviation);
					$elm.find('.all-results .result-deviation-percent').text(
						(table.data.deviation && table.data.plan
							? (+table.data.deviation / +table.data.plan * 100).toFixed(2)
							: 0
						) + '%'
					);

					console.log('table.data.fact', table.data.fact, !table.data.fact);
					$('input.calculator-fact').toggleClass('error', !table.data.fact);
					$('input.calculator-variant').toggleClass('error', !table.data.variant);
				}
			};
			$elm.find('.calc-block').each(function () {
				table.blocks.push(
					new me.block($(this), table, me)
				);
			});
			return table;
		},
		block: function ($elm, table, me) {
			var block = {
				table: table,
				$elm: $elm,
				$elms: {
					plan: $elm.find('.result-plan'),
					fact: $elm.find('.result-fact'),
					variant: $elm.find('.result-variant'),
					deviation: $elm.find('.result-deviation'),
					deviationPercent: $elm.find('.result-deviation-percent')
				},
				rows: [],
				data: {
					plan: 0,
					fact: 0,
					variant: 0,
					deviation: 0
				},
				calculate: function () {
					block.data = {
						plan: 0,
						fact: 0,
						variant: 0,
						deviation: 0
					};
					$.each(block.rows, function (i, row) {
						block.data.plan += +row.values[0].value;
						block.data.fact += +row.values[1].value;
						block.data.variant += +row.values[2].value;
					});
					block.data.deviation = block.data.plan - block.data.fact;

					block.$elms.plan.text(block.data.plan);
					block.$elms.fact.text(block.data.fact);
					block.$elms.variant.text(block.data.variant);
					block.$elms.deviation.text(block.data.deviation);
					block.$elms.deviationPercent.text(
						(block.data.deviation && block.data.plan
							? (block.data.deviation / block.data.plan * 100).toFixed(2)
							: 0
						) + '%'
					);

					table.calculate();
				}
			};
			$('.calc-row', $elm).each(function () {
				block.rows.push(
					new me.row($(this), block, me)
				);
			});
			return block;
		},
		row: function ($elm, block, me) {
			var row = {
				block: block,
				$elm: $elm,
				$elms: {
					$plan: $elm.find('.calc-plan'),
					$result: $elm.find('.calc-row-result'),
					$percent: $elm.find('.calc-row-result-percent')
				},
				values: [],
				result: 0,
				calculate: function () {
					this.result = this.values[0].value - this.values[1].value;
					row.$elms.$result.text(
						this.result
					);

					var percent = this.result ? (this.result / this.values[0].value * 100) : 0;
					row.$elms.$percent.text(
						!percent ? '' : percent.toFixed(2) + '%'
					);
					block.calculate();
				}
			};
			$elm.find('.calc-value').each(function () {
				row.values.push(
					new me.value($(this), row, me)
				);
				var $input = $(this);
				if ($input.hasClass('calc-plan') && typeof row.values[2] != 'undefined') {
					if ($(this).closest('.calc-row').find('.calc-variant').data('changed') == false) {
						row.values[2].value = this.value;
					}
				}
			});
			return row;
		},
		value: function ($elm, row, me) {
			var value = {
				row: row,
				$elm: $elm,
				value: +$elm.val(),
				set: function (value) {
					this.value = +value;
					if ($elm.hasClass('calc-plan')) {

						$elm.toggleClass('error', !this.value);

						var $variant = $elm.closest('.calc-row').find('.calc-variant');
						if ($variant.data('changed') == false) {
							row.values[2].value = this.value;
							$elm.closest('.calc-row').find('.calc-variant').val(this.value);
						}
					}
					row.calculate();
				}
			};
			if ($elm.hasClass('calc-plan')) {
				var $variant = $elm.closest('.calc-row').find('.calc-variant');
				if ($variant.data('changed') == false) {
					$variant.val($elm.val());
				}
			}
			$elm.bind('keyup', function () {
				var $inp = $(this);
				if (me.isNum($inp.val())) {
					if ($inp.hasClass('error')) {
						$inp.removeClass('error')
					}
					if ($elm.hasClass('calc-variant')) {
						$elm.data('changed', true);
					}
					value.set($inp.val());
				} else {
					$inp.addClass('error')
				}
			});
			return value;
		},
		init: function ($table) {
			var me = this;
			me.data = new me.table($table, me);
			$.each(me.data.blocks, function () {
				$.each(this.rows, function () {
					this.calculate();
				});
			});
			return this;
		}
	}
	).init(
		$('#fair-calculator')
	);

	ROICalculator.table = $('#oe-calculator');

	$('input', ROICalculator.table).bind('keyup', function () {
		ROICalculator.processBlock = $(this).parent('td').parent('.process-block');

		ROICalculator.priceCount = ROICalculator.processBlock.find('.calculator-price');
		ROICalculator.countInput = ROICalculator.processBlock.find('.calculator-count');

		if (this.value == '') {
			$(this).removeClass('error');
			if ($(this).hasClass('calculator-price')) {
				if (ROICalculator.countInput.val() != '') {
					ROICalculator.hasError = true;
					if (!ROICalculator.priceCount.hasClass('error')) {
						ROICalculator.priceCount.addClass('warning');
					}
				} else {
					ROICalculator.hasError = false;
					ROICalculator.clearErrors(ROICalculator.priceCount);
					ROICalculator.clearErrors(ROICalculator.countInput);
				}
			} else if ($(this).hasClass('calculator-count')) {
				if (ROICalculator.priceCount.val() != '') {
					ROICalculator.hasError = true;
					if (!ROICalculator.countInput.hasClass('error')) {
						ROICalculator.countInput.addClass('warning');
					}
				} else {
					ROICalculator.hasError = false;
					ROICalculator.clearErrors(ROICalculator.countInput);
					ROICalculator.clearErrors(ROICalculator.priceCount);
				}
			}
		} else if (ROICalculator.validateInputs(this)) {
			ROICalculator.hasError = true;
		} else {
			ROICalculator.clearErrors($(this));

			if ($(this).hasClass('calculator-price')) {
				if (ROICalculator.countInput.val() == '') {
					if (!ROICalculator.countInput.hasClass('error')) {
						ROICalculator.countInput.addClass('warning');
					}
					ROICalculator.hasError = true;
				} else {
					ROICalculator.hasError = false;
					ROICalculator.clearErrors(ROICalculator.countInput);
				}
			} else if ($(this).hasClass('calculator-count')) {
				if (ROICalculator.priceCount.val() == '') {
					if (!ROICalculator.priceCount.hasClass('error')) {
						ROICalculator.priceCount.addClass('warning');
					}
					ROICalculator.hasError = true;
				} else {
					ROICalculator.hasError = false;
					ROICalculator.clearErrors(ROICalculator.priceCount);
				}
			}
		}

		if (!ROICalculator.hasError) {
			ROICalculator.data = {};
			$.each(ROICalculator.objectNames, function (index, object) {
				ROICalculator.data[object.name] = {
					result: 0,
					count: 0,
					price: 0
				};
				$('[data-object="' + object.name + '"] input', ROICalculator.table).filter(':not([value=""])').each(function (i, elm) {
					if ($(elm).hasClass('calculator-count')) {
						ROICalculator.data[object.name].count = +elm.value;
					} else if ($(elm).hasClass('calculator-price')) {
						ROICalculator.data[object.name].price = +elm.value;
					}
				});
				if (ROICalculator.data[object.name].count > 0 && ROICalculator.data[object.name].price > 0) {
					ROICalculator.data[object.name].result = ROICalculator.data[object.name].count * ROICalculator.data[object.name].price;
				}
			});

			if (typeof ROICalculator.data !== 'undefined') {
				$.each(ROICalculator.data, function (name, object) {
					var summary = object.result;
					var $object = $('[data-object="' + name + '"]', ROICalculator.table);
					$object.find('.result').text(summary);
				});
				if (ROICalculator.data['DesiredLevelOfEfficiency'].result > 0 && ROICalculator.data['CostOfGoodsSold'].result > 0) {

					$('[data-object="IncomeFromExhibition"] .result-number', ROICalculator.table).text(
						ROICalculator.data['DesiredLevelOfEfficiency'].result - ROICalculator.data['CostOfGoodsSold'].result
					);

					var fact = +$('.calculator-fact', ROICalculator.table).val();
					$('.calculator-fact', ROICalculator.table).toggleClass('error', !fact);

					var variant = +$('.calculator-variant', ROICalculator.table).val();
					$('.calculator-variant', ROICalculator.table).toggleClass('error', !variant);

					var factValue = (
						(
							ROICalculator.data['DesiredLevelOfEfficiency'].result - ROICalculator.data['CostOfGoodsSold'].result
						)
						/ fact * 100
					);
					var variantValue = (
						(
							ROICalculator.data['DesiredLevelOfEfficiency'].result - ROICalculator.data['CostOfGoodsSold'].result
						)
						/ variant * 100
					);

					factValue = factValue.toString() == 'NaN' ? 0 : factValue;
					$('[data-object="ROI"] .result-percent.result-fact', ROICalculator.table).text(
						factValue.toString() == 'NaN' ? 0 : factValue.toFixed(1) + '%'
					);

					variantValue = variantValue.toString() == 'NaN' ? 0 : variantValue;
					$('[data-object="ROI"] .result-percent.result-variant', ROICalculator.table).text(
						 variantValue.toFixed(1) + '%'
					);

				}
			}
		}
	}).trigger('keyup');

})
;
</script>