<?php
/**
 * @var CController $this
 * @var Fair $model
 * @var CWebApplication $app
 * @var array $data
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerScriptFile(
	$am->publish($app->theme->basePath . '/assets' . '/js/fairCalculator.js')
);
$cs->registerCssFile(
	$am->publish($app->theme->basePath . '/assets' . '/css/fairCalculator.css')
);
$ctrl = $this;
$ctrl->widget('application.modules.fairs.components.FieldSetView', array(
	'header' => Yii::t('FairsModule.fairs', 'Evaluate your expenses and calculate the cost of participation in the exhibition'),
	'items' => array(
		array(
			'application.modules.fairs.components..HtmlView',

			'content' => function ($owner) use($data, $model) {
					/** @var FairController $owner */
					?>
					<?= TbHtml::label(Yii::t('FairsModule.fairs', 'Area, sq.m'), 'square', array('class' => 'square-label')) ?>
					<div class="error-box"></div>
					<?= TbHtml::numberField('square') . '<br />' ?>
					<?php if (Yii::app()->user->isGuest) {
						echo TbHtml::linkButton(Yii::t('FairsModule.fairs', 'Calculate'), array('color' => TbHtml::BUTTON_COLOR_SUCCESS, 'url' => $owner->createUrl('auth/default/login')));
					} else {
						echo TbHtml::button(Yii::t('FairsModule.fairs', 'Calculate'), array('color' => TbHtml::BUTTON_COLOR_SUCCESS, 'id' => 'calc-button'));
					} ?>
					<div id="result-list" style="display: none">
						<div class="result-block">
							<div class="name"></div>
							<div class="result" style="display: inline-block"></div>
							<div class="percent" style="display: inline-block"></div>
						</div>
					</div>
					<div class="calc-summary"></div>
					<style>
						.modal-body {
							max-height: 500px !important;
						}
					</style>
					<?php
					$owner->widget('bootstrap.widgets.TbModal',array(
						'id' => 'calculator-modal',
						'header' => Yii::t('FairsModule.fairs', 'BUDGET EXPENDITURES FOR PARTICIPATION IN EXHIBITION'),
						'content' => $owner->renderPartial('_budget_calculator', array(
								'data' => $data,
								'model' => $model,
							), true),
						'htmlOptions' => array(
							'style' => 'width: 80%; min-height: 400px; left:25%'
						)
					));
					echo TbHtml::button(Yii::t('FairsModule.fairs', 'Work with the budget'),
						array(
							'data-toggle' => 'modal',
							'data-target' => '#calculator-modal',
						));

				}
		),
	)
));
$expenses = array();
/** @var AdditionalExpenses[] $AEs */
$AEs = AdditionalExpenses::model()->findAll();
if (!empty($AEs)) {
	foreach ($AEs as $AE) {
		$expenses[$AE->name]['percent'] = $AE->percent;
		$expenses[$AE->name]['exPercent'] = $AE->exPercent;
	}
}
$fairData = array();
$fairData[$model->id]['pPrice'] = $model->participationPrice;
$fairData[$model->id]['rFee'] = $model->registrationFee;
?>
<script type="text/javascript">
	$(function () {
		fair.id = '<?=$model->id?>';
	});

	function objectData() {
		var data = <?= json_encode($expenses)?>;
		return data;
	}
	function objectFairData() {
		var fairData = <?= json_encode($fairData)?>;
		return fairData;
	}
</script>