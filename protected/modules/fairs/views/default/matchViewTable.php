<?php
/**
 * @var Fair $data
 * @var bool $enableButton
 * @var array $fairIds
 */
?>

<div class="b-switch-list-view__item">
	<div class="b-switch-list-view__cell b-switch-list-view__cell--fair-name">
		<?= TbHtml::link(
			'<span data-max-character="76">' . $data->getSeoTitle() . '</span>',
			Yii::app()->createUrl('fair/default/view', array('id' => $data->id)),
			[
				'title' => $data->getSeoTitle()
			]
		); ?>
	</div>
	<div title="<?= Fair::getTitleRatingIcon($data) ?>"
		 class="b-switch-list-view__cell rating-logo rating-logo-type-<?= $data->rating ?>">
		<div></div>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--date">
		<?= Fair::getDateRange($data->beginDate, $data->endDate) ?>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--industry b-switch-list-view__cell--hide-as-block">
		<?php
		$text = null;
		foreach (\yii\helpers\ArrayHelper::map($data->fairHasIndustries, 'id', 'industry.name') as $k => $name) {
			$text .= (!$text ? '' : '; ') . $name;
		}
		echo TbHtml::tag(
			'span', ['title' => $text],
			(mb_strlen($text, 'UTF-8') > 34
				? mb_substr($text, 0, 35, 'UTF-8') . '...'
				: ($text ?: Yii::t('FairsModule.fairs', 'Not set'))
			)
		);
		?>
	</div>
	<div class="b-switch-list-view__cell b-switch-list-view__cell--fair-region">
		<?php if ($data->exhibitionComplexId && null !== ($EC = $data->exhibitionComplex)) {
			$rName = $EC->region ? $EC->region->name : null;
			$cName = $EC->city ? $EC->city->name : null;
			$title = ($rName ?: Yii::t('FairsModule.fairs', 'Not set')) . ' / ' . ($cName ?: Yii::t('FairsModule.fairs', 'Not set'));
			echo '<a href="#" title="'.$title.'"><span data-max-character="21">'.$rName.'</span> / <span>'.$cName.'</span></a>';
		} ?>
	</div>

	<div class="b-switch-list-view__cell b-switch-list-view__cell--actions">
		<?php
		echo TbHtml::linkButton('<input type="hidden" value="' . $data->id . '" name="Match[]" class="add-to-match" id="add-to-mach-' . $data->id . '">', [
			'url' => 'javascript://',
			'class' => 'b-button js-add-to-mach d-bg-secondary-input f-text--tall d-text-light b-list-active-button-1'.
				(in_array($data->id, [3,4]) ? ' d-bg-success compare-checked' : ' d-bg-success--hover'),
			'data-icon' => '&#xe3d3;', //d-bg-success
			'encode' => false,
			'title' => Yii::t('FairsModule.fairs', 'compare'),
		]);
		echo TbHtml::linkButton('', [
			'url' => '#',
			'class' => 'b-button d-bg-secondary-input f-text--tall d-text-light b-list-active-button-2 d-bg-warning-dark--hover',
			'data-icon' => '&#xe3ca;', //d-bg-warning
			'encode' => false,
			'title' => Yii::t('FairsModule.fairs', 'save to bookmarks'),
		]);
		$this->widget('application.modules.fairs.components.DropDownContent', array(
			'content' => 'Я - Orgplan.</br>Я помогаю участвовать в выставках.</br></br>Something here</br>
						Something else here</br>And one more action</br>Action on hover</br>Something here</br>
						Something else here</br>',
			'label' => 'Возможности',
			'htmlOptions' => array(
				'labelOptions' => array(
					'class' => 'b-button js-drop-button drop-button d-bg-warning-dark d-text-light'
				)
			),
		));
		?>
	</div>
</div>