<?php
/**
 * @var CWebApplication $app
 * @var CController $this
 * @var Fair $model
 * @var Organizer $organizer
 */
?>
<div class="cols-row offset-small-t-1">
    <div class="col-md-12">
        <div class="cols-row">
            <div class="col-md-12 f-text--large p-offset-top-4 p-offset-bottom-3">
                <h2 class="page-fair__contact-table--header-h2">
                    <?= Yii::t('FairsModule.fairs', 'contact fair organizer') ?>
                </h2>
            </div>
            <div class="col-md-12">
                <?php
                if (null !== $model) {
                    $this->widget(
                        'application.modules.fairs.components.ContactDetailView',
                        array(
                            'data' => $model,
                            'organizer' => $organizer,
                            'htmlOptions' => array(
                                'class' => 'b-detail-view__table--simple p-offset-bottom-53 fair-tables fair-tables-contact',
                            ),
                        )
                    );
                    ?>
                    <?php
                    if ($model->exhibitionComplex) {
                        ?>
                        <h2 class="page-fair__contact-table--header-h2"><?= Yii::t('FairsModule.fairs', 'Contact of exhibition centre') ?></h2>
                        <?php
                        $this->renderPartial('application.modules.fairs.views.default.view._exhibition_contacts', array(
                            'model' => $model->exhibitionComplex,
                        ));
                    }
                } else {
                    echo Yii::t('FairsModule.fairs', 'Information not available');
                };
                ?>

            </div>
        </div>
    </div>
</div>
