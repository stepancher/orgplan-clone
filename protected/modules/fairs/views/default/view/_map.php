<?php
/**
 * @var CWebApplication $app
 * @var CController $this
 * @var ExhibitionComplex $model
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.fairs.assets'), false, -1, YII_DEBUG);
$cs->registerScriptFile('https://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU');
$cs->registerScriptFile($publishUrl . '/js/map/Map.js', CClientScript::POS_BEGIN);
?>
<div id="map" style="width: 100%; height: 400px; border: 1px solid #D3D2D0">
    <?= TbHtml::image($am->publish(Yii::getPathOfAlias('application.modules.fairs.assets') . '/img/ajax-loader.gif'), Yii::t('FairsModule.fairs', 'loading'), array(
        'title' => Yii::t('FairsModule.fairs', 'loading'),
        'style' => 'position:absolute; z-index: 99; display: none',
        'class' => 'ajax-loader'
    )) ?>
</div>
<?php
if (null !== $model) : ?>
    <script type="text/javascript">
        $(document).ready(function () {
            ymaps.ready(function () {
                new Map({
                    url: "<?= $this->createUrl('/exhibitionComplex/getMapData') ?>",
                    coordinates: "<?= $model->coordinates ?>",
                    hintContent: "Местоположение выставочного комплекса"
                });
            });
        });
    </script>
<?php endif; ?>
