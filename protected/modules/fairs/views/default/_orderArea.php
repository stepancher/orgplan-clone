<?php
/**
 * @var CController $this
 * @var Organizer $model
 */
$this->widget(
	'application.modules.fairs.components.FieldSetView',
	array(
		'header' => Yii::t('FairsModule.fairs', 'Contact the organizer'),
		'items' => array(
			array(
				'application.modules.fairs.components.OrganizerDetailView',
				'data' => $model,
			),
		)
	)
);