<?php
/**
 * @var CController $this
 * @var Fair $model
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerScriptFile(
    $am->publish($app->theme->basePath . '/assets' . '/js/fairCalculator.js'), CClientScript::POS_END);
$cs->registerCssFile(
    $am->publish($app->theme->basePath . '/assets' . '/css/fairCalculator.css'), CClientScript::POS_END);
$ctrl = $this;
$this->widget(
    'bootstrap.widgets.TbModal',
    array(
        'id' => 'modal-calculator',
        'closeText' => '<i class="icon" data-icon=""></i>',
        'htmlOptions' => array(
            'class' => 'modal--calculator'
        ),
        'header' => $this->renderPartial('application.modules.fairs.views.default.modal._header', array(), true),
        'content' => $this->renderPartial('application.modules.fairs.views.default.modal._content', array(), true),
    )
);
$course = $model->currency && $model->currency->course ? $model->currency->course : 1;
?>
<script type="text/javascript">
    $(function () {
        new Calculator({
            data: <?=CJavaScript::encode([
                'pPrice' => $model->participationPrice * $course,
                'rFee' => $model->registrationFee * $course,
                'errorNumText' => Yii::t('FairsModule.fairs', 'You entered is not a number')
            ])?>
        });
    });
</script>