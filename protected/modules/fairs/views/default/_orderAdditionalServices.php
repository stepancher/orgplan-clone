<?php
/**
 * @var CController $this
 * @var ExhibitionComplex $model
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerScriptFile(
	$am->publish($app->theme->basePath . '/assets' . '/js/map/ya_map.js')
);
?>
	<script type="text/javascript">
		$(function () {
			ExhibitionComplex.position = "<?=$model->coordinates ? : '55.76, 37.64'?>";
			ExhibitionComplex.url = "<?=Yii::app()->createUrl('exhibitionComplex/getMapData')?>";
			ymaps.ready(ExhibitionComplex.actionView.init);
		})
	</script>
<?php

$this->widget(
	'application.modules.fairs.components.FieldSetView',
	array(
		'items' => array(
			array(
				'application.modules.fairs.components.ActionsView',
				'items' => array(
					'orderTickets' => $app->user->getState('role') !== User::ROLE_ORGANIZERS && !$app->user->isGuest ? array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('FairsModule.fairs', 'Order tickets'),
						'attributes' => array(
//								'url' => $this->createUrl('exhibitionComplex/view', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_WARNING,
							'disabled' => true,
						)
					) : array(),
					'orderHotel' => $app->user->getState('role') !== User::ROLE_ORGANIZERS && !$app->user->isGuest ? array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('FairsModule.fairs', 'Booking a hotel'),
						'attributes' => array(
//								'url' => $this->createUrl('exhibitionComplex/view', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_WARNING,
							'disabled' => true,
						)
					) : array(),
				)
			),
			array(
				'application.modules.fairs.components.HtmlView',
				'content' => function ($owner) {
					/** @@var CWebApplication $app */
					$app = Yii::app();
					$am = $app->getAssetManager();

					echo TbHtml::checkBoxControlGroup('hotel', false, array('label' => Yii::t('FairsModule.fairs', 'Hotels'), 'class' => 'map-request'));
					echo TbHtml::checkBoxControlGroup('landmark', false, array('label' => Yii::t('FairsModule.fairs', 'Sights'), 'class' => 'map-request'));
					echo TbHtml::checkBoxControlGroup('cafe', false, array('label' => Yii::t('FairsModule.fairs', 'Cafe'), 'class' => 'map-request'));
					echo '<div id="map" style="width: 100%; height: 400px">';
					echo TbHtml::image($am->publish($app->theme->basePath . '/assets' . '/img/ajax-loader.gif'), Yii::t('FairsModule.fairs', 'loading'), array(
						'title' => Yii::t('FairsModule.fairs', 'loading'),
                        'style' => 'position:absolute; z-index: 99; display: none',
						'class' => 'ajax-loader'
					));
					echo '</div>';
				}
			)
		)
	)
);