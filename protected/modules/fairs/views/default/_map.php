<?php
/**
 * @var CController $this
 * @var Fair $model
 * @var ExhibitionComplex $exhibitionComplex
 * @var CWebApplication $app
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
?>
    <div data-tab-base="fair-redactor" data-tab-target="map">
        <div class="cols-row">
            <div class="col-md-12">
                <div class="cols-row map-checkbox">
                    <div class="col-md-4">
                        <div class="cols-row">
                            <div class="col-md-12">
                                <?= $exhibitionComplex && $exhibitionComplex->name ? $exhibitionComplex->name :
                                    Yii::t('FairsModule.fairs', 'Data not available') ?>
                            </div>
                            <div class="col-md-12">
                                <?php
                                if ($exhibitionComplex && $exhibitionComplex->cityId && null !== ($city = $exhibitionComplex->city)
                                ) {
                                    echo "$city->name, $exhibitionComplex->street<br /> ";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="Hotel-checkbox" class="checkbox">
                            <input type="checkbox" id="Hotel-checkbox" class="map-request" name="hotel">
                            <?= Yii::t('FairsModule.fairs', 'Hotels') ?>
                        </label>

                        <label for="Landmark-checkbox" class="checkbox">
                            <input type="checkbox" id="Landmark-checkbox" class="map-request" name="landmark">
                            <?= Yii::t('FairsModule.fairs', 'Sights') ?>
                        </label>

                        <label for="Cafe-checkbox" class="checkbox">
                            <input type="checkbox" id="Cafe-checkbox" class="map-request" name="cafe">
                            <?= Yii::t('FairsModule.fairs', 'Cafe') ?>
                        </label>
                    </div>
                </div>
                <div id="map" style="width: 100%; height: 400px; border: 1px solid #D3D2D0">
                    <?= TbHtml::image($am->publish($app->theme->basePath . '/assets' . '/img/ajax-loader.gif'),Yii::t('FairsModule.fairs', 'loading'), array(
                        'title' => Yii::t('FairsModule.fairs', 'loading'),
                        'style' => 'position:absolute; z-index: 99; display: none',
                        'class' => 'ajax-loader'
                    )) ?>
                </div>
            </div>
        </div>
    </div>

<?php
if (null !== $exhibitionComplex) : ?>
    <script type="text/javascript">
        $(document).ready(function(){
            ymaps.ready(function () {
                new Map({
                    url: "<?= $this->createUrl('exhibitionComplex/getMapData') ?>",
                    coordinates: "<?= $exhibitionComplex->coordinates ?>",
                    hintContent: "Местоположение высавочного комплекса"
                });
            });
        });
    </script>
<?php endif; ?>