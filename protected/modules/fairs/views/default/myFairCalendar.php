<?php
/**
 * @var CController $this
 * @var CalendarExhibition $model
 */


Yii::import('application.widgets.CalendarFairGridView');

$ctrl = $this;
$ctrl->widget('application.modules.fairs.components.FieldSetView', array(
	'items' => array(
		array(
			'ext.widgets.ActionsView',
			'items' => array(
				'add' => User::checkAdministrationRoles() || Yii::app()->user->isGuest
						? array()
						: array(
							TbHtml::BUTTON_TYPE_LINK,
							Yii::t('FairsModule.fairs', 'Add all the tasks'),
							array(
								'url' => $this->createUrl('task/addToCalendarAll', array('id' => $model->id)),
								'color' => TbHtml::BUTTON_COLOR_SUCCESS
							)
						)
			)
		),
		array(
			'application.widgets.CalendarFairGridView',
			'model' => CalendarExhibition::model(),
			'fair' => $model,
			'columnsAppend' => array(
				'buttons' => array(
					'class' => 'bootstrap.widgets.TbButtonColumn',
					'template' => '<nobr>{add}</nobr>',
					'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
					'buttons' => array(
						'add' => array(
							'label' => TbHtml::icon(TbHtml::ICON_BELL, array('color' => TbHtml::ICON_COLOR_WHITE)),
							'url' => function ($data) use ($ctrl, $model) {
								return $ctrl->createUrl('task/addToCalendar', array("id" => $data->id, 'id' => $model->id));
								},
							'options' => array('title' => Yii::t('FairsModule.fairs', 'Add to Calendar'), 'class' => 'btn btn-small btn-primary'),
							'visible' => User::checkAdministrationRoles() || Yii::app()->user->isGuest ? 'false' : 'true'
						)
					)
				)
			)
		),
	)
));

