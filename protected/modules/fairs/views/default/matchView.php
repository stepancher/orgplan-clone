<?php
Yii::import('application.modules.fairs.components.MatchFairGridView');
/**
 * @var FairController $this
 * @var CSqlDataProvider $dataProvider
 * @var CWebApplication $app
 * @var CHttpRequest $request
 */
$app = Yii::app();
$request = $app->getRequest();

$ctrl = $this;
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.fairs.assets'), false, -1, YII_DEBUG);
$cs->registerScriptFile($publishUrl . '/js/fair/list/script.js', CClientScript::POS_END);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js', CClientScript::POS_END);
$cs->registerScript(
    'js-b-back-history',
    '$(".js-b-back-history").click(function(){$.get(' . json_encode($this->createUrl('matchClear')) . ')})');

$ctrl = $this;
$models = $dataProvider->getData();
$returnUrl = $request->getParam('returnUrl', $this->createUrl('/search', ['sector' => 'fair']))
?>
<div class="header">
    <div class="col-md-12">
        <?= CHtml::link(
            Yii::t('FairsModule.fairs', 'Back to search'),
            $returnUrl,
            [
                'class' => 'offset-small-t-2 js-b-back-history b-button  b-button--no-margin offset-small-b-2 b-button d-bg-base d-bg-bordered d-bg-input--hover d-text-dark btn button-back'
            ]
        ) ?>


        <?php
            $description = '';/*Yii::t('FairsModule.fairs','Joined').': ';*/
//          foreach ($models as $k => $model) {
//              $description .= $models[$k]->name;
//              if (next($models)) {
//                  $description .= '<span class="b-header__splash">/</span>';
//              }
//          }
            $this->renderPartial('/_blocks/_b-header', array(
                'header' => Yii::t('FairsModule.fairs', 'Compare'),
                'description' => $description
            ));
        ?>
    </div>
    <div style="clear: both"></div>
</div>
<?php
/*$this->widget('application.components.widgets.SearchListView', [
	'dataProvider' => $dataProvider,
	'htmlOptions' => [
		'class' => 'b-switch-list-view__items js-updated-block b-switch-list-view js-switch-list-view-fair b-switch-list-view--as-' . $switcherType,
		'data-switcher' => 'js-switch-list-view-fair',
	],
	'sorterCssClass' => 'b-switch-list-view__sorter',
	'sortableAttributes' => $sortableAttributes + [''],
	'itemView' => $itemView,
	'template' => "{sorter}\n{items}",
]);*/
?>
<div class="b-switch-list-view">
    <div class="b-search-loader-block">
        <?= TbHtml::image($publishUrl . '/img/loader/ajax-loader.gif', 'Загрузка', ['class' => 'js-page-search__loader', 'title' => 'Загрузка']) ?>
    </div>
    <?php
    $ctrl->widget('application.modules.fairs.components.FieldSetView', array(
        'items' => array(
            array(
                'application.modules.fairs.components.MatchFairGridView',
                'dataProvider' => $dataProvider,
                'emptyText' => 'В сравнении выставок нет. Выбери выставки для сравнения, нажав на иконку, затем повтори процедуру, нажав на кнопку "Сравнить".',
                'model' => new Fair('sortMatch'),
                'template' => '{items}',
                'enableFilter' => false,
                'afterAjaxUpdate' => 'js:function() { favorite(); }',
                'htmlOptions' => [
                    'class' => 'b-grid-view b-grid-view__table--match',
                ],
            ),
        )
    ));
    ?>
</div>
