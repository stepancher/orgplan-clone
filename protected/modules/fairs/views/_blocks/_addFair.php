<?php
/**
 * @var integer $fairId
 */
$publishUrl = Yii::app()->assetManager->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
Yii::app()->getClientScript()->registerCssFile($publishUrl . '/less/global.css');

$isMyFair = NULL; //MyFair::model()->findByAttributes(['userId' => Yii::app()->user->id, 'fairId' => $fairId]);

if ($isMyFair === NULL) {
    $isMyFair = false;
} else {
    $isMyFair = true;
}

$controller = Yii::app()->controller->id;

if (Yii::app()->user->isGuest) { ?>
    <div class="add-fair">
        <a data-target="#modal-sign-up" data-toggle="modal" class="text-link" href="#"><?= Yii::t('FairsModule.fairs', 'Start preparing') ?></a>
        <a data-target="#modal-sign-up" data-toggle="modal" class="btn-icon" data-icon="" href="#"></a>
    </div>
<?php } elseif (Yii::app()->user->role == User::ROLE_USER || Yii::app()->user->role == User::ROLE_EXPONENT_SIMPLE || Yii::app()->user->role == User::ROLE_EXPONENT) { ?>
    <div class="add-fair">
        <a class="text-link" href="<?= Yii::app()->createUrl('exponentPrepare/', ['fairId' => $fairId]) ?>">
            <?= Yii::t('FairsModule.fairs', 'Start preparing') ?>
        </a>
        <a class="btn-icon" data-icon=""
           href="<?= Yii::app()->createUrl('exponentPrepare/', ['id' => $fairId]) ?>"></a>
    </div>
<?php }