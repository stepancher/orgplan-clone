<?php
/**
 * @var Fair $data
 * @var bool $enableButton
 */

if (Yii::app()->request->getParam('ctrl')) {
    $isSearchPage = Yii::app()->request->getParam('ctrl') == 'search';
} else {
    $isSearchPage = Yii::app()->controller->id == 'search';
}

$publishUrl = Yii::app()->assetManager->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
Yii::app()->getClientScript()->registerCssFile($publishUrl . '/less/global.css');

$module = NULL;
if(isset(Yii::app()->controller->getModule()->id) && !empty(Yii::app()->controller->getModule())){
    $module = Yii::app()->controller->getModule()->id;
}

$isMyFairsPage = $module == 'workspace' && in_array(Yii::app()->controller->action->id, ['index', 'archive']);
$isMyFairsPageArchive = $module == 'workspace' && Yii::app()->controller->action->id == 'archive';
$isBackoffice = $module == 'backoffice';

if(!isset($fairMatches)){
    $fairMatches = [];
}

$matches = Yii::app()->session->get('matches', []);

if (Yii::app()->user->isGuest) {
    $checked = isset($matches[$data->id]);
} else {
    $checked = in_array($data->id, $fairMatches);
}

$url = Yii::app()->createUrl('fair/default/view', array('id' => $data->id));

/**
$url = Yii::app()->createUrl('fair/html/view', [
    'urlFairName' => $data->shortUrl,
    'langId' => Yii::app()->language,
]);
**/

if ($isMyFairsPage) {
    if ($isMyFairsPageArchive) {
        $url = 'javascript://';
    }
    else {
        $url = Yii::app()->createUrl('/workspace/panel/panel', array('fairId' => $data->id));
    }
}
if ($isBackoffice) {
    $url = Yii::app()->createUrl('/backoffice/panel/panel', array('fairId' => $data->id));
}

?>

<div class="b-switch-list-view__item b-switch-list-view__item-fair" itemscope itemtype="http://schema.org/Service">

    <div class="b-switch-list-view__cell b-switch-list-view__cell--fair-name upper-case <?= $isMyFairsPage ? 'my-fairs-link' : '' ?>" itemprop="name">
        <?php echo TbHtml::link(
            $data->name,
            $url,
            [
                'title' => $data->name,
                'class' => 'trim-clamp',
                'data-max-line' => 4,
                'data-clamping' => 0,
                'data-fair-id' => $data->id,
                'itemprop' => 'url'
            ]
        );?>
    </div>
    
    <div
        class="b-switch-list-view__cell b-switch-list-view__cell--type b-switch-list-view__cell--hide-as-row">
        <?=Yii::t('FairsModule.fairs', 'fair')?>
    </div>
    
    <div title="<?= Fair::getTitleRatingIcon($data) ?>"
         class="b-switch-list-view__cell rating-logo rating-logo-type-<?= $data->rating ?>-<?='rus'?> <?= !$isSearchPage ? 'rating-logo-pick-up' : '' ?>">
        <div></div>
    </div>
    
    <div class="b-switch-list-view__cell b-switch-list-view__cell--date">
        <span itemprop="hoursAvailable" content="'<?= date('d.m.Y', strtotime($data->beginDate)) ?>'">
            <?= Fair::getDateRange($data->beginDate, $data->endDate) ?>
        </span>
    </div>
    
    <div class="b-switch-list-view__cell b-switch-list-view__cell--industry b-switch-list-view__cell--hide-as-block">
        <?php $industriesList = implode('; ', $data->combineIndustries());?>
        <?php echo TbHtml::tag('span', ['title' => $industriesList], $industriesList ?: Yii::t('FairsModule.fairs', 'Not set'));?>
    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--fair-region">

        <?php if(isset($data->regionName)) :?>
            <span itemprop="name">
                <span itemprop="serviceArea">
                    <a itemprop="url" title="<?=$data->regionName;?>" 
                       href="<?=Yii::app()->createUrl('regionInformation/default/view', array('id' => $data->regionInformationId))?>">
                        <span class="b-switch-list-view__content--hide-as-row"><?=$data->cityName?> / </span> <?=$data->regionName?>
                    </a>
                </span>
            </span>
        <?php endif;?>

    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--fair-city b-switch-list-view__cell--hide-as-block">
        <a title="<?php echo $data->cityName;?>"
           href="<?=Yii::app()->createUrl('search/index', array(
               'sector' => 'fair',
               'cityId' => !empty($data->cityId) ? $data->cityId : NULL,
           ))?>">
            <?php echo $data->cityName;?>
        </a>
    </div>

    <div class="b-switch-list-view__cell b-switch-list-view__cell--actions">
        <?php if ($isSearchPage) :?>

            <?php echo TbHtml::linkButton('', [
                'url' => 'javascript://',
                'class' => implode(' ',[
                    'b-button', 
                    'js-add-to-mach', 
                    'd-bg-secondary-icon', 
                    'f-text--tall', 
                    'd-text-light', 
                    'b-list-active-button-1', 
                    'd-bg-success--hover',
                    !$checked ? '' : ' compare-checked d-bg-success',
                ]),
                'data-icon' => '&#xe3d3;',
                'data-id' => $data->id,
                'encode' => false,
                'title' => !$checked ? Yii::t('FairsModule.fairs', 'compare') : Yii::t('FairsModule.fairs', 'remove from the comparison'),
            ]);?>

        <?php elseif ($isMyFairsPage): ?>

            <?php echo TbHtml::linkButton('',[
                'url' => 'javascript://',
                'class' => implode(' ', [
                    'b-button', 
                    'd-bg-secondary-icon', 
                    'f-text--tall', 
                    'd-text-light', 
                    'b-list-active-button-2', 
                    'd-bg-danger--hover', 
                    'js-remove-item',
                ]),
                'data-icon' => '&#xe3c1;',
                'data-id' => $data->id,
                'data-target' => '#modal-remove',
                'encode' => false,
                'title' => Yii::t('FairsModule.fairs', 'Delete'),
            ]);?>

        <?php endif;?>

        <?php $this->renderPartial('application.modules.fair.views._blocks._addFair', ['fairId' => $data->id]); ?>

        <div class="<?= IS_MODE_DEV  && !$isBackoffice ? Fair::getBottomButtonClass($data->id) : 'orange-block' ?>">

            <?php if (IS_MODE_DEV && !$isBackoffice):?>

                <?php $textIconTime = Fair::getDataForBottomButton($data->id, $data->beginDate);?>

                <div class="b-switch-list-view__cell--bottom-button-text">
                    <div class="uplifting-text"><?= $textIconTime['text'] ?></div>
                </div>
                <div class="b-switch-list-view__cell--bottom-button-icon">
                    <?= $textIconTime['icon-time'] ?>
                </div>

            <?php endif; ?>

        </div>
    </div>
</div>

<?php if(!IS_MODE_DEV) : ?>
    <style>
        .b-switch-list-view .b-switch-list-view--as-block .b-switch-list-view__item.b-switch-list-view__item-fair{
            height: 205px !important;
        }
    </style>
<?php endif; ?>