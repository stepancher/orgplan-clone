$(function () {
	var $mainList = $('.main-results').show();
	var $industryList = $('.search_industry_results').hide();
	var $regionList = $('.search_region_results').hide();
	var $cityList = $('.search_city_results').hide();

	$('.filter-search-block__catalog a:not(.active)').click(function () {
		$('.filter-search-block__catalog a').not(this).removeClass('active');
		var $elm = $(this);
		$elm.addClass('active');
		$mainList.hide();
		$industryList.hide();
		$regionList.hide();
		$cityList.hide();
		if($elm.data('category') == 'industry') {
			$industryList.show();
		} else if($elm.data('category') == 'city') {
			$cityList.show();
		} else if($elm.data('category') == 'region') {
			$regionList.show();
		} else if($elm.data('category') == 'date') {
			$('.date-periods').addClass('active');
			$('.list-counts').removeClass('active');
			$('.search-fair-sort').val('beginDate').trigger('change');
			$mainList.show();
		}
	});
});