$(function() {
	$('.js-drop-down-content').each(function () {
		resizeContentBlock($(this))
	});

	/* SlimScroll*/
	$('.js-drop-box').each(function(){
		var $box = $(this);
		var $boxContent = $('<div/>').append($box.children());
		$box.empty().append($boxContent);
		$boxContent.slimScroll({
			color: '#000',
			size: '5px',
			height: '180px',
			alwaysVisible: true
		});
	});

	$(document).click(function (e) {
		var showBlock = $(e.target).closest('.js-drop-down-content').get(0);
		$('.js-drop-down-content').each(function () {
			if (this != showBlock) {
				$(this).removeClass('active');
			}
		});
	});
	$(document).delegate('.js-drop-down-content .js-drop-down-content__label', 'click', function() {
		var $elm = $(this).closest('.js-drop-down-content');
		$elm.toggleClass('active');
		resizeContentBlock($elm)
	});
});

function resizeContentBlock($elm) {
	$elm.find('.js-drop-down-content__drop-block').width($elm.find('.js-drop-down-content__label').width());
}