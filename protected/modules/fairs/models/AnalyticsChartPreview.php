<?php

/**
 * Class AnalyticsChartPreview
 *
 * @property integer id
 * @property string name
 * @property integer typeId
 * @property integer objId
 * @property integer epoch
 * @property integer setId
 * @property integer widget
 * @property integer widgetType
 * @property string header
 * @property string measure
 * @property string color
 * @property integer groupId
 * @property integer related
 * @property Chart rel
 * @property integer parent
 *
 * @property array $purifyAttributes
 */
class AnalyticsChartPreview extends \AR{

    public static $_description = NULL;

    public $notPurifyAttributes = [
        'measure'
    ];

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'stat' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'widget' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'widgetType' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industryId' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'position' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'header' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'measure' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'color' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );

    }
}