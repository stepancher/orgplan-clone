<?php
Yii::import('application.modules.profile.ProfileModule');
/**
 * Class LegalEntity
 */
class LegalEntity extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search' => array(
						array('safe')
					),
					'export,import' => array(
						array('safe'),
					)
				),
			),
			'logoId' => array(
				'integer',
				'label' => Yii::t('ProfileModule.profile', 'Logo'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
					'export,import' => array(
						array('safe'),
					)
				),
				'relation' => array(
					'CBelongsToRelation',
					'ObjectFile',
					array(
						'logoId' => 'id',
					),
				),
			),
			'shortName' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Company Name'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
                    'user-save' => array(
                        array('required')
                    ),
					'export,import' => array(
						array('safe'),
					)
				),
			),
			'fullName' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Full company name'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
                    'user-save' => array(
                        array('required')
                    ),
					'export,import' => array(
						array('safe'),
					)
				),
			),
			'legalRegionId' => array(
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'Region',
					array(
						'legalRegionId' => 'id',
					),
				),
				'label' => Yii::t('ProfileModule.profile', 'Legal address (region)'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
					'user-save' => array(
						array('required')
					),
					'export,import' => array(
						array('safe'),
					)
				),
			),
			'legalCityId' => array(
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'City',
					array(
						'legalCityId' => 'id',
					),
				),
				'label' => Yii::t('ProfileModule.profile', 'Legal address (city)'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
                    'user-save' => array(
                        array('required')
                    ),
					'export,import' => array(
						array('safe'),
					)
                ),
			),
			'legalAddress' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Legal address'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
                    'user-save' => array(
                        array('required')
                    ),
					'export,import' => array(
						array('safe'),
					)
                ),
			),
			'factRegionId' => array(
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'Region',
					array(
						'factRegionId' => 'id',
					),
				),
				'label' => Yii::t('ProfileModule.profile', 'Current address (region)'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
					'user-save' => array(
						array('required')
					),
					'export,import' => array(
						array('safe'),
					)
				),
			),
			'factCityId' => array(
				'integer',
				'label' => Yii::t('ProfileModule.profile', 'Current address (city)'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
                    'user-save' => array(
                        array('required')
                    ),
					'export,import' => array(
						array('safe'),
					)
                ),
				'relation' => array(
					'CBelongsToRelation',
					'City',
					array(
						'factCityId' => 'id',
					),
				),
			),
			'factAddress' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Actual Address'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
                    'user-save' => array(
                        array('required')
                    ),
					'export,import' => array(
						array('safe'),
					)
                ),
			),
			'phone' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Phone'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					),
                    'user-save' => array(
                        array('required'),
					),
					'export,import' => array(
						array('safe'),
					)
                ),
			),
			'INN' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'INN'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					),
					'user-save' => array(
						array('required'),
					),
					'export,import' => array(
						array('safe'),
					)
				),
			),
			'trLegalEntities' => array(
				'relation' => array(
					'CHasManyRelation',
					'TrLegalEntity',
					array(
						'trParentId' => 'id',
					),
				),
			),
			'users' => array(
				'relation' => array(
					'CHasManyRelation',
					'User',
					array(
						'legalEntityId' => 'id',
					),
				),
			),
		);
	}
}