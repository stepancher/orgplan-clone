<?php

/**
 * Class BankAccount
 */
class BankAccount extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search' => array(
						array('safe')
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'paymentAccount' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Current account'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
					'user-save' => array(
						array('required'),
						array('numerical', 'integerOnly' => true),
						array('length', 'is' => 20),
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'bank' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Name of the bank'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					),
					'user-save' => array(
						array('required'),
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'bankBik' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'BIC Bank'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
						array('numerical', 'integerOnly' => true),
						array('length', 'is' => 9)

					),
					'user-save' => array(
						array('required'),
						array('numerical', 'integerOnly' => true),
						array('length', 'is' => 9)
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'correspondentAccount' => array(
				'string',
				'label' => Yii::t('ProfileModule.profile', 'Cor / expense'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					),
					'user-save' => array(
						array('required'),
						array('numerical', 'integerOnly' => true),
						array('length', 'is' => 20),
						array('ext.validators.CorrValidator', 'bik' => 'bankBik')
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'user' => array(
				'relation' => array(
					'CHasOneRelation',
					'User',
					array(
						'bankAccountId' => 'id',
					),
				),
			),
		);
	}

	/**
	 * @param null $criteria
	 * @param array $ignoreCompareFields
	 * @return CActiveDataProvider
	 * Критерия поиска
	 */
	public function search($criteria = null, $ignoreCompareFields = array())
	{
		$criteria->mergeWith(
			new CDbCriteria(array(
					'with' => array(
						'user' => array(
							'together' => true,
							'with' => array()
						)
					)
				)
			)
		);
		return parent::search($criteria, $ignoreCompareFields);
	}

}