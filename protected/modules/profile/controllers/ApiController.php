<?php

class ApiController extends Controller {

    public function actionAuth($SystemId, $UserName, $Password) {

        echo json_encode(true);
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        echo json_encode(true);
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     */
    public function actionGetUrl($url, array $params = array()){
        echo json_encode(Yii::app()->createUrl($url, $params));
    }
}