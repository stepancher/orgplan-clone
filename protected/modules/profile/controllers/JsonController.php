<?php

class JsonController extends Controller
{
    const TYPE_DATE = 0;
    const TYPE_DROPDOWN = 1;
    const TYPE_FILE = 2;

    const SUCCESSFUL_RESPONSE = 0;
    const UNSUCCESSFUL_RESPONSE = 1;
    const IMAGE_RESPONSE_KEY = 'img';

    public static function responses(){
        return array(
            self::IMAGE_RESPONSE_KEY => array(
                Yii::t('ProfileModule.profile', 'Profile Image uploaded'),
                Yii::t('ProfileModule.profile', 'Profile Image not uploaded'),
            ),
        );
    }

    public static function getImageResponse($type){
        $response = '';
        $responses = self::responses();

        if(isset($responses[self::IMAGE_RESPONSE_KEY][$type]))
            $response =  $responses[self::IMAGE_RESPONSE_KEY][$type];
        return $response;
    }

    public function actionDeleteUserAvatar(){

        /** @var User $user */
        $user = Yii::app()->user->model;

        if(isset($user->logo))
            $objectFile = $user->logo;

        if(empty($objectFile)){
            $json = array(
                'success' => FALSE,
                'message' => 'picture doesn\'t exist',
            );
            echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
            return;
        }

        if(!empty($user->logoId))
            $user->logoId = NULL;

        $transaction = Yii::app()->db->beginTransaction();
        try{
            $objectFile->delete();
            $user->save();
            $transaction->commit();
            $json = array(
                'success' => TRUE,
                'message' => Yii::t('ProfileModule.profile','Profile image deleted'),
            );
            echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
        } catch (CException $e){
            $transaction->rollback();
            $json = array(
                'success' => FALSE,
                'message' => 'picture didn\'t delete',
            );
            echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
        }
    }

    public function actionSaveUserAvatar(){

        /** @var User $user */
        $user = Yii::app()->user->model;
        /** @var CUploadedFile $userLogo */
        $userLogo = CUploadedFile::getInstanceByName('User');

        if (!empty($userLogo) && in_array($userLogo->getType(), ObjectFile::$acceptedImageExtCUpload)) {

            if (!$user->logo) {
                $objFile = new ObjectFile;
                $objFile->type = ObjectFile::TYPE_USER_LOGO;
                $objFile->ext = ObjectFile::EXT_IMAGE;
                $objFile->label = $userLogo->name;
                $objFile->setOwnerId($user->id);
                $user->logoId = $objFile->saveFile($userLogo);

                $transaction = Yii::app()->db->beginTransaction();
                try{
                    $user->save();
                    $transaction->commit();
                    $json = array(
                        'success' => TRUE,
                        'message' => self::getImageResponse(self::SUCCESSFUL_RESPONSE),
                    );
                    echo json_encode($json);
                } catch (CException $e){
                    $transaction->rollback();
                    $json = array(
                        'success' => FALSE,
                        'message' => self::getImageResponse(self::UNSUCCESSFUL_RESPONSE),
                    );
                    echo json_encode($json);
                }
            } else {
                $path = $user->logo->getDirPath(true);
                $user->logo->label = $userLogo->name;

                if ($user->logo->saveFile($userLogo)) {
                    unset($path);
                    $json = array(
                        'success' => TRUE,
                        'message' => self::getImageResponse(self::SUCCESSFUL_RESPONSE),
                    );
                    echo json_encode($json);
                }
            }
        } else {
            $json = array(
                'success' => FALSE,
                'message' => self::getImageResponse(self::UNSUCCESSFUL_RESPONSE),
            );
            echo json_encode($json);
        }
    }

    public function actionSaveCompanyData(){

        $postFlow = json_decode(file_get_contents('php://input'), FILE_USE_INCLUDE_PATH);

        if(empty($_POST) && empty($postFlow)){
            $json = array(
                'success' => FALSE,
                'message' => 'post didn\'t find',
            );
            echo json_encode($json);
            return;
        }

        /** @var User $user */
        $user = Yii::app()->user->model;

        if(isset($user->legalEntity)){
            $legalEntity = $user->legalEntity;
        } else {
            $legalEntity = new LegalEntity;
        }

        if(isset($user->bankAccount)){
            $bankAccount = $user->bankAccount;
        } else {
            $bankAccount = new BankAccount;
        }

        $bankAccount->setScenario('user-save');

        $post = array();
        array_walk(
            $postFlow,
            function ($value, $key) use(&$post){

                if(preg_match('/LegalEntity\[(\w+)\]/', $key, $matches)){

                    if(isset($matches['1'])){
                        $post['LegalEntity'][$matches['1']] = $value;
                    }
                }

                if(preg_match('/BankAccount\[(\w+)\]/', $key, $matches)){

                    if(isset($matches['1'])){
                        $post['BankAccount'][$matches['1']] = $value;
                    }
                }
            }
        );

        if(isset($post['LegalEntity']))
            $legalEntity->attributes = $post['LegalEntity'];

        if(isset($post['BankAccount']))
            $bankAccount->attributes = $post['BankAccount'];

        if($this->accountControlKeyCheck() === FALSE){
            $json = array(
                'success' => FALSE,
                'message' => 'account control key value is not true',
            );
            echo json_encode($json);
        }

        $transaction = Yii::app()->db->beginTransaction();
        try{
            $legalEntity->save();

            if($user->legalEntityId !== $legalEntity->id)
                $user->legalEntityId = $legalEntity->id;

            $bankAccount->save();

            if($user->bankAccountId !== $bankAccount->id)
                $user->bankAccountId = $bankAccount->id;

            $user->save();
            $transaction->commit();
            $json = array(
                'success' => TRUE,
                'message' => 'company information saved',
            );
            echo json_encode($json);
        } catch (CException $e){
            $transaction->rollback();
            $json = array(
                'success' => FALSE,
                'message' => 'company information didn\'t save',
            );
            echo json_encode($json);
        }
    }

    public function accountControlKeyCheck(){
        return TRUE;
    }

    public function actionSaveUserData(){

        $postFlow = json_decode(file_get_contents('php://input'), FILE_USE_INCLUDE_PATH);

        if(empty($postFlow))
            $postFlow = array();

        /** @var User $user */
        $user = Yii::app()->user->model;

        if(isset($user) && isset($user->contact)){
            $contact = $user->contact;
        } elseif(isset($user)) {
            $contact = new Contact;
        }

        $post = array();
        array_walk(
            $postFlow,
            function ($value, $key) use(&$post){

                if(preg_match('/User\[(\w+)\]/', $key, $matches)){
                    if(isset($matches['1'])){
                        $post['User'][$matches['1']] = $value;
                    }
                }

                if(preg_match('/Contact\[(\w+)\]/', $key, $matches)){
                    if(isset($matches['1'])){
                        $post['Contact'][$matches['1']] = $value;
                    }
                }
            }
        );

        if(isset($post['User']) && isset($user))
            $user->attributes = $post['User'];

        if(isset($post['Contact']) && isset($contact))
            $contact->attributes = $post['Contact'];

        $transaction = Yii::app()->db->beginTransaction();
        try{
            if(isset($contact))
                $contact->save();

            if(isset($user) && isset($contact) && $user->contactId !== $contact->id)
                $user->contactId = $contact->id;

            if(isset($user))
                $user->save();

            if(isset($user) && !empty($user->getErrors())){
                throw new CHttpException(404, 'errors');
            }

            if(isset($contact) && !empty($contact->getErrors())){
                throw new CHttpException(404, 'errors');
            }

            $transaction->commit();
        } catch (CException $e){
            $transaction->rollback();
        }

        $errors = array(
            'User' => isset($user) ? $user->getErrors() : array(),
            'Contact' => isset($contact) ? $contact->getErrors() : array(),
        );

        $message = array();

        if(empty(array_filter($errors))){
            $message['message'] = Yii::t('ProfileModule.profile','information is saved');
        }

        $json = array_merge(
            array(
                "rows" => array(
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Profile Photo'),
                        "value" => $this->getUserAvatarPath($user),
                        "name" =>  "User[avatar]",
                        "button" => Yii::t('ProfileModule.profile', 'Upload'),
                        "type" =>  self::TYPE_FILE,
                        "error" =>  "",
                    ),
                    array(
                        "label" =>  Yii::t('ProfileModule.profile', 'First name'),
                        "value" =>  isset($user) ? $user->firstName : '',
                        "name" =>  "User[firstName]",
                        "error" =>  !empty($errors['User']['firstName']) ? reset($errors['User']['firstName']) : "",
                    ),
                    array(
                        "label" =>  Yii::t('ProfileModule.profile', 'Surname'),
                        "value" =>  isset($user) ? $user->lastName : '',
                        "name" =>  "User[lastName]",
                        "error" =>  !empty($errors['User']['lastName']) ? reset($errors['User']['lastName']) : "",
                    ),
                    array(
                        "label" =>  Yii::t('ProfileModule.profile', 'Patronymic'),
                        "value" =>  isset($user) ? $user->middleName : '',
                        "name" =>  "User[middleName]",
                        "error" =>  !empty($errors['User']['middleName']) ? reset($errors['User']['middleName']) : "",
                    ),
                    array(
                        "label" =>  Yii::t('ProfileModule.profile', 'Sex'),
                        "value" => array('id' => isset($user) ? $user->gender : '', 'label' => isset($user) ? User::getGender($user->gender) : ''),
                        "list" => $this->getGenders(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" =>  "User[gender]",
                        "error" =>  !empty($errors['User']['gender']) ? reset($errors['User']['gender']) : "",
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Date of Birth'),
                        "value" => isset($user) ? $user->dob : '',
                        "type" => self::TYPE_DATE,
                        "name" =>  "User[dob]",
                        "error" =>  !empty($errors['User']['dob']) ? reset($errors['User']['dob']) : "",
                    ),
                    array(
                        "label" =>  Yii::t('ProfileModule.profile', 'Current address (city)'),
                        "value" =>  array('id' => isset($user) ? $user->cityId : '', 'label' => isset($user->city->translate) ? $user->city->name : ''),
                        "list" => $this->getCities(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" =>  "User[cityId]",
                        "error" =>  !empty($errors['User']['cityId']) ? reset($errors['User']['cityId']) : "",
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Company'),
                        "value" => isset($contact) ? $contact->companyName : '',
                        "name" =>  "Contact[companyName]",
                        "error" =>  !empty($errors['Contact']['companyName']) ? reset($errors['Contact']['companyName']) : "",
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Position'),
                        "value" => isset($contact) ? $contact->post : '',
                        "name" => "Contact[post]",
                        "error" =>  !empty($errors['Contact']['post']) ? reset($errors['Contact']['post']) : "",
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Email'),
                        "value" => isset($contact) ? $contact->email : '',
                        "name" => "Contact[email]",
                        "error" =>  !empty($errors['Contact']['email']) ? reset($errors['Contact']['email']) : "",
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Mobile Phone'),
                        "value" => isset($contact) ? $contact->phone : '',
                        "name" => "Contact[phone]",
                        "error" =>  !empty($errors['Contact']['phone']) ? reset($errors['Contact']['phone']) : "",
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Work Phone'),
                        "value" => isset($contact) ? $contact->workPhone : '',
                        "name" => "Contact[workPhone]",
                        "error" =>  !empty($errors['Contact']['workPhone']) ? reset($errors['Contact']['workPhone']) : "",
                    ),
                ),
            ),
            $message
        );
        echo json_encode($json);
    }

    public function actionGetData()
    {
        $user = Yii::app()->user->model;

        if(isset($user->contact))
            $contact = $user->contact;

        if(isset($user->legalEntity))
            $legalEntity = $user->legalEntity;

        if(isset($user->bankAccount))
            $bankAccount = $user->bankAccount;

        $json = array(
            "authorized" => !Yii::app()->user->isGuest,
            "profile" => array(
                "header" => Yii::t('ProfileModule.profile', 'Information about employee'),
                "subheader" => Yii::t('ProfileModule.profile', 'Profile'),
                "button" => Yii::t('ProfileModule.profile', 'Save'),
                "avatarButtonHeader" => Yii::t('ProfileModule.profile', 'Upload'),
                "rows" => array(
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Profile Photo'),
                        "value" => $this->getUserAvatarPath($user),
                        "name" => 'User[avatar]',
                        "button" => Yii::t('ProfileModule.profile', 'Upload'),
                        "type" => self::TYPE_FILE,
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'First name'),
                        "value" => isset($user) ? $user->firstName : '',
                        "name" => 'User[firstName]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Surname'),
                        "value" => isset($user) ? $user->lastName : '',
                        "name" => 'User[lastName]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Patronymic'),
                        "value" => isset($user) ? $user->middleName : '',
                        "name" => 'User[middleName]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Sex'),
                        "value" => array('id' => isset($user) ? $user->gender : '', 'label' => isset($user) ? User::getGender($user->gender) : ''),
                        "list" => $this->getGenders(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" => 'User[gender]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Date of Birth'),
                        "value" => isset($user) ? $user->dob : '',
                        "type" => self::TYPE_DATE,
                        "name" => 'User[dob]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Current address (city)'),
                        "value" => array('id' => isset($user) ? $user->cityId : '', 'label' => isset($user->city->translate) ? $user->city->name : ''),
                        "list" => $this->getCities(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" => 'User[cityId]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Company'),
                        "value" => isset($contact) ? $contact->companyName : '',
                        "name" => 'Contact[companyName]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Position'),
                        "value" => isset($contact) ? $contact->post : '',
                        "name" => 'Contact[post]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Email'),
                        "value" => isset($contact) ? $contact->email : '',
                        "name" => 'Contact[email]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Mobile Phone'),
                        "value" => isset($contact) ? $contact->phone : '',
                        "name" => 'Contact[phone]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Work Phone'),
                        "value" => isset($contact) ? $contact->workPhone : '',
                        "name" => 'Contact[workPhone]',
                    ),
                ),
            ),
            "company" => array(
                "header" => Yii::t('ProfileModule.profile', 'Information about company'),
                "subheader" => Yii::t('ProfileModule.profile', 'Profile'),
                "button" => Yii::t('ProfileModule.profile', 'Save'),
                "avatar" => array(
                    "button" => Yii::t('ProfileModule.profile', 'Upload'),
                    "path" => $this->getCompanyAvatarPath($user),
                ),
                "rows" => array(
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Company Name'),
                        "value" => isset($legalEntity) ? $legalEntity->shortName : '',
                        "name" => 'LegalEntity[shortName]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Full company name'),
                        "value" => isset($legalEntity) ? $legalEntity->fullName : '',
                        "name" => 'LegalEntity[fullName]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Legal address (region)'),
                        "value" => array(
                            'id' => isset($legalEntity) ? $legalEntity->legalRegionId : '',
                            'label' => isset($legalEntity) && isset($legalEntity->legalRegion) ? $legalEntity->legalRegion->name : '',
                        ),
                        "list" => $this->getRegions(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" => 'LegalEntity[legalRegionId]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Legal address (city)'),
                        "value" => array(
                            'id' => isset($legalEntity) ? $legalEntity->legalCityId : '',
                            'label' => isset($legalEntity) && isset($legalEntity->legalCity) ? $legalEntity->legalCity->name : '',
                        ),
                        "list" => $this->getCities(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" => 'LegalEntity[legalCityId]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Address'),
                        "value" => isset($legalEntity) ? $legalEntity->legalAddress : '',
                        "name" => 'LegalEntity[legalAddress]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Actual Address(region)'),
                        "value" => array(
                            'id' => isset($legalEntity) ? $legalEntity->factRegionId : '',
                            'label' => isset($legalEntity) && isset($legalEntity->factRegion) ? $legalEntity->factRegion->name : '',
                        ),
                        "list" => $this->getRegions(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" => 'LegalEntity[factRegionId]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Actual Address(city)'),
                        "value" => array(
                            'id' => isset($legalEntity) ? $legalEntity->factCityId : '',
                            'label' => isset($legalEntity) && isset($legalEntity->factCity) ? $legalEntity->factCity->name : '',
                        ),
                        "list" => $this->getCities(),
                        "type" => self::TYPE_DROPDOWN,
                        "name" => 'LegalEntity[factCityId]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Actual Address(street)'),
                        "value" => isset($legalEntity) ? $legalEntity->factAddress : '',
                        "name" => 'LegalEntity[factAddress]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Phone'),
                        "value" => isset($legalEntity) ? $legalEntity->phone : '',
                        "name" => 'LegalEntity[phone]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Settlement account'),
                        "value" => isset($bankAccount) ? $bankAccount->paymentAccount : '',
                        "name" => 'BankAccount[paymentAccount]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Name of the bank'),
                        "value" => isset($bankAccount) ? $bankAccount->bank : '',
                        "name" => 'BankAccount[bank]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'BIC Bank'),
                        "value" => isset($bankAccount) ? $bankAccount->bankBik : '',
                        "name" => 'BankAccount[bankBik]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'Cor / expense'),
                        "value" => isset($bankAccount) ? $bankAccount->correspondentAccount : '',
                        "name" => 'BankAccount[correspondentAccount]',
                    ),
                    array(
                        "label" => Yii::t('ProfileModule.profile', 'INN'),
                        "value" => isset($legalEntity) ? $legalEntity->INN : '',
                        "name" => 'LegalEntity[INN]',
                    ),
                ),
            ),
        );
        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    public function getUserAvatarPath($user){
        if(isset($user->logo))
            return Yii::app()->getBaseUrl(true) . $user->logo->getFileUrl();
        return '';
    }

    public function getCompanyAvatarPath($user){
        if(isset($user->legalEntity->logo))
            return Yii::app()->getBaseUrl(true) . $user->legalEntity->logo->getFileUrl();
        return '';
    }

    public function getCities(){
        $sql = "SELECT trParentId AS id, name AS label FROM tbl_trcity WHERE langId = :langId";
        $data = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        return $data;
    }

    public function getRegions(){
        $sql = "SELECT trParentId AS id, name AS label FROM tbl_trregion WHERE langId = :langId";
        $data = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));
        return $data;
    }

    public function getGenders(){
        $data = array();
        $genders = User::genders();

        foreach ($genders as $key => $value){
            $data[] = array(
                'id' => $key,
                'label' => $value,
            );
        }
        return $data;
    }
}