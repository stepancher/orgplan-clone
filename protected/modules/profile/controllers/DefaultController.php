<?php

class DefaultController extends Controller{

    /**
     * Профиль пользователя
     */
    public function actionIndex()
    {
        /**
         * @var CWebApplication $app
         * @var User $user
         */
        $app = Yii::app();
        $user = null;

        if (!empty($_GET['activated']) && !empty($_GET['id'])) {
            $user = User::model()->findByPk($_GET['id']);
            if ($user !== null && md5($user->password) == $_GET['activated']) {
                if ($app->user->isGuest && $user->contact && !empty($user->contact->email)) {
                    $identity = new UserIdentity($user->contact->email, $user->password);
                    if ($identity->authenticateByRecord($user)) {
                        $app->user->login($identity);
                    }
                }

                if ($user->active == User::ACTIVE_ON) {
                    $app->user->setFlash('success', Yii::t('ProfileModule.profile', 'Your account is already activated'));
                } else {
                    $user->active = User::ACTIVE_ON;
                    if ($user->save(false)) {
                        $app->user->setFlash('success', Yii::t('ProfileModule.profile', 'Your account is activated'));
                    }
                }
                
                $this->redirect($this->createUrl('/profile/default/index', array('langId' => Yii::app()->language)));
            }
        }

        if (!$user && !$app->user->isGuest) {
            $user = User::model()->findByPK($app->user->id);
        }

        if (!$user || $app->user->isGuest) {
            $app->user->setFlash('success', Yii::t('ProfileModule.profile', 'Information not available'));
            $this->redirect($this->createAbsoluteUrl('/home'));
        }

        if ($user->active != User::ACTIVE_ON) {
            $app->user->setFlash('warning', Yii::t('ProfileModule.profile', 'Your account is not activated'));
        }

        $save = TRUE;
        $errors = [];

        if(isset($_POST['User'])) {
            /** @var User $user */
            $user->attributes = $_POST['User'];
            if(!$user->save()){
                $save = FALSE;
                $errors['User'] = $user->getErrors();
                Yii::app()->user->setFlash('error', reset($errors['User'])[0]);
            } else {

                $userLogo = CUploadedFile::getInstance($user, 'logoId');
                if (!empty($userLogo) && in_array($userLogo->getType(), ObjectFile::$acceptedImageExtCUpload)) {
                    if (!$user->logo) {
                        $objFile = new ObjectFile();
                        $objFile->type = ObjectFile::TYPE_USER_LOGO;
                        $objFile->ext = ObjectFile::EXT_IMAGE;
                        $objFile->label = $userLogo->name;
                        $objFile->setOwnerId($user->id);
                        $user->logoId = $objFile->saveFile($userLogo);
                        if(!$user->save())
                            $app->user->setFlash('warning', Yii::t('ProfileModule.profile', 'Profile Image not uploaded'));
                    } else {
                        $path = $user->logo->getDirPath(true);
                        $user->logo->label = $userLogo->name;
                        if ($user->logo->saveFile($userLogo)) {
                            unset($path);
                        }
                    }
                }
            }
        }

        if(isset($_POST['Contact'])) {
            $user->contact->attributes = $_POST['Contact'];
            if(!$user->contact->save()){
                $save = FALSE;
                $errors['Contact'] = $user->contact->getErrors();
                Yii::app()->user->setFlash('error', reset($errors['Contact'])[0]);
            }
        }

        if(isset($_POST['BankAccount'])) {
            if(empty($user->bankAccount)){
                $user->bankAccount = new BankAccount();
            }
            $user->bankAccount->attributes = $_POST['BankAccount'];
            if(!$user->bankAccount->save()){
                $save = FALSE;
                $errors['BankAccount'] = $user->bankAccount->getErrors();
                Yii::app()->user->setFlash('error', reset($errors['BankAccount'])[0]);
            }
        }

        if(isset($_POST['LegalEntity'])) {
            if(empty($user->legalEntity)){
                $user->legalEntity = new LegalEntity();
            }
            $user->legalEntity->attributes = $_POST['LegalEntity'];
            if(!$user->legalEntity->save()){
                $save = FALSE;
                $errors['LegalEntity'] = $user->legalEntity->getErrors();
                Yii::app()->user->setFlash('error', reset($errors['LegalEntity'])[0]);
            }
            $user->legalEntityId = $user->legalEntity->id;
            $user->save();

        }

        if(!empty($_POST) && $save){
            Yii::app()->user->setFlash('success', Yii::t('ProfileModule.profile', 'information is saved'));
            $this->refresh();
        }

        if (Yii::app()->user->getState('role') == User::ROLE_ORGANIZERS) {
            $this->redirect(Yii::app()->createUrl('/organizer', array('langId' => Yii::app()->language)));
        }

        $records = [
            'user' => $user,
            'contact' => $user->contact,
            'bankAccount' => $user->bankAccount ?: new BankAccount,
            'legalEntity' => $user->legalEntity ?: new LegalEntity,
            'userSetting' => $user->userSetting ?: new UserSetting(),
        ];

        $this->render('profile', $records);

    }

    /**
     * @param $tariff
     */
    public function actionSelectTariff($tariff)
    {

        if (empty(Yii::app()->user->id)) {
            Yii::app()->request->redirect(Yii::app()->createUrl('/profile', array('langId' => Yii::app()->language)));
        }

        $user = User::model()->findByPk(Yii::app()->user->id);

        if (!$user || Yii::app()->user->isGuest) {
            Yii::app()->user->setFlash('success', Yii::t('ProfileModule.profile', 'Information not available'));
            $this->redirect($this->createAbsoluteUrl('/home'));
        }

        if ($user->active != User::ACTIVE_ON) {
            Yii::app()->user->setFlash('warning', Yii::t('ProfileModule.profile', 'Your account is not activated'));
        }

        if (!empty($tariff) && is_numeric($tariff)) {
            $user->tariffId = $tariff;

            if(Yii::app()->user->role == User::ROLE_USER){
                $user->role = User::ROLE_EXPONENT_SIMPLE;
            }

            Yii::app()->user->role = $user->role; //@TODO WARNING!!!
        }

        if (!$user->save()) {
            if (!empty($user->getErrors())) {
                $keys = array_keys($user->getErrors());
                $fields = array();
                foreach ($keys as $key) {
                    $fields[] = User::model()->getAttributeLabel($key);
                }

                $fields = implode(', ', $fields);
            }

            Yii::app()->user->setFlash('error', Yii::t('ProfileModule.profile', 'Attention, next fields need to be full:') . ' ' . $fields);
        }

        if (isset($_GET['to']) && !empty($_GET['to'])) {
            if (isset($_GET['clickButton']) && !empty($_GET['clickButton'])) {

                Yii::$app->session->set('clickButton', $_GET['clickButton']);
            }
            $this->redirect($_GET['to']);
        } else {
            $this->redirect($this->createUrl('/profile', array('tab' => 'tariffs', 'langId' => Yii::app()->language)));
        }
    }

    public function actionRemoveTariff()
    {

        if (empty(Yii::app()->user->id)) {
            Yii::app()->request->redirect(Yii::app()->createUrl('/profile', array('langId' => Yii::app()->language)));
        }

        $user = User::model()->findByPk(Yii::app()->user->id);

        if (!$user || Yii::app()->user->isGuest) {
            Yii::app()->user->setFlash('success', Yii::t('ProfileModule.profile', 'Information not available'));
            $this->redirect($this->createAbsoluteUrl('/home'));
        }

        if (Yii::app()->user->getState('role') != User::ROLE_ADMIN) {
            Yii::app()->user->setFlash('warning', Yii::t('ProfileModule.profile', 'You do not have enough privileges to do that'));
        }

        $user->tariffId = 0;
        $user->role = User::ROLE_USER;
        $user->save();
        $this->redirect($this->createUrl('/profile', array('tab' => 'tariffs', 'langId' => Yii::app()->language)));
    }


}