<?php
/**
 * @var CController $this
 * @var User $user
 * @var UserSetting $userSetting
 * @var Contact $contact
 * @var BankAccount $bankAccount
 * @var LegalEntity $legalEntity
 * @var CWebApplication $app
 */
$app = Yii::app();
$cs = $app->clientScript;

$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish(Yii::getPathOfAlias('application.modules.profile.assets'), false, -1, YII_DEBUG);

$cs->registerCssFile($publishUrl . '/css/tariff/tariff.css');
$cs->registerCssFile($publishUrl . '/css/main.css');

$cs->registerScriptFile($publishUrl . '/js/dropdown/script.js');
$cs->registerScriptFile($publishUrl . '/js/profile/script.js');
$cs->registerScriptFile($publishUrl . '/js/ListAutoComplete.js');
$cs->registerScriptFile($publishUrl . '/js/LinkedSelect.js', CClientScript::POS_END);
$cs->registerScript('LinkedSelect',
	'LinkedSelect.init("' . $this->createUrl('site/getDataByParentId') . '").run();',
	CClientScript::POS_READY
);
$cs->registerScriptFile($publishUrl . '/js/historyButton/script.js');

$periodPicker = $am->publish($app->extensionPath . '/periodPicker/build');
$cs->registerCssFile($periodPicker . '/jquery.periodpicker.min.css');
$cs->registerCssFile($periodPicker . '/jquery.timepicker.min.css');
$cs->registerScriptFile($periodPicker . '/jquery.periodpicker.full.min.js');

$this->widget('application.modules.profile.components.SimpleTabs', array(
	'base' => 'fair-redactor',
	'fixed' => true,
	'enableAjax' => false,
	'contentType' => 'json',
	'onClick' => 'js:function(o) {
        if("$tab" in o && !o.$tab.hasClass("active")) {
            $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").show();
        } else if ("$tab" in o && o.$tab.hasClass("active")){
            //ничего не делаем
        } else {
            $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").show();
        }
    }',
	'afterUpdate' => 'js:function(o) {
        changePositionDateRegion();
        $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").hide();
    }',
	'tabs' => array_filter(array(
			array(
				'active' => $this->action->id == 'index' && !isset($_GET['tab']) || (isset($_GET['tab']) && $_GET['tab'] == 'aboutMe'),
				'label' => Yii::t('ProfileModule.profile', 'about me'),
				'target' => 'aboutMe',
				'url' => Yii::app()->createUrl('/profile', ['tab' => 'aboutMe']),
				'pageTitle' => $this->pageTitle,
			),
			array(
				'active' => $this->action->id == 'index' && isset($_GET['tab']) && $_GET['tab'] == 'aboutCompany',
				'label' => Yii::t('ProfileModule.profile', 'about company'),
				'target' => 'aboutCompany',
				'url' => Yii::app()->createUrl('profile', ['tab' => 'aboutCompany']),
				'pageTitle' => $this->pageTitle,
			),
			(Yii::app()->user->role != User::ROLE_EXPONENT && Yii::app()->user->role != User::ROLE_ORGANIZERS) ? array(
				'active' => $this->action->id == 'index' && isset($_GET['tab']) && $_GET['tab'] == 'tariffs',
				'label' => Yii::t('ProfileModule.profile', 'Tariffs'),
				'target' => 'tariffs',
				'url' => Yii::app()->createUrl('profile', ['tab' => 'tariffs']),
				'pageTitle' => $this->pageTitle,
			) : NULL,
			IS_MODE_DEV ? array(
				'active' => $this->action->id == 'index' && isset($_GET['tab']) && $_GET['tab'] == 'settings',
				'label' => Yii::t('ProfileModule.profile', 'settings'),
				'target' => 'settings',
				'url' => Yii::app()->createUrl('profile', ['tab' => 'settings']),
				'pageTitle' => $this->pageTitle,
			) : null,
		)
	)));
?>

<div class="page-fair__content-wrapper offset-small-t-40">
	<div data-target-base="fair-redactor" class="tab-content">
		<div class="b-search-loader-block">
			<img alt="Загрузка" title="Загрузка" class="js-page-search__loader"
				src="<?= $publishUrl . '/img/loader/ajax-loader.gif' ?>"
				style="display: none;">
		</div>
	</div>
	<div class="simple-tabs-content">
		<?php
			$actionName = $this->action->id;
		?>

		<?php if ($actionName == 'index' && (!isset($_GET['tab']) || (isset($_GET['tab']) && $_GET['tab'] == 'aboutMe'))): ?>
			<?= $this->renderPartial('_about_me', array('user' => $user, 'contact' => $contact), true);?>
		<?php endif;?>

		<?php if ($actionName == 'index' && isset($_GET['tab']) && $_GET['tab'] == 'aboutCompany'): ?>
			<?= $this->renderPartial('_about_company', [
				'user' => $user, 'bankAccount' => $bankAccount,
				'legalEntity' => $legalEntity,
			], true)?>
		<?php endif;?>

		<?php if ($actionName == 'index' && isset($_GET['tab']) && $_GET['tab'] == 'tariffs'): ?>
			<?= $this->renderPartial('_tariffs', [
				'user' => $user,
			], true)?>
		<?php endif;?>

		<?php if ($actionName == 'index' && isset($_GET['tab']) && $_GET['tab'] == 'settings'): ?>
			<?= $this->renderPartial('_settings', [
				'user' => $user,
				'userSetting' => $userSetting
			], true);?>
		<?php endif;?>
	</div>
</div>