<?php
/**
 * @var OrganizerContact $model
 */
?>

<div data-tab-base="fair-redactor">
	<div class="cols-row position-rel" style="position:relative;">
			<?php echo  $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'Profile'),
				'description' => Yii::t('ProfileModule.profile', 'New worker')
			)) ;?>
	</div>

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<form class="orgplan-form" method="post" action="" enctype="multipart/form-data">

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Position'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerContact[position]" value="<?= $model->position ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Surname'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerContact[surname]" value="<?= $model->surname ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'First name'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerContact[name]" value="<?= $model->name ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Patronymic'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerContact[patronymic]" value="<?= $model->patronymic ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Phone'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerContact[phone]" value="<?= $model->phone ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Email'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerContact[email]" value="<?= $model->email ?>">
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-green" value="Сохранить">
					</div>
				</div>
				<input id="file" type="file" name="file" style="display: none;" />
			</form>
		</div>

		<div class="col-md-4 col-sm-12">
			<div class="row">

			</div>
			<div class="row">
				<div style="background-image: url(/uploads/organizer/team/<?= $model->image ?>);" class="picture avatar"><i class="btn btn-green upload-picture"></i></div>
			</div>
		</div>

	</div> <!-- .cols-row -->
</div>

<script>
	$('.upload-picture').click(function () {
		$('#file').trigger('click');
	});
</script>
