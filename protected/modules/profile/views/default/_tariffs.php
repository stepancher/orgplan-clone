<?php
/**
 * @var CController $this
 * @var User $user
 */
?>
<div data-tab-base="fair-redactor" data-tab-target="tariffs">
    <div style="width:960px;">
        <div class="cols-row position-rel">
            <div class="col-md-4">
                <?= $this->renderPartial('/_blocks/_b-header', array(
                    'header' => Yii::t('ProfileModule.profile', 'Tariffs'),
                )) ?>
            </div>
        </div>

        <div class="cols-row position-rel">
            <div class="col-md-4">
                <div class="tariff-block tariff-test">
                    <div class="tariff-card">
                        <div class="tariff-name">
                            Test
                        </div>
                        <div class="tariff-announce">
                            <?=Yii::t('ProfileModule.profile', 'FREE');?>
                            <div class="tariff-triangle tariff-triangle-green">

                            </div>
                        </div>
                        <div class="tariff-info">
                            <div class="tariff-description">
                                <?=Yii::t('ProfileModule.profile', 'tariff_1_1');?>
                                <br/><br/>
                                <?=Yii::t('ProfileModule.profile', 'tariff_1_2');?>
                                <br/>
                                <?=Yii::t('ProfileModule.profile', 'tariff_1_3');?>
                                <br/>
                                <br/>
                                <center>
                                    <?=Yii::t('ProfileModule.profile', 'tariff_1_4');?>
                                </center>
                            </div>
                            <div class="tariff-button tariff-add">
                                <?php
                                    if(isset($user) && !empty($user)){
                                        $params = array('tariff' => '1');
                                        
                                        if(isset($_GET['to']) && !empty($_GET['to'])){
                                            $params = array_merge($params, array('to' => $_GET['to']));
                                        }

                                        if(isset($_GET['clickButton']) && !empty($_GET['clickButton'])){
                                            $params = array_merge($params, array('clickButton' => $_GET['clickButton']));
                                        }

                                        if((int)$user->tariffId !== 1){
                                            echo TbHtml::link(
                                                Yii::t('ProfileModule.profile', 'tariff_1_5'),
                                                $this->createUrl('default/selectTariff', $params),
                                                array('onclick' => '
                                                try {
                                                    yaCounter31914033.reachGoal("choose_a_plan");
                                                    ga("tracker.send", "event", "choose_a_plan", "click_button");
                                                } catch (e) {
                                                    console.log(e);
                                                    if (Raven != undefined) {
                                                        Raven.config("http://6c0aa533332643918320060f986ea813@sentry.orgplan.ru/4").install();
                                                        Raven.captureException(e);
                                                    }
                                                }
                                                '));
                                        }else{
                                            if(Yii::app()->user->getState('role') == User::ROLE_ADMIN){
                                                echo TbHtml::link(Yii::t('ProfileModule.profile', 'tariff_1_6'), $this->createUrl('default/removeTariff'), array('style' => 'margin-top:20px;'));
                                            }else{
                                                echo '<div style="padding:10px;">'.Yii::t('ProfileModule.profile', 'tariff_1_7').'</div>';
                                            }
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tariff-block  tariff-standard">
                    <div class="tariff-card">
                        <div class="tariff-name">
                            Standard
                        </div>
                        <div class="tariff-announce">
                            <?=Yii::t('ProfileModule.profile', 'tariff_2_1');?>
                            <div class="tariff-triangle tariff-triangle-gray">

                            </div>
                        </div>
                        <div class="tariff-info">
                            <div class="tariff-description">
                                <?=Yii::t('ProfileModule.profile', 'tariff_2_2');?>
                                <br/><br/>
                                <?=Yii::t('ProfileModule.profile', 'tariff_2_3');?>
                                <div style="text-align:center;margin-top:15px;">
                                    <?=Yii::t('ProfileModule.profile', 'tariff_2_4');?>
                                </div>
                                <center style="margin-top:10px;">
                                    <?=Yii::t('ProfileModule.profile', 'tariff_2_5');?>
                                </center>
                                <center style="margin-top:10px;font-size:14pt;font-weight: bold;">
                                    <?=Yii::t('ProfileModule.profile', 'tariff_2_6');?>
                                </center>
                            </div>
                            <div class="tariff-button tariff-request" style="display:none;">
                                <?=Yii::t('ProfileModule.profile', 'tariff_2_7');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tariff-block  tariff-premium">
                    <div class="tariff-card">
                        <div class="tariff-name">
                            Professional
                        </div>
                        <div class="tariff-announce">
                            <?=Yii::t('ProfileModule.profile', 'tariff_2_1');?>
                            <div class="tariff-triangle tariff-triangle-gray">

                            </div>
                        </div>
                        <div class="tariff-info">
                            <div class="tariff-description">
                                <?=Yii::t('ProfileModule.profile', 'tariff_3_1');?>
                                <br/><br/>
                                <?=Yii::t('ProfileModule.profile', 'tariff_3_2');?>
                                <div style="text-align:center;margin-top:15px;">
                                    <?=Yii::t('ProfileModule.profile', 'tariff_3_3');?>
                                </div>
                                <center style="margin-top:10px;">
                                    <?=Yii::t('ProfileModule.profile', 'tariff_3_4');?>
                                </center>
                                <center style="margin-top:10px;font-size:14pt;font-weight: bold;">
                                    <?=Yii::t('ProfileModule.profile', 'tariff_3_5');?>
                                </center>
                            </div>
                            <div class="tariff-button tariff-request" style="display:none;">
                                <?=Yii::t('ProfileModule.profile', 'tariff_3_6');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>