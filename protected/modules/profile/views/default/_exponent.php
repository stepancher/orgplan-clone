<?php
/**
 * @var array $models
 * @var OrganizerExponent $model
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$actionName = $this->action->id;

$cs->registerScriptFile($publishUrl . '/js/profile/remove.js');
?>

<div data-tab-base="fair-redactor" data-tab-target="aboutMe">
	<div class="cols-row position-rel" style="position:relative;">
		<div class="col-md-6">
			<?php echo  $this->renderPartial('//layouts/partial/_header', array(
				'header' => Yii::t('ProfileModule.profile', 'Profile'),
				'description' => Yii::t('ProfileModule.profile', 'Company')
			)) ;?>
		</div>
		<div class="col-md-6">
			<a href="<?= Yii::app()->createUrl('site/organizer', ['tab' => 'exponent-edit']); ?>" class="btn btn-orange pull-right">Добавить экспонента</a>
		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12">
			<div class="col-md-12">
				<h2><?= Yii::t('ProfileModule.profile', 'Exponents:'); ?></h2>
			</div>

			<div class="col-md-12">
				<div class="b-grid-view" id="yw1">
					<div class="summary"></div>
					<table class="items table">
						<thead>
						<tr>
							<th id="yw1_c0">Почта</th>
							<th id="yw1_c1">Название компании</th>
							<th id="yw1_c2">№ договора</th>
							<th id="yw1_c3">ID</th>
							<th id="yw1_c4">№ стенда</th>
							<th id="yw1_c5">Площадь стенда</th>
							<th id=""></th>
						</tr>
						</thead>
						<tbody>
						<?php
						foreach ($models as $model) {
							?>
							<tr class="odd" data-remove="<?= $model->id ?>">
								<td><?= $model->email ?></td>
								<td><?= $model->name ?></td>
								<td><?= $model->bill ?></td>
								<td><?= $model->number ?></td>
								<td><?= $model->stand ?></td>
								<td><?= $model->square ?> м²</td>
								<td class="actions">
									<a href="<?= Yii::app()->createUrl('site/organizer', ['tab' => 'exponent-edit', 'exponentId' => $model->id]); ?>" class="btn edit" data-icon=""></a>
									<a href="javascript://" class="btn remove js-remove-item" data-remove-target="<?= $model->id ?>" data-icon=""></a>
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
					<div class="keys" style="display:none" title="/ru/profile/default/index?tab=exponent">
						<span>38</span>
						<span>40</span>
						<span>41</span>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?php
$this->widget('bootstrap.widgets.TbModal', array(
	'id' => 'modal-remove',
	'closeText' => '<i class="icon icon--white-close"></i>',
	'htmlOptions' => array(
		'class' => 'modal--sign-up modal-danger'
	),
	'header' => '<span class="header-offset h1-header">Внимание!</span>',
	'content' => '
                    <span>Удалить сотрудника?</span>
                    <button class="btn btn-orange js-modal-ok">Ok</button>
                    <button class="btn btn-orange js-modal-cancel">Отмена</button>
                    '
));
?>

<script type="text/javascript">
	new removeItem({
		lang: '<?=Yii::app()->getLanguage();?>',
		route: "<?= Yii::app()->createUrl('site/organizer', ['action' => 'delete']); ?>",
		key: 'exponentId'
	});
</script>