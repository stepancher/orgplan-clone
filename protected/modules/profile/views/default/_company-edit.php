<?php
/**
 * @var OrganizerInfo $model
 */
?>

<div data-tab-base="fair-redactor">
	<div class="cols-row position-rel" style="position:relative;">
			<?php echo  $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'Profile'),
				'description' => Yii::t('ProfileModule.profile', 'Company')
			)) ;?>
	</div>

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<form class="orgplan-form" action="<?= $this->createUrl('site/organizer') ?>" method="post" enctype="multipart/form-data">
				<div class="col-md-4"></div>
				<h2 class="col-md-8"><?= Yii::t('ProfileModule.profile', 'Information about company:'); ?></h2>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Name'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[name]" value="<?= $model->name ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Full name'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[fullName]" value="<?= $model->fullName ?>">
					</div>
				</div>

				<div class="col-md-4"></div>
				<h2 class="col-md-8"><?= Yii::t('ProfileModule.profile', 'Post address:'); ?></h2>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Postcode'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postPostcode]" value="<?= $model->postPostcode ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Country'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postCountry]" value="<?= $model->postCountry ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Region'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postRegion]" value="<?= $model->postRegion ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'City'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postCity]" value="<?= $model->postCity ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Street'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postStreet]" value="<?= $model->postStreet ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Phone'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postPhone]" value="<?= $model->postPhone ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Site'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[postSite]" value="<?= $model->postSite ?>">
					</div>
				</div>

				<div class="col-md-4"></div>
				<h2 class="col-md-8"><?= Yii::t('ProfileModule.profile', 'Legal address:'); ?></h2>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Postcode'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[legalPostcode]" value="<?= $model->legalPostcode ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Country'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[legalCountry]" value="<?= $model->legalCountry ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Region'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[legalRegion]" value="<?= $model->legalRegion ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'City'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[legalCity]" value="<?= $model->legalCity ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Street'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[legalStreet]" value="<?= $model->legalStreet ?>">
					</div>
				</div>

				<div class="col-md-4"></div>
				<h2 class="col-md-8"><?= Yii::t('ProfileModule.profile', 'Requisites:'); ?></h2>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Settlement account'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[requisitesSettlementAccount]" value="<?= $model->requisitesSettlementAccount ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Bank'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[requisitesBank]" value="<?= $model->requisitesBank ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'BIC'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[requisitesBic]" value="<?= $model->requisitesBic ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'ITN'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[requisitesItn]" value="<?= $model->requisitesItn ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'IEC'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[requisitesIec]" value="<?= $model->requisitesIec ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Correspondent account'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[requisitesCorrespondentAccount]"  value="<?= $model->requisitesCorrespondentAccount ?>">
					</div>
				</div>

				<div class="col-md-4"></div>
				<h2 class="col-md-8"><?= Yii::t('ProfileModule.profile', 'Sign rules:'); ?></h2>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Full name'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[signRulesName]" value="<?= $model->signRulesName ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Position'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerInfo[signRulesPosition]" value="<?= $model->signRulesPosition ?>">
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-green" value="Сохранить">
					</div>
				</div>
				<?php
				//@TODO create iframe here and ajax loading picture
				?>
				<!--<iframe src="<?= $this->createUrl('/site/image', ['img' => time()]) ?>" frameborder="0"></iframe>-->
				<input id="file" type="file" name="file" style="display: none;" />
			</form>
		</div>

		<div class="col-md-4 col-sm-12">
			<div class="row">

			</div>
			<div class="row">
				<div style="background-image: url(/uploads/organizer/company/<?= $model->image ?>);" class="picture logotype"><i class="btn btn-green upload-picture"></i></div>
			</div>
		</div>

	</div> <!-- .cols-row -->
</div>

<script>
	$('.upload-picture').click(function () {
		$('#file').trigger('click');
	});
</script>
