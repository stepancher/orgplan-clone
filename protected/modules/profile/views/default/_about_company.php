<?php
/**
 * @var CController $this
 * @var User $user
 * @var BankAccount $bankAccount
 * @var LegalEntity $legalEntity
 */
?>

<div data-tab-base="fair-redactor" data-tab-target="aboutCompany">
	<div class="cols-row position-rel" style="position:relative;">
		<div class="col-md-12">
			<?= $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'profile'),
				'description' => Yii::t('ProfileModule.profile', 'Information about company')
			)) ?>

		</div>
		<div class="social-web-block">
			<div class="p-pull-left header-fixed-width-2">
				<?= Yii::t('ProfileModule.profile', 'Add social networks') ?>
			</div>
			<div class="social-links-block">
				<?php
					echo $this->widget('ZAuthWidget', array('action' => '/auth/default/socialAuth'), true);
				?>
			</div>
		</div>
	</div>
<?php
	Yii::import('application.modules.profile.components.ProfileAboutCompanyForm');
	$this->widget('application.modules.profile.components.FormView', array(
		'form' => new ProfileAboutCompanyForm(
			array(
				'bankAccount' => $bankAccount,
				'user' => $user,
				'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
				'action' => $this->createUrl('/profile/default/'),
				'enctype' => 'multipart/form-data'
			),
			$legalEntity, $this
		),
	));
	$this->widget('CMaskedTextField', array(
		'model' => $legalEntity,
		'attribute' => 'phone',
		'mask' => '+7 (999) 999-99-99',
		'htmlOptions' => array('size' => 15, 'maxlength'=>11,'class' => 'hidden')
	));
	$this->widget('CMaskedTextField', array(
		'model' => $legalEntity,
		'attribute' => 'INN',
		'mask' => '9999999999',
		'htmlOptions' => array('size' => 15, 'maxlength'=>12,'class' => 'hidden')
	));
	$this->widget('CMaskedTextField', array(
		'model' => $bankAccount,
		'attribute' => 'paymentAccount',
		'mask' => '99999999999999999999',
		'htmlOptions' => array('size' => 15, 'maxlength'=>20,'class' => 'hidden')
	));
	$this->widget('CMaskedTextField', array(
		'model' => $bankAccount,
		'attribute' => 'correspondentAccount',
		'mask' => '99999999999999999999',
		'htmlOptions' => array('size' => 15, 'maxlength'=>20,'class' => 'hidden')
	));
	$this->widget('CMaskedTextField', array(
		'model' => $bankAccount,
		'attribute' => 'bankBik',
		'mask' => '999999999',
		'htmlOptions' => array('size' => 15, 'maxlength'=>9,'class' => 'hidden')
	));
?>
</div>
