<?php

/**
 * @var CController $this
 * @var $userSetting UserSetting
 */
?>

<style>
	.slimScrollDiv {
		max-height: 212px !important;
	}

	label.control-label {
		font-size: 16px;
		margin: 0;
	}
</style>

<div data-tab-base="fair-redactor" data-tab-target="settings">
	<div class="cols-row position-rel">
		<div class="col-md-4">
			<?= $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'profile'),
				'description' => Yii::t('ProfileModule.profile', 'settings')
			)) ?>
		</div>
	</div>

	<form method="POST" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('profile/default/'.$this->action->id, !empty($model->id) ? array('id' => $model->id) : array()) ?>">

	<div class="col-md-6">
		<div class="cols-row">

			<div class="control-group">
				<label class="control-label"><?= $userSetting->getAttributeLabel('notificationTime'); ?></label>
				<div class="controls">
					<?php
					$hours = [];
					for ($i = 0; $i < 25; $i++) {
						$hours[$i] = sprintf('%02d:00', $i);
					}
					echo Yii::app()->controller->widget('application.modules.profile.components.ListAutoComplete', array(
						'model' => $userSetting,
						'attribute' => 'notificationTime',
						'multiple' => false,
						'selectedItems' => array('checked' => $userSetting->notificationTime),
						'runSynonym' => false,
						'htmlOptions' => array(
							'labelOptions' => array(
								'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
								'style' => 'color: #515056 !important; margin: 10px 0 5px 0;',
							),
						),
						'itemOptions' => array(
							'data' => $hours,
							'value' => 'id',
							'text' => 'name',
						),
						'label' => '&nbsp;',
						'autoComplete' => false
					), true);
					?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label"><?= $userSetting->getAttributeLabel('notificationBeforeDay'); ?></label>
				<div class="controls">
					<?= Yii::app()->controller->widget('application.modules.profile.components.ListAutoComplete', array(
						'model' => $userSetting,
						'attribute' => 'notificationBeforeDay',
						'multiple' => false,
						'selectedItems' => array('checked' => $userSetting->notificationBeforeDay),
						'runSynonym' => false,
						'htmlOptions' => array(
							'labelOptions' => array(
								'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
								'style' => 'color: #515056 !important; margin: 10px 0 5px 0;',
							),
						),
						'itemOptions' => array(
							'data' => [
								1 => 'За один день',
								2 => 'За два дня',
								3 => 'За три дня'
							],
							'value' => 'id',
							'text' => 'name',
						),
						'label' => '&nbsp;',
						'autoComplete' => false
					), true); ?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label checkbox <?= $userSetting->dailyEventNotification ? 'checked' : '' ?>" style="padding-top: 6px;">
					<?= TbHtml::activeCheckBox($userSetting, 'dailyEventNotification'); ?>
					<?= $userSetting->getAttributeLabel('dailyEventNotification'); ?>
				</label>
			</div>

		</div>
	</div>

	<div class="cols-row">
		<div class="col-md-12">
			<div class="cols-row">
				<div class="col-md-10"></div>
				<div class="col-md-2">
					<button type="submit" class="b-button d-bg-success-dark d-bg-success--hover d-text-light btn">
						<?= Yii::t('ProfileModule.profile', 'Save') ?>
					</button>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>