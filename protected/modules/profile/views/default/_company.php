<?php
/**
 * @var OrganizerInfo $model
 */
?>

<div data-tab-base="fair-redactor" data-tab-target="company">
	<?php echo  $this->renderPartial('/layouts/partial/_header', array(
		'header' => Yii::t('ProfileModule.profile', 'Profile'),
		'description' => Yii::t('ProfileModule.profile', 'Company')
	)) ;?>

	<div class="cols-row">
		<div class="col-md-8">
			<div class="col-md-12">
				<h2><?= Yii::t('ProfileModule.profile', 'Information about company:'); ?></h2>
				<table class="table two-cols">
					<tbody>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Name'); ?></td>
						<td><?= $model->name ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Full name'); ?></td>
						<td><?= $model->fullName ?></td>
					</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12">
				<h2><?= Yii::t('ProfileModule.profile', 'Post address:'); ?></h2>

				<table class="table two-cols">
					<tbody>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Postcode'); ?></td>
						<td><?= $model->postPostcode ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Country'); ?></td>
						<td><?= $model->postCountry ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Region'); ?></td>
						<td><?= $model->postRegion ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'City'); ?></td>
						<td><?= $model->postCity ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Street'); ?></td>
						<td><?= $model->postStreet ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Phone'); ?></td>
						<td><?= $model->postPhone ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Site'); ?></td>
						<td><?= $model->postSite ?></td>
					</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12">
				<h2><?= Yii::t('ProfileModule.profile', 'Legal address:'); ?></h2>

				<table class="table two-cols">
					<tbody>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Postcode'); ?></td>
						<td><?= $model->legalPostcode ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Country'); ?></td>
						<td><?= $model->legalCountry ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Region'); ?></td>
						<td><?= $model->legalRegion ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'City'); ?></td>
						<td><?= $model->legalCity ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Street'); ?></td>
						<td><?= $model->legalStreet ?></td>
					</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12">
				<h2><?= Yii::t('ProfileModule.profile', 'Requisites:'); ?></h2>

				<table class="table two-cols">
					<tbody>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Settlement account'); ?></td>
						<td><?= $model->requisitesSettlementAccount ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Bank'); ?></td>
						<td><?= $model->requisitesBank ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'BIC'); ?></td>
						<td><?= $model->requisitesBic ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'ITN'); ?></td>
						<td><?= $model->requisitesItn ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'IEC'); ?></td>
						<td><?= $model->requisitesIec ?></td>
					</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12">

				<h2><?= Yii::t('ProfileModule.profile', 'Permissions to sign:'); ?></h2>

				<table class="table two-cols">
					<tbody>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Full name'); ?></td>
						<td><?= $model->signRulesName ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('ProfileModule.profile', 'Position'); ?></td>
						<td><?= $model->signRulesPosition ?></td>
					</tr>
					</tbody>
				</table>
			</div>

		</div>

		<div class="col-md-4">
			<div class="row">
				<a href="<?= Yii::app()->createUrl('site/organizer', ['tab' => 'company-edit']); ?>" class="btn btn-orange pull-right" data-content-target="company-edit"><?= Yii::t('ProfileModule.profile', 'Edit profile'); ?></a>
			</div>
			<div class="row">
				<?php
				//@TODO change div to img
				?>
				<div style="background-image: url(/uploads/organizer/company/<?= $model->image ?>);" class="picture logotype pull-right"></div>
			</div>
		</div>
	</div>

</div>
