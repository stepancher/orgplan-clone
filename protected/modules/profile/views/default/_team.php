<?php
/**
 * @var OrganizerContact $model
 */

$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
$actionName = $this->action->id;

$cs->registerScriptFile($publishUrl . '/js/profile/remove.js');
?>

<div data-tab-base="fair-redactor" data-tab-target="aboutMe">
	<div class="cols-row position-rel" style="position:relative;">
			<?php echo  $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'Profile'),
				'description' => Yii::t('ProfileModule.profile', 'Company')
			)) ;?>
	</div>

	<div class="cols-row">
		<div class="col-md-8">
			<?php if (!empty($models)) { ?>
				<h2>Контакты:</h2>

				<div class="row"></div>
			<ul class="contacts list-unstyled">
				<?php foreach ($models as $model) { ?>
				<li class="row" data-remove="<?= $model->id ?>">
					<div style="background-image: url(/uploads/organizer/team/<?= $model->image ?>);" class="col-xs-1 avatar"></div>
					<div class="col-xs-4">
						<span class="name"><?= $model->name ?></span><span class="surname"><?= $model->surname ?></span>
						<span class="position"><?= $model->position ?></span>
					</div>

					<div class="col-xs-6">
						<span class="phone"><?= $model->phone ?></span>
						<span class="email"><?= $model->email ?></span>
					</div>
					<div class="col-xs-1">
						<div class="wrap">
							<a href="<?= Yii::app()->createUrl('site/organizer', ['tab' => 'team-edit', 'contactId' => $model->id]); ?>" class="btn edit" data-icon=""></a>
							<a href="javascript://" class="btn remove js-remove-item" data-remove-target="<?= $model->id ?>" data-icon=""></a>
						</div>
					</div>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>

		<div class="col-md-4 col-sm-12">
			<div class="row">
				<a href="<?= Yii::app()->createUrl('site/organizer', ['tab' => 'team-edit']); ?>" class="btn btn-green pull-right" data-content-target="team-edit"><?= Yii::t('ProfileModule.profile', 'Add worker'); ?></a>
			</div>
		</div>

	</div>
</div>
<?php
$this->widget('bootstrap.widgets.TbModal', array(
	'id' => 'modal-remove',
	'closeText' => '<i class="icon icon--white-close"></i>',
	'htmlOptions' => array(
		'class' => 'modal--sign-up modal-danger'
	),
	'header' => '<span class="header-offset h1-header">Внимание!</span>',
	'content' => '
                    <span>Удалить сотрудника?</span>
                    <button class="btn btn-orange js-modal-ok">Ok</button>
                    <button class="btn btn-orange js-modal-cancel">Отмена</button>
                    '
));
?>

<script type="text/javascript">
	new removeItem({
		lang: '<?=Yii::app()->getLanguage();?>',
		route: "<?= Yii::app()->createUrl('site/organizer', ['action' => 'delete']); ?>",
		key: 'contactId',
	});
</script>
