<?php
/**
 * @var CController $this
 * @var User $user
 * @var Contact $contact
 * @var BankAccount $bankAccount
 * @var LegalEntity $legalEntity
 * @var Exponent $exponent
 * @var CActiveDataProvider $dataProvider
 */
?>

<div data-tab-base="fair-redactor" data-tab-target="aboutMe">
	<div class="cols-row position-rel" style="position:relative;">
		<div class="col-md-12">
			<?php echo  $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'profile'),
				'description' => Yii::t('ProfileModule.profile', 'Information about employee')
			)) ;?>

		</div>
		<div class="social-web-block">
			<div class="p-pull-left header-fixed-width-2">
				<?= Yii::t('ProfileModule.profile', 'Add social networks') ?>
			</div>
			<div class="social-links-block">
				<?php
					echo $this->widget('ZAuthWidget', array('action' => '/auth/default/socialAuth'), true);
				?>
			</div>
		</div>
	</div>
<?php
	Yii::import('application.modules.profile.components.ProfileForm');
	echo $this->widget('application.modules.profile.components.FormView', array(
		'form' => new ProfileForm(
			array(
				'contact' => $contact,
				'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
				'action' => $this->createUrl('/profile/default/'),
				'enctype' => 'multipart/form-data',
				'class' => 'profile__form-about-me'
				//'elementGroupName' => '[]',
			),
			$user, $this
		),
	), true);

	$this->widget('CMaskedTextField', array(
		'model' => $contact,
		'attribute' => 'phone',
		'mask' => '+7 (999) 999-99-99',
		'htmlOptions' => array('size' => 15, 'maxlength' => 11, 'class' => 'hidden')
	));
	$this->widget('CMaskedTextField', array(
		'model' => $contact,
		'attribute' => 'workPhone',
		'mask' => '+7 (999) 999-99-99',
		'htmlOptions' => array('size' => 15, 'maxlength' => 11, 'class' => 'hidden')
	));
?>
</div>
