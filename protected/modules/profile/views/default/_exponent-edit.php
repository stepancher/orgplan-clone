<?php
/**
 * @var OrganizerExponent $model
 */
?>

<div data-tab-base="fair-redactor">
	<div class="cols-row position-rel" style="position:relative;">
			<?php echo  $this->renderPartial('/_blocks/_b-header', array(
				'header' => Yii::t('ProfileModule.profile', 'New exponent'),
			)) ;?>
	</div>

	<div class="cols-row">
		<div class="col-md-8 col-sm-12">
			<form class="orgplan-form" method="post" action="">

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Email'); ?></label>
					<div class="controls col-md-8">
						<input type="text" placeholder="<?= Yii::t('ProfileModule.profile', 'Enter exponents email'); ?>" name="OrganizerExponent[email]" value="<?= $model->email ?>">
					</div>
				</div>

				<div class="col-md-4"></div>
				<h2 class="col-md-8"><?= Yii::t('ProfileModule.profile', 'Company information:'); ?></h2>


				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Company name'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[name]" value="<?= $model->name ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', '№ of bill'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[bill]" value="<?= $model->bill ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'ID'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[number]" value="<?= $model->number ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', '№ of stand'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[stand]" value="<?= $model->stand ?>">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label col-md-4" for=""><?= Yii::t('ProfileModule.profile', 'Stand square'); ?></label>
					<div class="controls col-md-8">
						<input type="text" name="OrganizerExponent[square]" value="<?= $model->square ?>">
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-green" value="Сохранить">
					</div>
				</div>
			</form>
		</div>

	</div> <!-- .cols-row -->
</div>
