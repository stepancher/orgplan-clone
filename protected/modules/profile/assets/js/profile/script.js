$(function () {
	$('.gender').click(function () {
		$(this).find('input').prop("checked", true);
	});
	$('.user').click(function () {
		var value = $(this).find('input').val();
		showFiles(value);
		$(this).find('input').prop("checked", true);
	});
});
$(document).on('ready', function () {
	var value = $('.user input:checked').val();
	showFiles(value);
});
function showFiles(val) {
	if (val == 4) {
		$('.performer-certificates').show();
	} else if (val == 3) {
		$('.performer-certificates').hide();
	}

}