function SimpleTabs(params) {
    return ({
        $tabs: null,
        contentType: 'html',
        isLoading: false,
        enableAjax: false,
        baseName: null,
        activeTargetName: null,
        afterUpdate: null,
        onClick: null,
        handlers: null,
        clicked: true,
        $tab: null,
        initParams: function (params) {
            var me = this;
            $.each(params, function (prop, value) {
                if (typeof me[prop] !== 'undefined') {
                    me[prop] = value;
                }
            });
        },
        run: function (params) {
            var me = this;

            me.initParams(params);

            if (!me.baseName) {
                return;
            }

            me.$tabs = $('.b-tabs[data-base="' + me.baseName + '"]');
            me.activeTargetName = me.$tabs.find('.b-tabs__tab.active').data('content-target');

            $(document).on('ready', function () {
                me.initContents();
                me.execute(me.handlers);
            });

            me.$tabs.find('.b-tabs__tab').click(function (e) {
                window.scrollTo(0, 0);
                var $tab = $(this),
                    url = $tab.find('a').attr('href');
                me.$tab = $tab;

                me.activeTargetName = $tab.data('content-target');

                // Используется при создании ТЗ (может еще где прикодиться)
                var $tabIdentity = $('input[name="tab-identity"]');
                if ($tabIdentity.length) {
                    $tabIdentity.val(me.activeTargetName);
                }
                if(me.enableAjax == true){
                    e.preventDefault();
                }

                var alwaysAjax = $tab.data('always-ajax');
                if (typeof alwaysAjax === 'undefined' || alwaysAjax == 0) {
                    alwaysAjax = false;
                }

                if (me.clicked) {
                    me.execute(me.onClick, $tab);
                }

                if (me.isLoading || $tab.hasClass('active')) {
                    return false;
                }

                $('.b-tabs[data-base=' + me.baseName + '] .b-tabs__tab').each(function () {
                    $(this).removeClass('active');
                });

                if (me.enableAjax &&
                    ($('[data-tab-base=' + me.baseName + '][data-tab-target=' + me.activeTargetName + ']').length == 0 || alwaysAjax)
                ) {
                    me.isLoading = true;
                    $.get(url, function (data) {
                        if (alwaysAjax) {
                            $('div[data-tab-target="' + me.activeTargetName + '"]').remove();
                        }
                        if ($tab.data('page-title') != null) {
                            document.title = $tab.data('page-title');
                        }
                        if ('content' in data) {
                            if('title' in data) {
                                document.title = data.title;
                            } else if($tab.data('page-title') != null) {
                                document.title = $tab.data('page-title');
                            } else {
                                document.title = '';
                            }

                            $('meta[name="description"]').attr("content", ('description' in data) ? data.description : '');
                            $('div.tab-content[data-target-base=' + me.baseName + ']').after(data.content);
                            $tab.data('page-title', ('title' in data) ? data.title : '');
                            $tab.data('page-description', ('description' in data) ? data.description : '');
                        }
                        else {
                            $('div.tab-content[data-target-base=' + me.baseName + ']').after(data);
                        }
                        $tab.addClass('active');
                        me.initContents();
                        me.execute(me.afterUpdate);
                        me.isLoading = false;
                        historyLoader();
                    }, me.contentType);
                } else {
                    if ($tab.data('page-description') != null) {
                        $('meta[name="description"]').attr("content", $tab.data('page-description'));
                    }

                    if ($tab.data('page-title') != null) {
                        document.title = $tab.data('page-title');
                    }

                    $tab.addClass('active');
                    me.initContents();
                    me.execute(me.afterUpdate);
                }

                if ('pushState' in window.history) {
                    if (location.hash.length <= 1 || location.hash != url) {
                        window.history.pushState(self.filterData, document.title, url);
                    }
                }
            });

            if (me.enableAjax) {
                $(window).on('popstate', function (event) {
                    if (event.state) {
                        if (location.hash.length < 1) {
                            location.reload();
                        }
                    }
                }, false);
            }
        },
        initContents: function () {
            var me = this;
            $('[data-tab-target][data-tab-base=' + me.baseName + ']').each(function () {
                var $content = $(this);
                if ($content.data('tab-target') == me.activeTargetName) {
                    $content.addClass('active');
                } else {
                    $content.removeClass('active');
                }
            });
        },
        execute: function (data) {
            try {
                if (data !== null) {
                    data(this);
                }
            } catch (msg) {
                console.log(msg);
            }
        }
    }).run(params);
}