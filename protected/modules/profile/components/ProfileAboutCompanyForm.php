<?php
/**
 * File TestForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class ProfileForm*/
class ProfileAboutCompanyForm extends TbForm
{
    /**
     * @var array
     */
    public $elementGroupName = '';
    public $bankAccount;
    public $user;
    public $exponent;

    private $regions;
    private $regionNames;

    /**
     * @var string
     */
    public $template = '
		<div class="cols-row">
			<div class="col-md-12 offset-small-b-3 profile__form-about-company--company-image">
				{logo}
				{image}
				{file-button}
			</div>
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-4 profile__form-about-company--column">
						<div class="cols-row">
							<div class="col-md-12">
								{shortName}
							</div>
							<div class="col-md-12">
								{fullName}
							</div>
							<div class="col-md-12 profile__industry">
								{industryId}
							</div>
							<div class="col-md-12">
							</div>
						</div>
					</div>
					<div class="col-md-4 profile__form-about-company--column">
						<div class="cols-row">
							<div class="col-md-12">
								{legalRegionId}
							</div>
							<div class="col-md-12">
								{legalCityId}
							</div>
							<div class="col-md-12 offset-small-b-4">
							{legalAddress}
							</div>
							<div class="col-md-12">
							{factRegionId}
							</div>
							<div class="col-md-12">
							{factCityId}
							</div>
							<div class="col-md-12">
							{factAddress}
							</div>
							<div class="col-md-12">
							{phone}
							</div>
						</div>
					</div>
					<div class="col-md-4 profile__form-about-company--column">
						<div class="cols-row">
							<div class="col-md-12">
								{paymentAccount}
							</div>
							<div class="col-md-12">
								{bank}
							</div>
							<div class="col-md-12">
								{bankBik}
							</div>
							<div class="col-md-12">
								{correspondentAccount}
							</div>
							<div class="col-md-12">
								{INN}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-10">
						{error-block}
					</div>
					<div class="col-md-2">
						{submit}
					</div>
				</div>
			</div>
		</div>
	';

    public function renderElements()
    {
        /**
         * @var CController $ctrl
         * @var User $model
         */
        $model = $this->getModel();
        $parts = array();
        foreach ($this->getElements() as $element) {
            $parts['{' . $element->name . '}'] = $this->renderElement($element);
        }


        $parts['{logo}'] = TbHtml::createInput(TbHtml::INPUT_TYPE_FILE, 'LegalEntity[logoId]', '', array('class' => 'hidden', 'data-file-input' => 'user-logo'));
        $parts['{image}'] = TbHtml::image(static::getLogoUrl($model));
        $parts['{paymentAccount}'] = $this->getBankAccountField('paymentAccount');
        $parts['{bank}'] = $this->getBankAccountField('bank');
        $parts['{bankBik}'] = $this->getBankAccountField('bankBik');
        $parts['{correspondentAccount}'] = $this->getBankAccountField('correspondentAccount');
        $parts['{file-button}'] = TbHtml::button(
            Yii::t('ProfileModule.profile', 'Upload'),
            array(
                'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light btn p-offset-l-1 fixed-width-1 p-offset-b-1 p-no-t-margin',
                'data-file-input-target' => 'user-logo'
            )
        );
        $parts['{industryId}'] = $this->getIndustry();

        $parts['{submit}'] = TbHtml::submitButton(
            Yii::t('ProfileModule.profile', 'Save'),
            array(
                'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light  btn',
            )
        );
        $parts['{error-block}'] = $this->getErrorBlock();

        return strtr($this->template, $parts);
    }

    static function getLogoUrl($model)
    {
        $linkAssets = H::getImageUrl($model, 'logo');
        $assets = strstr($linkAssets, 'assets', true);
        $newLinkAssets = dirname($linkAssets) . DIRECTORY_SEPARATOR . 'legalEntity-logo-resized-' . basename($linkAssets);
        $newAbsoluteImagePath = ($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $newLinkAssets;
        if (!file_exists($newAbsoluteImagePath)) {
            $image = new Image(($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $linkAssets);
            $image->resize(null, 120);
            $image->save($newAbsoluteImagePath);
        }
        return $newLinkAssets;
    }

    public function getBankAccountField($name)
    {
        $delete = '<i data-icon="&#xe3c1" class="js-clear-field b-input-icon b-input-icon--append d-text-dark f-text--middle f-position-1 offset-large-t-1"></i>';

        return H::controlGroup(
            TbHtml::activeTextField($this->bankAccount,
                $name,
                array('class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark')
            ) . $delete,
            BankAccount::model()->getAttributeLabel($name)
        );
    }


    public function getErrorBlock()
    {
        //return '<div class="error-block f-pull-right offset-small-all-1">!!error-block!!</div>';
    }

    public function getIndustry()
    {
        /**
         * @var User $user
         */
        $user = $this->user;
        $content = null;
        $result = null;
        return $result;
    }

    /**
     * Инициализация формы
     */
    public function init()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();

        /** @var User $model */
        $model = $this->getModel();

        $this->regions = Region::model()->findAll();
        $this->regionNames = [];

        foreach ($this->regions as $region) {
            $this->regionNames[] = $region->loadName();
        }

        $this->elements = CMap::mergeArray(
            array_flip(array_keys($this->initElements())),
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );

        parent::init();
    }

    /**
     * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=User[]['login']
     * @param string $name
     * @param TbFormInputElement $element
     * @param bool $forButtons
     */
    public function addedElement($name, $element, $forButtons)
    {
        $element->name = $this->elementGroupName . $name;
    }

    /**
     * Инициализация элементов формы
     * @return array
     */
    public function initElements()
    {
        /**
         * @var CController $ctrl
         * @var LegalEntity $model
         */
        $model = $this->getModel();
        $delete = '<i data-icon="&#xe3c1" class="js-clear-field b-input-icon b-input-icon--append d-text-dark f-text--middle f-position-1 offset-large-t-1"></i>';

        return array(
            'shortName' => array(
                'input' => TbHtml::activeTextField($model, 'shortName', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'fullName' => array(
                'input' => TbHtml::activeTextField($model, 'fullName', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'INN' => array(
                'input' => TbHtml::activeTextField($model, 'INN', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'factAddress' => array(
                'attributes' => array(
                    'label' => ''
                ),
                'input' => TbHtml::activeTextField($model, 'factAddress', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'phone' => array(
                'input' => TbHtml::activeTextField($model, 'phone', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'legalAddress' => array(
                'attributes' => array(
                    'label' => Yii::t('ProfileModule.profile', 'Address')
                ),
                'input' => TbHtml::activeTextField($model, 'legalAddress', array(
                            'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                        )
                    ) . $delete,
            ),
            'legalRegionId' => array(
                'attributes' => array(
                    'label' => LegalEntity::model()->getAttributeLabel('legalAddress')
                ),
                'input' =>
                    Yii::app()->controller->widget('application.modules.profile.components.ListAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'legalRegionId',
                        'multiple' => false,
                        'selectedItems' => array('checked'=>$model->legalRegionId),
                        'runSynonym' => true,
                        'htmlOptions' => array(
                            'labelOptions' => array(
                                'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
                                'style' => 'color: #515056 !important;',
                            ),
                        ),
                        'itemOptions' => array(
                            'data' => $this->regionNames,
                            'value' => 'id',
                            'text' => 'name',
                        )
                    ), true),
                    /*H::dropDownListActiveGroup($model, 'legalRegionId', 'Region',
                    [
                        'groupOptions' => [
                            'class' => 'js-first-filter-list offsets',
                            'data-related' => 'region#legal',
                            'data-child-related' => 'city#legal'
                        ],
                    ],
                    [
                        'order' => 'name ASC'
                    ]
                )*/
            ),
            'factRegionId' => array(
                'attributes' => array(
                    'label' => $model->getAttributeLabel('factAddress')
                ),
                'input' =>
                    Yii::app()->controller->widget('ext.widgets.ListAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'factRegionId',
                        'multiple' => false,
                        'selectedItems' => array('checked'=>$model->factRegionId),
                        'runSynonym' => true,
                        'htmlOptions' => array(
                            'labelOptions' => array(
                                'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
                                'style' => 'color: #515056 !important;',
                            ),
                        ),
                        'itemOptions' => array(
                            'data' => $this->regionNames,
                            'value' => 'id',
                            'text' => 'name',
                        )
                    ), true),
                    /*H::dropDownListActiveGroup($model, 'factRegionId', 'Region', [
                    'groupOptions' => [
                        'class' => 'js-first-filter-list offsets',
                        'data-related' => 'region#fact',
                        'data-child-related' => 'city#fact'
                    ]
                ],
                    [
                        'order' => 'name ASC'
                    ]
                ),*/
            ),
            'factCityId' => array(
                'attributes' => array(
                    'label' => ''
                ),
                'input' =>
                    Yii::app()->controller->widget('ext.widgets.ListAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'factCityId',
                        'multiple' => false,
                        'selectedItems' => array('checked'=>$model->factCityId),
                        'runSynonym' => true,
                        'htmlOptions' => array(
                            'labelOptions' => array(
                                'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
                                'style' => 'color: #515056 !important;',
                            ),
                        ),
                        'itemOptions' => array(
                            'data' => CHtml::listData(City::model()->findAll(), 'id', 'name'),
                            'value' => 'id',
                            'text' => 'name',
                        )
                    ), true),
                  /*  H::dropDownListActiveGroup($model, 'factCityId', 'City', [
                    'groupOptions' => [
                        'class' => 'offsets',
                        'data-related' => 'city#fact'
                    ]
                ],
                    [
                        'order' => 'name ASC'
                    ]
                ),*/
            ),
            'legalCityId' => array(
                'attributes' => array(
                    'label' => ''
                ),
                'input' =>
                    Yii::app()->controller->widget('ext.widgets.ListAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'legalCityId',
                        'multiple' => false,
                        'selectedItems' => array('checked'=>$model->legalCityId),
                        'runSynonym' => true,
                        'htmlOptions' => array(
                            'labelOptions' => array(
                                'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
                                'style' => 'color: #515056 !important;',
                            ),
                        ),
                        'itemOptions' => array(
                            'data' => CHtml::listData(City::model()->findAll(), 'id', 'name'),
                            'value' => 'id',
                            'text' => 'name',
                        )
                    ), true),
                    /*H::dropDownListActiveGroup($model, 'legalCityId', 'City',
                    [
                        'groupOptions' => [
                            'class' => 'offsets',
                            'data-related' => 'city#legal'
                        ]
                    ],
                    [
                        'order' => 'name ASC'
                    ]
                )*/
            ),
        );
    }
}