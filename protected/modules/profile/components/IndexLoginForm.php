<?php
/**
 * File LoginForm.php
 */
Yii::import('application.widgets.LoginForm');

/**
 * Class LoginForm*/
class IndexLoginForm extends LoginForm
{
	/**
	 * @var array
	 */
	public $elementGroupName = '';

	/**
	 * @var string
	 */
	public $template = '
		<div class="cols-row offset-small-l-5 offset-small-r-5">
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-12">
						{firstName}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{email}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{password}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12 submit">
						{submit-2}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12 login">
						{login-2}
					</div>
				</div>
			</div>
		</div>
	';

	public function renderElements()
	{
		$result = parent::renderElements();

		$parts['{submit-2}'] = TbHtml::submitButton(Yii::t('system', 'try for free'), [
			'class' => 'b-button d-bg-warning-dark d-text-light btn fixed-width-1 p-centralize',
			'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200)',
		]);

		$parts['{login-2}'] = Yii::t('system', 'or').' ' . TbHtml::link(Yii::t('system', 'sign in'), 'javascript://',
				[
					'data-toggle' => 'modal',
					'data-target' => '#modal-sign-up',
				]);

		return strtr($result, $parts);
	}

    /**
     * Инициализация элементов формы
     * @return array
     */
    public function initElements()
    {
        $success = '<i data-icon="&#xe426;" class="b-button b-button-icon b-input-icon d-bg-success"></i>';
        return array(
            'email' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'email', array(
                        'class' => 'input-block-level d-bg-tooltip d-bg-base--hover d-bg-base--focus d-text-dark',
                        'placeholder' => Contact::model()->getAttributeLabel('email'),
                    )) . $success
            ),
            'firstName' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'firstName', array(
                        'class' => 'input-block-level d-bg-tooltip d-bg-base--hover d-bg-base--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('firstName'),
                    )) . $success
            ),
            'password' => array(
                'input' => TbHtml::activePasswordField($this->getModel(), 'password', array(
                        'class' => 'input-block-level d-bg-tooltip d-bg-base--hover d-bg-base--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('password'),
                    )) . $success
            ),
        );
    }

}