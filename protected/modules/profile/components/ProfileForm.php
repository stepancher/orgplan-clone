<?php
/**
 * File TestForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class ProfileForm*/
class ProfileForm extends TbForm
{
    /**
     * @var array
     */
    public $elementGroupName = '';
    public $model;
    public $contact;
    /**
     * @var string
     */
    public $template = '
		<div class="cols-row">
			<div class="col-md-12 offset-small-b-3 profile__form-about-me--profile-image">
				{logo}
				{image}
				{file-button}
			</div>
		</div>
		<div class="cols-row">
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-4 profile__form-about-me--column">
						<div class="cols-row">
							<div class="col-md-12">
								{firstName}
							</div>
							<div class="col-md-12">
								{lastName}
							</div>
							<div class="col-md-12">
								{middleName}
							</div>
							<div class="col-md-12">
							{genders}
							</div>
							<div class="col-md-12">
							{dob}
							</div>
							<div class="col-md-12 performer-certificates">
							{files}
							</div>
						</div>
					</div>
					<div class="col-md-4 profile__form-about-me--column">
						<div class="cols-row">
							<div class="col-md-12">
								{cityId}
							</div>
							<div class="col-md-12">
								{companyName}
							</div>
							<div class="col-md-12">
								{post}
							</div>
						</div>
					</div>
					<div class="col-md-4 profile__form-about-me--column">
						<div class="cols-row">
							<div class="col-md-12">
								{email}
							</div>
							<div class="col-md-12">
								{phone}
							</div>
							<div class="col-md-12">
								{workPhone}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-10">
						{error-block}
					</div>
					<div class="col-md-2">
						{submit}
					</div>
				</div>
			</div>
		</div>
	';

    public function renderElements()
    {
        /**
         * @var CController $ctrl
         * @var User $model
         */
        $model = $this->getModel();

        $parts = array();
        $linkAssets = H::getImageUrl($model, 'logo', 'noUserImage');
        $assets = strstr($linkAssets, 'assets', true);
        $newLinkAssets = dirname($linkAssets) . DIRECTORY_SEPARATOR . 'user-logo-resized-' . basename($linkAssets);
        $newAbsoluteImagePath = ($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $newLinkAssets;
        if (!file_exists($newAbsoluteImagePath)) {
            $image = new Image(($assets ? dirname(Yii::app()->basePath) : Yii::app()->basePath) . $linkAssets);
            $image->resize(null, 120);
            $image->save($newAbsoluteImagePath);
        }
        foreach ($this->getElements() as $element) {
            $parts['{' . $element->name . '}'] = $this->renderElement($element);
        }

        $parts['{logo}'] = TbHtml::createInput(TbHtml::INPUT_TYPE_FILE, 'User[logoId]', '', array('class' => 'hidden', 'data-file-input' => 'user-logotype'));
        $parts['{image}'] = TbHtml::image($newLinkAssets, '', ['class' => 'profile__avatar']);
        $parts['{file-button}'] = TbHtml::button(
            Yii::t('ProfileModule.profile', 'Upload'),
            array(
                'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light btn p-offset-l-1 fixed-width-1 p-offset-b-1 p-no-t-margin',
                'data-file-input-target' => 'user-logotype'
            )
        );

        $parts['{submit}'] = TbHtml::submitButton(
            Yii::t('ProfileModule.profile', 'Save'),
            array(
                'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light btn',
            )
        );
        $parts['{error-block}'] = $this->getErrorBlock();
        $parts['{genders}'] = $this->getGenderFields();
        $parts['{role}'] = $this->getRoleFields();

        $parts['{files}'] = '';

        $parts['{companyName}'] = $this->getContactField('companyName');
        $parts['{email}'] = $this->getContactField('email');
        $parts['{phone}'] = $this->getContactField('phone');
        $parts['{workPhone}'] = $this->getContactField('workPhone');
        $parts['{post}'] = $this->getContactField('post');

        return strtr($this->template, $parts);
    }

    public function getErrorBlock()
    {
        //return '<div class="error-block f-pull-right offset-small-all-1">!!error-block!!</div>';
    }

    public function getGenderFields()
    {
        $male = $this->getModel()->gender == User::GENDER_MALE ? 'checked="checked"' : '';
        $female = ($this->getModel()->gender == User::GENDER_FEMALE) ? 'checked="checked"' : '';
        return '<div class="profile__gender">
					<label for="gender-name">' . Yii::t('ProfileModule.profile', 'Sex') . '</label>
					<div class="gender col-md-5">
						<input type="radio" name="User[gender]" class="gender-name"
						value="1" ' . $male . '>
						<label>' . Yii::t('ProfileModule.profile', 'Male') . '</label>
					</div>
					<div class="gender col-md-5">
						<input type="radio" name="User[gender]"
						value="2"' . $female . '>
						<label>' . Yii::t('ProfileModule.profile', 'Female') . '</label>
					</div>
				</div>';
    }

    public function getRoleFields()
    {
        $exponent = $this->getModel()->role == User::ROLE_EXPONENT ? 'checked="checked"' : '';
        $developer = ($this->getModel()->role == User::ROLE_DEVELOPER) ? 'checked="checked"' : '';
        return '<div class="profile__role">
					<label for="user-role">Роль</label>
					<div class="user col-md-5">
						<input type="radio" name="User[role]" class="user-role"
						value=' . User::ROLE_EXPONENT . ' ' . $exponent . '>
						<label>Заказчик</label>
					</div>
					<div class="user col-md-5">
						<input type="radio" name="User[role]"
						value=' . User::ROLE_DEVELOPER . ' ' . $developer . '>
						<label>Подрядчик</label>
					</div>
				</div>';
    }

    public function getFileButton()
    {
        /**
         * @var CController $ctrl
         * @var User $model
         */
        $ctrl = $this->getOwner();
        $model = $this->getModel();

        return '';
    }

    public function getContactField($name)
    {
        $delete = '<i data-icon="&#xe3c1" class="js-clear-field b-input-icon b-input-icon--append d-text-dark f-text--middle f-position-1 offset-large-t-1"></i>';

        return H::controlGroup(
            TbHtml::activeTextField(
                $this->contact, $name,
                array('class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark')
            ) . $delete,
            Contact::model()->getAttributeLabel($name)
        );
    }

    /**
     * Инициализация формы
     */
    public function init()
    {

        /** @var CController $ctrl */
        $ctrl = $this->getOwner();

        /** @var User $model */
        $model = $this->getModel();

        $this->elements = CMap::mergeArray(
            array_flip(array_keys($this->initElements())),
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );

        parent::init();
    }

    /**
     * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=User[]['login']
     * @param string $name
     * @param TbFormInputElement $element
     * @param bool $forButtons
     */
    public function addedElement($name, $element, $forButtons)
    {
        $element->name = $this->elementGroupName . $name;
    }

    /**
     * Инициализация элементов формы
     * @return array
     */
    public function initElements()
    {
        /**
         * @var CController $ctrl
         * @var User $model
         */
        $ctrl = $this->getOwner();
        $model = $this->getModel();

        $delete = '<i data-icon="&#xe3c1" class="js-clear-field b-input-icon b-input-icon--append d-text-dark f-text--middle f-position-1 offset-large-t-1"></i>';
        $datePicker = '
        new Protoplan.DatePicker("User_dob", 0, (new Date).toString(), {
            mousewheel: true,
            yearsLine: true,
            closeButton: true,
            fullsizeButton: true,
            startDate: \'03-18-1990\'
        });';

        Yii::app()->getClientScript()->registerScript('cross-link', $datePicker, CClientScript::POS_READY);

        return array(
            'firstName' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'firstName', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'middleName' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'middleName', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'lastName' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'lastName', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark',
                    )) . $delete
            ),
            'cityId' => array(
                'input' => Yii::app()->controller->widget('application.modules.profile.components.ListAutoComplete', array(
                    'model' => $model,
                    'attribute' => 'cityId',
                    'multiple' => false,
                    'selectedItems' => array('checked'=>$model->cityId),
                    'runSynonym' => true,
                    'htmlOptions' => array(
                        'labelOptions' => array(
                            'class' => 'd-bg-text-dark d-text-light f-contest-list js-drop-button drop-button',
                            'style' => 'color: #515056 !important;',
                        ),
                    ),
                    'itemOptions' => array(
                        'data' => CHtml::listData(City::model()->findAll(), 'id', 'name'),
                        'value' => 'id',
                        'text' => 'name',
                    )
                ), true),
            ),
            'dob' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'dob', array(
                        'class' => 'input-block-level d-bg-input d-bg-secondary-input--hover d-bg-tab--focus d-text-dark datePickerUser_dob',
                    ))
            ),
        );
    }
}