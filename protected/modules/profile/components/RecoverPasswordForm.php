<?php
/**
 * File RecoverPasswordForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class RecoverPasswordForm*/
class RecoverPasswordForm extends TbForm
{
	/**
	 * @var array
	 */
	public $elementGroupName = '';

	/**
	 * @var string
	 */
	public $template = '
		<div class="cols-row offset-small-l-5 offset-small-r-5">
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-12">
						{email}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{text}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{submit}
					</div>
				</div>
			</div>
		</div>
	';

	public function renderElements()
	{
		$parts = array();
		foreach ($this->getElements() as $element) {
			$parts['{' . $element->name . '}'] = $this->renderElement($element);
		}
		$parts['{submit}'] = TbHtml::submitButton(Yii::t('system', 'Recover'), [
			'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize',
			'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200)',
		]);
		$parts['{text}'] = "<div class='terms-of-use'>" .
			Yii::t('site', 'Please enter your e-mail. At the specified address will be sent a password reset link.') .
		"</div>";

		return strtr($this->template, $parts);
	}

	/**
	 * Инициализация формы
	 */
	public function init()
	{
		/** @var CController $ctrl */
		$ctrl = $this->getOwner();

		/** @var Contact $model */
		$model = $this->getModel();

		$this->elements = CMap::mergeArray(
			array_flip(array_keys($this->initElements())),
			ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
			$this->initElements()
		);

		parent::init();
	}

	/**
	 * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=User[]['login']
	 * @param string $name
	 * @param TbFormInputElement $element
	 * @param bool $forButtons
	 */
	public function addedElement($name, $element, $forButtons)
	{
		$element->name = $this->elementGroupName . $name;
	}

	/**
	 * Инициализация элементов формы
	 * @return array
	 */
	public function initElements()
	{
		$success = '<i data-icon="&#xe00a;" class="b-button b-button-icon b-input-icon d-bg-success"></i>';
		return array(
			'email' => array(
				'input' => TbHtml::activeTextField($this->getModel(), 'email', array(
						'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark',
						'placeholder' => Contact::model()->getAttributeLabel('email'),
					)) . $success
			),
		);
	}
}