<?php
/**
 * File LoginForm.php
 */
Yii::import('ext.helpers.ARForm');

/**
 * Class LoginForm*/
class LoginForm extends TbForm
{
    /**
     * @var array
     */
    public $elementGroupName = '';

    /**
     * @var string
     */
    public $template = '
		<div class="cols-row offset-small-l-5 offset-small-r-5">
			<div class="col-md-12">
				<div class="cols-row">
					<div class="col-md-12">
						{email}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{firstName}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{password}
					</div>
				</div>
    			<div class="cols-row">
					<div class="col-md-12">
						{repeatPassword}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{content}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{termsOfUse}
					</div>
				</div>
				<div class="cols-row">
					<div class="col-md-12">
						{submit}
					</div>
				</div>
			</div>
		</div>
	';

    public function renderElements()
    {
        $parts = array();
        foreach ($this->getElements() as $element) {
            $parts['{' . $element->name . '}'] = $this->renderElement($element);
        }

        if ($this->getModel()->getScenario() == 'login') {
            $content = CHtml::link(Yii::t('site', 'Forgot your password?'), 'javascript://', array('onclick' => 'toggleSignUpContent()'));
            $content = "<div class='terms-of-use'>$content</div>";
            $buttonLabel = Yii::t('system', 'Sign In');
        } elseif ($this->getModel()->getScenario() == 'registration-left-menu') {
            $content = '<div class="terms-of-use">
				<a href="' . Yii::app()->createUrl('/help/default/termsOfUse') . '">' . Yii::t('system', 'Terms of use') . '</a></br>
			    <a href="' . Yii::app()->createUrl('/help/default/confidentiality') . '">' . Yii::t('system', 'Privacy') . '</a>
			</div>';
            $buttonLabel = Yii::t('system', 'try for free');
        } else {
            $content = '<div class="terms-of-use">
				<a href="' . Yii::app()->createUrl('/help/default/termsOfUse') . '">' . Yii::t('system', 'Terms of use') . '</a></br>
			    <a href="' . Yii::app()->createUrl('/help/default/confidentiality') . '">' . Yii::t('system', 'Privacy') . '</a>
			</div>';
            $buttonLabel = Yii::t('system', 'Send');
        }

        $parts['{submit}'] = TbHtml::submitButton($buttonLabel, [
            'class' => 'b-button d-bg-success-dark d-bg-success--hover d-text-light btn fixed-width-1 p-centralize',
            'onclick' => '$(".modal__modal-content .js-ajax-loader").fadeIn(200);',
        ]);

        $parts['{content}'] = $content;

        return strtr($this->template, $parts);
    }

    /**
     * Инициализация формы
     */
    public function init()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();

        /** @var User $model */
        $model = $this->getModel();

        $this->elements = CMap::mergeArray(
            array_flip(array_keys($this->initElements())),
            ARForm::createElements($ctrl, $model, $model->description(), $this->elementGroupName),
            $this->initElements()
        );

        parent::init();
    }

    /**
     * Добавляем префикс в аттрибут name (например: "[]") для обработки полей формы с name=User[]['login']
     * @param string $name
     * @param TbFormInputElement $element
     * @param bool $forButtons
     */
    public function addedElement($name, $element, $forButtons)
    {
        $element->name = $this->elementGroupName . $name;
    }

    /**
     * Инициализация элементов формы
     * @return array
     */
    public function initElements()
    {
        $success = '<i data-icon="&#xe426;" class="b-button b-button-icon b-input-icon d-bg-success"></i>';
        return array(
            'email' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'email', array(
                        'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark',
                        'placeholder' => Contact::model()->getAttributeLabel('email'),
                    )) . $success
            ),
            'firstName' => array(
                'input' => TbHtml::activeTextField($this->getModel(), 'firstName', array(
                        'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('firstName'),
                    )) . $success
            ),
            'password' => array(
                'input' => TbHtml::activePasswordField($this->getModel(), 'password', array(
                        'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('password'),
                    )) . $success
            ),
            'repeatPassword' => array(
                'input' => TbHtml::activePasswordField($this->getModel(), 'repeatPassword', array(
                        'class' => 'input-block-level d-bg-secondary-input d-bg-tab--hover d-bg-tab--focus d-text-dark',
                        'placeholder' => User::model()->getAttributeLabel('repeatPassword'),
                    )) . $success
            ),            'termsOfUse' => array(
                'type' => TbHtml::INPUT_TYPE_CHECKBOX,
                'placeholder' => $this->getModel()->getAttributeLabel('termsOfUse'),
                'groupOptions' => array(
                    'class' => 'terms-of-use'
                )
            ),
        );
    }
}