<?php
/**
 * @var ARController    $this
 * @var string          $tabId
 * @var integer         $year
 * @var array           $columns
 * @var string          $country
 * @var RUEFModule      $module
 * @var Fair            $model
 */
?>
<?php $controllerId = Yii::app()->controller->id; ?>
<?php $actionId = $this->action->id; ?>
<?php $module = $this->module; ?>

<?php $module->tabs($actionId); ?>
<br/>
<br/>
<br/>
<?php if(!empty($h1)) : ?>
<h1>
    <?php echo $h1->name; ?>
</h1>
<?php endif;?>
<?php if($country == 'Russia') : ?>
<?php $module->grid($model, $columns);?>
<?php elseif ($country == 'CIS') : ?>
<?php $module->grid($model, $columns, 'CIS');?>
<?php endif; ?>

<p class="pull-left">

    <?php

    $urlParams = array();
    if(isset($_SERVER['QUERY_STRING']))
        parse_str($_SERVER['QUERY_STRING'], $urlParams);
    ?>

    <?php echo CHtml::link(
        TbHtml::button(
            Yii::t('RUEFModule.ruef', "Export"),
            ['class'=>'b-button d-bg-success-dark d-bg-success--hover d-text-light btn']
        ),
        $this->createUrl(
            "/RUEF/{$controllerId}/{$actionId}",
            [
                'year' => $year,
                'export' => $module::RUN_EXPORT,

            ] + $urlParams
        )
    )?>
</p>

<div style="clear:both"></div>