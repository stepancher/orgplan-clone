<?php

class RegionReportController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow admin user to perform 'FairReport' action
                'actions' => array('report'),
                'roles' => array(User::ROLE_ADMIN, User::ROLE_RUEF),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionReport() {

        $year = Yii::app()->request->getQuery('year', date('Y'));

        $model = new RUEFRegion('search');

        if(isset($_GET['RUEFRegion']))
            $model->setAttributes($_GET['RUEFRegion']);

        $model->year = $year;

        $this->pageTitle = "Регионы РФ - $year. Отчет РСВЯ | Protoplan";

        $columns = $this->module->columnsAgg('region name', $model);
        
        if((bool)Yii::app()->request->getQuery('export', FALSE)){
            $this->module->export($model, $columns);
            Yii::app()->request->redirect('RUEFReport/regionReport', ['year'=>$year]);
        }

        $this->render('/RUEF/_report', array(
            'model' => $model,
            'year' => $year,
            'columns' => $columns,
            'export' => (bool)Yii::app()->request->getQuery('export', FALSE),
            'country' => 'Russia',
        ));
        
    }

}