<?php

class IndustryReportController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow admin user to perform 'FairReport' action
                'actions' => array('report'),
                'roles' => array(User::ROLE_ADMIN, User::ROLE_RUEF),
            ),
            array('allow',  // allow admin user to perform 'FairReport' action
                'actions' => array('CISReport'),
                'roles' => array(User::ROLE_ADMIN, User::ROLE_RUEF),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionReport() {

        $year = Yii::app()->request->getQuery('year', date('Y'));

        $model = new RUEFIndustry('search');
        /**
         * @var RUEFModule  $module
         */
        $module = $this->module;

        if(isset($_GET['RUEFIndustry']))
            $model->setAttributes($_GET['RUEFIndustry']);

        $model->year = $year;

        $this->pageTitle = "Отрасли РФ - $year. Отчет РСВЯ | Protoplan";

        $columns = $module->columnsAgg('industry name', $model);

        if((bool)Yii::app()->request->getQuery('export', FALSE)){
            $module->export($model, $columns);
            Yii::app()->request->redirect('RUEFReport/industryReport', ['year' => $year]);
        }

        $this->render('/RUEF/_report', array(
            'model' => $model,
            'year' => $year,
            'columns' => $columns,
            'export' => (bool)Yii::app()->request->getQuery('export', FALSE),
            'country' => 'Russia',
        ));

    }

    public function actionCISReport() {

        $year = Yii::app()->request->getQuery('year', date('Y'));

        $model = new RUEFIndustry('search');

        if(isset($_GET['RUEFIndustry']))
            $model->setAttributes($_GET['RUEFIndustry']);

        $model->year = $year;

        $this->pageTitle = "Отрасли СНГ - $year. Отчет РСВЯ | Protoplan";

        $columns = $this->module->columnsAgg('industry name', $model, 'CIS');

        if((bool)Yii::app()->request->getQuery('export', FALSE)){
            $this->module->export($model, $columns, 'CIS');
            Yii::app()->request->redirect('RUEFReport/CISIndustryReport', ['year'=>$year]);
        }

        $this->render('/RUEF/_report', array(
            'model' => $model,
            'year' => $year,
            'columns' => $columns,
            'export' => (bool)Yii::app()->request->getQuery('export', FALSE),
            'country' => 'CIS',
        ));
    }

}