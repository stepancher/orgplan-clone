<?php

class FairReportController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow admin user to perform 'FairReport' action
                'actions' => array('report'),
                'roles' => array(User::ROLE_ADMIN, User::ROLE_RUEF),
            ),
            array('allow',  // allow admin user to perform 'CISFairReport' action
                'actions' => array('CISReport'),
                'roles' => array(User::ROLE_ADMIN, User::ROLE_RUEF),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionReport() {

        $model = new RUEFFair('search');
        $h1 = '';

        if(isset($_GET['RUEFFair'])){
            $model->setAttributes($_GET['RUEFFair']);

            if(!empty($_GET['industryId'])){
                $model->setAttribute('industryId', $_GET['industryId']);
                $h1 = Industry::model()->findByPk($_GET['industryId']);
            } elseif (!empty($_GET['regionId'])){
                $model->setAttribute('regionId', $_GET['regionId']);
                $h1 = Region::model()->findByPk($_GET['regionId']);
            } elseif (!empty($_GET['districtId'])){
                $model->setAttribute('districtId', $_GET['districtId']);
                $h1 = District::model()->findByPk($_GET['districtId']);
            }
        }

        $model->year = Yii::app()->request->getQuery('year', date('Y'));

        $this->pageTitle = "Выставки РФ - $model->year. Отчет РСВЯ | Protoplan";

        $columns = $this->module->columnsFair($model);

        if((bool)Yii::app()->request->getQuery('export', FALSE)){
            $this->module->export($model, $columns);
            Yii::app()->request->redirect('fairReport/report', ['year'=>$model->year]);
        }

        $this->render('/RUEF/_report', array(
            'model' => $model,
            'year' => $model->year,
            'columns' => $columns,
            'export' => (bool)Yii::app()->request->getQuery('export', FALSE),
            'country' => 'Russia',
            'h1' => $h1,
        ));
    }

    public function actionCISReport() {

        $model = new RUEFFair('search');
        $h1 = '';

        if(isset($_GET['RUEFFair'])){
            $model->setAttributes($_GET['RUEFFair']);

            if(!empty($_GET['industryId'])){
                $model->setAttribute('industryId', $_GET['industryId']);
                $h1 = Industry::model()->findByPk($_GET['industryId']);
            } elseif (!empty($_GET['regionId'])){
                $model->setAttribute('regionId', $_GET['regionId']);
                $h1 = Region::model()->findByPk($_GET['regionId']);
            } elseif (!empty($_GET['districtId'])){
                $model->setAttribute('districtId', $_GET['districtId']);
                $h1 = District::model()->findByPk($_GET['districtId']);
            }
        }

        $model->year = Yii::app()->request->getQuery('year', date('Y'));

        $this->pageTitle = "Выставки СНГ - $model->year. Отчет РСВЯ | Protoplan";

        $columns = $this->module->columnsFair($model, 'CIS');

        if((bool)Yii::app()->request->getQuery('export', FALSE)){
            $this->module->export($model, $columns, 'CIS');
            Yii::app()->request->redirect('fairReport/report', ['year'=>$model->year]);
        }

        $this->render('/RUEF/_report', array(
            'model' => $model,
            'year' => $model->year,
            'columns' => $columns,
            'export' => (bool)Yii::app()->request->getQuery('export', FALSE),
            'country' => 'CIS',
            'h1' => $h1,
        ));
    }

}