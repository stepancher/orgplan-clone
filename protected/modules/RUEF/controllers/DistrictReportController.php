<?php

class DistrictReportController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow admin user to perform 'FairReport' action
                'actions' => array('report'),
                'roles' => array(User::ROLE_ADMIN, User::ROLE_RUEF),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionReport() {

        $year = Yii::app()->request->getQuery('year', date('Y'));

        $model = new RUEFDistrict('search');

        if(isset($_GET['RUEFDistrict']))
            $model->setAttributes($_GET['RUEFDistrict']);

        $model->year = $year;

        $this->pageTitle = "Округи РФ - $year. Отчет РСВЯ | Protoplan";

        $columns = $this->module->columnsAgg('district name', $model);

        if((bool)Yii::app()->request->getQuery('export', FALSE)){
            $this->module->export($model, $columns);
            Yii::app()->request->redirect('districtReport/report', ['year'=>$year]);
        }

        $this->render('/RUEF/_report', array(
            'model' => $model,
            'year' => $year,
            'columns' => $columns,
            'export' => (bool)Yii::app()->request->getQuery('export', FALSE),
            'country' => 'Russia',
        ));

    }

}