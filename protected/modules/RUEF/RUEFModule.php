<?php

class RUEFModule extends CWebModule
{

    const RUN_EXPORT = TRUE;

    public static $filters = [
        'RUEFOnly' => array(
            [
                'label' => "Все",
                'value' => 0,
                'name' => 'default',
            ],
            [
                'label' => "РСВЯ",
                'value' => 1,
                'name' => 'yes',
            ],
            [
                'label' => "не РСВЯ",
                'value' => 2,
                'name' => 'no',
            ],
        ),
        'fairHasAssociationId' => array(
            [
                'label' => "Да",
                'value' => 1,
                'name' => 'yes',
            ],
            [
                'label' => "Нет",
                'value' => 2,
                'name' => 'no',
            ],
        ),
    ];

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
            'RUEF.models.*',
        ));
    }

    /**
     * @param $actionId
     * @throws Exception
     */
    public function tabs($actionId){

        $controllerId = Yii::app()->controller->id;
        $pageTitle = Yii::app()->controller->pageTitle;

        Yii::app()->controller->widget('application.modules.RUEF.components.SimpleTabs', array(
            'base' => 'fair-redactor',
            'fixed' => true,
            'enableAjax' => false,
            'contentType' => 'json',
            'onClick' => 'js:function(o) {
                if("$tab" in o && !o.$tab.hasClass("active")) {
                $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").show();
                } else if ("$tab" in o && o.$tab.hasClass("active")){
                //ничего не делаем
                } else {
                $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").show();
                }
                }',
            'afterUpdate' => 'js:function(o) {
                changePositionDateRegion();
                $("[data-target-base=\'" + o.baseName + "\'] > div.b-search-loader-block img").hide();
                }
            ',
            'tabs' => array_filter(array_map(
                function($year) use ($actionId, $controllerId, $pageTitle) {
                    return [
                        'active' => (isset($_GET['year']) && $_GET['year'] == $year)
                                        || (!isset($_GET['year']) && !isset($_GET['RUEFFair']['year']) && $year == date('Y'))
                                        || (isset($_GET['RUEFFair']['year']) && $_GET['RUEFFair']['year'] == $year),
                        'label' => $year,
                        'target' => $year,
                        'url' => Yii::app()->createUrl("/RUEF/{$controllerId}/{$actionId}",
                            [
                                'year' => $year,
                                'RUEFFair[fairHasAssociationId]' => isset($_GET['RUEFFair']['fairHasAssociationId']) ? $_GET['RUEFFair']['fairHasAssociationId'] : '',
                                'industryId' => isset($_GET['industryId']) ? $_GET['industryId'] : '',
                                'districtId' => isset($_GET['districtId']) ? $_GET['districtId'] : '',
                                'regionId' => isset($_GET['regionId']) ? $_GET['regionId'] : '',
                            ]
                        ),
                        'pageTitle' => $pageTitle,
                    ];
                },
                Fair::fairsYear()
            ))
        ));
    }

    /**
     * @param $model
     * @param $columns
     * @param $country
     * @throws Exception
     */
    public function grid($model, $columns, $country = 'Russia'){

        if($country == 'CIS'){
            $dataProvider = $model->reportRUEF('CIS');

            if(get_class($model) == 'RUEFFair'){
                $dataProvider = $model->reportRUEF(FALSE, 'CIS');
            }

            Yii::app()->controller->widget('bootstrap.widgets.TbGridView', array(
                'id' => 'grid',
                'dataProvider' => $dataProvider,
                'filter' => $model,
                'columns' => $columns,
                'ajaxUpdate'   => false,
                'htmlOptions' => array(
                    'style' => "overflow-y: auto;"
                ),
                'template' => '{summary}{items}{summary}'
            ));
            return;
        }

        $dataProvider = $model->reportRUEF();
        Yii::app()->controller->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filter' => $model,
            'columns' => $columns,
            'ajaxUpdate'   => false,
            'htmlOptions' => array(
                'style' => "overflow-y: auto;"
            ),
            'template' => '{summary}{items}{summary}'
        ));
    }

    /**
     * @param $model
     * @param $columns
     * @param string $country
     */
    public function export($model, $columns, $country = 'Russia'){

        if($country == 'CIS'){

            $factory = new CWidgetFactory;
            $widget = $factory->createWidget(Yii::app()->controller->id, 'ext.EExcelView.EExcelView', array(
                'dataProvider' => $model->reportRUEF(FALSE,'CIS'),
                'grid_mode'=>'export',
                'libPath' => 'application.vendor.phpoffice.phpexcel.Classes.PHPExcel',
                'title'=>'Report',
                'filename'=>'report.xlsx',
                'stream'=>true,
//    'exportType'=>'Excel2007',
                'columns' => $columns,
            ));

            $widget->init();
            $widget->run();
            return;
        }

        $factory = new CWidgetFactory;
        $widget = $factory->createWidget(Yii::app()->controller->id, 'ext.EExcelView.EExcelView', array(
            'dataProvider' => $model->reportRUEF(),
            'grid_mode'=>'export',
            'libPath' => 'application.vendor.phpoffice.phpexcel.Classes.PHPExcel',
            'title'=>'Report',
            'filename'=>'report.xlsx',
            'stream'=>true,
//    'exportType'=>'Excel2007',
            'columns' => $columns,
        ));

        $widget->init();
        $widget->run();
    }

    /**
     * @param $model
     * @param string $country
     * @return array
     */
    public function columnsFair($model, $country = 'Russia'){

        if($country == 'CIS'){
            $footer = $model->reportRUEF(TRUE, 'CIS')->getData();
            $footer = reset($footer);
        } else {
            $footer = $model->reportRUEF(TRUE)->getData();
            $footer = reset($footer);
        }

        return array(
            'name' => [
                'name' => 'name',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','fair name') . ',', $footer['id']),
                'type' => 'raw',
                'value' => function($data){
                    return CHtml::link($data['name'], Yii::app()->createUrl("fair/html/view", [
                        "langId" => Yii::app()->language,
                        "urlFairName" => $data['fairShortUrl'],
                    ]), array('target'=>'_blank'));
                }
            ],
            'organizerName' => [
                'name' => 'organizerName',
                'header' => Yii::t('RUEFModule.ruef','organizer name'),
            ],
            'fairHasAssociationId' => [
                'name' => 'fairHasAssociationId',
                'filter' => array_reduce(self::$filters['fairHasAssociationId'],function($res, $i){$res[$i['value']] =  $i['label']; return $res;}),
                'header' => self::getHeader(Yii::t('RUEFModule.ruef', 'ruef fair') . ',', $footer['fairHasAssociationId'] . ' ' . Yii::t('RUEFModule.ruef', 'pcs')),
                'type' => 'raw',
                'value' => function($el){
                    if($el['fairHasAssociationId'] == 1)
                        return 'Да';
                    return 'Нет';
                }
            ],
            'publicProtoPlan' => [
                'name' => 'publicProtoPlan',
                'filter' => FALSE,
                'header' => Yii::t('RUEFModule.ruef', 'public on protoplan'),
                'value' => function($el) {
                    return 'Да';
                },
                'type' => 'raw',
            ],
            'squareGross' => [
                'name' => 'squareGross',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','squareGross') . ',',$footer['squareGross'] . ' ' . Yii::t('RUEFModule.ruef','square m')),
                'type' => 'raw',
            ],
            'leasedArea' => [
                'name' => 'leasedArea',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','leased area') . ',', $footer['leasedArea'] . ' ' . Yii::t('RUEFModule.ruef','square m')),
                'type' => 'raw',
            ],
            'squareNet' => [
                'name' => 'squareNet',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','squareNet') . ',', $footer['squareNet'] . ' ' . Yii::t('RUEFModule.ruef','square m')),
                'type' => 'raw',
            ],
            'members' => [
                'name' => 'members',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','members') . ',', $footer['members'] . ' ' . Yii::t('RUEFModule.ruef','pcs')),
                'type' => 'raw',
            ],
            'visitors' => [
                'name' => 'visitors',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','visitors') . ',', $footer['visitors'] . ' ' . Yii::t('RUEFModule.ruef','pcs')),
                'type' => 'raw',
            ],
        );

    }

    /**
     * @param $section
     * @param $model
     * @param string $country
     * @return array
     */
    public function columnsAgg($section, $model, $country = 'Russia'){

        $actionId = Yii::app()->controller->action->id;
        $pageTitle = Yii::app()->controller->pageTitle;

        preg_match('([0-9]+)',$pageTitle, $matches);
        $year = $matches[0] - 1;

        if($country == 'CIS'){
            $footer = $model->aggReport('CIS')->getData();
            $footer = reset($footer);
            $urlPrefix = 'CIS';
        } else {
            $footer = $model->aggReport()->getData();
            $footer = reset($footer);
            $urlPrefix = '';
        }

        if($section == 'district name'){
            $header = Yii::t('RUEFModule.ruef', 'district name');
        } elseif ($section == 'industry name'){
            $header = Yii::t('RUEFModule.ruef', 'industry name');
        } elseif ($section == 'region name'){
            $header = Yii::t('RUEFModule.ruef', 'region name');
        } else {
            $header = '';
        }
        $fairsYears = Fair::fairsYear();
        $firstActiveFairYear = array_pop($fairsYears);

        return array(
            'name' => [
                'name' => 'name',
                'header' => self::getHeader($header),
                'type' => 'raw',
                'value' => function($data) use($model, $urlPrefix){
                    
                    return CHtml::link($data['name'], Yii::app()->createUrl("/RUEF/fairReport/{$urlPrefix}report", [
                        'RUEFFair[fairHasAssociationId]' => $model->RUEFOnly,
                        'year' => $model->year,
                        'industryId' => get_class($model) == 'RUEFIndustry' ? $data['id'] : '',
                        'districtId' => get_class($model) == 'RUEFDistrict' ? $data['id'] : '',
                        'regionId' => get_class($model) == 'RUEFRegion' ? $data['id'] : '',
                    ]), array('target'=>'_blank'));
                }
            ],
            'RUEFOnly' => [
                'name' => 'RUEFOnly',
                'header' => self::getHeader($actionId == 'CISIndustryReport' ? Yii::t('RUEFModule.ruef', 'fairsTotal cis') : Yii::t('RUEFModule.ruef','fairsTotal') . ',', $footer['RUEFOnly'] . ' ' . Yii::t('RUEFModule.ruef','pcs')),
                'filter' => array_reduce(self::$filters['RUEFOnly'],function($res, $i){$res[$i['value']] =  $i['label']; return $res;}),
                'type' => 'raw',
            ],
            'fairsPreviousYearCount' => [
                'name' => 'fairsPreviousYearCount',
                'header' => self::getHeader(
                    $actionId == 'CISIndustryReport' ? Yii::t('RUEFModule.ruef', 'fairsPreviousYearCount cis') . ',' : Yii::t('RUEFModule.ruef','fairsPreviousYearCount') . ',',
                    sprintf("%.1f", $footer['fairsPreviousYearCount']) . '%',
                    $footer['fairsPreviousYearCountAbs'] . Yii::t('RUEFModule.ruef','in',array('$year' => $year))
                ),
                'type' => 'raw',
                'value' => function($data) use($pageTitle, $year){
                    if($data['fairsPreviousYearCount'] == '0.0')
                        $data['fairsPreviousYearCount'] = 0;

                    return CHtml::tag(
                            "div",
                            array(
                                'style' => $data['fairsPreviousYearCount'] > 0 ? "color: green" : ($data['fairsPreviousYearCount'] < 0 ? "color: red" : ''),
                                'title' => $data['fairsPreviousYearCountAbs'] . str_replace('$year', $year, Yii::t('RUEFModule.ruef','in')),
                            ),
                            $data['fairsPreviousYearCount'] > 0 ? '+'.$data['fairsPreviousYearCount'] : $data['fairsPreviousYearCount']
                    );
                },
                'visible' => (Yii::app()->request->getQuery('year') == $firstActiveFairYear) ? FALSE : TRUE,
            ],
            'squareGrossSum' => [
                'name' => 'squareGrossSum',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','squareGross') . ',', $footer['squareGrossSum'] . ' ' . Yii::t('RUEFModule.ruef','square m')),
                'type' => 'raw',
            ],
            'leasedArea' => [
                'name' => 'leasedArea',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','leased area') . ',', $footer['leasedArea'] . ' ' . Yii::t('RUEFModule.ruef','square m')),
                'type' => 'raw',
            ],
            'leasedAreaPreviousYear' => [
                'name' => 'leasedAreaPreviousYear',
                'header' => self::getHeader(
                    Yii::t('RUEFModule.ruef', 'leasedAreaPreviousYear') . ',',
                    sprintf("%.1f", $footer['leasedAreaPreviousYear']) . '%',
                    $footer['leasedAreaPreviousYearAbs'] . Yii::t('RUEFModule.ruef','in',array('$year' => $year))
                ),
                'type' => 'raw',
                'value' => function($data) use ($pageTitle, $year){
                    if($data['leasedAreaPreviousYear'] == '0.0')
                        $data['leasedAreaPreviousYear'] = 0;

                    return CHtml::tag(
                        "div",
                        array(
                            'style' => $data['leasedAreaPreviousYear'] > 0 ? "color: green" : ($data['leasedAreaPreviousYear'] < 0 ? "color: red" : ''),
                            'title' => $data['leasedAreaPreviousYearAbs'] . str_replace('$year', $year, Yii::t('RUEFModule.ruef','in')),
                        ),
                        $data['leasedAreaPreviousYear'] > 0 ? '+'.$data['leasedAreaPreviousYear'] : $data['leasedAreaPreviousYear']
                    );
                },
                'visible' => (Yii::app()->request->getQuery('year') == $firstActiveFairYear) ? FALSE : TRUE,
            ],
            'squareNetSum' => [
                'name' => 'squareNetSum',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','squareNet') . ',', $footer['squareNetSum'] . ' ' . Yii::t('RUEFModule.ruef','square m')),
                'type' => 'raw',
            ],
            'squareNetPreviousYearSum' => [
                'name' => 'squareNetPreviousYearSum',
                'header' => self::getHeader(
                    Yii::t('RUEFModule.ruef','squareNetPreviousYearSum') . ',',
                    sprintf("%.1f", $footer['squareNetPreviousYearSum']) . '%',
                    $footer['squareNetPreviousYearSumAbs'] . Yii::t('RUEFModule.ruef','in', array('$year' => $year))
                ),
                'type' => 'raw',
                'value' => function($data) use ($pageTitle, $year){
                    if($data['squareNetPreviousYearSum'] == '0.0')
                        $data['squareNetPreviousYearSum'] = 0;

                    return CHtml::tag(
                        "div",
                        array(
                            'style' => $data['squareNetPreviousYearSum'] > 0 ? "color: green" : ($data['squareNetPreviousYearSum'] < 0 ? "color: red" : ''),
                            'title' => $data['squareNetPreviousYearSumAbs'] . str_replace('$year', $year, Yii::t('RUEFModule.ruef','in')),
                        ),
                        $data['squareNetPreviousYearSum'] > 0 ? '+'.$data['squareNetPreviousYearSum'] : $data['squareNetPreviousYearSum']
                    );
                },
                'visible' => (Yii::app()->request->getQuery('year') == $firstActiveFairYear) ? FALSE : TRUE,
            ],
            'membersSum' => [
                'name' => 'membersSum',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','members') . ',', $footer['membersSum']),
                'type' => 'raw',
            ],
            'membersPreviousYearSum' => [
                'name' => 'membersPreviousYearSum',
                'header' => self::getHeader(
                    Yii::t('RUEFModule.ruef','membersPreviousYearSum') . ',',
                    sprintf("%.1f", $footer['membersPreviousYearSum']) . '%',
                    $footer['membersPreviousYearSumAbs'] . Yii::t('RUEFModule.ruef','in', array('$year' => $year))
                ),
                'type' => 'raw',
                'value' => function($data) use ($pageTitle, $year){
                    if($data['membersPreviousYearSum'] == '0.0')
                        $data['membersPreviousYearSum'] = 0;

                    return CHtml::tag(
                        "div",
                        array(
                            'style' => $data['membersPreviousYearSum'] > 0 ? "color: green" : ($data['membersPreviousYearSum'] < 0 ? "color: red" : ''),
                            'title' => $data['membersPreviousYearSumAbs'] . str_replace('$year', $year, Yii::t('RUEFModule.ruef','in')),
                        ),
                        $data['membersPreviousYearSum'] > 0 ? '+'.$data['membersPreviousYearSum'] : $data['membersPreviousYearSum']
                    );
                },
                'visible' => (Yii::app()->request->getQuery('year') == $firstActiveFairYear) ? FALSE : TRUE,
            ],
            'visitorsSum' => [
                'name' => 'visitorsSum',
                'header' => self::getHeader(Yii::t('RUEFModule.ruef','visitors') . ',', $footer['visitorsSum']),
                'type' => 'raw',
            ],
            'visitorsPreviousYearSum' => [
                'name' => 'visitorsPreviousYearSum',
                'header' => self::getHeader(
                    Yii::t('RUEFModule.ruef','visitorsPreviousYearSum') . ',',
                    sprintf("%.1f", $footer['visitorsPreviousYearSum']) . '%',
                    $footer['visitorsPreviousYearSumAbs'] . Yii::t('RUEFModule.ruef','in', array('$year' => $year))
                    ),
                'type' => 'raw',
                'value' => function($data) use ($pageTitle, $year){
                    if($data['visitorsPreviousYearSum'] == '0.0')
                        $data['visitorsPreviousYearSum'] = 0;

                    return CHtml::tag(
                        "div",
                        array(
                            'style' => $data['visitorsPreviousYearSum'] > 0 ? "color: green" : ($data['visitorsPreviousYearSum'] < 0 ? "color: red" : ''),
                            'title' => $data['visitorsPreviousYearSumAbs'] . str_replace('$year', $year, Yii::t('RUEFModule.ruef','in')),
                        ),
                        $data['visitorsPreviousYearSum'] > 0 ? '+'.$data['visitorsPreviousYearSum'] : $data['visitorsPreviousYearSum']
                    );
                },
                'visible' => (Yii::app()->request->getQuery('year') == $firstActiveFairYear) ? FALSE : TRUE,
            ],
        );

    }

    /**
     * @param null $header
     * @param null $footer
     * @param null $title
     * @return string
     */
    public function getHeader($header = NULL, $footer = NULL, $title = NULL){

        if((bool)Yii::app()->request->getQuery('export', FALSE)){

            return $header . $footer;
        }

        if($header === NULL)
            $header = '';

        if($footer === NULL)
            $footer = '';

        return "<div class='help-table-container'>
                    <div class='header-help-table'>
                        <div class='help-row'>
                            <div class='help-cell'>
                                <div class='table-header-sum'>
                                    {$header}
                                </div>
                            </div>
                        </div>
                        
                        <div class='help-row' style='margin-top: 10px; width: 90px' >
                            <div class='help-cell'>
                                <div class='table-header-name' " . ($title ? "title=\"{$title}\"" : '') . ">
                                    {$footer}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    }
}
