<?php
/**
 * Class RUEFRegion
 */

class RUEFRegion extends \AR
{
    public $id;
    public $year;
    public $name = '';
    public $fairsPreviousYearCount = '';
    public $fairsPreviousYearCountAbs = '';
    public $fairHasAssociationCount = '';
    public $fairPreviousYearAssociationCount = '';
    public $fairPreviousYearAssociationCountAbs = '';
    public $squareGrossSum = '';
    public $squareGrossAssociationSum = '';
    public $squareNetSum = '';
    public $squareNetPreviousYearSum = '';
    public $squareNetPreviousYearSumAbs = '';
    public $squareNetAssociationSum = '';
    public $membersSum = '';
    public $membersAssociationSum = '';
    public $membersPreviousYearSum = '';
    public $membersPreviousYearSumAbs = '';
    public $visitorsSum = '';
    public $visitorsPreviousYearSum = '';
    public $visitorsPreviousYearSumAbs = '';
    public $visitorsAssociationSum = '';
    public $leasedArea = '';
    public $leasedAreaPreviousYear = '';
    public $leasedAreaPreviousYearAbs = '';
    public $RUEFOnly = '';

    public $oneCharacterOpr = array(
        '>',
        '<',
        '=',
    );

    public $twoCharacterOpr = array(
        '<>',
        '<=',
        '>=',
    );

    public function tableName()
    {
        return '{{region}}';
    }

    public function rules()
    {
        return array(
            array('id,
            name,
			RUEFOnly,
			fairsPreviousYearCount,
			fairHasAssociationCount,
			fairPreviousYearAssociationCount,
			squareGrossSum,
			squareNetSum,
			squareNetPreviousYearSum,
			squareNetAssociationSum,
			membersSum,
			membersPreviousYearSum,
			visitorsSum,
			visitorsPreviousYearSum,
			visitorsAssociationSum,
			leasedArea,
			leasedAreaPreviousYear',
                'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'cities' => array(
                self::HAS_MANY,
                'City',
                'regionId'
            ),
            'district' => array(
                self::BELONGS_TO,
                'District',
                'districtId'
            ),
            'translate' => array(
                self::HAS_ONE,
                'TrRegion',
                'trParentId',
                'on' => 'translate.langId = "'.Yii::app()->language.'"',
            ),
            'fairInfo' => array(
                self::BELONGS_TO,
                'FairInfo',
                'infoId'
            ),
        );
    }

    public function aggReport($cn = 'Russia'){

        $columns = [
            [
                'name' => 'yeard',
                'expr' => 'CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
                                	  END
							) THEN YEAR(fairs.beginDate) END',
                'aggFn' => "t.yeard",
            ],
            [
                'name' => 'id',
                'expr' => 'CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
									  END
							) THEN t.id END',
                'aggFn' => "t.id",
            ],
            [
                'name' => 'infoId',
                'expr' => 'CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
									  END
							) THEN fairs.infoId END',
                'aggFn' => "t.infoId",
            ],
            [
                'name' => 'RUEFOnly',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
									  END
							) THEN fairs.id END",
                'params' => array(
                    ':RUEFOnly' => 'RUEFOnly',
                ),
                'aggFn' => "CASE WHEN (t.yeard = :YEAR) THEN COUNT(t.RUEFOnly) END",
            ],
            [
                'name' => 'fairsPreviousYearCount',
                'aggFn2' => 'CASE WHEN (IFNULL(SUM(d.RUEFOnly),0) = 0 AND IFNULL(SUM(d.fairsPreviousYearCountAbs),0) = 0) THEN 0
                                  WHEN (IFNULL(SUM(d.RUEFOnly),0) != 0 AND IFNULL(SUM(d.fairsPreviousYearCountAbs),0) = 0) THEN 100
                             ELSE 100 * (SUM(d.RUEFOnly) - SUM(d.fairsPreviousYearCountAbs))/SUM(d.fairsPreviousYearCountAbs) END',
            ],
            [
                'name' => 'fairsPreviousYearCountAbs',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
									   END
							) THEN fairs.id END",
                'aggFn' => "CASE WHEN (t.yeard = :YearPreviousYear) THEN COUNT(t.RUEFOnly) END",
            ],
            [
                'name' => 'squareGrossSum',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
                                	  END
							) THEN fairInfo.squareGross END",
                'aggFn' => "CASE WHEN (t.yeard = :YEAR) THEN t.squareGrossSum END",
            ],
            [
                'name' => 'squareNetSum',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1 
									  END
							) THEN fairInfo.squareNet END",
                'aggFn' => "CASE WHEN (t.yeard = :YEAR) THEN t.squareNetSum END",
            ],
            [
                'name' => 'squareNetPreviousYearSum',
                'aggFn2' => 'CASE WHEN (IFNULL(SUM(d.squareNetSum),0) = 0 AND IFNULL(SUM(d.squareNetPreviousYearSumAbs),0) = 0) THEN 0
                                  WHEN (IFNULL(SUM(d.squareNetSum),0) != 0 AND IFNULL(SUM(d.squareNetPreviousYearSumAbs),0) = 0) THEN 100
                             ELSE 100 * (SUM(d.squareNetSum) - SUM(d.squareNetPreviousYearSumAbs))/SUM(d.squareNetPreviousYearSumAbs) END',
            ],
            [
                'name' => 'squareNetPreviousYearSumAbs',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									  END
							) THEN fairInfo.squareNet END",
                'aggFn' => "CASE WHEN (t.yeard = :YearPreviousYear) THEN t.squareNetSum END",
            ],
            [
                'name' => 'membersSum' ,
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									   END
							) THEN fairInfo.members END",
                'aggFn' => "CASE WHEN (t.yeard = :YEAR) THEN t.membersSum END",
            ],
            [
                'name' => 'membersPreviousYearSum',
                'aggFn2' => 'CASE WHEN (IFNULL(SUM(d.membersSum),0) = 0 AND IFNULL(SUM(d.membersPreviousYearSumAbs),0) = 0) THEN 0
                                  WHEN (IFNULL(SUM(d.membersSum),0) != 0 AND IFNULL(SUM(d.membersPreviousYearSumAbs),0) = 0) THEN 100
                             ELSE 100 * (SUM(d.membersSum) - SUM(d.membersPreviousYearSumAbs))/SUM(d.membersPreviousYearSumAbs) END',
            ],
            [
                'name' => 'membersPreviousYearSumAbs',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									  END
							) THEN fairInfo.members END",
                'aggFn' => "CASE WHEN (t.yeard = :YearPreviousYear) THEN t.membersSum END",
            ],
            [
                'name' => 'visitorsSum' ,
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									  END
							) THEN fairInfo.visitors END",
                'aggFn' => "CASE WHEN (t.yeard = :YEAR) THEN t.visitorsSum END",
            ],
            [
                'name' => 'visitorsPreviousYearSum',
                'aggFn2' => 'CASE WHEN (IFNULL(SUM(d.visitorsSum),0) = 0 AND IFNULL(SUM(d.visitorsPreviousYearSumAbs),0) = 0) THEN 0
                                  WHEN (IFNULL(SUM(d.visitorsSum),0) != 0 AND IFNULL(SUM(d.visitorsPreviousYearSumAbs),0) = 0) THEN 100
                             ELSE 100 * (SUM(d.visitorsSum) - SUM(d.visitorsPreviousYearSumAbs))/SUM(d.visitorsPreviousYearSumAbs) END',
            ],
            [
                'name' => 'visitorsPreviousYearSumAbs' ,
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									  END
							) THEN fairInfo.visitors END",
                'aggFn' => "CASE WHEN (t.yeard = :YearPreviousYear) THEN t.visitorsSum END",
            ],
            [
                'name' => 'leasedArea',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									   END
							) THEN (IFNULL(fairInfo.areaClosedExhibitorsLocal, 0) + IFNULL(fairInfo.areaClosedExhibitorsForeign, 0) + IFNULL(fairInfo.areaOpenExhibitorsLocal, 0) + IFNULL(fairInfo.areaOpenExhibitorsForeign, 0)) END",
                'aggFn' => "CASE WHEN (t.yeard = :YEAR) THEN t.leasedArea END",
            ],
            [
                'name' => 'leasedAreaPreviousYear',
                'aggFn2' => 'CASE WHEN (IFNULL(SUM(d.leasedArea),0) = 0 AND IFNULL(SUM(d.leasedAreaPreviousYearAbs),0) = 0) THEN 0
                                  WHEN (IFNULL(SUM(d.leasedArea),0) != 0 AND IFNULL(SUM(d.leasedAreaPreviousYearAbs),0) = 0) THEN 100
                    ELSE 100 * (SUM(d.leasedArea) - SUM(d.leasedAreaPreviousYearAbs))/SUM(d.leasedAreaPreviousYearAbs) END',
            ],
            [
                'name' => 'leasedAreaPreviousYearAbs',
                'expr' => "CASE WHEN (CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
										   WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
										   WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
										   ELSE 1
									  END
							) THEN (IFNULL(fairInfo.areaClosedExhibitorsLocal, 0) + IFNULL(fairInfo.areaClosedExhibitorsForeign, 0) + IFNULL(fairInfo.areaOpenExhibitorsLocal, 0) + IFNULL(fairInfo.areaOpenExhibitorsForeign, 0)) END",
                'aggFn' => "CASE WHEN (t.yeard = :YearPreviousYear) THEN t.leasedArea END",
            ],
        ];

        $criteria = new CDbCriteria;

        $criteria->select = array_reduce(
            $columns,
            function($res, $col){
                if(isset($col['expr']))
                    $res[] = $col['expr'] . ' AS ' . $col['name'];
                return $res;
            },
            []
        );

        $criteria->join = "
            LEFT JOIN tbl_district district ON district.id = t.districtId
            LEFT JOIN tbl_trregion translate ON translate.trParentId = t.id AND translate.langId = :langId
            LEFT JOIN tbl_city cities ON cities.regionId = t.id
            LEFT JOIN tbl_exhibitioncomplex exhibitionComplices ON exhibitionComplices.cityId = cities.id
            LEFT JOIN tbl_fair fairs ON fairs.exhibitionComplexId = exhibitionComplices.id AND fairs.active = :active AND fairs.canceled = :canceled
            LEFT JOIN tbl_fairhasassociation fairHasAssociations ON fairHasAssociations.fairId = fairs.id AND fairHasAssociations.associationId = :associationId
            LEFT JOIN tbl_fairinfo fairInfo ON fairInfo.id = fairs.infoId
            ";

        $criteria->params[':YEAR'] = $this->year;
        $criteria->params[':YearPreviousYear'] = $this->year - 1;
        $criteria->params[':active'] = Fair::ACTIVE_ON;
        $criteria->params[':canceled'] = Fair::CANCELED_NO;
        $criteria->params[':associationId'] = Association::RUEF_ASSOCIATION_ID;
        $criteria->params[':langId'] = Yii::app()->language;
        $criteria->addInCondition('YEAR(fairs.beginDate)', [$this->year, $this->year - 1]);

        if($cn == 'Russia'){

            $criteria->addCondition("district.countryId = :countryId");
            $country = TrCountry::model()->findByAttributes(['name' => 'Россия']);
            $criteria->params[':countryId'] = $country->trParentId;

        } elseif($cn == 'CIS'){

            $countries = TrCountry::model()->findAllByAttributes(
                [
                    'name' => [
                        'Азербайджан',
                        'Беларусь',
                        'Армения',
                        'Казахстан',
                        'Молдова',
                        'Узбекистан',
                        'Грузия',
                        'Украина',
                        'Киргизия',
                        'Туркменистан',
                        'Монголия',
                    ],
                ]
            );
            $list = array();
            foreach ($countries as $country){
                $list[] = $country->trParentId;

            }
            $criteria->addInCondition('district.countryId', $list);
        }

        array_walk(
            $columns,
            function($i) use($criteria){

                $params = isset($i['params']) ? $i['params'] : NULL;

                if($params){

                    foreach($i['params'] as $paramName => $variable){
                        $criteria->params[$paramName] = $this->{$variable};
                    }
                }

                if(isset(RUEFModule::$filters[$i['name']])){
                    foreach (RUEFModule::$filters[$i['name']] as $opt){
                        $criteria->params[":{$i['name']}_{$opt['name']}"] = $opt['value'];
                    }
                }
            }
        );

        $regionList = array();
        $noAggDataProvider = $this->reportRUEF($cn);
        $noAggData = $noAggDataProvider->getData();

        if(is_array($noAggData) && !empty($noAggData)){


            foreach ($noAggData as $region){

                if(isset($region['id']))
                    $regionList[] = $region['id'];
            }
        }

        if(!empty($regionList)){
            $criteria->addInCondition('t.id', $regionList);
        }

        $baseQuery = self::model()->getCommandBuilder()->createFindCommand(self::model()->getTableSchema(),$criteria)->getText();

        $tcols = implode(', ', array_reduce(
            $columns,
            function($res, $col){

                if(isset($col['aggFn'])) {
                    $res[] = "{$col['aggFn']} AS {$col['name']}";
                }
                return $res;
            },
            []
        ));
        $dataSql = "SELECT {$tcols} FROM ({$baseQuery}) t GROUP BY t.infoId, t.yeard";

        $dcols = implode(', ', array_reduce(
            $columns,
            function($res, $col){

                if(isset($col['aggFn2'])){
                    $res[] = "{$col['aggFn2']} AS {$col['name']}";
                } else {
                    $res[] = "SUM(d.{$col['name']}) AS {$col['name']}";
                }

                return $res;
            },
            []
        ));

        $dataSql2 = "SELECT {$dcols} FROM ({$dataSql}) d";

        return new CSqlDataProvider($dataSql2,
            array(
                'params' => $criteria->params,
            )
        );
    }

    /**
     * @param string $cn
     * @return CSqlDataProvider
     */
    public function reportRUEF($cn = 'Russia') {

        $columns = [
            [
                'name' => 'id',
                'expr' => 't.id',
            ],
            [
                'name' => 'name',
                'expr' => 'translate.name',
                'compare' => ['translate.name', 'name', TRUE],
            ],
            [
                'name' => 'RUEFOnly',
                'expr' => "
                        COUNT(CASE WHEN (
                                    YEAR(fairs.beginDate) = :YEAR AND 
                                    CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                    END
                                ) THEN fairs.id END)
                ",
                'params' => array(
                    ':RUEFOnly' => 'RUEFOnly',
                ),
                'aggFn1' => 'IFNULL(SUM(t.RUEFOnly),0)',
            ],
            [
                'name' => 'fairsPreviousYearCount',
                'having' => TRUE,
                'aggFn1' => 'CASE WHEN (SUM(t.RUEFOnly) = 0 AND SUM(t.fairsPreviousYearCountAbs) = 0) THEN 0
                                  WHEN (SUM(t.RUEFOnly) != 0 AND SUM(t.fairsPreviousYearCountAbs) = 0) THEN 100
                             ELSE IFNULL(ROUND(100 * (SUM(t.RUEFOnly) - SUM(t.fairsPreviousYearCountAbs))/SUM(t.fairsPreviousYearCountAbs),1),0) END',
            ],
            [
                'name' => 'fairsPreviousYearCountAbs',
                'expr' => "COUNT(CASE WHEN (YEAR(fairs.beginDate) = :YearPreviousYear AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
                            ) THEN fairs.id END)",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.fairsPreviousYearCountAbs),0)',
            ],
            [
                'name' => 'squareGrossSum',
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YEAR AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.squareGross END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.squareGrossSum),0)',
            ],
            [
                'name' => 'squareNetSum',
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YEAR AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.squareNet END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.squareNetSum),0)',
            ],
            [
                'name' => 'squareNetPreviousYearSum',
                'having' => TRUE,
                'aggFn1' => 'CASE WHEN (SUM(t.squareNetSum) = 0 AND SUM(t.squareNetPreviousYearSumAbs) = 0) THEN 0
                                  WHEN (SUM(t.squareNetSum) != 0 AND SUM(t.squareNetPreviousYearSumAbs) = 0) THEN 100
                             ELSE IFNULL(ROUND(100 * (SUM(t.squareNetSum) - SUM(t.squareNetPreviousYearSumAbs))/SUM(t.squareNetPreviousYearSumAbs),1),0) END',
            ],
            [
                'name' => 'squareNetPreviousYearSumAbs',
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YearPreviousYear AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.squareNet END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.squareNetPreviousYearSumAbs),0)',
            ],
            [
                'name' => 'membersSum' ,
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YEAR AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.members END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.membersSum),0)',
            ],
            [
                'name' => 'membersPreviousYearSum',
                'having' => TRUE,
                'aggFn1' => 'CASE WHEN (SUM(t.membersSum) = 0 AND SUM(t.membersPreviousYearSumAbs) = 0) THEN 0
                                  WHEN (SUM(t.membersSum) != 0 AND SUM(t.membersPreviousYearSumAbs) = 0) THEN 100
                             ELSE IFNULL(ROUND(100 * (SUM(t.membersSum) - SUM(t.membersPreviousYearSumAbs))/SUM(t.membersPreviousYearSumAbs),1),0) END',
            ],
            [
                'name' => 'membersPreviousYearSumAbs',
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YearPreviousYear AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.members END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.membersPreviousYearSumAbs),0)',
            ],
            [
                'name' => 'visitorsSum' ,
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YEAR AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.visitors END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.visitorsSum),0)',
            ],
            [
                'name' => 'visitorsPreviousYearSum' ,
                'having' => TRUE,
                'aggFn1' => 'CASE WHEN (SUM(t.visitorsSum) = 0 AND SUM(t.visitorsPreviousYearSumAbs) = 0) THEN 0
                                  WHEN (SUM(t.visitorsSum) != 0 AND SUM(t.visitorsPreviousYearSumAbs) = 0) THEN 100
                             ELSE IFNULL(ROUND(100 * (SUM(t.visitorsSum) - SUM(t.visitorsPreviousYearSumAbs))/SUM(t.visitorsPreviousYearSumAbs),1),0) END',
            ],
            [
                'name' => 'visitorsPreviousYearSumAbs' ,
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YearPreviousYear AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN fairInfo.visitors END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.visitorsPreviousYearSumAbs),0)',
            ],
            [
                'name' => 'leasedArea',
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YEAR AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN (IFNULL(fairInfo.areaClosedExhibitorsLocal, 0) + IFNULL(fairInfo.areaClosedExhibitorsForeign, 0) + IFNULL(fairInfo.areaOpenExhibitorsLocal, 0) + IFNULL(fairInfo.areaOpenExhibitorsForeign, 0)) END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.leasedArea),0)',
            ],
            [
                'name' => 'leasedAreaPreviousYear',
                'having' => TRUE,
                'aggFn1' => 'CASE WHEN (SUM(t.leasedArea) = 0 AND SUM(t.leasedAreaPreviousYearAbs) = 0) THEN 0
                                  WHEN (SUM(t.leasedArea) != 0 AND SUM(t.leasedAreaPreviousYearAbs) = 0) THEN 100
                             ELSE IFNULL(ROUND(100 * (SUM(t.leasedArea) - SUM(t.leasedAreaPreviousYearAbs))/SUM(t.leasedAreaPreviousYearAbs),1),0) END',
            ],
            [
                'name' => 'leasedAreaPreviousYearAbs',
				'expr' => "CASE WHEN (YEAR(fairs.beginDate) = :YearPreviousYear AND
                                CASE WHEN (:RUEFOnly = :RUEFOnly_default) THEN 1 
                                         WHEN (:RUEFOnly = :RUEFOnly_yes) THEN fairHasAssociations.associationId = :associationId 
                                         WHEN (:RUEFOnly = :RUEFOnly_no) THEN (fairHasAssociations.id IS NULL OR fairHasAssociations.associationId != :associationId) 
                                         ELSE 1 
                                END
							) THEN (IFNULL(fairInfo.areaClosedExhibitorsLocal, 0) + IFNULL(fairInfo.areaClosedExhibitorsForeign, 0) + IFNULL(fairInfo.areaOpenExhibitorsLocal, 0) + IFNULL(fairInfo.areaOpenExhibitorsForeign, 0)) END",
                'having' => TRUE,
                'aggFn1' => 'IFNULL(SUM(t.leasedAreaPreviousYearAbs),0)',
            ],
        ];

        $criteria = new CDbCriteria;

        $criteria->select = array_reduce(
            $columns,
            function($res, $col){
				if(isset($col['expr']))
                $res[] = $col['expr'] . ' AS ' . $col['name'];
                return $res;
            },
            []
        );

        $criteria->join = "
            LEFT JOIN tbl_district district ON district.id = t.districtId
            LEFT JOIN tbl_trregion translate ON translate.trParentId = t.id AND translate.langId = :langId
            LEFT JOIN tbl_city cities ON cities.regionId = t.id
            LEFT JOIN tbl_exhibitioncomplex exhibitionComplices ON exhibitionComplices.cityId = cities.id
            LEFT JOIN tbl_fair fairs ON fairs.exhibitionComplexId = exhibitionComplices.id AND fairs.active = :active AND fairs.canceled = :canceled
            LEFT JOIN tbl_fairhasassociation fairHasAssociations ON fairHasAssociations.fairId = fairs.id AND fairHasAssociations.associationId = :associationId
            LEFT JOIN tbl_fairinfo fairInfo ON fairInfo.id = fairs.infoId
        ";

        $criteria->params[':YEAR'] = $this->year;
        $criteria->params[':YearPreviousYear'] = $this->year - 1;
        $criteria->params[':active'] = Fair::ACTIVE_ON;
        $criteria->params[':canceled'] = Fair::CANCELED_NO;
        $criteria->params[':associationId'] = Association::RUEF_ASSOCIATION_ID;
        $criteria->params[':langId'] = Yii::app()->language;

        if($cn == 'Russia'){

            $criteria->addCondition("district.countryId = :countryId");
            $country = TrCountry::model()->findByAttributes(['name' => 'Россия']);
            $criteria->params[':countryId'] = $country->trParentId;

        } elseif($cn == 'CIS'){

            $countries = TrCountry::model()->findAllByAttributes(
                [
                    'name' => [
                        'Азербайджан',
                        'Беларусь',
                        'Армения',
                        'Казахстан',
                        'Молдова',
                        'Узбекистан',
                        'Грузия',
                        'Украина',
                        'Киргизия',
                        'Туркменистан',
                        'Монголия',
                    ],
                ]
            );
            $list = array();
            foreach ($countries as $country){
                $list[] = $country->trParentId;

            }
            $criteria->addInCondition('district.countryId', $list);
        }

		$criteria->group = 't.id, fairInfo.id';

        array_walk(
            $columns,
			function($i) use($criteria){

                $compare = isset($i['compare']) ? $i['compare'] : NULL;
                $params = isset($i['params']) ? $i['params'] : NULL;

                if(is_array($compare)){
                    list($column, $property, $partialMatch) = $compare;
                    $criteria->compare($column, $this->{$property}, $partialMatch);
                }

				if($params){

					foreach($i['params'] as $paramName => $variable){
						$criteria->params[$paramName] = $this->{$variable};
					}
				}

				if(isset(RUEFModule::$filters[$i['name']])){
					foreach (RUEFModule::$filters[$i['name']] as $opt){
						$criteria->params[":{$i['name']}_{$opt['name']}"] = $opt['value'];
					}
				}
			}
		);

		$baseQuery = self::model()->getCommandBuilder()->createFindCommand(self::model()->getTableSchema(),$criteria)->getText();

		$tcols = implode(', ', array_reduce(
			$columns,
			function($res, $col){

				if(isset($col['aggFn1'])) {
					$res[] = "{$col['aggFn1']} AS {$col['name']}";
				}
				return $res;
			},
			[]
		));

		$dataSql = "SELECT t.name, t.id, {$tcols} FROM ({$baseQuery}) t GROUP BY t.id";

		array_walk(
			$columns,
			function($i) use(&$dataSql){

				$having  = isset($i['having']) ? $i['having'] : NULL;

                if($having && preg_match('([0-9]+)', $this->{$i['name']})){

                    if(in_array(substr($this->{$i['name']},0,2),$this->twoCharacterOpr)){
                        $operator = substr($this->{$i['name']},0,2);
                    } elseif(in_array(substr($this->{$i['name']},0,1),$this->oneCharacterOpr)){
                        $operator = substr($this->{$i['name']},0,1);
                    } else{
                        $operator = '=';
                    }

                    preg_match('([0-9-+.]+)', $this->{$i['name']}, $matches);

					if(strpos($dataSql,'HAVING') != 0){
                        if($matches[0] == '0'){
							$dataSql .= " AND " . $i['aggFn1'] . $operator . '0';
                        } else {
							$dataSql .= " AND " . $i['aggFn1'] . $operator . $matches[0];
                        }
                    } else{
                        if($matches[0] == '0'){
							$dataSql .= ' HAVING ' . $i['aggFn1'] . $operator . '0';
                        } else {
							$dataSql .= ' HAVING ' . $i['aggFn1'] . $operator . $matches[0];
                        }
                    }

                }
            }
        );

		return new CSqlDataProvider($dataSql,
            array(
                'params' => $criteria->params,
                'sort' => array(
					'defaultOrder'=>'t.name',
                    'attributes' => array_reduce(
                        $columns,
                        function($res, $col){
                            $res[$col['name']] = [
                                'asc'  => $col['name'],
                                'desc' => $col['name']." DESC",
                            ];
                            return $res;
                        },
                        []
                    ),
                ),
                'pagination' => FALSE,
            )
        );
    }
}