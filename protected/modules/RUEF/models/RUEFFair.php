<?php
/**
 * Class RUEFFair
 */

class RUEFFair extends \AR
{
    public $name = '';
    public $organizerName = '';
    public $year;
    public $id = '';
    public $fairHasAssociationId = '';
    public $squareGross = '';
    public $squareNet = '';
    public $members = '';
    public $visitors = '';
    public $leasedArea = '';
    public $fairsYear = '';
    public $fairShortUrl = '';
    public $industryId = '';
    public $districtId = '';
    public $regionId = '';
    public $infoId = '';

    public function tableName()
    {
        return '{{fair}}';
    }

    public function rules()
    {
        return array(
            array('name,
            organizerName,
            year,
			id,
			fairHasAssociationId,
			squareGross,
			squareNet,
			members,
			visitors,
			leasedArea,
			fairsYear,
			industryId,
			districtId,
			regionId,
			infoId',
                'safe', 'on' => 'search'),
        );
    }    
    
    public function relations()
    {
        return array(
            'fairHasAssociations' => array(
                self::HAS_MANY,
                'FairHasAssociation',
                'fairId'
            ),
            'exhibitionComplex' => array(
                self::BELONGS_TO,
                'ExhibitionComplex',
                'exhibitionComplexId'
            ),
            'fairInfo' => array(
                self::BELONGS_TO,
                'FairInfo',
                'infoId'
            ),
            'translate' => array(
                self::HAS_ONE,
                'TrFair',
                'trParentId',
                'on' => 'translate.langId = "'.Yii::app()->language.'"',
            ),
            'fairHasIndustries' => array(
                self::HAS_MANY,
                'FairHasIndustry',
                'fairId',
            ),
        );
    }

    /**
     * @param bool $agg
     * @param string $cn
     * @return CSqlDataProvider
     */
    public function reportRUEF($agg = FALSE, $cn = 'Russia') {

        $columns = [
            [
                'name' => 'fairShortUrl',
                'expr' => 'translate.shortUrl',
            ],
            [
                'name' => 'organizerName',
                'expr' => 'trorg.name',
                'compare' => ['trorg.name', 'organizerName', TRUE],
            ],
            [
                'name' => 'name',
                'expr' => 'translate.name',
                'compare' => ['translate.name', 'name', TRUE],
            ],
            [
                'name' => 'id',
                'expr' => 't.id',
                'aggFn1' => 'COUNT(t.id)',
                'aggFn2' => 'SUM(d.id)',
            ],
            [
                'name' => 'infoId',
                'expr' => 't.infoId',
                'compare' => ['t.infoId', 'infoId', TRUE]
            ],
            [
                'name' => 'fairHasAssociationId',
                'expr' => 'fairHasAssociations.associationId',
                'aggFn1' => 'COUNT(t.fairHasAssociationId)',
                'aggFn2' => 'IFNULL(SUM(d.fairHasAssociationId),0)',
            ],
            [
                'name' => 'squareGross',
                'expr' => 'IFNULL(fairInfo.squareGross,0)',
                'compare'   => ['IFNULL(fairInfo.squareGross,0)', 'squareGross', FALSE],
                'aggFn1' => 't.squareGross',
                'aggFn2' => 'IFNULL(SUM(d.squareGross),0)',
            ],
            [
                'name' => 'squareNet',
                'expr' => 'IFNULL(fairInfo.squareNet,0)',
                'compare' => ['IFNULL(fairInfo.squareNet,0)', 'squareNet', FALSE],
                'aggFn1' => 't.squareNet',
                'aggFn2' => 'IFNULL(SUM(d.squareNet),0)',
            ],
            [
                'name' => 'members',
                'expr' => 'IFNULL(fairInfo.members,0)',
                'compare' => ['IFNULL(fairInfo.members,0)', 'members', FALSE],
                'aggFn1' => 't.members',
                'aggFn2' => 'IFNULL(SUM(d.members),0)',
            ],
            [
                'name' => 'visitors',
                'expr' => 'IFNULL(fairInfo.visitors,0)',
                'compare' => ['IFNULL(fairInfo.visitors,0)', 'visitors', FALSE],
                'aggFn1' => 't.visitors',
                'aggFn2' => 'IFNULL(SUM(d.visitors),0)',
            ],
            [
                'name' => 'leasedArea',
                'expr' => 'IFNULL(fairInfo.areaClosedExhibitorsLocal, 0) + IFNULL(fairInfo.areaClosedExhibitorsForeign, 0) + IFNULL(fairInfo.areaOpenExhibitorsLocal, 0) + IFNULL(fairInfo.areaOpenExhibitorsForeign, 0)',
                'compare' => ['IFNULL(fairInfo.areaClosedExhibitorsLocal, 0) + IFNULL(fairInfo.areaClosedExhibitorsForeign, 0) + IFNULL(fairInfo.areaOpenExhibitorsLocal, 0) + IFNULL(fairInfo.areaOpenExhibitorsForeign, 0)', 'leasedArea', FALSE],
                'aggFn1' => 't.leasedArea',
                'aggFn2' => 'IFNULL(SUM(d.leasedArea),0)',
            ],
        ];

        $criteria = new CDbCriteria;

        $criteria->join = "
            LEFT JOIN tbl_fairhasassociation fairHasAssociations ON fairHasAssociations.fairId = t.id AND fairHasAssociations.associationId = :associationId
            LEFT JOIN tbl_trfair translate ON translate.trParentId = t.id AND translate.langId = :langId
            LEFT JOIN tbl_fairinfo fairInfo ON fairInfo.id = t.infoId
            LEFT JOIN tbl_fairhasorganizer fho ON fho.fairId = t.id
            LEFT JOIN tbl_organizer o ON o.id = fho.organizerId
            LEFT JOIN tbl_trorganizercompany trorg ON trorg.trParentId = o.companyId AND trorg.langId = :langId
            LEFT JOIN tbl_exhibitioncomplex exhibitionComplex ON exhibitionComplex.id = t.exhibitionComplexId
            LEFT JOIN tbl_city city ON city.id = exhibitionComplex.cityId
            LEFT JOIN tbl_region region ON region.id = city.regionId
            LEFT JOIN tbl_district district ON district.id = region.districtId
        ";

        $criteria->addCondition("YEAR(t.beginDate) = :YEAR");
        $criteria->params[':YEAR'] = $this->year;
        $criteria->addCondition("t.active = :active");
        $criteria->addCondition("t.canceled = :canceled");
        $criteria->params[':active'] = Fair::ACTIVE_ON;
        $criteria->params[':canceled'] = Fair::CANCELED_NO;
        $criteria->params[':langId'] = Yii::app()->language;
        $criteria->params[':associationId'] = Association::RUEF_ASSOCIATION_ID;

        if(!empty($this->industryId)){

            $criteria->join .= "LEFT JOIN tbl_fairhasindustry fairHasIndustries ON fairHasIndustries.fairId = t.id";

            $criteria->addCondition("fairHasIndustries.industryId = :industryId");
            $criteria->params[':industryId'] = $this->industryId;
        } elseif (!empty($this->districtId)){

            $criteria->addCondition("district.id = :districtId");
            $criteria->params[':districtId'] = $this->districtId;
        } elseif (!empty($this->regionId)){

            $criteria->addCondition("region.id = :regionId");
            $criteria->params[':regionId'] = $this->regionId;
        }

        if($cn == 'Russia'){

            $criteria->addCondition("district.countryId = :countryId");
            $country = TrCountry::model()->findByAttributes(['name' => 'Россия']);
            $criteria->params[':countryId'] = $country->trParentId;

        } elseif($cn == 'CIS'){

            $countries = TrCountry::model()->findAllByAttributes(
                [
                    'name' => [
                        'Азербайджан',
                        'Беларусь',
                        'Армения',
                        'Казахстан',
                        'Молдова',
                        'Узбекистан',
                        'Грузия',
                        'Украина',
                        'Киргизия',
                        'Туркменистан',
                        'Монголия',
                    ],
                ]
            );
            $list = array();
            foreach ($countries as $country){
                $list[] = $country->trParentId;

            }
            $criteria->addInCondition('district.countryId', $list);
        }

        array_walk(
            $columns,
            function($i) use($criteria, $agg){

                $compare   = isset($i['compare'])   ? $i['compare']   : NULL;
                $having    = isset($i['having'])    ? $i['having']    : NULL;

                if(is_array($compare)){
                    list($column, $property, $partialMatch) = $compare;
                    $criteria->compare($column, $this->{$property}, $partialMatch);
                }

                if($having && !$agg && (!empty($this->{$i['name']}) || $this->{$i['name']} === '0')){
                    $criteria->having .= $i['name'] . ' = ' . $this->{$i['name']};
                }

                if(isset(RUEFModule::$filters[$i['name']]) && !empty($this->{$i['name']})){

                    $criteria->params[':associationId'] = Association::RUEF_ASSOCIATION_ID;

                    if($this->{$i['name']} == 1){

                        $criteria->addCondition("{$i['expr']} = :associationId");
                    } elseif ($this->{$i['name']} == 2){

                        $criteria->addCondition("{$i['expr']} != :associationId OR fairHasAssociations.id IS NULL");
                    }
                }
            }
        );

        $criteria->select = array_reduce(
            $columns,
            function($res, $col){
                $res[] = $col['expr'] . ' AS ' . $col['name'];
                return $res;
            },
            []
        );

        $baseQuery = self::model()->getCommandBuilder()->createFindCommand(self::model()->getTableSchema(),$criteria)->getText();

        if($agg){

            $tcol = implode(', ', array_reduce(
                $columns,
                function($res, $col){

                    if(isset($col['aggFn1']))
                        $res[] = "{$col['aggFn1']} AS {$col['name']}";

                    return $res;
                }
            ));

            $cols = implode(', ', array_reduce(
                $columns,
                function($res, $col){

                    if(isset($col['aggFn2'])) {
                        $res[] = "{$col['aggFn2']} AS {$col['name']}";
                    }

                    return $res;
                },
                []
            ));

            $dataSql = "SELECT {$cols} FROM (SELECT {$tcol} FROM ({$baseQuery}) t GROUP BY t.infoId) d";
            
            return new CSqlDataProvider($dataSql,
                array(
                    'params' => $criteria->params,
                )
            );
        }

        return new CSqlDataProvider($baseQuery,
            array(
                'params' => $criteria->params,
                'sort' => array(
                    'defaultOrder'=>'translate.name',
                    'attributes' => array_reduce(
                        $columns,
                        function($res, $col){
                            $res[$col['name']] = [
                                'asc'  => $col['name'],
                                'desc' => $col['name']." DESC",
                            ];
                            return $res;
                        },
                        []
                    ),
                ),
                'pagination' => false
            )
        );
    }
}