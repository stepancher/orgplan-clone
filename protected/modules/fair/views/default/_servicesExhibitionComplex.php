<?php
/**
 * @var CController $this
 * @var ExhibitionComplex $model
 */
$this->widget(
	'application.modules.fair.components.FieldSetView',
	array(
		'header' => Yii::t('FairModule.fair', 'Contact Information'),
		'items' => array(
			array(
				'application.widgets.ExhibitionComplexDetailView',
				'data' => $model,
			),
		)
	)
);