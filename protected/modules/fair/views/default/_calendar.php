<?php
/**
 * @var CController $this
 * @var Fair $model
 * @var CWebApplication $app
 * @var string $activeClass
 */

$app = Yii::app();
$am = $app->getAssetManager();
/*$task = Task::model()->findByAttributes(array('fairId' => $model->id));*/
Yii::import('application.widgets.CalendarFairGridView');

$ctrl = $this;
$ctrl->widget('application.modules.fair.components.FieldSetView', array(
	'items' => array(
		array(
			'application.widgets.CalendarFairGridView',
			'model' => CalendarExhibition::model(),
			'enableFilter' => false,
			'template' => '{items}',
			'fair' => $model,
			'htmlOptions' => array(
				'class' => 'table-bordered grid-view--simple g-color-tr'
			),
			'columnsAppend' => array(
				'activate' => array(
					'type' => 'html',
					'value' => function ($data) use ($ctrl, $am, $app, $model) {
						$label = TbHtml::imageButton($am->publish($app->theme->basePath . '/assets' . '/img/bell.png'), array('style' => 'background:none;margin-left:26px'));
						$url = $ctrl->createUrl('task/addToCalendar', array("id" => $data->id));
						$options = array('title' => Yii::t('FairModule.fair', 'Add to Calendar'));
						echo CHtml::link($label, $url, $options);
					},
				)

			)
		),
	)
));