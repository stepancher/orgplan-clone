<?php
/**
 * @var CController $this
 * @var Organizer $model
 */
$this->widget(
	'application.modules.fair.components.FieldSetView',
	array(
		'header' => Yii::t('FairModule.fair', 'Contact the organizer'),
		'items' => array(
			array(
				'application.modules.fair.components.OrganizerDetailView',
				'data' => $model,
			),
		)
	)
);