<?php
/**
 * @var CController $this
 * @var ExhibitionComplex $model
 * @var CWebApplication $app
 */
$app = Yii::app();
$am = $app->getAssetManager();
$cs = $app->getClientScript();
$cs->registerScriptFile(
	$am->publish($app->theme->basePath . '/assets' . '/js/map/ya_map.js')
);
?>
	<script type="text/javascript">
		$(function () {
			ExhibitionComplex.position = "<?=$model->coordinates ? : '55.76, 37.64'?>";
			ExhibitionComplex.url = "<?=Yii::app()->createUrl('exhibitionComplex/getMapData')?>";
			ymaps.ready(ExhibitionComplex.actionView.init);
		})
	</script>
<?php

$this->widget(
	'application.modules.fair.components.FieldSetView',
	array(
		'items' => array(
			array(
				'application.modules.fair.components.ActionsView',
				'items' => array(
					'orderTickets' => $app->user->getState('role') !== User::ROLE_ORGANIZERS && !$app->user->isGuest ? array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('FairModule.fair', 'Order tickets'),
						'attributes' => array(
//								'url' => $this->createUrl('exhibitionComplex/view', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_WARNING,
							'disabled' => true,
						)
					) : array(),
					'orderHotel' => $app->user->getState('role') !== User::ROLE_ORGANIZERS && !$app->user->isGuest ? array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('FairModule.fair', 'Booking a hotel'),
						'attributes' => array(
//								'url' => $this->createUrl('exhibitionComplex/view', array('id' => $model->id)),
							'color' => TbHtml::BUTTON_COLOR_WARNING,
							'disabled' => true,
						)
					) : array(),
				)
			),
			array(
				'application.modules.fair.components.HtmlView',
				'content' => function ($owner) {
					/** @@var CWebApplication $app */
					$app = Yii::app();
					$am = $app->getAssetManager();

					echo TbHtml::checkBoxControlGroup('hotel', false, array('label' => Yii::t('FairModule.fair', 'Hotels'), 'class' => 'map-request'));
					echo TbHtml::checkBoxControlGroup('landmark', false, array('label' => Yii::t('FairModule.fair', 'Sights'), 'class' => 'map-request'));
					echo TbHtml::checkBoxControlGroup('cafe', false, array('label' => Yii::t('FairModule.fair', 'Cafe'), 'class' => 'map-request'));
					echo '<div id="map" style="width: 100%; height: 400px">';
					echo TbHtml::image($am->publish($app->theme->basePath . '/assets' . '/img/ajax-loader.gif'), Yii::t('FairModule.fair', 'loading'), array(
						'title' => Yii::t('FairModule.fair', 'loading'),
                        'style' => 'position:absolute; z-index: 99; display: none',
						'class' => 'ajax-loader'
					));
					echo '</div>';
				}
			)
		)
	)
);