<?php
/**
 * @var CController $this
 * @var UsefulInformation $model
 */
$ctrl = $this;
$this->widget('application.modules.fair.components.FieldSetView', array(
		'header' => Yii::t('FairModule.fair', 'Select a destination and follow the advice'),
		'items' => array(
			array(
				'application.modules.fair.components.HtmlView',
				'content' => function () use ($model) {
						echo TbHtml::form('', 'post', array('id' => 'search'));
						$subTypes = SubType::model()->findAll();
						if (!empty($subTypes)) {
							$htmlArray = array();

							foreach ($subTypes as $subType) {
								$checked = MyFairViewData::model()->findByAttributes(array(
										'userId' => Yii::app()->user->id,
										'fairId' => $_GET['id'],
										'usefulCBId' => $subType->id
									)) !== null;
								$htmlArray['radio'][] = TbHtml::radioButton(
									'UsefulInformation[subTypeId][]',
									$checked,
									array('label' => $subType->name,
										'value' => $subType->id,
										'data-id' => $_GET['id'],
									)
								);
								$htmlArray['image'][] = TbHtml::image(
									H::getImageUrl($subType, 'image'),
									'',
									array(
										'style' => $checked ? 'display:block' : 'display:none',
										'data-sub-type-id' => $subType->id,
									)
								);
							}
							if (isset($htmlArray['radio'], $htmlArray['image'])) {
								foreach ($htmlArray['radio'] as $v) {
									echo $v;
								}
								foreach ($htmlArray['image'] as $v) {
									echo $v;
								}
							}
						};
						echo TbHtml::endForm();
					}
			),
		),
	)
);
?>
<script type="text/javascript">
	$(function () {
		var $radio = $('input[type="radio"]');
		$radio.change(function () {
			$('#search img').hide();
			$('#search img[data-sub-type-id="' + $(this).val() + '"]').show();
			$.get('<?=Yii::app()->createUrl('fair/saveMyFairViewData')?>', {id: $(this).data('id'), usefulCBId: $(this).val() }, function (data) {
			}, 'html');
		});
	})
</script>

<style>
	.radio input {
		margin-top: 4px;
	}
</style>

