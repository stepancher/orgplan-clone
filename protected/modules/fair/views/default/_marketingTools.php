<?php
/**
 * @var CController $this
 * @var Fair $model
 */
$ctrl = $this;
$this->widget('application.modules.fair.components.FieldSetView', array(
		'items' => array(
			array(
				'application.modules.fair.components.FieldSetView',
				'header' => Yii::t('FairModule.fair', 'Under the chosen target can be useful tools:'),
				'htmlOptions' => array('class' => ' hidden-thead'),
				'items' => array(
					array(
						'application.widgets.UsefulInformationGridView',
						'id' => 'UsefulInformationGridView',
						'compare' => array(
							array('t.type', UsefulInformation::TYPE_TARGET)
						),
						'model' => new UsefulInformation('search'),
						'fair' => $model,
						'columnsAppend' => array(
							'buttons' => array(
								'class' => 'bootstrap.widgets.TbButtonColumn',
								'template' => '',
								'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
							)
						)
					),
				)
			),
		),
	)
);
Yii::app()->clientScript->registerScript('search', "
	function UsefulInformationGridView() {
		$('#UsefulInformationGridView div.keys').attr('title','');
		$.fn.yiiGridView.update('UsefulInformationGridView',{
			data: $('#search').serialize()
		});
		return false;
	}
	$('.radio').bind('change',function(){
		return UsefulInformationGridView();
	});
		UsefulInformationGridView();
");