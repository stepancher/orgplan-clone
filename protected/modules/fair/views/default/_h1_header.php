<?php
/**
 * @var Fair $model
 */
?>
<div class="cols-row">
    <div class="col-md-8">
        <?php if (Yii::app()->user->getState('role') == User::ROLE_ADMIN): ?>
            <a href="<?= Yii::app()->createUrl('admin/fair/update', array('id' => $model->id)) ?>"
               class="b-button b-button-icon f-text--tall d-text-light d-bg-warning button-edit"
               data-icon="&#xe0ea;"></a>
        <?php endif; ?>
        <?php
        $this->renderPartial('application.modules.fair.views._blocks._b-header', ['header' => $model->loadName()]);
        ?>
        <div class="cols-row">
            <div class="col-md-12">
                <div class="f-text--base">
                    <span itemscope itemtype="http://schema.org/Organization">
                        <span itemprop="name">&nbsp;</span>
                        <span itemprop="address">&nbsp;</span>
                        <span itemprop="description">
                            <?= $model->loadDescription() ?: null ?>
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="offset-small-t-2">
            <?php

            if (IS_MODE_DEV) {
                $this->renderPartial('application.modules.fair.views._blocks._addFair', ['fairId' => $model->id]);
            }
            ?>

<!--                <a href="#"-->
<!--                   title="--><?php //echo Yii::t('FairModule.fair', 'compare') ?><!--"-->
<!--                   class="js-add-to-mach b-button b-button-icon d-bg-secondary-icon icon-back d-bg-success--hover btn b-button--no-margin p-pull-right"-->
<!--                   data-icon="&#xe3d3;"></a>-->
        </div>
    </div>
</div>