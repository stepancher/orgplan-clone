<?php
/**
 * @var CController $this
 * @var Fair $model
 */

$industries = $model->fairHasIndustries;
foreach ($industries as $FHI) {
	$industry = $FHI->industry;
	echo $industry && $industry->analytics ? $industry->analytics->description : null;
}
