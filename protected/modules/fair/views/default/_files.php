<?php
/**
 * @var FairController $this
 * @var Fair $model
 */
$ctrl = $this;
$objFileIds = array(0);
$FHS = FairHasFile::model()->findAllByAttributes(array('fairId' => $model->id));
if (!empty($FHS)) {
	$objFileIds = CHtml::listData($FHS, 'id', 'fileId');
}
$ctrl->widget(
	'application.modules.fair.components.HtmlView',
	array(
		'content' => function () use ($model) {
				echo TbHtml::thumbnails(
					H::getThumbnailsArray(
						$model->fairHasFiles, $model->id, 'file', ObjectFile::TYPE_PHOTOS_FROM_THE_SHOW
					)
				);
			}
	)
);
/*$ctrl->widget(
	'ext.widgets.FieldSetView', array(
		'header' => Yii::t('FairModule.fair', 'File layout of the exhibition'),
		'items' => array(
			array(
				'application.widgets.ObjectFileGridView',
				'model' => ObjectFile::model(),
				'compare' => array(
					array('t.id', $objFileIds),
					array('t.type', ObjectFile::TYPE_DISTRIBUTION_EXHIBITION)
				),
				'columnsAppend' => array(
					'buttons' => array(
						'class' => 'bootstrap.widgets.TbButtonColumn',
						'template' => User::checkAdministrationRoles() ? '{delete}' : null,
						'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
						'buttons' => array(
							'delete' => array(
								'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
								'url' => function ($data) use ($ctrl, $model) {
										return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
									},
								'options' => array('title' => Yii::t('FairModule.fair', 'Information'), 'class' => 'btn btn-small btn-danger'),
							),
						)
					)
				),
			)
		)
	)

);*/
/*$this->widget(
	'ext.widgets.FieldSetView', array(
		'header' => Yii::t('FairModule.fair', 'File with press releases'),
		'items' => array(
			array(
				'application.widgets.ObjectFileGridView',
				'model' => ObjectFile::model(),
				'compare' => array(
					array('t.id', $objFileIds),
					array('t.type', ObjectFile::TYPE_PRESS_RELEASE)
				),
				'columnsAppend' => array(
					'buttons' => array(
						'class' => 'bootstrap.widgets.TbButtonColumn',
						'template' => User::checkAdministrationRoles() ? '{delete}' : null,
						'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
						'buttons' => array(
							'delete' => array(
								'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
								'url' => function ($data) use ($ctrl, $model) {
										return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
									},
								'options' => array('title' => Yii::t('FairModule.fair', 'Information'), 'class' => 'btn btn-small btn-danger'),
							),
						)
					)
				),
			)
		)
	)
);*/
$ctrl->widget(
	'application.modules.fair.components.FieldSetView', array(
		'header' => Yii::t('FairModule.fair', 'File with the requirements of the exhibition'),
		'items' => array(
			array(
				'application.modules.fair.components.ObjectFileGridView',
				'model' => ObjectFile::model(),
				'compare' => array(
					array('t.id', $objFileIds),
					array('t.type', ObjectFile::TYPE_EXHIBITION_REQUIREMENTS)
				),
				'columnsAppend' => array(
					'buttons' => array(
						'class' => 'bootstrap.widgets.TbButtonColumn',
						'template' => User::checkAdministrationRoles() ? '{delete}' : null,
						'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
						'buttons' => array(
							'delete' => array(
								'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
								'url' => function ($data) use ($ctrl, $model) {
										return $ctrl->createUrl('deleteFile', array('id' => $model->id, 'fileId' => $data->id));
									},
								'options' => array('title' => Yii::t('FairModule.fair', 'Information'), 'class' => 'btn btn-small btn-danger'),
							),
						)
					)
				),
			)
		)
	)
);
