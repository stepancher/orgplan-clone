<?php
/**
 * @var CController $this
 * @var Fair $model
 */
$ctrl = $this;
$this->widget(
	'application.modules.fair.components.FieldSetView',
	array(
		'items' => array(
			array(
				'application.modules.fair.components.ActionsView',
				'items' => array(
					'tz' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('FairModule.fair', 'Create terms of reference'),
						'attributes' => array(
							'url' => !empty($model->tZs)
								&& in_array(Yii::app()->user->id, array_keys(CHtml::listData($model->tZs, 'userId', 'userId')))
									? $this->createUrl('fair/tZFair')
									: $this->createUrl('tZ/save'),
							'color' => TbHtml::BUTTON_COLOR_SUCCESS
						)
					),
					'portfolio' => array(
						'type' => TbHtml::BUTTON_TYPE_LINK,
						'label' => Yii::t('FairModule.fair', 'Choose from portfolio'),
						'attributes' => array(
							'url' => $this->createUrl('portfolio/index'),
							'color' => TbHtml::BADGE_COLOR_SUCCESS
						)
					),
				)
			),
		)
	)
);
