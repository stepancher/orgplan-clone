<?php
/**
 * @var CController $this
 * @var Fair $model
 */
$ctrl = $this;
$ctrl->widget('application.modules.fair.components.FieldSetView', array(
	'header' => Yii::t('FairModule.fair', 'List of technical specifications'),
	'items' => array(
		array(
			'application.widgets.TZGridView',
			'model' => TZ::model(),
			'compare' => array(
				array('t.userId', Yii::app()->user->id),
			),
			'columnsAppend' => array(
				'buttons' => array(
					'class' => 'bootstrap.widgets.TbButtonColumn',
					'template' => '<nobr>{show}</nobr>',
					'htmlOptions' => array('style' => 'width: 80px; text-align: left;'),
					'buttons' => array(
						'show' => array(
							'label' => TbHtml::icon(TbHtml::ICON_EYE_OPEN, array('color' => TbHtml::ICON_COLOR_WHITE)),
							'url' => function ($data) use ($ctrl) {
									return $ctrl->createUrl('view', array("id" => $data->id));
								},
							'options' => array('title' => Yii::t('FairModule.fair', 'Information'), 'class' => 'btn btn-small btn-success'),
						),
					)
				)
			)
		),
	)
));
