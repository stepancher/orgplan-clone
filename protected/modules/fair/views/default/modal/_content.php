<?php
/**
 * @var $this CController
 * @var $publishUrl string
 */

$publishUrl = Yii::app()->assetManager->publish(Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG);
?>
<div class="row-fluid modal__modal-content modal__modal-content-calculator">
	<div class="error-box"></div>
	<div class="login-access-notice">
		<span class="p-icon"></span>
		<?=Yii::t('FairModule.fair', 'calculator__body')?>
	</div>
	<div class="row"></div>
	<div class="modal__modal-content-calculator--data">
		<div class="calc-summary-text">
			<?=Yii::t('FairModule.fair', 'The total cost of participation in the exhibition');?>
			<div class="f-contest-icon" data-tooltip="tooltip">
				<i></i>
				<div id="tooltip" class="page-proposal__form-top-tooltip-block">
					<label class="page-proposal__form-tooltip-header">
					</label>
					<div class="header"><?= Yii::t('FairModule.fair','Why count so') ?></div>
					<div class="body"><?= Yii::t('FairModule.fair','Calculator help text') ?>
					</div>
				</div>
			</div>
		</div>
		<h3><img class="header-table-img" src="<?= $publishUrl ?>/img/fair/circle.png" alt=""><?=Yii::t('FairModule.fair', 'calculator__in-total')?></h3><div class="calc-summary"></div>
		<hr>
		<div class="result-list" id="result-list">
			<?php foreach (AdditionalExpenses::model()->findAll() as $k => $AE) : ?>
				<div class="result-block" data-index="<?=$k?>" data-name="<?= $AE->name ?>">
					<div class="image"><?= CHtml::image(
							H::getImageUrl($AE->getFileRelation(ObjectFile::TYPE_ADDITIONAL_EXPENSES_LIST_IMAGE), 'file')
						) ?></div>
					<div class="chart-image"><?= CHtml::image(
							H::getImageUrl($AE->getFileRelation(ObjectFile::TYPE_ADDITIONAL_EXPENSES_CHART_IMAGE), 'file')
						) ?></div>
					<div class="name" style="width: 160px"><?= $AE->name ?></div>
					<div data-percent="<?= $AE->percent ?>" data-ex-percent="<?= $AE->exPercent ?>" class="percent"><?= $AE->percent ?>%</div>
					<div class="result"></div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="modal__modal-content-calculator--chart">
		<div id="bagel-chart">

		</div>
		<div class="modal__modal-content-calculator--chart_small-circle">
			<svg>
				<circle cx="56" cy="56" r="55" fill="rgb(255,255,255)"/>
			</svg>
		</div>
		<div id="bagel-chart-layout" class="hide"></div>
	</div>
</div>
