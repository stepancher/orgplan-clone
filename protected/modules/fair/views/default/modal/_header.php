<?php

?>
<div class="f-size-24">
    <div class="b-fair__col-num-1"><?= Yii::t('FairModule.fair', 'The area of the exhibition stand') ?></div>
<!--    <div class="b-fair__col-num-3">-->
<!--        <div class="p-position-rel p-top-8">--><?//= Yii::t('FairModule.fair', 'm') ?><!--<sup>2</sup></div>-->
<!--    </div>-->
    <div class="b-fair__col-num-2">
        <?= TbHtml::createInput(
            TbHtml::INPUT_TYPE_TEXT,
            'square',
            '',
            [
                'class' => 'f-pull-right input-block-level fixed-width-modal fixed-height-modal border-none f-size-34 p-pad-input',
                'id' => 'calculator-square',
            ]
        )
        ?>
    </div>

</div>
