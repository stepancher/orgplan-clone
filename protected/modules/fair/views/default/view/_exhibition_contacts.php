<?php
/**
 * @var CWebApplication $app
 * @var CController $this
 * @var ExhibitionComplex $model
 */

$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'htmlOptions' => array(
        'class' => 'b-detail-view__table--simple table fair-tables fair-tables-contact'
    ),
    'attributes' => array(
        array(
            'label' => Yii::t('FairModule.fair', 'Organization'),
            'type' => 'raw',
            'value' =>
                CHtml::link(
                    $model->loadName(),
                    $this->createUrl('/exhibitionComplex/html/view', ['urlVenueName' => $model->id])
                ) . '<br>' .
                $model->loadStreet()
        ),
        array(
            'label' => Yii::t('FairModule.fair', 'Site exhibitionComplex'),
            'type' => 'raw',
            'value' => '<div class="organizer-link">' .
                CHtml::link(
                    H::getSiteAddress(Yii::t('FairModule.fair', 'Go to the website'), $hostOnly = true),
                    'http://' . H::getSiteAddress($model->site),
                    array('target' => '_blank', 'rel' => 'nofollow', 'title' => 'http://' . H::getSiteAddress($model->site))) .
                '</div>',
        ),
        array(
            'name' => 'phone',
            'visible' => !empty($model->phone)
        ),
        array(
            'name' => 'email',
            'type' => 'raw',
            'value' => Fair::getMailLinks($model->email),
            'visible' => !empty($model->email)
        ),
        array(
            'name' => 'standsConstructionContact',
            'label' => Yii::t('FairModule.fair', 'Stands construction contact'),
            'value' => ExhibitionComplex::getMailLinks($model->standsConstructionContact),
            'type' => 'raw',
            'visible' => null != $model->standsConstructionContact,
        ),
        array(
            'name' => 'cateringContact',
            'label' => Yii::t('FairModule.fair', 'Catering contact'),
            'value' => ExhibitionComplex::getMailLinks($model->cateringContact),
            'type' => 'raw',
            'visible' => null != $model->cateringContact,
        ),
        array(
            'name' => 'accreditationContact',
            'label' => Yii::t('FairModule.fair', 'Accreditation contact'),
            'value' => ExhibitionComplex::getMailLinks($model->accreditationContact),
            'type' => 'raw',
            'visible' => null != $model->accreditationContact,
        ),
        array(
            'name' => 'advertisingContact ',
            'label' => Yii::t('FairModule.fair', 'Advertising contact'),
            'value' => ExhibitionComplex::getMailLinks($model->advertisingContact),
            'type' => 'raw',
            'visible' => null != $model->advertisingContact,
        ),
    ),
));