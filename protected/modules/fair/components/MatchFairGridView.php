<?php
/**
 * File FairGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class FairGridView
 *
 * @property Fair $model
 */
class MatchFairGridView extends TbGridView
{
    /**
     * Модель Fair::model() or new Fair('scenario')
     * @var Fair
     */
    public $model;

    /**
     * Двумерный массив аргументов для создания условий CDbCriteria::compare
     * @see CDbCriteria::compare
     * @var array
     */
    public $compare = array(
        array()
    );
    /**
     * Для добавления колонок в начало таблицы
     * @var array
     */
    public $columnsPrepend = array();

    /**
     * Для добавления колонок в конец таблицы
     * @var array
     */
    public $columnsAppend = array();

    /** @var bool $enableFilter */
    public $enableFilter = true;

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {
        // Включаем фильтрацию по сценарию search
        $this->model->setScenario('search');
        $this->filter = $this->enableFilter ? ARGridView::createFilter($this->model, true) : null;

        // Создаем колонки таблицы
        $this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

        // Создаем провайдер данных для таблицы
        $this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

        // Инициализация
        parent::init();
    }

    /**
     * Колонки таблицы
     * @return array
     */
    public function getColumns()
    {
        /** @var CController $ctrl */
        $ctrl = $this->getOwner();
        $columns = array(
            'name' => array(
                'name' => 'name',
                'header' => Yii::t('FairModule.fair', 'Exhibition name'),
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, array("/fair/default/view", "id" => $data->id))',
            ),
            'rating' => array(
                'name' => 'rating',
                'header' => '',
                'type' => 'html',
                'value' => function ($data) {
                    /**
                     * @var Fair $data
                     * @var CWebApplication $app
                     */
                    $app = Yii::app();
                    $am = $app->getAssetManager();
                    $basePath = $app->theme->basePath . '/assets';
                    if ($data->rating !== null) {
                        $country = '';
                        if(!empty($data->exhibitionComplex)){
                            $country = isset($data->exhibitionComplex, $data->exhibitionComplex->region,$data->exhibitionComplex->region->district,$data->exhibitionComplex->region->district->country)
                                ? $data->exhibitionComplex->region->district->country->shortUrl : '';
                        }
                        $fullPath = TbHtml::image($am->publish($basePath . "/img/fair/ratingImage/rus/expo-rating-{$data->rating}.png"), '', ['title' => Fair::getTitleRatingIcon($data)]);
                        return $fullPath;
                    } else {
                        return TbHtml::image(ObjectFile::noImage(), '', array('style' => 'width:20px;height:20px'));
                    }
                },
                'htmlOptions' => array(
                    'style' => 'width: 40px;',
                    'class' => 'rating-block'
                ),
            ),
            'beginDate' => array(
                'name' => 'beginDate',
                'value' => 'date("d.m.Y",strtotime($data->beginDate))'
            ),
            'endDate' => array(
                'name' => 'endDate',
                'value' => 'date("d.m.Y",strtotime($data->endDate))'
            ),
            'squareNet' => array(
                'name' => 'squareNet',
                'value' => function($data){
                    if (!empty($data->squareNet)){
                        return $data->squareNet;
                    }
                    elseif (empty($data->squareNet) && !empty($data->squareGross)) {
                        return $data->squareGross . " (" . Yii::t('FairModule.fair','Square gross') . ")";
                    } else return NULL;
                }
            ),
            'members' => array(
                'name' => 'members',
            ),
            'visitors' => array(
                'name' => 'visitors',

            ),

        );
        return $columns;
    }
}