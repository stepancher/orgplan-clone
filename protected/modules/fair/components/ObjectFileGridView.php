<?php
/**
 * File ObjectFileGridView.php
 */
Yii::import('ext.helpers.ARGridView');
Yii::import('bootstrap.widgets.TbGridView');

/**
 * Class ObjectFileGridView
 *
 * @property ObjectFile $model
 */
class ObjectFileGridView extends TbGridView
{
	/**
	 * Модель ObjectFile::model() or new ObjectFile('scenario')
	 * @var ObjectFile
	 */
	public $model;

	/**
	 * Двумерный массив аргументов для создания условий CDbCriteria::compare
	 * @see CDbCriteria::compare
	 * @var array
	 */
	public $compare = array(
		array()
	);
	/**
	 * Для добавления колонок в начало таблицы
	 * @var array
	 */
	public $columnsPrepend = array();

	/**
	 * Для добавления колонок в конец таблицы
	 * @var array
	 */
	public $columnsAppend = array();

	/**
	 * Initializes the grid view.
	 * This method will initialize required property values and instantiate {@link columns} objects.
	 */
	public function init()
	{
		// Включаем фильтрацию по сценарию search
		$this->model->setScenario('search');

		// Создаем колонки таблицы
		$this->columns = ARGridView::createColumns($this->getColumns(), $this->columnsPrepend, $this->columnsAppend, $this->compare);

		// Создаем провайдер данных для таблицы
		$this->dataProvider = ARGridView::createDataProvider($this->dataProvider, $this->model, 'search', $this->compare);

		// Инициализация
		parent::init();
	}

	/**
	 * Колонки таблицы
	 * @return array
	 */
	public function getColumns()
	{
		$columns = array(
			'name' => !Yii::app()->user->isGuest
					? array(
						'name' => 'name',
						'type' => 'raw',
						'value' => function ($data) {
								/** @var ObjectFile $data */
								$id = $data->getOwnerId();
								return CHtml::link($data->label, array("download", "id" => $data->id, "objectId" => $id));
							},
					)
					: array(
						'name' => 'label',
					),
		);
		return $columns;
	}
}