<?php

Class TrUrl extends CComponent{

    public function init(){

    }

    public function getTrUrl($route, $params){
        if(empty($params['urlFairName'])){
            return NULL;
        }

        $trFair = TrFair::model()->findByAttributes(array('shortUrl' => $params['urlFairName']));

        if(empty($trFair)){
            return NULL;
        }

        $tr = TrFair::model()->findByAttributes(array('trParentId' => $trFair->trParentId, 'langId' => $params['langId']));

        if(empty($tr)){
            return NULL;
        }

        $params['urlFairName'] = $tr->shortUrl;

        return Yii::app()->createUrl($route, $params);
    }
}