<?php
/**
 * File FairDetailView.php
 * @var Fair $fair
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');

/**
 * Class FairDetailView
 *
 * @property Fair $data
 */
class ContactDetailView extends TbDetailView
{
    /**
     * Инициализация
     */
    public $organizer;

	public function init()
	{
        $exhibitionComplex = $this->data->exhibitionComplex;
		$organizer = $this->organizer;
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());
		if ($organizer) {
			/** @var Organizer $organizer */

			$pattern = "#([-0-9a-z_\.]+@[-0-9a-z_\.]+\.[a-z]{2,6})#i";
			$this->attributes['fairOrganizer']['label'] = Yii::t('FairModule.fair', 'organization');
			$this->attributes['fairOrganizer']['type'] = 'raw';
            $this->attributes['fairOrganizer']['visible'] = $organizer->name && $organizer->linkToTheSiteOrganizers != null;
            $this->attributes['fairOrganizer']['value'] = $organizer->name;

            $this->attributes['exhibitionComplexCity']['label'] =
            '<span itemscope itemtype="http://schema.org/Organization">
                <span itemprop="address">' . Yii::t('FairModule.fair', 'Address') . '</span>
                <span itemprop="name">&nbsp;</span>
                <span itemprop="telephone">&nbsp;</span>
                </span>';

            $this->attributes['site']['label'] = Yii::t('FairModule.fair', 'Site organizer');
            $this->attributes['site']['type'] = 'raw';
            $this->attributes['site']['value'] = TbHtml::link(
                Yii::t('FairModule.fair', 'Go to the website'),
                'http://' . H::getSiteAddress($organizer->linkToTheSiteOrganizers),
                array(
                    'target' => '_blank',
                    'rel' => 'nofollow',
                    'title' => (stristr($organizer->linkToTheSiteOrganizers, 'https://') ? 'https://' : 'http://') . H::getSiteAddress($organizer->linkToTheSiteOrganizers)));

            $this->attributes['exhibitionManagement']['label'] = Yii::t('FairModule.fair', 'Direction');
            $this->attributes['exhibitionManagement']['type'] = 'raw';
            $this->attributes['exhibitionManagement']['visible'] = $organizer->phone && $organizer->email != null;
            $this->attributes['exhibitionManagement']['value'] = $organizer->phone . '<div class="direction-email">' . Fair::getMailLinks($organizer->email) . '</div>';

            $this->attributes['servicesBuilding']['label'] = Organizer::model()->getAttributeLabel('servicesBuilding');
            $this->attributes['servicesBuilding']['type'] = 'raw';
            $this->attributes['servicesBuilding']['visible'] = $organizer->servicesBuilding != null;

            $this->attributes['servicesBuilding']['value'] = Fair::getMailLinks($organizer->servicesBuilding);

            $this->attributes['advertisingServicesTheFair']['label'] = Yii::t('FairModule.fair', 'Services in advertising on exhibition');
            $this->attributes['advertisingServicesTheFair']['type'] = 'raw';
            $this->attributes['advertisingServicesTheFair']['visible'] = $organizer->advertisingServicesTheFair != null;
            $this->attributes['advertisingServicesTheFair']['value'] = Fair::getMailLinks($organizer->advertisingServicesTheFair);

            $this->attributes['servicesLoadingAndUnloading']['label'] = Yii::t('FairModule.fair', 'Services for loading and unloading');
            $this->attributes['servicesLoadingAndUnloading']['type'] = 'raw';
            $this->attributes['servicesLoadingAndUnloading']['visible'] = $organizer->servicesLoadingAndUnloading != null;
            $this->attributes['servicesLoadingAndUnloading']['value'] = Fair::getMailLinks($organizer->servicesLoadingAndUnloading);

            $this->attributes['foodServices']['type'] = 'raw';
            $this->attributes['foodServices']['label'] = Organizer::model()->getAttributeLabel('foodServices');
            $this->attributes['foodServices']['visible'] = $organizer->foodServices != null;
            $this->attributes['foodServices']['value'] = Fair::getMailLinks($organizer->foodServices);
        }

        $this->attributes['statistics']['visible'] = false;
        $this->attributes['shard']['visible'] = false;
        $this->attributes['frequency']['visible'] = false;
        $this->attributes['lang']['visible'] = false;
        $this->attributes['mailLogoId']['visible'] = false;
        $this->attributes['datetimeModified']['visible'] = false;
        $this->attributes['statusId']['visible'] = false;
        $this->attributes['fairStatus']['visible'] = false;
        $this->attributes['fairStatusColor']['visible'] = false;
        $this->attributes['duration']['visible'] = false;
        $this->attributes['fairTypeId']['visible'] = false;
        $this->attributes['currencyId']['visible'] = false;
        $this->attributes['standId']['visible'] = false;
        $this->attributes['userId']['visible'] = false;
        $this->attributes['active']['visible'] = false;
        $this->attributes['fairIsForum']['visible'] = false;
        $this->attributes['id']['visible'] = false;
        $this->attributes['activateControl']['visible'] = false;
        $this->attributes['leasedArea']['visible'] = false;
        $this->attributes['associationName']['visible'] = false;
        $this->attributes['fairHasAssociationId']['visible'] = false;
        $this->attributes['match']['visible'] = false;
        $this->attributes['rating']['visible'] = false;
        $this->attributes['fairAssociation']['visible'] = false;
        $this->attributes['organizer_name']['visible'] = false;
        $this->attributes['organizer_linkToTheSiteOrganizers']['visible'] = false;
        $this->attributes['organizer_phone']['visible'] = false;
        $this->attributes['organizer_email']['visible'] = false;
        $this->attributes['logoId']['visible'] = false;
        $this->attributes['name']['visible'] = false;
        $this->attributes['fairTranslateName']['visible'] = false;
        $this->attributes['participationPrice']['visible'] = false;
        $this->attributes['registrationFee']['visible'] = false;
        $this->attributes['organizerId']['visible'] = false;
        $this->attributes['squareNet']['visible'] = false;
        $this->attributes['squareGross']['visible'] = false;
        $this->attributes['members']['visible'] = false;
        $this->attributes['visitors']['visible'] = false;
        $this->attributes['beginDate']['visible'] = false;
        $this->attributes['endDate']['visible'] = false;
        $this->attributes['beginMountingDate']['visible'] = false;
        $this->attributes['endMountingDate']['visible'] = false;
        $this->attributes['beginDemountingDate']['visible'] = false;
        $this->attributes['endDemountingDate']['visible'] = false;
        $this->attributes['price']['visible'] = false;
        $this->attributes['originalId']['visible'] = false;
        $this->attributes['industryId']['visible'] = false;
        $this->attributes['exhibitionComplexId']['visible'] = false;
        $this->attributes['description']['visible'] = false;
        $this->attributes['keyword']['visible'] = false;
        $this->attributes['uniqueText']['visible'] = false;
        $this->attributes['exhibitionComplexCity']['visible'] = false;

        parent::init();
    }
}