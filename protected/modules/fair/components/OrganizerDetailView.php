<?php
/**
 * File OrganizerDetailView.php
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');
/**
 * Class OrganizerDetailView
 *
 * @property Organizer $data
 */
class OrganizerDetailView extends TbDetailView
{
	/**
	 * Инициализация
	 */
	public function init()
	{
		$this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());

		//$this->attributes['id']['visible'] = false;
		$this->attributes['linkToTheSiteOrganizers']['value'] = CHtml::link(
			$this->data->linkToTheSiteOrganizers,
			$this->data->linkToTheSiteOrganizers,
			array('target' => '_blank')
		);
		$this->attributes['linkToTheSiteOrganizers']['type'] = 'raw';

		$this->attributes['phone']['value'] = $this->data->phoneWithEmail;

		parent::init();
	}
}