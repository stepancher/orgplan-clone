<?php
/**
 * File FairDetailView.php
 * @var Fair $fair
 */
Yii::import('ext.helpers.ARDetailView');
Yii::import('bootstrap.widgets.TbDetailView');

/**
 * Class FairDetailView
 *
 * @property Fair $data
 */
class FairDetailStatisticView extends TbDetailView
{
    /**
     * Инициализация
     */
    public function init()
    {
        $course = $this->data->currency && $this->data->currency->course ? $this->data->currency->course : 1;
        $currency = Yii::t('FairModule.fair', 'RUB');
        $price = $this->data->price;
        $registrationFee = $this->data->registrationFee;
        $participationPrice = $this->data->participationPrice;

        $this->attributes = ARDetailView::createAttributes($this->data, $this->data->description());

            $this->attributes['FairVisitors']['label'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' . $this->data->getAttributeLabel('visitors') . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';
            $this->attributes['FairVisitors']['visible'] = $this->data->visitors != null;
            $this->attributes['FairVisitors']['type'] = 'raw';
            $this->attributes['FairVisitors']['value'] = $this->data->visitors ?
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                number_format($this->data->visitors, 0, '.', ' ') . ' ' . Yii::t('FairModule.fair', 'People')
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>':
                Yii::t('FairModule.fair', 'Data not available');

            $this->attributes['FairMembers']['label'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                $this->data->getAttributeLabel('members')
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';
            $this->attributes['FairMembers']['visible'] = $this->data->members != null;
            $this->attributes['FairMembers']['type'] = 'raw';
            $this->attributes['FairMembers']['value'] = $this->data->members ?
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                number_format($this->data->members, 0, '.', ' ') . ' ' .Yii::t('FairModule.fair', 'Company')
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>': Yii::t('FairModule.fair', 'Data not available');

            if (!empty($this->data->squareNet)) {
                $this->attributes['FairSquare']['type'] = 'raw';
                $this->attributes['FairSquare']['visible'] = $this->data->squareNet != null;
                $this->attributes['FairSquare']['label'] =
                        '<span itemscope itemtype="http://schema.org/Organization">
                <span itemprop="name"> ' .
                        Yii::t('FairModule.fair', 'Square net')
                        . '</span>
                <span itemprop="address">&nbsp;</span>
                <span itemprop="telephone">&nbsp;</span>
            </span>';
                $this->attributes['FairSquare']['value'] = $this->data->squareNet ?
                        '<span title="'.Yii::t('FairModule.fair','Square net').'">
                <span itemscope itemtype="http://schema.org/Organization">
                <span itemprop="name">' .
                        number_format($this->data->squareNet, 0, '.', ' ') . ' ' . Yii::t('FairModule.fair', 'sqm')
                        . '</span>
                <span itemprop="address">&nbsp;</span>
                <span itemprop="telephone">&nbsp;</span>
            </span>' :
                        Yii::t('FairModule.fair', 'Data not available');

            } elseif (empty($this->data->squareNet) && !empty($this->data->squareGross)) {
                $this->attributes['FairSquare']['type'] = 'raw';
                $this->attributes['FairSquare']['visible'] = $this->data->squareGross != null;
                $this->attributes['FairSquare']['label'] =
                        '<span itemscope itemtype="http://schema.org/Organization">
                <span itemprop="name"> ' .
                        Yii::t('FairModule.fair', 'Square gross')
                        . '</span>
                <span itemprop="address">&nbsp;</span>
                <span itemprop="telephone">&nbsp;</span>
            </span>';
                $this->attributes['FairSquare']['value'] = $this->data->squareGross ?
                    '<span title="'.Yii::t('FairModule.fair','Square gross').'">
                <span itemscope itemtype="http://schema.org/Organization">
                <span itemprop="name">' .
                    number_format($this->data->squareGross, 0, '.', ' ') . ' ' . Yii::t('FairModule.fair', 'sqm')
                    . '</span>
                <span itemprop="address">&nbsp;</span>
                <span itemprop="telephone">&nbsp;</span>
            </span>' :
                    Yii::t('FairModule.fair', 'Data not available');
            }

            $this->attributes['FairParticipationPrice']['label'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                Yii::t('FairModule.fair', 'Participation fee')
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';
            $this->attributes['FairParticipationPrice']['type'] = 'raw';
            $this->attributes['FairParticipationPrice']['visible'] = $participationPrice != null;
            $this->attributes['FairParticipationPrice']['value'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                Fair::getFairParticipationPrice($participationPrice, $course, $currency)
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';

            $this->attributes['FairRegistrationFee']['label'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                $this->data->getAttributeLabel('registrationFee')
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';
            $this->attributes['FairRegistrationFee']['type'] = 'raw';
            $this->attributes['FairRegistrationFee']['visible'] = $registrationFee != null;
            $this->attributes['FairRegistrationFee']['value'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                Fair::getFairRegistrationFee($registrationFee, $course, $currency)
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';

            $this->attributes['FairPrice']['label'] =
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                $this->data->getAttributeLabel('price')
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>';
            $this->attributes['FairPrice']['type'] = 'html';
            $this->attributes['FairPrice']['visible'] = $price != null && !Yii::app()->user->isGuest;
            $this->attributes['FairPrice']['value'] = $price ?
                '<span itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name"> ' .
                Yii::t('FairModule.fair', 'from ') . number_format($price * $course, 0, '.', ' ') . ' ' . $currency
                . '</span>
            <span itemprop="address">&nbsp;</span>
            <span itemprop="telephone">&nbsp;</span>
        </span>' :
                Yii::t('FairModule.fair', 'Data not available');

            $this->attributes['rating']['type'] = 'raw';
            $this->attributes['rating']['value'] = '<div title="' . Fair::getTitleRatingIcon($this->data) . '"
			 class="b-switch-list-view__cell rating-logo rating-logo-type-' . $this->data->rating . '-' . $this->data->getCountryStyle() . ' rating-logo-fair-view">
			<div></div>
		</div>';

        $this->attributes['site']['visible'] = false;
        $this->attributes['shard']['visible'] = false;
        $this->attributes['currencyId']['visible'] = false;
        $this->attributes['mailLogoId']['visible'] = false;
        $this->attributes['datetimeModified']['visible'] = false;
        $this->attributes['frequency']['visible'] = false;
        $this->attributes['fairTypeId']['visible'] = false;
        $this->attributes['lang']['visible'] = false;
        $this->attributes['standId']['visible'] = false;
        $this->attributes['userId']['visible'] = false;
        $this->attributes['statusId']['visible'] = false;
        $this->attributes['fairStatus']['visible'] = false;
        $this->attributes['fairStatusColor']['visible'] = false;
        $this->attributes['duration']['visible'] = false;
        $this->attributes['active']['visible'] = false;
        $this->attributes['leasedArea']['visible'] = false;
        $this->attributes['fairIsForum']['visible'] = false;
        $this->attributes['id']['visible'] = false;
        $this->attributes['activateControl']['visible'] = false;
        $this->attributes['match']['visible'] = false;
        $this->attributes['rating']['visible'] = true;
        $this->attributes['fairHasAssociationId']['visible'] = false;
        $this->attributes['fairAssociation']['visible'] = false;
        $this->attributes['organizer_name']['visible'] = false;
        $this->attributes['organizer_linkToTheSiteOrganizers']['visible'] = false;
        $this->attributes['organizer_phone']['visible'] = false;
        $this->attributes['organizer_email']['visible'] = false;
        $this->attributes['associationName']['visible'] = false;
        $this->attributes['logoId']['visible'] = false;
        $this->attributes['name']['visible'] = false;
        $this->attributes['fairTranslateName']['visible'] = false;
        $this->attributes['participationPrice']['visible'] = false;
        $this->attributes['registrationFee']['visible'] = false;
        $this->attributes['organizerId']['visible'] = false;
        $this->attributes['squareNet']['visible'] = false;
        $this->attributes['squareGross']['visible'] = false;
        $this->attributes['members']['visible'] = false;
        $this->attributes['visitors']['visible'] = false;
        $this->attributes['beginDate']['visible'] = false;
        $this->attributes['endDate']['visible'] = false;
        $this->attributes['beginMountingDate']['visible'] = false;
        $this->attributes['endMountingDate']['visible'] = false;
        $this->attributes['beginDemountingDate']['visible'] = false;
        $this->attributes['endDemountingDate']['visible'] = false;
        $this->attributes['price']['visible'] = false;
        $this->attributes['originalId']['visible'] = false;
        $this->attributes['industryId']['visible'] = false;
        $this->attributes['exhibitionComplexId']['visible'] = false;
        $this->attributes['description']['visible'] = false;
        $this->attributes['keyword']['visible'] = false;
        $this->attributes['uniqueText']['visible'] = false;
        $this->attributes['statistics']['visible'] = false;
        parent::init();
    }
}