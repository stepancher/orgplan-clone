<?php
/**
 * File FormView.php
 */
Yii::import('ext.widgets.RowView');

/**
 * Class FormView
 */
class FormView extends RowView
{
	/**
	 * Форма
	 * @var TbForm
	 */
	public $form;

	public $widgetCss = 'widget-form-view';

	/**
	 * Флаг частичной отрисовки формы (без шапки и подвала)
	 * @var bool
	 */
	public $partialMode = false;

	/**
	 * Initializes the widget.
	 * This method is called by {@link CBaseController::createWidget}
	 * and {@link CBaseController::beginWidget} after the widget's
	 * properties have been initialized.
	 */
	public function init()
	{
		parent::init();
		if ($this->form) {
			TbHtml::addCssClass('widget-'.get_class($this->form), $this->htmlOptions);
		}
	}

	/**
	 * Запуск отрисовки
	 */
	public function run()
	{
		if (!$this->partialMode) {
			echo $this->form->renderBegin();
		} else {
			$this->form->renderBegin();
		}

		echo CHtml::tag('div', $this->htmlOptions, $this->form->renderElements());

		$this->renderItems();

		if (!$this->partialMode) {
			echo $this->form->renderEnd();
		} else {
			$this->form->renderEnd();

		}
	}
}