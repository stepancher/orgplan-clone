<?php

class HtmlController extends Controller {

    public function actionView($langId, $urlFairName){

        echo "langId: '{$langId}'<br/>urlFairName: '{$urlFairName}'";

        echo "<br/>";

        /** @var Fair $fair */
        $fair = Fair::model()->with([
            'translateRaw' => [
                'alias' => 'trFair',
                'on' => 'trFair.langId = "' . $langId . '"'
            ]
        ])->find('trFair.shortUrl = :urlFairName', [':urlFairName' => $urlFairName]);

        echo "fairId: '{$fair->id}'";
    }

}