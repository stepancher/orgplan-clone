<?php

class JsonController extends Controller
{
    public $venueShortTypeForForeignLangEnabled;

    protected $dump = FALSE;
    protected $fairId = null;
    protected $fair = null;
    public static $defaultCurrencies = array(
        'RUB',
        'EUR',
        'USD',
    );

    /** @var bool Set TRUE if exhibition complex description in foreign languages must contain exhibition complex short type. */
    protected $_venueShortTypeForForeignLangEnabled = FALSE;

    const GVALUE_ROUND_PRECISION = 2;
    const ENGLISH_LANGUAGE = 'en';

    public static $description_patterns = array(
        'DESCRIPTION_RU_PATTERN_PAST' => '{fairDate} в "{exhibitionComplexShortTypeName} {venueName}" прошла {fairTypeName} "{fairName}". Участники: {members} {membersUnit}. Посетители: {visitors} {visitorsUnit}',
        'DESCRIPTION_EN_PATTERN_PAST' => 'The exhibition "{fairName}" took place on {fairDate} in "{venueName}". Exhibitors: {members}. Visitors: {visitors}.',
        'DESCRIPTION_DE_PATTERN_PAST' => '{fairDate} in "{venueName}" hat die Messe "{fairName}" stattgefunden. Aussteller: {members}. Besucher: {visitors}',
        'DESCRIPTION_RU_PATTERN_FUTURE' => '{fairDate} в "{exhibitionComplexShortTypeName} {venueName}" пройдет "{fairName}". Участники: {members} {membersUnit}. Посетители: {visitors} {visitorsUnit}',
        'DESCRIPTION_EN_PATTERN_FUTURE' => '"{fairName}" will take place on {fairDate} in "{exhibitionComplexShortTypeName} {venueName}". Exhibitors: {members}. Visitors: {visitors}',
        'DESCRIPTION_DE_PATTERN_FUTURE' => '{fairDate} in "{venueName}" findet "{fairName}" statt. Aussteller: {members}. Visitors: {visitors}',
    );

    public function getEnabledVenueShortTypeForForeignLang(){

        if($this->_venueShortTypeForForeignLangEnabled == TRUE){
            return TRUE;
        }

        if(Yii::app()->language !== Yii::app()->params['defaultLanguage']){
            return FALSE;
        }

        return TRUE;
    }

    public function actionDump($langId, $urlFairName){
        $this->dump = TRUE;
        $this->actionGetData($langId, $urlFairName);
    }

    public function actionGetData($langId, $urlFairName)
    {

        $trfair = TrFair::model()->findByAttributes([
            'langId' => $langId,
            'shortUrl' => $urlFairName,
        ]);

        if($trfair === NULL){
            throw new CHttpException(404, 'Fair not found');
        }

        $fairId = $trfair->trParentId;
        $this->fairId = $fairId;

        $fair = Fair::model()->findByPk($fairId);

        if (empty($fair) /*|| $fair->active != 1*/) {
            echo json_encode([
                'status' => 'failure',
                'msg' => 'fair not found',
            ]);
            exit;
        }

        $costs = array();
        if (
            !empty($fair->exhibitionComplex) &&
            !empty($fair->exhibitionComplex->city)
        ) {

            $productIds = array(
                27, 16, 91
            );

            $costs = $this->getCityProduction($fair->exhibitionComplex->city->id, $productIds);
        }

        $json = array(
            "title" => $this->getTitle($fairId),
            "description" => $this->getDescription($fairId),
            "user" => array(
                "auth" => !Yii::app()->user->isGuest,
            ),
            "isMyFair" => MyFair::isMyFair($fair->id),
            "fair" => $this->getFairInfoArray($fair->id),
            "prices" => $costs,
            "stats" => $this->getFairStats($fair->id),
            'calc' => $this->getCalc($fair->id),
            'statistics' => $this->getStatistics($fair->id),
            "currencies" => array(
                'RUB' => '₽',
                'EUR' => '€',
                'USD' => '$',
            ),
            'industries' => $this->getIndustries($fair->id),
            'calendar' => $this->getCalendar($fair->id),
            "ratings" => $this->getRatings(),
            "checklist" => $this->getChecklistData(),
        );

        if($this->dump){
            CVarDumper::dump($json,10,1);exit;
        }

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    /**
     * @deprecated
     * @param $fairId
     * @return mixed
     */
    public function getMapCountryData($fairId){

        $sql = "
            SELECT 
                cntry.code code
            
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec     ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c                   ON c.id = ec.cityId
            LEFT JOIN tbl_region r                 ON r.id = c.regionId
            LEFT JOIN tbl_district d               ON r.districtId = d.id
            LEFT JOIN tbl_country cntry            ON cntry.id = d.countryId
            
            WHERE f.id = :fairId
            
            GROUP BY f.id
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
        ]);

        $res = $dataReader->read();

        return $res['code'];
    }

    /**
     * @deprecated
     * @param $fairId
     * @return array
     */
    public function getRegionCode($fairId){

        $sql = "
            SELECT 
                r.code code,
                trr.name regionName
            
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec     ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c                   ON c.id = ec.cityId
            LEFT JOIN tbl_region r                 ON r.id = c.regionId
            LEFT JOIN tbl_trregion trr ON trr.trParentId = r.id AND trr.langId = :langId
            
            WHERE f.id = :fairId
            
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
            ':langId' => Yii::app()->language,
        ]);

        $res = $dataReader->read();

        return array(
            array(
                "key" => $res['code'],
                "value" => 1,
                'fullName' => $res['regionName'],
            )
        );
    }

    public function getRatings(){

        $ratings = array(
            array(
                "key" => 1,
                "value" => Yii::t('FairModule.fair', 'Statistics confirmed exhibition rating.'),
            ),
            array(
                "key" => 2,
                "value" => Yii::t('FairModule.fair', 'Statistics confirmed exhibition audit.'),
            ),
            array(
                "key" => 3,
                "value" => Yii::t('FairModule.fair', 'Statistics confirmed only organizer of the exhibition.'),
            ),
        );

        return $ratings;
    }

    public function getCalendar($fairId){

        $sql = '
            SELECT 
                trcftc.name name,
                f.beginDate - INTERVAL cftc.before DAY date
                
            FROM tbl_calendarfairtaskscommon cftc
            LEFT JOIN tbl_trcalendarfairtaskscommon trcftc ON (cftc.uuid = trcftc.trParentId AND trcftc.langId = :langId) 
            LEFT JOIN tbl_fair f ON f.id = :fairId
            WHERE `before` <= 29 ORDER BY `before` DESC limit 5;
        ';

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':fairId'=>$fairId, ':langId' => Yii::app()->language]);


        //@TODO Warning -1 day in dates for calendar
        $sql = "
            SELECT 
                f.beginDate - INTERVAL 1 DAY beginDate,
                datediff(f.endDate, f.beginDate) datediff
                
            FROM tbl_fair f 
            WHERE f.id = :fairId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
        ]);

        $fair = $dataReader->read();

        $dates = array();
        $date = date('Y-m-d', strtotime($fair['beginDate']));
        $dates[] = $date.'T21:00:00.000Z';
        for($i = 0;$i < $fair['datediff']; $i++){
            $date = date('Y-m-d', strtotime($date.' + 1 days'));
            $dates[] =  $date.'T21:00:00.000Z';
        }

        $calendar = array(
            "header" => Yii::t('FairModule.fair', 'Important dates'),
            "dates" => $dates,
            "tasks" => $res,
            "calendarLabel" => Yii::t('FairModule.fair', 'go to calendar'),
            "gotoChecklistLabel" => Yii::t('FairModule.fair', 'your expoplanner'),
            "desc" => Yii::t('FairModule.fair', 'calendar desc')
        );

        return $calendar;
    }

    public function getIndustries($fairId){

        $sql = '
            SELECT
                tri.name name,
                count(DISTINCT ff.id) fairsCount,
                COUNT(DISTINCT statId, tableId) cnt
                
            FROM tbl_fair f
            
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.fairid = f.id
            LEFT JOIN tbl_fairhasindustry fhif ON fhif.industryId = fhi.industryId
            LEFT JOIN tbl_fair ff ON fhif.fairId = ff.id 
            LEFT JOIN tbl_trindustry tri ON fhi.industryId = tri.trParentId AND langId = :langId
            LEFT JOIN tbl_charttable ct ON ct.industryId = fhif.industryId
            LEFT JOIN tbl_chartcolumn cc ON cc.tableId = ct.id
            LEFT JOIN tbl_trchartcolumn trcc ON trcc.trParentId = cc.id AND trcc.langId = :langId AND trcc.header IS NOT NULL
            
            WHERE f.id = :fairId AND ff.active = 1 AND YEAR(f.beginDate) >= YEAR(NOW())
            GROUP BY fhif.industryId
        ';

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':fairId'=>$fairId, ':langId' => Yii::app()->language]);

        $industries = array();
        foreach($res as $row){
            $industries[] = array(
                "fairsLabel" => Yii::t('FairModule.fair', 'Fairs in industry')." ".$row['name'],
                "statsLabel" => Yii::t('FairModule.fair', 'Counters in industry')." ".$row['name'],
                "fairsCount" => $row['fairsCount'],
                "statsCount" => $row['cnt'],
            );
        }

        return $industries;
    }

    /**@TODO REFACTOR GETTING CURRENCIES !!!!**/
    /**@TODO WARNING: bad method logic!!!!**/
    public function getStatistics($fairId){

        $sql = "
            SELECT 
                f.participationPrice,
                f.registrationFee,
                curr.name currencyName,
                curr.id currId,
                curr.icon,
                curr.code AS currencySourceCode,
                ct.name AS currencyTargetName,
                cc.value AS course,
                ct.icon AS targetCourseIcon,
                ct.code AS currencyTargetCode
                
            FROM tbl_fair f
            
            LEFT JOIN tbl_currency curr            ON curr.id = f.currencyId
            LEFT JOIN tbl_currencycourse cc        ON curr.id = cc.source
            LEFT JOIN tbl_currency ct              ON ct.id = cc.target
            
            WHERE f.id = :fairId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
        ]);

        $currencies = $dataReader->readAll();

        if(!empty($currencies)){
            $firstRes = $currencies[0];
        } else {
            return array(
                'registrationFee' => array(),
                'participationPrice' => array(),
                'default' => array(),
            );
        }

        $result = array();
        //@TODO REFACTOR IF CONDITIONS !!!!!!!!
        if(
            isset($firstRes['currencySourceCode']) &&
//            isset($firstRes['registrationFee']) &&
            isset($firstRes['icon']) &&
//            isset($firstRes['participationPrice']) &&
            in_array($firstRes['currencySourceCode'], self::$defaultCurrencies)
        )
        {
            $result['registrationFee'][$firstRes['currencySourceCode']] = array(
                'header' => Yii::t('fair','Registration fee'),
                'value' => !empty($firstRes['registrationFee'])?number_format($firstRes['registrationFee'], 0, '.', ' '):Yii::t('FairModule.fair','Data is empty'),
                'unit' => !empty($firstRes['registrationFee'])?$firstRes['icon']:'',
                'emptyValue' => empty($firstRes['registrationFee']),
            );


            $result['participationPrice'][$firstRes['currencySourceCode']] = array(
                'header' => Yii::t('FairModule.fair', 'Costs of arrend'),
                'value' => !empty($firstRes['participationPrice'])?$firstRes['participationPrice']:Yii::t('FairModule.fair','Data is empty'),
                'unit' => !empty($firstRes['participationPrice'])?$firstRes['icon'] . '/' . Yii::t('FairModule.fair', 'sqm'):'',
                'prefix' => !empty($firstRes['participationPrice']) ? Yii::t('FairModule.fair', 'prefix') : '',
                'emptyValue' => empty($firstRes['participationPrice']),
            );

        }

        //@TODO REFACTOR !!!!!!!!
        foreach ($currencies as $currency){

            if(isset($currency['currencyTargetCode']) &&
//                isset($currency['registrationFee']) &&
                isset($currency['course']) &&
                isset($currency['targetCourseIcon']) &&
//                isset($currency['participationPrice']) &&
            in_array($currency['currencyTargetCode'], self::$defaultCurrencies)
            )
            {
                $result['registrationFee'][$currency['currencyTargetCode']] = array(
                    'header' => Yii::t('fair','Registration fee'),
                    'value' => !empty($currency['registrationFee'] * $currency['course'])?number_format($currency['registrationFee'] * $currency['course'], 0, '.', ' '):Yii::t('FairModule.fair','Data is empty'),
                    'unit' => !empty($currency['registrationFee'])?$currency['targetCourseIcon']:'',
                    'emptyValue' => empty($currency['registrationFee']),
                );

                $result['participationPrice'][$currency['currencyTargetCode']] = array(
                    'header' => Yii::t('FairModule.fair', 'Costs of arrend'),
                    'value' => !empty(round($currency['participationPrice'] * $currency['course']))?round($currency['participationPrice'] * $currency['course']):Yii::t('FairModule.fair','Data is empty'),
                    'unit' => !empty($currency['participationPrice'])?$currency['targetCourseIcon'] . '/' . Yii::t('FairModule.fair', 'sqm'):'',
                    'prefix' => !empty(round($currency['participationPrice'] * $currency['course'])) ? Yii::t('FairModule.fair', 'prefix') : '',
                    'emptyValue' => empty($currency['participationPrice']),
                );

            }
        }

        //@TODO REFACTOR !!!!!!!!
        $result['default'] = array(
            array(
                'header' => Yii::t('fair','Registration fee'),
                'value' => !empty($firstRes['registrationFee'])?number_format($firstRes['registrationFee'], 0, '.', ' '):Yii::t('FairModule.fair','Data is empty'),
                'unit' => !empty($firstRes['registrationFee'])?$firstRes['icon']:'',
                'emptyValue' => empty($firstRes['registrationFee']),
            ),
            array(
                'header' => Yii::t('FairModule.fair', 'Costs of arrend'),
                'value' => !empty($firstRes['participationPrice'])?number_format($firstRes['participationPrice'], 0, '.', ' '):Yii::t('FairModule.fair','Data is empty'),
                'unit' => !empty($firstRes['participationPrice'])?$firstRes['icon']. '/'.Yii::t('FairModule.fair', 'sqm'):'',
                'prefix' => !empty($firstRes['participationPrice']) ? Yii::t('FairModule.fair', 'prefix') : '',
                'emptyValue' => empty($firstRes['participationPrice']),
            ),
        );
        
        return $result;
    }

    public function getCalc($fairId){

        $sql = "
            SELECT 
                ae.percent,
                trae.name
            FROM tbl_additionalexpenses ae
            LEFT JOIN tbl_tradditionalexpenses trae ON ae.id = trae.trParentId AND trae.langId = :langId
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':langId' => Yii::app()->language]);

        $rows = array();
        $percent = array();
        foreach($res as $data){
            $rows[] = array($data['name']);
            $percent[] = $data['percent'];
        }

        $sql = "
            SELECT 
                f.participationPrice,
                f.registrationFee,
                curr.id sourceCurrencyId,
                curr.icon AS sourceCurrencyIcon,
                curr.code AS sourceCurrencyCode,
                curr.icon AS sourceCurrencyIcon,
                curr2.id AS targetCurrencyId,
                curr2.icon AS targetCurrencyIcon,
                curr2.code AS targetCurrencyCode,
                curr2.icon AS targetCurrencyIcon,
                cc.value AS course
            FROM tbl_fair f
            LEFT JOIN tbl_currency curr ON curr.id = f.currencyId
            LEFT JOIN tbl_currencycourse cc ON cc.source = curr.id
            LEFT JOIN tbl_currency curr2 ON cc.target = curr2.id
            WHERE f.id = :fairId
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
        ]);

        $currencies = $dataReader->readAll();
        if(isset($currencies[0]))
            $firstCurrency = $currencies[0];
        else
            return array(
                "header"=> Yii::t('FairModule.fair', 'Calculating the cost of participation in the exhibition'),
                "registrationFee" => array(),
                "participationPrice" => array(),
                "currencyCode" => '',
                "currencyIcons" => array(),
                "table"=> array(
                    "headers"=> [Yii::t('FairModule.fair', 'calculator__in-total')],
                    "rows"=> $rows,
                    "percents" => $percent,
                ),
                "squareLabel" => Yii::t('FairModule.fair', 'square'),
                "RUB" => '',
                "modal" => array(
                    "title" => "Тайтл",
                    "text" => "Текст модального окна",
                    "ok" => "Я все понял",
                )
            );

        $registrationFee[$firstCurrency['sourceCurrencyCode']] = round($firstCurrency['registrationFee'] * 1);
        $participationPrice[$firstCurrency['sourceCurrencyCode']] = round($firstCurrency['participationPrice'] * 1);
        $currencyIcons[$firstCurrency['sourceCurrencyCode']] = $firstCurrency['sourceCurrencyIcon'];

        foreach ($currencies as $currency){
            $registrationFee[$currency['targetCurrencyCode']] = round($currency['registrationFee'] * $currency['course']);
            $participationPrice[$currency['targetCurrencyCode']] = round($currency['participationPrice'] * $currency['course']);
            $currencyIcons[$currency['targetCurrencyCode']] = $currency['targetCurrencyIcon'];
        }

        $currencyIconQuery = "SELECT DISTINCT code, icon FROM tbl_currency";
        $currencyIconsData = Yii::app()->db->createCommand($currencyIconQuery)->queryAll();
        $currencyIcons = array();

        foreach ($currencyIconsData as $currencyIconData){
            $currencyIcons[$currencyIconData['code']] = $currencyIconData['icon'];
        }

        $data = array(
            "header"=> Yii::t('FairModule.fair', 'Calculating the cost of participation in the exhibition'),
            "registrationFee" => $registrationFee,
            "participationPrice" => $participationPrice,
            "currencyCode" => $firstCurrency['sourceCurrencyCode'],
            "currencyIcons" => $currencyIcons,
            "table"=> array(
                "headers"=> [Yii::t('FairModule.fair', 'calculator__in-total')],
                "rows"=> $rows,
                "percents" => $percent,
            ),
            "squareLabel" => Yii::t('FairModule.fair', 'square'),
            "RUB" => $firstCurrency['sourceCurrencyIcon'],
            "modal" => array(
                "title" => "Тайтл",
                "text" => "Текст модального окна",
                "ok" => "Я все понял",
            )
        );

        return $data;
    }

    public function getFairInfo($fairId)
    {
        $sql = "
            SELECT 
                trf.name                                        AS name,
                f.beginDate                                     AS beginDate,
                trf.uniqueText                                  AS uniqueText,
                trf.langId                                      AS lang,
                trf.statistics                                  AS statistics,
                YEAR(f.beginDate)                               AS fairYear,
                f.beginDate                                     AS startDate,
                f.endDate                                       AS endDate,
                f.id                                            AS id,
                f.site                                          AS site,
                f.canceled                                      AS canceled,
                trc.name                                        AS cityName,
                trr.name                                        AS regionName,
                group_concat(tri.name SEPARATOR :separator)     AS industriesNames,
                trec.name                                       AS exhibitionComplexName,
                group_concat(DISTINCT trorg.name SEPARATOR :separator)   AS organizerName,
                f.rating                                        AS rating,
                trf.description                                 AS description,
                fi.members                                      AS members,
                fi.visitors                                     AS visitors,
                fi.squareNet                                    AS squareNet,
                fi.squareGross                                  AS squareGross,
                f.participationPrice                            AS participationPrice,
                f.registrationFee                               AS registrationFee,
                f.datetimeModified                              AS datetimeModified,
                re.id                                           AS regionId,
                re.regionId                                     AS regionPk,
                ec.id                                           AS exhibitionComplexId,
                IFNULL(curr.course, 1)                          AS course,
                curr.name                                       AS currencyName,
                IF(fm.id IS NULL, 0, 1)                         AS inMatches,
                trf.shortUrl                                    AS shortUrl,
                IF(f.endDate < NOW(), 1, 0)                     AS disabled,
                ecst.name                                       AS exhibitionComplexShortTypeName,
                ft.name                                         AS fairTypeName,
                trec.shortUrl                                   AS exhibitionComplexShortUrl,
                fi.forumId                                      AS forumId,
                f.infoId                                        AS infoId,
                (SELECT COUNT(fff.id) FROM tbl_fair fff WHERE fff.infoId = f.infoId) AS countSameInfoId
            
            FROM tbl_fair f 
            LEFT JOIN tbl_trfair trf                        ON trf.trParentId = f.id AND trf.langId = :langId
            LEFT JOIN tbl_fairinfo fi                       ON fi.id = f.infoId
            LEFT JOIN tbl_fairtype ft                       ON ft.id = f.fairTypeId
            LEFT JOIN tbl_exhibitioncomplex ec              ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c                            ON c.id = ec.cityId
            LEFT JOIN tbl_trcity trc                        ON c.id = trc.trParentId AND trc.langId = :langId
            LEFT JOIN tbl_trregion trr                      ON trr.trParentId = c.regionId AND trr.langId = :langId
            LEFT JOIN tbl_regioninformation re              ON re.regionId = trr.trParentId
            LEFT JOIN tbl_fairhasindustry fhi               ON fhi.fairId = f.id
            LEFT JOIN tbl_trindustry tri                    ON tri.trParentId = fhi.industryId AND tri.langId = :langId
            LEFT JOIN tbl_trexhibitioncomplex trec          ON trec.trParentId = ec.id AND trec.langId = :langId
            LEFT JOIN tbl_exhibitioncomplexshorttype ecst   ON ecst.id = ec.exhibitionComplexShortTypeId
            LEFT JOIN tbl_fairhasorganizer fho              ON fho.fairId = f.id
            LEFT JOIN tbl_organizer o                       ON o.id = fho.organizerId
            LEFT JOIN tbl_trorganizercompany trorg          ON trorg.trParentId = o.companyId AND trorg.langId = :langId
            LEFT JOIN tbl_currency curr                     ON curr.id = f.currencyId
            LEFT JOIN tbl_fairmatches fm                    ON fm.fairId = f.id AND fm.userId = :userId
            
            WHERE f.id = :fairId
            
            GROUP BY f.id
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
            ':userId' => Yii::app()->user->id,
            ':langId' => Yii::app()->language,
            ':separator' => '; ',
        ]);

        $res = $dataReader->read();

        return $res;
    }

    public function getFairInfoArray($fairId){

        $fairInfo = $this->getFairInfo($fairId);

        $fairInfoArray = array(
            "header"=> Yii::t('leftMenu', 'About fair'),
            "checked" => Yii::t('FairModule.fair','Checked')." ".$fairInfo['datetimeModified'],
            "name" =>  array(
                "prop" => Yii::t('FairModule.fair','Name'),
                "value" => $fairInfo['name'],
            ),
            "fullName" => $fairInfo['name'] . " " . $fairInfo['fairYear'],
            "date" => array(
                "prop" => Yii::t('FairModule.fair','Data'),
                "value" => Fair::getDateRange($fairInfo['beginDate'], $fairInfo['endDate']),
            ),
            "site" => array(
                "prop" => Yii::t('FairModule.fair', 'Site fair'),
                "value" => $fairInfo['site']
            ),
            "region" => array(
                "prop" => Yii::t('FairModule.fair', 'Region'),
                "value" => $fairInfo["cityName"] . ' / ' . $fairInfo['regionName'],
                "link" => Yii::app()->createUrl('regionInformation/default/view', array('id' => $fairInfo['regionPk']))
            ),
            "exhibitionComplex" => array(
                "id" => $fairInfo['exhibitionComplexShortUrl'],
                "prop" => Yii::t('FairModule.fair', 'Platform'),
                "value" => $fairInfo["exhibitionComplexName"],
                //TODO deprecated
                "link" => Yii::app()->createUrl('exhibitionComplex/html/view', array('urlVenueName' => $fairInfo['exhibitionComplexId']))
            ),
            "organizer" => array(
                "prop" => Yii::t('FairModule.fair', 'Organizer'),
                "value" => $fairInfo["organizerName"],
            ),
            "rating" => array(
                "prop" => Yii::t('fair', 'Rating'),
                "value" => $fairInfo["rating"],
            ),
            "members" => array(
                "prop" => Yii::t('FairModule.fair','Exponents count'),
                "value" => $fairInfo["members"],
            ),
            "button" => Yii::t('FairModule.fair','Switch on fair'),
            "squareNet" => array(
                "prop" => Yii::t('FairModule.fair','Fair square'),
                "value" => !empty($fairInfo["squareNet"])?$fairInfo["squareNet"]:$fairInfo["squareGross"],
            ),
            "participationPrice" => array(
                "prop" => Yii::t('FairModule.fair','Arrend costs of fair square'),
                "value" => Fair::getFairParticipationPrice($fairInfo['participationPrice'], $fairInfo['course'], $fairInfo['currencyName']),
            ),
            "registrationFee" => array(
                "prop" => Yii::t('fair', 'Registration fee'),
                "value" => Fair::getFairRegistrationFee($fairInfo['registrationFee'], $fairInfo['course'], $fairInfo['currencyName']),
            ),
            "industry" => array(
                "prop" => Yii::t('FairModule.fair', 'Industry'),
                "value" => $fairInfo['industriesNames'],
            ),
            "hideLabel" => Yii::t('FairModule.fair','Hide'),
            "shortUrl" => $fairInfo['shortUrl'],
            "expandLabel" => Yii::t('FairModule.fair','Read full'),
            "inMatches" => $fairInfo['inMatches'],
            "disabled" => (bool)$fairInfo["disabled"],
            "siteLabel" => Yii::t('FairModule.fair', 'go to site'),
            "removeLabel" => Yii::t('FairModule.fair', 'remove'),
            "compareLabel" => Yii::t('FairModule.fair', 'compare'),
            "canceled" => $fairInfo['canceled'],
            "startLabel" => Yii::t('FairModule.fair', 'start'),
            "disabledLabel" => Yii::t('FairModule.fair', 'disabled'),
            "endLabel" => Yii::t('FairModule.fair', 'end'),
            "startDate" => $fairInfo['startDate'],
            "endDate" => $fairInfo['endDate'],
            "desc" => array(
                "title" => Yii::t('FairModule.fair', 'fair desc title'),
                "short" => $fairInfo["description"],
                "content" => $fairInfo["uniqueText"],
            ),
            "modified" => $this->getModifiedDate($fairInfo["datetimeModified"]),
            "image" => $this->getExhibitionComplexImage(Fair::model()->findByPk($fairInfo['id'])),
            "lang" => $fairInfo['lang'],
            "statisticTooltip" => [
                'label' => Yii::t('FairModule.fair', 'statisticTooltip label'),
                "title" => $this->getStatisticTooltipInfo($fairInfo),
            ],
            "pricesTooltip" => [
                'label' => Yii::t('FairModule.fair', 'pricesTooltip label'),
                "title" => Yii::t('FairModule.fair', 'pricesTooltip title')
            ],
        );

        return $fairInfoArray;
    }

    public function getStatisticTooltipInfo($fairInfo){

        $statisticTooltipInfo = '';
        if($fairInfo['forumId'] == $fairInfo['id']){

            $sql = "
                SELECT
                   trf.name fairName,
                   trf.shortUrl shortUrl
                FROM tbl_fair f
                LEFT JOIN tbl_trfair trf ON (f.id = trf.trParentId AND langId = :langId)
                LEFT JOIN tbl_fairinfo fi ON (f.infoId = fi.id)
                WHERE f.infoId = :infoId AND f.id != :fid
            ";

            $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
                ':langId' => Yii::app()->language,
                ':infoId' => $fairInfo['infoId'],
                ':fid' => $fairInfo['id'],
            ]);

            $links = $this->getFairStatisticLinks($res);

            $statisticTooltipInfo = Yii::t('FairModule.fair', 'Fairs that enter in forum:')." <br/>".$links;
        }

        if(!empty($fairInfo['forumId']) && $fairInfo['forumId'] != $fairInfo['id']){
            $sql = "
                SELECT
                   trf.name fairName,
                   trf.shortUrl shortUrl
                FROM tbl_fair f
                LEFT JOIN tbl_trfair trf ON (f.id = trf.trParentId AND langId = :langId)
                LEFT JOIN tbl_fairinfo fi ON (f.infoId = fi.id)
                WHERE f.id = :forumId
            ";

            $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
                ':langId' => Yii::app()->language,
                ':forumId' => $fairInfo['forumId'],
            ]);

            $links = $this->getFairStatisticLinks($res);

            $statisticTooltipInfo = Yii::t('FairModule.fair', 'Fair entered in ').$links;
        }

        if($fairInfo['countSameInfoId'] > 1 && empty($fairInfo['forumId'])){
            $sql = "
                SELECT
                   trf.name fairName,
                   trf.shortUrl shortUrl
                FROM tbl_fair f
                LEFT JOIN tbl_trfair trf ON (f.id = trf.trParentId AND langId = :langId)
                LEFT JOIN tbl_fairinfo fi ON (f.infoId = fi.id)
                WHERE f.infoId = :infoId AND f.id != :fid
            ";

            $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
                ':langId' => Yii::app()->language,
                ':infoId' => $fairInfo['infoId'],
                ':fid' => $fairInfo['id'],
            ]);

            $links = $this->getFairStatisticLinks($res);

            $statisticTooltipInfo = Yii::t('FairModule.fair', 'Common statistic:')." <br/>".$links;
        }

        if(!empty($statisticTooltipInfo)){
            $statisticTooltipInfo = $statisticTooltipInfo.'<br/><div style="margin-top:10px;"></div>'.$fairInfo['statistics'];
        }

        return $statisticTooltipInfo;
    }

    public function getFairStatisticLinks($resArr){
        $row = array();

        $count = 0;
        foreach($resArr as $data){
            $count ++;
            $url = Yii::app()->createUrl('fair/html/view', array('urlFairName' => $data['shortUrl']));
            $row[] = '<a style="color:white;" href="'.$url.'">'.$data['fairName'].'</a>';
        }

        return implode(',<br/>', $row).".";
    }

    private function getModifiedDate($date)
    {
        $result = array(
            "label" => '',
            "date" => '',
        );

        if ($date != '0000-00-00 00:00:00') {
            $dateModified = strtotime($date);

            $result = array(
                "label" => Yii::t('FairModule.fair', 'datetimeModifiedLabel'),
                "date" => date('d.m.Y', $dateModified),
            );
        }

        return $result;
    }

    /**
     * @deprecated
     * @param $fairId
     * @return array
     */
    public function getFairsByIndustry($fairId)
    {
        $sql = "
        SELECT 
            COUNT(*) cnt,
            tri.name industryName
            
        FROM tbl_fairhasindustry fhi0 
        LEFT JOIN tbl_trindustry tri ON tri.trParentId =fhi0.industryId AND tri.langId = :langId
        LEFT JOIN tbl_fairhasindustry fhi ON fhi.industryId = fhi0.industryId
        LEFT JOIN tbl_fair f ON fhi.fairId = f.id
        
        WHERE fhi0.fairId = :fairId AND f.active = 1 AND YEAR(f.beginDate) >= YEAR(now())
        GROUP BY fhi0.industryId;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':fairId' => $fairId,
            ':langId' => Yii::app()->language,
        ]);

        $data = [];
        foreach($res as $row){
            $data[] = array(
                'name' => Yii::t('FairModule.fair','Fairs in industry')." ".$row['industryName'],
                'value' => $row['cnt'],
            );
        }

        return $data;
    }

    /**
     * @deprecated
     * @param $countryId
     * @return array
     */
    public function getFairsByCountry($countryId)
    {
        $sql = "
            SELECT 
                COUNT(0) cnt,
                trct.name countryName
            
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c ON ec.cityId = c.id
            LEFT JOIN tbl_region r ON c.regionId = r.id
            LEFT JOIN tbl_district d ON r.districtId = d.id
            LEFT JOIN tbl_country ct ON ct.id = d.countryId
            LEFT JOIN tbl_trcountry trct ON ct.id = trct.trParentId AND trct.langId = :langId
            
            WHERE f.active = 1 AND ct.id = :countryId AND YEAR(f.beginDate) >= YEAR(NOW())
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':countryId' => $countryId, ':langId' => Yii::app()->language]);

        $res = $dataReader->read();

        $fairsCountry = array(
            "name" => Yii::t('FairModule.fair','Fairs in country')." ".$res['countryName'],
            "value" => $res['cnt'],
        );

        return $fairsCountry;
    }

    /**
     * @deprecated
     * @param $fairId
     * @return array
     */
    public function getCountersIndustry($fairId)
    {

        $sql = "
            SELECT 
                COUNT(DISTINCT cc.statId) cnt
                ,tri.name industryName
                
            FROM tbl_chartcolumn cc
            LEFT JOIN tbl_charttable ct ON ct.id = cc.tableId
            LEFT JOIN tbl_industry i ON i.id = ct.industryId
            LEFT JOIN tbl_trindustry tri ON tri.trParentId = i.id AND tri.langId = :langId
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.industryId = i.id AND fhi.fairId = :fairId
            
            WHERE fhi.id IS NOT NULL
            GROUP BY i.id
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':fairId' => $fairId, ':langId' => Yii::app()->language]);

        $res = $dataReader->readAll();

        $data = array();
        foreach($res as $ind){
            $data[] = array(
                'name' => Yii::t('FairModule.fair','Counters in industry').' '.$ind['industryName'],
                'value' => $ind['cnt'],
            );
        }

        return $data;
    }

    /**
     * @deprecated
     * @param $regionId
     * @return array
     */
    public function getFairCountByRegion($regionId)
    {

        $sql = "
            SELECT 
                COUNT(0) cnt,
                trr.name regionName
            
            FROM tbl_fair f 
            LEFT JOIN tbl_exhibitioncomplex ec ON ec.id = f.exhibitionComplexId
            LEFT JOIN tbl_city c ON ec.cityId = c.id
            LEFT JOIN tbl_region r ON c.regionId = r.id
            LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId AND trr.langId = :langId
            WHERE f.active = 1 AND r.id = :regionId AND YEAR(f.beginDate) >= YEAR(NOW())
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([':regionId' => $regionId, ':langId' => Yii::app()->language]);

        $res = $dataReader->read();

        $fairsRegion = array(
            'name' => Yii::t('FairModule.fair','Fairs on region').' '.$res['regionName'],
            'value' => $res['cnt'],
        );

        return $fairsRegion;
    }

    public function getCityProduction($cityId, $productIds)
    {

        $products = array();
        foreach ($productIds as $productId) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('cityId = :cityId');
            $criteria->addCondition('productId = :productId');
            $criteria->params[':productId'] = $productId;
            $criteria->params[':cityId'] = $cityId;

            $product = ProductPrice::model()->find($criteria);

            if (empty($product)) {
                continue;
            }

            $prodArr['header'] = $product->getCategoryName();
            $prodArr['description'] = $product->product->name;
            $prodArr['unit'] = Currency::getCurrencyIconById($product->product->currency) . ' / ' . $product->product->getCostTypeById($product->product->costType);
            $prodArr['value'] = $product->cost;

            $products[] = $prodArr;
        }

        $city = City::model()->findByPk($cityId);
        $cityName = '';
        if(!empty($city) && !empty($city->name)){
            $cityName = $city->name;
        }

        return array(
            'link' => '/' . Yii::app()->language . '/prices/'.$city->shortUrl,
            'header' => Yii::t('FairModule.fair','Prices in city').' '.$cityName,
            'allLabel' => Yii::t('FairModule.fair', 'all'),
            'list' => $products,
        );
    }

    /**
     * @deprecated
     * @return array
     */
    public function getFairCommonDocuments(){

        $sql = "
            SELECT 
                d.type type, 
                trd.description name, 
                trd.value path 
                
            FROM tbl_documents d
             
            LEFT JOIN tbl_trdocuments trd ON (d.id = trd.trParentId AND langId = '".Yii::app()->language."')
            WHERE d.fairId IS NULL AND d.active = 1;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll();
        $data = array();

        foreach($res as $row){
            $data[] = array(
                'name' => $row['name'],
                'value' => $row['path'],
            );
        }

        return array(
            'list' => $data,
            'docsLabel' => Yii::t('FairModule.fair', 'docs'),
            'allDocsLabel' => Yii::t('FairModule.fair', 'allDocs'),
            'link' => Yii::app()->createUrl('workspace/list/documents', ['id' => $this->fairId])
        );
    }
    
    /**
     * @deprecated
     * @param $fairId
     * @return array
     */
    public function getFairUsefulLinks($fairId)
    {

        if (empty($fairId) || !is_numeric($fairId)) {
            return array();
        }

        $fair = Fair::model()->findByPk($fairId);

        if (empty($fair)) {
            return array();
        }

        $linksArray = array();

        $fairHasIndustries = $fair->fairHasIndustries;

        if (is_array($fairHasIndustries) && !empty($fairHasIndustries)) {

            foreach ($fairHasIndustries as $fairHasIndustry){

                if(isset($fairHasIndustry->industry)){

                    $link['name'] = Yii::t('FairModule.fair', 'TOP fairs') . ' / ' . $fairHasIndustry->industry->name;
                    $link['value'] = 'catalog/industry/'.$fairHasIndustry->industryId;
                    $linksArray[] = $link;
                }
            }
        }

//        if (!empty($fair->fairHasIndustries)) {
//            foreach ($fair->fairHasIndustries as $item) {
//                if ($item->industry) {
//
//                    $countFairsByIndustryAndRegion = FairHasIndustry::countFairsByIndustryAndRegion($item->industry->id, $fair->exhibitionComplex->region->id);
//
//                    if ($countFairsByIndustryAndRegion > 2) {
//
//                        $link['name'] = $fair->exhibitionComplex->region->name . ' / ' . $item->industry->name;
//                        $link['value'] = $this->createUrl(
//                            '/search',
//                            [
//                                'sector' => 'fair',
//                                'industryId' => $item->industry->id,
//                                'regionId' => $fair->exhibitionComplex->region->id,
//                            ]
//                        );
//                        $linksArray[] = $link;
//                    }
//                }
//            }
//        }

//        $capital = City::model()->findByPk(1);//Moscow
//        if (
//            (!empty($fair->fairHasIndustries) && $capital) &&
//            !empty($fair->exhibitionComplex->city->id) &&
//            ($capital->id != $fair->exhibitionComplex->city->id)
//        ){
//            foreach ($fair->fairHasIndustries as $item){
//                if ($item->industry){
//
//                    $capitalFairsCountByIndustryAndCity = FairHasIndustry::countFairsByIndustryAndCity($item->industry->id, $capital->id);
//
//                    if ($capitalFairsCountByIndustryAndCity > 2) {
//
//                        $link['name'] = $capital->name . ' / ' . $item->industry->name;
//                        $link['value'] = $this->createUrl(
//                            '/search',
//                            ['sector' => 'fair', 'cityId' => $capital->id, 'industryId' => $item->industry->id]
//                        );
//
//                    }
//                }
//            }
//        }

        $data = array(
            "header" => Yii::t('FairModule.fair','Useful links'),
            "links" => $linksArray
        );

        return $data;
    }

    /**
     * @deprecated
     * @param $fairId
     * @return array
     */
    public function getAnalyticsData($fairId){
        Yii::import('application.modules.gradoteka.models.*');

        $sql = '
            SELECT
              fhi.industryId,
              tri.name,
              tri.shortNameUrl industryShortUrl,
              trr.name regionName,
              trr.shortUrl regionShortUrl,
              trctry.name countryName,
              ctry.shortUrl countryShortUrl
              
            FROM tbl_fair f
            LEFT JOIN tbl_exhibitioncomplex ec ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_city c ON c.id = ec.cityId
            LEFT JOIN tbl_region r ON r.id = c.regionId
            LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId AND trr.langId = :langId
            LEFT JOIN tbl_district d ON d.id = r.districtId
            LEFT JOIN tbl_country ctry ON ctry.id = d.countryId
            LEFT JOIN tbl_trcountry trctry on trctry.trParentId = ctry.id AND trctry.langId = :langId
            LEFT JOIN tbl_fairhasindustry fhi ON fhi.fairid = f.id
            LEFT JOIN tbl_industry i ON fhi.industryId = i.id
            LEFT JOIN tbl_trindustry tri ON tri.trParentId = i.id AND tri.langId = :langId
            WHERE f.id = :fairId
        ';

        $industriesIds = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [':fairId'=>$fairId, ':langId' => Yii::app()->language]);

        $analyticsData = array();
        foreach($industriesIds as $industryId){
            $circles = array();
            foreach ($this->getIndustryCirclesData($fairId, $industryId['industryId']) as $k => $r){

                if($r['gValId'] === NULL){
                    continue;
                }

                $gvalue = new GValue();
                $gvalue->id = $r['gValId'];
                $gvalue->setId = $r['gValSetId'];
                $gvalue->epoch = $r['gValEpoch'];

                //@TODO HOTFIX FOR UNAVAILABLE STATS!!!
                if(in_array($gvalue->setId, array('83904', '83905'))){

                    $circles[] = [
                        'percent'     => '0',
                        'header'      => $r['header'],
                        'ratingPlace' => '?',
                        'ratingLabel' => Yii::t('FairModule.fair','Empty'),
                        'statUnit'    => Yii::t('FairModule.fair','Empty in this region'),
                        'statValue'   => '',
                    ];

                    continue;
                }

                $circles[] = [
                    'percent'     => $gvalue->getTopValue() !== NULL ? round($r['gvalue']/($gvalue->getTopValue()/100), 2) : NULL,
                    'header'      => $r['header'],
                    'ratingPlace' => $gvalue->getRatingPoint(),
                    'ratingLabel' => Yii::t('FairModule.fair','Place in Russia'),
                    'statUnit'    => $r['unit'],
                    'statValue'   => strpos(round($r['gvalue'],self::GVALUE_ROUND_PRECISION), '.')
                        ? number_format(round($r['gvalue'],self::GVALUE_ROUND_PRECISION), self::GVALUE_ROUND_PRECISION, '.', ' ')
                        : number_format($r['gvalue'], 0, '.', ' '),
                ];
            }

            if(empty($circles)){
                continue;
            }

            $ind = [
                'header'  => $industryId['name'],
                'table'   => array(
                    "rows"=> $this->getFederalStats($fairId, $industryId['industryId']),
                ),
                'circles' => $circles,
                'regionName' => $industryId['regionName'],
                'countryName' => $industryId['countryName'],
                'regionLink' => '/' . Yii::app()->language . '/' . $industryId['countryShortUrl'] . '/' . $industryId['regionShortUrl'] . '/#analytics',
                'regionLabel' => Yii::t('FairModule.fair', 'go to region'),
                'industryLink' => Yii::app()->createUrl('industry/default/view', ['langId'=>Yii::app()->language, 'urlIndustryName'=>$industryId['industryShortUrl']]),
                'industryLabel' => Yii::t('FairModule.fair', 'go to industry'),
            ];

            $analyticsData[] = $ind;
        }

        return $analyticsData;
    }

    public function getFederalStats($fairId, $industryId){

        $sql = "
            SELECT 
                gval.id gValId
                ,gval.setId gValSetId
                ,IFNULL(acp.header,stat.name) header
                ,IFNULL(acp.measure,stat.unit) unit,
                
                CASE WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)
         
                    WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)
         
                    WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)
    
                    WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END) END gvalue,
                    
                CASE WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-1
                     
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-2
                     
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-3
                
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-4 END gValEpoch
                
            FROM tbl_fair f
            LEFT JOIN tbl_exhibitioncomplex ec       ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_city c                     ON c.id = ec.cityId
            LEFT JOIN tbl_region r                   ON r.id = c.regionId
            LEFT JOIN tbl_district d                   ON d.id = r.districtId
            LEFT JOIN tbl_gobjecthascountry gohc      ON gohc.countryId = d.countryId
            LEFT JOIN tbl_gobjects obj               ON obj.id = gohc.objId
            LEFT JOIN tbl_gobjectshasgstattypes gset ON gset.objId = obj.id
            LEFT JOIN tbl_gstattypes stat            ON stat.gId = gset.statId
            LEFT JOIN tbl_analyticschartpreview acp  ON acp.stat = gset.statId
            LEFT JOIN tbl_gvalue gval                ON gval.setId = gset.id
            
            WHERE f.id = :fairId AND 
                  acp.id IS NOT NULL AND 
                  acp.industryId = :industryId AND
                  acp.gType = obj.gType
            GROUP BY gval.setId;
        ";

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':fairId'=>$fairId,
            ':industryId' => $industryId,
        ]);

        $rawData = array();
        foreach($res as $row){
            $rawData[] = array(
                $row['header'],
                $row['unit'],
                strpos(round($row['gvalue'],2), '.')
                    ? number_format(round($row['gvalue'],2), 2, '.', ' ')
                    : number_format($row['gvalue'], 0, '.', ' ')
            );
        }

        return $rawData;
    }

    public function getIndustryCirclesData($fairId, $industryId){

        $sql = '
            SELECT 
                gval.setId gValSetId
                ,IFNULL(acp.header,stat.name) header
                ,IFNULL(acp.measure,stat.unit) unit,
                
                CASE WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)
         
                    WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)
    
                    WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)
    
                    WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END)) IS NOT NULL
                    THEN  SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END) END gvalue,

                CASE WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-1
                     
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-2
                
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-3
                
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END)) IS NOT NULL
                     THEN  YEAR(NOW())-4 END gValEpoch,
                     
                 CASE WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.value END)) IS NOT NULL
                     THEN  MAX(CASE WHEN (gval.epoch = YEAR(NOW())-1) THEN gval.id END)
                     
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.value END)) IS NOT NULL
                     THEN  MAX(CASE WHEN (gval.epoch = YEAR(NOW())-2) THEN gval.id END)
                
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.value END)) IS NOT NULL
                     THEN  MAX(CASE WHEN (gval.epoch = YEAR(NOW())-3) THEN gval.id END)
                
                     WHEN (SUM(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.value END)) IS NOT NULL
                     THEN  MAX(CASE WHEN (gval.epoch = YEAR(NOW())-4) THEN gval.id END) END gValId
                
            FROM tbl_fair f
            LEFT JOIN tbl_exhibitioncomplex ec       ON f.exhibitionComplexId = ec.id
            LEFT JOIN tbl_city c                     ON c.id = ec.cityId
            LEFT JOIN tbl_region r                   ON r.id = c.regionId
            LEFT JOIN tbl_gobjecthasregion gohr      ON gohr.regionId = r.id
            LEFT JOIN tbl_gobjects obj               ON obj.id = gohr.objId
            LEFT JOIN tbl_gobjectshasgstattypes gset ON gset.objId = obj.id
            LEFT JOIN tbl_gstattypes stat            ON stat.gId = gset.statId
            LEFT JOIN tbl_analyticschartpreview acp  ON acp.stat = gset.statId
            LEFT JOIN tbl_gvalue gval                ON gval.setId = gset.id
            
            WHERE f.id = :fairId AND 
                  acp.id IS NOT NULL AND 
                  acp.industryId = :industryId AND
	              acp.gType = obj.gType
            GROUP BY gval.setId;
        ';

        $res = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':fairId'=>$fairId,
            ':industryId' => $industryId,
        ]);

        return $res;
    }

    public function getFairStats($fairId){
        $sql = "
            SELECT 
              fi.members members,
              fi.visitors visitors,
              fi.squareNet squareNet,
              fi.squareGross squareGross,
              YEAR(fi.statisticsDate) `year`
            
            FROM tbl_fair f
            LEFT JOIN tbl_fairinfo fi ON fi.id = f.infoId
            WHERE f.id = :fairId
            GROUP BY f.id
        ";

        $dataReader = Yii::app()->db->createCommand($sql)->query([
            ':fairId' => $fairId,
        ]);

        $res = $dataReader->read();

        return array(
            array(
                "header" => Yii::t('FairModule.fair','Exponents count'),
                "unit" => '',
                "year" => $res["year"],
                "value" => $res["members"],
            ),
            array(
                "header" => Yii::t('FairModule.fair','Guests count'),
                "unit" => '',
                "year" => $res["year"],
                "value" => $res["visitors"],
            ),
            array(
                "header" => Yii::t('FairModule.fair','Fair square'),
                "unit" =>  !empty($res["squareNet"])?Yii::t('FairModule.fair','m2'):Yii::t('FairModule.fair','m2(brutto)'),
                "year" => $res["year"],
                "value" => !empty($res["squareNet"])?$res["squareNet"]:$res["squareGross"],
            ),
        );
    }

    public function actionCompare(){
        $json = array(
            "header"=> Yii::t('FairModule.fair','Compare'),
            "table"=> array(
                "headers"=> array(
                    "name"=> Yii::t('FairModule.fair','Fair name'),
                    "rating"=> "",
                    "startDate"=> Yii::t('FairModule.fair','Begin date'),
                    "endDate"=> Yii::t('FairModule.fair','End date'),
                    "square"=> Yii::t('FairModule.fair','Square net'),
                    "members"=> Yii::t('FairModule.fair','Exponents'),
                    "visitors"=> Yii::t('FairModule.fair','Visitors')
                ),
                "list"=> $this->getCompareList(),
                "ratings"=>$this->getRatings(),
            ),
        );

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . json_encode($json) . ');';
    }

    public function getCompareList(){

        $sql = '
            SELECT
                trf.name fairName,
                trf.shortUrl shortUrl,
                f.rating rating,
                f.beginDate startDate,
                f.endDate endDate,
                fi.squareNet square,
                fi.members members,
                fi.visitors visitors

            FROM tbl_fairmatches fm

            LEFT JOIN tbl_fair f ON (fm.fairId = f.id)
            LEFT JOIN tbl_trfair trf ON (trf.trParentId = f.id AND trf.langId = :langId)
            LEFT JOIN tbl_fairinfo fi ON (fi.id = f.infoId)

            WHERE fm.userId = :userId
        ';

        $fairMatches = Yii::app()->db->createCommand($sql)->queryAll(TRUE, [
            ':langId' => Yii::app()->language,
            ':userId' => Yii::app()->user->id,
        ]);

        $result = array();
        foreach($fairMatches as $fairMatch){
            $result[] = array(
                "name" =>$fairMatch['fairName'],
                "rating"=> $fairMatch['rating'],
                "startDate"=> $fairMatch['startDate'],
                "endDate"=>$fairMatch['endDate'],
                "square"=>$fairMatch['square'],
                "members"=>$fairMatch['members'],
                "visitors"=>$fairMatch['visitors'],
                "link"=>$fairMatch['shortUrl'],
            );
        }

        return $result;
    }

    public function getTitle($fairId){

        if(empty($fairId)){
            return NULL;
        }

        if(empty($fair = $this->getFairInfo($fairId))){
            return NULL;
        }

        $title = '';

        if(!empty($fair['name'])){
            $title = $title.$fair['name'];
        }

        $year = NULL;
        $startDay = NULL;
        $endDay = NULL;
        $monthName = NULL;
        if(!empty($fair['beginDate']) && !empty($fair['endDate'])){
            $title = $title.' - '.self::getDateRange($fair['beginDate'], $fair['endDate']);
        }

        return $title;
    }

    /**
     * @param $beginDate
     * @param $endDate
     * @param string $commaSeparate
     * @return string
     */
    public static function getDateRange($beginDate, $endDate, $commaSeparate = ''){

        $defaultDate = '1970-01-01';
        $bMonth = date('n', strtotime($beginDate));
        $bYear = date('Y', strtotime($beginDate));
        $eYear = date('Y', strtotime($endDate));
        $eMonth = date('n', strtotime($endDate));
        $beginDateNull = $beginDate == null || $beginDate == $defaultDate;
        $endDateNull = $endDate == null || $endDate == $defaultDate;

        if (!$beginDateNull && !$endDateNull) {
            return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate))  . ($bMonth != $eMonth ? ' '.static::getMonthData($bMonth) : '') . '-' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($eMonth) . $commaSeparate . ' ' . $eYear;
        } elseif ($beginDateNull) {
            return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate)) . ' - ' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($eMonth) . $commaSeparate . ' ' . $eYear;
        } elseif ($endDateNull) {
            return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate)) . ' - ' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($bMonth) . ' / ' . $commaSeparate . ' ' . $bYear;
        }

        return '';
    }

    public static function getMonthData($month = NULL)
    {
        if (null != $month) {
            $data = self::getMonthData();
            if (isset($data[$month])) {
                return $data[$month];
            }
        }

        return [
            1 => Yii::t('system', 'january'),
            2 => Yii::t('system', 'february'),
            3 => Yii::t('system', 'march'),
            4 => Yii::t('system', 'april'),
            5 => Yii::t('system', 'may'),
            6 => Yii::t('system', 'june'),
            7 => Yii::t('system', 'jule'),
            8 => Yii::t('system', 'august'),
            9 => Yii::t('system', 'september'),
            10 => Yii::t('system', 'october'),
            11 => Yii::t('system', 'november'),
            12 => Yii::t('system', 'december'),
        ];
    }

    public function getDescription($fairId){

        if(empty($fairId)){
            return NULL;
        }

        if(empty($fair = $this->getFairInfo($fairId))){
            return NULL;
        }

        $description = '';

        if($fair['endDate'] < date('Y-m-d')){

            $pattern = 'DESCRIPTION_'. strtoupper(Yii::app()->language) . '_PATTERN_PAST';

            if(isset(self::$description_patterns[$pattern])){
                $description = self::$description_patterns[$pattern];
            }

        } else {

            $pattern = 'DESCRIPTION_'. strtoupper(Yii::app()->language) . '_PATTERN_FUTURE';

            if(isset(self::$description_patterns[$pattern])){
                $description = self::$description_patterns[$pattern];
            }
        }

        if(isset($fair['beginDate']) && !empty($fair['beginDate']) && isset($fair['endDate']) && !empty($fair['endDate'])){
            $description = str_replace('{fairDate}', self::getDateRange($fair['beginDate'], $fair['endDate'], Yii::app()->language == strtolower(self::ENGLISH_LANGUAGE) ? ',' : ''), $description);
        }

        if(isset($fair['exhibitionComplexShortTypeName']) && !empty($fair['exhibitionComplexShortTypeName']) && $this->enabledVenueShortTypeForForeignLang){
            $description = str_replace('{exhibitionComplexShortTypeName}', $fair['exhibitionComplexShortTypeName'], $description);
        }

        if(isset($fair['exhibitionComplexName']) && !empty($fair['exhibitionComplexName'])){
            $description = str_replace('{venueName}', $fair['exhibitionComplexName'], $description);
        }

        if(isset($fair['fairTypeName']) && !empty($fair['fairTypeName'])){
            $description = str_replace('{fairTypeName}', $fair['fairTypeName'], $description);
        }

        if(isset($fair['name']) && !empty($fair['name'])){
            $description = str_replace('{fairName}', $fair['name'], $description);
        }

        if(isset($fair['beginDate']) && !empty($fair['beginDate'])){
            $description = str_replace('{fairYear}', date('Y', strtotime($fair['beginDate'])), $description);
        }

        if(isset($fair['members']) && !empty($fair['members'])){
            $description = preg_replace('/\{members}/', $fair['members'], $description, 1);
            $description = str_replace('{membersUnit}', H::getCountText($fair['members'], [Yii::t('FairModule.fair','companies'), Yii::t('FairModule.fair','company'), Yii::t('FairModule.fair','companies2')], TRUE), $description);
        }

        if(isset($fair['visitors']) && !empty($fair['visitors'])){
            $description = preg_replace('/\{visitors}/', $fair['visitors'], $description, 1);
            $description = str_replace('{visitorsUnit}', Yii::t('FairModule.fair',' mans'), $description);
        }

        if(preg_match('/\{(\w+)}[\s+]/', $description)){
            $description = preg_replace('/\{(\w+)}[\s+]/', '', $description);
        }

        if(preg_match('/\{(\w+)}/', $description)){
            $description = preg_replace('/\{(\w+)}/', '', $description);
        }

        return $description;
    }

    public function actionGetFairsByName($queryString)
    {
        $conditions = array();
        $additionalParams = array();

        if(!empty($queryString)){
            $conditions[] = " trf.name LIKE :searchQuery AND ";
            $additionalParams = [':searchQuery' => "%{$queryString}%"];
        }

        $params = $additionalParams;

        $sql = [
            "SELECT" => "
                f.id id,
                trf.name name,
                f.beginDate,
                trf.shortUrl shortUrl
            ",
            "FROM" => "tbl_fair f",
            "JOIN" => "
                LEFT JOIN tbl_trfair trf ON trf.trParentId = f.id
            ",
            "WHERE" => implode('', $conditions) . " f.active = 1 AND
                  f.beginDate >= NOW()",
            "GROUP" => "f.id",
            "ORDER" => "trf.name",
            "LIMIT" => 20,
        ];

        $fairs = Yii::app()->db->createCommand($sql)->queryAll(TRUE, $params);

        $data = array();
        foreach($fairs as $key => $value){
            $data[] = array(
                "id" => $value['shortUrl'],
                "display" => $value['name'],
                "date" => $value['beginDate'],
                "value" => mb_strtolower($value['name'], 'utf-8'),
                "image" => $this->getExhibitionComplexImage(Fair::model()->findByPk($value['id']))
            );
        }

        echo (isset($_GET['callback']) ? $_GET['callback'] : '') . ' (' . CJSON::encode(['models' => $data]) . ');';
    }

    public function actionGetFairData($langId, $urlFairName){

        $trFair = TrFair::model()->findByAttributes([
            'langId' => $langId,
            'shortUrl' => $urlFairName,
        ]);

        if($trFair === NULL) {
            $trFair = TrFair::model()->findByAttributes([
                'shortUrl' => $urlFairName,
            ]);
        }

        if($trFair === NULL){
            throw new CHttpException(404, 'Fair not found');
        }

        $fairId = $trFair->trParentId;

        $fair = Fair::model()->findByPk($fairId);

        if (empty($fair) /*|| $fair->active != 1*/) {
            echo json_encode([
                'status' => 'failure',
                'msg' => 'fair not found',
            ]);
            exit;
        }

        $json = array(
            "id" => $trFair->shortUrl,
            "name" => $trFair->name,
            "startDate" => $fair->beginDate,
            "endDate" => $fair->endDate,
            "image" => $this->getExhibitionComplexImage($fair),
            "lang" => $trFair->langId,
        );

        echo (isset($_GET['callback'])?$_GET['callback']:'') . ' (' . json_encode($json) . ');';
    }

    /**
     * @param Fair $fair
     * @return null|string
     */
    private function getExhibitionComplexImage(Fair $fair)
    {
        $result = null;
        $exhibitionComplexHasFile = null;
        $file = null;

        /** @var ExhibitionComplex $exhibitionComplex */
        $exhibitionComplex = $fair->exhibitionComplex;

        if ($exhibitionComplex != null) {
            /** @var ExhibitionComplexHasFile $file */
            $exhibitionComplexHasFile = $exhibitionComplex->getMainImage();
        }

        if ($exhibitionComplexHasFile != null) {
            /** @var ObjectFile $file */
            $file = $exhibitionComplexHasFile->file;
        }

        if ($file != null) {
            $result = $file->getAbsoluteUrl();
        }

        return $result;
    }

    private function getChecklistData()
    {
        return [
            'title' => 'ExpoPlanner',
            'list' => [
                [
                    'title' => Yii::t('FairModule.fair','You are exhibition?'),
                    'content' => Yii::t('FairModule.fair','Expoplanner desc'),
                    'accent' => true
                ]
            ],
            'button' => [
                'label' => Yii::t('FairModule.fair', 'To make a plan'),
                "gotoChecklistLabel" => Yii::t('FairModule.fair', 'your expoplanner'),
                'link' => 'http://expoplanner.protoplan.pro'
            ]
        ];
    }

    private function getStatisticTooltip()
    {
        return [
            'label' => Yii::t('FairModule.fair', 'statisticTooltip label'),
        ];
    }
}