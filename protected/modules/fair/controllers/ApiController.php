<?php

class ApiController extends Controller {

    const STATUS_NOT_AUTHORIZED = '403 Forbidden';

    protected $authenticated = false;

    /**
     * @param string $SystemId
     * @param string $UserName
     * @param string $Password
     * @return bool
     * @soap
     */
    public function actionAuth($SystemId, $UserName, $Password) {

        echo json_encode(true);
        // Здесь прописывается любая функция, которая выдает ПРАВДА или ЛОЖЬ при проверке логина и пароля
        // В данном случае проверяется через модель стандартные для фреймворка Yii UserIdentity и метод authenticate()
        // При положительном результате передается в переменную $authenticated значение true
        $identity = new ClientIdentity($UserName, $Password);
        if ($identity->authenticate())
            $this->authenticated = true;

        echo json_encode(true);
    }

    /**
     * @param integer $fairId
     * @return bool
     * @soap
     */
    public function actionGetFairExists($fairId){
        $result = FALSE;

        if(!empty($fair = Fair::model()->findByPk($fairId))){
            $result = TRUE;
        }

        echo json_encode($result);
    }

    /**
     * @param integer $fairId
     * @param array $properties
     * @return array
     */
    public function actionGetFairProperties($fairId, array $properties){

        if(!empty($fair = Fair::model()->findByPk($fairId))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($fair->{$name}) &&
                    !empty($fair->{$name}) &&
                    !is_object($fair->{$name})
                ){
                    $res[$name] = $fair->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param integer $fairId
     * @return bool
     * @soap
     */
    public function actionIsMyFair($fairId){
        echo json_encode(MyFair::isMyFair($fairId));
    }

    /**
     * @param integer $fairId
     * @return bool
     * @soap
     */
    public function actionIsExponentFair($fairId){
        echo json_encode(MyFair::isExponentFair($fairId));
    }

    /**
     * @param integer $fairId
     * @return bool
     * @soap
     */
    public function actionIsOrganizerOfFair($fairId){
        echo json_encode(MyFair::isOrganizerOfFair($fairId));
    }

    /**
     * @return array
     * @soap
     */
    public function actionGetConstants(){
        $oClass = new ReflectionClass('Fair');
        echo  json_encode($oClass->getConstants());
    }

    /**
     * @param integer $fairId
     * @return bool|string
     * @soap
     */
    public static function actionGetMailLogoPath($fairId){

        $logoPath = FALSE;
        $fair = Fair::model()->findByPk($fairId);

        if(!empty($fair->getMailLogoPath())){
            $logoPath = $fair->getMailLogoPath();
        }

        echo json_encode($logoPath);
    }

    /**
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetShardedFairsProperties(array $properties){

        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        $result = array();
        if(!empty($fairs)){
            foreach($fairs as $fair){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($fair->{$name}) &&
                        !empty($fair->{$name})
                    ){
                        $res[$name] = $fair->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    /**
     * @param string $className
     * @param string $constantName
     * @return mixed
     * @soap
     */
    public function actionGetClassConstant($className, $constantName){

        $oClass = new ReflectionClass($className);

        echo json_encode($oClass->getConstant($constantName));
    }

    /**
     * @param integer $userId
     * @return bool
     * @soap
     */
    public function actionGetMyFairsExists($userId){

        $result = FALSE;

        $myFairs = MyFair::model()->findAllByAttributes(
            array(
                'userId' => $userId,
                'status' => MyFair::STATUS_ACTIVE,
            )
        );

        if(!empty($myFairs)){
            $result = TRUE;
        }

        echo json_encode($result);
    }

    /**
     * @param array $attributes
     * @return bool
     * @soap
     */
    public function actionGetMyFairExistsByAttributes(array $attributes){

        $result = FALSE;

        $myFair = MyFair::model()->findByAttributes($attributes);

        if(!empty($myFair)){
            $result = TRUE;
        }

        echo json_encode($result);
    }

    /**
     * @param integer $userId
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetMyFairsProperties($userId, array $properties){
        $myFairs = MyFair::model()->findAllByAttributes(
            array(
                'userId' => $userId,
                'status' => MyFair::STATUS_ACTIVE,
            )
        );

        $result = array();
        if(!empty($myFairs)){
            foreach($myFairs as $myFair){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($myFair->{$name}) &&
                        !empty($myFair->{$name})
                    ){
                        $res[$name] = $myFair->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    /**
     * @param array $ids
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetFairsPropertiesByIds(array $ids, array $properties){

        $crit = new CDbCriteria();
        $crit->addInCondition('id', $ids);
        $crit->order = 'beginDate ASC';

        $fairs = Fair::model()->findAll($crit);

        $result = array();
        if(!empty($fairs)){
            foreach($fairs as $fair){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($fair->{$name}) &&
                        !empty($fair->{$name})
                    ){
                        $res[$name] = $fair->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    /**
     * @param array $attributes
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetFairsPropertiesByAttributes(array $attributes, array $properties){

        $fairs = Fair::model()->findAllByAttributes($attributes);

        $result = array();
        if(!empty($fairs)){
            foreach($fairs as $fair){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($fair->{$name}) &&
                        !empty($fair->{$name})
                    ){
                        $res[$name] = $fair->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    /**
     * @param array $attributes
     * @return bool
     * @soap
     */
    public function actionCreateMyFair(array $attributes){

        $myFair = new MyFair();

        foreach($attributes as $key => $value){
            if(!empty($myFair->{$key})){
                $myFair->{$key} = $value;
            }
        }

        echo json_encode($myFair->save());
    }

    /**
     * @param array $attributes
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetMyFairPropertiesByAttributes(array $attributes, array $properties){

        if(!empty($myFair = MyFair::model()->findByAttributes($attributes))){
            $res = [];

            foreach($properties as $k => $name){
                $res[$name] = NULL;
                if(
                    isset($myFair->{$name}) &&
                    !empty($myFair->{$name})
                ){
                    $res[$name] = $myFair->{$name};
                }
            }

            $properties = $res;
        }

        echo json_encode($properties);
    }

    /**
     * @param array $fairs
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetOrganizerFairsListProperties(array $fairs, array $properties){

        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $fairs);
        $criteria->order = 'beginDate ASC';

        $models = Fair::model()->findAll($criteria);

        $result = array();
        if(!empty($models)){
            foreach($models as $fair){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($fair->{$name}) &&
                        !empty($fair->{$name})
                    ){
                        $res[$name] = $fair->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    public function actionGetWebRootPath(){
        echo json_encode(Yii::getPathOfAlias('webroot'));
    }

    public function actionGetFairShortUrl($fairId, $langId){
        Yii::app()->setLanguage($langId);

        $fair = Fair::model()->findByPk($fairId);

        $result = null;

        if(!empty($fair->shortUrl)){
            $result = $fair->shortUrl;
        }

        echo json_encode($fair->shortUrl);
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     */
    public function actionGetUrl($url, array $params = array()){
        echo json_encode(Yii::app()->createUrl($url, $params));
    }

    public function actionGetFairOrganizerIds($fairId){

        $fairHasOrganizers = FairHasOrganizer::model()->findAllByAttributes(array(
            'fairId' => $fairId
        ));

        foreach($fairHasOrganizers as $fairHasOrganizer){
            $organizerIds[] = $fairHasOrganizer->organizerId;
        }

        if(!empty($organizerIds)){
            echo json_encode($organizerIds);
        }else{
            echo json_encode(FALSE);
        }

    }

    /**
     * @param array $organizerIds
     * @param array $properties
     * @return array
     * @soap
     */
    public function actionGetFairsPropertiesByOrganizerIds(array $organizerIds, array $properties){

        $criteria = new CDbCriteria();
        $criteria->addInCondition('organizerId', $organizerIds);

        $fairHasOrganizers = FairHasOrganizer::model()->findAll($criteria);

        $result = array();
        if(!empty($fairHasOrganizers)){
            foreach($fairHasOrganizers as $fairHasOrganizer){

                $res = [];
                foreach($properties as $k => $name){
                    $res[$name] = NULL;
                    if(
                        isset($fairHasOrganizer->fair->{$name}) &&
                        !empty($fairHasOrganizer->fair->{$name})
                    ){
                        $res[$name] = $fairHasOrganizer->fair->{$name};
                    }
                }

                $result[] = $res;
            }
        }

        echo json_encode($result);
    }

    public function actionGetFirstOrganizerId($fairId){

        $result = NULL;

        $FHO = FairHasOrganizer::model()->findByAttributes(array('fairId' => $fairId));

        if(!empty($FHO->organizerId)){
            $result = $FHO->organizerId;
        }

        echo json_encode($result);
    }
}