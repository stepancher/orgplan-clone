function SubmitRecallButton(params) {
    return ({
        errorText: '',
        init: function (params) {
            var me = this;
            if('errorText' in params) {
                me.errorText = params.errorText;
            }
        },
        run: function (params) {
            var me = this;
            me.init(params);
            $('.submit-recall-button').on('click', function (event) {
                var $texarea = $('#Recall_comment');
                var value = $texarea.val();
                if (value.length > 0) {
                    return true;
                } else {
                    event.preventDefault();
                    if ($('.widget-RecallForm p.help-block').length == 0) {
                        $texarea.after('<p class="help-block">' + me.errorText + '</p>');
                    }
                }
            });
        }
    }).run(params);
};
