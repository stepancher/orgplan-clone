<?php
return CMap::mergeArray(
	include(__DIR__ . '/_global.php'),
	array(
		'preload'=>array('log'),
		'components' => array(
			'messages' => array(
				'forceTranslation' => true
			),
			'params' => array(
				'titleName' => 'Protoplan',
				'translatedLanguages' => [
					'ru' => 'Russian',
					'en' => 'English',
					'de' => 'Deutsch',
				],
				'defaultLanguage' => 'ru',
			),
			'assetManager'=>array(
				'class'     =>'CAssetManager',
				'basePath'  =>realpath(__DIR__.'/../../assets'),
				'baseUrl'   =>'/assets',
			),
			'request' => array(
				'hostInfo' => 'http://localhost',
				'baseUrl' => '/orgplan',
				'scriptUrl' => 'console.php',
			),
			'theme' => 'bootstrap',
			'log' => array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'trace, info',
						'logFile' => 'console-info.log',
					),
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error, warning',
						'logFile' => 'console-error.log',
					),
				),
			),
		),
		'commandMap' => array(
			'notification' => array(
				'class' => 'application.modules.mail.commands.NotificationCommand',
				'baseUrl' => 'http://orgplan.pro',
			),
			'migrate' => array(
				'class' => 'system.cli.commands.MigrateCommand',
				'connectionID' => 'dbRuntime',
				'templateFile'=>'application.migrations.templates.template',
			),
			'notificationclickbutton' => array(
				'class' => 'application.modules.mail.commands.NotificationClickButtonCommand',
				'baseUrl' => 'http://orgplan.pro',
			),
			'productionCalendar' => array(
				'class' => 'application.commands.ProductionCalendar',
			),
			'sitemap1' => [
				'class' => 'application.commands.SiteMap1Command',
				'baseUrl' => 'https://orgplan.pro',
			],
			'gradoteka' => array(
				'class' => 'application.modules.gradoteka.commands.GradotekaCommand',
			),
		)
	),
	is_file(__DIR__.'/_custom_console.php')	? include(__DIR__.'/_custom_console.php') : array()
);