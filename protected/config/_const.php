<?php
if (isset($_GET['debug'])) {
    setcookie('debug', (bool)$_GET['debug']);
    //defined('YII_DEBUG') or define('YII_DEBUG', (bool)@$_COOKIE['debug']);
}
if (isset($_GET['dev'])) {
    setcookie('dev', (bool)$_GET['dev']);
    // defined('IS_MODE_DEV') or define('IS_MODE_DEV', (bool)@$_COOKIE['dev']);
}
if (is_file(__DIR__ . '/_custom_const.php')) {
    include(__DIR__ . '/_custom_const.php');
}
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('IS_MODE_DEV') or define('IS_MODE_DEV', false);

defined('IS_BLOCK_ORGANIZER') or define('IS_BLOCK_ORGANIZER', IS_MODE_DEV);
defined('IS_BLOCK_STAND') or define('IS_BLOCK_STAND', IS_MODE_DEV);
defined('IS_BLOCK_DEVELOPER') or define('IS_BLOCK_DEVELOPER', IS_MODE_DEV);
defined('IS_BLOCK_TENDER') or define('IS_BLOCK_TENDER', IS_MODE_DEV);
defined('IS_BLOCK_INDUSTRY') or define('IS_BLOCK_INDUSTRY', IS_MODE_DEV);
defined('IS_BLOCK_PRICE') or define('IS_BLOCK_PRICE', IS_MODE_DEV);
defined('PINBA') or define('PINBA', false);
defined('XHPROF') or define('XHPROF', false);