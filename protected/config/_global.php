<?php
$appPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR;

Yii::setPathOfAlias('vendor', $appPath . 'vendor');
Yii::setPathOfAlias('bootstrap', $appPath . '/vendor/2amigos/yiistrap');
Yii::setPathOfAlias('yiiwheels', $appPath . '/vendor/2amigos/yiiwheels');
Yii::setPathOfAlias('app', $appPath);
/** For Yii2 */
Yii::setPathOfAlias('frontend', dirname($appPath) . '/_app_/frontend');
Yii::$classMap['CValidator'] = $appPath . '/components/CValidator.php';

return CMap::mergeArray(
    array(
        'id' => 'proto-app',
        'name' => 'ORGPLAN',
        'basePath' => $appPath,
        'language' => 'ru',
        'sourceLanguage' => 'en',
        'import' => array(
            'application.models.*',
            'application.components.*',
            'application.businessModels.*',
            'ext.*',
            'bootstrap.helpers.TbHtml',
            'bootstrap.widgets.*',
            'bootstrap.form.*',
            'ext.mail.YiiMailMessage',
            'ext.image.Image',
            'application.components.ImageHandler.CImageHandler',
        ),
        'preload' => array('session'),
        'components' => array(
            'assetManager' => array(
                'class' => 'ZAssetManager',
            ),
            'session' => array(
                'class' => 'ZDbHttpSession',
                'sessionName' => 'sid', // Do not use spaces!
                'autoCreateSessionTable' => true,
                'connectionID' => 'dbSession',
                'sessionTableName' => 'tbl_session',
//                'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
                'autoStart' => true,
                'cookieMode' => 'only', // 'none', 'allow' (uses URL if cookie is not allowed!) or 'only'
                'gCProbability' => 1 / 10000, // 0.01% garbage collection. @TODO Use trigger in DB
                //'cookieParams' => array('domain' => '.orgplan.pro',),
                'timeout' => 3600 * 24 * 30,
            ),
            'db' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'expodata' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'expodataRaw' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbProposal' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbProposalAgrorus' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbProposalF10943' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbTz' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbRuntime' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbSession' => array(
                'class' => 'ZDbConnection',
                'tablePrefix' => 'tbl_',
                'charset' => 'utf8',
                'queryCachingDuration' => 5,
                'schemaCachingDuration' => 300,
                'enableParamLogging' => false,
            ),
            'dbMan' => array(
                'class' => 'application.components.DbMan',
            ),
            'bootstrap' => array(
                'class' => 'bootstrap.components.TbApi',
            ),
            'yiiwheels' => array(
                'class' => 'yiiwheels.YiiWheels',
            ),
            'loid' => array(
                'class' => 'loid',
            ),
            'eauth' => array(
                'class' => 'EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'cache' => true, // Cache component name or false to disable cache. Defaults to 'cache'.
                'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                'services' => array( // You can change the providers and their classes.
                    'google_oauth' => array(
                        'class' => 'application.modules.auth.components.GoogleOAuthServiceCustom',
                    ),
                    'twitter' => array(
                        'class' => 'application.modules.auth.components.TwitterOAuthService',
                    ),
                    'facebook' => array(
                        'class' => 'application.modules.auth.components.FacebookOAuthServiceCustom',
                    ),
                    'vkontakte' => array(
                        'class' => 'application.modules.auth.components.VKontakteOAuthServiceCustom',
                    ),
                ),
            ),
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(

                ),
            ),
            'image' => array(
                'class' => 'application.extensions.image.CImageComponent',
                // GD or ImageMagick
                'driver' => 'GD',
                // ImageMagick setup path
                'params' => array('directory' => '/opt/local/bin'),
            ),
            'consoleRunner' => array(
                'class' => 'application.components.ConsoleRunner',
                'app' => dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'console.php',
                'php' => 'php'
            ),
            'format' => array(
                'dateFormat' => 'd.m.Y',
                'datetimeFormat' => 'd.m.Y h:i:s A',
            ),
            'ih' => array(
                'class' => 'CImageHandler',
            ),
        ),
    ),
    is_file(__DIR__ . '/_custom.php') ? include(__DIR__ . '/_custom.php') : array()
);