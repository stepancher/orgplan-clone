<?php
    defined('PINBA') or define('PINBA', function_exists('pinba_script_name_set'));
    defined('XHPROF') or define('XHPROF', extension_loaded('xhprof'));
    defined('YII_DEBUG') or define('YII_DEBUG', (bool)@$_COOKIE['debug']);
    defined('IS_MODE_DEV') or define('IS_MODE_DEV', (bool)@$_COOKIE['dev']);
    defined('IS_MODE_DEMO_OFF') or define('IS_MODE_DEMO_OFF', true || (bool)@$_COOKIE['demo-off']);