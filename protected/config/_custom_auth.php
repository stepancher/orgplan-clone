<?php
return array(
	'developerAccess' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Доступ для Застройщика',
		'bizRule' => null,
		'data' => null,
	),
	'freelancerAccess' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Доступ для Фрилансера',
		'bizRule' => null,
		'data' => null,
	),
	'exponentAccess' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Доступ для Експонента',
		'bizRule' => null,
		'data' => null,
	),
	'expertAccess' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Доступ для Експерта',
		'bizRule' => null,
		'data' => null,
	),
	'organizersAccess' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Доступ для Организатора',
		'bizRule' => null,
		'data' => null,
	),
	'administrationVisible' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Видимость для администрации',
		'bizRule' => null,
		'data' => null,
	),
);