<?php
$config = CMap::mergeArray(
    include(__DIR__ . '/_global.php'),
    array(
        'theme' => 'bootstrap',
        'defaultController' => 'site',
        'preload' => array('log', 'bootstrap'),
        'components' => array(
            'messages' => array(
                'forceTranslation' => true
            ),
            'log' => array(
                'class' => 'ZLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'trace, info',
                        'logFile' => 'application-info.log',
                    ),
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning',
                        'logFile' => 'application-error.log',
                    ),
                ),
            ),
            'urlManager' => array(
                'class' => 'ZUrlManager',
                'urlFormat' => 'path',
                'urlSuffix' => '',
                'showScriptName' => false,
                'urlRuleClass' => 'application.components.ZUrlRule',
                'rules' => array(

                    array('class' => 'application.components.RedirectUrlRule'),

                    /**SOAP paths**/
                    '/<langId>/api/v1/fair/<method>'            => 'fair/api/<method>',
                    '/<langId>/api/v1/user/<method>'            => 'user/api/<method>',
                    '/<langId>/api/v1/venue/<method>'           => 'exhibitionComplex/api/<method>',
                    '/<langId>/api/v1/geo/<method>'             => 'geo/api/<method>',
                    '/<langId>/api/v1/file/<method>'            => 'file/api/<method>',
                    '/<langId>/api/v1/organizer/<method>'       => 'organizer/api/<method>',
                    '/<langId>/api/v1/notification/<method>'    => 'notification/api/<method>',
                    '/<langId>/api/v1/profile/<method>'         => 'profile/api/<method>',
                    /**end SOAP paths**/

                    '/<langId>/fair/<urlFairName>'          => 'fair/html/view',
                    '/<langId>/getFairsByName'              => 'fair/json/getFairsByName',
                    '/<langId>/getFairData'                 => 'fair/json/getFairData',
                    '/<langId>/menuJson/getLang'            => 'menu/json/getLanguage',
                    '/<langId>/blog'                        => 'blog/default/index',
                    '/<langId>/blog/<url>'                  => 'blog/default/view',
                    '/<langId>/fairJson/<urlFairName>'      => 'fair/json/getData',
                    '/<langId>/fairDump/<urlFairName>'      => 'fair/json/dump',
                    '/<langId>/industriesJson'              => 'industry/json/getList',
                    '/<langId>/industriesList'              => 'industry/html/view',
                    '/<langId>/industryDump'                => 'industry/json/dump',
                    '/<langId>/analyticsJson'               => 'industry/json/getAnalyticsList',
                    '/<langId>/catalogJson'                 => 'catalog/json/getData',
                    '/<langId>/catalog'                     => 'catalog/html/view',
                    '/<langId>/catalogDump'                 => 'catalog/json/dump',
                    '/<langId>/venuesJson'                  => 'exhibitionComplex/json/list',
                    '/<langId>/venues'                      => 'exhibitionComplex/html/list',
                    '/<langId>/venuesList'                  => 'exhibitionComplex/json/getExhibitionComplexes',
                    '/<langId>/venuesDump'                  => 'exhibitionComplex/json/dump',
                    '/<langId>/regionsJson'                 => 'region/json/getList',
                    '/<langId>/regions'                     => 'region/html/view',
                    '/<langId>/regionsDump'                 => 'region/json/dump',
                    '/<langId>/menuJson'                    => 'menu/json/getData',
                    '/<langId>/pricesJson'                  => 'price/json/getData',
                    '/<langId>/citiesJson'                  => 'price/json/getCitiesList',
                    '/<langId>/compareFair/<urlFairName>'   => 'fair/default/matchChange2',
                    '/<langId>/compareClear'                => 'fair/default/matchClear2',
                    '/<langId>/compareJson'                 => 'fair/json/compare',
                    '/<langId>/industry/<urlIndustryName>'  => 'industry/default/view',
                    '/<langId>/registration'                => 'auth/default/reg',
                    '/<langId>/requestDemo'                 => 'auth/default/requestDemo',
                    '/<langId>/venue/<urlVenueName>'        => 'exhibitionComplex/html/view',
                    '/<langId>/venueJson/<urlVenueName>'    => 'exhibitionComplex/json/getData',
                    '/<langId>/venueDump/<urlVenueName>'    => 'exhibitionComplex/json/dump',
                    '/<langId>/helpJson/confidentiality'    => 'help/json/getConfidentiality',
                    '/<langId>/helpJson/termsOfUse'         => 'help/json/getTermsOfUse',
                    '/de/helpJson/impressum'          => 'help/json/getImpressum',
                    '/<langId>/help/confidentiality'        => 'help/default/confidentiality',
                    '/<langId>/help/termsOfUse'             => 'help/default/index',
                    '/<langId>/login'                       => 'auth/default/auth',
                    '/<langId>/logout'                      => 'auth/default/logout',
                    '/<langId>/region/<urlRegionName>'      => 'regionInformation/default/viewRegion',
                    '/<langId>/profileJson'                 => 'profile/json/getData',
                    '/<langId>/profileSaveUserAvatar'       => 'profile/json/saveUserAvatar',
                    '/<langId>/profileDeleteUserAvatar'     => 'profile/json/deleteUserAvatar',
                    '/<langId>/profileUserData'             => 'profile/json/saveUserData',
                    '/<langId>/profileCompanyData'          => 'profile/json/saveCompanyData',

                    '<langId:[a-z]{0,2}>/<controller>' => '<controller>',
                    '<langId:[a-z]{0,2}>/<controller>/<action>' => '<controller>/<action>',
                    '<langId:[a-z]{0,2}>/<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                    '<controller>' => '<controller>',
                    '<controller>/<action>' => '<controller>/<action>',
                    '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                ),
                //'caseSensitive'  => true,
            ),
            'authManager' => array(
                // Будем использовать свой менеджер авторизации
                'class' => 'PhpAuthManager',
                // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
                'defaultRoles' => array('guest'),
            ),
            'user' => array(
                'class' => 'WebUser',
                'allowAutoLogin' => true,
                'loginUrl'=> '/login',
            ),
            'errorHandler' => !YII_DEBUG ? array(
                'errorAction' => 'error/error/error',
            ) : null,
        ),
        'modules' => [
            'gradoteka' => [
                'class' => 'app.modules.gradoteka.GradotekaModule',
            ],
            'priceAdmin' => [
                'class' => 'application.modules.priceAdmin.PriceAdminModule',
            ],
//            'massMediaAdmin' => [
//                'class' => 'vendor.orgplan.massMediaAdmin.MassMediaAdminModule',
//            ],
            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => '123123',
            ),
            'RUEF',
            'tz',
            'workspace' => [
                'defaultController' => 'list',
            ],
            'organizer' => [
                'defaultController' => 'company',
            ],
            'blog' => [
                'defaultController' => 'default',
            ],
            'help',
            'calendar',
            'profile',
            'auth' => [
                'defaultController' => 'default',
            ],
            'error',
            'industry' => array(
                'components' => array(
                    'TrUrl' => array(
                        'class' => 'application.modules.industry.components.TrUrl'
                    ),
                ),
            ),
            'fair' => array(
                'components' => array(
                    'TrUrl' => array(
                        'class' => 'application.modules.fair.components.TrUrl'
                    ),
                ),
            ),
            'exhibitionComplex' => array(
                'components' => array(
                    'TrUrl' => array(
                        'class' => 'application.modules.exhibitionComplex.components.TrUrl'
                    ),
                ),
            ),
            'regionInformation' => array(
                'components' => array(
                    'TrUrl' => array(
                        'class' => 'application.modules.regionInformation.components.TrUrl'
                    ),
                ),
            ),
            'productPrice',
            'analytics',
            'file',
            'admin',
            'notification',
            'user',
            'geo',
            'calendarExponent',
            'catalog',
            'fairs',
            'menu',
            'region',
            'price',
        ],
        'params' => [
            'titleName' => 'Protoplan',
            'translatedLanguages' => [
                'ru' => 'Russian',
                'en' => 'English',
                'de' => 'Deutsch',
            ],
            'defaultLanguage' => 'ru',
            'defaultAmpersand' => '&',
        ]
    ),
    is_file(__DIR__ . '/_custom_web.php') ? include(__DIR__ . '/_custom_web.php') : array()
);
return $config;