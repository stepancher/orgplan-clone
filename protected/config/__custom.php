<?php return array(
    'params' => array(
        'expoplannerUrl' => 'http://checklist.orgplan.loc',
    ),
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=DBNAME',
            'username' => 'root',
            'password' => '',
            'enableParamLogging' => true,
            'initSQLs' => array("set time_zone='+00:00';")
        ),
        'dbRuntime' => array(
            'connectionString' => 'mysql:host=localhost;dbname=DBNAME',
            'username' => 'root',
            'password' => '',
            'enableParamLogging' => true,
            'initSQLs' => array("set time_zone='+00:00';")
        ),
        'sentry'=>array(
            'class'=>'ext.yii-sentry.components.RSentryClient',
            'dsn'=>'<YOUR_DSN>',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'ext.yii-sentry.components.RSentryLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'info@protoplan.pro',
                'password' => '',
                'port' => '465',
                'encryption' => 'ssl',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => false,
            'dryRun' => false
        ),
    ),
);