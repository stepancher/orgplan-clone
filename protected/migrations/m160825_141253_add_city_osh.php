<?php

class m160825_141253_add_city_osh extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('7', 'osh-district');
            INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('51', 'osh-oblast');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('146', 'osh');
            
            UPDATE {{exhibitioncomplex}} SET cityId = 267 WHERE id = 1148;
            
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('51', 'ru', 'Округ Ош');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('51', 'en', 'Округ Ош');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('51', 'de', 'Округ Ош');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('146', 'ru', 'Ошская область');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('146', 'en', 'Ошская область');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('146', 'de', 'Ошская область');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('267', 'ru', 'Ош');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('267', 'en', 'Ош');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('267', 'de', 'Ош');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}