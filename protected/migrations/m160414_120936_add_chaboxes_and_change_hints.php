<?php

class m160414_120936_add_chaboxes_and_change_hints extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = '
				DELETE FROM {{attributeclass}} WHERE `name`=\'n5_checkbox_agreement\';
				DELETE FROM {{attributeclass}} WHERE `name`=\'n9_checkbox_agreement\';
				DELETE FROM {{attributeclass}} WHERE `name`=\'n3_checkbox_agreement\';
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_checkbox_agreement', 'bool', '0', '518', '518', '2', 'checkboxAgreement', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n5_checkbox_agreement', 'bool', '0', '571', '571', '2', 'checkboxAgreement', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_checkbox_agreement', 'bool', '0', '662', '662', '11', 'checkboxAgreement', '0');
			UPDATE {{attributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n\nНастоящая страница носит информационный характер, для заказа услуг необходимо заполнить Заявку №9-1 (Разгрузка экспонатов) и Заявку №9-2 (Хранение тары и погрузка экспонатов)/' WHERE `id`='763';
			UPDATE {{attributeclass}} SET `label`='Отправьте запоненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='661';
		";
	}
}