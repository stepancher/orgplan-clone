<?php

class m161103_122835_copy_fair_statistic_fields_to_fairinfo extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_fair_statistics_to_fairinfo`()
            BEGIN
            
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE squareGross INT DEFAULT NULL;
                DECLARE squareNet INT DEFAULT NULL;
                DECLARE members INT DEFAULT NULL;
                DECLARE visitors INT DEFAULT NULL;
                DECLARE statistics TEXT DEFAULT NULL;
                DECLARE statisticsDate DATE DEFAULT NULL;
                DECLARE fairInfoId INT DEFAULT NULL;
                DECLARE copy CURSOR FOR SELECT 
                        f.squareGross,
                        f.squareNet,
                        f.members,
                        f.visitors,
                        f.statistics,
                        f.statisticsDate,
                        fi.id AS fairInfoId
                    FROM {{fair}} f
                    JOIN {{fairinfo}} fi ON fi.fairId = f.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                START TRANSACTION;
                    DELETE FROM {{fairinfo}} WHERE 
                        areaClosedExhibitorsForeign IS NULL AND 
                        areaClosedExhibitorsLocal IS NULL AND 
                        areaOpenExhibitorsForeign IS NULL AND 
                        areaOpenExhibitorsLocal IS NULL AND 
                        areaSpecialExpositions IS NULL AND 
                        countriesCount IS NULL AND 
                        exhibitorsForeign IS NULL AND 
                        exhibitorsLocal IS NULL AND 
                        visitorsForeign IS NULL AND 
                        visitorsLocal IS NULL;
                COMMIT;
                
                OPEN copy;
                
                read_loop: LOOP
                    FETCH copy INTO 
                        squareGross,
                        squareNet,
                        members,
                        visitors,
                        statistics,
                        statisticsDate,
                        fairInfoId;
                
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{fairinfo}} SET `squareGross`=squareGross,`squareNet`=squareNet,`members`=members,`visitors`=visitors,
                            `statistics`=statistics,`statisticsDate`=statisticsDate WHERE `id` = fairInfoId;
                    COMMIT;
                    
                    END LOOP;
                CLOSE copy;
            END;
    
    CALL copy_fair_statistics_to_fairinfo();
    DROP PROCEDURE IF EXISTS `copy_fair_statistics_to_fairinfo`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}