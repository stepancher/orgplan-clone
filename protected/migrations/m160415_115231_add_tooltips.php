<?php

class m160415_115231_add_tooltips extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'591\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'593\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'596\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'598\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'600\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'602\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'604\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'606\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'618\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'623\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'625\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'627\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'630\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'632\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'634\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'636\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'638\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'640\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'646\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'648\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'651\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'653\';
			UPDATE {{attributeclass}} SET `tooltip`=\'<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\' WHERE `id`=\'656\';
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'639\';
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'637\';
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'635\';
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'633\';
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'631\';
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'629\';
			UPDATE {{attributeclass}} SET `label`=\'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. При этом Ораганизатор оставляет за собой право отказать Экспоненту в приеме заявки и предоставления услуги.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 04 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. \' WHERE `id`=\'659\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> \' WHERE `id`=\'648\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> \' WHERE `id`=\'651\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> \' WHERE `id`=\'653\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> \' WHERE `id`=\'656\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> \' WHERE `id`=\'796\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> \' WHERE `id`=\'797\';
			UPDATE {{attributeclass}} SET `label`=\'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. При этом Ораганизатор оставляет за собой право отказать Экспоненту в приеме заявки и предоставления услуги.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 16:00 08 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. \' WHERE `id`=\'659\';
			UPDATE {{attributeclass}} SET `label`=\'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. При этом Ораганизатор оставляет за собой право отказать Экспоненту в приеме заявки и предоставления услуги.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 16:00 08 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. \' WHERE `id`=\'610\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>\' WHERE `id`=\'606\';
			UPDATE {{attributeclassproperty}} SET `value`=\'179\' WHERE `id`=\'788\';
	    ';
	}
}