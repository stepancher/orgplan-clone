<?php

class m160517_111956_delete_repeat_trfairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{trfair}} WHERE `id`='117';
			DELETE FROM {{trfair}} WHERE `id`='876';
			DELETE FROM {{trfair}} WHERE `id`='112';
			DELETE FROM {{trfair}} WHERE `id`='872';
			DELETE FROM {{trfair}} WHERE `id`='110';
			DELETE FROM {{trfair}} WHERE `id`='870';
			DELETE FROM {{trfair}} WHERE `id`='109';
			DELETE FROM {{trfair}} WHERE `id`='869';
			DELETE FROM {{trfair}} WHERE `id`='108';
			DELETE FROM {{trfair}} WHERE `id`='868';
			DELETE FROM {{trfair}} WHERE `id`='107';
			DELETE FROM {{trfair}} WHERE `id`='867';
			DELETE FROM {{trfair}} WHERE `id`='86';
			DELETE FROM {{trfair}} WHERE `id`='846';
			DELETE FROM {{trfair}} WHERE `id`='84';
			DELETE FROM {{trfair}} WHERE `id`='844';
			DELETE FROM {{trfair}} WHERE `id`='101';
			DELETE FROM {{trfair}} WHERE `id`='861';
			DELETE FROM {{trfair}} WHERE `id`='100';
			DELETE FROM {{trfair}} WHERE `id`='860';
			DELETE FROM {{trfair}} WHERE `id`='33';
			DELETE FROM {{trfair}} WHERE `id`='795';
			DELETE FROM {{trfair}} WHERE `id`='97';
			DELETE FROM {{trfair}} WHERE `id`='857';
			DELETE FROM {{trfair}} WHERE `id`='85';
			DELETE FROM {{trfair}} WHERE `id`='845';
			DELETE FROM {{trfair}} WHERE `id`='32';
			DELETE FROM {{trfair}} WHERE `id`='794';
			DELETE FROM {{trfair}} WHERE `id`='120';
			DELETE FROM {{trfair}} WHERE `id`='878';
			DELETE FROM {{trfair}} WHERE `id`='94';
			DELETE FROM {{trfair}} WHERE `id`='854';
			DELETE FROM {{trfair}} WHERE `id`='114';
			DELETE FROM {{trfair}} WHERE `id`='874';
			DELETE FROM {{trfair}} WHERE `id`='135';
			DELETE FROM {{trfair}} WHERE `id`='893';
			DELETE FROM {{trfair}} WHERE `id`='76';
			DELETE FROM {{trfair}} WHERE `id`='838';
			DELETE FROM {{trfair}} WHERE `id`='136';
			DELETE FROM {{trfair}} WHERE `id`='894';
			DELETE FROM {{trfair}} WHERE `id`='104';
			DELETE FROM {{trfair}} WHERE `id`='864';
			DELETE FROM {{trfair}} WHERE `id`='102';
			DELETE FROM {{trfair}} WHERE `id`='862';
			DELETE FROM {{trfair}} WHERE `id`='95';
			DELETE FROM {{trfair}} WHERE `id`='855';
			DELETE FROM {{trfair}} WHERE `id`='80';
			DELETE FROM {{trfair}} WHERE `id`='842';
			DELETE FROM {{trfair}} WHERE `id`='81';
			DELETE FROM {{trfair}} WHERE `id`='843';
			DELETE FROM {{trfair}} WHERE `id`='115';
			DELETE FROM {{trfair}} WHERE `id`='875';
			DELETE FROM {{trfair}} WHERE `id`='106';
			DELETE FROM {{trfair}} WHERE `id`='866';
			DELETE FROM {{trfair}} WHERE `id`='130';
			DELETE FROM {{trfair}} WHERE `id`='888';
			DELETE FROM {{trfair}} WHERE `id`='129';
			DELETE FROM {{trfair}} WHERE `id`='887';
			DELETE FROM {{trfair}} WHERE `id`='87';
			DELETE FROM {{trfair}} WHERE `id`='847';
			DELETE FROM {{trfair}} WHERE `id`='28';
			DELETE FROM {{trfair}} WHERE `id`='790';
			DELETE FROM {{trfair}} WHERE `id`='74';
			DELETE FROM {{trfair}} WHERE `id`='836';
			DELETE FROM {{trfair}} WHERE `id`='128';
			DELETE FROM {{trfair}} WHERE `id`='886';
			DELETE FROM {{trfair}} WHERE `id`='127';
			DELETE FROM {{trfair}} WHERE `id`='885';
			DELETE FROM {{trfair}} WHERE `id`='126';
			DELETE FROM {{trfair}} WHERE `id`='884';
			DELETE FROM {{trfair}} WHERE `id`='125';
			DELETE FROM {{trfair}} WHERE `id`='883';
			DELETE FROM {{trfair}} WHERE `id`='124';
			DELETE FROM {{trfair}} WHERE `id`='882';
			DELETE FROM {{trfair}} WHERE `id`='123';
			DELETE FROM {{trfair}} WHERE `id`='881';
			DELETE FROM {{trfair}} WHERE `id`='122';
			DELETE FROM {{trfair}} WHERE `id`='880';
			DELETE FROM {{trfair}} WHERE `id`='92';
			DELETE FROM {{trfair}} WHERE `id`='852';
			DELETE FROM {{trfair}} WHERE `id`='90';
			DELETE FROM {{trfair}} WHERE `id`='850';
			DELETE FROM {{trfair}} WHERE `id`='77';
			DELETE FROM {{trfair}} WHERE `id`='839';
			DELETE FROM {{trfair}} WHERE `id`='93';
			DELETE FROM {{trfair}} WHERE `id`='853';
			DELETE FROM {{trfair}} WHERE `id`='91';
			DELETE FROM {{trfair}} WHERE `id`='851';
			DELETE FROM {{trfair}} WHERE `id`='89';
			DELETE FROM {{trfair}} WHERE `id`='849';
			DELETE FROM {{trfair}} WHERE `id`='111';
			DELETE FROM {{trfair}} WHERE `id`='871';
			DELETE FROM {{trfair}} WHERE `id`='121';
			DELETE FROM {{trfair}} WHERE `id`='879';
			DELETE FROM {{trfair}} WHERE `id`='119';
			DELETE FROM {{trfair}} WHERE `id`='877';
			DELETE FROM {{trfair}} WHERE `id`='105';
			DELETE FROM {{trfair}} WHERE `id`='865';
			DELETE FROM {{trfair}} WHERE `id`='113';
			DELETE FROM {{trfair}} WHERE `id`='873';
			DELETE FROM {{trfair}} WHERE `id`='72';
			DELETE FROM {{trfair}} WHERE `id`='834';
			DELETE FROM {{trfair}} WHERE `id`='44';
			DELETE FROM {{trfair}} WHERE `id`='806';
			DELETE FROM {{trfair}} WHERE `id`='43';
			DELETE FROM {{trfair}} WHERE `id`='805';
			DELETE FROM {{trfair}} WHERE `id`='75';
			DELETE FROM {{trfair}} WHERE `id`='837';
			DELETE FROM {{trfair}} WHERE `id`='73';
			DELETE FROM {{trfair}} WHERE `id`='835';
			DELETE FROM {{trfair}} WHERE `id`='46';
			DELETE FROM {{trfair}} WHERE `id`='808';
			DELETE FROM {{trfair}} WHERE `id`='45';
			DELETE FROM {{trfair}} WHERE `id`='807';
			DELETE FROM {{trfair}} WHERE `id`='47';
			DELETE FROM {{trfair}} WHERE `id`='809';
			DELETE FROM {{trfair}} WHERE `id`='34';
			DELETE FROM {{trfair}} WHERE `id`='796';
			DELETE FROM {{trfair}} WHERE `id`='35';
			DELETE FROM {{trfair}} WHERE `id`='797';
			DELETE FROM {{trfair}} WHERE `id`='27';
			DELETE FROM {{trfair}} WHERE `id`='789';
			DELETE FROM {{trfair}} WHERE `id`='26';
			DELETE FROM {{trfair}} WHERE `id`='788';
			DELETE FROM {{trfair}} WHERE `id`='37';
			DELETE FROM {{trfair}} WHERE `id`='799';
			DELETE FROM {{trfair}} WHERE `id`='39';
			DELETE FROM {{trfair}} WHERE `id`='801';
			DELETE FROM {{trfair}} WHERE `id`='38';
			DELETE FROM {{trfair}} WHERE `id`='800';
			DELETE FROM {{trfair}} WHERE `id`='36';
			DELETE FROM {{trfair}} WHERE `id`='798';
			DELETE FROM {{trfair}} WHERE `id`='103';
			DELETE FROM {{trfair}} WHERE `id`='863';
			DELETE FROM {{trfair}} WHERE `id`='137';
			DELETE FROM {{trfair}} WHERE `id`='895';
			DELETE FROM {{trfair}} WHERE `id`='71';
			DELETE FROM {{trfair}} WHERE `id`='833';
			DELETE FROM {{trfair}} WHERE `id`='70';
			DELETE FROM {{trfair}} WHERE `id`='832';
			DELETE FROM {{trfair}} WHERE `id`='69';
			DELETE FROM {{trfair}} WHERE `id`='831';
			DELETE FROM {{trfair}} WHERE `id`='55';
			DELETE FROM {{trfair}} WHERE `id`='817';
			DELETE FROM {{trfair}} WHERE `id`='57';
			DELETE FROM {{trfair}} WHERE `id`='819';
			DELETE FROM {{trfair}} WHERE `id`='56';
			DELETE FROM {{trfair}} WHERE `id`='818';
			DELETE FROM {{trfair}} WHERE `id`='68';
			DELETE FROM {{trfair}} WHERE `id`='830';
			DELETE FROM {{trfair}} WHERE `id`='133';
			DELETE FROM {{trfair}} WHERE `id`='891';
			DELETE FROM {{trfair}} WHERE `id`='67';
			DELETE FROM {{trfair}} WHERE `id`='829';
			DELETE FROM {{trfair}} WHERE `id`='66';
			DELETE FROM {{trfair}} WHERE `id`='828';
			DELETE FROM {{trfair}} WHERE `id`='65';
			DELETE FROM {{trfair}} WHERE `id`='827';
			DELETE FROM {{trfair}} WHERE `id`='64';
			DELETE FROM {{trfair}} WHERE `id`='826';
			DELETE FROM {{trfair}} WHERE `id`='63';
			DELETE FROM {{trfair}} WHERE `id`='825';
			DELETE FROM {{trfair}} WHERE `id`='62';
			DELETE FROM {{trfair}} WHERE `id`='824';
			DELETE FROM {{trfair}} WHERE `id`='61';
			DELETE FROM {{trfair}} WHERE `id`='823';
			DELETE FROM {{trfair}} WHERE `id`='60';
			DELETE FROM {{trfair}} WHERE `id`='822';
			DELETE FROM {{trfair}} WHERE `id`='59';
			DELETE FROM {{trfair}} WHERE `id`='821';
			DELETE FROM {{trfair}} WHERE `id`='58';
			DELETE FROM {{trfair}} WHERE `id`='820';
			DELETE FROM {{trfair}} WHERE `id`='134';
			DELETE FROM {{trfair}} WHERE `id`='892';
			DELETE FROM {{trfair}} WHERE `id`='24';
			DELETE FROM {{trfair}} WHERE `id`='786';
			DELETE FROM {{trfair}} WHERE `id`='131';
			DELETE FROM {{trfair}} WHERE `id`='889';
			DELETE FROM {{trfair}} WHERE `id`='132';
			DELETE FROM {{trfair}} WHERE `id`='890';
			DELETE FROM {{trfair}} WHERE `id`='54';
			DELETE FROM {{trfair}} WHERE `id`='816';
			DELETE FROM {{trfair}} WHERE `id`='53';
			DELETE FROM {{trfair}} WHERE `id`='815';
			DELETE FROM {{trfair}} WHERE `id`='52';
			DELETE FROM {{trfair}} WHERE `id`='814';
			DELETE FROM {{trfair}} WHERE `id`='51';
			DELETE FROM {{trfair}} WHERE `id`='813';
			DELETE FROM {{trfair}} WHERE `id`='50';
			DELETE FROM {{trfair}} WHERE `id`='812';
			DELETE FROM {{trfair}} WHERE `id`='49';
			DELETE FROM {{trfair}} WHERE `id`='811';
			DELETE FROM {{trfair}} WHERE `id`='23';
			DELETE FROM {{trfair}} WHERE `id`='785';
			DELETE FROM {{trfair}} WHERE `id`='48';
			DELETE FROM {{trfair}} WHERE `id`='810';
			DELETE FROM {{trfair}} WHERE `id`='99';
			DELETE FROM {{trfair}} WHERE `id`='859';
			DELETE FROM {{trfair}} WHERE `id`='15';
			DELETE FROM {{trfair}} WHERE `id`='777';
			DELETE FROM {{trfair}} WHERE `id`='22';
			DELETE FROM {{trfair}} WHERE `id`='784';
			DELETE FROM {{trfair}} WHERE `id`='21';
			DELETE FROM {{trfair}} WHERE `id`='783';
			DELETE FROM {{trfair}} WHERE `id`='20';
			DELETE FROM {{trfair}} WHERE `id`='782';
			DELETE FROM {{trfair}} WHERE `id`='98';
			DELETE FROM {{trfair}} WHERE `id`='858';
			DELETE FROM {{trfair}} WHERE `id`='96';
			DELETE FROM {{trfair}} WHERE `id`='856';
			DELETE FROM {{trfair}} WHERE `id`='12';
			DELETE FROM {{trfair}} WHERE `id`='774';
			DELETE FROM {{trfair}} WHERE `id`='11';
			DELETE FROM {{trfair}} WHERE `id`='773';
			DELETE FROM {{trfair}} WHERE `id`='10';
			DELETE FROM {{trfair}} WHERE `id`='772';
			DELETE FROM {{trfair}} WHERE `id`='9';
			DELETE FROM {{trfair}} WHERE `id`='771';
			DELETE FROM {{trfair}} WHERE `id`='17';
			DELETE FROM {{trfair}} WHERE `id`='779';
			DELETE FROM {{trfair}} WHERE `id`='16';
			DELETE FROM {{trfair}} WHERE `id`='778';
			DELETE FROM {{trfair}} WHERE `id`='14';
			DELETE FROM {{trfair}} WHERE `id`='776';
			DELETE FROM {{trfair}} WHERE `id`='13';
			DELETE FROM {{trfair}} WHERE `id`='775';
			DELETE FROM {{trfair}} WHERE `id`='8';
			DELETE FROM {{trfair}} WHERE `id`='770';
			DELETE FROM {{trfair}} WHERE `id`='79';
			DELETE FROM {{trfair}} WHERE `id`='841';
			DELETE FROM {{trfair}} WHERE `id`='78';
			DELETE FROM {{trfair}} WHERE `id`='840';
			DELETE FROM {{trfair}} WHERE `id`='42';
			DELETE FROM {{trfair}} WHERE `id`='804';
			DELETE FROM {{trfair}} WHERE `id`='41';
			DELETE FROM {{trfair}} WHERE `id`='803';
			DELETE FROM {{trfair}} WHERE `id`='40';
			DELETE FROM {{trfair}} WHERE `id`='802';
			DELETE FROM {{trfair}} WHERE `id`='29';
			DELETE FROM {{trfair}} WHERE `id`='791';
			DELETE FROM {{trfair}} WHERE `id`='18';
			DELETE FROM {{trfair}} WHERE `id`='780';
			DELETE FROM {{trfair}} WHERE `id`='19';
			DELETE FROM {{trfair}} WHERE `id`='781';
			DELETE FROM {{trfair}} WHERE `id`='31';
			DELETE FROM {{trfair}} WHERE `id`='793';
			DELETE FROM {{trfair}} WHERE `id`='30';
			DELETE FROM {{trfair}} WHERE `id`='792';
			DELETE FROM {{trfair}} WHERE `id`='7';
			DELETE FROM {{trfair}} WHERE `id`='769';
			DELETE FROM {{trfair}} WHERE `id`='6';
			DELETE FROM {{trfair}} WHERE `id`='768';
			DELETE FROM {{trfair}} WHERE `id`='5';
			DELETE FROM {{trfair}} WHERE `id`='767';
			DELETE FROM {{trfair}} WHERE `id`='4';
			DELETE FROM {{trfair}} WHERE `id`='766';
			DELETE FROM {{trfair}} WHERE `id`='3';
			DELETE FROM {{trfair}} WHERE `id`='765';
			DELETE FROM {{trfair}} WHERE `id`='1';
			DELETE FROM {{trfair}} WHERE `id`='763';
			DELETE FROM {{trfair}} WHERE `id`='2';
			DELETE FROM {{trfair}} WHERE `id`='764';
			DELETE FROM {{trfair}} WHERE `id`='651';
			DELETE FROM {{trfair}} WHERE `id`='896';
		";
	}
}