<?php

class m161216_135809_add_short_url_to_tag extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{tag}} 
            ADD COLUMN `shortUrl` VARCHAR(1024) NULL DEFAULT NULL AFTER `name`;
            
            UPDATE {{tag}} SET `shortUrl`='kak-uchastvovat-v-vystavkakh' WHERE `id`='1';
            UPDATE {{tag}} SET `shortUrl`='zastrojka-vystavochnykh-stendov' WHERE `id`='45';
            UPDATE {{tag}} SET `shortUrl`='organizaciya-raboty-stenda' WHERE `id`='73';
            UPDATE {{tag}} SET `shortUrl`='kak-rabotat-na-vystavke' WHERE `id`='74';
            UPDATE {{tag}} SET `shortUrl`='pr' WHERE `id`='75';
            UPDATE {{tag}} SET `shortUrl`='smeta-zatrat-po-vystavke' WHERE `id`='77';
            UPDATE {{tag}} SET `shortUrl`='protoplan' WHERE `id`='79';
            UPDATE {{tag}} SET `shortUrl`='protoplan' WHERE `id`='80';
            
            UPDATE {{blog}} SET `title`='Blog | Protoplan' WHERE `id`='87';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}