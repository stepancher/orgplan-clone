<?php

class m161002_195739_create_update_trcft extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Составьте чек-лист самого необходимого для работы стенда' WHERE `id`='1847';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Составьте чек-лист самого необходимого для работы стенда' WHERE `id`='1818';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Составьте список посетителей стенда после выставки' WHERE `id`='1832';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Разработайте программу продвижение вашего участия и выбирите каналы коммуникации с посетителями до и во время выставки' WHERE `id`='1824';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Разработайте программу продвижение вашего участия и выбирите каналы коммуникации с посетителями до и во время выставки' WHERE `id`='1853';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Закажите пропуски/бейджи для сотрудников и пропуски для транспорта на выставку' WHERE `id`='1812';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Закажите пропуски/бейджи для сотрудников и пропуски для транспорта на выставку' WHERE `id`='1841';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}