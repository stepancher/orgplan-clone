<?php

class m170222_071128_add_pushkin_city_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('27', 'pushkin');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1530', 'ru', 'Пушкин');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1530', 'en', 'Pushkin');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1530', 'de', 'Puschkin');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}