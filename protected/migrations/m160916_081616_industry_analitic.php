<?php

class m160916_081616_industry_analitic extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE IF NOT EXISTS {{charttablegroup}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `industryId` INT(11) NULL DEFAULT NULL,
              `color` VARCHAR(45) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Основные показатели отрасли', '11');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Показатели производства', '11');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('ЭКСПОРТ/ИМПОРТ', '11');

            ALTER TABLE {{charttable}} 
            ADD COLUMN `groupId` INT(11) NULL DEFAULT NULL AFTER `area`,
            ADD COLUMN `position` INT(11) NULL DEFAULT NULL AFTER `groupId`;
            
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`) VALUES ('Пиломатериалы', '3', '5', 'state');
            
            UPDATE {{charttable}} SET `groupId`='1' WHERE `id`='3';
            UPDATE {{charttable}} SET `groupId`='1' WHERE `id`='4';
            UPDATE {{charttable}} SET `groupId`='1' WHERE `id`='5';
            UPDATE {{charttable}} SET `groupId`='1' WHERE `id`='6';
            UPDATE {{charttable}} SET `groupId`='2' WHERE `id`='7';
            UPDATE {{charttable}} SET `groupId`='2' WHERE `id`='8';
            UPDATE {{charttable}} SET `groupId`='3' WHERE `id`='9';
            UPDATE {{charttable}} SET `groupId`='2' WHERE `id`='10';
            UPDATE {{charttable}} SET `groupId`='2' WHERE `id`='11';
            UPDATE {{charttable}} SET `position`='1' WHERE `id`='3';
            UPDATE {{charttable}} SET `position`='2' WHERE `id`='4';
            UPDATE {{charttable}} SET `position`='3' WHERE `id`='5';
            UPDATE {{charttable}} SET `position`='4' WHERE `id`='6';
            UPDATE {{charttable}} SET `position`='1' WHERE `id`='7';
            UPDATE {{charttable}} SET `position`='2' WHERE `id`='8';
            UPDATE {{charttable}} SET `position`='1' WHERE `id`='9';
            UPDATE {{charttable}} SET `position`='3' WHERE `id`='10';
            UPDATE {{charttable}} SET `position`='4' WHERE `id`='11';
            UPDATE {{charttable}} SET `position`='1' WHERE `id`='71';
            UPDATE {{charttable}} SET `position`='2' WHERE `id`='76';
            UPDATE {{charttable}} SET `position`='4' WHERE `id`='72';
            UPDATE {{charttable}} SET `position`='1' WHERE `id`='73';
            UPDATE {{charttable}} SET `position`='3' WHERE `id`='74';
            UPDATE {{charttable}} SET `position`='4' WHERE `id`='75';
            UPDATE {{charttable}} SET `groupId`='4' WHERE `id`='71';
            UPDATE {{charttable}} SET `groupId`='4' WHERE `id`='72';
            UPDATE {{charttable}} SET `groupId`='4' WHERE `id`='76';
            UPDATE {{charttable}} SET `groupId`='5' WHERE `id`='73';
            UPDATE {{charttable}} SET `groupId`='5' WHERE `id`='74';
            UPDATE {{charttable}} SET `groupId`='5' WHERE `id`='75';
            UPDATE {{charttable}} SET `groupId`='4', `position`='3' WHERE `id`='78';

            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Таблицы по федеральным округам (ФО):', '5');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Таблицы по субъектам федерации (СФ)', '5');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Таможенная справка', '5');

            UPDATE {{charttable}} SET `name`='Экспорт\\Импорт древесины и целлюлозы' WHERE `id`='72';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли (ФО)' WHERE `id`='71';
            UPDATE {{charttable}} SET `name`='Показатели производства (ФО)' WHERE `id`='76';
            UPDATE {{charttable}} SET `name`='Пиломатериалы (ФО)' WHERE `id`='78';
            UPDATE {{charttable}} SET `name`='Пиломатериалы (ФО)' WHERE `id`='74';

            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('5784b789bd4c56dc4f8b6f7e', '', '', NULL, '4', '2015', '78', '#43ade3', '1', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('5784b789bd4c56dc4f8b6f7e', 'Вывоз (продажа) пиломатериалов', '', NULL, NULL, '2015', '78', '#a0d468', '2', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('5784b781bd4c56dc4f8b5d56', '', '', NULL, '4', '2015', '78', '#4ec882', '3', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('5784b781bd4c56dc4f8b5d56', 'Ввоз (покупка) пиломатериалов', '', NULL, NULL, '2015', '78', '#a0d468', '4', NULL);
            UPDATE {{charttablegroup}} SET `name`='Таблицы по субъектам федерации (СФ):' WHERE `id`='5';
            UPDATE {{charttable}} SET `name`='Экспорт\\Импорт древесины и целлюлозы' WHERE `id`='72';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли (СФ)' WHERE `id`='73';
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Показатели производства (СФ)', '3', '5', 'region', '5', '2');

            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('571e46d877d89702498d0936', '', '', NULL, '4', '2015', '79', '#4ec882', '1', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('571e46d877d89702498d0936', 'Производство фанеры', '', NULL, NULL, '2015', '79', '#a0d468', '2', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('571e44c477d89702498bfc67', '', '', NULL, '4', '2015', '79', '#43ade3', '3', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('571e44c477d89702498bfc67', 'Производство лесоматериалов', '', NULL, NULL, '2015', '79', '#a0d468', '4', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('571e46e877d89702498d12eb', '', '', NULL, '4', '2015', '79', '#43ade3', '5', NULL);
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `parent`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`) VALUES ('571e46e877d89702498d12eb', 'Производство целлюлозы', '', NULL, NULL, '2015', '79', '#a0d468', '6', NULL);

            UPDATE {{charttable}} SET `name`='Пиломатериалы (СФ)' WHERE `id`='74';
            UPDATE {{charttable}} SET `name`='«Экспорт\\Импорт древесины и целлюлозы»' WHERE `id`='75';
            
            UPDATE {{charttablegroup}} SET `name`='Основные показатели отрасли (ФО)' WHERE `id`='4';
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Показатели производства (ФО)', '5');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Пиломатериалы', '5');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`) VALUES ('Экспорт\\Импорт (ФО)', '5');
            DELETE FROM {{charttablegroup}} WHERE `id`='5';
            
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по округам', `groupId`='4', `position`='1' WHERE `id`='71';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по регионам', `groupId`='4', `position`='2' WHERE `id`='73';
            UPDATE {{charttable}} SET `name`='Экспорт\\Импорт древесины и целлюлозы по округам', `groupId`='13', `position`='1' WHERE `id`='72';
            UPDATE {{charttable}} SET `name`='Пиломатериал', `groupId`='12', `position`='1' WHERE `id`='74';
            UPDATE {{charttable}} SET `name`='Экспорт\\Импорт древесины и целлюлозы по регионам', `groupId`='13', `position`='2' WHERE `id`='75';
            UPDATE {{charttable}} SET `name`='Показатели производства', `groupId`='11', `position`='1' WHERE `id`='76';
            DELETE FROM {{charttable}} WHERE `id`='78';
            DELETE FROM {{charttable}} WHERE `id`='79';
            
            ALTER TABLE {{charttablegroup}} 
            ADD COLUMN `position` INT(11) NULL DEFAULT NULL AFTER `color`;

            UPDATE {{charttablegroup}} SET `position`='1' WHERE `id`='4';
            UPDATE {{charttablegroup}} SET `position`='2' WHERE `id`='11';
            UPDATE {{charttablegroup}} SET `position`='3' WHERE `id`='12';
            UPDATE {{charttablegroup}} SET `position`='4' WHERE `id`='13';
            UPDATE {{charttablegroup}} SET `position`='5' WHERE `id`='6';

            UPDATE {{charttablegroup}} SET `position`='1' WHERE `id`='1';
            UPDATE {{charttablegroup}} SET `position`='2' WHERE `id`='2';
            UPDATE {{charttablegroup}} SET `position`='3' WHERE `id`='3';
            
            UPDATE {{charttable}} SET `name`='Основные сельскохозяйственные культуры' WHERE `id`='7';
            UPDATE {{charttable}} SET `name`='Основная продукция животноводства' WHERE `id`='8';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем и доля отраслей сельского хозяйства в КФХ и ИП' WHERE `id`='6';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем и доля отраслей сельского хозяйства в с/х организациях' WHERE `id`='5';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем сельского хозяйства' WHERE `id`='4';
            UPDATE {{charttable}} SET `name`='Объем и доля отраслей сельского хозяйства' WHERE `id`='3';
            UPDATE {{charttable}} SET `name`='Экспорт / Импорт сельскохозяйственной продукции' WHERE `id`='9';
            
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Основные показатели отрасли (ФО) – экономические характеристики', '3', '1');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Показатели производства (ФО)', '3', '2');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Объем ресурсов (ФО)', '3', '3');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Экспорт/Импорт', '3', '4');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Таможенная справка', '3', '5');

            UPDATE {{charttable}} SET `name`='Рентабельность тяжелой промышленности', `groupId`='14', `position`='1' WHERE `id`='60';
            UPDATE {{charttable}} SET `name`='Рентабельность тяжелой промышленности', `groupId`='14', `position`='2' WHERE `id`='62';
            UPDATE {{charttable}} SET `name`='Товарный объем тяжелой промышленности', `groupId`='15', `position`='1' WHERE `id`='59';
            UPDATE {{charttable}} SET `name`='Объем геологоразведывательных работ по регионам', `groupId`='16', `position`='2' WHERE `id`='63';
            UPDATE {{charttable}} SET `name`='Объем геологоразведывательных работ по округам', `groupId`='16', `position`='1' WHERE `id`='61';
            UPDATE {{charttable}} SET `name`='Экспорт/Импорт металлов и стальных труб', `groupId`='17', `position`='1' WHERE `id`='64';

            ALTER TABLE {{gstattypes}} 
            ADD COLUMN `autoUpdate` INT(11) NULL DEFAULT NULL AFTER `unit`;

            ALTER TABLE {{gobjects}} 
            CHANGE COLUMN `gId` `gId` VARCHAR(45) NULL DEFAULT NULL ;
            
            INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('80', 'ТН ВЭД', '3', '11', 'country', '3', '2');

            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('500009', 'Экспорт РФ', '2015', '80', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('500011', 'Импорт РФ', '2015', '80', '#a0d468', '2');
              
             CREATE TABLE {{gstatgroup}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `objId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
             
             ALTER TABLE {{gstatgroup}} 
            ADD COLUMN `name` VARCHAR(255) NULL DEFAULT NULL AFTER `objId`;
            
            ALTER TABLE {{gstatgroup}} 
            ADD COLUMN `position` INT(11) NULL DEFAULT NULL AFTER `name`;

            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`) VALUES ('276', 'Машины сельскохозяйственные', '1');
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`) VALUES ('276', 'Машины или механизмы для уборки или обмолота', '2');
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`) VALUES ('276', 'Установки и аппараты доильные', '3');
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`) VALUES ('276', 'Оборудование для виноделия', '4');
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`) VALUES ('276', 'Машины для очистки, сортировки или калибровки семян', '5');


            CREATE TABLE {{gstatgrouphasstat}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `statGroupId` INT(11) NULL DEFAULT NULL,
              `gObjectHasGStatTypeId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('1', '44816');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('1', '44817');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('2', '45018');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('2', '45019');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('3', '45112');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('3', '45113');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('4', '45162');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('4', '45163');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('5', '45284');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('5', '45285');
          
          CREATE TABLE {{gstattypegroup}} (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `groupId` INT(11) NULL DEFAULT NULL,
          `gId` INT(11) NULL DEFAULT NULL,
          PRIMARY KEY (`id`));
          
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('1', '500009');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('1', '500013');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('1', '500017');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('1', '500021');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('1', '500025');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('2', '500011');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('2', '500015');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('2', '500019');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('2', '500023');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`) VALUES ('2', '500027');
          
          UPDATE {{chartcolumn}} SET `statId`='1' WHERE `id`='379';
            UPDATE {{chartcolumn}} SET `statId`='2' WHERE `id`='380';

        ALTER TABLE {{gstattypegroup}} 
        ADD COLUMN `unit` VARCHAR(45) NULL DEFAULT NULL AFTER `gId`;
        
        UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='1';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='2';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='3';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='4';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='5';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='6';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='7';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='8';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='9';
            UPDATE {{gstattypegroup}} SET `unit`='Доллар США' WHERE `id`='10';

            ALTER TABLE {{gstattypes}} 
            CHANGE COLUMN `name` `name` VARCHAR(2000) NULL DEFAULT NULL ;
		
		UPDATE {{gstatgroup}} SET `objId`='379' WHERE `id`='5';
        UPDATE {{gstatgroup}} SET `objId`='379' WHERE `id`='4';
        UPDATE {{gstatgroup}} SET `objId`='379' WHERE `id`='3';
        UPDATE {{gstatgroup}} SET `objId`='379' WHERE `id`='2';
        UPDATE {{gstatgroup}} SET `objId`='379' WHERE `id`='1';
        
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59221' WHERE `id`='1';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59222' WHERE `id`='2';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59223' WHERE `id`='3';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59224' WHERE `id`='4';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59225' WHERE `id`='6';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='' WHERE `id`='5';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59226' WHERE `id`='7';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59227' WHERE `id`='8';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59228' WHERE `id`='9';
        UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='59229' WHERE `id`='10';
		
		DELETE FROM {{gobjects}};
		DELETE FROM {{gobjectshasgstattypes}};
		DELETE FROM {{gstattypes}};
		DELETE FROM {{gvalue}};
		
		CREATE TABLE {{newgstattypes}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `gId` VARCHAR(255) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `parentName` VARCHAR(255) NULL DEFAULT NULL,
              `type` VARCHAR(45) NULL DEFAULT NULL,
              `description` VARCHAR(2000) NULL DEFAULT NULL,
              `unit` VARCHAR(45) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}