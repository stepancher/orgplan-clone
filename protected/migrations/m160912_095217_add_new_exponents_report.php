<?php

class m160912_095217_add_new_exponents_report extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        list($peace1, $peace2, $dbProposal) = explode('=', Yii::app()->dbProposal->connectionString);

        return "
            INSERT INTO {$dbProposal}.`tbl_report` (`reportName`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('report-6', 'Отчёт по Экспонентам', '[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]', '[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\"}]');
            UPDATE {$dbProposal}.`tbl_report` SET `reportId`='6' WHERE `id`='6';
            UPDATE {$dbProposal}.`tbl_report` SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"bill\",\"displayName\": \"Номер договора\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"phone\",\"displayName\": \"Телефон\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"post\",\"displayName\": \"Должность\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"fullName\",\"displayName\": \"ФИО\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]', `exportColumns`='[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"bill\":\"Номер договора\",\"phone\":\"Телефон\",\"post\":\"Должность\",\"fullName\":\"ФИО\"}]' WHERE `id`='6';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}