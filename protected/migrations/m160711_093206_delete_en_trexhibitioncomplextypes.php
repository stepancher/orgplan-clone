<?php

class m160711_093206_delete_en_trexhibitioncomplextypes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			DELETE FROM {{trexhibitioncomplextype}} WHERE langId = 'en' AND name in (
			'Деловые, торгово-развлекательные центры',
			'Спортивные, научные, культурные центры',
			'Гостиницы, конгресс-холлы',
			'Выставочные центры',
			'Театры, музеи, галереи',
			'Специализированные площадки'
			);
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}