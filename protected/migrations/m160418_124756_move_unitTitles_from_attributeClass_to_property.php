<?php

class m160418_124756_move_unitTitles_from_attributeClass_to_property extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='140';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='141';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='148';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='149';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='150';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='151';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='152';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='153';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='246';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='248';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='250';
			UPDATE {{attributeclass}} SET `unitTitle`=NULL WHERE `id`='252';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('140','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('141','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('148','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('149','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('150','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('151','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('152','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('153','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('246','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('248','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('250','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('252','шт.', 'unitTitle');
	    ";
	}
}