<?php

class m160803_070745_add_table_fairinfo extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			CREATE TABLE {{fairinfo}} (
			  `id` INT(11) NOT NULL,
			  `fairId` INT(11) NULL DEFAULT NULL,
			  `exhibitorsLocal` INT(11) NULL DEFAULT NULL,
			  `exhibitorsLocalWithRentArea` INT(11) NULL DEFAULT NULL,
			  `exhibitorsForeign` INT(11) NULL DEFAULT NULL,
			  `exhibitorsForeignWithRentArea` INT(11) NULL DEFAULT NULL,
			  `countriesCount` INT(11) NULL DEFAULT NULL,
			  `areaClosedExhibitorsLocal` VARCHAR(255) NULL DEFAULT NULL,
			  `areaClosedExhibitorsForeign` VARCHAR(255) NULL DEFAULT NULL,
			  `areaOpenExhibitorsLocal` VARCHAR(255) NULL DEFAULT NULL,
			  `areaOpenExhibitorsForeign` VARCHAR(255) NULL DEFAULT NULL,
			  `areaSpecialExpositions` VARCHAR(255) NULL DEFAULT NULL,
			  `visitorsLocal` INT(11) NULL DEFAULT NULL,
			  `visitorsForeign` INT(11) NULL DEFAULT NULL,
			  PRIMARY KEY (`id`));

			ALTER TABLE {{fairinfo}} 
			ADD UNIQUE INDEX `fair_uniq` (`fairId` ASC);

			ALTER TABLE {{fairinfo}} 
			ADD CONSTRAINT `fk_fair`
			  FOREIGN KEY (`fairId`)
			  REFERENCES {{fair}} (`id`)
			  ON DELETE CASCADE
			  ON UPDATE CASCADE;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}