<?php

class m161028_064110_copy_proposal_2_to_another_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
        INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('4', 'de', 'Antragsformular Nr. 2', 'STANDARD-MESSESTAND');

		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('261', 'de', 'Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('262', 'de', 'Тип стандартной застройки:');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('263', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('264', 'de', 'Классик (от 9 до 36 м2)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('265', 'de', 'Премиум (от 9 до 54 м2)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('266', 'de', 'stand_standard.png');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('267', 'de', 'Стены по периметру');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('268', 'de', 'Ковровое покрытие (цвет черный)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('269', 'de', 'Изображение стенда является приблизительным, реальная конфигурация зависит от заказанной площади.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('274', 'de', 'Комплектация');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('281', 'de', 'Фриз, наименование компании (до 15 знаков)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('282', 'de', 'Стул (код 300) 3 шт.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('283', 'de', 'Стол (код 310 или 314)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('284', 'de', 'Архивный шкаф (код 317)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('285', 'de', 'Спот-бра (код 510) 1 шт. на 9м <sup>2</sup>');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('286', 'de', 'Розетка (код 504а) 1кВт');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('287', 'de', 'Корзина для мусора (код 377)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('288', 'de', 'stand_premium.png');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('289', 'de', 'Изображение стенда является приблизительным, реальная конфигурация зависит от заказанной площади.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('290', 'de', 'Комплектация');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('291', 'de', 'Стены по периметру');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('292', 'de', 'ковровое покрытие (цвет черный)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('293', 'de', 'Фриз, наименование компании (до 15 знаков)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('294', 'de', 'Стул (код 300) 3 шт.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('295', 'de', 'Стул барный (код 306) 1 шт. на 18м<sup>2</sup>');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('296', 'de', 'Стол (код 310 или 314)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('297', 'de', 'Архивный шкаф (код 317)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('298', 'de', 'Вешалка консольная (код 331)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('299', 'de', 'Спот-бра (код 510) 1 шт. на 9м<sup>2</sup>');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('300', 'de', 'Розетка (код 504а) 1кВт');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('301', 'de', 'Офисная часть:');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('302', 'de', 'дверь с замком (код 240а)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('303', 'de', 'стеновые панели ( код 220) 1шт. на 9 м<sup>2</sup>');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('304', 'de', 'Корзина для мусора (код 377)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `tooltip`) VALUES ('305', 'de', 'Площадь застройки', 'м2', '<div class=\"hint - header\">РАЗМЕР СТЕНДА</div> Стоимость услуг  увеличивается:<br/> \nна 50% при заказе после 25.08.16<br/>\nна 100% при заказе после 23.09.16');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('307', 'de', 'Площадь застройки', '<div class=\"hint - header\">РАЗМЕР СТЕНДА</div> Стоимость услуг  увеличивается:<br/> \nна 50% при заказе после 25.08.16<br/>\nна 100% при заказе после 23.09.16');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('308', 'de', 'Желаемый размер стенда:');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`) VALUES ('309', 'de', 'Длина', 'м');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`) VALUES ('310', 'de', 'Ширина', 'м');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('311', 'de', 'Если общая площадь Вашего стенда больше\nзаказанной стандартной застройки, необходимо\nдополнительно заказать ковровое покрытие\nна оставшуюся площадь (Заявка No3).');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('306', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('312', 'de', 'План стенда:');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('313', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('314', 'de', 'Если Вам необходимо больше места, начертите план на отдельном листе и приложите к этой заявке.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2001', 'de', 'Загрузите план стенда. При необходимости можете скачать бланк.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2002', 'de', 'Скачать бланк \"План Стенда\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('315', 'de', 'Дополнительное оборудование:');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('316', 'de', 'Схему оборудования необходимо отобразить в печатной версии заявки.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('317', 'de', 'Фризовая надпись:');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`, `tooltip`) VALUES ('318', 'de', 'Надпись', 'До 15 знаков бесплатно', '<div class=\"hint - header\">Стоимость услуг</div> Стоимость услугувеличивается:<br/> на 50% при заказе после 25.08.16<br/>на 100% при заказепосле 23.09.16');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('320', 'de', 'Разместить логотип на фризовой панели');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('321', 'de', 'Логотипы принимаются только в электронном виде, стоимость изготовления зависит от сложности логотипа.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('322', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `unitTitle`, `tooltip`) VALUES ('513', 'de', 'шт.', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> Стоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('514', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('515', 'de', 'Все цены включают НДС 18%. Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента.<br><br>');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('516', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('517', 'de', 'Заявка является приложением\nк Договору на участие в выставке и\nдолжна быть заполнена в двух экземплярах.\n<br/>\n<br/>\nПодписывая настоящую заявку Экспонент\nподтверждает согласие с Правилами выставки\nи гарантирует оплату заказанных услуг.\n<br/>\n<br/>\nСтоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.\n<br/>\n<br/>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 7 октября 2016 г.\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги,\nуказанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('839', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('840', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('841', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('842', 'de', 'Экспонент');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('843', 'de', 'должность');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('844', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('845', 'de', 'ФИО');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('846', 'de', 'Организатор');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('847', 'de', 'должность');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('848', 'de');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('849', 'de', 'ФИО');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('850', 'de', 'подпись');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('851', 'de', 'подпись');
           
        UPDATE {{trattributeclass}} SET `label`='Senden Sie bitte das ausgefüllte Antragsformular  bis spätestens 21. Oktober 2017 an die Messeleitung. Fax: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='3155';
        UPDATE {{trattributeclass}} SET `label`='Standardbebauungstyp:' WHERE `id`='3156';
        UPDATE {{trattributeclass}} SET `label`='Classic (9 bis 36 m2)' WHERE `id`='3158';
        UPDATE {{trattributeclass}} SET `label`='Premium (9 bis 54 m2)' WHERE `id`='3159';
        UPDATE {{trattributeclass}} SET `label`='Standwände umlaufend' WHERE `id`='3161';
        UPDATE {{trattributeclass}} SET `label`='Teppichbelag (Farbe schwarz)' WHERE `id`='3162';
        UPDATE {{trattributeclass}} SET `label`='Der Standplan ist vorläufig, der tatsächliche Aufbau hängt von der bestellten Fläche ab.' WHERE `id`='3163';
        UPDATE {{trattributeclass}} SET `label`='Ausstattung' WHERE `id`='3164';
        UPDATE {{trattributeclass}} SET `label`='Blendenbeschriftung (max. 15 Zeichen)' WHERE `id`='3165';
        UPDATE {{trattributeclass}} SET `label`='Stuhl (Code 300) 3 Stk.' WHERE `id`='3166';
        UPDATE {{trattributeclass}} SET `label`='Tisch (Code 310 oder 314)' WHERE `id`='3167';
        UPDATE {{trattributeclass}} SET `label`='Archivschrank (Code 317)' WHERE `id`='3168';
        UPDATE {{trattributeclass}} SET `label`='Spot-Wandleuchter (Code 510) 1 Stk. für  9m <sup>2</sup>' WHERE `id`='3169';
        UPDATE {{trattributeclass}} SET `label`='Steckdose (Code 504a) 1 kW' WHERE `id`='3170';
        UPDATE {{trattributeclass}} SET `label`='Müllkorb (Code 377)' WHERE `id`='3171';
        UPDATE {{trattributeclass}} SET `label`='Der Standplan ist vorläufig, der tatsächliche Aufbau hängt von der bestellten Fläche ab.' WHERE `id`='3173';
        UPDATE {{trattributeclass}} SET `label`='Ausstattung' WHERE `id`='3174';
        UPDATE {{trattributeclass}} SET `label`='Standwände umlaufend' WHERE `id`='3175';
        UPDATE {{trattributeclass}} SET `label`='Teppichbelag (Farbe schwarz)' WHERE `id`='3176';
        UPDATE {{trattributeclass}} SET `label`='Blendenbeschriftung (max. 15 Zeichen)' WHERE `id`='3177';
        UPDATE {{trattributeclass}} SET `label`='Stuhl (Code 300) 3 Stk.' WHERE `id`='3178';
        UPDATE {{trattributeclass}} SET `label`='Barhocker (Code 306) 1 Stk. für 18 m<sup>2</sup>' WHERE `id`='3179';
        UPDATE {{trattributeclass}} SET `label`='Tisch (Code 310 oder 314)' WHERE `id`='3180';
        UPDATE {{trattributeclass}} SET `label`='Archivschrank (Code 317)' WHERE `id`='3181';
        UPDATE {{trattributeclass}} SET `label`='Hakenleiste (Code 331)' WHERE `id`='3182';
        UPDATE {{trattributeclass}} SET `label`='Spot-Wandleuchter (Code 510) 1 Stk. für  9m<sup>2</sup>' WHERE `id`='3183';
        UPDATE {{trattributeclass}} SET `label`='Steckdose (Code 504a) 1 kW' WHERE `id`='3184';
        UPDATE {{trattributeclass}} SET `label`='Bürobereich:' WHERE `id`='3185';
        UPDATE {{trattributeclass}} SET `label`='Wandplatten (Code 220) 1 Stk.für 9m<sup>2</sup>' WHERE `id`='3187';
        UPDATE {{trattributeclass}} SET `label`='abschliessbare Tür (Code 240a)' WHERE `id`='3186';
        UPDATE {{trattributeclass}} SET `label`='Müllkorb (Code 377)' WHERE `id`='3188';
        UPDATE {{trattributeclass}} SET `label`='Bebauungsfläche' WHERE `id`='3189';
        UPDATE {{trattributeclass}} SET `label`='Bebauungsfläche' WHERE `id`='3190';
        UPDATE {{trattributeclass}} SET `label`='Gewünschte Standgröße:' WHERE `id`='3191';
        UPDATE {{trattributeclass}} SET `label`='Länge' WHERE `id`='3192';
        UPDATE {{trattributeclass}} SET `label`='Breite' WHERE `id`='3193';
        UPDATE {{trattributeclass}} SET `label`='Sollte die Fläche Ihres Standes die Fläche des Standard-Messestandes überschreiten, bestellen Sie bitte den Teppichbelag für die zusätzliche Fläche extra (Antragsformular Nr.3).' WHERE `id`='3194';
        UPDATE {{trattributeclass}} SET `label`='Standplan:' WHERE `id`='3196';
        UPDATE {{trattributeclass}} SET `label`='Sollten Sie mehr Fläche benötigen, legen Sie bitte dem Antragsformular einen separaten Standplan bei.' WHERE `id`='3198';
        UPDATE {{trattributeclass}} SET `label`='Standentwurf hochladen. Bei Bedarf können Sie das Formular herunterladen.' WHERE `id`='3199';
        UPDATE {{trattributeclass}} SET `label`='Formular „Standentwurf“ herunterladen' WHERE `id`='3200';
        UPDATE {{trattributeclass}} SET `label`='Zusatzausstattung:' WHERE `id`='3201';
        UPDATE {{trattributeclass}} SET `label`='Die räumliche Aufteilung der Ausstatung ist in der ausgedruckten Version des Antrags darzustellen.' WHERE `id`='3202';
        UPDATE {{trattributeclass}} SET `label`='Blendenbeschriftung' WHERE `id`='3203';
        UPDATE {{trattributeclass}} SET `label`='Beschriftung' WHERE `id`='3204';
        UPDATE {{trattributeclass}} SET `label`='Logo auf Schriftblende platzieren' WHERE `id`='3205';
        UPDATE {{trattributeclass}} SET `label`='Logos werden nur in digitaler Form akzeptiert, die Herstellungskosten hängen von der Komplexität eines Logos ab.' WHERE `id`='3206';
        UPDATE {{trattributeclass}} SET `label`='Alle Preise verstehen sich inkl. MwSt. 18%. Die Bezahlung erfolgt in Rubel zum Kurs der Zentralbank der Russischen Föderation, der am Tag der Überweisung vom Konto des Ausstellers gilt.<br><br>' WHERE `id`='3210';
        UPDATE {{trattributeclass}} SET `label`='Der Antrag ist die Anlage \nzum Vertrag über die Messeteilnahme und \nin zwei Exemplaren auszufüllen.\n<br/>\n<br/>\nMit der Unterschrift des Antragsformulars erklärt sich der Aussteller mit der Messeordnung einverstanden\nund garantiert die Bezahlung der bestellten Dienstleistungen.\n<br/>\n<br/>\nDie Servicekosten erhöhen sich um 50%, wenn die Bestellung nach dem 21.Oktober 2017 eingeht, und um 100%, wenn die Bestellung nach dem 23.November 2017 eingeht.\n<br/>\n<br/>\nAlle Beschwerden, die die Ausführung dieser Bestellung betreffen, werden vom Veranstalter bis zum 3.Dezember 2017 14:00 akzeptiert.\nFalls eine begründete Beschwerde nicht termingerecht beim Veranstalter eingeht, gelten alle Arbeiten und Dienstleistungen, die in diesem Antrag angegeben sind, als entsprechend qualitativ ausgeführt und vom Aussteller akzeptiert.\n' WHERE `id`='3212';
        UPDATE {{trattributeclass}} SET `label`='Aussteller' WHERE `id`='3216';
        UPDATE {{trattributeclass}} SET `label`='Position' WHERE `id`='3217';
        UPDATE {{trattributeclass}} SET `label`='vollständiger Name' WHERE `id`='3219';
        UPDATE {{trattributeclass}} SET `label`='Veranstalter' WHERE `id`='3220';
        UPDATE {{trattributeclass}} SET `label`='Position' WHERE `id`='3221';
        UPDATE {{trattributeclass}} SET `label`='vollständiger Name' WHERE `id`='3223';
        UPDATE {{trattributeclass}} SET `label`='Unterschrift' WHERE `id`='3224';
        UPDATE {{trattributeclass}} SET `label`='Unterschrift' WHERE `id`='3225';
        
        UPDATE {{trattributeclass}} SET `unitTitle`='m2', `tooltip`='<div class=\"hint - header\">Servicekosten</div> die Servicekosten erhöhen sich:<br/> um 50%, wenn die Bestellung nach dem 21.Oktober 2017 eingeht<br/>um 100%, wenn die Bestellung nach dem 23.November 2017 eingeht' WHERE `id`='3189';
        UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Servicekosten</div> die Servicekosten erhöhen sich:<br/> um 50%, wenn die Bestellung nach dem 21.Oktober 2017 eingeht<br/>um 100%, wenn die Bestellung nach dem 23.November 2017 eingeht' WHERE `id`='3190';
        UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Servicekosten</div> die Servicekosten erhöhen sich:<br/> um 50%, wenn die Bestellung nach dem 21.Oktober 2017 eingeht<br/>um 100%, wenn die Bestellung nach dem 23.November 2017 eingeht' WHERE `id`='3204';
        UPDATE {{trattributeclass}} SET `unitTitle`='Stk.', `tooltip`='<div class=\"\"hint-header\"\">SERVICEKOSTEN</div> \ndie Servicekosten erhöhen sich um 50%, wenn die Bestellung nach dem  21.Oktober 2017 eingeht<br/>um 100%, wenn die Bestellung nach dem 23.November 2017 eingeht' WHERE `id`='3208';
        UPDATE {{trattributeclass}} SET `unitTitle`='m' WHERE `id`='3192';
        UPDATE {{trattributeclass}} SET `unitTitle`='m' WHERE `id`='3193';
        
        UPDATE {{trattributeclass}} SET `placeholder`='Bis zu 15 Zeichen kostenlos' WHERE `id`='3204';
        INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('73', 'de', 'Zeichen');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}