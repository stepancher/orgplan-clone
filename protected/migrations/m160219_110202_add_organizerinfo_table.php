<?php

class m160219_110202_add_organizerinfo_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{organizerinfo}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
				DROP TABLE IF EXISTS {{organizerinfo}};
				CREATE TABLE {{organizerinfo}} (
				  `id` INT NOT NULL AUTO_INCREMENT,
				  `organizerId` INT NULL,
				  `name` VARCHAR(255) NULL,
				  `fullName` VARCHAR(255) NULL,
				  `scope` INT NULL,
				  `territoryOfActivity` INT NULL,
				  `postPostcode` VARCHAR(45) NULL,
				  `postCountry` INT NULL,
				  `postRegion` INT NULL,
				  `postCity` INT NULL,
				  `postStreet` VARCHAR(45) NULL,
				  `postPhone` VARCHAR(45) NULL,
				  `postSite` VARCHAR(45) NULL,
				  `legalPostcode` VARCHAR(45) NULL,
				  `legalCountry` INT NULL,
				  `legalRegion` INT NULL,
				  `legalCity` INT NULL,
				  `legalStreet` VARCHAR(45) NULL,
				  `requisitesSettlementAccount` VARCHAR(45) NULL,
				  `requisitesBank` VARCHAR(45) NULL,
				  `requisitesBic` VARCHAR(45) NULL,
				  `requisitesItn` VARCHAR(45) NULL,
				  `requisitesIec` VARCHAR(45) NULL,
				  `requisitesCorrespondentAccount` VARCHAR(45) NULL,
				  `signRulesName` VARCHAR(45) NULL,
				  `signRulesPosition` VARCHAR(45) NULL,
				  PRIMARY KEY (`id`));
				  ';
	}
}