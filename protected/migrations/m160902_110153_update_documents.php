<?php

class m160902_110153_update_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/11699/agrorus_application_02_plan_stend_ENG.pdf' WHERE `id`='54';
            UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/11699/agrorus_application_02_plan_stend_RUS.pdf' WHERE `id`='55';
            UPDATE {{documents}} SET `active`='1' WHERE `id`='28';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}