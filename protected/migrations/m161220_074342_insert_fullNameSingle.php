<?php

class m161220_074342_insert_fullNameSingle extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trexhibitioncomplextype}} SET `fullNameSingle`='Деловой, торгово-развлекательный центр' WHERE `id`='14';
            UPDATE {{trexhibitioncomplextype}} SET `fullNameSingle`='Спортивный, научный, культурный центр' WHERE `id`='15';
            UPDATE {{trexhibitioncomplextype}} SET `fullNameSingle`='Гостиница, конгресс-холл' WHERE `id`='16';
            UPDATE {{trexhibitioncomplextype}} SET `fullNameSingle`='Выставочный центр' WHERE `id`='17';
            UPDATE {{trexhibitioncomplextype}} SET `fullNameSingle`='Театр, музей, галерея' WHERE `id`='18';
            UPDATE {{trexhibitioncomplextype}} SET `fullNameSingle`='Специализированная площадка' WHERE `id`='19';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}