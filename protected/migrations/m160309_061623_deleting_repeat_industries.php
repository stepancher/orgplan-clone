<?php

class m160309_061623_deleting_repeat_industries extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			DELETE FROM {{industry}} WHERE `id` IN (156, 63);
			DELETE FROM {{trindustry}} WHERE `trParentId` IN (156, 63);
		';
	}
}