<?php

class m170406_133908_del_unused_scheme extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        return "
            DELETE FROM {{myfair}} WHERE `id`='457';
            DELETE FROM {{myfair}} WHERE `id`='458';
            DELETE FROM {{myfair}} WHERE `id`='459';
            UPDATE {{fair}} SET `proposalVersion`=NULL WHERE `id`='13863';
            
            DROP DATABASE `orgplan_proposal_f13863_orgplan`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}