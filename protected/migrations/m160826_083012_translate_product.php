<?php

class m160826_083012_translate_product extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{trproduct}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(255) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `param` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {{trproduct}} (`trParentId`, `langId`, `name`, `param`)
            (SELECT `id`, 'ru', `name`, `param` FROM {{product}});

            INSERT INTO {{trproduct}} (`trParentId`, `langId`, `name`, `param`)
            (SELECT `id`, 'en', `name`, `param` FROM {{product}});
            
            INSERT INTO {{trproduct}} (`trParentId`, `langId`, `name`, `param`)
            (SELECT `id`, 'de', `name`, `param` FROM {{product}});
            
            UPDATE {{trproduct}} SET `name` = 'Audio set' WHERE `langId` = 'en' AND `name` = 'Звуковой комплект';
            UPDATE {{trproduct}} SET `name` = 'Plasma Display Panel' WHERE `langId` = 'en' AND `name` = 'Плазменная панель';
            UPDATE {{trproduct}} SET `name` = 'Refreshment break' WHERE `langId` = 'en' AND `name` = 'Кофе-брейк';
            UPDATE {{trproduct}} SET `name` = 'Standing buffet' WHERE `langId` = 'en' AND `name` = 'Фуршет';
            UPDATE {{trproduct}} SET `name` = 'Dinner event' WHERE `langId` = 'en' AND `name` = 'Банкет';
            UPDATE {{trproduct}} SET `name` = 'Business cards printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление визиток';
            UPDATE {{trproduct}} SET `name` = 'Flyers printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление листовок';
            UPDATE {{trproduct}} SET `name` = 'A4 flyers printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление листовок А4';
            UPDATE {{trproduct}} SET `name` = 'A5 flyers printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление листовок А5';
            UPDATE {{trproduct}} SET `name` = 'Leaflets printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление лифлета';
            UPDATE {{trproduct}} SET `name` = 'Poster printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление плакатов';
            UPDATE {{trproduct}} SET `name` = 'Complimentary tickets printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление пригласительных';
            UPDATE {{trproduct}} SET `name` = 'Event posters printing service' WHERE `langId` = 'en' AND `name` = 'Изготовление афиши';
            UPDATE {{trproduct}} SET `name` = 'Flyer design' WHERE `langId` = 'en' AND `name` = 'Дизайн листовки';
            UPDATE {{trproduct}} SET `name` = 'Leaflet design' WHERE `langId` = 'en' AND `name` = 'Дизайн лифлета';
            UPDATE {{trproduct}} SET `name` = 'Banner design' WHERE `langId` = 'en' AND `name` = 'Дизайн баннера';
            UPDATE {{trproduct}} SET `name` = 'Banner printing service' WHERE `langId` = 'en' AND `name` = 'Печать баннера 	 ';
            UPDATE {{trproduct}} SET `name` = 'Promotional staff service' WHERE `langId` = 'en' AND `name` = 'Услуги промо персонала';
            UPDATE {{trproduct}} SET `name` = 'Stand-assistant service' WHERE `langId` = 'en' AND `name` = 'Услуги стендиста';
            UPDATE {{trproduct}} SET `name` = 'Hostess service' WHERE `langId` = 'en' AND `name` = 'Услуги хостес';
            UPDATE {{trproduct}} SET `name` = 'Entertainment representative service' WHERE `langId` = 'en' AND `name` = 'Услуги аниматора';
            UPDATE {{trproduct}} SET `name` = 'Balloon garlands' WHERE `langId` = 'en' AND `name` = 'Гирлянды из шаров';
            UPDATE {{trproduct}} SET `name` = 'Pop up promotion counter' WHERE `langId` = 'en' AND `name` = 'Бокс-трибуна';
            UPDATE {{trproduct}} SET `name` = 'Pop-up stand' WHERE `langId` = 'en' AND `name` = 'Мобильный стенд Pop-up';
            UPDATE {{trproduct}} SET `name` = 'Roll-up stand' WHERE `langId` = 'en' AND `name` = 'Мобильный стенд Roll-up';
            UPDATE {{trproduct}} SET `name` = 'Fold up stand' WHERE `langId` = 'en' AND `name` = 'Планшетный стенд  ( Fold up )';
            
            UPDATE {{trproduct}} SET `param` = 'Sound board, amplifier, wireless microphone, acoustical system, power to 1kW.' WHERE `langId` = 'en' AND `param` = 'микшерный пульт, усилитель, радиомикрофон, акуст. система,  мощность до 1кВт. ';
            UPDATE {{trproduct}} SET `param` = 'Plasma Display Panel 50\"' WHERE `langId` = 'en' AND `param` = 'Плазменная панель  50\"
';
            UPDATE {{trproduct}} SET `param` = 'Portion calculated for 50 persons.' WHERE `langId` = 'en' AND `param` = 'Порция, из расчета на 50 чел.
';
            UPDATE {{trproduct}} SET `param` = 'Portion calculated for 50 persons.' WHERE `langId` = 'en' AND `param` = 'Порция, из расчета на 50 чел.
';
            UPDATE {{trproduct}} SET `param` = 'Portion calculated for 50 persons.' WHERE `langId` = 'en' AND `param` = 'Порция, из расчета на 50 чел.
';
            UPDATE {{trproduct}} SET `param` = '90х50mm, cardboard, 250 g/m2, 100 pcs' WHERE `langId` = 'en' AND `param` = '90х50мм, картон, 250 г/м2, 100 шт';
            UPDATE {{trproduct}} SET `param` = '100х210 Euro format, Coated paper, 115g/m2, double-sided, 1000 pcs.' WHERE `langId` = 'en' AND `param` = '100х210 евро формат, миловка, 115г/м2,  двусторонняя, 1000 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А4, 115g/m, Coated paper, double-sided, 1000 pcs.' WHERE `langId` = 'en' AND `param` = 'А4, 115г/м, миловка, двусторонняя, 1000 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А5, 115g/m, Coated paper, double-sided, 1000 pcs.' WHERE `langId` = 'en' AND `param` = 'А5, 115г/м,миловка, двусторонняя, 1000 шт.';
            UPDATE {{trproduct}} SET `param` = 'А4, two folgings, full-colored, 1000 pcs.' WHERE `langId` = 'en' AND `param` = 'А4, две фальцовки, полноцвет, 1000 шт.';
            UPDATE {{trproduct}} SET `param` = 'А3, one-side printing, colour, interior printing, 1000 pcs.' WHERE `langId` = 'en' AND `param` = 'А3, односторонняя, цветная, интерьерная печать, 1000 шт.
';
            UPDATE {{trproduct}} SET `param` = '200х210, digital printing, 100 pcs.' WHERE `langId` = 'en' AND `param` = '200х210, цифровая печать, 100 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А2, newsprint, 50 pcs.' WHERE `langId` = 'en' AND `param` = 'А2, газетная бумага, 50 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А5, double-sided' WHERE `langId` = 'en' AND `param` = 'А5,  двусторонняя
';
            UPDATE {{trproduct}} SET `param` = 'А4, folded to 1/3 of sheet, double-sided.' WHERE `langId` = 'en' AND `param` = ' А4, сфальцованный до 1/3 листа, двусторонний.
';
            UPDATE {{trproduct}} SET `param` = '3х6 m, one-side printing' WHERE `langId` = 'en' AND `param` = '3х6 м, односторонний
';
            UPDATE {{trproduct}} SET `param` = '3х6 m, density 300 g/m, wide format printing, grommets 1,2 cm' WHERE `langId` = 'en' AND `param` = '3х6 м, плотность 300 г/м., широкоформатная печать, люверсы 1,2 см
';
            UPDATE {{trproduct}} SET `param` = 'Net, section 500х1000, m2, grommets' WHERE `langId` = 'en' AND `param` = 'Сетка, сечение 500х1000, м2, люверсы

';
            UPDATE {{trproduct}} SET `param` = '1 promoter' WHERE `langId` = 'en' AND `param` = ' 1 промоутер 
';
            UPDATE {{trproduct}} SET `param` = '1 stand-assistant' WHERE `langId` = 'en' AND `param` = ' 1 стендист
';
            UPDATE {{trproduct}} SET `param` = '1 stand-assistant' WHERE `langId` = 'en' AND `param` = ' 1 стендист 
';
            UPDATE {{trproduct}} SET `param` = '1 entertainment representative' WHERE `langId` = 'en' AND `param` = '1 аниматор';
            UPDATE {{trproduct}} SET `param` = '1 m' WHERE `langId` = 'en' AND `param` = '1 м';
            UPDATE {{trproduct}} SET `param` = '' WHERE `langId` = 'en' AND `param` = '';
            UPDATE {{trproduct}} SET `param` = '3х3 section, with papering' WHERE `langId` = 'en' AND `param` = '3х3 секции, с оклейкой
';
            UPDATE {{trproduct}} SET `param` = '100x200 cm, with papering' WHERE `langId` = 'en' AND `param` = '100x200 см, с оклейкой
';
            UPDATE {{trproduct}} SET `param` = '8 sections 70х100cm, with borders 70х30cm, with papering' WHERE `langId` = 'en' AND `param` = '8 секций 70х100см, с фризами 70х30см, с оклейкой
';
            
            
            UPDATE {{trproduct}} SET `param` = 'микшерный пульт, усилитель, радиомикрофон, акуст. система,  мощность до 1кВт.' WHERE `param` = 'микшерный пульт, усилитель, радиомикрофон, акуст. система,  мощность до 1кВт. ';
            UPDATE {{trproduct}} SET `param` = 'Плазменная панель  50\"' WHERE `param` = 'Плазменная панель  50\"
';
            UPDATE {{trproduct}} SET `param` = 'Порция, из расчета на 50 чел.' WHERE `param` = 'Порция, из расчета на 50 чел.
';
            UPDATE {{trproduct}} SET `param` = 'Порция, из расчета на 50 чел.' WHERE `param` = 'Порция, из расчета на 50 чел.
';
            UPDATE {{trproduct}} SET `param` = 'Порция, из расчета на 50 чел.' WHERE `param` = 'Порция, из расчета на 50 чел.
';
            UPDATE {{trproduct}} SET `param` = '100х210 евро формат, миловка, 115г/м2,  двусторонняя, 1000 шт.' WHERE `param` = '100х210 евро формат, миловка, 115г/м2,  двусторонняя, 1000 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А4, 115г/м, миловка, двусторонняя, 1000 шт.' WHERE `param` = 'А4, 115г/м, миловка, двусторонняя, 1000 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А3, односторонняя, цветная, интерьерная печать, 1000 шт.' WHERE `param` = 'А3, односторонняя, цветная, интерьерная печать, 1000 шт.
';
            UPDATE {{trproduct}} SET `param` = '200х210, цифровая печать, 100 шт.' WHERE `param` = '200х210, цифровая печать, 100 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А2, газетная бумага, 50 шт.' WHERE `param` = 'А2, газетная бумага, 50 шт.
';
            UPDATE {{trproduct}} SET `param` = 'А5,  двусторонняя' WHERE `param` = 'А5,  двусторонняя
';
            UPDATE {{trproduct}} SET `param` = 'А4, сфальцованный до 1/3 листа, двусторонний' WHERE `param` = ' А4, сфальцованный до 1/3 листа, двусторонний.
';
            UPDATE {{trproduct}} SET `param` = '3х6 м, односторонний' WHERE `param` = '3х6 м, односторонний
';
            UPDATE {{trproduct}} SET `param` = '3х6 м, плотность 300 г/м, широкоформатная печать, люверсы 1,2 см.' WHERE `param` = '3х6 м, плотность 300 г/м., широкоформатная печать, люверсы 1,2 см
';
            UPDATE {{trproduct}} SET `param` = 'Сетка, сечение 500х1000, м2, люверсы' WHERE `param` = 'Сетка, сечение 500х1000, м2, люверсы

';
            UPDATE {{trproduct}} SET `param` = '1 промоутер' WHERE `param` = ' 1 промоутер 
';
            UPDATE {{trproduct}} SET `param` = '1 стендист' WHERE `param` = ' 1 стендист
';
            UPDATE {{trproduct}} SET `param` = '1 стендист' WHERE `param` = ' 1 стендист 
';
            UPDATE {{trproduct}} SET `param` = '3х3 секции, с оклейкой' WHERE `param` = '3х3 секции, с оклейкой
';
            UPDATE {{trproduct}} SET `param` = '100x200 см, с оклейкой' WHERE `param` = '100x200 см, с оклейкой
';
            UPDATE {{trproduct}} SET `param` = '8 секций 70х100см., с фризами 70х30см., с оклейкой' WHERE `param` = '8 секций 70х100см, с фризами 70х30см, с оклейкой
';
            
            ALTER TABLE {{product}} 
            DROP COLUMN `param`,
            DROP COLUMN `name`;

            ALTER TABLE {{trproduct}} 
            ADD UNIQUE INDEX `UNIQ_trParentLangId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trproduct}} 
            ADD CONSTRAINT `fk_product_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{product}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}