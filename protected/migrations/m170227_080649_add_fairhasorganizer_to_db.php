<?php

class m170227_080649_add_fairhasorganizer_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE IF NOT EXISTS {{fairhasorganizer}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `fairId` INT(11) NULL DEFAULT NULL,
              `organizerId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            ALTER TABLE {{fairhasorganizer}} 
            ADD INDEX `fairIdidx` (`fairId` ASC);

            ALTER TABLE {{fairhasorganizer}} 
            ADD INDEX `organizerIdidx` (`organizerId` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}