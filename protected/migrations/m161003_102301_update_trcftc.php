<?php

class m161003_102301_update_trcftc extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Разработайте концепцию участия в выставке, учитывая поставленную цель' WHERE `id`='1805';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Оформите заявку на размещение наружной рекламы, медиа рекламы, рекламы внутри помещений' WHERE `id`='1807';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Оформите заявку на помещение и оборудование для проведения мероприятия в рамках выставки' WHERE `id`='1809';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор для экспедирования в случае таможенного оформления' WHERE `id`='1810';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор для доставки экспонатов на стенд' WHERE `id`='1811';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Утвердите список сотрудников для работы на стенде, закажите гостиницу и билеты авиа или ж/д' WHERE `id`='1814';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор с кейтеринговым бюро для обеспечения питания гостей и сотрудников на стенде' WHERE `id`='1815';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор по предоставлению наемного персонала (переводчики, хостессы, промо, артисты, ведущие...)' WHERE `id`='1819';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и закажите проект стенда или оформите заявку на строительство стенда от Организатора' WHERE `id`='1821';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Закажите подключение необходимых коммуникаций у Организатора выставки: электричество, вода, интернет и другое' WHERE `id`='1822';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Разработайте программу продвижения вашего участия и выберите каналы коммуникации с посетителями до и во время выставки' WHERE `id`='1824';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Составьте письмо для электронной рассылки и разработайте электронное/печатное приглашение' WHERE `id`='1828';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Запустите рекламную кампания вашего участия в выставке: новости в соц. сетях, рассылка, анонсы в СМИ' WHERE `id`='1829';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Разработайте концепцию участия в выставке, учитывая поставленную цель' WHERE `id`='1834';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Оформите заявку на размещение наружной рекламы, медиа рекламы, рекламы внутри помещений' WHERE `id`='1836';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Оформите заявку на помещение и оборудование для проведения мероприятия в рамках выставки' WHERE `id`='1838';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор для экспедирования в случае таможенного оформления' WHERE `id`='1839';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор для доставки экспонатов на стенд' WHERE `id`='1840';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Утвердите список сотрудников для работы на стенде, закажите гостиницу и билеты авиа или ж/д' WHERE `id`='1843';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор с кейтеринговым бюро для обеспечения питания гостей и сотрудников на стенде' WHERE `id`='1844';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и заключите договор по предоставлению наемного персонала (переводчики, хостессы, промо, артисты, ведущие...)' WHERE `id`='1848';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Выберите подрядчика и закажите проект стенда или оформите заявку на строительство стенда от Организатора' WHERE `id`='1850';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Закажите подключение необходимых коммуникаций у Организатора выставки: электричество, вода, интернет и другое' WHERE `id`='1851';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Разработайте программу продвижения вашего участия и выберите каналы коммуникации с посетителями до и во время выставки' WHERE `id`='1853';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Составьте письмо для электронной рассылки и разработайте электронное/печатное приглашение' WHERE `id`='1857';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Запустите рекламную кампания вашего участия в выставке: новости в соц. сетях, рассылка, анонсы в СМИ' WHERE `id`='1858';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}