<?php

class m160609_143953_update_6_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1430';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1499';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1436';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1505';
			UPDATE {{trattributeclass}} SET `unitTitle`=NULL, `placeholder`='2016/10/04 - 2016/10-07' WHERE `id`='1507';
			UPDATE {{trattributeclass}} SET `placeholder`=NULL WHERE `id`='1507';
			UPDATE {{trattributeclass}} SET `placeholder`='2016/10/04 - 2016/10-07' WHERE `id`='1508';
			UPDATE {{attributeclass}} SET `priority`='40' WHERE `id`='2027';
			UPDATE {{attributeclass}} SET `priority`='50' WHERE `id`='2030';
			UPDATE {{attributeclass}} SET `priority`='40' WHERE `id`='2030';
			UPDATE {{attributeclass}} SET `priority`='50' WHERE `id`='2027';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1508';
			UPDATE {{trattributeclass}} SET `placeholder`='2016/10/04 - 2016/10-07' WHERE `id`='1511';
			UPDATE {{trattributeclass}} SET `label`='', `placeholder`='2016/10/04 - 2016/10-07' WHERE `id`='1508';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1511';
			UPDATE {{trattributeclass}} SET `placeholder`=NULL WHERE `id`='1498';
			UPDATE {{trattributeclass}} SET `placeholder`=NULL WHERE `id`='1440';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1439';
			UPDATE {{trattributeclass}} SET `placeholder`='2016/10/04 - 2016/10-07' WHERE `id`='1442';
			UPDATE {{attributeclass}} SET `class`='datePickerDisabled' WHERE `id`='2025';
			UPDATE {{attributeclass}} SET `class`='datePicker' WHERE `id`='2028';
			UPDATE {{attributeclass}} SET `class`='datePicker' WHERE `id`='2025';
			UPDATE {{attributeclass}} SET `class`='datePickerDisabled' WHERE `id`='2031';

		";
	}
}
