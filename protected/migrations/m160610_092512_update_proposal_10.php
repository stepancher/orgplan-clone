<?php

class m160610_092512_update_proposal_10 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2158', 'en', '1');
		DELETE FROM {{trattributeclassproperty}} WHERE `id`='2607';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2566';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2564';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2568';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2570';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2572';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2574';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2576';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2578';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2580';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2582';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2584';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2588';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2586';
		";
	}
}