<?php

class m160525_100932_translates_contest extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{trattributeclass}} SET `label`='Компания' WHERE `id`='82';
			UPDATE {{trattributeclass}} SET `label`='Company name\'s' WHERE `id`='1148';
			UPDATE {{trattributeclass}} SET `label`='Country' WHERE `id`='83';
			UPDATE {{trattributeclass}} SET `label`='Страна' WHERE `id`='1112';
			UPDATE {{trattributeclass}} SET `label`='Город' WHERE `id`='1113';
			UPDATE {{trattributeclass}} SET `label`='City' WHERE `id`='84';
			UPDATE {{trattributeclass}} SET `placeholder`='Максимум 430 знаков, включая пробелы' WHERE `id`='121';
			UPDATE {{trattributeclass}} SET `placeholder`='Максимум 430 знаков, включая пробелы' WHERE `id`='1116';
			UPDATE {{trattributeclass}} SET `placeholder`='Maximum of 430 symbols, including blank' WHERE `id`='121';
		";
	}
}
