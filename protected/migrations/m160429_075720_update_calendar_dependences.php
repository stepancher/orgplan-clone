<?php

class m160429_075720_update_calendar_dependences extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {
		return true;
	}

	private function upSql() {

		return "
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/86' WHERE `id`='97';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/86' WHERE `id`='98';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/77' WHERE `id`='99';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/77' WHERE `id`='100';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/77' WHERE `id`='101';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/77' WHERE `id`='102';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/77' WHERE `id`='103';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/79' WHERE `id`='104';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/79' WHERE `id`='105';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/81' WHERE `id`='106';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/81' WHERE `id`='107';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/87' WHERE `id`='108';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/87' WHERE `id`='109';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/87' WHERE `id`='110';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/87' WHERE `id`='111';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/87' WHERE `id`='112';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/89' WHERE `id`='113';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/89' WHERE `id`='114';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/91' WHERE `id`='115';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/91' WHERE `id`='116';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/93' WHERE `id`='117';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/93' WHERE `id`='118';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/93' WHERE `id`='119';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/93' WHERE `id`='120';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/93' WHERE `id`='121';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/95' WHERE `id`='122';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/95' WHERE `id`='123';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/97' WHERE `id`='124';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/97' WHERE `id`='125';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/99' WHERE `id`='126';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/99' WHERE `id`='127';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/99' WHERE `id`='128';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/99' WHERE `id`='129';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/99' WHERE `id`='130';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/101' WHERE `id`='131';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/101' WHERE `id`='132';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/103' WHERE `id`='133';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/118' WHERE `id`='145';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/118' WHERE `id`='146';
			UPDATE {{dependences}} SET `run`='calendar/gantt/add/id/118' WHERE `id`='147';
			UPDATE {{dependences}} SET `run`='calendar/gantt/complete/id/118' WHERE `id`='148';

			INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/send', '{\"fairId\":10556, \"ecId\":16}', 'calendar/gantt/complete/id/103');
		";
	}
}