<?php

class m170512_111549_adding_australia_desc extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{trcatalogdescription}} 
            CHANGE COLUMN `text` `text` LONGTEXT NULL DEFAULT NULL ;

            UPDATE {{trcatalogdescription}} SET `text`='<p>Австралия - страна с особым географическим положением, богатейшими природными ресурсами, стабильной политической обстановкой, огромным экономическим потенциалом. После освобождения от колонизации Великобританией государство быстро превратилось в индустриально-аграрную страну западного типа с высоким научно-техническим потенциалом, высокоразвитой многоотраслевой экономикой.\n</p>\n<p>Сейчас Австралия – одна из держав, занимающих на аграрном и промышленном мировых рынках ведущие позиции. Но развитию страны мешают неблагоприятные факторы: частые засухи, низкое плодородие почв в некоторых регионах материка, трудности в развитии промышленности. Для поддержания роста производственных объемов требуется импортное сырье, внедрение новых технологий и грамотно сбалансированная экономика.\n</p>\n<p>Один из наиболее эффективных способов преодоления трудностей в развитии экономики страны – проведение выставок в Австралии. Они:\n</p>\n<ul>\n	<li>представляют новые разработки в отраслях хозяйства;</li>\n	<li>знакомят с новыми технологиями и способствуют их внедрению в практическую работу промышленных и сельскохозяйственных предприятий;</li>\n	<li>привлекают международных инвесторов;</li>\n	<li>расширяют деловые связи на межрегиональном и международном уровнях;</li>\n	<li>помогают скорректировать развитие бизнеса в соответствии с требованиями рынка;</li>\n	<li>раздвигают границы торговых отношений. </li>\n</ul>' WHERE `id`='1';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}