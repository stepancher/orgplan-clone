<?php

class m160427_121121_insert_envVariable_class extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='808';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='810';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='821';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='823';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='834';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='836';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='847';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='849';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='873';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='875';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='886';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='888';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='899';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='901';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='912';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='914';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='925';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='927';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='804';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='806';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='817';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='819';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='830';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='832';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='843';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='845';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='869';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='871';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='882';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='884';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='895';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='897';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='908';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='910';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='921';
			UPDATE {{attributeclass}} SET `class`='envVariable' WHERE `id`='923';
			
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES
				('817', 'exponentPost',  'envVariable'),
				('819', 'exponentName',  'envVariable'),
				('821', 'organizerPost', 'envVariable'),
				('823', 'organizerName', 'envVariable'),
				('830', 'exponentPost',  'envVariable'),
				('832', 'exponentName',  'envVariable'),
				('834', 'organizerPost', 'envVariable'),
				('836', 'organizerName', 'envVariable'),
				('843', 'exponentPost',  'envVariable'),
				('845', 'exponentName',  'envVariable'),
				('847', 'organizerPost', 'envVariable'),
				('849', 'organizerName', 'envVariable'),
				('869', 'exponentPost',  'envVariable'),
				('871', 'exponentName',  'envVariable'),
				('873', 'organizerPost', 'envVariable'),
				('875', 'organizerName', 'envVariable'),
				('882', 'exponentPost',  'envVariable'),
				('884', 'exponentName',  'envVariable'),
				('886', 'organizerPost', 'envVariable'),
				('888', 'organizerName', 'envVariable');
			";
	}

	private function downSql() {

		return "";
	}
}