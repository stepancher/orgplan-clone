<?php

class m160415_062457_add_dropdowns_on_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e1_type_option2', 'bool', '0', 'Погрузчик, час', '669', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e1_type_option3', 'bool', '0', 'Самоходный', '669', '663', '2', 'dropDownOption', '0');
			UPDATE {{attributeclass}} SET `dataType`='string', `isArray`='1', `label`=NULL, `class`='dropDownMultiply', `unitTitle`=NULL, `placeholder`='Тип погрузки' WHERE `id`='669';
			UPDATE {{attributeclass}} SET `isArray`='0', `label`='Тип погрузки' WHERE `id`='669';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='669';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='678';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='687';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='696';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='705';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='714';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='723';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='732';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='741';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='750';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e2_type_option', 'bool', '0', 'Кран, час', '669', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e2_type_option2', 'bool', '0', 'Погрузчик, час', '669', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e2_type_option3', 'bool', '0', 'Самоходный', '669', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e3_type_option', 'bool', '0', 'Кран, час', '687', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e3_type_option2', 'bool', '0', 'Погрузчик, час', '687', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e3_type_option3', 'bool', '0', 'Самоходный', '687', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e4_type_option', 'bool', '0', 'Кран, час', '696', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e4_type_option2', 'bool', '0', 'Погрузчик, час', '696', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e4_type_option3', 'bool', '0', 'Самоходный', '696', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e5_type_option', 'bool', '0', 'Кран, час', '705', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e5_type_option2', 'bool', '0', 'Погрузчик, час', '705', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e5_type_option3', 'bool', '0', 'Самоходный', '705', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e6_type_option', 'bool', '0', 'Кран, час', '714', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e6_type_option2', 'bool', '0', 'Погрузчик, час', '714', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e6_type_option3', 'bool', '0', 'Самоходный', '714', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e7_type_option', 'bool', '0', 'Кран, час', '723', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e7_type_option2', 'bool', '0', 'Погрузчик, час', '723', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e7_type_option3', 'bool', '0', 'Самоходный', '723', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e8_type_option', 'bool', '0', 'Кран, час', '732', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e8_type_option2', 'bool', '0', 'Погрузчик, час', '732', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e8_type_option3', 'bool', '0', 'Самоходный', '732', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e9_type_option', 'bool', '0', 'Кран, час', '741', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e9_type_option2', 'bool', '0', 'Погрузчик, час', '741', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e9_type_option3', 'bool', '0', 'Самоходный', '741', '663', '2', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e10_type_option', 'bool', '0', 'Кран, час', '750', '663', '0', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e10_type_option2', 'bool', '0', 'Погрузчик, час', '750', '663', '1', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e10_type_option3', 'bool', '0', 'Самоходный', '750', '663', '2', 'dropDownOption', '0');
			UPDATE {{attributeclass}} SET `parent`='678' WHERE `id`='789';
			UPDATE {{attributeclass}} SET `parent`='678' WHERE `id`='790';
			UPDATE {{attributeclass}} SET `parent`='678' WHERE `id`='791';

		";
	}
}