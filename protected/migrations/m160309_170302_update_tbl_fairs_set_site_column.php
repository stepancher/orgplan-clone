<?php

class m160309_170302_update_tbl_fairs_set_site_column extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			UPDATE {{fair}} f LEFT JOIN {{organizer}} o ON (f.organizerId = o.id) SET f.site = o.linkToTheSiteOrganizers WHERE f.organizerId = o.id;
		';
	}
}