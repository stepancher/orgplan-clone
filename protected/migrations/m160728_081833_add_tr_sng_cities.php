<?php

class m160728_081833_add_tr_sng_cities extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('241', 'ru', 'Баку');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('242', 'ru', 'Ереван');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('243', 'ru', 'Минск');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('244', 'ru', 'Бобруйск');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('245', 'ru', 'Могилев');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('246', 'ru', 'Костюковичи');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('247', 'ru', 'Гродно');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('248', 'ru', 'Астана');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('249', 'ru', 'Алматы');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('250', 'ru', 'Атырау');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('251', 'ru', 'Актау');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('252', 'ru', 'Актобе');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('253', 'ru', 'Павлодар');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('254', 'ru', 'Бишкек');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('255', 'ru', 'Кишинев');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('256', 'ru', 'Караганда');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('257', 'ru', 'Ташкент');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('258', 'ru', 'Фергана');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('259', 'ru', 'Ашхабад');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('260', 'ru', 'Туркменбашы');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('261', 'ru', 'Тбилиси');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('262', 'ru', 'Улан-Батор');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('263', 'ru', 'Киев');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('264', 'ru', 'Одесса');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('265', 'ru', 'Симферополь');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('266', 'ru', 'Севастополь');

INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('241', 'en', 'Baku');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('242', 'en', 'Yerevan');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('243', 'en', 'Minsk');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('244', 'en', 'Babruysk');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('245', 'en', 'Mogilev');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('246', 'en', 'Kastsyukovichy');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('247', 'en', 'Grodno');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('248', 'en', 'Astana');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('249', 'en', 'Almaty');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('250', 'en', 'Atyrau');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('251', 'en', 'Aktau');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('252', 'en', 'Aktobe');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('253', 'en', 'Pavlodar');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('254', 'en', 'Bishkek');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('255', 'en', 'Kishinev');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('256', 'en', 'Karaganda');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('257', 'en', 'Tashkent');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('258', 'en', 'Fergana');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('259', 'en', 'Ashgabat');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('260', 'en', 'Turkmenbashi');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('261', 'en', 'Tbilisi');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('262', 'en', 'Ulaanbaatar');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('263', 'en', 'Kiev');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('264', 'en', 'Odessa');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('265', 'en', 'Simferopol');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('266', 'en', 'Sevastopol');

INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('241', 'de', 'Baku');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('242', 'de', 'Jerewan');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('243', 'de', 'Minsk');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('244', 'de', 'Babrujsk');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('245', 'de', 'Mahiljou');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('246', 'de', 'Kastjukowitschy');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('247', 'de', 'Hrodna');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('248', 'de', 'Astana');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('249', 'de', 'Almaty');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('250', 'de', 'Atyrau');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('251', 'de', 'Aqtau');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('252', 'de', 'Aqtöbe');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('253', 'de', 'Pawlodar');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('254', 'de', 'Bischkek');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('255', 'de', 'Chișinău');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('256', 'de', 'Qaraghandy');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('257', 'de', 'Taschkent');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('258', 'de', 'Fargʻona');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('259', 'de', 'Aşgabat');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('260', 'de', 'Turkmenbashi');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('261', 'de', 'Tiflis');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('262', 'de', 'Ulaanbaatar');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('263', 'de', 'Kiew');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('264', 'de', 'Odessa');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('265', 'de', 'Simferopol');
INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('266', 'de', 'Sewastopol');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}