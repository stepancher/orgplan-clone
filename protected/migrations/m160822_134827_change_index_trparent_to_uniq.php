<?php

class m160822_134827_change_index_trparent_to_uniq extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{tradditionalexpenses}} 
            DROP INDEX `fk_additionalexpenses_trParentId` ,
            ADD UNIQUE INDEX `fk_additionalexpenses_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{tranalytics}} 
            DROP INDEX `fk_analytics_trParentId` ,
            ADD UNIQUE INDEX `fk_analytics_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trblog}} 
            DROP INDEX `fk_blog_trParentId` ,
            ADD UNIQUE INDEX `fk_blog_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trcountry}} 
            DROP INDEX `fk_country_trParentId` ,
            ADD UNIQUE INDEX `fk_country_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trdecor}} 
            DROP INDEX `fk_decor_trParentId` ,
            ADD UNIQUE INDEX `fk_decor_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trdistrict}} 
            DROP INDEX `fk_district_trParentId` ,
            ADD UNIQUE INDEX `fk_district_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trdocuments}} 
            DROP INDEX `fk_documents_trParentId` ,
            ADD UNIQUE INDEX `fk_documents_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trelectricity}} 
            DROP INDEX `fk_electricity_trParentId` ,
            ADD UNIQUE INDEX `fk_electricity_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trelement}} 
            DROP INDEX `fk_element_rtParentId` ,
            ADD UNIQUE INDEX `fk_element_rtParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trequipment}} 
            DROP INDEX `fk_equipment_trParentId` ,
            ADD UNIQUE INDEX `fk_equipment_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trequipmentparams}} 
            DROP INDEX `fk_equipmentparams_trParentId` ,
            ADD UNIQUE INDEX `fk_equipmentparams_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trexhibitioncomplextype}} 
            DROP INDEX `fk_exhibitioncomplextype_trParentId` ,
            ADD UNIQUE INDEX `fk_exhibitioncomplextype_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trexhibitionpreparing}} 
            DROP INDEX `fk_exhibitionpreparing_trParentId` ,
            ADD UNIQUE INDEX `fk_exhibitionpreparing_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trextraservice}} 
            DROP INDEX `fk_extraservice_trParentId` ,
            ADD UNIQUE INDEX `fk_extraservice_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trflooring}} 
            DROP INDEX `fk_flooring_trParentId` ,
            ADD UNIQUE INDEX `fk_flooring_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trfurniture}} 
            DROP INDEX `fk_furniture_trParentId` ,
            ADD UNIQUE INDEX `fk_furniture_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trindustry}} 
            DROP INDEX `fk_industry_trParentId` ,
            ADD UNIQUE INDEX `fk_industry_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trlegalentity}} 
            DROP INDEX `fk_legalentity_trParentId` ,
            ADD UNIQUE INDEX `fk_legalentity_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trmarketinginformation}} 
            DROP INDEX `fk_marketinginformation_trParentId` ,
            ADD UNIQUE INDEX `fk_marketinginformation_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trnews}} 
            DROP INDEX `fk_news_trParentId` ,
            ADD UNIQUE INDEX `fk_news_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trnotification}} 
            DROP INDEX `fk_notification_trParentId` ,
            ADD UNIQUE INDEX `fk_notification_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trorganizer}} 
            DROP INDEX `fk_organizer_trParentId` ,
            ADD UNIQUE INDEX `fk_organizer_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trorganizerinfo}} 
            DROP INDEX `fk_organizerinfo_trParentId` ,
            ADD UNIQUE INDEX `fk_organizerinfo_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trpage}} 
            DROP INDEX `fk_page_trParentId` ,
            ADD UNIQUE INDEX `fk_page_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trpodium}} 
            DROP INDEX `fk_podium_trParentId` ,
            ADD UNIQUE INDEX `fk_podium_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trraexpert}} 
            DROP INDEX `fk_raexpert_trParentId` ,
            ADD UNIQUE INDEX `fk_raexpert_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trregion}} 
            DROP INDEX `fk_region_trParentId` ,
            ADD UNIQUE INDEX `fk_region_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trregioninformation}} 
            DROP INDEX `fk_regioninformation_trParentId` ,
            ADD UNIQUE INDEX `fk_regioninformation_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trservices}} 
            DROP INDEX `fk_services_trParentId` ,
            ADD UNIQUE INDEX `fk_services_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trstand}} 
            DROP INDEX `fk_stand_trParentId` ,
            ADD UNIQUE INDEX `fk_stand_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trsubtype}} 
            DROP INDEX `fk_subtype_trParentId` ,
            ADD UNIQUE INDEX `fk_subtype_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trusefulinformation}} 
            DROP INDEX `fk_usefulinformation_trParentId` ,
            ADD UNIQUE INDEX `fk_usefulinformation_trParentId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trwalltype}} 
            DROP INDEX `fk_walltype_trParentId` ,
            ADD UNIQUE INDEX `fk_walltype_trParentId` (`trParentId` ASC, `langId` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}