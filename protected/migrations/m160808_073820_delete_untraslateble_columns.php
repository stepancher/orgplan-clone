<?php

class m160808_073820_delete_untraslateble_columns extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->getCreateTable();
        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        echo 'doesn support migrate down, but return true';

        return true;
    }

    public function getCreateTable()
    {
        return '
            ALTER TABLE {{trorganizerinfo}} 
            DROP COLUMN `requisitesSettlementAccountUSD`,
            DROP COLUMN `requisitesSettlementAccountEUR`,
            DROP COLUMN `requisitesCorrespondentAccount`,
            DROP COLUMN `requisitesIec`,
            DROP COLUMN `requisitesItn`,
            DROP COLUMN `requisitesBic`,
            DROP COLUMN `requisitesSettlementAccountRUR`,
            DROP COLUMN `legalPostcode`,
            DROP COLUMN `postSite`,
            DROP COLUMN `postPhone`,
            DROP COLUMN `postPostcode`;

            ALTER TABLE {{organizerinfo}} 
            ADD COLUMN `postPhone` VARCHAR(255) NULL DEFAULT NULL AFTER `requisitesSettlementAccountUSD`;

        ';
    }
}
