<?php

class m170203_061728_import_countries extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('au', 'au');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('16','en','Australia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('16','ru','Австралия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('16','de','Australien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('at', 'at');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('17','en','Austria');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('17','ru','Австрия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('17','de','Österreich');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('dz', 'dz');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('18','en','Algeria');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('18','ru','Алжир');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('18','de','Algerien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ar', 'ar');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('19','en','Argentina');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('19','ru','Аргентина');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('19','de','Argentinien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('bd', 'bd');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('20','en','Bangladesh');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('20','ru','Бангладеш');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('20','de','Bangladesch');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('bh', 'bh');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('21','en','Bahrain');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('21','ru','Бахрейн');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('21','de','Bahrain');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('be', 'be');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('22','en','Belgium');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('22','ru','Бельгия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('22','de','Belgien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('bg', 'bg');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('23','en','Bulgaria');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('23','ru','Болгария');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('23','de','Bulgarien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('bo', 'bo');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('24','en','Bolivia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('24','ru','Боливия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('24','de','Bolivien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ba', 'ba');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('25','en','Bosnia-Hercegovina');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('25','ru','Босния и Герцеговина');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('25','de','Bosnien und Herzegowina');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('bw', 'bw');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('26','en','Botswana');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('26','ru','Ботсвана');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('26','de','Botswana');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('br', 'br');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('27','en','Brazil');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('27','ru','Бразилия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('27','de','Brasilien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('uk', 'uk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('28','en','United Kingdom');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('28','ru','Великобритания');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('28','de','Vereinigtes Königreich');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('hu', 'hu');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('29','en','Hungary');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('29','ru','Венгрия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('29','de','Ungarn');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('gh', 'gh');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('30','en','Ghana');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('30','ru','Гана');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('30','de','Ghana');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('gt', 'gt');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('31','en','Guatemala');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('31','ru','Гватемала');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('31','de','Guatemala');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('hk', 'hk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('32','en','Hong Kong');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('32','ru','Гонконг');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('32','de','Hongkong');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('gr', 'gr');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('33','en','Greece');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('33','ru','Греция');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('33','de','Griechenland');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('dk', 'dk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('34','en','Denmark');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('34','ru','Дания');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('34','de','Dänemark');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cd', 'cd');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('35','en','Congo (Dem. Republic)');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('35','ru','Демократическая Республика Конго');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('35','de','Demokratische Republik Kongo');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('eg', 'eg');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('36','en','Egypt');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('36','ru','Египет');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('36','de','Ägypten');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('zm', 'zm');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('37','en','Zambia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('37','ru','Замбия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('37','de','Sambia');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('zw', 'zw');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('38','en','Zimbabwe');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('38','ru','Зимбабве');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('38','de','Simbabwe');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('il', 'il');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('39','en','Israel');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('39','ru','Израиль');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('39','de','Israel');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('in', 'in');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('40','en','India');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('40','ru','Индия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('40','de','Indien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('id', 'id');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('41','en','Indonesia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('41','ru','Индонезия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('41','de','Indonesien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('jo', 'jo');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('42','en','Jordan');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('42','ru','Иордания');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('42','de','Jordanien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('iq', 'iq');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('43','en','Iraq');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('43','ru','Ирак');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('43','de','Irak');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ir', 'ir');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('44','en','Iran');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('44','ru','Иран');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('44','de','Iran');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ie', 'ie');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('45','en','Ireland');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('45','ru','Ирландия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('45','de','Irland');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('is', 'is');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('46','en','Iceland');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('46','ru','Исландия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('46','de','Island');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('es', 'es');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('47','en','Spain');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('47','ru','Испания');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('47','de','Spanien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('it', 'it');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('48','en','Italy');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('48','ru','Италия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('48','de','Italien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('kh', 'kh');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('49','en','Cambodia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('49','ru','Камбоджа');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('49','de','Kambodscha');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cm', 'cm');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('50','en','Cameroon');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('50','ru','Камерун');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('50','de','Kamerun');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ca', 'ca');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('51','en','Canada');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('51','ru','Канада');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('51','de','Kanada');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('qa', 'qa');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('52','en','Qatar');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('52','ru','Катар');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('52','de','Katar');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ke', 'ke');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('53','en','Kenya');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('53','ru','Кения');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('53','de','Kenia');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cn', 'cn');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('54','en','China, PR');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('54','ru','Китайская Народная Республика');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('54','de','Volksrepublik China');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('tw', 'tw');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('55','en','Taiwan');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('55','ru','Китайская Республика');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('55','de','Republik China (Taiwan)');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('co', 'co');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('56','en','Colombia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('56','ru','Колумбия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('56','de','Kolumbien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cu', 'cu');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('57','en','Cuba');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('57','ru','Куба');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('57','de','Kuba');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('kw', 'kw');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('58','en','Kuwait');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('58','ru','Кувейт');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('58','de','Kuwait');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('lv', 'lv');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('59','en','Latvia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('59','ru','Латвия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('59','de','Lettland');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('lb', 'lb');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('60','en','Lebanon');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('60','ru','Ливан');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('60','de','Libanon');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('lt', 'lt');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('61','en','Lithuania');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('61','ru','Литва');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('61','de','Litauen');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('li', 'li');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('62','en','Liechtenstein');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('62','ru','Лихтенштейн');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('62','de','Liechtenstein');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('lu', 'lu');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('63','en','Luxembourg');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('63','ru','Люксембург');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('63','de','Luxemburg');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mu', 'mu');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('64','en','Mauritius');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('64','ru','Маврикий');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('64','de','Mauritius');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mo', 'mo');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('65','en','Macau');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('65','ru','Макао');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('65','de','Macau');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('my', 'my');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('66','en','Malaysia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('66','ru','Малайзия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('66','de','Malaysia');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mt', 'mt');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('67','en','Malta');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('67','ru','Мальта');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('67','de','Malta');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ma', 'ma');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('68','en','Morocco');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('68','ru','Марокко');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('68','de','Marokko');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mx', 'mx');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('69','en','Mexico');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('69','ru','Мексика');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('69','de','Mexiko');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mz', 'mz');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('70','en','Mozambique');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('70','ru','Мозамбик');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('70','de','Mosambik');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mc', 'mc');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('71','en','Monaco');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('71','ru','Монако');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('71','de','Monaco');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mm', 'mm');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('72','en','Myanmar');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('72','ru','Мьянма');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('72','de','Myanmar');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ng', 'ng');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('73','en','Nigeria');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('73','ru','Нигерия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('73','de','Nigeria');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('nl', 'nl');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('74','en','Netherlands');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('74','ru','Нидерланды');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('74','de','Niederlande');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('nz', 'nz');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('75','en','New Zealand');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('75','ru','Новая Зеландия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('75','de','Neuseeland');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('no', 'no');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('76','en','Norway');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('76','ru','Норвегия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('76','de','Norwegen');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ae', 'ae');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('77','en','United Arab Emirates');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('77','ru','Объединённые Арабские Эмираты');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('77','de','Vereinigte Arabische Emirate');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('om', 'om');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('78','en','Oman');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('78','ru','Оман');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('78','de','Oman');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('pk', 'pk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('79','en','Pakistan');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('79','ru','Пакистан');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('79','de','Pakistan');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('pa', 'pa');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('80','en','Panama');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('80','ru','Панама');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('80','de','Panama');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('py', 'py');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('81','en','Paraguay');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('81','ru','Парагвай');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('81','de','Paraguay');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('pe', 'pe');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('82','en','Peru');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('82','ru','Перу');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('82','de','Peru');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('pl', 'pl');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('83','en','Poland');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('83','ru','Польша');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('83','de','Polen');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('pt', 'pt');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('84','en','Portugal');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('84','ru','Португалия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('84','de','Portugal');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cg', 'cg');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('85','en','Congo (Republic)');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('85','ru','Республика Конго');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('85','de','Republik Kongo');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('kr', 'kr');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('86','en','Korea, Republic');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('86','ru','Республика Корея');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('86','de','Südkorea');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('xk', 'xk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('87','en','Kosovo');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('87','ru','Республика Косово');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('87','de','Kosovo');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('mk', 'mk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('88','en','Macedonia (FYROM)');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('88','ru','Республика Македония');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('88','de','Mazedonien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('rw', 'rw');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('89','en','Rwanda');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('89','ru','Руанда');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('89','de','Ruanda');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ro', 'ro');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('90','en','Romania');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('90','ru','Румыния');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('90','de','Rumänien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('sa', 'sa');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('91','en','Saudi Arabia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('91','ru','Саудовская Аравия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('91','de','Saudi-Arabien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('sn', 'sn');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('92','en','Senegal');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('92','ru','Сенегал');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('92','de','Senegal');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('sg', 'sg');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('93','en','Singapore');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('93','ru','Сингапур');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('93','de','Singapur');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('sk', 'sk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('94','en','Slovak Republic');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('94','ru','Словакия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('94','de','Slowakei');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('si', 'si');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('95','en','Slovenia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('95','ru','Словения');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('95','de','Slowenien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('us', 'us');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('96','en','USA');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('96','ru','Соединённые Штаты Америки');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('96','de','Vereinigte Staaten');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('sd', 'sd');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('97','en','Sudan');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('97','ru','Судан');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('97','de','Sudan');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('sr', 'sr');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('98','en','Surinam');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('98','ru','Суринам');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('98','de','Suriname');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('th', 'th');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('99','en','Thailand');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('99','ru','Таиланд');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('99','de','Thailand');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('tz', 'tz');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('100','en','Tanzania');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('100','ru','Танзания');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('100','de','Tansania');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('tn', 'tn');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('101','en','Tunisia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('101','ru','Тунис');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('101','de','Tunesien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('tr', 'tr');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('102','en','Turkey');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('102','ru','Турция');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('102','de','Türkei');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ug', 'ug');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('103','en','Uganda');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('103','ru','Уганда');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('103','de','Uganda');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ph', 'ph');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('104','en','Philippines');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('104','ru','Филиппины');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('104','de','Philippinen');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('fi', 'fi');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('105','en','Finland');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('105','ru','Финляндия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('105','de','Finnland');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('fr', 'fr');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('106','en','France');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('106','ru','Франция');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('106','de','Frankreich');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('hr', 'hr');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('107','en','Croatia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('107','ru','Хорватия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('107','de','Kroatien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cz', 'cz');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('108','en','Czech Republic');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('108','ru','Чехия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('108','de','Tschechien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('cl', 'cl');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('109','en','Chile');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('109','ru','Чили');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('109','de','Chile');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ch', 'ch');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('110','en','Switzerland');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('110','ru','Швейцария');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('110','de','Schweiz');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('se', 'se');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('111','en','Sweden');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('111','ru','Швеция');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('111','de','Schweden');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('lk', 'lk');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('112','en','Sri Lanka');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('112','ru','Шри-Ланка');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('112','de','Sri Lanka');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ec', 'ec');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('113','en','Ecuador');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('113','ru','Эквадор');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('113','de','Ecuador');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ee', 'ee');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('114','en','Estonia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('114','ru','Эстония');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('114','de','Estland');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('et', 'et');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('115','en','Ethiopia');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('115','ru','Эфиопия');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('115','de','Äthiopien');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('za', 'za');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('116','en','South Africa');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('116','ru','Южно-Африканская Республика');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('116','de','Südafrika');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('jp', 'jp');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('117','en','Japan');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('117','ru','Япония');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('117','de','Japan');
            
            INSERT INTO {{country}} (`shorturl`, `code`) VALUES ('ao', 'ao');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('118','en','Angola');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('118','ru','Ангола');
            INSERT INTO {{trcountry}} (`trParentId`,`langId`,`name`) VALUES ('118','de','Algerien');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}