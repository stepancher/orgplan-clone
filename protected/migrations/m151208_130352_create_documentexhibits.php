<?php

class m151208_130352_create_documentexhibits extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{documentexhibits}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			CREATE TABLE IF NOT EXISTS {{documentexhibits}} (
					`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
					`data` text,
					`fairId` int(11),
					`userId` int(11)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;

			ALTER TABLE {{documentexhibits}} ADD `logoId` int(11);
			ALTER TABLE {{documentexhibits}} ADD `color` int(11);
		";
	}
}