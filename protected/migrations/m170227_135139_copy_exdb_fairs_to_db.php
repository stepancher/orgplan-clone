<?php

class m170227_135139_copy_exdb_fairs_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairs_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairs_to_db`()
            BEGIN
            
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_exdb_id INT DEFAULT 0;
                DECLARE f_exdb_crc BIGINT DEFAULT 0;
                DECLARE f_exdb_frequency TEXT DEFAULT '';
                DECLARE f_name TEXT DEFAULT '';
                DECLARE f_name_de TEXT DEFAULT '';
                DECLARE f_exhibition_complex_id INT DEFAULT '';
                DECLARE f_begin_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_date DATE DEFAULT '1970-01-01';
                DECLARE f_begin_mounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_mounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_begin_demounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_demounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_exdb_business_sectors TEXT DEFAULT '';
                DECLARE f_exdb_costs TEXT DEFAULT '';
                DECLARE f_exdb_show_type TEXT DEFAULT '';
                DECLARE f_site TEXT DEFAULT '';
                DECLARE f_story_id INT DEFAULT 0;
                DECLARE f_area_special_expositions INT DEFAULT 0;
                DECLARE f_square_net INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_exhibitors_foreign INT DEFAULT 0;
                DECLARE f_exhibitors_local INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_visitors_foreign INT DEFAULT 0;
                DECLARE f_visitors_local INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statistics_date TEXT DEFAULT '';
                DECLARE f_countries_count TEXT DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT 
                                        ef.exdbId, 
                                        ef.exdbCrc, 
                                        ef.exdbFrequency, 
                                        ef.name, 
                                        ef.name_de, 
                                        ec.id AS exhibitionComplexId,
                                        ef.beginDate,
                                        ef.endDate,
                                        ef.beginMountingDate,
                                        ef.endMountingDate,
                                        ef.beginDemountingDate,
                                        ef.endDemountingDate,
                                        ef.exdbBusinessSectors,
                                        ef.exdbCosts,
                                        ef.exdbShowType,
                                        ef.site,
                                        ef.storyId,
                                        efs.areaSpecialExpositions,
                                        efs.squareNet,
                                        efs.members,
                                        efs.exhibitorsForeign,
                                        efs.exhibitorsLocal,
                                        efs.visitors,
                                        efs.visitorsForeign,
                                        efs.visitorsLocal,
                                        efs.statistics,
                                        efs.statisticsDate,
                                        efs.exdbCountriesCount
                                        FROM {$expodata}.{{exdbfair}} ef
                                            LEFT JOIN {$expodata}.{{exdbfairstatistic}} efs ON efs.id = ef.infoId
                                            LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} eec ON eec.id = ef.exhibitionComplexId
                                            LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON ec.exdbId = eec.exdbId
                                            LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((f.exdbCrc IS NOT NULL AND f.exdbCrc = ef.exdbCrc) OR f.exdbCrc IS NULL)
                                        WHERE f.id IS NULL
                                        ORDER BY ef.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_exdb_id,
                                    f_exdb_crc,
                                    f_exdb_frequency,
                                    f_name,
                                    f_name_de,
                                    f_exhibition_complex_id,
                                    f_begin_date,
                                    f_end_date,
                                    f_begin_mounting_date,
                                    f_end_mounting_date,
                                    f_begin_demounting_date,
                                    f_end_demounting_date,
                                    f_exdb_business_sectors,
                                    f_exdb_costs,
                                    f_exdb_show_type,
                                    f_site,
                                    f_story_id,
                                    f_area_special_expositions,
                                    f_square_net,
                                    f_members,
                                    f_exhibitors_foreign,
                                    f_exhibitors_local,
                                    f_visitors,
                                    f_visitors_foreign,
                                    f_visitors_local,
                                    f_statistics,
                                    f_statistics_date,
                                    f_countries_count;
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != f_exdb_id THEN
                        START TRANSACTION;
                            INSERT INTO {$db}.{{fairinfo}} (`exdbFairId`,
                                `areaSpecialExpositions`,
                                `squareNet`,
                                `members`,
                                `exhibitorsForeign`,
                                `exhibitorsLocal`,
                                `visitors`,
                                `visitorsForeign`,
                                `visitorsLocal`,
                                `statistics`,
                                `exdbStatisticsDate`,
                                `exdbCountriesCount`) VALUES (f_exdb_id,
                                f_area_special_expositions,
                                f_square_net,
                                f_members,
                                f_exhibitors_foreign,
                                f_exhibitors_local,
                                f_visitors,
                                f_visitors_foreign,
                                f_visitors_local,
                                f_statistics,
                                f_statistics_date,
                                f_countries_count);
                        COMMIT;
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := f_exdb_id;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fair}} (`exdbId`,
                            `exdbCrc`,
                            `exdbFrequency`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                            `exdbBusinessSectors`,
                            `exdbCosts`,
                            `exdbShowType`,
                            `site`,
                            `storyId`,
                            `infoId`) VALUES (f_exdb_id,
                            f_exdb_crc,
                            f_exdb_frequency,
                            f_exhibition_complex_id,
                            f_begin_date,
                            f_end_date,
                            f_begin_mounting_date,
                            f_end_mounting_date,
                            f_begin_demounting_date,
                            f_end_demounting_date,
                            f_exdb_business_sectors,
                            f_exdb_costs,
                            f_exdb_show_type,
                            f_site,
                            f_story_id,
                            @last_insert_id);
                    COMMIT;
                    
                    SET @fair_last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name, 'ru');
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name, 'en');
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name_de, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_fairs_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}