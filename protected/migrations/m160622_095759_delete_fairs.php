<?php

class m160622_095759_delete_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			DELETE FROM {{fair}} WHERE id IN (5022,10104,10103,10100,10098,10097,10096,10095,3869,3868,3867,3866,3864,3861,3860);
			DELETE FROM {{trfair}} WHERE trParentId IN (5022,10104,10103,10100,10098,10097,10096,10095,3869,3868,3867,3866,3864,3861,3860);
		';
	}
}