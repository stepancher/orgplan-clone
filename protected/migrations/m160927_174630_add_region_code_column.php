<?php

class m160927_174630_add_region_code_column extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{region}}
              ADD COLUMN `code` VARCHAR(10) NULL AFTER `sponsorPos`;
            
            UPDATE {{region}} SET `code`='ck' WHERE `id`='81';
            UPDATE {{region}} SET `code`='ar' WHERE `id`='3';
            UPDATE {{region}} SET `code`='ar' WHERE `id`='88';
            UPDATE {{region}} SET `code`='ar' WHERE `id`='89';
            UPDATE {{region}} SET `code`='nn' WHERE `id`='33';
            UPDATE {{region}} SET `code`='yn' WHERE `id`='82';
            UPDATE {{region}} SET `code`='ky' WHERE `id`='24';
            UPDATE {{region}} SET `code`='sk' WHERE `id`='55';
            UPDATE {{region}} SET `code`='kh' WHERE `id`='76';
            UPDATE {{region}} SET `code`='sl' WHERE `id`='65';
            UPDATE {{region}} SET `code`='ka' WHERE `id`='18';
            UPDATE {{region}} SET `code`='kt' WHERE `id`='22';
            UPDATE {{region}} SET `code`='2510' WHERE `id`='30';
            UPDATE {{region}} SET `code`='rz' WHERE `id`='61';
            UPDATE {{region}} SET `code`='sa' WHERE `id`='62';
            UPDATE {{region}} SET `code`='ul' WHERE `id`='75';
            UPDATE {{region}} SET `code`='om' WHERE `id`='37';
            UPDATE {{region}} SET `code`='ns' WHERE `id`='36';
            UPDATE {{region}} SET `code`='mm' WHERE `id`='32';
            UPDATE {{region}} SET `code`='ln' WHERE `id`='27';
            UPDATE {{region}} SET `code`='sp' WHERE `id`='63';
            UPDATE {{region}} SET `code`='ki' WHERE `id`='51';
            UPDATE {{region}} SET `code`='kc' WHERE `id`='19';
            UPDATE {{region}} SET `code`='in' WHERE `id`='49';
            UPDATE {{region}} SET `code`='kb' WHERE `id`='15';
            UPDATE {{region}} SET `code`='no' WHERE `id`='56';
            UPDATE {{region}} SET `code`='st' WHERE `id`='68';
            UPDATE {{region}} SET `code`='sm' WHERE `id`='67';
            UPDATE {{region}} SET `code`='ps' WHERE `id`='43';
            UPDATE {{region}} SET `code`='tv' WHERE `id`='70';
            UPDATE {{region}} SET `code`='vo' WHERE `id`='9';
            UPDATE {{region}} SET `code`='iv' WHERE `id`='13';
            UPDATE {{region}} SET `code`='ys' WHERE `id`='83';
            UPDATE {{region}} SET `code`='kg' WHERE `id`='17';
            UPDATE {{region}} SET `code`='br' WHERE `id`='6';
            UPDATE {{region}} SET `code`='ks' WHERE `id`='26';
            UPDATE {{region}} SET `code`='lp' WHERE `id`='28';
            UPDATE {{region}} SET `code`='ms' WHERE `id`='31';
            UPDATE {{region}} SET `code`='ol' WHERE `id`='39';
            UPDATE {{region}} SET `code`='nz' WHERE `id`='34';
            UPDATE {{region}} SET `code`='pz' WHERE `id`='40';
            UPDATE {{region}} SET `code`='vl' WHERE `id`='7';
            UPDATE {{region}} SET `code`='vr' WHERE `id`='10';
            UPDATE {{region}} SET `code`='ko' WHERE `id`='52';
            UPDATE {{region}} SET `code`='sv' WHERE `id`='66';
            UPDATE {{region}} SET `code`='bk' WHERE `id`='46';
            UPDATE {{region}} SET `code`='ud' WHERE `id`='74';
            UPDATE {{region}} SET `code`='mr' WHERE `id`='54';
            UPDATE {{region}} SET `code`='cv' WHERE `id`='80';
            UPDATE {{region}} SET `code`='cl' WHERE `id`='78';
            UPDATE {{region}} SET `code`='ob' WHERE `id`='38';
            UPDATE {{region}} SET `code`='sr' WHERE `id`='64';
            UPDATE {{region}} SET `code`='tt' WHERE `id`='57';
            UPDATE {{region}} SET `code`='to' WHERE `id`='71';
            UPDATE {{region}} SET `code`='ty' WHERE `id`='73';
            UPDATE {{region}} SET `code`='ga' WHERE `id`='45';
            UPDATE {{region}} SET `code`='kk' WHERE `id`='59';
            UPDATE {{region}} SET `code`='cn' WHERE `id`='79';
            UPDATE {{region}} SET `code`='kl' WHERE `id`='50';
            UPDATE {{region}} SET `code`='da' WHERE `id`='48';
            UPDATE {{region}} SET `code`='ro' WHERE `id`='60';
            UPDATE {{region}} SET `code`='bl' WHERE `id`='5';
            UPDATE {{region}} SET `code`='tu' WHERE `id`='58';
            UPDATE {{region}} SET `code`='ir' WHERE `id`='14';
            UPDATE {{region}} SET `code`='ct' WHERE `id`='12';
            UPDATE {{region}} SET `code`='yv' WHERE `id`='11';
            UPDATE {{region}} SET `code`='am' WHERE `id`='2';
            UPDATE {{region}} SET `code`='tb' WHERE `id`='69';
            UPDATE {{region}} SET `code`='tl' WHERE `id`='72';
            UPDATE {{region}} SET `code`='ng' WHERE `id`='35';
            UPDATE {{region}} SET `code`='vg' WHERE `id`='8';
            UPDATE {{region}} SET `code`='kv' WHERE `id`='21';
            UPDATE {{region}} SET `code`='me' WHERE `id`='53';
            UPDATE {{region}} SET `code`='ke' WHERE `id`='20';
            UPDATE {{region}} SET `code`='as' WHERE `id`='4';
            UPDATE {{region}} SET `code`='pr' WHERE `id`='42';
            UPDATE {{region}} SET `code`='mg' WHERE `id`='29';
            UPDATE {{region}} SET `code`='bu' WHERE `id`='47';
            UPDATE {{region}} SET `code`='kn' WHERE `id`='16';
            UPDATE {{region}} SET `code`='kd' WHERE `id`='23';
            UPDATE {{region}} SET `code`='ku' WHERE `id`='25';
            UPDATE {{region}} SET `code`='al' WHERE `id`='1';
            UPDATE {{region}} SET `code`='km' WHERE `id`='77';
            UPDATE {{region}} SET `code`='pe' WHERE `id`='41';
            UPDATE {{region}} SET `code`='ad' WHERE `id`='44';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}