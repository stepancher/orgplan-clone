<?php

class m170322_103832_delete_fair extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE `id` = '17880';
            DELETE FROM {{fair}} WHERE `id` = '4454';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '11456';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2997';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14377';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}