<?php

class m160413_103211_alter_table_organizerInfo_add_columns extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			ALTER TABLE {{organizerinfo}}
			CHANGE COLUMN `requisitesSettlementAccountRUR` `requisitesSettlementAccount` VARCHAR(255) NULL DEFAULT NULL;
			
			ALTER TABLE {{organizerinfo}}
			DROP COLUMN `requisitesSettlementAccountEUR`;
			
			ALTER TABLE {{organizerinfo}}
			DROP COLUMN `requisitesSettlementAccountUSD`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{organizerinfo}}
			CHANGE COLUMN `requisitesSettlementAccount` `requisitesSettlementAccountRUR` VARCHAR(255) NULL DEFAULT NULL ,
			ADD COLUMN `requisitesSettlementAccountEUR` VARCHAR(255) NULL AFTER `signRulesPosition`,
			ADD COLUMN `requisitesSettlementAccountUSD` VARCHAR(255) NULL AFTER `requisitesSettlementAccountEUR`;
		";
	}
}