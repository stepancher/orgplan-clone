<?php

class m161124_064247_add_tr_for_region_analytic extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{trchartgroup}} (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `trParentId` INT(11) NULL DEFAULT NULL,
          `langId` VARCHAR(45) NULL DEFAULT NULL,
          `header` TEXT NULL DEFAULT NULL,
          PRIMARY KEY (`id`));

        ALTER TABLE {{trchartgroup}} 
        ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);

        ALTER TABLE {{trchartgroup}} 
        ADD CONSTRAINT `fk_trchartgroup_chartgroup`
          FOREIGN KEY (`trParentId`)
          REFERENCES {{chartgroup}} (`id`)
          ON DELETE CASCADE
          ON UPDATE CASCADE;

        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('1' , 'ru', '');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('2' , 'ru', NULL);
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('3' , 'ru', NULL);
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('4' , 'ru', 'Структура сельского хозяйства / Растениеводство');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('5' , 'ru', 'Структура сельского хозяйства / Животноводство');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('6' , 'ru', 'Основные показатели сельского хозяйства / Растениеводство');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('7' , 'ru', 'Основные показатели сельского хозяйства / Животноводство');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('8' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('17' , 'ru', 'КРЕДИТЫ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('18' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('19' , 'ru', 'Пиломатериал');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('20' , 'ru', 'Древесина и целлюлоза');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('21' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('22' , 'ru', 'КРЕДИТЫ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('23' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ПОТРЕБЛЕНИЯ ПРОДУКТОВ ПИТАНИЯ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('24' , 'ru', 'Потребительская корзина');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('25' , 'ru', 'Потребительская корзина (продолжение)');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('26' , 'ru', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('27' , 'ru', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО (ПРОДОЛЖЕНИЕ)');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('28' , 'ru', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('29' , 'ru', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО (ПРОДОЛЖЕНИЕ)');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('30' , 'ru', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('31' , 'ru', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ (ПРОДОЛЖЕНИЕ)');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('32' , 'ru', 'ЕМКОСТЬ РЫНКА АЛКОГОЛЬНОЙ ПРОДУКЦИИ (ПРОДОЛЖЕНИЕ)');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('33' , 'ru', 'СТРУКТУРА СЕЛЬСКОГО ХОЗЯЙСТВА / РАСТЕНИЕВОДСТВО');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('34' , 'ru', 'СТРУКТУРА СЕЛЬСКОГО ХОЗЯЙСТВА / ЖИВОТНОВОДСТВО');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('35' , 'ru', 'ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ И СЕЛЬСКОХОЗЯЙСТВЕННОЕ СЫРЬЕ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('36' , 'ru', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / РАСТЕНИЕВОДСТВО');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('37' , 'ru', 'ДВИЖЕНИЕ ОСНОВНЫХ ПРОДУКТОВ ПИТАНИЯ / ЖИВОТНОВОДСТВО');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('38' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('39' , 'ru', 'ИМПОРТ/ЭКСПОРТ ГОРЮЧЕ-СМАЗОЧНЫХ МАТЕРИАЛОВ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('40' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('41' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('42' , 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('43' , 'ru', 'ОБЪЕМ ГЕОЛОГОРАЗВЕДОВАТЕЛЬНЫХ РАБОТ');
        INSERT INTO {{trchartgroup}} (`trParentId`,`langId`,`header`) VALUES ('44' , 'ru', 'ЭКСПОРТ/ИМПОРТ МЕТАЛЛОВ И СТАЛЬНЫХ ТРУБ');

        ALTER TABLE {{chartgroup}} 
        DROP COLUMN `header`;

        CREATE TABLE {{trindustryanalyticsinformation}} (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `trParentId` INT(11) NULL DEFAULT NULL,
          `langId` VARCHAR(45) NULL DEFAULT NULL,
          `header` TEXT NULL DEFAULT NULL,
          PRIMARY KEY (`id`));

        ALTER TABLE {{trindustryanalyticsinformation}} 
        ADD UNIQUE INDEX `trPArentIdLangId` (`trParentId` ASC, `langId` ASC);

        ALTER TABLE {{trindustryanalyticsinformation}} 
        ADD CONSTRAINT `fk_trindustryanalyticsinformation_industryanalyticsinformation`
          FOREIGN KEY (`trParentId`)
          REFERENCES {{industryanalyticsinformation}} (`id`)
          ON DELETE CASCADE
          ON UPDATE CASCADE;

        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('1', 'ru', 'Мебель для дома и офиса, оформление интерьера, предметы быта');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('2', 'ru', 'Строительство, отделочные материалы и комплектация');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('3', 'ru', 'Машиностроение, металлообработка, станки, промышленное оборудование');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('5', 'ru', 'Лес и деревообработка');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('8', 'ru', 'Энергетика');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('11', 'ru', 'Сельское хозяйство');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('13', 'ru', 'Пищевая промышленность: оборудование и ингридиенты');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('14', 'ru', 'Продукты питания');
        INSERT INTO {{trindustryanalyticsinformation}} (`trParentId`,`langId`,`header`) VALUES ('37', 'ru', 'Нефть и газ');

        ALTER TABLE {{industryanalyticsinformation}} 
        DROP COLUMN `header`;

        CREATE TABLE {{trchart}} (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `trParentId` INT(11) NULL DEFAULT NULL,
          `langId` VARCHAR(45) NULL DEFAULT NULL,
          `header` TEXT NULL DEFAULT NULL,
          `measure` VARCHAR(45) NULL DEFAULT NULL,
          PRIMARY KEY (`id`));

        ALTER TABLE {{trchart}} 
        ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);

        ALTER TABLE {{trchart}} 
        ADD CONSTRAINT `fk_trchart_chart`
          FOREIGN KEY (`trParentId`)
          REFERENCES {{chart}} (`id`)
          ON DELETE CASCADE
          ON UPDATE CASCADE;

        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('1', 'ru', 'Скот и птица <br/> в убойном весе', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('2', 'ru', 'Молоко', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('3', 'ru', 'Яйца', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('4', 'ru', 'Шерсть', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('5', 'ru', 'Мед', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('6', 'ru', 'Крупный <br/> рогатый скот', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('7', 'ru', 'Свиньи', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('8', 'ru', 'Овцы и козы', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('9', 'ru', 'Расход корма <br/> на 1 голову скота', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('10','ru', 'Рентабельность <br/> живот-во', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('11', 'ru', 'Зерно', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('12', 'ru', 'Сахарная <br/> свекла', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('13', 'ru', 'Подсолнечник', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('14', 'ru', 'Картофель', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('15', 'ru', 'Овощи', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('16', 'ru', 'Фрукты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('17', 'ru', 'Продукция Сельского <br/>хозяйства всех<br/> категорий', 'место <br/>в России');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('18', 'ru', 'Количество <br/> организаций','ед.');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('19', 'ru', 'Продукция сельскохозяйственных организаций', 'место <br/>в России');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('20', 'ru', 'Из них:', 'раст-во');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('21', 'ru', 'Продукция крестьянско-фермерских хозяйств и ИП', 'место <br/>в России');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('22', 'ru', 'Из них:', 'раст-во');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('23', 'ru', NULL,'живот-во');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('24', 'ru', NULL,'живот-во');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('25', 'ru', 'Сельскохозяйст&shy;венные угодья', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('26', 'ru', 'в том числе: Пашни', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('27', 'ru', 'С/з угодья от общей площади земель', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('28', 'ru', 'Рентабельность раст-ва', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('29', 'ru', 'Минеральные <br/> удобрения','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('30', 'ru', 'Органические <br/> удобрения', NULL);
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('38', 'ru', NULL, NULL);
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('39', 'ru', 'Строительный объем', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('41', 'ru', 'Число зданий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('42', 'ru', 'Построенных:', 'населением');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('43', 'ru', '', 'организациями');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('46', 'ru', 'Людей, занятых в строительстве', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('47', 'ru', 'Число организаций', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('48', 'ru', 'Жилищные кредиты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('49', 'ru', 'Ипотечные кредиты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('50', 'ru', 'Производство древесины', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('51', 'ru', 'Число предприятий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('52', 'ru', 'Лесистость территорий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('53', 'ru', 'Численность работников', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('54', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('55', 'ru', 'Вывоз (продажа)', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('56', 'ru', 'Ввоз (покупка)', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('57', 'ru', 'Экспорт', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('58', 'ru', 'Импорт', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('59', 'ru', 'Общая площадь введенных зданий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('60', 'ru', 'Гостиницы и рестораны', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('61', 'ru', 'Оборот розничной торговли', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('62', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('63', 'ru', 'Численность населения', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('64', 'ru', 'Выше прожиточного минимума', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('65', 'ru', 'Кредиты (всего)', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('66', 'ru', 'Жилищные кредиты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('67', 'ru', 'Ипотечные кредиты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('68', 'ru', 'Потребительские кредиты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('69', 'ru', 'Оборот розничной торговли', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('70', 'ru', 'Потребительские расходы', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('71', 'ru', 'Доля продовольственных товаров', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('72', 'ru', 'Численность населения', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('73', 'ru', 'Картофель', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('74', 'ru', 'Овощи и бахчевые', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('75', 'ru', 'Сахар', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('76', 'ru', 'Масло растительное', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('77', 'ru', 'Хлебные продукты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('78', 'ru', 'Мясо и мясопродукты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('79', 'ru', 'Молоко и молочные продукты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('80', 'ru', 'Яйца и яйцепродукты', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('81', 'ru', 'Масла растительные', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('82', 'ru', 'Изделия колбасные', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('83', 'ru', 'Консервы мясные', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('84', 'ru', 'Ввоз (покупка) мясных консервов', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('85', 'ru', 'Вывоз (продажа) колбасных изделий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('86', 'ru', 'Ввоз (покупка) колбасных изделий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('87', 'ru', 'Вывоз (продажа) сыра', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('88', 'ru', 'Ввоз (покупка) сыра', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('89', 'ru', 'Вывоз (продажа) масла животного', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('90', 'ru', 'Ввоз (покупка) масла животного', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('91', 'ru', 'Вывоз (продажа) масла растительного', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('92', 'ru', 'Ввоз (покупка) масла растительного', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('93', 'ru', 'Вывоз (продажа) сахара', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('94', 'ru', 'Ввоз (покупка) сахара', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('95', 'ru', 'Вывоз (продажа) муки', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('96', 'ru', 'Ввоз (покупка) муки', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('97', 'ru', 'Вывоз (продажа) крупы', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('98', 'ru', 'Ввоз (покупка) крупы', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('99', 'ru', 'Продажа водки', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('100', 'ru', 'Продажа водки и ликеро-водочных изделий', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('101', 'ru', 'Продажа коньяка, коньячных напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('102', 'ru', 'Продажа коньяка, коньячных напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('103', 'ru', 'Продажа слабоалкогольных напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('104', 'ru', 'Продажа слабоалкогольных напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('105', 'ru', 'Продажа вина в натуральном выражении', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('106', 'ru', 'Продажа винодельческой продукции в расчете на душу населения', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('107', 'ru', 'Продажа вина игристого и шампанских напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('108', 'ru', 'Продажа шампанских и игристых вин', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('109', 'ru', 'Продажа пива, кроме пивных коктейлей и солодовых напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('110', 'ru', 'Продажа пива, кроме пивных коктейлей и солодовых напитков', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('111', 'ru', 'Продажа напитков, изготавливаемых на основе пива', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('112', 'ru', 'Продажа напитков, изготавливаемых на основе пива', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('113', 'ru', 'Валовой сбор зерновых и зернобобовых культур', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('114', 'ru', 'Валовой сбор сахарной свеклы', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('115', 'ru', 'Валовой сбор подсолнечника', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('116', 'ru', 'Валовой сбор картофеля', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('117', 'ru', 'Валовой сбор овощей', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('118', 'ru', 'Валовой сбор плодово-ягодных', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('119', 'ru', 'роизводство скота и птицы на убой', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('120', 'ru', 'Производство молока', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('121', 'ru', 'Производство яиц', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('122', 'ru', 'Производство шерсти', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('123', 'ru', 'Производство товарного меда', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('124', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('125', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('126', 'ru', 'Экспорт продовольственных товаров и сельскохозяйственного сырья', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('127', 'ru', 'Импорт продовольственных товаров и сельскохозяйственного сырья', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('128', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('129', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('130', 'ru', 'Ввоз (покупка) мяся и птицы', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('131', 'ru', 'Ввоз (покупка) мясных консервов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('132', 'ru', 'Ввоз (покупка) колбасных изделий', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('133', 'ru', 'Вывоз (продажа) мяса и птицы', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('134', 'ru', 'Вывоз (продажа) мясных консервов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('135', 'ru', 'Вывоз (продажа) колбасных изделий', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('136', 'ru', 'Ввоз (покупка) сыра', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('137', 'ru', 'Ввоз (покупка) масла животного', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('138', 'ru', 'Вывоз (продажа) сыра', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('139', 'ru', 'Вывоз (продажа) масла животного', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('140', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('141', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('142', 'ru', 'Ввоз (покупка) масла растительного', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('143', 'ru', 'Ввоз (покупка) сахара', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('144', 'ru', 'Ввоз (покупка) муки', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('145', 'ru', 'Ввоз (покупка) крупы', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('146', 'ru', 'Вывоз (продажа) масла растительного', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('147', 'ru', 'Вывоз (продажа) сахара', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('148', 'ru', 'Вывоз (продажа) муки', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('149', 'ru', 'Вывоз (продажа) крупы', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('150', 'ru', 'Число организаций', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('151', 'ru', 'Добыча топливно-энергетических полезных ископаемых', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('152', 'ru', 'Рентабельность организаций', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('153', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('154', 'ru', 'Фонд нефтяных скважин', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('155', 'ru', 'Глубокое разведочное бурение на нефть и газ', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('156', 'ru', 'Объем геологоразведочных нефти, газа и конденсата', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('157', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('158', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('159', 'ru', 'Ввоз (покупка) автомобильных бензинов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('160', 'ru', 'Ввоз (покупка) дизельного топлива', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('161', 'ru', 'Ввоз (покупка) мазута', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('162', 'ru', 'Вывоз (продажа) автомобильных бензинов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('163', 'ru', 'Вывоз (продажа) дизельного топлива', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('164', 'ru', 'Вывоз (продажа) мазута', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('165', 'ru', 'Оборот электроэнергии', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('166', 'ru', 'Инвестиции', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('167', 'ru', 'Рентабельность', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('168', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('169', 'ru', 'Мощность электростанций', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('170', 'ru', 'Производство электроэнергии', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('171', 'ru', 'Число организаций', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('172', 'ru', 'Численность работников', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('173', 'ru', 'Добыча металлов, минералов, строиматериалов', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('174', 'ru', 'Рентабельность организаций', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('175', 'ru', 'Геологоразвед. работы (металлы)', 'место');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('176', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('177', 'ru', 'Геологоразвед. работы черных металлов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('178', 'ru', 'Геологоразвед. работы благородных металлов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('179', 'ru', 'Геологоразвед. работы цветных и редких металлов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('180', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('181', 'ru', '','');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('182', 'ru', 'Ввоз (покупка) черных металлов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('183', 'ru', 'Ввоз (покупка) труб стальных', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('184', 'ru', 'Вывоз (продажа) черных металлов', '');
        INSERT INTO {{trchart}} (`trPArentId`,`langId`,`header`,`measure`) VALUES ('185', 'ru', 'Вывоз (продажа) стальных труб','');

        ALTER TABLE {{chart}} 
        DROP COLUMN `measure`,
        DROP COLUMN `header`;
        
        UPDATE {{trindustry}} SET `shortNameUrl` = 'furniture' WHERE `trParentId` = '1' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'construction' WHERE `trParentId` = '2' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'automotive-engineering' WHERE `trParentId` = '3' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'security-safety' WHERE `trParentId` = '4' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'forestry' WHERE `trParentId` = '5' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'chemicals' WHERE `trParentId` = '6' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'steel-manufacturing' WHERE `trParentId` = '7' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'energy' WHERE `trParentId` = '8' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'medical' WHERE `trParentId` = '9' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'aerospace' WHERE `trParentId` = '10' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'agriculture' WHERE `trParentId` = '11' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'veterinary' WHERE `trParentId` = '12' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'food-equipment' WHERE `trParentId` = '13' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'food-retail' WHERE `trParentId` = '14' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'beauty' WHERE `trParentId` = '16' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'food-service' WHERE `trParentId` = '17' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'electric-engineering' WHERE `trParentId` = '18' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'IT-telecommunications' WHERE `trParentId` = '19' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'jewellery' WHERE `trParentId` = '20' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'electronics' WHERE `trParentId` = '21' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'cars' WHERE `trParentId` = '22' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'art' WHERE `trParentId` = '23' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'religion' WHERE `trParentId` = '24' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'pharmaceutical' WHERE `trParentId` = '25' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'packaging' WHERE `trParentId` = '26' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'travel' WHERE `trParentId` = '27' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'innovations' WHERE `trParentId` = '28' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'logistics' WHERE `trParentId` = '29' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'governmental-services' WHERE `trParentId` = '30' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'houseware' WHERE `trParentId` = '31' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'landscape-design' WHERE `trParentId` = '32' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'recreation' WHERE `trParentId` = '35' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'education' WHERE `trParentId` = '36' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'oil-and-gas' WHERE `trParentId` = '37' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'natural-resources' WHERE `trParentId` = '38' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'children\'s-products' WHERE `trParentId` = '39' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'advertising' WHERE `trParentId` = '40' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'printing' WHERE `trParentId` = '41' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'environment' WHERE `trParentId` = '43' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'apparel' WHERE `trParentId` = '45' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'cleaning-services' WHERE `trParentId` = '46' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'celebrations' WHERE `trParentId` = '47' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'аntiques' WHERE `trParentId` = '49' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'exhibition-industry' WHERE `trParentId` = '50' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'lifting' WHERE `trParentId` = '51' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'patents' WHERE `trParentId` = '53' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'shipbuilding' WHERE `trParentId` = '54' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'publishing' WHERE `trParentId` = '55' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'business' WHERE `trParentId` = '56' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'property' WHERE `trParentId` = '59' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'optics' WHERE `trParentId` = '60' AND `langId` = 'en';
        UPDATE {{trindustry}} SET `shortNameUrl` = 'textile' WHERE `trParentId` = '61' AND `langId` = 'en';
        
        ALTER TABLE {{trchartcolumn}} 
        ADD COLUMN `unit` VARCHAR(45) NULL DEFAULT NULL AFTER `header`;

        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '19' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '20' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '21' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '22' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '23' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '24' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '25' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '26' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '27' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '28' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '29' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '30' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '31' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '32' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '33' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '34' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '35' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '36' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '37' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '38' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча тонн' WHERE `trParentId` = '39' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча тонн' WHERE `trParentId` = '40' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион штук' WHERE `trParentId` = '41' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '42' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '43' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча гектаров' WHERE `trParentId` = '44' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча гектаров' WHERE `trParentId` = '45' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '46' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'килограмм' WHERE `trParentId` = '47' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '48' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '49' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча голов' WHERE `trParentId` = '50' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча голов' WHERE `trParentId` = '51' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча голов' WHERE `trParentId` = '52' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'центнер кормовых единиц' WHERE `trParentId` = '53' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '54' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '55' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '56' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '57' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '58' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '59' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '60' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча кубических метров' WHERE `trParentId` = '61' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча кубических метров' WHERE `trParentId` = '62' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча кубических метров' WHERE `trParentId` = '63' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча кубических метров' WHERE `trParentId` = '64' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'единица' WHERE `trParentId` = '65' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '66' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '67' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '68' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '69' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '70' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '71' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '72' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '73' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '74' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '75' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '76' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча кубических метров' WHERE `trParentId` = '77' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча кубических метров' WHERE `trParentId` = '78' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'единица' WHERE `trParentId` = '79' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '80' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '81' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '82' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '83' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '84' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '85' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '86' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '87' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '88' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '89' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '90' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион условных кирпичей' WHERE `trParentId` = '91' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион условных кирпичей' WHERE `trParentId` = '92' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млрд руб' WHERE `trParentId` = '93' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млрд руб' WHERE `trParentId` = '94' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '95' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'мегаватт (тысяча киловатт)' WHERE `trParentId` = '96' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллиардов киловатт-часов' WHERE `trParentId` = '97' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '98' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '99' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '102' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '103' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млрд руб' WHERE `trParentId` = '104' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млрд руб' WHERE `trParentId` = '105' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '106' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'мегаватт (тысяча киловатт)' WHERE `trParentId` = '107' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллиардов киловатт-часов' WHERE `trParentId` = '108' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '109' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '110' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '113' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '114' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '115' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '116' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '117' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '118' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '119' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '120' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '121' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '122' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '123' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '124' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '125' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '126' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '127' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '128' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '129' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '130' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча полулитров' WHERE `trParentId` = '131' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '132' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '134' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '138' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '140' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '142' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '146' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'рублей' WHERE `trParentId` = '147' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '148' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '149' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '150' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '151' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '152' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '153' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '154' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '155' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '156' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Штука' WHERE `trParentId` = '157' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '158' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'рублей' WHERE `trParentId` = '159' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '160' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '161' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '162' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '163' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '164' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '165' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '166' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '167' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Килограмм' WHERE `trParentId` = '168' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Штука' WHERE `trParentId` = '169' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '188' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '190' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '194' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '196' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '198' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '202' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '203' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '204' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '205' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '206' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '207' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '208' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '209' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '210' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '211' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '212' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '213' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '214' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '215' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '216' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '217' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча полулитров' WHERE `trParentId` = '218' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '219' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '220' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '221' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '222' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '223' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '224' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча тонн' WHERE `trParentId` = '225' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча тонн' WHERE `trParentId` = '226' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион штук' WHERE `trParentId` = '227' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '228' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '229' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '230' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '231' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '232' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '233' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '234' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '235' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. условных банок' WHERE `trParentId` = '236' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. условных банок' WHERE `trParentId` = '237' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '238' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '239' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '240' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '241' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '242' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '243' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '244' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '245' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '246' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '247' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '248' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '249' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '250' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '251' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча квадратных метров' WHERE `trParentId` = '252' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча квадратных метров' WHERE `trParentId` = '253' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллионов рублей' WHERE `trParentId` = '254' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллионов рублей' WHERE `trParentId` = '255' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '256' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '257' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча квадратных метров' WHERE `trParentId` = '263' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллионов рублей' WHERE `trParentId` = '264' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '265' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '266' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '267' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '268' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '269' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '270' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '271' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '272' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '273' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '274' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '275' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Штука' WHERE `trParentId` = '276' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '277' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '278' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '282' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '283' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '284' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '285' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '286' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '287' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '288' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '289' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '290' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '293' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '294' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '295' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '296' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '297' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '298' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '299' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '300' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '301' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '302' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '303' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '304' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '305' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '306' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '307' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'единиц' WHERE `trParentId` = '308' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс.км' WHERE `trParentId` = '309' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '310' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '312' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '313' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион кубических метров' WHERE `trParentId` = '314' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион кубических метров' WHERE `trParentId` = '315' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '316' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '317' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '318' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '319' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '320' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '321' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'единиц' WHERE `trParentId` = '322' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс.км' WHERE `trParentId` = '323' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '324' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '326' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '327' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '328' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '329' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '330' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '331' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '332' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '333' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '334' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '335' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '336' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'чел.' WHERE `trParentId` = '337' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '338' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '339' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '340' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '341' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '342' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '343' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '344' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '345' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '346' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'чел.' WHERE `trParentId` = '347' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. м3' WHERE `trParentId` = '348' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. м3' WHERE `trParentId` = '349' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. м3' WHERE `trParentId` = '350' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. м3' WHERE `trParentId` = '351' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '352' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '353' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '354' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '355' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'м3' WHERE `trParentId` = '356' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс.м3' WHERE `trParentId` = '357' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс.м3' WHERE `trParentId` = '358' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс.м3' WHERE `trParentId` = '359' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '360' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонн' WHERE `trParentId` = '361' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '362' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '363' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '364' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '365' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '366' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '367' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '368' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '381' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '382' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '383' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '384' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '385' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '386' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '387' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '388' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион кубических метров' WHERE `trParentId` = '389' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион кубических метров' WHERE `trParentId` = '390' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '391' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '392' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '393' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '394' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '395' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс. т' WHERE `trParentId` = '396' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '397' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '398' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '399' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча тонн' WHERE `trParentId` = '400' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча кубических метров' WHERE `trParentId` = '401' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча кубических метров' WHERE `trParentId` = '402' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион условных кирпичей' WHERE `trParentId` = '403' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Миллион условных кирпичей' WHERE `trParentId` = '404' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '405' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '406' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '407' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '408' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '409' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '410' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '411' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '412' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '413' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '414' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '415' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '416' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '417' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '418' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '419' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '420' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '421' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча декалитров' WHERE `trParentId` = '422' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тысяча полулитров' WHERE `trParentId` = '423' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '424' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '425' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '426' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '427' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '428' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '429' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '430' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '431' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '432' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '433' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн руб.' WHERE `trParentId` = '434' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '435' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '436' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '437' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '438' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '439' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '440' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '441' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '442' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '443' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '444' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '445' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '' WHERE `trParentId` = '446' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '447' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '448' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '449' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '450' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '451' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '452' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '453' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '454' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '455' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тонна' WHERE `trParentId` = '456' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'Тонна' WHERE `trParentId` = '457' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '458' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '459' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '460' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '461' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '462' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'чел.' WHERE `trParentId` = '463' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '464' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тыс м³' WHERE `trParentId` = '465' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '466' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '467' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '468' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'чел.' WHERE `trParentId` = '469' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '470' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'ед.' WHERE `trParentId` = '471' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча квадратных метров' WHERE `trParentId` = '472' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '473' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '474' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'тысяча квадратных метров' WHERE `trParentId` = '475' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = 'человек' WHERE `trParentId` = '476' AND `langId` = 'ru';
        UPDATE {{trchartcolumn}} SET `unit` = '%' WHERE `trParentId` = '477' AND `langId` = 'ru';

        ALTER TABLE {{trchart}} 
        ADD COLUMN `unit` VARCHAR(45) NULL DEFAULT NULL AFTER `measure`;

        UPDATE {{trchart}} SET `unit` = 'тысяча тонн' WHERE `trParentId` = '1' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '2' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион штук' WHERE `trParentId` = '3' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонна' WHERE `trParentId` = '4' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонна' WHERE `trParentId` = '5' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча голов' WHERE `trParentId` = '6' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча голов' WHERE `trParentId` = '7' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча голов' WHERE `trParentId` = '8' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'центнер кормовых единиц' WHERE `trParentId` = '9' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '10' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '11' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '12' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '13' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '14' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '15' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '16' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '17' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'ед.' WHERE `trParentId` = '18' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '19' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '20' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '21' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '22' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '23' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '24' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча гектаров' WHERE `trParentId` = '25' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча гектаров' WHERE `trParentId` = '26' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '27' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '28' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'килограмм' WHERE `trParentId` = '29' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонна' WHERE `trParentId` = '30' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '38' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча кубических метров' WHERE `trParentId` = '39' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'единица' WHERE `trParentId` = '41' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '42' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '43' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'человек' WHERE `trParentId` = '46' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'ед.' WHERE `trParentId` = '47' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн руб.' WHERE `trParentId` = '48' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн руб.' WHERE `trParentId` = '49' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс м³' WHERE `trParentId` = '50' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'ед.' WHERE `trParentId` = '51' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '52' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'чел.' WHERE `trParentId` = '53' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '54' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. м3' WHERE `trParentId` = '55' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. м3' WHERE `trParentId` = '56' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '57' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '58' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча квадратных метров' WHERE `trParentId` = '59' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллионов рублей' WHERE `trParentId` = '60' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '61' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '62' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'человек' WHERE `trParentId` = '63' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '64' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн руб.' WHERE `trParentId` = '65' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн руб.' WHERE `trParentId` = '66' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн руб.' WHERE `trParentId` = '67' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн руб.' WHERE `trParentId` = '68' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион рублей' WHERE `trParentId` = '69' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'рублей' WHERE `trParentId` = '70' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '71' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'человек' WHERE `trParentId` = '72' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '73' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '74' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '75' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '76' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '77' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '78' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '79' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Штука' WHERE `trParentId` = '80' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '81' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '82' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. условных банок' WHERE `trParentId` = '83' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. условных банок' WHERE `trParentId` = '84' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '85' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '86' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '87' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '88' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '89' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '90' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '91' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '92' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '93' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '94' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '95' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '96' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '97' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '98' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '99' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '100' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '101' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '102' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '103' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '104' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '105' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '106' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '107' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '108' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '109' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '110' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча декалитров' WHERE `trParentId` = '111' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'литр (кубический дециметр)' WHERE `trParentId` = '112' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '113' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '114' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '115' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Килограмм' WHERE `trParentId` = '116' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '117' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча центнеров' WHERE `trParentId` = '118' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча тонн' WHERE `trParentId` = '119' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'Тонна' WHERE `trParentId` = '120' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллион штук' WHERE `trParentId` = '121' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонна' WHERE `trParentId` = '122' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонна' WHERE `trParentId` = '123' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '124' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '125' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '126' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млн долларов США' WHERE `trParentId` = '127' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '128' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '129' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '130' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. условных банок' WHERE `trParentId` = '131' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '132' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '133' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. условных банок' WHERE `trParentId` = '134' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '135' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '136' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '137' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '138' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '139' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '140' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '141' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '142' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '143' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '144' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '145' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '146' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '147' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '148' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тонн' WHERE `trParentId` = '149' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'ед.' WHERE `trParentId` = '150' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '151' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '152' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '153' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'единиц' WHERE `trParentId` = '154' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс.км' WHERE `trParentId` = '155' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '156' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '157' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '158' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '159' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '160' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '161' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '162' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '163' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '164' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млрд руб' WHERE `trParentId` = '165' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'млрд руб' WHERE `trParentId` = '166' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '167' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '168' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'мегаватт (тысяча киловатт)' WHERE `trParentId` = '169' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'миллиардов киловатт-часов' WHERE `trParentId` = '170' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'ед.' WHERE `trParentId` = '171' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'человек' WHERE `trParentId` = '172' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '173' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '%' WHERE `trParentId` = '174' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '175' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '176' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '177' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '178' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тысяча рублей' WHERE `trParentId` = '179' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '180' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = '' WHERE `trParentId` = '181' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '182' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '183' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '184' AND `langId` = 'ru';
        UPDATE {{trchart}} SET `unit` = 'тыс. т' WHERE `trParentId` = '185' AND `langId` = 'ru';
        
        CREATE TABLE {{trgstattypegroup}} (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `trParentId` INT(11) NULL DEFAULT NULL,
          `langId` VARCHAR(45) NULL DEFAULT NULL,
          `unit` VARCHAR(45) NULL DEFAULT NULL,
          PRIMARY KEY (`id`));

        ALTER TABLE {{trgstattypegroup}} 
        ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);

        ALTER TABLE {{trgstattypegroup}} 
        ADD CONSTRAINT `fk_trgstattypegroup_gstattypegroup`
          FOREIGN KEY (`trParentId`)
          REFERENCES {{gstattypegroup}} (`id`)
          ON DELETE CASCADE
          ON UPDATE CASCADE;

        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('1', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('2', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('3', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('4', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('5', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('6', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('7', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('8', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('9', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('10', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('11', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('14', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('17', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('18', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('19', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('20', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('21', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('22', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('23', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('24', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('25', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('26', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('27', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('28', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('29', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('30', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('31', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('32', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('33', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('34', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('35', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('36', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('37', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('38', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('39', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('40', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('41', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('42', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('43', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('44', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('45', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('46', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('47', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('48', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('49', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('50', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('51', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('52', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('53', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('54', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('55', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('56', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('57', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('58', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('59', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('60', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('61', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('62', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('63', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('64', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('65', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('66', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('67', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('68', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('69', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('70', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('71', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('72', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('73', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('74', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('75', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('76', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('77', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('78', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('79', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('80', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('81', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('82', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('83', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('84', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('85', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('86', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('87', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('88', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('89', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('90', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('91', 'ru', 'Доллар США');
        INSERT INTO {{trgstattypegroup}} (`trParentId`,`langId`,`unit`) VALUES ('92', 'ru', 'Доллар США');
        
        ALTER TABLE {{gstattypegroup}} 
        DROP COLUMN `unit`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}