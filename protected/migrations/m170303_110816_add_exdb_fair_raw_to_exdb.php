<?php

class m170303_110816_add_exdb_fair_raw_to_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);

        return "
            DROP TABLE IF EXISTS {$expodata}.{{exdbfair}};
            CREATE TABLE {$expodata}.{{exdbfair}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` BIGINT(20) NULL DEFAULT NULL,
              `exdbFrequency` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `name_de` TEXT NULL DEFAULT NULL,
              `exhibitionComplexId` INT(11) NULL DEFAULT NULL,
              `beginDate` DATE NULL DEFAULT NULL,
              `endDate` DATE NULL DEFAULT NULL,
              `beginMountingDate` DATE NULL DEFAULT NULL,
              `endMountingDate` DATE NULL DEFAULT NULL,
              `beginDemountingDate` DATE NULL DEFAULT NULL,
              `endDemountingDate` DATE NULL DEFAULT NULL,
              `exdbBusinessSectors` TEXT NULL DEFAULT NULL,
              `exdbCosts` TEXT NULL DEFAULT NULL,
              `exdbShowType` TEXT NULL DEFAULT NULL,
              `site` TEXT NULL DEFAULT NULL,
              `storyId` TEXT NULL DEFAULT NULL,
              `infoId` TEXT NULL DEFAULT NULL,
              `exdbRaw` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            ALTER TABLE {$db}.{{fair}} 
            ADD COLUMN `exdbRaw` TEXT NULL DEFAULT NULL AFTER `exdbShowType`;

            DROP PROCEDURE IF EXISTS {$expodata}.`add_fairstatistic_countries_count_to_exdb_db`;
            
            DELETE FROM {$expodata}.{{exdbfairhasassociation}};
            DELETE FROM {$expodata}.{{exdbfairhasaudit}};
            DELETE FROM {$expodata}.{{exdbfairhasorganizer}};
            DELETE FROM {$expodata}.{{exdbfairstatistic}};
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairs_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairs_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_beginDate DATE DEFAULT '1970-01-01';
                DECLARE f_endDate DATE DEFAULT '1970-01-01';
                DECLARE f_beginMountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_endMountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_beginDemountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_endDemountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_crc BIGINT DEFAULT 0;
                DECLARE f_name TEXT DEFAULT '';
                DECLARE f_name_de TEXT DEFAULT '';
                DECLARE f_frequency TEXT DEFAULT '';
                DECLARE f_exhibitioncomplex_id INT DEFAULT 0;
                DECLARE f_business_sectors TEXT DEFAULT '';
                DECLARE f_costs TEXT DEFAULT '';
                DECLARE f_show_type TEXT DEFAULT '';
                DECLARE f_further_information TEXT DEFAULT '';
                DECLARE f_story_id TEXT DEFAULT '';
				DECLARE f_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT s.id, 
                                        s.beginDate, 
                                        s.endDate,
                                        s.beginMountingDate,
                                        s.endMountingDate,
                                        s.beginDemountingDate,
                                        s.endDemountingDate, 
                                        CRC32(CONCAT(s.id, s.beginDate)) AS exdbCrc,
                                        ed.name_en,
										ed.name_de,
                                        ed.frequency_en,
                                        ec.id as exhibitionComplexId,
                                        ed.business_sectors_en,
                                        ed.costs_en,
                                        ed.show_type_en,
                                        ed.further_information_en,
                                        ed.id AS storyId,
										ed.raw_en AS raw
                                         FROM
                                        (SELECT p.id,
                                        CASE WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0 
                                                    AND p.beginDateYear IS NOT NULL AND p.beginDateYear != 0) 
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.beginDateMonth,'-',p.beginDateYear), '%e-%c-%Y')
                                             
                                             WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0 
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0) 
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.beginDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             
                                             WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.endDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginDate,
                                        
                                        CASE WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.endDateYear IS NOT NULL AND p.endDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.endDateMonth,'-',p.endDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.endDateMonth,'-',p.fyear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.beginDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endDate,
                                        
                                        CASE WHEN (p.beginMountingDateDay IS NOT NULL AND p.beginMountingDateDay != 0
                                                    AND p.beginMountingDateMonth IS NOT NULL AND p.beginMountingDateMonth != 0
                                                    AND p.beginMountingDateYear IS NOT NULL AND p.beginMountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginMountingDateDay,'-',p.beginMountingDateMonth,'-',p.beginMountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.beginMountingDateDay IS NOT NULL AND p.beginMountingDateDay != 0
                                                    AND p.beginMountingDateMonth IS NOT NULL AND p.beginMountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginMountingDateDay,'-',p.beginMountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginMountingDate,
                                        
                                        CASE WHEN (p.endMountingDateDay IS NOT NULL AND p.endMountingDateDay != 0
                                                    AND p.endMountingDateMonth IS NOT NULL AND p.endMountingDateMonth != 0
                                                    AND p.endMountingDateYear IS NOT NULL AND p.endMountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endMountingDateDay,'-',p.endMountingDateMonth,'-',p.endMountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endMountingDateDay IS NOT NULL AND p.endMountingDateDay != 0
                                                    AND p.endMountingDateMonth IS NOT NULL AND p.endMountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endMountingDateDay,'-',p.endMountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endMountingDate,
                                        
                                        CASE WHEN (p.beginDemountingDateDay IS NOT NULL AND p.beginDemountingDateDay != 0
                                                    AND p.beginDemountingDateMonth IS NOT NULL AND p.beginDemountingDateMonth != 0
                                                    AND p.beginDemountingDateYear IS NOT NULL AND p.beginDemountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDemountingDateDay,'-',p.beginDemountingDateMonth,'-',p.beginDemountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.beginDemountingDateDay IS NOT NULL AND p.beginDemountingDateDay != 0
                                                    AND p.beginDemountingDateMonth IS NOT NULL AND p.beginDemountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDemountingDateDay,'-',p.beginDemountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginDemountingDate,
                                        
                                        CASE WHEN (p.endDemountingDateDay IS NOT NULL AND p.endDemountingDateDay != 0
                                                    AND p.endDemountingDateMonth IS NOT NULL AND p.endDemountingDateMonth != 0
                                                    AND p.endDemountingDateYear IS NOT NULL AND p.endDemountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDemountingDateDay,'-',p.endDemountingDateMonth,'-',p.endDemountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDemountingDateDay IS NOT NULL AND p.endDemountingDateDay != 0
                                                    AND p.endDemountingDateMonth IS NOT NULL AND p.endDemountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDemountingDateDay,'-',p.endDemountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endDemountingDate
                                        FROM 
                                        (SELECT q.id, 
                                        q.fyear,
                                        
                                        q.beginDate,
                                        DAY(STR_TO_DATE(q.beginDate, '%e')) AS beginDateDay,
                                        MONTH(STR_TO_DATE(q.beginDate, '%e%b')) AS beginDateMonth,
                                        YEAR(STR_TO_DATE(q.beginDate, '%e%b%Y')) AS beginDateYear,
                                        
                                        q.endDate,
                                        DAY(STR_TO_DATE(q.endDate, '%e')) AS endDateDay,
                                        MONTH(STR_TO_DATE(q.endDate, '%e%b')) AS endDateMonth,
                                        YEAR(STR_TO_DATE(q.endDate, '%e%b%Y')) AS endDateYear,
                                        
                                        q.beginMountingDate,
                                        DAY(STR_TO_DATE(q.beginMountingDate, '%e')) AS beginMountingDateDay,
                                        MONTH(STR_TO_DATE(q.beginMountingDate, '%e%b')) AS beginMountingDateMonth,
                                        YEAR(STR_TO_DATE(q.beginMountingDate, '%e%b%Y')) AS beginMountingDateYear,
                                        
                                        q.endMountingDate,
                                        DAY(STR_TO_DATE(q.endMountingDate, '%e')) AS endMountingDateDay,
                                        MONTH(STR_TO_DATE(q.endMountingDate, '%e%b')) AS endMountingDateMonth,
                                        YEAR(STR_TO_DATE(q.endMountingDate, '%e%b%Y')) AS endMountingDateYear,
                                        
                                        q.beginDemountingDate,
                                        DAY(STR_TO_DATE(q.beginDemountingDate, '%e')) AS beginDemountingDateDay,
                                        MONTH(STR_TO_DATE(q.beginDemountingDate, '%e%b')) AS beginDemountingDateMonth,
                                        YEAR(STR_TO_DATE(q.beginDemountingDate, '%e%b%Y')) AS beginDemountingDateYear,
                                        
                                        q.endDemountingDate,
                                        DAY(STR_TO_DATE(q.endDemountingDate, '%e')) AS endDemountingDateDay,
                                        MONTH(STR_TO_DATE(q.endDemountingDate, '%e%b')) AS endDemountingDateMonth,
                                        YEAR(STR_TO_DATE(q.endDemountingDate, '%e%b%Y')) AS endDemountingDateYear,
                                        
                                        q.tHeader,
                                        q.tRow
                                        FROM
                                        (SELECT d.id, REPLACE(d.fyear,'','') AS fyear, 
                                        REPLACE(SUBSTRING_INDEX(d.showDates, '-', 1), '.', '') AS beginDate,
                                        REPLACE(SUBSTR(REPLACE(d.showDates, SUBSTRING_INDEX(d.showDates, '-', 1), ''), 2), '.', '') endDate,
                                        SUBSTRING_INDEX(d.buildUp, '-', 1) AS beginMountingDate,
                                        SUBSTR(REPLACE(d.buildUp, SUBSTRING_INDEX(d.buildUp, '-', 1), ''), 2) AS endMountingDate,
                                        SUBSTRING_INDEX(d.dismantling, '-', 1) AS beginDemountingDate,
                                        SUBSTR(REPLACE(d.dismantling, SUBSTRING_INDEX(d.dismantling, '-', 1), ''), 2) AS endDemountingDate,
                                        d.tHeader,
                                        d.tRow
                                        FROM
                                        (SELECT id, row_1 AS tHeader, row_2 AS tRow, SUBSTR(row_2, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_2, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',2), SUBSTRING_INDEX(ed.dates_en,'\n',1),'') AS `row_2`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_3 AS tRow, SUBSTR(row_3, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_3, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',3), SUBSTRING_INDEX(ed.dates_en,'\n',2),'') AS `row_3`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_4 AS tRow, SUBSTR(row_4, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_4, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',4), SUBSTRING_INDEX(ed.dates_en,'\n',3),'') AS `row_4`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_5 AS tRow, SUBSTR(row_5, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_5, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',5), SUBSTRING_INDEX(ed.dates_en,'\n',4),'') AS `row_5`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_6 AS tRow, SUBSTR(row_6, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_6, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',6), SUBSTRING_INDEX(ed.dates_en,'\n',5),'') AS `row_6`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_7 AS tRow, SUBSTR(row_7, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_7, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',7), SUBSTRING_INDEX(ed.dates_en,'\n',6),'') AS `row_7`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_8 AS tRow, SUBSTR(row_8, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_8, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',8), SUBSTRING_INDEX(ed.dates_en,'\n',7),'') AS `row_8`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_9 AS tRow, SUBSTR(row_9, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_9, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',9), SUBSTRING_INDEX(ed.dates_en,'\n',8),'') AS `row_9`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_10 AS tRow, SUBSTR(row_10, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_10, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',10), SUBSTRING_INDEX(ed.dates_en,'\n',9),'') AS `row_10`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_11 AS tRow, SUBSTR(row_11, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_11, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',11), SUBSTRING_INDEX(ed.dates_en,'\n',10),'') AS `row_11`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        ) d) q) p where p.fyear != '') s 
                                        LEFT JOIN {$expodata}.{{exdbfair}} f ON f.exdbCrc = CRC32(CONCAT(s.id, s.beginDate)) AND f.exdbId = s.id
                                        LEFT JOIN {$expodataRaw}.expo_data ed ON ed.id = s.id
                                        LEFT JOIN {$expodataRaw}.expo_ground gr ON ed.expo_loc_id = gr.id
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = gr.id
                                        WHERE f.id IS NULL
                                        ORDER BY s.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, 
                        f_beginDate, 
                        f_endDate, 
                        f_beginMountingDate, 
                        f_endMountingDate, 
                        f_beginDemountingDate, 
                        f_endDemountingDate, 
                        f_crc, 
                        f_name,
						f_name_de,
                        f_frequency,
                        f_exhibitioncomplex_id,
                        f_business_sectors,
                        f_costs,
                        f_show_type,
                        f_further_information,
                        f_story_id,
						f_raw;
                    
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfair}} (
                            `exdbId`,
                            `exdbCrc`,
                            `exdbFrequency`,
                            `name`,
                            `name_de`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                            `exdbBusinessSectors`,
                            `exdbCosts`,
                            `exdbShowType`,
                            `site`,
                            `storyId`,
							`exdbRaw`) 
                            VALUES (
                            f_id,
                            f_crc,
                            f_frequency,
                            f_name,
							f_name_de,
                            f_exhibitioncomplex_id,
                            f_beginDate,
                            f_endDate,
                            f_beginMountingDate,
                            f_endMountingDate,
                            f_beginDemountingDate,
                            f_endDemountingDate,
                            f_business_sectors,
                            f_costs,
                            f_show_type,
                            f_further_information,
                            f_story_id,
							f_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairs_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairs_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_exdb_id INT DEFAULT 0;
                DECLARE f_exdb_crc BIGINT DEFAULT 0;
                DECLARE f_exdb_frequency TEXT DEFAULT '';
                DECLARE f_name TEXT DEFAULT '';
                DECLARE f_name_de TEXT DEFAULT '';
                DECLARE f_exhibition_complex_id INT DEFAULT '';
                DECLARE f_begin_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_date DATE DEFAULT '1970-01-01';
                DECLARE f_begin_mounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_mounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_begin_demounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_demounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_exdb_business_sectors TEXT DEFAULT '';
                DECLARE f_exdb_costs TEXT DEFAULT '';
                DECLARE f_exdb_show_type TEXT DEFAULT '';
                DECLARE f_site TEXT DEFAULT '';
                DECLARE f_story_id INT DEFAULT 0;
                DECLARE f_area_special_expositions INT DEFAULT 0;
                DECLARE f_square_net INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_exhibitors_foreign INT DEFAULT 0;
                DECLARE f_exhibitors_local INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_visitors_foreign INT DEFAULT 0;
                DECLARE f_visitors_local INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statistics_date TEXT DEFAULT '';
                DECLARE f_countries_count TEXT DEFAULT '';
				DECLARE f_raw TEXT DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT 
                                        ef.exdbId, 
                                        ef.exdbCrc, 
                                        ef.exdbFrequency, 
                                        ef.name, 
                                        ef.name_de, 
                                        ec.id AS exhibitionComplexId,
                                        ef.beginDate,
                                        ef.endDate,
                                        ef.beginMountingDate,
                                        ef.endMountingDate,
                                        ef.beginDemountingDate,
                                        ef.endDemountingDate,
                                        ef.exdbBusinessSectors,
                                        ef.exdbCosts,
                                        ef.exdbShowType,
                                        ef.site,
                                        ef.storyId,
                                        efs.areaSpecialExpositions,
                                        efs.squareNet,
                                        efs.members,
                                        efs.exhibitorsForeign,
                                        efs.exhibitorsLocal,
                                        efs.visitors,
                                        efs.visitorsForeign,
                                        efs.visitorsLocal,
                                        efs.statistics,
                                        efs.statisticsDate,
                                        efs.exdbCountriesCount,
										ef.exdbRaw
                                        FROM {$expodata}.{{exdbfair}} ef
                                            LEFT JOIN {$expodata}.{{exdbfairstatistic}} efs ON efs.id = ef.infoId
                                            LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} eec ON eec.id = ef.exhibitionComplexId
                                            LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON ec.exdbId = eec.exdbId
                                            LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((f.exdbCrc IS NOT NULL AND f.exdbCrc = ef.exdbCrc) OR f.exdbCrc IS NULL)
                                        WHERE f.id IS NULL
                                        ORDER BY ef.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_exdb_id,
                                    f_exdb_crc,
                                    f_exdb_frequency,
                                    f_name,
                                    f_name_de,
                                    f_exhibition_complex_id,
                                    f_begin_date,
                                    f_end_date,
                                    f_begin_mounting_date,
                                    f_end_mounting_date,
                                    f_begin_demounting_date,
                                    f_end_demounting_date,
                                    f_exdb_business_sectors,
                                    f_exdb_costs,
                                    f_exdb_show_type,
                                    f_site,
                                    f_story_id,
                                    f_area_special_expositions,
                                    f_square_net,
                                    f_members,
                                    f_exhibitors_foreign,
                                    f_exhibitors_local,
                                    f_visitors,
                                    f_visitors_foreign,
                                    f_visitors_local,
                                    f_statistics,
                                    f_statistics_date,
                                    f_countries_count,
									f_raw;
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != f_exdb_id THEN
                        START TRANSACTION;
                            INSERT INTO {$db}.{{fairinfo}} (`exdbFairId`,
                                `areaSpecialExpositions`,
                                `squareNet`,
                                `members`,
                                `exhibitorsForeign`,
                                `exhibitorsLocal`,
                                `visitors`,
                                `visitorsForeign`,
                                `visitorsLocal`,
                                `statistics`,
                                `exdbStatisticsDate`,
                                `exdbCountriesCount`) VALUES (f_exdb_id,
                                f_area_special_expositions,
                                f_square_net,
                                f_members,
                                f_exhibitors_foreign,
                                f_exhibitors_local,
                                f_visitors,
                                f_visitors_foreign,
                                f_visitors_local,
                                f_statistics,
                                f_statistics_date,
                                f_countries_count);
                        COMMIT;
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := f_exdb_id;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fair}} (`exdbId`,
                            `exdbCrc`,
                            `exdbFrequency`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                            `exdbBusinessSectors`,
                            `exdbCosts`,
                            `exdbShowType`,
                            `site`,
                            `storyId`,
                            `infoId`,
							`exdbRaw`) VALUES (f_exdb_id,
                            f_exdb_crc,
                            f_exdb_frequency,
                            f_exhibition_complex_id,
                            f_begin_date,
                            f_end_date,
                            f_begin_mounting_date,
                            f_end_mounting_date,
                            f_begin_demounting_date,
                            f_end_demounting_date,
                            f_exdb_business_sectors,
                            f_exdb_costs,
                            f_exdb_show_type,
                            f_site,
                            f_story_id,
                            @last_insert_id,
							f_raw);
                    COMMIT;
                    
                    SET @fair_last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name, 'ru');
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name, 'en');
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name_de, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}