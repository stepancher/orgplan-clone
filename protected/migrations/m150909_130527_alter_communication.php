<?php

class m150909_130527_alter_communication extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{communication}} CHANGE `powerSupply` `powerCapacity`  float DEFAULT NULL;
			ALTER TABLE {{communication}} CHANGE `compressedAirSupply` `telecommunication`  int(11) DEFAULT NULL;
			ALTER TABLE {{communication}} CHANGE `heatingSupply` `audioVisual`  int(11) DEFAULT NULL;
			ALTER TABLE {{communication}} CHANGE `airSupply` `officeEquipment`  int(11) DEFAULT NULL;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAlterTable(){
		return '
			ALTER TABLE {{communication}} CHANGE `powerCapacity` `powerSupply` float DEFAULT NULL;
			ALTER TABLE {{communication}} CHANGE `telecommunication` `compressedAirSupply` int(11) DEFAULT NULL;
			ALTER TABLE {{communication}} CHANGE `audioVisual` `heatingSupply` int(11) DEFAULT NULL;
			ALTER TABLE {{communication}} CHANGE `officeEquipment` `airSupply` int(11) DEFAULT NULL;
		';
	}
}