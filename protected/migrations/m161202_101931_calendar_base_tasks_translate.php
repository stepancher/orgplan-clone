<?php

class m161202_101931_calendar_base_tasks_translate extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */

    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Develop the concept of participation in the exhibition, considering the setgoal' WHERE `id`='1834';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Sign the exhibition space lease agreement with the Organizer' WHERE `id`='1835';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a request for placement of outdoor, media, and indoor advertising' WHERE `id`='1836';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a request for placement of information about the company on the exhibition website and in the official catalog' WHERE `id`='1837';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a request for premises and equipment for the event execution in the exhibition framework' WHERE `id`='1838';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Select a contractor, and in case of customs clearance sign a contract for freight forwarding' WHERE `id`='1839';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Select a contractor and sign a contract for the exhibit items delivery to the stand' WHERE `id`='1840';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Order passes / badges for employees and the passes for transport to the exhibition' WHERE `id`='1841';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Obtain accreditation from the organizing committee of the exhibition' WHERE `id`='1842';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Approve the list of staff at the stand,order the hotel rooms, and air / railway tickets' WHERE `id`='1843';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Select a contractor and sign a contract with the catering office for the food supply to the stand visitors and staff' WHERE `id`='1844';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a list of products for purchase for the stand' WHERE `id`='1845';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Buy products for the stand' WHERE `id`='1846';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a checklist of the items necessary for the stand operation' WHERE `id`='1847';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Select a contractor and sign a contract for the provision of hired staff (interpreters, hostesses, actors, masters ...)' WHERE `id`='1848';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Define the stand concept based on the scope of participation and make a technical specification for the stand' WHERE `id`='1849';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Select a contractor and make a stand project or make an application for the stand construction through the Organizer' WHERE `id`='1850';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Order from the Organizer the connection of necessary communications: electricity, water, internet, etc.' WHERE `id`='1851';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Order multimedia equipment for the stand' WHERE `id`='1852';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Develop a program of your participation promotion and select the channels of communication with the visitors before and during the exhibition' WHERE `id`='1853';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Prepare training for employees' WHERE `id`='1854';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Provide training for employees' WHERE `id`='1855';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Prepare a questionnaire for theregistration of visitors at the stand' WHERE `id`='1856';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a message for newsletter and develop electronic / printed invitation' WHERE `id`='1857';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Start an advertising campaign for your participation in the exhibition: news in social networks, newsletter, announcements in the media' WHERE `id`='1858';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Order POS-materials / gifts / promotional materials' WHERE `id`='1859';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Purchase / fabricate branded clothing for the stand staff and employees' WHERE `id`='1860';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Make a list of the stand visitors after the exhibition' WHERE `id`='1861';
            UPDATE {{trcalendarfairtaskscommon}} SET `name`='Send a thank-you letter' WHERE `id`='1862';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}