<?php

class m170213_082938_add_exdb_organizer_associations extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6651', 'TEA');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6665', 'MATSO');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6581', 'EEAA');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6644', 'SACEOS');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6580', 'AOCA');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('18156', 'AAXO');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('7519', 'TECA');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6606', 'JExA');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6616', 'MACEOS');
            INSERT INTO {{exdbassociation}} (`exdbId`,`name`) VALUES ('6654', 'TFYD');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}