<?php

class m161026_073806_add_new_documents_to_10943 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{documents}} WHERE `id`='34';
            DELETE FROM {{documents}} WHERE `id`='35';
            DELETE FROM {{documents}} WHERE `id`='36';
            DELETE FROM {{documents}} WHERE `id`='37';

            UPDATE {{trdocuments}} SET `name`='Правила выставки', `value`='/static/documents/fairs/10943/rules.pdf' WHERE `id`='58';
            UPDATE {{trdocuments}} SET `name`='Requirements and Rules-rus', `value`='/static/documents/fairs/10943/rules.pdf' WHERE `id`='59';
            UPDATE {{trdocuments}} SET `name`='Положение о конкурсе', `value`='/static/documents/fairs/10943/competition.pdf' WHERE `id`='60';
            UPDATE {{trdocuments}} SET `name`='Contest terms & conditions', `value`='/static/documents/fairs/10943/competition.pdf' WHERE `id`='61';
            UPDATE {{trdocuments}} SET `name`='Схема проезда', `value`='/static/documents/fairs/10943/scheme.docx' WHERE `id`='62';
            UPDATE {{trdocuments}} SET `name`='Passage scheme', `value`='/static/documents/fairs/10943/scheme.docx' WHERE `id`='63';
            UPDATE {{trdocuments}} SET `name`='Инструкция для экспонента', `value`='/static/documents/fairs/10943/instruction.pdf' WHERE `id`='64';
            UPDATE {{trdocuments}} SET `name`='User manual', `value`='/static/documents/fairs/10943/instruction.pdf' WHERE `id`='65';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}