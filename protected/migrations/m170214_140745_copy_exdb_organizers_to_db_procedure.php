<?php

class m170214_140745_copy_exdb_organizers_to_db_procedure extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);
        
        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_organizer_company_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_organizer_company_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT exdbo.id, exdbtro.name, exdbtro.langId 
                                        FROM {$expodata}.{{exdborganizercompany}} exdbo
                                            LEFT JOIN {$expodata}.{{exdbtrorganizercompany}} exdbtro ON exdbtro.trParentId = exdbo.id
                                            LEFT JOIN {$db}.{{trorganizercompany}} tro ON tro.name = exdbtro.name AND tro.langId = exdbtro.langId
                                        WHERE exdbtro.name IS NOT NULL AND exdbtro.name != '' AND tro.id IS NULL
                                        GROUP BY exdbtro.name, exdbtro.langId ORDER BY exdbo.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO exdb_id, exdb_name, exdb_lang_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != exdb_id THEN
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{organizercompany}} (`active`) VALUES (NULL);
                        COMMIT;
                        
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := exdb_id;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, exdb_name, exdb_lang_id);
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}