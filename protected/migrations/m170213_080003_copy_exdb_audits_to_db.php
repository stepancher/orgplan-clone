<?php

class m170213_080003_copy_exdb_audits_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `copy_exdb_audits_to_db`;
            
            CREATE PROCEDURE `copy_exdb_audits_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_active INT DEFAULT 0;
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE temp_name TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT a.id, a.active, tra.name, tra.langId FROM {{exdbaudit}} a
                                            LEFT JOIN {{exdbtraudit}} tra ON tra.trParentId = a.id
                                            LEFT JOIN {{traudit}} traud ON traud.langId = tra.langId AND traud.name = tra.name
                                        WHERE traud.id IS NULL
                                        GROUP BY tra.name, tra.langId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO exdb_id, exdb_active, exdb_name, exdb_lang_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_name != exdb_name THEN
                        
                        START TRANSACTION;
                            INSERT INTO {{audit}} (`active`) VALUES (exdb_active);
                        COMMIT;
                        
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_name := exdb_name;
                    END IF;
                
                    START TRANSACTION;
                        INSERT INTO {{traudit}} (`name`,`trParentId`,`langId`) VALUES (exdb_name, @last_insert_id, exdb_lang_id);
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS `copy_exdb_audits_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}