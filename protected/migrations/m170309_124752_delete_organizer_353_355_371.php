<?php

class m170309_124752_delete_organizer_353_355_371 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{organizercompany}} WHERE `id` = '353';
            DELETE FROM {{organizercompany}} WHERE `id` = '355';
            DELETE FROM {{organizercompany}} WHERE `id` = '371';
            
            DELETE FROM {{organizer}} WHERE `id` = '74';
            DELETE FROM {{organizer}} WHERE `id` = '1313';
            DELETE FROM {{organizer}} WHERE `id` = '1314';
            DELETE FROM {{organizer}} WHERE `id` = '3012';
            
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17288';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17289';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17290';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17291';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17292';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17293';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17294';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17295';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17296';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17297';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17298';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17299';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17300';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17301';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17302';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17303';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17304';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17305';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17306';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '17307';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29940';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29941';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29942';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29943';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29944';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29945';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29946';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29947';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29948';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29949';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29950';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29951';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29952';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29953';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29954';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29955';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29956';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29957';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29958';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29959';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29960';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29961';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29962';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29963';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29964';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29965';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29966';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29967';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29968';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29969';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29970';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29971';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29972';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29973';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29974';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29975';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29976';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29977';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29978';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '29979';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43131';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43132';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43133';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43134';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43135';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43136';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43137';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43138';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43139';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '43140';
            
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '662';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2070';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2071';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3249';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3250';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3251';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3252';
            
            DELETE FROM {{fair}} WHERE `id` = '835';
            DELETE FROM {{fair}} WHERE `id` = '3374';
            DELETE FROM {{fair}} WHERE `id` = '3376';
            DELETE FROM {{fair}} WHERE `id` = '4985';
            DELETE FROM {{fair}} WHERE `id` = '4987';
            DELETE FROM {{fair}} WHERE `id` = '4989';
            DELETE FROM {{fair}} WHERE `id` = '4991';
            
            DELETE FROM {{fairhasindustry}} WHERE `id` = '16046';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '16047';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '16048';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '16049';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '27731';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '55484';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '55485';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '56294';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}