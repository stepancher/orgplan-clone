<?php

class m161130_134328_updated_proposal_hints extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trattributeclass}} SET `label`='This form should be filled in until November 29, 2017. Please return the application form by fax +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='2899';
            UPDATE {{trattributeclass}} SET `label`='This form should be filled in until November 11, 2017. Please return the application form by fax +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='2539';
            UPDATE {{trattributeclass}} SET `label`='This form should be filled in until October 16, 2017. Please return the application form by fax +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='2451';
            UPDATE {{trattributeclass}} SET `label`='This form should be filled in until October 21, 2017. Please return the application form by fax +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='1024';
            UPDATE {{trattributeclass}} SET `label`='This form should be filled in until October 21, 2017. Please return the application form by fax +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='1235';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}