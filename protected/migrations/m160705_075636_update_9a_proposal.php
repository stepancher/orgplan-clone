<?php

class m160705_075636_update_9a_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('24', 'en', 'APPLICATION No.8', 'INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_zone_header', 'int', '0', '2625', '2625', '30', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone_input1', 'int', '0', '2625', '2625', '40', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone_input2', 'int', '0', '2625', '2625', '50', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone_input3', 'int', '0', '2625', '2625', '60', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone_hint2', 'int', '0', '2625', '2625', '70', 'hintWithView', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone_hint3', 'int', '0', '2625', '2625', '80', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_parking', 'int', '0', '0', '0', '10', 'headerH1', '0');
			UPDATE {{attributeclass}} SET `super`='2644' WHERE `id`='2644';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_input1', 'int', '0', '2644', '2644', '20', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_input2', 'int', '0', '2644', '2644', '30', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_input3', 'int', '0', '2644', '2644', '40', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_parking_checkbox', 'int', '0', '2644', '2644', '50', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_parking_date', 'string', '0', '2644', '2644', '60', 'datePicker', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_parking_input4', 'int', '0', '2644', '2644', '70', 'coefficient', '0');
			UPDATE {{attributeclass}} SET `name`='n9a_parking_day1' WHERE `id`='2648';
			UPDATE {{attributeclass}} SET `name`='n9a_parking_day1_date', `parent`='2648', `priority`='10' WHERE `id`='2649';
			UPDATE {{attributeclass}} SET `name`='n9a_parking_day1_input', `parent`='2648', `priority`='20' WHERE `id`='2650';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day2', 'int', '0', '2644', '2644', '50', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day2_date', 'string', '0', '2648', '2644', '10', 'datePicker', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day2_input', 'int', '0', '2648', '2644', '20', 'coefficient', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day3', 'int', '0', '2644', '2644', '50', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day3_date', 'string', '0', '2648', '2644', '10', 'datePicker', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day3_input', 'int', '0', '2648', '2644', '20', 'coefficient', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day4', 'int', '0', '2644', '2644', '50', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day4_date', 'string', '0', '2648', '2644', '10', 'datePicker', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_day4_input', 'int', '0', '2648', '2644', '20', 'coefficient', '0');
			UPDATE {{attributeclass}} SET `priority`='60' WHERE `id`='2651';
			UPDATE {{attributeclass}} SET `parent`='2651' WHERE `id`='2652';
			UPDATE {{attributeclass}} SET `parent`='2651' WHERE `id`='2653';
			UPDATE {{attributeclass}} SET `priority`='70' WHERE `id`='2654';
			UPDATE {{attributeclass}} SET `parent`='2654' WHERE `id`='2655';
			UPDATE {{attributeclass}} SET `parent`='2654' WHERE `id`='2656';
			UPDATE {{attributeclass}} SET `priority`='80' WHERE `id`='2657';
			UPDATE {{attributeclass}} SET `parent`='2658' WHERE `id`='2658';
			UPDATE {{attributeclass}} SET `parent`='2658' WHERE `id`='2659';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_parking_hint1', 'int', '0', '2644', '2644', '90', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_parking_hint2', 'int', '0', '2644', '2644', '100', 'hintWithView', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_summary', 'string', '0', '0', '2619', '10', 'summary', '0');
			UPDATE {{attributeclass}} SET `super`='2662' WHERE `id`='2662';
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_summary_hint1', 'int', '0', '2662', '2662', '20', 'hintWithView', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_summary_hint2', 'int', '0', '2662', '2662', '30', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9a_agreement', 'bool', '0', '2662', '2662', '40', 'checkboxAgreement', '0');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2662', '0', '0', '50');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2624', 'ru', 'n9a_hint_danger');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2625', 'ru', 'n9a_zone');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2626', 'ru', 'n9a_zone_hint');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2638', 'ru', 'n9a_zone_header');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2639', 'ru', 'n9a_zone_input1');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2640', 'ru', 'n9a_zone_input2');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2641', 'ru', 'n9a_zone_input3');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2642', 'ru', 'n9a_zone_hint2');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2643', 'ru', 'n9a_zone_hint3');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2644', 'ru', 'n9a_parking');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2645', 'ru', 'n9a_parking_input1');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2646', 'ru', 'n9a_parking_input2');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2647', 'ru', 'n9a_parking_input3');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2648', 'ru', 'n9a_parking_day1');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2649', 'ru', 'n9a_parking_day1_date');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2650', 'ru', 'n9a_parking_day1_input');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2651', 'ru', 'n9a_parking_day2');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2652', 'ru', 'n9a_parking_day2_date');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2653', 'ru', 'n9a_parking_day2_input');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2654', 'ru', 'n9a_parking_day3');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2655', 'ru', 'n9a_parking_day3_date');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2656', 'ru', 'n9a_parking_day3_input');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2657', 'ru', 'n9a_parking_day4');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2658', 'ru', 'n9a_parking_day4_date');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2659', 'ru', 'n9a_parking_day4_input');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2660', 'ru', 'n9a_parking_hint1');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2661', 'ru', 'n9a_parking_hint2');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2662', 'ru', 'n9a_summary');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2663', 'ru', 'n9a_summary_hint1');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2664', 'ru', 'n9a_summary_hint2');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2665', 'ru', 'n9a_agreement');
			UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 17 сентября 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='3011';
			UPDATE {{trattributeclass}} SET `label`='Пропуск в зону разгрузки-погрузки' WHERE `id`='3012';
			UPDATE {{trattributeclass}} SET `label`='Зона проведения погрузо-разгрузочных работ включает в себя всю территорию МВЦ «Крокус Экспо» за исключением бесплатных парковок, предназначенных только для легкового автотранспорта и зоны временной парковки грузового автотранспорта, ожидающего разгрузки/погрузки.' WHERE `id`='3013';
			UPDATE {{trattributeclass}} SET `label`='На период монтажа-демонтажа мероприятия' WHERE `id`='3014';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3015';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3016';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3017';
			UPDATE {{trattributeclass}} SET `label`='При заказе пропуска на мультикран (манипулятор и пр.) разрешается разгрузка-погрузка только груза прибывшего на самом транспортном средстве.' WHERE `id`='3018';
			UPDATE {{trattributeclass}} SET `label`='Норматив времени нахождения транспорта в зоне погрузки-разгрузки: для легковых автомобилей — 0,5 часа, для грузовых автомобилей — 1 час. При превышении норматива необходимы повторный заказ и оплата пропуска.\r\n\r\nДля проезда в зону разгрузки-погрузки транспортных средств, в отношении которых заказана разгрузка/погрузка силами «Крокус Экспо» (заполнены и оплачены Заявки №9-1 и или №9-2) необходимо получить БЕСПЛАТНЫЙ пропуск для получения которого представителю компании необходимо обратиться в сервис-центр «Крокус Экспо» не позже чем за один час до начала работ в соответствии с графиком разгрузки погрузки.' WHERE `id`='3019';
			UPDATE {{trattributeclass}} SET `label`='Пропуск на VIP стоянку (только легковые автомобили)' WHERE `id`='3020';
			UPDATE {{trattributeclass}} SET `label`='На один день' WHERE `id`='3024';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3025';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3026';
			UPDATE {{trattributeclass}} SET `label`='На один день' WHERE `id`='3027';
			UPDATE {{trattributeclass}} SET `label`='На один день' WHERE `id`='3030';
			UPDATE {{trattributeclass}} SET `label`='На один день' WHERE `id`='3033';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3028';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3029';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3031';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3032';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3034';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3035';
			UPDATE {{trattributeclass}} SET `label`='Транспортные средства с нанесенными рекламными изображениями, логотипами, а также оборудованные рекламными конструкциями на стоянку не допускаются.' WHERE `id`='3036';
			UPDATE {{trattributeclass}} SET `label`='До 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ заказываются у Организатора.\r\nПосле 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ могут быть приобретены как у Организатора, так и через сервис-центр МВЦ «Крокус Экспо». При приобретении пропусков через сервис-центр их стоимость определяется непосредственно МВЦ «Крокус Экспо». Для оформления пропусков необходимо указать тип и государственный номер транспортного средства.' WHERE `id`='3037';
			DELETE FROM {{trattributeclass}} WHERE `id`='3038';
			UPDATE {{trattributeclass}} SET `label`='Все цены включают НДС 18%.' WHERE `id`='3039';
			UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n<br><br>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.\n<br><br>\nСтоимость услуг увеличивается на 100% при заказе после 17.09.16 г.\n<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 07 октября 2016г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='3040';
			UPDATE {{trattributeclass}} SET `label`='Экспонент' WHERE `id`='2891';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='2528';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='2529';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='2530';
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2630', 'en', 'Exhibitor');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2633', 'en', 'Organizer');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2636', 'en', 'signature');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2637', 'en', 'signature');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2631', 'en', 'position');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2632', 'en', 'Name');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2634', 'en', 'position');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2635', 'en', 'Name');
		";
	}
}