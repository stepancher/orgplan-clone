<?php

class m160919_063954_return_gradoteka_data extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{gstattypes}};
            DELETE FROM {{gobjectshasgstattypes}};
            DELETE FROM {{gvalue}};
            DELETE FROM {{gobjects}};
            DELETE FROM {{gobjecthasregion}};
            DELETE FROM {{chartcolumn}};
            DELETE FROM {{charttable}};
            DELETE FROM {{chart}};
            
            ALTER TABLE {{gobjectshasgstattypes}}
                DROP FOREIGN KEY `fk_gobjectshasgstattypes_2`,
                DROP FOREIGN KEY `fk_gobjectshasgstattypes_1`;
            
            DROP TABLE IF EXISTS {{gstattypes}};
            CREATE TABLE IF NOT EXISTS {{gstattypes}}
                SELECT * FROM {{oldgstattypes}};
            DELETE FROM {{gstattypes}};
            ALTER TABLE {{gstattypes}} 
                CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
                ADD PRIMARY KEY (`id`);

            ALTER TABLE {{gvalue}}
            DROP FOREIGN KEY `fk_gvalue_1`;

            DROP TABLE IF EXISTS {{gobjectshasgstattypes}};
            CREATE TABLE IF NOT EXISTS {{gobjectshasgstattypes}}
                SELECT * FROM {{oldgobjectshasgstattypes}};
		    DELETE FROM {{gobjectshasgstattypes}};
		    ALTER TABLE {{gobjectshasgstattypes}}
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            ADD PRIMARY KEY (`id`);

            DROP TABLE IF EXISTS {{gvalue}};
            CREATE TABLE IF NOT EXISTS {{gvalue}}
                SELECT * FROM {{oldgvalue}};
            DELETE FROM {{gvalue}};
            
            ALTER TABLE {{gvalue}} 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            ADD PRIMARY KEY (`id`);
            
            ALTER TABLE {{gobjectshasgstattypes}} 
            ADD UNIQUE INDEX `uniq_objId_statId` (`objId` ASC, `statId` ASC),
            ADD INDEX `statId_index` (`statId` ASC);
            
            ALTER TABLE {{gvalue}}
            ADD UNIQUE INDEX `UNIQ_setId_epoch` (`setId` ASC, `epoch` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}