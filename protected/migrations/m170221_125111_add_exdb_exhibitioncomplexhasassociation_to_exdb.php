<?php

class m170221_125111_add_exdb_exhibitioncomplexhasassociation_to_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP TABLE IF EXISTS {$expodata}.{{exdbvenuehasassociation}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbexhibitioncomplexhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exhibitionComplexId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_venues_from_exdb_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}