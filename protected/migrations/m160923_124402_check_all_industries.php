<?php

class m160923_124402_check_all_industries extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{charttablegroup}} (`industryId`, `position`) VALUES ('2', '1');
            INSERT INTO {{charttablegroup}} (`industryId`, `position`) VALUES ('8', '1');
            INSERT INTO {{charttablegroup}} (`industryId`, `position`) VALUES ('1', '1');
            INSERT INTO {{charttablegroup}} (`industryId`, `position`) VALUES ('14', '1');
            INSERT INTO {{charttablegroup}} (`industryId`, `position`) VALUES ('13', '1');
            INSERT INTO {{charttablegroup}} (`industryId`, `position`) VALUES ('37', '1');
            UPDATE {{charttable}} SET `groupId`='15', `position`='1' WHERE `id`='15';
            UPDATE {{charttable}} SET `groupId`='15', `position`='2' WHERE `id`='16';
            UPDATE {{charttable}} SET `groupId`='15', `position`='3' WHERE `id`='17';
            UPDATE {{charttable}} SET `groupId`='15', `position`='4' WHERE `id`='18';
            UPDATE {{charttable}} SET `groupId`='15', `position`='5' WHERE `id`='19';
            UPDATE {{charttable}} SET `groupId`='15', `position`='6' WHERE `id`='20';
            UPDATE {{charttable}} SET `groupId`='15', `position`='7' WHERE `id`='21';

            UPDATE {{charttable}} SET `groupId`='16', `position`='1' WHERE `id`='22';
            UPDATE {{charttable}} SET `groupId`='16', `position`='2' WHERE `id`='23';
            UPDATE {{charttable}} SET `groupId`='16', `position`='3' WHERE `id`='24';
            UPDATE {{charttable}} SET `groupId`='16', `position`='4' WHERE `id`='25';
            UPDATE {{charttable}} SET `groupId`='17', `position`='1' WHERE `id`='55';
            UPDATE {{charttable}} SET `groupId`='17', `position`='2' WHERE `id`='56';
            UPDATE {{charttable}} SET `groupId`='17', `position`='3' WHERE `id`='57';
            UPDATE {{charttable}} SET `groupId`='17', `position`='4' WHERE `id`='58';
            UPDATE {{chartcolumn}} SET `statId`='500003' WHERE `id`='267';
            
            UPDATE {{charttable}} SET `groupId`='18', `position`='1' WHERE `id`='26';
            UPDATE {{charttable}} SET `groupId`='18', `position`='2' WHERE `id`='27';
            UPDATE {{charttable}} SET `groupId`='18', `position`='3' WHERE `id`='28';
            UPDATE {{charttable}} SET `groupId`='18', `position`='4' WHERE `id`='29';
            UPDATE {{charttable}} SET `groupId`='18', `position`='5' WHERE `id`='30';
            UPDATE {{charttable}} SET `groupId`='18', `position`='6' WHERE `id`='31';
            UPDATE {{charttable}} SET `groupId`='18', `position`='7' WHERE `id`='32';
            UPDATE {{charttable}} SET `groupId`='18', `position`='8' WHERE `id`='33';
            UPDATE {{charttable}} SET `groupId`='18', `position`='9' WHERE `id`='34';
            UPDATE {{charttable}} SET `groupId`='18', `position`='10' WHERE `id`='35';
            UPDATE {{charttable}} SET `groupId`='18', `position`='11' WHERE `id`='36';
            UPDATE {{charttable}} SET `groupId`='18', `position`='12' WHERE `id`='37';
            UPDATE {{charttable}} SET `groupId`='18', `position`='13' WHERE `id`='38';
            UPDATE {{charttable}} SET `groupId`='18', `position`='14' WHERE `id`='39';
            UPDATE {{charttable}} SET `groupId`='18', `position`='15' WHERE `id`='40';
            UPDATE {{charttable}} SET `groupId`='18', `position`='16' WHERE `id`='41';
            UPDATE {{charttable}} SET `groupId`='18', `position`='17' WHERE `id`='42';
            UPDATE {{charttable}} SET `groupId`='18', `position`='18' WHERE `id`='43';
            UPDATE {{charttable}} SET `groupId`='18', `position`='19' WHERE `id`='44';
            
            UPDATE {{charttable}} SET `groupId`='19', `position`='1' WHERE `id`='45';
            UPDATE {{charttable}} SET `groupId`='19', `position`='2' WHERE `id`='46';
            UPDATE {{charttable}} SET `groupId`='19', `position`='3' WHERE `id`='47';
            UPDATE {{charttable}} SET `groupId`='19', `position`='4' WHERE `id`='48';
            UPDATE {{charttable}} SET `groupId`='19', `position`='5' WHERE `id`='49';
            UPDATE {{charttable}} SET `groupId`='19', `position`='6' WHERE `id`='50';
            UPDATE {{charttable}} SET `groupId`='19', `position`='7' WHERE `id`='51';
            UPDATE {{charttable}} SET `groupId`='19', `position`='8' WHERE `id`='52';
            UPDATE {{charttable}} SET `groupId`='19', `position`='9' WHERE `id`='53';
            UPDATE {{charttable}} SET `groupId`='19', `position`='10' WHERE `id`='54';
            
            UPDATE {{charttable}} SET `groupId`='20', `position`='1' WHERE `id`='65';
            UPDATE {{charttable}} SET `groupId`='20', `position`='2' WHERE `id`='66';
            UPDATE {{charttable}} SET `groupId`='20', `position`='3' WHERE `id`='67';
            UPDATE {{charttable}} SET `groupId`='20', `position`='4' WHERE `id`='68';
            UPDATE {{charttable}} SET `groupId`='20', `position`='5' WHERE `id`='69';
            UPDATE {{charttable}} SET `groupId`='20', `position`='6' WHERE `id`='70';
            
            UPDATE {{chartcolumn}} SET `statId`='5784b781bd4c56dc4f8b5d44' WHERE `id`='181';
            UPDATE {{chartcolumn}} SET `statId`='5784b781bd4c56dc4f8b5d48' WHERE `id`='185';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}