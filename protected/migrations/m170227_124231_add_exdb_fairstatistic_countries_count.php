<?php

class m170227_124231_add_exdb_fairstatistic_countries_count extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fair_statistic_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fair_statistic_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_areaSpecialExpositions INT DEFAULT 0;
                DECLARE f_squareNet INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_exhibitorsForeign INT DEFAULT 0;
                DECLARE f_exhibitorsLocal INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_visitorsForeign INT DEFAULT 0;
                DECLARE f_visitorsLocal INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statisticsDate TEXT DEFAULT '';
                DECLARE f_countries_count TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT d.id,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.specialShow, ',', 4), SUBSTRING_INDEX(d.specialShow, ',', 3), ''), 2) AS areaSpecialExpositions,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.netSqm, ',', 4), SUBSTRING_INDEX(d.netSqm, ',', 3), ''), 2) AS squareNet,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 4), SUBSTRING_INDEX(d.exhibitors, ',', 3), ''), 2) AS members,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 3), SUBSTRING_INDEX(d.exhibitors, ',', 2), ''), 2) AS exhibitorsForeign,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 2), SUBSTRING_INDEX(d.exhibitors, ',', 1), ''), 2) AS exhibitorsLocal,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 4), SUBSTRING_INDEX(d.visitors, ',', 3), ''), 2) AS visitors,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 3), SUBSTRING_INDEX(d.visitors, ',', 2), ''), 2) AS visitorsForeign,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 2), SUBSTRING_INDEX(d.visitors, ',', 1), ''), 2) AS visitorsLocal,
                                d.statistics_with_en AS statistics,
                                d.stat_year_en AS statisticsDate,
                                d.exhibitors_profile_en AS countriesCount
                            FROM
                            (SELECT t.id, t.exhibitors_profile_en, t.statistics_with_en, t.stat_year_en, t.row_1, t.stats_en,
                            CASE WHEN LOCATE('Net sqm', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Net sqm', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Net sqm', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Net sqm', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Net sqm', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Net sqm', row_7) != 0 THEN row_7
                                 ELSE NULL END netSqm,
                            CASE WHEN LOCATE('Exhibitors', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Exhibitors', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Exhibitors', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Exhibitors', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Exhibitors', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Exhibitors', row_7) != 0 THEN row_7
                                 ELSE NULL END exhibitors,
                            CASE WHEN LOCATE('Visitors', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Visitors', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Visitors', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Visitors', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Visitors', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Visitors', row_7) != 0 THEN row_7
                                 ELSE NULL END visitors,
                            CASE WHEN LOCATE('Special show', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Special show', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Special show', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Special show', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Special show', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Special show', row_7) != 0 THEN row_7
                                 ELSE NULL END specialShow
                            FROM
                            (SELECT ed.id, ed.stats_en, ed.exhibitors_profile_en, ed.statistics_with_en, ed.stat_year_en,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',1), SUBSTRING_INDEX(ed.stats_en,'\n',0),'') AS `row_1`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',2), SUBSTRING_INDEX(ed.stats_en,'\n',1),'') AS `row_2`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',3), SUBSTRING_INDEX(ed.stats_en,'\n',2),'') AS `row_3`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',4), SUBSTRING_INDEX(ed.stats_en,'\n',3),'') AS `row_4`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',5), SUBSTRING_INDEX(ed.stats_en,'\n',4),'') AS `row_5`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',6), SUBSTRING_INDEX(ed.stats_en,'\n',5),'') AS `row_6`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',7), SUBSTRING_INDEX(ed.stats_en,'\n',6),'') AS `row_7`
                                    FROM {$expodataRaw}.expo_data ed) t) d
                                    LEFT JOIN {$expodata}.{{exdbfairstatistic}} fs ON fs.exdbId = d.id
                                    WHERE fs.id IS NULL ORDER BY d.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                FETCH copy INTO f_id, 
                    f_areaSpecialExpositions, 
                    f_squareNet, 
                    f_members, 
                    f_exhibitorsForeign, 
                    f_exhibitorsLocal,
                    f_visitors, 
                    f_visitorsForeign, 
                    f_visitorsLocal, 
                    f_statistics, 
                    f_statisticsDate,
                    f_countries_count;
                
                IF done THEN
                    LEAVE read_loop;
                END IF;
                
                START TRANSACTION;
                    INSERT INTO {$expodata}.{{exdbfairstatistic}} (`exdbId`,
                        `areaSpecialExpositions`,
                        `squareNet`,
                        `members`,
                        `exhibitorsForeign`,
                        `exhibitorsLocal`,
                        `visitors`,
                        `visitorsForeign`,
                        `visitorsLocal`,
                        `statistics`,
                        `statisticsDate`,
                        `exdbCountriesCount`) 
                        VALUES (f_id,
                        f_areaSpecialExpositions,
                        f_squareNet,
                        f_members,
                        f_exhibitorsForeign,
                        f_exhibitorsLocal,
                        f_visitors,
                        f_visitorsForeign,
                        f_visitorsLocal,
                        f_statistics,
                        f_statisticsDate,
                        f_countries_count
                    );
                COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`update_exdb_fair_info_id`;
            CREATE PROCEDURE {$expodata}.`update_exdb_fair_info_id`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE s_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT f.id AS fairId, fs.id AS statisticId 
                                        FROM {$expodata}.{{exdbfair}} f
                                        LEFT JOIN {$expodata}.{{exdbfairstatistic}} fs ON fs.exdbId = f.exdbId
                                        ORDER BY fairId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, s_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {$expodata}.{{exdbfair}} SET `infoId` = s_id WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairs_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairs_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_beginDate DATE DEFAULT '1970-01-01';
                DECLARE f_endDate DATE DEFAULT '1970-01-01';
                DECLARE f_beginMountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_endMountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_beginDemountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_endDemountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_crc BIGINT DEFAULT 0;
                DECLARE f_name TEXT DEFAULT '';
                DECLARE f_name_de TEXT DEFAULT '';
                DECLARE f_frequency TEXT DEFAULT '';
                DECLARE f_exhibitioncomplex_id INT DEFAULT 0;
                DECLARE f_business_sectors TEXT DEFAULT '';
                DECLARE f_costs TEXT DEFAULT '';
                DECLARE f_show_type TEXT DEFAULT '';
                DECLARE f_further_information TEXT DEFAULT '';
                DECLARE f_story_id TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT s.id, 
                                        s.beginDate, 
                                        s.endDate,
                                        s.beginMountingDate,
                                        s.endMountingDate,
                                        s.beginDemountingDate,
                                        s.endDemountingDate, 
                                        CRC32(CONCAT(s.id, s.beginDate)) AS exdbCrc,
                                        ed.name_en,
										ed.name_de,
                                        ed.frequency_en,
                                        ec.id as exhibitionComplexId,
                                        ed.business_sectors_en,
                                        ed.costs_en,
                                        ed.show_type_en,
                                        ed.further_information_en,
                                        ed.id AS storyId
                                         FROM
                                        (SELECT p.id,
                                        CASE WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0 
                                                    AND p.beginDateYear IS NOT NULL AND p.beginDateYear != 0) 
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.beginDateMonth,'-',p.beginDateYear), '%e-%c-%Y')
                                             
                                             WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0 
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0) 
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.beginDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             
                                             WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.endDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginDate,
                                        
                                        CASE WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.endDateYear IS NOT NULL AND p.endDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.endDateMonth,'-',p.endDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.endDateMonth,'-',p.fyear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.beginDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endDate,
                                        
                                        CASE WHEN (p.beginMountingDateDay IS NOT NULL AND p.beginMountingDateDay != 0
                                                    AND p.beginMountingDateMonth IS NOT NULL AND p.beginMountingDateMonth != 0
                                                    AND p.beginMountingDateYear IS NOT NULL AND p.beginMountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginMountingDateDay,'-',p.beginMountingDateMonth,'-',p.beginMountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.beginMountingDateDay IS NOT NULL AND p.beginMountingDateDay != 0
                                                    AND p.beginMountingDateMonth IS NOT NULL AND p.beginMountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginMountingDateDay,'-',p.beginMountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginMountingDate,
                                        
                                        CASE WHEN (p.endMountingDateDay IS NOT NULL AND p.endMountingDateDay != 0
                                                    AND p.endMountingDateMonth IS NOT NULL AND p.endMountingDateMonth != 0
                                                    AND p.endMountingDateYear IS NOT NULL AND p.endMountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endMountingDateDay,'-',p.endMountingDateMonth,'-',p.endMountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endMountingDateDay IS NOT NULL AND p.endMountingDateDay != 0
                                                    AND p.endMountingDateMonth IS NOT NULL AND p.endMountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endMountingDateDay,'-',p.endMountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endMountingDate,
                                        
                                        CASE WHEN (p.beginDemountingDateDay IS NOT NULL AND p.beginDemountingDateDay != 0
                                                    AND p.beginDemountingDateMonth IS NOT NULL AND p.beginDemountingDateMonth != 0
                                                    AND p.beginDemountingDateYear IS NOT NULL AND p.beginDemountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDemountingDateDay,'-',p.beginDemountingDateMonth,'-',p.beginDemountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.beginDemountingDateDay IS NOT NULL AND p.beginDemountingDateDay != 0
                                                    AND p.beginDemountingDateMonth IS NOT NULL AND p.beginDemountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDemountingDateDay,'-',p.beginDemountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginDemountingDate,
                                        
                                        CASE WHEN (p.endDemountingDateDay IS NOT NULL AND p.endDemountingDateDay != 0
                                                    AND p.endDemountingDateMonth IS NOT NULL AND p.endDemountingDateMonth != 0
                                                    AND p.endDemountingDateYear IS NOT NULL AND p.endDemountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDemountingDateDay,'-',p.endDemountingDateMonth,'-',p.endDemountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDemountingDateDay IS NOT NULL AND p.endDemountingDateDay != 0
                                                    AND p.endDemountingDateMonth IS NOT NULL AND p.endDemountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDemountingDateDay,'-',p.endDemountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endDemountingDate
                                        FROM 
                                        (SELECT q.id, 
                                        q.fyear,
                                        
                                        q.beginDate,
                                        DAY(STR_TO_DATE(q.beginDate, '%e')) AS beginDateDay,
                                        MONTH(STR_TO_DATE(q.beginDate, '%e%b')) AS beginDateMonth,
                                        YEAR(STR_TO_DATE(q.beginDate, '%e%b%Y')) AS beginDateYear,
                                        
                                        q.endDate,
                                        DAY(STR_TO_DATE(q.endDate, '%e')) AS endDateDay,
                                        MONTH(STR_TO_DATE(q.endDate, '%e%b')) AS endDateMonth,
                                        YEAR(STR_TO_DATE(q.endDate, '%e%b%Y')) AS endDateYear,
                                        
                                        q.beginMountingDate,
                                        DAY(STR_TO_DATE(q.beginMountingDate, '%e')) AS beginMountingDateDay,
                                        MONTH(STR_TO_DATE(q.beginMountingDate, '%e%b')) AS beginMountingDateMonth,
                                        YEAR(STR_TO_DATE(q.beginMountingDate, '%e%b%Y')) AS beginMountingDateYear,
                                        
                                        q.endMountingDate,
                                        DAY(STR_TO_DATE(q.endMountingDate, '%e')) AS endMountingDateDay,
                                        MONTH(STR_TO_DATE(q.endMountingDate, '%e%b')) AS endMountingDateMonth,
                                        YEAR(STR_TO_DATE(q.endMountingDate, '%e%b%Y')) AS endMountingDateYear,
                                        
                                        q.beginDemountingDate,
                                        DAY(STR_TO_DATE(q.beginDemountingDate, '%e')) AS beginDemountingDateDay,
                                        MONTH(STR_TO_DATE(q.beginDemountingDate, '%e%b')) AS beginDemountingDateMonth,
                                        YEAR(STR_TO_DATE(q.beginDemountingDate, '%e%b%Y')) AS beginDemountingDateYear,
                                        
                                        q.endDemountingDate,
                                        DAY(STR_TO_DATE(q.endDemountingDate, '%e')) AS endDemountingDateDay,
                                        MONTH(STR_TO_DATE(q.endDemountingDate, '%e%b')) AS endDemountingDateMonth,
                                        YEAR(STR_TO_DATE(q.endDemountingDate, '%e%b%Y')) AS endDemountingDateYear,
                                        
                                        q.tHeader,
                                        q.tRow
                                        FROM
                                        (SELECT d.id, REPLACE(d.fyear,'','') AS fyear, 
                                        REPLACE(SUBSTRING_INDEX(d.showDates, '-', 1), '.', '') AS beginDate,
                                        REPLACE(SUBSTR(REPLACE(d.showDates, SUBSTRING_INDEX(d.showDates, '-', 1), ''), 2), '.', '') endDate,
                                        SUBSTRING_INDEX(d.buildUp, '-', 1) AS beginMountingDate,
                                        SUBSTR(REPLACE(d.buildUp, SUBSTRING_INDEX(d.buildUp, '-', 1), ''), 2) AS endMountingDate,
                                        SUBSTRING_INDEX(d.dismantling, '-', 1) AS beginDemountingDate,
                                        SUBSTR(REPLACE(d.dismantling, SUBSTRING_INDEX(d.dismantling, '-', 1), ''), 2) AS endDemountingDate,
                                        d.tHeader,
                                        d.tRow
                                        FROM
                                        (SELECT id, row_1 AS tHeader, row_2 AS tRow, SUBSTR(row_2, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_2, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',2), SUBSTRING_INDEX(ed.dates_en,'\n',1),'') AS `row_2`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_3 AS tRow, SUBSTR(row_3, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_3, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',3), SUBSTRING_INDEX(ed.dates_en,'\n',2),'') AS `row_3`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_4 AS tRow, SUBSTR(row_4, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_4, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',4), SUBSTRING_INDEX(ed.dates_en,'\n',3),'') AS `row_4`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_5 AS tRow, SUBSTR(row_5, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_5, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',5), SUBSTRING_INDEX(ed.dates_en,'\n',4),'') AS `row_5`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_6 AS tRow, SUBSTR(row_6, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_6, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',6), SUBSTRING_INDEX(ed.dates_en,'\n',5),'') AS `row_6`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_7 AS tRow, SUBSTR(row_7, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_7, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',7), SUBSTRING_INDEX(ed.dates_en,'\n',6),'') AS `row_7`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_8 AS tRow, SUBSTR(row_8, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_8, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',8), SUBSTRING_INDEX(ed.dates_en,'\n',7),'') AS `row_8`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_9 AS tRow, SUBSTR(row_9, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_9, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',9), SUBSTRING_INDEX(ed.dates_en,'\n',8),'') AS `row_9`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_10 AS tRow, SUBSTR(row_10, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_10, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',10), SUBSTRING_INDEX(ed.dates_en,'\n',9),'') AS `row_10`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_11 AS tRow, SUBSTR(row_11, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_11, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',11), SUBSTRING_INDEX(ed.dates_en,'\n',10),'') AS `row_11`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        ) d) q) p where p.fyear != '') s 
                                        LEFT JOIN {$expodata}.{{exdbfair}} f ON f.exdbCrc = CRC32(CONCAT(s.id, s.beginDate)) AND f.exdbId = s.id
                                        LEFT JOIN {$expodataRaw}.expo_data ed ON ed.id = s.id
                                        LEFT JOIN {$expodataRaw}.expo_ground gr ON ed.expo_loc_id = gr.id
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = gr.id
                                        WHERE f.id IS NULL
                                        ORDER BY s.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, 
                        f_beginDate, 
                        f_endDate, 
                        f_beginMountingDate, 
                        f_endMountingDate, 
                        f_beginDemountingDate, 
                        f_endDemountingDate, 
                        f_crc, 
                        f_name,
						f_name_de,
                        f_frequency,
                        f_exhibitioncomplex_id,
                        f_business_sectors,
                        f_costs,
                        f_show_type,
                        f_further_information,
                        f_story_id;
                    
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfair}} (
                            `exdbId`,
                            `exdbCrc`,
                            `exdbFrequency`,
                            `name`,
                            `name_de`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                            `exdbBusinessSectors`,
                            `exdbCosts`,
                            `exdbShowType`,
                            `site`,
                            `storyId`) 
                            VALUES (
                            f_id,
                            f_crc,
                            f_frequency,
                            f_name,
							f_name_de,
                            f_exhibitioncomplex_id,
                            f_beginDate,
                            f_endDate,
                            f_beginMountingDate,
                            f_endMountingDate,
                            f_beginDemountingDate,
                            f_endDemountingDate,
                            f_business_sectors,
                            f_costs,
                            f_show_type,
                            f_further_information,
                            f_story_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_fair_statistic_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairs_to_exdb_db`();
            CALL {$expodata}.`update_exdb_fair_info_id`();
            CALL {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}