<?php

class m160525_125806_proposal_translates_fix extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '960', 'en', 'm²');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '307', 'м²', 'unitTitle');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1747', 'ru', 'м²');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1747', 'en', 'sq.m.');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_hint_danger_last', 'string', '0', '518', '518', '10', 'hintDanger', '0');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2008', 'ru', 'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br/><br/>Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br/><br/>Все претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 4 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2008', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 04, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.');
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='2008';
		";
	}
}