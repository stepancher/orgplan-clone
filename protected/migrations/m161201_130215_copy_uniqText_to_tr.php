<?php

class m161201_130215_copy_uniqText_to_tr extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_uniqText_to_tr`()
            BEGIN 
            
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE uniq TEXT DEFAULT '';
                DECLARE fair_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT uniqueText, id FROM {{fair}};
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                    read_loop: LOOP
                        
                        FETCH copy INTO uniq, fair_id;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            UPDATE {{trfair}} SET `uniqueText` = uniq WHERE `trParentId` = fair_id;
                        COMMIT;
                        
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL copy_uniqText_to_tr();
            DROP PROCEDURE IF EXSISTS `copy_uniqText_to_tr`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}