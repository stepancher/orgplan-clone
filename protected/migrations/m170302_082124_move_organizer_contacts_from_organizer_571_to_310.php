<?php

class m170302_082124_move_organizer_contacts_from_organizer_571_to_310 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '1120';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '1125';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '1946';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '1947';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '3237';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '3304';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '3334';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '3587';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '3590';
            UPDATE {{organizer}} SET `companyId` = '310' WHERE `id` = '4056';
            
            DELETE FROM {{organizercompany}} WHERE `id` = '571';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}