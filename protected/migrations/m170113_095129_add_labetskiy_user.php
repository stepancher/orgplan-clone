<?php

class m170113_095129_add_labetskiy_user extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `add_labetskiy_user`()
            BEGIN
                
                START TRANSACTION;
                    INSERT INTO {{contact}} (`companyName`, `post`, `email`, `phone`, `workPhone`) VALUES ('', '', 'labetskiy@protoplan.pro', '', '');
                COMMIT;
                
                SET @new_contact_id = LAST_INSERT_ID();
                
                START TRANSACTION;
                    INSERT INTO {{user}} (`password`,`firstName`,`role`,`contactId`,`active`,`banned`,`tariffId`) VALUES ('$2y$10$1rNt8MPwconjWQRzLgqvsuvkxtoCQLMPiC50O58U/yQI.ybSClKWO','Илья','15',@new_contact_id,'1','0','1');
                COMMIT;
                
            END;
            
            CALL add_labetskiy_user();
            DROP PROCEDURE IF EXISTS `add_labetskiy_user`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}