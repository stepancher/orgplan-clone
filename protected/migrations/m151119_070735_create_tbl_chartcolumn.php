<?php

class m151119_070735_create_tbl_chartcolumn extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{chartcolumn}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			CREATE TABLE IF NOT EXISTS {{chartcolumn}} (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`statId` int(11) DEFAULT NULL,
				`header` varchar(200) DEFAULT NULL,
				`count` varchar(45) DEFAULT NULL,
				`parent` int(11) DEFAULT NULL,
				`widgetType` int(11) DEFAULT NULL,
				`epoch` int(11) DEFAULT NULL,
				`columnGroupId` int(11) DEFAULT NULL,
				`color` varchar(45) DEFAULT NULL,
				`position` varchar(45) DEFAULT NULL,
				`related` int(11) DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (1,103783,'',NULL,NULL,4,2014,1,'#43ade3','1',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (2,103784,'',NULL,NULL,4,2014,1,'#42c0b4','3',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (3,103783,'Продукция сельс&shy;ко&shy;хо&shy;зяйс&shy;твен&shy;ных организаций',NULL,NULL,NULL,2014,1,NULL,'2',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (4,103784,'Продукция крестьянско-фермерских хозяйств и ИП',NULL,NULL,NULL,2014,1,NULL,'4',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (5,220731,'раст-во',NULL,NULL,NULL,2014,2,NULL,'3',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (6,220732,'живот-во',NULL,NULL,NULL,2014,2,NULL,'5',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (7,103784,'',NULL,NULL,4,2014,2,'#2b98d5','1',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (8,103784,'Продукция сельс&shy;ко&shy;хо&shy;зяйс&shy;твен&shy;ных организаций',NULL,NULL,NULL,2014,2,NULL,'2',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (10,220731,NULL,NULL,NULL,5,2014,2,NULL,'4',6);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (11,103784,NULL,NULL,NULL,4,2014,1,'#42c0b4','5',0);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (12,103784,'Продукция крестьянско-фермерских хозяйств и ИП',NULL,NULL,NULL,2014,1,'#42c0b4','6',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (13,103781,'Продукция сельского хозяйства по хозяйствам всех категорий',NULL,NULL,6,2014,3,'#43ade3','1',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (14,103781,'Продукция сельского хозяйства по хозяйствам всех категорий',NULL,NULL,6,2014,3,'#43ade3','2',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (15,103781,'Продукция сельского хозяйства по хозяйствам всех категорий',NULL,NULL,6,2014,3,'#43ade3','3',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (16,103781,'Продукция сельского хозяйства по хозяйствам всех категорий',NULL,NULL,6,2014,3,'#43ade3','4',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (17,103781,'Продукция сельского хозяйства по хозяйствам всех категорий',NULL,NULL,6,2014,3,'#43ade3','5',NULL);
				INSERT INTO {{chartcolumn}} (`id`,`statId`,`header`,`count`,`parent`,`widgetType`,`epoch`,`columnGroupId`,`color`,`position`,`related`) VALUES (18,103781,'Продукция сельского хозяйства по хозяйствам всех категорий',NULL,NULL,6,2014,3,'#43ade3','6',0);
		";
	}

}