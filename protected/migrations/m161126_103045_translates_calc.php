<?php

class m161126_103045_translates_calc extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{tradditionalexpenses}} SET `name`='Standfläche' WHERE `id`='204';
            UPDATE {{tradditionalexpenses}} SET `name`='Standbau' WHERE `id`='205';
            UPDATE {{tradditionalexpenses}} SET `name`='Logistik' WHERE `id`='206';
            UPDATE {{tradditionalexpenses}} SET `name`='Leistungen des Messezentrums' WHERE `id`='207';
            UPDATE {{tradditionalexpenses}} SET `name`='Werbung und Vertrieb' WHERE `id`='208';
            UPDATE {{tradditionalexpenses}} SET `name`='Flug, Hotels' WHERE `id`='209';
            UPDATE {{tradditionalexpenses}} SET `name`='andere Kosten' WHERE `id`='210';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}