<?php

class m170217_135257_add_exdb_fair_statistic_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbfairstatistic}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `areaSpecialExpositions` INT(11) NULL DEFAULT NULL,
              `squareNet` INT(11) NULL DEFAULT NULL,
              `members` INT(11) NULL DEFAULT NULL,
              `exhibitorsForeign` INT(11) NULL DEFAULT NULL,
              `exhibitorsLocal` INT(11) NULL DEFAULT NULL,
              `visitors` INT(11) NULL DEFAULT NULL,
              `visitorsForeign` INT(11) NULL DEFAULT NULL,
              `visitorsLocal` INT(11) NULL DEFAULT NULL,
              `statistics` TEXT NULL DEFAULT NULL,
              `statisticsDate` DATE NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {$expodata}.{{exdbfairstatistic}} (`exdbId`,`areaSpecialExpositions`,`squareNet`,`members`,`exhibitorsForeign`,`exhibitorsLocal`,`visitors`,`visitorsForeign`,`visitorsLocal`,`statistics`,`statisticsDate`)
            SELECT d.id,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.specialShow, ',', 4), SUBSTRING_INDEX(d.specialShow, ',', 3), ''), 2) AS areaSpecialExpositions,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.netSqm, ',', 4), SUBSTRING_INDEX(d.netSqm, ',', 3), ''), 2) AS squareNet,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 4), SUBSTRING_INDEX(d.exhibitors, ',', 3), ''), 2) AS members,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 3), SUBSTRING_INDEX(d.exhibitors, ',', 2), ''), 2) AS exhibitorsForeign,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 2), SUBSTRING_INDEX(d.exhibitors, ',', 1), ''), 2) AS exhibitorsLocal,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 4), SUBSTRING_INDEX(d.visitors, ',', 3), ''), 2) AS visitors,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 3), SUBSTRING_INDEX(d.visitors, ',', 2), ''), 2) AS visitorsForeign,
                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 2), SUBSTRING_INDEX(d.visitors, ',', 1), ''), 2) AS visitorsLocal,
                d.statistics_with_en AS statistics,
                d.stat_year_en AS statisticsDate
            FROM
            (SELECT t.id, t.statistics_with_en, t.stat_year_en, t.row_1, t.stats_en,
            CASE WHEN LOCATE('Net sqm', row_2) != 0 THEN row_2
                 WHEN LOCATE('Net sqm', row_3) != 0 THEN row_3
                 WHEN LOCATE('Net sqm', row_4) != 0 THEN row_4
                 WHEN LOCATE('Net sqm', row_5) != 0 THEN row_5
                 WHEN LOCATE('Net sqm', row_6) != 0 THEN row_6
                 WHEN LOCATE('Net sqm', row_7) != 0 THEN row_7
                 ELSE NULL END netSqm,
            CASE WHEN LOCATE('Exhibitors', row_2) != 0 THEN row_2
                 WHEN LOCATE('Exhibitors', row_3) != 0 THEN row_3
                 WHEN LOCATE('Exhibitors', row_4) != 0 THEN row_4
                 WHEN LOCATE('Exhibitors', row_5) != 0 THEN row_5
                 WHEN LOCATE('Exhibitors', row_6) != 0 THEN row_6
                 WHEN LOCATE('Exhibitors', row_7) != 0 THEN row_7
                 ELSE NULL END exhibitors,
            CASE WHEN LOCATE('Visitors', row_2) != 0 THEN row_2
                 WHEN LOCATE('Visitors', row_3) != 0 THEN row_3
                 WHEN LOCATE('Visitors', row_4) != 0 THEN row_4
                 WHEN LOCATE('Visitors', row_5) != 0 THEN row_5
                 WHEN LOCATE('Visitors', row_6) != 0 THEN row_6
                 WHEN LOCATE('Visitors', row_7) != 0 THEN row_7
                 ELSE NULL END visitors,
            CASE WHEN LOCATE('Special show', row_2) != 0 THEN row_2
                 WHEN LOCATE('Special show', row_3) != 0 THEN row_3
                 WHEN LOCATE('Special show', row_4) != 0 THEN row_4
                 WHEN LOCATE('Special show', row_5) != 0 THEN row_5
                 WHEN LOCATE('Special show', row_6) != 0 THEN row_6
                 WHEN LOCATE('Special show', row_7) != 0 THEN row_7
                 ELSE NULL END specialShow
            FROM
            (SELECT ed.id, ed.stats_en, ed.statistics_with_en, ed.stat_year_en,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',1), SUBSTRING_INDEX(ed.stats_en,'\n',0),'') AS `row_1`,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',2), SUBSTRING_INDEX(ed.stats_en,'\n',1),'') AS `row_2`,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',3), SUBSTRING_INDEX(ed.stats_en,'\n',2),'') AS `row_3`,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',4), SUBSTRING_INDEX(ed.stats_en,'\n',3),'') AS `row_4`,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',5), SUBSTRING_INDEX(ed.stats_en,'\n',4),'') AS `row_5`,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',6), SUBSTRING_INDEX(ed.stats_en,'\n',5),'') AS `row_6`,
            REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',7), SUBSTRING_INDEX(ed.stats_en,'\n',6),'') AS `row_7`
            FROM {$expodata}.expo_data ed) t) d;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}