<?php

class m160810_081308_update_tbl_report extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql  = $this->getAlterTable();
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getAlterTable(){
        return <<<EOL
            DROP TABLE IF EXISTS {{report}};
            
            CREATE TABLE {{report}} (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `reportId` int(11) DEFAULT NULL,
              `reportName` varchar(255) DEFAULT NULL,
              `reportLabel` text,
              `columnDefs` text,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
            
            INSERT INTO tbl_report VALUES (1,1,'report-1','Заказы клиента в разрезе номенклатуры','[{"field":"attributeValue","displayName":"\\\\\u041a\\\\\u043e\\\\u043b - \\\\u0432\\\\u043e","sortable":true,"aggregate":true,"sortingType":"number"},{"field":"sumRUB","displayName":"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c RUB","sortable":true,"aggregate":true,"sortingType":"number"},{"field":"sumUSD","displayName":"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c USD","sortable":true,"aggregate":true,"sortingType":"number"},{"field":"currency","displayName":"\\\u0412\\\u0430\\\u043b\\\u044e\\\u0442\\\u0430","sortable":true,"filterable":true,"sortingType":"string"}]'),(2,2,'report-2','Заказы клиента в разрезе клиента','[{"field":"attributeValue","displayName":"\\\u041a\\\u043e\\\u043b - \\\u0432\\\u043e","sortable":true,"aggregate":true,"sortingType":"number"},{"field":"sumRUB","displayName":"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c RUB","sortable":true,"aggregate":true,"sortingType":"number"},{"field":"sumUSD","displayName":"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c USD","sortable":true,"aggregate":true,"sortingType":"number"},{"field":"currency","displayName":"\\\u0412\\\u0430\\\u043b\\\u044e\\\u0442\\\u0430","sortable":true,"filterable":true,"sortingType":"string"}]'),(3,3,'report-3','Сдача заявок в разрезе номенклатуры','[{"field":"attributeValue","displayName":"\\\u041a\\\u043e\\\u043b - \\\u0432\\\u043e","sortable":true,"aggregate":false,"sortingType":"number"},{"field":"currency","displayName":" % \\\u0432\\\u044b\\\u043f\\\u043e\\\u043b\\\u043d\\\u0435\\\u043d\\\u0438\\\u044f","sortable":true,"aggregate":false,"sortingType":"number"}]'),(4,4,'report-4','Сдача заявок в разрезе клиента','[{"field":"status","displayName":"\\\u0421\\\u0442\\\u0430\\\u0442\\\u0443\\\u0441","sortable":true,"aggregate":false,"sortingType":"number"}]');
EOL;
    }
}