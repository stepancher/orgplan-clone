<?php

class m150909_130516_alter_tz extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{tz}} DROP COLUMN `customerData`;
			ALTER TABLE {{tz}} DROP COLUMN `exhibitionArea`;
			ALTER TABLE {{tz}} DROP COLUMN `standFloors`;
			ALTER TABLE {{tz}} DROP COLUMN `accreditationDeveloper`;
			ALTER TABLE {{tz}} DROP COLUMN `propelledByGoods`;
			ALTER TABLE {{tz}} DROP COLUMN `tzAdditionalDataId`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAlterTable(){
		return '
			ALTER TABLE {{tz}} ADD `customerData` varchar(255);
			ALTER TABLE {{tz}} ADD `exhibitionArea` int(11);
			ALTER TABLE {{tz}} ADD `standFloors` int(11);
			ALTER TABLE {{tz}} ADD `accreditationDeveloper` varchar(255);
			ALTER TABLE {{tz}} ADD `propelledByGoods` varchar(255);
			ALTER TABLE {{tz}} ADD `tzAdditionalDataId` int(11);
		';
	}
}