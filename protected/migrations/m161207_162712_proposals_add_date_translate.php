<?php

class m161207_162712_proposals_add_date_translate extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {


        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        list($peace1, $peace2, $dbSourceName) = explode('=', Yii::app()->db->connectionString);
        list($peace1, $peace2, $dbF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        $sql= '';

        /**
         * @var Fair $fair
         */
        foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){
                $dbTargetName = Yii::app()->dbMan->createShardName($fair->id);

                $sql = $sql."
                        
                    DELETE FROM  `{$dbTargetName}`.tbl_trattributeclassproperty where trParentId in (
                     1133,
                     1134,
                     1135,
                     1136,
                     1137,
                     1138,
                     1139,
                     1140,
                     1142,
                     1143,
                     1144,
                     1145,
                     1146,
                     1147,
                     1148,
                     1149,
                     1150,
                     1151) AND langId = 'en';
                    
                    insert into  `{$dbTargetName}`.tbl_trattributeclassproperty (trParentId, langId, value)
                    select trParentId, 'en', 'Date' FROM  `{$dbTargetName}`.tbl_trattributeclassproperty where value = 'дата';
                    
                    insert into  `{$dbTargetName}`.tbl_trattributeclassproperty (trParentId, langId, value)
                    select trParentId, 'de', 'Datum' FROM  `{$dbTargetName}`.tbl_trattributeclassproperty where value = 'дата';
            
                ";
            }
        }

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}