<?php

class m160704_092028_add_exhibititonComplexCount_for_regionsummary extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			ALTER TABLE {{regionsummary}} 
			ADD COLUMN `exhibitionComplexCount` INT(11) NOT NULL AFTER `fairsCount`;
			
			UPDATE {{route}} SET `route`='search/regionSummaryByExhibitionComplex', `params`='[]' WHERE `url`='/ru/venues/region';
			UPDATE {{route}} SET `route`='search/regionSummaryByExhibitionComplex', `params`='[]' WHERE `url`='/en/venues/region';

			ALTER TABLE {{citysummary}} 
			ADD COLUMN `exhibitionComplexCount` INT(11) NOT NULL AFTER `fairsCount`;

			UPDATE {{route}} SET `route`='search/citySummaryByExhibitionComplex', `params`='[]' WHERE `url`='/ru/venues/city';
			UPDATE {{route}} SET `route`='search/citySummaryByExhibitionComplex', `params`='[]' WHERE `url`='/en/venues/city';

			ALTER TABLE {{industrysummary}} 
			DROP COLUMN `ready`;
			
			DELETE FROM {{trcity}}
			WHERE langId = 'en';

			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`)
			SELECT trParentId, 'en', name FROM {{trcity}}
			WHERE langId = 'ru';
";
	}

	public function downSql()
	{
		return "
			ALTER TABLE {{regionsummary}} 
			DROP COLUMN `exhibitionComplexCount`;
			
			ALTER TABLE {{citysummary}} 
			DROP COLUMN `exhibitionComplexCount`;
		 	";
	}
}