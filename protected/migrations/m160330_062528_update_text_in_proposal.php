<?php

class m160330_062528_update_text_in_proposal extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='209';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='246';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='248';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='250';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='252';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='153';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='152';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='151';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='150';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='149';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='148';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='141';
			UPDATE {{attributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 23.08.16 и на 100% после 21.09.16.' WHERE `id`='140';
			UPDATE {{attributeclass}} SET `label`='<div class=\"margin-top-20\">Шланги для подключения потребителей длиной до 10 метров включительно предоставляются без дополнительной платы. Если длина подключения превышает 10 метров, шланги необходимой длины предоставляются в аренду.</div>' WHERE `id`='212';
			UPDATE {{attributeclass}} SET `label`='Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ на дату платежа.' WHERE `id`='239';
			UPDATE {{attributeclass}} SET `label`='Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента.' WHERE `id`='239';
			UPDATE {{attributeclass}} SET `label`='Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента.' WHERE `id`='242';
		";
	}

}