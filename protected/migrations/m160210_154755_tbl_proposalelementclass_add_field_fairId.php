<?php

class m160210_154755_tbl_proposalelementclass_add_field_fairId extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{proposalelementclass}} DROP COLUMN `fairId`;
			ALTER TABLE {{proposalelementclass}} DROP COLUMN `description`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{proposalelementclass}} ADD `fairId` int;
			ALTER TABLE {{proposalelementclass}} ADD `description` text;
			UPDATE {{proposalelementclass}} SET `fairId`='645' WHERE `id`='1';
			UPDATE {{proposalelementclass}} SET `fairId`='645' WHERE `id`='2';
			UPDATE {{proposalelementclass}} SET `fairId`='645' WHERE `id`='3';
			UPDATE {{proposalelementclass}} SET `description`='Lorem Ipsum' WHERE `id`='1';
			UPDATE {{proposalelementclass}} SET `description`='dolor sit' WHERE `id`='2';
			UPDATE {{proposalelementclass}} SET `description`='amet, consectetur adipiscing elit' WHERE `id`='3';
		";
	}
}