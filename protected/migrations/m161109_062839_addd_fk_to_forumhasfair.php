<?php

class m161109_062839_addd_fk_to_forumhasfair extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{forumhasfairs}} 
            DROP FOREIGN KEY `fk_forumId`,
            DROP FOREIGN KEY `fk_fairId`;
            ALTER TABLE {{forumhasfairs}} 
            DROP INDEX `fk_fairId_idx` ,
            ADD UNIQUE INDEX `fk_fairId_idx` (`fairId` ASC),
            DROP INDEX `forum_fair` ;

        ALTER TABLE {{forumhasfairs}} 
        ADD INDEX `fk_forumhasfairs_forumId_idx` (`forumId` ASC);
        ALTER TABLE {{forumhasfairs}} 
        ADD CONSTRAINT `fk_forumhasfairs_forumId`
          FOREIGN KEY (`forumId`)
          REFERENCES {{fair}} (`id`)
          ON DELETE CASCADE
          ON UPDATE CASCADE,
        ADD CONSTRAINT `fk_forumhasfairs_fairId`
          FOREIGN KEY (`fairId`)
          REFERENCES {{fair}} (`id`)
          ON DELETE CASCADE
          ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}