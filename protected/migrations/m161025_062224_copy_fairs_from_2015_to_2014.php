<?php

class m161025_062224_copy_fairs_from_2015_to_2014 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_fair_2015_to_2014`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE reverse_year_interval INT DEFAULT 1;
                DECLARE source_fair_id INT DEFAULT 0;
                DECLARE uniq_text_save_pattern VARCHAR(50) DEFAULT \"<p>Раздел%\";
                DECLARE copy CURSOR FOR SELECT f15.id FROM {{fair}} f15
                                        LEFT JOIN {{fair}} f14 ON f14.storyId = f15.storyId 
                                            AND YEAR(f14.beginDate) = 2014 
                                            AND MONTH(f14.beginDate) = MONTH(f15.beginDate)
                                        WHERE YEAR(f15.beginDate) = 2015 
                                            AND YEAR(f15.statisticsDate) = 2014 
                                            AND f14.id IS NULL AND f15.active = 1;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
            OPEN copy;
            
            read_loop: LOOP
                FETCH copy INTO source_fair_id;
                
                IF done THEN 
                    LEAVE read_loop;
                END IF;
                
                START TRANSACTION;
                    INSERT INTO {{fair}}(
                                `logoId`,`userId`,`rating`,`participationPrice`,`registrationFee`,
                                `price`,`organizerId`,`active`,`fairIsForum`,`frequency`,`description`,
                                `exhibitionComplexId`,`beginDate`,`endDate`,`beginMountingDate`,
                                `endMountingDate`,`beginDemountingDate`,`endDemountingDate`,`squareGross`,
                                `squareNet`,`members`,`visitors`,`activateControl`,`originalId`,`keyword`,
                                `uniqueText`,`statistics`,`currencyId`,`fairTypeId`,`statusId`,`site`,
                                `mailLogoId`,`lang`,`shard`,`storyId`,`statisticsDate`
                            ) SELECT
                                `logoId`,`userId`,`rating`,`participationPrice`,`registrationFee`,
                                `price`,`organizerId`,`active`,`fairIsForum`,`frequency`,`description`,
                                `exhibitionComplexId`,
                                `beginDate` - INTERVAL `reverse_year_interval` YEAR,
                                `endDate`              - INTERVAL `reverse_year_interval` YEAR,
                                `beginMountingDate`    - INTERVAL `reverse_year_interval` YEAR,
                                `endMountingDate`      - INTERVAL `reverse_year_interval` YEAR,
                                `beginDemountingDate`  - INTERVAL `reverse_year_interval` YEAR,
                                `endDemountingDate`    - INTERVAL `reverse_year_interval` YEAR,
                                `squareGross`,`squareNet`,`members`,`visitors`,`activateControl`,
                                `originalId`,`keyword`,`uniqueText`,`statistics`,`currencyId`,
                                `fairTypeId`,`statusId`,`site`,`mailLogoId`,`lang`,`shard`,`storyId`,
                                '2014-01-01'
                            FROM {{fair}}
                            WHERE `id` = source_fair_id;
                COMMIT;
                
                SET @new_fair_id = LAST_INSERT_ID();
                
                START TRANSACTION;
                    UPDATE {{fairinfo}} SET `fairId` = @new_fair_id WHERE `fairId` = source_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    INSERT INTO {{trfair}} (`trParentId`,`langId`,`name`,`description`,`shortUrl`)
                    SELECT @new_fair_id, `langId`, `name`, `description`,`shortUrl` FROM {{trfair}} WHERE `trParentId` = source_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    INSERT INTO {{fairhasindustry}} (`fairId`,`industryId`)
                    SELECT @new_fair_id, `industryId` FROM {{fairhasindustry}} WHERE `fairId` = source_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    UPDATE {{fair}} SET `rating` = 3 WHERE `id` = source_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    UPDATE {{fair}} SET `uniqueText` = NULL WHERE `id` = @new_fair_id AND `uniqueText` NOT LIKE uniq_text_save_pattern;
                COMMIT;
                            
            END LOOP;
        CLOSE copy;
    END;
    
    CALL copy_fair_2015_to_2014();
    DROP PROCEDURE IF EXISTS `copy_fair_2015_to_2014`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}