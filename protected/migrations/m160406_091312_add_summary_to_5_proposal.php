<?php

class m160406_091312_add_summary_to_5_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{attributemodel}} WHERE `id`=\'57\';
			DELETE FROM {{attributeclass}} WHERE `id`=\'518\';
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_summary', 'string', '0', '', '0', '518', NULL, 'summary', '0');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('5', '518', '0', '0', '100');
		";
	}
}