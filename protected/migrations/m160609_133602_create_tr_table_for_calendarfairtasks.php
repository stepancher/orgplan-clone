<?php

class m160609_133602_create_tr_table_for_calendarfairtasks extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{trcalendarfairtasks}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "

			CREATE TABLE {{trcalendarfairtasks}} (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`trParentId` varchar(255) DEFAULT NULL,
			`langId` varchar(45) DEFAULT NULL,
			`name` text,
			`desc` text,
			PRIMARY KEY (`id`),
			UNIQUE KEY `cft_uniq` (`langId`,`trParentId`)
			) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

			insert into {{trcalendarfairtasks}} (trParentId, langId, name, `desc`)
			select uuid, 'ru', name, `desc` from {{calendarfairtasks}};

			insert into {{trcalendarfairtasks}} (trParentId, langId, name, `desc`)
			select uuid, 'en', name, `desc` from {{calendarfairtasks}};

			insert into {{trcalendarfairtasks}} (trParentId, langId, name, `desc`)
			select uuid, 'de', name, `desc` from {{calendarfairtasks}};

			ALTER TABLE {{calendarfairtasks}}
			DROP COLUMN `desc`,
			DROP COLUMN `name`;
		";
	}
}