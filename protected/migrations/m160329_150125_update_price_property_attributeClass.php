<?php

class m160329_150125_update_price_property_attributeClass extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('278', 'USD', '140', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('331', 'USD', '246', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('551', 'USD', '248', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('890', 'USD', '250', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('975', 'USD', '252', 'price');
			UPDATE {{attributeclassproperty}} SET `value`='446' WHERE `id`='6';
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('211', 'USD', '148', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('508', 'USD', '149', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('253', 'USD', '150', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('39', 'USD', '151', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('69', 'USD', '152', 'price');
			INSERT INTO {{attributeclassproperty}} (`value`, `extra`, `attributeClass`, `class`) VALUES ('142', 'USD', '153', 'price');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('140', '140', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('246', '246', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('248', '248', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('250', '250', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('252', '252', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('148', '148', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('149', '149', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('150', '150', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('151', '151', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('152', '152', 'inherit');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('153', '153', 'inherit');
		";
	}
}