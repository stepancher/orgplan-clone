<?php

class m161201_102725_move_tbl_documents_into_shards extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        list($peace1, $peace2, $dbSourceName) = explode('=', Yii::app()->db->connectionString);
        list($peace1, $peace2, $dbF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        $sql= '';

        /**
         * @var Fair $fair
         */
        foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){
                $dbTargetName = Yii::app()->dbMan->createShardName($fair->id);

                $sql = $sql."
                    CREATE TABLE `{$dbTargetName}`.tbl_documents LIKE `{$dbSourceName}`.tbl_documents ;
                    INSERT INTO  `{$dbTargetName}`.tbl_documents SELECT * FROM `{$dbSourceName}`.tbl_documents ; 
                    
                    CREATE TABLE `{$dbTargetName}`.tbl_trdocuments LIKE `{$dbSourceName}`.tbl_trdocuments ;
                    INSERT INTO  `{$dbTargetName}`.tbl_trdocuments SELECT * FROM `{$dbSourceName}`.tbl_trdocuments ; 
                    
                    DELETE FROM  `{$dbTargetName}`.tbl_trdocuments WHERE trParentId NOT IN (SELECT id FROM `{$dbTargetName}`.tbl_documents);
                ";
            }
        }

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}