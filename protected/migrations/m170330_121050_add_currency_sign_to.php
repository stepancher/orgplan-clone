<?php

class m170330_121050_add_currency_sign_to extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{currency}} 
            ADD COLUMN `icon` VARCHAR(55) NULL DEFAULT NULL AFTER `course`;
            UPDATE {{currency}} SET `icon`='₽' WHERE `id`='1';
            UPDATE {{currency}} SET `icon`='$' WHERE `id`='2';
            UPDATE {{currency}} SET `icon`='€' WHERE `id`='3';
            UPDATE {{currency}} SET `icon`='¥' WHERE `id`='6';
            UPDATE {{currency}} SET `icon`='₴' WHERE `id`='7';
            UPDATE {{currency}} SET `icon`='£' WHERE `id`='8';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}