<?php

class m160517_150412_translates_for_2_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '261', 'en', 'This form should be filled in until August 25, 2016. Please return the application form by fax +7(495)781-37-05, or e-mail: agrosalon@agrosalon.ru', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '262', 'en', 'Standard stand type:', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '263', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '264', 'en', 'Classic', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '265', 'en', 'Premium', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '266', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '267', 'en', 'Walls on perimeter', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '268', 'en', 'Carpet (black)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '269', 'en', 'Approximate image of the stand', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '274', 'en', 'Configuration', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '281', 'en', 'Fascia panel with company''s name (up to 15 symbols)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '282', 'en', '3 chairs (code 300)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '283', 'en', 'Table (code 310 or 314)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '284', 'en', 'Cabinet (code 317)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '285', 'en', 'Spotlight (code 510) 1 pcs. for 9 sq.m.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '286', 'en', 'Socket (code 504a) 1 kW', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '287', 'en', 'Waste-paper basket (code 377)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '288', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '289', 'en', 'Approximate image of the stand', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '290', 'en', 'Configuration', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '291', 'en', 'Walls on perimeter', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '292', 'en', 'Carpet (black)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '293', 'en', 'Fascia panel with company''s name (up to 15 symbols)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '294', 'en', '3 chairs (code 300)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '295', 'en', 'Bar stool (code 306) 1 pcs.for 18 sq.m', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '296', 'en', 'Table (code 310 or 314)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '297', 'en', 'Cabinet (code 317)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '298', 'en', 'Coat rack (console) (code 331)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '299', 'en', 'Spotlight (code 510) 1 pcs. for 9 sq.m.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '300', 'en', 'Socket (code 504a) 1 kW', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '301', 'en', 'Business or private part:', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '302', 'en', 'Lockable door (code 204a)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '303', 'en', 'Wall panels (code 220) 1 pcs. for 9 sq.m.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '304', 'en', 'Waste-paper basket (code 377)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '305', 'en', 'Space', NULL, NULL, 'STAND SIZE Cost of services increases by 50% if ordered after Aug.25, 2016, by 100% if ordered after Sep.23, 2016'
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '307', 'en', 'Space', NULL, NULL, 'STAND SIZE Cost of services increases by 50% if ordered after Aug.25, 2016, by 100% if ordered after Sep.23, 2016'
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '308', 'en', 'Required stand size:', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '309', 'en', 'Length', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '310', 'en', 'Width', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '311', 'en', 'If the total space of your stand is bigger than the ordered standard stand site, you must additionally order the carpet for the rest part of the space floor (Application No.3)', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '306', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '312', 'en', 'Stand layout:', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '313', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '314', 'en', 'If you need more space for drawing, you can draw the plan on a separate sheet and attach it to this application.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '2001', 'en', 'Attach the plan. Also you can download a form.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '2002', 'en', 'Download a form \"Stand layout\"', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '315', 'en', 'Ancillary equipment:', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '316', 'en', 'The equipment scheme needs to be imaged in the printed version of the application.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '317', 'en', 'Fascia name:', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '318', 'en', 'Name', NULL, 'Up to the 15th symbols is free of charge', 'STAND SIZE Cost of services increases by 50% if ordered after Aug.25, 2016, by 100% if ordered after Sep.23, 2016'
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '320', 'en', 'Logo on the fascia panel', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '321', 'en', 'Logos are only submitted in the electronic form, vector format only; the production price depends on the complexity of the logo.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '322', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '513', 'en', NULL, 'pcs.', NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '514', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '515', 'en', 'Payments shall be effected in USD. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '516', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '517', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies. By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered. Any claims related to the perfomance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be perfomed with the proper quality and accepted by the Exhibitor.', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '839', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '840', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '841', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '842', 'en', 'Exhibitor', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '843', 'en', 'position', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '844', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '845', 'en', 'Name', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '846', 'en', 'Organizer', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '847', 'en', 'position', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '848', 'en', NULL, NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '849', 'en', 'Name', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '850', 'en', 'Signature', NULL, NULL, NULL
			);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
				  '851', 'en', 'Signature', NULL, NULL, NULL
			);

			UPDATE {{attributeclass}} SET `label`=NULL WHERE `id`='262';
			ALTER TABLE {{attributeclass}} 
			DROP COLUMN `label`;
			DELETE FROM {{attributeclassdependency}} WHERE `id`='782';
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '598', '963', 'summaryAndRound', '{\"min\": 2}', '0');
			UPDATE {{attributeclassdependency}} SET `function`='aggregate' WHERE `id`='778';
			UPDATE {{attributeclass}} SET `name`='n9_2_work', `group`=NULL, `priority`='0.45' WHERE `id`='628';
			UPDATE {{attributeclass}} SET `priority`='0' WHERE `id`='628';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='643';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='654';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='628';
			UPDATE {{attributeclass}} SET `priority`='2' WHERE `id`='654';
			UPDATE {{attributeclass}} SET `priority`='2' WHERE `id`='643';
			UPDATE {{attributemodel}} SET `priority`='22' WHERE `id`='84';
			UPDATE {{attributemodel}} SET `priority`='21' WHERE `id`='83';
			UPDATE {{attributeclass}} SET `parent`='654', `super`='654' WHERE `id`='991';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'n9_summary', 'string', '0', '654', '654', 'summary', '0');
			UPDATE {{attributeclass}} SET `priority`='0' WHERE `id`='2005';
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2005', 'руб', 'currency');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_hint_last', 'string', '0', '654', '654', '-1', 'hintDanger', '0');
			UPDATE {{attributeclass}} SET `collapse`='10' WHERE `id`='2005';
			UPDATE {{attributeclass}} SET `collapse`='9' WHERE `id`='2006';
			UPDATE {{attributeclass}} SET `priority`='10', `collapse`='0' WHERE `id`='2005';
			UPDATE {{attributeclass}} SET `priority`='9', `collapse`='0' WHERE `id`='2006';
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2006', 'ru', 'Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.\nЗаявка является приложение к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 18:00 дня проведения работ.\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке, считаются выполненными надлежащего качества и принятыми Экспонентом.');
			UPDATE {{trattributeclass}} SET `label`='Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.\nЗаявка является приложение к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 18:00 дня проведения работ.\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке, считаются выполненными надлежащего качества и принятыми Экспонентом.\n' WHERE `id`='1095';
			UPDATE {{trattributeclass}} SET `label`='Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br>\nЗаявка является приложение к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 18:00 дня проведения работ.<br>\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке, считаются выполненными надлежащего качества и принятыми Экспонентом.\n' WHERE `id`='1095';
			UPDATE {{trattributeclass}} SET `label`='Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br><br>\nЗаявка является приложение к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 18:00 дня проведения работ.<br><br>\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке, считаются выполненными надлежащего качества и принятыми Экспонентом.\n' WHERE `id`='1095';

	    ";
	}
}