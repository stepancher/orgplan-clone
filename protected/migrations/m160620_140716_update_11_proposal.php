<?php

class m160620_140716_update_11_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		UPDATE {{attributeclass}} SET `dataType`='int' WHERE `id`='2101';
		UPDATE {{attributeclass}} SET `dataType`='int' WHERE `id`='2104';
		UPDATE {{attributeclass}} SET `parent`='2187', `class`='dropDownOption' WHERE `id`='2106';
		UPDATE {{attributeclass}} SET `parent`='2187', `class`='dropDownOption' WHERE `id`='2107';
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n11_hall_dropdown', 'enum', '0', '0', '2088', '2088', '10', 'dropDown', '0');
		UPDATE {{attributeclass}} SET `parent`='2105', `super`='2105' WHERE `id`='2187';
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2187', 'ru', 'Тип помещения');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2187', 'en', 'Hall for the event');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES (NULL, '19', '2087', '0', '0', '9');
		UPDATE {{trattributeclass}} SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 18 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='1562';
";
	}
}