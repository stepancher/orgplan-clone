<?php

class m160629_120946_update_7_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('23', 'en', 'APPLICATION No.7', 'SECURITY, CLEANING, STAFF');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n7', 'int', '1', '0', '0', '2495', 'partiesSignature', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n7', 'int', '1', '2495', '2495', '1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_right_block_n7', 'int', '1', '2495', '2495', '2', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n7_1', 'string', '1', '2496', '2495', '1', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n7_2', 'string', '1', '2496', '2495', '2', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n7_2.5', 'string', '1', '2496', '2495', '4', 'headerH2', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n7_3', 'string', '1', '2496', '2495', '3', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n7_1', 'string', '1', '2497', '2495', '1', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n7_2', 'string', '1', '2497', '2495', '2', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n7_2.5', 'string', '1', '2497', '2495', '4', 'headerH2', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n7_3', 'string', '1', '2497', '2495', '3', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n7_4', 'string', '1', '2496', '2495', '5', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n7_4', 'string', '1', '2497', '2495', '5', 'text', '0');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('23', '2495', '0', '0', '60');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2498', 'ru', 'parties_signature_row_left_n7_1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2499', 'ru', 'parties_signature_row_left_n7_2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2500', 'ru', 'parties_signature_row_left_n7_2.5');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2501', 'ru', 'parties_signature_row_left_n7_3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2502', 'ru', 'parties_signature_row_right_n7_1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2503', 'ru', 'parties_signature_row_right_n7_2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2504', 'ru', 'parties_signature_row_right_n7_2.5');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2505', 'ru', 'parties_signature_row_right_n7_3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2506', 'ru', 'parties_signature_row_left_n7_4');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2507', 'ru', 'parties_signature_row_right_n7_4');
		UPDATE {{trattributeclass}} SET `label`='дата' WHERE `id`='2766';
		UPDATE {{trattributeclass}} SET `label`='дата' WHERE `id`='2767';
		UPDATE {{trattributeclass}} SET `label`='Экспонент' WHERE `id`='2758';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2759';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2760';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2761';
		UPDATE {{trattributeclass}} SET `label`='Организатор' WHERE `id`='2762';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2763';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2764';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2765';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2766';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2767';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2765';
		UPDATE {{trattributeclass}} SET `label`='дата' WHERE `id`='2764';
		UPDATE {{trattributeclass}} SET `label`='дата' WHERE `id`='2760';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2761';
		UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='2504';
		UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='2500';
		DELETE FROM {{attributeclass}} WHERE `id`='2500';
		DELETE FROM {{attributeclass}} WHERE `id`='2504';
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2498', 'en', 'Exhibitor');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2499', 'en', 'position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2501', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2502', 'en', 'Organizer');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2503', 'en', 'position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2505', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2506', 'en', 'Signature');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2507', 'en', 'Signature');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2503', 'organizerPost', 'envVariable');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2505', 'organizerName', 'envVariable');
		UPDATE {{trattributeclassproperty}} SET `value`='чел.' WHERE `id`='2672';
		UPDATE {{trattributeclassproperty}} SET `value`='чел.' WHERE `id`='2674';

		";
	}
}