<?php

class m161021_165259_delete_proposalelementclasses extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalProtoplan) = explode('=', Yii::app()->dbProposalF10943->connectionString);
        
        return "
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='1';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='2';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='3';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='5';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='7';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='10';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='18';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='19';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='20';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='21';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='22';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='23';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='25';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='26';
            DELETE FROM {$dbProposalProtoplan}.tbl_proposalelementclass WHERE `id`='27';
            
            UPDATE {$dbProposalProtoplan}.tbl_organizerexponent SET `organizerId`='4066' WHERE `id`='288';
            UPDATE {$dbProposalProtoplan}.tbl_organizerexponent SET `organizerId`='4066' WHERE `id`='289';

            DELETE FROM {{dependences}} WHERE `id`='559';
            DELETE FROM {{dependences}} WHERE `id`='560';
            DELETE FROM {{dependences}} WHERE `id`='561';
            DELETE FROM {{dependences}} WHERE `id`='562';
            DELETE FROM {{dependences}} WHERE `id`='563';
            DELETE FROM {{dependences}} WHERE `id`='564';
            DELETE FROM {{dependences}} WHERE `id`='565';
            DELETE FROM {{dependences}} WHERE `id`='566';
            DELETE FROM {{dependences}} WHERE `id`='567';
            DELETE FROM {{dependences}} WHERE `id`='568';
            DELETE FROM {{dependences}} WHERE `id`='569';
            DELETE FROM {{dependences}} WHERE `id`='570';
            DELETE FROM {{dependences}} WHERE `id`='571';
            DELETE FROM {{dependences}} WHERE `id`='572';
            DELETE FROM {{dependences}} WHERE `id`='573';
            DELETE FROM {{dependences}} WHERE `id`='574';
            DELETE FROM {{dependences}} WHERE `id`='575';
            DELETE FROM {{dependences}} WHERE `id`='576';
            DELETE FROM {{dependences}} WHERE `id`='577';
            DELETE FROM {{dependences}} WHERE `id`='578';
            DELETE FROM {{dependences}} WHERE `id`='579';
            DELETE FROM {{dependences}} WHERE `id`='580';
            DELETE FROM {{dependences}} WHERE `id`='581';
            DELETE FROM {{dependences}} WHERE `id`='582';
            DELETE FROM {{dependences}} WHERE `id`='583';
            DELETE FROM {{dependences}} WHERE `id`='584';
            DELETE FROM {{dependences}} WHERE `id`='585';
            DELETE FROM {{dependences}} WHERE `id`='586';
            DELETE FROM {{dependences}} WHERE `id`='587';
            DELETE FROM {{dependences}} WHERE `id`='588';
            DELETE FROM {{dependences}} WHERE `id`='589';
            DELETE FROM {{dependences}} WHERE `id`='590';
            DELETE FROM {{dependences}} WHERE `id`='591';
            DELETE FROM {{dependences}} WHERE `id`='592';
            DELETE FROM {{dependences}} WHERE `id`='593';
            DELETE FROM {{dependences}} WHERE `id`='594';
            DELETE FROM {{dependences}} WHERE `id`='595';
            DELETE FROM {{dependences}} WHERE `id`='596';
            DELETE FROM {{dependences}} WHERE `id`='597';
            DELETE FROM {{dependences}} WHERE `id`='598';
            DELETE FROM {{dependences}} WHERE `id`='599';
            DELETE FROM {{dependences}} WHERE `id`='600';
            DELETE FROM {{dependences}} WHERE `id`='601';
            DELETE FROM {{dependences}} WHERE `id`='602';
            DELETE FROM {{dependences}} WHERE `id`='603';
            DELETE FROM {{dependences}} WHERE `id`='604';
            DELETE FROM {{dependences}} WHERE `id`='605';
            DELETE FROM {{dependences}} WHERE `id`='606';
            DELETE FROM {{dependences}} WHERE `id`='607';
            DELETE FROM {{dependences}} WHERE `id`='608';
            DELETE FROM {{dependences}} WHERE `id`='609';
            DELETE FROM {{dependences}} WHERE `id`='610';
            DELETE FROM {{dependences}} WHERE `id`='611';
            DELETE FROM {{dependences}} WHERE `id`='612';
            DELETE FROM {{dependences}} WHERE `id`='613';
            DELETE FROM {{dependences}} WHERE `id`='614';
            DELETE FROM {{dependences}} WHERE `id`='615';
            DELETE FROM {{dependences}} WHERE `id`='616';
            DELETE FROM {{dependences}} WHERE `id`='617';
            DELETE FROM {{dependences}} WHERE `id`='618';
            DELETE FROM {{dependences}} WHERE `id`='619';
            DELETE FROM {{dependences}} WHERE `id`='620';
            DELETE FROM {{dependences}} WHERE `id`='621';
            DELETE FROM {{dependences}} WHERE `id`='622';
            DELETE FROM {{dependences}} WHERE `id`='623';
            DELETE FROM {{dependences}} WHERE `id`='624';
            DELETE FROM {{dependences}} WHERE `id`='625';
            DELETE FROM {{dependences}} WHERE `id`='626';
            DELETE FROM {{dependences}} WHERE `id`='627';
            DELETE FROM {{dependences}} WHERE `id`='628';
            DELETE FROM {{dependences}} WHERE `id`='629';
            DELETE FROM {{dependences}} WHERE `id`='630';
            DELETE FROM {{dependences}} WHERE `id`='631';
            DELETE FROM {{dependences}} WHERE `id`='632';
            DELETE FROM {{dependences}} WHERE `id`='633';
            DELETE FROM {{dependences}} WHERE `id`='634';
            DELETE FROM {{dependences}} WHERE `id`='635';
            DELETE FROM {{dependences}} WHERE `id`='636';
            DELETE FROM {{dependences}} WHERE `id`='637';
            DELETE FROM {{dependences}} WHERE `id`='638';
            DELETE FROM {{dependences}} WHERE `id`='639';
            DELETE FROM {{dependences}} WHERE `id`='640';
            DELETE FROM {{dependences}} WHERE `id`='641';
            DELETE FROM {{dependences}} WHERE `id`='642';
            DELETE FROM {{dependences}} WHERE `id`='643';
            DELETE FROM {{dependences}} WHERE `id`='644';
            DELETE FROM {{dependences}} WHERE `id`='645';
            DELETE FROM {{dependences}} WHERE `id`='646';
            DELETE FROM {{dependences}} WHERE `id`='647';
            DELETE FROM {{dependences}} WHERE `id`='648';
            DELETE FROM {{dependences}} WHERE `id`='649';
            DELETE FROM {{dependences}} WHERE `id`='650';
            DELETE FROM {{dependences}} WHERE `id`='651';
            DELETE FROM {{dependences}} WHERE `id`='652';
            DELETE FROM {{dependences}} WHERE `id`='653';
            DELETE FROM {{dependences}} WHERE `id`='654';
            DELETE FROM {{dependences}} WHERE `id`='655';
            DELETE FROM {{dependences}} WHERE `id`='656';
            DELETE FROM {{dependences}} WHERE `id`='657';
            DELETE FROM {{dependences}} WHERE `id`='658';
            DELETE FROM {{dependences}} WHERE `id`='659';
            DELETE FROM {{dependences}} WHERE `id`='660';
            DELETE FROM {{dependences}} WHERE `id`='661';
            DELETE FROM {{dependences}} WHERE `id`='662';
            DELETE FROM {{dependences}} WHERE `id`='663';
            DELETE FROM {{dependences}} WHERE `id`='664';
            DELETE FROM {{dependences}} WHERE `id`='665';
            DELETE FROM {{dependences}} WHERE `id`='666';
            DELETE FROM {{dependences}} WHERE `id`='667';
            DELETE FROM {{dependences}} WHERE `id`='738';
            DELETE FROM {{dependences}} WHERE `id`='739';
            DELETE FROM {{dependences}} WHERE `id`='740';
            DELETE FROM {{dependences}} WHERE `id`='741';
            DELETE FROM {{dependences}} WHERE `id`='742';
            DELETE FROM {{dependences}} WHERE `id`='745';
            DELETE FROM {{dependences}} WHERE `id`='746';
            DELETE FROM {{dependences}} WHERE `id`='747';
            DELETE FROM {{dependences}} WHERE `id`='748';
            DELETE FROM {{dependences}} WHERE `id`='749';
            DELETE FROM {{dependences}} WHERE `id`='758';
            DELETE FROM {{dependences}} WHERE `id`='759';
            DELETE FROM {{dependences}} WHERE `id`='760';
            DELETE FROM {{dependences}} WHERE `id`='761';
            DELETE FROM {{dependences}} WHERE `id`='762';
            DELETE FROM {{dependences}} WHERE `id`='763';
            DELETE FROM {{dependences}} WHERE `id`='764';
            DELETE FROM {{dependences}} WHERE `id`='765';
            DELETE FROM {{dependences}} WHERE `id`='766';
            DELETE FROM {{dependences}} WHERE `id`='767';
            DELETE FROM {{dependences}} WHERE `id`='768';
            DELETE FROM {{dependences}} WHERE `id`='775';
            DELETE FROM {{dependences}} WHERE `id`='776';
            DELETE FROM {{dependences}} WHERE `id`='777';
            DELETE FROM {{dependences}} WHERE `id`='778';
            DELETE FROM {{dependences}} WHERE `id`='779';
            DELETE FROM {{dependences}} WHERE `id`='780';
            DELETE FROM {{dependences}} WHERE `id`='781';
            DELETE FROM {{dependences}} WHERE `id`='782';
            DELETE FROM {{dependences}} WHERE `id`='783';
            DELETE FROM {{dependences}} WHERE `id`='784';
            DELETE FROM {{dependences}} WHERE `id`='785';
            DELETE FROM {{dependences}} WHERE `id`='792';
            DELETE FROM {{dependences}} WHERE `id`='793';
            DELETE FROM {{dependences}} WHERE `id`='794';
            DELETE FROM {{dependences}} WHERE `id`='795';
            DELETE FROM {{dependences}} WHERE `id`='796';
            DELETE FROM {{dependences}} WHERE `id`='797';
            DELETE FROM {{dependences}} WHERE `id`='798';
            DELETE FROM {{dependences}} WHERE `id`='799';
            DELETE FROM {{dependences}} WHERE `id`='800';
            DELETE FROM {{dependences}} WHERE `id`='801';
            DELETE FROM {{dependences}} WHERE `id`='802';
            DELETE FROM {{dependences}} WHERE `id`='803';
            DELETE FROM {{dependences}} WHERE `id`='804';
            DELETE FROM {{dependences}} WHERE `id`='805';
            DELETE FROM {{dependences}} WHERE `id`='806';

            ALTER TABLE {{dependences}} 
            AUTO_INCREMENT = 5000 ;
            
            INSERT INTO {{calendarfairtasks}} (id, fairId, uuid, parent, `group`, duration, `date`, color, completeButton, `first`) VALUES
            ('4990', '10943', '33755881984295000', NULL,                '33755881984295000', NULL, '2017-09-10 23:59:59', '',         '0', '0'),
            ('4991', '10943', '33755881984295001', '33755881984295000', '33755881984295001', '44', NULL,                  '#f6a800',  '1', '0'),
            ('4992', '10943', '33755881984295002', NULL,                '33755881984295000', '29', '2017-10-21 00:00:01', '',         '0', '0'),
            ('4993', '10943', '33755881984295003', '33755881984295002', '33755881984295003', '29',  NULL,                  '#f6a800', '1', '0'),
            ('4994', '10943', '33755881984295004', NULL,                '33755881984295000', '11', '2017-11-20 00:00:01', '',         '0', '0'),
            ('4995', '10943', '33755881984295005', '33755881984295004', '33755881984295005', '11',  NULL,                  '#f96a0e', '1', '0'),
            
            ('4996', '10943', '33755881984295006', NULL,                '33755881984295006', NULL, '2017-09-30 23:59:59',  NULL,      '0', '0'),
            ('4997', '10943', '33755881984295007', '33755881984295006', '33755881984295007', '65', NULL,                  '#f6a800',  '1', '0');




            INSERT INTO {{dependences}} (`action`, params, run) VALUES
            
            
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":4, \"save\":1}', 'calendar/gantt/add/id/4990'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":4, \"send\":1}', 'calendar/gantt/add/id/4990'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":4, \"send\":1}', 'calendar/gantt/complete/id/4990'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":4, \"send\":1}', 'calendar/gantt/complete/id/4992'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":4, \"send\":1}', 'calendar/gantt/complete/id/4994'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":4}', 'calendar/gantt/add/id/4990'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":4}', 'calendar/gantt/complete/id/4990'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":4}', 'calendar/gantt/complete/id/4992'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":4}', 'calendar/gantt/complete/id/4994'),
            
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/reject/id/4990/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/reject/id/4991/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/reject/id/4992/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/reject/id/4993/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/reject/id/4994/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/reject/id/4995/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/add/id/4990'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/add/id/4992'),
            ('proposal/proposal/reject', '{\"ecId\":4}', 'calendar/gantt/add/id/4994'),
            
            ('proposal/proposal/reject', '{\"ecId\":24}', 'calendar/gantt/reject/id/4996/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":24}', 'calendar/gantt/reject/id/4997/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":24}', 'calendar/gantt/add/id/4996'),
            
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":24, \"save\":1}', 'calendar/gantt/add/id/4996'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":24, \"send\":1}', 'calendar/gantt/add/id/4996'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":24, \"send\":1}', 'calendar/gantt/complete/id/4996'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":24}', 'calendar/gantt/add/id/4997'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":24}', 'calendar/gantt/complete/id/4996');


            
 

          INSERT INTO {{trcalendarfairtasks}} (trParentId, langId, `name`, `desc`) VALUES
            ('33755881984295000','en','Send the Application No.2 STANDARD STAND', 'The task will be completed automatically after sending the Application No.2'),
            ('33755881984295000','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД', 'Задача завершится автоматически, когда будет отправлена заявка №2'),
            ('33755881984295000','de','Send the Application No.2 STANDARD STAND', 'The task will be completed automatically after sending the Application No.2'),
            
            ('33755881984295001','en','Pay for the service ordered (Application No.2)', 'Click FINISH if you have paid'),
            ('33755881984295001','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД', 'Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295001','de','Pay for the service ordered (Application No.2)', 'Click FINISH if you have paid'),
            
            ('33755881984295002','en','Send the Application No.2 STANDARD STAND (increase of price 50%)', 'The task will be completed automatically after sending the Application No.2'),
            ('33755881984295002','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)', 'Задача завершится автоматически, когда будет отправлена заявка №2'),
            ('33755881984295002','de','Send the Application No.2 STANDARD STAND (increase of price 50%)', 'The task will be completed automatically after sending the Application No.2'),
            
            ('33755881984295003','en','Pay for the service ordered (Application No.2) (increase of price 50%)' , 'Click FINISH if you have paid'),
            ('33755881984295003','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)', 'Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295003','de','Pay for the service ordered (Application No.2) (increase of price 50%)', 'Click FINISH if you have paid'),
            
            ('33755881984295004','en','Send the Application No.2 STANDARD STAND (increase of price 100%)', 'The task will be completed automatically after sending the Application No.2'),
            ('33755881984295004','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)', 'Задача завершится автоматически, когда будет отправлена заявка №2'),
            ('33755881984295004','de','Send the Application No.2 STANDARD STAND (increase of price 100%)', 'The task will be completed automatically after sending the Application No.2'),
            
            ('33755881984295005','en','Pay for the service ordered (Application No.2) (increase of price 100%)', 'Click FINISH if you have paid'),
            ('33755881984295005','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)', 'Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295005','de','Pay for the service ordered (Application No.2) (increase of price 100%)', 'Click FINISH if you have paid'),
            
            ('33755881984295006','ru','Отправить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ . ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Задача завершится автоматически, когда будет отправлена заявка №8'),
            ('33755881984295006','en','Send the Application No.8 INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION','The task will be completed automatically after sending the Application No.12a'),
            ('33755881984295006','de','Send the Application No.8 INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION','The task will be completed automatically after sending the Application No.12a'),

            ('33755881984295007','ru','Оплатить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ . ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295007','en','Pay for the service ordered (Application No.8)','Click FINISH if you have paid'),
            ('33755881984295007','de','Pay for the service ordered (Application No.8)','Click FINISH if you have paid');
            
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}