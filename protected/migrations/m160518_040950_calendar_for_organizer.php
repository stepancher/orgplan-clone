<?php

class m160518_040950_calendar_for_organizer extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		return true;
	}

	private function upSql() {

		return "
			CREATE TABLE {{calendarfairtemptasks}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `fairId` int(11) NOT NULL,
			  `uuid` char(36) NOT NULL,
			  `parent` char(36) DEFAULT NULL,
			  `group` char(36) DEFAULT NULL,
			  `name` varchar(245) NOT NULL,
			  `desc` text,
			  `duration` mediumint(8) DEFAULT NULL,
			  `date` datetime DEFAULT NULL,
			  `color` varchar(45) DEFAULT NULL,
			  `completeButton` tinyint(1) NOT NULL DEFAULT '0',
			  `first` tinyint(1) NOT NULL DEFAULT '0',
			  `action` tinyint(1) NOT NULL DEFAULT '0',
			  `editDate` datetime NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			UPDATE {{calendarfairtasks}} SET `group`='20' WHERE `id`='20';
			UPDATE {{calendarfairtasks}} SET `group`='20' WHERE `id`='22';
			UPDATE {{calendarfairtasks}} SET `group`='20' WHERE `id`='24';
			UPDATE {{calendarfairtasks}} SET `group`='31' WHERE `id`='31';
			UPDATE {{calendarfairtasks}} SET `group`='31' WHERE `id`='33';
			UPDATE {{calendarfairtasks}} SET `group`='31' WHERE `id`='35';
			UPDATE {{calendarfairtasks}} SET `group`='37' WHERE `id`='37';
			UPDATE {{calendarfairtasks}} SET `group`='37' WHERE `id`='39';
			UPDATE {{calendarfairtasks}} SET `group`='37' WHERE `id`='41';
			UPDATE {{calendarfairtasks}} SET `group`='43' WHERE `id`='43';
			UPDATE {{calendarfairtasks}} SET `group`='43' WHERE `id`='45';
			UPDATE {{calendarfairtasks}} SET `group`='43' WHERE `id`='47';
			UPDATE {{calendarfairtasks}} SET `group`=NULL WHERE `id`='49';
			UPDATE {{calendarfairtasks}} SET `group`='26' WHERE `id`='26';
			UPDATE {{calendarfairtasks}} SET `group`='26' WHERE `id`='28';
			UPDATE {{calendarfairtasks}} SET `group`='26' WHERE `id`='64';
			UPDATE {{calendarfairtasks}} SET `group`='50' WHERE `id`='50';
			UPDATE {{calendarfairtasks}} SET `group`='50' WHERE `id`='52';
			UPDATE {{calendarfairtasks}} SET `group`='50' WHERE `id`='54';
			UPDATE {{calendarfairtasks}} SET `group`='56' WHERE `id`='56';
			UPDATE {{calendarfairtasks}} SET `group`='56' WHERE `id`='58';
			UPDATE {{calendarfairtasks}} SET `group`='56' WHERE `id`='60';

			UPDATE {{calendarfairtasks}} SET `group`='65' WHERE `id`='65';
			UPDATE {{calendarfairtasks}} SET `group`='65' WHERE `id`='70';
			UPDATE {{calendarfairtasks}} SET `group`='65' WHERE `id`='74';
			UPDATE {{calendarfairtasks}} SET `group`='65' WHERE `id`='86';
			UPDATE {{calendarfairtasks}} SET `group`='77' WHERE `id`='77';
			UPDATE {{calendarfairtasks}} SET `group`='77' WHERE `id`='79';
			UPDATE {{calendarfairtasks}} SET `group`='77' WHERE `id`='81';
			UPDATE {{calendarfairtasks}} SET `group`='87' WHERE `id`='87';
			UPDATE {{calendarfairtasks}} SET `group`='87' WHERE `id`='89';
			UPDATE {{calendarfairtasks}} SET `group`='87' WHERE `id`='91';
			UPDATE {{calendarfairtasks}} SET `group`='93' WHERE `id`='93';
			UPDATE {{calendarfairtasks}} SET `group`='93' WHERE `id`='95';
			UPDATE {{calendarfairtasks}} SET `group`='93' WHERE `id`='97';
			UPDATE {{calendarfairtasks}} SET `group`='99' WHERE `id`='99';
			UPDATE {{calendarfairtasks}} SET `group`='99' WHERE `id`='101';
			UPDATE {{calendarfairtasks}} SET `group`='99' WHERE `id`='103';
			UPDATE {{calendarfairtasks}} SET `group`=NULL WHERE `id`='105';
			UPDATE {{calendarfairtasks}} SET `group`='83' WHERE `id`='83';
			UPDATE {{calendarfairtasks}} SET `group`='83' WHERE `id`='85';
			UPDATE {{calendarfairtasks}} SET `group`='83' WHERE `id`='120';
			UPDATE {{calendarfairtasks}} SET `group`='106' WHERE `id`='106';
			UPDATE {{calendarfairtasks}} SET `group`='106' WHERE `id`='108';
			UPDATE {{calendarfairtasks}} SET `group`='106' WHERE `id`='110';
			UPDATE {{calendarfairtasks}} SET `group`='112' WHERE `id`='112';
			UPDATE {{calendarfairtasks}} SET `group`='112' WHERE `id`='114';
			UPDATE {{calendarfairtasks}} SET `group`='112' WHERE `id`='116';

			ALTER TABLE {{calendarfairtasks}}
			CHANGE COLUMN `fairId` `fairId` INT(11) NULL DEFAULT NULL AFTER `id`,
			CHANGE COLUMN `parent` `parent` CHAR(36) NULL DEFAULT NULL,
			CHANGE COLUMN `group` `group` CHAR(36) NULL DEFAULT NULL,
			ADD COLUMN `uuid` CHAR(36) NULL DEFAULT NULL AFTER `fairId`;

			UPDATE {{calendarfairtasks}} SET `uuid` = uuid_short();

			UPDATE {{calendarfairtasks}} task
			LEFT JOIN {{calendarfairtasks}} parent ON task.parent = parent.id
			LEFT JOIN {{calendarfairtasks}} `group` ON task.`group` = `group`.id
			SET task.`parent` = parent.uuid, task.`group` = `group`.uuid;

			UPDATE {{calendarfairtasks}}
			SET `group` = uuid
			WHERE `group` IS NULL;
			
			
			
			
			
		";
	}
}