<?php

class m161128_063303_delete_task1753_fairs extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE `id` = '11670';
            DELETE FROM {{fair}} WHERE `id` = '306';
            DELETE FROM {{fair}} WHERE `id` = '1727';
            DELETE FROM {{fair}} WHERE `id` = '66';
            DELETE FROM {{fair}} WHERE `id` = '1658';
            DELETE FROM {{fair}} WHERE `id` = '12842';
            DELETE FROM {{fair}} WHERE `id` = '1988';
            DELETE FROM {{fair}} WHERE `id` = '2118';
            DELETE FROM {{fair}} WHERE `id` = '260';
            DELETE FROM {{fair}} WHERE `id` = '417';
            DELETE FROM {{fair}} WHERE `id` = '442';
            DELETE FROM {{fair}} WHERE `id` = '459';
            DELETE FROM {{fair}} WHERE `id` = '234';
            DELETE FROM {{fair}} WHERE `id` = '544';
            DELETE FROM {{fair}} WHERE `id` = '1346';
            DELETE FROM {{fair}} WHERE `id` = '154';
            DELETE FROM {{fair}} WHERE `id` = '3593';
            DELETE FROM {{fair}} WHERE `id` = '13379';
            DELETE FROM {{fair}} WHERE `id` = '3591';
            DELETE FROM {{fair}} WHERE `id` = '12640';
            DELETE FROM {{fair}} WHERE `id` = '626';
            DELETE FROM {{fair}} WHERE `id` = '12811';
            DELETE FROM {{fair}} WHERE `id` = '1500';
            DELETE FROM {{fair}} WHERE `id` = '10994';
            DELETE FROM {{fair}} WHERE `id` = '364';
            DELETE FROM {{fair}} WHERE `id` = '4000';
            DELETE FROM {{fair}} WHERE `id` = '10090';
            DELETE FROM {{fair}} WHERE `id` = '2181';
            DELETE FROM {{fair}} WHERE `id` = '2183';
            DELETE FROM {{fair}} WHERE `id` = '3998';
            DELETE FROM {{fair}} WHERE `id` = '10091';
            
            DELETE FROM {{fairinfo}} WHERE `id` = '10957';
            DELETE FROM {{fairinfo}} WHERE `id` = '5003';
            DELETE FROM {{fairinfo}} WHERE `id` = '5794';
            DELETE FROM {{fairinfo}} WHERE `id` = '4859';
            DELETE FROM {{fairinfo}} WHERE `id` = '5750';
            DELETE FROM {{fairinfo}} WHERE `id` = '10254';
            DELETE FROM {{fairinfo}} WHERE `id` = '5893';
            DELETE FROM {{fairinfo}} WHERE `id` = '5949';
            DELETE FROM {{fairinfo}} WHERE `id` = '4976';
            DELETE FROM {{fairinfo}} WHERE `id` = '5065';
            DELETE FROM {{fairinfo}} WHERE `id` = '5082';
            DELETE FROM {{fairinfo}} WHERE `id` = '5094';
            DELETE FROM {{fairinfo}} WHERE `id` = '4958';
            DELETE FROM {{fairinfo}} WHERE `id` = '5167';
            DELETE FROM {{fairinfo}} WHERE `id` = '5610';
            DELETE FROM {{fairinfo}} WHERE `id` = '4907';
            DELETE FROM {{fairinfo}} WHERE `id` = '6793';
            DELETE FROM {{fairinfo}} WHERE `id` = '10700';
            DELETE FROM {{fairinfo}} WHERE `id` = '6791';
            DELETE FROM {{fairinfo}} WHERE `id` = '10117';
            DELETE FROM {{fairinfo}} WHERE `id` = '5206';
            DELETE FROM {{fairinfo}} WHERE `id` = '10235';
            DELETE FROM {{fairinfo}} WHERE `id` = '5681';
            DELETE FROM {{fairinfo}} WHERE `id` = '8673';
            DELETE FROM {{fairinfo}} WHERE `id` = '5033';
            DELETE FROM {{fairinfo}} WHERE `id` = '7151';
            DELETE FROM {{fairinfo}} WHERE `id` = '7825';
            DELETE FROM {{fairinfo}} WHERE `id` = '5978';
            DELETE FROM {{fairinfo}} WHERE `id` = '5979';
            DELETE FROM {{fairinfo}} WHERE `id` = '7149';
            DELETE FROM {{fairinfo}} WHERE `id` = '7826';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}