<?php

class m160804_091032_add_missed_vc_for_sng_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('БАКУ ЭКСПО ЦЕНТР', '241', '124');
			INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('АККО', '263', '144');
			
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1143', 'ru', 'БАКУ ЭКСПО ЦЕНТР', 'БАКУ ЭКСПО ЦЕНТР');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1143', 'en', 'БАКУ ЭКСПО ЦЕНТР', 'БАКУ ЭКСПО ЦЕНТР');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1143', 'de', 'БАКУ ЭКСПО ЦЕНТР', 'БАКУ ЭКСПО ЦЕНТР');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1144', 'ru', 'АККО', 'АККО');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1144', 'en', 'АККО', 'АККО');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1144', 'de', 'АККО', 'АККО');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}