<?php

class m160620_110930_update_6_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1497';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1500';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1503';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1506';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1512';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1509';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1513';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1514';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1561';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1452';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1521';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1449';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1518';		
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1524';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1455';
			UPDATE {{trattributeclass}} SET `placeholder`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1458';
			UPDATE {{trattributeclass}} SET `placeholder`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1527';
			UPDATE {{trattributeclass}} SET `placeholder`=NULL, `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1458';
			UPDATE {{trattributeclass}} SET `placeholder`=NULL, `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1527';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1530';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='2050';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1533';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1464';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1467';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1536';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1540';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1471';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1474';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1543';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1546';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1477';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1480';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1549';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1552';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1483';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1554';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016' WHERE `id`='1555';

";
	}
}
