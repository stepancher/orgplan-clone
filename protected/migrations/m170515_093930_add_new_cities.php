<?php

class m170515_093930_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('188', 'bayreuth');

            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1594', 'ru', 'Байройт');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1594', 'en', 'Bayreuth');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1594', 'de', 'Bayreuth');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('157', 'kerken');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1595', 'ru', 'Керкен');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1595', 'en', 'Kerken');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1595', 'de', 'Kerken');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}