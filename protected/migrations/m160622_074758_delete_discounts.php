<?php

class m160622_074758_delete_discounts extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			DELETE FROM {{attributeclassproperty}} WHERE `id`='1985';
			DELETE FROM {{attributeclassproperty}} WHERE `id`='1986';
			DELETE FROM {{attributeclassproperty}} WHERE `id`='1987';
";
	}
}