<?php

class m161005_094518_tbl_fair_add_column_storyId extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{fair}} 
              ADD COLUMN `storyId` INT(11) NULL AFTER `shard`;



            update tbl_fair uf
            LEFT JOIN (
            select 
                f14_id,
                f15_id,
                f16_id,
                f17_id,
                f18_id
            
                from (select 
                f14.id f14_id,
                f14.name f14_name,
                f14.beginDate f14_beginDate,
                f15.id f15_id,
                f15.name f15_name,
                f15.beginDate f15_beginDate,
                f16.id f16_id,
                f16.name f16_name,
                f16.beginDate f16_beginDate,
                f17.id f17_id,
                f17.name f17_name,
                f17.beginDate f17_beginDate,
                f18.id f18_id,
                f18.name f18_name,
                f18.beginDate f18_beginDate,
                f19.id f19_id,
                f19.name f19_name,
                f19.beginDate f19_beginDate
                from 
            
                (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                ) fAll
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2014
                ) f14 on f14.name = fAll.name and f14.ecId = fAll.ecId and month(f14.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2015
                ) f15 on f15.name = fAll.name and f15.ecId = fAll.ecId and month(f15.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2016
                ) f16 on f16.name = fAll.name and f16.ecId = fAll.ecId and month(f16.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2017
                ) f17 on f17.name = fAll.name and f17.ecId = fAll.ecId and month(f17.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2018
                ) f18 on f18.name = fAll.name and f18.ecId = fAll.ecId and month(f18.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2019
                ) f19 on f19.name = fAll.name and f19.ecId = fAll.ecId and month(f19.beginDate) = month(fAll.beginDate)) fbig1
                    WHERE fbig1.f14_id is not null
            ) fj ON (fj.f14_id = uf.id)
            set uf.storyId =  CASE
                WHEN fj.f14_id IS NOT NULL THEN fj.f14_id
                WHEN fj.f15_id IS NOT NULL THEN fj.f15_id
                WHEN fj.f16_id IS NOT NULL THEN fj.f16_id
                WHEN fj.f17_id IS NOT NULL THEN fj.f17_id
                WHEN fj.f18_id IS NOT NULL THEN fj.f18_id
                ELSE NULL
                END
            
            where fj.f14_id IS NOT NULL;
            
            
            
            
            
            update tbl_fair uf
            LEFT JOIN (
            select 
                f14_id,
                f15_id,
                f16_id,
                f17_id,
                f18_id
            
                from (select 
                f14.id f14_id,
                f14.name f14_name,
                f14.beginDate f14_beginDate,
                f15.id f15_id,
                f15.name f15_name,
                f15.beginDate f15_beginDate,
                f16.id f16_id,
                f16.name f16_name,
                f16.beginDate f16_beginDate,
                f17.id f17_id,
                f17.name f17_name,
                f17.beginDate f17_beginDate,
                f18.id f18_id,
                f18.name f18_name,
                f18.beginDate f18_beginDate,
                f19.id f19_id,
                f19.name f19_name,
                f19.beginDate f19_beginDate
                from 
            
                (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                ) fAll
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2014
                ) f14 on f14.name = fAll.name and f14.ecId = fAll.ecId and month(f14.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2015
                ) f15 on f15.name = fAll.name and f15.ecId = fAll.ecId and month(f15.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2016
                ) f16 on f16.name = fAll.name and f16.ecId = fAll.ecId and month(f16.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2017
                ) f17 on f17.name = fAll.name and f17.ecId = fAll.ecId and month(f17.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2018
                ) f18 on f18.name = fAll.name and f18.ecId = fAll.ecId and month(f18.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2019
                ) f19 on f19.name = fAll.name and f19.ecId = fAll.ecId and month(f19.beginDate) = month(fAll.beginDate)) fbig1
                    WHERE fbig1.f15_id is not null
            ) fj ON (fj.f15_id = uf.id)
            
            set uf.storyId =  CASE
                WHEN fj.f14_id IS NOT NULL THEN fj.f14_id
                WHEN fj.f15_id IS NOT NULL THEN fj.f15_id
                WHEN fj.f16_id IS NOT NULL THEN fj.f16_id
                WHEN fj.f17_id IS NOT NULL THEN fj.f17_id
                WHEN fj.f18_id IS NOT NULL THEN fj.f18_id
                ELSE NULL
                END
            
            where fj.f15_id IS NOT NULL;
            
            
            update tbl_fair uf
            LEFT JOIN (
            select 
                f14_id,
                f15_id,
                f16_id,
                f17_id,
                f18_id
            
                from (select 
                f14.id f14_id,
                f14.name f14_name,
                f14.beginDate f14_beginDate,
                f15.id f15_id,
                f15.name f15_name,
                f15.beginDate f15_beginDate,
                f16.id f16_id,
                f16.name f16_name,
                f16.beginDate f16_beginDate,
                f17.id f17_id,
                f17.name f17_name,
                f17.beginDate f17_beginDate,
                f18.id f18_id,
                f18.name f18_name,
                f18.beginDate f18_beginDate,
                f19.id f19_id,
                f19.name f19_name,
                f19.beginDate f19_beginDate
                from 
            
                (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                ) fAll
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2014
                ) f14 on f14.name = fAll.name and f14.ecId = fAll.ecId and month(f14.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2015
                ) f15 on f15.name = fAll.name and f15.ecId = fAll.ecId and month(f15.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2016
                ) f16 on f16.name = fAll.name and f16.ecId = fAll.ecId and month(f16.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2017
                ) f17 on f17.name = fAll.name and f17.ecId = fAll.ecId and month(f17.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2018
                ) f18 on f18.name = fAll.name and f18.ecId = fAll.ecId and month(f18.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2019
                ) f19 on f19.name = fAll.name and f19.ecId = fAll.ecId and month(f19.beginDate) = month(fAll.beginDate)) fbig1
                    WHERE fbig1.f16_id is not null
            ) fj ON (fj.f16_id = uf.id)
            
            set uf.storyId =  CASE
                WHEN fj.f14_id IS NOT NULL THEN fj.f14_id
                WHEN fj.f15_id IS NOT NULL THEN fj.f15_id
                WHEN fj.f16_id IS NOT NULL THEN fj.f16_id
                WHEN fj.f17_id IS NOT NULL THEN fj.f17_id
                WHEN fj.f18_id IS NOT NULL THEN fj.f18_id
                ELSE NULL
                END
            
            where fj.f16_id IS NOT NULL;
            
            
            
            update tbl_fair uf
            LEFT JOIN (
            select 
                 f14_id,
                 f15_id,
                 f16_id,
                 f17_id,
                 f18_id
            
                from (select 
                f14.id f14_id,
                f14.name f14_name,
                f14.beginDate f14_beginDate,
                f15.id f15_id,
                f15.name f15_name,
                f15.beginDate f15_beginDate,
                f16.id f16_id,
                f16.name f16_name,
                f16.beginDate f16_beginDate,
                f17.id f17_id,
                f17.name f17_name,
                f17.beginDate f17_beginDate,
                f18.id f18_id,
                f18.name f18_name,
                f18.beginDate f18_beginDate,
                f19.id f19_id,
                f19.name f19_name,
                f19.beginDate f19_beginDate
                from 
            
                (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                ) fAll
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2014
                ) f14 on f14.name = fAll.name and f14.ecId = fAll.ecId and month(f14.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2015
                ) f15 on f15.name = fAll.name and f15.ecId = fAll.ecId and month(f15.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2016
                ) f16 on f16.name = fAll.name and f16.ecId = fAll.ecId and month(f16.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2017
                ) f17 on f17.name = fAll.name and f17.ecId = fAll.ecId and month(f17.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2018
                ) f18 on f18.name = fAll.name and f18.ecId = fAll.ecId and month(f18.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2019
                ) f19 on f19.name = fAll.name and f19.ecId = fAll.ecId and month(f19.beginDate) = month(fAll.beginDate)) fbig1
                    WHERE fbig1.f17_id is not null
            ) fj ON (fj.f17_id = uf.id)
            
            set uf.storyId =  CASE
                WHEN fj.f14_id IS NOT NULL THEN fj.f14_id
                WHEN fj.f15_id IS NOT NULL THEN fj.f15_id
                WHEN fj.f16_id IS NOT NULL THEN fj.f16_id
                WHEN fj.f17_id IS NOT NULL THEN fj.f17_id
                WHEN fj.f18_id IS NOT NULL THEN fj.f18_id
                ELSE NULL
                END
            
            where fj.f17_id IS NOT NULL;
            
            
            
            update tbl_fair uf
            LEFT JOIN (
            select 
                f14_id,
                 f15_id,
                 f16_id,
                 f17_id,
                 f18_id
            
                from (select 
                f14.id f14_id,
                f14.name f14_name,
                f14.beginDate f14_beginDate,
                f15.id f15_id,
                f15.name f15_name,
                f15.beginDate f15_beginDate,
                f16.id f16_id,
                f16.name f16_name,
                f16.beginDate f16_beginDate,
                f17.id f17_id,
                f17.name f17_name,
                f17.beginDate f17_beginDate,
                f18.id f18_id,
                f18.name f18_name,
                f18.beginDate f18_beginDate,
                f19.id f19_id,
                f19.name f19_name,
                f19.beginDate f19_beginDate
                from 
            
                (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                ) fAll
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2014
                ) f14 on f14.name = fAll.name and f14.ecId = fAll.ecId and month(f14.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2015
                ) f15 on f15.name = fAll.name and f15.ecId = fAll.ecId and month(f15.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2016
                ) f16 on f16.name = fAll.name and f16.ecId = fAll.ecId and month(f16.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2017
                ) f17 on f17.name = fAll.name and f17.ecId = fAll.ecId and month(f17.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2018
                ) f18 on f18.name = fAll.name and f18.ecId = fAll.ecId and month(f18.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2019
                ) f19 on f19.name = fAll.name and f19.ecId = fAll.ecId and month(f19.beginDate) = month(fAll.beginDate)) fbig1
                    WHERE fbig1.f18_id is not null
            ) fj ON (fj.f18_id = uf.id)
            
            set uf.storyId =  CASE
                WHEN fj.f14_id IS NOT NULL THEN fj.f14_id
                WHEN fj.f15_id IS NOT NULL THEN fj.f15_id
                WHEN fj.f16_id IS NOT NULL THEN fj.f16_id
                WHEN fj.f17_id IS NOT NULL THEN fj.f17_id
                WHEN fj.f18_id IS NOT NULL THEN fj.f18_id
                ELSE NULL
                END
            
            where fj.f18_id IS NOT NULL;
            
            update tbl_fair uf
            LEFT JOIN (
            select 
                f14_id,
                 f15_id,
                 f16_id,
                 f17_id,
                 f18_id,
                 f19_id
            
                from (select 
                f14.id f14_id,
                f14.name f14_name,
                f14.beginDate f14_beginDate,
                f15.id f15_id,
                f15.name f15_name,
                f15.beginDate f15_beginDate,
                f16.id f16_id,
                f16.name f16_name,
                f16.beginDate f16_beginDate,
                f17.id f17_id,
                f17.name f17_name,
                f17.beginDate f17_beginDate,
                f18.id f18_id,
                f18.name f18_name,
                f18.beginDate f18_beginDate,
                f19.id f19_id,
                f19.name f19_name,
                f19.beginDate f19_beginDate
                from 
            
                (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                ) fAll
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2014
                ) f14 on f14.name = fAll.name and f14.ecId = fAll.ecId and month(f14.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2015
                ) f15 on f15.name = fAll.name and f15.ecId = fAll.ecId and month(f15.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2016
                ) f16 on f16.name = fAll.name and f16.ecId = fAll.ecId and month(f16.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2017
                ) f17 on f17.name = fAll.name and f17.ecId = fAll.ecId and month(f17.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2018
                ) f18 on f18.name = fAll.name and f18.ecId = fAll.ecId and month(f18.beginDate) = month(fAll.beginDate)
            
                left join (
                select f.id, trf.name, f.beginDate, f.exhibitionComplexId ecId from tbl_fair f
                left join tbl_trfair trf on trf.trParentId = f.id and trf.langId = 'ru'
                where year(f.beginDate) = 2019
                ) f19 on f19.name = fAll.name and f19.ecId = fAll.ecId and month(f19.beginDate) = month(fAll.beginDate)) fbig1
                    WHERE fbig1.f19_id is not null
            ) fj ON (fj.f19_id = uf.id)
            
            set uf.storyId =  CASE
                WHEN fj.f14_id IS NOT NULL THEN fj.f14_id
                WHEN fj.f15_id IS NOT NULL THEN fj.f15_id
                WHEN fj.f16_id IS NOT NULL THEN fj.f16_id
                WHEN fj.f17_id IS NOT NULL THEN fj.f17_id
                WHEN fj.f18_id IS NOT NULL THEN fj.f18_id
                WHEN fj.f19_id IS NOT NULL THEN fj.f19_id
                ELSE NULL
                END
            
            where fj.f19_id IS NOT NULL;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}