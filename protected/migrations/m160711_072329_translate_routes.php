<?php

class m160711_072329_translate_routes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			DELETE FROM {{route}} where url LIKE '%/en/venues' AND header = 'Каталог площадок';
			UPDATE {{route}} SET `title`='Events Catalogue', `description`='The catalogue contains announcements of exhibitions and reports of past events in Russia, CIS and worldwide. Using the filter you can find an exhibition by country, city, and industry.', `header`='Exhibitions of Russian Federation' WHERE url LIKE '%/en/fairs';
			UPDATE {{route}} SET `title`='Venues of Russia, CIS, worldwide', `description`='The catalogue contains venues of Russia, CIS and worldwide. Using the filter you can find the venue by region and city.', `header`='Venues Catalogue' WHERE url LIKE '%/en/venues';
			UPDATE {{route}} SET `title`='Industry sector catalogue I Protoplan', `description`='The catalogue contains 9 Russian industry sectors. Each sector contains an overview with core indicators of development.', `header`='Industry sector catalogue' WHERE url LIKE '%/en/industries';
			UPDATE {{route}} SET `title`='Russian Regions', `description`='The catalogue presents the regions of Russia, each of which contains information about economic development indicators, KPI of industry sectors and investment attractiveness rating.', `header`='Region Information Catalogue' WHERE url LIKE '%/en/regions';
			UPDATE {{route}} SET `title`='Supplies and Service prices I Protoplan', `description`='Price catalogue with a breakdown by Russian cities.', `header`='Prices Catalogue' WHERE url LIKE '%/en/prices';
			UPDATE {{route}} SET `title`='Exhibition stands projects', `description`='The catalogue contains complete layout of exhibition stands. Using the filter, you can find the layout of the stand with breakdown by area size and layout planning type.', `header`='Stands Catalogue' WHERE url LIKE '%/en/stands';
			UPDATE {{route}} SET `title`='Guide I Protoplan', `description`='With PROTOPLAN guide you can find any exhibition in Russia. Records since January 2015.', `header`='Guide I Protoplan' WHERE url LIKE '%/en/help/quickGuide';
			UPDATE {{route}} SET `title`='Exhibitions by towns of Russian Federation', `description`='Exhibitions by cities of the Russian Federation.', `header`='Exhibitions by towns of Russian Federation' WHERE url LIKE '%/en/fairs/city';
			UPDATE {{route}} SET `title`='Regions Catalogue in alphabetic order', `description`='Catalogue of regions in alphabetic order.', `header`='Regions Catalogue in alphabetic order' WHERE url LIKE '%/en/regions/alphabet';
			UPDATE {{route}} SET `title`='Towns of Russian Federation Catalogue', `description`='Catalogue by cities of the Russian Federation.', `header`='Towns of Russian Federation Catalogue' WHERE url LIKE '%/en/venues/city';
			UPDATE {{route}} SET `title`='Industry sector Catalogue in alphabetic order', `description`='Catalogгу of industry sectors in alphabetic order.', `header`='Industry sector Catalogue in alphabetic order' WHERE url LIKE '%/en/industries/alphabet';
			UPDATE {{route}} SET `title`='Exhibitions Catalogue by date', `description`='Catalogue of exhibitions by date.', `header`='Exhibitions Catalogue by date' WHERE url LIKE '%/en/fairs/date';
			UPDATE {{route}} SET `title`='Russian Federation Regions Catalogue', `description`='Catalogue by regions of the Russian Federation.', `header`='Russian Federation Regions Catalogue' WHERE url LIKE '%/en/venues/region';
			UPDATE {{route}} SET `title`='Question-Answer I Protoplan', `description`='Can I trust PROTOPLAN statistics? All statistics have been taken from official sources, the page contains the reference to the source, you can check this information.', `header`='Question-Answer I Protoplan' WHERE url LIKE '%/en/help/questionAnswer';
			UPDATE {{route}} SET `title`='Privacy Policy I Protoplan', `description`='The present document «Privacy Policy» (hereinafter referred to as «Policy») represents regulations for use of User’s personal data.', `header`='Privacy Policy I Protoplan' WHERE url LIKE '%/en/help/confidentiality';
			UPDATE {{route}} SET `title`='Ehxibitions by industry sectors', `description`='Exhibitions by industry sectors.', `header`='Ehxibitions by industry sector' WHERE url LIKE '%/en/fairs/industry';
			UPDATE {{route}} SET `title`='Blog I Protoplan', `description`='Information about participation in exhibitions, inviting customers, job scheduling of personnel, ideas for exhibition stands, other topics from the protoplan.pro blog experts.', `header`='Blog I Protoplan' WHERE url LIKE '%/en/blog';
			UPDATE {{route}} SET `title`='Exhibitions by Russian Federation Regions', `description`='Exhibitions by regions of the Russian Federation.', `header`='Exhibitions by Russian Federation Regions' WHERE url LIKE '%/en/fairs/region';
			UPDATE {{route}} SET `title`='Exhibitions and Conferences in Russia, CIS, worldwide - PROTOPLAN.PRO', `description`='Service that helps to organize and effectively hold an exhibition or conference in Russia and worldwide. Helpful tips for organization of exhibition arrangements.', `header`='Exhibitions and Conferences in Russia, CIS, worldwide - PROTOPLAN.PRO' WHERE url LIKE '%/en';
			UPDATE {{route}} SET `title`='Venues Catalogue by category', `description`='The page contains venues catalog in the Russian Federation by category.', `header`='Venues Catalogue by category' WHERE url LIKE '%/en/venues/category';
			UPDATE {{route}} SET `title`='User Agreement I Protoplan', `description`='The present document «User Agreement» represents the offer of ООО «Kubex Rus» (hereinafter referred to as «Copyright Holder») for concluding of the contract under below terms.', `header`='User Agreement I Protoplan' WHERE url LIKE '%/en/help/termsOfUse';
			UPDATE {{route}} SET `title`='Protoplan I Authorisation', `header`='Authorisation' WHERE url LIKE '%/en/login';
			UPDATE {{route}} SET `title`='Protoplan I Registration', `header`='Registration' WHERE url LIKE '%/en/registration';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}