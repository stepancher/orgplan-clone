<?php

class m170227_062217_add_exdb_fairhasaudit_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`;
            
            CREATE PROCEDURE {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fair_id INT DEFAULT 0;
                DECLARE audit_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.id AS fairId, eau.id AS auditId
                                        FROM {$expodataRaw}.expo_data ed 
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = ed.id
                                        LEFT JOIN {$expodata}.{{exdbaudit}} eau ON eau.exdbId = ed.audit_id_en
                                        LEFT JOIN {$expodata}.{{exdbfairhasaudit}} efhau ON efhau.fairId = ef.id AND efhau.auditId = eau.id
                                        WHERE ed.audit_id_en != 0 AND efhau.id IS NULL
                                        ORDER BY ef.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    
                    FETCH copy INTO fair_id, audit_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairhasaudit}} (`fairId`,`auditId`) VALUES (fair_id, audit_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}