<?php

class m160321_140609_update_tbl_attributeclass_set_class_headerH1 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='13';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='14';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='22';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='42';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='66';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='71';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='75';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='88';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='97';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='100';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='105';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='109';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='113';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='131';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='138';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='154';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='157';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='207';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='218';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='224';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='225';
			UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='234';

			UPDATE {{attributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru', `class`='hint_danger' WHERE `id`='234';
			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='234';

			DELETE FROM {{attributeclass}} WHERE `id`='177';
			
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='10';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='15';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='19';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='21';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='23';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='27';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='29';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='30';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='31';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='32';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='34';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='36';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='37';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='38';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='39';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='41';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='43';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='44';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='45';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='47';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='51';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='54';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='58';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='60';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='61';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='62';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='63';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='65';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='67';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='68';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='69';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='70';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='94';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='98';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='101';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='102';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='106';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='155';
			UPDATE {{attributeclass}} SET `class`= NULL  WHERE `id`='164';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='172';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='173';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='174';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='175';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='176';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='229';

			UPDATE {{attributeclass}} SET `class`='number' WHERE `id`='11';
			UPDATE {{attributeclass}} SET `class`='number' WHERE `id`='12';
			UPDATE {{attributeclass}} SET `class`=NULL WHERE `id`='128';

			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='179';
			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='209';
			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='217';
			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='223';
			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='226';
			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='236';

			UPDATE {{attributeclass}} SET `class`='enumDropDown' WHERE `id`='72';
			UPDATE {{attributeclass}} SET `class`='enumDropDown' WHERE `id`='117';
			UPDATE {{attributeclass}} SET `class`='enumDropDown' WHERE `id`='139';

			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='74';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='73';
			UPDATE {{attributeclass}} SET `class`=NULL WHERE `id`='229';

			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='110';

			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='190';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='191';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='192';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='193';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='194';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='195';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='196';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='197';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='198';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='200';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='201';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='202';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='203';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='204';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='205';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='206';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='229';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='230';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='231';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='232';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='233';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='189';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='188';
			UPDATE {{attributeclass}} SET `isArray`='0' WHERE `id`='187';

			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='111';

			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='112';

			UPDATE {{attributeclass}} SET `class`='radioContainer' WHERE `id`='128';

			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='114';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='115';

			UPDATE {{attributeclass}} SET `class`='double' WHERE `id`='119';
			UPDATE {{attributeclass}} SET `class`='double' WHERE `id`='120';

			UPDATE {{attributeclass}} SET `class`='radioContainer' WHERE `id`='129';
			UPDATE {{attributeclass}} SET `class`='radioContainer' WHERE `id`='130';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='121';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='122';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='123';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='124';

			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='185';

			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='80';

			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='237';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='81';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='82';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='83';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='84';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='85';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='86';
			UPDATE {{attributeclass}} SET `class`='checkboxSwitcher' WHERE `id`='87';

			UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='213';
			UPDATE {{attributeclass}} SET `class`='radioContainer' WHERE `id`='165';
			UPDATE {{attributeclass}} SET `class`='radioContainer' WHERE `id`='170';
			UPDATE {{attributeclass}} SET `class`='radioContainer' WHERE `id`='171';

			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='159';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='160';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='162';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='163';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='167';
			UPDATE {{attributeclass}} SET `class`='radio' WHERE `id`='168';

			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='164';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='169';
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `class`, `collapse`) VALUES ('zayavka1_summary', 'string', '0', '0', '198', 'summary', '0');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('1', '238', '0', '0', '9');

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `label`, `parent`, `super`, `class`) VALUES ('zayavka1_nds', 'string', 'Все цены включают НДС 18%. Оплата производится в российских рублях по курсу ЦБ РФ на дату платежа.', '238', '198', 'hint');
			UPDATE {{attributeclass}} SET `label`='' WHERE `id`='238';

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `parent`, `super`, `class`) VALUES ('zayavka1_checkbox_agreement', 'bool', '238', '198', 'checkboxAgreement');

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `label`, `parent`, `super`, `class`) VALUES ('zayavka4_summary', 'string', NULL, '0', '241', 'summary');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `label`, `parent`, `super`, `class`) VALUES ('zayavka4_nds', 'string', 'Все цены включают НДС 18%. Оплата производится в российских рублях по курсу ЦБ РФ на дату платежа.', '241', '241', 'hint');
			UPDATE {{attributeclass}} SET `super`='238' WHERE `id`='238';
			UPDATE {{attributeclass}} SET `super`='238' WHERE `id`='239';
			UPDATE {{attributeclass}} SET `super`='238' WHERE `id`='240';
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `parent`, `super`, `class`) VALUES ('zayavka4_checkbox_agreement', 'bool', '241', '241', 'checkboxAgreement');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('2', '241', '5');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `parent`, `super`, `class`) VALUES ('zayavka_konkursnaya_checkbox_agreement', 'bool', '0', '244', 'checkboxAgreement');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('3', '244', '12');
		";
	}
}