<?php

class m160203_105310_insert_into_trcity_ru_values_from_city extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			insert into {{trcity}} (trParentId, langId, name)
				select c.id, "ru", c.name from {{city}} c
				where c.id not in (
				 select trc.trParentId from {{trcity}} trc where trc.langId = "ru" and trc.trParentId is not null
				) AND 
				c.name is not null;
		';
	}
}