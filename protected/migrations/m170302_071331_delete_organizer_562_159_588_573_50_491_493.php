<?php

class m170302_071331_delete_organizer_562_159_588_573_50_491_493 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{organizercompany}} WHERE `id` = '562';
            DELETE FROM {{organizercompany}} WHERE `id` = '159';
            DELETE FROM {{organizercompany}} WHERE `id` = '588';
            DELETE FROM {{organizercompany}} WHERE `id` = '573';
            DELETE FROM {{organizercompany}} WHERE `id` = '50';
            DELETE FROM {{organizercompany}} WHERE `id` = '491';
            DELETE FROM {{organizercompany}} WHERE `id` = '493';

            DELETE FROM {{organizer}} WHERE `id` = '512';
            DELETE FROM {{organizer}} WHERE `id` = '1293';
            DELETE FROM {{organizer}} WHERE `id` = '1415';
            DELETE FROM {{organizer}} WHERE `id` = '2859';
            DELETE FROM {{organizer}} WHERE `id` = '2865';
            DELETE FROM {{organizer}} WHERE `id` = '2866';
            DELETE FROM {{organizer}} WHERE `id` = '2893';
            DELETE FROM {{organizer}} WHERE `id` = '3723';

            DELETE FROM {{organizercontactlist}} WHERE `id`= '21817';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21818';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21819';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21820';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21821';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21822';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21823';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21824';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21825';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21826';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21827';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21828';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21829';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21830';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21831';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21832';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21833';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21834';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21835';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '21836';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29735';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29736';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29737';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29738';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29739';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29740';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '29741';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31124';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31125';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31126';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31127';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31128';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31129';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31130';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31131';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31132';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '31133';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42032';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42033';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42034';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42035';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42036';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42037';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42038';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42039';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42040';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42041';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42042';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42043';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42044';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42045';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42046';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42335';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42336';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42337';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42338';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42339';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42340';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42341';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42342';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42343';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '42344';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49085';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49086';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49087';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49088';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49089';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49090';
            DELETE FROM {{organizercontactlist}} WHERE `id`= '49091';

            DELETE FROM {{fair}} WHERE `id` = '1010';
            DELETE FROM {{fair}} WHERE `id` = '1037';
            DELETE FROM {{fair}} WHERE `id` = '1038';
            DELETE FROM {{fair}} WHERE `id` = '1044';
            DELETE FROM {{fair}} WHERE `id` = '4227';

            DELETE FROM {{fairhasindustry}} WHERE `id` = '15207';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '15223';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '16390';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '57168';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}