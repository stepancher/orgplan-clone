<?php

class m161128_073448_rename_protoplan_fair extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trfair}} SET `shortUrl`='protoplanexpo-2017-krasnodar-1st-december' WHERE `id`='13204';
            UPDATE {{trfair}} SET `shortUrl`='protoplanexpo-2017-krasnodar-1st-december' WHERE `id`='17620';
            
            DELETE FROM {{contact}} WHERE id = 1965;
            DELETE FROM {{user}} WHERE id = 2630;
            
            UPDATE {{fair}} SET lang = 'de' WHERE id IN (10943, 13863, 13864, 13865);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}