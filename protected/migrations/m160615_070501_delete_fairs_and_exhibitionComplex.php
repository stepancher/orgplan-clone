<?php

class m160615_070501_delete_fairs_and_exhibitionComplex extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE id IN (10060,10066,10070,10077,10078,10080,10081,10082);
			DELETE FROM {{trfair}} WHERE trParentId IN (10060,10066,10070,10077,10078,10080,10081,10082);

			DELETE FROM {{exhibitioncomplex}} WHERE id IN (88,1028,1029);
			DELETE FROM {{trexhibitioncomplex}} WHERE trParentId IN (88,1028,1029);
		";
	}
}