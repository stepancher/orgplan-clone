<?php

class m160601_061553_add_main_en_pages_to_route extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{route}} (`id`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, '/en/logout', 'site/logout', '[]', '', 'en', NULL);
			INSERT INTO {{route}} (`id`, `title`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Protoplan | Регистрация', '/en/registration', 'site/reg', '[]', 'Регистрация', 'en', NULL);
			INSERT INTO {{route}} (`id`, `title`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Protoplan | Авторизация', '/en/login', 'site/auth', '[]', 'Авторизация', 'en', NULL);
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Protoplan', 'Protoplan', '/en', 'site/index', '[]', 'Protoplan', 'en', NULL);
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог событий', 'В каталоге представлены анонсы выставок и отчеты прошедших мероприятий в России, СНГ, мире. При помощи фильтра можно найти выставку по стране, городу, отрасли.', '/en/fairs', 'search/index', '{\"sector\":\"fair\"}', 'Выставки Российской Федерации', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Площадки России, СНГ, мира', 'В каталоге представлены площадки России, СНГ, мира. Воспользовавшись фильтром, можно найти площадку по региону и городу', '/en/venues', 'search/index', '{\"sector\":\"exhibitionComplex\"}', 'Каталог площадок', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог отраслей | Protoplan', 'В каталоге представлены 9 отраслей промышленности России. В каждой отрасли содержится краткий обзор с основными показателями развития.', '/en/industries', 'search/index', '{\"sector\":\"industry\"}', 'Каталог отраслей', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Регионы России', 'В каталоге представлены регионы России, в каждом опубликованы экономические показатели развития, KPI отраслей и рейтинг инвестиционной привлекательности', '/en/regions', 'search/index', '{\"sector\":\"region\"}', 'Каталог информации о регионах', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог цен на товары и услуги | Protoplan', 'Каталог цен по городам России', '/en/prices', 'search/index', '{\"sector\":\"price\"}', 'Каталог Цен', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Проекты выставочных стендов', 'В каталоге представлены готовые макеты выставочных стендов. Воспользовавшись фильтром, можно найти макет стенда по площади и типу планировки.', '/en/stands', 'search/index', '{\"sector\":\"stand\"}', 'Каталог стендов', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Краткое руководство | Protoplan', 'С помощью поиска ORGPLAN вы можете найти любую выставку на территории России. Архив с января 2015 года.', '/en/help/quickGuide', 'help/quickGuide', '[]', 'Краткое руководство | Orgplan', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Выставки по городам Российской Федерации', 'Выставки по городам Российской Федерации', '/en/fairs/city', 'search/index', '{\"by\":\"byCity\",\"sector\":\"fair\"}', 'Выставки по городам Российской Федерации', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог регионов по алфавиту', 'Каталог регионов по алфавиту', '/en/regions/alphabet', 'search/index', '{\"by\":\"byAlphabet\",\"sector\":\"region\"}', 'Каталог регионов по алфавиту', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог по городам Российской Федерации', 'Каталог по городам Российской Федерации', '/en/venues/city', 'search/index', '{\"by\":\"byCity\",\"sector\":\"exhibitionComplex\"}', 'Каталог по городам Российской Федерации', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог отраслей по алфавиту', 'Каталог отраслей по алфавиту', '/en/industries/alphabet', 'search/index', '{\"by\":\"byAlphabet\",\"sector\":\"industry\"}', 'Каталог отраслей по алфавиту', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог Выставок по дате', 'Каталог Выставок по дате', '/en/fairs/date', 'search/index', '{\"by\":\"byDate\",\"sector\":\"fair\"}', 'Каталог Выставок по дате', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог по регионам Российской Федерации', 'Каталог по регионам Российской Федерации', '/en/venues/region', 'search/index', '{\"by\":\"byRegion\",\"sector\":\"exhibitionComplex\"}', 'Каталог по регионам Российской Федерации', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Вопрос-ответ | Protoplan', 'Могу ли я доверять статистике ORGPLAN? Все статистические данные взяты из официальных источников, ссылка на источник публикуется на странице, вы можете проверить.', '/en/help/questionAnswer', 'help/questionAnswer', '[]', 'Вопрос-ответ | Orgplan', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог отраслей | Protoplan', 'В каталоге представлены 9 отраслей промышленности России. В каждой отрасли содержится краткий обзор с основными показателями развития.', '/en/industries', 'search/index', '{\"sector\":\"industry\"}', 'Каталог отраслей | Orgplan', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Политика конфиденциальности | Protoplan', 'Настоящий документ «Политика конфиденциальности» (далее — по тексту — «Политика») представляет собой правила использования персональной информации Пользователя.', '/en/help/confidentiality', 'help/confidentiality', '[]', 'Политика конфиденциальности | Orgplan', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Выставки по отраслям', 'Выставки по отраслям', '/en/fairs/industry', 'search/index', '{\"by\":\"byIndustry\",\"sector\":\"fair\"}', 'Выставки по отраслям', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Блог | Protoplan', 'Как участвовать в выставках, как пригласить клиентов, планирование работы персонала,  идеи для выставочных стендов, другие темы от экспертов блога orgplan.ru', '/en/blog', 'blog/index', '[]', 'Блог | Orgplan', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Выставки по регионам Российской Федерации', 'Выставки по регионам Российской Федерации', '/en/fairs/region', 'search/index', '{\"by\":\"byRegion\",\"sector\":\"fair\"}', 'Выставки по регионам Российской Федерации', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог площадок по категориям', 'На странице представлен каталог площадок Российской Федерации по категориям.', '/en/venues/category', 'search/index', '{\"by\":\"byCategory\",\"sector\":\"exhibitionComplex\"}', 'Каталог площадок по категориям', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Регионы России', 'В каталоге представлены регионы России, в каждом опубликованы экономические показатели развития, KPI отраслей и рейтинг инвестиционной привлекательности', '/en/regions', 'search/index', '{\"sector\":\"region\"}', 'Регионы России', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Каталог цен на товары и услуги | Protoplan', 'Каталог цен по городам России', '/en/prices', 'search/index', '{\"sector\":\"price\"}', 'Каталог цен на товары и услуги | Orgplan', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Площадки России, СНГ, мира', 'В каталоге представлены площадки России, СНГ, мира. Воспользовавшись фильтром, можно найти площадку по региону и городу', '/en/venues', 'search/index', '{\"sector\":\"exhibitionComplex\"}', 'Площадки России, СНГ, мира', 'en');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Пользовательское соглашение | Protoplan', 'Настоящий документ «Пользовательское соглашение» представляет собой предложение ООО «Кубэкс Рус» (далее — «Правообладатель») заключить договор на изложенных ниже условиях.', '/en/help/termsOfUse', 'help/termsOfUse', '[]', 'Пользовательское соглашение | Orgplan', 'en');
		";
	}
}