<?php

class m160408_063718_delete_de_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE `id` = 10001;
			DELETE FROM {{fair}} WHERE `id` = 10002;
			DELETE FROM {{fair}} WHERE `id` = 10003;
			DELETE FROM {{fair}} WHERE `id` = 10004;
			DELETE FROM {{fair}} WHERE `id` = 10017;
			DELETE FROM {{fair}} WHERE `id` = 10006;
			DELETE FROM {{fair}} WHERE `id` = 10007;
			DELETE FROM {{fair}} WHERE `id` = 10008;
			DELETE FROM {{fair}} WHERE `id` = 10010;
			DELETE FROM {{fair}} WHERE `id` = 10011;
			DELETE FROM {{fair}} WHERE `id` = 10012;
			DELETE FROM {{fair}} WHERE `id` = 10013;
			DELETE FROM {{fair}} WHERE `id` = 10014;
			DELETE FROM {{fair}} WHERE `id` = 10015;
			DELETE FROM {{fair}} WHERE `id` = 10016;

			DELETE FROM {{trfair}} WHERE `trParentId` = 10001;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10002;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10003;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10004;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10017;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10006;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10007;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10008;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10010;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10011;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10012;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10013;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10014;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10015;
			DELETE FROM {{trfair}} WHERE `trParentId` = 10016;
		";
	}
}