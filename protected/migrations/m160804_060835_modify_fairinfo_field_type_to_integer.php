<?php

class m160804_060835_modify_fairinfo_field_type_to_integer extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			ALTER TABLE {{fairinfo}} 
			CHANGE COLUMN `areaClosedExhibitorsLocal` `areaClosedExhibitorsLocal` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `areaClosedExhibitorsForeign` `areaClosedExhibitorsForeign` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `areaOpenExhibitorsLocal` `areaOpenExhibitorsLocal` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `areaOpenExhibitorsForeign` `areaOpenExhibitorsForeign` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `areaSpecialExpositions` `areaSpecialExpositions` INT(11) NULL DEFAULT NULL ;
			
			ALTER TABLE {{fairinfo}} 
			CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}