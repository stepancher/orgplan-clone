<?php

class m160727_164915_replace_name_in_trdistrict extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{trdistrict}} SET `name`='Округ Минская область' WHERE `id`='80';
			UPDATE {{trdistrict}} SET `name`='Округ Могилёвская область' WHERE `id`='81';
			UPDATE {{trdistrict}} SET `name`='Округ Гродненская область' WHERE `id`='82';
			UPDATE {{trdistrict}} SET `name`='Округ Атырауская область' WHERE `id`='85';
			UPDATE {{trdistrict}} SET `name`='Округ Мангистауская область' WHERE `id`='86';
			UPDATE {{trdistrict}} SET `name`='Округ Актюбинская область' WHERE `id`='87';
			UPDATE {{trdistrict}} SET `name`='Округ Павлодарская область' WHERE `id`='88';
			UPDATE {{trdistrict}} SET `name`='Округ Кишиневская агломерация' WHERE `id`='90';
			UPDATE {{trdistrict}} SET `name`='Округ Карагандинская область' WHERE `id`='91';
			UPDATE {{trdistrict}} SET `name`='Округ Ташкентская область' WHERE `id`='92';
			UPDATE {{trdistrict}} SET `name`='Округ Ферганская область' WHERE `id`='93';
			UPDATE {{trdistrict}} SET `name`='Округ Балканский велаят' WHERE `id`='95';
			UPDATE {{trdistrict}} SET `name`='Округ Киевская область' WHERE `id`='98';
			UPDATE {{trdistrict}} SET `name`='Округ Одесская область' WHERE `id`='99';
			UPDATE {{trdistrict}} SET `name`='Округ Минская область' WHERE `id`='55';
			UPDATE {{trdistrict}} SET `name`='Округ Могилёвская область' WHERE `id`='56';
			UPDATE {{trdistrict}} SET `name`='Округ Гродненская область' WHERE `id`='57';
			UPDATE {{trdistrict}} SET `name`='Округ Атырауская область' WHERE `id`='60';
			UPDATE {{trdistrict}} SET `name`='Округ Мангистауская область' WHERE `id`='61';
			UPDATE {{trdistrict}} SET `name`='Округ Актюбинская область' WHERE `id`='62';
			UPDATE {{trdistrict}} SET `name`='Округ Павлодарская область' WHERE `id`='63';
			UPDATE {{trdistrict}} SET `name`='Округ Кишиневская агломерация' WHERE `id`='65';
			UPDATE {{trdistrict}} SET `name`='Округ Карагандинская область' WHERE `id`='66';
			UPDATE {{trdistrict}} SET `name`='Округ Ташкентская область' WHERE `id`='67';
			UPDATE {{trdistrict}} SET `name`='Округ Ферганская область' WHERE `id`='68';
			UPDATE {{trdistrict}} SET `name`='Округ Балканский велаят' WHERE `id`='70';
			UPDATE {{trdistrict}} SET `name`='Округ Киевская область' WHERE `id`='73';
			UPDATE {{trdistrict}} SET `name`='Округ Одесская область' WHERE `id`='74';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}