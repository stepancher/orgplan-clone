<?php

class m150909_130356_create_tzadditionaldata extends CDbMigration
{
/**
 * @return bool
 * @throws CDbException
 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{tzadditionaldata}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE {{tzadditionaldata}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`tzId` int(11),
				`additionalDataId` int(11)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;
		';
	}
}