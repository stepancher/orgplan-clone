<?php

class m160411_114713_drop_tooltipCoefficient_add_tooltipCustom extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
	
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();
	
			echo $e->getMessage();
	
			return false;
		}
	
		return true;
	}


	public function down()
	{
		$sql = "
			ALTER TABLE {{attributeclass}} 
			ADD COLUMN `coefficientTooltipText` TEXT NULL AFTER `tooltip`,
			DROP COLUMN `tooltipCustom`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return "
				ALTER TABLE {{attributeclass}} 
				DROP COLUMN `coefficientTooltipText`,
				ADD COLUMN `tooltipCustom` TEXT NULL AFTER `tooltip`;
			";
	}
}