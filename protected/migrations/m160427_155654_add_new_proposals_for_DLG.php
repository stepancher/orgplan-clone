<?php

class m160427_155654_add_new_proposals_for_DLG extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */

    public function up()
    {

        $sql  = $this->getCreateTable();

        $transaction = Yii::app()->db->beginTransaction();
        try
        {
                Yii::app()->db->createCommand($sql)->execute();
                $transaction->commit();
        }

        catch(Exception $e)
        {
                $transaction->rollback();

                echo $e->getMessage();

                return false;
        }

        return true;
    }

    public function down()
    {

            return true;
    }

    public function getCreateTable(){

        return "
            INSERT INTO {{proposalelementclass}}

                    (`name`, `super`, `fixed`, `label`, `pluralLabel`, `fairId`, `description`)

                VALUES

                    ('zayavka_n1_agritechnica',NULL,0,'Заявка №1','ЭКСПОНЕНТ',10556,'Lorem Ipsum'),

                    ('zayavka_n4_agritechnica',NULL,0,'Заявка №4','ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ',10556,'dolor sit'),

                    ('zayavka_konkursnaya_agritechnica',NULL,0,'КОНКУРСНАЯ ЗАЯВКА','НЕЗАВИСИМЫЙ ПРОФЕСCИОНАЛЬНЫЙ КОНКУРС ИННОВАЦИОННОЙ СЕЛЬСКОХОЗЯЙСТВЕННОЙ ТЕХНИКИ',10556,'amet, consectetur adipiscing elit'),

                    ('zayavka_n2_agritechnica',NULL,0,'Заявка №2','СТАНДАРТНЫЙ СТЕНД',10556,NULL),

                    ('zayavka_n3_agritechnica',NULL,0,'Заявка №3','ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ',10556,NULL),

                    ('zayavka_n5_agritechnica',NULL,0,'Заявка №5','СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ',10556,NULL),

                    ('zayavka_n9_agritechnica',NULL,0,'Заявка №9','РАЗГРУЗКА/ПОГРУЗКА',10556,NULL);



            INSERT INTO {{attributemodel}}

                    (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`)

                VALUES

                    (1,13,0,0,0),

                    (1,14,0,0,1),

                    (1,22,0,0,2),

                    (1,42,0,0,4),

                    (1,131,0,0,3),

                    (1,66,0,0,5),

                    (1,71,0,0,6),

                    (2,75,0,0,2),

                    (3,88,0,0,2),

                    (3,97,0,0,3),

                    (3,100,0,0,10),

                    (1,109,0,0,7),

                    (1,113,0,0,8),

                    (2,138,0,0,3),

                    (3,105,0,0,4),

                    (3,154,0,0,5),

                    (3,157,0,0,6),

                    (2,218,0,0,4),

                    (3,225,0,0,11),

                    (1,234,0,0,-1),

                    (1,238,0,0,9),

                    (2,241,0,0,5),

                    (3,244,0,0,12),

                    (2,260,0,0,0),

                    (4,261,0,0,1),

                    (4,262,0,0,2),

                    (4,306,0,0,3),

                    (4,312,0,0,4),

                    (4,315,0,0,5),

                    (5,323,0,0,1),

                    (5,324,0,0,2),

                    (5,329,0,0,3),

                    (5,336,0,0,4),

                    (5,511,0,0,0),

                    (4,317,0,0,6),

                    (4,514,0,0,8),

                    (5,518,0,0,100),

                    (5,520,0,0,10),

                    (7,522,0,0,0),

                    (7,523,0,0,1),

                    (7,532,0,0,2),

                    (7,558,0,0,3),

                    (7,561,0,0,4),

                    (7,571,0,0,5),

                    (7,574,0,0,-1),

                    (10,576,0,0,13),

                    (10,577,0,0,2),

                    (10,585,0,0,3),

                    (10,594,0,0,4),

                    (10,611,0,0,-1),

                    (10,612,0,0,-2),

                    (10,614,0,0,100),

                    (10,616,0,0,5.5),

                    (10,619,0,0,5.6),

                    (10,628,0,0,20),

                    (10,643,0,0,6),

                    (10,654,0,0,8),

                    (10,661,0,0,-1),

                    (1,800,0,0,20),

                    (2,813,0,0,20),

                    (3,826,0,0,20),

                    (4,839,0,0,20),

                    (5,865,0,0,101),

                    (7,878,0,0,20),

                    (10,891,0,0,10),

                    (10,904,0,0,11),

                    (10,917,0,0,12),

                    (10,1044,0,0,1),

                    (10,1045,0,0,5.4);

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='99';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='100';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='101';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='102';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='103';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='104';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='105';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='110';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='111';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='118';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='119';

            UPDATE {{attributemodel}} SET `elementClass`='11' WHERE `id`='157';

            UPDATE {{attributemodel}} SET `elementClass`='12' WHERE `id`='106';

            UPDATE {{attributemodel}} SET `elementClass`='12' WHERE `id`='112';

            UPDATE {{attributemodel}} SET `elementClass`='12' WHERE `id`='116';

            UPDATE {{attributemodel}} SET `elementClass`='12' WHERE `id`='120';

            UPDATE {{attributemodel}} SET `elementClass`='12' WHERE `id`='122';

            UPDATE {{attributemodel}} SET `elementClass`='12' WHERE `id`='158';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='108';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='109';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='113';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='114';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='115';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='117';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='121';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='159';

            UPDATE {{attributemodel}} SET `elementClass`='13' WHERE `id`='107';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='123';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='124';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='125';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='126';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='127';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='133';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='134';

            UPDATE {{attributemodel}} SET `elementClass`='14' WHERE `id`='160';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='128';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='129';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='130';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='131';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='132';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='135';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='136';

            UPDATE {{attributemodel}} SET `elementClass`='15' WHERE `id`='161';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='137';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='138';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='139';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='140';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='141';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='142';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='143';

            UPDATE {{attributemodel}} SET `elementClass`='16' WHERE `id`='162';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='144';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='145';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='146';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='147';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='148';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='149';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='150';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='151';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='152';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='153';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='154';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='155';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='156';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='163';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='164';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='165';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='166';

            UPDATE {{attributemodel}} SET `elementClass`='17' WHERE `id`='167';
        ";
    }
}