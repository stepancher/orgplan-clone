<?php

class m160715_153205_fk_forumhasfairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			ALTER TABLE {{forumhasfairs}} 
			ADD UNIQUE INDEX `forum_fair` (`forumId` ASC, `fairId` ASC);
			
			ALTER TABLE {{forumhasfairs}} 
			ADD CONSTRAINT `fk_forumId`
			  FOREIGN KEY (`forumId`)
			  REFERENCES {{fair}} (`id`)
			  ON DELETE CASCADE
			  ON UPDATE CASCADE;

			ALTER TABLE {{forumhasfairs}} 
			ADD INDEX `fk_fairId_idx` (`fairId` ASC);
			ALTER TABLE {{forumhasfairs}} 
			ADD CONSTRAINT `fk_fairId`
			  FOREIGN KEY (`fairId`)
			  REFERENCES {{fair}} (`id`)
			  ON DELETE CASCADE
			  ON UPDATE CASCADE;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}