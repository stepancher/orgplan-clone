<?php

class m160322_142328_create_tbl_attributeclassdependency extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			DROP TABLE IF EXISTS {{attributeclasscoefficientdependency}};
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			DROP TABLE IF EXISTS {{attributeclasscoefficientdependency}};
			CREATE TABLE `tbl_attributeclasscoefficientdependency` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `attributeClassCoefficient` int(10) DEFAULT NULL,
			  `attributeClassRelated` int(10) unsigned DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  KEY `fk_tbl_attributeclassdependency_1_idx` (`attributeClassCoefficient`),
			  KEY `fk_attribute_class_coefficient_related_dependency_idx` (`attributeClassRelated`),
			  CONSTRAINT `fk_attribute_class_coefficient_dependency` FOREIGN KEY (`attributeClassCoefficient`) REFERENCES `tbl_attributeclasscoefficient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
			  CONSTRAINT `fk_attribute_class_dependency` FOREIGN KEY (`attributeClassRelated`) REFERENCES `tbl_attributeclass` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


		";
	}
}