<?php

class m160316_160308_update_trindustry_set_shortNameUrl extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
			update {{trindustry}} tri set tri.shortNameUrl = NULL where tri.langId = 'ru';
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			update {{trindustry}} tri
			left join {{industry}} i on (tri.trParentId = i.id)
			set tri.shortNameUrl = i.shortNameUrl where tri.langId = 'ru';
		";
	}





}