<?php

class m161003_151033_update_regions_code extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{region}} SET `code`='ua-kr' WHERE `id`='93';
            UPDATE {{region}} SET `code`='ru-ar' WHERE `id`='89';
            UPDATE {{region}} SET `code`='ru-ar' WHERE `id`='88';
            UPDATE {{region}} SET `code`='ru-ys' WHERE `id`='83';
            UPDATE {{region}} SET `code`='ru-yn' WHERE `id`='82';
            UPDATE {{region}} SET `code`='ru-ck' WHERE `id`='81';
            UPDATE {{region}} SET `code`='ru-cv' WHERE `id`='80';
            UPDATE {{region}} SET `code`='ru-cn' WHERE `id`='79';
            UPDATE {{region}} SET `code`='ru-cl' WHERE `id`='78';
            UPDATE {{region}} SET `code`='ru-km' WHERE `id`='77';
            UPDATE {{region}} SET `code`='ru-kh' WHERE `id`='76';
            UPDATE {{region}} SET `code`='ru-ul' WHERE `id`='75';
            UPDATE {{region}} SET `code`='ru-ud' WHERE `id`='74';
            UPDATE {{region}} SET `code`='ru-ty' WHERE `id`='73';
            UPDATE {{region}} SET `code`='ru-tl' WHERE `id`='72';
            UPDATE {{region}} SET `code`='ru-to' WHERE `id`='71';
            UPDATE {{region}} SET `code`='ru-tv' WHERE `id`='70';
            UPDATE {{region}} SET `code`='ru-tb' WHERE `id`='69';
            UPDATE {{region}} SET `code`='ru-st' WHERE `id`='68';
            UPDATE {{region}} SET `code`='ru-sm' WHERE `id`='67';
            UPDATE {{region}} SET `code`='ru-sv' WHERE `id`='66';
            UPDATE {{region}} SET `code`='ru-sl' WHERE `id`='65';
            UPDATE {{region}} SET `code`='ru-sr' WHERE `id`='64';
            UPDATE {{region}} SET `code`='ru-sp' WHERE `id`='63';
            UPDATE {{region}} SET `code`='ru-sa' WHERE `id`='62';
            UPDATE {{region}} SET `code`='ru-rz' WHERE `id`='61';
            UPDATE {{region}} SET `code`='ru-ro' WHERE `id`='60';
            UPDATE {{region}} SET `code`='ru-kk' WHERE `id`='59';
            UPDATE {{region}} SET `code`='ru-tu' WHERE `id`='58';
            UPDATE {{region}} SET `code`='ru-tt' WHERE `id`='57';
            UPDATE {{region}} SET `code`='ru-no' WHERE `id`='56';
            UPDATE {{region}} SET `code`='ru-sk' WHERE `id`='55';
            UPDATE {{region}} SET `code`='ru-mr' WHERE `id`='54';
            UPDATE {{region}} SET `code`='ru-me' WHERE `id`='53';
            UPDATE {{region}} SET `code`='ru-ko' WHERE `id`='52';
            UPDATE {{region}} SET `code`='ru-ki' WHERE `id`='51';
            UPDATE {{region}} SET `code`='ru-kl' WHERE `id`='50';
            UPDATE {{region}} SET `sponsorPos`=NULL, `code`='ru-in' WHERE `id`='49';
            UPDATE {{region}} SET `code`='ru-da' WHERE `id`='48';
            UPDATE {{region}} SET `code`='ru-bu' WHERE `id`='47';
            UPDATE {{region}} SET `code`='ru-bk' WHERE `id`='46';
            UPDATE {{region}} SET `code`='ru-ga' WHERE `id`='45';
            UPDATE {{region}} SET `code`='ru-ad' WHERE `id`='44';
            UPDATE {{region}} SET `code`='ru-ps' WHERE `id`='43';
            UPDATE {{region}} SET `code`='ru-pr' WHERE `id`='42';
            UPDATE {{region}} SET `code`='ru-pe' WHERE `id`='41';
            UPDATE {{region}} SET `code`='ru-pz' WHERE `id`='40';
            UPDATE {{region}} SET `code`='ru-ol' WHERE `id`='39';
            UPDATE {{region}} SET `code`='ru-ob' WHERE `id`='38';
            UPDATE {{region}} SET `code`='ru-om' WHERE `id`='37';
            UPDATE {{region}} SET `code`='ru-ns' WHERE `id`='36';
            UPDATE {{region}} SET `code`='ru-ng' WHERE `id`='35';
            UPDATE {{region}} SET `code`='ru-nz' WHERE `id`='34';
            UPDATE {{region}} SET `code`='ru-nn' WHERE `id`='33';
            UPDATE {{region}} SET `code`='ru-mm' WHERE `id`='32';
            UPDATE {{region}} SET `code`='ru-ms' WHERE `id`='31';
            UPDATE {{region}} SET `code`='ru-2510' WHERE `id`='30';
            UPDATE {{region}} SET `code`='ru-mg' WHERE `id`='29';
            UPDATE {{region}} SET `code`='ru-lp' WHERE `id`='28';
            UPDATE {{region}} SET `code`='ru-ln' WHERE `id`='27';
            UPDATE {{region}} SET `code`='ru-ks' WHERE `id`='26';
            UPDATE {{region}} SET `code`='ru-ku' WHERE `id`='25';
            UPDATE {{region}} SET `code`='ru-ky' WHERE `id`='24';
            UPDATE {{region}} SET `code`='ru-kd' WHERE `id`='23';
            UPDATE {{region}} SET `code`='ru-kt' WHERE `id`='22';
            UPDATE {{region}} SET `code`='ru-kv' WHERE `id`='21';
            UPDATE {{region}} SET `code`='ru-ke' WHERE `id`='20';
            UPDATE {{region}} SET `code`='ru-kc' WHERE `id`='19';
            UPDATE {{region}} SET `code`='ru-ka' WHERE `id`='18';
            UPDATE {{region}} SET `code`='ru-kg' WHERE `id`='17';
            UPDATE {{region}} SET `code`='ru-kn' WHERE `id`='16';
            UPDATE {{region}} SET `code`='ru-kb' WHERE `id`='15';
            UPDATE {{region}} SET `code`='ru-ir' WHERE `id`='14';
            UPDATE {{region}} SET `sponsorPos`=NULL, `code`='ru-iv' WHERE `id`='13';
            UPDATE {{region}} SET `code`='ru-ct' WHERE `id`='12';
            UPDATE {{region}} SET `code`='ru-yv' WHERE `id`='11';
            UPDATE {{region}} SET `code`='ru-vr' WHERE `id`='10';
            UPDATE {{region}} SET `code`='ru-vo' WHERE `id`='9';
            UPDATE {{region}} SET `code`='ru-vg' WHERE `id`='8';
            UPDATE {{region}} SET `code`='ru-vl' WHERE `id`='7';
            UPDATE {{region}} SET `code`='ru-br' WHERE `id`='6';
            UPDATE {{region}} SET `code`='ru-bl' WHERE `id`='5';
            UPDATE {{region}} SET `code`='ru-as' WHERE `id`='4';
            UPDATE {{region}} SET `code`='ru-ar' WHERE `id`='3';
            UPDATE {{region}} SET `code`='ru-am' WHERE `id`='2';
            UPDATE {{region}} SET `code`='ru-al' WHERE `id`='1';
            UPDATE {{region}} SET `code`='ua-kv' WHERE `id`='144';
            UPDATE {{region}} SET `code`='ua-my' WHERE `id`='145';
            UPDATE {{region}} SET `code`='az-ba' WHERE `id`='124';
            UPDATE {{region}} SET `code`='am-er' WHERE `id`='125';
            UPDATE {{region}} SET `code`='by-hr' WHERE `id`='128';
            UPDATE {{region}} SET `code`='by-ma' WHERE `id`='127';
            UPDATE {{region}} SET `code`='by-mi' WHERE `id`='126';
            UPDATE {{region}} SET `code`='kz-as' WHERE `id`='129';
            UPDATE {{region}} SET `code`='kz-aa' WHERE `id`='130';
            UPDATE {{region}} SET `code`='kz-ar' WHERE `id`='131';
            UPDATE {{region}} SET `code`='kz-mg' WHERE `id`='132';
            UPDATE {{region}} SET `code`='kz-at' WHERE `id`='133';
            UPDATE {{region}} SET `code`='kz-pa' WHERE `id`='134';
            UPDATE {{region}} SET `code`='kz-qg' WHERE `id`='137';
            UPDATE {{region}} SET `code`='kg-gb' WHERE `id`='135';
            UPDATE {{region}} SET `code`='kg-os' WHERE `id`='146';
            UPDATE {{region}} SET `code`='uz-ta' WHERE `id`='138';
            UPDATE {{region}} SET `code`='uz-fa' WHERE `id`='139';
            UPDATE {{region}} SET `code`='tm-ba' WHERE `id`='141';
            UPDATE {{region}} SET `code`='ge-tb' WHERE `id`='142';
            UPDATE {{region}} SET `code`='mn-ub' WHERE `id`='143';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}