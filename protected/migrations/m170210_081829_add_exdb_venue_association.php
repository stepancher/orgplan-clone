<?php

class m170210_081829_add_exdb_venue_association extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{exdbvenueassociation}} (
              `id` INT NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('3116', 'IAEE');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('5337', 'EFU');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('5734', 'IDFA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6576', 'FAMA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6586', 'FEBELUX');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6587', 'UBRAFE');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6593', 'UNIMEV');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6596', 'AEO');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6599', 'RUEF');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6600', 'HKECIA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6601', 'ASPERAPI');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6603', 'AEFI');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6617', 'AMPROFEC');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6631', 'MA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6634', 'PACEOS');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6635', 'PIPT');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6640', 'Expo-Event');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6647', 'Fairlink');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6648', 'AFE');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6650', 'EXSA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6667', 'AFIDA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6672', 'EMECA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6676', 'IAFE');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6686', 'SISO');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('6689', 'UFI');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('7712', 'CENTREX');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('9182', 'CEFA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('10987', 'EURASCO');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('11681', 'CFI');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('11881', 'AFECA');
            INSERT INTO {{exdbvenueassociation}} (`exdbId`,`name`) VALUES ('12358', 'IEIA');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}