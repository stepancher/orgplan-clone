<?php

class m160706_151513_add_column_priority_to_peoposalelementclass extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			ALTER TABLE {{proposalelementclass}}
			ADD COLUMN `priority` FLOAT NULL DEFAULT NULL AFTER `deadline`;

			UPDATE {{proposalelementclass}} SET `priority`='1' WHERE `id`='1';
			UPDATE {{proposalelementclass}} SET `priority`='2' WHERE `id`='4';
			UPDATE {{proposalelementclass}} SET `priority`='3' WHERE `id`='5';
			UPDATE {{proposalelementclass}} SET `priority`='4' WHERE `id`='2';
			UPDATE {{proposalelementclass}} SET `priority`='5' WHERE `id`='7';
			UPDATE {{proposalelementclass}} SET `priority`='6' WHERE `id`='18';
			UPDATE {{proposalelementclass}} SET `priority`='7' WHERE `id`='23';
			UPDATE {{proposalelementclass}} SET `priority`='8' WHERE `id`='24';
			UPDATE {{proposalelementclass}} SET `priority`='9' WHERE `id`='10';
			UPDATE {{proposalelementclass}} SET `priority`='10' WHERE `id`='25';
			UPDATE {{proposalelementclass}} SET `priority`='11' WHERE `id`='20';
			UPDATE {{proposalelementclass}} SET `priority`='12' WHERE `id`='19';
			UPDATE {{proposalelementclass}} SET `priority`='14' WHERE `id`='21';
			UPDATE {{proposalelementclass}} SET `priority`='13' WHERE `id`='22';
			UPDATE {{proposalelementclass}} SET `priority`='15' WHERE `id`='3';
		";
	}
}