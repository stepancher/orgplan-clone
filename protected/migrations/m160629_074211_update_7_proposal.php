<?php

class m160629_074211_update_7_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		UPDATE {{trattributeclassproperty}} SET `value`='persons' WHERE `id`='2672';
		UPDATE {{trattributeclassproperty}} SET `value`='час.' WHERE `id`='2673';
		UPDATE {{trattributeclassproperty}} SET `value`='persons' WHERE `id`='2674';
		UPDATE {{trattributeclassproperty}} SET `value`='час.' WHERE `id`='2675';
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2412', '2411', 'multiple', '0');
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2412';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2417';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2422';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2427';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2434';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2439';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2444';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2449';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2469';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2473';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2477';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2481';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2485';
		UPDATE {{attributeclass}} SET `class`='coefficientMultiple' WHERE `id`='2488';
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2417', '2416', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2422', '2421', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2427', '2426', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2434', '2433', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2439', '2438', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2444', '2443', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2449', '2448', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2469', '2468', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2473', '2472', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2477', '2476', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2481', '2480', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2485', '2484', 'multiple', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2488', '2487', 'multiple', '0');
		
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2404', 'en', 'This form should be filled in until August 22, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2405', 'en', 'Individual security');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2406', 'en', 'Security services are provided by the LLC PSE \"Crocus Profi\", license No.986 dated Apr. 25, 2003, issued by the GUVD of Moscow region.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2407', 'en', 'Security services:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2408', 'en', 'Order No.1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2410', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2411', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2412', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2413', 'en', 'Order No.2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2415', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2416', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2417', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2418', 'en', 'Order No.3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2420', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2421', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2422', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2423', 'en', 'Order No.4');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2425', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2426', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2427', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2428', 'en', 'Minimum possible order 8 hours.<br><br>The object is taken for security and released from security in presence of an authorized representative of the Exhibitor!');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2429', 'en', 'Event access control:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2430', 'en', 'Order No.1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2432', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2433', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2434', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2435', 'en', 'Order No.2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2437', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2438', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2439', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2440', 'en', 'Order No.3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2442', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2443', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2444', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2445', 'en', 'Order No.4');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2447', 'en', 'Start of Shift', '__:__ ____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2448', 'en', 'Duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2449', 'en', 'Person(s)', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2450', 'en', 'Minimum possible order 8 hours.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2451', 'en', 'Stand cleaning:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2452', 'en', 'General (once a day) stand cleaning', '<div class=\"header\">GENERAL STAND CLEANING</div><div class=\"body\">includes cleaning of carpet with the vakuum cleaner and cleaning of waste-paper baskets.</div>');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2453', 'en', 'Order No.1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2454', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2455', 'en', 'Space for cleaning', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2456', 'en', 'Order No.2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2457', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2458', 'en', 'Space for cleaning', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2459', 'en', 'Order No.3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2460', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2461', 'en', 'Space for cleaning', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2462', 'en', 'Order No.4');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2463', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2464', 'en', 'Space for cleaning', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2465', 'en', 'Cleaning services personnel', '<div class=\"header\">CLEANING SERVICES PERSONNEL</div><div class=\"body\">includes washing of the solid floor coating (laminate, tile), glass cleaning, dusting of the exhibits equipment.</div>');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2466', 'en', 'Order No.1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2467', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2468', 'en', 'Duration', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2469', 'en', 'Person(s)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2470', 'en', 'Order No.2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2471', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2472', 'en', 'Duration', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2473', 'en', 'Person(s)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2474', 'en', 'Order No.3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2475', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2476', 'en', 'Duration', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2477', 'en', 'Person(s)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2478', 'en', 'Order No.4');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2479', 'en', 'Date', '____/__/__');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2480', 'en', 'Duration', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2481', 'en', 'Person(s)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2482', 'en', 'Service staff:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2483', 'en', 'Unskilled workers');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2484', 'en', 'Person(s)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2485', 'en', 'Duration', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2486', 'en', 'Foreman of unskilled workers');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2487', 'en', 'Person(s)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltip`) VALUES ('2488', 'en', 'Duration', '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 100% if ordered after Sept. 15, 2016.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2489', 'en', 'Unskilled workers and foreman are not supposed to be taken to unloading/loading operations/works.<br>At order of more than two unskilled workers it is supposed to take the foreman is services.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2491', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>Cost of services increases by 100% if ordered after Sept. 15, 2016.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application are considered to be performed with the proper quality and accepted by the Exhibitor.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2493', 'en', 'Payments shall be effected in USD according to exchange rate of Central Bank of RF (www.cbr.ru) on the invoice date. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1992', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1993', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1994', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1995', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1996', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1997', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1998', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1999', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2000', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2001', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2002', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2003', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2004', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2005', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2006', 'en', 'sq.m.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2007', 'en', 'sq.m.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2008', 'en', 'sq.m.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2009', 'en', 'sq.m.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2010', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2011', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2012', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2013', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2014', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2015', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2016', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2017', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2018', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2019', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2020', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2021', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2022', 'en', 'hours');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2023', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2042', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2043', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2044', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2045', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2046', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2047', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2048', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2049', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2050', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2051', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2052', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2053', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2054', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2055', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2056', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2057', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2058', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2059', 'en', 'RUB');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2060', 'en', 'RUB');

		";
	}
}