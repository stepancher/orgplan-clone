<?php

class m160404_190345_add_new_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
{
	return true;
}

	public function down()
{
	return true;
}

	public function getCreateTable(){
	return <<<EOL
	INSERT INTO {{proposalelementclass}} (`name`, `fixed`, `label`, `pluralLabel`, `fairId`, `description`) VALUES ('zayavka_n3', '0', 'Заявка №3', 'ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ', '184', NULL);
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n3_hint_danger', 'string', '0', 'Отправьте запоненную форму в дирекцию выставки не позднее 25 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru', '0', '323', '0', 'hintDanger', '0');
	INSERT INTO {{attributemodel}}` (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('5', '323', '0', '0', '1');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n3_size_of_stand', 'int', '0', 'Размер стенда:', '0', '324', '0', 'headerH1', '0', NULL);
	INSERT INTO {{attributemodel}}` (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('5', '324', '0', '0', '2');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n3_size_of_stand_length', 'double', '0', 'Длина', '324', '324', '1', 'double', '0', 'м');
	INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES (NULL, 'n3_size_of_stand_width', 'double', '0', 'Ширина', '324', '324', '1', 'double', '0', 'м');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n3_size_of_stand_square', 'double', '0', 'Площадь', '324', '324', '1', 'double', '0', 'м²');
	INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_hint_danger2', 'string', '0', 'Вся площадь стенда должна иметь ковровое покрытие. <br> Если Вами уже заказана стандартная застройка на часть площади стенда, закажите ковровое покрытие на всю оставшуюся площадь.', '324', '324', '4', 'hintDanger', '0');
	UPDATE {{attributeclass}} SET `priority`='3' WHERE `id`='327';
	UPDATE {{attributeclass}} SET `priority`='2' WHERE `id`='326';
	INSERT INTO {{attributeclass}} (`name`) VALUES ('');
	UPDATE {{attributeclass}} SET `name`='n3_schema', `label`='Схема размещения оборудования на стенде:', `super`='329', `priority`='0', `class`='headerH1' WHERE `id`='329';
	INSERT INTO {{attributemodel}}` (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('5', '329', '0', '0', '3');
	INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_plan_image1', 'string', '0', 'plan.png', '329', '329', '1', 'image', '0');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n3_schema_spoiler1', 'int', '0', 'ПРИМЕР СХЕМЫ РАЗМЕЩЕНИЯ ОБОРУДОВАНИЯ НА СТЕНДЕ', '329', '329', '2', 'spoiler', '1');
	INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_plan_image2', 'string', '0', 'plan.png', '331', '329', '1', 'image', '0');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n3_hint_danger3', 'int', '0', 'Не изображенное оборудование на стенд установлено не будет. <br> <br> Схему оборудования необходимо отобразить в печатной версии заявки', '329', '329', '3', 'hintDanger', '0');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n3_hint', 'int', '0', 'Если Вам необходимо больше места, начертите план на отдельном листе и приложите к этой заявке.', '329', '329', '4', 'hint', '0');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n3_catalog', 'int', '0', 'Каталог оборудования:', '329', '329', '5', 'headerH1', '0');
	INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_hint_danger4', 'int', '0', 'Если Вы заказываете электрооборудование (светильники, розетки и др.), необходимо дополнительно заказать подключение к источнику электроснабжения соответсвующей мощности (Заявка №4).', '336', '336', '1', 'hintDanger', '0');
	UPDATE {{attributeclass}} SET `parent`='0', `super`='336', `priority`='0' WHERE `id`='336';
	INSERT INTO {{attributemodel}}` (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('5', '336', '0', '0', '4');
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n3_catalog_spoiler1', 'int', '0', NULL, 'Конструкции ADVANTEC', '336', '336', '2', 'spoiler', '0');


	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler1', 'int', '0', NULL,
	  'Конструкции ADVANTEC', '336', '336',
	  '2', 'spoiler', '0'
	  );
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler2', 'int', '0', NULL,
	  'Конструкции для стадартных стендов', '336', '336',
	  '3', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler3', 'int', '0', NULL,
	  'Мебель', '336', '336',
	  '4', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler4', 'int', '0', NULL,
	  'Информационные стойки', '336', '336',
	  '5', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler5', 'int', '0', NULL,
	  'Витрины', '336', '336',
	  '6', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler6', 'int', '0', NULL,
	  'Стеллажи и настенные полки', '336', '336',
	  '7', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler7', 'int', '0', NULL,
	  'Шкафы архивные', '336', '336',
	  '8', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler8', 'int', '0', NULL,
	  'Вешала', '336', '336',
	  '9', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler9', 'int', '0', NULL,
	  'Оборудование для офиса', '336', '336',
	  '10', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler10', 'int', '0', NULL,
	  'Оборудование для кухни', '336', '336',
	  '11', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler11', 'int', '0', NULL,
	  'Электрооборудование', '336', '336',
	  '12', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler12', 'int', '0', NULL,
	  'Художественное оформление', '336', '336',
	  '13', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler13', 'int', '0', NULL,
	  'Элементы конструктива', '336', '336',
	  '14', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler14', 'int', '0', NULL,
	  'Дополнительное оборудование', '336', '336',
	  '15', 'spoiler', '0'
	  );
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	  'n3_catalog_spoiler15', 'int', '0', NULL,
	  'Залоги за оборудование', '336', '336',
	  '16', 'spoiler', '0'
	  );

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item1', 'int', '0', NULL,
	'Стеновая панель Advantec 1000 x 3000',
	'338',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item2', 'int', '0', NULL,
	'Стеновая панель Advantec 500 x 3000',
	'338',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item3', 'int', '0', NULL,
	'Дверь распашная с замком Advantec 1000 x 3000',
	'338',
	'336',
	'3', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item4', 'int', '0', NULL,
	'Дверь стеклянная Advantec (цвет серый)',
	'338',
	'336',
	'4', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item5', 'int', '0', NULL,
	'Стеновая панель Advantec 1000 x 3000 со стеклом 810 x 2090',
	'338',
	'336',
	'5', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item6', 'int', '0', NULL,
	'Стеновая панель 1000 x 3000 со стеклом 810 x 2090 и горизонт. жалюзи',
	'338',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item7', 'int', '0', NULL,
	'Стеновая балка Advantec с шинопроводом, за м/п',
	'338',
	'336',
	'7', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item8', 'int', '0', NULL,
	'Полка настенная (1000 x 300) H =1200',
	'338',
	'336',
	'8', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler1_item9', 'int', '0', NULL,
	'Информационная стойка Advantec 1000 x 400 H=1000',
	'338',
	'336',
	'9', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item1', 'int', '0', NULL,
	'Элемент стены (1000 х 2500)',
	'342',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item2', 'int', '0', NULL,
	'Элемент стены (500 х 2500)',
	'342',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item3', 'int', '0', NULL,
	'Элемент стены радиусный R=1000 H=2500 (1/4 круга)',
	'342',
	'336',
	'3', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item4', 'int', '0', NULL,
	'Элемент стены радиусный R=500 H=2500 (1/4 круга)',
	'342',
	'336',
	'4', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item5', 'int', '0', NULL,
	'Элемент стены диагональный 1х1м (~1390 x 2500)',
	'342',
	'336',
	'5', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item6', 'int', '0', NULL,
	'Элемент стены диагональный 0,5 х 0,5м (~680 x 2500)',
	'342',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item7', 'int', '0', NULL,
	'Элемент стены диагональный для установки двери (~390 x 2500)',
	'342',
	'336',
	'7', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item8', 'int', '0', NULL,
	'Элемент стены со стеклом (1000 х 2500, стекло 970 х 1942)',
	'342',
	'336',
	'8', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item9', 'int', '0', NULL,
	'Элемент стены со стеклом (1000 х 2500, стекло 970 х 1262)',
	'342',
	'336',
	'9', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item10', 'int', '0', NULL,
	'Элемент стены со стеклом (500 х 2500, стекло 470 х 1262)',
	'342',
	'336',
	'10', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item11', 'int', '0', NULL,
	'Дверь-гармошка с замком (1000 х 2500) (пластик)',
	'342',
	'336',
	'11', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item12', 'int', '0', NULL,
	'Дверь-гармошка с замком (1000 х 2500) (кожзам., улучшенный замок)',
	'342',
	'336',
	'12', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item13', 'int', '0', NULL,
	'Дверь распашная с замком (1000 х 2500)',
	'342',
	'336',
	'13', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item14', 'int', '0', NULL,
	'Занавес-штора (1000 х 2500)',
	'342',
	'336',
	'14', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item15', 'int', '0', NULL,
	'Жалюзи (1400 х 1000)',
	'342',
	'336',
	'15', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item16', 'int', '0', NULL,
	'Увеличение высоты стены на 0,5м, за 1 п.м.',
	'342',
	'336',
	'16', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item17', 'int', '0', NULL,
	'Потолочный растр из прогонов H=70 (1000 x 1000)',
	'342',
	'336',
	'17', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item18', 'int', '0', NULL,
	'Потолочная решетка без потолочного растра (980 х 980)',
	'342',
	'336',
	'18', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item19', 'int', '0', NULL,
	'Ограждение - стойки H=480(a), 750(b), 1100(c), за 1 шт.',
	'342',
	'336',
	'19', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item20', 'int', '0', NULL,
	'Цепь декоративная, за 1 п.м.',
	'342',
	'336',
	'20', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item21', 'int', '0', NULL,
	'Ковровое покрытие шир. 2м (без подрезки) за 1кв.м.',
	'342',
	'336',
	'21', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item22', 'int', '0', NULL,
	'Ковровое покрытие шир. 2м (с подрезкой) за 1кв.м.',
	'342',
	'336',
	'22', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item23', 'int', '0', NULL,
	'Ламинат (включая работу по укладке), за 1кв.м.',
	'342',
	'336',
	'23', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item24', 'int', '0', NULL,
	'Работа по укладке ламината (материал Заказчика), за 1 кв.м.',
	'342',
	'336',
	'24', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler2_item25', 'int', '0', NULL,
	'Полиэтиленовая пленка, (включая работу по укладке) за 1кв.м.',
	'342',
	'336',
	'25', 'coefficientDoubleInline', '0');

	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item1', 'int', '0', NULL,
	'Стул',
	'343',
	'336',
	'1', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item2', 'int', '0', NULL,
	'Стул мягкий',
	'343',
	'336',
	'2', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item3', 'int', '0', NULL,
	'Стул офисный вращающийся',
	'343',
	'336',
	'3', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item4', 'int', '0', NULL,
	'Стул барный',
	'343',
	'336',
	'4', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item5', 'int', '0', NULL,
	'Стол 800 х 800мм',
	'343',
	'336',
	'5', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item6', 'int', '0', NULL,
	'Стол круглый D=800мм',
	'343',
	'336',
	'6', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item7', 'int', '0', NULL,
	'Стол 800 x 1200мм',
	'343',
	'336',
	'7', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item8', 'int', '0', NULL,
	'Стол барный D=600мм H=1200мм(a), 800мм(b)',
	'343',
	'336',
	'8', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item9', 'int', '0', NULL,
	'Стол круглый стеклянный D=600мм',
	'343',
	'336',
	'9', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item10', 'int', '0', NULL,
	'Стол дисплейный из конструктива 500 х 1000мм H=800мм',
	'343',
	'336',
	'10', 'coefficientDoubleInline', '0');
	
	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler3_item11', 'int', '0', NULL,
	'Стол дисплейный из конструктива 1000 х 1000мм H=800мм',
	'343',
	'336',
	'11', 'coefficientDoubleInline', '0');


	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler4_item1', 'int', '0', NULL,
	'Информационная стойка 500 х 1000мм H=1100мм',
	'344',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler4_item2', 'int', '0', NULL,
	'Инфо-стойка с узкой верхней полкой 500 х 1000мм H=1100мм',
	'344',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler4_item3', 'int', '0', NULL,
	'Информационная стойка радиусная R1=1000мм R2=500мм 1000 х 1000мм',
	'344',
	'336',
	'3', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler4_item4', 'int', '0', NULL,
	'Информационная стойка радиусная R=500мм 500 х 500мм',
	'344',
	'336',
	'4', 'coefficientDoubleInline', '0');


	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item1', 'int', '0', NULL,
	'Витрина стеклянная 500 х 1000мм H=1100мм',
	'345',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item2', 'int', '0', NULL,
	'Витрина стеклянная 500 х 500мм H=1100мм',
	'345',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item3', 'int', '0', NULL,
	'Витрина стеклянная 500 х 500 R=500 H=1100мм',
	'345',
	'336',
	'3', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item4', 'int', '0', NULL,
	'Витрина стеклянная 1000 x 1000 R(внешн.)=1000 R(внутр.)=500 H=1100мм',
	'345',
	'336',
	'4', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item5', 'int', '0', NULL,
	'Витрина стеклянная, две стекл. полки 500 x 1000мм H=2000мм',
	'345',
	'336',
	'5', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item6', 'int', '0', NULL,
	'Витрина стеклянная, две стекл. полки, подсветка 500 x 1000мм H=2500мм',
	'345',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item7', 'int', '0', NULL,
	'Витрина стеклянная, две стекл. полки, подсветка 500 x 500мм H=2500мм',
	'345',
	'336',
	'7', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item8', 'int', '0', NULL,
	'Витрина стеклянная, две стекл. полки, подсветка 500 x 500мм R=500 x H=1100мм',
	'345',
	'336',
	'8', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item9', 'int', '0', NULL,
	'Витрина стеклянная, две стекл. полки, подсветка, шторки 500 x 1000 H=2500мм',
	'345',
	'336',
	'9', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item10', 'int', '0', NULL,
	'Витрина стеклянная радиусная, две стекл. полки, подсветка, R(внутр.)=500, R(внешн.)=1000, H=2500',
	'345',
	'336',
	'10', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item11', 'int', '0', NULL,
	'Витрина стекл. круглая, две стекл. полки, D=1000мм H=2500мм',
	'345',
	'336',
	'11', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item12', 'int', '0', NULL,
	'Дополнительная стеклянная полка (500 х 1000, 500 х 500) в витрину 398б 398а',
	'345',
	'336',
	'12', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item13', 'int', '0', NULL,
	'Дверцы в витрину 396, 398',
	'345',
	'336',
	'13', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler5_item14', 'int', '0', NULL,
	'Дополнительня деревянная полка (500 х 1000, 500 х 500)',
	'345',
	'336',
	'14', 'coefficientDoubleInline', '0');



	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item1', 'int', '0', NULL,
	'Стеллаж, пять полок 300 х 1000мм H=2070мм',
	'346',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item2', 'int', '0', NULL,
	'Стеллаж, пять полок 500 х 1000мм H=2070мм',
	'346',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item3', 'int', '0', NULL,
	'Стеллаж, пять полок 1000 х 1000мм H=2070мм',
	'346',
	'336',
	'3', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item4', 'int', '0', NULL,
	'Стеллаж передвижной 500 х 1000мм H=1600мм',
	'346',
	'336',
	'4', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item5', 'int', '0', NULL,
	'Проспектодержатель отдельностоящий',
	'346',
	'336',
	'5', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item6', 'int', '0', NULL,
	'Полка настенная 300 х 1000мм, (по умолчанию H(уст.)=1200мм)',
	'346',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item7', 'int', '0', NULL,
	'Полка настенная наклонная, 45° 300 х 1000мм, (по умолчанию H(уст.)=1200мм)',
	'346',
	'336',
	'7', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item8', 'int', '0', NULL,
	'Решетка настенная, ячейка 50х50, 800 х 1500мм без крючков',
	'346',
	'336',
	'8', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item9', 'int', '0', NULL,
	'Европанель с перфорацией, навесная, 900 х 1200мм(1000мм) без крючков',
	'346',
	'336',
	'9', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler6_item10', 'int', '0', NULL,
	'Крючок S-образный L=50 для решетки (L=70 для прогонов), за 1 шт.',
	'346',
	'336',
	'10', 'coefficientDoubleInline', '0');


	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler7_item1', 'int', '0', NULL,
	'Шкаф архивный с дверками 645 х 492 (500 х 1000 х 1100)',
	'347',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler7_item2', 'int', '0', NULL,
	'Шкаф архивный с дверками 645 х 492 (500 х 1000 х 800)',
	'347',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler7_item3', 'int', '0', NULL,
	'Шкаф архивный с дверками 952 х 492 (500 х 1000 х 1100)',
	'347',
	'336',
	'3', 'coefficientDoubleInline', '0');



	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler8_item1', 'int', '0', NULL,
	'Вешало передвижное (1000 х 1600)',
	'348',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler8_item2', 'int', '0', NULL,
	'Вешало-консоль, за 1 п.м.',
	'348',
	'336',
	'2', 'coefficientDoubleInline', '0');





	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item1', 'int', '0', NULL,
	'Холодильник 200л (600 х 600 х H=1600), необходима круглосуточная розетка',
	'350',
	'336',
	'1', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item2', 'int', '0', NULL,
	'Холодильник 280л (600 х 600 х H=2000), необходима круглосуточная розетка',
	'350',
	'336',
	'2', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item3', 'int', '0', NULL,
	'Кофемашина, выдается под залог',
	'350',
	'336',
	'3', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item4', 'int', '0', NULL,
	'Кулер + 1 бутыль воды (19л)',
	'350',
	'336',
	'4', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item5', 'int', '0', NULL,
	'Бутыль воды 19л для кулера',
	'350',
	'336',
	'5', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item6', 'int', '0', NULL,
	'Кухонный узел (мойка) (550 х 900 х 850)',
	'350',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item1', 'int', '0', NULL,
	'Спот-бра (100 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item2', 'int', '0', NULL,
	'Спот-бра (70 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item3', 'int', '0', NULL,
	'Спот-бра галогеновый (75 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item4', 'int', '0', NULL,
	'Прожектор галогеновый (300 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item5', 'int', '0', NULL,
	'Прожектор металогалогеновый (150 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');


	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item6', 'int', '0', NULL,
	'Светильник галогеновый на штанге (150 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item7', 'int', '0', NULL,
	'Лампа дневного света (40 Вт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item8', 'int', '0', NULL,
	'Розетка 220v (одинарная до 1,0 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item9', 'int', '0', NULL,
	'Розетка 220v (одинарная до 2,5 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item10', 'int', '0', NULL,
	'Розетка 220v (силовой разъем 32A, до 5,0 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item11', 'int', '0', NULL,
	'Розетка 220v (тройная до 1,0 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item12', 'int', '0', NULL,
	'Розетка 220v (тройная до 2,5 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item13', 'int', '0', NULL,
	'Розетка 220v (одинарная круглосуточная до 1,0 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item14', 'int', '0', NULL,
	'Розетка 220v (одинарная круглосуточная до 2,5 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item15', 'int', '0', NULL,
	'Розетка 220v (тройная круглосуточная до 1,0 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item16', 'int', '0', NULL,
	'Розетка 220v (тройная круглосуточная до 2,5 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item17', 'int', '0', NULL,
	'Розетка 380 v (силовой разъем 16А до 10 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item18', 'int', '0', NULL,
	'Розетка 380 v (силовой разъем 32А до 20 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item19', 'int', '0', NULL,
	'Розетка 380 v (силовой разъем 63А до 40 кВт)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler11_item20', 'int', '0', NULL,
	'Световой вращающийся куб/эллипс 500 Вт, без логотипа (1000х1000х1000)',
	'351',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler10_item6', 'int', '0', NULL,
	'Надпись на фризе (до 9 знаков)',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item1', 'int', '0', NULL,
	'Дополнительный знак для надписи на фризе',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item2', 'int', '0', NULL,
	'Оклейка цветной пленкой, за 1 кв.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item3', 'int', '0', NULL,
	'Оклейка цветной пленкой «полоса» (ширина до 0,5 м ), за 1 п.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item4', 'int', '0', NULL,
	'Оклейка материалом заказчика, за 1 кв.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item5', 'int', '0', NULL,
	'Логотип компании на фризовой доске',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item6', 'int', '0', NULL,
	'Логотип компании на световом кубе',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item7', 'int', '0', NULL,
	'Печать банера (с люверсами, с карманами), за 1 кв.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item8', 'int', '0', NULL,
	'Печать на баннерной сетке, за 1 кв.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item9', 'int', '0', NULL,
	'Печать на самоклеющейся пленке, за 1 кв.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler12_item10', 'int', '0', NULL,
	'Монтаж банера (без учета конструкции, на высоте до 5м), за 1 кв.м.',
	'352',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item1', 'int', '0', NULL,
	'Стеновая панель 1000 х 1000, 1000 х 700',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item2', 'int', '0', NULL,
	'Стеновая панель 500 х 1000, 500 х 700',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item3', 'int', '0', NULL,
	'Стойка (восьмигранный опорный профиль) Н=2480 , 2070 , 1600(d)',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item4', 'int', '0', NULL,
	'Стойка (восьмигранный опорный профиль) Н=1100 , 750 , 480(a)',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item5', 'int', '0', NULL,
	'Прогон – соединительный профиль (Н=70), за п.м.',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item6', 'int', '0', NULL,
	'Прогон – усиленный профиль (Н=175), за п.м.',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item7', 'int', '0', NULL,
	'Фризовая доска навесная (Н=300), за 1 п.м.',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item8', 'int', '0', NULL,
	'Фризовая панель вставная (Н=355)',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item9', 'int', '0', NULL,
	'Модуль «Astralite», несущая ферма (без подвесных работ), за 1 п.м.',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler13_item10', 'int', '0', NULL,
	'Модуль «Astralite», соединительный элемент, за 1 шт.',
	'353',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item1', 'int', '0', NULL,
	'Подиум с подсветкой без коврового покрытия (Н=0.2 м), за 1 кв.м',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item2', 'int', '0', NULL,
	'Подиум без коврового покрытия (Н=0. м), за 1 кв. м.',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item3', 'int', '0', NULL,
	'Подиум без коврового покрытия (Н=0. м) за 1 кв. м',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item4', 'int', '0', NULL,
	'Подиум без коврового покрытия (Н=0.5 м) за 1 кв. м',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item5', 'int', '0', NULL,
	'Окантовка подиума металлическим уголком за 1 п. м',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item6', 'int', '0', NULL,
	'Кресло одноместное (кож.зам. 870 х 820 х 860)',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item7', 'int', '0', NULL,
	'Диван двухместный (кож.зам. 1460 х 820 х 860)',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item8', 'int', '0', NULL,
	'Столик журнальный стеклянный прямоугольный (900 х 550 х H=400)',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler14_item9', 'int', '0', NULL,
	'Столик журнальный стеклянный круглый (D=800, Н=495)',
	'354',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler15_item1', 'int', '0', NULL,
	'Кулер + вода',
	'339',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler15_item2', 'int', '0', NULL,
	'Кофемашина',
	'339',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler15_item3', 'int', '0', NULL,
	'Замок реечный для витрины и/или шкафа архивного',
	'339',
	'336',
	'6', 'coefficientDoubleInline', '0');

	INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`)
	VALUES (
	'n3_catalog_spoiler15_item4', 'int', '0', NULL,
	'Ключ от распашной/раздвижной двери',
	'339',
	'336',
	'6', 'coefficientDoubleInline', '0');




	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='338';
	UPDATE {{attributeclass}} SET `group`='' WHERE `id`='338';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='339';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='342';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='343';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='344';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='345';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='346';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='347';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='348';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='349';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='350';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='351';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='352';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='353';
	UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='354';
EOL;
}

}