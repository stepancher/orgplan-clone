<?php

class m170327_114804_edit_organizer_additional_contact_information extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{organizercontactlist}} SET `additionalContactInformation` = NULL WHERE `additionalContactInformation` = '';
            
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб. 108' WHERE `id`='49024';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56067';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56068';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56075';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56076';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56083';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56084';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56113';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56114';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56143';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56144';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56147';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56148';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56059';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.615' WHERE `id`='56060';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.624' WHERE `id`='56115';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.624' WHERE `id`='56116';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.624' WHERE `id`='56117';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.624' WHERE `id`='56118';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.657' WHERE `id`='56119';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.657' WHERE `id`='56120';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='56007';
INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '6', '1');
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='56008';
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='56009';
INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '7', '1');
UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='56010';
UPDATE {{organizercontactlist}} SET `value`='7(495)937-40-81', `additionalContactInformation`='доб.324' WHERE `id`='56005';
INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('681', '7(919)236-27-78', '1', NULL, '1');
UPDATE {{organizercontactlist}} SET `value`='7(499)681-33-83', `additionalContactInformation`='доб.324' WHERE `id`='56006';
UPDATE {{organizercontactlist}} SET `countryId`='1' WHERE `id`='56256';
UPDATE {{organizercontactlist}} SET `countryId`='1' WHERE `id`='56274';
UPDATE {{organizercontactlist}} SET `countryId`='1' WHERE `id`='56275';

            DROP PROCEDURE IF EXISTS `edit_organizer_contact`;
            CREATE PROCEDURE `edit_organizer_contact`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_dob TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT t.id, t.dob FROM
                                        (SELECT ocl.id, ocl.additionalContactInformation,
                                        CASE WHEN SUBSTR(ocl.additionalContactInformation,1,3) = 'доб' THEN SUBSTR(ocl.additionalContactInformation,5) 
                                         WHEN SUBSTR(ocl.additionalContactInformation,1,3) = 'доп' THEN SUBSTR(ocl.additionalContactInformation,5) 
                                        ELSE NULL END dob
                                        FROM {{organizercontactlist}} ocl WHERE ocl.additionalContactInformation IS NOT NULL
                                        ) t WHERE t.dob IS NOT NULL ORDER BY t.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO c_id, c_dob;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{organizercontactlist}} SET `additionalContactInformation` = c_dob WHERE `id` = c_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `edit_organizer_contact`();
            DROP PROCEDURE IF EXISTS `edit_organizer_contact`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}