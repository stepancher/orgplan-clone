<?php

class m160215_140118_tbl_proposalelementclass_update_fairId extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			UPDATE {{proposalelementclass}} SET `fairId`=\'645\' WHERE `id`=\'1\';
			UPDATE {{proposalelementclass}} SET `fairId`=\'645\' WHERE `id`=\'2\';
			UPDATE {{proposalelementclass}} SET `fairId`=\'645\' WHERE `id`=\'3\';
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{proposalelementclass}} SET `fairId`='184' WHERE `id`='1';
			UPDATE {{proposalelementclass}} SET `fairId`='184' WHERE `id`='2';
			UPDATE {{proposalelementclass}} SET `fairId`='184' WHERE `id`='3';
		";
	}
}