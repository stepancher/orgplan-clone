<?php

class m151210_134133_fake_stattypes_with_values extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '

			update {{chartcolumn}} set statId = NULL where id = 68;
			update {{chartcolumn}} set related = NULL where id = 67;
			update {{chartcolumn}} set statId = NULL where id = 82;
			update {{chartcolumn}} set related = NULL where id = 81;

			delete from {{gvalue}} where setId in (
				select ohs.id from {{gobjectshasgstattypes}} ohs left join
				{{gstattype}} s on (ohs.statId = s.id)
				where ohs.statId = 500000 OR ohs.statId = 500001
			);

			delete from {{gobjectshasgstattypes}} where statid = 500000 or statId = 500001;

			delete from {{gstattypes}} where gId = 500000 or gId = 500001;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			update {{chartcolumn}} set statId = 500001 where id = 68;
			update {{chartcolumn}} set related = 68 where id = 67;
			update {{chartcolumn}} set statId = 500000 where id = 82;
			update {{chartcolumn}} set related = 82 where id = 81;
			insert into {{gstattypes}} (id, gId, gType, name, description, units, epochType)
			values(500000, 500000, "region", "Построенных организациями", "Количество зданий, построенных организациями", "%", "years");

			insert into {{gstattypes}} (id, gId, gType, name, description, units, epochType)
			values(500001, 500001, "state", "Построенных организациями", "Количество зданий, построенных организациями", "%", "years");

			insert into {{gobjectshasgstattypes}} (objId, statId, groupId, groupName, frequency, gType)
			select id, 500000, 500000, "test", null, "region" from {{gobjects}} where gType = "region";

			insert into {{gobjectshasgstattypes}} (objId, statId, groupId, groupName, frequency, gType)
			select id, 500001, 500001, "test", null, "state" from {{gobjects}} where gType = "state";

			insert into {{gvalue}} (setId, timestamp, value, epoch)
			select ohs2.id as ohs2Id, ohs2.objId as ohs2ObjId, 100 - v.value as needed_value, v.epoch from
			{{gobjectshasgstattypes}} ohs left join
			{{gvalue}} v on (ohs.id = v.setId) left join
			{{gobjectshasgstattypes}} ohs2 on (ohs.objId = ohs2.objId and ohs2.statId = 500000) left join
			{{gvalue}} v2 on (ohs2.id = v2.setId) where
			ohs.statId = 4908 and v.epoch = "2013";

			insert into {{gvalue}} (setId, timestamp, value, epoch)
			select ohs2.id as ohs2Id, ohs2.objId as ohs2ObjId, 100 - v.value as needed_value, v.epoch from
			{{gobjectshasgstattypes}} ohs left join
			{{gvalue}} v on (ohs.id = v.setId) left join
			{{gobjectshasgstattypes}} ohs2 on (ohs.objId = ohs2.objId and ohs2.statId = 500001) left join
			{{gvalue}} v2 on (ohs2.id = v2.setId) where
			ohs.statId = 4992 and v.epoch = "2013";
		';
	}
}