<?php

class m170110_122550_to_lower_case_tr_industry_short_name_url extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `trIndustryShortNameUrlToLowerCase`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ind_id INT DEFAULT 0;
                DECLARE ind_lcShUrl VARCHAR(50) DEFAULT '';
                DECLARE lc CURSOR FOR SELECT id, lcase(shortNameUrl) AS lcShortNameUrl from {{trindustry}};
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN lc;
                    read_loop: LOOP
                        
                        FETCH lc INTO ind_id, ind_lcShUrl;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            UPDATE {{trindustry}} SET `shortNameUrl` = ind_lcShUrl WHERE `id` = ind_id;
                        COMMIT;
                        
                    END LOOP;
                CLOSE lc;
            END;
            
            CALL trIndustryShortNameUrlToLowerCase();
            DROP PROCEDURE IF EXISTS `trIndustryShortNameUrlToLowerCase`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}