<?php

class m151119_065911_create_tbl_charttable extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{charttable}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS  {{charttable}} (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`name` varchar(500) DEFAULT NULL,
				`type` int(11) DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

			INSERT INTO {{charttable}} (`id`,`name`,`type`) VALUES (1,"Структура сельского хозяйства",3);
			INSERT INTO {{charttable}} (`id`,`name`,`type`) VALUES (2,"Продукция сельского хозяйства",1);
			INSERT INTO {{charttable}} (`id`,`name`,`type`) VALUES (3,"Продукция сельского хозяйства по хозяйствам всех категорий",2);
		';
	}
}