<?php

class m160418_222406_insert_new_parties_signature_to_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`) VALUES ('parties_signature_n4', 'int', '1', '0', NULL);
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n4', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n4', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n4_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n4_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n4_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n4_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n4_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n4_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n4_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n4_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n4_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n4_4', 'string', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='839', `class`='partiesSignature' WHERE `id`='839';
			UPDATE {{attributeclass}} SET `parent`='839', `super`='839', `priority`='1' WHERE `id`='840';
			UPDATE {{attributeclass}} SET `parent`='839', `super`='839', `priority`='2' WHERE `id`='841';
			UPDATE {{attributeclass}} SET `parent`='840', `super`='839', `priority`='1', `class`='headerH1' WHERE `id`='842';
			UPDATE {{attributeclass}} SET `parent`='840', `super`='839', `priority`='2', `class`='text' WHERE `id`='843';
			UPDATE {{attributeclass}} SET `parent`='840', `super`='839', `priority`='4', `class`='headerH2' WHERE `id`='844';
			UPDATE {{attributeclass}} SET `parent`='840', `super`='839', `priority`='3', `class`='text' WHERE `id`='845';
			UPDATE {{attributeclass}} SET `parent`='841', `super`='839', `priority`='1', `class`='headerH1' WHERE `id`='846';
			UPDATE {{attributeclass}} SET `parent`='841', `super`='839', `priority`='2', `class`='text' WHERE `id`='847';
			UPDATE {{attributeclass}} SET `parent`='841', `super`='839', `priority`='4', `class`='headerH2' WHERE `id`='848';
			UPDATE {{attributeclass}} SET `parent`='841', `super`='839', `priority`='3', `class`='text' WHERE `id`='849';
			UPDATE {{attributeclass}} SET `parent`='841', `super`='839', `priority`='5', `class`='text' WHERE `id`='851';
			UPDATE {{attributeclass}} SET `parent`='841', `super`='839', `priority`='5', `class`='text' WHERE `id`='850';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('850', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('851', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('4', '839', '20');


			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_n5', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n5', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n5', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n5_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n5_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n5_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n5_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n5_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n5_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n5_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n5_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n5_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n5_4', 'string', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='865', `class`='partiesSignature' WHERE `id`='865';
			UPDATE {{attributeclass}} SET `parent`='865', `super`='865', `priority`='1' WHERE `id`='866';
			UPDATE {{attributeclass}} SET `parent`='865', `super`='865', `priority`='2' WHERE `id`='867';
			UPDATE {{attributeclass}} SET `parent`='866', `super`='865', `priority`='1', `class`='headerH1' WHERE `id`='868';
			UPDATE {{attributeclass}} SET `parent`='866', `super`='865', `priority`='2', `class`='text' WHERE `id`='869';
			UPDATE {{attributeclass}} SET `parent`='866', `super`='865', `priority`='4', `class`='headerH2' WHERE `id`='870';
			UPDATE {{attributeclass}} SET `parent`='866', `super`='865', `priority`='3', `class`='text' WHERE `id`='871';
			UPDATE {{attributeclass}} SET `parent`='867', `super`='865', `priority`='1', `class`='headerH1' WHERE `id`='872';
			UPDATE {{attributeclass}} SET `parent`='867', `super`='865', `priority`='2', `class`='text' WHERE `id`='873';
			UPDATE {{attributeclass}} SET `parent`='867', `super`='865', `priority`='4', `class`='headerH2' WHERE `id`='874';
			UPDATE {{attributeclass}} SET `parent`='867', `super`='865', `priority`='3', `class`='text' WHERE `id`='875';
			UPDATE {{attributeclass}} SET `parent`='866', `super`='865', `priority`='5', `class`='text' WHERE `id`='876';
			UPDATE {{attributeclass}} SET `parent`='867', `super`='865', `priority`='5', `class`='text' WHERE `id`='877';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('876', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('877', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('5', '865', '20');


			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_n6', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n6', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n6', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n6_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n6_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n6_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n6_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n6_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n6_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n6_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n6_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n6_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n6_4', 'string', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='878', `priority`=NULL WHERE `id`='878';
			UPDATE {{attributeclass}} SET `parent`='878', `super`='878', `priority`='1' WHERE `id`='879';
			UPDATE {{attributeclass}} SET `parent`='878', `super`='878', `priority`='2' WHERE `id`='880';
			UPDATE {{attributeclass}} SET `parent`='879', `super`='878', `priority`='1', `class`='headerH1' WHERE `id`='881';
			UPDATE {{attributeclass}} SET `parent`='879', `super`='878', `priority`='2', `class`='text' WHERE `id`='882';
			UPDATE {{attributeclass}} SET `parent`='879', `super`='878', `priority`='4', `class`='headerH2' WHERE `id`='883';
			UPDATE {{attributeclass}} SET `parent`='879', `super`='878', `priority`='3', `class`='text' WHERE `id`='884';
			UPDATE {{attributeclass}} SET `parent`='880', `super`='878', `priority`='1', `class`='headerH1' WHERE `id`='885';
			UPDATE {{attributeclass}} SET `parent`='880', `super`='878', `priority`='2', `class`='text' WHERE `id`='886';
			UPDATE {{attributeclass}} SET `parent`='880', `super`='878', `priority`='4', `class`='headerH2' WHERE `id`='887';
			UPDATE {{attributeclass}} SET `parent`='880', `super`='878', `priority`='3', `class`='text' WHERE `id`='888';
			UPDATE {{attributeclass}} SET `parent`='879', `super`='878', `priority`='5', `class`='text' WHERE `id`='889';
			UPDATE {{attributeclass}} SET `parent`='880', `super`='878', `priority`='5', `class`='text' WHERE `id`='890';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('889', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('890', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('7', '878', '20');


			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`) VALUES ('parties_signature_n8', 'int', '1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n8', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n8', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n8_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n8_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n8_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n8_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n8_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n8_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n8_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n8_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n8_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n8_4', 'string', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='891' WHERE `id`='891';
			UPDATE {{attributeclass}} SET `parent`='891', `super`='891', `priority`='1' WHERE `id`='892';
			UPDATE {{attributeclass}} SET `parent`='891', `super`='891', `priority`='2' WHERE `id`='893';
			UPDATE {{attributeclass}} SET `parent`='892', `super`='891', `priority`='1', `class`='headerH1' WHERE `id`='894';
			UPDATE {{attributeclass}} SET `parent`='892', `super`='891', `priority`='2', `class`='text' WHERE `id`='895';
			UPDATE {{attributeclass}} SET `parent`='892', `super`='891', `priority`='4', `class`='headerH2' WHERE `id`='896';
			UPDATE {{attributeclass}} SET `parent`='892', `super`='891', `priority`='3', `class`='text' WHERE `id`='897';
			UPDATE {{attributeclass}} SET `parent`='893', `super`='891', `priority`='1', `class`='headerH1' WHERE `id`='898';
			UPDATE {{attributeclass}} SET `parent`='893', `super`='891', `priority`='2', `class`='text' WHERE `id`='899';
			UPDATE {{attributeclass}} SET `parent`='893', `super`='891', `priority`='4', `class`='headerH2' WHERE `id`='900';
			UPDATE {{attributeclass}} SET `parent`='893', `super`='891', `priority`='3', `class`='text' WHERE `id`='901';
			UPDATE {{attributeclass}} SET `parent`='892', `super`='891', `priority`='5', `class`='text' WHERE `id`='902';
			UPDATE {{attributeclass}} SET `parent`='893', `super`='891', `priority`='5', `class`='text' WHERE `id`='903';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('902', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('903', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('8', '891', '20');


			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`) VALUES ('parties_signature_n9', 'int', '1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n9', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n9', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n9_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n9_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n9_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n1_9', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n9_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n9_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n9_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n9_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n9_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n9_4', 'string', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='904' WHERE `id`='904';
			UPDATE {{attributeclass}} SET `parent`='904', `super`='904', `priority`='1' WHERE `id`='905';
			UPDATE {{attributeclass}} SET `parent`='904', `super`='904', `priority`='2' WHERE `id`='906';
			UPDATE {{attributeclass}} SET `parent`='905', `super`='904', `priority`='1', `class`='headerH1' WHERE `id`='907';
			UPDATE {{attributeclass}} SET `parent`='905', `super`='904', `priority`='2', `class`='text' WHERE `id`='908';
			UPDATE {{attributeclass}} SET `parent`='905', `super`='904', `priority`='4', `class`='headerH2' WHERE `id`='909';
			UPDATE {{attributeclass}} SET `parent`='905', `super`='904', `priority`='3', `class`='text' WHERE `id`='910';
			UPDATE {{attributeclass}} SET `parent`='905', `super`='904', `priority`='5', `class`='text' WHERE `id`='915';
			UPDATE {{attributeclass}} SET `parent`='906', `super`='904', `priority`='1', `class`='headerH1' WHERE `id`='911';
			UPDATE {{attributeclass}} SET `parent`='906', `super`='904', `priority`='2', `class`='text' WHERE `id`='912';
			UPDATE {{attributeclass}} SET `parent`='906', `super`='904', `priority`='4', `class`='headerH2' WHERE `id`='913';
			UPDATE {{attributeclass}} SET `parent`='906', `super`='904', `priority`='3', `class`='text' WHERE `id`='914';
			UPDATE {{attributeclass}} SET `parent`='906', `super`='904', `priority`='5', `class`='text' WHERE `id`='916';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('915', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('916', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('9', '904', '20');


			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`) VALUES ('parties_signature_n10', 'int', '1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n10', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n10', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n10_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n10_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n10_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n10_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n10_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n10_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n10_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n10_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n10_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n10_4', 'string', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='917' WHERE `id`='917';
			UPDATE {{attributeclass}} SET `parent`='917', `super`='917', `priority`='1' WHERE `id`='918';
			UPDATE {{attributeclass}} SET `parent`='917', `super`='917', `priority`='2' WHERE `id`='919';
			UPDATE {{attributeclass}} SET `parent`='918', `super`='917', `priority`='1', `class`='headerH1' WHERE `id`='920';
			UPDATE {{attributeclass}} SET `parent`='918', `super`='917', `priority`='2', `class`='text' WHERE `id`='921';
			UPDATE {{attributeclass}} SET `parent`='918', `super`='917', `priority`='4', `class`='headerH2' WHERE `id`='922';
			UPDATE {{attributeclass}} SET `parent`='918', `super`='917', `priority`='3', `class`='text' WHERE `id`='923';
			UPDATE {{attributeclass}} SET `parent`='918', `super`='917', `priority`='5', `class`='text' WHERE `id`='928';
			UPDATE {{attributeclass}} SET `parent`='919', `super`='917', `priority`='1', `class`='headerH1' WHERE `id`='924';
			UPDATE {{attributeclass}} SET `parent`='919', `super`='917', `priority`='2', `class`='text' WHERE `id`='925';
			UPDATE {{attributeclass}} SET `parent`='919', `super`='917', `priority`='4', `class`='headerH2' WHERE `id`='926';
			UPDATE {{attributeclass}} SET `parent`='919', `super`='917', `priority`='3', `class`='text' WHERE `id`='927';
			UPDATE {{attributeclass}} SET `parent`='919', `super`='917', `priority`='5', `class`='text' WHERE `id`='929';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('928', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('929', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('10', '917', '20');
	    ";
	}
}