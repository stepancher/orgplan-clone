<?php

class m170510_084127_add_new_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('267', 'reutlingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1574', 'ru', 'Ройтлинген');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1574', 'en', 'Reutlingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1574', 'de', 'Reutlingen');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}