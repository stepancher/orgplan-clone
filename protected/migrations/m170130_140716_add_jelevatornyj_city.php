<?php

class m170130_140716_add_jelevatornyj_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('10', 'jelevatornyj');

            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('274', 'ru', 'поселок Элеваторный');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('274', 'en', 'поселок Элеваторный');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('274', 'de', 'поселок Элеваторный');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}