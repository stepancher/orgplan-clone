<?php

class m170220_142206_add_pk_to_exdb_organizercompanyhasassociation extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DELETE FROM {$expodata}.{{exdborganizercompanyhasassociation}};
            
            ALTER TABLE {$expodata}.{{exdborganizercompanyhasassociation}} 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            ADD PRIMARY KEY (`id`);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}