<?php

class m160713_125409_adding_en_de_exhibitioncomplexes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`,`langId`,`name`,`street`,`description`,`services`)
			SELECT id, 'de', name, street, description, services from {{trexhibitioncomplex}} where langId = 'ru';
			
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`,`langId`,`name`,`street`,`description`,`services`)
			SELECT id, 'en', name, street, description, services from {{trexhibitioncomplex}} where langId = 'ru' and name <> 'Крокус Экспо';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}