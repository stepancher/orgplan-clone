<?php

class m170116_114614_delete_help_login_logout_routes_from_route extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{route}} WHERE `id`='93598';
            DELETE FROM {{route}} WHERE `id`='93632';
            DELETE FROM {{route}} WHERE `id`='93650';

            DELETE FROM {{route}} WHERE `id`='93600';
            DELETE FROM {{route}} WHERE `id`='93603';
            DELETE FROM {{route}} WHERE `id`='93607';

            DELETE FROM {{route}} WHERE `id`='93602';
            DELETE FROM {{route}} WHERE `id`='93605';
            DELETE FROM {{route}} WHERE `id`='93651';
            
            DELETE FROM {{route}} WHERE `id`='93654';
            DELETE FROM {{route}} WHERE `id`='93655';
            DELETE FROM {{route}} WHERE `id`='93656';
            DELETE FROM {{route}} WHERE `id`='93657';
            DELETE FROM {{route}} WHERE `id`='93658';
            DELETE FROM {{route}} WHERE `id`='93659';
            
            DELETE FROM {{route}} WHERE `id`='93593';
            DELETE FROM {{route}} WHERE `id`='93599';
            DELETE FROM {{route}} WHERE `id`='93608';
            
            DROP TABLE {{route}};
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}