<?php

class m160429_114333_update_route_title extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {
		return true;
	}

	private function upSql() {

		return "
			UPDATE {{route}} SET `title`='Каталог отраслей | Protoplan' WHERE `id`='93576';
			UPDATE {{route}} SET `title`='Каталог цен на товары и услуги | Protoplan' WHERE `id`='93578';
			UPDATE {{route}} SET `title`='Краткое руководство | Protoplan' WHERE `id`='93580';
			UPDATE {{route}} SET `title`='Вопрос-ответ | Protoplan' WHERE `id`='93587';
			UPDATE {{route}} SET `title`='Каталог отраслей | Protoplan' WHERE `id`='93588';
			UPDATE {{route}} SET `title`='Политика конфиденциальности | Protoplan' WHERE `id`='93589';
			UPDATE {{route}} SET `title`='Блог | Protoplan' WHERE `id`='93591';
			UPDATE {{route}} SET `title`='Выставки и конференции в городах России, СНГ и Мира - PROTOPLAN.PRO' WHERE `id`='93593';
			UPDATE {{route}} SET `title`='Каталог цен на товары и услуги | Protoplan' WHERE `id`='93596';
			UPDATE {{route}} SET `title`='Пользовательское соглашение | Protoplan' WHERE `id`='93598';
			UPDATE {{route}} SET `title`='Protoplan' WHERE `id`='93599';
			UPDATE {{route}} SET `title`='Protoplan | Авторизация' WHERE `id`='93600';
			UPDATE {{route}} SET `title`='Protoplan | Регистрация' WHERE `id`='93601';
			UPDATE {{route}} SET `title`='Protoplan | Авторизация' WHERE `id`='93603';
			UPDATE {{route}} SET `title`='Protoplan | Регистрация' WHERE `id`='93604';
		";
	}
}