<?php

class m170125_071718_add_kaluzhskaya_sp extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('17', 'kaluzhskaya-opytnaya-stanciya');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('272', 'ru', 'cело Калужская опытная сельскохозяйственная станция');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('272', 'en', 'cело Калужская опытная сельскохозяйственная станция');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('272', 'de', 'cело Калужская опытная сельскохозяйственная станция');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}