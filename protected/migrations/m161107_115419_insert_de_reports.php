<?php

class m161107_115419_insert_de_reports extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('1', 'de', 'Kundenbestellungen nach einzelnen Positionen', '[{\"field\":\"attributeValue\",\"displayName\":\"\\\u041a\\\u043e\\\u043b - \\\u0432\\\u043e\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumRUB\",\"displayName\":\"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c RUB\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumUSD\",\"displayName\":\"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c USD\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\"\\\u0412\\\u0430\\\u043b\\\u044e\\\u0442\\\u0430\",\"sortable\":true,\"filterable\":true,\"sortingType\":\"string\"}]', '[{\"rowLabel\":\"Название\",\"dependencyVal\":\"доп . единицы значение\",\"dependencyValUnitTitle\":\"доп . единицы название\",\"unitTitle\":\"ед . \",\"attributeValue\":\"кол . -во\",\"price\":\"цена\",\"currency\":\"валюта\",\"discount\":\"скидка\",\"sumRUB\":\"рубли\",\"sumUSD\":\"доллары\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('2', 'de', 'Bestellungen eines einzelnen Kunden', '[{\"field\":\"attributeValue\",\"displayName\":\"\\\u041a\\\u043e\\\u043b - \\\u0432\\\u043e\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumRUB\",\"displayName\":\"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c RUB\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumUSD\",\"displayName\":\"\\\u0421\\\u0442\\\u043e\\\u0438\\\u043c\\\u043e\\\u0441\\\u0442\\\u044c USD\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\"\\\u0412\\\u0430\\\u043b\\\u044e\\\u0442\\\u0430\",\"sortable\":true,\"filterable\":true,\"sortingType\":\"string\"}]', '[{\"rowLabel\":\"Название\",\"dependencyVal\":\"доп . единицы значение\",\"dependencyValUnitTitle\":\"доп . единицы название\",\"unitTitle\":\"ед . \",\"attributeValue\":\"кол . -во\",\"price\":\"цена\",\"currency\":\"валюта\",\"discount\":\"скидка\",\"sumRUB\":\"рубли\",\"sumUSD\":\"доллары\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('3', 'de', 'eingegangene Anträge nach einzelnen Positionen', '[{\"field\":\"attributeValue\",\"displayName\":\"\\\u041a\\\u043e\\\u043b - \\\u0432\\\u043e\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\" % \\\u0432\\\u044b\\\u043f\\\u043e\\\u043b\\\u043d\\\u0435\\\u043d\\\u0438\\\u044f\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"}]', '[{\"rowLabel\":\"Название\",\"attributeValue\":\"кол . -во\",\"currency\":\" % выполнения\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('4', 'de', 'eingegangene Anträge eines einzelnen Kunden', '[{\"field\":\"status\",\"displayName\":\"\\\u0421\\\u0442\\\u0430\\\u0442\\\u0443\\\u0441\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"}]', '[{\"rowLabel\":\"Название\",\"status\":\"Статус\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('5', 'de', 'Standardstand und Blendenbeschriftung', '[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Тип застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Площадь застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Фризовая надпись\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Размещение логотипа\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Заявка №3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Заявка №4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]', '[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"buildingType\":\"тип застройки\",\"buildingSquare\":\"площадь застройки\",\"letters\":\"Фризовая надпись\",\"logo\":\"Размещение логотипа\",\"thirdProposal\":\"ЗАявка №3\",\"fourthProposal\":\"Заявка №4\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('6', 'de', 'Ausstellerliste ', '[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"bill\",\"displayName\": \"Номер договора\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"phone\",\"displayName\": \"Телефон\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"post\",\"displayName\": \"Должность\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"fullName\",\"displayName\": \"ФИО\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]', '[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"bill\":\"Номер договора\",\"phone\":\"Телефон\",\"post\":\"Должность\",\"fullName\":\"ФИО\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('9', 'de', 'Бэйджы. Заявка №12', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\":true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"companyRu\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', '[{\"firstName\":\"Имя\",\"lastName\":\"Фамилия\",\"countryRu\":\"Страна\",\"companyRu\":\"Компания\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('10', 'de', 'Бэйджы. Заявка №12a', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"regionRu\",\"displayName\":\"Регион\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"street\",\"displayName\":\"Улица\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"house\",\"displayName\":\"Дом\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"index\",\"displayName\":\"Индекс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"phone\",\"displayName\":\"Телефон\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"fax\",\"displayName\":\"Факс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', '[{\n\"firstName\":\"Имя\",\n\"lastName\":\"Фамилия\",\n\"surname\":\"Отчество\",\n\"countryRu\":\"Страна\",\n\"regionRu\":\"Регион\",\n\"cityRu\":\"Город\",\n\"company\":\"Компания\",\n\"post\":\"Должность\",\n\"street\":\"Улица\",\n\"house\":\"Дом\",\n\"index\":\"Индекс\",\n\"phone\":\"Телефон\",\n\"fax\":\"Факс\"\n}]');
            
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('30', 'de', 'Messeordnung', 'Messeordnung', '/static/documents/fairs/10943/rules.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('31', 'de', 'Ausschreibungsbestimmungen', 'Ausschreibungsbestimmungen', '/static/documents/fairs/10943/competition.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('32', 'de', 'Anfahrtsplan', 'Anfahrtsplan', '/static/documents/fairs/10943/scheme.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('33', 'de', 'Anweisungen für Aussteller', 'Anweisungen für Aussteller', '/static/documents/fairs/10943/instruction.pdf');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}