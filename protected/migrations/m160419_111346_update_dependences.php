<?php

class m160419_111346_update_dependences extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			TRUNCATE {{dependences}};

			INSERT INTO {{dependences}} VALUES 
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(5,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/29'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"save\":1}','calendarm/gantt/add/id/20'),
			(7,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/add/id/20'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/20'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/22'),
			(10,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/24'),
			(11,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"save\":1}','calendarm/gantt/add/id/31'),
			(12,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/add/id/31'),
			(13,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/31'),
			(14,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/33'),
			(15,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/35'),
			(16,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"save\":1}','calendarm/gantt/add/id/37'),
			(17,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/add/id/37'),
			(18,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/37'),
			(19,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/39'),
			(20,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/41'),
			(21,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"save\":1}','calendarm/gantt/add/id/43'),
			(22,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/add/id/43'),
			(23,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/43'),
			(24,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/45'),
			(25,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/47'),
			(26,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/49'),
			(27,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/50'),
			(28,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/56'),
			(29,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/49'),
			(30,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/complete/id/49'),
			(31,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/50'),
			(32,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/56'),
			(33,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/49'),
			(34,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/50'),
			(35,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/56'),
			(36,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/49'),
			(37,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/50'),
			(38,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/56'),
			(39,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/50'),
			(40,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/52'),
			(41,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/54'),
			(42,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/49'),
			(43,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/50'),
			(44,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/56'),
			(45,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/49'),
			(46,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/50'),
			(47,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/56'),
			(48,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/56'),
			(49,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/58'),
			(50,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/60'),
			(51,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"save\":1}', 'calendarm/gantt/add/id/62'),
			(52,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"send\":1}', 'calendarm/gantt/add/id/62'),
			(53,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"send\":1}', 'calendarm/gantt/complete/id/62');
		";
	}

	private function downSql() {

		return "
			TRUNCATE {{dependences}};

			INSERT INTO {{dependences}} VALUES
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(5,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/15'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"save\":1}','calendarm/gantt/add/id/20'),
			(7,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/add/id/20'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/20'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/22'),
			(10,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/24');
		";
	}
}