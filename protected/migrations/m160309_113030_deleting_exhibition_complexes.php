<?php

class m160309_113030_deleting_exhibition_complexes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{exhibitioncomplex}} WHERE `id` IN (
				1030, 1031, 1032, 1033, 1034, 1035, 1036,
				1037, 1038, 1039, 1040, 1041, 1042, 1043,
				1044, 1045, 1046, 1047, 1048, 1049, 1050,
				1051, 1052, 1053, 1054, 1055, 1056, 1057,
				1058, 1059, 1060, 1061, 1062, 1063, 1064,
				1065, 1066, 1067, 1068
			);
			DELETE FROM {{trexhibitioncomplex}} WHERE `trParentId` IN (
				1030, 1031, 1032, 1033, 1034, 1035, 1036,
				1037, 1038, 1039, 1040, 1041, 1042, 1043,
				1044, 1045, 1046, 1047, 1048, 1049, 1050,
				1051, 1052, 1053, 1054, 1055, 1056, 1057,
				1058, 1059, 1060, 1061, 1062, 1063, 1064,
				1065, 1066, 1067, 1068
			);
		";
	}
}