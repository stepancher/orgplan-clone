<?php

class m170410_134511_add_many_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1095', 'ismaning');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1549', 'ru', 'Исманинг');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1549', 'en', 'Ismaning');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1549', 'de', 'Ismaning');

            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1092', 'oberboihingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1550', 'ru', 'Обербойхинген');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1550', 'en', 'Oberboihingen');
            INSERT INTO {{trcity}} (`id`, `trParentId`, `langId`, `name`) VALUES ('', '1550', 'de', 'Oberboihingen');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('265', 'kirchensittenbach');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1551', 'ru', 'Кирхензиттенбах');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1551', 'en', 'Kirchensittenbach');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1551', 'de', 'Kirchensittenbach');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}