<?php

class m161121_093533_move_fair_description_to_tr extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trfair}} SET `description`='18-ая международная специализированная выставка технологий, оборудования и инструментов для деревообрабатывающей и мебельной промышленности - ЭКСПОДРЕВ' WHERE `id`='1474';
            UPDATE {{trfair}} SET `description`='Выставка - ART TREND' WHERE `id`='2940';
            UPDATE {{trfair}} SET `description`='13-я Международная выставка оборудования и технологий для водоочистки, переработки и утилизации отходов - WASMA' WHERE `id`='2973';
            UPDATE {{trfair}} SET `description`='20-й фестиваль - МАЙСКИЙ ЭКСТРИМ' WHERE `id`='3727';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='4202';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='4204';
            UPDATE {{trfair}} SET `description`='16-я Международная выставка сварочных материалов, оборудования и технологий - Weldex / Россварка' WHERE `id`='5675';
            UPDATE {{trfair}} SET `description`='13-я Международная выставка оборудования и технологий для водоочистки, переработки и утилизации отходов - WASMA' WHERE `id`='7189';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='8398';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='8400';
            UPDATE {{trfair}} SET `description`='8-я Международная специализированная выставка сырья, оборудования и технологий для производства изделий из пластмасс - Роспласт. Пластмассы. Оборудование. Изделия' WHERE `id`='13049';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='13080';
            UPDATE {{trfair}} SET `description`='13-я Международная выставка оборудования и технологий для водоочистки, переработки и утилизации отходов - WASMA' WHERE `id`='15948';
            UPDATE {{trfair}} SET `description`='20-й фестиваль - МАЙСКИЙ ЭКСТРИМ' WHERE `id`='16679';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='17144';
            UPDATE {{trfair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='17146';
            UPDATE {{trfair}} SET `description`='9-я Международная Выставка Электротехники, Энергетики и Освещения - Central Asia Electrocity World' WHERE `id`='18028';
            UPDATE {{trfair}} SET `description`='9-я Международная выставка - UZMETALMASHEEXPO' WHERE `id`='18095';
            UPDATE {{trfair}} SET `description`='9-я Международная выставка - UZMETALMASHEEXPO' WHERE `id`='19015';
            UPDATE {{trfair}} SET `description`='1-я специализированная выставка \"Медицина\" - MEDEXPO YUG' WHERE `id`='19258';
            UPDATE {{trfair}} SET `description`='Специализированная выставка «Стоматология» - DENTALEXPO' WHERE `id`='19263';
            UPDATE {{trfair}} SET `description`='XV Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='19394';
            UPDATE {{trfair}} SET `description`='XVI  Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='19395';
            UPDATE {{trfair}} SET `description`='11-я Кавказская международная выставка все для отелей, ресторанов и супермаркетов - HOREX CAUCASUS' WHERE `id`='19405';
            UPDATE {{trfair}} SET `description`='1-я специализированная выставка \"Медицина\" - MEDEXPO YUG' WHERE `id`='19497';
            UPDATE {{trfair}} SET `description`='2-я специализированная выставка \"Стоматология\" - DENTALEXPO' WHERE `id`='19502';
            UPDATE {{trfair}} SET `description`='XV Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='19631';
            UPDATE {{trfair}} SET `description`='XVI  Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='19632';
            UPDATE {{trfair}} SET `description`='11-я Кавказская международная выставка все для отелей, ресторанов и супермаркетов - HOREX CAUCASUS' WHERE `id`='19642';
            UPDATE {{trfair}} SET `description`='17-ая международная специализированная выставка технологий, оборудования и инструментов для деревообрабатывающей и мебельной промышленности - ЭКСПОДРЕВ' WHERE `id`='20016';
            UPDATE {{trfair}} SET `description`='19-ая международная специализированная выставка технологий, оборудования и инструментов для деревообрабатывающей и мебельной промышленности - ЭКСПОДРЕВ' WHERE `id`='20017';
            UPDATE {{trfair}} SET `description`='2-я специализированная выставка \"Медицина\" - MEDEXPO YUG' WHERE `id`='21097';
            UPDATE {{trfair}} SET `description`='9-ая Международная выставка Охраны, Безопасности, Противопожарной защиты и Автоматизации - CENTRAL ASIA SECURE WORLD' WHERE `id`='21114';
            UPDATE {{trfair}} SET `description`='XV Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='22547';
            UPDATE {{trfair}} SET `description`='XV Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='22548';
            UPDATE {{trfair}} SET `description`='7-я выставка-ярмарка товаров и услуг для людей  элегантного возраста - ЗОЛОТОЙ ВОЗРАСТ. СЕКРЕТЫ ДОЛГОЛЕТИЯ' WHERE `id`='22734';
            UPDATE {{trfair}} SET `description`='XVI  Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='23466';
            UPDATE {{trfair}} SET `description`='9-ая Международная выставка Охраны, Безопасности, Противопожарной защиты и Автоматизации - CENTRAL ASIA SECURE WORLD' WHERE `id`='23497';
            UPDATE {{trfair}} SET `description`='9-ая Международная выставка Охраны, Безопасности, Противопожарной защиты и Автоматизации - CENTRAL ASIA SECURE WORLD' WHERE `id`='23498';
		
		    UPDATE {{trfair}} SET `description`='XXIV СИБИРСКИЙ СТРОИТЕЛЬНЫЙ ФОРУМ:ТЕМАТИЧЕСКИЕ ВЫСТАВКИ ФОРУМА «Строительство. Строительные материалы и конструкции»,«Малоэтажное строительство»,«Вода. Тепло. Энергия», «Вертикальный транспорт»,«Оборудование и инструмент»,«Отделочные материалы. Деко' WHERE `id`='5773';
            UPDATE {{trfair}} SET `description`='XXV СИБИРСКИЙ СТРОИТЕЛЬНЫЙ ФОРУМ:ТЕМАТИЧЕСКИЕ ВЫСТАВКИ ФОРУМА«Строительство. Строительные материалы и конструкции»,«Малоэтажное строительство»,«Вода. Тепло. Энергия», «Вертикальный транспорт»,«Оборудование и инструмент»,«Отделочные материалы. Декор' WHERE `id`='8153';
            UPDATE {{trfair}} SET `description`='8-я Международная Выставка Сырья и технологий для производства изделий из пластмасс - PLAST WORLD' WHERE `id`='18029';
            UPDATE {{trfair}} SET `description`='8-я Международная Выставка Сырья и технологий для производства изделий из пластмасс - PLAST WORLD' WHERE `id`='18949';
            UPDATE {{trfair}} SET `description`='8-я Международная Выставка Сырья и технологий для производства изделий из пластмасс - PLAST WORLD' WHERE `id`='22322';
            UPDATE {{trfair}} SET `description`='8-я Международная Выставка Сырья и технологий для производства изделий из пластмасс - PLAST WORLD' WHERE `id`='22324';

            UPDATE {{fair}} SET `description`='20-й фестиваль - МАЙСКИЙ ЭКСТРИМ' WHERE `id`='1301';
            UPDATE {{fair}} SET `description`='18-ая международная специализированная выставка технологий, оборудования и инструментов для деревообрабатывающей и мебельной промышленности - ЭКСПОДРЕВ' WHERE `id`='3581';
            UPDATE {{fair}} SET `description`='13-я Международная выставка оборудования и технологий для водоочистки, переработки и утилизации отходов - WASMA' WHERE `id`='3645';
            UPDATE {{fair}} SET `description`='Выставка - ART TREND' WHERE `id`='3782';
            UPDATE {{fair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='10459';
            UPDATE {{fair}} SET `description`='Выставка предметов интерьера и декора - INDECOR Krasnodar' WHERE `id`='10461';
            UPDATE {{fair}} SET `description`='8-я Международная специализированная выставка сырья, оборудования и технологий для производства изделий из пластмасс - Роспласт. Пластмассы. Оборудование. Изделия' WHERE `id`='11096';
            UPDATE {{fair}} SET `description`='9-я Международная Выставка Электротехники, Энергетики и Освещения - Central Asia Electrocity World' WHERE `id`='11357';
            UPDATE {{fair}} SET `description`='8-я Международная Выставка Сырья и технологий для производства изделий из пластмасс - PLAST WORLD' WHERE `id`='11358';
            UPDATE {{fair}} SET `description`='9-я Международная выставка - UZMETALMASHEEXPO' WHERE `id`='11424';
            UPDATE {{fair}} SET `description`='1-я специализированная выставка \"Медицина\" - MEDEXPO YUG' WHERE `id`='11667';
            UPDATE {{fair}} SET `description`='Специализированная выставка «Стоматология» - DENTALEXPO' WHERE `id`='11672';
            UPDATE {{fair}} SET `description`='XV Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='11802';
            UPDATE {{fair}} SET `description`='XVI  Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='11803';
            UPDATE {{fair}} SET `description`='11-я Кавказская международная выставка все для отелей, ресторанов и супермаркетов - HOREX CAUCASUS' WHERE `id`='11813';
            UPDATE {{fair}} SET `description`='17-ая международная специализированная выставка технологий, оборудования и инструментов для деревообрабатывающей и мебельной промышленности - ЭКСПОДРЕВ' WHERE `id`='12187';
            UPDATE {{fair}} SET `description`='19-ая международная специализированная выставка технологий, оборудования и инструментов для деревообрабатывающей и мебельной промышленности - ЭКСПОДРЕВ' WHERE `id`='12188';
            UPDATE {{fair}} SET `description`='2-я специализированная выставка \"Медицина\" - MEDEXPO YUG' WHERE `id`='12676';
            UPDATE {{fair}} SET `description`='9-ая Международная выставка Охраны, Безопасности, Противопожарной защиты и Автоматизации - CENTRAL ASIA SECURE WORLD' WHERE `id`='12693';
            UPDATE {{fair}} SET `description`='8-я Международная Выставка Сырья и технологий для производства изделий из пластмасс - PLAST WORLD' WHERE `id`='13119';
            UPDATE {{fair}} SET `description`='XV Казахстанско-Российская международная выставка в г. Актобе - ЕВРОПА - АЗИЯ. СОТРУДНИЧЕСТВО БЕЗ ГРАНИЦ' WHERE `id`='13194';
            UPDATE {{fair}} SET `description`='7-я выставка-ярмарка товаров и услуг для людей  элегантного возраста - ЗОЛОТОЙ ВОЗРАСТ. СЕКРЕТЫ ДОЛГОЛЕТИЯ' WHERE `id`='13303';

            ALTER TABLE {{trfair}} 
            CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL ;

            UPDATE {{trfair}} SET `description` = '3-я международная специализированная выставка оборудования, принадлежностей, инструментов и аксессуаров для приготовления барбекю, гриля, шашлыка - БАРБЕКЮ ЭКСПО 2015 (в рамках Фестиваля Загородная жизни, который объединяет выставки: Барбекю ЭКСПО, GardenTools,GardenComfort)' WHERE `id` = '723';
            UPDATE {{trfair}} SET `description` = 'V Петербургский Международный Газовый Форум (В рамках ПМГФ-2015 пройдут: IV международный конгресс специалистов нефтегазовой индустрии, III Международная специализированная выставка «INGAS STREAM 2015 – ИННОВАЦИИ В ГАЗОВОЙ ОТРАСЛИ», II Международная специализированная выставка «Газомоторное топливо\", XIX международная специализированная выставка газовой промышленности и технических средств для газового хозяйства «РОС-ГАЗ-ЭКСПО 2015»)' WHERE `id` = '519';
            UPDATE {{trfair}} SET `description` = '17-я Международная выставка - PHARMTECH &amp; INGREDIENTS' WHERE `id` = '245';
            UPDATE {{trfair}} SET `description` = 'Выставочно-конгрессное мероприятие -Петербургская техническая ярмарка (выставка в рамках ПТЯ - «Металлургия. Литейное дело», «Обработка металлов», «Компрессоры. Насосы. Арматура. Приводы», «Машиностроение», «Высокие технологии. Инновации. Инвестиции (Hi-Tech)», «Неметаллические материалы для промышленности», «Услуги для промышленных предприятий»)' WHERE `id` = '521';
            UPDATE {{trfair}} SET `description` = 'Выставочно-конгрессное мероприятие -Петербургская техническая ярмарка (выставка в рамках ПТЯ - «Металлургия. Литейное дело», «Обработка металлов», «Компрессоры. Насосы. Арматура. Приводы», «Машиностроение», «Высокие технологии. Инновации. Инвестиции (Hi-Tech)», «Неметаллические материалы для промышленности», «Услуги для промышленных предприятий»)' WHERE `id` = '522';
            UPDATE {{trfair}} SET `description` = '8-ая международная специализированная выставка оборудования, техники, фармпрепаратов для диагностики заболеваний человека - МЕДИАГНОСТИКА - 2016 (в рамках VIII ВСЕРОССИЙСКОГО НАУЧНО-ОБРАЗОВАТЕЛЬНОГО ФОРУМА С МЕЖДУНАРОДНЫМ УЧАСТИЕМ -МЕДИЦИНСКАЯ ДИАГНОСТИКА 2016)' WHERE `id` = '584';
            UPDATE {{trfair}} SET `description` = '4-я международная специализированная выставка оборудования, принадлежностей, инструментов и аксессуаров для приготовления барбекю, гриля, шашлыка - БАРБЕКЮ ЭКСПО 2015 (в рамках Фестиваля Загородная жизни, который объединяет выставки: Барбекю ЭКСПО, GardenTools,GardenComfort)' WHERE `id` = '1622';
            UPDATE {{trfair}} SET `description` = 'XIV Cпециализированный проект для индустрии чистоты: поставщиков и потребителей уборочной техники и инвентаря, санитарно-гигиенического оборудования и расходных материалов, профессиональной и бытовой химии, оборудования и аксессуаров для химчисток и прачечных' WHERE `id` = '18196';
            UPDATE {{trfair}} SET `description` = 'International specialized exhibition of agricultural machinery AGROSALON 2018' WHERE `id` = '20065';
            UPDATE {{trfair}} SET `description` = 'Международный форум в сфере фармацевтики и биотехнологий и выставка фармацевтических ингредиентов, производства и дистрибуции лекарственных средств - IPhEB &amp; CPhI Russia' WHERE `id` = '20476';
            UPDATE {{trfair}} SET `description` = '7-ая международная специализированная выставка оборудования, техники, фармпрепаратов для диагностики заболеваний человека - МЕДИАГНОСТИКА - 2015 (в рамках VII ВСЕРОССИЙСКОГО НАУЧНО-ОБРАЗОВАТЕЛЬНОГО ФОРУМА С МЕЖДУНАРОДНЫМ УЧАСТИЕМ -МЕДИЦИНСКАЯ ДИАГНОСТИКА 2015)' WHERE `id` = '22908';
		
            ALTER TABLE {{fair}} 
            DROP COLUMN `description`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}