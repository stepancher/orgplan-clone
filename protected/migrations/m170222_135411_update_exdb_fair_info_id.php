<?php

class m170222_135411_update_exdb_fair_info_id extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`update_exdb_fair_info_id`;
            
            CREATE PROCEDURE {$expodata}.`update_exdb_fair_info_id`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE s_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT f.id AS fairId, fs.id AS statisticId 
                                        FROM {$expodata}.{{exdbfair}} f
                                        LEFT JOIN {$expodata}.{{exdbfairstatistic}} fs ON fs.exdbId = f.exdbId
                                        WHERE f.infoId IS NULL ORDER BY fairId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, s_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {$expodata}.{{exdbfair}} SET `infoId` = s_id WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`update_exdb_fair_info_id`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}