<?php

class m170221_062249_add_exdb_association_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_association_to_exdb_db`;
            
            CREATE PROCEDURE {$expodata}.`add_exdb_association_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE ass_name TEXT DEFAULT '';
                DECLARE ass_contact TEXT DEFAULT '';
                DECLARE ass_contact_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ass.id, ass.name_en, ass.contacts_en, ass.contacts_raw_en FROM {$expodataRaw}.expo_assoc ass
                                            LEFT JOIN {$expodata}.{{exdbassociation}} eass ON eass.exdbId = ass.id
                                        WHERE eass.id IS NULL AND ass.id != 0 ORDER BY ass.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO ass_id, ass_name, ass_contact, ass_contact_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbassociation}} (`name`,`exdbId`,`exdbContacts`,`exdbContactsRaw`) VALUES (ass_name, ass_id, ass_contact, ass_contact_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_association_to_exdb_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}