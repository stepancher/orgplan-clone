<?php

class m160426_150058_add_headers_to_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('1044', 'n9_header', 'int', '0', '0', '', '762', '762', '20', 'headerH1Bold', '0');
			UPDATE {{attributeclass}} SET `label`='РАЗГРУЗКА', `parent`='577', `super`='577', `priority`='0' WHERE `id`='1044';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('1045', 'n9_header2', 'int', '0', '0', 'ПОГРУЗКА', '577', '577', '0', 'headerH1Bold', '0');
			INSERT INTO {{attributeclass}} (`id`) VALUES ('2000');
			DELETE FROM {{attributeclass}} WHERE `id`='2000';
			UPDATE {{attributeclass}} SET `collapse`='-1' WHERE `id`='1044';
			UPDATE {{attributeclass}} SET `parent`='616', `super`='616', `collapse`='-1' WHERE `id`='1045';
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('10', '1044', '0', '0', '0.5');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('10', '1045', '0', '0', '-1.5');
			UPDATE {{attributemodel}} SET `priority`='2' WHERE `id`='70';
			UPDATE {{attributemodel}} SET `priority`='3' WHERE `id`='71';
			UPDATE {{attributemodel}} SET `priority`='4' WHERE `id`='72';
			UPDATE {{attributemodel}} SET `priority`='6' WHERE `id`='79';
			UPDATE {{attributemodel}} SET `priority`='7' WHERE `id`='80';
			UPDATE {{attributemodel}} SET `priority`='8' WHERE `id`='84';
			UPDATE {{attributemodel}} SET `priority`='-2' WHERE `id`='97';
			UPDATE {{attributemodel}} SET `priority`='1' WHERE `id`='97';
			UPDATE {{attributemodel}} SET `priority`='1' WHERE `id`='98';
			UPDATE {{attributemodel}} SET `priority`='20' WHERE `id`='98';
			ALTER TABLE {{attributemodel}} 
			CHANGE COLUMN `priority` `priority` FLOAT NULL DEFAULT '0' ;
			UPDATE {{attributemodel}} SET `priority`='5.5' WHERE `id`='79';
			UPDATE {{attributemodel}} SET `priority`='5.6' WHERE `id`='80';
			UPDATE {{attributemodel}} SET `priority`='5.4' WHERE `id`='98';
	    ";
	}
}