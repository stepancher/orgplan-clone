<?php

class m160416_171728_rename_attrbuteClassProperty__from_priceUnit_to_currency extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclassproperty}} SET `class`='currency' WHERE `id`='75' AND `attributeClass` = '318';
			UPDATE {{attributeclass}} SET `class`='multiplierElement' WHERE `id`='325';
			UPDATE {{attributeclass}} SET `class`='multiplierElement' WHERE `id`='326';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='671';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='672';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='680';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='681';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='689';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='690';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='698';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='699';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='707';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='708';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='716';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='717';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='725';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='726';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='734';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='735';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='743';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='744';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='752';
			UPDATE {{attributeclass}} SET `dataType`='datetime' WHERE `id`='753';
	    ";
	}
}