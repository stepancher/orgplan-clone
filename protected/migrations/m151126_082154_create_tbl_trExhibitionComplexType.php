<?php

class m151126_082154_create_tbl_trExhibitionComplexType extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{trexhibitioncomplextype}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS {{trexhibitioncomplextype}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`trParentId` int(11),
				`langId` varchar(255),
				`name` varchar(255),
				`fullNameSingle` varchar(255)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;

			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `fullNameSingle`) VALUES ("1", "de", "Geschäftszentren, Einkaufs- und Unterhaltungszentren", "Geschäftszentrum, Einkaufs- und Unterhaltungszentrum");
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `fullNameSingle`) VALUES ("2", "de", "Sportzentren, Wissenschaftszentren, Kulturzentren", "Sportzentrum, Wissenschaftszentrum, Kulturzentrum");
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `fullNameSingle`) VALUES ("3", "de", "Hotels, Kongresshallen", "Hotel, Kongresshalle");
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `fullNameSingle`) VALUES ("4", "de", "Messezentren", "Messezentrum");
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `fullNameSingle`) VALUES ("5", "de", "Theater, Museen, Galerien", "Theater, Museum, Galerie");
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `fullNameSingle`) VALUES ("6", "de", "Spezialisierte Flächen", "Spezialisierte Fläche");

		';
	}
}