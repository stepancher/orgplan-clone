<?php

class m160418_141032_add_new_proposal_class_footer extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "

		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`) VALUES 	('parties_signature_n1', 				'int', 		'1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES 				('parties_signature_left_block_n1', 	'int', 		'1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES 				('parties_signature_right_block_n1', 	'int', 		'1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_left_n1_1', 	'string', 	'1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_left_n1_2', 	'string', 	'1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES 				('parties_signature_row_left_n1_2.5', 	'string', 	'1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_left_n1_3', 	'string', 	'1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_right_n1_1', 	'string', 	'1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_right_n1_2', 	'string', 	'1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_right_n1_2.5', 	'string', 	'1', NULL);
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_right_n1_3', 	'string', 	'1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_left_n1_4', 	'string', 	'1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES 		('parties_signature_row_right_n1_4', 	'string', 	'1', 'подпись');


			UPDATE {{attributeclass}} SET `super`='800',  `class`='partiesSignature' 									WHERE `id`='800';
			UPDATE {{attributeclass}} SET `parent`='800', `super`='800', 			`priority`='1' 						WHERE `id`='801';
			UPDATE {{attributeclass}} SET `parent`='800', `super`='800', 			`priority`='2' 						WHERE `id`='802';
			UPDATE {{attributeclass}} SET `parent`='801', `super`='800', 			`priority`='1', `class`='headerH1' 	WHERE `id`='803';
			UPDATE {{attributeclass}} SET `parent`='801', `super`='800', 			`priority`='2', `class`='text' 		WHERE `id`='804';
			UPDATE {{attributeclass}} SET `parent`='801', `super`='800', 			`priority`='4', `class`='headerH2' 	WHERE `id`='805';
			UPDATE {{attributeclass}} SET `parent`='801', `super`='800', 			`priority`='3', `class`='text' 		WHERE `id`='806';
			UPDATE {{attributeclass}} SET `parent`='802', `super`='800', 			`priority`='1', `class`='headerH1' 	WHERE `id`='807';
			UPDATE {{attributeclass}} SET `parent`='802', `super`='800', 			`priority`='2', `class`='text' 		WHERE `id`='808';
			UPDATE {{attributeclass}} SET `parent`='802', `super`='800', 			`priority`='4', `class`='headerH2' 	WHERE `id`='809';
			UPDATE {{attributeclass}} SET `parent`='802', `super`='800', 			`priority`='3', `class`='text' 		WHERE `id`='810';
			UPDATE {{attributeclass}} SET `parent`='801', `super`='800', 			`priority`='5', `class`='text' 		WHERE `id`='811';
			UPDATE {{attributeclass}} SET `parent`='802', `super`='800', 			`priority`='5', `class`='text' 		WHERE `id`='812';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('811', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('812', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('1', '800', '20');


			INSERT INTO {{attributeclass}} (`name`, `isArray`, `isMulty`) VALUES ('parties_signature_n2', '1', '0');
			INSERT INTO {{attributeclass}} (`name`, `isArray`) VALUES ('parties_signature_left_block_n2', '1');
			INSERT INTO {{attributeclass}} (`name`, `isArray`) VALUES ('parties_signature_right_block_n2', '1');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_left_n2_1', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_left_n2_2', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `isArray`) VALUES ('parties_signature_row_left_n2_2.5', '1');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_left_n2_3', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_right_n2_1', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_right_n2_2', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `isArray`) VALUES ('parties_signature_row_right_n2_2.5', '1');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_right_n2_3', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_left_n2_4', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `isArray`, `label`) VALUES ('parties_signature_row_right_n2_4', '1', 'подпись');

			UPDATE {{attributeclass}} SET `super`='813', `class`='partiesSignature' WHERE `id`='813';
			UPDATE {{attributeclass}} SET `parent`='813', `super`='813', `priority`='1' WHERE `id`='814';
			UPDATE {{attributeclass}} SET `parent`='813', `super`='813', `priority`='2' WHERE `id`='815';
			UPDATE {{attributeclass}} SET `parent`='814', `super`='813', `priority`='1', `class`='headerH1' WHERE `id`='816';
			UPDATE {{attributeclass}} SET `parent`='814', `super`='813', `priority`='2', `class`='text' WHERE `id`='817';
			UPDATE {{attributeclass}} SET `parent`='814', `super`='813', `priority`='4', `class`='headerH2' WHERE `id`='818';
			UPDATE {{attributeclass}} SET `parent`='814', `super`='813', `priority`='3', `class`='text' WHERE `id`='819';
			UPDATE {{attributeclass}} SET `parent`='815', `super`='813', `priority`='1', `class`='headerH1' WHERE `id`='820';
			UPDATE {{attributeclass}} SET `parent`='815', `super`='813', `priority`='2', `class`='text' WHERE `id`='821';
			UPDATE {{attributeclass}} SET `parent`='815', `super`='813', `priority`='4', `class`='headerH2' WHERE `id`='822';
			UPDATE {{attributeclass}} SET `parent`='815', `super`='813', `priority`='3', `class`='text' WHERE `id`='823';
			UPDATE {{attributeclass}} SET `parent`='814', `super`='813', `priority`='5', `class`='text' WHERE `id`='824';
			UPDATE {{attributeclass}} SET `parent`='815', `super`='813', `priority`='5', `class`='text' WHERE `id`='825';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('824', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('825', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `priority`) VALUES ('2', '813', '20');


			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_n3', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_left_block_n3', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_right_block_n3', 'int', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n3_1', 'string', '1', 'Экспонент');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n3_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_left_n3_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n3_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n3_1', 'string', '1', 'Организатор');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n3_2', 'string', '1', 'должность');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`) VALUES ('parties_signature_row_right_n3_2.5', 'string', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n3_3', 'string', '1', 'ФИО');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_left_n3_4', 'string', '1', 'подпись');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`) VALUES ('parties_signature_row_right_n3_4', 'string', '1', 'подпись');
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='816';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='817';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='818';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='819';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='820';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='821';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='822';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='823';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='824';
			UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='825';

			UPDATE {{attributeclass}} SET `super`='826', `class`='partiesSignature' WHERE `id`='826';
			UPDATE {{attributeclass}} SET `parent`='826', `super`='826', `priority`='1' WHERE `id`='827';
			UPDATE {{attributeclass}} SET `parent`='826', `super`='826', `priority`='2' WHERE `id`='828';
			UPDATE {{attributeclass}} SET `parent`='827', `super`='826', `priority`='1', `class`='headerH1' WHERE `id`='829';
			UPDATE {{attributeclass}} SET `parent`='827', `super`='826', `priority`='2', `class`='text' WHERE `id`='830';
			UPDATE {{attributeclass}} SET `parent`='827', `super`='826', `priority`='4', `class`='headerH2' WHERE `id`='831';
			UPDATE {{attributeclass}} SET `parent`='827', `super`='826', `priority`='3', `class`='text' WHERE `id`='832';
			UPDATE {{attributeclass}} SET `parent`='828', `super`='826', `priority`='1', `class`='headerH1' WHERE `id`='833';
			UPDATE {{attributeclass}} SET `parent`='828', `super`='826', `priority`='2', `class`='text' WHERE `id`='834';
			UPDATE {{attributeclass}} SET `parent`='828', `super`='826', `priority`='4', `class`='headerH2' WHERE `id`='835';
			UPDATE {{attributeclass}} SET `parent`='828', `super`='826', `priority`='3', `class`='text' WHERE `id`='836';
			UPDATE {{attributeclass}} SET `parent`='827', `super`='826', `priority`='5', `class`='text' WHERE `id`='837';
			UPDATE {{attributeclass}} SET `parent`='828', `super`='826', `priority`='5', `class`='text' WHERE `id`='838';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('837', 'дата', 'additionalLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('838', 'дата', 'additionalLabel');

			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('3', '826', '0', '0', '20');
	    ";
	}
}