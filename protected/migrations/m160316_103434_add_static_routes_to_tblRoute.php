<?php

class m160316_103434_add_static_routes_to_tblRoute extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
			DELETE FROM {{route}} where
			`url` =  '/ru/help/quickGuide' AND
			`route` = 'help/quickGuide' AND
			`params` = '[]';

			DELETE FROM {{route}} where
			`url` =  '/ru/fairs/city' AND
			`route` = 'search/index' AND
			`params` = '{\"by\":\"byCity\",\"sector\":\"fair\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/regions/alphabet' AND
			`route` = 'search/index' AND
			`params` = '{\"by\":\"byAlphabet\",\"sector\":\"region\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/venues/city' AND
			`route` = 'search/index' AND
			`params` = '{\"by\":\"byCity\",\"sector\":\"exhibitionComplex\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/industries/alphabet' AND
			`route` = 'search/index' AND
			`params` = '{\"by\":\"byAlphabet\",\"sector\":\"industry\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/fairs/date' AND
			`route` = 'search/index' AND
			`params` = '{\"by\":\"byDate\",\"sector\":\"fair\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/venues/region' AND
			`route` = 'search/index' AND
			`params` = '{\"by\":\"byRegion\",\"sector\":\"exhibitionComplex\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/help/questionAnswer' AND
			`route` = 'help/questionAnswer' AND
			`params` =  '[]';

			DELETE FROM {{route}} where
			`url` =  '/ru/industries' AND
			`route` = 'search/index' AND
			`params` =  '{\"sector\":\"industry\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/help/confidentiality' AND
			`route` = 'help/confidentiality' AND
			`params` =  '[]';

			DELETE FROM {{route}} where
			`url` =  '/ru/fairs/industry' AND
			`route` = 'search/index' AND
			`params` =  '{\"by\":\"byIndustry\",\"sector\":\"fair\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/blog' AND
			`route` = 'blog/index' AND
			`params` =  '[]';

			DELETE FROM {{route}} where
			`url` =  '/ru/fairs/region' AND
			`route` = 'search/index' AND
			`params` =  '{\"by\":\"byRegion\",\"sector\":\"fair\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru' AND
			`route` = 'site/index' AND
			`params` =  '[]';

			DELETE FROM {{route}} where
			`url` =  '/ru/venues/category' AND
			`route` = 'search/index' AND
			`params` =  '{\"by\":\"byCategory\",\"sector\":\"exhibitionComplex\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/regions' AND
			`route` = 'search/index' AND
			`params` =  '{\"sector\":\"region\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/prices' AND
			`route` = 'search/index' AND
			`params` =  '{\"sector\":\"price\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/venues' AND
			`route` = 'search/index' AND
			`params` =  '{\"sector\":\"exhibitionComplex\"}';

			DELETE FROM {{route}} where
			`url` =  '/ru/help/termsOfUse' AND
			`route` = 'help/termsOfUse' AND
			`params` =  '[]';
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Краткое руководство | Orgplan', 'С помощью поиска ORGPLAN вы можете найти любую выставку на территории России. Архив с января 2015 года.', '/ru/help/quickGuide', 'help/quickGuide', '[]', 'Краткое руководство | Orgplan');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Выставки по городам Российской Федерации', 'Выставки по городам Российской Федерации', '/ru/fairs/city', 'search/index', '{\"by\":\"byCity\",\"sector\":\"fair\"}', 'Выставки по городам Российской Федерации');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог регионов по алфавиту', 'Каталог регионов по алфавиту', '/ru/regions/alphabet', 'search/index', '{\"by\":\"byAlphabet\",\"sector\":\"region\"}', 'Каталог регионов по алфавиту');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог по городам Российской Федерации', 'Каталог по городам Российской Федерации', '/ru/venues/city', 'search/index', '{\"by\":\"byCity\",\"sector\":\"exhibitionComplex\"}', 'Каталог по городам Российской Федерации');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог отраслей по алфавиту', 'Каталог отраслей по алфавиту', '/ru/industries/alphabet', 'search/index', '{\"by\":\"byAlphabet\",\"sector\":\"industry\"}', 'Каталог отраслей по алфавиту');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог Выставок по дате', 'Каталог Выставок по дате', '/ru/fairs/date', 'search/index', '{\"by\":\"byDate\",\"sector\":\"fair\"}', 'Каталог Выставок по дате');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог по регионам Российской Федерации', 'Каталог по регионам Российской Федерации', '/ru/venues/region', 'search/index', '{\"by\":\"byRegion\",\"sector\":\"exhibitionComplex\"}', 'Каталог по регионам Российской Федерации');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Вопрос-ответ | Orgplan', 'Могу ли я доверять статистике ORGPLAN? Все статистические данные взяты из официальных источников, ссылка на источник публикуется на странице, вы можете проверить.', '/ru/help/questionAnswer', 'help/questionAnswer', '[]', 'Вопрос-ответ | Orgplan');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог отраслей | Orgplan', 'В каталоге представлены 9 отраслей промышленности России. В каждой отрасли содержится краткий обзор с основными показателями развития.', '/ru/industries', 'search/index', '{\"sector\":\"industry\"}', 'Каталог отраслей | Orgplan');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Политика конфиденциальности | Orgplan', 'Настоящий документ «Политика конфиденциальности» (далее — по тексту — «Политика») представляет собой правила использования персональной информации Пользователя.', '/ru/help/confidentiality', 'help/confidentiality', '[]', 'Политика конфиденциальности | Orgplan');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Выставки по отраслям', 'Выставки по отраслям', '/ru/fairs/industry', 'search/index', '{\"by\":\"byIndustry\",\"sector\":\"fair\"}', 'Выставки по отраслям');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `header`) VALUES
			('Блог | Orgplan', 'Как участвовать в выставках, как пригласить клиентов, планирование работы персонала,  идеи для выставочных стендов, другие темы от экспертов блога orgplan.ru', '/ru/blog', 'blog/index', 'Блог | Orgplan');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Выставки по регионам Российской Федерации', 'Выставки по регионам Российской Федерации', '/ru/fairs/region', 'search/index', '{\"by\":\"byRegion\",\"sector\":\"fair\"}', 'Выставки по регионам Российской Федерации');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Выставки и конференции в городах России, СНГ и Мира - ORGPLAN.PRO', 'Специализированный сервис, помогающий организовать и эффективно провести выставку или конференцию в России или мире. Полезные советы по организации выставочного мероприятия.', '/ru', 'site/index', '[]', 'Выставки и конференции в городах России, СНГ и Мира - ORGPLAN.PRO');

			UPDATE {{route}} SET `params`='[]' WHERE `url` = '/ru/blog' AND `route` = 'blog/index';

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог площадок по категориям', 'На странице представлен каталог площадок Российской Федерации по категориям.', '/ru/venues/category', 'search/index', '{\"by\":\"byCategory\",\"sector\":\"exhibitionComplex\"}', 'Каталог площадок по категориям');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Регионы России', 'В каталоге представлены регионы России, в каждом опубликованы экономические показатели развития, KPI отраслей и рейтинг инвестиционной привлекательности', '/ru/regions', 'search/index', '{\"sector\":\"region\"}', 'Регионы России');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Каталог цен на товары и услуги | Orgplan', 'Каталог цен по городам России', '/ru/prices', 'search/index', '{\"sector\":\"price\"}', 'Каталог цен на товары и услуги | Orgplan');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Площадки России, СНГ, мира', 'В каталоге представлены площадки России, СНГ, мира. Воспользовавшись фильтром, можно найти площадку по региону и городу', '/ru/venues', 'search/index', '{\"sector\":\"exhibitionComplex\"}', 'Площадки России, СНГ, мира');

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES
			('Пользовательское соглашение | Orgplan', 'Настоящий документ «Пользовательское соглашение» представляет собой предложение ООО «Кубэкс Рус» (далее — «Правообладатель») заключить договор на изложенных ниже условиях.', '/ru/help/termsOfUse', 'help/termsOfUse', '[]', 'Пользовательское соглашение | Orgplan');
		";
	}
}