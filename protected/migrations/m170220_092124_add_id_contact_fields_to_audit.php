<?php

class m170220_092124_add_id_contact_fields_to_audit extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{audit}} 
            ADD COLUMN `exdbId` INT(11) NULL DEFAULT NULL AFTER `id`,
            ADD COLUMN `contacts_en` TEXT NULL DEFAULT NULL AFTER `active`;
            
            DELETE FROM {{audit}};
            DELETE FROM {{traudit}};
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}