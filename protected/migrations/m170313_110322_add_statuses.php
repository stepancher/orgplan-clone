<?php

class m170313_110322_add_statuses extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{fairstatus}} 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

            INSERT INTO {{fairstatus}} (`status`) VALUES ('менеджер 5 - нет цены/статистики');
            INSERT INTO {{fairstatus}} (`status`) VALUES ('пустые');
            
            CREATE TABLE {{organizercompanystatus}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `color` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            ALTER TABLE {{organizercompany}} 
            ADD COLUMN `statusId` INT(11) NULL DEFAULT NULL AFTER `active`;
            
            INSERT INTO {{organizercompanystatus}} (`name`) VALUES ('статус отсутствует');
            INSERT INTO {{organizercompanystatus}} (`name`) VALUES ('в работу');
            INSERT INTO {{organizercompanystatus}} (`name`) VALUES ('на перевод');
            INSERT INTO {{organizercompanystatus}} (`name`) VALUES ('переведено');
            
            INSERT INTO {{exhibitioncomplexstatus}} (`name`) VALUES ('переведено');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}