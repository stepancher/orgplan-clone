<?php

class m161130_104206_udapte_trreports extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
           
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Standnummer\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Firmenname\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Standfläche\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"bill\",\"displayName\": \"Vertragsnummer\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"phone\",\"displayName\": \"Telefon\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"post\",\"displayName\": \"Position\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"fullName\",\"displayName\": \"Name, Vorname\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]' WHERE `id`='22';
            
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\":\"firstName\",\"displayName\":\"Name\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"lastName\",\"displayName\":\"Surname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"surname\",\"displayName\":\"Middle name\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Country\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"regionRu\",\"displayName\":\"Region\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"cityRu\",\"displayName\":\"City\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"company\",\"displayName\":\"Company\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"post\",\"displayName\":\"Position\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"street\",\"displayName\":\"Street\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"house\",\"displayName\":\"House\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"index\",\"displayName\":\"ZIP code\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"phone\",\"displayName\":\"Telephone\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"fax\",\"displayName\":\"Fax\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', `exportColumns`='[{\n\"firstName\":\"Name\",\n\"lastName\":\"Surname\",\n\"surname\":\"Middle name\",\n\"countryRu\":\"Country\",\n\"regionRu\":\"Region\",\n\"cityRu\":\"City\",\n\"company\":\"Company\",\n\"post\":\"Position\",\n\"street\":\"Street\",\n\"house\":\"House\",\n\"index\":\"ZIP code\",\n\"phone\":\"Telephone\",\n\"fax\":\"Fax\"\n}]' WHERE `id`='16';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\":\"firstName\",\"displayName\":\"Name\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\":true},\n{\"field\":\"lastName\",\"displayName\":\"Surname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Country\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"companyRu\",\"displayName\":\"Company\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', `exportColumns`='[{\"firstName\":\"Name\",\"lastName\":\"Surname\",\"countryRu\":\"Country\",\"companyRu\":\"Company\"}]' WHERE `id`='15';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Stand#\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, \n{\"field\": \"companyName\",\"displayName\": \"Company name\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, \n{\"field\": \"standSquare\",\"displayName\": \"Stand size (in square meters)\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, \n{\"field\": \"bill\",\"displayName\": \"Contract#\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, \n{\"field\": \"phone\",\"displayName\": \"Telephone\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, \n{\"field\": \"post\",\"displayName\": \"Position\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, \n{\"field\": \"fullName\",\"displayName\": \"Full name\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]', `exportColumns`='[{\"rowLabel\":\"Exhibitor\",\"standNumber\":\"Stand#\",\"companyName\":\"Company name\",\"standSquare\":\"Stand size (in square meters)\",\"bill\":\"Contract#\",\"phone\":\"Telephone\",\"post\":\"Position\",\"fullName\":\"Full name\"}]' WHERE `id`='12';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Stand#\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"},\n {\"field\": \"companyName\",\"displayName\": \"Company name\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, \n{\"field\": \"standSquare\",\"displayName\": \"Stand size (in square meters)\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"},\n {\"field\": \"buildingType\",\"displayName\": \"Standard stand type\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"},\n {\"field\": \"buildingSquare\",\"displayName\": \"Space\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"},\n {\"field\": \"letters\",\"displayName\": \"Fascia name\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, \n{\"field\": \"logo\",\"displayName\": \"Logo on the fascia panel\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, \n{\"field\": \"thirdProposal\",\"displayName\": \"Application No.3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, \n{\"field\": \"fourthProposal\",\"displayName\": \"Application No.4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]', `exportColumns`='[{\"rowLabel\":\"Exhibitor\",\"standNumber\":\"Stand#\",\"companyName\":\"Company name\",\"standSquare\":\"Stand size (in square meters)\",\"buildingType\":\"Standard stand type\",\"buildingSquare\":\"Space\",\"letters\":\"Fascia name\",\"logo\":\"Logo on the fascia panel\",\"thirdProposal\":\"Application No.3\",\"fourthProposal\":\"Application No.4\"}]' WHERE `id`='11';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\":\"status\",\"displayName\":\"Status\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"}]', `exportColumns`='[{\"rowLabel\":\"Name\",\"status\":\"Status\"}]' WHERE `id`='10';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\":\"attributeValue\",\"displayName\":\"Amount\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\" Percentage completion\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"}]', `exportColumns`='[{\"rowLabel\":\"Name\",\"attributeValue\":\"Amount\",\"currency\":\" Percentage completion\"}]' WHERE `id`='9';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\":\"attributeValue\",\"displayName\":\"Amount\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},\n{\"field\":\"sumRUB\",\"displayName\":\"Price RUB\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},\n{\"field\":\"sumUSD\",\"displayName\":\"Price USD\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},\n{\"field\":\"currency\",\"displayName\":\"Currency\",\"sortable\":true,\"filterable\":true,\"sortingType\":\"string\"}]', `exportColumns`='[{\"rowLabel\":\"Name\",\"dependencyVal\":\"Level: value\",\"dependencyValUnitTitle\":\"Level: name\",\"unitTitle\":\"Unit\",\"attributeValue\":\"Amount\",\"price\":\"Price\",\"currency\":\"Currency\",\"discount\":\"Discount\",\"sumRUB\":\"rub.\",\"sumUSD\":\"USD\"}]' WHERE `id`='8';
            UPDATE {$dbProposalF10943}.tbl_trreport SET `columnDefs`='[{\"field\":\"attributeValue\",\"displayName\":\"Amount\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},\n{\"field\":\"sumRUB\",\"displayName\":\"Price RUB\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},\n{\"field\":\"sumUSD\",\"displayName\":\"Price USD\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},\n{\"field\":\"currency\",\"displayName\":\"Currency\",\"sortable\":true,\"filterable\":true,\"sortingType\":\"string\"}]' WHERE `id`='7';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}