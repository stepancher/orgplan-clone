<?php

class m160610_075231_update_translates_for_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{trcalendarfairtasks}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
		UPDATE {{trproposalelementclass}} SET `label`='Application No.6', `pluralLabel`='AUDIO & VIDEO EQUIPMENT' WHERE `id`='18';

		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2009', 'en', '');
		UPDATE {{trattributeclass}} SET `label`='This form should be filled in until August 19, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru' WHERE `id`='1493';
		UPDATE {{trattributeclass}} SET `label`='LSD displays lease' WHERE `id`='1494';
		UPDATE {{trattributeclass}} SET `label`='Diagonal 42'' (4 days)' WHERE `id`='1495';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1496';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1497';
		UPDATE {{trattributeclass}} SET `label`='Diagonal 42'' (1 day)' WHERE `id`='1498';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1499';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1500';
		UPDATE {{trattributeclass}} SET `label`='Diagonal 50'' (4 days)' WHERE `id`='1501';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1502';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1503';
		UPDATE {{trattributeclass}} SET `label`='Diagonal 50'' (1 day)' WHERE `id`='1504';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1505';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1506';
		UPDATE {{trattributeclass}} SET `label`='Diagonal 60'' (1 day)' WHERE `id`='1507';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1508';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1509';
		UPDATE {{trattributeclass}} SET `label`='Diagonal 60'' (4 days)' WHERE `id`='1510';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1511';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1512';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1513';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1514';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1561';
		UPDATE {{trattributeclass}} SET `label`='Presentation equipment lease' WHERE `id`='1515';
		UPDATE {{trattributeclass}} SET `label`='LCD projector, 4 000 lumen (daily)' WHERE `id`='1516';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1517';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1518';
		UPDATE {{trattributeclass}} SET `label`='LCD projector, 4 000 lumen (hourly)' WHERE `id`='1519';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1520';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1521';
		UPDATE {{trattributeclass}} SET `label`='LCD projector, 8 000 lumen' WHERE `id`='1522';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1523';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1524';
		UPDATE {{trattributeclass}} SET `label`='Screen (1.5 m x 1.5 m)' WHERE `id`='1525';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1526';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1527';
		UPDATE {{trattributeclass}} SET `label`='Screen (2 m x 2 m)' WHERE `id`='1528';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1529';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1530';
		UPDATE {{trattributeclass}} SET `label`='Screen (2.5 m x 2.5 m)' WHERE `id`='1531';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1532';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1533';
		UPDATE {{trattributeclass}} SET `label`='Laptop (with presentation programs)' WHERE `id`='1534';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1535';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1536';
		UPDATE {{trattributeclass}} SET `label`='Sound amplification equipment lease' WHERE `id`='1537';
		UPDATE {{trattributeclass}} SET `label`='Wire microphone' WHERE `id`='1538';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1539';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1540';
		UPDATE {{trattributeclass}} SET `label`='Radio microphone' WHERE `id`='1541';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1542';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1543';
		UPDATE {{trattributeclass}} SET `label`='Mobile sound amplification set (600W)' WHERE `id`='1544';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1545';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1546';
		UPDATE {{trattributeclass}} SET `label`='Sound amplification set (power 1 kW)' WHERE `id`='1547';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1548';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1549';
		UPDATE {{trattributeclass}} SET `label`='Sound amplification set (power 2 kW)' WHERE `id`='1550';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1551';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1552';
		UPDATE {{trattributeclass}} SET `label`='Permission' WHERE `id`='1553';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1554';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1555';
		UPDATE {{trattributeclass}} SET `label`='Permission to use audio-visual presentation equipment' WHERE `id`='1560';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1556';
		UPDATE {{trattributeclass}} SET `label`='All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.' WHERE `id`='1557';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 04, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='1558';
		UPDATE {{trattributeclass}} SET `label`='' WHERE `id`='1559';

		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1748', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1771', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1805', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1749', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1772', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1806', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1750', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1773', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1807', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1751', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1774', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1808', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1752', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1775', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1809', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1753', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1776', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1810', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1754', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1777', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1794', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1800', 'en', 'Set of speakers, mounted');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1811', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1755', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1778', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1795', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1801', 'en', 'Floor support for display');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1812', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1756', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1779', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1796', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1802', 'en', 'DVD, VHS, MD player');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1813', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1757', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1780', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1814', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1758', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1781', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1815', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1759', 'en', 'hour');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1782', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1816', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1760', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1783', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1817', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1761', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1784', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1818', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1762', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1785', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1819', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1763', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1786', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1820', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1764', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1787', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1821', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1765', 'en', 'hour');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1788', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1822', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1766', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1789', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1823', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1767', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1790', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1824', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1768', 'en', 'days');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1791', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1825', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1769', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1792', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1797', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1803', 'en', 'In conference hall \"Crocus Expo\"');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1826', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1770', 'en', 'pcs.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1793', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1798', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1804', 'en', 'In exhibition hall \"Crocus Expo\"');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1827', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1799', 'en', 'USD');


		UPDATE {{attributeclassproperty}} SET `defaultValue`='1' WHERE `id`='1794';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2503';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2508';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2513';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2554';
		UPDATE {{trattributeclassproperty}} SET `value`='1' WHERE `id`='2559';

		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2175', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2132', 'en', 'Event type:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2133', 'en', 'Event type');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2134', 'en', 'Conference');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2135', 'en', 'Seminar');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2136', 'en', 'Master-class');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2137', 'en', 'Round table');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2138', 'en', 'Presentation');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2140', 'en', 'Other');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2141', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2142', 'en', 'Topic');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2143', 'en', 'Number of participants');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2144', 'en', 'Event duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2145', 'en', 'Preferred time for holding');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2146', 'en', 'Conference hall');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2147', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2148', 'en', 'Meals for the event participants');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2149', 'en', 'Project manager:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2150', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2151', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2152', 'en', 'First name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2153', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2154', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2155', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2156', 'en', 'E-mail');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2157', 'en', 'Equipment lease:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2158', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2159', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2160', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2161', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2162', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2163', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2164', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2165', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2166', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2167', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2168', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2169', 'en', 'Permission');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2170', 'en', 'Permission to use audio-visual presentation equipment');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2171', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2172', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2173', 'en', 'All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2174', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2176', 'en', 'This form should be filled in until August 18, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2177', 'en', '');

		UPDATE {{trattributeclassproperty}} SET `value`='' WHERE `id`='2482';
		UPDATE {{trattributeclassproperty}} SET `value`='persons' WHERE `id`='2416';
		UPDATE {{trattributeclassproperty}} SET `value`='hour' WHERE `id`='2417';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2428';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1872', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1885', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Rental period' WHERE `id`='2469';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2429';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1873', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1886', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='LCD projector (4 000 lumen, for the period)' WHERE `id`='2470';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2430';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1874', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1887', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='LCD projector (8 000 lumen, for the period)' WHERE `id`='2471';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2418';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2431';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1875', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1888', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Screen' WHERE `id`='2472';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2419';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2432';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1876', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1889', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Laptop (with set of presentation programs)' WHERE `id`='2473';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2420';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2433';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1877', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1890', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Mobile sound amplification set (600 W)' WHERE `id`='2474';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2421';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2434';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1878', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1891', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Sound amplification set (1 kW)' WHERE `id`='2475';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2422';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2435';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1879', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1892', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Sound amplification set (2 kW)' WHERE `id`='2476';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2423';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2436';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1880', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1893', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Wired microphone' WHERE `id`='2477';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2424';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2437';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1881', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1894', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Radio microphone' WHERE `id`='2478';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2425';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2438';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1882', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1895', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Pulpit (with the sound equipment)' WHERE `id`='2479';
		UPDATE {{trattributeclassproperty}} SET `value`='hour' WHERE `id`='2426';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2439';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1883', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1896', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Technical support (one person)' WHERE `id`='2480';
		UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2427';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2440';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1884', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1897', 'en', '');
		UPDATE {{trattributeclassproperty}} SET `value`='Participant\'s equipment' WHERE `id`='2481';
		UPDATE {{trattributeclassproperty}} SET `value`='USD' WHERE `id`='2441';

		UPDATE {{trproposalelementclass}} SET `label`='Application No.11', `pluralLabel`='EDUCATIONAL EVENT' WHERE `id`='20';

		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2088', 'en', 'Event type:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2089', 'en', 'Event type');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2090', 'en', 'Conference');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2091', 'en', 'Presentation');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2092', 'en', 'Master-class');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2093', 'en', 'Round table');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2094', 'en', 'Seminar');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2095', 'en', 'Press-conference');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2097', 'en', 'Other:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2098', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2099', 'en', 'Topic');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2100', 'en', 'Event duration');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2101', 'en', 'Number of participants');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2102', 'en', 'Report themes / event program');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2103', 'en', 'Target audience');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2104', 'en', 'Number of specialists planned for the event');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2105', 'en', 'Hall for the event:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2106', 'en', 'Conference hall');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2107', 'en', 'Press center');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2108', 'en', 'Equipment');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2109', 'en', 'LCD projector');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2110', 'en', 'Wired microphone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2111', 'en', 'Sound amplification set');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2112', 'en', 'Screen');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2113', 'en', 'Radio microphone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2114', 'en', 'Laptop (with presentation program)');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2115', 'en', 'Obligatory conditions:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2116', 'en', '1. Purpose of the event - education, demonstration of new technologies, discussion of actual problems.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2117', 'en', '2. Topic - actual, interesting for the target audience, allows to apply the received experience, knowledge and technologies in practice.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2118', 'en', '3. Target audience - farmers, specialists from middle and large agricultural companies, farm directors.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2119', 'en', '4. Duration - 1-2 hours, availability of the PowerPoint presentation. ');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2120', 'en', '5. Free participation in the event for exhibition visitors.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2121', 'en', 'Project manager:');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2122', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2123', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2124', 'en', 'First name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2125', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2126', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2127', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2128', 'en', 'E-mail');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2129', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application the parties confirm their agreement with the following conditions: the Organizer takes all the costs related to the lease of conference halls and equipment declared for realization of the event. The Organizer announces the event in mass-media and by advertising and information carries, and also attracts visitors. The Exhibitors takes all the costs related to the formation of the event program and invitation of reporters and participants.<br/><br/>The Organizer reserves the right to cancel the exhibitor\'s event, if the program is changed or the event topic doesn\'t correspond with the topics of \"AGROSALON\" exhibition.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2130', 'en', '');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2131', 'en', '');

		UPDATE {{trattributeclassproperty}} SET `value`='' WHERE `id`='2482';
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1828', 'en', 'hour');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1829', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1830', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1831', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1836', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1841', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1832', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1837', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1842', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1833', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1838', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1843', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1834', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1839', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1844', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1835', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1840', 'en', '');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1845', 'en', '');
		";
	}
}