<?php

class m160323_173006_update_organizers_site extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://bbg.kz/' WHERE name = 'BBG COMMUNICATION ТОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.dental-expo.com/' WHERE name = 'DENTALEXPO ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expogroup.kz/' WHERE name = 'EXPOGROUP МЕЖДУНАРОДНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.fairexpo.kz/' WHERE name = 'FAIR EXPO ВЫСТАВОЧНАЯ КОМПАНИЯ ТОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ifw-expo.de/' WHERE name = 'IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ite-expo.ru/' WHERE name = 'ITE МОСКВА ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ite-siberia.ru/' WHERE name = 'ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ite-ural.ru/' WHERE name = 'ITE УРАЛ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.iteca.kz/' WHERE name = 'ITECA ТОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.korme-expo.kz/' WHERE name = 'KORME-EXPO ТОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://mediaglobe.ru/' WHERE name = 'MEDIA GLOBE (МЕДИА ГЛОБ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ite-expo.ru/' WHERE name = 'MVK В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.reedexpo.ru/' WHERE name = 'REED EXHIBITIONS (RUSSIA)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rte-expo.ru/' WHERE name = 'RTE-МОСКВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rte-expo.ru/' WHERE name = 'RTE-САМАРА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rte-expo.ru/' WHERE name = 'RTE-УРАЛ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.smileexpo.ru/' WHERE name = 'SMILE-EXPO ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.woodenhouse-expo.ru/' WHERE name = 'WORLD EXPO GROUP МЕЖДУНАРОДНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://autocomplex.net/' WHERE name = 'АЗС-ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.aktivexpo.ru/' WHERE name = 'АКТИВ ЭКСПО ГРУПП ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.altfair.ru/' WHERE name = 'АЛТАЙСКАЯ ЯРМАРКА ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.amscort.ru/' WHERE name = 'АМСКОРТ ИНТЕРНЭШНЛ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.amurfair.ru/' WHERE name = 'АМУРСКАЯ ЯРМАРКА ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.asiz.ru/' WHERE name = 'АССОЦИАЦИЯ СИЗ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.astana-expo.com/' WHERE name = 'АСТАНА-ЭКСПО КС ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.atakentexpo.kz/' WHERE name = 'АТАКЕНТ-ЭКСПО МЕЖДУНАРОДНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ ТОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://atomexpo.com/' WHERE name = 'АТОМЭКСПО КОММУНИКАЦИОННАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.baikalexpo.com/' WHERE name = 'БАЙКАЛ ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.balticfair.com/' WHERE name = 'БАЛТИК-ЭКСПО ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://bvkexpo.ru/' WHERE name = 'БАШКИРСКАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://bashexpo.ru/' WHERE name = 'БАШЭКСПО ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.belexpocentr.ru/' WHERE name = 'БЕЛЭКСПОЦЕНТР ВЫСТАВОЧНО-КОНГРЕССНЫЙ КОМПЛЕКС';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://bd-event.ru/' WHERE name = 'БИЗНЕС ДИАЛОГ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.b95.ru/' WHERE name = 'БИЗОН ОБЪЕДИНЕНИЕ ВЫСТАВОЧНЫХ КОМПАНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.vertolexpo.ru/' WHERE name = 'ВЕРТОЛЭКСПО КОНГРЕССНО-ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.veta.ru/' WHERE name = 'ВЕТА ВЫСТАВОЧНЫЙ ЦЕНТР В СТРУКТУРЕ ТОРГОВО-ПРОМЫШЛЕННОЙ ПАЛАТЫ ВОРОНЕЖСКОЙ ОБЛАСТИ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://volgogradexpo.ru/' WHERE name = 'ВОЛГОГРАДЭКСПО ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.tppkaluga.ru/' WHERE name = 'ВЫСТАВОЧНЫЙ ЦЕНТР КАЛУЖСКОЙ ТОРГОВО-ПРОМЫШЛЕННОЙ ПАЛАТЫ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoprom.ru/' WHERE name = 'ВЭСТСТРОЙ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.vcci.ru/' WHERE name = 'ВЯТКА-ЭКСПО ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.flowers-expo.ru/' WHERE name = 'ГРИНЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.dagexpo.ru/' WHERE name = 'ДАГЕСТАН-ЭКСПО РЕСПУБЛИКАНСКИЙ ВЫСТАВОЧНО-МАРКЕТИНГОВЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://messe-russia.ru/' WHERE name = 'ДОЙЧЕ МЕССЕ РУС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.euroexpo.ru/' WHERE name = 'ЕВРОЭКСПО МЕЖДУНАРОДНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.chitaexpo.ru/' WHERE name = 'ЗАБАЙКАЛЬСКИЙ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://imperiaforum.ru/' WHERE name = 'ИМПЕРИЯ КОНГРЕССНО-ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.inconnect-group.ru/' WHERE name = 'ИНКОННЕКТ ГРУППА КОМПАНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.interopttorg.ru/' WHERE name = 'ИНТЕРОПТТОРГ ОРГАНИЗАЦИОННО-ТЕХНИЧЕСКИЙ ЦЕНТР ИНТЕРОПТТОРГ ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.intersib.ru/' WHERE name = 'ИНТЕРСИБ МЕЖДУНАРОДНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expokazan.ru/' WHERE name = 'КАЗАНСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.kazexpo.kz/' WHERE name = 'КАЗЭКСПО МЕЖДУНАРОДНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.kamexpocenter.ru/' WHERE name = 'КРАЕВОЕ ГОСУДАРСТВЕННОЕ АВТОНОМНОЕ УЧРЕЖДЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.karelexpo.ru/' WHERE name = 'КАРЕЛЭКСПО ВЫСТАВОЧНОЕ ОБЪЕДИНЕНИЕ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.tppkomi.ru/' WHERE name = 'КОМИЭКСПО ПРИ ТОРГОВО-ПРОМЫШЛЕННОЙ ПАЛАТЕ РЕСПУБЛИКЕ КОМИ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.komexpo.ru/' WHERE name = 'КОМЭКС ВЫСТАВОЧНЫЙ БИЗНЕС-ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.concordgroup.ru/' WHERE name = 'КОНКОРД ГРУППА КОМПАНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.krasnodarexpo.ru/' WHERE name = 'КРАСНОДАРЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.krasfair.ru/' WHERE name = 'КРАСНОЯРСКАЯ ЯРМАРКА ВЫСТАВОЧНАЯ КОМПАНИЯ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.crocus-expo.ru/' WHERE name = 'КРОКУС ЭКСПО МЕЖДУНАРОДНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://crimea-expo.com/' WHERE name = 'КРЫМ ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.kuzbass-fair.ru/' WHERE name = 'КУЗБАССКАЯ ЯРМАРКА ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ligas-ufa.ru/' WHERE name = 'ЛИГАС КОММЕРЧЕСКИЙ ИННОВАЦИОННЫЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expolipetsk.ru/' WHERE name = 'ЛИПЕЦ-ЭКСПО МЕЖДУНАРОДНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mediexpo.ru/' WHERE name = 'МЕДИ ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.icecompany.org/' WHERE name = 'МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.messe-duesseldorf.ru/' WHERE name = 'МЕССЕ ДЮССЕЛЬДОРФ МОСКВА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://messefrankfurt.ru/' WHERE name = 'МЕССЕ ФРАНКФУРТ РУС';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.metal-expo.ru/' WHERE name = 'МЕТАЛЛ-ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://midexpo.ru/' WHERE name = 'МИДЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mirexpo.ru/' WHERE name = 'МИР-ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo73.ru/' WHERE name = 'МОЗАИКА ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.hhexpo.ru/' WHERE name = 'МОККА ЭКСПО ГРУПП ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mordovexpo.ru/' WHERE name = 'МОРДОВЭКСПОЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://murmanexpo.ru/' WHERE name = 'МУРМАНЭКСПОЦЕНТР АВТОНОМНАЯ НЕКОММЕРЧЕСКАЯ ОРГАНИЗАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mfvk-center.ru/' WHERE name = 'МФВК-ВВЦ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mos-expo.com/' WHERE name = 'МЦВДНТ МОСКВА ГОСУДАРЕСТВЕННОЕ УНИТАРНОЕ ПРЕДПРИЯТИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.yarmarka.ru/' WHERE name = 'НИЖЕГОРОДСКАЯ ЯРМАРКА ВСЕРОССИЙСКОЕ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.allexpo.ru/' WHERE name = 'ОМСК-ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.parad-expo.ru/' WHERE name = 'ПАРАД-ЭКСПО ВЫСТАВОЧНО-РЕКЛАМНАЯ ФИРМА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://cnti-penza.ru/' WHERE name = 'ПЕНЗЕНСКИЙ ЦЕНТР НАУЧНО-ТЕХНИЧЕСКОЙ ИНФОРМАЦИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://pvo74.ru/' WHERE name = 'ПЕРВОЕ ВЫСТАВОЧНОЕ ОБЪЕДИНЕНИЕ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoperm.ru/' WHERE name = 'ПЕРМСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://pirexpo.com/' WHERE name = 'ПИР ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.exponica.ru/' WHERE name = 'ПОКРОВСКИЙ ЦЕНТР ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.pomfair.ru/' WHERE name = 'ПОМОРСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.primexpo.ru/' WHERE name = 'ПРИМЭКСПО ООО В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.progrexpo.ru/' WHERE name = 'ПРОГРЕСС ВЫСТАВОЧНЫЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.right-solution.com/' WHERE name = 'РАЙТ СОЛЮШН ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rbk-expo.ru/' WHERE name = 'РБК ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.regionex.ru/' WHERE name = 'РЕГИОН ВОЛГОГРАДСКИЙ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://restec.ru/' WHERE name = 'РЕСТЭК ГРУППА ПРЕДПРИЯТИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.legpromexpo.ru/' WHERE name = 'РЛП-ЯРМАРКА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rosinex.ru/' WHERE name = 'РОСИНЭКС ВЫСТАВОЧНОЕ ОБЩЕСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rostovexpo.ru/' WHERE name = 'РОСТОВ-ЭКСПО ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rotexpo.ru/' WHERE name = 'РОТЕКС ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rt-expo.ru/' WHERE name = 'РТ-ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rusudom.ru/' WHERE name = 'РУССДОМ ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.62expo.ru/' WHERE name = 'РЯЗАНСКАЯ ВЫСТАВКА ВЫСТАВОЧНЫЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.sakhalinexpo.ru/' WHERE name = 'САХАЛИНСКИЙ МЕЖДУНАРОДНЫЙ ЭКСПОЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://sakhaexpo.ru/' WHERE name = 'САХАЭКСПОСЕРВИС ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.svkvvc.ru/' WHERE name = 'СВК ВВЦ ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-pskov.ru/' WHERE name = 'СЕВЕРО-ЗАПАДНЫЙ ЦЕНТР СОЦИОЛОГИИ И МАРКЕТИНГА МУНИЦИПАЛЬНОЕ ПРЕДПРИЯТИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.sib-info.ru/' WHERE name = 'СИБ-ИНФО ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ (ПРОЕКТ КОМПАНИИ ЗАО «ИНФОЦЕНТР ПЛЮС»)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://siberiaexpo.ru/' WHERE name = 'СИБИРЬ ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://sibexpo.ru/' WHERE name = 'СИБЭКСПОЦЕНТР ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://skrepkaexpo.ru/' WHERE name = 'СКРЕПКА ЭКСПО ПРОЕКТ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://smolcnti.com/' WHERE name = 'СМОЛЕНСКИЙ ЦЕНТР НАУЧНО-ТЕХНИЧЕСКОЙ ИНФОРМАЦИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'https://www.sokolniki.com/' WHERE name = 'СОКОЛЬНИКИ КОНГРЕССНО-ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.soud.ru/' WHERE name = 'СОУД - СОЧИНСКИЙ ВЫСТАВКИ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.sochi-expo.ru/' WHERE name = 'СОЧИ-ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.souzpromexpo.ru/' WHERE name = 'СОЮЗПРОМЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.stargor.ru/' WHERE name = 'ЦЕНТРАЛЬНЫЙ РЫНОК МУП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoyar.ru/' WHERE name = 'СТАРЫЙ ГОРОД МУНИЦИПАЛЬНОЕ УНИТАРНОЕ ПРЕДПРИЯТИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.textilexpo.ru/' WHERE name = 'ТЕКСТИЛЬЭКСПО ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.t-park.ru/' WHERE name = 'ТЕХНОПАРК ТОМСКИЙ МЕЖДУНАРОДНЫЙ ДЕЛОВОЙ ЦЕНТР ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://tdnt.ru/' WHERE name = 'ТУЛЬСКИЙ ДОМ НАУКИ И ТЕХНИКИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo72.ru/' WHERE name = 'ТЮМЕНСКАЯ МЕЖДУНАРОДНАЯ ЯРМАРКА ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.vcudmurtia.ru/' WHERE name = 'УДМУРТИЯ ВЫСТАВОЧНЫЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vk-uzor.ru/' WHERE name = 'УЗОРЧЬЕ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.unexpo.ru/' WHERE name = 'УНИВЕРСАЛЬНЫЕ ВЫСТАВКИ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoural.com/' WHERE name = 'УРАЛ МЕЖРЕГИОНАЛЬНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.uv66.ru/' WHERE name = 'УРАЛЬСКИЕ ВЫСТАВКИ ВЫСТАВОЧНОЕ ОБЩЕСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.uralex.ru/' WHERE name = 'УРАЛЭКСПОЦЕНТР ЕВРОАЗИАТСКИЙ ВЫСТАВОЧНЫЙ ХОЛДИНГ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://farexpo.ru/' WHERE name = 'ФАРЭКСПО ВЫСТАВОЧНОЕ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.sibico.com/' WHERE name = 'ФИРМА СИБИКО ИНТЕРНЭШНЛ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://formika.ru/' WHERE name = 'ФОРМИКА ГРУППА КОМПАНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.khabexpo.ru/' WHERE name = 'ХАБАРОВСКАЯ МЕЖДУНАРОДНАЯ ЯРМАРКА ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.chipexpo.ru/' WHERE name = 'ЧИПЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo96.com/' WHERE name = 'ЭКСПЕРТ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expokontur.ru/' WHERE name = 'ЭКСПО КОНТУР ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expopark.ru/' WHERE name = 'ЭКСПО ПАРК ВЫСТАВОЧНЫЕ ПРОЕКТЫ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo-volga.ru/' WHERE name = 'ЭКСПО-ВОЛГА ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expograd.ru/' WHERE name = 'ЭКСПОГРАД КОМПАНИЯ СОВРЕМЕННЫХ КОММУНИКАЦИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-design.ru/' WHERE name = 'ЭКСПОДИЗАЙН РУССКАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo-com.info/' WHERE name = 'ЭКСПОКОМ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://exposib.ru/' WHERE name = 'ЭКСПО-СИБИРЬ КУЗБАССКАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.pta-expo.ru/' WHERE name = 'ЭКСПОТРОНИКА ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoforum.ru/' WHERE name = 'ЭКСПОФОРУМ-ИНТЕРНЭШНЛ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.breadbusiness.ru/' WHERE name = 'ЭКСПОХЛЕБ ЦЕНТР МАРКЕТИНГА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expocentr.ru/' WHERE name = 'ЭКСПОЦЕНТР ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.elistaexpo.ru/' WHERE name = 'ЭЛИСТАЭКСПО ВЫСТАВОЧНЫЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.yugcont.ru/' WHERE name = 'ЮГОРСКИЕ КОНТРАКТЫ ОКРУЖНОЙ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ug-expo.ru/' WHERE name = 'ЮГ-ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoural.ru/' WHERE name = 'ЮЖУРАЛЭКСПО РВЦ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.y-expo.ru/' WHERE name = 'ЯРМАРКА НЕДВИЖИМОСТИ ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rual-interex.ru/' WHERE name = 'РУАЛЬ ИНТЕРЭКС ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mallgroup.ru/' WHERE name = 'МОЛЛ ГРУППА КОМПАНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.businessmediarussia.ru/' WHERE name = 'БИЗНЕСМЕДИАРАША ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://pro-conference.ru/' WHERE name = 'PROESTATE EVENTS';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.turkelmoscow.ru/' WHERE name = 'ТЮРКЕЛЬ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expomtg.com/' WHERE name = 'EMTG (EXHIBITION MANAGEMENT TECHNOLOGY GROUP)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://salonexpo.ru/' WHERE name = 'ИННОВАТИКА НЕКОММЕРЧЕСКОЕ ПАРТНЕРСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.alitmix.ru/' WHERE name = 'АЛИТ АКАДЕМИЧЕСКИЙ НАУЧНО-ТЕХНИЧЕСКИЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.nkhp.ru/' WHERE name = 'НАРОДНЫЕ ХУДОЖЕСТВЕННЫЕ ПРОМЫСЛЫ РОССИИ АССОЦИАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://zwonkolokolow.ru/' WHERE name = 'ПАРТНЁР ВК ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.calligraphy-museum.com/' WHERE name = 'СОВРЕМЕННЫЙ МУЗЕЙ КАЛЛИГРАФИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.innovexpo.ru/' WHERE name = 'ИННОВЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://russkie-prostori.com/' WHERE name = 'РУССКИЕ ПРОСТОРЫ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.stavros.ru/' WHERE name = 'СТАВРОС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.capitel.ru/' WHERE name = 'КАПИТЕЛЬ С ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.jewer.ru/' WHERE name = 'ЮВЕЛИРНЫЙ ВЕРНИСАЖ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.fashion-fair.ru/' WHERE name = 'ФЕШН МЕССЕ ПИРМАЗЕНС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rosatom.ru/' WHERE name = 'РОСАТОМ ГОСУДАРСТВЕННАЯ КОРПОРАЦИЯ ПО АТОМНОЙ ЭНЕРГИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.comtransexpo.ru/' WHERE name = 'ITEMF EXPO АО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mayer.ru/' WHERE name = 'ИНТЕРДЕКО ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.formula-rukodeliya.ru/' WHERE name = 'ФОРМУЛА РУКОДЕЛИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://antaresexpo.ru/' WHERE name = 'АНТАРЕС ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expocrimea.com/' WHERE name = 'КОНЦЕПТ ГРУПП ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expoforum.biz/' WHERE name = 'ФОРУМ. КРЫМСКИЕ ВЫСТАВКИ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.odinexpo.ru/' WHERE name = 'ОДИНЦОВО-ЭКСПО МУП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.naars.ru/' WHERE name = 'НАЦИОНАЛЬНАЯ АССОЦИАЦИЯ АЛМАЗНОЙ РЕЗКИ И СВЕРЛЕНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://furexpo.ru/' WHERE name = 'ЗОЛОТОЙ ВЕК ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.astigroup.ru/' WHERE name = 'АСТИ ГРУПП ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.thervs.org/' WHERE name = 'РУССКИЕ ВЫСТАВОЧНЫЕ СИСТЕМЫ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://muslim.ru/' WHERE name = 'СОВЕТ МУФТИЕВ РОССИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rosmould.ru/' WHERE name = 'ЭМГ МЕЖДУНАРОДНАЯ ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://udmtpp.ru/' WHERE name = 'УДМУРТСКАЯ ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://kosk.ru/' WHERE name = 'АГЕНТСТВО БИЗНЕС КОНТАКТОВ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.evolution-m.ru/' WHERE name = 'EVOLUTION GROUP';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.fairclub.ru/' WHERE name = 'ЯРМАРОЧНЫЙ КЛУБ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ruscastings.ru/' WHERE name = 'РОССИЙСКАЯ АССОЦИАЦИЯ ЛИТЕЙЩИКОВ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ryazancci.ru/' WHERE name = 'РЯЗАНСКАЯ ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.wtc-chel.ru/' WHERE name = 'ЦЕНТР МЕЖДУНАРОДНОЙ ТОРГОВЛИ ЧЕЛЯБИНСК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://raduga-expo.ru/' WHERE name = 'РАДУГА-ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.intexpo.ru/' WHERE name = 'ИНТЕР ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expos.ru/' WHERE name = 'ЭКСПОСЕРВИС-1 ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.raapa.ru/' WHERE name = 'РОССИЙСКАЯ АССОЦИАЦИЯ ПАРКОВ И ПРОИЗВОДИТЕЛЕЙ АТТРАКЦИОНОВ (РАППА)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://kidplay.ru/' WHERE name = 'ГОРОД ДЕТСТВА НП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.v-mc.ru/' WHERE name = 'ВЫСТАВОЧНО-МАРКЕТИНГОВЫЙ ЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://valve-forum.ru/' WHERE name = 'ПРОМЫШЛЕННЫЙ ФОРУМ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.aborigenexpo.ru/' WHERE name = 'АБОРИГЕН ЭКСПО ТУР КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://uar.ru/' WHERE name = 'СОЮЗ АРХИТЕКТОРОВ РОССИИ ОБЩЕРОССИЙСКАЯ ОБЩЕСТВЕННАЯ ОРГАНИЗАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.bizinform.ru/' WHERE name = 'БИЗНЕС-ИНФОРМ ИНФОРМАЦИОННОЕ АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.nat.ru/' WHERE name = 'НАЦИОНАЛЬНАЯ АССОЦИАЦИЯ ТЕЛЕРАДИОВЕЩАТЕЛЕЙ РОССИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expoelectroseti.ru/' WHERE name = 'ЭЛЕКТРИЧЕСКИЕ СЕТИ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expochel.ru/' WHERE name = 'ЭКСПОЧЕЛ ЮЖНО-УРАЛЬСКИЙ КОНГРЕССНО-ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.dialcom-expo.ru/' WHERE name = 'ДАЭЛКОМ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.erg-expo.ru/' WHERE name = 'ЭРГ ГРУППА КОМПАНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rostex-expo.ru/' WHERE name = 'РОСТЭКС ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expotver.ru/' WHERE name = 'ЭКСПО-ТВЕРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.spikmo.ru/' WHERE name = 'СОЮЗ ПАРИКМАХЕРОВ И КОСМЕТОЛОГОВ МОСКОВСКОЙ ОБЛАСТИ РОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expodium.ru/' WHERE name = 'ЭКСПОДИУМ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.bijou-moscow.com/' WHERE name = 'НОК ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.avtor-moda.ru/' WHERE name = 'ЦЕНТР ПОДДЕРЖКИ И РАЗВИТИЯ ИНДУСТРИИ МОДЫ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.50plus.ru/' WHERE name = 'БЮРО ПАРАД ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://real-fair.ru/' WHERE name = 'РЕАЛ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expotol.ru/' WHERE name = 'ЭКСПО-ТОЛЬЯТТИ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ontico.ru/' WHERE name = 'ОНТИКО КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://arb.ru/' WHERE name = 'АССОЦИАЦИЯ РОССИЙСКИХ БАНКОВ (АРБ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.vostockcapital.com/' WHERE name = 'VOSTOCK CAPITAL';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://apex-expo.net/' WHERE name = 'АПЕКС-Н ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rostov-kupec.ru/' WHERE name = 'РОСТОВ КУПЕЧЕСКИЙ ВФ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.exponsk.ru/' WHERE name = 'ЭКСПОНСК ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://tpppnz.ru/' WHERE name = 'ПЕНЗЕНСКАЯ ОБЛАСТНАЯ ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expokama.ru/' WHERE name = 'ЭКСПО-КАМА ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://minstroyrf.ru/' WHERE name = 'МИНИСТЕРСТВО СТРОИТЕЛЬСТВА И ЖИЛИЩНО-КОММУНАЛЬНОГО ХОЗЯЙСТВА РОССИЙСКОЙ ФЕДЕРАЦИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.inconex.ru/' WHERE name = 'ИНКОНЭКС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.nt-expo.ru/' WHERE name = 'НОВОЕ ТЫСЯЧЕЛЕТИЕ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://begingroup.com/' WHERE name = 'BEGINE GROUP';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://consef.ru/' WHERE name = 'КОНСЭФ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expobroker.ru/' WHERE name = 'ПРОМЫШЛЕННОСТЬ И СТРОИТЕЛЬСТВО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.pw-expo.ru/' WHERE name = 'МБ ЭКСПО ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.asimut.ru/' WHERE name = 'ПЛАНЕТА АЗИМУТ ИНФОРМАЦИОННО-АНАЛИТИЧЕСКАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.exhibitionport.ru/' WHERE name = 'EXHIBITION PORT/ВЫСТАВОЧНАЯ ГАВАНЬ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.finexpo.ru/' WHERE name = 'FINEXPO';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://avtograd-expo.ru/' WHERE name = 'АВТОГРАД ЭКСПО КВП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.wellness-expo.info/' WHERE name = 'SEED EXHIBITIONS & EVENTS';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://zarexpo.ru/' WHERE name = 'ЦАРИЦЫНСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://exponovo.ru/' WHERE name = 'ИНТЕРГЕО-СИБИРЬ ВЫСТАВОЧНЫЙ ОПЕРАТОР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.asmap-service.ru/' WHERE name = 'АСПАМ-СЕРВИС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expoeventhall.ru/' WHERE name = 'EXPO EVENT HALL';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.vfcenter.ru/' WHERE name = 'ЦЕНТР ВЫСТАВОЧНАЯ ФИРМА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.davaypogenimsya.ru/' WHERE name = 'ДАВАЙ ПОЖЕНИМСЯ СВАДЕБНОЕ АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://exposiberia.ru/' WHERE name = 'ЭКСПО СИБИРЬ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'https://metro-expo.ru/' WHERE name = 'METRO CASH & KERRY';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://214-fz.net/' WHERE name = 'ЗАКОННОЕ ЖИЛЬЕ МПОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-unity.ru/' WHERE name = 'ЭКСПО-ЕДИНСТВО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://sun-expo.com/' WHERE name = 'САН-ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://artflection.ru/' WHERE name = 'ARTFLECTION ПРОЕКТ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.glassacademyrussia.com/' WHERE name = 'АКАДЕМИЯ СТЕКЛА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.absolut-expo.ru/' WHERE name = 'АБСОЛЮТ ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.parkzoo.ru/' WHERE name = 'АРТИС ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://kordon.ru/' WHERE name = 'КОРДОН ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.softool.ru/' WHERE name = 'ИТ-ЭКСПО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.messe.org/' WHERE name = 'СПРИНГ МЕССЕ МЕНЕДЖЕМЕНТ ГМБХ (ГЕРМАНИЯ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://raec.ru/' WHERE name = 'РАЭК НП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.nta-rus.com/' WHERE name = 'НТА (НАЦИОНАЛЬНАЯ ТОРГОВАЯ АССОЦИАЦИЯ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.znanie.info/' WHERE name = 'РТВ-МЕДИА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mosbiotechworld.ru/' WHERE name = 'ЭКСПО-БИОХИМ-ТЕХНОЛОГИИ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.starexpo.ru/' WHERE name = 'СТАР ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://skibuild.ru/' WHERE name = 'ЭРЦОГ-ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.trier.ru/' WHERE name = 'ТРИЭР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vbik.ru/' WHERE name = 'ВЯТСКИЙ БАЗАР ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://lafeerie.ru/' WHERE name = 'LA FEERIE СТУДИЯ СТИЛЬНЫХ СВАДЕБ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vrn-uk.ru/' WHERE name = 'ДЕПАРТАМЕНТ КУЛЬТУРЫ ВОРОНЕЖСКОЙ ОБЛАСТИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rapremiera.ru/' WHERE name = 'ПРЕМЬЕРА ПЛЮС РА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.niva-expo.ru/' WHERE name = 'КВИТКИНА Л.И. ИП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.tatexpo.com/' WHERE name = 'ТАТЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.eedrift.com/' WHERE name = 'EVIL-EMPIRE TEAM';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://kollekcioner-spb.ru/' WHERE name = 'КОЛЛЕКЦИОНЕР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.nevberega.ru/' WHERE name = 'НЕВСКИЕ БЕРЕГА ОБЩЕСТВЕННЫЙ ФОНД';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.gilkom-complex.ru/' WHERE name = 'ЖИЛИЩНЫЙ КОМИТЕТ ПРАВИТЕЛЬСТВА САНКТ-ПЕТЕРБУРГА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.bn.ru/' WHERE name = 'БЮЛЛЕТЕНЬ НЕДВИЖИМОСТИ ГК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://darya-expo.ru/' WHERE name = 'ВОЗРОЖДЕНИЕ ТВОРЧЕСКОЕ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.spb-expo.ru/' WHERE name = 'ДУХОВНОЕ НАСЛЕДИЕ ТВОРЧЕСКОЕ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://gemworld.ru/' WHERE name = 'МИР КАМНЯ ВЫСТАВОЧНОН ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.comicconspb.ru/' WHERE name = 'КОМИК КОН САНКТ-ПЕТЕРБУРГ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.vkrussdom.ru/' WHERE name = 'ПРЕЗЕНТАЦИОННО-СЕРВИСНЫЙ ЦЕНТР БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ ВОЛОГОДСКОЙ ОБЛАСТИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.oldtimer.ru/' WHERE name = 'РЕТРОГРАД ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ulanexpo.ru/' WHERE name = 'УЛАН-УДЭНСКАЯ ЯРМАРКА ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.gifts-expo.com/' WHERE name = 'GIFTS INTERNATIONAL';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://baltgaz.ru/' WHERE name = 'КУЛЬТУРНАЯ СТОЛИЦА АНО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.skladcom.ru/' WHERE name = 'ЭКСПО МЕДИА ГРУПП КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.kirovexpo.ru/' WHERE name = 'КИРОВЭКСПОЦЕНТР АНО ЦОВ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mixmax.ru/' WHERE name = 'MIXMAX ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://mineral-show.ru/' WHERE name = 'МИНЕРАЛ-ШОУ ВЫСТАВОЧНОЕ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://arvd.ru/' WHERE name = 'АГЕНТСТВО РЕКЛАМНО-ВЫСТАВОЧНОЙ ДЕЯТЕЛЬНОСТИ ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://aspolrf.ru/' WHERE name = 'АССОЦИАЦИЯ ПОЛЯРНИКОВ МЕЖРЕГИОНАЛЬНАЯ ОБЩЕСТВЕННАЯ ОРГАНИЗАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.imd.ru/' WHERE name = 'IMD КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rtedc.org/' WHERE name = 'СОВЕТ ПО РАЗВИТИЮ ВНЕШНЕЙ ТОРГОВЛИ И МЕЖДУНАРОДНЫХ ЭКОНОМИЧЕСКИХ ОТНОШЕНИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.anapa-sea.ru/' WHERE name = 'ЗДРАВНИЦЫ ГОРОДА-КУРОРТА АНАПА СКО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.marttoys.ru/' WHERE name = 'МАРТ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.fapmc.ru/' WHERE name = 'ГЕНЕРАЛЬНАЯ ДИРЕКЦИЯ МЕЖДУНАРОДНЫХ КНИЖНЫХ ВЫСТАВОК И ЯРМАРОК ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.oar-info.ru/' WHERE name = 'ОБЪЕДИНЕНИЕ АВТОПРОИЗВОДИТЕЛЕЙ РОССИИ НП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.professionalfairs.ru/' WHERE name = 'NUERNBERGMESSE GMBH';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://irken.ru/' WHERE name = 'АВЕГА КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://antikshow.ru/' WHERE name = 'АНТИК ШОУ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ruspitomniki.ru/' WHERE name = 'АССОЦИАЦИЯ ПРОИЗВОДИТЕЛЕЙ ПОСАДОЧНОГО МАТЕРИАЛА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.dsk-moscow.ru/' WHERE name = 'ДИЗАЙН-СТЕНД-КОНСАЛТ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.plusworld.ru/' WHERE name = 'ПЛАС ЖУРНАЛ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.retail-loyalty.org/' WHERE name = 'RETAIL & LOYALTY ЖУРНАЛ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://veg-bazar.ru/' WHERE name = 'МОСКОВСКАЯ ШКОЛА СЫРОЕДЕНИЯ И ВЕГЕТАРИАНСТВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://motowinter.ru/' WHERE name = 'ДИ АР ЭВЕНТС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.diveshow.ru/' WHERE name = 'АНДЕРВОТЕР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://medpalatarb.ru/' WHERE name = 'КОМПЛЕКСНЫЙ МЕДИЦИНСКИЙ КОНСАЛТИНГ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.redenex.com/' WHERE name = 'REDENEX КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ecwatech-ltd.ru/' WHERE name = 'ЭКВАТЭК КОМПАНИЯ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.groteck.ru/' WHERE name = 'ГРОТЕК ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://amplua.ru/' WHERE name = 'АМПЛУА КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expoelectro.ru/' WHERE name = 'ЭКСПОНЕНТА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.cosmopress.ru/' WHERE name = 'СТАРАЯ КРЕПОСТЬ ЭКСПОМЕДИАГРУППА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.svadba-club.ru/' WHERE name = 'ПЕТЕРБУРГСКИЕ ЖЕНИХ И НЕВЕСТА ЖУРНАЛ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ifinmedia.ru/' WHERE name = 'IFIN MEDIA';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.exposystems.ru/' WHERE name = 'EXPOSYSTEMS';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://praesens.ru/' WHERE name = 'STATUS PRAESENS';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo21vek.ru/' WHERE name = 'ЭКСПО XXI ВЕК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://privetexpo.ru/' WHERE name = 'ПРИВЕТ-МЕДИА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://krasknedra.ru/' WHERE name = 'ЦЕНТРСИБНЕДРА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rabota-enisey.ru/' WHERE name = 'АГЕНТСТВО ТРУДА И ЗАНЯТОСТИ КРАСНОЯРСКОГО КРАЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.prokhorovfund.ru/' WHERE name = 'ФОНД МИХАИЛА ПРОХОРОВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.krasfair.ru/' WHERE name = 'КРАСНОЯРСКАЯ КУЛЬТУРНАЯ КОМПАНИЯ АНО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rus-inter.com/' WHERE name = 'РУСИНТЕРЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://domlavka.ru/' WHERE name = 'ДОМАШНЯЯ ЛАВКА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://thebezebazar.ru/' WHERE name = 'THE BEZE BAZAR ЯРМАРКА УНИКАЛЬНЫХ ВЕЩЕЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.art-weekend.ru/' WHERE name = 'АРТ УИКЭНД ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.museum.ru/' WHERE name = 'ИСКУССТВО БУДУЩЕГО ПРОДЮСЕРСКИЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://shoesstar.ru/' WHERE name = 'ШУЗСТАР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.moslem.ru/' WHERE name = 'ДУХОВНОЕ УПРАВЛЕНИЕ МУСУЛЬМАН ПЕРМСКОГО КРАЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.remontexpo.com/' WHERE name = 'ФОРИНФОРМ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://pripravaspb.ru/' WHERE name = 'ПРИПРАВА КОММУНИКАТИВНОЕ АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ledentcovawedding.ru/' WHERE name = 'LEDENTSOVA WEDDING AGENCY';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.svadba-discont.ru/' WHERE name = 'ВДВОЕМ СВАДЕБНЫЙ ЖУРНАЛ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://wedding-fair.ru/' WHERE name = 'СВАДЕБНЫЙ ПЕРЕПОЛОХ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://gubgorod76.ru/' WHERE name = 'ГУБЕРНСКИЙ ГОРОД ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vsegrani.info/' WHERE name = 'ВСЕ ГРАНИ ТВОРЧЕСТВА НП АМДПИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.m-raduga.ru/' WHERE name = 'МЕХОВАЯ РАДУГА ФАБРИКА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.yarinfo.com/' WHERE name = 'ИНФОКОМ РЕКЛАМНО-ИНФОРМАЦИОННОЕ АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-kazak.ru/' WHERE name = 'ВОЛЬНЫЙ БЕРЕГ ПК ВК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-a.ru/' WHERE name = 'ЭКСПО-АКТИВ ВК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ultrastar.ru/' WHERE name = 'ФИНИСТ БЛАГОТВОРИТЕЛЬНЫЙ ФОНД';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.werewolfmc.ru/' WHERE name = 'WEREWOLF MC МОТОКЛУБ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo26.ru/' WHERE name = 'АВА ФИРМА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://jfair.ru/' WHERE name = 'РЕФОРМА И РАЗВИТИЕ КОСТРОМСКОЙ ОБЛАСТНОЙ ФОНД';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.volga2014.ru/' WHERE name = 'VOLGA BOAT SHOW';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.artexplorer.ru/' WHERE name = 'ARTEXPLORER GROUP';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ugpik.ru/' WHERE name = 'ЮЖНАЯ ГИЛЬДИЯ ПЕКАРЕЙ, КОНДИТЕРОВ, ИНДУСТРИИ ГОСТЕПРИИМСТВА НП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://stavexpo.org/' WHERE name = 'СТАВЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.tppsk.ru/' WHERE name = 'ТПП СТАВРОПОЛЬСКОГО КРАЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ruslady.org/' WHERE name = 'ЖЕНЩИНЫ БИЗНЕСА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://megapolis-angel.ru/' WHERE name = 'АНГЕЛЫ МЕГАПОЛИСА АГЕНТСТВО БИЗНЕС МЕРОПРИЯТИЙ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://odfevents.ru/' WHERE name = 'ODF EVENTS КОНГРЕСС БЮРО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo-yug.ru/' WHERE name = 'ЭКСПО-ЮГ ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://valinorgrad.com/' WHERE name = 'ВАЛИНОР - ГРАД МАСТЕРОВ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.labmedicina.ru/' WHERE name = 'НПО СЛМ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ramld.ru/' WHERE name = 'РАМЛД';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ligazn.ru/' WHERE name = 'ЛИГА ЗДОРОВЬЯ НАЦИИ ОБЩЕРОССИЙСКАЯ ОБЩЕСТВЕННАЯ ОРГАНИЗАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://cosmoscow.com/' WHERE name = 'COSMOSCOW';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.lm-international.com/' WHERE name = 'LEIPZIGER MESSE INTERNATIONAL GMBH';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.nghn.ru/' WHERE name = 'НАЦИОНАЛЬНАЯ ГИЛЬДИЯ ХРАНИТЕЛЕЙ НАСЛЕДИЯ АНО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://nastrelke.ru/' WHERE name = 'НА СТРЕЛКЕ ЦЕНТР НЕСОВРЕМЕННОГО ИСКУССТВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.designspb.ru/' WHERE name = 'САНКТ-ПЕТЕРБУРГСКИЙ СОЮЗ ДИЗАЙНЕРОВ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.franch-region.com/' WHERE name = 'FRANCH РЕГИОН';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://plastic-surgery.ru/' WHERE name = 'ЦЕНТР КОСМЕТОЛОГИИ И ПЛАСТИЧЕСКОЙ ХИРУРГИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.natexpo.ru/' WHERE name = 'ЭКСПОНАТ ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.kinorinok.ru/' WHERE name = 'СОЮЗКИНОРЫНОК ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ugraces.ru/' WHERE name = 'ЦЕНТР ЭНЕРГОСБЕРЕЖЕНИЯ ЮГРЫ АНО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.gidroaviasalon.com/' WHERE name = 'ГИДРОАВИАСАЛОН ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.watermark.ru/' WHERE name = 'УОТЕР МАРК ИЗДАТЕЛЬСКИЙ ДОМ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://fort-m.biz/' WHERE name = 'ФОРТ-М';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rusmet.ru/' WHERE name = 'РУСМЕТ ГК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vladmore.ru/' WHERE name = 'МОРСКИЕ ПУТЕШЕСТВИЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://sfyc.ru/' WHERE name = 'СЕМЬ ФУТОВ ЯХТ-КЛУБ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.applezzz.ru/' WHERE name = 'СИНИЕ ЯБЛОКИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.bioethanol.ru/' WHERE name = 'РОССИЙСКАЯ БИОТОПЛИВНАЯ АССОЦИАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://primamedia.ru/' WHERE name = 'PRIMAMEDIA МЕДИАХОЛДИНГ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expocontainer.ru/' WHERE name = 'ОРГТЕХСТРОЙ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.m-f.ru/' WHERE name = 'МЕЛЕХОВ И ФИЛЮРИН РЕКЛАМНАЯ ГРУППА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.e-prof.ru/' WHERE name = 'КАК ДЕЛАТЬ HR-КЛУБ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.web2win.ru/' WHERE name = 'WEB2WIN';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.confspb.ru/' WHERE name = 'CONFERENCE POINT';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rusrealexpo.ru/' WHERE name = 'РУСРЕАЛЭКСПО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo.sfyc.ru/' WHERE name = 'ВЛАДИВОСТОК БОТ-ШОУ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ivf.tatarstan.ru/' WHERE name = 'ИНВЕСТИЦИОННО-ВЕНЧУРНЫЙ ФОНД РТ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.gorn.pro/' WHERE name = 'ГОРОДСКОЕ ОБЪЕДИНЕНИЕ РЕКЛАМИСТОВ НОВОСИБИРСКА НП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mma-expo.ru/' WHERE name = 'ММА-ЭКСПО МЕДИЦИНСКОЕ МАРКЕТИНГОВОЕ АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-astrakhan.ru/' WHERE name = 'EXPO-ASTRAKHAN РЕКЛАМНО-ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.owp-international.com/' WHERE name = 'OST-WEST-PARTNER GMBH';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ptf.su/' WHERE name = 'PRIME TIME FORUMS';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://minenergo.gov.ru/' WHERE name = 'МИНИСТЕРСТВО ЭНЕРГЕТИКИ РОССИЙСКОЙ ФЕДЕРАЦИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://chelovekilekarstvo.ru/' WHERE name = 'ЧЕЛОВЕК И ЛЕКАРСТВО РИЦ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.beriev.com/' WHERE name = 'ТАНТК ИМ. Г. М. БЕРИЕВА ПАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://sobtur.com/' WHERE name = 'СТОЛЬНЫЙ ГРАД АНО МФКД';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://csh.sibagro.ru/' WHERE name = 'ЦЕНТР СЕЛЬСКОХОЗЯЙСТВЕННОГО КОНСУЛЬТИРОВАНИЯ КГБУ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.guild.breadbusiness.ru/' WHERE name = 'РОССИЙСКАЯ ГИЛЬДИЯ ПЕКАРЕЙ И КОНДИТЕРОВ НП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rosyuvelirexpo.ru/' WHERE name = 'Р.О.С.ЮВЕЛИРЭКСПО ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://museum.ametist-kostroma.ru/' WHERE name = 'МУЗЕЙ ЮВЕЛИРНОГО ИСКУССТВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.fedlab.ru/' WHERE name = 'ФЕДЕРАЦИЯ ЛАБОРАТОРНОЙ МЕДИЦИНЫ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://мичуринск-наукоград.рф/' WHERE name = 'АДМИНИСТРАЦИЯ Г. МИЧУРИНСКА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://artinfo.pro/' WHERE name = 'АРТ-МЕНЕДЖМЕНТ СЕКЦИЯ ТВОРЧЕСКОГО СОЮЗА ХУДОЖНИКОВ РОССИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://dszn.mos.ru/' WHERE name = 'ДЕПАРТАМЕНТ ТРУДА И ЗАНЯТОСТИ НАСЕЛЕНИЯ ГОРОДА МОСКВЫ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rnc-consult.ru/' WHERE name = 'R&C ВЫСТАВОЧНЫЙ НАУЧНО-ИССЛЕДОВАТЕЛЬСКИЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://будьграмотным.рф/' WHERE name = 'ФИНАНСОВАЯ ГРАМОТНОСТЬ ПРОО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://minsoc.permkrai.ru/' WHERE name = 'МИНИСТЕРСТВО СОЦИАЛЬНОГО РАЗВИТИЯ ПЕРМСКОГО КРАЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.b2bcg.ru/' WHERE name = 'B2B CONFERENCE GROUP (BBCG)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://svetich.info/' WHERE name = 'СВЕТИЧ АГРАРНЫЙ МЕДИАХОЛДИНГ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://astra23.pulscen.ru/' WHERE name = 'АСТРА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://bike-expo.ru/' WHERE name = 'КАЙЗЕР СПОРТ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://hleb-da-sol.spb.ru/' WHERE name = 'ХЛЕБ ДА СОЛЬ ЭТНО-ДОСУГОВЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ярмаркаудача.рф/' WHERE name = 'ЭКСПОЛАЙФ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://csh.sibagro.ru/' WHERE name = 'ЦЕНТР СЕЛЬСКОХОЗЯЙСТВЕННОГО КОНСУЛЬТИРОВАНИЯ КГБУ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vk.com/club68339874/' WHERE name = 'МЕТРО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://batist-trikotazh.ru/' WHERE name = 'МАЛЬЦЕВА С.В. ИП';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.gkop.ru/' WHERE name = 'ГУБЕРНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mingeoforum.ru/' WHERE name = 'АССОЦИАЦИЯ ГЕОЛОГОВ И ГОРНОПРОМЫШЛЕННИКОВ КРАСНОЯРСКОГО КРАЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://минобрнауки.рф/' WHERE name = 'МИНИСТЕРСТВО ОБРАЗОВАНИЯ И НАУКИ РФ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://msi.mos.ru/' WHERE name = 'МОССТРОЙИНФОРМ ГБУ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ecmgroup.pro/' WHERE name = 'ЕСМ ГРУПП ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.etm.ru/' WHERE name = 'ЭТМ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.r-o-g.ru/' WHERE name = 'НЕКОММЕРЧЕСКАЯ АССОЦИАЦИЯ ПОСТАВЩИКОВ ТОВАРОВ И УСЛУГ ДЛЯ ТУРИЗМА И АКТИВНОГО ОТДЫХА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.invaexpo.ru/' WHERE name = 'ИНВА ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://world-art-dance.com/' WHERE name = 'WORLD ART & DANCE ALLIANCE (WADA)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.iciep.ru/' WHERE name = 'МВ-ЦЕНТР МЕЖВУЗОВСКИЙ ЦЕНТР ИНТЕРНАЦИОНАЛЬНЫХ ОБРАЗОВАТЕЛЬНЫХ ПРОГРАММ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo-red.com/' WHERE name = 'RED GROUP';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.adamsmithconferences.com/' WHERE name = 'ADAM SMITH CONFERENCES';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.professionalfairs.ru/' WHERE name = 'ПРОФЕССИОНАЛЬНЫЕ ВЫСТАВКИ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.salonemilano.it/' WHERE name = 'FEDERLEGNO ARREDO EVENTI SPA';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://shop.ki-expo.ru/' WHERE name = 'KOSMETIK INTERNATIONAL ИЗДАТЕЛЬСКИЙ ДОМ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://neva.transtec-neva.ru/' WHERE name = 'ТРАНСТЕХ НЕВА ЭКСИБИШИНС ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://torgmeh.ru/' WHERE name = 'ЯРМАРКА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://exposfera.spb.ru/' WHERE name = 'EXPO СФЕРА ВЫСТАВОЧНОЕ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ddsgallery.ru/' WHERE name = 'ГАЛЕРЕЯ ДЭDИС';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.interbat.ru/' WHERE name = 'ИНТЕРБАТ МА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://ccitula.ru/' WHERE name = 'ТУЛЬСКАЯ ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://bryansk.cci.ru/' WHERE name = 'БРЯНСКАЯ ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.dalexpo.vl.ru/' WHERE name = 'ДАЛЬЭКСПОЦЕНТР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://euroforum.karelia.ru/' WHERE name = 'ЕВРОФОРУМ ВЫСТАВОЧНОЕ АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.kya.reg-kursk.ru/' WHERE name = 'КУРСКАЯ КОРЕНСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР ОБЛАСТНОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ses.net.ru/' WHERE name = 'СИБЭКСПОСЕРВИС-Н ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://днт-тверь.рф/' WHERE name = 'ТВЕРСКОЙ ОБЛАСТНОЙ ДОМ НАУКИ И ТЕХНИКИ РОССИЙСКОГО СОЮЗА НАУЧНЫХ И ИНЖЕНЕРНЫХ ОБЩЕСТВЕННЫХ ОБЪЕДИНЕНИЙ (ТДНТ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo.sofit.ru/' WHERE name = 'СОФИТ-ЭКСПО ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://sivel.spb.ru/' WHERE name = 'СИВЕЛ КОНГРЕССНО-ВЫСТАВОЧНОЕ ОБЪЕДИНЕНИЕ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://kostroma.tpprf.ru/' WHERE name = 'ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА КОСТРОМСКОЙ ОБЛАСТИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vladimir.tpprf.ru/' WHERE name = 'ТПП ВЛАДИМИРСКОЙ ОБЛАСТИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://уралэкспо.рф/' WHERE name = 'УРАЛЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vk-forum-ekspo.alloy.ru/' WHERE name = 'ФОРУМ-ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://novgorod.tpprf.ru/' WHERE name = 'ЭКСПОНОВГОРОД НОВГОРОДСКОЙ ТОРГОВО-ПРОМЫШЛЕННОЙ ПАЛАТЫ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expocentr.vrn.ru/' WHERE name = 'ЭКСПОЦЕНТР ВГАУ АГРОБИЗНЕС ЧЕРНОЗЕМЬЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.msu.ru/' WHERE name = 'МГУ ИМЕНИ М.В.ЛОМОНОСОВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ruscustomsinform.ru/' WHERE name = 'РОСТЭК-ТАМОЖИНФОРМ, ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.electrotrans-expo.ru/' WHERE name = 'РУСГОРТРАНС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://oborot.ru/' WHERE name = 'OBOROT.RU КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.5lb.ru/' WHERE name = '5ЛБ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://mettem.bz/' WHERE name = 'ГРУППА КОМПАНИЙ МЕТТЭМ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://travelexhibition.ru/' WHERE name = 'САНКТ-ПЕТЕРБУРГ ЭКСПРЕСС ЗАО В СОСТАВЕ ГРУППЫ ПРЕДПРИЯТИЙ РЕСТЭК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mosshoes.com/' WHERE name = 'МОСШУЗ ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.licensingworld.ru/' WHERE name = 'ГРАНД ЭКСПО ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expotour.org/' WHERE name = 'ЭКСПОТУР ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://kriconf.ru/' WHERE name = 'ОРГКОМИТЕТ КОНФЕРЕНЦИИ РАЗРАБОТЧИКОВ КОМПЬЮТЕРНЫХ ФИРМ (КРИ)';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://fsad.ucoz.ru/' WHERE name = 'ФЬЮЖН-САД ООК';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.navalshow.ru/' WHERE name = 'МОРСКОЙ САЛОН ЗАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.gost.ru/' WHERE name = 'РОССТАНДАРТ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://minpromtorg.gov.ru/' WHERE name = 'МИНПРОМТОРГ РОССИИ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.atmosferatvorchestva.ru/' WHERE name = 'АТМОСФЕРА ТВОРЧЕСТВА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.aigroup.ru/' WHERE name = 'AIGROUP КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://tattoopharma.ru/' WHERE name = 'TATTOO PHARMA ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://expo-don.ru/' WHERE name = 'ЭКСПО-ДОН ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.optimaexpo.ru/' WHERE name = 'ОПТИМА ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://модныйгород.рф/' WHERE name = 'ЛОТОС-ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://профорганизатор.рф/' WHERE name = 'СТАВРОПОЛЬЕ ВЫСТАВОЧНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.maxinform.ru/' WHERE name = 'МАКСИМУМ-ИНФОРМ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.транс-экспо.рф/' WHERE name = 'ТРАНС ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.dollmanege.ru/' WHERE name = 'КУКЛЫ МИРА КУЛЬТУРНЫЙ ФОНД ПОДДЕРЖКИ И СОДЕЙСТВИЯ РАЗВИТИЮ КУКОЛЬНОГО ИСКУССТВА';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.rimiexpo.ru/' WHERE name = 'РИМИЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ireks.ru/' WHERE name = 'ИРЕКС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://rusteplica.ru/' WHERE name = 'ТЕПЛИЦЫ РОССИИ АССОЦИАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.jetexpo.ru/' WHERE name = 'ДЖЕТ ЭКСПО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.interbytchim.ru/' WHERE name = 'ЭВЕНТА КОМЬЮНИКЕЙШН ГРУП ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.docflow.ru/' WHERE name = 'DOCFLOW';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.expo-elektra.ru/' WHERE name = 'ЭЛЕКТРИФИКАЦИЯ ВЫСТАВОЧНЫЙ ПАВИЛЬОН ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.svadba-discont.ru/' WHERE name = 'СТИЛЬ ВАШЕГО ПРАЗДНИКА АГЕНТСТВО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://unionarts.ru/' WHERE name = 'ВСЕМИРНЫЙ ФОНД ИСКУССТВ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mostpp.ru/' WHERE name = 'АГЕНТСТВО ПО ИНВЕСТИЦИОННОМУ РАЗВИТИЮ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://eremeyka.ru/' WHERE name = 'ОТКРЫТЫЙ МИР КУЛЬТУРНЫЙ ЦЕНТР';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ebrandexpo.com/' WHERE name = 'ПОДКОМИТЕТ ЭЛЕКТРОНИКИ И ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ ПРИ CCPIT';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.ebrandexpo.com/' WHERE name = 'МЕЖДУНАРОДНЫЙ ЦЕНТР ПО ТЕХНИКО-ЭКОНОМИЧЕСКОМУ СОТРУДНИЧЕСТВУ ПРИ МИНИСТЕРСТВЕ ПРОМЫШЛЕННОСТИ И ИНФОРМАТИЗАЦИИ КНР, ПЕКИН';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://789.ru/' WHERE name = 'ГРУППА 7/89 АССОЦИАЦИЯ';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://vuzpromexpo.ru/' WHERE name = 'ИНКОНСАЛТ К ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.aviasalon.com/' WHERE name = 'АВИАСАЛОН ОАО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://dollsalon.ru/' WHERE name = 'КЛУБ КОЛЛЕКЦИОНЕРОВ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.goldenseasons.ru/' WHERE name = 'ЗОЛОТЫЕ СЕЗОНЫ ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.e-transport.ru/' WHERE name = 'БЕЛТЕКО ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://powx-russia.com/' WHERE name = 'ОВП-РУС ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.watchexpo.ru/' WHERE name = 'ВОЧ МЕДИА ООО';
			UPDATE tbl_organizer o  SET o.linkToTheSiteOrganizers = 'http://www.mir-forum.ru/' WHERE name = 'МЕГА-ЭКСПО ООО';


			update tbl_organizer o set o.name = 'КАЗАНСКАЯ ЯРМАРКА ОАО' where o.name = 'КАЗАНСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР ОАО';
			update tbl_organizer o set o.name = 'КАЗАНСКАЯ ЯРМАРКА ОАО' where o.name = 'КАЗАНСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://www.expokazan.ru/'
			WHERE name = 'КАЗАНСКАЯ ЯРМАРКА ОАО';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://kubanexpocentr.ru/'
			WHERE name = 'КУБАНЬЭКСПОЦЕНТР ВЫСТАВОЧНЫЙ ЦЕНТР';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://www.днт-тверь.рф/'
			WHERE name = 'ТВЕРСКОЙ ОБЛАСТНОЙ ДОМ НАУКИ И ТЕХНИКИ';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://arvd.ru/'
			WHERE name = 'АО АГЕНТСТВО РАЗВИТИЯ И ИНВЕСТИЦИЙ ОМСКОЙ';

			update tbl_organizer o set o.name = 'ПЕРМСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР' where o.name = 'ВЫСТАВОЧНЫЙ ЦЕНТР ПЕРМСКАЯ ЯРМАРКА';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://www.expoperm.ru/'
			WHERE name = 'ПЕРМСКАЯ ЯРМАРКА ВЫСТАВОЧНЫЙ ЦЕНТР';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'https://vdnh.ru/'
			WHERE name = 'ГАО ВВЦ ОАО';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://www.armit.ru/'
			WHERE name = 'АССОЦИАЦИЯ РАЗВИТИЯ МЕДИЦИНСКИХ ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ (АРМИТ)';


			update tbl_organizer o set o.name = 'СИБЭКСПОСЕРВИС-Н ВЫСТАВОЧНАЯ КОМПАНИЯ ООО, САХАЭКСПОСЕРВИС ВЫСТАВОЧНАЯ КОМПАНИЯ ООО' where o.name = 'САХАЭКСПОСЕРВИС ВЫСТАВОЧНАЯ КОМПАНИЯ ООО, СИБЭКСПОСЕРВИС-Н ВЫСТАВОЧНАЯ КОМПАНИЯ ООО';

			update tbl_organizer o set o.name = 'ФЕШН МЕССЕ ПИРМАЗЕНС ООО' where o.name = 'ФЕШН МЕССЕ ПИРМАЗЕНС, ООО';

			UPDATE tbl_organizer o
			SET o.linkToTheSiteOrganizers = 'http://www.kamexpocenter.ru/'
			WHERE name = 'КАМЧАТСКИЙ ВЫСТАВОЧНО-ИНВЕСТИЦИОННЫЙ ЦЕНТР КРАЕВОЕ ГОСУДАРСТВЕННОЕ АВТОНОМНОЕ УЧРЕЖДЕНИЕ';
		";
	}
}