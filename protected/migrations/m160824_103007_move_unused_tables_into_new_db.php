<?php

class m160824_103007_move_unused_tables_into_new_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbTz) = explode('=', Yii::app()->dbTz->connectionString);

        return "
            RENAME TABLE {{additionaldatafortz}} TO {$dbTz}.tbl_additionaldatafortz;
            RENAME TABLE {{answer}} TO {$dbTz}.tbl_answer;
            RENAME TABLE {{auction}} TO {$dbTz}.tbl_auction;
            RENAME TABLE {{auctionhasfile}} TO {$dbTz}.tbl_auctionhasfile;
            RENAME TABLE {{auctionhasnotification}} TO {$dbTz}.tbl_auctionhasnotification;
            RENAME TABLE {{auctionhasquestion}} TO {$dbTz}.tbl_auctionhasquestion;
            RENAME TABLE {{bid}} TO {$dbTz}.tbl_bid;
            RENAME TABLE {{bidhasfile}} TO {$dbTz}.tbl_bidhasfile;
            RENAME TABLE {{changefairdate}} TO {$dbTz}.tbl_changefairdate;
            RENAME TABLE {{choosingcontractor}} TO {$dbTz}.tbl_choosingcontractor;
            RENAME TABLE {{comment}} TO {$dbTz}.tbl_comment;
            RENAME TABLE {{commenthasfile}} TO {$dbTz}.tbl_commenthasfile;
            RENAME TABLE {{communication}} TO {$dbTz}.tbl_communication;
            RENAME TABLE {{communicationelectricity}} TO {$dbTz}.tbl_communicationelectricity;
            RENAME TABLE {{contract}} TO {$dbTz}.tbl_contract;
            RENAME TABLE {{decor}} TO {$dbTz}.tbl_decor;
            RENAME TABLE {{documentcontacts}} TO {$dbTz}.tbl_documentcontacts;
            RENAME TABLE {{electricity}} TO {$dbTz}.tbl_electricity;
            RENAME TABLE {{element}} TO {$dbTz}.tbl_element;
            RENAME TABLE {{equipment}} TO {$dbTz}.tbl_equipment;
            RENAME TABLE {{equipmenthasfile}} TO {$dbTz}.tbl_equipmenthasfile;
            RENAME TABLE {{equipmentparams}} TO {$dbTz}.tbl_equipmentparams;
            RENAME TABLE {{event}} TO {$dbTz}.tbl_event;
            RENAME TABLE {{exhibitioncomplexclone}} TO {$dbTz}.tbl_exhibitioncomplexclone;
            RENAME TABLE {{exhibitioncomplexhasblog}} TO {$dbTz}.tbl_exhibitioncomplexhasblog;
            RENAME TABLE {{exhibitionpreparing}} TO {$dbTz}.tbl_exhibitionpreparing;
            RENAME TABLE {{extraservice}} TO {$dbTz}.tbl_extraservice;
            RENAME TABLE {{fairclone}} TO {$dbTz}.tbl_fairclone;
            RENAME TABLE {{fairhascomment}} TO {$dbTz}.tbl_fairhascomment;
            RENAME TABLE {{fairnomination}} TO {$dbTz}.tbl_fairnomination;
            RENAME TABLE {{faq}} TO {$dbTz}.tbl_faq;
            RENAME TABLE {{feedback}} TO {$dbTz}.tbl_feedback;
            RENAME TABLE {{flooring}} TO {$dbTz}.tbl_flooring;
            RENAME TABLE {{flooringgroup}} TO {$dbTz}.tbl_flooringgroup;
            RENAME TABLE {{furniture}} TO {$dbTz}.tbl_furniture;
            RENAME TABLE {{ganttlinks}} TO {$dbTz}.tbl_ganttlinks;
            RENAME TABLE {{identityofstand}} TO {$dbTz}.tbl_identityofstand;
            RENAME TABLE {{lighting}} TO {$dbTz}.tbl_lighting;
            RENAME TABLE {{marketinginformation}} TO {$dbTz}.tbl_marketinginformation;
            RENAME TABLE {{message}} TO {$dbTz}.tbl_message;
            RENAME TABLE {{messagehasfile}} TO {$dbTz}.tbl_messagehasfile;
            RENAME TABLE {{modeling}} TO {$dbTz}.tbl_modeling;
            RENAME TABLE {{modelinghasfurniture}} TO {$dbTz}.tbl_modelinghasfurniture;
            RENAME TABLE {{modelinghasmultimedia}} TO {$dbTz}.tbl_modelinghasmultimedia;
            RENAME TABLE {{multimedia}} TO {$dbTz}.tbl_multimedia;
            RENAME TABLE {{news}} TO {$dbTz}.tbl_news;
            RENAME TABLE {{newshascomment}} TO {$dbTz}.tbl_newshascomment;
            RENAME TABLE {{newshasfile}} TO {$dbTz}.tbl_newshasfile;
            RENAME TABLE {{objectivesconcept}} TO {$dbTz}.tbl_objectivesconcept;
            RENAME TABLE {{organizationofarrival}} TO {$dbTz}.tbl_organizationofarrival;
            RENAME TABLE {{organizationofmissions}} TO {$dbTz}.tbl_organizationofmissions;
            RENAME TABLE {{organizerusers}} TO {$dbTz}.tbl_organizerusers;
            RENAME TABLE {{page}} TO {$dbTz}.tbl_page;
            RENAME TABLE {{pagehasfile}} TO {$dbTz}.tbl_pagehasfile;
            RENAME TABLE {{passport}} TO {$dbTz}.tbl_passport;
            RENAME TABLE {{performerhasservices}} TO {$dbTz}.tbl_performerhasservices;
            RENAME TABLE {{podium}} TO {$dbTz}.tbl_podium;
            RENAME TABLE {{postexhibitionofwork}} TO {$dbTz}.tbl_postexhibitionofwork;
            RENAME TABLE {{programandrentalspace}} TO {$dbTz}.tbl_programandrentalspace;
            RENAME TABLE {{question}} TO {$dbTz}.tbl_question;
            RENAME TABLE {{responsible}} TO {$dbTz}.tbl_responsible;
            RENAME TABLE {{review}} TO {$dbTz}.tbl_review;
            RENAME TABLE {{services}} TO {$dbTz}.tbl_services;
            RENAME TABLE {{servicesstandmakingwork}} TO {$dbTz}.tbl_servicesstandmakingwork;
            RENAME TABLE {{showcase}} TO {$dbTz}.tbl_showcase;
            RENAME TABLE {{standdesign}} TO {$dbTz}.tbl_standdesign;
            RENAME TABLE {{standdesignhasfile}} TO {$dbTz}.tbl_standdesignhasfile;
            RENAME TABLE {{subtype}} TO {$dbTz}.tbl_subtype;
            RENAME TABLE {{suspendedconstruction}} TO {$dbTz}.tbl_suspendedconstruction;
            RENAME TABLE {{technic}} TO {$dbTz}.tbl_technic;
            RENAME TABLE {{tender}} TO {$dbTz}.tbl_tender;
            RENAME TABLE {{trdecor}} TO {$dbTz}.tbl_trdecor;
            RENAME TABLE {{trelectricity}} TO {$dbTz}.tbl_trelectricity;
            RENAME TABLE {{trelement}} TO {$dbTz}.tbl_trelement;
            RENAME TABLE {{trequipment}} TO {$dbTz}.tbl_trequipment;
            RENAME TABLE {{trequipmentparams}} TO {$dbTz}.tbl_trequipmentparams;
            RENAME TABLE {{trextraservice}} TO {$dbTz}.tbl_trextraservice;
            RENAME TABLE {{trflooring}} TO {$dbTz}.tbl_trflooring;
            RENAME TABLE {{trfurniture}} TO {$dbTz}.tbl_trfurniture;
            RENAME TABLE {{trmarketinginformation}} TO {$dbTz}.tbl_trmarketinginformation;
            RENAME TABLE {{trnews}} TO {$dbTz}.tbl_trnews;
            RENAME TABLE {{trpage}} TO {$dbTz}.tbl_trpage;
            RENAME TABLE {{trpodium}} TO {$dbTz}.tbl_trpodium;
            RENAME TABLE {{trservices}} TO {$dbTz}.tbl_trservices;
            RENAME TABLE {{trsubtype}} TO {$dbTz}.tbl_trsubtype;
            RENAME TABLE {{trusefulinformation}} TO {$dbTz}.tbl_trusefulinformation;
            RENAME TABLE {{trwalltype}} TO {$dbTz}.tbl_trwalltype;
            RENAME TABLE {{tz}} TO {$dbTz}.tbl_tz;
            RENAME TABLE {{tzadditionaldata}} TO {$dbTz}.tbl_tzadditionaldata;
            RENAME TABLE {{tzhasfile}} TO {$dbTz}.tbl_tzhasfile;
            RENAME TABLE {{tztarget}} TO {$dbTz}.tbl_tztarget;
            RENAME TABLE {{uefexpo}} TO {$dbTz}.tbl_uefexpo;
            RENAME TABLE {{usefulinformation}} TO {$dbTz}.tbl_usefulinformation;
            RENAME TABLE {{usefulinformationhasfile}} TO {$dbTz}.tbl_usefulinformationhasfile;
            RENAME TABLE {{wall}} TO {$dbTz}.tbl_wall;
            RENAME TABLE {{walltype}} TO {$dbTz}.tbl_walltype;
            RENAME TABLE {{zone}} TO {$dbTz}.tbl_zone;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}