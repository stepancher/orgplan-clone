<?php

class m160429_133937__add_column_fairID_to_tbl_documents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {
		return true;
	}

	private function upSql() {

		return "
			ALTER TABLE {{documents}}
			ADD COLUMN `fairId` INT(11) NULL AFTER `active`;

			UPDATE {{documents}} SET `fairId`='184' WHERE `id`='4';
			UPDATE {{documents}} SET `fairId`='184' WHERE `id`='5';
			UPDATE {{documents}} SET `fairId`='184' WHERE `id`='6';
			UPDATE {{documents}} SET `fairId`='184' WHERE `id`='7';

		";
	}

}