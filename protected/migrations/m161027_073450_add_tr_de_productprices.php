<?php

class m161027_073450_add_tr_de_productprices extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trproductcategory}} SET `name` = 'Audio-Video-Technik' WHERE `trParentId` = '7' AND `langId` = 'de';
            UPDATE {{trproductcategory}} SET `name` = 'Catering' WHERE `trParentId` = '8' AND `langId` = 'de';
            UPDATE {{trproductcategory}} SET `name` = 'Polygraphie' WHERE `trParentId` = '9' AND `langId` = 'de';
            UPDATE {{trproductcategory}} SET `name` = 'Dienstleistungen des Promotionspersonals' WHERE `trParentId` = '10' AND `langId` = 'de';
            UPDATE {{trproductcategory}} SET `name` = 'Mobile Messeausstattung' WHERE `trParentId` = '11' AND `langId` = 'de';
            UPDATE {{trproductcategory}} SET `name` = 'Souvenirs' WHERE `trParentId` = '12' AND `langId` = 'de';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}