<?php

class m170307_144650_add_additional_contact_field_to_organizer extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{organizercontactlist}} 
            ADD COLUMN `additionalContactInformation` VARCHAR(255) NULL DEFAULT NULL AFTER `type`,
            ADD COLUMN `countryId` INT(11) NULL DEFAULT NULL AFTER `additionalContactInformation`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}