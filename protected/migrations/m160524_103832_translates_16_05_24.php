<?php

class m160524_103832_translates_16_05_24 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{trattributeclassproperty}} SET `value`='connection(s)' WHERE `trParentId`='1121';
			UPDATE {{trattributeclassproperty}} SET `value`='up to 5 kW' WHERE `trParentId`='1299';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1129', 'en', 'connection(s)');
			UPDATE {{trattributeclassproperty}} SET `value`='up to 10 kW' WHERE `trParentId`='1301';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1130', 'en', 'connection(s)');
			UPDATE {{trattributeclassproperty}} SET `value`='up to 20 kW' WHERE `trParentId`='1303';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1131', 'en', 'connection(s)');
			UPDATE {{trattributeclassproperty}} SET `value`='up to 40 kW' WHERE `trParentId`='1305';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1132', 'en', 'connection(s)');
			UPDATE {{trattributeclassproperty}} SET `value`='up to 60 kW' WHERE `trParentId`='1307';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1122', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='cold water supply and the water drain' WHERE `trParentId`='1309';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1123', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='hot water supply' WHERE `trParentId`='1311';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1124', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='cold water supply and the water drain' WHERE `trParentId`='1313';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1125', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='hot water supply' WHERE `trParentId`='1315';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1126', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='water hose 1/2'' (5 long meters)' WHERE `trParentId`='1317';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1127', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='water drain hose 38x42 mm (5 long meters) ' WHERE `trParentId`='1319';
			UPDATE {{trattributeclassproperty}} SET `value`='Hose laying (water supply and water drain) (5 long meters)' WHERE `trParentId`='1321';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1135', 'en', 'date');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1136', 'en', 'date');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1128', 'en', 'pcs.');
			UPDATE {{trattributeclass}} SET `label`='stand_standard.png' WHERE `id`='1029';
			UPDATE {{trattributeclass}} SET `label`='stand_premium.png' WHERE `id`='1041';
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '1', 'en', 'Заявка №1', 'ЭКСПОНЕНТ');
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '2', 'en', 'Application No.4', 'TECHNICAL AND ENGINEERING CONNECTIONS');
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '3', 'en', 'CONTEST APPLICATION', 'INNOVATIVE AGRICULTURAL MACHINERY CONTEST');
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '4', 'en', 'Application No.2', 'Standard stand');
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '5', 'en', 'Application No.3', 'ANCILLARY EQUIPMENT');
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '7', 'en', 'Application No.5', 'COMMUNICATION SERVICES');
			INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '10', 'en', 'Заявка №9', 'РАЗГРУЗКА/ПОГРУЗКА');
			ALTER TABLE {{proposalelementclass}} 
			DROP COLUMN `pluralLabel`,
			DROP COLUMN `label`;
			INSERT INTO {{trfair}} (`id`, `trParentId`, `langId`, `name`, `description`) VALUES (NULL, '184', 'en', 'Agrosalon', 'International Agricultural Machinery Fair in Russia AGROSALON-2016');
		";
	}
}