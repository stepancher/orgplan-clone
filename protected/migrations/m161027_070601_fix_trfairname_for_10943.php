<?php

class m161027_070601_fix_trfairname_for_10943 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            UPDATE {{trfair}} SET `name`='Protoplanexpo', `description`='Сервис для эффективной подготовки к выставкам. Все инструменты на одной платформе - PROTOPLANEXPO' WHERE `id`='13204';
            UPDATE {{trfair}} SET `name`='Protoplanexpo', `description`='Сервис для эффективной подготовки к выставкам. Все инструменты на одной платформе - PROTOPLANEXPO' WHERE `id`='17620';
            
            DELETE FROM {$dbProposalF10943}.tbl_report WHERE `id`='7';
            DELETE FROM {$dbProposalF10943}.tbl_report WHERE `id`='8';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}