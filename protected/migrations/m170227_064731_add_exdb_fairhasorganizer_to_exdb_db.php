<?php

class m170227_064731_add_exdb_fairhasorganizer_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fair_id INT DEFAULT 0;
                DECLARE organizer_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT t.fairId, t.orgId FROM
                                        (SELECT ef.id AS fairId, eorg.id AS orgId
                                        FROM {$expodataRaw}.expo_data ed 
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = ed.id
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eorg ON eorg.exdbId = ed.organizer_id_en
                                        WHERE ed.organizer_id_en != 0
                                        UNION
                                        SELECT ef.id AS fairId, eorg.id AS orgId
                                        FROM {$expodataRaw}.expo_data ed 
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = ed.id
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eorg ON eorg.exdbId = ed.co_organizer_id_en
                                        WHERE ed.co_organizer_id_en != 0
                                        ) t LEFT JOIN {$expodata}.{{exdbfairhasorganizer}} efho ON efho.fairId = t.fairId AND efho.organizerId = t.orgId
                                        WHERE efho.id IS NULL
                                        ORDER BY t.fairId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    FETCH copy INTO fair_id, organizer_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairhasorganizer}} (`fairId`,`organizerId`) VALUES (fair_id, organizer_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}