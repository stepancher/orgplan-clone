<?php

class m160728_134112_add_sng_organizers extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('Iteca Caspian LLC', '7(99412)447-47-74', 'http://www.iteca.az/?l=ru', '7(99412)447-47-74', '7(99412)447-47-74', '7(99412)447-47-74', '7(99412)447-47-74', '7(99412)447-47-74');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('Caspian Event Organisers', '7(99412)447-47-74', 'http://ceo.az/index_ru.php', '7(99412)447-47-74', '7(99412)447-47-74', '7(99412)447-47-74', '7(99412)447-47-74', '7(99412)447-47-74');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('LOGOS EXPO Center', '7(37410)235-775, 7(37410)229-813', 'http://expo.am/', '7(37410)235-775, 7(37410)229-813', '7(37410)235-775, 7(37410)229-813', '7(37410)235-775, 7(37410)229-813', '7(37410)235-775, 7(37410)229-813', '7(37410)235-775, 7(37410)229-813');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('Зарубеж-Экспо', '7(495)637-50-79, 7(495)637-36-33', 'http://zarubezhexpo.ru/', '7(495)637-50-79, 7(495)637-36-33', '7(495)637-50-79, 7(495)637-36-33', '7(495)637-50-79, 7(495)637-36-33', '7(495)637-50-79, 7(495)637-36-33', '7(495)637-50-79, 7(495)637-36-33');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('Нью Экспо', '7(37477)824-688', 'http://autoexpo.am/', '7(37477)824-688', '7(37477)824-688', '7(37477)824-688', '7(37477)824-688', '7(37477)824-688');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('МинскЭкспо', '7(37517)226-90-84', 'http://www.minskexpo.com/', '7(37517)226-90-84', '7(37517)226-90-84', '7(37517)226-90-84', '7(37517)226-90-84', '7(37517)226-90-84');
			INSERT INTO {{organizer}} (`name`, `linkToTheSiteOrganizers`) VALUES ('ТЕХНИКА И КОММУНИКАЦИИ', 'http://www.tc.by/');
			INSERT INTO {{organizer}} (`name`, `phone`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ЭКСПОЛИСТ', '7(37517)250-12-74', '7(37517)250-12-74', '7(37517)250-12-74', '7(37517)250-12-74', '7(37517)250-12-74', '7(37517)250-12-74');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('БЕЛЭКСПО', '7(37517)334-24-46', 'http://www.belexpo.by/o-kompanii/', '7(37517)334-24-46', '7(37517)334-24-46', '7(37517)334-24-46', '7(37517)334-24-46', '7(37517)334-24-46');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('АЭРКОМБЕЛ', '7(37517)234-32-08', 'http://www.aercom.by', '7(37517)234-32-08', '7(37517)234-32-08', '7(37517)234-32-08', '7(37517)234-32-08', '7(37517)234-32-08');
			INSERT INTO {{organizer}} (`name`) VALUES ('БЕЛТПП');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ЭКСПОСИСТЕМС', '7(37517)237-20-50', 'http://www.exposystems.by/', '7(37517)237-20-50', '7(37517)237-20-50', '7(37517)237-20-50', '7(37517)237-20-50', '7(37517)237-20-50');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ЭКСПОНЕНТ', '7(37517)240-29-65', 'http://exponent.by/', '7(37517)240-29-65', '7(37517)240-29-65', '7(37517)240-29-65', '7(37517)240-29-65', '7(37517)240-29-65');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ЭКСПОФОРУМ', '7(37517)314-34-30', 'http://www.expoforum.by/', '7(37517)314-34-30', '7(37517)314-34-30', '7(37517)314-34-30', '7(37517)314-34-30', '7(37517)314-34-30');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('БЕЛИНТЕРЭКСПО', '7(37517)290-72-55', 'http://www.belinterexpo.by/o-kompanii.html', '7(37517)290-72-55', '7(37517)290-72-55', '7(37517)290-72-55', '7(37517)290-72-55', '7(37517)290-72-55');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('СТУДИЯГОЛД', '7(37517)290-81-92', 'http://www.studiagold.by', '7(37517)290-81-92', '7(37517)290-81-92', '7(37517)290-81-92', '7(37517)290-81-92', '7(37517)290-81-92');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('Worlddidac', '7(4131)311-76-82', 'http://www.worlddidac.org', '7(4131)311-76-82', '7(4131)311-76-82', '7(4131)311-76-82', '7(4131)311-76-82', '7(4131)311-76-82');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('Атакент-Экспо', '7(727)275-09-112', 'http://www.atakentexpo.kz/', '7(727)275-09-112', '7(727)275-09-112', '7(727)275-09-112', '7(727)275-09-112', '7(727)275-09-112');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ITECA Алматы', '7(727)258-34-34', 'http://www.iteca.kz', '7(727)258-34-34', '7(727)258-34-34', '7(727)258-34-34', '7(727)258-34-34', '7(727)258-34-34');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('КАЗЭКСПО', '7(727)250-75-19', 'http://www.kazexpo.kz/', '7(727)250-75-19', '7(727)250-75-19', '7(727)250-75-19', '7(727)250-75-19', '7(727)250-75-19');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('FAIR EXPO', '7(717)254-26-78', 'http://www.fairexpo.kz/', '7(717)254-26-78', '7(717)254-26-78', '7(717)254-26-78', '7(717)254-26-78', '7(717)254-26-78');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('CATEXPO', '7(727)338-90-89', 'http://catexpo.kz/index.php?lang=ru', '7(727)338-90-89', '7(727)338-90-89', '7(727)338-90-89', '7(727)338-90-89', '7(727)338-90-89');
			INSERT INTO {{organizer}} (`name`, `phone`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('D-EXPO', '7(727)329-28-69', '7(727)329-28-69', '7(727)329-28-69', '7(727)329-28-69', '7(727)329-28-69', '7(727)329-28-69');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('АСТАНА-ЭКСПО КС', '7(717)252-42-33', 'http://www.astana-expo.com/', '7(717)252-42-33', '7(717)252-42-33', '7(717)252-42-33', '7(717)252-42-33', '7(717)252-42-33');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('АЙДАХАР ЭКСПО', '7(717)252-29-63', 'http://www.aydaharexpo.com/', '7(717)252-29-63', '7(717)252-29-63', '7(717)252-29-63', '7(717)252-29-63', '7(717)252-29-63');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('CENTRAL ASIA TRADE EXHIBITIONS', '7(727)266-36-80', 'http://www.industryplatform.kz', '7(727)266-36-80', '7(727)266-36-80', '7(727)266-36-80', '7(727)266-36-80', '7(727)266-36-80');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('EXPOGROUP', '7(727)327-24-65', 'http://expogroup.kz/', '7(727)327-24-65', '7(727)327-24-65', '7(727)327-24-65', '7(727)327-24-65', '7(727)327-24-65');
			INSERT INTO {{organizer}} (`name`) VALUES ('ТПП КЫРГЫЗСТАНСКОЙ РЕСПУБЛИКИ');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('MOLDEXPO', '7(3732)281-04-62, 7(3732)281-04-62', 'http://moldexpo.md/', '7(3732)281-04-62, 7(3732)281-04-62', '7(3732)281-04-62, 7(3732)281-04-62', '7(3732)281-04-62, 7(3732)281-04-62', '7(3732)281-04-62, 7(3732)281-04-62', '7(3732)281-04-62, 7(3732)281-04-62');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('TNT PRODUCTIONS', '7(727)250-19-99', 'http://www.tntexpo.kz/', '7(727)250-19-99', '7(727)250-19-99', '7(727)250-19-99', '7(727)250-19-99', '7(727)250-19-99');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('POLIPROJECT EXHIBITIONS', '7(3732)222-20-70', 'http://poliproject.md/', '7(3732)222-20-70', '7(3732)222-20-70', '7(3732)222-20-70', '7(3732)222-20-70', '7(3732)222-20-70');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ITECA ALA TOO', '7(99677)791-19-12', 'http://iteca.kg/', '7(99677)791-19-12', '7(99677)791-19-12', '7(99677)791-19-12', '7(99677)791-19-12', '7(99677)791-19-12');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ITE UZBEKISTAN', '7(99871)113-01-80', 'http://www.ite-uzbekistan.uz/rus/index.php', '7(99871)113-01-80', '7(99871)113-01-80', '7(99871)113-01-80', '7(99871)113-01-80', '7(99871)113-01-80');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('IEG UZBEKISTAN', '7(99871)238-59-88', 'http://ieg.uz/', '7(99871)238-59-88', '7(99871)238-59-88', '7(99871)238-59-88', '7(99871)238-59-88', '7(99871)238-59-88');
			INSERT INTO {{organizer}} (`name`) VALUES ('ЦЕНТРАЛЬНЫЙ БАНК РЕСПУБЛИКИ УЗБЕКИСТАН');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('EXPO POSITION OOO', '7(99871)234-46-72', 'http://www.expoposition.com/', '7(99871)234-46-72', '7(99871)234-46-72', '7(99871)234-46-72', '7(99871)234-46-72', '7(99871)234-46-72');
			INSERT INTO {{organizer}} (`name`, `linkToTheSiteOrganizers`) VALUES ('ТОРГОВО-ПРОМЫШЛЕННАЯ ПАЛАТА ТУРКМЕНИСТАНА', 'http://cci.gov.tm/');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('EXPOGEORGIA', '7(99532)234-11-00', 'http://www.expogeorgia.ge/', '7(99532)234-11-00', '7(99532)234-11-00', '7(99532)234-11-00', '7(99532)234-11-00', '7(99532)234-11-00');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('БИЗНЕС КОНТАКТ', '7(914)959-10-11', 'http://expomongolia.ru', '7(914)959-10-11', '7(914)959-10-11', '7(914)959-10-11', '7(914)959-10-11', '7(914)959-10-11');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('PREMIER EXPO', '7(38044)496-86-45', 'http://www.pe.com.ua', '7(38044)496-86-45', '7(38044)496-86-45', '7(38044)496-86-45', '7(38044)496-86-45', '7(38044)496-86-45');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ЭКСПО-ЮГ-СЕРВИС', '7(38048)777-60-68', 'http://expodessa.com', '7(38048)777-60-68', '7(38048)777-60-68', '7(38048)777-60-68', '7(38048)777-60-68', '7(38048)777-60-68');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('КМКЯ', '7(38044)461-93-42', 'http://www.kmkya.kiev.ua', '7(38044)461-93-42', '7(38044)461-93-42', '7(38044)461-93-42', '7(38044)461-93-42', '7(38044)461-93-42');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('EUROINDEX', '7(38044)461-93-00', 'www.euroindex.ua', '7(38044)461-93-00', '7(38044)461-93-00', '7(38044)461-93-00', '7(38044)461-93-00', '7(38044)461-93-00');
			INSERT INTO {{organizer}} (`name`) VALUES ('СПОРТ ЭКСПО');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('LMT CORPORATION', '7(38044)206-10-15', 'http://www.lmt.kiev.ua/', '7(38044)206-10-15', '7(38044)206-10-15', '7(38044)206-10-15', '7(38044)206-10-15', '7(38044)206-10-15');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('МЭДВИН', '7(38044)501-03-42', 'http://www.medvin.kiev.ua/ru/', '7(38044)501-03-42', '7(38044)501-03-42', '7(38044)501-03-42', '7(38044)501-03-42', '7(38044)501-03-42');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('S-TEAM', '7(38044)366-26-45', 'http://s-team.ua', '7(38044)366-26-45', '7(38044)366-26-45', '7(38044)366-26-45', '7(38044)366-26-45', '7(38044)366-26-45');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('PRIMUS', '7(38044)537-69-99', 'http://www.theprimus.com', '7(38044)537-69-99', '7(38044)537-69-99', '7(38044)537-69-99', '7(38044)537-69-99', '7(38044)537-69-99');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ВЫСТАВОЧНЫЙ МИР', '7(38044)498-42-04', 'http://www.vsvit.com.ua/index.php/ru', '7(38044)498-42-04', '7(38044)498-42-04', '7(38044)498-42-04', '7(38044)498-42-04', '7(38044)498-42-04');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('АГРОИНКОМ', '7(38044)593-19-01', 'http://www.agroinkom.com.ua', '7(38044)593-19-01', '7(38044)593-19-01', '7(38044)593-19-01', '7(38044)593-19-01', '7(38044)593-19-01');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ACCO', '7(38044)456-38-04', 'http://acco.ua', '7(38044)456-38-04', '7(38044)456-38-04', '7(38044)456-38-04', '7(38044)456-38-04', '7(38044)456-38-04');
			INSERT INTO {{organizer}} (`name`, `phone`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ТЕЛЕ-РАДИО-КУРЬЕР', '7(38044)289-79-31', '7(38044)289-79-31', '7(38044)289-79-31', '7(38044)289-79-31', '7(38044)289-79-31', '7(38044)289-79-31');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ОДЕССКИЙ ДОМ', '7(38048)237-17-37', 'http://expohome.com.ua/', '7(38048)237-17-37', '7(38048)237-17-37', '7(38048)237-17-37', '7(38048)237-17-37', '7(38048)237-17-37');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('МВЦ', '7(38044)201-11-61', 'http://mvc-expo.com.ua/ru', '7(38044)201-11-61', '7(38044)201-11-61', '7(38044)201-11-61', '7(38044)201-11-61', '7(38044)201-11-61');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ARTEXPO', '7(38044)254-63-80', 'http://artexpo.ua/ru/', '7(38044)254-63-80', '7(38044)254-63-80', '7(38044)254-63-80', '7(38044)254-63-80', '7(38044)254-63-80');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ПОТОК ЦЕНТРУМ', '7(38044)525-30-45', 'http://potok.activexpo.com.ua', '7(38044)525-30-45', '7(38044)525-30-45', '7(38044)525-30-45', '7(38044)525-30-45', '7(38044)525-30-45');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('PARUS EXPOMEDIA', '7(38044)484-68-91', 'http://pem.com.ua', '7(38044)484-68-91', '7(38044)484-68-91', '7(38044)484-68-91', '7(38044)484-68-91', '7(38044)484-68-91');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ДОМИНАНТА', '7(365)254-14-04', 'http://www.dominanta-expo.com', '7(365)254-14-04', '7(365)254-14-04', '7(365)254-14-04', '7(365)254-14-04', '7(365)254-14-04');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ФОРУМ. КРЫМСКИЕ ВЫСТАВКИ', '7(365)254-60-66', 'http://expoforum.biz', '7(365)254-60-66', '7(365)254-60-66', '7(365)254-60-66', '7(365)254-60-66', '7(365)254-60-66');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('ЭКСПОКРЫМ', '7(365)262-06-70', 'http://expocrimea.com', '7(365)262-06-70', '7(365)262-06-70', '7(365)262-06-70', '7(365)262-06-70', '7(365)262-06-70');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('КРЫМ-ЮГ РОССИИ', '7(978)137-38-30', 'http://krymyug.ru', '7(978)137-38-30', '7(978)137-38-30', '7(978)137-38-30', '7(978)137-38-30', '7(978)137-38-30');
			INSERT INTO {{organizer}} (`name`, `phone`, `linkToTheSiteOrganizers`, `exhibitionManagement`, `servicesBuilding`, `advertisingServicesTheFair`, `servicesLoadingAndUnloading`, `foodServices`) VALUES ('КРЫМ-ЭКСПО', '7(978)767-02-12', 'http://crimea-expo.com', '7(978)767-02-12', '7(978)767-02-12', '7(978)767-02-12', '7(978)767-02-12', '7(978)767-02-12');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}