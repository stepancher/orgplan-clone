<?php

class m160726_071050_add_report_columns extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '

			ALTER TABLE {{proposalelementclass}}
				DROP COLUMN `reportAttributeClass`,
				DROP COLUMN `emptyJSON`,
				DROP COLUMN `reportJSON`;

			ALTER TABLE {{proposalelement}}
				DROP COLUMN `treeJSON`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAlterTable(){
		return "

			ALTER TABLE {{proposalelementclass}}
				ADD COLUMN `reportJSON` MEDIUMTEXT NULL AFTER `priority`,
				ADD COLUMN `emptyJSON` MEDIUMTEXT NULL AFTER `reportJSON`,
				ADD COLUMN `reportAttributeClass` MEDIUMTEXT NULL AFTER `emptyJSON`;

			ALTER TABLE {{proposalelement}}
				ADD COLUMN `treeJSON` MEDIUMTEXT NULL AFTER `seen`;

				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2308';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2316';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2324';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2332';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2340';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2348';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2356';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2364';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2372';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2380';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2298';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2299';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2300';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2301';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2302';
				UPDATE {{attributeclass}} SET `class`='empty' WHERE `id`='2303';
		";
	}
}