<?php

class m151030_102840_create_documents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{documents}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS {{documents}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`name` varchar(255),
				`description` text,
				`type` int(11),
				`value` varchar(255)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;

			INSERT INTO {{documents}} (`id`, `name`, `description`, `type`, `value`) VALUES
			(1, "Анкета посетителя выставки", "Описание анкеты посетителя выставки", 2, "/ru/site/questionnaire/")
			ON DUPLICATE KEY UPDATE `name` = "Анкета посетителя выставки",
									`description` = "Описание анкеты посетителя выставки",
									`type` = 2,
									`value` = "/ru/site/questionnaire/";
		';
	}
}