<?php

class m160418_093559_update_data_class_field_in_attributeClass extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `class`='headerH1Invisible' WHERE `id`='315';

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('365','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('366','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('367','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('368','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('369','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('370','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('371','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('372','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('373','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('374','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('375','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('376','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('377','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('378','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('379','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('380','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('381','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('382','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('383','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('384','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('385','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('386','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('387','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('388','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('389','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('390','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('391','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('392','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('393','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('394','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('395','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('396','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('397','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('398','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('399','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('400','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('401','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('402','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('403','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('404','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('405','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('406','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('407','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('408','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('409','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('410','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('411','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('412','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('413','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('414','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('415','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('416','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('417','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('418','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('419','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('420','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('421','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('422','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('423','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('424','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('425','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('426','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('427','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('428','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('429','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('430','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('431','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('432','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('433','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('434','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('435','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('436','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('437','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('438','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('441','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('442','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('443','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('444','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('445','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('446','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('447','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('448','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('449','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('450','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('451','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('452','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('453','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('454','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('455','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('456','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('457','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('458','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('459','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('460','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('461','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('462','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('463','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('464','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('465','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('466','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('467','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('468','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('469','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('470','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('471','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('472','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('473','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('474','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('476','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('477','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('478','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('479','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('480','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('481','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('482','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('483','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('484','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('485','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('486','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('487','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('488','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('489','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('490','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('491','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('492','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('493','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('494','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('495','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('496','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('497','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('498','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('499','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('500','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('501','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('502','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('503','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('504','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('505','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('506','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('508','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('509','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('510','шт.', 'unitTitle');
	    ";
	}
}