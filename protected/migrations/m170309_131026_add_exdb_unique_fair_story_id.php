<?php

class m170309_131026_add_exdb_unique_fair_story_id extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_unique_story_id_to_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_unique_story_id_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_exdb_id INT DEFAULT 0;
                DECLARE f_story_id INT DEFAULT 0;
                DECLARE exdb_id_temp INT DEFAULT 0;
                DECLARE f_id_temp INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT f.id, f.exdbId, f.storyId
                                        FROM {$db}.{{fair}} f 
                                        WHERE f.exdbId IS NOT NULL 
                                        ORDER BY f.exdbId, YEAR(f.beginDate);
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, f_exdb_id, f_story_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF exdb_id_temp != f_exdb_id THEN
                        SET f_id_temp := f_id;
                        SET exdb_id_temp := f_exdb_id;
                    END IF;
                    
                    IF f_story_id != f_id_temp THEN
                        START TRANSACTION;
                            UPDATE {$db}.{{fair}} SET `storyId` = f_id_temp WHERE `id` = f_id;
                        COMMIT;
                    END IF;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_unique_story_id_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}