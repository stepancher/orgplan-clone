<?php

class m170220_150640_add_exdb_organizerhasassociation_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        
        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_organizerhasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_organizerhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT orgcomp.id, assoc.id AS assId
                                        FROM
                                        (SELECT org.id, org.member_of_id_en AS assId, org.member_of_name_en AS assName
                                        FROM {$expodataRaw}.expo_org org
                                        UNION
                                        SELECT org.id, org.member_of_second_id_en AS assId, org.member_of_second_name_en AS assName
                                        FROM {$expodataRaw}.expo_org org
                                        UNION
                                        SELECT org.id, org.member_of_3_id_en AS assId, org.member_of_3_name_en AS assName
                                        FROM {$expodataRaw}.expo_org org
                                        UNION
                                        SELECT org.id, org.member_of_4_id_en AS assId, org.member_of_4_name_en AS assName
                                        FROM {$expodataRaw}.expo_org org
                                        ) t LEFT JOIN {$expodata}.{{exdbassociation}} assoc ON assoc.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} orgcomp ON orgcomp.exdbId = t.id
                                        WHERE t.assId != 0 ORDER BY t.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                    read_loop: LOOP
                    
                        FETCH copy INTO org_id, ass_id;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$expodata}.{{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES (org_id, ass_id);
                        COMMIT;
                        
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_organizerhasassociation_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}