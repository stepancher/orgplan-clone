<?php

class m161205_115119_add_en_description_to_de extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `add_en_description_to_de`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE trf_id INT DEFAULT 0;
                DECLARE trf_description TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT 
                    trf.id, 
                    trf1.description
                    FROM {{trfair}} trf
                    LEFT JOIN {{trfair}} trf1 ON trf1.trParentId = trf.trParentId AND trf1.langId = 'en'
                    WHERE trf.description = '' 
                    AND trf.langId = 'de' 
                    AND trf1.description != '';
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                    read_loop: LOOP
                        FETCH copy INTO trf_id, trf_description;
                    
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            UPDATE {{trfair}} SET `description` = trf_description WHERE `id` = trf_id;
                        COMMIT;
                        
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL add_en_description_to_de();
            DROP PROCEDURE IF EXISTS `add_en_description_to_de`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}