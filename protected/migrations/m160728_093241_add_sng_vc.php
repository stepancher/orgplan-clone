<?php

class m160728_093241_add_sng_vc extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('пр. Г.Алиева, 515', 'ЦЕНТР ВЫСТАВОК И КОНФЕРЕНЦИЙ \"БАКУ ЭКСПО ЦЕНТР\"', '241', '124', '+7(99412)404-48-02', 'http://www.bakuexpocenter.az/en/contacts.php');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('пр. Победителей, 4', 'КС РУП «ДВОРЕЦ СПОРТА»', '243', '126', '+7(37517)226-70-96', 'http://www.sportpalace.by/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('ул.Достык, 3', 'ВЫСТАВОЧНЫЙ ЦЕНТР \"КОРМЕ\"', '248', '129', '+7(717)252-43-12', 'http://korme-expo.kz/#sthash.wF3NhhFV.dpbs');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('ул. Гиочеилор, 1', 'МВЦ \"MOLDEXPO\" АО', '255', '136', '+7(721)241-11-13', 'http://stadion.kz/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('Амира Темура улица, 107', 'УЗЭКСПОЦЕНТР НВК', '257', '138', '+7(99871)234-54-40', 'http://www.uzexpocentre.uz/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('Церетели, 118', 'EXPOGEORGIA', '261', '142', '+7(99532)234-11-00', 'http://www.expogeorgia.ge/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('ул.Салютная, 2-Б', 'КИЕВЭКСПОПЛАЗА', '263', '144', '+7(38044)461-99-21', 'http://www.expoplaza.kiev.ua/rus/main');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`, `site`) VALUES ('ул. Казахстанская 1', 'СТАДИОН \"ШАХТЕР\"', '256', '137', '+7(721)241-11-13', 'http://stadion.kz/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `site`) VALUES ('ул.Я.Купалы, 27', 'НАЦИОНАЛЬНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР \"БЕЛЭКСПО\"', '243', '126', 'http://www.belexpo.by/o-kompanii/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `site`) VALUES ('ул. Беруни, 41', 'Республиканский центр выставочно-ярмарочной торговли «УзКургазмаСавдо»', '257', '138', 'http://www.ksym.uz/');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `site`) VALUES ('Броварской пр. 15', 'МВЦ', '263', '144', 'http://mvc-expo.com.ua/ru');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `site`) VALUES ('просп. Победы, 40-Б', 'ВЦ АККО ИНТЕРЕШНЛ', '263', '144', 'http://acco.ua');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`) VALUES ('ул. Ульяновская, 35/31', 'ГОСУДАРСТВЕННОЕ УЧРЕЖДЕНИЕ КУЛЬТУРЫ \"ДВОРЕЦ ИСКУССТВ\" Г.БОБРУЙСКА', '244', '127', '+7(37522)572-60-95');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`) VALUES ('29 А мкр.', 'Grand Nur Plaza Hotel & Convention Centre', '251', '132', '+7(729)240-56-01');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`) VALUES ('ул. Мазепы Ивана, 13', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА', '263', '144', '+7(38044)280‑14-20');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`, `phone`) VALUES ('ул. Приморская, 6', 'ОДЕССКИЙ МОРСКОЙ ПОРТ', '264', '145', '+7(38048)729-44-47');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. А. Акопяна 3', 'ВЫСТАВОЧНЫЙ КОМПЛЕКС \"ЕРЕВАН EXPO\"', '242', '125');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('пр-т Победителей, 20/2', 'ФУТБОЛЬНЫЙ МАНЕЖ', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('пр.Дзержинского, 83', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул.30 лет Победы, 1А', 'СК \"ОЛИМПИЕЦ\"', '245', '127');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Ленинская, 39', 'КУЛЬТУРНЫЙ ЦЕНТР \"ЮНОСТЬ\"', '246', '127');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('пр.Победителей, 59', 'ГОСТИНИЦА ВИКТОРИЯ', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('Тимирязева, 42', 'ВЦ \"АТАКЕНТ\"', '249', '130');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('УЛ СМАГУЛОВА, 5А', 'СПОРТКОМПЛЕКС \"АТЫРАУ\"', '250', '131');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('улица Сатпаева, 15 Б', 'ГОСТИНИЦА \"RENAISSANCE ATYRAU HOTEL\"', '250', '131');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('просп. Тауелсыздык, 52', 'ДВОРЕЦ НЕЗАВИСИМОСТИ', '248', '129');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('проспект Астана 12', 'ВЦ «Корме Орталыгы»', '248', '129');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('пр. Абулхаир-хана, 52', 'Дворец спорта \"Коныс\"', '252', '133');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('пл. Конституции, 1', 'Теннисный центр \"Энергетик\"', '253', '134');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('Самал 1, д.44', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ', '249', '130');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('пр. Абая, 44, уг. ул. Байтурсынова', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА', '249', '130');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('район Международного аэропорта “Астана”', 'АВИАЦИОННАЯ БАЗА ВС РК', '248', '129');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('М.Чиботарь, 16', 'PALATUL REPABLICII', '255', '136');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул.Ахунбаева, 97', 'МАНЕЖ КГАФКИС', '254', '135');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Т. Молдо, 40', 'ДВОРЕЦ СПОРТА', '254', '135');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('проспект Мустакиллик, 2', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ', '257', '138');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('проспект Чандыбиль шаёлы, 143', 'ТПП ТУРКМЕНИСТАНА', '259', '140');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Салютная, 2-Б', 'ВЫСТАВОЧНЫЙ ЦЕНТР КИЕВЭКСПОПЛАЗА', '263', '144');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Приморская, 6', 'Концертно-выставочный Зал Одесского Морвокзала', '264', '145');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('улица Дерибасовская', 'OPEN-AIR', '264', '145');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('Французский бульвар 33', 'ОДЕССКАЯ КИНОСТУДИЯ', '264', '145');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('парк Дружбы Народов, проспект Генерала Ватутина', 'Яхт-клуб \"Червона Калина\"', '263', '144');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Пантелеймоновская, 3', 'Одесский академический театр музыкальной комедии имени Михаила Водяного', '264', '145');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Большая Васильковская, 55', 'НСК ОЛИМПИЙСКИЙ', '263', '144');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Большая Житомирская, 33', 'ТПП УКРАИНЫ', '263', '144');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Киевская, 115', 'ДКП СК \"ДРУЖБА\"', '265', '93');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Дражинского 50', 'ГК \"ЯЛТА-ТУРИСТ\"', '159', '93');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('Ул.Вакуленчука 29', 'ТРЦ МУССОН', '266', '93');
				INSERT INTO {{exhibitioncomplex}} (`street`, `description`, `cityId`, `regionId`) VALUES ('ул. Байзакова, 280', 'ALMATY TOWERS', '249', '130');
				INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('АГ. ЩОМЫСЛИЦА, 28', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА', '254', '135');
				INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('Салон \"Автотеххизмат-Ф\"', '258', '139');
				INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('НАЦИОНАЛЬНАЯ ТУРИСТИЧЕСКАЯ ЗОНА «АВАЗА»', '260', '141');
				INSERT INTO {{exhibitioncomplex}} (`description`, `cityId`, `regionId`) VALUES ('ТПП МОНГОЛИИ', '262', '143');
				INSERT INTO {{exhibitioncomplex}} (`street`, `cityId`, `regionId`) VALUES ('ул. Чкалова 7', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`street`, `cityId`, `regionId`) VALUES ('бульвар Тракторостроителей', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`street`, `cityId`, `regionId`) VALUES ('пл. Советская', '247', '128');
				INSERT INTO {{exhibitioncomplex}} (`street`, `cityId`, `regionId`) VALUES ('Старовиленский тракт, 41', '243', '126');
				INSERT INTO {{exhibitioncomplex}} (`street`, `cityId`, `regionId`) VALUES ('ул. Козлова, 3', '243', '126');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}