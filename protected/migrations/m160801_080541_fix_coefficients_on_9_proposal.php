<?php

class m160801_080541_fix_coefficients_on_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('579', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('589', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('591', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('593', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('598', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('602', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '614', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('606', '614', 'inherit', '0');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}