<?php

class m160727_121953_add_sng_city extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('3', 'Baku-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('4', 'Yerevan-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('5', 'Minsk-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('5', 'Mogilev-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('5', 'Grodno-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Astana-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Almaty-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Atyrau-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Mangystau-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Aktobe-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Pavlodar-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('7', 'Bishkek-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('8', 'Kishinev-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'Karaganda-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('9', 'Tashkent-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('9', 'Fergana-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('9', 'Ashgabat-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('10', 'Balkan-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('11', 'Tbilisi-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('12', 'Ulaanbaatar-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('13', 'Kiev-region');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('13', 'Odessa-oblast');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('1', 'Simferopol-municipality');
			INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('1', 'Yalta-municipality');
			INSERT INTO {{district}} (`countryId`) VALUES ('1');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}