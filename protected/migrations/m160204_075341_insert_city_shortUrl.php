<?php

class m160204_075341_insert_city_shortUrl extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{city}} SET `name`='Глазов', `shortUrl`='glazov' WHERE `id`='210';
			UPDATE {{city}} SET `name`='Глазов', `shortUrl`='glazov' WHERE `id`='224';
			UPDATE {{city}} SET `name`='ст. Воронежская', `shortUrl`='st-voronezhskaya' WHERE `id`='211';
			UPDATE {{city}} SET `name`='ст. Воронежская', `shortUrl`='st-voronezhskaya' WHERE `id`='225';
			UPDATE {{city}} SET `name`='Михайловск', `shortUrl`='mikhaylovsk' WHERE `id`='212';
			UPDATE {{city}} SET `name`='Михайловск', `shortUrl`='mikhaylovsk' WHERE `id`='215';
			UPDATE {{city}} SET `name`='Михайловск', `shortUrl`='mikhaylovsk' WHERE `id`='226';
			UPDATE {{city}} SET `name`='Михайловск', `shortUrl`='mikhaylovsk' WHERE `id`='229';
			UPDATE {{city}} SET `name`='Мичуринск-наукоград', `shortUrl`='michurinsk-naukograd' WHERE `id`='213';
			UPDATE {{city}} SET `name`='Мичуринск-наукоград', `shortUrl`='michurinsk-naukograd' WHERE `id`='227';
			UPDATE {{city}} SET `name`='Новороссийск', `shortUrl`='novorossyisk' WHERE `id`='214';
			UPDATE {{city}} SET `name`='Новороссийск', `shortUrl`='novorossyisk' WHERE `id`='228';
			UPDATE {{city}} SET `name`='Прутской п.', `shortUrl`='prutskoy-p' WHERE `id`='216';
			UPDATE {{city}} SET `name`='Прутской п.', `shortUrl`='prutskoy-p' WHERE `id`='230';
			UPDATE {{city}} SET `name`='Знаменск', `shortUrl`='znamensk' WHERE `id`='217';
			UPDATE {{city}} SET `name`='Знаменск', `shortUrl`='znamensk' WHERE `id`='231';
			UPDATE {{city}} SET `name`='Ахтубинск', `shortUrl`='akhtubinsk' WHERE `id`='218';
			UPDATE {{city}} SET `name`='Ахтубинск', `shortUrl`='akhtubinsk' WHERE `id`='232';
			UPDATE {{city}} SET `name`='Армавир', `shortUrl`='armavir' WHERE `id`='219';
			UPDATE {{city}} SET `name`='Армавир', `shortUrl`='armavir' WHERE `id`='233';
			UPDATE {{city}} SET `name`='Шахты', `shortUrl`='shakhty' WHERE `id`='220';
			UPDATE {{city}} SET `name`='Шахты', `shortUrl`='shakhty' WHERE `id`='222';
			UPDATE {{city}} SET `name`='Шахты', `shortUrl`='shakhty' WHERE `id`='234';
			UPDATE {{city}} SET `name`='Шахты', `shortUrl`='shakhty' WHERE `id`='236';
			UPDATE {{city}} SET `name`='Сальск', `shortUrl`='salsk' WHERE `id`='221';
			UPDATE {{city}} SET `name`='Сальск', `shortUrl`='salsk' WHERE `id`='235';
			UPDATE {{city}} SET `name`='Анапа, Витязево', `shortUrl`='anapa-vityazevo' WHERE `id`='223';
			UPDATE {{city}} SET `name`='Анапа, Витязево', `shortUrl`='anapa-vityazevo' WHERE `id`='237';
			UPDATE {{city}} SET `name`='пос.  Санаторий им. Цюрупы', `shortUrl`='pos-sanatoriy-im-tsuryupy' WHERE `id`='238';

		";
	}
}