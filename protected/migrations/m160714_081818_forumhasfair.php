<?php

class m160714_081818_forumhasfair extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			CREATE TABLE {{forumhasfairs}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `forumId` INT(11) NULL DEFAULT NULL,
			  `fairId` INT(11) NULL DEFAULT NULL,
			  PRIMARY KEY (`id`));
		
			ALTER TABLE {{fair}} 
			ADD COLUMN `fairIsForum` TINYINT(4) NULL DEFAULT NULL AFTER `active`;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}