<?php

class m160623_152514_create_summary_table_for_industry extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
		  CREATE TABLE IF NOT EXISTS {{industrysummary}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `industryId` INT(11) NOT NULL,
			  `fairsCount` INT(11) NOT NULL,
			  `datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  `ready` TINYINT NOT NULL DEFAULT 0,
		  PRIMARY KEY (`id`));
		";
	}

	public function downSql()
	{
		return "
		  DROP TABLE IF EXISTS {{industrysummary}};
		";
	}
}