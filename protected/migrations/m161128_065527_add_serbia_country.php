<?php

class m161128_065527_add_serbia_country extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{country}} (`shortUrl`, `code`) VALUES ('rs', 'rs');
            INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('14', 'en', 'Serbia');
            INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('14', 'de', 'Serbien');
            INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('14', 'ru', 'Сербия');

            INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('14', 'belgrade-district');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('54', 'ru', 'Округ Белград');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('54', 'en', 'Belgrade District');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('54', 'de', 'Округ Белград');

            INSERT INTO {{region}} (`districtId`, `shortUrl`, `code`, `active`) VALUES ('54', 'belgrade-region', 'rs-bg', '1');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('152', 'ru', 'Белград', 'belgrade-region');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('152', 'en', 'Belgrade');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('152', 'de', 'Belgrad');

            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('152', 'belgrade');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('269', 'ru', 'Белград');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('269', 'en', 'Belgrade');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('269', 'de', 'Belgrad');
            
            INSERT INTO {{country}} (`shortUrl`, `code`) VALUES ('vn', 'vn');
            INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('15', 'ru', 'Вьетнам');
            INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('15', 'en', 'Vietnam');
            INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('15', 'de', 'Vietnam');

            INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('15', 'hanoi-district');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('55', 'ru', 'Округ Ханой');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('55', 'en', 'Hanoi District');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('55', 'de', 'Округ Ханой');

            INSERT INTO {{region}} (`districtId`, `shortUrl`, `code`, `active`) VALUES ('55', 'hanoi-district', 'vn-hn', '1');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('153', 'ru', 'Ханой', 'hanoi-district');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('153', 'en', 'Hanoi');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('153', 'de', 'Hanoi');

            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('153', 'hanoi');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('270', 'ru', 'Ханой');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('270', 'en', 'Hanoi');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('270', 'de', 'Hanoi');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}