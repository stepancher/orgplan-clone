<?php

class m160923_170958_add_industry_3_analytic4_2 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68665','11887.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68666','864643','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68666','203078','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68667','34905.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68667','38822','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68668','93450','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68668','37200','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68669','361741','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68670','437.6','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68671','19809.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68672','113910','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68673','103525','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68673','22514.7','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68674','666682','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68674','332640','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68675','74464','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68676','2220860','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68676','70438.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68677','807338','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68678','23692.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68678','73434.1','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68679','142959','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68680','19010.4','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68681','54267.4','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68682','67104.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68683','11322','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68684','4405','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68685','2678','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68686','731386','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68687','62039','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68688','19928.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68688','780.6','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68689','20694.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68689','509','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68690','3957.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68691','24658.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68691','11222.8','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68692','10070','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68693','26040','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68694','341070','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68695','39791.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68696','7160920','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68696','55786.6','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68697','2586020','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68697','630645','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68698','1778940','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68698','448753','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68699','450465','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68700','42290500','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68700','2534780','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68701','5218','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68702','43046.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68702','95100','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68703','1426140','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68703','1202110','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68704','20358700','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68704','781969','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68705','4348','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68705','77.5','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68706','12394300','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68706','4096040','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68707','11780600','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68707','6459990','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68708','402.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68709','952','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68710','2061.9','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68711','16119.9','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68712','2328480','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68712','807171','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68713','7796.4','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68714','887015','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68714','340162','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68715','1879990','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68715','46612.1','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68716','407428','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68717','6169110','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68717','858200','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68718','1051.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68719','25032200','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68719','6342600','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68720','40803','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68721','503492','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68721','351284','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68722','46548.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68723','2066180','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68723','1412.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68724','23944400','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68724','1424010','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68725','1968740','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68725','110267','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68726','16718.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68727','18922100','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68727','3660870','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68728','32693.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68729','25590.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68730','440006','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68730','13796.2','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68731','21390.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68731','165797','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68732','21792','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68732','108440','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68733','185502','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68733','4392.4','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68734','5034','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68734','485.4','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68735','33083.1','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68735','7400','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68736','142675','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68736','21925.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68737','415.2','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68738','2971.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68738','52132.8','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68739','754691','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68739','123518','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68740','12800','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68741','2835','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68742','303346','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68743','9535.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68744','3240.8','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68744','4990.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68745','198966','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68746','21869.8','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68746','2299','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68747','437.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68748','3950.2','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68749','2282','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68750','273.8','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68751','99834.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68751','111947','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68752','39873.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68753','27217.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68753','94651.6','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68754','4933.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68755','58095','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68756','31284.9','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68757','59046','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68758','6462.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68759','302906','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68760','44120','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68761','162504','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68762','39764.7','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68762','9068.1','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68763','842.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68764','15751.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68764','8972.2','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68765','1516.4','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68766','13530.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68766','3003','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68767','217990','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68767','3272.4','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68768','4715950','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68768','539079','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68769','60.2','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68770','217219','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68770','6088.2','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68771','1309.8','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68772','28342700','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68772','2011730','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68773','18945.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68774','4527.5','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68775','88222.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68776','3186560','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68776','10934.6','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68777','17975300','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68777','8179180','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68778','9566.6','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68779','408062','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68780','9276790','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68780','1833650','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68781','1736290','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68781','524618','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68782','1571.2','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68782','2417.7','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68783','283995','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68783','27815.5','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68784','3137.6','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68785','7374','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68786','1853360','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68786','96085.2','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68787','28558.1','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68787','9198.4','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68788','59053.3','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68789','334026','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68789','5071.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68790','1204430','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68790','168439','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68791','22695.2','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68792','9840600','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68792','2335540','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68793','293071','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68793','249178','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68794','667504','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68794','69651.1','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68795','105427','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68795','3237.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68796','214771','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68796','17776.3','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68797','7739.1','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68798','5353940','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68798','3881540','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68799','821697','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68799','110521','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68800','211','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68801','4496360','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68801','35987.5','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68802','9965970','2015');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68802','1669280','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68803','50414900','2016');
            INSERT INTO {{gvalue}} (`setId`,`value`,`epoch`) VALUES ('68803','276274000','2015');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('14', '68802');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('14', '68803');
            
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='68663' WHERE `id`='21';
            DELETE FROM {{gstatgrouphasstat}} WHERE `id`='23';
            DELETE FROM {{gstatgrouphasstat}} WHERE `id`='24';
            
            DELETE FROM {{gstatgroup}} WHERE `id`='13';
            UPDATE {{gstatgroup}} SET `id`='13' WHERE `id`='14';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}