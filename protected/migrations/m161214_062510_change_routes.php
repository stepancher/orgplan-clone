<?php

class m161214_062510_change_routes extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93600';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93603';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93607';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93654';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93655';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93656';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93657';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93658';
            UPDATE {{route}} SET `route`='auth/default/auth' WHERE `id`='93659';
            
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='472751';
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='472771';
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='472801';
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='474142';
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='481301';
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='487971';
            UPDATE {{seo}} SET `route`='auth/default/auth' WHERE `id`='493692';
            
            update {{route}} set `route` = 'auth/default/logout' WHERE `route` = 'site/logout';
            update {{seo}} set `route` = 'auth/default/logout' WHERE `route` = 'site/logout';
            
            update {{route}} set `route` = 'auth/default/reg' WHERE `route` = 'site/reg';
            update {{seo}} set `route` = 'auth/default/reg' WHERE `route` = 'site/reg';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}