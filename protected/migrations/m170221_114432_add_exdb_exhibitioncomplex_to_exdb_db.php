<?php

class m170221_114432_add_exdb_exhibitioncomplex_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_exhibitioncomplex_to_exdb_db`;
            
            CREATE PROCEDURE {$expodata}.`add_exdb_exhibitioncomplex_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE vc_name TEXT DEFAULT '';
                DECLARE vc_contact TEXT DEFAULT '';
                DECLARE vc_coordinate TEXT DEFAULT '';
                DECLARE vc_city_id INT DEFAULT 0;
                DECLARE vc_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT gr.id, gr.name_en, gr.contacts_en, gr.gr_lat_lng_en, trc.trParentId as cityId, gr.raw_en
                                        FROM {$expodataRaw}.expo_ground gr
                                        LEFT JOIN {$expodataRaw}.expo_data ed ON ed.expo_loc_id = gr.id
                                        LEFT JOIN {$db}.{{trcity}} trc ON trc.name = ed.city_en AND trc.langId = 'en'
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = gr.id
                                        WHERE ec.id IS NULL GROUP BY gr.id ORDER BY gr.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO vc_id, vc_name, vc_contact, vc_coordinate, vc_city_id, vc_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbexhibitioncomplex}} (`exdbId`,`name`,`contacts`,`coordinates`,`cityId`,`raw`) 
                            VALUES (vc_id, vc_name, vc_contact, vc_coordinate, vc_city_id, vc_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_exhibitioncomplex_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}