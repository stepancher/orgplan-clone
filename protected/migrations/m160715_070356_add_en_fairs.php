<?php

class m160715_070356_add_en_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10816', 'en', 'Indecor Krasnodar', 'Выставка предметов интерьера и декора - INDECOR Krasnodar\n');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10817', 'en', 'Охота и рыбалка', '10 выставка товаров и услуг для охоты и рыболовства - ОХОТА И РЫБАЛКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10818', 'en', 'Православная Русь', 'VII межрегиональная выставка-ярмарка - ПРАВОСЛАВНАЯ РУСЬ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10819', 'en', 'Мир здорового питания', 'Выставка-ярмарка продуктов питания и напитков для здорового образа жизни - МИР ЗДОРОВОГО ПИТАНИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10820', 'en', 'Росмебельпром', 'Международная специализированная выставка материалов, фурнитуры и оборудования для мебельной и интерьерной отрасли - РОСМЕБЕЛЬПРОМ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10821', 'en', 'Fidexpo', '7-я международная мебельно-интерьерная выставка - FIDexpo (в рамках Форума мебельной и деревообрабатывающей промышленности России)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10822', 'en', 'Энергетика и электротехника', 'Выставка энергетического, электротехнического и светотехнического оборудования и технологий - Энергетика и электротехника');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10823', 'en', 'Форум мебельной и деревообрабатывающей промышленности', 'ФОРУМ МЕБЕЛЬНОЙ И ДЕРЕВООБРАБАТЫВАЮЩЕЙ ПРОМЫШЛЕННОСТИ 2017 (в рамках выставочной программы форума: 14-я международная специализированная выставка материалов, фурнитуры и оборудования для мебельно-интерьерной индустрии - РосМебельПром, 7-я международная ме');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10824', 'en', 'Кожа-Обувь-Меха-Технология', '24 (47) Международная ярмарка - КОЖА-ОБУВЬ-МЕХА-ТЕХНОЛОГИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10825', 'en', 'Текстильлегпром', '46 Федеральная оптовая ярмарка товаров - ТЕКСТИЛЬЛЕГПРОМ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10826', 'en', 'Байкальская строительная неделя', 'Выставка строительных технологий, оборудования, материалов и услуг');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10827', 'en', 'Охота. Рыболовство', 'XV специализированная выставка - ОХОТА. РЫБОЛОВСТВО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10828', 'en', 'Туризм. Отдых', 'XV специализированная выставка - ТУРИЗМ. ОТДЫХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10829', 'en', 'Туризм. Отдых', 'XV специализированная выставка - ТУРИЗМ. ОТДЫХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10830', 'en', 'Знания. Профессия. Карьера', '13-я специализированная выставка учебных заведений - ЗНАНИЯ. ПРОФЕССИЯ. КАРЬЕРА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10833', 'en', 'Знания. Профессия. Карьера', '14-я специализированная выставка учебных заведений - ЗНАНИЯ. ПРОФЕССИЯ. КАРЬЕРА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10834', 'en', 'Все для школы', 'Выставка-ярмарка - ВСЁ ДЛЯ ШКОЛЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10835', 'en', 'Золотой возраст', '6-я Выставка-ярмарка товаров и услуг для населения старшего поколения - ЗОЛОТОЙ ВОЗРАСТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10836', 'en', 'Нужные вещи. Стиль и мода', '6-я Выставка-ярмарка модной одежды и обуви - НУЖНЫЕ ВЕЩИ. СТИЛЬ И МОДА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10837', 'en', 'Нужные вещи для любимых', '5-я выставка-ярмарка товаров для женщин и мужчин к весенним праздникам - НУЖНЫЕ ВЕЩИ ДЛЯ ЛЮБИМЫХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10838', 'en', 'Нужные вещи для любимых', '5-я выставка-ярмарка товаров для женщин и мужчин к весенним праздникам - НУЖНЫЕ ВЕЩИ ДЛЯ ЛЮБИМЫХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10839', 'en', 'Дети. Семья. Творчество', 'Специализированная выставка - ДЕТИ. СЕМЬЯ. ТВОРЧЕСТВО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10840', 'en', 'Красота. Здоровье. Молодость', '36-я Межрегиональная с международным участием специализированная выставка-ярмарка - КРАСОТА. ЗДОРОВЬЕ. МОЛОДОСТЬ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10841', 'en', 'Товары в дорогу', '6-я Выставка-ярмарка товаров весеннего ассортимента для путешествий и отдыха - ТОВАРЫ В ДОРОГУ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10843', 'en', 'Ландшафт и быт усадьбы', '13-ая специализированная выставка-ярмарка товаров и услуг по озеленению, ландшафтной архитектуре, флордизайну, благоустройству жилого дома, хозяйственных и досуговых построек, приусадебной территории - ЛАНДШАФТ И БЫТ УСАДЬБЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10844', 'en', 'Дачные хлопоты', '14-я Специализированная выставка-ярмарка - ДАЧНЫЕ ХЛОПОТЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10845', 'en', 'Подарок Санкт-Петербургу', '14-я Межрегиональная выставка-ярмарка произведений декоративно-прикладного искусства, изделий народных художественных промыслов, сувенирно-подарочной продукции - ПОДАРОК САНКТ-ПЕТЕРБУРГУ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10846', 'en', 'Охрана труда', '11-я Межрегиональная специализированная выставка-форум - ОХРАНА ТРУДА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10847', 'en', 'Пиво', 'XХVI международный форум - ПИВО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10848', 'en', 'Твой дом', 'Специализированная выставка-ярмарка - ТВОЙ ДОМ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10849', 'en', 'Ярмарка недвижимости в Сочи', 'II ярмарка недвижимости - ЯРМАРКА НЕДВИЖИМОСТИ В СОЧИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10850', 'en', 'Ярмарка недвижимости в Сочи', 'IV ярмарка недвижимости - ЯРМАРКА НЕДВИЖИМОСТИ В СОЧИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10851', 'en', 'Ярмарка жилья', 'I ярмарка курортной недвижимости - ЯРМАРКА ЖИЛЬЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10852', 'en', 'Пищевые биотехнологии', 'XXIII Международная выставка напитков, продуктов и технологий - ПИЩЕВЫЕ БИОТЕХНОЛОГИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10853', 'en', 'Фестиваль Народных Мастеров И Художников России', 'ФЕСТИВАЛЬ НАРОДНЫХ МАСТЕРОВ И ХУДОЖНИКОВ РОССИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10854', 'en', 'Весенняя строительная ярмарка. Энергоресурсосбережение', 'Выставка - ВЕСЕННЯЯ СТРОИТЕЛЬНАЯ ЯРМАРКА. ЭНЕРГОРЕСУРСОСБЕРЕЖЕНИЕ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10855', 'en', 'Мусульманская ярмарка', 'Выставка - МУСУЛЬМАНСКАЯ ЯРМАРКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10856', 'en', 'Пасхальная ярмарка. Медовый пир', 'Межрегиональная выставка - ПАСХАЛЬНАЯ ЯРМАРКА. МЕДОВЫЙ ПИР');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10857', 'en', 'Модный салон. Весна', '27 межрегиональная выставка-ярмарка - МОДНЫЙ САЛОН. ВЕСНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10858', 'en', 'Энергетика. Энергоэффективность', '19-я специализированная выставка - ЭНЕРГЕТИКА 2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10859', 'en', 'Строительство. Отделочные материалы. Дизайн', '20-я юбилейная специализированная выставка оборудования, стройматериалов, технологий для строительной отрасли - СТРОИТЕЛЬСТВО. ОТДЕЛОЧНЫЕ МАТЕРИАЛЫ. ДИЗАЙН-2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10860', 'en', 'Ярмарка недвижимости. Финансы. Кредит. Инвестиции', '16-я специализированная выставка - ЯРМАРКА НЕДВИЖИМОСТИ. ФИНАНСЫ. КРЕДИТЫ. ИНВЕСТИЦИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10861', 'en', 'Строительство. Отделочные материалы. Дизайн', '20-я юбилейная специализированная выставка оборудования, стройматериалов, технологий для строительной отрасли - СТРОИТЕЛЬСТВО. ОТДЕЛОЧНЫЕ МАТЕРИАЛЫ. ДИЗАЙН-2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10862', 'en', 'Expofood - Весна', 'Специализированная выставка-ярмарка продуктов питания - EXPOFOOD');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10863', 'en', 'Золото летней столицы. Весна', 'X специализированная выставка-ярмарка  - ЗОЛОТО ЛЕТНЕЙ СТОЛИЦЫ. ВЕСНА (в рамках Международного фестиваля \"Красота и грация\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10864', 'en', 'Золото летней столицы. Лето', 'X специализированная выставка-ярмарка  - ЗОЛОТО ЛЕТНЕЙ СТОЛИЦЫ. ЛЕТО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10865', 'en', 'Первомайская Ярмарка', 'IV Всероссийская универсальная ярмарка - ПЕРВОМАЙСКАЯ ЯРМАРКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10866', 'en', 'Sochi yacht show', 'Выставка яхт и катеров - SOCHI YACHT SHOW');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10867', 'en', 'Уральский автосалон. Коммерческий транспорт', 'Выставка - УРАЛЬСКИЙ АВТОСАЛОН. КОММЕРЧЕСКИЙ ТРАНСПОРТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10868', 'en', 'Зеленое хозяйство: весенний сезон', '19-ая специализированная выставка - ЗЕЛЕНОЕ ХОЗЯЙСТВО: Весенний сезон');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10869', 'en', 'Мода и стиль. Казань-лето', 'Специализированная ярмарка-продажа - МОДА И СТИЛЬ. КАЗАНЬ-ЛЕТО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10870', 'en', 'Elite-ювелир', 'Оптово-розничная ювелирная выставка - ELITE-ЮВЕЛИР');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10871', 'en', 'Дачный сезон.коттедж.', '8-я специализированная выставка-ярмарка - ДАЧНЫЙ СЕЗОН.КОТТЕДЖ.');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10872', 'en', 'Связь', '28-я международная выставка информационных и коммуникационных технологий - СВЯЗЬ-2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10873', 'en', 'Ландшафтный дизайн. Малоэтажное домостроение. Ярмарка загородной недвижимости', 'Специализированная выставка - Ландшафтный дизайн. Малоэтажное домостроение. Ярмарка загородной недвижимости');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10874', 'en', 'Навитех', '9-я международная выставка «Навигационные системы, технологии и услуги» - НАВИТЕХ 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10875', 'en', 'СТЛ. Системы транспорта и логистики', '23-я международная выставка транспортных технологий, логистических решений, сервиса и складских систем - СТЛ. СИСТЕМЫ ТРАНСПОРТА И ЛОГИСТИКИ-2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10876', 'en', 'Малоэтажное домостроение. Строительные и отделочные материалы.', 'Специализированная выставка - Малоэтажное домостроение. Строительные и отделочные материалы');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10877', 'en', 'Мир семьи. Страна детства', 'X универсальная выставка товаров и услуг для жизнедеятельности и социальной поддержки семьи и детей - МИР СЕМЬИ. СТРАНА ДЕТСТВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10878', 'en', 'Весна-лето', 'Универсальная выставка-ярмарка - ВЕСНА-ЛЕТО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10879', 'en', 'Пасхальный праздник', '14-я всероссийская выставка-ярмарка - ПАСХАЛЬНЫЙ ПРАЗДНИК (в рамках церковно-общественной выставки-форума \"Православная Русь\"');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10880', 'en', 'Климатакватэкс', 'Специализированная выставка инженерного оборудования и климатической техники - КЛИМАТАКВАТЭКС 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10881', 'en', 'Нижневартовск: строймаркет. Энергетика. ЖКХ', 'Шестнадцатая межрегиональная специализированная выставка - НИЖНЕВАРТОВСК: СТРОЙМАРКЕТ. ЭНЕРГЕТИКА. ЖКХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10882', 'en', 'Beauty show Krasnodar', '16-я выставка косметики, оборудования и аксессуаров для салонов красоты, СПА центров, фитнес-клубов - BEAUTY SHOW KRASNODAR');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10883', 'en', 'Свадебный салон', '9-я выставка товаров и услуг для свадебных и семейных торжеств - СВАДЕБНЫЙ САЛОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10884', 'en', 'Ювелирный салон', 'Выставка ювелирных изделий, драгоценных и полудрагоценных камней, торгового оборудования - ЮВЕЛИРНЫЙ САЛОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10885', 'en', 'Все для бизнеса. Банки. Госконтракт. Информационные и рекламные технологии', 'Выставка - ВСЕ ДЛЯ БИЗНЕСА. БАНКИ. ГОСКОНТРАКТ. ИНФОРМАЦИОННЫЕ И РЕКЛАМНЫЕ ТЕХНОЛОГИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10886', 'en', 'Машиностроение. Металлообработка. Сварка. Проминновации', 'XV Cпециализированная выставка - МАШИНОСТРОЕНИЕ. МЕТАЛЛООБРАБОТКА. СВАРКА. ПРОМИННОВАЦИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10887', 'en', 'Град креста', 'Православная выставка-ярмарка - ГРАД КРЕСТА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10888', 'en', 'Все Для Сада и Огорода', '11 специализированная выставка-ярмарка - ВСЕ ДЛЯ САДА И ОГОРОДА - 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10889', 'en', 'Babytime', 'Выставка - BABYTIME');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10890', 'en', 'Весенняя ярмарка', 'Ярмарка - ВЕСЕННЯЯ ЯРМАРКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10891', 'en', 'Летний сезон', 'Универсальная выставка-ярмарка - ЛЕТНИЙ СЕЗОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10892', 'en', 'Comic con', 'Фестиваль комиксов и компьютерных игр - COMIC CON');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10893', 'en', 'Дентима Сибирь', 'Стоматологическая выставка - Дентима Сибирь');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10894', 'en', 'Защита от коррозии', '20-я Международная выставка-конгресс - Защита от коррозии');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10895', 'en', 'Медима Сибирь', 'Международная медицинская выставка - Медима Сибирь');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10896', 'en', 'Сварка/Welding', '18 Международная выставка - СВАРКА/Welding');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10897', 'en', 'Энергетика и электротехника', 'XXIV Международная специализированная выставка Энергетика и Электротехника (в рамках Российского Международного Энергетического Форума (РМЭФ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10898', 'en', 'Российский международный энергетический форум', 'IV Российский Международный Энергетический Форум (РМЭФ): конгрессная программа и 12 международная специализированная выставка - Энергетика и Электротехника');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10899', 'en', 'Энергетика. Электротехника. Энерго- и ресурсосбережение', '19-ая специализированная выставка - Энергетика. Электротехника. Энерго- и ресурсосбережение (в рамках 18-го международного научно-промышленного форума «Великие реки (экологическая, гидрометеорологическая, энергетическая безопасность)’ICEF»)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10900', 'en', 'Docflow', 'Конференция-выставка по управлению информационными ресурсами и электронным документооборотом - DOCFLOW');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10901', 'en', 'Комплексная безопасность', 'X Международный салон - КОМПЛЕКСНАЯ БЕЗОПАСНОСТЬ 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10902', 'en', 'Строительство и ремонт', 'Межрегиональная специализированная выставка - СТРОИТЕЛЬСТВО И РЕМОНТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10903', 'en', 'Великие реки', 'Специализированая выставка - ВЕЛИКИЕ РЕКИ (в рамках Международного научно-промышленного форума «Великие реки (экологическая, гидрометеорологическая, энергетическая безопасность)» / ICEF)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10904', 'en', 'Чистая вода. Технологии. Оборудование', '19-я Международная выставка -ЧИСТАЯ ВОДА. ТЕХНОЛОГИИ. ОБОРУДОВАНИЕ (в рамках Международного научно-промышленного форума «Великие реки (экологическая, гидрометеорологическая, энергетическая безопасность)» / ICEF)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10905', 'en', 'Для дома и дачи', 'Ярмарка - ДЛЯ ДОМА И ДАЧИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10906', 'en', 'Российский архитектурно-строительный форум', 'Российский архитектурно-строительный форум');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10907', 'en', 'Точные измерения — основа качества и безопасности', '13-й Московский международный инновационный форум - ТОЧНЫЕ ИЗМЕРЕНИЯ — ОСНОВА КАЧЕСТВА И БЕЗОПАСНОСТИ (в рамках форума пройдут выставки: \"MetrolExpo\", \"Control & Diagnostic\", \"ResMetering\", \"LabTest\", \"PromAutomation\"');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10908', 'en', 'Metrolexpo', '12-я Международная выставка средств измерений, испытательного оборудования и метрологического обеспечения - METROLEXPO (в рамках Московского международного инновационного форума - ТОЧНЫЕ ИЗМЕРЕНИЯ — ОСНОВА КАЧЕСТВА И БЕЗОПАСНОСТИ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10909', 'en', 'Control&diagnostic', '5-я Специализированная выставка средств неразрушающего контроля, технической диагностики, КИП и А - CONTROL&DIAGNOSTIC (в рамках Московского международного инновационного форума - ТОЧНЫЕ ИЗМЕРЕНИЯ — ОСНОВА КАЧЕСТВА И БЕЗОПАСНОСТИ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10910', 'en', 'Resmetering', '5-я специализированная выставка коммерческого и технологического учета энергоресурсов - RESMETERING (в рамках Московского международного инновационного форума - ТОЧНЫЕ ИЗМЕРЕНИЯ — ОСНОВА КАЧЕСТВА И БЕЗОПАСНОСТИ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10911', 'en', 'Labtest', '5 -я Специализированная выставка лабораторного оборудования - LABTEST (в рамках Московского международного инновационного форума - ТОЧНЫЕ ИЗМЕРЕНИЯ — ОСНОВА КАЧЕСТВА И БЕЗОПАСНОСТИ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10912', 'en', 'Promautomatic', '5-я Специализированная выставка автоматизированных систем управления технологическими процессами - PROMAUTOMATION (в рамках Московского международного инновационного форума - ТОЧНЫЕ ИЗМЕРЕНИЯ — ОСНОВА КАЧЕСТВА И БЕЗОПАСНОСТИ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10913', 'en', 'Морская индустрия России', 'Международный форум - МОРСКАЯ ИНДУСТРИЯ РОССИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10914', 'en', 'Свет веры православной - Брянск', 'Межрегиональная выставка-ярмарка - СВЕТ ВЕРЫ ПРАВОСЛАВНОЙ - БРЯНСК');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10915', 'en', 'Сибирская Акварель. Sibbeauty', 'Выставка косметики, инструментов и оборудования для салонов красоты - СИБИРСКАЯ АКВАРЕЛЬ. SIBBEAUTY');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10916', 'en', 'Solids Russia', 'Конференция и выставка по технологиям обработки порошковых и сыпучих материалов - SOLIDS RUSSIA 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10917', 'en', 'Стройка', '20-я специализированная выставка строительных материалов, оборудования, технологий и услуг - СТРОЙКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10918', 'en', 'Уральская неделя моды и легкой промышленности', 'Межрегиональная универсальная выставка-ярмарка - УРАЛЬСКАЯ НЕДЕЛЯ МОДЫ И ЛЕГКОЙ ПРОМЫШЛЕННОСТИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10919', 'en', 'Дача. Сад. Огород', 'Межрегиональная ярмарка - ДАЧА. САД. ОГОРОД');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10920', 'en', 'Арх Москва', '22-я Международная выставка архитектуры и дизайна - АРХ МОСКВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10921', 'en', 'Сибирская строительная неделя', 'Всероссийский строительный форум - СИБИРСКАЯ СТРОИТЕЛЬНАЯ НЕДЕЛЯ (В рамках форума проходят выставки:  \"СТРОЙПРОГРЕСС\", \"ИНДУСТРИЯ ЖИЛИЩНОГО И КОММУНАЛЬНОГО ХОЗЯЙСТВА\", \"ЭНЕРГОРЕСУРСОСБЕРЕЖЕНИЕ В СТРОИТЕЛЬСТВЕ И ЖКХ\", \"ПОЖАРНАЯ БЕЗОПАСНОСТЬ В СТРОИТЕЛЬСТВЕ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10922', 'en', 'Стройпрогресс', 'Специализированная выставка с международным участием - СТРОЙПРОГРЕСС(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10923', 'en', 'Индустрия жилищного и коммунального хозяйства', 'Специализированная выставка - ИНДУСТРИЯ ЖИЛИЩНОГО И КОММУНАЛЬНОГО ХОЗЯЙСТВА(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10924', 'en', 'Энергоресурсосбережение в строительстве и ЖКХ', 'Специализированная выставка - ЭНЕРГОРЕСУРСОСБЕРЕЖЕНИЕ В СТРОИТЕЛЬСТВЕ И ЖКХ(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10925', 'en', 'Пожарная безопасность в строительстве', 'Специализированный салон - ПОЖАРНАЯ БЕЗОПАСНОСТЬ В СТРОИТЕЛЬСТВЕ(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10926', 'en', 'Лифты', 'Выставка - конференция - ЛИФТЫ(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10927', 'en', 'Дерево и металлы в строительстве', 'Специализированная выставка с международным участием - ДЕРЕВО И МЕТАЛЛЫ В СТРОИТЕЛЬСТВЕ(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10928', 'en', 'Риэлт - салон', 'Ярмарка недвижимости - РИЭЛТ - САЛОН(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10929', 'en', 'Чистая вода . Реки Сибири . Мир климата', 'ЧИСТАЯ ВОДА . РЕКИ СИБИРИ . МИР КЛИМАТА(в рамках Всероссийского строительного форума \"Сибирская строительная неделя\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10930', 'en', 'Строительство и ремонт', 'Международная специализированная строительная выставка современных технологий и оборудования для стройиндустрии, дорожно - строительной техники, инженерных сетей, а также строительных и отделочных материалов - СТРОИТЕЛЬСТВО И РЕМОНТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10931', 'en', 'Жилищно - коммунальное хозяйство', '17я Специализированная выставка жилищно - коммунального хозяйства и энергосберегающих технологий, спецтехники - ЖИЛИЩНО - КОММУНАЛЬНОЕ ХОЗЯЙСТВО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10932', 'en', 'Строительство и благоустройство', 'Специализированная выставка - СТРОИТЕЛЬСТВО И БЛАГОУСТРОЙСТВО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10933', 'en', 'Helirussia', '10 - я Юбилейная международная выставка вертолетной индустрии - HELIRUSSIA');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10934', 'en', 'Майская', 'Межрегиональная специализированная выставка - ярмарка - МАЙСКАЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10935', 'en', 'Омскредитэкспо', 'Специализированная выставка - ОМСКРЕДИТЭКСПО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10936', 'en', 'Sochi - Build', 'Международный строительный форум - SOCHI - BUILD');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10937', 'en', 'Открой свою Россию', 'Международная выставка активного отдыха и приключенческого туризма - ОТКРОЙ СВОЮ РОССИЮ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10938', 'en', 'Гуфсин', 'Выставка - продажа продукции исправительных учреждений Пермского края - ГУФСИН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10939', 'en', 'Здоровье Семьи', 'Выставка - ярмарка - ЗДОРОВЬЕ СЕМЬИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10940', 'en', 'Vladivostok boat show', 'Международная выставка яхт и катеров - VLADIVOSTOK BOAT SHOW');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10941', 'en', 'Ретро - фест', 'Фестиваль старинных автомобилей и антиквариата - РЕТРО - ФЕСТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10942', 'en', 'Экология Мозга: Искусство Взаимодействия с Окружающей средой', '5 - й Фестиваль здоровья и Международный междисциплинарный конгресс - Экология мозга: искусство взаимодействия с окружающей средой');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10943', 'en', 'Металлообработка', '18 - я международная специализированная выставка «Оборудование, приборы и инструменты для металлообрабатывающей промышленности» - Металлообработка - 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10944', 'en', 'МедФармДиагностика', 'IX МЕЖДУНАРОДНАЯ СПЕЦИАЛИЗИРОВАННАЯ ВЫСТАВКА ОБОРУДОВАНИЯ, ТЕХНИКИ, ФАРМПРЕПАРАТОВ ДЛЯ ДИАГНОСТИКИ ЗАБОЛЕВАНИЙ ЧЕЛОВЕКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10945', 'en', 'Газ . Нефть . Технологии', 'XXV Международная специализированная выставка - ГАЗ . НЕФТЬ . ТЕХНОЛОГИИ - 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10946', 'en', 'Медицина + ', '26 - я международная выставка - МЕДИЦИНА + ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10947', 'en', 'Всероссийская детская выставка', 'Всероссийская детская выставка');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10948', 'en', 'Interfood St . Petersburg', '20 - я Международная выставка продуктов питания, напитков и оборудования для пищевой промышленности - InterFood St . Petersburg');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10949', 'en', 'Город XXI века', 'XVIII Международная специализированная выставка - ГОРОД XXI ВЕКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10950', 'en', 'Business - inform', 'Международная выставка - BUSINESS - INFORM');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10951', 'en', 'Золотая нива', 'Международная агропромышленная выставка - ЗОЛОТАЯ НИВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10952', 'en', 'Ecom Expo', 'Выставка технологий для e - commerce - ECOM EXPO - 16');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10953', 'en', 'Transsiberia', 'Специализированная выставка индустрии транспорта, транспортной, складской логистики и инфраструктуры - TRANSSIBERIA');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10954', 'en', 'Дентима Краснодар', '17 - я стоматологическая выставка - Дентима');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10955', 'en', 'Автосиб', 'Выставка запасных частей, автохимии, автоаксессуаров, оборудования и технического обслуживания автомобилей - АВТОСИБ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10956', 'en', 'Современные системы безопасности - антитеррор', '13 специализированный форум - выставка - СОВРЕМЕННЫЕ СИСТЕМЫ БЕЗОПАСНОСТИ - АНТИТЕРРОР');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10957', 'en', 'Арт - Челябинск', 'Специализированная выставка - АРТ - ЧЕЛЯБИНСК');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10958', 'en', 'Малый и средний бизнес . Инновации . Инвестиции . Нанотехнологии . Бизнес для бизнеса', 'Специализированная выставка - МАЛЫЙ И СРЕДНИЙ БИЗНЕС . ИННОВАЦИИ . ИНВЕСТИЦИИ . НАНОТЕХНОЛОГИИ . БИЗНЕС ДЛЯ БИЗНЕСА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10959', 'en', 'Информационные технологии . Безопасность . Связь', 'XIV межрегиональная специализированная выставка - ИНФОРМАЦИОННЫЕ ТЕХНОЛОГИИ . БЕЗОПАСНОСТЬ . СВЯЗЬ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10960', 'en', 'Реклама и дизайн . Полиграфия', 'Межрегиональная специализированная выставка - РЕКЛАМА И ДИЗАЙН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10961', 'en', 'Здравствуй, лето', 'Универсальная выставка - ярмарка - ЗДРАВСТВУЙ, ЛЕТО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10962', 'en', 'Junwex новый русский стиль', 'XVI международная выставка ювелирных и часовых брендов - JUNWEX НОВЫЙ РУССКИЙ СТИЛЬ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10963', 'en', 'Магазин . Отель . Ресторан', 'XIII межрегиональная специализированная выставка - МАГАЗИН . ОТЕЛЬ . РЕСТОРАН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10964', 'en', 'Петербургский международный инновационный форум пассажирского транспорта', 'ПЕТЕРБУРГСКИЙ МЕЖДУНАРОДНЫЙ ИННОВАЦИОННЫЙ ФОРУМ ПАССАЖИРСКОГО ТРАНСПОРТА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10965', 'en', 'Крафт базар', 'Выставка - ярмарка товаров и услуг для рукоделия, хобби и творчества - КРАФТ БАЗАР');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10966', 'en', 'Крафт базар', 'Выставка - ярмарка товаров и услуг для рукоделия, хобби и творчества - КРАФТ БАЗАР');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10967', 'en', 'Ювелирная весна', '8 Всероссийская специализированная выставка - ЮВЕЛИРНАЯ ВЕСНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10968', 'en', 'Лето', 'Международная туристская выставка - Лето');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10969', 'en', 'Auto show Челябинск', 'Автомобильная выставка - AUTO SHOW ЧЕЛЯБИНСК');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10970', 'en', 'Архитектура . Стройиндустрия дв региона', 'XXI специализированная выставка - АРХИТЕКТУРА . СТРОЙИНДУСТРИЯ ДВ РЕГИОНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10971', 'en', 'Med travel focus', 'Выставка - MED TRAVEL FOCUS(в рамках выставки ЛЕТО)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10972', 'en', 'Sigold', '19 - я Cпециализированная выставка cовременной техники, технологий , товаров , услуг технического назначения для ТЭК Сахалинской области - SIGOLD');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10973', 'en', 'Сахалинстройэкспо', '15 - ая Специализированная выставка современной техники, технологий, услуг и инноваций жилищного строительства, архитектуры, градостроительства и жилищно - коммунального хозяйства - САХАЛИНСТРОЙЭКСПО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10974', 'en', 'Туризм и отдых', 'III Межрегиональная специализированная выставка - ярмарка - ТУРИЗМ И ОТДЫХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10975', 'en', 'Город . Экология', 'Специализированная выставка - ГОРОД . ЭКОЛОГИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10976', 'en', 'Балтийский морской фестиваль', 'Международная специализированная выставка яхт, катеров, оборудования и снаряжения для водных видов спорта, рыбалки, туризма и отдыха - БАЛТИЙСКИЙ МОРСКОЙ ФЕСТИВАЛЬ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10977', 'en', 'Тыва экспо . Весна: строительство . Энергетика . ЖКХ', 'Шестнадцатая межрегиональная универсальная промышленная выставка - ярмарка - ТЫВА ЭКСПО . ВЕСНА: СТРОИТЕЛЬСТВО . ЭНЕРГЕТИКА . ЖКХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10978', 'en', 'Защищенный грунт России', 'XIV cпециализированная выставка - ЗАЩИЩЕННЫЙ ГРУНТ РОССИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10979', 'en', 'Зоомагия', 'Специализированная выставка - ЗООМАГИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10980', 'en', 'Вернисаж премиум недвижимости / Premium estate vernissage', 'Выставка - ВЕРНИСАЖ ПРЕМИУМ НЕДВИЖИМОСТИ | PREMIUM ESTATE VERNISSAGE');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10981', 'en', 'Атомэкспо', 'IX Международный Форум - АТОМЭКСПО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10982', 'en', 'Строительная Техника И Технологии / СТТ', 'Международная специализированная выставка - Строительная Техника и Технологии');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10983', 'en', 'РЗА(релейная защита и автоматика энергосистем)', 'Международная выставка - РЗА(РЕЛЕЙНАЯ ЗАЩИТА И АВТОМАТИКА ЭНЕРГОСИСТЕМ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10984', 'en', 'Interlogistika', 'Международная выставка - INTERLOGISTIKA');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10985', 'en', 'Rusrealexpo / Русреалэкспо', 'Международный выставочный форум инвестиций и проектов развития территорий Российской Федерации - RUSREALEXPO / РУСРЕАЛЭКСПО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10986', 'en', 'Apparel textile salon', 'Международный салон текстиля и аксессуаров для производства одежды - APPAREL TEXTILE SALON');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10987', 'en', 'Home textile salon', 'Международный салон декоративных тканей, домашнего текстиля и интерьера - HOME TEXTILE SALON(В рамках 46 - й Федеральной оптовой ярмарки товаров и оборудования текстильной и легкой промышленности \"Текстильлегпром\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10988', 'en', 'Children''s salon', 'Международный детский салон - CHILDREN’S SALON');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10989', 'en', 'Textillegmash', 'Международный салон оборудования и технологий для текстильной и легкой промышленности - TEXTILLEGMASH');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10990', 'en', 'Techniсal textile and raw materials salon', 'Международный салон технического текстиля, нетканых материалов, защитной одежды и сырьевых ресурсов - TECHNIСAL TEXTILE AND RAW MATERIALS SALON');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10991', 'en', 'Salon of lingerie', 'Международный салон белья - SALON OF LINGERIE');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10992', 'en', 'Salon of knitwear', 'Международный салон трикотажа - SALON OF KNITWEAR');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10993', 'en', 'Garment & Accessories salon', 'Международный салон одежды, головных уборов и аксессуаров - GARMENT & ACCESSORIES SALON');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10994', 'en', 'Недвижимость', 'Специализированная выставка - НЕДВИЖИМОСТЬ - 2016 (в рамках форума «Строительство и архитектура»)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10995', 'en', 'Золотые россыпи', 'Специализированная выставка-ярмарка - ЗОЛОТЫЕ РОССЫПИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10996', 'en', 'Туризм, спорт, отдых. Охоты и рыбалка', 'Специализированная выставка - ТУРИЗМ, СПОРТ, ОТДЫХ. ОХОТЫ И РЫБАЛКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10997', 'en', 'Индустрия красоты', 'Специализированная выставка - ИНДУСТРИЯ КРАСОТЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10998', 'en', 'Дача. Сад. Огород', 'Выставка-ярмарка - Дача. Сад. Огород.');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('10999', 'en', 'Ярмарка кредитов. Вклады. Сбережения', 'Специализированная выставка - ЯРМАРКА КРЕДИТОВ. ВКЛАДЫ. СБЕРЕЖЕНИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11000', 'en', 'Ярмарка кредитов. Вклады. Сбережения', 'Специализированная выставка - ЯРМАРКА КРЕДИТОВ. ВКЛАДЫ. СБЕРЕЖЕНИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11001', 'en', 'Автомир. Коммерческий транспорт', '22-я специализированная выставка - АВТОМИР. КОММЕРЧЕСКИЙ ТРАНСПОРТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11002', 'en', 'Ярмарка кредитов. Вклады. Сбережения', 'Специализированная выставка - ЯРМАРКА КРЕДИТОВ. ВКЛАДЫ. СБЕРЕЖЕНИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11003', 'en', 'Ярмарка недвижимости', 'Специализированная выставка - ЯРМАРКА НЕДВИЖИМОСТИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11004', 'en', 'China Commodity Fair', 'Национальная выставка качественных потребительских товаров из Китая - China Commodity Fair');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11005', 'en', 'DRONE EXPO SHOW', 'Первая в России выставка-шоу дронов и квадрокоптеров - DRONE EXPO SHOW');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11006', 'en', 'Executive mba и обучение для топ-менеджеров', 'Выставка - EXECUTIVE MBA И ОБУЧЕНИЕ ДЛЯ ТОП-МЕНЕДЖЕРОВ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11007', 'en', 'Кузбасский образовательный форум', 'Выставка - КУЗБАССКИЙ ОБРАЗОВАТЕЛЬНЫЙ ФОРУМ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11008', 'en', 'Экспо-уголь', 'Международная выставка-ярмарка - ЭКСПО-УГОЛЬ (в рамках \"Кузбасского промышленного форума\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11009', 'en', 'Энергетика и энергосбережение', 'Международная выставка-ярмарка - ЭНЕРГЕТИКА И ЭНЕРГОСБЕРЕЖЕНИЕ (в рамках \"Кузбасского промышленного форума\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11010', 'en', 'ЭКОТЕК', 'Международная выставка-ярмарка - ЭКОТЕК (в рамках \"Кузбасского промышленного форума\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11011', 'en', 'Dance & art expo', 'Международная специализированная выставка-ярмарка товаров и услуг для танца, театра и здорового образа жизни - DANCE & ART EXPO');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11012', 'en', 'Дорожная и строительная техника и технологии. (дст и технологии)', 'Специализированная выставка - ДОРОЖНАЯ И СТРОИТЕЛЬНАЯ ТЕХНИКА И ТЕХНОЛОГИИ. (ДСТ И ТЕХНОЛОГИИ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11013', 'en', 'Северо-Кавказский Мебельный Салон', 'Специализированная выставка с международным участием - СЕВЕРО-КАВКАЗСКИЙ МЕБЕЛЬНЫЙ САЛОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11014', 'en', 'Фермер Северного Кавказа', 'Сельскохозяйственный форум - ФЕРМЕР СЕВЕРНОГО КАВКАЗА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11015', 'en', 'Малая энергетика', 'Международная специализированная выставка - МАЛАЯ ЭНЕРГЕТИКА ( в рамках 16 Петербургского международного энергетического форума)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11016', 'en', 'Созвездие мастеров', 'Международная выставка-ярмарка - СОЗВЕЗДИЕ МАСТЕРОВ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11017', 'en', 'Всероссийская ярмарка в Набережных Челнах', 'Всероссийская ярмарка продукции предприятий регионов России - ВСЕРОССИЙСКАЯ ЯРМАРКА В НАБЕРЕЖНЫХ ЧЕЛНАХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11018', 'en', 'Всероссийская ярмарка в Набережных Челнах', 'Всероссийская ярмарка продукции предприятий регионов России - ВСЕРОССИЙСКАЯ ЯРМАРКА В НАБЕРЕЖНЫХ ЧЕЛНАХ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11019', 'en', 'Мир и клир', 'XIV Православная международная выставка-ярмарка - МИР И КЛИР');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11020', 'en', 'Нижегородский Край-Земля Серафима Саровского', 'XXVII Международная православная выставка-ярмарка - НИЖЕГОРОДСКОЙ КРАЙ-ЗЕМЛЯ СЕРАФИМА САРОВСКОГО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11021', 'en', 'Радэл-экспо: радиоэлектроника и приборостроение', 'XVI Международная специализированная выставка - РАДЭЛ-ЭКСПО: РАДИОЭЛЕКТРОНИКА И ПРИБОРОСТРОЕНИЕ 2015 (в рамках фрума \"Радиоэлектроника. Приборостроение. Автоматизация\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11022', 'en', 'Новогодний подарок', 'Международная выставка-ярмарка - НОВОГОДНИЙ ПОДАРОК');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11023', 'en', 'IFIN электронные финансовые услуги и технологии', 'Международный Форум - IFIN ЭЛЕКТРОННЫЕ ФИНАНСОВЫЕ УСЛУГИ И ТЕХНОЛОГИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11024', 'en', 'Wan expo весна', 'Фестиваль беременных и младенцев - WAN EXPO ВЕСНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11025', 'en', 'Шарм. Beauty shop', '20-я Межрегиональная выставка - ШАРМ. BEAUTY SHOP');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11026', 'en', 'Благословенная Самара', 'Православная выставка - БЛАГОСЛОВЕННАЯ САМАРА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11027', 'en', 'Благословенная Самара', 'Православная выставка - БЛАГОСЛОВЕННАЯ САМАРА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11028', 'en', 'Ваше здоровье', 'I специализированная выставка  - ВАШЕ ЗДОРОВЬЕ (в рамках форума МЕДИЦИНА И ФАРМАЦИЯ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11029', 'en', 'Дизайн и реклама', '23-я специализированная выставка рекламной индустрии - ДИЗАЙН И РЕКЛАМА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11030', 'en', 'Garden фест', 'Выставка - GARDEN ФЕСТ (в рамках Международной выставки архитектуры и дизайна \"АРХ МОСКВА\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11031', 'en', 'Обувь. Мир Кожи. Весна', '46-я международная выставка обуви и готовых изделий из кожи - ОБУВЬ. МИР КОЖИ. ВЕСНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11032', 'en', '5pEXPO', 'X Международный форум выставочной индустрии - 5pEXPO-2018');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11033', 'en', 'Мир стекла', '19-я международная выставка - МИР СТЕКЛА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11034', 'en', 'Индустрия детского отдыха', 'Специализированная выставка - ИНДУСТРИЯ ДЕТСКОГО ОТДЫХА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11035', 'en', 'Индустрия детского отдыха', 'Специализированная выставка - ИНДУСТРИЯ ДЕТСКОГО ОТДЫХА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11036', 'en', 'AutoForum Russia', 'Форум автомобильной промышленности России - AUTOFORUM RUSSIA (В рамках форума проходит выставка «Autoinvest & Autosales»)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11037', 'en', 'AutoForum Russia', 'Форум автомобильной промышленности России - AUTOFORUM RUSSIA (В рамках форума проходит выставка «Autoinvest & Autosales»)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11038', 'en', 'Югорский рыбный фестиваль', 'XVII выставка-ярмарка - ЮГОРСКИЙ РЫБНЫЙ ФЕСТИВАЛЬ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11039', 'en', 'Образование и карьера', 'Специализированная выставка - ОБРАЗОВАНИЕ И КАРЬЕРА 2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11040', 'en', 'Золото Югры', 'IV выставка-продажа  - ЗОЛОТО ЮГРЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11041', 'en', 'Золото Югры', 'V выставка-продажа  - ЗОЛОТО ЮГРЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11042', 'en', 'Дары югры', 'Выставка-ярмарка - ДАРЫ ЮГРЫ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11043', 'en', 'Уральская строительная неделя', 'XVII Специализированная выставка - УРАЛЬСКАЯ СТРОИТЕЛЬНАЯ НЕДЕЛЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11044', 'en', 'Denkmal Москва', 'Международная специализированная выставка по сохранению, реставрации, использованию и популяризации культурного наследия - DENKMAL МОСКВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11045', 'en', 'Ваше здоровье', 'Выставка-ярмарка - ВАШЕ ЗДОРОВЬЕ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11046', 'en', 'Медицина и фармация', 'Поволжский специализированный форум - МЕДИЦИНА И ФАРМАЦИЯ (в рамках форума проходит выставки ВАШЕ ЗДОРОВЬЕ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11047', 'en', 'Ваше здоровье', 'II специализированная выставка  - ВАШЕ ЗДОРОВЬЕ (в рамках форума МЕДИЦИНА И ФАРМАЦИЯ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11048', 'en', 'Золото летней столицы. Весна', 'XI специализированная выставка-ярмарка  - ЗОЛОТО ЛЕТНЕЙ СТОЛИЦЫ. ВЕСНА (в рамках Международного фестиваля \"Красота и грация\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11049', 'en', 'Ярмарка жилья', 'II ярмарка курортной недвижимости - ЯРМАРКА ЖИЛЬЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11050', 'en', 'Art weekend', 'Выставка-ярмарка - ART WEEKEND');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11051', 'en', 'Международные дни поля в Поволжье', 'ВЫСТАВКА-ФОРУМ - МЕЖДУНАРОДНЫЕ ДНИ ПОЛЯ В ПОВОЛЖЬЕ.');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11052', 'en', 'Пикник атмосфера творчества', 'Фестиваль - ПИКНИК АТМОСФЕРА ТВОРЧЕСТВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11053', 'en', 'День воронежского поля', 'Межрегинальная выставка-демонстрация - ДЕНЬ ВОРОНЕЖСКОГО ПОЛЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11054', 'en', 'Югра малоэтажная', 'Специализированная выставка - ЮГРА МАЛОЭТАЖНАЯ (В рамках ЮГОРСКОГО ПРОМЫШЛЕННОГО ФОРУМА)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11055', 'en', 'Нефть. Газ. Добыча. Сервисное оборудование', 'Специализированная выставка - НЕФТЬ. ГАЗ. ДОБЫЧА. СЕРВИСНОЕ ОБОРУДОВАНИЕ (в рамках Югорского промышленного форума)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11056', 'en', 'Малая и средняя авиация в Югре', 'Межрегиональная специализированная выставка - МАЛАЯ И СРЕДНЯЯ АВИАЦИЯ В ЮГРЕ (В рамках Югорского промышленного форума)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11057', 'en', 'Модное лето. Дачный сезон', 'Выставка-ярмарка - МОДНОЕ ЛЕТО. ДАЧНЫЙ СЕЗОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11058', 'en', 'Продлить жизнь. Осень', 'Выставка современных средств оздоровления и долголетия - ПРОДЛИТЬ ЖИЗНЬ. ОСЕНЬ - 2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11059', 'en', 'Подарки. Весна', 'Международная специализированная выставка - ПОДАРКИ. ВЕСНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11060', 'en', 'Зеленая неделя', 'XIII межрегиональная выставка - ЗЕЛЕНАЯ НЕДЕЛЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11061', 'en', 'Стройпрогресс', 'IX межрегиональная выставка-СТРОЙПРОГРЕСС');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11062', 'en', 'Здоровье и красота', 'X межрегиональная выставка - ЗДОРОВЬЕ И КРАСОТА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11063', 'en', 'Владимирская весна', 'XVIII межрегиональная выставка - ВЛАДИМИРСКАЯ ВЕСНА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11064', 'en', 'Гидроавиасалон', '11 Международная выставка и научная конференция по гидроавиации - ГИДРОАВИАСАЛОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11065', 'en', 'Предпринимательство', '8-я Межрегиональная специализированная выставка-ярмарка - ПРЕДПРИНИМАТЕЛЬСТВО 2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11066', 'en', 'Архитектура. Градостроительство. Транспорт и дорожное хозяйство. Системы жизнеобеспечения региона и города', 'XXI-я Межрегиональная универсальная выставка-конгресс - АРХИТЕКТУРА. ГРАДОСТРОИТЕЛЬСТВО. ТРАНСПОРТ И ДОРОЖНОЕ ХОЗЯЙСТВО. СИСТЕМЫ ЖИЗНЕОБЕСПЕЧЕНИЯ РЕГИОНА И ГОРОДА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11067', 'en', 'Нефть. Газ. Геология. Тэк', 'XVII-я Межрегиональная специализированная выставка-конгресс- НЕФТЬ. ГАЗ. ГЕОЛОГИЯ. ТЭК-2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11068', 'en', 'Вербная неделя', 'Православная выставка-ярмарка - ВЕРБНАЯ НЕДЕЛЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11069', 'en', 'Осенняя Православная выставка-ярмарка', 'ОСЕННЯЯ ПРАВОСЛАВНАЯ ВЫСТАВКА-ЯРМАРКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11070', 'en', 'Кавказ-энерго', 'Выставка - КАВКАЗ-ЭНЕРГО (в рамках Градостроительного форума Северного Кавказа)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11071', 'en', 'Альп-Экспо', 'Первая выставкатоваров для туризма, альпинизма и активного отдыха - АЛЬП-ЭКСПО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11072', 'en', 'Территория безопасности', 'Выставка - ТЕРРИТОРИЯ БЕЗОПАСНОСТИ (в рамках Градостроительного форума Северного Кавказа)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11073', 'en', 'Дорожное строительство', 'Выставка - ДОРОЖНОЕ СТРОИТЕЛЬСТВО (в рамках Градостроительного форума Северного Кавказа)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11074', 'en', 'Охота. Рыбалка', '21 международная выставка-продажа - ОХОТА. РЫБАЛКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11075', 'en', 'Весенний сад 2', 'Специализированная выставка-ярмарка - ВЕСЕННИЙ САД 2');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11076', 'en', 'Весенний сад', 'Специализированная выставка-ярмарка - ВЕСЕННИЙ САД');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11077', 'en', 'Весенний сад 2', 'Специализированная выставка-ярмарка - ВЕСЕННИЙ САД 2');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11078', 'en', 'Весенний сад', 'Специализированная выставка-ярмарка - ВЕСЕННИЙ САД');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11079', 'en', 'Valve industry forum & expo', 'III Международный форум - VALVE INDUSTRY FORUM & EXPO');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11080', 'en', 'Kosmetik expo Поволжье', 'Выставка для профессионалов индустрии красоты - KOSMETIK EXPO ПОВОЛЖЬЕ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11081', 'en', 'Техническая ярмарка. Сварка. Оборудование. Инструмент', 'Специализированная техническая выставка - ТЕХНИЧЕСКАЯ ЯРМАРКА. СВАРКА. ОБОРУДОВАНИЕ. ИНСТРУМЕНТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11082', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11083', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11084', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11085', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11086', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11087', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11088', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11089', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11090', 'en', '1000 и 1 вещь!', 'Выставка-ярмарка - 1000 И 1 ВЕЩЬ!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11091', 'en', 'Беларусь на Белгородчине', 'Выставка - БЕЛАРУСЬ НА БЕЛГОРОДЧИНЕ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11092', 'en', 'Регионы России', 'Выставка - РЕГИОНЫ РОССИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11093', 'en', 'Летняя ярмарка', 'Всероссийская универсальная ярмарка - ЛЕТНЯЯ ЯРМАРКА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11094', 'en', 'Арт-салон', 'III Межрегиональная специализированная выставка-ярмарка изделий декоративно-прикладного искусства - АРТ-САЛОН');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11095', 'en', 'Здравствуй, лето!', '6-я Выставка-ярмарка товаров летнего ассортимента для дома и отдыха - ЗДРАВСТВУЙ, ЛЕТО!');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11096', 'en', 'Роспласт. Пластмассы. Оборудование. Изделия', '8-я Международная специализированная выставка сырья, оборудования и технологий для производства изделий из пластмасс - РОСПЛАСТ. ПЛАСТМАССЫ. ОБОРУДОВАНИЕ. ИЗДЕЛИЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11097', 'en', 'Rosmould / Росмолд', '12-я Международная выставка дизайна и проектирования изделий, производства и эксплуатации форм, пресс-форм, штампов, 3D-оборудования и технологий - ROSMOULD /РОСМОЛД');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11098', 'en', 'Продукты питания. Ярмарка сельхозпродукции', 'Специализированная выставка-ярмарка - ПРОДУКТЫ ПИТАНИЯ. ЯРМАРКА СЕЛЬХОЗПРОДУКЦИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11099', 'en', 'Мир детства', '10-я специализированная выставка - МИР ДЕТСТВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11100', 'en', 'Модный салон. Лето', '26-я межрегиональная выставка-ярмарка - МОДНЫЙ САЛОН. ЛЕТО');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11101', 'en', 'Сибирь православная', 'Ежегодная церковно-общественная выставка-ярмарка - СИБИРЬ ПРАВОСЛАВНАЯ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11102', 'en', 'Мир детства', 'Специализированная выставка - МИР ДЕТСТВА');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11103', 'en', 'Недра россии', 'III международная специализированная выставка - НЕДРА РОССИИ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11104', 'en', 'Уголь россии и майнинг', 'XXIV международная специализированная выставка технологий горных разработок - УГОЛЬ РОССИИ И МАЙНИНГ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11105', 'en', 'Территория Красоты И Стиля', 'ХI выставка ювелирных изделий, моды, парикмахерского искусства и косметологии - СОЧИ - ТЕРРИТОРИЯ КРАСОТЫ И СТИЛЯ-2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11106', 'en', 'Православие (Петров Пост)', 'XV Православная выставка-ярмарка - ПРАВОСЛАВИЕ (ПЕТРОВ ПОСТ)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11107', 'en', 'Спортивная индустрия', 'XII специализированная выставка - СПОРТИВНАЯ ИНДУСТРИЯ (В рамках форума «Здоровье России. Сочи – 2016»)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11108', 'en', 'Медицина - сегодня и завтра', 'XVIII международная специализированная выставка медицинской техники, технологий и фармпрепаратов для здравоохране - МЕДИЦИНА - СЕГОДНЯ И ЗАВТРА. (В рамках форума «Здоровье России. Сочи – 2016»)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11109', 'en', 'Красота И Грация', '17-я Специализированная выставка индустрии красоты - КРАСОТА И ГРАЦИЯ (в рамках 12-го Международного фестиваля \"Красота и грация\")');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11110', 'en', 'Технопром', 'Выставка V международного форума технологического развития - Технопром');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11111', 'en', 'Металлургия-Литмаш', 'Международная промышленная выставка - МЕТАЛЛУРГИЯ-ЛИТМАШ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11112', 'en', 'Алюминий / Цветмет', 'Международная промышленная выставка - АЛЮМИНИЙ / ЦВЕТМЕТ');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11113', 'en', 'Vision Russia', 'Международная специализированная выставка машинного зрения-VISION RUSSIA-2016');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11114', 'en', 'Rosupack', '22-я международная выставка упаковочной индустрии - ROSUPACK');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11115', 'en', 'Printech', '3-я Международная выставка оборудования, технологий и материалов для печатного и рекламного производства - PRINTECH');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11116', 'en', 'Медицина. Здравоохранение', 'Межрегиональная специализированная выставка - МЕДИЦИНА. ЗДРАВООХРАНЕНИЕ -2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11117', 'en', 'Металлоконструкции', 'Международная выставка - МЕТАЛЛОКОНСТРУКЦИИ 2017');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11118', 'en', 'Sam-expo', 'Специализированная выставка по эстетической медицине - SAM-EXPO (в рамках международного симпозиума по эстетической медицине)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11119', 'en', 'Интерпластика', '8 специализированная выставка - ПЛАСТИК. КАУЧУК (в рамках ТАТАРСТАНСКОГО НЕФТЕГАЗОХИМИЧЕСКОГО ФОРУМА)');
			INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES('11120', 'en', ' ','Всероссийская ярмарка продукции предприятий регионов России - ВСЕРОССИЙСКАЯ ЯРМАРКА В НАБЕРЕЖНЫХ ЧЕЛНАХ');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}