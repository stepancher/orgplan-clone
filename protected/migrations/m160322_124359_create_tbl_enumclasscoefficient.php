<?php

class m160322_124359_create_tbl_enumclasscoefficient extends CDbMigration
{/**
 * @return bool
 * @throws CDbException
 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			DROP TABLE IF EXISTS {{attributeenumcoefficient}};
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			DROP TABLE IF EXISTS {{attributeenumcoefficient}};
			CREATE TABLE {{attributeenumcoefficient}} (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`attributeEnum` int(10) unsigned NOT NULL,
			`coefficient` double NOT NULL,
			`unit` varchar(45) DEFAULT NULL,
			`priority` float(10) DEFAULT NULL,
			PRIMARY KEY (`id`),
			KEY `fk_attribute_enum_coefficient` (`attributeEnum`),
			CONSTRAINT `fk_attribute_enum_coefficient` FOREIGN KEY (`attributeEnum`) REFERENCES {{attributeenum}} (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
			
			INSERT INTO {{attributeenumcoefficient}} (`attributeEnum`, `coefficient`, `unit`, `priority`) VALUES ('10', '200', 'USD', '1');
			INSERT INTO {{attributeenumcoefficient}} (`attributeEnum`, `coefficient`, `unit`, `priority`) VALUES ('11', '234', 'USD', '1');
			INSERT INTO {{attributeenumcoefficient}} (`attributeEnum`, `coefficient`, `unit`, `priority`) VALUES ('12', '241', 'USD', '1');
			INSERT INTO {{attributeenumcoefficient}} (`attributeEnum`, `coefficient`, `unit`, `priority`) VALUES ('13', '248', 'USD', '1');

		";
	}
}