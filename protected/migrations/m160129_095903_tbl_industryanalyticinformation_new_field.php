<?php

class m160129_095903_tbl_industryanalyticinformation_new_field extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{industryanalyticsinformation}} DROP COLUMN `icon`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{industryanalyticsinformation}} ADD `icon` varchar(45);

			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='37';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='14';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='13';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='8';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='5';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='1';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='3';
			UPDATE {{industryanalyticsinformation}} SET `icon`='' WHERE `id`='2';
			UPDATE {{industryanalyticsinformation}} SET `icon`='a' WHERE `id`='11';

		";
	}
}