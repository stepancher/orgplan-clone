<?php

class m160216_060504_add_column_user_id_proposalElement extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{proposalelement}} DROP COLUMN `userId` ;



			ALTER TABLE {{attribute}}
			ADD COLUMN `index` int(10) unsigned NOT NULL DEFAULT "0",
			DROP INDEX `attribute_index_UNIQUE` ,
			ADD UNIQUE INDEX `attribute_index_UNIQUE` (`element` ASC, `class` ASC, `index` ASC);


			ALTER TABLE {{proposalelement}}
			ADD COLUMN `guid` varchar(63) NOT NULL,
			ADD COLUMN `parent` INT UNSIGNED NULL DEFAULT NULL,
			ADD INDEX `fk_element_hierarchy` (`parent` ASC);
			ALTER TABLE {{proposalelement}}
			ADD CONSTRAINT `fk_element_hierarchy`
			  FOREIGN KEY (`parent`)
			  REFERENCES `orgplan`.`tbl_proposalelement` (`id`)
			  ON DELETE CASCADE
			  ON UPDATE CASCADE;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{proposalelement}}
			ADD COLUMN `userId` INT NOT NULL AFTER `enabled`;



			ALTER TABLE {{attribute}}
			DROP COLUMN `index`,
			DROP INDEX `attribute_index_UNIQUE` ,
			ADD UNIQUE INDEX `attribute_index_UNIQUE` (`element` ASC, `class` ASC);


			ALTER TABLE {{proposalelement}}
			DROP FOREIGN KEY `fk_element_hierarchy`;
			ALTER TABLE {{proposalelement}}
			DROP COLUMN `parent`,
			DROP COLUMN `guid`,
			DROP INDEX `fk_element_hierarchy` ;

		";
	}


}