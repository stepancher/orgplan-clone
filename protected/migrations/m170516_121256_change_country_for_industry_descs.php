<?php

class m170516_121256_change_country_for_industry_descs extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='8';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='4';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='10';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='9';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='12';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='11';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='5';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='6';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='13';
            UPDATE {{catalogdescription}} SET `countryId`='1' WHERE `id`='7';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}