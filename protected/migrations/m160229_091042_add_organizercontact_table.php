<?php

class m160229_091042_add_organizercontact_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{organizercontact}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			DROP TABLE IF EXISTS {{organizercontact}};
			CREATE TABLE {{organizercontact}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `organizerId` INT NULL,
			  `name` VARCHAR(255) NULL,
			  `phone` VARCHAR(45) NULL,
			  `email` VARCHAR(45) NULL,
			  PRIMARY KEY (`id`));
			  ';
	}
}