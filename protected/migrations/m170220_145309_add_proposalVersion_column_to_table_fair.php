<?php

class m170220_145309_add_proposalVersion_column_to_table_fair extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{fair}}
            ADD COLUMN `proposalVersion` VARCHAR(45) NULL DEFAULT NULL AFTER `exdbId`;
            
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='184';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='10943';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='12722';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='13863';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='13864';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='13927';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='13986';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='14060';
            UPDATE {{fair}} SET `proposalVersion`='acamar' WHERE `id`='14468';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}