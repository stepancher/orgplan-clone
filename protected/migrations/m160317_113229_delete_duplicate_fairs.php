<?php

class m160317_113229_delete_duplicate_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE `id` IN (10071, 10072, 10086);
			DELETE FROM {{trfair}} WHERE `trParentId` IN (10071, 10072, 10086);
		";
	}
}