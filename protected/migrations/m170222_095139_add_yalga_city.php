<?php

class m170222_095139_add_yalga_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('54', 'yalga');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1532', 'ru', 'Ялга');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1532', 'en', 'Yalga');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1532', 'de', 'Yalga');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}