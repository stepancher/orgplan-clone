<?php

class m160930_071023_gradoteka_final_check extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{charttablegroup}} SET `name`='Основные показатели отрасли' WHERE `id`='18';
            UPDATE {{charttablegroup}} SET `name`='Сбытовая статистика' WHERE `id`='38';
            UPDATE {{charttablegroup}} SET `name`='Косвенные показатели отрасли' WHERE `id`='25';
            UPDATE {{chartcolumn}} SET `header`='Потребление картофеля в расчете на душу населения' WHERE `id`='150';
            UPDATE {{chartcolumn}} SET `header`='Потребление овощей и бахчевых в расчете на душу населения' WHERE `id`='151';
            UPDATE {{chartcolumn}} SET `header`='Потребление сахара в расчете на душу населения' WHERE `id`='152';
            UPDATE {{chartcolumn}} SET `header`='Продажа вина в натуральном выражении' WHERE `id`='138';
            UPDATE {{chartcolumn}} SET `header`='Продажа пива, кроме пивных коктейлей и солодовых напитков' WHERE `id`='142';
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('5745b180bd4c56b4668b4713', '', '2015', '5', '#43ade3', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `widgetType`, `epoch`, `tableId`, `position`) VALUES ('5745b180bd4c56b4668b4713', 'Число С/Х организаций', '4', '2015', '5', '2');
            UPDATE {{chartcolumn}} SET `position`='3' WHERE `id`='23';
            UPDATE {{chartcolumn}} SET `position`='4' WHERE `id`='24';
            UPDATE {{chartcolumn}} SET `position`='5' WHERE `id`='25';
            UPDATE {{chartcolumn}} SET `position`='6' WHERE `id`='26';
            UPDATE {{chartcolumn}} SET `position`='7' WHERE `id`='27';
            
            ALTER TABLE {{chartcolumn}} 
            ADD COLUMN `visible` INT(11) NULL DEFAULT NULL AFTER `accessory`;
            
            UPDATE {{chartcolumn}} SET `visible` = 1;
            
            UPDATE {{chartcolumn}} SET `header`='Число С/Х организаций', `position`='2' WHERE `id`='470';
            UPDATE {{chartcolumn}} SET `header`='', `position`='1' WHERE `id`='471';
            UPDATE {{charttable}} SET `visible`=NULL WHERE `id`='4';
            UPDATE {{gobjects}} SET `name`='Санкт-Петербург' WHERE `id`='315';
            UPDATE {{gobjects}} SET `name`='Москва' WHERE `id`='319';
            
            UPDATE {{charttable}} SET `name`='Таможенная справка' WHERE `id`='88';
            
            UPDATE {{charttable}} SET `name`='Экспорт/импорт продовольственных товаров по округам данные росстат' WHERE `id`='92';
            UPDATE {{charttable}} SET `name`='Экспорт/импорт горюче-смазочных материалов по округам данные росстат' WHERE `id`='85';
            UPDATE {{charttable}} SET `name`='Экспорт/Импорт древесины и целлюлозы по регионам данные росстат' WHERE `id`='75';
            UPDATE {{charttable}} SET `name`='Экспорт/Импорт древесины и целлюлозы по округам данные росстат' WHERE `id`='72';
            UPDATE {{charttable}} SET `name`='Экспорт/импорт горюче-смазочных материалов по регионам данные росстат' WHERE `id`='70';
            UPDATE {{charttable}} SET `name`='Экспорт/импорт металлов и стальных труб по регионам данные росстат' WHERE `id`='64';
            UPDATE {{charttable}} SET `name`='Экспорт/импорт продовольственных товаров по регионам данные росстат' WHERE `id`='50';
            UPDATE {{charttable}} SET `name`='Экспорт/Импорт сельскохозяйственной продукции данные росстат' WHERE `id`='9';

            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500209', '20.20.11 Фанера клееная, состоящая только из листов древесины', 'Производство основных видов продукции в натуральном выражении с 2010 г.(в соответствии с ОКПД)', 'derivative', 'тыс.м3', '0');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500209', 'custom', '[1]/1000');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`, `statId`) VALUES ('103', '1', '571e46d877d89702498d0936');
            
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83533', '370', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83534', '371', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83535', '372', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83536', '374', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83537', '375', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83538', '376', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83539', '377', '500209', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83540', '379', '500209', 'country');

            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750743', '83533', '2016-09-30 11:26:52', '765.508', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750744', '83533', '2016-09-30 11:26:52', '860.898', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750745', '83533', '2016-09-30 11:26:52', '912.845', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750746', '83534', '2016-09-30 11:26:52', '1083.537', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750747', '83534', '2016-09-30 11:26:52', '1164.741', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750748', '83534', '2016-09-30 11:26:52', '1168.742', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750749', '83535', '2016-09-30 11:26:52', '3.066', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750750', '83535', '2016-09-30 11:26:52', '4.451', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750751', '83535', '2016-09-30 11:26:52', '3.962', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750752', '83536', '2016-09-30 11:26:52', '890.239', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750753', '83536', '2016-09-30 11:26:52', '958.794', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750754', '83536', '2016-09-30 11:26:52', '1002.087', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750755', '83537', '2016-09-30 11:26:52', '316.556', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750756', '83537', '2016-09-30 11:26:52', '339.347', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750757', '83537', '2016-09-30 11:26:52', '344.523', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750758', '83538', '2016-09-30 11:26:52', '269.675', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750759', '83538', '2016-09-30 11:26:52', '238.941', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750760', '83538', '2016-09-30 11:26:53', '222.99', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750761', '83539', '2016-09-30 11:26:53', '0.293', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750762', '83539', '2016-09-30 11:26:53', '1.476', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750763', '83539', '2016-09-30 11:26:53', '2.336', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750764', '83540', '2016-09-30 11:26:53', '3328.874', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750765', '83540', '2016-09-30 11:26:53', '3568.648', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750766', '83540', '2016-09-30 11:26:53', '3657.485', '2015');

            UPDATE {{chartcolumn}} SET `statId`='500209' WHERE `id`='357';
            
            UPDATE {{gstatgroup}} SET `name`='Товарооборот черных металлов' WHERE `id`='11';
            UPDATE {{gstatgroup}} SET `name`='Товарооборот коррозионностойкой стали' WHERE `id`='12';
            UPDATE {{gstatgroup}} SET `name`='Товарооборот легированных сталей' WHERE `id`='14';

            UPDATE {{charttablegroup}} SET `visible`='1' WHERE `id`='34';
            UPDATE {{charttablegroup}} SET `name`='Косвенные показатели' WHERE `id`='34';
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `granularity`, `area`, `groupId`, `position`, `visible`) VALUES ('Косвенные показатели емкости рынка по округам', '2', '1', '', 'state', '34', '1', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `accessory`, `visible`) VALUES ('56d2f8c2f7a9ad68148cdff1', 'Общая площадь введенных зданий', '', '6', '2015', '103', '#fa694c', '1', NULL, '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `related`, `visible`) VALUES ('56ec793a75d897c6069d7b82', 'Численность населения', '', '6', '2015', '103', '#42c0b4', '2', NULL, '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `visible`) VALUES ('500003', 'Численность населения с доходом выше прожиточного минимума', '', '6', '2015', '103', '#42c0b4', '3', '1');
            INSERT INTO {{charttable}} (`id`, `name`, `type`, `industryId`, `area`, `groupId`, `position`, `visible`) VALUES ('104', 'Косвенные показатели емкости рынка по регионам', '2', '1', 'region', '34', '2', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `visible`) VALUES ('56d2f8c2f7a9ad68148cdff1', 'Общая площадь введенных зданий', '', '6', '2015', '104', '#fa694c', '1', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `visible`) VALUES ('56ec793a75d897c6069d7b82', 'Численность населения', '', '6', '2015', '104', '#42c0b4', '2', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`, `visible`) VALUES ('500003', 'Численность населения с доходом выше прожиточного минимума', '', '6', '2015', '104', '#42c0b4', '3', '1');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}