<?php

class m151022_120930_insert_in_chartgroup_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getInsertInTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{chartgroup}} WHERE id in (1,2,3,4,5,6,7);
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getInsertInTable(){
		return '
			INSERT INTO {{chartgroup}} (`id`, `header`, `type`, `industryId`) VALUES
			(1, "", 1, 11),
			(2, NULL, 1, 11),
			(3, NULL, 1, 11),
			(4, "Структура сельского хозяйства / Растениеводство", 2, 11),
			(5, "Структура сельского хозяйства / Животноводство", 2, 11),
			(6, "Основные показатели сельского хозяйства / Растениеводство", 2, 11),
			(7, "Основные показатели сельского хозяйства / Животноводство", 2, 11);
		';
	}
}