<?php

class m161124_131949_new_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('1', '1', '10943');
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('1', '1', '10943');
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('1', '1', '10943');
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('1', '1', '10943');
            
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('60', 'ru', 'Инструкция для экспонента', '/static/documents/fairs/10943/exponent_instruction_ru.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('61', 'ru', 'Положение о конкурсе', '/static/documents/fairs/10943/contest_ru.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('62', 'ru', 'Правла выставки', '/static/documents/fairs/10943/terms_ru.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('63', 'ru', 'Схема проезда', '/static/documents/fairs/10943/scheme_ru.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('60', 'en', 'User manual', '/static/documents/fairs/10943/exponent_instruction_en.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('61', 'en', 'Contest terms & conditions', '/static/documents/fairs/10943/contest_en.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('62', 'en', 'Requirements and Rules-rus', '/static/documents/fairs/10943/terms_en.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('63', 'en', 'Passage scheme', '/static/documents/fairs/10943/scheme_en.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('60', 'de', 'Anfahrtsplan', '/static/documents/fairs/10943/exponent_instruction_de.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('61', 'de', 'Anweisungen für Aussteller', '/static/documents/fairs/10943/contest_de.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('62', 'de', 'Ausschreibungsbestimmungen', '/static/documents/fairs/10943/terms_de.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('63', 'de', 'Messeordnung', '/static/documents/fairs/10943/scheme_de.docx');
            
            DELETE FROM {{documents}} WHERE `id`='30';
            DELETE FROM {{documents}} WHERE `id`='31';
            DELETE FROM {{documents}} WHERE `id`='32';
            DELETE FROM {{documents}} WHERE `id`='33';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}