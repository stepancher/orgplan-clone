<?php

class m160525_073701_translates_16_05_25 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{trattributeclassproperty}} SET `value`='до 5 кВт включительно' WHERE `id`='978';
			UPDATE {{trattributeclassproperty}} SET `value`='до 10 кВт включительно' WHERE `id`='980';
			UPDATE {{trattributeclassproperty}} SET `value`='до 20 кВт включительно' WHERE `id`='982';
			UPDATE {{trattributeclassproperty}} SET `value`='до 40 кВт включительно' WHERE `id`='984';
			UPDATE {{trattributeclassproperty}} SET `value`='до 60 кВт включительно' WHERE `id`='986';
			UPDATE {{trattributeclassproperty}} SET `value`='холодное водоснабжение и канализация' WHERE `id`='988';
			UPDATE {{trattributeclassproperty}} SET `value`='холодное водоснабжение и канализация' WHERE `id`='992';
			UPDATE {{trattributeclassproperty}} SET `value`='горячее водоснабжение' WHERE `id`='990';
			UPDATE {{trattributeclassproperty}} SET `value`='горячее водоснабжение' WHERE `id`='994';
			UPDATE {{trattributeclassproperty}} SET `value`='Шланг 1/2\" (для воды) по 5 п . м . ' WHERE `id`='996';
			UPDATE {{trattributeclassproperty}} SET `value`='Канализация 38 х 42 мм по 5 п . м . ' WHERE `id`='998';
			UPDATE {{trattributeclassproperty}} SET `value`='Прокладка воды и канализации по 5 п . м . ' WHERE `id`='1000';
		";
	}
}