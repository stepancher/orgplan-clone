<?php

class m170330_111725_add_st_albans_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('128');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1094', 'ru', 'Хартфордшир', 'hertfordshire');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1094', 'en', 'Hertfordshire', 'hertfordshire');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1094', 'de', 'Hertfordshire', 'hertfordshire');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1094', 'st-albans');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1543', 'ru', 'Сент-Олбанс');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1543', 'en', 'St Albans');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1543', 'de', 'St Albans');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}