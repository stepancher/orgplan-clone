<?php

class m161128_153310_update_report_DE extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalName) = explode('=', Yii::app()->dbProposalF10943->connectionString);
        
        return "
            UPDATE {$dbProposalName}.tbl_trreport SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Standnummer\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Firmenname\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Standfläche\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"bill\",\"displayName\": \"Vertragsnummer\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"phone\",\"displayName\": \"Telefon\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"post\",\"displayName\": \"Position\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"fullName\",\"displayName\": \"ФИО\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]', `exportColumns`='[{\"rowLabel\":\"Der Aussteller\",\"standNumber\":\"Standnummer\",\"companyName\":\"Firmenname\",\"standSquare\":\"Standfläche\",\"bill\":\"Vertragsnummer\",\"phone\":\"Telefon\",\"post\":\"Position\",\"fullName\":\"Name, Vorname\"}]' WHERE `id`='22';
            UPDATE {$dbProposalName}.tbl_trreport SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Standnummer\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Firmenname\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Standfläche\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Standardbebauungstyp\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Bebauungsfläche\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Blendenbeschriftung\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Blendenbeschriftung\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Antrag Nr.3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Antrag Nr.4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]', `exportColumns`='[{\"rowLabel\":\"Der Aussteller\",\"standNumber\":\"Standnummer\",\"companyName\":\"Firmenname\",\"standSquare\":\"Standfläche\",\"buildingType\":\"Standardbebauungstyp:\",\"buildingSquare\":\"Bebauungsfläche\",\"letters\":\"Fascia name\",\"logo\":\"Logo auf Schriftblende\",\"thirdProposal\":\"Antrag Nr.3\",\"fourthProposal\":\"Antrag Nr.4\"}]' WHERE `id`='21';
            UPDATE {$dbProposalName}.tbl_trreport SET `columnDefs`='[{\"field\":\"status\",\"displayName\":\"Status\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"}]', `exportColumns`='[{\"rowLabel\":\"Название\",\"status\":\"Status\"}]' WHERE `id`='20';
            UPDATE {$dbProposalName}.tbl_trreport SET `columnDefs`='[{\"field\":\"attributeValue\",\"displayName\":\"Zahl\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\" Prozent der Ausführung\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"number\"}]', `exportColumns`='[{\"rowLabel\":\"Name\",\"attributeValue\":\"Zahl\",\"currency\":\" Prozent der Ausführung\"}]' WHERE `id`='19';
            UPDATE {$dbProposalName}.tbl_trreport SET `columnDefs`='[{\"field\":\"attributeValue\",\"displayName\":\"Zahl\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumRUB\",\"displayName\":\"Kosten RUB\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumUSD\",\"displayName\":\"Kosten USD\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\"Währung\",\"sortable\":true,\"filterable\":true,\"sortingType\":\"string\"}]', `exportColumns`='[{\"rowLabel\":\"Name\",\"dependencyVal\":\"Hilfsstufung: Bedeutung\",\"dependencyValUnitTitle\":\"Hilfsstufung: Name\",\"unitTitle\":\"ед . \",\"attributeValue\":\"Zahl\",\"price\":\"Preis\",\"currency\":\"Währung\",\"discount\":\"Rabatt\",\"sumRUB\":\"Rubel\",\"sumUSD\":\"USD\"}]' WHERE `id`='18';
            UPDATE {$dbProposalName}.tbl_trreport SET `columnDefs`='[{\"field\":\"attributeValue\",\"displayName\":\"Zahl\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumRUB\",\"displayName\":\"Kosten RUB\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"sumUSD\",\"displayName\":\"Kosten USD\",\"sortable\":true,\"aggregate\":true,\"sortingType\":\"number\"},{\"field\":\"currency\",\"displayName\":\"Zahl\",\"sortable\":true,\"filterable\":true,\"sortingType\":\"string\"}]', `exportColumns`='[{\"rowLabel\":\"Name\",\"dependencyVal\":\"Hilfsstufung: Bedeutung\",\"dependencyValUnitTitle\":\"Hilfsstufung: Name\",\"unitTitle\":\"Stufung \",\"attributeValue\":\"Zahl\",\"price\":\"Preis\",\"currency\":\"Währung\",\"discount\":\"Rabatt\",\"sumRUB\":\"Rubel\",\"sumUSD\":\"USD\"}]' WHERE `id`='17';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}