<?php

class m170510_103408_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('655', 'gold-coast');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1575', 'ru', 'Голд-Кост');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1575', 'en', 'Gold Coast');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1575', 'de', 'Gold Coast');


            INSERT INTO {{region}} (`districtId`) VALUES ('79');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1101', 'ru', 'Лайтнинг Ридж', 'Lightning-Ridge');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1101', 'en', 'Lightning Ridge', 'Lightning-Ridge');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1101', 'de', 'Lightning Ridge', 'Lightning-Ridge');
            UPDATE {{trregion}} SET `shortUrl`='lightning-ridge' WHERE `id`='3568';
            UPDATE {{trregion}} SET `shortUrl`='lightning-ridge' WHERE `id`='3569';
            UPDATE {{trregion}} SET `shortUrl`='lightning-ridge' WHERE `id`='3570';
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1101', 'lightning-ridge');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1576', 'ru', 'Лайтнинг Ридж');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1576', 'en', 'Lightning Ridge');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1576', 'de', 'Lightning Ridge');
            
            
            INSERT INTO {{city}} (`regionId`) VALUES ('329');
            UPDATE {{city}} SET `shortUrl`='ried-im-innkreis' WHERE `id`='1577';
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1577', 'ru', 'Рид-им-Иннкрайс');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1577', 'en', 'Ried im Innkreis');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1577', 'de', 'Ried im Innkreis');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('322', 'kuchl');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1578', 'ru', 'Кухль');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1578', 'en', 'Kuchl');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1578', 'de', 'Kuchl');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('328', 'oberwart');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1579', 'ru', 'Оберварт');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1579', 'en', 'Oberwart');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1579', 'de', 'Oberwart');
            
            
            INSERT INTO {{region}} (`districtId`) VALUES ('85');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1102', 'ru', 'Гензерндорф', 'ganserndorf-district');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1102', 'en', 'Gänserndorf', 'ganserndorf-district');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1102', 'de', 'Gänserndorf', 'ganserndorf-district');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1102', 'gross-enzersdorf');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1580', 'ru', 'Грос-Энцерсдорф');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1580', 'en', 'Gross-Enzersdorf');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1580', 'de', 'Groß-Enzersdorf');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('333', 'mondsee');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1581', 'ru', 'Мондзе');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1581', 'en', 'Mondsee');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1581', 'de', 'Mondsee');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('169', 'frondenberg');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1582', 'ru', 'Фрёнденберг-на-Руре');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1582', 'en', 'Fröndenberg');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1582', 'de', 'Fröndenberg');
            
            
            INSERT INTO {{region}} (`districtId`) VALUES ('207');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1103', 'ru', 'Ломбардия', 'lombardy');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1103', 'en', 'Lombardy', 'lombardy');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1103', 'de', 'Lombardei', 'lombardy');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1103', 'como');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1583', 'ru', 'Комо');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1583', 'en', 'Como');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1583', 'de', 'Como');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}