<?php

class m161129_125413_add_rating_to_orgplan extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{fairrating}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {{fairrating}} (`id`, `name`) VALUES ('1', 'Рейтинг');
            INSERT INTO {{fairrating}} (`id`, `name`) VALUES ('2', 'Аудит');
            INSERT INTO {{fairrating}} (`id`, `name`) VALUES ('3', 'Организатор');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}