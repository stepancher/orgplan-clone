<?php

class m160706_091455_add_new_dependences extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			UPDATE {{calendarfairtasks}} SET `date`="2016-09-05 00:00:00" WHERE `id`="147";

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":24}\', 	"calendar/gantt/reject/id/145/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":24}\', 	"calendar/gantt/reject/id/146/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":24}\', 	"calendar/gantt/reject/id/147/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":24}\', 	"calendar/gantt/reject/id/148/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":24}\', 	"calendar/gantt/add/id/145");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":24}\', 	"calendar/gantt/add/id/147");



			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":24, "save":1}\', 	"calendar/gantt/add/id/145");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":24, "send":1}\', 	"calendar/gantt/add/id/145");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", 	 \'{"fairId":184, "ecId":24}\', 	"calendar/gantt/add/id/145");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":24, "send":1}\', 	"calendar/gantt/complete/id/145");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":24, "send":1}\', 	"calendar/gantt/add/id/146");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":24}\', 	"calendar/gantt/complete/id/145");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":24}\', 	"calendar/gantt/add/id/146");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":24, "send":1}\', 	"calendar/gantt/complete/id/147");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":24, "send":1}\', 	"calendar/gantt/add/id/148");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":24}\', 	"calendar/gantt/complete/id/147");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":24}\', 	"calendar/gantt/add/id/148");








			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":25}\', 	"calendar/gantt/reject/id/149/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":25}\', 	"calendar/gantt/reject/id/150/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":25}\', 	"calendar/gantt/reject/id/151/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":25}\', 	"calendar/gantt/reject/id/152/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":25}\', 	"calendar/gantt/add/id/149");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":25}\', 	"calendar/gantt/add/id/151");



			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":25, "save":1}\', 	"calendar/gantt/add/id/149");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":25, "send":1}\', 	"calendar/gantt/add/id/149");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", 	 \'{"fairId":184, "ecId":25}\', 	"calendar/gantt/add/id/149");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":25, "send":1}\', 	"calendar/gantt/complete/id/149");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":25, "send":1}\', 	"calendar/gantt/add/id/150");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":25}\', 	"calendar/gantt/complete/id/149");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":25}\', 	"calendar/gantt/add/id/150");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":25, "send":1}\', 	"calendar/gantt/complete/id/151");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":25, "send":1}\', 	"calendar/gantt/add/id/152");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":25}\', 	"calendar/gantt/complete/id/151");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send",   \'{"fairId":184, "ecId":25}\', 	"calendar/gantt/add/id/152");

		';
	}
}