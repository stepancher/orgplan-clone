<?php

class m160406_062916_change_plumbing_connections extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `label`='Шланг 1/2\" (для воды) по 5 п.г.' WHERE `id`='85';
			UPDATE {{attributeclass}} SET `label`='Канализация 38 х 42 мм по 5 п.г.' WHERE `id`='86';
			UPDATE {{attributeclass}} SET `label`='Прокладка воды и канализации по 5 п.г.' WHERE `id`='87';
			UPDATE {{attributeclass}} SET `unitTitle`='шт.' WHERE `id`='151';
			UPDATE {{attributeclass}} SET `unitTitle`='шт.' WHERE `id`='152';
			UPDATE {{attributeclass}} SET `unitTitle`='шт.' WHERE `id`='153';
		";
	}
}