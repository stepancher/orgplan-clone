<?php

class m160428_115808_add_calendarfiertasks_for_dlg extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "

			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Первый день монтажа (по согласованию с Организатором)', '2016-09-29', '2016-09-30', 'gray');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Дни монтажа', '2016-09-30', '2016-10-04', 'black');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Дни работы выставки', '2016-10-04', '2016-10-08', 'green');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Дни демонтажа', '2016-10-07', '2016-10-09', 'black');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Последний день подачи заявки N1 со скидкой 20%', '2015-12-23', '2015-12-24', 'red');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Последний день подачи заявки N1 со скидкой 15%', '2016-03-31', '2016-04-01', 'red');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Последний день подачи заявки N1 со скидкой 10%', '2016-07-29', '2016-07-30', 'red');
			INSERT INTO {{calendarfairdays}} (`id`, `fairId`, `name`, `startDate`, `endDate`, `cssClass`) VALUES (NULL, '10556', 'Последний день подачи заявок N1', '2016-08-31', '2016-09-01', 'red');

			INSERT INTO {{calendarfairtasks}} (`parent`, `group`, `fairId`, `name`, `desc`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES
			(NULL,1,10556,'Отправить заявку №1 ЭКСПОНЕНТ (скидка 20%)','Задача завершится автоматически, когда будет отправлена заявка №1',NULL,'2015-12-23 23:59:59','',0,1),
			(1,NULL,10556,'Подписать договор (скидка 20%)','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(2,NULL,10556,'Оплатить участие (аванс 10%)','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800 ',1,0),
			(3,NULL,10556,'Оплатить участие (аванс 40%)','Нажмите \"Завершить\", когда будет произведена оплата',15,'2016-04-16 00:00:00','#f6a800 ',1,0),
			(4,NULL,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',17,'2016-07-15 00:00:00','#f6a800 ',1,0),
			(NULL,1,10556,'Отправить заявку №1 ЭКСПОНЕНТ (скидка 15%)','Задача завершится автоматически, когда будет отправлена заявка №1',99,'2015-12-24 00:00:00','',0,0),
			(6,NULL,10556,'Подписать договор (скидка 15%)','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(7,NULL,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800 ',1,0),
			(8,NULL,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',17,'2016-07-15 00:00:00','#f6a800 ',1,0),
			(NULL,1,10556,'Отправить заявку №1 ЭКСПОНЕНТ (скидка 10%)','Задача завершится автоматически, когда будет отправлена заявка №1',120,'2016-04-01 00:00:00','',0,0),
			(10,NULL,10556,'Подписать договор (скидка 10%)','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(29,NULL,10556,'Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата',5,NULL,'#f96a0e',1,0),
			(NULL,2,10556,'Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД','Задача завершится автоматически, когда будет отправлена заявка №2',NULL,'2016-08-25 23:59:59','',0,0),
			(20,NULL,10556,'Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800',1,0),
			(NULL,2,10556,'Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №2',29,'2016-08-26 00:00:00','',0,0),
			(22,NULL,10556,'Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата',5,NULL,'#f6a800',1,0),
			(NULL,2,10556,'Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №2',10,'2016-09-24 00:00:00','',0,0),
			(24,NULL,10556,'Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата',3,NULL,'#f96a0e',1,0),
			(10,10,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',NULL,'2016-06-30 23:59:59','#f6a800',1,0),
			(26,NULL,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n',NULL,'2016-07-29 23:59:59','#f96a0e',1,0),
			(10,10,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',15,'2016-07-01 00:00:00','#f6a800 ',1,0),
			(NULL,1,10556,'Отправить заявку №1 ЭКСПОНЕНТ','Задача завершится автоматически, когда будет отправлена заявка №1',33,'2016-07-30 00:00:00','',0,0),
			(NULL,3,10556,'Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Задача завершится автоматически, когда будет отправлена заявка №3',NULL,'2016-08-25 23:59:00','',0,0),
			(31,NULL,10556,'Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800 ',1,0),
			(NULL,3,10556,'Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №3',29,'2016-08-26 00:00:00','',0,0),
			(33,NULL,10556,'Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата',5,NULL,'#f6a800 ',1,0),
			(NULL,3,10556,'Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №3',10,'2016-09-24 00:00:00','',0,0),
			(35,NULL,10556,'Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата',3,NULL,'#f96a0e',1,0),
			(NULL,4,10556,'Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Задача завершится автоматически, когда будет отправлена заявка №4',NULL,'2016-08-23 23:59:00','',0,0),
			(37,NULL,10556,'Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800',1,0),
			(NULL,4,10556,'Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №4',29,'2016-08-24 00:00:00','',0,0),
			(39,NULL,10556,'Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата',5,NULL,'#f6a800 ',1,0),
			(NULL,4,10556,'Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №4',12,'2016-09-22 00:00:00','',0,0),
			(41,NULL,10556,'Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата',3,NULL,'#f96a0e',1,0),
			(NULL,5,10556,'Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5',NULL,'2016-08-24 23:59:00','',0,0),
			(43,NULL,10556,'Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800 ',1,0),
			(NULL,5,10556,'Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5',23,'2016-08-25 00:00:00','',0,0),
			(45,NULL,10556,'Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата',5,NULL,'#f96a0e',1,0),
			(NULL,5,10556,'Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №5',17,'2016-09-17 00:00:00','',0,0),
			(47,NULL,10556,'Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата',3,NULL,'#f96a0e',1,0),
			(NULL,9,10556,'Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9',NULL,'2016-07-29 23:59:00','',0,0),
			(NULL,91,10556,'Отправить заявку №9-1 РАЗГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-1',NULL,'2016-07-29 23:59:00','',0,0),
			(50,NULL,10556,'Оплатить заявку №9-1 РАЗГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800',1,0),
			(NULL,91,10556,'Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-1',49,'2016-07-30 00:00:00','',0,0),
			(52,NULL,10556,'Оплатить заявку №9-1 (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата',5,NULL,'#f6a800 ',1,0),
			(NULL,91,10556,'Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-1',17,'2016-09-17 00:00:00','',0,0),
			(54,NULL,10556,'Оплатить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата',3,NULL,'#f96a0e',1,0),
			(NULL,92,10556,'Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-2',NULL,'2016-07-29 23:59:00','',0,0),
			(56,NULL,10556,'Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800 ',1,0),
			(NULL,92,10556,'Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-2',53,'2016-07-30 00:00:00','',0,0),
			(58,NULL,10556,'Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n',5,NULL,'#f6a800 ',1,0),
			(NULL,92,10556,'Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-2',13,'2016-09-21 00:00:00','',0,0),
			(60,NULL,10556,'Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n',3,NULL,'#f96a0e ',1,0),
			(NULL,NULL,10556,'Отправить Конкурсную заявку и все необходимые приложения','Задача завершится автоматически, когда будет отправлена Конкурсная заявка',NULL,'2016-06-01 00:00:00','',0,0),
			(28,NULL,10556,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',14,'2016-07-16 00:00:00','#f96a0e',1,0),
			(10,10,10556,'Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата',14,'2016-07-16 00:00:00','#f96a0e',1,0);



			UPDATE {{calendarfairtasks}} SET `parent`='65' WHERE `id`='66';
			UPDATE {{calendarfairtasks}} SET `parent`='66' WHERE `id`='67';
			UPDATE {{calendarfairtasks}} SET `parent`='67' WHERE `id`='68';
			UPDATE {{calendarfairtasks}} SET `parent`='68' WHERE `id`='69';
			UPDATE {{calendarfairtasks}} SET `parent`='70' WHERE `id`='71';
			UPDATE {{calendarfairtasks}} SET `parent`='71' WHERE `id`='72';
			UPDATE {{calendarfairtasks}} SET `parent`='72' WHERE `id`='73';
			UPDATE {{calendarfairtasks}} SET `parent`='74' WHERE `id`='75';
			UPDATE {{calendarfairtasks}} SET `parent`='93' WHERE `id`='76';
			UPDATE {{calendarfairtasks}} SET `parent`='84' WHERE `id`='78';
			UPDATE {{calendarfairtasks}} SET `parent`='86' WHERE `id`='80';
			UPDATE {{calendarfairtasks}} SET `parent`='88' WHERE `id`='82';
			UPDATE {{calendarfairtasks}} SET `parent`='74' WHERE `id`='83';
			UPDATE {{calendarfairtasks}} SET `parent`='90' WHERE `id`='84';
			UPDATE {{calendarfairtasks}} SET `parent`='74' WHERE `id`='85';
			UPDATE {{calendarfairtasks}} SET `parent`='95' WHERE `id`='88';
			UPDATE {{calendarfairtasks}} SET `parent`='97' WHERE `id`='90';
			UPDATE {{calendarfairtasks}} SET `parent`='98' WHERE `id`='92';
			UPDATE {{calendarfairtasks}} SET `parent`='101' WHERE `id`='94';
			UPDATE {{calendarfairtasks}} SET `parent`='103' WHERE `id`='96';
			UPDATE {{calendarfairtasks}} SET `parent`='105' WHERE `id`='98';
			UPDATE {{calendarfairtasks}} SET `parent`='107' WHERE `id`='100';
			UPDATE {{calendarfairtasks}} SET `parent`='109' WHERE `id`='102';
			UPDATE {{calendarfairtasks}} SET `parent`='111' WHERE `id`='104';
			UPDATE {{calendarfairtasks}} SET `parent`='114' WHERE `id`='107';
			UPDATE {{calendarfairtasks}} SET `parent`='116' WHERE `id`='109';
			UPDATE {{calendarfairtasks}} SET `parent`='118' WHERE `id`='111';
			UPDATE {{calendarfairtasks}} SET `parent`='120' WHERE `id`='113';
			UPDATE {{calendarfairtasks}} SET `parent`='122' WHERE `id`='115';
			UPDATE {{calendarfairtasks}} SET `parent`='124' WHERE `id`='117';
			UPDATE {{calendarfairtasks}} SET `parent`='92' WHERE `id`='119';
			UPDATE {{calendarfairtasks}} SET `parent`='74' WHERE `id`='120';


			INSERT INTO {{dependences}} (`action`, `params`, `run`)
				VALUES
				('workspace/addFair','{\"id\":10556}','calendar/gantt/add/id/65'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":11, \"send\":1}','calendar/gantt/complete/id/65'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":11}','calendar/gantt/complete/id/65'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":11, \"send\":1}','calendar/gantt/complete/id/70'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":11}','calendar/gantt/complete/id/70'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":11, \"send\":1}','calendar/gantt/complete/id/74'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":11}','calendar/gantt/complete/id/74'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":11, \"send\":1}','calendar/gantt/complete/id/93'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":11}','calendar/gantt/complete/id/93'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":14, \"save\":1}','calendar/gantt/add/id/84'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":14, \"send\":1}','calendar/gantt/add/id/84'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":14}','calendar/gantt/add/id/84'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":14, \"send\":1}','calendar/gantt/complete/id/84'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":14}','calendar/gantt/complete/id/84'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":14, \"send\":1}','calendar/gantt/complete/id/86'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":14}','calendar/gantt/complete/id/86'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":14, \"send\":1}','calendar/gantt/complete/id/88'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":14}','calendar/gantt/complete/id/88'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":15, \"save\":1}','calendar/gantt/add/id/95'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":15, \"send\":1}','calendar/gantt/add/id/95'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":15}','calendar/gantt/add/id/95'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":15, \"send\":1}','calendar/gantt/complete/id/95'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":15}','calendar/gantt/complete/id/95'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":15, \"send\":1}','calendar/gantt/complete/id/97'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":15}','calendar/gantt/complete/id/97'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":15, \"send\":1}','calendar/gantt/complete/id/99'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":15}','calendar/gantt/complete/id/99'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":12, \"save\":1}','calendar/gantt/add/id/101'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":12, \"send\":1}','calendar/gantt/add/id/101'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":12}','calendar/gantt/add/id/101'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":12, \"send\":1}','calendar/gantt/complete/id/101'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":12}','calendar/gantt/complete/id/101'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":12, \"send\":1}','calendar/gantt/complete/id/103'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":12}','calendar/gantt/complete/id/103'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":12, \"send\":1}','calendar/gantt/complete/id/105'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":12}','calendar/gantt/complete/id/105'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":16, \"save\":1}','calendar/gantt/add/id/107'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":16, \"send\":1}','calendar/gantt/add/id/107'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":16}','calendar/gantt/add/id/107'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":16, \"send\":1}','calendar/gantt/complete/id/107'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":16}','calendar/gantt/complete/id/107'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":16, \"send\":1}','calendar/gantt/complete/id/109'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":16}','calendar/gantt/complete/id/109'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":16, \"send\":1}','calendar/gantt/complete/id/111'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"save\":1}','calendar/gantt/add/id/113'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"save\":1}','calendar/gantt/add/id/114'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"save\":1}','calendar/gantt/add/id/120'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"send\":1}','calendar/gantt/add/id/113'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":17}','calendar/gantt/add/id/113'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"send\":1}','calendar/gantt/complete/id/113'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":17}','calendar/gantt/complete/id/113'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"send\":1}','calendar/gantt/add/id/114'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":17}','calendar/gantt/add/id/114'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":17, \"send\":1}','calendar/gantt/add/id/120'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":17}','calendar/gantt/add/id/120'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":13, \"save\":1}','calendar/gantt/add/id/126'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":13, \"send\":1}','calendar/gantt/add/id/126'),
				('proposal/proposal/send','{\"fairId\":10556, \"ecId\":13}','calendar/gantt/add/id/126'),
				('proposal/proposal/create','{\"fairId\":10556, \"ecId\":13, \"send\":1}','calendar/gantt/complete/id/126');

				UPDATE {{calendarfairtasks}} SET `parent`='99' WHERE `id`='100';
				UPDATE {{calendarfairtasks}} SET `parent`='101' WHERE `id`='102';
				UPDATE {{calendarfairtasks}} SET `parent`='103' WHERE `id`='104';
				UPDATE {{calendarfairtasks}} SET `parent`='106' WHERE `id`='107';
				UPDATE {{calendarfairtasks}} SET `parent`='108' WHERE `id`='109';
				UPDATE {{calendarfairtasks}} SET `parent`='110' WHERE `id`='111';
				UPDATE {{calendarfairtasks}} SET `parent`='112' WHERE `id`='113';
				UPDATE {{calendarfairtasks}} SET `parent`='114' WHERE `id`='115';
				UPDATE {{calendarfairtasks}} SET `parent`='116' WHERE `id`='117';
				UPDATE {{calendarfairtasks}} SET `parent`='77' WHERE `id`='78';
				UPDATE {{calendarfairtasks}} SET `parent`='79' WHERE `id`='80';
				UPDATE {{calendarfairtasks}} SET `parent`='81' WHERE `id`='82';
				UPDATE {{calendarfairtasks}} SET `parent`='82' WHERE `id`='83';
				UPDATE {{calendarfairtasks}} SET `parent`='83' WHERE `id`='84';
				UPDATE {{calendarfairtasks}} SET `parent`='82' WHERE `id`='85';
				UPDATE {{calendarfairtasks}} SET `parent`='87' WHERE `id`='88';
				UPDATE {{calendarfairtasks}} SET `parent`='89' WHERE `id`='90';
				UPDATE {{calendarfairtasks}} SET `parent`='91' WHERE `id`='92';
				UPDATE {{calendarfairtasks}} SET `parent`='93' WHERE `id`='94';
				UPDATE {{calendarfairtasks}} SET `parent`='95' WHERE `id`='96';
				UPDATE {{calendarfairtasks}} SET `parent`='97' WHERE `id`='98';
				UPDATE {{calendarfairtasks}} SET `parent`='85' WHERE `id`='119';
				UPDATE {{calendarfairtasks}} SET `parent`='74' WHERE `id`='83';
				UPDATE {{calendarfairtasks}} SET `parent`='74' WHERE `id`='85';
				UPDATE {{calendarfairtasks}} SET `parent`='86' WHERE `id`='76';

		";
	}
}