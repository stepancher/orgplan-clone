<?php

class m160428_110508_update_proposal_plan_files extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			UPDATE {{attributeclass}} SET `name`='n2_plan_files', `label`=NULL, `class`='file', `hasFile`='1', `priority`='4' WHERE `id`='313';
			UPDATE {{attributeclass}} SET `priority`='3' WHERE `id`='314';

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES
			('n2_plan_text', 'string', '0', 'Загрузите план стенда. При необходимости можете скачать бланк.', '312', '312', '1', 'hint', '0'),
			('n2_plan_link', 'string', '0', 'Скачать бланк \"План Стенда\"', '312', '312', '2', 'envLink', '0');

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES
			((SELECT `id` FROM {{attributeclass}} WHERE `name` = 'n2_plan_link'), 'schema', 'envLink');

			UPDATE {{attributeclass}} SET `name`='n3_plan_files', `label`=NULL, `class`='file', `hasFile`='1', `priority`='4' WHERE `id`='330';
			UPDATE {{attributeclass}} SET `priority`='3' WHERE `id`='335';
			UPDATE {{attributeclass}} SET `priority`='6', `label`='Не изображенное оборудование на стенд установлено не будет.' WHERE `id`='334';

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES
			('n3_plan_text', 'string', '0', 'Загрузите план стенда. При необходимости можете скачать бланк.', '329', '329', '1', 'hint', '0'),
			('n3_plan_link', 'string', '0', 'Скачать бланк \"План Стенда\"', '329', '329', '2', 'envLink', '0');

			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES
			((SELECT `id` FROM {{attributeclass}} WHERE `name` = 'n3_plan_link'), 'schema', 'envLink');

			DELETE FROM {{attributeclass}} WHERE `id`='331';
			DELETE FROM {{attributeclass}} WHERE `id`='333';

			INSERT INTO {{documents}} (`id`, `name`, `type`, `value`, `active`) VALUES
			('7', 'Схема размещения оборудования на стенде', '2', '/static/documents/fairs/184/plan.png', '1');
		";
	}

	private function downSql() {

		return "";
	}
}