<?php

class m160913_112744_add_fair_module_seo extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            update {{seo}} set route = 'fair/default/view' where route = 'fair/view';
            update {{seo}} set route = 'fair/default/fairComment' where route = 'fair/fairComment';
            update {{seo}} set route = 'regionInformation/default/view' where route = 'regionInformation/view';
            
            update {{seo}} set route = 'exhibitionComplex/default/view' where route = 'exhibitionComplex/view';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}