<?php

class m160302_131747_update_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `parent`='136', `priority`='3' WHERE `id`='236';
			DELETE FROM {{attributeclass}} WHERE `id`='199';
			UPDATE {{attributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='223';
			UPDATE {{attributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 22 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='209';
			UPDATE {{attributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='177';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='17';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='18';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='19';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='20';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='21';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='22';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='23';
			UPDATE {{attributeclasscoefficient}} SET `coefficient`='2' WHERE `id`='24';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='173';
			UPDATE {{attributeclass}} SET `priority`='8' WHERE `id`='34';
		";
	}
}