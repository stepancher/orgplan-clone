<?php

class m161116_111417_insert_invite_persons extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        return "

            CREATE TABLE IF NOT EXISTS {{invite}} (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `email` varchar(255) NOT NULL,
              `name` varchar(255) NOT NULL,
              `company` text NOT NULL,
              `hash` varchar(255) NOT NULL,
              `siteVisitTime` datetime NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

            DELETE FROM {{invite}};

            INSERT INTO {{invite}} (`id`, `email`, `name`, `company`, `hash`, `siteVisitTime`) VALUES
                (1, 'info@afag.de', 'Herr Könicke', 'AFAG Messen und Ausstellungen GmbH', 'd9356d91a41fe86906e0c979b6118c35', '0000-00-00 00:00:00'),
                (2, 'info@messeaugsburg.de', 'Herr Reiter', 'ASMV GmbH Messe Augsburg', '5f0216c849df52e8e1b79437378e3bd6', '0000-00-00 00:00:00'),
                (3, 'm.kynast@c3-chemnitz.de', 'Herr Kynast', 'C³ Chemnitzer Veranstaltungszentren GmbH c/o Messe Chemnitz', 'f6ba05f9e0a2f79f6f21a6181ae901e9', '0000-00-00 00:00:00'),
                (4, 'service@messedornbirn.at', 'Herr Mutschlechner', 'Dornbirner Messe GmbH', 'dd02837209b7a0af286c236ef7994c2e', '0000-00-00 00:00:00'),
                (5, 'info@exclusiv-events.com', 'Herr Hertwig', 'exclusiv events', '1244b14b7893deac4cd2ccf8652d5eec', '0000-00-00 00:00:00'),
                (6, 'carola.schwennsen@fh.messe.de', 'Frau Schwennsen', 'Fachausstellungen Heckmann GmbH', '8acf04f877328fb76fdaa6261750ae53', '0000-00-00 00:00:00'),
                (7, 'willenberg@forumfutura.de', 'Herr Willenberg', 'Forum Futura UG', 'fdef6b707087c468dcc5b842d3a6bd09', '0000-00-00 00:00:00'),
                (8, 'Daniel.Strowitzki@fwtm.de', 'Herr Strowitzki', 'Freiburg Wirtschaft Touristik und Messe GmbH & Co. KG', '0126fa9580852a4f705d658a25a86949', '0000-00-00 00:00:00'),
                (9, 'Bernd.Dallmann@fwtm.de', 'Herr Dallmann', 'Freiburg Wirtschaft Touristik und Messe GmbH & Co. KG', 'be9de6010b5d5b5d89884c836ec02ba3', '0000-00-00 00:00:00'),
                (10, ' info@globana.com', 'Herr Sturm', 'GLOBANA TRADE CENTER', 'c26761e718e6a4529250889e6b094138', '0000-00-00 00:00:00'),
                (11, 'mkohrmann@hinte-messe.de', 'Herr  Hinte', 'HINTE GmbH', 'c16e51e813dec3274d9f825ec2fe7c83', '0000-00-00 00:00:00'),
                (12, 'office@intergem-messe.de', 'Herr Hille', 'Intergem Messe GmbH', '7de2a75e8501451774580b4baf31c25d', '0000-00-00 00:00:00'),
                (13, 'festwoche@kempten.de', 'Frau Dufner-Wucher', 'Kempten Messe- und Veranstaltungsbetrieb', '31fbc203b2d9d326b1fb7b6a89b51499', '0000-00-00 00:00:00'),
                (14, 'kinold@kinold.de', 'Herr Kinold', 'Kinold Ausstellungsgesellschaft mbH', '8b82193ed4a67c219a69f8c33a3549fa', '0000-00-00 00:00:00'),
                (15, 'info@mahab-mannheim.de', 'Frau Goschmann', 'Mannheimer Hallenbetriebs-GmbH', '98928edc12a31ddbcf0876224d5f0289', '0000-00-00 00:00:00'),
                (16, 'info@mahab-mannheim.de', 'Herr Goschmann', 'Mannheimer Hallenbetriebs-GmbH', '98928edc12a31ddbcf0876224d5f0289', '0000-00-00 00:00:00'),
                (17, 'weissenborn@messe-erfurt.de', 'Herr Weißenborn', 'Messe Erfurt GmbH', '8f6a97b00323a6da3ed82562a4f5d119', '0000-00-00 00:00:00'),
                (18, 'info@messehusum.de', 'Herr Becker', 'Messe Husum & Congress GmbH & Co. KG', 'b85a70c9f7080a8fd53d9a52ddc1e368', '0000-00-00 00:00:00'),
                (19, 'kircher@messe-offenburg.de', 'Frau Kircher', 'Messe Offenburg-Ortenau GmbH', '3aa9137d1c702e2732d90abdb2feeac2', '0000-00-00 00:00:00'),
                (20, 'nfo@messeostwestfalen.de', 'Herr Reibchen', 'Messe Ostwestfalen GmbH', '74c80c6365ff8d38ce8cc974707f080c', '0000-00-00 00:00:00'),
                (21, 'nfo@messeostwestfalen.de', 'Herr Schäfermeier', 'Messe Ostwestfalen GmbH', '74c80c6365ff8d38ce8cc974707f080c', '0000-00-00 00:00:00'),
                (22, ' office@messe-ried.at', 'Herr Slezak', 'MESSE RIED GmbH', 'd32e8da2aee157545abb31b91c88c69c', '0000-00-00 00:00:00'),
                (23, 'paschke@mcc-halle-muensterland.de', 'Frau Paschke', 'Messe und Congress Centrum Halle Münsterland GmbH', 'a1bd50a894d6e22bc0b6126eeffb08b8', '0000-00-00 00:00:00'),
                (24, 'messe@stadt-waechtersbach.de', 'Herr Wilhelm', 'Messe Wächtersbach GmbH', '9ea01532736468bc025bc78514d9878f', '0000-00-00 00:00:00'),
                (25, 'geschaeftsfuehrer@mvgm.de', 'Herr Schüller', 'Messe- und Veranstaltungsgesellschaft Magdeburg GmbH MVGM GmbH', '932398af86f89d325687620da5bb40a9', '0000-00-00 00:00:00'),
                (26, 'wagner@messezentrum-salzburg.at', 'Herr Wagner', 'Messezentrum Salzburg GmbH', 'c86454e854bbcb056686f02e00b74a1d', '0000-00-00 00:00:00'),
                (27, 'willi.schaugg@ravensburg.de', 'Herr Schaugg', 'Oberschwabenhallen Ravensburg GmbH', 'd8d3e99a929f23d54eb10457cfa8e0af', '0000-00-00 00:00:00'),
                (28, 'c.kreuser@ram-gmbH.de', 'Frau Kreuser', 'RAM Regio Ausstellungs GmbH Erfurt', '84ecc1a72d77ae8be1200f2b77c88c7a', '0000-00-00 00:00:00'),
                (29, ' e.kreuser@ram-gmbh.de', 'Herr Kreuser', 'RAM Regio Ausstellungs GmbH Erfurt', 'f038b42f053761d4c3a8c8dfb5a26d9b', '0000-00-00 00:00:00'),
                (30, 'infomainz@ram-gmbh.de', 'Herr Kreuser', 'RAM Regio Ausstellungs GmbH Mainz', '2881c3be8542cb9de5302de1ccf512d6', '0000-00-00 00:00:00'),
                (31, ' michael.freter@reedexpo.de', 'Herr Freter', 'Reed Exhibitions Deutschland GmbH', 'ee7d52a3060a4e333ad51ce644ac11c0', '0000-00-00 00:00:00'),
                (32, 'hans-joachim.erbel@reedexpo.de', 'Herr Erbel', 'Reed Exhibitions Deutschland GmbH', 'ace6ea37dcef71ac6ce18ee61cffc99e', '0000-00-00 00:00:00'),
                (33, 'stefan.kaufmann@rmcc.de', 'Herr Kaufmann', 'Rhein-Main-Hallen GmbH', '154636a68538c0ceeda29eb1cdaec316', '0000-00-00 00:00:00'),
                (34, 'p.burmeister@messeundstadthalle.de', 'Frau Burmeister', 'Rostocker Messe- und Stadthallengesellschaft mbH, HanseMesse', 'd1cb6b0783811ac5eb65c656a6600e48', '0000-00-00 00:00:00'),
                (35, 'marion.linder@saarbruecken.de', 'Frau Linder', 'Saarmesse GmbH', '8673ef79ce2bfc8a9e84bba5e6768f64', '0000-00-00 00:00:00'),
                (36, 'r.kirch@saarmesse.de', 'Herr Kirch', 'Saarmesse GmbH', 'ddb99edab79c1eb33d094d6845a5d3e6', '0000-00-00 00:00:00'),
                (37, 'info@jws.de', 'Herr Schmid', 'Messe- und Ausstellungsorganisation Josef Werner Schmid GmbH', '00e679b814ab9d61a57284f76a611136', '0000-00-00 00:00:00'),
                (38, 'schellkes@wellfairs.de', 'Herr Schllkes', 'Wellfairs GmbH', 'd6a6deb0ef81a32cb8f38b8ec4a548f6', '0000-00-00 00:00:00'),
                (39, 'info@ulm-messe.de', 'Herr Eilts', 'Ulm-Messe GmbH', '420b723cb019cd516a8de164cc121b3a', '0000-00-00 00:00:00');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}