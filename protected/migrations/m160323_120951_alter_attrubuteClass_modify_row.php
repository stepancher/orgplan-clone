<?php

class m160323_120951_alter_attrubuteClass_modify_row extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `label`='При необходимости подключения оборудования мощностью более 60 кВт необходимо заказать несколько подключений кратных указанным выше мощностям.<br/>\nСечение электрического кабеля для подключения электрооборудования к источнику электроэнергии должно соответствовать нагрузке. Для стендов самостоятельной застройки силовой кабель предоставляется застройщиком стенда (экспонентом), при этом длина кабеля должна быть не менее 30 метров. <br/><br/> Подключение электрооборудования участника мероприятия к источнику электроснабжения производится только после выполнения работ по замеру сопротивления изоляции подключаемой электрической схемы.' WHERE `id`='237';
		";
	}
}