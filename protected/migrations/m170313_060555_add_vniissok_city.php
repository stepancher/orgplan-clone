<?php

class m170313_060555_add_vniissok_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('31', 'vniissok');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1536', 'ru', 'Посёлок ВНИИССОК');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1536', 'en', 'Посёлок ВНИИССОК');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1536', 'de', 'Посёлок ВНИИССОК');
            
            DELETE FROM {{organizercompany}} WHERE `id` = '102';
            DELETE FROM {{organizer}} WHERE `id` = '435';
            
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21019';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21020';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21021';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21022';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21023';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21024';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21025';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21026';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21027';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21028';

            DELETE FROM {{organizercompany}} WHERE `id` = '177';
            DELETE FROM {{organizercompany}} WHERE `id` = '178';
            DELETE FROM {{organizercompany}} WHERE `id` = '180';
            DELETE FROM {{organizercompany}} WHERE `id` = '199';
            DELETE FROM {{organizercompany}} WHERE `id` = '257';
            DELETE FROM {{organizercompany}} WHERE `id` = '272';
            DELETE FROM {{organizercompany}} WHERE `id` = '278';
            DELETE FROM {{organizercompany}} WHERE `id` = '279';
            DELETE FROM {{organizercompany}} WHERE `id` = '280';
            
            DELETE FROM {{organizer}} WHERE `id` = '139';
            DELETE FROM {{organizer}} WHERE `id` = '345';
            DELETE FROM {{organizer}} WHERE `id` = '385';
            DELETE FROM {{organizer}} WHERE `id` = '384';
            DELETE FROM {{organizer}} WHERE `id` = '371';
            DELETE FROM {{organizer}} WHERE `id` = '1652';
            DELETE FROM {{organizer}} WHERE `id` = '1335';
            DELETE FROM {{organizer}} WHERE `id` = '312';
            DELETE FROM {{organizer}} WHERE `id` = '432';
            DELETE FROM {{organizer}} WHERE `id` = '894';
            
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18083';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18084';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18085';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18086';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18087';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18088';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18089';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18090';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18091';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18092';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18093';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18094';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18095';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18096';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18097';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18098';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18099';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18100';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18101';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18102';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20107';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20108';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20109';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20110';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20111';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20112';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20113';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20114';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20115';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20116';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20117';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20118';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20119';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20120';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20121';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20122';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20123';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20124';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20125';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20126';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20563';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20564';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20565';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20566';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20567';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20568';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20569';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20570';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20571';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20572';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20573';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20574';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20575';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20576';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20577';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20578';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20579';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20580';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20581';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20582';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20553';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20554';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20555';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20556';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20557';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20558';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20559';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20560';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20561';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20562';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20380';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20381';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20382';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20383';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20384';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20385';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20386';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20387';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20388';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20389';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33333';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33334';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33335';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33336';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33337';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33338';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33339';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33340';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33341';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '33342';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '30254';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '30255';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '30256';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '30257';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '30258';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19790';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19791';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19792';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19793';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19794';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19795';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19796';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19797';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19798';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '19799';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20985';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20986';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20987';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20988';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20989';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20990';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20991';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20992';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20993';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20994';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20995';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20996';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20997';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20998';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20999';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21000';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21001';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21002';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21003';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '21004';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26155';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26156';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26157';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26158';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26159';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26160';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26161';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26162';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26163';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '26164';
            
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3184';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2996';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2956';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2957';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2970';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2049';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3025';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2910';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2478';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3919';
            
            DELETE FROM {{fair}} WHERE `id` = '4860';
            DELETE FROM {{fair}} WHERE `id` = '4502';
            DELETE FROM {{fair}} WHERE `id` = '4428';
            DELETE FROM {{fair}} WHERE `id` = '4430';
            DELETE FROM {{fair}} WHERE `id` = '4452';
            DELETE FROM {{fair}} WHERE `id` = '3337';
            DELETE FROM {{fair}} WHERE `id` = '4566';
            DELETE FROM {{fair}} WHERE `id` = '4350';
            DELETE FROM {{fair}} WHERE `id` = '3828';
            DELETE FROM {{fair}} WHERE `id` = '10735';
            
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14585';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14650';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14584';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14731';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14889';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '15920';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '27630';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '57101';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '15980';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '15979';
            
            DELETE FROM {{organizercompany}} WHERE `id` = '418';
            DELETE FROM {{organizercompany}} WHERE `id` = '430';
            
            DELETE FROM {{organizer}} WHERE `id` = '141';
            DELETE FROM {{organizer}} WHERE `id` = '1473';
            
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18103';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18104';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18105';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18106';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18107';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18108';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18109';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18110';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18111';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '18112';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31766';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31767';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31768';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31769';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31770';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31771';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31772';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31773';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31774';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '31775';
            
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '3183';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '1914';
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('23', 'ust-labinsk');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1537', 'ru', 'Усть-Лабинск');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1537', 'en', 'Ust-Labinsk');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1537', 'de', 'Ust-Labinsk');
            
            DELETE FROM {{exhibitioncomplex}} WHERE `id` = '5675';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}