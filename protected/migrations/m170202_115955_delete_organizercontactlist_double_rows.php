<?php

class m170202_115955_delete_organizercontactlist_double_rows extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `delete_organizercontactlist_double_rows`;
            
            CREATE PROCEDURE `delete_organizercontactlist_double_rows`()
            BEGIN
                
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE contact_id INT DEFAULT 0;
                DECLARE contact_value TEXT DEFAULT '';
                DECLARE contact_type INT DEFAULT 0;
                DECLARE contact_organizer_id INT DEFAULT 0;
                
                DECLARE del CURSOR FOR select id, value, type, organizerId
                        from (select value, organizerId, count(id) as count, id, type from {{organizercontactlist}} 
                        group by value, organizerId, type
                        ) t where count > 1;
                    
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN del;
                    
                    read_loop: LOOP
                        
                        FETCH del INTO contact_id, contact_value, contact_type, contact_organizer_id;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            DELETE FROM {{organizercontactlist}} WHERE `organizerId` = contact_organizer_id 
                                                                   AND `value` = contact_value
                                                                   AND `type` = contact_type
                                                                   AND `id` != contact_id;
                        COMMIT;
                        
                    END LOOP;
                CLOSE del;
            END;
            
            CALL delete_organizercontactlist_double_rows();
            DROP PROCEDURE IF EXISTS `delete_organizercontactlist_double_rows`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}