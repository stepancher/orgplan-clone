<?php

class m161026_071223_add_fair_version extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{scdtrfair}} LIKE {{trfair}};
            
            ALTER TABLE {{scdtrfair}} 
            ADD COLUMN `changeDate` DATE NULL DEFAULT NULL AFTER `preserveUrl`,
            ADD COLUMN `whoChange` VARCHAR(45) NULL DEFAULT NULL AFTER `changeDate`;

            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `changeDate` `changeDate` DATETIME NULL DEFAULT NULL ;

            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `changeDate` `changeDate` TIMESTAMP NULL DEFAULT NULL ;

            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `changeDate` `changeDate` TIMESTAMP NOT NULL ;

            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `changeDate` `changeDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;
            
            ALTER TABLE {{scdtrfair}} 
            DROP INDEX `trParentIdAndLangId` ;
            
            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `id` `id` INT(11) NULL DEFAULT NULL ,
            ADD COLUMN `scd_id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
            DROP PRIMARY KEY,
            ADD PRIMARY KEY (`scd_id`);
            
            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `scd_id` `scdId` INT(11) NOT NULL AUTO_INCREMENT ;
            
            ALTER TABLE {{scdtrfair}} 
            CHANGE COLUMN `changeDate` `manipulateDatetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
            CHANGE COLUMN `whoChange` `manipulateUser` VARCHAR(45) NULL DEFAULT NULL ;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}