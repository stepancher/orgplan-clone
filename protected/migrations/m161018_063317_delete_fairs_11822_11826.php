<?php

class m161018_063317_delete_fairs_11822_11826 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE `id` = '11822';
            DELETE FROM {{fair}} WHERE `id` = '11823';
            DELETE FROM {{fair}} WHERE `id` = '11824';
            DELETE FROM {{fair}} WHERE `id` = '11825';
            DELETE FROM {{fair}} WHERE `id` = '11826';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}