<?php

class m151105_071922_table_chartgroup extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{chartgroup}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return '
			DROP TABLE IF EXISTS {{chartgroup}};

			CREATE TABLE IF NOT EXISTS {{chartgroup}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `header` varchar(200) DEFAULT NULL,
			  `type` int(11) DEFAULT NULL,
			  `industryId` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

			INSERT INTO {{chartgroup}} (`id`, `header`, `type`, `industryId`) VALUES
				(1, "", 1, 11),
				(2, NULL, 1, 11),
				(3, NULL, 1, 11),
				(4, "Структура сельского хозяйства / Растениеводство", 2, 11),
				(5, "Структура сельского хозяйства / Животноводство", 2, 11),
				(6, "Основные показатели сельского хозяйства / Растениеводство", 2, 11),
				(7, "Основные показатели сельского хозяйства / Животноводство", 2, 11);
		';
	}
}