<?php

class m160728_065334_change_district_shorturl_and_names extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{district}} SET `shortUrl`='odessa-oblast' WHERE `shortUrl`='Odessa-oblast';
UPDATE {{district}} SET `shortUrl`='kiev-district' WHERE `shortUrl`='Kiev-district';
UPDATE {{district}} SET `shortUrl`='ulaanbaatar-district' WHERE `shortUrl`='Ulaanbaatar-district';
UPDATE {{district}} SET `shortUrl`='tbilisi-district' WHERE `shortUrl`='Tbilisi-district';
UPDATE {{district}} SET `shortUrl`='balkan-district' WHERE `shortUrl`='Balkan-district';
UPDATE {{district}} SET `shortUrl`='ashgabat-district' WHERE `shortUrl`='Ashgabat-district';
UPDATE {{district}} SET `shortUrl`='fergana-district' WHERE `shortUrl`='Fergana-district';
UPDATE {{district}} SET `shortUrl`='tashkent-district' WHERE `shortUrl`='Tashkent-district';
UPDATE {{district}} SET `shortUrl`='karaganda-district' WHERE `shortUrl`='Karaganda-district';
UPDATE {{district}} SET `shortUrl`='kishinev-district' WHERE `shortUrl`='Kishinev-district';
UPDATE {{district}} SET `shortUrl`='bishkek-district' WHERE `shortUrl`='Bishkek-district';
UPDATE {{district}} SET `shortUrl`='pavlodar-district' WHERE `shortUrl`='Pavlodar-district';
UPDATE {{district}} SET `shortUrl`='aktobe-district' WHERE `shortUrl`='Aktobe-district';
UPDATE {{district}} SET `shortUrl`='mangystau-district' WHERE `shortUrl`='Mangystau-district';
UPDATE {{district}} SET `shortUrl`='atyrau-district' WHERE `shortUrl`='Atyrau-district';
UPDATE {{district}} SET `shortUrl`='almaty-district' WHERE `shortUrl`='Almaty-district';
UPDATE {{district}} SET `shortUrl`='astana-district' WHERE `shortUrl`='Astana-district';
UPDATE {{district}} SET `shortUrl`='grodno-district' WHERE `shortUrl`='Grodno-district';
UPDATE {{district}} SET `shortUrl`='mogilev-district' WHERE `shortUrl`='Mogilev-district';
UPDATE {{district}} SET `shortUrl`='minsk-district' WHERE `shortUrl`='Minsk-district';
UPDATE {{district}} SET `shortUrl`='yerevan-district' WHERE `shortUrl`='Yerevan-district';
UPDATE {{district}} SET `shortUrl`='baku-district' WHERE `shortUrl`='Baku-district';
UPDATE {{district}} SET `shortUrl`='berlin' WHERE `shortUrl`='Berlin';
UPDATE {{district}} SET `shortUrl`='bremen' WHERE `shortUrl`='Bremen';
UPDATE {{district}} SET `shortUrl`='hamburg' WHERE `shortUrl`='Hamburg';
UPDATE {{district}} SET `shortUrl`='saarland' WHERE `shortUrl`='Saarland';
UPDATE {{district}} SET `shortUrl`='rheinland-pfalz' WHERE `shortUrl`='Rheinland-Pfalz';
UPDATE {{district}} SET `shortUrl`='hessen' WHERE `shortUrl`='Hessen';
UPDATE {{district}} SET `shortUrl`='thüringen' WHERE `shortUrl`='Thüringen';
UPDATE {{district}} SET `shortUrl`='sachsen' WHERE `shortUrl`='Sachsen';
UPDATE {{district}} SET `shortUrl`='schleswig-holstein' WHERE `shortUrl`='Schleswig-Holstein';
UPDATE {{district}} SET `shortUrl`='sachsen-anhalt' WHERE `shortUrl`='Sachsen-Anhalt';
UPDATE {{district}} SET `shortUrl`='brandenburg' WHERE `shortUrl`='Brandenburg';
UPDATE {{district}} SET `shortUrl`='mecklenburg-vorpommern' WHERE `shortUrl`='Mecklenburg-Vorpommern';
UPDATE {{district}} SET `shortUrl`='baden-württemberg' WHERE `shortUrl`='Baden-Württemberg';
UPDATE {{district}} SET `shortUrl`='nordrhein-westfalen' WHERE `shortUrl`='Nordrhein-Westfalen';
UPDATE {{district}} SET `shortUrl`='niedersachsen' WHERE `shortUrl`='Niedersachsen';
UPDATE {{district}} SET `shortUrl`='bayern' WHERE `shortUrl`='Bayern';

UPDATE {{region}} SET `shortUrl`='baku-region' WHERE `shortUrl`='Baku-region';
UPDATE {{region}} SET `shortUrl`='yerevan-region' WHERE `shortUrl`='Yerevan-region';
UPDATE {{region}} SET `shortUrl`='minsk-region' WHERE `shortUrl`='Minsk-region';
UPDATE {{region}} SET `shortUrl`='mogilev-region' WHERE `shortUrl`='Mogilev-region';
UPDATE {{region}} SET `shortUrl`='grodno-region' WHERE `shortUrl`='Grodno-region';
UPDATE {{region}} SET `shortUrl`='astana-region' WHERE `shortUrl`='Astana-region';
UPDATE {{region}} SET `shortUrl`='almaty-region' WHERE `shortUrl`='Almaty-region';
UPDATE {{region}} SET `shortUrl`='atyrau-region' WHERE `shortUrl`='Atyrau-region';
UPDATE {{region}} SET `shortUrl`='mangystau-region' WHERE `shortUrl`='Mangystau-region';
UPDATE {{region}} SET `shortUrl`='aktobe-region' WHERE `shortUrl`='Aktobe-region';
UPDATE {{region}} SET `shortUrl`='pavlodar-region' WHERE `shortUrl`='Pavlodar-region';
UPDATE {{region}} SET `shortUrl`='bishkek-region' WHERE `shortUrl`='Bishkek-region';
UPDATE {{region}} SET `shortUrl`='kishinev-region' WHERE `shortUrl`='Kishinev-region';
UPDATE {{region}} SET `shortUrl`='karaganda-region' WHERE `shortUrl`='Karaganda-region';
UPDATE {{region}} SET `shortUrl`='tashkent-region' WHERE `shortUrl`='Tashkent-region';
UPDATE {{region}} SET `shortUrl`='fergana-region' WHERE `shortUrl`='Fergana-region';
UPDATE {{region}} SET `shortUrl`='ashgabat-region' WHERE `shortUrl`='Ashgabat-region';
UPDATE {{region}} SET `shortUrl`='balkan-region' WHERE `shortUrl`='Balkan-region';
UPDATE {{region}} SET `shortUrl`='tbilisi-region' WHERE `shortUrl`='Tbilisi-region';
UPDATE {{region}} SET `shortUrl`='ulaanbaatar-region' WHERE `shortUrl`='Ulaanbaatar-region';
UPDATE {{region}} SET `shortUrl`='kiev-region' WHERE `shortUrl`='Kiev-region';
UPDATE {{region}} SET `shortUrl`='odessa-oblast' WHERE `shortUrl`='Odessa-oblast';

UPDATE {{trdistrict}} SET `name`='Baku District' WHERE `name`='Baku-district';
UPDATE {{trdistrict}} SET `name`='Yerevan District' WHERE `name`='Yerevan-district';
UPDATE {{trdistrict}} SET `name`='Minsk District' WHERE `name`='Minsk-district';
UPDATE {{trdistrict}} SET `name`='Mogilev District' WHERE `name`='Mogilev-district';
UPDATE {{trdistrict}} SET `name`='Grodno District' WHERE `name`='Grodno-district';
UPDATE {{trdistrict}} SET `name`='Astana District' WHERE `name`='Astana-district';
UPDATE {{trdistrict}} SET `name`='Almaty District' WHERE `name`='Almaty-district';
UPDATE {{trdistrict}} SET `name`='Atyrau District' WHERE `name`='Atyrau-district';
UPDATE {{trdistrict}} SET `name`='Mangystau District' WHERE `name`='Mangystau-district';
UPDATE {{trdistrict}} SET `name`='Aktobe District' WHERE `name`='Aktobe-district';
UPDATE {{trdistrict}} SET `name`='Pavlodar District' WHERE `name`='Pavlodar-district';
UPDATE {{trdistrict}} SET `name`='Bishkek District' WHERE `name`='Bishkek-district';
UPDATE {{trdistrict}} SET `name`='Kishinev District' WHERE `name`='Kishinev-district';
UPDATE {{trdistrict}} SET `name`='Karaganda District' WHERE `name`='Karaganda-district';
UPDATE {{trdistrict}} SET `name`='Tashkent District' WHERE `name`='Tashkent-district';
UPDATE {{trdistrict}} SET `name`='Fergana District' WHERE `name`='Fergana-district';
UPDATE {{trdistrict}} SET `name`='Ashgabat District' WHERE `name`='Ashgabat-district';
UPDATE {{trdistrict}} SET `name`='Balkan District' WHERE `name`='Balkan-district';
UPDATE {{trdistrict}} SET `name`='Tbilisi District' WHERE `name`='Tbilisi-district';
UPDATE {{trdistrict}} SET `name`='Ulaanbaatar District' WHERE `name`='Ulaanbaatar-district';
UPDATE {{trdistrict}} SET `name`='Kiev District' WHERE `name`='Kiev-district';
UPDATE {{trdistrict}} SET `name`='Odessa Oblast' WHERE `name`='Odessa-oblast';

UPDATE {{trregion}} SET `name`='Baku Region' WHERE `name`='Baku-region';
UPDATE {{trregion}} SET `name`='Yerevan Region' WHERE `name`='Yerevan-region';
UPDATE {{trregion}} SET `name`='Minsk Region' WHERE `name`='Minsk-region';
UPDATE {{trregion}} SET `name`='Mogilev Region' WHERE `name`='Mogilev-region';
UPDATE {{trregion}} SET `name`='Grodno Region' WHERE `name`='Grodno-region';
UPDATE {{trregion}} SET `name`='Astana Region' WHERE `name`='Astana-region';
UPDATE {{trregion}} SET `name`='Almaty Region' WHERE `name`='Almaty-region';
UPDATE {{trregion}} SET `name`='Atyrau Region' WHERE `name`='Atyrau-region';
UPDATE {{trregion}} SET `name`='Mangystau Region' WHERE `name`='Mangystau-region';
UPDATE {{trregion}} SET `name`='Aktobe Region' WHERE `name`='Aktobe-region';
UPDATE {{trregion}} SET `name`='Pavlodar Region' WHERE `name`='Pavlodar-region';
UPDATE {{trregion}} SET `name`='Bishkek Region' WHERE `name`='Bishkek-region';
UPDATE {{trregion}} SET `name`='Kishinev Region' WHERE `name`='Kishinev-region';
UPDATE {{trregion}} SET `name`='Karaganda Region' WHERE `name`='Karaganda-region';
UPDATE {{trregion}} SET `name`='Tashkent Region' WHERE `name`='Tashkent-region';
UPDATE {{trregion}} SET `name`='Fergana Region' WHERE `name`='Fergana-region';
UPDATE {{trregion}} SET `name`='Ashgabat Region' WHERE `name`='Ashgabat-region';
UPDATE {{trregion}} SET `name`='Balkan Region' WHERE `name`='Balkan-region';
UPDATE {{trregion}} SET `name`='Tbilisi Region' WHERE `name`='Tbilisi-region';
UPDATE {{trregion}} SET `name`='Ulaanbaatar Region' WHERE `name`='Ulaanbaatar-region';
UPDATE {{trregion}} SET `name`='Kiev Region' WHERE `name`='Kiev-region';
UPDATE {{trregion}} SET `name`='Odessa Oblast' WHERE `name`='Odessa-oblast';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}