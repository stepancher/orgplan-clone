<?php

class m161216_075641_add_uaserHasOrganizer extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{userhasorganizer}} SET `organizerId`='4103' WHERE `id`='18';
            DELETE FROM {{userhasorganizer}} WHERE `id`='14';
            DELETE FROM {{myfair}} WHERE `id`='454';
            DELETE FROM {{userhasorganizer}} WHERE `id`='13';
            DELETE FROM {{myfair}} WHERE `id`='453';
            INSERT INTO {{userhasorganizer}} (`organizerId`, `userId`) VALUES ('4102', '2655');
            INSERT INTO {{myfair}} (`userId`, `fairId`, `status`, `createdAt`, `updatedAt`) VALUES ('2655', '10943', '1', '2016-12-16 06:17:02', '2016-12-16 09:17:02');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}