<?php

class m161027_134042_add_de_tr_exhibitionComplexType extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{exhibitioncomplextype}} 
            DROP COLUMN `shortUrl`;

            UPDATE {{trexhibitioncomplextype}} SET `name`='Geschäfts-, Einkaufs- und Freizeitzentren', `fullNameSingle`='Geschäfts-, Einkaufs- und Freizeitzentrum', `shortUrl`='Geschäfts-, Einkaufs-, Freizeitzentren' WHERE `id`='1';
            UPDATE {{trexhibitioncomplextype}} SET `name`='Sport-, Wissenschafts- und Kulturzentren', `fullNameSingle`='Sport-, Wissenschafts- und Kulturzentrum', `shortUrl`='Sport-, Wissenschafts-, Kulturzentren' WHERE `id`='2';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='Hotels-Kongresshallen' WHERE `id`='3';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='Messezentren' WHERE `id`='4';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='Theater-Museen-Galerien' WHERE `id`='5';
            UPDATE {{trexhibitioncomplextype}} SET `name`='Spezialisierte Ausstellungsbereiche', `fullNameSingle`='Spezialisierter Ausstellungsbereich', `shortUrl`='Spezialisierte-Ausstellungsbereiche' WHERE `id`='6';
		    
		    UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='delovye-torgovo-razvlekatelnye-centry' WHERE `id`='14';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='sportivnye-nauchnye-kulturnye-centry' WHERE `id`='15';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='gostinicy-kongress-kholly' WHERE `id`='16';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='vystavochnye-centry' WHERE `id`='17';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='teatry-muzei-galerei' WHERE `id`='18';
            UPDATE {{trexhibitioncomplextype}} SET `shortUrl`='specializirovannye-ploshhadki' WHERE `id`='19';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}