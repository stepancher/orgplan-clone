<?php

class m160816_090616_create_json_elementClass_table extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql  = $this->getAlterTable();
        $transaction = Yii::app()->dbProposal->beginTransaction();
        try
        {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getAlterTable(){

        return "
            CREATE TABLE {{proposalelementclassjson}} (
              `elementClassId` INT NOT NULL,
              `reportJSON` MEDIUMTEXT NULL,
              `emptyJSON` MEDIUMTEXT NULL,
              `reportAttributeClass` MEDIUMTEXT NULL,
              PRIMARY KEY (`elementClassId`));
              
            insert into {{proposalelementclassjson}} (elementClassId, reportJSON, emptyJSON, reportAttributeClass)
                select id, reportJSON, emptyJSON, reportAttributeClass from {{proposalelementclass}};
                
            ALTER TABLE {{proposalelementclass}}
                DROP COLUMN `reportAttributeClass`,
                DROP COLUMN `emptyJSON`,
                DROP COLUMN `reportJSON`;
        ";
    }
}