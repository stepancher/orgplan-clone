<?php

class m170302_180056_rename extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
       /* $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);*/
        /**
         * @var Fair $fair
         */
        /*$sql = '';*/
       /* foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){*/
                $dbTargetName = Yii::app()->dbMan->createShardName(184);

                $sql = "
                    UPDATE `{$dbTargetName}`.`tbl_trreport` SET `trParentId`='7' WHERE `id`='13';
                    UPDATE `{$dbTargetName}`.`tbl_trreport` SET `trParentId`='8' WHERE `id`='14';
                    UPDATE `{$dbTargetName}`.`tbl_trreport` SET `trParentId`='7' WHERE `id`='15';
                    UPDATE `{$dbTargetName}`.`tbl_trreport` SET `trParentId`='8' WHERE `id`='16';
                    UPDATE `{$dbTargetName}`.`tbl_trreport` SET `trParentId`='7' WHERE `id`='23';
                    UPDATE `{$dbTargetName}`.`tbl_trreport` SET `trParentId`='8' WHERE `id`='24';
                ";
         /*   }
        }*/

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}