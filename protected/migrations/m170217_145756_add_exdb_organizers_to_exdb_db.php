<?php

class m170217_145756_add_exdb_organizers_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);

        return "
            DROP TABLE IF EXISTS {$expodata}.{{exdborganizercompany}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdborganizercompany}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `active` INT(11) NULL DEFAULT NULL,
              `linkToTheSiteOrganizers` VARCHAR(255) NULL DEFAULT NULL,
              `phone` VARCHAR(255) NULL DEFAULT NULL,
              `email` VARCHAR(255) NULL DEFAULT NULL,
              `cityId` INT(11) NULL DEFAULT NULL,
              `coordinates` VARCHAR(255) NULL DEFAULT NULL,
              `contacts_en` TEXT NULL DEFAULT NULL,
              `contacts_raw_en` TEXT NULL DEFAULT NULL,
                PRIMARY KEY (`id`));
            
            DELETE FROM {$expodata}.{{exdbtrorganizercompany}};
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_organizers_to_exdb_db`;
            
            CREATE PROCEDURE {$expodata}.`add_exdb_organizers_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE org_name TEXT DEFAULT '';
                DECLARE org_contacts TEXT DEFAULT '';
                DECLARE org_contacts_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT org.id, org.name_en, org.contacts_en, org.contacts_raw_en FROM {$expodataRaw}.expo_org org
                                            LEFT JOIN {$expodata}.{{exdborganizercompany}} o ON o.exdbId = org.id
                                        WHERE org.id != 0 AND org.name_en IS NOT NULL AND o.id IS NULL
                                        ORDER BY org.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_id, org_name, org_contacts, org_contacts_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdborganizercompany}} (`exdbId`,`contacts_en`,`contacts_raw_en`) VALUES (org_id, org_contacts, org_contacts_raw);
                        SET @last_insert_id := LAST_INSERT_ID();
                        INSERT INTO {$expodata}.{{exdbtrorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, 'ru');
                        INSERT INTO {$expodata}.{{exdbtrorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, 'en');
                        INSERT INTO {$expodata}.{{exdbtrorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_organizers_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}