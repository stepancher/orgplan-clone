<?php

class m160719_124107_new_document extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "			
			INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '184');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('14', 'ru', 'Инструкция для клиентов', '/static/documents/fairs/184/instruction.pdf');
		";
	}
}