<?php

class m160329_093612_delete_organizers extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			DELETE FROM {{organizer}} WHERE id IN (
				165,
				269,
				270,
				314,
				972,
				976,
				1004,
				1005,
				1006,
				1007,
				1008,
				1009,
				1010,
				1011,
				1018,
				1019,
				1020,
				1021,
				1022,
				1023,
				1072,
				1074,
				1109,
				1169,
				1189,
				1202,
				1204,
				1205,
				1211,
				1303,
				1514,
				1516,
				1517,
				1708,
				1883,
				1958,
				1959,
				1960,
				1962,
				1970,
				1974,
				1978,
				2035,
				2862,
				2870,
				2872,
				2876,
				2883,
				2885,
				2898,
				2899,
				2907,
				2958,
				2988,
				3036,
				3088,
				3119,
				3209,
				3221,
				3416,
				3454,
				3465,
				3516,
				3549,
				3561,
				3589
			);

			DELETE FROM {{trorganizer}} WHERE trParentId IN (
				165,
				269,
				270,
				314,
				972,
				976,
				1004,
				1005,
				1006,
				1007,
				1008,
				1009,
				1010,
				1011,
				1018,
				1019,
				1020,
				1021,
				1022,
				1023,
				1072,
				1074,
				1109,
				1169,
				1189,
				1202,
				1204,
				1205,
				1211,
				1303,
				1514,
				1516,
				1517,
				1708,
				1883,
				1958,
				1959,
				1960,
				1962,
				1970,
				1974,
				1978,
				2035,
				2862,
				2870,
				2872,
				2876,
				2883,
				2885,
				2898,
				2899,
				2907,
				2958,
				2988,
				3036,
				3088,
				3119,
				3209,
				3221,
				3416,
				3454,
				3465,
				3516,
				3549,
				3561,
				3589
			);
		';
	}
}