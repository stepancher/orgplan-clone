<?php

class m160722_114326_update_documents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "			
			INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '184');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('14', 'ru', 'Дополнительное соглашение', '/static/documents/fairs/184/agreement_ru.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('14', 'en', 'Supplementary Agreement', '/static/documents/fairs/184/agreement_en.pdf');
		";
	}
}