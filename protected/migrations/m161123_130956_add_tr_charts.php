<?php

class m161123_130956_add_tr_charts extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{trcharttablegroup}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(255) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('1', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('2', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('3', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('4', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('7', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('8', 'ru', 'Пиломатериалы');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('9', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('10', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('11', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('12', 'ru', 'Объем ресурсов');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('13', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('15', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('16', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('17', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('18', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('19', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('20', 'ru', 'Основные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('21', 'ru', 'Объем ресурсов');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('22', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('24', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('25', 'ru', 'Косвенные показатели отрасли');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('26', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('30', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('31', 'ru', 'Движение основных продуктов питания');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('32', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('34', 'ru', 'Косвенные показатели');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('35', 'ru', 'Финансовые инструменты');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('36', 'ru', 'Экспорт/Импорт');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('37', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('38', 'ru', 'Сбытовая статистика');
            INSERT INTO {{trcharttablegroup}} (`trParentId`, `langId`,`name`) VALUES ('39', 'ru', 'Экспорт/Импорт');
            
            ALTER TABLE {{charttablegroup}} 
            DROP COLUMN `name`;
            
            CREATE TABLE {{trcharttable}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(45) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('3', 'ru', 'Товарный объем и доля отраслей сельского хозяйства по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('4', 'ru', 'Региональный товарный объем и количество организаций, КФХ и ИП');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('5', 'ru', 'Региональный товарный объем и доля отраслей сельского хозяйства в с/х организациях');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('6', 'ru', 'Региональный товарный объем и доля отраслей сельского хозяйства в КФХ и ИП');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('7', 'ru', 'Основные сельскохозяйственные культуры');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('8', 'ru', 'Основная продукция животноводства');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('9', 'ru', 'Экспорт/Импорт сельскохозяйственной продукции данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('10', 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ СЕЛЬСКОГО ХОЗЯЙСТВА');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('11', 'ru', 'ОСНОВНЫЕ ПОКАЗАТЕЛИ СЕЛЬСКОГО ХОЗЯЙСТВА');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('15', 'ru', 'Основные показатели отрасли по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('16', 'ru', 'Основные показатели отрасли по округам/рентабельность');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('17', 'ru', 'Жилищные и ипотечные кредиты по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('18', 'ru', 'Основные показатели отрасли по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('19', 'ru', 'Основные показатели отрасли/рентабельность по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('20', 'ru', 'Жилищные и ипотечные кредиты по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('21', 'ru', 'Производство строительных материалов по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('22', 'ru', 'Основные показатели отрасли по округам/рентабельность');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('23', 'ru', 'Количественные показатели по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('24', 'ru', 'Основные показатели отрасли по регионам/рентабельность');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('25', 'ru', 'Количественные показатели по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('26', 'ru', 'Производство основных пищевых продуктов по округам/растениеводство');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('27', 'ru', 'Производство основных пищевых продуктов по округам/животноводство');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('28', 'ru', 'Производство напитков по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('29', 'ru', 'Емкость рынка алкогольной продукции');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('32', 'ru', 'Основные показатели потребления продуктов питания по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('33', 'ru', 'Потребительская корзина по округам/овощи и бакалея');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('34', 'ru', 'Потребительская корзина по округам/продукция животноводства');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('35', 'ru', 'Основные показатели потребления продуктов питания по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('36', 'ru', 'Потребительская корзина по регионам/овощи и бакалея');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('37', 'ru', 'Потребительская корзина по регионам/продукция животноводства');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('42', 'ru', 'Емкость рынка алкогольной продукции');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('45', 'ru', 'Производство основных пищевых продуктов/ растениеводство/по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('46', 'ru', 'Производство основных пищевых продуктов/ животноводство/по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('47', 'ru', 'Производство напитков по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('48', 'ru', 'Основные сельскохозяйственные культуры по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('49', 'ru', 'Основные продукты животноводства по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('50', 'ru', 'Экспорт/импорт продовольственных товаров по регионам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('51', 'ru', 'Движение основных продуктов  питания / растениеводство');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('52', 'ru', 'Движение основных продуктов  питания / растениеводство (продолжение)');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('53', 'ru', 'Движение основных продуктов питания / животноводство');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('54', 'ru', 'Движение основных продуктов питания / животноводство (продолжение)');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('55', 'ru', NULL);
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('57', 'ru', 'Основные показатели отрасли/добыча леса по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('58', 'ru', 'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ ПО РЕГИОНАМ');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('59', 'ru', 'Товарный объем тяжелой промышленности');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('60', 'ru', 'Рентабельность тяжелой промышленности по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('61', 'ru', 'Объем геологоразведывательных работ по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('62', 'ru', 'Рентабельность тяжелой промышленности по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('63', 'ru', 'Объем геологоразведывательных работ по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('64', 'ru', 'Экспорт/импорт металлов и стальных труб по регионам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('65', 'ru', 'Основные показатели отрасли по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('66', 'ru', 'Объем ресурсов по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('67', 'ru', 'Добыча нефти и газа по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('68', 'ru', 'Основные показатели отрасли по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('69', 'ru', 'Объем ресурсов по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('70', 'ru', 'Экспорт/импорт горюче-смазочных материалов по регионам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('71', 'ru', 'Добыча леса по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('72', 'ru', 'Экспорт/Импорт древесины и целлюлозы по округам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('73', 'ru', 'Добыча леса по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('74', 'ru', 'Пиломатериалы');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('75', 'ru', 'Экспорт/Импорт древесины и целлюлозы по регионам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('76', 'ru', 'Показатели производства');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('81', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('82', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('83', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('84', 'ru', 'Добыча нефти и газа по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('85', 'ru', 'Экспорт/импорт горюче-смазочных материалов по округам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('86', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('87', 'ru', 'Производство строительных материалов по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('88', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('89', 'ru', 'Производство основных пищевых продуктов/ растениеводство/по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('90', 'ru', 'Производство основных пищевых продуктов/ животноводство/по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('91', 'ru', 'Производство напитков по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('92', 'ru', 'Экспорт/импорт продовольственных товаров по округам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('93', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('94', 'ru', 'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ ПО ОКРУГАМ');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('95', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('96', 'ru', 'Экспорт/импорт продовольственных товаров по регионам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('97', 'ru', 'Экспорт/импорт продовольственных товаров по округам данные росстат');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('98', 'ru', 'Таможенная справка');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('99', 'ru', 'Производство основных пищевых продуктов по регионам/растениеводство');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('100', 'ru', 'Производство основных пищевых продуктов по регионам/животноводство');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('101', 'ru', 'Добыча леса по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('102', 'ru', 'Добыча леса по регионам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('103', 'ru', 'Косвенные показатели емкости рынка по округам');
            INSERT INTO {{trcharttable}} (`trParentId`,`langId`,`name`) VALUES ('104', 'ru', 'Косвенные показатели емкости рынка по регионам');

            ALTER TABLE {{charttable}} 
            DROP COLUMN `name`;
            
            ALTER TABLE {{trcharttable}} 
            ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trcharttablegroup}} 
            ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);

            ALTER TABLE {{trcharttable}} 
            ADD CONSTRAINT `fk_trcharttable_charttable`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{charttable}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            ALTER TABLE {{trcharttablegroup}} 
            ADD CONSTRAINT `fk_trcharttablegroup_charttablegroup`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{charttablegroup}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            CREATE TABLE {{trchartcolumn}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(45) NULL DEFAULT NULL,
              `header` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            ALTER TABLE {{trchartcolumn}} 
            ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);

            ALTER TABLE {{trchartcolumn}} 
            ADD CONSTRAINT `fk_trchartcolumn_chartcolumn`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{chartcolumn}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('19', 'ru', NULL);
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('20', 'ru', 'Хозяйства всех категорий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('21', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('22', 'ru', 'Число С/Х организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('23', 'ru', NULL);
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('24', 'ru', 'Продукция сельского хозяйства');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('25', 'ru', 'Растение- водство');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('26', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('27', 'ru', 'Животно- водство');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('28', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('29', 'ru', 'Продукция сельского хозяйства');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('30', 'ru', 'Растение- водство');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('31', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('32', 'ru', 'Животно- водство');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('33', 'ru', 'Зерно');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('34', 'ru', 'Сахарная свекла');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('35', 'ru', 'Подсол- нечник');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('36', 'ru', 'Картофель');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('37', 'ru', 'Овощи');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('38', 'ru', 'Фрукты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('39', 'ru', 'Скот и птица');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('40', 'ru', 'Молоко');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('41', 'ru', 'Яйца');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('42', 'ru', 'Шерсть');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('43', 'ru', 'Мед');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('44', 'ru', 'Площадь С/Х угодий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('45', 'ru', 'Посевные площади');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('46', 'ru', 'С/Х угодья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('47', 'ru', 'Минеральные удобрения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('48', 'ru', 'Органические удобрения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('49', 'ru', 'Рентабельность растениеводства');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('50', 'ru', 'Крупный рогатый скот');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('51', 'ru', 'Свиньи');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('52', 'ru', 'Овцы и козы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('53', 'ru', 'Расход кормов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('54', 'ru', 'Рентабель- ность');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('55', 'ru', NULL);
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('56', 'ru', 'Экспорт');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('57', 'ru', NULL);
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('58', 'ru', 'Импорт');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('59', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('60', 'ru', 'Производство цемента');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('61', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('62', 'ru', 'Производство железобетонных конструкций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('63', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('64', 'ru', 'Строительный объем зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('65', 'ru', 'Число зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('66', 'ru', 'Построенных населением');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('67', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('68', 'ru', 'Построенных организациями');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('69', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('70', 'ru', 'Людей, занятых в строительстве');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('71', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('72', 'ru', 'Строительных организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('73', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('74', 'ru', 'Жилищные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('75', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('76', 'ru', 'Ипотечные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('77', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('78', 'ru', 'Строительный объем зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('79', 'ru', 'Число зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('80', 'ru', 'Построенных населением');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('81', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('82', 'ru', 'Построенных организациями');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('83', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('84', 'ru', 'Людей, занятых в строительстве');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('85', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('86', 'ru', 'Строительных организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('87', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('88', 'ru', 'Жилищные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('89', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('90', 'ru', 'Ипотечные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('91', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('92', 'ru', 'Производство кирпича');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('93', 'ru', 'Инвестиции');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('94', 'ru', 'Оборот электроэнергии');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('95', 'ru', 'Рентабельность');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('96', 'ru', 'Мощность электростанций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('97', 'ru', 'Производство электроэнергии');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('98', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('99', 'ru', 'Число организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('102', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('103', 'ru', 'Численность работников');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('104', 'ru', 'Инвестиции');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('105', 'ru', 'Оборот электроэнергии');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('106', 'ru', 'Рентабельность');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('107', 'ru', 'Мощность электростанций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('108', 'ru', 'Производство электроэнергии');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('109', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('110', 'ru', 'Число организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('113', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('114', 'ru', 'Численность работников');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('115', 'ru', 'Производство муки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('116', 'ru', 'Производство крупы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('117', 'ru', 'Производство хлеба');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('118', 'ru', 'Проиводство сахара');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('119', 'ru', 'Производство масла');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('120', 'ru', 'Производство мяса убойных животных');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('121', 'ru', 'Производства мяса убойных птицы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('122', 'ru', 'Производство колбасных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('123', 'ru', 'Производство рыбы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('124', 'ru', 'Производство молока');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('125', 'ru', 'Производство масла сливочного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('126', 'ru', 'Производство водки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('127', 'ru', 'Производство коньяка');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('128', 'ru', 'Производство вина');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('129', 'ru', 'Производство игристых и газированных вин ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('130', 'ru', 'Производство пива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('131', 'ru', 'Производство воды минеральной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('132', 'ru', 'Продажа водки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('134', 'ru', 'Продажа коньяка, коньячных напитков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('138', 'ru', 'Продажа вина в натуральном выражении');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('140', 'ru', 'Продажа вина игристого и шампанских напитков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('142', 'ru', 'Продажа пива, кроме пивных коктейлей и солодовых напитков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('146', 'ru', 'Оборот розничной торговли');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('147', 'ru', 'Потребительские расходы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('148', 'ru', 'Пищ. продукты, в розничной торговле %');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('149', 'ru', 'Численность населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('150', 'ru', 'Потребление картофеля в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('151', 'ru', 'Потребление овощей и бахчевых в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('152', 'ru', 'Потребление сахара в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('153', 'ru', 'Потребление растительного масла на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('154', 'ru', 'Потребление хлебных продуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('155', 'ru', 'Потребление мяса и мясопродуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('156', 'ru', 'Потребление молока и молочных продуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('157', 'ru', 'Потребление яиц и яйцепродуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('158', 'ru', 'Оборот розничной торговли');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('159', 'ru', 'Потребительские расходы в среднем на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('160', 'ru', 'Пищ. продукты, в розничной торговле %');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('161', 'ru', 'Численность населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('162', 'ru', 'Потребление картофеля в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('163', 'ru', 'Потребление овощей и бахчевых в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('164', 'ru', 'Потребление сахара в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('165', 'ru', 'Потребление растительного масла в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('166', 'ru', 'Потребление хлебных продуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('167', 'ru', 'Потребление мяса и мясопродуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('168', 'ru', 'Потребление молока и молочных продуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('169', 'ru', 'Потребление яиц и яйцепродуктов в расчете на душу населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('188', 'ru', 'Продажа водки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('190', 'ru', 'Продажа коньяка, коньячных напитков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('194', 'ru', 'Продажа вина в натуральном выражении');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('196', 'ru', 'Продажа вина игристого и шампанских напитков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('198', 'ru', 'Продажа пива, кроме пивных коктейлей и солодовых напитков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('202', 'ru', 'Производство муки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('203', 'ru', 'Производство крупы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('204', 'ru', 'Производство хлеба');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('205', 'ru', 'Производство сахара');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('206', 'ru', 'Производство масла растительного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('207', 'ru', 'Производство мяса и субпродуктов пищевых убойных животных');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('208', 'ru', 'Производство мяса и субпродуктов пищевых домашней птицы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('209', 'ru', 'Производство колбасных изделий в натуральном выражении');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('210', 'ru', 'Производство рыбы и рыбных продуктов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('211', 'ru', 'Производство молока');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('212', 'ru', 'Производство масла сливочного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('213', 'ru', 'Производство водки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('214', 'ru', 'Производство коньяка');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('215', 'ru', 'Производство вина');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('216', 'ru', 'Производство игристых и газированных вин');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('217', 'ru', 'Производство пива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('218', 'ru', 'Производство воды минеральной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('219', 'ru', 'Валовой сбор зерновых и зернобобовых культур');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('220', 'ru', 'Валовой сбор сахарной свеклы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('221', 'ru', 'Валовой сбор подсолнечника');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('222', 'ru', 'Валовой сбор картофеля');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('223', 'ru', 'Валовой сбор овощей');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('224', 'ru', 'Валовой сбор плодово-ягодных');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('225', 'ru', 'Производство скота и птицы на убой');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('226', 'ru', 'Производство молока');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('227', 'ru', 'Производство яиц');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('228', 'ru', 'Производство шерсти');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('229', 'ru', 'Производство товарного меда');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('230', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('231', 'ru', 'Экспорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('232', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('233', 'ru', 'Импорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('234', 'ru', 'Вывоз (продажа) мяса и птицы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('235', 'ru', 'Ввоз (покупка) мяся и птицы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('236', 'ru', 'Вывоз (продажа) мясных консервов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('237', 'ru', 'Ввоз (покупка) мясных консервов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('238', 'ru', 'Вывоз (продажа) колбасных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('239', 'ru', 'Ввоз (покупка) колбасных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('240', 'ru', 'Вывоз (продажа) сыра');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('241', 'ru', 'Ввоз (покупка) сыра');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('242', 'ru', 'Вывоз (продажа) масла животного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('243', 'ru', 'Ввоз (покупка) масла животного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('244', 'ru', 'Вывоз (продажа) масла растительного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('245', 'ru', 'Ввоз (покупка) масла растительного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('246', 'ru', 'Вывоз (продажа) сахара');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('247', 'ru', 'Ввоз (покупка) сахара');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('248', 'ru', 'Вывоз (продажа) муки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('249', 'ru', 'Ввоз (покупка) муки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('250', 'ru', 'Вывоз (продажа) крупы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('251', 'ru', 'Ввоз (покупка) крупы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('252', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('253', 'ru', 'Общая площадь введенных зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('254', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('255', 'ru', 'Инвестиции в основной капитал');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('256', 'ru', 'Численность населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('257', 'ru', 'Численность населения с доходом выше прожиточного минимума');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('263', 'ru', 'Общая площадь введенных зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('264', 'ru', 'Инвестиции в основной капитал');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('265', 'ru', 'Оборот розничной торговли');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('266', 'ru', 'Численность населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('267', 'ru', 'Численность населения с доходом выше прожиточного минимума');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('268', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('269', 'ru', 'Кредиты (всего)');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('270', 'ru', 'Жилищные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('271', 'ru', 'Ипотечные жилищные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('272', 'ru', 'Потребительские кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('273', 'ru', 'Производство стали');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('274', 'ru', 'Производство черных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('275', 'ru', 'Производство стальных труб');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('276', 'ru', 'Производство металлорежущих станков');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('277', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('278', 'ru', 'Добыча металлов, минералов, строиматериалов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('282', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('283', 'ru', 'Геологоразвед. работы (металлы)');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('284', 'ru', 'Геологоразвед. работы черных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('285', 'ru', 'Геологоразвед. работы благородных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('286', 'ru', 'Геологоразвед. работы цветных и редких металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('287', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('288', 'ru', 'Добыча металлов, минералов, строиматериалов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('289', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('290', 'ru', 'Рентабельность обрабатывающих производств');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('293', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('294', 'ru', 'Геологоразвед. работы (металлы)');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('295', 'ru', 'Геологоразвед. работы черных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('296', 'ru', 'Геологоразвед. работы благородных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('297', 'ru', 'Геологоразвед. работы цветных и редких металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('298', 'ru', 'Вывоз (продажа) черных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('299', 'ru', 'Ввоз (покупка) черных металлов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('300', 'ru', 'Вывоз (продажа) стальных труб');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('301', 'ru', 'Ввоз (покупка) труб стальных');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('302', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('303', 'ru', 'Число организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('304', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('305', 'ru', 'Добыча топливно-энергетических полезных ископаемых');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('306', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('307', 'ru', 'Рентабельность организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('308', 'ru', 'Фонд нефтяных скважин');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('309', 'ru', 'Глубокое разведочное бурение на нефть и газ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('310', 'ru', 'Объем геологоразведочных нефти, газа и конденсата');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('312', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('313', 'ru', 'Добыча нефти');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('314', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('315', 'ru', 'Добыча газа');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('316', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('317', 'ru', 'Число организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('318', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('319', 'ru', 'Добыча топливно-энергетических полезных ископаемых');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('320', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('321', 'ru', 'Рентабельность организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('322', 'ru', 'Фонд нефтяных скважин');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('323', 'ru', 'Глубокое разведочное бурение на нефть и газ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('324', 'ru', 'Объем геологоразведочных нефти, газа и конденсата');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('326', 'ru', 'Вывоз (продажа) автомобильных бензинов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('327', 'ru', 'Ввоз (покупка) автомобильных бензинов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('328', 'ru', 'Вывоз (продажа) дизельного топлива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('329', 'ru', 'Ввоз (покупка) дизельного топлива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('330', 'ru', 'Вывоз (продажа) мазута');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('331', 'ru', 'Ввоз (покупка) мазута');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('332', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('333', 'ru', 'Производство древесины необработанной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('334', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('335', 'ru', 'Число предприятий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('336', 'ru', 'Лесистость территории');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('337', 'ru', 'Численность работников');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('338', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('339', 'ru', 'Экспорт древесины и целлюлозно-бумажных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('340', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('341', 'ru', 'Импорт древесины и целлюлозно-бумажных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('342', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('343', 'ru', 'Производство древесины необработанной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('344', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('345', 'ru', 'Число предприятий и организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('346', 'ru', 'Лесистость территории');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('347', 'ru', 'Численность работников ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('348', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('349', 'ru', 'Вывоз (продажа) пиломатериалов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('350', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('351', 'ru', 'Ввоз (покупка) пиломатериалов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('352', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('353', 'ru', 'Экспорт древесины и целлюлозно-бумажных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('354', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('355', 'ru', 'Импорт древесины и целлюлозно-бумажных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('356', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('357', 'ru', 'Производство фанеры');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('358', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('359', 'ru', 'Производство лесоматериалов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('360', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('361', 'ru', 'Производство целлюлозы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('362', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('363', 'ru', 'Рентабельность обрабатывающих производств');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('364', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('365', 'ru', 'Хозяйства всех категорий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('366', 'ru', 'Растение-водство');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('367', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('368', 'ru', 'Животно-водство');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('381', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('382', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('383', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('384', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('385', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('386', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('387', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('388', 'ru', 'Добыча нефти');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('389', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('390', 'ru', 'Добыча газа');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('391', 'ru', 'Вывоз (продажа) автомобильных бензинов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('392', 'ru', 'Ввоз (покупка) автомобильных бензинов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('393', 'ru', 'Вывоз (продажа) дизельного топлива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('394', 'ru', 'Ввоз (покупка) дизельного топлива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('395', 'ru', 'Вывоз (продажа) мазута');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('396', 'ru', 'Ввоз (покупка) мазута');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('397', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('398', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('399', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('400', 'ru', 'Производство цемента');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('401', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('402', 'ru', 'Производство железобетонных конструкций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('403', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('404', 'ru', 'Производство кирпича');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('405', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('406', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('407', 'ru', 'Производство муки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('408', 'ru', 'Производство крупы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('409', 'ru', 'Производство хлеба');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('410', 'ru', 'Производство сахара');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('411', 'ru', 'Производство масла растительного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('412', 'ru', 'Производство мяса и субпродуктов пищевых убойных животных');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('413', 'ru', 'Производство мяса и субпродуктов пищевых домашней птицы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('414', 'ru', 'Производство колбасных изделий в натуральном выражении');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('415', 'ru', 'Производство рыбы и рыбных продуктов');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('416', 'ru', 'Производство молока');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('417', 'ru', 'Производство масла сливочного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('418', 'ru', 'Производство водки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('419', 'ru', 'Производство коньяка');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('420', 'ru', 'Производство вина');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('421', 'ru', 'Производство игристых и газированных вин');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('422', 'ru', 'Производство пива');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('423', 'ru', 'Производство воды минеральной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('424', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('425', 'ru', 'Экспорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('426', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('427', 'ru', 'Импорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('428', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('429', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('430', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('431', 'ru', 'Кредиты (всего)');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('432', 'ru', 'Жилищные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('433', 'ru', 'Ипотечные жилищные кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('434', 'ru', 'Потребительские кредиты');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('435', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('436', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('437', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('438', 'ru', 'Экспорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('439', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('440', 'ru', 'Импорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('441', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('442', 'ru', 'Экспорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('443', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('444', 'ru', 'Импорт продовольственных товаров и сельскохозяйственного сырья');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('445', 'ru', 'Экспорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('446', 'ru', 'Импорт РФ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('447', 'ru', 'Производство муки');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('448', 'ru', 'Производство крупы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('449', 'ru', 'Производство хлеба');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('450', 'ru', 'Проиводство сахара');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('451', 'ru', 'Производство масла');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('452', 'ru', 'Производство мяса убойных животных');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('453', 'ru', 'Производства мяса убойных птицы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('454', 'ru', 'Производство колбасных изделий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('455', 'ru', 'Производство рыбы');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('456', 'ru', 'Производство молока');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('457', 'ru', 'Производство масла сливочного');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('458', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('459', 'ru', 'Производство древесины необработанной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('460', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('461', 'ru', 'Число предприятий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('462', 'ru', 'Лесистость территории');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('463', 'ru', 'Численность работников');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('464', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('465', 'ru', 'Производство древесины необработанной');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('466', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('467', 'ru', 'Число предприятий и организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('468', 'ru', 'Лесистость территории');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('469', 'ru', 'Численность работников ');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('470', 'ru', 'Число С/Х организаций');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('471', 'ru', '');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('472', 'ru', 'Общая площадь введенных зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('473', 'ru', 'Численность населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('474', 'ru', 'Численность населения с доходом выше прожиточного минимума');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('475', 'ru', 'Общая площадь введенных зданий');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('476', 'ru', 'Численность населения');
            INSERT INTO {{trchartcolumn}} (`trParentId`, `langId`, `header`) VALUES ('477', 'ru', 'Численность населения с доходом выше прожиточного минимума');
		
            ALTER TABLE {{chartcolumn}} 
            DROP COLUMN `header`;
            
            CREATE TABLE {{trgobjects}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(45) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
              ALTER TABLE {{trgobjects}} 
            ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);
            
            ALTER TABLE {{trgobjects}} 
            ADD CONSTRAINT `fk_trgobjects_gobjects`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{gobjects}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('370', 'ru', 'Центральный федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('283', 'ru', 'Алтайский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('371', 'ru', 'Северо-Западный федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('372', 'ru', 'Южный федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('284', 'ru', 'Краснодарский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('373', 'ru', 'Северо-Кавказский федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('285', 'ru', 'Красноярский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('374', 'ru', 'Приволжский федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('286', 'ru', 'Приморский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('375', 'ru', 'Уральский федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('376', 'ru', 'Сибирский федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('287', 'ru', 'Ставропольский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('377', 'ru', 'Дальневосточный федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('288', 'ru', 'Хабаровский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('378', 'ru', 'Крымский федеральный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('379', 'ru', 'Российская Федерация');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('289', 'ru', 'Амурская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('290', 'ru', 'Архангельская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('291', 'ru', 'Архангельская область (включая Ненецкий АО)');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('292', 'ru', 'Ненецкий автономный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('293', 'ru', 'Астраханская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('294', 'ru', 'Белгородская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('295', 'ru', 'Брянская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('296', 'ru', 'Владимирская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('297', 'ru', 'Волгоградская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('298', 'ru', 'Вологодская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('299', 'ru', 'Воронежская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('300', 'ru', 'Нижегородская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('301', 'ru', 'Ивановская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('302', 'ru', 'Иркутская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('303', 'ru', 'Республика Ингушетия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('304', 'ru', 'Калининградская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('305', 'ru', 'Тверская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('306', 'ru', 'Калужская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('307', 'ru', 'Камчатский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('308', 'ru', 'Кемеровская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('309', 'ru', 'Кировская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('310', 'ru', 'Костромская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('311', 'ru', 'Республика Крым');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('312', 'ru', 'Самарская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('313', 'ru', 'Курганская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('314', 'ru', 'Курская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('315', 'ru', 'Санкт-Петербург');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('316', 'ru', 'Ленинградская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('317', 'ru', 'Липецкая область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('318', 'ru', 'Магаданская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('319', 'ru', 'Москва');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('320', 'ru', 'Московская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('321', 'ru', 'Мурманская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('322', 'ru', 'Новгородская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('323', 'ru', 'Новосибирская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('324', 'ru', 'Омская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('325', 'ru', 'Оренбургская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('326', 'ru', 'Орловская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('327', 'ru', 'Пензенская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('328', 'ru', 'Пермский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('329', 'ru', 'Псковская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('330', 'ru', 'Ростовская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('331', 'ru', 'Рязанская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('332', 'ru', 'Саратовская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('333', 'ru', 'Сахалинская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('334', 'ru', 'Свердловская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('335', 'ru', 'Смоленская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('336', 'ru', 'Город федерального значения Севастополь');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('337', 'ru', 'Тамбовская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('338', 'ru', 'Томская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('339', 'ru', 'Тульская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('340', 'ru', 'Тюменская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('341', 'ru', 'Тюменская область (включая Ханты-Мансийский АО и Ямало-Ненецкий АО)');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('342', 'ru', 'Ханты-Мансийский автономный округ – Югра');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('343', 'ru', 'Ямало-Ненецкий автономный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('344', 'ru', 'Ульяновская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('345', 'ru', 'Челябинская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('346', 'ru', 'Забайкальский край');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('347', 'ru', 'Чукотский автономный округ');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('348', 'ru', 'Ярославская область');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('349', 'ru', 'Республика Адыгея (Адыгея)');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('350', 'ru', 'Республика Башкортостан');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('351', 'ru', 'Республика Бурятия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('352', 'ru', 'Республика Дагестан');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('353', 'ru', 'Кабардино-Балкарская Республика');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('354', 'ru', 'Республика Алтай');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('355', 'ru', 'Республика Калмыкия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('356', 'ru', 'Республика Карелия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('357', 'ru', 'Республика Коми');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('358', 'ru', 'Республика Марий Эл');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('359', 'ru', 'Республика Мордовия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('360', 'ru', 'Республика Северная Осетия - Алания');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('361', 'ru', 'Карачаево-Черкесская Республика');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('362', 'ru', 'Республика Татарстан');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('363', 'ru', 'Республика Тыва');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('364', 'ru', 'Удмуртская Республика');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('365', 'ru', 'Республика Хакасия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('366', 'ru', 'Чеченская Республика');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('367', 'ru', 'Чувашская Республика - Чувашия');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('368', 'ru', 'Республика Саха (Якутия)');
            INSERT INTO {{trgobjects}} (`trParentId`,`langId`,`name`) VALUES ('369', 'ru', 'Еврейская автономная область');

            ALTER TABLE {{gobjects}} 
            DROP COLUMN `name`,
            DROP INDEX `index2` ,
            ADD UNIQUE INDEX `index2` (`gId` ASC);

            CREATE TABLE {{trgstatgroup}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(45) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            ALTER TABLE {{trgstatgroup}} 
            ADD UNIQUE INDEX `trParentIdLangId` (`langId` ASC, `trParentId` ASC);

            ALTER TABLE {{trgstatgroup}} 
            ADD INDEX `fk_trgstatgroup_gstatgroup_idx` (`trParentId` ASC);
            ALTER TABLE {{trgstatgroup}} 
            ADD CONSTRAINT `fk_trgstatgroup_gstatgroup`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{gstatgroup}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('1', 'ru', 'Машины сельскохозяйственные');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('2', 'ru', 'Машины или механизмы для уборки или обмолота');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('3', 'ru', 'Установки и аппараты доильные');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('4', 'ru', 'Оборудование для виноделия');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('5', 'ru', 'Машины для очистки, сортировки или калибровки семян');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('6', 'ru', 'Древесина топливная');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('7', 'ru', 'Лесоматериалы, необработанные');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('8', 'ru', 'Пеллеты – гранулы древесные');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('9', 'ru', 'Производство чугуна');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('10', 'ru', 'Отходы и лом черных металлов');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('11', 'ru', 'Товарооборот черных металлов');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('12', 'ru', 'Товарооборот коррозионностойкой стали');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('13', 'ru', 'Станки металлорежущие');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('14', 'ru', 'Товарооборот легированных сталей');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('16', 'ru', 'Нефть');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('17', 'ru', 'Газы');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('18', 'ru', 'Цемент');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('19', 'ru', 'Изделия из гипса');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('20', 'ru', 'Колбасы и аналогичные продукты из мяса');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('21', 'ru', 'Готовые или консервированные продукты из мяса');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('22', 'ru', 'Готовая или консервированная рыба');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('23', 'ru', 'Сахар тростниковый или свекловичный');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('24', 'ru', 'Какао-бобы, целые или дробленые');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('25', 'ru', 'Шоколад готовый');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('26', 'ru', 'Макаронные изделия');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('27', 'ru', 'Хлеб, мучные кондитерские изделия, пирожные');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('28', 'ru', 'Овощи, фрукты, орехи');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('29', 'ru', 'Соки фруктовые');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('30', 'ru', 'Мороженое');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('31', 'ru', 'Воды, включая минеральные и газированные, не содержащие сахар');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('32', 'ru', 'Воды, включая минеральные и газированные, содержащие сахар');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('33', 'ru', 'Пиво солодовое');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('34', 'ru', 'Вина виноградные натуральные');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('35', 'ru', 'Табачное сырье, сигареты');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('36', 'ru', 'Сигары');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('37', 'ru', 'Свинина');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('38', 'ru', 'Баранина');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('39', 'ru', 'Мясо и пищевые субпродукты домашней птицы');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('40', 'ru', 'Кондитерские изделия из сахара');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('44', 'ru', 'Листы для облицовки');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('45', 'ru', 'Пиломатериалы');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('46', 'ru', 'ДСП и аналоги');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('47', 'ru', 'ДВП и аналоги');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('48', 'ru', 'Фанера');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('49', 'ru', 'Мебель (вся корпусная и мягкая)');
            INSERT INTO {{trgstatgroup}} (`trParentId`, `langId`, `name`) VALUES ('50', 'ru', 'Мебель медицинская');
            
            ALTER TABLE {{gstatgroup}} 
            DROP COLUMN `name`;
            
            CREATE TABLE {{trgstatgroupconstituents}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(45) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            ALTER TABLE {{trgstatgroupconstituents}} 
            ADD UNIQUE INDEX `trParentIdLangId` (`trParentId` ASC, `langId` ASC);

            ALTER TABLE {{trgstatgroupconstituents}} 
            ADD CONSTRAINT `fk_trgstatgroupconstituents_gstatgroupconstituents`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{gstatgroupconstituents}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('1', 'ru', 'Плуги 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('2', 'ru', 'Бороны 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('3', 'ru', 'Рыхлители 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('4', 'ru', 'Культиваторы 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('5', 'ru', 'Полольники 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('6', 'ru', 'Мотыги 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('7', 'ru', 'Сеялки 8432');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('8', 'ru', 'Косилки для газонов 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('9', 'ru', 'Косилки для парков 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('10', 'ru', 'Косилки для спортплощадок 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('11', 'ru', 'Прессы 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('12', 'ru', 'Пресс-подборщики 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('13', 'ru', 'Машины для уборки клубней и корнеплодов 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('14', 'ru', 'Комбайны зерноуборочные 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('15', 'ru', 'Комбайны виноградоуборочные 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('16', 'ru', 'Машины для очистки, сортировки, калибровки яиц 8433');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('17', 'ru', 'Установки доильные 8434');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('18', 'ru', 'Оборудование для обработки и переработки молока 8434');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('19', 'ru', 'Прессы 8435');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('20', 'ru', 'Дробилки 8435');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('21', 'ru', 'Машины для очистки, сортировки, калибровки семян, зерна или сухих бобовых культур 8437');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('22', 'ru', 'Древесина топливная в виде бревен, поленьев, ветвей, вязанок, хвороста 4401');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('23', 'ru', 'Древесина топливная в виде цепок или стружки 4401');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('24', 'ru', 'Опилки 4401');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('25', 'ru', 'Лесоматериалы необработанные, с удаленной или неудаленной корой 4403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('26', 'ru', 'Лесоматериалы обработанные краской, травителями, преозотом или другими консервантами 4403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('27', 'ru', 'Отходды и лом литейного чугуна 7204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('28', 'ru', 'Отходды и лом легированной стали 7204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('29', 'ru', 'Отходды и лом коррозионностойкой стали 7204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('30', 'ru', 'Отходды и лом черных металлов 7204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('31', 'ru', 'Токарная тружка, обрезки, обломки, отходы фрезерного производства, опилки, отходы штамповки 7204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('32', 'ru', 'Железо и нелегированная сталь в слитках 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('33', 'ru', 'Полуфабрикаты из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('34', 'ru', 'Прокат плоский из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('35', 'ru', 'Прутки горячекатаные в свободно смотанных бухтах из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('36', 'ru', 'Уголки, фасонные и специальные профили из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('37', 'ru', 'Проволока из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('38', 'ru', 'Сталь коррозионностойкая в слитках или прочих первичных формах 7218,7219,7220,7221,7222,7223');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('39', 'ru', 'Прокат плоский из коррозионностойкой стали 7218,7219,7220,7221,7222,7223');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('40', 'ru', 'Прутки горячекатаные 7218,7219,7220,7221,7222,7223');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('41', 'ru', 'Уголки, фасонные и специальные профили из коррозионностойкой стали 7218,7219,7220,7221,7222,7223');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('42', 'ru', 'Проволока из коррозионностойкой стали 7218,7219,7220,7221,7222,7223');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('43', 'ru', 'Станки токарные металлорежущие 7224,7225,7226,7227,7228,7229');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('44', 'ru', 'Станки металлорежущие для сверления, растачивания, фрезерования, нарезания наружной или внутренней резьбы 7224,7225,7226,7227,7228,7229');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('45', 'ru', 'Сталь легированная в слитках или других первичных формах прочая 8458,8459');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('46', 'ru', 'Прокат плоский из прочих легированных сталей 8458,8459');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('47', 'ru', 'Прутки, уголки из прочих легированных сталей 8458,8459');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('48', 'ru', 'Проволока из прочих легированных сталей 8458,8459');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('49', 'ru', 'Нефть сырая 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('50', 'ru', 'Газовый конденсат 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('51', 'ru', 'Легкие дистилляты 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('52', 'ru', 'Бензины моторные 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('53', 'ru', 'Керосин 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('54', 'ru', 'Тяжелые дистилляты 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('55', 'ru', 'Газойли 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('56', 'ru', 'Дизельное топливо 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('57', 'ru', 'Мазуты 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('58', 'ru', 'Масла смазочные 2709,2710');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('59', 'ru', 'Пропан 2711');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('60', 'ru', 'Бутаны 2711');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('61', 'ru', 'Этилен, пропилен, бутилен и бутадиен 2711');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('62', 'ru', 'Портландцемент 2523');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('63', 'ru', 'Цемент глиноземистый 2523');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('64', 'ru', 'Цемент шлаковый 2523');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('65', 'ru', 'Цемент суперсульфатный 2523');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('66', 'ru', 'Изделия из гипса 6809');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('67', 'ru', 'Колбасы и аналогичные продукты из мяса, мясных субпродуктов или крови; готовые пищевые продукты, изготовленные на их основе 1601');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('68', 'ru', 'Готовые или консервированные продукты из мяса, мясных субпродуктов или крови прочие 1602');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('69', 'ru', 'Готовая или консервированная рыба; икра осетровых и ее заменители, изготовленные из икринок рыбы 1604');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('70', 'ru', 'Сахар тростниковый или свекловичный и химически чистая сахароза, в твердом состоянии 1701');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('71', 'ru', 'Какао-бобы, целые или дробленые, сырые или жареные 1801');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('72', 'ru', 'Шоколад и прочие готовые пищевые продукты, содержащие какао 1806');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('73', 'ru', 'Макароны 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('74', 'ru', 'Спагетти 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('75', 'ru', 'Лапша 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('76', 'ru', 'Рожки 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('77', 'ru', 'Клецки 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('78', 'ru', 'Равиоли 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('79', 'ru', 'Каннеллони 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('80', 'ru', 'Кускус 1902');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('81', 'ru', 'Хлеб 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('82', 'ru', 'Мучные кондитерские изделия 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('83', 'ru', 'Пирожные 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('84', 'ru', 'Печенье 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('85', 'ru', 'Вафельные пластины 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('86', 'ru', 'Пустые капсулы 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('87', 'ru', 'Вафельные облатки 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('88', 'ru', 'Рисовая бумага 1905');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('89', 'ru', 'Овощи, фрукты, орехи и другие съедобные части растений, приготовленные или консервированные с добавлением уксуса или уксусной кислоты 2001');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('90', 'ru', 'Соки фруктовые (включая виноградное сусло) и соки овощные, несброженные и не содержащие добавок спирта, с добавлением или без добавления сахара или других подслащивающих веществ 2009');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('91', 'ru', 'Мороженое и прочие виды пищевого льда, не содержащие или содержащие какао 2105');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('92', 'ru', 'Воды, включая природные или искусственные минеральные, газированные, без добавления сахара или других подслащивающих или вкусо-ароматических веществ; лед и снег 2201');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('93', 'ru', 'Воды, включая минеральные и газированные, содержащие добавки сахара или других подслащивающих или вкусо-ароматических веществ 2202');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('94', 'ru', 'Пиво солодовое 2203');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('95', 'ru', 'Вина виноградные натуральные, включая крепленые 2204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('96', 'ru', 'Cусло виноградное 2204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('97', 'ru', 'Табачное сырье 2401');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('98', 'ru', 'Табачные отходы 2401');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('99', 'ru', 'Сигары 2402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('100', 'ru', 'Сигариллы 2402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('101', 'ru', 'Сигареты из табака или его заменителей 2402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('102', 'ru', 'Свинина 0203');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('103', 'ru', 'Баранина 0204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('104', 'ru', 'Козлятина 0204');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('105', 'ru', 'Мясо и пищевые субпродукты домашней птицы 0207');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('106', 'ru', 'Кондитерские изделия из сахара (включая белый шоколад), не содержащие какао 1704');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('107', 'ru', 'Листы для облицовки 4408');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('108', 'ru', 'Пиломатериалы 4409');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('109', 'ru', 'Плиты древесно-стружечные 4410');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('110', 'ru', 'Плиты с ориентированной стружкой 4410');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('111', 'ru', 'Вафельные плиты 4410');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('112', 'ru', 'Плиты древесно-волокнистые из древесины 4411');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('113', 'ru', 'Фанера клееная 4412');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('114', 'ru', 'Панели фанерованные 4412');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('115', 'ru', 'Мебель металлическая типа используемой в учреждениях столы письменные 9403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('116', 'ru', 'Шкафы, снабженные дверями, задвижками или откидными досками 9403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('117', 'ru', 'Шкафы для хранения документов, картотечные и прочие шкафы 9403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('118', 'ru', 'Кровати 9403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('119', 'ru', 'Мебель кухонной секционная 9403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('120', 'ru', 'Мебель спальная 9403');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('121', 'ru', 'Хирургическая 9402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('122', 'ru', 'Стоматологическая 9402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('123', 'ru', 'Ветеринарная 9402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('124', 'ru', 'Столы для осмотра 9402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('125', 'ru', 'Больничные койки с механическими приспособлениями 9402');
            INSERT INTO {{trgstatgroupconstituents}} (`trParentId`,`langId`,`name`) VALUES ('126', 'ru', 'Парикмахерские кресла 9402');

            ALTER TABLE {{gstatgroupconstituents}} 
            DROP COLUMN `name`;
            
            ALTER TABLE {{gstatgroupconstituents}} 
            CHANGE COLUMN `gstatgroupId` `gStatGroupId` INT(11) NULL DEFAULT NULL ;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}