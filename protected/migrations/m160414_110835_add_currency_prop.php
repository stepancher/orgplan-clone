<?php

class m160414_110835_add_currency_prop extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
				DELETE FROM {{attributeclassproperty}} WHERE `attributeClass`='514';
				DELETE FROM {{attributeclassproperty}} WHERE `attributeClass`='238';
				DELETE FROM {{attributeclassproperty}} WHERE `attributeClass`='241';
				DELETE FROM {{attributeclassproperty}} WHERE `attributeClass`='518';
				";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('514', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('238', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('241', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('518', 'USD', 'currency');

		";
	}
}