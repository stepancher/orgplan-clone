<?php

class m160822_080519_new_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '184');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('16', 'ru', 'Сроки подачи заявок', '/static/documents/fairs/184/important_dates.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('16', 'en', 'Deadline', '/static/documents/fairs/184/important_dates.pdf');
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '184');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('17', 'ru', 'Форма №14. Представитель участника', '/static/documents/fairs/184/f14.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('17', 'en', 'Form No14. Power of attorney', '/static/documents/fairs/184/f14.pdf');
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '184');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('18', 'ru', 'Письмо на ввоз-вывоз экспонатов и выставочного оборудования.', '/static/documents/fairs/184/f15.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('18', 'en', 'Request for delivery/removal of the exhibits and equipments.', '/static/documents/fairs/184/f15.pdf');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}