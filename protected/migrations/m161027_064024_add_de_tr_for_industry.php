<?php

class m161027_064024_add_de_tr_for_industry extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trindustry}} SET `name` = 'Haus- und Büromöbel, Inneneinrichtung, Haushaltsartikel' WHERE `trParentId` = '1' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Bau, Innenausbau und Baubedarf' WHERE `trParentId` = '2' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Maschinenbau, Metallbearbeitung, Maschinen, Industrieausrüstung' WHERE `trParentId` = '3' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Sicherheitstechnik, Feuerwehr und Brandschutz, Arbeitsschutz' WHERE `trParentId` = '4' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Holz und Holzverarbeitung' WHERE `trParentId` = '5' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Chemie. Polymermaterialien' WHERE `trParentId` = '6' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Metallurgie' WHERE `trParentId` = '7' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Energie' WHERE `trParentId` = '8' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Luft- und Raumfahrtsindustrie' WHERE `trParentId` = '10' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Tiere, Tierhaltung. Veterinärmedizin' WHERE `trParentId` = '12' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Nahrungsmittelindustrie: Nahrungsmittelmaschinen und -inhaltsstoffe' WHERE `trParentId` = '13' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Beauty, Kosmetik, Parfümerie' WHERE `trParentId` = '16' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Hotel- und Gastronomiegewerbe' WHERE `trParentId` = '17' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Informationstechnik' WHERE `trParentId` = '19' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Uhren und Schmuck' WHERE `trParentId` = '20' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Elektronik und Zubehör' WHERE `trParentId` = '21' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Automobile, Fahrrad- und Motorradtechnik' WHERE `trParentId` = '22' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Kunst und Kultur' WHERE `trParentId` = '23' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Pharmaindustrie' WHERE `trParentId` = '25' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Tourismus und Erholung' WHERE `trParentId` = '27' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Hightech. Wissenschaft. Innovationen' WHERE `trParentId` = '28' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Transportwesen, Lieferung, Lagertechnik, Logistik' WHERE `trParentId` = '29' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Dienstleistungen für Staat' WHERE `trParentId` = '30' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Haushaltsbedarf, Geschirr, Souvenirs' WHERE `trParentId` = '31' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Blumen. Floristik. Garten und Landschaftsarchitektur' WHERE `trParentId` = '32' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Erholung, Jagd und Fischerei' WHERE `trParentId` = '35' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Bildung' WHERE `trParentId` = '36' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Öl- und Gasförderung' WHERE `trParentId` = '37' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Naturressourcen. Bergbau' WHERE `trParentId` = '38' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Kinderwelt. Kinderbedarf' WHERE `trParentId` = '39' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Druckwesen und Drucktechnik' WHERE `trParentId` = '41' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Bekleidung, Schuhe, Leder- und Pelzwaren' WHERE `trParentId` = '45' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Reinigungs­dienstleistungen' WHERE `trParentId` = '46' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Festartikel (Hochzeitsbedarf, Neujahr, Weihnachten)' WHERE `trParentId` = '47' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Hebemaschinen' WHERE `trParentId` = '51' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Geistiges Eigentum und gewerblicher Rechtsschutz' WHERE `trParentId` = '53' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Boote, Yachten, Schiffbau' WHERE `trParentId` = '54' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Mittel- und Kleinunternehmen' WHERE `trParentId` = '56' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Immobilien - Miete und Verkauf' WHERE `trParentId` = '59' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Optik. Lasertechnik' WHERE `trParentId` = '60' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `name` = 'Textilien, Stoffe' WHERE `trParentId` = '61' AND `langId` = 'de';

            UPDATE {{trindustry}} SET `shortNameUrl` = 'Möbel' WHERE `trParentId` = '1' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Bau' WHERE `trParentId` = '2' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Maschinenbau' WHERE `trParentId` = '3' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Sicherheitstechnik' WHERE `trParentId` = '4' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Holzverarbeitung' WHERE `trParentId` = '5' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Chemie' WHERE `trParentId` = '6' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Metallurgie' WHERE `trParentId` = '7' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Energie' WHERE `trParentId` = '8' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Medizin' WHERE `trParentId` = '9' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Luft-und-Raumfahrtsindustrie' WHERE `trParentId` = '10' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Landwirtschaft' WHERE `trParentId` = '11' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Veterinärmedizin' WHERE `trParentId` = '12' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Nahrungsmittelindustrie' WHERE `trParentId` = '13' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Lebensmittel' WHERE `trParentId` = '14' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Parfümerie' WHERE `trParentId` = '16' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Hotel-und-Gastronomiegewerbe' WHERE `trParentId` = '17' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Elektrotechnik' WHERE `trParentId` = '18' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Informationstechnik' WHERE `trParentId` = '19' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Schmuck' WHERE `trParentId` = '20' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Elektronik' WHERE `trParentId` = '21' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Auto-Fahhrad-Motorrad' WHERE `trParentId` = '22' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Kultur' WHERE `trParentId` = '23' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Religion' WHERE `trParentId` = '24' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Pharmaindustrie' WHERE `trParentId` = '25' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Verpackung' WHERE `trParentId` = '26' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Tourismus' WHERE `trParentId` = '27' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Wissenschaft' WHERE `trParentId` = '28' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Logistik' WHERE `trParentId` = '29' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Dienstleistungen' WHERE `trParentId` = '30' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Haushaltsbedarf' WHERE `trParentId` = '31' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Gartenbau' WHERE `trParentId` = '32' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Jagd-und-Fischerei' WHERE `trParentId` = '35' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Bildung' WHERE `trParentId` = '36' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Öl-und-Gas' WHERE `trParentId` = '37' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Naturressourcen' WHERE `trParentId` = '38' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Kinderwelt' WHERE `trParentId` = '39' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Werbung' WHERE `trParentId` = '40' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Druckwesen' WHERE `trParentId` = '41' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Umweltschutz' WHERE `trParentId` = '43' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Bekleidung' WHERE `trParentId` = '45' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Reinigungs­dienstleistungen' WHERE `trParentId` = '46' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Festartikel' WHERE `trParentId` = '47' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Antiquitäten' WHERE `trParentId` = '49' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Messeindustrie' WHERE `trParentId` = '50' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Hebemaschinen' WHERE `trParentId` = '51' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'gewerblicher Rechtsschutz' WHERE `trParentId` = '53' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Schiffbau' WHERE `trParentId` = '54' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Verlagswesen' WHERE `trParentId` = '55' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Business' WHERE `trParentId` = '56' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Immobilien' WHERE `trParentId` = '59' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Lasertechnik' WHERE `trParentId` = '60' AND `langId` = 'de';
            UPDATE {{trindustry}} SET `shortNameUrl` = 'Textilien' WHERE `trParentId` = '61' AND `langId` = 'de';
        ";
    }

    public function downSql()
    {
        return TRUE;
    }
}