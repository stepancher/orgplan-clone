<?php

class m150909_130429_create_communicationelectricity extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{communicationelectricity}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE {{communicationelectricity}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`communicationId` int(11),
				`type` int(11),
				`number` int(11),
				`power` int(11),
				`totalPower` int(11)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;
		';
	}
}