<?php

class m160905_065106_update_discounts_on_10_proposal extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 17:2}' WHERE `id`='978';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 17:2}' WHERE `id`='979';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 17:2}' WHERE `id`='980';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 17:2}' WHERE `id`='957';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2125';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2138';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2139';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2140';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2141';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2142';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2143';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2144';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2145';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2146';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2149';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2246';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2247';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2248';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2249';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2286';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 21.09.16' WHERE `id`='2287';
            UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. <br/>\nСтоимость услуг увеличивается на 100% при заказе услуг после 21.09.16г.<br> \n<br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ' WHERE `id`='2152';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='130' WHERE `id`='1809';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='250' WHERE `id`='1810';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}