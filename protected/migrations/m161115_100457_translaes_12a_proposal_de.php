<?php

class m161115_100457_translaes_12a_proposal_de extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '21', 'de', 'Antrag Nr. 12a', 'VIP NAMENSSCHILDER');

            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2205', 'de', 'Senden Sie bitte das ausgefüllte Antragsformular  bis spätestens 20. August 2016 an die Messeleitung. Fax: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2206', 'de', '1. VIP Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2207', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2208', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2209', 'de', 'Mittelname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2210', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2211', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2212', 'de', 'Postfach');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2213', 'de', 'PLZ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2214', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2215', 'de', 'Region');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2216', 'de', 'Stadt');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2217', 'de', 'Straße');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2218', 'de', 'Hausnummer');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2219', 'de', 'Tel.');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2220', 'de', 'Fax');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2221', 'de', '2. VIP Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2222', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2223', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2224', 'de', 'Mittelname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2225', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2226', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2227', 'de', 'Postfach');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2228', 'de', 'PLZ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2229', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2230', 'de', 'Region');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2231', 'de', 'Stadt');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2232', 'de', 'Straße');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2233', 'de', 'Hausnummer');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2234', 'de', 'Tel.');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2235', 'de', 'Fax');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2236', 'de', '3. VIP Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2237', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2238', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2239', 'de', 'Mittelname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2240', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2241', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2242', 'de', 'Postfach');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2243', 'de', 'PLZ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2244', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2245', 'de', 'Region');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2246', 'de', 'Stadt');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2247', 'de', 'Straße');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2248', 'de', 'Hausnummer');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2249', 'de', 'Tel.');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2250', 'de', 'Fax');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2251', 'de', '4. VIP Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2252', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2253', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2254', 'de', 'Mittelname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2255', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2256', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2257', 'de', 'Postfach');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2258', 'de', 'PLZ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2259', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2260', 'de', 'Region');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2261', 'de', 'Stadt');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2262', 'de', 'Straße');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2263', 'de', 'Hausnummer');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2264', 'de', 'Tel.');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2265', 'de', 'Fax');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2266', 'de', '5. VIP Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2267', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2268', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2269', 'de', 'Mittelname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2270', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2271', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2272', 'de', 'Postfach');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2273', 'de', 'PLZ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2274', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2275', 'de', 'Region');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2276', 'de', 'Stadt');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2277', 'de', 'Straße');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2278', 'de', 'Hausnummer');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2279', 'de', 'Tel.');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2280', 'de', 'Fax');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2281', 'de', '6. VIP Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2282', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2283', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2284', 'de', 'Mittelname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2285', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2286', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2287', 'de', 'Postfach');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2288', 'de', 'PLZ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2289', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2290', 'de', 'Region');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2291', 'de', 'Stadt');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2292', 'de', 'Straße');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2293', 'de', 'Hausnummer');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2294', 'de', 'Tel.');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2295', 'de', 'Fax');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2296', 'de', 'Die Besitzer von VIP-Namensschildern können die Messe Metalloobrabotka und sämtliche Veranstaltungen der Messe besuchen. Die Anzahl der VIP-Namensschilder, die dem Aussteller zur Verfügung gestellt werden, hängt von der Größe des Messestandes ab und wird wie folgt bestimmt:<br> Messestand bis 100 m² - 1 St. <br> Messestand zwischen 100 м² und 500м² - 2 St. <br> \nMessestand ab 500м² - 3 St.', '');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2399', 'de', 'Aussteller');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2400', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2401', 'de', 'Name, Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2402', 'de', 'Unterschrift');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}