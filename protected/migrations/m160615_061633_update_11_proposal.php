<?php

class m160615_061633_update_11_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `parent`='2088', `priority`='30' WHERE `id`='2099';
			UPDATE {{attributeclass}} SET `parent`='2088', `priority`='40' WHERE `id`='2100';
			UPDATE {{attributeclass}} SET `parent`='2088', `priority`='50' WHERE `id`='2101';
			UPDATE {{attributeclass}} SET `parent`='2088', `priority`='60' WHERE `id`='2102';
			UPDATE {{attributeclass}} SET `parent`='2088', `priority`='70' WHERE `id`='2103';
			UPDATE {{attributeclass}} SET `parent`='2088', `priority`='80' WHERE `id`='2104';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2098';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2099';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2100';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2101';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2102';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2103';
			UPDATE {{attributeclass}} SET `collapse`='0' WHERE `id`='2104';
			UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='2098';
			
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES (NULL, '2100', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2101', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2104', 'unitTitle');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2100', 'ru', 'час');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2101', 'ru', 'человек');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2104', 'ru', 'человек');
			
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1956' WHERE `id`='2614';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1957' WHERE `id`='2615';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1958' WHERE `id`='2616';
			
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle1' WHERE `id`='1828';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle1' WHERE `id`='1829';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle1' WHERE `id`='1830';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle1' WHERE `id`='1846';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle1' WHERE `id`='1847';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1848';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1849';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1850';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1851';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1852';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1853';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1854';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1855';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1856';
			UPDATE {{attributeclassproperty}} SET `class`='unitTitle' WHERE `id`='1857';
			UPDATE {{trattributeclass}} SET `label`='2. Тема - актуальная, интересная для целевой группы, позволяющая применить получаемые опыт, знания, технологии на практике.' WHERE `id`='1591';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='2089';
		";
	}
}