<?php

class m160727_161854_replace_district_to_region_in_districts extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{trdistrict}} SET `name`='Baku-district' WHERE `id`='28';
			UPDATE {{trdistrict}} SET `name`='Yerevan-district' WHERE `id`='29';
			UPDATE {{trdistrict}} SET `name`='Minsk-district' WHERE `id`='30';
			UPDATE {{trdistrict}} SET `name`='Mogilev-district' WHERE `id`='31';
			UPDATE {{trdistrict}} SET `name`='Grodno-district' WHERE `id`='32';
			UPDATE {{trdistrict}} SET `name`='Astana-district' WHERE `id`='33';
			UPDATE {{trdistrict}} SET `name`='Almaty-district' WHERE `id`='34';
			UPDATE {{trdistrict}} SET `name`='Atyrau-district' WHERE `id`='35';
			UPDATE {{trdistrict}} SET `name`='Mangystau-district' WHERE `id`='36';
			UPDATE {{trdistrict}} SET `name`='Aktobe-district' WHERE `id`='37';
			UPDATE {{trdistrict}} SET `name`='Pavlodar-district' WHERE `id`='38';
			UPDATE {{trdistrict}} SET `name`='Bishkek-district' WHERE `id`='39';
			UPDATE {{trdistrict}} SET `name`='Kishinev-district' WHERE `id`='40';
			UPDATE {{trdistrict}} SET `name`='Karaganda-district' WHERE `id`='41';
			UPDATE {{trdistrict}} SET `name`='Tashkent-district' WHERE `id`='42';
			UPDATE {{trdistrict}} SET `name`='Fergana-district' WHERE `id`='43';
			UPDATE {{trdistrict}} SET `name`='Ashgabat-district' WHERE `id`='44';
			UPDATE {{trdistrict}} SET `name`='Balkan-district' WHERE `id`='45';
			UPDATE {{trdistrict}} SET `name`='Tbilisi-district' WHERE `id`='46';
			UPDATE {{trdistrict}} SET `name`='Ulaanbaatar-district' WHERE `id`='47';
			UPDATE {{trdistrict}} SET `name`='Kiev-district' WHERE `id`='48';

			UPDATE {{district}} SET `shortUrl`='Kiev-district' WHERE `id`='46';
			UPDATE {{district}} SET `shortUrl`='Baku-district' WHERE `id`='26';
			UPDATE {{district}} SET `shortUrl`='Yerevan-district' WHERE `id`='27';
			UPDATE {{district}} SET `shortUrl`='Minsk-district' WHERE `id`='28';
			UPDATE {{district}} SET `shortUrl`='Mogilev-district' WHERE `id`='29';
			UPDATE {{district}} SET `shortUrl`='Grodno-district' WHERE `id`='30';
			UPDATE {{district}} SET `shortUrl`='Astana-district' WHERE `id`='31';
			UPDATE {{district}} SET `shortUrl`='Almaty-district' WHERE `id`='32';
			UPDATE {{district}} SET `shortUrl`='Atyrau-district' WHERE `id`='33';
			UPDATE {{district}} SET `shortUrl`='Mangystau-district' WHERE `id`='34';
			UPDATE {{district}} SET `shortUrl`='Aktobe-district' WHERE `id`='35';
			UPDATE {{district}} SET `shortUrl`='Pavlodar-district' WHERE `id`='36';
			UPDATE {{district}} SET `shortUrl`='Bishkek-district' WHERE `id`='37';
			UPDATE {{district}} SET `shortUrl`='Kishinev-district' WHERE `id`='38';
			UPDATE {{district}} SET `shortUrl`='Karaganda-district' WHERE `id`='39';
			UPDATE {{district}} SET `shortUrl`='Tashkent-district' WHERE `id`='40';
			UPDATE {{district}} SET `shortUrl`='Fergana-district' WHERE `id`='41';
			UPDATE {{district}} SET `shortUrl`='Ashgabat-district' WHERE `id`='42';
			UPDATE {{district}} SET `shortUrl`='Balkan-district' WHERE `id`='43';
			UPDATE {{district}} SET `shortUrl`='Tbilisi-district' WHERE `id`='44';
			UPDATE {{district}} SET `shortUrl`='Ulaanbaatar-district' WHERE `id`='45';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}