<?php

class m160229_131355_insert_power_enums extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			delete from {{attributeenum}} where `attributeClass` = 139;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeenum}} (`attributeClass`, `value`, `fixed`) VALUES ('139', 'до 5 кВт включительно', '0');
			INSERT INTO {{attributeenum}} (`attributeClass`, `value`, `fixed`) VALUES ('139', 'до 10 кВт включительно', '0');
			INSERT INTO {{attributeenum}} (`attributeClass`, `value`, `fixed`) VALUES ('139', 'до 20 кВт включительно', '0');
			INSERT INTO {{attributeenum}} (`attributeClass`, `value`, `fixed`) VALUES ('139', 'до 40 кВт включительно', '0');
			INSERT INTO {{attributeenum}} (`attributeClass`, `value`, `fixed`) VALUES ('139', 'до 60 кВт включительно', '0');
	    ";
	}

}