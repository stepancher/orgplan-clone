<?php

class m170221_073838_add_exdb_associationhasassociation_to_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbassociationhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `associationId` INT(11) NULL DEFAULT NULL,
              `hasAssociationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}