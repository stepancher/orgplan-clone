<?php

class m160831_082800_new_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '11699');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('20', 'en', 'Unilateral contract', '/static/documents/fairs/11699/unilateral_contract.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('20', 'ru', 'Публичная офферта ООО \"ЭФ Интернэшнл\"', '/static/documents/fairs/11699/unilateral_contract.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('21', 'en', 'Organizers and Exponents manual', '/static/documents/fairs/11699/organizers_and_exponents_manual.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('21', 'ru', 'Руководство для Организаторов и Экспонентов ', '/static/documents/fairs/11699/organizers_and_exponents_manual.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('22', 'en', 'Agrorus Exhibitor  manual', '/static/documents/fairs/11699/agrorus_exhibitor_manual.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('22', 'ru', 'Руководство участника выставки \"Агрорусь\"', '/static/documents/fairs/11699/agrorus_exhibitor_manual.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('23', 'en', 'Agrorus Exhibitor guide', '/static/documents/fairs/11699/agrorus_exhibitor_guide.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('23', 'ru', 'Памятка участника выставки Агрорусь', '/static/documents/fairs/11699/agrorus_exhibitor_guide.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('24', 'en', 'Passage scheme', '/static/documents/fairs/11699/passage_scheme.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('24', 'ru', 'Схема проезда ', '/static/documents/fairs/11699/passage_scheme.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('25', 'en', 'The terms of participation in Agrorus', '/static/documents/fairs/11699/the_terms_of_participation_in_agrorus.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('25', 'ru', 'Краткие условия участия в выставке Агрорусь', '/static/documents/fairs/11699/the_terms_of_participation_in_agrorus.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('26', 'en', 'Deadline', '/static/documents/fairs/11699/deadline.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('26', 'ru', 'Важные даты', '/static/documents/fairs/11699/deadline.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('27', 'en', 'Letter of attorney', NULL, '/static/documents/fairs/11699/letter_of_attorney.docx');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('27', 'ru', 'Доверенность', '/static/documents/fairs/11699/letter_of_attorney.docx');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('28', 'en', 'Stand layout', '/static/documents/fairs/11699/unilateral_contract.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('28', 'ru', 'План стенда', '/static/documents/fairs/11699/unilateral_contract.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('29', 'en', 'User manual', '/static/documents/fairs/11699/unilateral_contract.pdf');
             INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('29', 'ru', 'Инструкция Экспонента', '/static/documents/fairs/11699/unilateral_contract.pdf');
             UPDATE {{documents}} SET `active`='0' WHERE `id`='28';
             UPDATE {{documents}} SET `active`='0' WHERE `id`='29';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}