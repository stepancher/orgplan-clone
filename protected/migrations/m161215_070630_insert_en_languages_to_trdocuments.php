<?php

class m161215_070630_insert_en_languages_to_trdocuments extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        $sql= '';

        /**
         * @var Fair $fair
         */
        foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){
                $dbTargetName = Yii::app()->dbMan->createShardName($fair->id);

                $sql = $sql."
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Booth visitor checklist', `description`='Booth visitor profile' WHERE `id`='99';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Stuff instruction checklist', `description`='Stuff instruction checklist' WHERE `id`='100';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Coffee break, buffet menu', `description`='Coffee break, buffet menu' WHERE `id`='101';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Booth kitchen checklist', `description`='Booth kitchen checklist' WHERE `id`='102';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibitor tips for the goal PR, BRANDING', `description`='Goal PR, BRANDING' WHERE `id`='103';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibitor tips for the goal MARKETING RESEARCH', `description`='Goal MARKETING RESEARCH' WHERE `id`='104';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibitor tips for the goal CUSTOMER RELATIONS', `description`='Goal CUSTOMER RELATIONS' WHERE `id`='105';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibitor tips for the goal SALES SUPPORT', `description`='Goal SALES SUPPORT' WHERE `id`='106';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibitor tips for the goal SALES', `description`='Goal SALES' WHERE `id`='107';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibit items checklist', `description`='Exhibit items checklist' WHERE `id`='108';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Follow-up checklist', `description`='Follow-up checklist' WHERE `id`='109';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Checklist for exhibit items demonstration', `description`='Checklist of the product demonstration at the booth' WHERE `id`='110';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Contest checklist', `description`='Contest checklist' WHERE `id`='111';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Conference checklist', `description`='Conference checklist' WHERE `id`='112';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Workshop checklist', `description`='Workshop checklist' WHERE `id`='113';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Menu checklist for booth stuff food', `description`='Menu checklist for booth stuff food' WHERE `id`='114';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Exhibition activities checklist', `description`='Exhibition activities checklist' WHERE `id`='115';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Press conference checklist', `description`='Press conference checklist' WHERE `id`='117';
                    UPDATE {$dbTargetName}.`tbl_trdocuments` SET `name`='Press release checklist', `description`='Press release checklist' WHERE `id`='118';

                ";
            }
        }

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}