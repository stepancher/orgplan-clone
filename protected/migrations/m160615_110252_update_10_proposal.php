<?php

class m160615_110252_update_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2186';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2185';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2184';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2183';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2180';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2179';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2147';
			
			/** proposal 6 **/
			
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='945';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='946';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='947';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='948';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='949';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='950';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='951';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='952';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='953';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='954';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='955';
			UPDATE {{attributeclassdependency}} SET `function`='loadDaysCount' WHERE `id`='956';
			UPDATE {{attributeclass}} SET `class`='hintWithView' WHERE `id`='2082';
			
			
			/** /proposal 6 **/
			
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2175', '2175', 'increaseByLeftDay', '{1000:1, 46:2}', '1');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2147', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2179', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2180', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2183', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2184', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2185', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2160', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2161', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2162', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2163', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2164', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2165', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2166', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2167', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2168', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2171', '2175', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2186', '2175', 'inherit', '0');
		";
	}
}