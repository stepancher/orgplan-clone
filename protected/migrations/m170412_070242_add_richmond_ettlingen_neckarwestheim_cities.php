<?php

class m170412_070242_add_richmond_ettlingen_neckarwestheim_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('126');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1096', 'ru', 'Ричмонд-апон-Темс', 'richmond-upon-thames');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1096', 'en', 'London Borough of Richmond upon Thames', 'richmond-upon-thames');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1096', 'de', 'London Borough of Richmond upon Thames', 'richmond-upon-thames');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1096', 'richmond-london');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1552', 'ru', 'Ричмонд, Лондон');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1552', 'en', 'Richmond, London');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1552', 'de', 'Richmond, London');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('158', 'ettlingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1553', 'ru', 'Эттлинген');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1553', 'en', 'Ettlingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1553', 'de', 'Ettlingen');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('163', 'neckarwestheim');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1554', 'ru', 'Неккарвестхайм');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1554', 'en', 'Neckarwestheim');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1554', 'de', 'Neckarwestheim');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}