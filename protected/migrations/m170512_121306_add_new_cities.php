<?php

class m170512_121306_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('293');

            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1107', 'ru', 'Вирсэло', 'weerselo');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1107', 'en', 'Weerselo', 'weerselo');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1107', 'de', 'Weerselo', 'weerselo');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1107', 'weerselo');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1592', 'ru', 'Вирсэло');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1592', 'en', 'Weerselo');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1592', 'de', 'Weerselo');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('158', 'neuenburg');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1593', 'ru', 'Нойенбюрг');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1593', 'en', 'Neuenbürg');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1593', 'de', 'Neuenbürg');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}