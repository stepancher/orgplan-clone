<?php

class m160512_063805_create_tr_tables_for_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			CREATE TABLE {{trproposalelementclass}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `label` VARCHAR(255) NULL,
			  `pluralLabel` VARCHAR(255) NULL,
		  	PRIMARY KEY (`id`));

			CREATE TABLE {{trattributeclass}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `label` TEXT NULL,
			  `unitTitle` VARCHAR(255) NULL,
			  `placeholder` VARCHAR(255) NULL,
			  `tooltip` TEXT NULL,
			  `tooltipCustom` TEXT NULL,
		  	PRIMARY KEY (`id`));

			CREATE TABLE {{trattributeclassproperty}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `value` TEXT NULL,
		  	PRIMARY KEY (`id`));

		  	CREATE TABLE {{trattributeenum}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `value` TEXT NULL,
		  	PRIMARY KEY (`id`));
	    ";
	}
}