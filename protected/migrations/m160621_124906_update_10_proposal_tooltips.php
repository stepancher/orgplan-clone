<?php

class m160621_124906_update_10_proposal_tooltips extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2171';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2184';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2185';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2186';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2187';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2188';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2189';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2190';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2191';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2192';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2195';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016' WHERE `id`='2250';
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `tooltip`) VALUES (NULL, '2184', 'en', '<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `tooltip`) VALUES (NULL, '2183', 'en', '<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `tooltip`) VALUES (NULL, '2185', 'en', '<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `tooltip`) VALUES (NULL, '2186', 'en', '<div class=\"hint-header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after August 18, 2016');
";
	}
}