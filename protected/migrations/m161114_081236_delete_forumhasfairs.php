<?php

class m161114_081236_delete_forumhasfairs extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `move_forum_stat_to_fairinfo`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fairId INT DEFAULT 0;
                DECLARE infoId INT DEFAULT 0;
                
                DECLARE copy CURSOR FOR SELECT f2.id, f1.infoId
                    FROM {{forumhasfairs}} fhf
                    LEFT JOIN {{fair}} f1 ON f1.id = fhf.forumId
                    LEFT JOIN {{fair}} f2 ON f2.id = fhf.fairId;
                    
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    read_loop: LOOP
                    
                        FETCH copy INTO fairId, infoId;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            UPDATE {{fair}} SET `infoId` = infoId WHERE `id` = fairId;
                        COMMIT;
                        
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL move_forum_stat_to_fairinfo();
            DROP PROCEDURE IF EXISTS `move_forum_stat_to_fairinfo`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}