<?php

class m160330_055829_alter_organizercontact extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return '
			ALTER TABLE {{organizerinfo}}
			CHANGE COLUMN `postPostcode` `postPostcode` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `postCountry` `postCountry` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `postRegion` `postRegion` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `postCity` `postCity` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `postStreet` `postStreet` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `postPhone` `postPhone` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `postSite` `postSite` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalPostcode` `legalPostcode` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalCountry` `legalCountry` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalRegion` `legalRegion` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalCity` `legalCity` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalStreet` `legalStreet` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesSettlementAccount` `requisitesSettlementAccount` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesBank` `requisitesBank` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesBic` `requisitesBic` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesItn` `requisitesItn` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesIec` `requisitesIec` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesCorrespondentAccount` `requisitesCorrespondentAccount` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `signRulesName` `signRulesName` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `signRulesPosition` `signRulesPosition` VARCHAR(255) NULL DEFAULT NULL ;
		';
	}

	private function downSql() {

		return '
			ALTER TABLE {{organizerinfo}}
			CHANGE COLUMN `postPostcode` `postPostcode` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postCountry` `postCountry` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postRegion` `postRegion` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postCity` `postCity` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postStreet` `postStreet` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postPhone` `postPhone` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postSite` `postSite` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalPostcode` `legalPostcode` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalCountry` `legalCountry` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalRegion` `legalRegion` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalCity` `legalCity` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalStreet` `legalStreet` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesSettlementAccount` `requisitesSettlementAccount` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesBank` `requisitesBank` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesBic` `requisitesBic` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesItn` `requisitesItn` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesIec` `requisitesIec` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `requisitesCorrespondentAccount` `requisitesCorrespondentAccount` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `signRulesName` `signRulesName` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `signRulesPosition` `signRulesPosition` VARCHAR(45) NULL DEFAULT NULL ;
		';
	}
}