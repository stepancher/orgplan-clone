<?php

class m170116_133832_copy_organizer_contacts_to_contactlist extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_organizer_contacts_to_contact_list`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0; 
                DECLARE org_phone VARCHAR(255) DEFAULT ''; 
                DECLARE org_email VARCHAR(255) DEFAULT ''; 
                DECLARE org_exhibitionManagement VARCHAR(255) DEFAULT ''; 
                DECLARE org_servicesBuilding VARCHAR(255) DEFAULT ''; 
                DECLARE org_advertisingServicesTheFair VARCHAR(255) DEFAULT ''; 
                DECLARE org_servicesLoadingAndUnloading VARCHAR(255) DEFAULT ''; 
                DECLARE org_foodServices VARCHAR(255) DEFAULT ''; 
                DECLARE copy CURSOR FOR SELECT id, 
                                               CASE WHEN (phone != '' AND phone IS NOT NULL) THEN phone ELSE 'empty' END phone, 
                                               CASE WHEN (email != '' AND email IS NOT NULL) THEN email ELSE 'empty' END email, 
                                               CASE WHEN (exhibitionManagement != '' AND exhibitionManagement IS NOT NULL) 
                                                    THEN exhibitionManagement ELSE 'empty' END exhibitionManagement, 
                                               CASE WHEN (servicesBuilding != '' AND servicesBuilding IS NOT NULL) 
                                                    THEN servicesBuilding ELSE 'empty' END servicesBuilding,
                                               CASE WHEN (advertisingServicesTheFair != '' AND advertisingServicesTheFair IS NOT NULL) 
                                                    THEN advertisingServicesTheFair ELSE 'empty' END advertisingServicesTheFair, 
                                               CASE WHEN (servicesLoadingAndUnloading != '' AND servicesLoadingAndUnloading IS NOT NULL) 
                                                    THEN servicesLoadingAndUnloading ELSE 'empty' END servicesLoadingAndUnloading, 
                                               CASE WHEN (foodServices != '' AND foodServices IS NOT NULL) 
                                                    THEN foodServices ELSE 'empty' END foodServices  
                                           FROM {{organizer}};
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    
                    read_loop: LOOP
                    
                    FETCH copy INTO org_id,
                                    org_phone,
                                    org_email,
                                    org_exhibitionManagement,
                                    org_servicesBuilding,
                                    org_advertisingServicesTheFair,
                                    org_servicesLoadingAndUnloading,
                                    org_foodServices;
                    
                    IF done THEN LEAVE read_loop; END IF;
                    
                    IF org_phone != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_phone, 1);
                        COMMIT;
                    END IF;
                    
                    IF org_email != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_email, 2);
                        COMMIT;
                    END IF;
                    
                    IF org_exhibitionManagement != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_exhibitionManagement, 3);
                        COMMIT;
                    END IF;
                    
                    IF org_servicesBuilding != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_servicesBuilding, 4);
                        COMMIT;
                    END IF;
                    
                    IF org_advertisingServicesTheFair != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_advertisingServicesTheFair, 5);
                        COMMIT;
                    END IF;
                    
                    IF org_servicesLoadingAndUnloading != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_servicesLoadingAndUnloading, 6);
                        COMMIT;
                    END IF;
                    
                    IF org_foodServices != 'empty' THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`,`type`) VALUES (org_id, org_foodServices, 7);
                        COMMIT;
                    END IF;
                    
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL copy_organizer_contacts_to_contact_list();
            DROP PROCEDURE IF EXISTS `copy_organizer_contacts_to_contact_list`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}