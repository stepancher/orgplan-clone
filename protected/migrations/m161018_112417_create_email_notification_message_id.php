<?php

class m161018_112417_create_email_notification_message_id extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{sentmessage}} (
              `id` INT NOT NULL AUTO_INCREMENT,
              `viewLayout` VARCHAR(200) NULL,
              `from` VARCHAR(200) NULL,
              `subject` TEXT NULL,
              `content` TEXT NULL,
              `logo` TEXT NULL,
              `viewPath` VARCHAR(200) NULL,
              `view` VARCHAR(200) NULL,
              `mailViewPath` VARCHAR(200) NULL,
              `attachmentData` TEXT NULL,
              `attachmentFileName` TEXT NULL,
              `attachmentContentType` TEXT NULL,
              PRIMARY KEY (`id`));

            ALTER TABLE {{sentmessage}} 
            ADD COLUMN `raw` TEXT NULL AFTER `attachmentContentType`,
            ADD COLUMN `messageId` INT NULL AFTER `raw`;
            
            ALTER TABLE {{sentmessage}} 
            CHANGE COLUMN `messageId` `messageId` TEXT NULL DEFAULT NULL ;
            ALTER TABLE {{sentmessage}} 
            ADD COLUMN `to` VARCHAR(200) NULL AFTER `messageId`;
		";

    }

    public function downSql()
    {
        return TRUE;
    }
}