<?php

class m160330_060232_alter_organizerinfo extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return '
			ALTER TABLE {{organizercontact}}
			ADD COLUMN `administrator` TINYINT(1) NOT NULL DEFAULT 0 AFTER `position`;
		';
	}

	private function downSql() {

		return '
			ALTER TABLE {{organizercontact}} DROP COLUMN `administrator`;
		';
	}
}