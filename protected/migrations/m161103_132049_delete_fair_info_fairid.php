<?php

class m161103_132049_delete_fair_info_fairid extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{fairinfo}} 
            DROP FOREIGN KEY `fk_fair`;
            
            ALTER TABLE {{fairinfo}} 
            DROP COLUMN `fairId`,
            DROP INDEX `fair_uniq` ;

            ALTER TABLE {{fair}} 
            DROP COLUMN `statisticsDate`,
            DROP COLUMN `statistics`,
            DROP COLUMN `visitors`,
            DROP COLUMN `members`,
            DROP COLUMN `squareNet`,
            DROP COLUMN `squareGross`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}