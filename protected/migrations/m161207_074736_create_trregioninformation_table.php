<?php

class m161207_074736_create_trregioninformation_table extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }


    public function upSql()
    {
        return "
            DROP TABLE {{trregioninformation}};
            
            CREATE TABLE {{trregioninformation}} (
              `id` INT NOT NULL,
              `trParentId` INT NULL,
              `text` TEXT NULL,
              `descriptionSnippet` TEXT NULL,
              `langId` VARCHAR(45) NULL,
              PRIMARY KEY (`id`));
              
              ALTER TABLE {{trregioninformation}} 
                CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;


              INSERT INTO {{trregioninformation}} (trParentId, text, descriptionSnippet, langId)
              SELECT id, text, descriptionSnippet, 'ru'
              FROM {{regioninformation}};
              
              ALTER TABLE {{regioninformation}}
                DROP COLUMN `descriptionSnippet`,
                DROP COLUMN `text`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}