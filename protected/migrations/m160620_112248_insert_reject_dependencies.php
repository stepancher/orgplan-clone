<?php

class m160620_112248_insert_reject_dependencies extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getInsertInTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getInsertInTable(){
		return '

			INSERT INTO {{dependences}} (action, params, run)
			VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/reject/id/1/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run)
			VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/add/id/1");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/reject/id/6/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/add/id/6");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/reject/id/10/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/add/id/10");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/reject/id/29/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":1}", "calendar/gantt/add/id/29");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/reject/id/21/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/reject/id/23/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/reject/id/25/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/reject/id/20/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/add/id/20");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/reject/id/22/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/add/id/22");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/reject/id/24/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":4}", "calendar/gantt/add/id/24");


			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/reject/id/33/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/add/id/33");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/reject/id/31/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/add/id/31");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/reject/id/35/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/add/id/35");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/reject/id/37/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/add/id/37");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/reject/id/39/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/add/id/39");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/reject/id/41/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/add/id/41");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/reject/id/45/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/add/id/45");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/reject/id/43/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/add/id/43");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/reject/id/47/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/add/id/47");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/reject/id/49/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/add/id/49");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":3}", "calendar/gantt/reject/id/62/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":3}", "calendar/gantt/add/id/62");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/reject/id/130/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/add/id/130");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/reject/id/128/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/add/id/128");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/reject/id/126/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/add/id/126");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/reject/id/124/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/add/id/124");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/reject/id/122/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/add/id/122");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":19}", "calendar/gantt/reject/id/136/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":19}", "calendar/gantt/add/id/136");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":19}", "calendar/gantt/reject/id/134/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":19}", "calendar/gantt/add/id/134");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":20}", "calendar/gantt/reject/id/132/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":20}", "calendar/gantt/add/id/132");

			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/reject/id/32/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/reject/id/34/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":5}", "calendar/gantt/reject/id/36/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/reject/id/38/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/reject/id/40/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":2}", "calendar/gantt/reject/id/42/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/reject/id/44/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/reject/id/46/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":7}", "calendar/gantt/reject/id/48/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/reject/id/121/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/reject/id/123/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":10}", "calendar/gantt/reject/id/125/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/reject/id/127/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/reject/id/129/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":18}", "calendar/gantt/reject/id/131/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":20}", "calendar/gantt/reject/id/133/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) 		VALUES ("proposal/proposal/reject", "{\"ecId\":20}", "calendar/gantt/reject/id/135/userId/{:userId}");
		';
	}
}

/**
 *
 *
153 №13
145 №8
147 №8
149 №9а
151 №9а
141 №7
143 №7
139 №7
137 №12
138 №12a



2 	Подписать договор (скидка 20%)	Нажмите "Завершить", когда подпишите и отправите договор
3	Оплатить участие (аванс 10%)	Нажмите "Завершить", когда будет произведена оплата
4	Оплатить участие (аванс 40%)	Нажмите "Завершить", когда будет произведена оплата
5	Оплатить участие (аванс 50%)	Нажмите "Завершить", когда будет произведена оплата
7	Подписать договор (скидка 15%)	Нажмите "Завершить", когда подпишите и отправите договор
8	Оплатить участие (аванс 50%)	Нажмите "Завершить", когда будет произведена оплата
9	Оплатить участие (аванс 50%)	Нажмите "Завершить", когда будет произведена оплата
11	Подписать договор (скидка 10%)	Нажмите "Завершить", когда подпишите и отправите договор
16	Предоплата в размере 100%	Нажмите "Завершить", когда будет произведена оплата

21	Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД	Нажмите "Завершить", когда будет произведена оплата
23	Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)	Нажмите "Завершить", когда будет произведена оплата
25	Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)	Нажмите "Завершить", когда будет произведена оплата

26	Оплатить участие (аванс 50%)	Нажмите "Завершить", когда будет произведена оплата
27	Оплатить участие (аванс 50%)	"Нажмите ""Завершить"", когда будет произведена оплата
28	Оплатить участие (аванс 50%)	Нажмите "Завершить", когда будет произведена оплата

63	Оплатить участие (аванс 50%)	Нажмите "Завершить", когда будет произведена оплата
64	Предоплата в размере 100%	Нажмите "Завершить", когда будет произведена оплата

140	№7 "ОХРАНА, УБОРКА, ПЕРСОНАЛ"	Нажмите "Завершить", когда будет произведена оплата
142	№7 "ОХРАНА, УБОРКА, ПЕРСОНАЛ"	Нажмите "Завершить", когда будет произведена оплата
144	№7 "ОХРАНА, УБОРКА, ПЕРСОНАЛ" (с наценкой 100%)	Нажмите "Завершить", когда будет произведена оплата
146	№8 "ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ. ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ"	Нажмите "Завершить", когда будет произведена оплата
148	№8 "ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ. ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ"	Нажмите "Завершить", когда будет произведена оплата
150	№9а "ПРОПУСКА НА АВТОТРАНСПОРТ"	Нажмите "Завершить", когда будет произведена оплата
152	№9а "ПРОПУСКА НА АВТОТРАНСПОРТ" (с наценкой 100%)	Нажмите "Завершить", когда будет произведена оплата
 *
**/