<?php

class m161128_133423_add_auth_block_fileds_to_user extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{user}} 
            ADD COLUMN `lastAuthFailDatetime` TIMESTAMP NULL DEFAULT NULL AFTER `googleAPI`,
            ADD COLUMN `authFailCount` INT(11) NULL DEFAULT 0 AFTER `lastAuthFailDatetime`,
            ADD COLUMN `authFailTotalCount` INT(11) NULL DEFAULT 0 AFTER `authFailCount`;
            
            UPDATE {{user}} SET `authFailCount` = 0;
            UPDATE {{user}} SET `authFailTotalCount` = 0;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}