<?php

class m160623_090837_update_12a_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2', 'int', '0', '0', '0', '2221', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_surname', 'string', '0', '0', '2221', '2221', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_name', 'string', '0', '0', '2221', '2221', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_lastname', 'string', '0', '0', '2221', '2221', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_position', 'string', '0', '0', '2221', '2221', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_company', 'string', '0', '0', '2221', '2221', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_post', 'string', '0', '0', '2221', '2221', '70', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_index', 'string', '0', '0', '2221', '2221', '80', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_country', 'int', '0', '0', '2221', '2221', '90', 'dropDownCountry', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_region', 'int', '0', '0', '2221', '2221', '100', 'dropDownRegion', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_city', 'int', '0', '0', '2221', '2221', '110', 'dropDownCity', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_street', 'string', '0', '0', '2221', '2221', '120', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_house', 'string', '0', '0', '2221', '2221', '130', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_phone', 'string', '0', '0', '2221', '2221', '140', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge2_fax', 'string', '0', '0', '2221', '2221', '150', 'text', '0');
			
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3', 'int', '0', '0', '2236', '2236', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_surname', 'string', '0', '0', '2236', '2236', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_name', 'string', '0', '0', '2236', '2236', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_lastname', 'string', '0', '0', '2236', '2236', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_position', 'string', '0', '0', '2236', '2236', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_company', 'string', '0', '0', '2236', '2236', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_post', 'string', '0', '0', '2236', '2236', '70', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_index', 'string', '0', '0', '2236', '2236', '80', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_country', 'int', '0', '0', '2236', '2236', '90', 'dropDownCountry', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_region', 'int', '0', '0', '2236', '2236', '100', 'dropDownRegion', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_city', 'int', '0', '0', '2236', '2236', '110', 'dropDownCity', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_street', 'string', '0', '0', '2236', '2236', '120', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_house', 'string', '0', '0', '2236', '2236', '130', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_phone', 'string', '0', '0', '2236', '2236', '140', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge3_fax', 'string', '0', '0', '2236', '2236', '150', 'text', '0');
			
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4', 'int', '0', '0', '2251', '2251', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_surname', 'string', '0', '0', '2251', '2251', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_name', 'string', '0', '0', '2251', '2251', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_lastname', 'string', '0', '0', '2251', '2251', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_position', 'string', '0', '0', '2251', '2251', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_company', 'string', '0', '0', '2251', '2251', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_post', 'string', '0', '0', '2251', '2251', '70', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_index', 'string', '0', '0', '2251', '2251', '80', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_country', 'int', '0', '0', '2251', '2251', '90', 'dropDownCountry', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_region', 'int', '0', '0', '2251', '2251', '100', 'dropDownRegion', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_city', 'int', '0', '0', '2251', '2251', '110', 'dropDownCity', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_street', 'string', '0', '0', '2251', '2251', '120', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_house', 'string', '0', '0', '2251', '2251', '130', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_phone', 'string', '0', '0', '2251', '2251', '140', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge4_fax', 'string', '0', '0', '2251', '2251', '150', 'text', '0');
			
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5', 'int', '0', '0', '2266', '2266', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_surname', 'string', '0', '0', '2266', '2266', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_name', 'string', '0', '0', '2266', '2266', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_lastname', 'string', '0', '0', '2266', '2266', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_position', 'string', '0', '0', '2266', '2266', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_company', 'string', '0', '0', '2266', '2266', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_post', 'string', '0', '0', '2266', '2266', '70', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_index', 'string', '0', '0', '2266', '2266', '80', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_country', 'int', '0', '0', '2266', '2266', '90', 'dropDownCountry', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_region', 'int', '0', '0', '2266', '2266', '100', 'dropDownRegion', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_city', 'int', '0', '0', '2266', '2266', '110', 'dropDownCity', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_street', 'string', '0', '0', '2266', '2266', '120', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_house', 'string', '0', '0', '2266', '2266', '130', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_phone', 'string', '0', '0', '2266', '2266', '140', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge5_fax', 'string', '0', '0', '2266', '2266', '150', 'text', '0');
			
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6', 'int', '0', '0', '2281', '2281', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_surname', 'string', '0', '0', '2281', '2281', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_name', 'string', '0', '0', '2281', '2281', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_lastname', 'string', '0', '0', '2281', '2281', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_position', 'string', '0', '0', '2281', '2281', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_company', 'string', '0', '0', '2281', '2281', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_post', 'string', '0', '0', '2281', '2281', '70', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_index', 'string', '0', '0', '2281', '2281', '80', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_country', 'int', '0', '0', '2281', '2281', '90', 'dropDownCountry', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_region', 'int', '0', '0', '2281', '2281', '100', 'dropDownRegion', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_city', 'int', '0', '0', '2281', '2281', '110', 'dropDownCity', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_street', 'string', '0', '0', '2281', '2281', '120', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_house', 'string', '0', '0', '2281', '2281', '130', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_phone', 'string', '0', '0', '2281', '2281', '140', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge6_fax', 'string', '0', '0', '2281', '2281', '150', 'text', '0');
			
			UPDATE {{attributeclass}} SET `parent`='0' WHERE `id`='2281';
			UPDATE {{attributeclass}} SET `parent`='0' WHERE `id`='2266';
			UPDATE {{attributeclass}} SET `parent`='0' WHERE `id`='2251';
			UPDATE {{attributeclass}} SET `parent`='0' WHERE `id`='2236';
";
	}
}