<?php

class m160930_173810_gradoteka_add_data extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{gobjectshasgstattypes}} WHERE `statId` = '5784b485bd4c56591c8b6399';
            
            DELETE FROM {{gvalue}} WHERE `setId` IN (
                '74131','74132','74133','74134','74135','74136','74137','74138','74139','74140','74141','74142','74143','74144',
                '74145','74146','74147','74148','74149','74150','74151','74152','74153','74154','74155','74156','74157','74158',
                '74159','74160','74161','74162','74163','74164','74165','74166','74167','74168','74169','74170','74171','74172',
                '74173','74174','74175','74176','74177','74178','74179','74180','74181','74182','74183','74184','74185','74186',
                '74187','74188','74189','74190','74191','74192','74193','74194','74195','74196','74197','74198','74199','74200',
                '74201','74202'
            );
            
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83637', '283', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83638', '284', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83639', '285', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83640', '286', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83641', '287', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83642', '288', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83643', '289', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83644', '290', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83645', '291', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83646', '292', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83647', '293', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83648', '294', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83649', '295', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83650', '296', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83651', '297', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83652', '298', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83653', '299', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83654', '300', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83655', '301', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83656', '302', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83657', '303', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83658', '304', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83659', '305', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83660', '306', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83661', '307', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83662', '308', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83663', '309', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83664', '310', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83665', '311', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83666', '312', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83667', '313', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83668', '314', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83669', '316', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83670', '317', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83671', '318', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83672', '319', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83673', '320', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83674', '321', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83675', '322', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83676', '323', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83677', '324', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83678', '325', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83679', '326', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83680', '327', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83681', '328', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83682', '329', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83683', '330', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83684', '331', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83685', '332', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83686', '333', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83687', '334', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83688', '335', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83689', '336', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83690', '337', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83691', '338', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83692', '339', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83693', '340', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83694', '341', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83695', '342', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83696', '343', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83697', '344', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83698', '345', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83699', '346', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83700', '347', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83701', '348', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83702', '349', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83703', '350', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83704', '351', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83705', '352', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83706', '353', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83707', '354', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83708', '355', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83709', '356', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83710', '357', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83711', '358', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83712', '359', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83713', '360', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83714', '361', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83715', '362', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83716', '363', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83717', '364', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83718', '365', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83719', '366', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83720', '367', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83721', '368', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83722', '369', '5784b485bd4c56591c8b6399', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83723', '370', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83724', '371', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83725', '372', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83726', '373', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83727', '374', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83728', '375', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83729', '376', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83730', '377', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83731', '378', '5784b485bd4c56591c8b6399', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83732', '379', '5784b485bd4c56591c8b6399', 'country');
            
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751052', '83637', '2016-09-30 20:41:46', '57.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751053', '83637', '2016-09-30 20:41:46', '45.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751054', '83637', '2016-09-30 20:41:46', '54.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751055', '83638', '2016-09-30 20:41:46', '75.3', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751056', '83638', '2016-09-30 20:41:46', '73.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751057', '83638', '2016-09-30 20:41:46', '73.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751058', '83639', '2016-09-30 20:41:46', '49.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751059', '83639', '2016-09-30 20:41:46', '47.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751060', '83639', '2016-09-30 20:41:46', '47', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751061', '83640', '2016-09-30 20:41:46', '51.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751062', '83640', '2016-09-30 20:41:46', '53', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751063', '83640', '2016-09-30 20:41:46', '50.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751064', '83641', '2016-09-30 20:41:46', '75.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751065', '83641', '2016-09-30 20:41:46', '75', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751066', '83641', '2016-09-30 20:41:46', '73.6', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751067', '83642', '2016-09-30 20:41:46', '15', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751068', '83642', '2016-09-30 20:41:46', '20.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751069', '83642', '2016-09-30 20:41:46', '20', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751070', '83643', '2016-09-30 20:41:46', '56.8', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751071', '83643', '2016-09-30 20:41:46', '74.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751072', '83643', '2016-09-30 20:41:46', '76.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751073', '83644', '2016-09-30 20:41:46', '17.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751074', '83644', '2016-09-30 20:41:46', '17.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751075', '83644', '2016-09-30 20:41:46', '27.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751076', '83645', '2016-09-30 20:41:46', '16.3', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751077', '83645', '2016-09-30 20:41:46', '16.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751078', '83645', '2016-09-30 20:41:46', '25.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751079', '83646', '2016-09-30 20:41:46', '8.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751080', '83646', '2016-09-30 20:41:46', '7.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751081', '83646', '2016-09-30 20:41:46', '9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751082', '83647', '2016-09-30 20:41:46', '45.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751083', '83647', '2016-09-30 20:41:46', '45.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751084', '83647', '2016-09-30 20:41:46', '38', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751085', '83648', '2016-09-30 20:41:46', '23.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751086', '83648', '2016-09-30 20:41:46', '20.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751087', '83648', '2016-09-30 20:41:47', '22.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751088', '83649', '2016-09-30 20:41:47', '29.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751089', '83649', '2016-09-30 20:41:47', '27', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751090', '83649', '2016-09-30 20:41:47', '23', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751091', '83650', '2016-09-30 20:41:47', '21', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751092', '83650', '2016-09-30 20:41:47', '23.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751093', '83650', '2016-09-30 20:41:47', '27.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751094', '83651', '2016-09-30 20:41:47', '75.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751095', '83651', '2016-09-30 20:41:47', '74.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751096', '83651', '2016-09-30 20:41:47', '75.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751097', '83652', '2016-09-30 20:41:47', '20', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751098', '83652', '2016-09-30 20:41:47', '21', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751099', '83652', '2016-09-30 20:41:47', '21.7', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751100', '83653', '2016-09-30 20:41:47', '61.8', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751101', '83653', '2016-09-30 20:41:47', '58.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751102', '83653', '2016-09-30 20:41:47', '60', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751103', '83654', '2016-09-30 20:41:47', '35.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751104', '83654', '2016-09-30 20:41:47', '36.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751105', '83654', '2016-09-30 20:41:47', '35.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751106', '83655', '2016-09-30 20:41:47', '27', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751107', '83655', '2016-09-30 20:41:47', '34.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751108', '83655', '2016-09-30 20:41:47', '24.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751109', '83656', '2016-09-30 20:41:47', '29.2', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751110', '83656', '2016-09-30 20:41:47', '26.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751111', '83656', '2016-09-30 20:41:47', '17.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751112', '83657', '2016-09-30 20:41:47', '97.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751113', '83657', '2016-09-30 20:41:47', '93.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751114', '83657', '2016-09-30 20:41:47', '85.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751115', '83658', '2016-09-30 20:41:47', '37.8', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751116', '83658', '2016-09-30 20:41:47', '40.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751117', '83658', '2016-09-30 20:41:47', '41.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751118', '83659', '2016-09-30 20:41:47', '19.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751119', '83659', '2016-09-30 20:41:47', '18.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751120', '83659', '2016-09-30 20:41:47', '17.7', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751121', '83660', '2016-09-30 20:41:47', '28.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751122', '83660', '2016-09-30 20:41:47', '26', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751123', '83660', '2016-09-30 20:41:47', '29.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751124', '83661', '2016-09-30 20:41:47', '19.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751125', '83661', '2016-09-30 20:41:47', '26.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751126', '83661', '2016-09-30 20:41:47', '19.7', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751127', '83662', '2016-09-30 20:41:47', '35.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751128', '83662', '2016-09-30 20:41:47', '33.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751129', '83662', '2016-09-30 20:41:47', '32.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751130', '83663', '2016-09-30 20:41:47', '31.8', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751131', '83663', '2016-09-30 20:41:47', '30.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751132', '83663', '2016-09-30 20:41:47', '27.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751133', '83664', '2016-09-30 20:41:47', '15.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751134', '83664', '2016-09-30 20:41:47', '17.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751135', '83664', '2016-09-30 20:41:47', '17', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751136', '83665', '2016-09-30 20:41:47', '57.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751137', '83665', '2016-09-30 20:41:47', '65.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751138', '83666', '2016-09-30 20:41:47', '69.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751139', '83666', '2016-09-30 20:41:47', '69.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751140', '83666', '2016-09-30 20:41:47', '68.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751141', '83667', '2016-09-30 20:41:47', '75', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751142', '83667', '2016-09-30 20:41:47', '68.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751143', '83667', '2016-09-30 20:41:47', '74.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751144', '83668', '2016-09-30 20:41:47', '61', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751145', '83668', '2016-09-30 20:41:47', '52.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751146', '83668', '2016-09-30 20:41:47', '48.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751147', '83669', '2016-09-30 20:41:47', '16.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751148', '83669', '2016-09-30 20:41:47', '15.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751149', '83669', '2016-09-30 20:41:47', '14.7', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751150', '83670', '2016-09-30 20:41:47', '59.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751151', '83670', '2016-09-30 20:41:47', '55.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751152', '83670', '2016-09-30 20:41:47', '56.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751153', '83671', '2016-09-30 20:41:47', '15.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751154', '83671', '2016-09-30 20:41:47', '8.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751155', '83671', '2016-09-30 20:41:47', '21.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751156', '83672', '2016-09-30 20:41:47', '69.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751157', '83672', '2016-09-30 20:41:47', '66.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751158', '83672', '2016-09-30 20:41:47', '59.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751159', '83673', '2016-09-30 20:41:47', '24.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751160', '83673', '2016-09-30 20:41:47', '30.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751161', '83673', '2016-09-30 20:41:47', '39.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751162', '83674', '2016-09-30 20:41:47', '6.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751163', '83674', '2016-09-30 20:41:47', '8.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751164', '83674', '2016-09-30 20:41:47', '12', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751165', '83675', '2016-09-30 20:41:47', '13.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751166', '83675', '2016-09-30 20:41:47', '16.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751167', '83675', '2016-09-30 20:41:47', '18.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751168', '83676', '2016-09-30 20:41:48', '38', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751169', '83676', '2016-09-30 20:41:48', '32.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751170', '83676', '2016-09-30 20:41:48', '36.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751171', '83677', '2016-09-30 20:41:48', '46.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751172', '83677', '2016-09-30 20:41:48', '41.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751173', '83677', '2016-09-30 20:41:48', '44.7', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751174', '83678', '2016-09-30 20:41:48', '58.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751175', '83678', '2016-09-30 20:41:48', '49.1', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751176', '83678', '2016-09-30 20:41:48', '51', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751177', '83679', '2016-09-30 20:41:48', '72.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751178', '83679', '2016-09-30 20:41:48', '71', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751179', '83679', '2016-09-30 20:41:48', '72.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751180', '83680', '2016-09-30 20:41:48', '51.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751181', '83680', '2016-09-30 20:41:48', '48.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751182', '83680', '2016-09-30 20:41:48', '52.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751183', '83681', '2016-09-30 20:41:48', '20.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751184', '83681', '2016-09-30 20:41:48', '22.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751185', '83681', '2016-09-30 20:41:48', '18', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751186', '83682', '2016-09-30 20:41:48', '17.2', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751187', '83682', '2016-09-30 20:41:48', '12.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751188', '83682', '2016-09-30 20:41:48', '10.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751189', '83683', '2016-09-30 20:41:48', '78.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751190', '83683', '2016-09-30 20:41:48', '78.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751191', '83683', '2016-09-30 20:41:48', '78.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751192', '83684', '2016-09-30 20:41:48', '48.2', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751193', '83684', '2016-09-30 20:41:48', '47.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751194', '83684', '2016-09-30 20:41:48', '50.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751195', '83685', '2016-09-30 20:41:48', '76.3', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751196', '83685', '2016-09-30 20:41:48', '72.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751197', '83685', '2016-09-30 20:41:48', '69', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751198', '83686', '2016-09-30 20:41:48', '46', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751199', '83686', '2016-09-30 20:41:48', '45.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751200', '83686', '2016-09-30 20:41:48', '42.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751201', '83687', '2016-09-30 20:41:48', '24', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751202', '83687', '2016-09-30 20:41:48', '22.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751203', '83687', '2016-09-30 20:41:48', '20.6', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751204', '83688', '2016-09-30 20:41:48', '34', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751205', '83688', '2016-09-30 20:41:48', '24.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751206', '83688', '2016-09-30 20:41:48', '26.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751207', '83689', '2016-09-30 20:41:48', '98.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751208', '83689', '2016-09-30 20:41:48', '97.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751209', '83690', '2016-09-30 20:41:48', '66.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751210', '83690', '2016-09-30 20:41:48', '55.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751211', '83690', '2016-09-30 20:41:48', '65.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751212', '83691', '2016-09-30 20:41:48', '26.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751213', '83691', '2016-09-30 20:41:48', '20.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751214', '83691', '2016-09-30 20:41:48', '19.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751215', '83692', '2016-09-30 20:41:48', '52.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751216', '83692', '2016-09-30 20:41:48', '57.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751217', '83692', '2016-09-30 20:41:48', '60.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751218', '83693', '2016-09-30 20:41:48', '42.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751219', '83693', '2016-09-30 20:41:48', '40.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751220', '83693', '2016-09-30 20:41:48', '38', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751221', '83694', '2016-09-30 20:41:48', '40.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751222', '83694', '2016-09-30 20:41:48', '38.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751223', '83694', '2016-09-30 20:41:48', '36.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751224', '83695', '2016-09-30 20:41:48', '16.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751225', '83695', '2016-09-30 20:41:48', '6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751226', '83695', '2016-09-30 20:41:48', '4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751227', '83696', '2016-09-30 20:41:48', '1.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751228', '83696', '2016-09-30 20:41:48', '1.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751229', '83696', '2016-09-30 20:41:48', '2.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751230', '83697', '2016-09-30 20:41:48', '60.8', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751231', '83697', '2016-09-30 20:41:48', '66', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751232', '83697', '2016-09-30 20:41:48', '64.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751233', '83698', '2016-09-30 20:41:48', '20.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751234', '83698', '2016-09-30 20:41:48', '18.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751235', '83698', '2016-09-30 20:41:48', '20.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751236', '83699', '2016-09-30 20:41:48', '52.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751237', '83699', '2016-09-30 20:41:48', '55.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751238', '83699', '2016-09-30 20:41:48', '40.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751239', '83700', '2016-09-30 20:41:48', '4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751240', '83700', '2016-09-30 20:41:48', '2.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751241', '83700', '2016-09-30 20:41:48', '2.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751242', '83701', '2016-09-30 20:41:48', '15.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751243', '83701', '2016-09-30 20:41:48', '15', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751244', '83701', '2016-09-30 20:41:48', '14.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751245', '83702', '2016-09-30 20:41:48', '49.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751246', '83702', '2016-09-30 20:41:49', '38.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751247', '83702', '2016-09-30 20:41:49', '50.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751248', '83703', '2016-09-30 20:41:49', '49.2', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751249', '83703', '2016-09-30 20:41:49', '46.8', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751250', '83703', '2016-09-30 20:41:49', '49.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751251', '83704', '2016-09-30 20:41:49', '42.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751252', '83704', '2016-09-30 20:41:49', '23.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751253', '83704', '2016-09-30 20:41:49', '12.9', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751254', '83705', '2016-09-30 20:41:49', '39.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751255', '83705', '2016-09-30 20:41:49', '39.1', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751256', '83705', '2016-09-30 20:41:49', '41.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751257', '83706', '2016-09-30 20:41:49', '65.8', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751258', '83706', '2016-09-30 20:41:49', '61.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751259', '83706', '2016-09-30 20:41:49', '59', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751260', '83707', '2016-09-30 20:41:49', '31.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751261', '83707', '2016-09-30 20:41:49', '26.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751262', '83707', '2016-09-30 20:41:49', '22.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751263', '83708', '2016-09-30 20:41:49', '44.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751264', '83708', '2016-09-30 20:41:49', '49.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751265', '83708', '2016-09-30 20:41:49', '59.6', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751266', '83709', '2016-09-30 20:41:49', '14.2', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751267', '83709', '2016-09-30 20:41:49', '13.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751268', '83709', '2016-09-30 20:41:49', '14.2', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751269', '83710', '2016-09-30 20:41:49', '10.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751270', '83710', '2016-09-30 20:41:49', '10.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751271', '83710', '2016-09-30 20:41:49', '9.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751272', '83711', '2016-09-30 20:41:49', '13.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751273', '83711', '2016-09-30 20:41:49', '11.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751274', '83711', '2016-09-30 20:41:49', '9.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751275', '83712', '2016-09-30 20:41:49', '35.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751276', '83712', '2016-09-30 20:41:49', '31.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751277', '83712', '2016-09-30 20:41:49', '29.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751278', '83713', '2016-09-30 20:41:49', '72.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751279', '83713', '2016-09-30 20:41:49', '67.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751280', '83713', '2016-09-30 20:41:49', '73.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751281', '83714', '2016-09-30 20:41:49', '62.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751282', '83714', '2016-09-30 20:41:49', '60.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751283', '83714', '2016-09-30 20:41:49', '63.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751284', '83715', '2016-09-30 20:41:49', '46.1', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751285', '83715', '2016-09-30 20:41:49', '45.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751286', '83715', '2016-09-30 20:41:49', '45.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751287', '83716', '2016-09-30 20:41:49', '30.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751288', '83716', '2016-09-30 20:41:49', '25', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751289', '83716', '2016-09-30 20:41:49', '15.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751290', '83717', '2016-09-30 20:41:49', '21', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751291', '83717', '2016-09-30 20:41:49', '26.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751292', '83717', '2016-09-30 20:41:49', '25.6', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751293', '83718', '2016-09-30 20:41:49', '36.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751294', '83718', '2016-09-30 20:41:49', '36.7', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751295', '83718', '2016-09-30 20:41:49', '38.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751296', '83719', '2016-09-30 20:41:49', '90.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751297', '83719', '2016-09-30 20:41:49', '80.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751298', '83719', '2016-09-30 20:41:49', '83.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751299', '83720', '2016-09-30 20:41:49', '37.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751300', '83720', '2016-09-30 20:41:49', '38.1', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751301', '83720', '2016-09-30 20:41:49', '38.4', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751302', '83721', '2016-09-30 20:41:49', '19.7', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751303', '83721', '2016-09-30 20:41:49', '19.4', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751304', '83721', '2016-09-30 20:41:49', '14.7', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751305', '83722', '2016-09-30 20:41:49', '86.4', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751306', '83722', '2016-09-30 20:41:49', '96.6', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751307', '83722', '2016-09-30 20:41:49', '97.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751308', '83723', '2016-09-30 20:41:49', '42.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751309', '83723', '2016-09-30 20:41:49', '40.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751310', '83723', '2016-09-30 20:41:49', '42.3', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751311', '83724', '2016-09-30 20:41:49', '18.3', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751312', '83724', '2016-09-30 20:41:49', '18.2', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751313', '83724', '2016-09-30 20:41:49', '18.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751314', '83725', '2016-09-30 20:41:49', '75.3', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751315', '83725', '2016-09-30 20:41:49', '74', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751316', '83725', '2016-09-30 20:41:49', '74.5', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751317', '83726', '2016-09-30 20:41:49', '70.5', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751318', '83726', '2016-09-30 20:41:50', '69.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751319', '83726', '2016-09-30 20:41:50', '69.1', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751320', '83727', '2016-09-30 20:41:50', '45.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751321', '83727', '2016-09-30 20:41:50', '43.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751322', '83727', '2016-09-30 20:41:50', '42.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751323', '83728', '2016-09-30 20:41:50', '32.3', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751324', '83728', '2016-09-30 20:41:50', '28.3', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751325', '83728', '2016-09-30 20:41:50', '28.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751326', '83729', '2016-09-30 20:41:50', '44', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751327', '83729', '2016-09-30 20:41:50', '37.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751328', '83729', '2016-09-30 20:41:50', '39.8', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751329', '83730', '2016-09-30 20:41:50', '40.6', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751330', '83730', '2016-09-30 20:41:50', '50.9', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751331', '83730', '2016-09-30 20:41:50', '51', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751332', '83731', '2016-09-30 20:41:50', '59', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751333', '83731', '2016-09-30 20:41:50', '66.6', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751334', '83732', '2016-09-30 20:41:50', '47.9', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751335', '83732', '2016-09-30 20:41:50', '45.5', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751336', '83732', '2016-09-30 20:41:50', '46.3', '2015');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}