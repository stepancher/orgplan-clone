<?php

class m170313_144314_cut_country_code_from_organizer_contact extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `cut_country_code_from_organizer_contact`;
            CREATE PROCEDURE `cut_country_code_from_organizer_contact`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_country_id INT DEFAULT 0;
                DECLARE c_new_country_id INT DEFAULT 0;
                DECLARE c_value TEXT DEFAULT '';
                DECLARE c_new_value TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT t.id, t.countryId, t.newCountryId, t.value, t.newValue FROM 
                                        (SELECT ocl.id, ocl.countryId, ocl.value,
                                        CASE WHEN ocl.value LIKE '77%' OR ocl.value LIKE '7(7%' OR ocl.value LIKE '+7(7%' THEN 6 #Казахстан
                                             WHEN ocl.value LIKE '7%' OR ocl.value LIKE '+7%' OR ocl.value LIKE '(%' OR ocl.value LIKE '8%' THEN 1 #Россия
                                             WHEN ocl.value LIKE '+373%' OR ocl.value LIKE '373%' THEN 8 #Молдова
                                             WHEN ocl.value LIKE '+374%' THEN 4 #Армения
                                             WHEN ocl.value LIKE '+375%' OR ocl.value LIKE '375%' THEN 5 #Беларусь
                                             WHEN ocl.value LIKE '+38(0%' OR ocl.value LIKE '+380%' OR ocl.value LIKE '380%' OR ocl.value LIKE '38(0%' THEN 13 #Украина
                                             WHEN ocl.value LIKE '+44%' OR ocl.value LIKE '44%' THEN 28 #Великобритания
                                             WHEN ocl.value LIKE '+49%' OR ocl.value LIKE '49%' THEN 2 #Германия
                                             WHEN ocl.value LIKE '+99(4%' OR ocl.value LIKE '99(4%' THEN 3 #Азербайджан
                                             WHEN ocl.value LIKE '+99(6%' OR ocl.value LIKE '+996%' OR ocl.value LIKE '996%' OR ocl.value LIKE '99(6%' THEN 7 #Киргизия
                                             WHEN ocl.value LIKE '+993%' THEN 10 #Туркменистан
                                             WHEN ocl.value LIKE '+995%' THEN 11 #Грузия
                                             WHEN ocl.value LIKE '+998%' OR ocl.value LIKE '998%' THEN 9 #Узбекистан
                                        ELSE NULL END newCountryId,
                                        CASE WHEN ocl.value LIKE '77%' OR ocl.value LIKE '49%' OR ocl.value LIKE '44%' OR ocl.value LIKE '+7%' THEN SUBSTR(ocl.value,3) 
                                             WHEN ocl.value LIKE '7(7%' OR ocl.value LIKE '998%' OR ocl.value LIKE '996%' OR ocl.value LIKE '+49%' OR ocl.value LIKE '+44%' OR ocl.value LIKE '380%' OR ocl.value LIKE '373%' OR ocl.value LIKE '375%' THEN SUBSTR(ocl.value,4) 
                                             WHEN ocl.value LIKE '+7(7%' OR ocl.value LIKE '+998%' OR ocl.value LIKE '+995%' OR ocl.value LIKE '+993%' OR ocl.value LIKE '99(6%' OR ocl.value LIKE '+996%' OR ocl.value LIKE '99(4%' OR ocl.value LIKE '38(0%' OR ocl.value LIKE '+380%' OR ocl.value LIKE '+373%' OR ocl.value LIKE '+374%' OR ocl.value LIKE '+375%' THEN SUBSTR(ocl.value,5) 
                                             WHEN ocl.value LIKE '7%' OR ocl.value LIKE '(%' OR ocl.value LIKE '8%' THEN SUBSTR(ocl.value,2) 
                                             WHEN ocl.value LIKE '+38(0%' OR ocl.value LIKE '+99(6%' OR ocl.value LIKE '+99(4%' THEN SUBSTR(ocl.value,6) 
                                        ELSE NULL END newValue
                                        FROM {{organizercontactlist}} ocl
                                        WHERE ocl.type != 2
                                        AND ocl.value NOT LIKE '%@%'
                                        ) t WHERE t.countryId IS NOT NULL AND t.newValue IS NOT NULL;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO c_id, c_country_id, c_new_country_id, c_value, c_new_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF c_country_id != c_new_country_id OR c_country_id IS NULL THEN
                        START TRANSACTION;
                            UPDATE {{organizercontactlist}} SET `countryId` = c_new_country_id WHERE `id` = c_id;
                        COMMIT;
                    END IF;
                    
                    IF c_value != c_new_value THEN
                        START TRANSACTION;
                            UPDATE {{organizercontactlist}} SET `value` = c_new_value WHERE `id` = c_id;
                        COMMIT;
                    END IF;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `cut_country_code_from_organizer_contact`();
            DROP PROCEDURE IF EXISTS `cut_country_code_from_organizer_contact`;
            
            UPDATE {{organizercontactlist}} SET `value`='7(495)937-40-81', `additionalContactInformation`='доб.324' WHERE `id`='54934';
            UPDATE {{organizercontactlist}} SET `value`='7(499)681-33-83', `additionalContactInformation`='доб.324' WHERE `id`='54935';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '1', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '1', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3879', '44)496-86-46', '7', 'доб.277', '13');
            UPDATE {{organizercontactlist}} SET `value`='44)496-86-45', `additionalContactInformation`='доб.277' WHERE `id`='50997';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3879', '44)496-86-46', '6', 'доб.277', '13');
            UPDATE {{organizercontactlist}} SET `value`='44)496-86-45', `additionalContactInformation`='доб.277' WHERE `id`='50996';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3879', '44)496-86-46', '4', 'доб.277', '13');
            UPDATE {{organizercontactlist}} SET `value`='44)496-86-45', `additionalContactInformation`='доб.277' WHERE `id`='50994';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3879', '44)496-86-46', '1', 'доб.277', '13');
            UPDATE {{organizercontactlist}} SET `value`='44)496-86-45', `additionalContactInformation`='доб.277' WHERE `id`='50992';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1206', '(926)996-39-04', '1', NULL, '1');
            UPDATE {{organizercontactlist}} SET `value`='(926)996-39-03' WHERE `id`='28987';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3721', '(495)925-6562', '1', 'доб.162', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-6561', `additionalContactInformation`='доб.162' WHERE `id`='49058';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3719', '(495)925-6561', '7', 'доб.162', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-6561' WHERE `id`='55083';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1114', '(495)925-65-62', '5', 'доб.135', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='28141';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1114', '(495)925-65-62', '4', 'доб.209', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='28140';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3443', '(495)925-65-62', '7', 'доб.178', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='46540';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3443', '(495)925-65-62', '6', 'доб.178', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='46539';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3443', '(495)925-65-62', '5', 'доб.178', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='46538';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3443', '(495)925-65-62', '1', 'доб.178', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='46532';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1282', '(495)925-65-62', '1', 'доб.187', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='54969';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1262', '(495)925-65-62', '7', 'доб.201', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='54967';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('1108', '(495)925-65-62', '5', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='28074';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('1107', '(495)925-65-62', '5', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='28056';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3578', '(495)925-65-62', '7', 'доб.180', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='47670';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3578', '(495)925-65-62', '6', 'доб.180', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='47667';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3578', '(495)925-65-62', '5', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='47665';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3578', '(495)925-65-62', '4', 'доб.180', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3578', '(495)925-65-62', '4', 'доб.180', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='47662';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3578', '(495)925-65-62', '1', 'доб.180', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='47660';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3701', '(495)925-65-61', '7', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3701', '(495)925-65-62', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='48933';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3701', '(495)925-65-62', '6', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='48930';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3701', '(495)925-65-62', '5', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='48927';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3701', '(495)925-65-62', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='48924';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3701', '(495)925-65-62', '1', '1');
            UPDATE {{organizercontactlist}} SET `value`='(495)925-65-61' WHERE `id`='48921';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-46', '7', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-49', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='26859';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-46', '6', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-49', '6', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='26856';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-46', '4', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-49', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='26852';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-46', '1', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('953', '(343)355-01-49', '1', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='26849';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-46', '7', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-49', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='42905';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-46', '6', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-49', '6', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='42902';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-46', '5', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-49', '5', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='42899';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-46', '4', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-49', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='42896';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-46', '1', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('2980', '(343)355-01-49', '1', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='42893';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-46', '7', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-49', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='44670';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-46', '6', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-49', '6', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='44668';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-46', '4', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-49', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='44665';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-46', '1', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3197', '(343)355-01-49', '1', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)355-01-42' WHERE `id`='44662';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('1258', '(343)286-11-53', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)286-11-63' WHERE `id`='29475';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('1258', '(343)286-11-53', '6', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)286-11-63' WHERE `id`='29473';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('1258', '(343)286-11-53', '5', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('1258', '(343)286-11-53', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='(343)286-11-63' WHERE `id`='29471';
            UPDATE {{organizercontactlist}} SET `value`='(343)286-11-63' WHERE `id`='29469';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}