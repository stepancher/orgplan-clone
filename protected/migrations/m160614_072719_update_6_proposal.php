<?php

class m160614_072719_update_6_proposal extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trattributeclass}} SET `label`=NULL, `placeholder`='Выберите дату или период' WHERE `id`='1517';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1448';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1454';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1523';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1457';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1526';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1529';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1460';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1463';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1532';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1466';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату или период' WHERE `id`='1535';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату и время' WHERE `id`='1451';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату и время' WHERE `id`='1520';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2038', '2037', 'inherit', '0');
			UPDATE {{trattributeclassproperty}} SET `value`='часов' WHERE `id`='2354';
			UPDATE {{trattributeclassproperty}} SET `value`='hours' WHERE `id`='2519';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n6_summary_hint_danger2', 'string', '0', '2081', '2081', '21', 'hintDanger', '0');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2178', 'ru', 'Наладка оборудования заказчика выполняется за отдельную плату. ');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2178', 'en', 'Наладка оборудования заказчика выполняется за отдельную плату. ');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES (NULL, '2019', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2016', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2022', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2025', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2031', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2028', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2037', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2040', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2043', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2046', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2049', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2052', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2055', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2059', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2062', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2065', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2068', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2071', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2019', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2016', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2022', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2025', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2031', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2028', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2037', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2040', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2043', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2046', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2049', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2052', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2055', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2059', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2062', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2065', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2068', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2071', 'endDate');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1929', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1930', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1931', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1932', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1933', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1934', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1935', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1936', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1937', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1938', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1939', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1940', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1941', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1942', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1943', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1944', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1945', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1946', 'ru', '2016-10-07');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1911', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1912', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1913', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1914', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1915', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1916', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1917', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1918', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1919', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1920', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1921', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1922', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1923', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1924', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1925', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1926', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1927', 'ru', '2016-10-04');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1928', 'ru', '2016-10-04');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2041', '2040', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2044', '2043', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2047', '2046', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2050', '2049', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2053', '2052', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2056', '2055', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2060', '2059', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2063', '2062', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2066', '2065', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2069', '2068', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2072', '2071', 'inherit', '0');
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEQUIPMENT RENT\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div> ' WHERE `id`='1537';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date' WHERE `id`='1499';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date' WHERE `id`='1505';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date' WHERE `id`='1511';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date and time' WHERE `id`='1520';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1517';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1523';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1526';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1529';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1532';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1535';
		";
	}
}