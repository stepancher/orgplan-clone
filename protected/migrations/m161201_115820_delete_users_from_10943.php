<?php

class m161201_115820_delete_users_from_10943 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        return "
            DELETE FROM tbl_userhasorganizer WHERE id = 12;
            DELETE FROM tbl_myfair WHERE id = 69;
            DELETE FROM tbl_user WHERE id = 2069;
            DELETE FROM tbl_myfair WHERE userId = 2069;
            DELETE FROM tbl_contact WHERE id = 1565;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}