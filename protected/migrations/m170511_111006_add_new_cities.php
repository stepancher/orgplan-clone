<?php

class m170511_111006_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('187', 'karlsfeld');

            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1584', 'ru', 'Карлсфельд');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1584', 'en', 'Karlsfeld');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1584', 'de', 'Karlsfeld');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('163', 'schwaigern');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1585', 'ru', 'Швайгерн');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1585', 'en', 'Schwaigern');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1585', 'de', 'Schwaigern');
            
            
            INSERT INTO {{region}} (`districtId`) VALUES ('81');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1104', 'ru', 'Кумера', 'coomera');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1104', 'en', 'Coomera', 'coomera');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1104', 'de', 'Coomera', 'coomera');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1104', 'coomera');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1586', 'ru', 'Кумера');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1586', 'en', 'Coomera');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1586', 'de', 'Coomera');
            
            
            INSERT INTO {{region}} (`districtId`) VALUES ('363');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('1105', 'ru', 'Санта-Барбара');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('1105', 'en', 'Santa Barbara');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('1105', 'de', 'Santa Barbara');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1105', 'santa-barbara');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1587', 'ru', 'Санта-Барбара');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1587', 'en', 'Santa Barbara');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1587', 'de', 'Santa Barbara');
            
            
            INSERT INTO {{region}} (`districtId`) VALUES ('61');
            
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1106', 'ru', 'Рейн-Пфальц (район)', 'rhein-pfalz-kreis');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1106', 'en', 'Rhein-Pfalz-Kreis', 'rhein-pfalz-kreis');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1106', 'de', 'Rhein-Pfalz-Kreis', 'rhein-pfalz-kreis');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1106', 'otterstadt');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1588', 'ru', 'Оттерштадт');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1588', 'en', 'Otterstadt');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1588', 'de', 'Otterstadt');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}