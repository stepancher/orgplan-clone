<?php

class m160506_063454_9_proposal_upd extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		UPDATE {{attributeclass}} SET `parent`='628' WHERE `id`='640';
		DELETE FROM {{attributeclass}} WHERE `id`='639';
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `value`, `class`) VALUES (NULL, '640', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `value`, `class`) VALUES (NULL, '640', 'Такелажник, стропальщик', 'selfCollapseLabel');
		UPDATE {{attributeclass}} SET `priority`='20' WHERE `id`='640';
		UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='640';
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '672', '960', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('671', '1002', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('681', '962', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('680', '1004', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('690', '964', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('689', '1006', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('672', '960', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('699', '966', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('698', '1008', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('708', '968', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('707', '1010', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('717', '970', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('716', '1012', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('726', '972', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('725', '1014', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('735', '974', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('734', '1016', 'get', '0');
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '602', '734', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '602', '725', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '716', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '707', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '698', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '689', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '680', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '671', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '636', '753', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '744', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '735', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '726', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '717', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '708', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '699', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '690', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '681', 'summaryAndRound', '{\"min\": 2}', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '672', 'summaryAndRound', '{\"min\": 2}', '0');
	    ";
	}
}