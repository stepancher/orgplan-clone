<?php

class m150826_134449_create_Price_tables extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateProductCategory();
		$sql .= $this->getCreateProduct();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{productcategory}};
			DROP TABLE {{product}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateProduct(){
		return '
			CREATE TABLE {{product}} (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) DEFAULT NULL,
		  `description` varchar(1000) DEFAULT NULL,
		  `param` varchar(1000) DEFAULT NULL,
		  `cost` int(11) DEFAULT NULL,
		  `costType` int(11) DEFAULT NULL,
		  `categoryId` int(11) DEFAULT NULL,
		  `cityId` int(11) DEFAULT NULL,
		  `currency` int(11) DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  KEY `fk_tbl_product_1_idx` (`categoryId`),
		  KEY `fk_tbl_product_2_idx` (`cityId`),
		  CONSTRAINT `fk_tbl_product_2` FOREIGN KEY (`cityId`) REFERENCES `tbl_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
		  CONSTRAINT `fk_tbl_product_1` FOREIGN KEY (`categoryId`) REFERENCES `tbl_productcategory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
		) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
		';
	}

	public function getCreateProductCategory(){
		return '
			CREATE TABLE {{productcategory}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(100) DEFAULT NULL,
			  `description` varchar(1000) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

		';
	}
}