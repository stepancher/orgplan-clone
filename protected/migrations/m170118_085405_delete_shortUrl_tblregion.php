<?php

class m170118_085405_delete_shortUrl_tblregion extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql(){

        Yii::app()->consoleRunner->run(
            'translates regionUpdateShortUrl ',
            true
        );

        return "
            ALTER TABLE {{region}} 
              DROP COLUMN `shortUrl`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}