<?php

class m160727_114412_added_sng_exhibition_complexes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			INSERT INTO {{country}} (`shortUrl`) VALUES ('az');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('am');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('by');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('kz');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('kg');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('md');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('uz');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('tm');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('ge');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('mn');
			INSERT INTO {{country}} (`shortUrl`) VALUES ('ua');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}