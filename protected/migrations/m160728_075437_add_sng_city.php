<?php

class m160728_075437_add_sng_city extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{city}} SET `shortUrl`='koryamja' WHERE `shortUrl`='Koryamja';
			UPDATE {{city}} SET `shortUrl`='sarapul' WHERE `shortUrl`='Sarapul';
			UPDATE {{city}} SET `shortUrl`='jukovskiy' WHERE `shortUrl`='Jukovskiy';
			UPDATE {{city}} SET `shortUrl`='noyabrsk' WHERE `shortUrl`='Noyabrsk';
			UPDATE {{city}} SET `shortUrl`='bratsk' WHERE `shortUrl`='Bratsk';
			UPDATE {{city}} SET `shortUrl`='sosnogorsk' WHERE `shortUrl`='Sosnogorsk';
			UPDATE {{city}} SET `shortUrl`='ukhta' WHERE `shortUrl`='Ukhta';
			UPDATE {{city}} SET `shortUrl`='novodvinsk' WHERE `shortUrl`='Novodvinsk';
			UPDATE {{city}} SET `shortUrl`='velsk' WHERE `shortUrl`='Velsk';
			UPDATE {{city}} SET `shortUrl`='odintsovo' WHERE `shortUrl`='Odintsovo';
			UPDATE {{city}} SET `shortUrl`='reutov' WHERE `shortUrl`='Reutov';
			UPDATE {{city}} SET `shortUrl`='chichkovo-selo' WHERE `shortUrl`='Chichkovo selo';
			UPDATE {{city}} SET `shortUrl`='syzran' WHERE `shortUrl`='Syzran';
			UPDATE {{city}} SET `shortUrl`='solnechniy-pos' WHERE `shortUrl`='Solnechniy pos';
			UPDATE {{city}} SET `shortUrl`='stariy-oskol' WHERE `shortUrl`='Stariy Oskol';
			UPDATE {{city}} SET `shortUrl`='novaya-usman-s.' WHERE `shortUrl`='Novaya Usman s.';
			UPDATE {{city}} SET `shortUrl`='shestakovo-s.' WHERE `shortUrl`='Shestakovo s.';
			UPDATE {{city}} SET `shortUrl`='shatilovo-p.' WHERE `shortUrl`='Shatilovo p.';
			UPDATE {{city}} SET `shortUrl`='makarie-s.' WHERE `shortUrl`='Makarie s.';
			UPDATE {{city}} SET `shortUrl`='novoanninskiy-rayon' WHERE `shortUrl`='Novoanninskiy rayon';
			UPDATE {{city}} SET `shortUrl`='yalta' WHERE `shortUrl`='Yalta';
			
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('124', 'baku');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('125', 'yerevan');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('126', 'minsk');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('127', 'bobruysk');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('127', 'mogilev');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('127', 'kastsyukovichy');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('128', 'grodno');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('129', 'astana');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('130', 'almaty');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('131', 'atyrau');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('132', 'aktau');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('133', 'aktobe');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('134', 'pavlodar');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('135', 'bishkek');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('136', 'kishinev');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('137', 'karaganda');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('138', 'tashkent');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('139', 'fergana');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('140', 'ashgabat');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('141', 'turkmenbashi');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('142', 'tbilisi');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('143', 'ulaγanbaγatur');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('144', 'kiev');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('145', 'odessa');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('93', 'simferopol');
			INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('93', 'sevastopol');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}