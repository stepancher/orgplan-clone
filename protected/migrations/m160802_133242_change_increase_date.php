<?php

class m160802_133242_change_increase_date extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{20:2}' WHERE `id`='641';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 41:1.5}' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 41:1.5}' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 41:1.5}' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 41:1.5}' WHERE `id`='65';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='1';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='2';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='3';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='4';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='5';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='7';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='10';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='18';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='19';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='20';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='21';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='22';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='23';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='24';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='25';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 42:1.5}' WHERE `id`='65';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 42:1.5}' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:1.5, 13:2}' WHERE `id`='921';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 21:2}' WHERE `id`='981';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 22:2}' WHERE `id`='981';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 21:2}' WHERE `id`='981';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{70:1.5, 20:2}' WHERE `id`='689';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{69:1.5, 20:2}' WHERE `id`='689';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 20:2}' WHERE `id`='1052';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}