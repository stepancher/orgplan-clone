<?php

class m160823_133651_new_proposal_4a extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
        INSERT INTO {{proposalelementclass}} (`id`, `name`, `fixed`, `fairId`, `active`, `deadline`, `priority`) VALUES (NULL, 'zayavka_n4a', '0', '184', '1', '2016-10-08 06:00:00', '4.1');
        INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('27', 'ru', 'Заявка №4a', 'Подключение сжатого воздуха');
        UPDATE {{trproposalelementclass}} SET `pluralLabel`='ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ. ПОДКЛЮЧЕНИЕ СЖАТОГО ВОЗДУХА' WHERE `id`='32';
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_hint', 'int', '0', '0', '2706', '0', 'hint', '0');
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_compressed', 'int', '0', '2706', '2706', '10', 'coefficientDouble', '0');
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_compressed2', 'int', '0', '2706', '2706', '20', 'coefficientDouble', '0');
        UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='2706';
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_hint2', 'int', '0', '2706', '2706', '30', 'hintDanger', '0');
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_compressed3', 'int', '0', '2706', '2706', '40', 'coefficientDouble', '0');
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_hint3', 'int', '0', '2706', '2706', '50', 'hintDanger', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n4a_summary', 'int', '0', '2706', '2706', '60', 'summary', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n4a_hint4', 'int', '0', '2706', '2706', '70', 'hint', '0');
        INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4a_hint5', 'int', '0', '2706', '2706', '80', 'hintDanger', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n4a', 'int', '1', '0', '0', '2695', 'partiesSignature', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n4a', 'int', '1', '2695', '2695', '1', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_right_block_n4a', 'int', '1', '2695', '2695', '2', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n4a_1', 'string', '1', '2696', '2695', '1', 'headerH1', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n4a_2', 'string', '1', '2696', '2695', '2', 'envVariable', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n4a_3', 'string', '1', '2696', '2695', '3', 'envVariable', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n4a_1', 'string', '1', '2697', '2695', '1', 'headerH1', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n4a_2', 'string', '1', '2697', '2695', '2', 'envVariable', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n4a_3', 'string', '1', '2697', '2695', '3', 'envVariable', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n4a_4', 'string', '1', '2696', '2695', '5', 'text', '0');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n4a_4', 'string', '1', '2697', '2695', '5', 'text', '0');
        INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('27', '2706', '0', '0', '10');
        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n4a_shadow_time', 'string', '0', '0', '2745', '10', 'hiddenDiscount', '0');
        INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('27', '2726', '0', '0', '0');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2706', 'ru', 'n4a_hint');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2707', 'ru', 'n4a_compressed');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2708', 'ru', 'n4a_compressed2');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2709', 'ru', 'n4a_hint2');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2710', 'ru', 'n4a_compressed3');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2711', 'ru', 'n4a_hint3');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2712', 'ru', 'n4a_summary');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2713', 'ru', 'n4a_hint4');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2714', 'ru', 'n4a_hint5');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2715', 'ru', 'parties_signature_n4a');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2716', 'ru', 'parties_signature_left_block_n4a');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2717', 'ru', 'parties_signature_right_block_n4a');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2718', 'ru', 'parties_signature_row_left_n4a_1');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2719', 'ru', 'parties_signature_row_left_n4a_2');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2720', 'ru', 'parties_signature_row_left_n4a_3');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2721', 'ru', 'parties_signature_row_right_n4a_1');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2722', 'ru', 'parties_signature_row_right_n4a_2');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2723', 'ru', 'parties_signature_row_right_n4a_3');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2724', 'ru', 'parties_signature_row_left_n4a_4');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2725', 'ru', 'parties_signature_row_right_n4a_4');
        UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 26 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='3115';
        UPDATE {{trattributeclass}} SET `label`='Подключение сжатого воздуха, расход до 30 куб.м./час' WHERE `id`='3116';
        UPDATE {{trattributeclass}} SET `label`='Подключение сжатого воздуха, расход более 30 куб.м./час' WHERE `id`='3117';
        UPDATE {{trattributeclass}} SET `label`='\"Давление в магистрали ~6 кг / кв . см\nПодключение производится шлангом 1 / 2\"\" . Шланг для подключения до 10,0 п . м . предоставляется без дополнительной оплаты . Если длина шланга подключения превышает 10 п . м ., то шланг необходимой длины предоставляется Участнику в аренду . \"' WHERE `id`='3118';
        UPDATE {{trattributeclass}} SET `label`='Аренда шланга, диаметр внутренний 1/2\" (15мм), по 5м' WHERE `id`='3119';
        UPDATE {{trattributeclass}} SET `label`='Подключение потребителей к подведенному шлангу Участник осуществляет самостоятельно с использованием собственных комплектующих и материалов . ' WHERE `id`='3120';
        UPDATE {{trattributeclass}} SET `label`='Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента . ' WHERE `id`='3122';
        UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах .<br /><br />Стоимость услуг на технические подключения увеличивается на 50 % при заказе после 26.08.16 и на 100 % после 21.09.16 . <br /><br /> Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг . Все претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00,  07 октября 2016 г . В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом . ' WHERE `id`='3123';
        UPDATE {{attributeclass}} SET `class`='coefficientDoubleInline' WHERE `id`='2726';
        UPDATE {{attributeclass}} SET `class`='coefficientDoubleInline' WHERE `id`='2727';
        UPDATE {{attributeclass}} SET `class`='coefficientDoubleInline' WHERE `id`='2729';
        UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2729';
        UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2727';
        UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2726';
        UPDATE {{attributeclass}} SET `parent`='2706', `super`='2706', `priority`='90' WHERE `id`='2734';
        UPDATE {{attributeclass}} SET `parent`='2734', `super`='2706' WHERE `id`='2735';
        UPDATE {{attributeclass}} SET `parent`='2734', `super`='2706' WHERE `id`='2736';
        UPDATE {{attributeclass}} SET `parent`='2735', `super`='2706' WHERE `id`='2737';
        UPDATE {{attributeclass}} SET `parent`='2735', `super`='2706' WHERE `id`='2738';
        UPDATE {{attributeclass}} SET `parent`='2735', `super`='2706' WHERE `id`='2739';
        UPDATE {{attributeclass}} SET `parent`='2736', `super`='2706' WHERE `id`='2740';
        UPDATE {{attributeclass}} SET `parent`='2736', `super`='2706' WHERE `id`='2741';
        UPDATE {{attributeclass}} SET `parent`='2736', `super`='2706' WHERE `id`='2742';
        UPDATE {{attributeclass}} SET `parent`='2735', `super`='2706' WHERE `id`='2743';
        UPDATE {{attributeclass}} SET `parent`='2736', `super`='2706' WHERE `id`='2744';
        UPDATE {{trattributeclass}} SET `label`='Экспонент' WHERE `id`='3127';
        UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='3128';
        UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='3129';
        UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='3133';
        UPDATE {{trattributeclass}} SET `label`='Организатор' WHERE `id`='3130';
        UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='3131';
        UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='3132';
        UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='3134';
        UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 26.08.16 и на 100% после 21.09.16.' WHERE `id`='3116';
        UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 26.08.16 и на 100% после 21.09.16.' WHERE `id`='3117';
        UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 26.08.16 и на 100% после 21.09.16.' WHERE `id`='3119';
        UPDATE {{attributeclass}} SET `class`='hiddenDiscount' WHERE `id`='2726';
        
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2707', '405', 'price');
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2708', '575', 'price');
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2710', '40', 'price');
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2707', 'USD', 'currency');
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2708', 'USD', 'currency');
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2710', 'USD', 'currency');
        INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2712', 'USD', 'currency');
        INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '2677', '2677', 'increaseByLeftDay', '{1000:1, 43:1.5, 17:2}', '1');
        INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2726', '2726', 'inherit', '0');
        UPDATE {{attributeclassdependency}} SET `attributeClass`='2707', `attributeClassRelated`='2726' WHERE `id`='1077';
        INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2708', '2726', 'inherit', '0');
        INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2710', '2726', 'inherit', '0');
        UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 42:1.5, 16:2}' WHERE `id`='1077';
        
        UPDATE {{attributeclassdependency}} SET `attributeClass`='2726' WHERE `id`='1077';
        UPDATE {{attributeclassdependency}} SET `attributeClass`='2707' WHERE `id`='1078';

        INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n4a_agreement', 'bool', '0', '2706', '2706', '100', 'checkboxAgreement', '0');
        UPDATE {{attributeclass}} SET `super`='2706', `priority`='90' WHERE `id`='2715';
        UPDATE {{attributeclass}} SET `parent`='2706', `super`='2706' WHERE `id`='2716';
        UPDATE {{attributeclass}} SET `parent`='2706', `super`='2706' WHERE `id`='2717';
        UPDATE {{attributeclass}} SET `parent`='2716', `super`='2706' WHERE `id`='2718';
        UPDATE {{attributeclass}} SET `parent`='2716', `super`='2706' WHERE `id`='2719';
        UPDATE {{attributeclass}} SET `parent`='2716', `super`='2706' WHERE `id`='2720';
        UPDATE {{attributeclass}} SET `parent`='2717', `super`='2706' WHERE `id`='2721';
        UPDATE {{attributeclass}} SET `parent`='2717', `super`='2706' WHERE `id`='2722';
        UPDATE {{attributeclass}} SET `parent`='2717', `super`='2706' WHERE `id`='2723';
        UPDATE {{attributeclass}} SET `parent`='2716', `super`='2706' WHERE `id`='2724';
        UPDATE {{attributeclass}} SET `parent`='2717', `super`='2706' WHERE `id`='2725';
        UPDATE {{attributeclass}} SET `super`='2726' WHERE `id`='2726';
        
        UPDATE {{attributeclass}} SET `parent`='2706' WHERE `id`='2715';
        UPDATE {{attributeclass}} SET `parent`='2715' WHERE `id`='2716';
        UPDATE {{attributeclass}} SET `parent`='2715' WHERE `id`='2717';
        
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2706', 'en', 'This form should be filled in until August 26, 2016. Please return the application form by fax +7(495)781-37-05, or e-mail: agrosalon@agrosalon.ru');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES (NULL, '2707', 'en', 'Compressed air, consumption up to 30 m³ / h', '<div class=\"hint - header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.26, 2016, by 100% if ordered after Sep.21, 2016');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES (NULL, '2708', 'en', 'Compressed air, consumption of more than 30 m³ / h', '<div class=\"hint - header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.26, 2016, by 100% if ordered after Sep.21, 2016');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2709', 'en', 'Line pressure about 6 kg / cm. Hose 1/2 \" . The hose for connecting up to 10.0 LM is provided without extra charge . The hose more than 10 LM are provided by lease . ');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES (NULL, '2710', 'en', 'Lease of hoses, the internal diameter of 1 / 2 \"(15mm) (5 long meters)', '<div class=\"hint - header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.26, 2016, by 100% if ordered after Sep.21, 2016');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2711', 'en', 'The Exhibitor connects the hoses on one\'s own using their own utilities and materials.');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2712', 'en', 'n4a_summary');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2713', 'en', 'Payments shall be effected in USD. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2714', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.</div></div>Cost of services increases by 50% if ordered after Aug.26, 2016, by 100% if ordered after Sep.21, 2016<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application are considered to be performed with the proper quality and accepted by the Exhibitor.');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2715', 'en', 'parties_signature_n4a');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2716', 'en', 'parties_signature_left_block_n4a');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2717', 'en', 'parties_signature_right_block_n4a');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2718', 'en', 'Exhibitor');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2719', 'en', 'position');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2720', 'en', 'Name');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2721', 'en', 'Organizer');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2722', 'en', 'position');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2723', 'en', 'Name');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2724', 'en', 'signature');
        INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2725', 'en', 'signature');
        UPDATE {{trproposalelementclass}} SET `pluralLabel`='TECHNICAL AND ENGINEERING CONNECTIONS. COMPRESSED AIR' WHERE `id`='31';
        ";
    }

    public function downSql()
    {
        return TRUE;
    }
}