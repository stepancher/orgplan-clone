<?php

class m160925_141705_gradoteka_industry4_derivate extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500061', '2709+2710', 'Экспорт', 'derivative', 'ДолларСША', '0');
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500063', '2709+2710', 'Импорт', 'derivative', 'ДолларСША', '0');
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503232';
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503231';

            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500061', 'aggregatable', 'SUM');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500063', 'aggregatable', 'SUM');

            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','1','5745eb7f74d897cb088b636f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','2','5745ec4d74d897cb088d9ecf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','3','5745eb7f74d897cb088b636b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','4','5745eb7f74d897cb088b6367');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','5','5745eb7f74d897cb088b6397');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','6','5745ed1874d897cb088f7814');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','7','5745eb7f74d897cb088b637b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','8','5745ed1874d897cb088f77e6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','9','5745eb7f74d897cb088b638f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','10','5745ee7b74d897cb08929871');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','11','5745ebbf74d897cb088c3507');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','12','5745ecd474d897cb088edc22');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','13','5745eb7f74d897cb088b6383');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','14','5745eb7f74d897cb088b639b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','15','5745eb7f74d897cb088b63a7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','16','5745ebbf74d897cb088c351d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','17','5745eb7f74d897cb088b6373');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','18','5745eb7f74d897cb088b63a3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','19','5745edea74d897cb08915422');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','20','5745ec4d74d897cb088d9ef5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','21','5745eb7f74d897cb088b63ab');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','22','5745ebbf74d897cb088c3527');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','23','5745ecd474d897cb088edc30');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','24','5745ec0474d897cb088ceba9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','25','5745ed1874d897cb088f77f8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','26','5745eb7f74d897cb088b63af');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','27','5745eb7f74d897cb088b63b3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','28','5745ebbf74d897cb088c352d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','29','5745eb7f74d897cb088b63bb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','30','5745eb7f74d897cb088b63bf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','31','5745eb7f74d897cb088b63c3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','32','5745ec0474d897cb088cebbb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','33','5745eb7f74d897cb088b63cb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','34','5745ee3374d897cb0891f7d7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','35','5745ec0474d897cb088cebc5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','36','5745eb7f74d897cb088b63cf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','37','5745eb7f74d897cb088b63d7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','38','5745ec9474d897cb088e4a40');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','39','5745eb7f74d897cb088b63d3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','40','5745ec4d74d897cb088d9f13');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','41','5745ec4d74d897cb088d9f0f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','42','5745eb7f74d897cb088b63b7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','43','5745eb7f74d897cb088b6387');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','44','5745eb7f74d897cb088b638b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','45','5745eb7f74d897cb088b6393');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','46','5745eb7f74d897cb088b6377');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','47','5745eb7f74d897cb088b63c7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','48','5745eb7f74d897cb088b637f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','49','5745eb7f74d897cb088b639f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','50','5745eb7f74d897cb088b63db');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','51','5745eb7f74d897cb088b63f7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','52','5745eb7f74d897cb088b63f3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','53','5745eb7f74d897cb088b63fb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','54','5745ed1874d897cb088f781e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','55','5745eb7f74d897cb088b6447');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','56','5745eb7f74d897cb088b63eb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','57','5745eb7f74d897cb088b63ef');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','58','5745eb7f74d897cb088b63e7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','59','5745eb7f74d897cb088b63e3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','60','5745ebbf74d897cb088c3555');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','61','5745eb7f74d897cb088b63ff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','62','5745eeff74d897cb0893bcf7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','63','5745eb7f74d897cb088b640f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','64','5745ec0574d897cb088cebe3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','65','5745eb7f74d897cb088b6403');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','66','5745ec9474d897cb088e4a5c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','67','5745eb7f74d897cb088b6407');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','68','5745ef4574d897cb0894553c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','69','5745eb7f74d897cb088b640b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','70','5745ebbf74d897cb088c359f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','71','5745eb7f74d897cb088b656b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','72','5745eb7f74d897cb088b656f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','73','5745eb7f74d897cb088b6573');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','74','5745ef4574d897cb0894556e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','75','5745ebbf74d897cb088c359b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','76','5745ec0574d897cb088cec1d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','77','5745eb7f74d897cb088b6477');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','78','5745eeff74d897cb0893bd37');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','79','5745eb7f74d897cb088b643f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','80','5745eb7f74d897cb088b646f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','81','5745eb7f74d897cb088b647f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','82','5745eb7f74d897cb088b647b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','83','5745eb7f74d897cb088b6473');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','84','5745eb7f74d897cb088b646b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','85','5745eb7f74d897cb088b6443');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','86','5745ebbf74d897cb088c356f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','87','5745ebbf74d897cb088c3575');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','88','5745eb7f74d897cb088b6453');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','89','5745eb7f74d897cb088b648f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','90','5745eb7f74d897cb088b6493');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','91','5745eb7f74d897cb088b6487');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','92','5745ec0574d897cb088cec41');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','93','5745eb7f74d897cb088b6497');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','94','5745eb7f74d897cb088b649b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','95','5745eb7f74d897cb088b648b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','96','5745eb7f74d897cb088b649f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','97','5745eb7f74d897cb088b6457');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','98','5745eb7f74d897cb088b64a3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','99','5745eb7f74d897cb088b6577');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','100','5745ee7b74d897cb089298cd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','101','5745eb7f74d897cb088b64bb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','102','5745edeb74d897cb08915482');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','103','5745eb7f74d897cb088b6413');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','104','5745ef4574d897cb089455dc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','105','5745ebc074d897cb088c35bd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','106','5745eb7f74d897cb088b6437');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','107','5745eb7f74d897cb088b64ab');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','108','5745eb7f74d897cb088b642b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','109','5745eb7f74d897cb088b642f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','110','5745eb7f74d897cb088b641b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','111','5745eb7f74d897cb088b6417');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','112','5745eb7f74d897cb088b64af');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','113','5745eb7f74d897cb088b64b3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','114','5745eb7f74d897cb088b6433');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','115','5745eb7f74d897cb088b6423');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','116','5745ec0574d897cb088cebf5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','117','5745eb7f74d897cb088b64b7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','118','5745ed5d74d897cb089014f1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','119','5745eb7f74d897cb088b64cf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','120','5745ec0574d897cb088cec55');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','121','5745eb7f74d897cb088b64bf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','122','5745eb7f74d897cb088b64d3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','123','5745eb7f74d897cb088b64c7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','124','5745eb7f74d897cb088b64cb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','125','5745ebc074d897cb088c35e3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','126','5745eb7f74d897cb088b64ef');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','127','5745ed5d74d897cb0890155b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','128','5745eeff74d897cb0893bd7d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','129','5745eb7f74d897cb088b64e7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','130','5745eb7f74d897cb088b64d7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','131','5745ee7b74d897cb08929933');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','132','5745eb7f74d897cb088b64eb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','133','5745ed1874d897cb088f785e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','134','5745ebc074d897cb088c35ed');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','135','5745eb7f74d897cb088b64db');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','136','5745eb7f74d897cb088b64e3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','137','5745ebc074d897cb088c35dd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','138','5745ee7b74d897cb08929943');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','139','5745ec9574d897cb088e4ae4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','140','5745eb7f74d897cb088b64ff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','141','5745eb7f74d897cb088b64f3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','142','5745eb7f74d897cb088b64f7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','143','5745eb7f74d897cb088b64fb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','144','5745ed1974d897cb088f78b0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','145','5745ec0574d897cb088cec77');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','146','5745ebc074d897cb088c35f1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','147','5745eb7f74d897cb088b6503');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','148','5745eb7f74d897cb088b63df');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','149','5745ec4d74d897cb088d9fab');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','150','5745ebbf74d897cb088c35a9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','151','5745ed5d74d897cb0890156b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','152','5745ecd474d897cb088edcf0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','153','5745ec4d74d897cb088d9fb9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','154','5745eb7f74d897cb088b651b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','155','5745eb7f74d897cb088b650b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','156','5745ecd474d897cb088edcea');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','157','5745eb7f74d897cb088b650f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','158','5745eb7f74d897cb088b6517');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','159','5745eb7f74d897cb088b651f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','160','5745ec0574d897cb088cec85');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','161','5745eb7f74d897cb088b6523');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','162','5745eb7f74d897cb088b652b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','163','5745ec0574d897cb088cec9d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','164','5745eb7f74d897cb088b6527');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','165','5745eb7f74d897cb088b6537');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','166','5745eb7f74d897cb088b653f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','167','5745ec0574d897cb088cec99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','168','5745eb7f74d897cb088b653b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','169','5745eb7f74d897cb088b6467');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','170','5745eb7f74d897cb088b655f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','171','5745ec0574d897cb088ceca1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','172','5745eb7f74d897cb088b652f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','173','5745ed5d74d897cb08901591');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','174','5745eb7f74d897cb088b6547');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','175','5745eb7f74d897cb088b6543');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','176','5745eb7f74d897cb088b6557');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','177','5745ec4d74d897cb088d9fdb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','178','5745ebc074d897cb088c3619');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','179','5745ec4d74d897cb088d9fd5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','180','5745eb7f74d897cb088b654f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','181','5745eb7f74d897cb088b654b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','182','5745eb7f74d897cb088b6553');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','183','5745ee3474d897cb0891f8d1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','184','5745eb7f74d897cb088b6567');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','185','5745eb7f74d897cb088b655b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','186','5745eb7f74d897cb088b6563');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','187','5745ebbf74d897cb088c3587');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','188','5745eb7f74d897cb088b6513');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','189','5745eb7f74d897cb088b645b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','190','5745eb7f74d897cb088b645f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','191','5745eb7f74d897cb088b6463');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','192','5745ec0574d897cb088cec19');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','193','5745eb7f74d897cb088b6483');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','194','5745eb7f74d897cb088b64df');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','195','5745eb7f74d897cb088b643b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','196','5745eb7f74d897cb088b6427');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','197','5745eb7f74d897cb088b641f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','198','5745eb7f74d897cb088b6533');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','199','5745eb7f74d897cb088b64c3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','200','5745eb7f74d897cb088b644b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','201','5745ef4574d897cb0894557c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','202','5745ec9574d897cb088e4b08');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','203','5745eb7f74d897cb088b644f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','204','5745eb7f74d897cb088b657b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','205','5745eb7f74d897cb088b6507');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','206','5745ed5d74d897cb08901597');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','207','5745ebc074d897cb088c35b7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('28','208','5745eb7f74d897cb088b64a7');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','1','5745f17474d897cb0899cb42');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','2','5745ef9174d897cb08950c71');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','3','5745f2f174d897cb089d1179');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','4','5745ef9174d897cb08950c75');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','5','5745f1d474d897cb089a9f0f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','6','5745ef9174d897cb08950c81');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','7','5745ef9174d897cb08950c7d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','8','5745ef9174d897cb08950c89');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','9','5745ef9174d897cb08950c8d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','10','5745ef9174d897cb08950c85');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','11','5745f1d474d897cb089a9f19');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','12','5745ef9174d897cb08950cc1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','13','5745ef9174d897cb08950d11');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','14','5745ef9174d897cb08950ca1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','15','5745eff574d897cb089654c2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','16','5745f05574d897cb08974187');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','17','5745ef9174d897cb08950ca5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','18','5745ef9174d897cb08950cad');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','19','5745eff574d897cb089654cc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','20','5745ef9174d897cb08950cc5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','21','5745f1d474d897cb089a9f3d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','22','5745eff574d897cb089654c8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','23','5745ef9174d897cb08950cb1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','24','5745ef9174d897cb08950cc9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','25','5745eff574d897cb089654d8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','26','5745ef9174d897cb08950c91');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','27','5745f2f174d897cb089d11bd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','28','5745ef9174d897cb08950c99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','29','5745ef9174d897cb08950cd1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','30','5745f11674d897cb0898fb2b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','31','5745ef9174d897cb08950cd5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','32','5745ef9174d897cb08950cdd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','33','5745ef9174d897cb08950cd9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','34','5745ef9174d897cb08950ce5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','35','5745ef9174d897cb08950ce1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','36','5745f43874d897cb089f591a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','37','5745ef9174d897cb08950c79');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','38','5745ef9174d897cb08950ce9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','39','5745ef9174d897cb08950ced');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','40','5745ef9174d897cb08950cf1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','41','5745ef9174d897cb08950cf5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','42','5745ef9174d897cb08950cfd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','43','5745f0b674d897cb089820d2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','44','5745f0b674d897cb089820ce');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','45','5745ef9174d897cb08950cbd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','46','5745ef9174d897cb08950d0d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','47','5745f1d474d897cb089a9f31');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','48','5745ef9174d897cb08950d01');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','49','5745eff574d897cb089654f0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','50','5745ef9174d897cb08950d05');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','51','5745f23374d897cb089b71e8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','52','5745ef9174d897cb08950d09');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','53','5745ef9174d897cb08950cb5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','54','5745ef9174d897cb08950cb9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','55','5745f05574d897cb0897418b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','56','5745ef9174d897cb08950c9d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','57','5745ef9174d897cb08950c95');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','58','5745ef9174d897cb08950cf9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','59','5745f0b574d897cb0898209a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','60','5745ef9174d897cb08950ca9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','61','5745eff574d897cb089654fa');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`,`statId`) VALUES ('29','62','5745ef9174d897cb08950ccd');
                        
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500061', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500063', 'country');
            
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68805', '2016-09-25 17:33:06', '156186000000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68805', '2016-09-25 17:33:06', '23617300000', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68806', '2016-09-25 17:35:22', '1466140000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68806', '2016-09-25 17:35:22', '145945000', '2016');
            
            UPDATE {{charttablegroup}} SET `name`='Основные показатели отрасли' WHERE `id`='20';
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Объем ресурсов', '37', '2');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Экспорт\\Импорт', '37', '4');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Таможенная справка', '37', '5');
            
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по округам' WHERE `id`='65';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по регионам', `position`='2' WHERE `id`='68';
            UPDATE {{charttable}} SET `name`='Объем ресурсов по округам', `groupId`='21', `position`='1' WHERE `id`='66';
            UPDATE {{charttable}} SET `name`='Добыча нефти и газа по округам', `groupId`='21', `position`='3' WHERE `id`='67';
            UPDATE {{charttable}} SET `name`='Объем ресурсов по регионам', `groupId`='21', `position`='2' WHERE `id`='69';
            UPDATE {{charttable}} SET `name`='Экспорт/импорт горюче-смазочных материалов по округам', `groupId`='22', `position`='2' WHERE `id`='70';
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Добыча нефти и газа по регионам', '3', '37', 'region', '21', '4');
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Экспорт/импорт горюче-смазочных материалов по округам', '2', '37', 'state', '22', '1');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e452577d89702498c346f', '', '4', '2015', '84', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e452577d89702498c346f', 'Добыча нефти', '2015', '84', '#a0d468', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e43f477d89702498b771f', '', '4', '2015', '84', '#43ade3', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e43f477d89702498b771f', 'Добыча газа', '2015', '84', '#a0d468', '4');
            
            UPDATE {{charttable}} SET `name`='Экспорт/импорт горюче-смазочных материалов по регионам' WHERE `id`='70';
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b789bd4c56dc4f8b6f74', 'Вывоз (продажа) автомобильных бензинов', '', '6', '2015', '85', '#43ade3', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b781bd4c56dc4f8b5d4c', 'Ввоз (покупка) автомобильных бензинов', '', '6', '2015', '85', '#4ec882', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b789bd4c56dc4f8b6f76', 'Вывоз (продажа) дизельного топлива', '', '6', '2015', '85', '#43ade3', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b781bd4c56dc4f8b5d4e', 'Ввоз (покупка) дизельного топлива', '', '6', '2015', '85', '#4ec882', '4');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b789bd4c56dc4f8b6f78', 'Вывоз (продажа) мазута', '', '6', '2015', '85', '#43ade3', '5');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b781bd4c56dc4f8b5d50', 'Ввоз (покупка) мазута', '', '6', '2015', '85', '#4ec882', '6');
            
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`, `industryId`) VALUES ('379', 'Нефть', '1', '37');
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`, `industryId`) VALUES ('379', 'Газы', '2', '37');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('16', '68805');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('16', '68806');
            
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('9', '500061', 'Доллар США');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('10', '500063', 'Доллар США');
            
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('ТН ВЭД', '3', '37', 'country', '23', '1');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('9', 'Экспорт РФ', '2015', '86', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('10', 'Импорт РФ', '2015', '86', '#a0d468', '2');
            
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500065', '2711', 'Экспорт', 'derivative', 'ДолларСША', '1');
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500067', '2711', 'Импорт', 'derivative', 'ДолларСША', '1');
            
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500065', 'aggregatable', 'SUM');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500067', 'aggregatable', 'SUM');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','1','5745eb7f74d897cb088b657f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','2','5745eb7f74d897cb088b658b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','3','5745ec9574d897cb088e4b2c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','4','5745eb7f74d897cb088b6587');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','5','5745eb7f74d897cb088b6583');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','6','5745eb7f74d897cb088b6597');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','7','5745ebc074d897cb088c363f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','8','5745eb7f74d897cb088b6593');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','9','5745eb7f74d897cb088b658f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','10','5745eb7f74d897cb088b65c3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','11','5745eb7f74d897cb088b65a3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','12','5745eb7f74d897cb088b65bf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','13','5745eb7f74d897cb088b65bb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','14','5745eb7f74d897cb088b65a7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','15','5745ec9574d897cb088e4b48');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','16','5745eda274d897cb0890b06d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','17','5745eb7f74d897cb088b65c7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','18','5745eb7f74d897cb088b65db');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','19','5745eec674d897cb089341d9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','20','5745eb7f74d897cb088b65cf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','21','5745ebc074d897cb088c3649');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','22','5745eb7f74d897cb088b65d3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','23','5745eb7f74d897cb088b65d7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','24','5745eb7f74d897cb088b65e3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','25','5745ec9574d897cb088e4b64');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','26','5745eec674d897cb08934205');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','27','5745eb7f74d897cb088b65df');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','28','5745ee7c74d897cb089299d1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','29','5745eb7f74d897cb088b65e7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','30','5745eb7f74d897cb088b65eb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','31','5745eb7f74d897cb088b65ef');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','32','5745ec9574d897cb088e4b76');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','33','5745eb7f74d897cb088b65f7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','34','5745ec9574d897cb088e4b6e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','35','5745eb7f74d897cb088b65fb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','36','5745eb7f74d897cb088b65ff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','37','5745eb7f74d897cb088b6603');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','38','5745eb7f74d897cb088b65b7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','39','5745eb7f74d897cb088b6607');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','40','5745edeb74d897cb08915594');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','41','5745edeb74d897cb0891558e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','42','5745edeb74d897cb0891558a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','43','5745eb7f74d897cb088b660b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','44','5745ec9574d897cb088e4b8c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','45','5745eb7f74d897cb088b660f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','46','5745eb7f74d897cb088b65af');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','47','5745eb7f74d897cb088b65b3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','48','5745eb7f74d897cb088b659f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','49','5745eb7f74d897cb088b659b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','50','5745ebc074d897cb088c367f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','51','5745eb7f74d897cb088b65ab');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','52','5745eb7f74d897cb088b65f3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('30','53','5745eb7f74d897cb088b65cb');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','1','5745ef9174d897cb08950d15');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','2','5745ef9174d897cb08950d19');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','3','5745f2f174d897cb089d11d5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','4','5745ef9174d897cb08950d21');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','5','5745f23374d897cb089b71fc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','6','5745f1d474d897cb089a9f77');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','7','5745eff574d897cb0896550c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','8','5745ef9174d897cb08950d2d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','9','5745ef9174d897cb08950d1d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','10','5745ef9174d897cb08950d29');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','11','5745eff574d897cb08965514');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','12','5745f17474d897cb0899cba6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','13','5745eff574d897cb08965508');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','14','5745ef9174d897cb08950d31');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','15','5745eff574d897cb08965518');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','16','5745ef9174d897cb08950d25');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','17','5745f1d474d897cb089a9f6f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('31','18','5745f0b674d897cb089820ea');
            
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500065', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500067', 'country');
            
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68807', '2016-09-25 18:10:16', '47698900000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68807', '2016-09-25 18:10:16', '9419290000', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68808', '2016-09-25 18:11:05', '190038000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68808', '2016-09-25 18:11:05', '46983200', '2016');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('17', '68807');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('17', '68808');
            
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('9', '500065', 'Доллар США');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('10', '500067', 'Доллар США');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}