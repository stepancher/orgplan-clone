<?php

class m160610_104937_add_fairtasksdependencies extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":18, "save":1}\', 	"calendar/gantt/add/id/126");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":18, "send":1}\', 	"calendar/gantt/add/id/126");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":18}\', 				"calendar/gantt/add/id/126");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":18, "send":1}\', 	"calendar/gantt/complete/id/126");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":18}\', 				"calendar/gantt/complete/id/126");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":18, "send":1}\', 	"calendar/gantt/complete/id/128");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":18}\', 				"calendar/gantt/complete/id/128");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":18, "send":1}\', 	"calendar/gantt/complete/id/130");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":18}\', 				"calendar/gantt/complete/id/130");

			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":20, "save":1}\', 	"calendar/gantt/add/id/132");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":20, "send":1}\', 	"calendar/gantt/add/id/132");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":20}\', 				"calendar/gantt/add/id/132");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":20, "send":1}\', 	"calendar/gantt/complete/id/132");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":20}\', 				"calendar/gantt/complete/id/132");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":20, "send":1}\', 	"calendar/gantt/complete/id/134");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":20}\', 				"calendar/gantt/complete/id/134");

			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":19, "save":1}\', 	"calendar/gantt/add/id/136");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":19, "send":1}\', 	"calendar/gantt/add/id/136");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":19}\', 				"calendar/gantt/add/id/136");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":19, "send":1}\', 	"calendar/gantt/complete/id/136");
			INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":19}\', 				"calendar/gantt/complete/id/136");
		';
	}
}

/*
 	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":12, "save":1}\', 	"calendar/gantt/add/id/137");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":12, "send":1}\', 	"calendar/gantt/add/id/137");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":12}\', 				"calendar/gantt/add/id/137");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":12, "send":1}\',	"calendar/gantt/complete/id/137");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":12}\', 				"calendar/gantt/complete/id/137");

	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":12a, "save":1}\', 	"calendar/gantt/add/id/138");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":12a, "send":1}\', 	"calendar/gantt/add/id/138");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":12a}\', 			"calendar/gantt/add/id/138");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":12a, "send":1}\', 	"calendar/gantt/complete/id/138");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":12a}\', 			"calendar/gantt/complete/id/138");


	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":7, "save":1}\', 	"calendar/gantt/add/id/139");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":7, "send":1}\', 	"calendar/gantt/add/id/139");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":7}\', 				"calendar/gantt/add/id/139");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":7, "send":1}\', 	"calendar/gantt/complete/id/139");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":7}\', 				"calendar/gantt/complete/id/139");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":7, "send":1}\', 	"calendar/gantt/complete/id/141");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":7}\', 				"calendar/gantt/complete/id/141");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":7, "send":1}\', 	"calendar/gantt/complete/id/143");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":7}\', 				"calendar/gantt/complete/id/143");

	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":8, "save":1}\', 	"calendar/gantt/add/id/145");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":8, "send":1}\', 	"calendar/gantt/add/id/145");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":8}\', 				"calendar/gantt/add/id/145");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":8, "send":1}\', 	"calendar/gantt/complete/id/145");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":8}\', 				"calendar/gantt/complete/id/145");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":8, "send":1}\', 	"calendar/gantt/complete/id/147");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":8}\', 				"calendar/gantt/complete/id/147");

	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":9a, "save":1}\', 	"calendar/gantt/add/id/149");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":9a, "send":1}\', 	"calendar/gantt/add/id/149");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":9a}\', 				"calendar/gantt/add/id/149");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":9a, "send":1}\', 	"calendar/gantt/complete/id/149");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":9a}\', 				"calendar/gantt/complete/id/149");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":9a, "send":1}\', 	"calendar/gantt/complete/id/151");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":9a}\', 				"calendar/gantt/complete/id/151");

	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":13, "save":1}\', 	"calendar/gantt/add/id/153");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":13, "send":1}\', 	"calendar/gantt/add/id/153");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":13}\', 				"calendar/gantt/add/id/153");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/create", 	\'{"fairId":184, "ecId":13, "send":1}\', 	"calendar/gantt/complete/id/153");
	INSERT INTO {{dependences}} (action, params, run) VALUES( "proposal/proposal/send", 	\'{"fairId":184, "ecId":13}\', 				"calendar/gantt/complete/id/153");
*/