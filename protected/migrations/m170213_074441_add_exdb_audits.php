<?php

class m170213_074441_add_exdb_audits extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{exdbaudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `active` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            CREATE TABLE {{exdbtraudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `langId` VARCHAR(55) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            INSERT INTO {{exdbaudit}} (`active`) VALUES (NULL);
            
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('1', 'RUEF - Russian Union of Exhibitions & Fairs', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('2', 'CLC-VECTA Centrum voor Live Communication', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('3', 'AFE - Asociación de Ferias Españolas', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('4', 'ABC - Audit Bureau of Circulations Ltd.', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('5', 'CENTREX - International Exhibition Statistics Union', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('6', 'FKM - Gesellschaft zur freiwilligen Kontrolle von Messe- und Ausstellungszahlen', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('7', 'FKM-Austria - Verein zur freiwilligen Kontrolle von Messezahlen, c/o Reed Exhibitions Messe Wien', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('8', 'OJS - Office de Justification des Statistiques', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('9', 'SFC - Scandinavian Fair Control', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('10', 'MediaAuditFinland', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('11', 'CAB - Circulations Audit Board', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('12', 'RussCom IT Systems', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('13', 'ISFCERT Srl - Istituto di Certificazione dei Dati Statistici Fieristici', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('14', 'BPA Worldwide', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('15', 'EXPOCERT', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('16', 'Thailand Convention & Exhibition Bureau (TCEB)', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('17', 'ITFCC - Inter-Regional Trade Fair Coordination Committee, c/o Regione Emilia-Romagna', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('18', 'MOTIE - Ministry of Trade, Industry and Energy', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('19', 'UCCET - The Union of Chambers and Commodity Exchanges of Turkey', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('20', 'TREVOR - Revisione e organizzazione contabile', 'ru');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('21', 'Japan Exhibition Certification Council - JECC', 'ru');
            
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('1', 'RUEF - Russian Union of Exhibitions & Fairs', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('2', 'CLC-VECTA Centrum voor Live Communication', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('3', 'AFE - Asociación de Ferias Españolas', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('4', 'ABC - Audit Bureau of Circulations Ltd.', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('5', 'CENTREX - International Exhibition Statistics Union', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('6', 'FKM - Gesellschaft zur freiwilligen Kontrolle von Messe- und Ausstellungszahlen', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('7', 'FKM-Austria - Verein zur freiwilligen Kontrolle von Messezahlen, c/o Reed Exhibitions Messe Wien', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('8', 'OJS - Office de Justification des Statistiques', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('9', 'SFC - Scandinavian Fair Control', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('10', 'MediaAuditFinland', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('11', 'CAB - Circulations Audit Board', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('12', 'RussCom IT Systems', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('13', 'ISFCERT Srl - Istituto di Certificazione dei Dati Statistici Fieristici', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('14', 'BPA Worldwide', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('15', 'EXPOCERT', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('16', 'Thailand Convention & Exhibition Bureau (TCEB)', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('17', 'ITFCC - Inter-Regional Trade Fair Coordination Committee, c/o Regione Emilia-Romagna', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('18', 'MOTIE - Ministry of Trade, Industry and Energy', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('19', 'UCCET - The Union of Chambers and Commodity Exchanges of Turkey', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('20', 'TREVOR - Revisione e organizzazione contabile', 'en');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('21', 'Japan Exhibition Certification Council - JECC', 'en');
            
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('1', 'RUEF - Russian Union of Exhibitions & Fairs', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('2', 'CLC-VECTA Centrum voor Live Communication', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('3', 'AFE - Asociación de Ferias Españolas', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('4', 'ABC - Audit Bureau of Circulations Ltd.', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('5', 'CENTREX - International Exhibition Statistics Union', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('6', 'FKM - Gesellschaft zur freiwilligen Kontrolle von Messe- und Ausstellungszahlen', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('7', 'FKM-Austria - Verein zur freiwilligen Kontrolle von Messezahlen, c/o Reed Exhibitions Messe Wien', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('8', 'OJS - Office de Justification des Statistiques', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('9', 'SFC - Scandinavian Fair Control', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('10', 'MediaAuditFinland', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('11', 'CAB - Circulations Audit Board', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('12', 'RussCom IT Systems', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('13', 'ISFCERT Srl - Istituto di Certificazione dei Dati Statistici Fieristici', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('14', 'BPA Worldwide', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('15', 'EXPOCERT', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('16', 'Thailand Convention & Exhibition Bureau (TCEB)', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('17', 'ITFCC - Inter-Regional Trade Fair Coordination Committee, c/o Regione Emilia-Romagna', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('18', 'MOTIE - Ministry of Trade, Industry and Energy', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('19', 'UCCET - The Union of Chambers and Commodity Exchanges of Turkey', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('20', 'TREVOR - Revisione e organizzazione contabile', 'de');
            INSERT INTO {{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES ('21', 'Japan Exhibition Certification Council - JECC', 'de');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}