<?php

class m160517_065636_delete_tbl_searchQuery_from_mainDb extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			CREATE TABLE {{searchquery}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `userId` int(11) DEFAULT NULL,
			  `date` datetime DEFAULT NULL,
			  `text` text,
			  `userIp` varchar(255) DEFAULT NULL,
			  `referer` varchar(255) DEFAULT NULL,
			  `params` varchar(255) DEFAULT NULL,
			  `context` int(11) DEFAULT NULL,
			  `filterName` varchar(255) DEFAULT NULL,
			  `filterValue` varchar(255) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=243540 DEFAULT CHARSET=utf8;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			DROP TABLE IF EXISTS {{searchquery}};
		';
	}
}