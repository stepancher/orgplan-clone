<?php

class m160804_072615_add_name_to_vc_195 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('195', 'en', 'ДВОРЕЦ КУЛЬТУРЫ', 'Фронтовых бригад ул., д. 6', 'ДВОРЕЦ КУЛЬТУРЫ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('195', 'de', 'ДВОРЕЦ КУЛЬТУРЫ', 'Фронтовых бригад ул., д. 6', 'ДВОРЕЦ КУЛЬТУРЫ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('195', 'ru', 'ДВОРЕЦ КУЛЬТУРЫ', 'Фронтовых бригад ул., д. 6', 'ДВОРЕЦ КУЛЬТУРЫ');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}