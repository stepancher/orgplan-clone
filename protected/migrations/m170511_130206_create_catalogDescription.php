<?php

class m170511_130206_create_catalogDescription extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{catalogdescription}} (
              `id` INT NOT NULL AUTO_INCREMENT,
              `industryId` INT NULL,
              `countryId` INT NULL,
              `regionId` INT NULL,
              `year` INT NULL,
              PRIMARY KEY (`id`),
              INDEX `DESC_UNIQ` (`industryId` ASC, `countryId` ASC, `regionId` ASC, `year` ASC));

            CREATE TABLE {{trcatalogdescription}} (
              `id` INT NOT NULL AUTO_INCREMENT,
              `trParentId` INT NULL,
              `langId` VARCHAR(45) NULL,
              `header` VARCHAR(1023) NULL,
              `text` VARCHAR(1023) NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_trcatalogdescription_1_idx` (`trParentId` ASC),
              CONSTRAINT `fk_trcatalogdescription_1`
                FOREIGN KEY (`trParentId`)
                REFERENCES {{catalogdescription}} (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE);
              
                INSERT INTO {{catalogdescription}} (`countryId`) VALUES ('16');
                INSERT INTO {{trcatalogdescription}} (`trParentId`, `langId`) VALUES ('1', 'ru');
                UPDATE {{trcatalogdescription}} SET `header`='Выставки Австралии – эффективная помощь в развитии экономики страны', `text`='Австралия - страна с особым географическим положением, богатейшими природными ресурсами, стабильной политической обстановкой, огромным экономическим потенциалом. После освобождения от колонизации Великобританией государство быстро превратилось в индустриально-аграрную страну западного типа с высоким научно-техническим потенциалом, высокоразвитой многоотраслевой экономикой. \n\nСейчас Австралия – одна из держав, занимающих на аграрном и промышленном мировых рынках ведущие позиции. Но развитию страны мешают неблагоприятные факторы: частые засухи, низкое плодородие почв в некоторых регионах материка, трудности в развитии промышленности. Для поддержания роста производственных объемов требуется импортное сырье, внедрение новых технологий и грамотно сбалансированная экономика. \n\nОдин из наиболее эффективных способов преодоления трудностей в развитии экономики страны – проведение выставок в Австралии. Они: \nпредставляют новые разработки в отраслях хозяйства;\nзнакомят с новыми технологиями и способствуют их внедрению в практическую работу промышленных и сельскохозяйственных предприятий;\nпривлекают международных инвесторов;\nрасширяют деловые связи на межрегиональном и международном уровнях;\nпомогают скорректировать развитие бизнеса в соответствии с требованиями рынка;\nраздвигают границы торговых отношений.  ' WHERE `id`='1';
		";
    }

    public function downSql()
    {
        return "
          DROP TABLE {{trcatalogdescription}};
          DROP TABLE {{catalogdescription}};
        ";
    }
}