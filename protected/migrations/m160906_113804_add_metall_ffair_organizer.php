<?php

class m160906_113804_add_metall_ffair_organizer extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql(){

        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            UPDATE {{user}} SET `role`='7' WHERE `id`='823';
            UPDATE {{myfair}} SET `fairId`='10943' WHERE `id`='69';
            UPDATE {{fair}} SET `shard`='dbProposalF10943' WHERE `id`='10943';
            UPDATE {{user}} SET `password`='\$2y\$10\$V4Jau36q3F9NuW10bjK7auq0yGqM0FUrUUGshmVNzHF3lkGGhmgM6' WHERE `id`='823';
            INSERT INTO {{userhasorganizer}} (`organizerId`, `userId`) VALUES ('3460', '823');
            DELETE FROM {$dbProposalF10943}.tbl_organizerexponent WHERE `id`='283';
            DELETE FROM {$dbProposalF10943}.tbl_organizerexponent WHERE `id`='284';
            DELETE FROM {$dbProposalF10943}.tbl_organizerexponent WHERE `id`='286';
            DELETE FROM {$dbProposalF10943}.tbl_organizerexponent WHERE `id`='287';
            delete from {$dbProposalF10943}.tbl_proposalelement;

            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='1';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='2';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='3';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='4';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='5';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='7';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='10';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='18';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='19';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='20';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='21';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='22';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='23';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='24';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='25';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='26';
            UPDATE {$dbProposalF10943}.tbl_proposalelementclass SET `fairId`='10943' WHERE `id`='27';
            
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки, гарантирует оплату заказанных услуг и разрешает Организатору использование логотипа Экспонента в рекламной кампании выставки Металлообработка.\n                        <br/><br/>\n                        Заявка является офертой и должна быть заполнена, подписана Экспонентом и направлена Организатору (2экз.) Подписывая настоящую заявку, Экспонент подтверждает принятие им условий Договора на участие в выставке Металлообработка, размещенного по адресу www.expoforum.ru и его заключение с Организатором. Подписанный Договор Экспонент обязуется направить Организатору в течении пяти дней.' WHERE `id`='146';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='167';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 мая 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='174';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Конкурсная заявка должна быть заполнена на двух языках, русском и английском. К заявке обязательно должны быть приложены: <br/>1. Не менее четырех фотографий номинируемого инновационного продукта (изделия) <br/>2. Описание сути и ключевых особенностей инновации (около 5000 знаков) на русском и английском языках.\r\n<br/><br/>\r\nДополнительно: желательно предоставление протокола испытаний при его наличии.\r\n<br/><br/>\r\nНе полностью заполненные Конкурсные заявки и заявки без необходимых приложений к рассмотрению не принимаются.\r\n<br/><br/>\r\nВсе номинируемые на конкурс изделия обязательно должны быть представлены на выставке Металлообработка 2016.' WHERE `id`='177';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='183';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='204';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='255';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки, гарантирует оплату заказанных услуг и разрешает Организатору использование логотипа Экспонента в рекламной кампании выставки Металлообработка.\n                        <br/><br/>\n                        Заявка является офертой и должна быть заполнена, подписана Экспонентом и направлена Организатору (2экз.) Подписывая настоящую заявку, Экспонент подтверждает принятие им условий Договора на участие в выставке Металлообработка, размещенного по адресу www.expoforum.ru и его заключение с Организатором. Подписанный Договор Экспонент обязуется направить Организатору в течении пяти дней.' WHERE `id`='146';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='167';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 мая 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='174';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Конкурсная заявка должна быть заполнена на двух языках, русском и английском. К заявке обязательно должны быть приложены: <br/>1. Не менее четырех фотографий номинируемого инновационного продукта (изделия) <br/>2. Описание сути и ключевых особенностей инновации (около 5000 знаков) на русском и английском языках.\r\n<br/><br/>\r\nДополнительно: желательно предоставление протокола испытаний при его наличии.\r\n<br/><br/>\r\nНе полностью заполненные Конкурсные заявки и заявки без необходимых приложений к рассмотрению не принимаются.\r\n<br/><br/>\r\nВсе номинируемые на конкурс изделия обязательно должны быть представлены на выставке Металлообработка 2016.' WHERE `id`='177';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='183';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='204';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='255';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 24 августа 2016 г. Факс: +7(812)240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='433';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='При погрузочно-разгрузочных работах на территории выставочного комплекса может использоваться только грузоподъемная техника, предоставленная ЦВК Экспоцентр.' WHERE `id`='484';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='При погрузочно-разгрузочных работах на территории выставочного комплекса может использоваться только грузоподъемная техника, предоставленная ЦВК Экспоцентр.' WHERE `id`='505';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='520';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 25, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1024';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки, гарантирует оплату заказанных услуг и разрешает Организатору использование логотипа Экспонента в рекламной кампании выставки Металлообработка.\n                        <br/><br/>\n                        Заявка является офертой и должна быть заполнена, подписана Экспонентом и направлена Организатору (2экз.) Подписывая настоящую заявку, Экспонент подтверждает принятие им условий Договора на участие в выставке Металлообработка, размещенного по адресу www.expoforum.ru и его заключение с Организатором. Подписанный Договор Экспонент обязуется направить Организатору в течении пяти дней.' WHERE `id`='146';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='167';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 мая 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='174';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Конкурсная заявка должна быть заполнена на двух языках, русском и английском. К заявке обязательно должны быть приложены: <br/>1. Не менее четырех фотографий номинируемого инновационного продукта (изделия) <br/>2. Описание сути и ключевых особенностей инновации (около 5000 знаков) на русском и английском языках.\r\n<br/><br/>\r\nДополнительно: желательно предоставление протокола испытаний при его наличии.\r\n<br/><br/>\r\nНе полностью заполненные Конкурсные заявки и заявки без необходимых приложений к рассмотрению не принимаются.\r\n<br/><br/>\r\nВсе номинируемые на конкурс изделия обязательно должны быть представлены на выставке Металлообработка 2016.' WHERE `id`='177';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='183';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='204';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='255';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 24 августа 2016 г. Факс: +7(812)240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='433';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='При погрузочно-разгрузочных работах на территории выставочного комплекса может использоваться только грузоподъемная техника, предоставленная ЦВК Экспоцентр.' WHERE `id`='484';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='При погрузочно-разгрузочных работах на территории выставочного комплекса может использоваться только грузоподъемная техника, предоставленная ЦВК Экспоцентр.' WHERE `id`='505';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='520';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 25, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1024';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Will be showcased on Agrorus 2016' WHERE `id`='1118';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This contest application must be filled in two languages (English and Russian). The contest application must be enclosed with: <br/>1. Nominated product photos (not less than four photos). <br/>2. Innovative description in Russian and English (about 5000 symbols). <br/> <br/>In addition: please, submit the engineering test record if it is involved. <br/> <br/>Thank you for your understanding. Any incomplete application forms or forms without attachments will not be processed. <br/> <br/>All products nominated for contest must be exhibited at AGRORUS 2016.' WHERE `id`='1133';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 24, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1190';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 25, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1235';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='' WHERE `id`='1410';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 19 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1424';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 19, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1493';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 18 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1562';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n<br><br>\nПодписывая настоящую заявку стороны подтверждают свое согласие со следующими условиями Организатор берет на себя все расходы, связанные с арендой конференц-залов и заявленного оборудования для проведения мероприятия. Организатор анонсирует мероприятие в СМИ и на рекламно-информационных носителях , а также ведет привлечение посетителей. Экспонент берет на себя все расходы, связанные с формированием программы мероприятия, и привлечением докладчиков и участников.\n<br><br>\nОрганизатор оставляет за собой право отказать экспоненту в проведении мероприятия, если его программа будет изменена или тема мероприятия не будет соответствовать тематике выставки Металлообработка.\n<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='1603';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1996';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 18 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2154';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 18, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2199';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application the parties confirm their agreement with the following conditions: the Organizer takes all the costs related to the lease of conference halls and equipment declared for realization of the event. The Organizer announces the event in mass-media and by advertising and information carries, and also attracts visitors. The Exhibitors takes all the costs related to the formation of the event program and invitation of reporters and participants.<br/><br/>The Organizer reserves the right to cancel the exhibitor\'s event, if the program is changed or the event topic doesn\'t correspond with the topics of \"AGRORUS\" exhibition.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='2241';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 18, 2016. Please return the application form by fax +7(812)-240-40-40 or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2253';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2294';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 23, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2295';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 20 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2308';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 25, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1235';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='' WHERE `id`='1410';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 19 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1424';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 19, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1493';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 18 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1562';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n<br><br>\nПодписывая настоящую заявку стороны подтверждают свое согласие со следующими условиями Организатор берет на себя все расходы, связанные с арендой конференц-залов и заявленного оборудования для проведения мероприятия. Организатор анонсирует мероприятие в СМИ и на рекламно-информационных носителях , а также ведет привлечение посетителей. Экспонент берет на себя все расходы, связанные с формированием программы мероприятия, и привлечением докладчиков и участников.\n<br><br>\nОрганизатор оставляет за собой право отказать экспоненту в проведении мероприятия, если его программа будет изменена или тема мероприятия не будет соответствовать тематике выставки Металлообработка.\n<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='1603';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1996';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 18 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2154';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 18, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2199';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application the parties confirm their agreement with the following conditions: the Organizer takes all the costs related to the lease of conference halls and equipment declared for realization of the event. The Organizer announces the event in mass-media and by advertising and information carries, and also attracts visitors. The Exhibitors takes all the costs related to the formation of the event program and invitation of reporters and participants.<br/><br/>The Organizer reserves the right to cancel the exhibitor\'s event, if the program is changed or the event topic doesn\'t correspond with the topics of \"AGRORUS\" exhibition.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='2241';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 18, 2016. Please return the application form by fax +7(812)-240-40-40 or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2253';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2294';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 23, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2295';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 20 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2308';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='VIP бейдж дает право посещения выставки Металлообработка и всех мероприятий деловой программы. <br><br> Количество VIP бейджей, предоставляемых Экспоненту зависит от площади его стенда и определяется следующим образом: <br> Стенд до 100м² - 1 шт. <br> Стенд от 100м² до 500м² - 2 шт. <br> Стенд более 500м² - 3 шт. ' WHERE `id`='2309';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполенную заявку в дирекцию выставки не позднее 15 сентября 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2400';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 20, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2451';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='VIP badge entitles visiting the exhibition \"AGRORUS\" and all the activities of the business program.<br><br>Available number of VIP badges depends on the area of Exhibitor\'s stand. It\'s defined as follows: <br> stand up to 100 m² - 1 pcs. <br> Stand from 100sq.m. up to 500sq.m. - 2 pcs. <br> Stand more than 500sq.m. - 3 pcs.' WHERE `id`='2452';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until Sept. 15, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2539';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 22 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2588';
            DELETE FROM {$dbProposalF10943}.tbl_trattributeclass WHERE `id`='2590';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 25, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1235';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='' WHERE `id`='1410';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 19 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1424';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 19, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1493';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отпрвьте заполненную форму в дирекцию выставку не позднее 18 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1562';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n<br><br>\nПодписывая настоящую заявку стороны подтверждают свое согласие со следующими условиями Организатор берет на себя все расходы, связанные с арендой конференц-залов и заявленного оборудования для проведения мероприятия. Организатор анонсирует мероприятие в СМИ и на рекламно-информационных носителях , а также ведет привлечение посетителей. Экспонент берет на себя все расходы, связанные с формированием программы мероприятия, и привлечением докладчиков и участников.\n<br><br>\nОрганизатор оставляет за собой право отказать экспоненту в проведении мероприятия, если его программа будет изменена или тема мероприятия не будет соответствовать тематике выставки Металлообработка.\n<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='1603';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='1996';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 18 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2154';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 18, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2199';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application the parties confirm their agreement with the following conditions: the Organizer takes all the costs related to the lease of conference halls and equipment declared for realization of the event. The Organizer announces the event in mass-media and by advertising and information carries, and also attracts visitors. The Exhibitors takes all the costs related to the formation of the event program and invitation of reporters and participants.<br/><br/>The Organizer reserves the right to cancel the exhibitor\'s event, if the program is changed or the event topic doesn\'t correspond with the topics of \"AGRORUS\" exhibition.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='2241';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 18, 2016. Please return the application form by fax +7(812)-240-40-40 or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2253';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2294';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 23, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2295';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 20 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2308';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='VIP бейдж дает право посещения выставки Металлообработка и всех мероприятий деловой программы. <br><br> Количество VIP бейджей, предоставляемых Экспоненту зависит от площади его стенда и определяется следующим образом: <br> Стенд до 100м² - 1 шт. <br> Стенд от 100м² до 500м² - 2 шт. <br> Стенд более 500м² - 3 шт. ' WHERE `id`='2309';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполенную заявку в дирекцию выставки не позднее 15 сентября 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2400';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 20, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2451';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='VIP badge entitles visiting the exhibition \"AGRORUS\" and all the activities of the business program.<br><br>Available number of VIP badges depends on the area of Exhibitor\'s stand. It\'s defined as follows: <br> stand up to 100 m² - 1 pcs. <br> Stand from 100sq.m. up to 500sq.m. - 2 pcs. <br> Stand more than 500sq.m. - 3 pcs.' WHERE `id`='2452';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until Sept. 15, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2539';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 22 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2588';
            DELETE FROM {$dbProposalF10943}.tbl_trattributeclass WHERE `id`='2590';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 22, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2678';
            DELETE FROM {$dbProposalF10943}.tbl_trattributeclass WHERE `id`='2680';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 4 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2776';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until Aug. 04, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='2899';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 17 сентября 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='3011';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Норматив времени нахождения транспорта в зоне погрузки-разгрузки: для легковых автомобилей — 0,5 часа, для грузовых автомобилей — 1 час. При превышении норматива необходимы повторный заказ и оплата пропуска.\r\n\r\nДля проезда в зону разгрузки-погрузки транспортных средств, в отношении которых заказана разгрузка/погрузка силами ЦВК Экспоцентр (заполнена и оплачена заявка №9) необходимо получить БЕСПЛАТНЫЙ пропуск для получения которого представителю компании необходимо обратиться в сервис-центр ЦВК Экспоцентр не позже чем за один час до начала работ в соответствии с графиком разгрузки погрузки.' WHERE `id`='3019';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='До 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ заказываются у Организатора.\r\nПосле 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ могут быть приобретены как у Организатора, так и через сервис-центр ЦВК Экспоцентр. При приобретении пропусков через сервис-центр их стоимость определяется непосредственно ЦВК Экспоцентр. Для оформления пропусков необходимо указать тип и государственный номер транспортного средства.' WHERE `id`='3037';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 17 сентября 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='3059';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Норматив времени нахождения транспорта в зоне погрузки-разгрузки: для легковых автомобилей — 0,5 часа, для грузовых автомобилей — 1 час. При превышении норматива необходимы повторный заказ и оплата пропуска.\r\n\r\nДля проезда в зону разгрузки-погрузки транспортных средств, в отношении которых заказана разгрузка/погрузка силами ЦВК Экспоцентр (заполнена и оплачена заявка №9) необходимо получить БЕСПЛАТНЫЙ пропуск для получения которого представителю компании необходимо обратиться в сервис-центр ЦВК Экспоцентр не позже чем за один час до начала работ в соответствии с графиком разгрузки погрузки.' WHERE `id`='3066';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='До 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ заказываются у Организатора.\r\nПосле 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ могут быть приобретены как у Организатора, так и через сервис-центр ЦВК Экспоцентр. При приобретении пропусков через сервис-центр их стоимость определяется непосредственно ЦВК Экспоцентр. Для оформления пропусков необходимо указать тип и государственный номер транспортного средства.' WHERE `id`='3083';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='3087';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 26 августа 2016 г. Факс: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru' WHERE `id`='3115';
            UPDATE {$dbProposalF10943}.tbl_trattributeclass SET `label`='This form should be filled in until August 26, 2016. Please return the application form by fax +7(812)-240-40-40, or e-mail: LebedevaUE@expocentr.ru' WHERE `id`='3135';
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
           
            INSERT INTO {{calendarfairtasks}} VALUES 
(401,10943,'23755881984295336',NULL,'23755881984295336',NULL,'2015-12-23 23:59:59','',0,1),
(402,10943,'23755881984295337','23755881984295336','23755881984295337',10,NULL,NULL,1,0),
(403,10943,'23755881984295338','23755881984295337','23755881984295338',10,NULL,'#f6a800 ',1,0),
(404,10943,'23755881984295339','23755881984295338','23755881984295339',15,'2016-04-16 00:00:00','#f6a800 ',1,0),
(405,10943,'23755881984295340','23755881984295339','23755881984295340',17,'2016-07-15 00:00:00','#f6a800 ',1,0),
(406,10943,'23755881984295341',NULL,'23755881984295336',99,'2015-12-24 00:00:00','',0,0),
(407,10943,'23755881984295342','23755881984295341','23755881984295342',10,NULL,NULL,1,0),
(408,10943,'23755881984295343','23755881984295342','23755881984295343',10,NULL,'#f6a800 ',1,0),
(409,10943,'23755881984295344','23755881984295343','23755881984295344',17,'2016-07-15 00:00:00','#f6a800 ',1,0),
(410,10943,'23755881984295345',NULL,'23755881984295336',120,'2016-04-01 00:00:00','',0,0),
(411,10943,'23755881984295346','23755881984295345','23755881984295346',10,NULL,NULL,1,0),
(416,10943,'23755881984295347','23755881984295357','23755881984295347',5,NULL,'#f96a0e',1,0),
(420,10943,'23755881984295348',NULL,'23755881984295348',NULL,'2016-08-25 23:59:59','',0,0),
(421,10943,'23755881984295349','23755881984295348','23755881984295349',10,NULL,'#f6a800',1,0),
(422,10943,'23755881984295350',NULL,'23755881984295348',29,'2016-08-26 00:00:00','',0,0),
(423,10943,'23755881984295351','23755881984295350','23755881984295351',5,NULL,'#f6a800',1,0),
(424,10943,'23755881984295352',NULL,'23755881984295348',10,'2016-09-24 00:00:00','',0,0),
(425,10943,'23755881984295353','23755881984295352','23755881984295353',3,NULL,'#f96a0e',1,0),
(426,10943,'23755881984295354','23755881984295345','23755881984295354',NULL,'2016-06-30 23:59:59','#f6a800',1,0),
(427,10943,'23755881984295355','23755881984295354','23755881984295355',NULL,'2016-07-29 23:59:59','#f96a0e',1,0),
(428,10943,'23755881984295356','23755881984295345','23755881984295354',15,'2016-07-01 00:00:00','#f6a800 ',1,0),
(429,10943,'23755881984295357',NULL,'23755881984295336',33,'2016-07-30 00:00:00','',0,0),
(431,10943,'23755881984295358',NULL,'23755881984295358',NULL,'2016-08-25 23:59:00','',0,0),
(432,10943,'23755881984295359','23755881984295358','23755881984295359',10,NULL,'#f6a800 ',1,0),
(433,10943,'23755881984295360',NULL,'23755881984295358',29,'2016-08-26 00:00:00','',0,0),
(434,10943,'23755881984295361','23755881984295360','23755881984295361',5,NULL,'#f6a800 ',1,0),
(435,10943,'23755881984295362',NULL,'23755881984295358',10,'2016-09-24 00:00:00','',0,0),
(436,10943,'23755881984295363','23755881984295362','23755881984295363',3,NULL,'#f96a0e',1,0),
(437,10943,'23755881984295364',NULL,'23755881984295364',NULL,'2016-08-23 23:59:00','',0,0),
(438,10943,'23755881984295365','23755881984295364','23755881984295365',10,NULL,'#f6a800',1,0),
(439,10943,'23755881984295366',NULL,'23755881984295364',29,'2016-08-24 00:00:00','',0,0),
(440,10943,'23755881984295367','23755881984295366','23755881984295367',5,NULL,'#f6a800 ',1,0),
(441,10943,'23755881984295368',NULL,'23755881984295364',12,'2016-09-22 00:00:00','',0,0),
(442,10943,'23755881984295369','23755881984295368','23755881984295369',3,NULL,'#f96a0e',1,0),
(443,10943,'23755881984295370',NULL,'23755881984295370',NULL,'2016-08-24 23:59:00','',0,0),
(444,10943,'23755881984295371','23755881984295370','23755881984295371',10,NULL,'#f6a800 ',1,0),
(445,10943,'23755881984295372',NULL,'23755881984295370',23,'2016-08-25 00:00:00','',0,0),
(446,10943,'23755881984295373','23755881984295372','23755881984295373',5,NULL,'#f96a0e',1,0),
(447,10943,'23755881984295374',NULL,'23755881984295370',17,'2016-09-17 00:00:00','',0,0),
(448,10943,'23755881984295375','23755881984295374','23755881984295375',3,NULL,'#f96a0e',1,0),
(449,10943,'23755881984295376',NULL,'23755881984295376',NULL,'2016-07-29 23:59:00','',0,0),
(462,10943,'23755881984295389',NULL,'23755881984295389',NULL,'2016-06-01 00:00:00','',0,0),
(463,10943,'23755881984295390','23755881984295356','23755881984295390',14,'2016-07-16 00:00:00','#f96a0e',1,0),
(464,10943,'23755881984295391','23755881984295345','23755881984295354',14,'2016-07-16 00:00:00','#f96a0e',1,0),
(521,10943,'23755881984295448','23755881984295376','23755881984295448',10,NULL,'#f6a800',1,0),
(522,10943,'23755881984295449',NULL,'23755881984295376',49,'2016-07-30 00:00:00','#43ade3',0,0),
(523,10943,'23755881984295450','23755881984295449','23755881984295450',5,NULL,'#f6a800',1,0),
(524,10943,'23755881984295451',NULL,'23755881984295376',17,'2016-09-17 00:00:00','#43ade3',0,0),
(525,10943,'23755881984295452','23755881984295451','23755881984295452',3,NULL,'#f96a0e',1,0),
(526,10943,'23755881984295453',NULL,'23755881984295453',NULL,'2016-08-19 23:59:59',NULL,0,0),
(527,10943,'23755881984295454','23755881984295453','23755881984295454',10,NULL,'#f6a800',1,0),
(528,10943,'23755881984295455',NULL,'23755881984295453',35,'2016-08-20 00:00:00',NULL,0,0),
(529,10943,'23755881984295456','23755881984295455','23755881984295456',5,NULL,'#f6a800',1,0),
(530,10943,'23755881984295457',NULL,'23755881984295453',10,'2016-09-24 00:00:00',NULL,0,0),
(531,10943,'23755881984295458','23755881984295457','23755881984295458',3,NULL,'#f96a0e',1,0),
(532,10943,'23755881984295459',NULL,'23755881984295459',NULL,'2016-08-18 23:59:59',NULL,0,0),
(533,10943,'23755881984295460','23755881984295459','23755881984295460',10,NULL,'#f6a800',1,0),
(534,10943,'23755881984295463',NULL,'23755881984295459',46,'2016-08-19 00:00:00',NULL,0,0),
(535,10943,'23755881984295464','23755881984295463','23755881984295464',3,NULL,'#f96a0e',1,0),
(536,10943,'23755881984295465',NULL,'23755881984295465',NULL,'2016-08-18 23:59:59',NULL,0,0),
(537,10943,'23755881984295471',NULL,'23755881984295471',NULL,'2016-09-15 23:59:59',NULL,0,0),
(538,10943,'23755881984295477',NULL,'23755881984295477',NULL,'2016-08-20 23:59:59',NULL,0,0),
(539,10943,'23755881984295478',NULL,'23755881984295478',NULL,'2016-08-22 23:59:59',NULL,0,0),
(540,10943,'23755881984295479','23755881984295478','23755881984295479',10,NULL,'#f6a800',1,0),
(543,10943,'23755881984295482',NULL,'23755881984295478',18,'2016-09-16 00:00:00',NULL,0,0),
(544,10943,'23755881984295483','23755881984295482','23755881984295483',3,NULL,'#f96a0e',1,0),
(545,10943,'23755881984295484',NULL,'23755881984295484',NULL,'2016-08-04 23:59:59',NULL,0,0),
(546,10943,'23755881984295485','23755881984295484','23755881984295485',10,NULL,'#f6a800',1,0),
(547,10943,'23755881984295486',NULL,'23755881984295484',22,'2016-09-05 00:00:00',NULL,0,0),
(548,10943,'23755881984295487','23755881984295486','23755881984295487',5,NULL,'#f6a800',1,0),
(549,10943,'23755881984295490',NULL,'23755881984295490',NULL,'2016-09-17 23:59:59',NULL,0,0),
(550,10943,'23755881984295491','23755881984295490','23755881984295491',10,NULL,'#f6a800',1,0),
(551,10943,'23755881984295492',NULL,'23755881984295490',29,'2016-09-18 00:00:00',NULL,0,0),
(552,10943,'23755881984295493','23755881984295492','23755881984295493',5,NULL,'#f6a800',1,0),
(553,10943,'23755881984295496',NULL,'23755881984295496',NULL,'2016-09-02 23:59:59',NULL,0,0);
                
 ALTER TABLE {{calendarfairtasks}} AUTO_INCREMENT = 554;
                
INSERT INTO {{dependences}} 
    (`action`, `params`, `run`)
VALUES 
    ('workspace/addFair',       '{\"id\":10943}',                                 'calendar/gantt/add/id/401'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":1, \"send\":1}',     'calendar/gantt/complete/id/401'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":1}',                 'calendar/gantt/complete/id/401'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":1, \"send\":1}',     'calendar/gantt/complete/id/406'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":1}',                 'calendar/gantt/complete/id/406'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":1, \"send\":1}',     'calendar/gantt/complete/id/410'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":1}',                 'calendar/gantt/complete/id/410'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":1, \"send\":1}',     'calendar/gantt/complete/id/429'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":1}',                 'calendar/gantt/complete/id/429'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":4, \"save\":1}',     'calendar/gantt/add/id/420'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":4, \"send\":1}',     'calendar/gantt/add/id/420'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":4}',                 'calendar/gantt/add/id/420'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":4, \"send\":1}',     'calendar/gantt/complete/id/420'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":4}',                 'calendar/gantt/complete/id/420'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":4, \"send\":1}',     'calendar/gantt/complete/id/422'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":4}',                   'calendar/gantt/complete/id/422'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":4, \"send\":1}',     'calendar/gantt/complete/id/424'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":4}',                   'calendar/gantt/complete/id/424'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":5, \"save\":1}',     'calendar/gantt/add/id/431'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":5, \"send\":1}',     'calendar/gantt/add/id/431'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":5}',                   'calendar/gantt/add/id/431'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":5, \"send\":1}',     'calendar/gantt/complete/id/431'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":5}',                   'calendar/gantt/complete/id/431'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":5, \"send\":1}',     'calendar/gantt/complete/id/433'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":5}',                   'calendar/gantt/complete/id/433'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":5, \"send\":1}',     'calendar/gantt/complete/id/435'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":5}',                   'calendar/gantt/complete/id/435'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":2, \"save\":1}',     'calendar/gantt/add/id/437'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":2, \"send\":1}',     'calendar/gantt/add/id/437'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":2}',                   'calendar/gantt/add/id/437'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":2, \"send\":1}',     'calendar/gantt/complete/id/437'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":2}',                   'calendar/gantt/complete/id/437'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":2, \"send\":1}',     'calendar/gantt/complete/id/439'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":2}',                   'calendar/gantt/complete/id/439'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":2, \"send\":1}',     'calendar/gantt/complete/id/441'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":2}',                   'calendar/gantt/complete/id/441'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":7, \"save\":1}',     'calendar/gantt/add/id/443'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":7, \"send\":1}',     'calendar/gantt/add/id/443'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":7}',                   'calendar/gantt/add/id/443'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":7, \"send\":1}',     'calendar/gantt/complete/id/443'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":7}',                   'calendar/gantt/complete/id/443'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":7, \"send\":1}',     'calendar/gantt/complete/id/445'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":7}',                   'calendar/gantt/complete/id/445'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":7, \"send\":1}',     'calendar/gantt/complete/id/447'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"save\":1}',    'calendar/gantt/add/id/449'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"save\":1}',    'calendar/gantt/add/id/522'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"save\":1}',    'calendar/gantt/add/id/524'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"send\":1}',    'calendar/gantt/add/id/521'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":10}',                  'calendar/gantt/add/id/521'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"send\":1}',    'calendar/gantt/complete/id/449'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":10}',                  'calendar/gantt/complete/id/449'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"send\":1}',    'calendar/gantt/add/id/523'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":10}',                  'calendar/gantt/add/id/523'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":10, \"send\":1}',    'calendar/gantt/add/id/525'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":10}',                  'calendar/gantt/add/id/525'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"save\":1}',     'calendar/gantt/add/id/449'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"save\":1}',     'calendar/gantt/add/id/450'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"save\":1}',     'calendar/gantt/add/id/456'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"send\":1}',     'calendar/gantt/add/id/449'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":8}',                   'calendar/gantt/add/id/449'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"send\":1}',     'calendar/gantt/add/id/450'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":8}',                   'calendar/gantt/add/id/450'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"send\":1}',     'calendar/gantt/add/id/456'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":8}',                   'calendar/gantt/add/id/456'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"send\":1}',     'calendar/gantt/complete/id/450'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":8}',                   'calendar/gantt/complete/id/450'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"send\":1}',     'calendar/gantt/complete/id/452'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":8}',                   'calendar/gantt/complete/id/452'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":8, \"send\":1}',     'calendar/gantt/complete/id/454'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":8}',                   'calendar/gantt/complete/id/454'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"save\":1}',     'calendar/gantt/add/id/449'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"save\":1}',     'calendar/gantt/add/id/450'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"save\":1}',     'calendar/gantt/add/id/456'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"send\":1}',     'calendar/gantt/add/id/449'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":9}',                   'calendar/gantt/add/id/449'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"send\":1}',     'calendar/gantt/add/id/450'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":9}',                   'calendar/gantt/add/id/450'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"send\":1}',     'calendar/gantt/add/id/456'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":9}',                   'calendar/gantt/add/id/456'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"send\":1}',     'calendar/gantt/complete/id/456'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":9}',                   'calendar/gantt/complete/id/456'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"send\":1}',     'calendar/gantt/complete/id/458'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":9}',                   'calendar/gantt/complete/id/458'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":9, \"send\":1}',     'calendar/gantt/complete/id/460'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":9}',                   'calendar/gantt/complete/id/460'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":3, \"save\":1}',     'calendar/gantt/add/id/462'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":3, \"send\":1}',     'calendar/gantt/add/id/462'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":3}',                   'calendar/gantt/add/id/462'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":3, \"send\":1}',     'calendar/gantt/complete/id/462'),
    
    
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":18, \"save\":1}',    'calendar/gantt/add/id/526'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":18, \"send\":1}',    'calendar/gantt/add/id/526'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":18}',                  'calendar/gantt/add/id/526'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":18, \"send\":1}',    'calendar/gantt/complete/id/526'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":18}',                  'calendar/gantt/complete/id/526'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":18, \"send\":1}',    'calendar/gantt/complete/id/528'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":18}',                  'calendar/gantt/complete/id/528'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":18, \"send\":1}',    'calendar/gantt/complete/id/530'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":18}',                  'calendar/gantt/complete/id/530'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":20, \"save\":1}',    'calendar/gantt/add/id/532'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":20, \"send\":1}',    'calendar/gantt/add/id/532'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":20}',                  'calendar/gantt/add/id/532'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":20, \"send\":1}',    'calendar/gantt/complete/id/532'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":20}',                  'calendar/gantt/complete/id/532'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":20, \"send\":1}',    'calendar/gantt/complete/id/534'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":20}',                  'calendar/gantt/complete/id/534'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":19, \"save\":1}',    'calendar/gantt/add/id/536'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":19, \"send\":1}',    'calendar/gantt/add/id/536'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":19}',                  'calendar/gantt/add/id/536'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":19, \"send\":1}',    'calendar/gantt/complete/id/536'),
    ('proposal/proposal/send',  '{\"fairId\":10943, \"ecId\":19}',                'calendar/gantt/complete/id/536'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/reject/id/401/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/add/id/401'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/reject/id/406/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/add/id/406'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/reject/id/410/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/add/id/410'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/reject/id/429/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":1}',                                   'calendar/gantt/add/id/429'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/reject/id/421/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/reject/id/423/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/reject/id/425/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/reject/id/420/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/add/id/420'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/reject/id/422/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/add/id/422'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/reject/id/424/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":4}',                                   'calendar/gantt/add/id/424'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/reject/id/433/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/add/id/433'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/reject/id/431/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/add/id/431'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/reject/id/435/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/add/id/435'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/reject/id/437/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/add/id/437'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/reject/id/439/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/add/id/439'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/reject/id/441/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/add/id/441'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/reject/id/445/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/add/id/445'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/reject/id/443/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/add/id/443'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/reject/id/447/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/add/id/447'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/reject/id/449/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/add/id/449'),
    ('proposal/proposal/reject','{\"ecId\":3}',                                   'calendar/gantt/reject/id/462/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":3}',                                   'calendar/gantt/add/id/462'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/reject/id/530/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/add/id/530'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/reject/id/528/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/add/id/528'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/reject/id/526/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/add/id/526'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/reject/id/524/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/add/id/524'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/reject/id/522/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/add/id/522'),
    ('proposal/proposal/reject','{\"ecId\":19}',                                  'calendar/gantt/reject/id/536/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":19}',                                  'calendar/gantt/add/id/536'),
    ('proposal/proposal/reject','{\"ecId\":20}',                                  'calendar/gantt/reject/id/532/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":20}',                                  'calendar/gantt/add/id/532'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/reject/id/432/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/reject/id/434/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":5}',                                   'calendar/gantt/reject/id/436/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/reject/id/438/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/reject/id/440/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":2}',                                   'calendar/gantt/reject/id/442/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/reject/id/444/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/reject/id/446/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":7}',                                   'calendar/gantt/reject/id/448/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/reject/id/521/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/reject/id/523/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":10}',                                  'calendar/gantt/reject/id/525/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/reject/id/527/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/reject/id/529/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":18}',                                  'calendar/gantt/reject/id/531/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":20}',                                  'calendar/gantt/reject/id/533/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":20}',                                  'calendar/gantt/reject/id/535/userId/{:userId}'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":22, \"save\":1}',    'calendar/gantt/add/id/537'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":22, \"send\":1}',    'calendar/gantt/add/id/537'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":22}',                  'calendar/gantt/add/id/537'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":22, \"send\":1}',    'calendar/gantt/complete/id/537'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":22}',                  'calendar/gantt/complete/id/537'),
    ('proposal/proposal/reject','{\"ecId\":22}',                                  'calendar/gantt/reject/id/537/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":22}',                                  'calendar/gantt/add/id/537'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":21, \"save\":1}',    'calendar/gantt/add/id/538'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":21, \"send\":1}',    'calendar/gantt/add/id/538'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":21}',                  'calendar/gantt/add/id/538'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":21, \"send\":1}',    'calendar/gantt/complete/id/538'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":21}',                  'calendar/gantt/complete/id/538'),
    ('proposal/proposal/reject','{\"ecId\":21}',                                  'calendar/gantt/reject/id/538/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":21}',                                  'calendar/gantt/add/id/538'),
    ('proposal/proposal/reject','{\"ecId\":23}',                                  'calendar/gantt/reject/id/539/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":23}',                                  'calendar/gantt/reject/id/540/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":23}',                                  'calendar/gantt/reject/id/543/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":23}',                                  'calendar/gantt/reject/id/544/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":23}',                                  'calendar/gantt/add/id/539'),
    ('proposal/proposal/reject','{\"ecId\":23}',                                  'calendar/gantt/add/id/543'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":23, \"save\":1}',    'calendar/gantt/add/id/539'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":23, \"send\":1}',    'calendar/gantt/add/id/539'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":23}',                  'calendar/gantt/add/id/539'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":23, \"send\":1}',    'calendar/gantt/complete/id/539'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":23, \"send\":1}',    'calendar/gantt/add/id/540'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":23}',                  'calendar/gantt/complete/id/539'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":23}',                  'calendar/gantt/add/id/540'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":23, \"send\":1}',    'calendar/gantt/complete/id/543'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":23, \"send\":1}',    'calendar/gantt/add/id/544'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":23}',                  'calendar/gantt/complete/id/543'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":23}',                  'calendar/gantt/add/id/544'),
    ('proposal/proposal/reject','{\"ecId\":24}',                                  'calendar/gantt/reject/id/545/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":24}',                                  'calendar/gantt/reject/id/546/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":24}',                                  'calendar/gantt/reject/id/547/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":24}',                                  'calendar/gantt/reject/id/548/userId/{:userId}'),
    ('proposal/proposal/reject','{\"ecId\":24}',                                  'calendar/gantt/add/id/545'),
    ('proposal/proposal/reject','{\"ecId\":24}',                                  'calendar/gantt/add/id/547'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":24, \"save\":1}',    'calendar/gantt/add/id/545'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":24, \"send\":1}',    'calendar/gantt/add/id/545'),
    ('proposal/proposal/send','{\"fairId\":10943, \"ecId\":24}',                  'calendar/gantt/add/id/545'),
    ('proposal/proposal/create','{\"fairId\":10943, \"ecId\":24, \"send\":1}',    'calendar/gantt/complete/id/545'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":24, \"send\":1}',  'calendar/gantt/add/id/546'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":24}',              'calendar/gantt/complete/id/545'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":24}',              'calendar/gantt/add/id/546'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":24, \"send\":1}',  'calendar/gantt/complete/id/547'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":24, \"send\":1}',  'calendar/gantt/add/id/548'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":24}',              'calendar/gantt/complete/id/547'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":24}',              'calendar/gantt/add/id/548'),
    ('proposal/proposal/reject',  '{\"ecId\":25}',                                'calendar/gantt/reject/id/549/userId/{:userId}'),
    ('proposal/proposal/reject',  '{\"ecId\":25}',                                'calendar/gantt/reject/id/550/userId/{:userId}'),
    ('proposal/proposal/reject',  '{\"ecId\":25}',                                'calendar/gantt/reject/id/551/userId/{:userId}'),
    ('proposal/proposal/reject',  '{\"ecId\":25}',                                'calendar/gantt/reject/id/552/userId/{:userId}'),
    ('proposal/proposal/reject',  '{\"ecId\":25}',                                'calendar/gantt/add/id/549'),
    ('proposal/proposal/reject',  '{\"ecId\":25}',                                'calendar/gantt/add/id/551'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":25, \"save\":1}',  'calendar/gantt/add/id/549'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":25, \"send\":1}',  'calendar/gantt/add/id/549'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":25}',              'calendar/gantt/add/id/549'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":25, \"send\":1}',  'calendar/gantt/complete/id/549'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":25, \"send\":1}',  'calendar/gantt/add/id/550'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":25}',              'calendar/gantt/complete/id/549'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":25}',              'calendar/gantt/add/id/550'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":25, \"send\":1}',  'calendar/gantt/complete/id/551'),
    ('proposal/proposal/create',  '{\"fairId\":10943, \"ecId\":25, \"send\":1}',  'calendar/gantt/add/id/552'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":25}',              'calendar/gantt/complete/id/551'),
    ('proposal/proposal/send',    '{\"fairId\":10943, \"ecId\":25}',              'calendar/gantt/add/id/552');

                
INSERT INTO {{trcalendarfairtasks}} 
    (`trParentId`, `langId`, `name`, `desc`)
VALUES 
    ('23755881984295536','ru','Отправить заявку №1 ЭКСПОНЕНТ (скидка 20%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295537','ru','Подписать договор (скидка 20%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295538','ru','Оплатить участие (аванс 10%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295539','ru','Оплатить участие (аванс 40%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295540','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295541','ru','Отправить заявку №1 ЭКСПОНЕНТ (скидка 15%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295542','ru','Подписать договор (скидка 15%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295543','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295544','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295545','ru','Отправить заявку №1 ЭКСПОНЕНТ (скидка 10%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295546','ru','Подписать договор (скидка 10%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295547','ru','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295548','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295549','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295550','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295551','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295552','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295553','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295554','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295555','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295556','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295557','ru','Отправить заявку №1 ЭКСПОНЕНТ','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295558','ru','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295559','ru','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295560','ru','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295561','ru','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295562','ru','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295563','ru','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295564','ru','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295565','ru','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295566','ru','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295567','ru','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295568','ru','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295569','ru','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295570','ru','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295571','ru','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295572','ru','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295573','ru','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295574','ru','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295575','ru','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295576','ru','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295589','ru','Отправить Конкурсную заявку и все необходимые приложения','Задача завершится автоматически, когда будет отправлена Конкурсная заявка'),
    ('23755881984295590','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295591','ru','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295592','ru','Отправить заявку №1 ЭКСПОНЕНТ (скидка 20%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295593','ru','Подписать договор (скидка 20%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295594','ru','Оплатить участие (аванс 10%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295595','ru','Оплатить участие (аванс 40%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295596','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295597','ru','Отправить заявку №1 ЭКСПОНЕНТ (скидка 15%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295598','ru','Подписать договор (скидка 15%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295599','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295600','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295601','ru','Отправить заявку №1 ЭКСПОНЕНТ (скидка 10%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295602','ru','Подписать договор (скидка 10%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295603','ru','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295604','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295605','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295606','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295607','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295608','ru','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295609','ru','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295610','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295611','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295612','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295613','ru','Отправить заявку №1 ЭКСПОНЕНТ','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295614','ru','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295615','ru','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295616','ru','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295617','ru','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295618','ru','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295619','ru','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295620','ru','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295621','ru','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295622','ru','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295623','ru','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295624','ru','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295625','ru','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295626','ru','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295627','ru','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295628','ru','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295629','ru','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295630','ru','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295631','ru','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295632','ru','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295633','ru','Отправить заявку №9-1 РАЗГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295634','ru','Оплатить заявку №9-1 РАЗГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295635','ru','Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295636','ru','Оплатить заявку №9-1 (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295637','ru','Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295638','ru','Оплатить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295639','ru','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295640','ru','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295641','ru','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295642','ru','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295643','ru','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295644','ru','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295645','ru','Отправить Конкурсную заявку и все необходимые приложения','Задача завершится автоматически, когда будет отправлена Конкурсная заявка'),
    ('23755881984295646','ru','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295647','ru','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295648','ru','Оплатить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295649','ru','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295650','ru','Оплатить заявку №9 РАЗГРУЗКА/ПОГРУЗКА(с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295651','ru','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА(с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295652','ru','Оплатить заявку №9 РАЗГРУЗКА/ПОГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295536','en','Send the Application No.1 EXHIBITOR (discount 20%)','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295537','en','Sign the Contract (discount 20%) ','Click FINISH after signature and sending the Contract'),
    ('23755881984295538','en','Pay for Participation (down payment 10%)','Click FINISH if you have paid'),
    ('23755881984295539','en','Pay for Participation (down payment 40%)','Click FINISH if you have paid'),
    ('23755881984295540','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295541','en','Send the Application No.1 EXHIBITOR (discount 15%)','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295542','en','Sign the Contract (discount 15%) ','Click FINISH after signature and sending the Contract'),
    ('23755881984295543','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295544','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295545','en','Send the Application No.1 EXHIBITOR (discount 10%)','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295546','en','Sign the Contract (discount 10%) ','Click FINISH after signature and sending the Contract'),
    ('23755881984295547','en','Pay for Participation (down payment 100%)','Click FINISH if you have paid'),
    ('23755881984295548','en','Send the Application No.2 STANDARD STAND','The task will be completed automatically after sending the Application No.2'),
    ('23755881984295549','en','Pay for the service odered (Application No.2)','Click FINISH if you have paid'),
    ('23755881984295550','en','Send the Application No.2 STANDARD STAND (increase of price 50%)','The task will be completed automatically after sending the Application No.2'),
    ('23755881984295551','en','Pay for the service odered (Application No.2) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295552','en','Send the Application No.2 STANDARD STAND (increase of price 100%)','The task will be completed automatically after sending the Application No.2'),
    ('23755881984295553','en','Pay for the service odered (Application No.2) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295554','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295555','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295556','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295557','en','Send the Application No.1 EXHIBITOR','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295558','en','Send the Application No.3 ANCILLARY EQUIPMENT','The task will be completed automatically after sending the Application No.3'),
    ('23755881984295559','en','Pay for the service odered (Application No.3)','Click FINISH if you have paid'),
    ('23755881984295560','en','Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 50%)','The task will be completed automatically after sending the Application No.3'),
    ('23755881984295561','en','Pay for the service odered (Application No.3) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295562','en','Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 100%)','The task will be completed automatically after sending the Application No.3'),
    ('23755881984295563','en','Pay for the service odered (Application No.3) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295564','en','Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS','The task will be completed automatically after sending the Application No.4'),
    ('23755881984295565','en','Pay for the service odered (Application No.4)','Click FINISH if you have paid'),
    ('23755881984295566','en','Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 50%)','The task will be completed automatically after sending the Application No.4'),
    ('23755881984295567','en','Pay for the service odered (Application No.4) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295568','en','Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 100%)','The task will be completed automatically after sending the Application No.4'),
    ('23755881984295569','en','Pay for the service odered (Application No.4) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295570','en','Send the Application No.5 COMMUNICATION SERVICES','The task will be completed automatically after sending the Application No.5'),
    ('23755881984295571','en','Pay for the service odered (Application No.5)','Click FINISH if you have paid'),
    ('23755881984295572','en','Send the Application No.5 COMMUNICATION SERVICES','The task will be completed automatically after sending the Application No.5'),
    ('23755881984295573','en','Pay for the service odered (Application No.5)','Click FINISH if you have paid'),
    ('23755881984295574','en','Send the Application No.5 COMMUNICATION SERVICES (increase of price 100%)','The task will be completed automatically after sending the Application No.5'),
    ('23755881984295575','en','Pay for the service odered (Application No.5) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295576','en','Send the Application No.9 Freight forwarding services including reservation of loading times','The task will be completed automatically after sending the Application No.9'),
    ('23755881984295589','en','Send the Contest Application form including photos and descriptions','The task will be completed automatically after sending the Contest Application'),
    ('23755881984295590','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295591','en','Pay for Participation (down payment 100%)','Click FINISH if you have paid'),
    ('23755881984295592','en','Send the Application No.1 EXHIBITOR (discount 20%)','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295593','en','Sign the Contract (discount 20%) ','Click FINISH after signature and sending the Contract'),
    ('23755881984295594','en','Pay for Participation (down payment 10%)','Click FINISH if you have paid'),
    ('23755881984295595','en','Pay for Participation (down payment 40%)','Click FINISH if you have paid'),
    ('23755881984295596','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295597','en','Send the Application No.1 EXHIBITOR (discount 15%)','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295598','en','Sign the Contract (discount 15%) ','Click FINISH after signature and sending the Contract'),
    ('23755881984295599','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295600','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295601','en','Send the Application No.1 EXHIBITOR (discount 10%)','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295602','en','Sign the Contract (discount 10%) ','Click FINISH after signature and sending the Contract'),
    ('23755881984295603','en','Pay for Participation (down payment 100%)','Click FINISH if you have paid'),
    ('23755881984295604','en','Send the Application No.2 STANDARD STAND','The task will be completed automatically after sending the Application No.2'),
    ('23755881984295605','en','Pay for the service odered (Application No.2)','Click FINISH if you have paid'),
    ('23755881984295606','en','Send the Application No.2 STANDARD STAND (increase of price 50%)','The task will be completed automatically after sending the Application No.2'),
    ('23755881984295607','en','Pay for the service odered (Application No.2) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295608','en','Send the Application No.2 STANDARD STAND (increase of price 100%)','The task will be completed automatically after sending the Application No.2'),
    ('23755881984295609','en','Pay for the service odered (Application No.2) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295610','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295611','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295612','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295613','en','Send the Application No.1 EXHIBITOR','The task will be completed automatically after sending the Application No.1'),
    ('23755881984295614','en','Send the Application No.3 ANCILLARY EQUIPMENT','The task will be completed automatically after sending the Application No.3'),
    ('23755881984295615','en','Pay for the service odered (Application No.3)','Click FINISH if you have paid'),
    ('23755881984295616','en','Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 50%)','The task will be completed automatically after sending the Application No.3'),
    ('23755881984295617','en','Pay for the service odered (Application No.3) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295618','en','Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 100%)','The task will be completed automatically after sending the Application No.3'),
    ('23755881984295619','en','Pay for the service odered (Application No.3) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295620','en','Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS','The task will be completed automatically after sending the Application No.4'),
    ('23755881984295621','en','Pay for the service odered (Application No.4)','Click FINISH if you have paid'),
    ('23755881984295622','en','Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 50%)','The task will be completed automatically after sending the Application No.4'),
    ('23755881984295623','en','Pay for the service odered (Application No.4) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295624','en','Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 100%)','The task will be completed automatically after sending the Application No.4'),
    ('23755881984295625','en','Pay for the service odered (Application No.4) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295626','en','Send the Application No.5 COMMUNICATION SERVICES','The task will be completed automatically after sending the Application No.5'),
    ('23755881984295627','en','Pay for the service odered (Application No.5)','Click FINISH if you have paid'),
    ('23755881984295628','en','Send the Application No.5 COMMUNICATION SERVICES','The task will be completed automatically after sending the Application No.5'),
    ('23755881984295629','en','Pay for the service odered (Application No.5)','Click FINISH if you have paid'),
    ('23755881984295630','en','Send the Application No.5 COMMUNICATION SERVICES (increase of price 100%)','The task will be completed automatically after sending the Application No.5'),
    ('23755881984295631','en','Pay for the service odered (Application No.5) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295632','en','Send the Application No.9 Freight forwarding services including reservation of loading times','The task will be completed automatically after sending the Application No.9'),
    ('23755881984295633','en','Отправить заявку №9-1 РАЗГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295634','en','Оплатить заявку №9-1 РАЗГРУЗКА','Click FINISH if you have paid'),
    ('23755881984295635','en','Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295636','en','Оплатить заявку №9-1 (с наценкой 50%)','Click FINISH if you have paid'),
    ('23755881984295637','en','Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295638','en','Оплатить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Click FINISH if you have paid'),
    ('23755881984295639','en','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295640','en','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Click FINISH if you have paid'),
    ('23755881984295641','en','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295642','en','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Click FINISH if you have paid'),
    ('23755881984295643','en','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295644','en','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Click FINISH if you have paid'),
    ('23755881984295645','en','Send the Contest Application form including photos and descriptions','The task will be completed automatically after sending the Contest Application'),
    ('23755881984295646','en','Pay for Participation (down payment 50%)','Click FINISH if you have paid'),
    ('23755881984295647','en','Pay for Participation (down payment 100%)','Click FINISH if you have paid'),
    ('23755881984295648','en','Pay for the service odered (Application No.9)','Click FINISH if you have paid'),
    ('23755881984295649','en','Send the Application No.9 Freight forwarding services including reservation of loading times (increase of price 50%)','The task will be completed automatically after sending the Application No.9'),
    ('23755881984295650','en','Pay for the service odered (Application No.9) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295651','en','Send the Application No.9 Freight forwarding services including reservation of loading times (increase of price 100%)','The task will be completed automatically after sending the Application No.9'),
    ('23755881984295652','en','Pay for the service odered (Application No.9) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295536','de','Отправить заявку №1 ЭКСПОНЕНТ (скидка 20%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295537','de','Подписать договор (скидка 20%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295538','de','Оплатить участие (аванс 10%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295539','de','Оплатить участие (аванс 40%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295540','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295541','de','Отправить заявку №1 ЭКСПОНЕНТ (скидка 15%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295542','de','Подписать договор (скидка 15%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295543','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295544','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295545','de','Отправить заявку №1 ЭКСПОНЕНТ (скидка 10%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295546','de','Подписать договор (скидка 10%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295547','de','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295548','de','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295549','de','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295550','de','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295551','de','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295552','de','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295553','de','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295554','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295555','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295556','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295557','de','Отправить заявку №1 ЭКСПОНЕНТ','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295558','de','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295559','de','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295560','de','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295561','de','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295562','de','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295563','de','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295564','de','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295565','de','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295566','de','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295567','de','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295568','de','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295569','de','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295570','de','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295571','de','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295572','de','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295573','de','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295574','de','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295575','de','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295576','de','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295589','de','Отправить Конкурсную заявку и все необходимые приложения','Задача завершится автоматически, когда будет отправлена Конкурсная заявка'),
    ('23755881984295590','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295591','de','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295592','de','Отправить заявку №1 ЭКСПОНЕНТ (скидка 20%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295593','de','Подписать договор (скидка 20%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295594','de','Оплатить участие (аванс 10%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295595','de','Оплатить участие (аванс 40%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295596','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295597','de','Отправить заявку №1 ЭКСПОНЕНТ (скидка 15%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295598','de','Подписать договор (скидка 15%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295599','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295400','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295601','de','Отправить заявку №1 ЭКСПОНЕНТ (скидка 10%)','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295602','de','Подписать договор (скидка 10%)','Нажмите \"Завершить\", когда подпишите и отправите договор'),
    ('23755881984295603','de','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295604','de','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295605','de','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295606','de','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295607','de','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295608','de','Отправить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №2'),
    ('23755881984295609','de','Оплатить заявку №2 СТАНДАРТНЫЙ СТЕНД (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295610','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295611','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295612','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295613','de','Отправить заявку №1 ЭКСПОНЕНТ','Задача завершится автоматически, когда будет отправлена заявка №1'),
    ('23755881984295614','de','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295615','de','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295616','de','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295617','de','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295618','de','Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №3'),
    ('23755881984295619','de','Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295620','de','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295621','de','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295622','de','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295623','de','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295624','de','Отправить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №4'),
    ('23755881984295625','de','Оплатить заявку №4 ТЕХНИЧЕСКИЕ ПОДКЛЮЧЕНИЯ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295626','de','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295627','de','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295628','de','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295629','de','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295630','de','Отправить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №5'),
    ('23755881984295631','de','Оплатить заявку №5 СВЯЗЬ, ОБОРУДОВАНИЕ СВЯЗИ (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295632','de','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295633','de','Отправить заявку №9-1 РАЗГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295634','de','Оплатить заявку №9-1 РАЗГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295635','de','Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295636','de','Оплатить заявку №9-1 (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295637','de','Отправить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-1'),
    ('23755881984295638','de','Оплатить заявку №9-1 РАЗГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295639','de','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295640','de','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295641','de','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295642','de','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295643','de','Отправить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9-2'),
    ('23755881984295644','de','Оплатить заявку №9-2 ХРАНЕНИЕ И ПОГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата\r\n'),
    ('23755881984295645','de','Отправить Конкурсную заявку и все необходимые приложения','Задача завершится автоматически, когда будет отправлена Конкурсная заявка'),
    ('23755881984295646','de','Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295647','de','Предоплата в размере 100%','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295648','de','Оплатить заявку №9 РАЗГРУЗКА/ПОГРУЗКА','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295649','de','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295650','de','Оплатить заявку №9 РАЗГРУЗКА/ПОГРУЗКА(с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295651','de','Отправить заявку №9 РАЗГРУЗКА/ПОГРУЗКА(с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9'),
    ('23755881984295652','de','Оплатить заявку №9 РАЗГРУЗКА/ПОГРУЗКА (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295653','ru','Отправить заявку №6 \"АУДИО - И ВИДЕОТЕХНИКА\"','Задача завершится автоматически, когда будет отправлена заявка №6'),
    ('23755881984295653','en','Send the Application No.6 AUDIO & VIDEO EQUIPMENT','The task will be completed automatically after sending the Application No.6'),
    ('23755881984295654','ru','Оплатить заявку №6 \"АУДИО - И ВИДЕОТЕХНИКА\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295654','en','Pay for the service odered (Application No.6)','Click FINISH if you have paid'),
    ('23755881984295655','ru','Отправить заявку №6 \"АУДИО - И ВИДЕОТЕХНИКА\" (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №6'),
    ('23755881984295655','en','Send the Application No.6 AUDIO & VIDEO EQUIPMENT (increase of price 50%)','The task will be completed automatically after sending the Application No.6'),
    ('23755881984295656','ru','Оплатить заявку №6 \"АУДИО - И ВИДЕОТЕХНИКА\" (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295656','en','Pay for the service odered (Application No.6) (increase of price 50%)','Click FINISH if you have paid'),
    ('23755881984295657','ru','Отправить заявку №6 \"АУДИО - И ВИДЕОТЕХНИКА\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №6'),
    ('23755881984295657','en','Send the Application No.6 AUDIO & VIDEO EQUIPMENT (increase of price 100%)	','The task will be completed automatically after sending the Application No.6	'),
    ('23755881984295658','ru','Оплатить заявку №6 \"АУДИО - И ВИДЕОТЕХНИКА\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295658','en','Pay for the service odered (Application No.6) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295659','ru','Отправить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\"','Задача завершится автоматически, когда будет отправлена заявка №10'),
    ('23755881984295659','en','Send the Application No.10 BUSINESS EVENT','The task will be completed automatically after sending the Application No.10'),
    ('23755881984295660','ru','Оплатить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295660','en','Pay for the service odered (Application No.10)','Click FINISH if you have paid'),
    ('23755881984295663','ru','Отправить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №10'),
    ('23755881984295663','en','Send the Application No.10 BUSINESS EVENT (increase of price 100%)','The task will be completed automatically after sending the Application No.10'),
    ('23755881984295664','ru','Оплатить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295664','en','Pay for the service odered (Application No.10) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295665','ru','Отправить заявку №11 \"ОБРЗОВАТЕЛЬНОЕ МЕРОПРИЯТИЕ\"','Задача завершится автоматически, когда будет отправлена заявка №11'),
    ('23755881984295665','en','Send the Application No.11 EDUCATIONAL EVENT','The task will be completed automatically after sending the Application No.11'),
    ('23755881984295671','ru','Отправить заявку №12 \"БЕЙДЖИ УЧАСТНИКОВ\"','Задача завершится автоматически, когда будет отправлена заявка №12'),
    ('23755881984295671','en','Send the Application No.12 PARTICIPANT BADGES','The task will be completed automatically after sending the Application No.12'),
    ('23755881984295677','ru','Отправить заявку №12a \"VIP БЕЙДЖИ\"','Задача завершится автоматически, когда будет отправлена заявка №12a'),
    ('23755881984295677','en','Send the Application No.12a VIP BADGES','The task will be completed automatically after sending the Application No.12a'),
    ('23755881984295678','ru','Отправить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Задача завершится автоматически, когда будет отправлена заявка №7'),
    ('23755881984295678','en','Send the Application No.7 SECURITY, CLEANING, STAFF','The task will be completed automatically after sending the Application No.7'),
    ('23755881984295679','ru','Оплатить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295679','en','Pay for the service odered (Application No.7)','Click FINISH if you have paid'),
    ('23755881984295680','ru','Отправить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Задача завершится автоматически, когда будет отправлена заявка №7'),
    ('23755881984295680','en','Send the Application No.7 SECURITY, CLEANING, STAFF','The task will be completed automatically after sending the Application No.7'),
    ('23755881984295681','ru','Оплатить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295681','en','Pay for the service odered (Application No.7)','Click FINISH if you have paid'),
    ('23755881984295682','ru','Отправить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №7'),
    ('23755881984295682','en','Send the Application No.7 SECURITY, CLEANING, STAFF (increase of price 100%)','The task will be completed automatically after sending the Application No.7'),
    ('23755881984295683','ru','Оплатить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295683','en','Pay for the service odered (Application No.7) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295684','ru','Отправить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ . ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Задача завершится автоматически, когда будет отправлена заявка №8'),
    ('23755881984295684','en','Send the Application No.8 INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION','The task will be completed automatically after sending the Application No.12a'),
    ('23755881984295685','ru','Оплатить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ . ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295685','en','Pay for the service odered (Application No.8)','Click FINISH if you have paid'),
    ('23755881984295686','ru','Отправить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ . ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Задача завершится автоматически, когда будет отправлена заявка №8.<br/><br/>Если в срок до 26.08.16 заявка не будет получена/принята Организатором, Организатор оставляетс за собой право не публиковать информацию в каталог.'),
    ('23755881984295686','en','Send the Application No.8 INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION','The task will be completed automatically after sending the Application No.8.<br/><br/>Should this Application be not received/accepted by the Organizer by August 26, 2016, the Organizer shall reserv the right not to publish any information on the particioant.'),
    ('23755881984295687','ru','Оплатить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ . ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295687','en','Pay for the service odered (Application No.8)','Click FINISH if you have paid'),
    ('23755881984295690','ru','Отправить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\"','Задача завершится автоматически, когда будет отправлена заявка №9а.<br/><br/>До 17.09.16 пропуска в зону проведения разгрузочно-погрузочных работ заказываются у Организатора.'),
    ('23755881984295690','en','Send the Application No.9a','The task will be completed automatically after sending the Application No.9а.<br/><br/>Note! Before Sept. 17, 2016 you can order the pass only through Organizers service.'),
    ('23755881984295691','ru','Оплатить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\"','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295691','en','Pay for the service odered (Application No.9а)','Click FINISH if you have paid'),
    ('23755881984295692','ru','Отправить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9а.<br/><br/>После 17.09 2016 пропуска в зону разгрузочно-погрузочных работ могут быть приобретены как у Организатора, так и через сервис-центр МВЦ \"Крокус Экспо\".'),
    ('23755881984295692','en','Send the Application No.9а (increase of price 100%)','The task will be completed automatically after sending the Application No.9а<br/><br/>Note! You can order the pass through Organizer\'s or Crocus Expo service after Sept. 17, 2016.'),
    ('23755881984295693','ru','Оплатить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата'),
    ('23755881984295693','en','Pay for the service odered (Application No.9а) (increase of price 100%)','Click FINISH if you have paid'),
    ('23755881984295696','ru','Отправить заявку №13 \"ОРГАНИЗАЦИЯ ГРУППОВОЙ ПОЕЗДКИ\"','Задача завершится автоматически, когда будет отправлена заявка №13'),
    ('23755881984295696','en','Send the Application No.13','The task will be completed automatically after sending the Application No.13');
    
    INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":10, \"send\":1}', 'calendar/gantt/complete/id/522');
    INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/send', '{\"fairId\":10943, \"ecId\":10}', 'calendar/gantt/complete/id/522');
    INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":10, \"send\":1}', 'calendar/gantt/complete/id/524');
    INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/send', '{\"fairId\":10943, \"ecId\":10}', 'calendar/gantt/complete/id/524');
		
    INSERT INTO {{organizerinfo}} (`organizerId`, `postPostcode`, `postSite`, `legalPostcode`, `requisitesSettlementAccountRUR`, `requisitesBic`, `requisitesItn`, `requisitesIec`, `requisitesCorrespondentAccount`, `postPhone`) VALUES ('3460', '99999', 'http://test.ru', '999999', '4548 9879 9 87 09 807 98', '56454', '654654', '654654', '65465465465465465465', '+7 (777) 777 77 77');
   
   
   
   
   
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('30', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('31', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('32', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('33', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('34', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('35', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('36', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('37', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('38', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('39', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('40', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('41', '2', '1', '10943');
    INSERT INTO {{documents}} (`id`, `type`, `active`, `fairId`) VALUES ('42', '2', '1', '10943');
    
    
    
    
    
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('30', 'ru', 'Формы участника', '/static/documents/fairs/10943/exhibitor_forms.docx');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('31', 'ru', 'General Terms of Participation', '/static/documents/fairs/10943/General Terms of participations.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('32', 'ru', 'Общие условия участия в выставках на Центральном выставочном комплексе «ЭКСПОЦЕНТР»', '/static/documents/fairs/10943/obsh_uslovia.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('33', 'ru', 'Participants forms', '/static/documents/fairs/10943/participants_forms_eng.docx');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('34', 'ru', 'Презентация 2017', '/static/documents/fairs/10943/presentation-metobr-2017-2016.08.30-rus.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('35', 'ru', 'Порядок ввоза/вывоза оборудования и экспонатов', '/static/documents/fairs/10943/Procedure_for_Delivery_and_Removal_of_Equipment_and_Exhibits_15.12.2014_ru.doc');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('36', 'ru', 'Procedure for Delivery/Removal of Equipment and Exhibits', '/static/documents/fairs/10943/Procedure_for_Delivery_Removal_of_Equipment_and_Exhibits.docx');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('37', 'ru', 'Rules for Contractors of Exhibition Stands and Expositions at Expocentre Fairgrounds', '/static/documents/fairs/10943/rules_for_developers_2011_eng.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('38', 'ru', 'Rules for the Use of Vehicles at Expocentre Fairgrounds (Enclosure No.7)', '/static/documents/fairs/10943/rules_for_the_use_of_vehicles.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('39', 'ru', 'Правила организации работ застройщиков выставочных стендов и экспозиций на территории Центрального выставочного комплекса «ЭКСПОЦЕНТР»', '/static/documents/fairs/10943/rules_of_work.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('40', 'ru', 'Правила нахождения транспортных средств на территории ЦВК «ЭКСПОЦЕНТР»', '/static/documents/fairs/10943/rules_transport.pdf');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('41', 'ru', 'Схема проезда', '/static/documents/fairs/10943/scheme_ride.jpg');
    INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('42', 'ru', 'General Terms for Holding Events at Expocentre Fairgrounds', '/static/documents/fairs/10943/terms_for_holding_events.pdf');

    ";
    }

    public function downSql()
    {
        return TRUE;
    }
}