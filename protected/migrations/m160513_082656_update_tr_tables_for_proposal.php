<?php

class m160513_082656_update_tr_tables_for_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{trattributeclass}}
			ADD UNIQUE INDEX `index2` (`trParentId` ASC, `langId` ASC);

			ALTER TABLE {{trattributeclassproperty}}
			ADD UNIQUE INDEX `index2` (`trParentId` ASC, `langId` ASC);

			ALTER TABLE {{trattributeenum}}
			ADD UNIQUE INDEX `index2` (`trParentId` ASC, `langId` ASC);

			ALTER TABLE {{trproposalelementclass}}
			ADD UNIQUE INDEX `index2` (`trParentId` ASC, `langId` ASC);

			INSERT INTO {{trattributeclass}} (trParentId, langId, label, unitTitle, placeholder, tooltip, tooltipCustom)
			SELECT id, 'ru', label, unitTitle, placeholder, tooltip, tooltipCustom from {{attributeclass}};

			INSERT INTO {{trproposalelementclass}} (trParentId, langId, label, pluralLabel)
			SELECT id, 'ru', label, pluralLabel from {{proposalelementclass}};

			INSERT INTO {{trattributeenum}} (trParentId, langId, value)
			SELECT id, 'ru', value from {{attributeenum}};

			INSERT INTO {{trattributeclassproperty}} (trParentId, langId, value)
			SELECT id, 'ru', defaultValue from {{attributeclassproperty}};
	    ";
	}
}