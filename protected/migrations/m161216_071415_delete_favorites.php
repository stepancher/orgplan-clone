<?php

class m161216_071415_delete_favorites extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP TABLE IF EXISTS {{favorites}};
            
            DELETE FROM {{seo}} WHERE `id`='476535';
            DELETE FROM {{seo}} WHERE `id`='476830';
            DELETE FROM {{seo}} WHERE `id`='476912';
            DELETE FROM {{seo}} WHERE `id`='476961';
            DELETE FROM {{seo}} WHERE `id`='482898';
            DELETE FROM {{seo}} WHERE `id`='483088';
            
            DELETE FROM {{route}} WHERE `id`='93660';
            DELETE FROM {{route}} WHERE `id`='93661';
            DELETE FROM {{route}} WHERE `id`='93662';
            DELETE FROM {{route}} WHERE `id`='93663';
            
            ALTER TABLE {{portfolio}} 
            DROP COLUMN `favoritesId`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}