<?php

class m161121_122209_proposals_signature_translate_de extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1133', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1134', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1135', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1136', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1137', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1138', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1139', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1140', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1142', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1143', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1144', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1145', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1146', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1147', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1148', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1149', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1150', 'de', 'Datum');
            INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1151', 'de', 'Datum');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}