<?php

class m160304_123020_04_03_2016___15_30_FIS_patches extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{task}} DROP COLUMN `parent`;
			ALTER TABLE {{task}} DROP COLUMN `progress`;
			ALTER TABLE {{task}} DROP COLUMN `source`;
			ALTER TABLE {{task}} DROP COLUMN `target`;
			ALTER TABLE {{fair}} DROP COLUMN `datetimeModified`;
			ALTER TABLE {{fairclone}} DROP COLUMN `datetimeModified`;
			ALTER TABLE {{trexhibitioncomplextype}} DROP COLUMN `shortUrl`;
			ALTER TABLE {{trindustry}} DROP COLUMN `shortNameUrl`;
			ALTER TABLE {{trregion}} DROP COLUMN `shortUrl`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			ALTER TABLE {{task}} ADD `parent` int(11);
			ALTER TABLE {{task}} ADD `progress` varchar(255);
			ALTER TABLE {{task}} ADD `source` int(11);
			ALTER TABLE {{task}} ADD `target` int(11);
			ALTER TABLE {{fair}} ADD `datetimeModified` timestamp;
			ALTER TABLE {{fairclone}} ADD `datetimeModified` timestamp;
			ALTER TABLE {{trexhibitioncomplextype}} ADD `shortUrl` varchar(255);
			ALTER TABLE {{trindustry}} ADD `shortNameUrl` varchar(255);
			ALTER TABLE {{trregion}} ADD `shortUrl` varchar(255);
		';
	}
}