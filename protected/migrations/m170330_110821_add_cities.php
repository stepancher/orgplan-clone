<?php

class m170330_110821_add_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('65');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1093', 'ru', 'Ашерслебен-Штасфурт', 'aschersleben-staßfurt');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1093', 'en', 'Aschersleben-Staßfurt', 'aschersleben-staßfurt');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1093', 'de', 'Landkreis Aschersleben-Staßfurt', 'aschersleben-staßfurt');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1093', 'falkenstein');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1542', 'ru', 'Фалькенштайн (Гарц)');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1542', 'en', 'Falkenstein');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1542', 'de', 'Falkenstein/Harz');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}