<?php

class m160614_110208_delete_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE id IN (4646, 4701, 4826, 10418, 10419);
			DELETE FROM {{trfair}} WHERE trParentId IN (4646, 4701, 4826, 10418, 10419);
		";
	}
}