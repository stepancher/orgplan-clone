<?php

class m170221_113709_add_exdb_exhibitioncomplex_to_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP TABLE IF EXISTS {$expodata}.{{exdbvenue}};
            DROP TABLE IF EXISTS {$expodata}.{{exdbtrvenue}};
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbexhibitioncomplex}};
            DROP TABLE IF EXISTS {$expodata}.{{exdbtrexhibitioncomplex}};
            
            CREATE TABLE {$expodata}.{{exdbexhibitioncomplex}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `cityId` INT(11) NULL DEFAULT NULL,
              `coordinates` VARCHAR(255) NULL DEFAULT NULL,
              `contacts` TEXT NULL DEFAULT NULL,
              `raw` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}