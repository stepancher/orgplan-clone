<?php

class m161215_132156_delete_services extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbTz->beginTransaction();
        try {
            Yii::app()->dbTz->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbTz->beginTransaction();
        try {
            Yii::app()->dbTz->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP TABLE IF EXISTS {{trservices}};
            DROP TABLE IF EXISTS {{services}};
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}