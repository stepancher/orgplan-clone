<?php

class m160614_163512_update_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2179', 'n10_conf_price2', 'int', '0', '2146', '2146', '11', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2180', 'n10_conf_price3', 'int', '0', '2146', '2146', '12', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2183', 'n10_equip_lcd_price2', 'int', '0', '2157', '2157', '11', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2184', 'n10_equip_lcd_price3', 'int', '0', '2157', '2157', '12', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2185', 'n10_equip_lcd2_price2', 'int', '0', '2157', '2157', '21', 'coefficientDouble', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2186', 'n10_equip_lcd2_price3', 'int', '0', '2157', '2157', '22', 'coefficientDouble', '0');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('2185', 'ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('2186', 'ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('2179', 'ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('2180', 'ru');
			/** ok **/
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2179', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2180', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2179', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2180', 'selfCollapseLabel');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1951', 'ru', 'Период аренды (1/2 дня)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1952', 'ru', 'Период аренды (1 час)');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2183', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2184', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2185', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2186', '1', 'selfCollapse');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1953', 'ru', 'LCD проектор 4000 Lum (1 час)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1954', 'ru', 'LCD проектор 4000 Lum (1 день)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1955', 'ru', 'LCD проектор 8000 Lum (1 час)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1956', 'ru', 'LCD проектор 4000 Lum (1 день)');
			/** ok **/
			
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2183', NULL, 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2184', NULL, 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2185', NULL, 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2186', NULL, 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2179', '760', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2180', '225', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2183', '196', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2184', '345', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2185', '260', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2186', '495', 'price');
			/** ok **/
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2179', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2180', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2183', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2184', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2185', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2186', 'USD', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2143', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2144', 'unitTitle');
			
			UPDATE {{attributeclass}} SET `class`='datePickerTime' WHERE `id`='2145';
			UPDATE {{attributeclass}} SET `name`='n10_conf_price' WHERE `id`='2147';
			UPDATE {{attributeclass}} SET `name`='n10_equip_lcd_price' WHERE `id`='2158';
			UPDATE {{attributeclass}} SET `name`='n10_equip_lcd2_price' WHERE `id`='2159';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1957' WHERE `id`='2646';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1958' WHERE `id`='2647';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1959' WHERE `id`='2648';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1960' WHERE `id`='2649';
			/** ok **/
			
			
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2147';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2179';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2180';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2183';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2184';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2185';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2186';
			UPDATE {{attributeclass}} SET `class`='dropDown' WHERE `id`='2133';
			UPDATE {{trattributeclassproperty}} SET `value`='Период аренды (1 день)' WHERE `id`='2456';
			
			DELETE FROM {{attributeclass}} WHERE `id`='2158';
			DELETE FROM {{attributeclass}} WHERE `id`='2159';
			
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1948' WHERE `id`='2608';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1949' WHERE `id`='2609';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1950' WHERE `id`='2610';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1951' WHERE `id`='2611';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1952' WHERE `id`='2612';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1953' WHERE `id`='2613';
			
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1932' WHERE `id`='2608';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1933' WHERE `id`='2609';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1938' WHERE `id`='2610';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1939' WHERE `id`='2611';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1940' WHERE `id`='2612';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1941' WHERE `id`='2613';
		";
	}
}