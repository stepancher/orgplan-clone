<?php

class m160922_162339_create_calendarfairtaskscommon_table extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{calendarfairtaskscommon}} (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `fairId` int(11) DEFAULT NULL,
              `uuid` char(36) NOT NULL,
              `parent` char(36) DEFAULT NULL,
              `group` char(36) NOT NULL,
              `duration` mediumint(8) DEFAULT NULL,
              `date` datetime DEFAULT NULL,
              `color` varchar(45) DEFAULT NULL,
              `completeButton` tinyint(1) NOT NULL DEFAULT '0',
              `first` tinyint(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=559 DEFAULT CHARSET=utf8;
            
            CREATE TABLE {{trcalendarfairtaskscommon}} (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `trParentId` varchar(255) DEFAULT NULL,
              `langId` varchar(45) DEFAULT NULL,
              `name` text,
              `desc` text,
              PRIMARY KEY (`id`),
              UNIQUE KEY `cft_uniq` (`langId`,`trParentId`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1805 DEFAULT CHARSET=utf8;
            
            ALTER TABLE {{calendarfairtaskscommon}} 
            ADD COLUMN `before` INT NULL AFTER `first`;


            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('1', '1', '30', '1', '1', '120');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('2', '2', '5', '1', '1', '120');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('3', '3', '3', '1', '1', '60');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('4', '4', '3', '1', '1', '60');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('5', '5', '3', '1', '1', '60');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('6', '6', '3', '1', '1', '90');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('7', '7', '3', '1', '1', '60');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('8', '8', '3', '1', '1', '30');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('9', '9', '1', '1', '1', '1');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('10', '10', '14', '1', '1', '60');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('11', '11', '10', '1', '1', '45');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('12', '12', '3', '1', '1', '7');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('13', '13', '1', '1', '1', '3');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('14', '14', '2', '1', '1', '20');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('15', '15', '14', '1', '1', '45');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('16', '16', '30', '1', '1', '120');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('17', '17', '30', '1', '1', '90');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('18', '18', '14', '1', '1', '45');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('19', '19', '14', '1', '1', '30');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('20', '20', '14', '1', '1', '30');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('21', '21', '2', '1', '1', '7');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('22', '22', '1', '1', '1', '1');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('23', '23', '2', '1', '1', '45');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('24', '24', '3', '1', '1', '45');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('25', '25', '1', '1', '1', '30');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('26', '26', '10', '1', '1', '45');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('27', '27', '10', '1', '1', '30');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('28', '28', '2', '1', '1', '4');
            INSERT INTO {{calendarfairtaskscommon}} (`uuid`, `group`, `duration`, `completeButton`, `first`, `before`) VALUES ('29', '29', '2', '1', '1', '5');



            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('1', 'ru', 'Разработайте концепцию участия в выставке учитывая поставленную цель', 'Смотри наши шаблоны');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('2', 'ru', 'Заключите договор аренды выставочной площади с Организатором');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('3', 'ru', 'Оформите заявку на размещение наружной рекламы, медиарекламы, рекламы внутри помещений');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('4', 'ru', 'Оформите заявку на размещение информации о компании на сайте выставки и в официальном каталоге');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('5', 'ru', 'Оформите заявку на помещение и оборудование для проведения мероприятия в рамкках выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('6', 'ru', 'Выбирите подрядчика и заключите договор для экспедирования в случае таможенного оформления');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('7', 'ru', 'Выбирите подрядчика и заключите договор для доставки экспонатов на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('8', 'ru', 'Закажите пропуски/бейджи для сотрудников и пропуски для транспорта на всытавку');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('9', 'ru', 'Пройдите аккредитацию в оргкомитете выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('10', 'ru', 'Утвердите список сотрудников для работы на стенде, закажите гостиницу и билеты авия или ж/д');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('11', 'ru', 'Выбирите подрядчика и заключите договор с кейтеринговым бюро для обеспечения питания гостей и сотрудников на стенде');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('12', 'ru', 'Составьте список продуктов для  закупки на стенд', 'Чек-лист продукты на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('13', 'ru', 'Купите продукты на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('14', 'ru', 'Составте чек-лист самого необходимого для работы стенда', 'Степлер, прайсы и т.д.');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('15', 'ru', 'Выбирите подрядчика и заключите договор по предоставлению наемного персонала (переводчики, хостессы, промо, артисты, ведущие...)', 'Если Ваша фирма привлекает стороннюю организацию для строительно-монтажных работ по застройке стенда или занимается оборудованием стенда самостоятельно, необходимо получить разрешение на проведение монтажных работ у организатора пройти аккредитацию');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('16', 'ru', 'Определите концепцию стенда от цели участия и составьте техническое задание на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('17', 'ru', 'Выбирите подрядчика и закажите проект стенда или оформите заявку на строительство стенда от Организатора');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('18', 'ru', 'Закажите подключение неаобходимых коммуникаций у Организатора всытавки: электричество, вода, интернет и другое');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('19', 'ru', 'Закажите мультимедийное оборудование на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('20', 'ru', 'Разработайте программу продвижения вашего участия и выбирите каналы коммуникации с посетитеями до и во время выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('21', 'ru', 'Составьте инструктаж для сотрудников');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('22', 'ru', 'Проведите инструктаж сотрудников');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('23', 'ru', 'Составьте анкету для регистрации посетителей на стенде');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('24', 'ru', 'Составьте письмо для электронной рассылки и разработать электронное приглашение/печатное приглашение');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('25', 'ru', 'Запустите рекламную кампания вашего участия в выставке: новости в соц.сетях, рассылка, анонсы в СМИ');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('26', 'ru', 'Закажите POS-материалы/сувенирную продукцию/презентационные материалы');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('27', 'ru', 'Приобретите/изготовьте фирменную одежду для работы стендистов и сотрудников');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('28', 'ru', 'Составьте список посеетителей стенда после выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('29', 'ru', 'Отправьте благодарственное письмо');

            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('1', 'en', 'Разработайте концепцию участия в выставке учитывая поставленную цель', 'Смотри наши шаблоны');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('2', 'en', 'Заключите договор аренды выставочной площади с Организатором');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('3', 'en', 'Оформите заявку на размещение наружной рекламы, медиарекламы, рекламы внутри помещений');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('4', 'en', 'Оформите заявку на размещение информации о компании на сайте выставки и в официальном каталоге');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('5', 'en', 'Оформите заявку на помещение и оборудование для проведения мероприятия в рамкках выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('6', 'en', 'Выбирите подрядчика и заключите договор для экспедирования в случае таможенного оформления');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('7', 'en', 'Выбирите подрядчика и заключите договор для доставки экспонатов на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('8', 'en', 'Закажите пропуски/бейджи для сотрудников и пропуски для транспорта на всытавку');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('9', 'en', 'Пройдите аккредитацию в оргкомитете выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('10', 'en', 'Утвердите список сотрудников для работы на стенде, закажите гостиницу и билеты авия или ж/д');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('11', 'en', 'Выбирите подрядчика и заключите договор с кейтеринговым бюро для обеспечения питания гостей и сотрудников на стенде');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('12', 'en', 'Составьте список продуктов для  закупки на стенд', 'Чек-лист продукты на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('13', 'en', 'Купите продукты на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('14', 'en', 'Составте чек-лист самого необходимого для работы стенда', 'Степлер, прайсы и т.д.');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`, `desc`) VALUES ('15', 'en', 'Выбирите подрядчика и заключите договор по предоставлению наемного персонала (переводчики, хостессы, промо, артисты, ведущие...)', 'Если Ваша фирма привлекает стороннюю организацию для строительно-монтажных работ по застройке стенда или занимается оборудованием стенда самостоятельно, необходимо получить разрешение на проведение монтажных работ у организатора пройти аккредитацию ');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('16', 'en', 'Определите концепцию стенда от цели участия и составьте техническое задание на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('17', 'en', 'Выбирите подрядчика и закажите проект стенда или оформите заявку на строительство стенда от Организатора');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('18', 'en', 'Закажите подключение неаобходимых коммуникаций у Организатора всытавки: электричество, вода, интернет и другое');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('19', 'en', 'Закажите мультимедийное оборудование на стенд');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('20', 'en', 'Разработайте программу продвижения вашего участия и выбирите каналы коммуникации с посетитеями до и во время выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('21', 'en', 'Составьте инструктаж для сотрудников');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('22', 'en', 'Проведите инструктаж сотрудников');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('23', 'en', 'Составьте анкету для регистрации посетителей на стенде');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('24', 'en', 'Составьте письмо для электронной рассылки и разработать электронное приглашение/печатное приглашение');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('25', 'en', 'Запустите рекламную кампания вашего участия в выставке: новости в соц.сетях, рассылка, анонсы в СМИ');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('26', 'en', 'Закажите POS-материалы/сувенирную продукцию/презентационные материалы');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('27', 'en', 'Приобретите/изготовьте фирменную одежду для работы стендистов и сотрудников');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('28', 'en', 'Составьте список посеетителей стенда после выставки');
            INSERT INTO {{trcalendarfairtaskscommon}} (`trParentId`, `langId`, `name`) VALUES ('29', 'en', 'Отправьте благодарственное письмо');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}