<?php

class m170222_075719_add_kubinka_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('31', 'shchyolkovo');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1531', 'ru', 'Щёлково');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1531', 'en', 'Shchyolkovo');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1531', 'de', 'Schtscholkowo');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}