<?php

class m170220_082345_alter_exdb_audits_in_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DELETE FROM {$expodata}.{{exdbtraudit}};
            DROP TABLE IF EXISTS {$expodata}.{{exdbaudit}};
            CREATE TABLE {$expodata}.{{exdbaudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `active` INT(11) NULL DEFAULT NULL,
              `contacts_en` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            ALTER TABLE {$expodata}.{{exdbtraudit}} 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
            ADD PRIMARY KEY (`id`);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}