<?php

class m161025_142552_copy_fairs_stat_from_2016_to_2015 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_stat_fairs_from_2016_to_2015`()
            BEGIN
            
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE source_fair_id INT DEFAULT 0;
                DECLARE target_fair_id INT DEFAULT 0;
                DECLARE sourceSquareNet INT DEFAULT 0;
                DECLARE sourceSquareGross INT DEFAULT 0;
                DECLARE sourceMembers INT DEFAULT 0;
                DECLARE sourceVisitors INT DEFAULT 0;
                DECLARE sourceStatistics INT DEFAULT 0;
                DECLARE sourceRating INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT
                            f16.id, f15.id, f16.squareNet, f16.squareGross, f16.members, f16.visitors, f16.statistics, f16.rating
                            FROM {{fair}} f16
                            LEFT JOIN {{fairhasassociation}} fha ON fha.fairId = f16.id
                            LEFT JOIN {{fair}} f15 ON f15.storyId = f16.storyId AND YEAR(f15.beginDate) = 2015 AND MONTH(f16.beginDate) = MONTH(f15.beginDate)
                            LEFT JOIN {{trfair}} trf16 ON trf16.trParentId = f16.id AND trf16.langId = 'ru'
                            LEFT JOIN {{trfair}} trf15 ON trf15.trParentId = f15.id AND trf15.langId = 'ru'
                            WHERE YEAR(f16.beginDate) = 2016 AND
                            f16.active = 1 AND
                            YEAR(f16.statisticsDate) = 2015 AND
                            fha.id IS NOT NULL AND
                            f15.id IS NOT NULL AND
                            trf16.name = trf15.name;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                START TRANSACTION;
                    DELETE FROM {{fairinfo}} WHERE 
                        areaClosedExhibitorsForeign IS NULL AND 
                        areaClosedExhibitorsLocal IS NULL AND 
                        areaOpenExhibitorsForeign IS NULL AND 
                        areaOpenExhibitorsLocal IS NULL AND 
                        areaSpecialExpositions IS NULL AND 
                        countriesCount IS NULL AND 
                        exhibitorsForeign IS NULL AND 
                        exhibitorsLocal IS NULL AND 
                        visitorsForeign IS NULL AND 
                        visitorsLocal IS NULL;
                COMMIT;
                
            OPEN copy;
            
            read_loop: LOOP
                FETCH copy INTO 
                    source_fair_id, 
                    target_fair_id, 
                    sourceSquareNet, 
                    sourceSquareGross, 
                    sourceMembers, 
                    sourceVisitors, 
                    sourceStatistics,
                    sourceRating;
                
                IF done THEN
                    LEAVE read_loop;
                END IF;
                
                START TRANSACTION;
                    UPDATE {{fair}} SET `squareNet` = sourceSquareNet, `squareGross` = sourceSquareGross,
                        `members` = sourceMembers, `visitors` = sourceVisitors, `statistics` = sourceStatistics,
                        `rating` = sourceRating
                    WHERE `id` = target_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    UPDATE {{fairinfo}} SET `fairId` = target_fair_id WHERE `fairId` = source_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    UPDATE {{fairhasassociation}} SET `fairId` = target_fair_id WHERE `fairId` = source_fair_id;
                COMMIT;
                
                START TRANSACTION;
                    UPDATE {{fair}} SET `rating` = 3 WHERE `id` = source_fair_id;
                COMMIT;
            
            END LOOP;
        CLOSE copy;
    END;
    
    CALL copy_stat_fairs_from_2016_to_2015();
    DROP PROCEDURE IF EXISTS `copy_stat_fairs_from_2016_to_2015`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}