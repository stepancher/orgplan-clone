<?php

class m160826_135837_change_discount_day extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 43:1.5}' WHERE `id`='65';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 43:1.5}' WHERE `id`='62';
            UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 03:00:00' WHERE `id`='27';
            UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 03:00:00' WHERE `id`='26';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{16:2, 45:1.5}' WHERE `id`='47';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{21:2}' WHERE `id`='641';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:1.5, 14:2}' WHERE `id`='921';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 22:2}' WHERE `id`='981';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{70:1.5, 21:2}' WHERE `id`='689';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 50:2}' WHERE `id`='978';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 50:2}' WHERE `id`='979';
            UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 50:2}' WHERE `id`='980';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}