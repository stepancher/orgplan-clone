<?php

class m160727_081112_create_user_report_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAdditionalDataForTz();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{userreport}};
			DROP TABLE IF EXISTS {{report}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAdditionalDataForTz(){
		return '
			CREATE TABLE IF NOT EXISTS {{userreport}} (
				`id` INT NOT NULL,
				`userId` INT NULL,
				`reportId` INT NULL,
				`reportJSON` MEDIUMTEXT NULL,
				PRIMARY KEY (`id`)
			);

			CREATE TABLE IF NOT EXISTS {{report}} (
			  `id` INT NOT NULL,
			  `reportId` INT NULL,
			  `reportJSON` INT NULL,
			  PRIMARY KEY (`id`));

			ALTER TABLE {{report}}
			CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

			ALTER TABLE {{userreport}}
			CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

			ALTER TABLE {{report}}
			CHANGE COLUMN `reportJSON` `reportJSON` LONGBLOB NULL DEFAULT NULL ;
		';
	}
}