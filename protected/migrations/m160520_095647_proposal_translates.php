<?php

class m160520_095647_proposal_translates extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">STAND SIZE</div> Cost of services increases by 50%<br/> if ordered after Aug.25, 2016,<br/>by 100% if ordered after Sep.23, 2016' WHERE `id`='251';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">STAND SIZE</div> Cost of services increases by 50%<br/> if ordered after Aug.25, 2016,<br/>by 100% if ordered after Sep.23, 2016' WHERE `id`='1073';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Стоимость услуг</div> Стоимость услугувеличивается:<br/> на 50% при заказе после 25.08.16<br/>на 100% при заказепосле 23.09.16' WHERE `id`='251';
			
			ALTER TABLE {{attributeclass}} 
			DROP COLUMN `tooltip`;
			
			ALTER TABLE {{attributeclass}} 
			DROP COLUMN `unitTitle`;
			UPDATE {{trattributeclass}} SET `unitTitle`='m²' WHERE `id`='1058';
			UPDATE {{trattributeclass}} SET `unitTitle`='m' WHERE `id`='1061';
			UPDATE {{trattributeclass}} SET `unitTitle`='m' WHERE `id`='1062';
			DELETE FROM {{attributeclass}} WHERE `id`='1';
			UPDATE {{trattributeclass}} SET `unitTitle`='m²' WHERE `id`='1058';
			ALTER TABLE {{attributeclass}} 
			DROP COLUMN `placeholder`;
			UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ВЕСА</div><div class=\"body\">При расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.</div>' WHERE `id`='589';
			UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ВЕСА</div><div class=\"body\">При расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.</div>' WHERE `id`='591';
			UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ОБЪЕМА</div><div class=\"body\">При расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.</div>' WHERE `id`='627';
			UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ОБЪЕМА</div><div class=\"body\">При расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.</div>' WHERE `id`='593';
			UPDATE {{trattributeclassproperty}} SET `value`='м³' WHERE `id`='579';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ВРЕМЕНИ</div><div class=\"body\">При расчетах каждый начавшийся час считается, как целый час.</div>' WHERE `id`='481';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ВРЕМЕНИ</div><div class=\"body\">При расчетах каждый начавшийся час считается, как целый час.</div>' WHERE `id`='482';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАБОЧАЯ СИЛА</div><div class=\"body\">Согласно принятым техническим нормам не менее одного рабочего или установленной группы рабочих должны быть наняты для управления грузоподъемным механизмом.</div>' WHERE `id`='483';
			ALTER TABLE {{attributeclass}} 
			DROP COLUMN `tooltipCustom`;
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ОБЪЕМА</div><div class=\"body\">При расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.</div>' WHERE `id`='497';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ВЕСА</div><div class=\"body\">При расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.</div>' WHERE `id`='474';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ВЕСА</div><div class=\"body\">При расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.</div>' WHERE `id`='475';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">РАСЧЕТ ОБЪЕМА</div><div class=\"body\">При расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.</div>' WHERE `id`='476';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_hint_after_summary', 'string', '0', '654', '654', '11', 'hint', '0');
			UPDATE {{attributeclass}} SET `name`='n9_hint_danger_last' WHERE `id`='2006';
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2007', 'ru', 'Все цены включают НДС 18%.<br>Оплата настоящей Заявки производится в российских рублях.');
			UPDATE {{attributeclass}} SET `priority`='10.5' WHERE `id`='2007';
			UPDATE {{attributeclass}} SET `priority`='10.1' WHERE `id`='2007';
			UPDATE {{attributeclass}} SET `priority`='9.9' WHERE `id`='2007';
			UPDATE {{attributeclass}} SET `priority`='9.5' WHERE `id`='2005';
			UPDATE {{attributeclass}} SET `priority`='9.6' WHERE `id`='2007';
			UPDATE {{attributeclass}} SET `priority`='10' WHERE `id`='2006';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '960', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '961', 'clear', '0');
			UPDATE {{attributeclassdependency}} SET `attributeClassRelated`='962' WHERE `id`='862';
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '604', '964', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '966', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '968', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '970', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '972', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '974', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '976', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('604', '978', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1002', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1004', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1006', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1008', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1010', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1012', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1014', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1016', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1018', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '638', '1020', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '961', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '963', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '965', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '967', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '969', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '971', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '973', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '975', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '977', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('596', '979', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '961', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '963', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '965', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '967', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '969', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '971', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '973', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '975', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '977', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('600', '979', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1003', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1005', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1007', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1009', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1011', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1013', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1015', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1017', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1019', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('630', '1021', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '961', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '963', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '965', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '967', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '969', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '971', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '973', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '975', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '977', 'clear', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('634', '979', 'clear', '0');
		";
	}
}