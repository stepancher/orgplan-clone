<?php

class m161213_081203_insert_trcities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql(){

        return "
            DELETE FROM tbl_trcity WHERE langId = 'en';
            DELETE FROM tbl_trcity WHERE langId = 'de';
            
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(134,'Orel', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(134,'Orjol', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(210,'Glazov', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(210,'Glasow', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(211,'Voronezhskaya', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(211,'Voronezhskaya', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(212,'Mikhaylovsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(212,'Michailowsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(213,'Michurinsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(213,'Michurinsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(214,'Novorossiysk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(214,'Noworossijsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(216,'Prutskoi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(216,'Prutskoi', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(217,'Znamensk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(217,'Snamensk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(218,'Akhtubinsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(218,'Achtubinsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(219,'Armavir', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(219,'Armawir', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(220,'Shakhty', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(220,'Schachty', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(221,'Salsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(221,'Salsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(223,'Anapa, Vityazevo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(223,'Anapa, Witjasewo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(238,'The village (Resort named Cjurupy)', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(238,'Ein Dorf von Sanatorium namens Zurupy', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(1,'Moscow', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(1,'Moskau', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(2,'Dzyarzhynsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(2,'Dsjarschynsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(3,'Kaluga', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(3,'Kaluga', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(4,'Saint Petersburg', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(4,'Sankt Petersburg', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(5,'Yekaterinburg', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(5,'Jekaterinburg', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(6,'Surgut', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(6,'Surgut', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(7,'Omsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(7,'Omsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(8,'Rostov-on-Don', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(8,'Rostow am Don', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(9,'Krasnodar', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(9,'Krasnodar', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(10,'Yuzhno-Sahalinsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(10,'Juschno-Sachalinsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(11,'Petropavlovsk-Kamchatsky', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(11,'Petropawlowsk-Kamtschatski', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(12,'Adler', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(12,'Adler', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(13,'Sochi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(13,'Sotschi', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(14,'Yaroslavl', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(14,'Jaroslawl', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(15,'Chelyabinsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(15,'Tscheljabinsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(16,'Ulyanovsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(16,'Uljanowsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(17,'Tula', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(17,'Tula', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(18,'Tomsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(18,'Tomsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(19,'Tver', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(19,'Tver', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(20,'Smolensk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(20,'Smolensk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(21,'Saratov', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(21,'Saratow', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(22,'Ryazan', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(22,'Rjasan', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(23,'Pskov', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(23,'Pskow', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(24,'Borisovichi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(24,'Borissowitschi', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(25,'Penza', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(25,'Pensa', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(26,'Orenburg', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(26,'Orenburg', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(27,'Veliky Novgorod', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(27,'Weliki Nowgorod', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(28,'Murmansk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(28,'Murmansk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(29,'Lipetsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(29,'Lipezk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(30,'Kostroma', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(30,'Kostroma', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(31,'Kirov', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(31,'Kirow', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(32,'Kemerovo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(32,'Kemerowo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(33,'Voronezh', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(33,'Woronesch', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(34,'Vologda', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(34,'Wologda', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(35,'Volgograd', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(35,'Wolgograd', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(36,'Bryansk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(36,'Brjansk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(37,'Vladimir', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(37,'Wladimir', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(38,'Astrakhan', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(38,'Astrachan', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(39,'Arkhangelsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(39,'Archangelsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(40,'Blagoveshchensk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(40,'Blagoweschtschensk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(41,'Khabarovsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(41,'Chabarowsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(42,'Vladivostok', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(42,'Wladiwostok', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(43,'Barnaul', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(43,'Barnaul', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(44,'Cheboksary', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(44,'Tscheboksary', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(45,'Grozny', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(45,'Grosny', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(46,'Abakan', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(46,'Abakan', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(47,'Izhevsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(47,'Ischewsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(48,'Kyzyl', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(48,'Kysyl', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(49,'Yakutsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(49,'Jakutsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(50,'Yoshkar-Ola', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(50,'Joschkar-Ola', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(51,'Syktyvkar', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(51,'Syktywkar', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(52,'Petrozavodsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(52,'Petrosawodsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(53,'Mahachkala', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(53,'Machatschkala', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(54,'Ulan-Ude', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(54,'Ulan-Ude', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(55,'Ufa', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(55,'Ufa', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(56,'Chita', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(56,'Tschita', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(57,'Khanty-Mansiysk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(57,'Chanty-Mansijsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(58,'Tyumen', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(58,'Tjumen', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(59,'Stavropol', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(59,'Stawropol', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(60,'Saransk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(60,'Saransk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(61,'Samara', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(61,'Samara', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(62,'Perm', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(62,'Perm', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(63,'Novosibirsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(63,'Nowosibirsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(64,'Novokuznetsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(64,'Nowokusnezk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(65,'Nizhnii Novgorod', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(65,'Nischni Nowgorod', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(66,'Kursk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(66,'Kursk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(67,'Krasnoyarsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(67,'Krasnojarsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(68,'Kaliningrad', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(68,'Kaliningrad', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(69,'Kazan', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(69,'Kasan', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(70,'Irkutsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(70,'Irkutsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(71,'Belgorod', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(71,'Belgorod', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(72,'Krasnogorsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(72,'Krasnogorsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(74,'Kubinka', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(74,'Kubinka', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(79,'Yelizovo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(79,'Jelisowo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(80,'Almetyevsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(80,'Almetjewsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(88,'Vladikavkaz', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(88,'Wladikawkas', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(92,'Gelendzhik', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(92,'Gelendschik', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(93,'Gorno-Altaysk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(93,'Gorno-Altaisk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(98,'Yeysk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(98,'Jeisk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(101,'Yessentuki', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(101,'Jessentuki', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(107,'Kurgan', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(107,'Kurgan', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(112,'Lyubertsy', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(112,'Ljuberzy', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(114,'Maykop', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(114,'Maikop', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(119,'Mytishchi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(119,'Mytischtschi', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(120,'Naberezhnye Chelny', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(120,'Nabereschnyje Tschelny', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(126,'Nizhnevartovsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(126,'Nischnewartowsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(127,'Nizhnekamsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(127,'Nischnekamsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(128,'Nizhny Tagil', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(128,'Nischni Tagil', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(129,'Novy Urengoy', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(129,'Nowy Urengoi', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(136,'Pyatigorsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(136,'Pjatigorsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(143,'Tolyatti', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(143,'Toljatti', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(148,'Cherepovets', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(148,'Tscherepowez', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(156,'Velikiye Luki', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(156,'Welikije Luki', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(157,'Magnitogorsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(157,'Magnitogorsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(158,'Kolomna', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(158,'Kolomna', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(159,'Yalta', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(159,'Jalta', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(160,'Novoanninsky District', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(160,'Distrikt Novoanninsky', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(163,'Makarye', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(163,'Makarye', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(164,'Shatilovo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(164,'Shatilovo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(166,'Shestakovo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(166,'Schestakowo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(167,'Novaya Usman', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(167,'Nowaja Usman', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(168,'Stary Oskol', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(168,'Stary Oskol', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(169,'Solnechny', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(169,'Solnetschny', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(170,'Syzran', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(170,'Sysran', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(171,'Chichkovo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(171,'Chichkovo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(172,'Reutov', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(172,'Reutow', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(173,'Odintsovo', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(173,'Odinzowo', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(174,'Velsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(174,'Welsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(175,'Novodvinsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(175,'Nowodwinsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(176,'Ukhta', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(176,'Uchta', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(177,'Sosnogorsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(177,'Sosnogorsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(178,'Bratsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(178,'Bratsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(179,'Noyabrsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(179,'Nojabrsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(180,'Zhukovsky', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(180,'Schukowski', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(181,'Sarapul', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(181,'Sarapul', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(182,'Koryazhma', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(182,'Korjaschma', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(196,'Vsevolozhsky District', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(196,'Distrikt Vsevolozhsky', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(239,'Krestovskoye', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(239,'Krestovskoye', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(240,'Tatinets', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(240,'Tatinets', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(241,'Baku', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(241,'Baku', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(242,'Yerevan', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(242,'Jerewan', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(243,'Minsk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(243,'Minsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(244,'Babruysk', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(244,'Babrujsk', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(245,'Mogilev', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(245,'Mahiljou', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(246,'Kastsyukovichy', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(246,'Kastjukowitschy', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(247,'Grodno', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(247,'Hrodna', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(248,'Astana', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(248,'Astana', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(249,'Almaty', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(249,'Almaty', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(250,'Atyrau', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(250,'Atyrau', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(251,'Aktau', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(251,'Aqtau', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(252,'Aktobe', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(252,'Aqtöbe', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(253,'Pavlodar', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(253,'Pawlodar', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(254,'Bishkek', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(254,'Bischkek', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(255,'Kishinev', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(255,'Kischinjow', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(256,'Karaganda', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(256,'Qaraghandy', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(257,'Tashkent', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(257,'Taschkent', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(258,'Fergana', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(258,'Fargʻona', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(259,'Ashgabat', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(259,'Aşgabat', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(260,'Turkmenbashi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(260,'Turkmenbashi', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(261,'Tbilisi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(261,'Tiflis', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(262,'Ulaanbaatar', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(262,'Ulaanbaatar', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(263,'Kiev', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(263,'Kiew', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(264,'Odessa', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(264,'Odessa', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(265,'Simferopol', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(265,'Simferopol', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(266,'Sevastopol', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(266,'Sewastopol', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(267,'Osh', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(267,'Osch', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(268,'Shymkent', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(268,'Schymkent', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(269,'Belgrade', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(269,'Belgrad', 'de');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(270,'Hanoi', 'en');
            INSERT INTO tbl_trcity (trParentId, name, langId) VALUES(270,'Hanoi', 'de');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}