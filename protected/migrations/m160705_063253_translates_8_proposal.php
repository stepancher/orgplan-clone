<?php

class m160705_063253_translates_8_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2508', 'en', 'This form should be filled in until Aug. 04, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2509', 'en', 'Your Company - for Official exhibition catalogue');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2510', 'en', 'The company information provided in English is only allocated in the English-language part of catalogue.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2511', 'en', 'Company\'s Name');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2512', 'en', 'Zip code/Postcode');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2513', 'en', 'Country');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2514', 'en', 'State(Region)');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2515', 'en', 'City');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2516', 'en', 'Street');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2517', 'en', 'House');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2518', 'en', 'Telephone');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2519', 'en', 'Fax');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2520', 'en', 'Website');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2521', 'en', 'E-mail');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2522', 'en', 'Your Company\'s Activity', 'Up to 430 symbols for free, including spaces');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2523', 'en', 'Additional symbols');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2524', 'en', 'Your Company - for Official exhibition catalogue (Russian-language version)');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2525', 'en', 'The company information provided in Russian is only allocated in the Russian-language part of catalogue.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2526', 'en', 'Название компании');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2527', 'en', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2528', 'en', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2529', 'en', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2530', 'en', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2531', 'en', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2532', 'en', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2533', 'en', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2534', 'en', 'Факс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2535', 'en', 'Сайт');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2536', 'en', 'Почта');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2537', 'en', 'Информация о компании', 'Up to 430 symbols for free, including spaces');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2538', 'en', 'Дополнительные знаки');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2539', 'en', 'Should this Application be not received/accepted by the Organizer by Aug.26, 2016 or the participant\'s failure to complete the form for information on the company on the exhibition website or the form is only partially completed, the Organizer shall reserve the right not to publish any information on the participant or is free to complete missed sections and/or translate the information provided by the participant and/or place it in the appropriate catalogue section and/or exhibition website at its own discretion. Failure on the part of the participant to specify any section for information placement or more sections were specified then paid under this Application, the Organizer shall be free to select the section/sections for placement of the information on the participant at its own discretion.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2540', 'en', 'Catalogue sections');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2541', 'en', 'Укажите разделы в которых необходимо разместить информацию о Вашей компании, дополнительно отметьте разделы для размещения Вашего логотипа.\n<br><br>\nИнформация о компании-участнике размещается бесплатно в одном разделе каталога. \n');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2542', 'en', '1. Tractors', '1. ТРАКТОРЫ<br>\n1.1 Колесные тракторы<br>\n1.2 Гусеничные тракторы<br>\n1.3 Специализированные тракторы<br>\n1.4 Другое из группы «Тракторы»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2543', 'en', '2. Mobile loading machines', '2. МОБИЛЬНЫЕ ПОГРУЗОЧНЫЕ МАШИНЫ<br>\n2.1 Компактные погрузчики для животноводческих ферм<br>\n2.2 Телескопические погрузчики<br>\n2.3 Колесные погрузчики<br>\n2.4 Фронтальные и тыловые погрузчики<br>\n2.5 Вилочные погрузчики<br>\n2.6 Экскаваторы, краны<br>\n2.7 Другое из группы «Мобильные погрузочные машины»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2544', 'en', '3. Machinery and equipment for soil working and seed-bed preparation', '3. МАШИНЫ ДЛЯ ОБРАБОТКИ ПОЧВЫ И ПОДГОТОВКИ ПОСЕВА<br>\n3.1 Плуги<br>\n3.2 Почвоуглубители<br>\n3.3 Культиваторы<br>\n3.4 Бороны<br>\n3.5 Почвообрабатывающие фрезы<br>\n3.6 Комбинированные почвообрабатывающие агрегаты<br>\n3.7 Почвообрабатывающие агрегаты с приводом от вала отбора мощности<br>\n3.8 Катки<br>\n3.9 Другое из группы «Машины и обору­дование для обработки почвы и подготовки посева»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2545', 'en', '4. Machinery and equipment for drilling and sowing', '4. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ ПОСЕВА И РЯДОВОГО СЕВА<br>\n4.1 Рядовые сеялки<br>\n4.2 Сеялки точного высева<br>\n4.3 Машины для посадки с мульчирующей пленкой<br>\n4.4 Стерневые сеялки<br>\n4.5 Комбинации рядовых сеялок<br>\n4.6 Рядовые сеялки для травы<br>\n4.7 Сеялки для повторного пересева<br>\n4.8 Посевные и посадочные машины<br>\n4.9 Сеялки для опытных участков<br>\n4.10 Другое из группы «Машины и обору­дование для посева и рядового сева»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2546', 'en', '5. Machinery and equipment for fertilizing', '5. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ ВНЕСЕНИЯ УДОБРЕНИЙ<br>\n5.1 Внесение органических удобрений<br>\n5.2 Внесение минеральных удобрений<br>\n5.3 Разбрасыватели удобрений<br>\n5.4 Внесение жидких удобрений<br>\n5.5 Другое из группы «Машины и оборудование для внесения удобрений»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2547', 'en', '6. Machinery and equipment for plant protection', '6. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ ЗАЩИТЫ РАСТЕНИЙ<br>\n6.1 Машины и оборудование для химической защиты растений<br>\n6.2 Машины и оборудование для механической защиты растений<br>\n6.3 Машины и оборудование для термической защиты растений<br>\n6.4 Другое из группы «Машины и обору­дование для защиты растений»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2548', 'en', '7. Machinery and equipment for irrigation and drainage', '7. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ ОРОШЕНИЯ И ВОДООТВОДА<br>\n7.1 Системы орошения и принадлежности<br>\n7.2 Машины и оборудование для водоотвода<br>\n7.3 Другое из группы «Машины и обору­дование для орошения и водоотвода»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2549', 'en', '8. Machinery and equipment for harvesting', '8. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ УБОРКИ УРОЖАЯ<br>\n8.1 Машины и оборудование для комбайновой уборки<br>\n8.2 Машины и оборудование для уборки корнеплодов<br>\n8.3 Машины и оборудование для прореживания<br>\n8.4 Машины и оборудование для покоса<br>\n8.5 Машины и оборудование для обработки скошенного материала<br>\n8.6 Машины и оборудование для прессования в тюки<br>\n8.7 Другое из группы «Машины и оборудование для уборки урожая»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2550', 'en', '9. Machinery and equipment for harvest conditioning, conveying, preservation and storage', '9. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ СОРТИРОВКИ, ТРАНСПОРТИРОВКИ, ОБРАБОТКИ И ХРАНЕНИЯ УРОЖАЯ<br>\n9.1 Машины и оборудование для зерна<br>\n9.2 Машины и оборудование для картофеля<br>\n9.3 Машины и оборудование для свеклы<br>\n9.4 Машины и оборудование для силоса и соломы<br>\n9.5 Прочие машины и оборудование<br>\n9.6 Другое из группы «Машины и обору­дование для сортировки, транспортировки, обработки и хранения урожая»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2551', 'en', '10. Machinery and equipment for fruit, vegetables and other special crops', '10. МАШИНЫ И ОБОРУДОВАНИЕ ДЛЯ ФРУКТОВ, ОВОЩЕЙ И ДРУГИХ КУЛЬТУР<br>\n10.1 Оборудование для садоводства<br>\n10.2 Овощеводство<br>\n10.3 Оборудование для других культур<br>\n10.4 Прочие машины и оборудование<br>\n10.5 Другое из группы «Машины и обору­дование для фруктов, овощей и других культур»<br>');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2552', 'en', '11. Components, spare parts, accessories', '11. КОМПЛЕКТУЮЩИЕ, ЗАПАСНЫЕ ЧАСТИ, ПРИНАДЛЕЖНОСТИ<br>\n11.1 Корпуса, надстройки, кабины, сидения<br>\n11.2 Электрика и электроника для транспортных средств<br>\n11.3 Тяговые устройства<br>\n11.4 Гидравлика<br>\n11.5 Покрышки и колеса, тормоза и рулевое управление<br>\n11.6 Кондиционирование воздуха<br>\n11.7 Снабжение запасными частями<br>\n11.8 Другое из группы «Комплектующие, запасные части, принадлежности»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2553', 'en', '12. Agricultural electronics, Equipment for measuring and weighing', '12. СЕЛЬСКОХОЗЯЙСТВЕННАЯ ЭЛЕКТРОНИКА, ОБОРУДОВАНИЕ ДЛЯ ИЗМЕРЕНИЯ И ВЗВЕШИВАНИЯ<br>\n12.1 Сельскохозяйственная электроника<br>\n12.2 Оборудование для измерения и взвешивания<br>\n12.3 Точное ведение сельского хозяйства<br>\n12.4 Другое из группы «Сельскохозяйственная электроника, оборудование для измерения и взвешивания»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2554', 'en', '13. Management, services, organizations, management', '13. УПРАВЛЕНИЕ, СЛУЖБЫ, ОРГАНИЗАЦИИ<br>\n13.1 Управление<br>\n13.2 Консультирование<br>\n13.3 Программное обеспечение<br>\n13.4 Финансирование<br>\n13.5 Службы<br>\n13.6 Технические книги, отраслевые журналы, издательства<br>\n13.7 Исследования, наука - научные сообщества<br>\n13.8 Обучение<br>\n13.9 Общества, организации<br>\n13.10 Другое из группы «Управление, службы, организации»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2555', 'en', '14. Special shows', '14. СПЕЦИАЛЬНЫЕ ВЫСТАВКИ<br>\n14.1 Информационные показы (Министерства)<br>\n14.2 Специальные показы (общества)<br>\n14.3 Совместные показы (фирмы)<br>\n14.4 Торговые ярмарки и выставки<br>\n14.5 Другое из группы «Специальные выставки»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2556', 'en', '15. Used machine trade', '15. ТОРГОВЛЯ ПОДЕРЖАННЫМ ОБОРУДОВАНИЕМ<br>\n15.1 Подержанное оборудование на продажу (журналы, Интернет)<br>\n15.2 Финансирование (подержанное оборудование)<br>\n15.3 Снабжение (подержанное оборудование)<br>\n15.4 Экспедиторы (подержанное оборудование)<br>\n15.5 Подержанные детали на продажу (журналы, Интернет)<br>\n15.6 Таможенная очистка<br>\n15.7 Другое из группы «Торговля подержанным оборудованием»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2557', 'en', '16. Bioenergy', '16. БИОЭНЕРГЕТИКА<br>\n16.1 Установки для производства биоэнергии<br>\n16.2 Другое из группы «Биоэнергетика»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2558', 'en', '17. Biofuels, liquid. biodiesel, vegetable oil, methanol, ethanol', '17. БИОТОПЛИВО<br>\n17.1 Производство, хранение<br>\n17.2 Использование топлива<br>\n17.3 Торговля биотопливом<br>\n17.4 Другое из группы «Биотопливо»');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2560', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2561', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2562', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2563', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2564', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2565', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2566', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2567', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2568', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2569', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2570', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2571', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2572', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2573', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2574', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2575', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2576', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2577', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2578', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2579', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2580', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2581', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2582', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2583', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2584', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2585', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2586', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2587', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2588', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2589', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2590', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2591', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2592', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2593', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2594', 'en', 'Company information');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2595', 'en', 'Logo');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2596', 'en', 'Total selected');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2597', 'en', 'Sections');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2598', 'en', 'Размещение логотипов');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2599', 'en', 'Logos');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2600', 'en', 'Requirements to the file prepared for publication in the official catalogue: Logotype - black and white picture, format *.eps or *.tif, 300dpi, scale 1:1.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2601', 'en', 'Advertising pages in the official catalogue');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2602', 'en', 'The Organizer shall be entitled to choose the place for the advertising pages in the catalogue. The participant wishing to place its advertising page in the first section of the catalogue or in the specific section it must order and pay for the \"Special Placement\" service. If the \"Special Placement\" service is ordered but no section chosen the participant\'s advertising page will be placed in the first section of the catalogue.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2603', 'en', 'Advertising page (color, 140x215 mm)');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2604', 'en', 'Advertising page');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2605', 'en', 'Requirements to the file prepared for publication in the official catalogue: advertising page - CMYK imag, dimensions 140x215 mm, on perimeter +5mm \"bleed page\", format*.eps or *.tif, 350 dpi.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2606', 'en', '\"Special Placement\" service');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2607', 'en', 'Section', 'Укажите раздел для специального размещения');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2608', 'en', 'To place the logotype in the catalogue, the participant must provide its black and white version in the electronic format by Aug.04, 2016. To place the advertising page, the participant must provide its makeup page in the electronic format by Aug.04, 2016. Should the e-version be not provided by the fixed deadline or not complying with the requirements, no placement claims will be accepted or funds paid back.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2609', 'en', 'Advertisement pages in magazine AGROREPORT', '<div class=\"header\">ЖУРНАЛ AGROREPORT</div>\n<div class=\"body\">\nAgroreport – специализированный журнал о сельхозтехнике (новинки/эксклюзивные репортажи/сравнительные тесты/независимые испытания). Предложение действительно для Экспонентов, размещающих рекламу в номерах, приуроченных к выставке АГРОСАЛОН 2016. Дополнительную информацию вы можете получить по тел. +7(495)784-37-56 доб.251.\n</div>');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2610', 'en', 'Requirements to the file prepared for publication: advertising page - CMYK imag, dimensions 210x297 mm, on perimeter +5mm \"bleed page\", format*.eps or *.tif, 350 dpi. Layout should contain the note \"Advertising\" according to the Federal law-3В dated March 13, 2006 \"About Advertising\"');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2611', 'en', 'Advertisement page A4 in No.4 July/August');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2612', 'en', 'Advertisement page');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2613', 'en', 'Advertising layout in No.4 should be provided up to July 15, 2016. Issue date July 27, 2016.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2614', 'en', 'Advertisement page A4 in No.5 September/October');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2615', 'en', 'Advertisement page');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2616', 'en', 'Advertising layout in No.5 should be provided up to September 15, 2016. Issue date July 27, 2016.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2617', 'en', 'Advertisement page A4 in No.4 and No.5');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2618', 'en', 'Advertisement page');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2620', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br><br>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br><br>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2622', 'en', 'Payments shall be effected in USD. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2619', 'currency');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2127', 'ru', 'USD');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2127', 'en', 'USD');
			UPDATE {{trattributeclass}} SET `tooltipCustom`='1. TRACTORS<br>\n1.1. Wheeled tractors<br>\n1.2. Crawler tractors<br>\n1.3. Special tractors<br>\n1.4. Others in the Tractors Group' WHERE `id`='2933';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='2. MOBILE LOADING MACHINES<br>\n2.1. Compact loaders<br>\n2.2. Telehandler<br>\n2.3. Wheeled loaders<br>\n2.4. Front and rear loaders<br>\n2.5. Forklifts<br>\n2.6. Excavators, cranes<br>\n2.7. Others in Mobile loading machines Group' WHERE `id`='2934';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='3. MACHINERY AND EQUIPMENT FOR SOIL WORKING AND SEED-BED PREPARATION<br>\n3.1. Ploughs<br>\n3.2. Subsoilers<br>\n3.3. Cultivators<br>\n3.4. Field and crop harrows<br>\n3.5. Rotary tillers<br>\n3.6. Seed-bed combinations<br>\n3.7. Seed-bed combinations driven soil-working machinery<br>\n3.8. Rollers<br>\n3.9. Others in the Machinery and equipment for soil working and seed-bed preparation' WHERE `id`='2935';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='4. MACHINERY AND EQUIPMENT FOR DRILLING AND SOWING<br>\n4.1. Seed drills<br>\n4.2. Precision seed drills<br>\n4.3. Mulch drilling machines<br>\n4.4. Direct seed drills<br>\n4.5. Seed drill combinations<br>\n4.6. Grass seed drills<br>\n4.7. Re-seeding drills<br>\n4.8. Planters<br>\n4.9. Plot seeders<br>\n4.10. Others in the Machinery and equipment for drilling and sowing' WHERE `id`='2936';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='5. MACHINERY AND EQUIPMENT FOR FERTILIZING<br>\n5.1. Organic Fertilizing<br>\n5.2. Mineral Fertilizing<br>\n5.3. Fertilizer spreaders<br>\n5.4. Liquid fertilizing<br>\n5.5. Others in the Machinery and equipment for fertilizing' WHERE `id`='2937';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='6. MACHINERY AND EQUIPMENT FOR PLANT PROTECTION<br>\n6.1. Machinery and equipment for chemical plant protection<br>\n6.2. Machinery and equipment for mechanical plant protection<br>\n6.3. Machinery and equipment for thermal plant protection<br>\n6.4. Others in the Machinery and equipment for plant protection<br>' WHERE `id`='2938';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='7. MACHINERY AND EQUIPMENT FOR IRRIGATION AND DRAINAGE<br>\n7.1 Irrigation systems and accessories<br>\n7.2. Machinery and equipment for drainage<br>\n7.3. Others in the Machinery and equipment for irrigation and drainage' WHERE `id`='2939';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='8. MACHINERY AND EQUIPMENT FOR HARVESTING<br>\n8.1. Machinery and equipment for combining<br>\n8.2. Machinery and equipment for lifting<br>\n8.3. Machinery and equipment for chopping<br>\n8.4. Machinery and equipment for mowers<br>\n8.5. Machinery and equipment for conditioning mowed material<br>\n8.6. Machinery and equipment for baling<br>\n8.7. Others in the Machinery and equipment for harvesting' WHERE `id`='2940';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='9. MACHINERY AND EQUIPMENT FOR HARVEST CONDITIONING, CONVEYING, PRESERVATION AND STORAGE<br>\n9.1. Cross-segment machinery and equipment<br>\n9.2. Machinery and equipment for grain<br>\n9.3. Machinery and equipment for potatoes<br>\n9.4. Machinery and equipment for beets<br>\n9.5. Machinery and equipment for grain crops<br>\n9.6. Others in the Machinery and equipment for harvest conditioning, conveying, preservation and storage' WHERE `id`='2941';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='10. MACHINERY AND EQUIPMENT FOR FRUIT, VEGETABLES AND OTHER SPECIAL CROPS<br>\n10.1. Fruitgrowing<br>\n10.2. Vegetable growing<br>\n10.3. Other special crops<br>\n10.4. Cross-segment machinery and equipment<br>\n10.5. Others in the Machinery and equipment for fruit, vegetables and other special crops' WHERE `id`='2942';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='11. COMPONENTS, SPARE PARTS, ACCESSORIES<br><br>\n11.1. Chassis, superstructures, cabs, seats<br>\n11.2. Vehicle electrics and electronics<br>\n11.3. Motive power engineering<br>\n11.4. Hydraulics<br>\n11.5. Tyres and wheels, brakes and steering<br>\n11.6. Air conditioning<br>\n11.7. Spare parts logistics<br>\n11.8. Others in the Components, spare parts and accessories' WHERE `id`='2943';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='12. AGRICULTURAL ELECTRONICS, EQUIPMENT FOR MEASURING AND WEIGHING<br>\n12.1. Agricultural electronics<br>\n12.2. Equipment for measuring and weighing<br>\n12.3. Precision Farming<br>\n12.4. Others in the Agricultural electronics, equipment for measuring and weighing' WHERE `id`='2944';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='13. MANAGEMENT, SERVICES, ORGANIZATIONS, MANAGEMENT<br>\n13.1. Management<br>\n13.2. Consultancy<br>\n13.3 Software<br>\n13.4. Financing<br>\n13.5. Services<br>\n13.6. Technical books, trade journals, publishers<br>\n13.7. Research, science – academia<br>\n13.8. Training<br>\n13.9. Associations, organizations<br>\n13.10. Others in the Management, services, organizations' WHERE `id`='2945';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='14. SPECIAL SHOWS<br>\n14.1. Information shows (Ministries)<br>\n14.2. Special shows (associations)<br>\n14.3. Collective shows (firms)<br>\n14.4. Trade fairs and exhibitions<br>\n14.5. Others in the Special shows' WHERE `id`='2946';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='15. USED MACHINE TRADE<br>\n15.1. Used machines for sale (magazines, Internet)<br>\n15.2. Financing (used machines)<br>\n15.3. Logistics (used machines)<br>\n15.4. Forwarders (used machines)<br>\n15.5. Used machine parts for sale (magazines, Internet)<br>\n15.6. Customs clearance<br>\n15.7. Others in the Used machine trade' WHERE `id`='2947';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='16. BIOENERGY<br>\n16.1 Energy-producing plants<br>\n16.2. Others in the Bioenergy' WHERE `id`='2948';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='17. BIOFUELS, LIQUID. BIODIESEL, VEGETABLE OIL, METHANOL, ETHANOL<br>\n17.1.Production, storage<br>\n17.2. Fuel use<br>\n17.3. Trade with biofuels, liquid<br>\n17.4. Others in the Biofuels, liquid biodiesel, vegetable oil, methanol, ethanol' WHERE `id`='2949';
			UPDATE {{trattributeclass}} SET `placeholder`='До 430 знаков бесплатно, включая пробелы' WHERE `id`='2928';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">MAGAZINE AGROREPORT</div>\n<div class=\"body\">\nAGROREORT – specialized magazine about agricultural machinery (technological innovations/exclusive report/competitive tests/independent testing). This offer is valid for the Exhibitors, placed advertisement in issue of magazine timed to the fair AGROSALON 2016. Additional information you can get by phone +7(495)784-37-56 ext.251.\n</div>' WHERE `id`='2999';
			UPDATE {{trattributeclass}} SET `placeholder`='Choose the section for the \"Special Placement\" service' WHERE `id`='2997';
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2113', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2114', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2115', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2116', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2117', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2118', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2119', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2120', 'en', 'psc.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2121', 'en', 'psc.');
		";
	}
}