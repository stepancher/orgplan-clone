<?php

class m160418_215734_update_units_proposal_n3 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "

		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclassproperty}} SET `value`='м.п.' WHERE `id`='967';
			UPDATE {{attributeclassproperty}} SET `value`='м.п.' WHERE `id`='985';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='989';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='991';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='992';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='993';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='994';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1038';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1071';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1072';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1073';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1076';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1077';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1078';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1079';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1084';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1085';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1086';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1088';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1090';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1091';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1092';
			UPDATE {{attributeclassproperty}} SET `value`='кв.м.' WHERE `id`='1093';
			UPDATE {{attributeclassproperty}} SET `value`='п.м.' WHERE `id`='1094';
	    ";
	}
}