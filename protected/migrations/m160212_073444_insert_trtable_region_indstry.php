<?php

class m160212_073444_insert_trtable_region_indstry extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{trregion}}
			ADD COLUMN `shortUrl` VARCHAR(255) NULL AFTER `name`;
			
			DELETE FROM {{region}} WHERE `name` IS NULL;
			
			DELETE FROM {{trregion}} WHERE `langId` = 'ru';
			
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`)
			(SELECT `id`, 'ru', `name`, `shortUrl` FROM {{region}} WHERE `name` != '' AND `name` IS NOT NULL);
			
			
			ALTER TABLE {{trindustry}}
			ADD COLUMN `shortNameUrl` VARCHAR(255) NULL AFTER `name`;
			
			DELETE FROM {{industry}} WHERE `name` IS NULL;
			
			DELETE FROM {{trindustry}} WHERE `langId` = 'ru';
			
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`, `shortNameUrl`)
			(SELECT `id`, 'ru', `name`, `shortNameUrl` FROM {{industry}} WHERE `name` != '' AND `name` IS NOT NULL);
		";
	}
}