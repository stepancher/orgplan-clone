<?php

class m160428_235712_update_dates_for_DLG_calendar_tasks extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{calendarfairdays}} SET `startDate`='2017-11-11', `endDate`='2017-11-12' WHERE `id`='9';
			UPDATE {{calendarfairdays}} SET `startDate`='2017-11-11', `endDate`='2017-11-14' WHERE `id`='10';
			UPDATE {{calendarfairdays}} SET `startDate`='2017-11-12', `endDate`='2017-11-18' WHERE `id`='11';
			UPDATE {{calendarfairdays}} SET `startDate`='2017-11-18', `endDate`='2017-11-19' WHERE `id`='12';
		";
	}
}