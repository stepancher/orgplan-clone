<?php

class m170303_111622_add_exdb_fair_raw_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fair_statistic_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fair_statistic_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DE FAULT 0;
                DECLARE f_areaSpecialExpositions INT DEFAULT 0;
                DECLARE f_squareNet INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_exhibitorsForeign INT DEFAULT 0;
                DECLARE f_exhibitorsLocal INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_visitorsForeign INT DEFAULT 0;
                DECLARE f_visitorsLocal INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statisticsDate TEXT DEFAULT '';
                DECLARE f_countries_count TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT d.id,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.specialShow, ',', 4), SUBSTRING_INDEX(d.specialShow, ',', 3), ''), 2) AS areaSpecialExpositions,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.netSqm, ',', 4), SUBSTRING_INDEX(d.netSqm, ',', 3), ''), 2) AS squareNet,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 4), SUBSTRING_INDEX(d.exhibitors, ',', 3), ''), 2) AS members,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 3), SUBSTRING_INDEX(d.exhibitors, ',', 2), ''), 2) AS exhibitorsForeign,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 2), SUBSTRING_INDEX(d.exhibitors, ',', 1), ''), 2) AS exhibitorsLocal,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 4), SUBSTRING_INDEX(d.visitors, ',', 3), ''), 2) AS visitors,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 3), SUBSTRING_INDEX(d.visitors, ',', 2), ''), 2) AS visitorsForeign,
                                SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 2), SUBSTRING_INDEX(d.visitors, ',', 1), ''), 2) AS visitorsLocal,
                                d.statistics_with_en AS statistics,
                                d.stat_year_en AS statisticsDate,
                                d.exhibitors_profile_en AS countriesCount
                            FROM
                            (SELECT t.id, t.exhibitors_profile_en, t.statistics_with_en, t.stat_year_en, t.row_1, t.stats_en,
                            CASE WHEN LOCATE('Net sqm', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Net sqm', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Net sqm', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Net sqm', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Net sqm', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Net sqm', row_7) != 0 THEN row_7
                                 ELSE NULL END netSqm,
                            CASE WHEN LOCATE('Exhibitors', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Exhibitors', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Exhibitors', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Exhibitors', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Exhibitors', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Exhibitors', row_7) != 0 THEN row_7
                                 ELSE NULL END exhibitors,
                            CASE WHEN LOCATE('Visitors', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Visitors', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Visitors', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Visitors', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Visitors', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Visitors', row_7) != 0 THEN row_7
                                 ELSE NULL END visitors,
                            CASE WHEN LOCATE('Special show', row_2) != 0 THEN row_2
                                 WHEN LOCATE('Special show', row_3) != 0 THEN row_3
                                 WHEN LOCATE('Special show', row_4) != 0 THEN row_4
                                 WHEN LOCATE('Special show', row_5) != 0 THEN row_5
                                 WHEN LOCATE('Special show', row_6) != 0 THEN row_6
                                 WHEN LOCATE('Special show', row_7) != 0 THEN row_7
                                 ELSE NULL END specialShow
                            FROM
                            (SELECT ed.id, ed.stats_en, ed.exhibitors_profile_en, ed.statistics_with_en, ed.stat_year_en,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',1), SUBSTRING_INDEX(ed.stats_en,'',0),'') AS `row_1`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',2), SUBSTRING_INDEX(ed.stats_en,'',1),'') AS `row_2`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',3), SUBSTRING_INDEX(ed.stats_en,'',2),'') AS `row_3`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',4), SUBSTRING_INDEX(ed.stats_en,'',3),'') AS `row_4`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',5), SUBSTRING_INDEX(ed.stats_en,'',4),'') AS `row_5`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',6), SUBSTRING_INDEX(ed.stats_en,'',5),'') AS `row_6`,
                            REPLACE(SUBSTRING_INDEX(ed.stats_en,'',7), SUBSTRING_INDEX(ed.stats_en,'',6),'') AS `row_7`
                                    FROM {$expodataRaw}.expo_data ed) t) d
                                    LEFT JOIN {$expodata}.{{exdbfairstatistic}} fs ON fs.exdbId = d.id
                                    WHERE fs.id IS NULL ORDER BY d.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                FETCH copy INTO f_id, 
                    f_areaSpecialExpositions, 
                    f_squareNet, 
                    f_members, 
                    f_exhibitorsForeign, 
                    f_exhibitorsLocal,
                    f_visitors, 
                    f_visitorsForeign, 
                    f_visitorsLocal, 
                    f_statistics, 
                    f_statisticsDate,
                    f_countries_count;
                
                IF done THEN
                    LEAVE read_loop;
                END IF;
                
                START TRANSACTION;
                    INSERT INTO {$expodata}.{{exdbfairstatistic}} (`exdbId`,
                        `areaSpecialExpositions`,
                        `squareNet`,
                        `members`,
                        `exhibitorsForeign`,
                        `exhibitorsLocal`,
                        `visitors`,
                        `visitorsForeign`,
                        `visitorsLocal`,
                        `statistics`,
                        `statisticsDate`,
                        `exdbCountriesCount`) 
                        VALUES (f_id,
                        f_areaSpecialExpositions,
                        f_squareNet,
                        f_members,
                        f_exhibitorsForeign,
                        f_exhibitorsLocal,
                        f_visitors,
                        f_visitorsForeign,
                        f_visitorsLocal,
                        f_statistics,
                        f_statisticsDate,
                        f_countries_count
                    );
                COMMIT;
                
                END LOOP;
                CLOSE copy;
            END;

            CALL {$expodata}.`add_exdb_fair_statistic_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairs_to_exdb_db`();
            CALL {$expodata}.`update_exdb_fair_info_id`();
            CALL {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`();
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fair_raw_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fair_raw_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_raw TEXT DEFAULT '';
                DECLARE f_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.exdbRaw, f.id FROM {$expodata}.{{exdbfair}} ef
                                        LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND ef.exdbCrc = f.exdbCrc) OR ef.exdbCrc IS NULL)
                                        WHERE f.exdbRaw IS NULL
                                        AND ef.exdbRaw IS NOT NULL
                                        AND f.id IS NOT NULL;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_raw, f_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {$db}.{{fair}} SET `exdbRaw` = f_raw WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_fair_raw_to_db`();
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fair_raw_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}