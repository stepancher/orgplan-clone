<?php

class m160321_105953_alter_calendarexhibition__remove_standart extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	private function upSql()
	{
		return '
			ALTER TABLE {{calendarexhibition}} DROP COLUMN `standard`;
		';
	}
}