<?php

class m160707_143749_refactoring_venues_country extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return " 
			ALTER TABLE {{country}} 
			DROP COLUMN `name`;
			
			INSERT INTO {{trcountry}} (`id`, `trParentId`, `langId`, `name`) VALUES (NULL, '2', 'en', 'Germany');
			INSERT INTO {{trcountry}} (`id`, `trParentId`, `langId`, `name`) VALUES (NULL, '2', 'ru', 'Германия');
 		";
	}

	public function downSql()
	{
		return TRUE;
	}
}