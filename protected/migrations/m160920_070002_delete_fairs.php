<?php

class m160920_070002_delete_fairs extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE id in 
            (
            '3955',
            '3956',
            '3989',
            '4020',
            '4023',
            '4026',
            '4028',
            '4070',
            '4095',
            '2078',
            '2080',
            '4130',
            '4403',
            '4518',
            '4548',
            '4559',
            '4624',
            '4635',
            '4711',
            '4723',
            '4764',
            '4808',
            '4858',
            '5041',
            '5073',
            '10846',
            '10855',
            '11068',
            '11069'
            );
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}