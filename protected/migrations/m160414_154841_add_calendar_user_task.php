<?php

class m160414_154841_add_calendar_user_task extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			DROP TABLE IF EXISTS {{calendaruserstasks}};

			CREATE TABLE {{calendaruserstasks}} (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			  `userId` int(11) NOT NULL,
			  `fairId` int(11) DEFAULT NULL,
			  `parent` varchar(45) DEFAULT NULL,
			  `name` varchar(245) NOT NULL,
			  `startDate` datetime NOT NULL,
			  `endDate` datetime NOT NULL,
			  `completeDate` datetime DEFAULT NULL,
			  `desc` varchar(245) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
		";
	}

	private function downSql() {

		return "DROP TABLE IF EXISTS {{calendaruserstasks}};";
	}

}