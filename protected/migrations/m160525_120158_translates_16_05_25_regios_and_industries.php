<?php

class m160525_120158_translates_16_05_25_regios_and_industries extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('1', 'en', 'Altai Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('2', 'en', 'Amur Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('3', 'en', 'Arkhangelsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('4', 'en', 'Astrakhan Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('5', 'en', 'Belgorod Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('6', 'en', 'Bryansk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('7', 'en', 'Vladimir Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('8', 'en', 'Volgograd Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('9', 'en', 'Vologda Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('10', 'en', 'Voronezh Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('11', 'en', 'Jewish Autonomous Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('12', 'en', 'Trans-Baikal Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('13', 'en', 'Ivanovo Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('14', 'en', 'Irkutsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('15', 'en', 'Kabardino-Balkarian Republic');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('16', 'en', 'Kaliningrad Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('17', 'en', 'Kaluga Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('18', 'en', 'Kamchatka Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('19', 'en', 'Karachayevo-Circassian Republic');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('20', 'en', 'Kemerovo Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('21', 'en', 'Kirov Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('22', 'en', 'Kostroma Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('23', 'en', 'Krasnodar Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('24', 'en', 'Krasnoyarsk Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('25', 'en', 'Kurgan Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('26', 'en', 'Kursk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('27', 'en', 'Leningrad Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('28', 'en', 'Lipetsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('29', 'en', 'Magadan Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('30', 'en', 'Moscow');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('31', 'en', 'Moscow Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('32', 'en', 'Murmansk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('33', 'en', 'Nenets Autonomous Area');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('34', 'en', 'Nizhny Novgorod Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('35', 'en', 'Novgorod Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('36', 'en', 'Novosibirsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('37', 'en', 'Omsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('38', 'en', 'Orenburg Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('39', 'en', 'Orel Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('40', 'en', 'Penza Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('41', 'en', 'Perm Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('42', 'en', 'Primorye Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('43', 'en', 'Pskov Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('44', 'en', 'Republic of Adygeya');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('45', 'en', 'Republic of Altai');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('46', 'en', 'Republic of Bashkortostan');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('47', 'en', 'Republic of Buryatia');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('48', 'en', 'Republic of Daghestan');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('49', 'en', 'Republic of Ingushetia');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('50', 'en', 'Republic of Kalmykia');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('51', 'en', 'Republic of Karelia');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('52', 'en', 'Komi Republic');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('53', 'en', 'Republic of Mari El');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('54', 'en', 'Republic of Mordovia');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('55', 'en', 'Republic of Sakha (Yakutia)');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('56', 'en', 'Republic of North Ossetia – Alania');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('57', 'en', 'Republic of Tatarstan');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('58', 'en', 'Republic of Tuva');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('59', 'en', 'Republic of Khakassia');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('60', 'en', 'Rostov Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('61', 'en', 'Ryazan Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('62', 'en', 'Samara Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('63', 'en', 'St. Petersburg');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('64', 'en', 'Saratov Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('65', 'en', 'Sakhalin Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('66', 'en', 'Sverdlovsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('67', 'en', 'Smolensk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('68', 'en', 'Stavropol Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('69', 'en', 'Tambov Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('70', 'en', 'Tver Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('71', 'en', 'Tomsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('72', 'en', 'Tula Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('73', 'en', 'Tyumen Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('74', 'en', 'Udmurtian Republic');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('75', 'en', 'Ulyanovsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('76', 'en', 'Khabarovsk Territory');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('77', 'en', 'Khanty-Mansi Autonomous Area – Yugra');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('78', 'en', 'Chelyabinsk Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('79', 'en', 'Chechen Republic');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('80', 'en', 'Chuvash Republic');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('81', 'en', 'Chukotka Autonomous Area');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('82', 'en', 'Yamal-Nenets Autonomous Area');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('83', 'en', 'Yaroslavl Region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('88', 'en', 'Arkhangelsk Region with Nenets Autonomous Area');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('89', 'en', 'Arkhangelsk Region with Nenets Autonomous Area');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('93', 'en', 'Republic of Crimea');
			
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('1', 'en', 'Furniture for home and office, interior design, home goods');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('2', 'en', 'Construction, finishing materials and equipment');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('3', 'en', 'Automotive engineering, metalworking manufacturing, machinery, industrial equipment');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('4', 'en', 'Security and safety, fire safety, occupational safety');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('5', 'en', 'Forestry and woodworking');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('6', 'en', 'Chemicals. Plastics');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('7', 'en', 'Steel and metal manufacturing');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('8', 'en', 'Energy');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('9', 'en', 'Medical, health, hygiene');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('10', 'en', 'Aerospace');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('11', 'en', 'Agriculture');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('12', 'en', 'Pets products. Veterinary ');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('13', 'en', 'Food processing and manufacturing: equipment and ingredients');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('14', 'en', 'Food - Retail');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('16', 'en', 'Beauty, cosmetics, perfume');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('17', 'en', 'Food service and hospitality');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('18', 'en', 'Electric engineering');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('19', 'en', 'IT and Telecommunications');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('20', 'en', 'Jewellery and watches');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('21', 'en', 'Electronics and parts');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('22', 'en', 'Cars, bicycles, motorcycles');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('23', 'en', 'Culture and art');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('24', 'en', 'Religion');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('25', 'en', 'Pharmaceutical');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('26', 'en', 'Packaging and labeling');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('27', 'en', 'Travel and recreation');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('28', 'en', 'High tech. Science. Innovations');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('29', 'en', 'Transportation, material handling, logistics');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('30', 'en', 'Governmental services');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('31', 'en', 'Houseware, dishes, gifts');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('32', 'en', 'Flowers. Floral decor. Gardening. Landscape design');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('35', 'en', 'Recreation, hunting, fishing');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('36', 'en', 'Education');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('37', 'en', 'Oil and gas');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('38', 'en', 'Natural resources. Mining');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('39', 'en', 'Child\'s world. Children\'s products');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('40', 'en', 'Advertising, design');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('41', 'en', 'Printing, graphics');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('43', 'en', 'Environment');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('45', 'en', 'Apparel, footwear, leather, fur');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('46', 'en', 'Cleaning services');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('47', 'en', 'Special events and celebrations (weddings, New Year, Christmas)');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('49', 'en', 'Antiques');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('50', 'en', 'Exhibition industry');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('51', 'en', 'Lifting machines');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('53', 'en', 'Intellectual property, patents');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('54', 'en', 'Boats, yachts, shipbuilding');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('55', 'en', 'Books, publishing');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('56', 'en', 'Small and medium business');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('59', 'en', 'Property and Real estate - rent and sale');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('60', 'en', 'Optics. Lasers');
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ('61', 'en', 'Textile and fabric');
		";
	}
}