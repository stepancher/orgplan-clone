<?php

class m160212_090701__tr_table2 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{trexhibitioncomplextype}}
			ADD COLUMN `shortUrl` VARCHAR(255) NULL AFTER `fullNameSingle`;
			
			DELETE FROM {{exhibitioncomplextype}} WHERE `name` IS NULL;
			
			DELETE FROM {{trexhibitioncomplextype}} WHERE `langId` = 'ru';
			
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`, `shortUrl`)
			(SELECT `id`, 'ru', `name`, `shortUrl` FROM {{exhibitioncomplextype}} WHERE `name` != '' AND `name` IS NOT NULL);
		";
	}
}