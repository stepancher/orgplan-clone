<?php

class m161018_070732_fairs_associations_fks extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fairhasassociation}} WHERE id = '412';
            
            ALTER TABLE {{fairhasassociation}} 
            ADD INDEX `fk_fairhasassociation_fairId_idx` (`fairId` ASC);
            ALTER TABLE {{fairhasassociation}} 
            ADD CONSTRAINT `fk_fairhasassociation_fairId`
              FOREIGN KEY (`fairId`)
              REFERENCES {{fair}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;

            ALTER TABLE {{fairhasassociation}} 
            ADD INDEX `fk_fairhasassociation_associationId_idx` (`associationId` ASC);
            ALTER TABLE {{fairhasassociation}} 
            ADD CONSTRAINT `fk_fairhasassociation_associationId`
              FOREIGN KEY (`associationId`)
              REFERENCES {{association}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}