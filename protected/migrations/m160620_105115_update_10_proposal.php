<?php

class m160620_105115_update_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trattributeclassproperty}} SET `value`='days' WHERE `id`='2540';
			UPDATE {{trattributeclassproperty}} SET `value`='дней' WHERE `id`='2361';
			UPDATE {{attributeclass}} SET `class`='datePickerPeriod' WHERE `id`='2062';
		";
	}
}