<?php

class m150909_130445_create_standdecor extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{standdecor}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE {{standdecor}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`standId` int(11),
				`posters` varchar(255),
				`additionalInscription` varchar(255),
				`colorRange` varchar(255),
				`decor` varchar(255)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;
		';
	}
}