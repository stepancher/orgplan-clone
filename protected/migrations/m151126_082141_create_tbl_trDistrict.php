<?php

class m151126_082141_create_tbl_trDistrict extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{trdistrict}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS {{trdistrict}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`trParentId` int(11),
				`langId` varchar(255),
				`name` varchar(255)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;

			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("1", "de", "Föderationskreis Zentralrussland");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("2", "de", "Föderationskreis Südrussland");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("3", "de", "Föderationskreis Nordwestrussland");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("4", "de", "Föderationskreis Ferner Osten");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("5", "de", "Föderationskreis Sibirien");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("6", "de", "Föderationskreis Ural");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("7", "de", "Föderationskreis Wolga");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("8", "de", "Föderationskreis Nordkaukasus");
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ("9", "de", "Föderationskreis Krim");
		';
	}
}