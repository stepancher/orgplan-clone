<?php

class m160712_115727_rounding_region_area extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			UPDATE {{regioninformation}} SET area = '36.3'
			WHERE regionId = 11;
			
			UPDATE {{regioninformation}} SET area = '2366.8'
			WHERE regionId = 24;
			
			UPDATE {{regioninformation}} SET area = '76.6'
			WHERE regionId = 34;
			
			UPDATE {{regioninformation}} SET area = '160.2'
			WHERE regionId = 41;
			
			UPDATE {{regioninformation}} SET area = '314.4'
			WHERE regionId = 71;
			
			UPDATE {{regioninformation}} SET area = '37.2'
			WHERE regionId = 75;
			
			UPDATE {{regioninformation}} SET area = '534.8'
			WHERE regionId = 77;
			
			UPDATE {{regioninformation}} SET area = '36.2'
			WHERE regionId = 83;
	";
	}

	public function downSql()
	{
		return TRUE;
	}
}