<?php

class m160607_143931_add_9_proposal_tr extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{

		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){

		return "
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1942', '577', 'en', 'Прием самоходных экспонатов');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES ('1943', '579', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\n');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1944', '580', 'en', 'Без использования передвижного трапа');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES ('1945', '582', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nПри заказе после 29 июля 2016 г. <br>\nстоимость услуги составит 1 500 руб. <br>\nПри заказе после 16 сентября 2016 г. <br>\nстоимость услуг увеличивается <br>\nна 100%');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES ('1946', '584', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nПри заказе после 29 июля 2016 г. <br>\nстоимость услуги составит 7 300 руб. <br>\nПри заказе после 16 сентября 2016 г. <br>\nстоимость услуг увеличивается <br>\nна 100%');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1947', '585', 'en', 'Автогрузы (укомплектованные грузы, группы грузов):');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1948', '586', 'en', 'Минимальный заказ - 200 кг или 1 м³. Расчеты по тяжеловесным (с единичной массой более 5 000 кг), опасным грузам, производятся по специальным тарифам.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1949', '587', 'en', 'Выставочные грузы');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1950', '589', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАСЧЕТ ВЕСА</div><div class=\"body\">При расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1951', '591', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАСЧЕТ ВЕСА</div><div class=\"body\">При расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1952', '593', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАСЧЕТ ОБЪЕМА</div><div class=\"body\">При расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1953', '594', 'en', 'Рабочая сила и грузоподъемные механизмы:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1954', '596', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1955', '598', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1956', '600', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1957', '602', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАСЧЕТ ВРЕМЕНИ</div><div class=\"body\">При расчетах каждый начавшийся час считается, как целый час.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1958', '604', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАСЧЕТ ВРЕМЕНИ</div><div class=\"body\">При расчетах каждый начавшийся час считается, как целый час.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1959', '606', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАБОЧАЯ СИЛА</div><div class=\"body\">Согласно принятым техническим нормам не менее одного рабочего или установленной группы рабочих должны быть наняты для управления грузоподъемным механизмом.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1960', '607', 'en', 'При погрузочно-разгрузочных работах на территории выставочного комплекса может использоваться только грузоподъемная техника, предоставленная МВЦ «Крокус Экспо».');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1961', '609', 'en', 'Все цены включают НДС 18%. Оплата производится в российских рублях. ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1962', '610', 'en', 'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. \nПри заказе работ и услуг после 29 июля 2016г. Организатор оставляет за собой право отказать Экспоненту в приеме заявки и предоставлении услуги.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 16:00 03 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('1963', '611', 'en');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('1964', '612', 'en');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('1965', '614', 'en');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1966', '616', 'en', 'Погрузка самоходных экспонатов');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`) VALUES ('1967', '618', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1968', '619', 'en', 'Автогрузы (укомплектованные грузы, группы грузов):');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1969', '620', 'en', 'Минимальный заказ - 200 кг или 1 м³. Расчеты по тяжеловесным (с единичной массой более 5 000 кг), опасным грузам, производятся по специальным тарифам.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1970', '621', 'en', 'Выставочные грузы');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1971', '623', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1972', '625', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1973', '627', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">РАСЧЕТ ОБЪЕМА</div><div class=\"body\">При расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1974', '628', 'en', 'Рабочая сила и грузоподъемные механизмы:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1975', '630', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1976', '632', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1977', '634', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1978', '636', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1979', '638', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1980', '640', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАБОЧАЯ СИЛА\n</div>\n<div class=\"body\">\nСогласно принятым техническим нормам не менее одного рабочего или установленной группы рабочих должны быть наняты для управления грузоподъемным механизмом.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1981', '641', 'en', 'При погрузочно-разгрузочных работах на территории выставочного комплекса может использоваться только грузоподъемная техника, предоставленная МВЦ «Крокус Экспо».');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('1982', '642', 'en');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1983', '643', 'en', 'Обработка и хранение грузов:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1984', '644', 'en', 'Доставка груза на склад и обратно:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1985', '646', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1986', '648', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1987', '649', 'en', 'Хранение груза:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1988', '650', 'en', 'На открытом складе (min 5 м³)');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `tooltip`, `tooltipCustom`) VALUES ('1989', '651', 'en', '', 'м³', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1990', '652', 'en', 'На закрытом слкаде (min 5 м³)');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `tooltip`, `tooltipCustom`) VALUES ('1991', '653', 'en', '', 'м³', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('1992', '654', 'en', 'Обработка и хранение тары:', '<div class=\"header\">\nОБРАБОТКА И ХРАНЕНИЕ ТАРЫ\n</div>\n<div class=\"body\">\nВключает перемещение от стенда до склада, хранение в период проведения мероприятия, доставку на стенд. Организатор и МВЦ «Крокус Экспо» не несут ответственности за содержимое тары (груз, находящийся в таре).\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltip`, `tooltipCustom`) VALUES ('1993', '656', 'en', '', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>', '<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1994', '658', 'en', 'Все цены включают НДС 18%. Оплата производится в российских рублях. ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1995', '659', 'en', 'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. \nПри заказе работ и услуг после 29 июля 2016г. Организатор оставляет за собой право отказать Экспоненту в приеме заявки и предоставлении услуги.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 16:00 08 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1996', '661', 'en', 'Отправьте запоненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1997', '663', 'en', 'Экспонат №1');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('1998', '664', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('1999', '665', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2000', '666', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2001', '667', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2002', '668', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2003', '671', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2004', '672', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2005', '673', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2006', '674', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2007', '675', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2008', '676', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2009', '677', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2010', '680', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2011', '681', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2012', '682', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2013', '683', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2014', '684', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2015', '685', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2016', '686', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2017', '689', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2018', '690', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2019', '691', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2020', '692', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2021', '693', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2022', '694', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2023', '695', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2024', '698', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2025', '699', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2026', '700', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2027', '701', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2028', '702', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2029', '703', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2030', '704', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2031', '707', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2032', '708', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2033', '709', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2034', '710', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2035', '711', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2036', '712', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2037', '713', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2038', '716', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2039', '717', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2040', '718', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2041', '719', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2042', '720', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2043', '721', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2044', '722', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2045', '725', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2046', '726', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2047', '727', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2048', '728', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2049', '729', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2050', '730', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2051', '731', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2052', '734', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2053', '735', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2054', '736', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2055', '737', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2056', '738', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2057', '739', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2058', '740', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2059', '743', 'en', 'Дата разгрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `placeholder`) VALUES ('2060', '744', 'en', 'Дата погрузки (желаемая)', '', '____/__/__');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2061', '745', 'en', 'Наименование', 'Укажите наименование машины или оборудования');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2062', '746', 'en', 'Длина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2063', '747', 'en', 'Ширина', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2064', '748', 'en', 'Высота', 'м');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2065', '749', 'en', 'Масса', 'кг');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2066', '752', 'en', 'Дата разгрузки (желаемая)', '');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2067', '753', 'en', 'Дата погрузки (желаемая)', '');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2068', '754', 'en', 'Экспонат №2');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2069', '755', 'en', 'Экспонат №3');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2070', '756', 'en', 'Экспонат №4');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2071', '757', 'en', 'Экспонат №5');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2072', '758', 'en', 'Экспонат №6');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2073', '759', 'en', 'Экспонат №7');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2074', '760', 'en', 'Экспонат №8');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2075', '761', 'en', 'Экспонат №9');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2076', '762', 'en', 'Экспонат №10');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2077', '767', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2078', '768', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2079', '769', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2080', '770', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2081', '771', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2082', '772', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2083', '773', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2084', '774', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2085', '775', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2086', '776', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2087', '777', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2088', '778', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2089', '779', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2090', '780', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2091', '781', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2092', '782', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2093', '783', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2094', '784', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2095', '785', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2096', '786', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2097', '787', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2098', '788', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2099', '789', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2100', '790', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2101', '791', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2102', '792', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2103', '793', 'en', 'Кран, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2104', '794', 'en', 'Погрузчик, час');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2105', '795', 'en', 'Самоходный');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2106', '796', 'en', 'Кран, час', '<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `tooltipCustom`) VALUES ('2107', '797', 'en', '', 'сут.', '<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div> ');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`, `tooltipCustom`) VALUES ('2108', '798', 'en', '', 'сут.', '<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div>');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2109', '1044', 'en', 'РАЗГРУЗКА');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2110', '1045', 'en', 'ПОГРУЗКА');
";
	}

}