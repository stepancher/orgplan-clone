<?php

class m160405_093732_create_attributefile_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			ALTER TABLE {{attributeclass}}
			DROP COLUMN `hasFile`;
			
			DROP TABLE IF NOT EXISTS {{attributefile}};
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{attributeclass}}
			ADD COLUMN `hasFile` TINYINT(4) NULL AFTER `coefficientTooltipText`;
			
			CREATE TABLE IF NOT EXISTS {{attributefile}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` text,
			  `attribute` int(10) unsigned NOT NULL,
			  `ext` varchar(45) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  KEY `fk_tbl_attributefile` (`attribute`),
			  CONSTRAINT `fk_tbl_attributefile_1` FOREIGN KEY (`attribute`) REFERENCES `tbl_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `group`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `placeholder`, `tooltip`, `coefficientTooltipText`, `hasFile`)
			VALUES('n2_file_uploader', 'string', '0', '0', NULL, NULL, '320', '317', NULL, 'file', '0', NULL, NULL, NULL, NULL, '1');
		";
	}
}