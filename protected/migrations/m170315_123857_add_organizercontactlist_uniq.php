<?php

class m170315_123857_add_organizercontactlist_uniq extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{organizercontactlist}} WHERE `organizerId` IS NULL;
            
            DROP PROCEDURE IF EXISTS `add_organizercontactlist_uniq`;
            CREATE PROCEDURE `add_organizercontactlist_uniq`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_crc TEXT DEFAULT '';
                DECLARE temp_crc TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT t.id, t.crc FROM
                                        (SELECT ocl.id,
                                        CRC32(CONCAT(ocl.organizerId,ocl.value,ocl.type,CASE WHEN ocl.additionalContactInformation IS NULL THEN '' ELSE ocl.additionalContactInformation END)) AS crc
                                        FROM {{organizercontactlist}} ocl
                                        WHERE ocl.organizerId IS NOT NULL
                                        ) t  ORDER BY t.crc;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO c_id, c_crc;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF c_crc = temp_crc THEN
                        
                        START TRANSACTION;
                            DELETE FROM {{organizercontactlist}} WHERE `id` = c_id;
                        COMMIT;
                        
                    END IF;
                    
                    SET temp_crc := c_crc;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `add_organizercontactlist_uniq`();
            DROP PROCEDURE IF EXISTS `add_organizercontactlist_uniq`;
            
            ALTER TABLE {{organizercontactlist}} 
            ADD UNIQUE INDEX `orgIdValueTypeAddinfoidx` (`organizerId` ASC, `value` ASC, `type` ASC, `additionalContactInformation` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}