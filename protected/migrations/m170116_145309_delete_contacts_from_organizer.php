<?php

class m170116_145309_delete_contacts_from_organizer extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{organizer}} 
            DROP COLUMN `foodServices`,
            DROP COLUMN `servicesLoadingAndUnloading`,
            DROP COLUMN `advertisingServicesTheFair`,
            DROP COLUMN `servicesBuilding`,
            DROP COLUMN `exhibitionManagement`,
            DROP COLUMN `email`,
            DROP COLUMN `phone`;
            
            ALTER TABLE {{organizercontactlist}} 
            ADD INDEX `organizer_id_idx` (`organizerId` ASC);
            
            ALTER TABLE {{organizercontactlist}} 
            ADD UNIQUE INDEX `organizerIdTypeUniq` (`type` ASC, `organizerId` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}