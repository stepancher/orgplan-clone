<?php

class m170221_080115_add_exdb_associationhasassociation_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        
        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_associationhasassociation_to_exdb_db`;
            
            CREATE PROCEDURE {$expodata}.`add_exdb_associationhasassociation_to_exdb_db`();
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_one_id INT DEFAULT 0;
                DECLARE ass_second_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ass1.id, ass2.id FROM
                                        (SELECT ass.id, ass.member_of_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        UNION
                                        SELECT ass.id, ass.member_of_second_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        UNION
                                        SELECT ass.id, ass.member_of_3_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        UNION
                                        SELECT ass.id, ass.member_of_4_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        ) t LEFT JOIN {$expodata}.{{exdbassociation}} ass1 ON ass1.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass2 ON ass2.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdbassociationhasassociation}} aha ON aha.associationId = ass1.id AND aha.hasAssociationId = ass2.id
                                        WHERE t.id != 0 AND t.assId != 0 AND aha.id IS NULL ORDER BY ass1.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO ass_one_id, ass_second_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbassociationhasassociation}} (`associationId`,`hasAssociationId`) VALUES (ass_one_id, ass_second_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_associationhasassociation_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}