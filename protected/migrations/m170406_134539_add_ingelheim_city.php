<?php

class m170406_134539_add_ingelheim_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('235', 'ingelheim-am-rhein');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1547', 'ru', 'Ингельхайм-ам-Райн');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1547', 'en', 'Ingelheim am Rhein');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1547', 'de', 'Ingelheim am Rhein');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}