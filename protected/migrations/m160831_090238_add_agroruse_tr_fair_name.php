<?php

class m160831_090238_add_agroruse_tr_fair_name extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{trfair}} (`trParentId`, `langId`, `name`, `description`) VALUES ('11699', 'en', 'Агрорусь', 'XХVI Международная агропромышленная выставка-ярмарка - АГРОРУСЬ');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}