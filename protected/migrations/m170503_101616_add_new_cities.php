<?php

class m170503_101616_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('187', 'freising');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1561', 'ru', 'Фрайзинг');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1561', 'en', 'Freising');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1561', 'de', 'Freising');
            
            
            
            INSERT INTO {{region}} (`districtId`) VALUES ('61');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1100', 'ru', 'Альтенкирхен', 'altenkirchen');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1100', 'en', 'Altenkirchen', 'altenkirchen');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1100', 'de', 'Landkreis Altenkirchen', 'altenkirchen');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1100', 'scheuerfeld');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1562', 'ru', 'Шойерфельд');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1562', 'en', 'Scheuerfeld');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1562', 'de', 'Scheuerfeld');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}