<?php

class m161025_115810_added_reports_for_protoplan_proposals extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        list($peace1, $peace2, $dbProposalProtoplan) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            INSERT INTO {$dbProposalProtoplan}.tbl_report (`id`, `reportId`, `reportName`, `reportLabel`, `columnDefs`, `dateCreated`, `exportColumns`) VALUES ('5', '5', 'report-5', 'Стандартный стенд и фризовая надпись', '[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Тип застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Площадь застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Фризовая надпись\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Размещение логотипа\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Заявка №3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Заявка №4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]', '2016-10-01 17:16:04', '[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"buildingType\":\"тип застройки\",\"buildingSquare\":\"площадь застройки\",\"letters\":\"Фризовая надпись\",\"logo\":\"Размещение логотипа\",\"thirdProposal\":\"ЗАявка №3\",\"fourthProposal\":\"Заявка №4\"}]');
            INSERT INTO {$dbProposalProtoplan}.tbl_report (`id`, `reportId`, `reportName`, `reportLabel`, `columnDefs`, `dateCreated`, `exportColumns`) VALUES ('6', '6', 'report-6', 'Отчёт по Экспонентам', '[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"bill\",\"displayName\": \"Номер договора\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"phone\",\"displayName\": \"Телефон\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"post\",\"displayName\": \"Должность\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"fullName\",\"displayName\": \"ФИО\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}]', '2016-10-01 17:16:04', '[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"bill\":\"Номер договора\",\"phone\":\"Телефон\",\"post\":\"Должность\",\"fullName\":\"ФИО\"}]');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}