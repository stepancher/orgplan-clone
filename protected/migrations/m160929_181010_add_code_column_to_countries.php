<?php

class m160929_181010_add_code_column_to_countries extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{country}} 
              ADD COLUMN `code` VARCHAR(45) NULL AFTER `shortUrl`;

            UPDATE {{country}} SET `code`='ru' WHERE `id`='1';
            UPDATE {{country}} SET `code`='de' WHERE `id`='2';
            UPDATE {{country}} SET `code`='az' WHERE `id`='3';
            UPDATE {{country}} SET `code`='am' WHERE `id`='4';
            UPDATE {{country}} SET `code`='by' WHERE `id`='5';
            UPDATE {{country}} SET `code`='kz' WHERE `id`='6';
            UPDATE {{country}} SET `code`='kg' WHERE `id`='7';
            UPDATE {{country}} SET `code`='md' WHERE `id`='8';
            UPDATE {{country}} SET `code`='uz' WHERE `id`='9';
            UPDATE {{country}} SET `code`='tm' WHERE `id`='10';
            UPDATE {{country}} SET `code`='ge' WHERE `id`='11';
            UPDATE {{country}} SET `code`='mn' WHERE `id`='12';
            UPDATE {{country}} SET `code`='ua' WHERE `id`='13';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}