<?php

class m161223_124346_delete_unused_tables extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP TABLE 
            {{additionalexpenseshasfile}}, 
            {{regionrating}}, 
            {{selectedexhibitioncomplex}}, 
            {{trstand}}, 
            {{stand}}, 
            {{standdecor}}, 
            {{standhasfair}}, 
            {{standhasfile}}, 
            {{standhasindustry}}, 
            {{standhaslighting}},
            {{standhasnotification}}, 
            {{standhasobjective}},
            {{standhassuspendedconstruction}}, 
            {{standhaszone}}, 
            {{standtechnicaldata}}, 
            {{standtechnicaldatahaselectricity}}, 
            {{standtechnicaldatahasextraservice}}, 
            {{standtype}}, 
            {{tariffdiscount}}, 
            {{tariffreport}}, 
            {{trnotification}}, 
            {{userexhibitionspreparing}}, 
            {{userhascity}}, 
            {{userhasexhibitioncomplex}}, 
            {{userhasfair}}, 
            {{userlog}},
            {{userhasstand}};
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}