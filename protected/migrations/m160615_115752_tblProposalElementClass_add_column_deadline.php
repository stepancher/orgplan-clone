<?php

class m160615_115752_tblProposalElementClass_add_column_deadline extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			ALTER TABLE {{proposalelementclass}} DROP COLUMN `deadline`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{proposalelementclass}}
			ADD COLUMN `deadline` TIMESTAMP NULL AFTER `active`;

			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='1';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='2';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='3';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='4';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='5';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='7';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='10';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='18';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='19';
			UPDATE {{proposalelementclass}} SET `deadline`='2016-10-08 00:00:00' WHERE `id`='20';
		";
	}
}