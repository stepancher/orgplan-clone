<?php

class m170407_103516_copy_exdb_fair_raw_field_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {

        list($a,$b,$db) = explode('=', Yii::app()->db->connectionString);
        list($a,$b,$expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fair_raw_field_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fair_raw_field_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_raw TEXT DEFAULT '';
                DECLARE f_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.exdbRaw, f.id FROM {$expodata}.{{exdbfair}} ef
                                        LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId
                                        WHERE f.exdbRaw IS NULL OR f.exdbRaw = '';
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                
                    FETCH copy INTO f_raw, f_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {$db}.{{fair}} SET `exdbRaw` = f_raw WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_fair_raw_field_to_db`();
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fair_raw_field_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}