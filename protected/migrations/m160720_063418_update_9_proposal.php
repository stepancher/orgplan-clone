<?php

class m160720_063418_update_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "			
			UPDATE {{attributeclass}} SET `class`='spinner' WHERE `id`='591';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='500' WHERE `id`='1413';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='9800' WHERE `id`='910';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='14.52' WHERE `id`='920';
			UPDATE {{trattributeclassproperty}} SET `value`='500' WHERE `id`='1090';
			UPDATE {{trattributeclassproperty}} SET `value`='9800' WHERE `id`='599';
			UPDATE {{trattributeclassproperty}} SET `value`='14.52' WHERE `id`='609';

		";
	}
}