<?php

class m170213_115817_copy_organizercompany_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `copy_exdb_organizer_company_to_db`;
            
            CREATE PROCEDURE `copy_exdb_organizer_company_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE temp_name TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT DISTINCT exdbo.id, exdbtro.name, exdbtro.langId 
                                        FROM {{exdborganizercompany}} exdbo
                                            LEFT JOIN {{exdbtrorganizercompany}} exdbtro ON exdbtro.trParentId = exdbo.id
                                            LEFT JOIN {{trorganizercompany}} tro ON tro.name = exdbtro.name AND tro.langId = exdbtro.langId
                                        WHERE exdbtro.name IS NOT NULL AND exdbtro.name != '' AND tro.id IS NULL
                                        GROUP BY exdbtro.name, exdbtro.langId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO exdb_id, exdb_name, exdb_lang_id;
                    
                    IF temp_name != exdb_name THEN
                        
                        START TRANSACTION;
                            INSERT INTO {{organizercompany}} (`active`) VALUES (NULL);
                        COMMIT;
                        
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_name := exdb_name;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {{trorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, exdb_name, exdb_lang_id);
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS `copy_exdb_organizer_company_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}