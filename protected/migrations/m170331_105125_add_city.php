<?php

class m170331_105125_add_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('68');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1095', 'ru', 'Мюнхен', 'munich');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1095', 'en', 'Munich', 'munich');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1095', 'de', 'Landkreis München', 'munich');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1095', 'oberhaching');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1544', 'ru', 'Оберхахинг');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1544', 'en', 'Oberhaching');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1544', 'de', 'Oberhaching');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}