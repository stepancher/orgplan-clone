<?php

class m160805_150019_create_9b_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->dbProposal->beginTransaction();
		try {
			Yii::app()->dbProposal->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		echo 'doesn support migrate down, but return true';

		return true;
	}

	public function getCreateTable()
	{
		return "
		INSERT INTO {{proposalelementclass}} (`id`, `name`, `fixed`, `fairId`, `active`, `deadline`, `priority`) VALUES (NULL, 'zayavka_n9b', '0', '184', '1', '2016-10-08 03:00:00', '10.5');
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('26', 'ru', 'Заявка №9b', 'АРЕНДА ТРАКТОРА');
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('26', 'en', 'APPLICATION No.9b', NULL);
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_shadow_time', 'string', '0', '0', NULL, '0', 'hiddenDiscount', '0');
		UPDATE {{attributeclass}} SET `super`='2677' WHERE `id`='2677';
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_hint', 'int', '0', '0', '2678', '0', 'hintDanger', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_rent', 'int', '0', '0', '2679', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n9b_rent_tbl', 'int', '0', '2679', '2679', '10', 'plainHtml');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_model', 'string', '1', '0', '2679', '2679', '20', 'dropDownMultiply', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_model_1', 'bool', '0', '2681', '2679', '0', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_model_2', 'bool', '0', '2681', '2679', '10', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_model_3', 'bool', '0', '2681', '2679', '20', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_rent_time', 'string', '0', '2679', '2679', '20', 'datePicker', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_rent_hours', 'int', '0', '2679', '2679', '30', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_rent_hint', 'int', '0', '0', '2679', '40', 'hintDanger', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_hydro', 'int', '0', '0', '2688', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_hydro_hint', 'int', '0', '2688', '2688', '10', 'hintDanger', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_hydro_oil', 'int', '0', '2688', '2688', '20', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_summary', 'string', '0', '0', '2691', '0', 'summary', '0');
		UPDATE {{attributeclass}} SET `class`='hint' WHERE `id`='2689';
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_summary_hint', 'int', '0', '2691', '2691', '10', 'hint', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_summary_hintDanger', 'int', '0', '2691', '2691', '20', 'hintDanger', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9b_agreement', 'bool', '0', '0', '2694', '0', 'checkboxAgreement', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n9b', 'int', '1', '0', '0', '2695', 'partiesSignature', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n9b', 'int', '1', '2695', '2695', '1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_right_block_n9b', 'int', '1', '2695', '2695', '2', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9b_1', 'string', '1', '2696', '2695', '1', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9b_2', 'string', '1', '2696', '2695', '2', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9b_3', 'string', '1', '2696', '2695', '3', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9b_1', 'string', '1', '2697', '2695', '1', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9b_2', 'string', '1', '2697', '2695', '2', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9b_3', 'string', '1', '2697', '2695', '3', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9b_4', 'string', '1', '2696', '2695', '5', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9b_4', 'string', '1', '2697', '2695', '5', 'text', '0');
		UPDATE {{attributeclass}} SET `parent`='2679' WHERE `id`='2687';
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2677', '0', '0', '0');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2678', '0', '0', '10');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2679', '0', '0', '20');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2688', '0', '0', '30');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2691', '0', '0', '40');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2694', '0', '0', '50');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('26', '2695', '0', '0', '60');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2678', 'ru', 'Отправьте заполненную форму в дирекцию выставки не позднее 31 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2679', 'ru', 'n9b_rent');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2680', 'ru', 'n9b_rent_tbl');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2681', 'ru', 'n9b_model');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2682', 'ru', 'n9b_model_1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2683', 'ru', 'n9b_model_2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2684', 'ru', 'n9b_model_3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2685', 'ru', 'n9b_rent_time');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2686', 'ru', 'n9b_rent_hours');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2687', 'ru', 'n9b_rent_hint');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2688', 'ru', 'n9b_hydro');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2689', 'ru', 'n9b_hydro_hint');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2690', 'ru', 'n9b_hydro_oil');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2691', 'ru', 'n9b_summary');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2692', 'ru', 'n9b_summary_hint');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2693', 'ru', 'n9b_summary_hintDanger');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2694', 'ru', 'n9b_agreement');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2695', 'ru', 'parties_signature_n9b');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2696', 'ru', 'parties_signature_left_block_n9b');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2697', 'ru', 'parties_signature_right_block_n9b');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2698', 'ru', 'parties_signature_row_left_n9b_1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2699', 'ru', 'parties_signature_row_left_n9b_2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2700', 'ru', 'parties_signature_row_left_n9b_3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2701', 'ru', 'parties_signature_row_right_n9b_1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2702', 'ru', 'parties_signature_row_right_n9b_2');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2703', 'ru', 'parties_signature_row_right_n9b_3');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2704', 'ru', 'parties_signature_row_left_n9b_4');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2705', 'ru', 'parties_signature_row_right_n9b_4');
		UPDATE {{trattributeclass}} SET `label`='Аренда трактора' WHERE `id`='3088';
		UPDATE {{trattributeclass}} SET `label`='Модель трактора' WHERE `id`='3090';
		UPDATE {{trattributeclass}} SET `label`='Terrion ATM 4200' WHERE `id`='3091';
		UPDATE {{trattributeclass}} SET `label`='Terrion ATM 5280' WHERE `id`='3092';
		UPDATE {{trattributeclass}} SET `label`='Terrion 7360' WHERE `id`='3093';
		UPDATE {{trattributeclass}} SET `label`='Начало смены' WHERE `id`='3094';
		UPDATE {{trattributeclass}} SET `label`='Продолжительность' WHERE `id`='3095';
		UPDATE {{trattributeclass}} SET `label`='Для перемещения в выставочные залы прицепного оборудования трактором, заказанным по настоящей заявке необходимо получить у Организатора БЕСПЛАТНЫЙ пропуск.' WHERE `id`='3096';
		UPDATE {{trattributeclass}} SET `label`='Заполнение гидросистемы' WHERE `id`='3097';
		UPDATE {{trattributeclass}} SET `label`='Если орудие приходит на выставочную площадку с незаполненной или заполненной частично гидросистемой, и при этом для его перемещения перевода в транспортное положение необходимо использование гидравлики, Вам необходимо заказать достаточное количество масла для ее заполнения. ' WHERE `id`='3098';
		UPDATE {{trattributeclass}} SET `label`='Масло' WHERE `id`='3099';
		DELETE FROM {{trattributeclass}} WHERE `id`='3100';
		UPDATE {{trattributeclass}} SET `label`='Все цены включают НДС 18%. Оплата настоящей заявки производится в рублях РФ. ' WHERE `id`='3101';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br><br> Подписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг. <br><br> Стоимость услуг увеличивается на 100% при заказе после 31.08.2016 г. <br><br> Все претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 4 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными.' WHERE `id`='3102';
		UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату и время' WHERE `id`='3094';
		UPDATE {{trattributeclass}} SET `unitTitle`='час' WHERE `id`='3094';
		DELETE FROM {{trattributeclass}} WHERE `id`='3103';
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2686', NULL, 'unitTitle');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2686', '4000', 'price');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2690', NULL, 'unitTitle');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2690', '220', 'price');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2691', 'currency');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2686', 'currency');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2690', 'currency');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2191', 'ru', 'час');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2193', 'ru', 'л.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2195', 'ru', 'руб');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2196', 'ru', 'руб');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2197', 'ru', 'руб');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2685', '2016-09-29', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2685', '2016-10-08', 'endDate');
		UPDATE {{attributeclass}} SET `class`='table' WHERE `id`='2680';
		UPDATE {{trattributeclass}} SET `label`='{\n        columns: [\n            {\n                text: \'ТРАКТОР TERRION\',\n                name: \'param\',\n                sortable: false\n            },\n            {\n                text: \'ATM 4200\',\n                name: \'4200\',\n                sortable: false\n            },\n            {\n                text: \'ATM 5280\',\n                name: \'5280\',\n                sortable: false\n            },\n            {\n                text: \'ATM 7360\',\n                name: \'7360\',\n                sortable: false\n            }\n        ],\n\n        rows: [\n            [\n                {\n                    value: \'Максимальная мощность (л. с.)\'\n                },\n                {\n                    value: \'200\'\n                },\n                {\n                    value: \'280\'\n                },\n                {\n                    value: \'360\'\n                }\n            ],\n            [\n                {\n                    value: \'Тяговый класс\'\n                },\n                {\n                    value: \'4\'\n                },\n                {\n                    value: \'5\'\n                },\n                {\n                    value: \'6-8\'\n                }\n            ],\n            [\n                {\n                    value: \'Заднее навесное устройство\'\n                },\n                {\n                    value: \'Сат3\'\n                },\n                {\n                    value: \'Сат3\'\n                },\n                {\n                    value: \'Сат4\'\n                }\n            ],\n            [\n                {\n                    value: \'Маятниковое сцепное устройство, тягово-сцепное устройство регулируемое по высоте\'\n                },\n                {\n                    value: \'Да\'\n                },\n                {\n                    value: \'Да\'\n                },\n                {\n                    value: \'Да\'\n                }\n            ],\n            [\n                {\n                    value: \'Количество пар гидравлики для орудий\'\n                },\n                {\n                    value: \'4\'\n                },\n                {\n                    value: \'5\'\n                },\n                {\n                    value: \'5\'\n                }\n            ]\n        ]\n    }' WHERE `id`='3089';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 31.08.16 г.' WHERE `id`='3095';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 31.08.16 г.' WHERE `id`='3099';
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '2677', '2677', 'increaseByLeftDay', '{1000:1, 38:2}', '1');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2686', '2677', 'inherit', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2690', '2677', 'inherit', '0');
		UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 37:2}' WHERE `id`='1074';
		UPDATE {{trattributeclass}} SET `label`='Экспонент' WHERE `id`='3107';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='3108';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='3109';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='3110';
		UPDATE {{trattributeclass}} SET `label`='Организатор' WHERE `id`='3110';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='3113';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='3111';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='3112';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='3114';
		DELETE FROM {{trattributeclass}} WHERE `id`='3104';
		DELETE FROM {{trattributeclass}} WHERE `id`='3105';
		DELETE FROM {{trattributeclass}} WHERE `id`='3106';
		";
	}
}

