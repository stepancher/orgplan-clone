<?php

class m170307_061334_separate_exdb_statistics extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `separate_exdb_statistics`;
            CREATE PROCEDURE `separate_exdb_statistics`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_exdb_id INT DEFAULT 0;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_info_id INT DEFAULT 0;
                DECLARE f_exhibitorsLocal INT DEFAULT 0;
                DECLARE f_exhibitorsForeign INT DEFAULT 0;
                DECLARE f_countriesCount INT DEFAULT 0;
                DECLARE f_areaClosedExhibitorsLocal INT DEFAULT 0;
                DECLARE f_areaClosedExhibitorsForeign INT DEFAULT 0;
                DECLARE f_areaOpenExhibitorsLocal INT DEFAULT 0;
                DECLARE f_areaOpenExhibitorsForeign INT DEFAULT 0;
                DECLARE f_areaSpecialExpositions INT DEFAULT 0;
                DECLARE f_visitorsLocal INT DEFAULT 0;
                DECLARE f_visitorsForeign INT DEFAULT 0;
                DECLARE f_squareGross INT DEFAULT 0;
                DECLARE f_squareNet INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statisticsDate DATE DEFAULT NULL;
                DECLARE f_exdb_fair_Id INT DEFAULT 0;
                DECLARE f_exdb_statistics_date TEXT DEFAULT '';
                DECLARE f_exdbCountriesCount TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT t.exdbId,
                                                f2.id,
                                                f2.infoId,
                                                fi2.exhibitorsLocal,
                                                fi2.exhibitorsForeign,
                                                fi2.countriesCount,
                                                fi2.areaClosedExhibitorsLocal,
                                                fi2.areaClosedExhibitorsForeign,
                                                fi2.areaOpenExhibitorsLocal,
                                                fi2.areaOpenExhibitorsForeign,
                                                fi2.areaSpecialExpositions,
                                                fi2.visitorsLocal,
                                                fi2.visitorsForeign,
                                                fi2.squareGross,
                                                fi2.squareNet,
                                                fi2.members,
                                                fi2.visitors,
                                                fi2.statistics,
                                                fi2.statisticsDate,
                                                fi2.exdbFairId,
                                                fi2.exdbStatisticsDate,
                                                fi2.exdbCountriesCount
                                                FROM
                                                (SELECT f.exdbId, COUNT(f.id), COUNT(DISTINCT fi.id) FROM {{fair}} f
                                                LEFT JOIN {{fairinfo}} fi ON fi.id = f.infoId
                                                WHERE f.exdbId IS NOT NULL
                                                GROUP BY f.exdbId HAVING COUNT(f.id) != COUNT(DISTINCT fi.id)
                                                ) t LEFT JOIN {{fair}} f2 ON f2.exdbId = t.exdbId
                                                LEFT JOIN {{fairinfo}} fi2 ON fi2.id = f2.infoId
                                                ORDER BY f2.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_exdb_id,
                                    f_id,
                                    f_info_id,
                                    f_exhibitorsLocal,
                                    f_exhibitorsForeign,
                                    f_countriesCount,
                                    f_areaClosedExhibitorsLocal,
                                    f_areaClosedExhibitorsForeign,
                                    f_areaOpenExhibitorsLocal,
                                    f_areaOpenExhibitorsForeign,
                                    f_areaSpecialExpositions,
                                    f_visitorsLocal,
                                    f_visitorsForeign,
                                    f_squareGross,
                                    f_squareNet,
                                    f_members,
                                    f_visitors,
                                    f_statistics,
                                    f_statisticsDate,
                                    f_exdb_fair_Id,
                                    f_exdb_statistics_date,
                                    f_exdbCountriesCount;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        DELETE FROM {{fairinfo}} WHERE `id` = f_info_id;
                    COMMIT;
                    
                    START TRANSACTION;
                        INSERT INTO {{fairinfo}} (`exhibitorsLocal`,
                                                    `exhibitorsForeign`,
                                                    `countriesCount`,
                                                    `areaClosedExhibitorsLocal`,
                                                    `areaClosedExhibitorsForeign`,
                                                    `areaOpenExhibitorsLocal`,
                                                    `areaOpenExhibitorsForeign`,
                                                    `areaSpecialExpositions`,
                                                    `visitorsLocal`,
                                                    `visitorsForeign`,
                                                    `squareGross`,
                                                    `squareNet`,
                                                    `members`,
                                                    `visitors`,
                                                    `statistics`,
                                                    `statisticsDate`,
                                                    `exdbFairId`,
                                                    `exdbStatisticsDate`,
                                                    `exdbCountriesCount`)
                                    VALUES (f_exhibitorsLocal,
                                    f_exhibitorsForeign,
                                    f_countriesCount,
                                    f_areaClosedExhibitorsLocal,
                                    f_areaClosedExhibitorsForeign,
                                    f_areaOpenExhibitorsLocal,
                                    f_areaOpenExhibitorsForeign,
                                    f_areaSpecialExpositions,
                                    f_visitorsLocal,
                                    f_visitorsForeign,
                                    f_squareGross,
                                    f_squareNet,
                                    f_members,
                                    f_visitors,
                                    f_statistics,
                                    f_statisticsDate,
                                    f_exdb_fair_Id,
                                    f_exdb_statistics_date,
                                    f_exdbCountriesCount);
                    COMMIT;
                    
                    SET @last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        UPDATE {{fair}} SET `infoId` = @last_insert_id WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `separate_exdb_statistics`();
            DROP PROCEDURE IF EXISTS `separate_exdb_statistics`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}