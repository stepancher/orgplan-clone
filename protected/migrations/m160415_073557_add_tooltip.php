<?php

class m160415_073557_add_tooltip extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, \'n9_e1_type_option\', \'bool\', \'0\', \'Кран, час\', \'669\', \'663\', \'0\', \'dropDownOption\', \'0\');
			UPDATE {{attributeclass}} SET `tooltipCustom`=NULL WHERE `id`=\'639\';
			UPDATE {{attributeclass}} SET `tooltipCustom`=\'<div class=\"header\">\nРАБОЧАЯ СИЛА\n</div>\n<div class=\"body\">\nСогласно принятым техническим нормам не менее одного рабочего или установленной группы рабочих должны быть наняты для управления грузоподъемным механизмом.\n</div>\' WHERE `id`=\'640\';
			UPDATE {{attributeclass}} SET `class`=\'coefficient\' WHERE `id`=\'651\';
			UPDATE {{attributeclass}} SET `class`=\'coefficient\' WHERE `id`=\'653\';
			UPDATE {{attributeclass}} SET `unitTitle`=\'м³\' WHERE `id`=\'651\';
			UPDATE {{attributeclass}} SET `unitTitle`=\'м³\' WHERE `id`=\'653\';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `tooltipCustom`) VALUES (NULL, \'n9_2_safe_time2\', \'int\', \'0\', \'\', \'652\', \'643\', \'0\', \'coefficient\', \'0\', \'сут.\', \'<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div>\');
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'653\';
			UPDATE {{attributeclass}} SET `class`=\'text\' WHERE `id`=\'816\';
			UPDATE {{attributeclass}} SET `class`=\'double\' WHERE `id`=\'816\';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `tooltipCustom`) VALUES (NULL, \'n9_2_safe_time\', \'int\', \'0\', \'\', \'650\', \'643\', \'0\', \'double\', \'0\', \'сут.\', \'<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема прнимается за полный кубометр.\n</div>\');
			UPDATE {{attributeclass}} SET `priority`=\'1\' WHERE `id`=\'651\';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (\'651\', \'817\', \'inheritValueMultiplier\', \'0\');
			UPDATE {{attributeclassdependency}} SET `function`=\'multiple\' WHERE `id`=\'705\';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (\'651\', \'816\', \'multiple\', \'0\');
			UPDATE {{attributeclassdependency}} SET `attributeClass`=\'653\' WHERE `id`=\'706\';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, \'n9_e1_type_option\', \'bool\', \'0\', \'Кран, час\', \'669\', \'663\', \'0\', \'dropDownOption\', \'0\');
			UPDATE {{attributeclass}} SET `parent`=\'678\' WHERE `id`=\'769\';
			UPDATE {{attributeclass}} SET `parent`=\'678\' WHERE `id`=\'770\';
			UPDATE {{attributeclass}} SET `parent`=\'678\' WHERE `id`=\'771\';
			UPDATE {{attributeclass}} SET `parent`=\'732\' WHERE `id`=\'789\';
			UPDATE {{attributeclass}} SET `parent`=\'741\' WHERE `id`=\'790\';
			UPDATE {{attributeclass}} SET `parent`=\'741\' WHERE `id`=\'791\';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES (\'651\', \'797\', \'multiple\');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES (\'653\', \'796\', \'multiple\');
			UPDATE {{attributeclass}} SET `class`=\'double\' WHERE `id`=\'796\';
	    ';
	}
}