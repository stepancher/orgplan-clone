<?php

class m150912_131223_create_industryhasmassMedia extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{industryhasmassmedia}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
				CREATE TABLE {{industryhasmassmedia}} (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `industryId` int(11) DEFAULT NULL,
				  `massMediaId` int(11) DEFAULT NULL,
				  `isGeneral` tinyint(4) DEFAULT NULL,
				  PRIMARY KEY (`id`),
				  UNIQUE KEY `index2` (`industryId`,`massMediaId`)
				) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
		';
	}
}