<?php

class m160301_081131_delete_fairs_from_tr extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */

	public function up(){
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
				Yii::app()->db->createCommand($sql)->execute();
				$transaction->commit();
		}
		catch(Exception $e)
		{
				$transaction->rollback();
				echo $e->getMessage();
				return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{trfair}} WHERE `trParentId` IN (1229, 2189);
		";
	}
}