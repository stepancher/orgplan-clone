<?php

class m170221_135154_add_fair_statistic_to_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairstatistic}};
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairdate}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbfairstatistic}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` INT(11) NULL DEFAULT NULL,
              `areaSpecialExpositions` INT(11) NULL DEFAULT NULL,
              `squareNet` INT(11) NULL DEFAULT NULL,
              `members` INT(11) NULL DEFAULT NULL,
              `exhibitorsForeign` INT(11) NULL DEFAULT NULL,
              `exhibitorsLocal` INT(11) NULL DEFAULT NULL,
              `visitors` INT(11) NULL DEFAULT NULL,
              `visitorsForeign` INT(11) NULL DEFAULT NULL,
              `visitorsLocal` INT(11) NULL DEFAULT NULL,
              `statistics` TEXT NULL DEFAULT NULL,
              `statisticsDate` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            DROP TABLE IF EXISTS {$expodata}.{{exdbfair}};
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbfair}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` INT(11) NULL DEFAULT NULL,
              `exdbFrequency` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `exhibitionComplexId` TEXT NULL DEFAULT NULL,
              `beginDate` DATE NULL DEFAULT NULL,
              `endDate` DATE NULL DEFAULT NULL,
              `beginMountingDate` DATE NULL DEFAULT NULL,
              `endMountingDate` DATE NULL DEFAULT NULL,
              `beginDemountingDate` DATE NULL DEFAULT NULL,
              `endDemountingDate` DATE NULL DEFAULT NULL,
              `exdbBusinessSectors` TEXT NULL DEFAULT NULL,
              `exdbCosts` TEXT NULL DEFAULT NULL,
              `exdbShowType` TEXT NULL DEFAULT NULL,
              `site` TEXT NULL DEFAULT NULL,
              `storyId` TEXT NULL DEFAULT NULL,
              `infoId` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            ALTER TABLE {$db}.{{fair}} 
            ADD COLUMN `exdbCrc` INT(11) NULL DEFAULT NULL AFTER `exdbId`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}