<?php

class m160623_062245_create_proposal_12 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{proposalelementclass}} (`name`, `fixed`, `fairId`, `active`) VALUES ('zayavka_n12a', '0', '184', '0');
			
			INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('21', 'ru', 'Заявка №12а', 'VIP БЕЙДЖИ');

			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_hintDanger', 'int', '0', '0', '0', '2205', '10', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1', 'int', '0', '0', '0', '2206', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_surname', 'string', '0', '0', '2206', '2206', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_name', 'string', '0', '0', '2206', '2206', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_lastname', 'string', '0', '0', '2206', '2206', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_position', 'string', '0', '0', '2206', '2206', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_company', 'string', '0', '0', '2206', '2206', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_post', 'string', '0', '0', '2206', '2206', '70', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_index', 'string', '0', '0', '2206', '2206', '80', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12a_badge1_country', 'int', '0', '0', '2206', '2206', '90', 'dropDownCountry', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12a_badge1_region', 'int', '0', '0', '2206', '2206', '100', 'dropDownRegion', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12a_badge1_city', 'int', '0', '0', '2206', '2206', '110', 'dropDownCity', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_street', 'string', '0', '0', '2206', '2206', '120', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_house', 'string', '0', '0', '2206', '2206', '130', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_phone', 'string', '0', '0', '2206', '2206', '140', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12a_badge1_fax', 'string', '0', '0', '2206', '2206', '150', 'text', '0');
			
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2205', '0', '0', '10');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2206', '0', '0', '20');

";
	}
}