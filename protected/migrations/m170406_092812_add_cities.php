<?php

class m170406_092812_add_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('263', 'tinnum');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1545', 'ru', 'Тиннум');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1545', 'en', 'Tinnum');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1545', 'de', 'Tinnum');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('6', 'kokino');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1546', 'ru', 'Кокино');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1546', 'en', 'Kokino');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1546', 'de', 'Kokino');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}