<?php

class m160920_091324_reanslate_shemkent extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trdistrict}} SET `name`='Südkasachstan' WHERE `id`='108';
            UPDATE {{trdistrict}} SET `name`='South Kazakhstan District' WHERE `id`='107';
            
            UPDATE {{trregion}} SET `name`='South Kazakhstan Region' WHERE `id`='511';
            UPDATE {{trregion}} SET `name`='Südkasachstan' WHERE `id`='512';

            UPDATE {{trcity}} SET `name`='Schymkent' WHERE `id`='1767';
            UPDATE {{trcity}} SET `name`='Shymkent' WHERE `id`='1766';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}