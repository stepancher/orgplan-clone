<?php

class m160405_092429_add_prices_for_new_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return file_get_contents(Yii::getPathOfAlias('app.data').'/proposal_dump_05_04_2016__17_17.sql');
	}
}