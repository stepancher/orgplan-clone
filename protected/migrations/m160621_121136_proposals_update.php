<?php

class m160621_121136_proposals_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2186', 'unitTitle');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1981', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1981', 'en', 'psc.');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2185', 'unitTitle');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2184', 'unitTitle');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2183', 'unitTitle');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1982', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1982', 'en', 'psc.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1983', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1983', 'en', 'psc.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1984', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1984', 'en', 'psc.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2184', 'ru', '<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2183', 'ru', '<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16');
";
	}
}
