<?php

class m160822_074804_add_fk_to_tr_tables extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{tradditionalexpenses}} 
	
            ADD CONSTRAINT `fk_additionalexpenses_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{additionalexpenses}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{tranalytics}} 
            ADD CONSTRAINT `fk_analytics_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{analytics}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trblog}} 
            ADD CONSTRAINT `fk_blog_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{blog}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trcountry}} 
            ADD CONSTRAINT `fk_country_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{country}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trdecor}} 
            ADD CONSTRAINT `fk_decor_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{decor}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trdistrict}} 
            ADD CONSTRAINT `fk_district_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{district}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trdocuments}} 
            CHANGE COLUMN `trParentId` `trParentId` INT(11) NULL DEFAULT NULL ;

              ALTER TABLE {{trdocuments}}
            ADD CONSTRAINT `fk_documents_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{documents}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trelectricity}}
            ADD CONSTRAINT `fk_electricity_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{electricity}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trelement}}
            ADD CONSTRAINT `fk_element_rtParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{element}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trequipment}}
            ADD CONSTRAINT `fk_equipment_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{equipment}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trequipmentparams}}
            ADD CONSTRAINT `fk_equipmentparams_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{equipmentparams}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trexhibitioncomplextype}}
            ADD CONSTRAINT `fk_exhibitioncomplextype_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{exhibitioncomplextype}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trexhibitionpreparing}}
            ADD CONSTRAINT `fk_exhibitionpreparing_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{exhibitionpreparing}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trextraservice}}
            ADD CONSTRAINT `fk_extraservice_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{extraservice}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trflooring}}
            ADD CONSTRAINT `fk_flooring_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{flooring}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trfurniture}}
            ADD CONSTRAINT `fk_furniture_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{furniture}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              DELETE FROM {{trindustry}} WHERE trParentId = 62;
            DELETE FROM {{trindustry}} WHERE trParentId = 64;
              
              ALTER TABLE {{trindustry}}
            ADD CONSTRAINT `fk_industry_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{industry}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trlegalentity}}
            ADD CONSTRAINT `fk_legalentity_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{legalentity}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trmarketinginformation}}
            ADD CONSTRAINT `fk_marketinginformation_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{marketinginformation}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trnews}}
            ADD CONSTRAINT `fk_news_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{news}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trnotification}}
            ADD CONSTRAINT `fk_notification_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{notification}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trorganizer}}
            ADD CONSTRAINT `fk_organizer_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{organizer}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trorganizerinfo}}
            ADD CONSTRAINT `fk_organizerinfo_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{organizerinfo}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trpage}}
            ADD CONSTRAINT `fk_page_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{page}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trpodium}}
            ADD CONSTRAINT `fk_podium_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{podium}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trraexpert}}
            ADD CONSTRAINT `fk_raexpert_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{raexpert}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trregion}}
            ADD CONSTRAINT `fk_region_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{region}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trregioninformation}}
            ADD CONSTRAINT `fk_regioninformation_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{regioninformation}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trservices}}
            ADD CONSTRAINT `fk_services_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{services}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trstand}}
            ADD CONSTRAINT `fk_stand_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{stand}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trsubtype}}
            ADD CONSTRAINT `fk_subtype_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{subtype}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trusefulinformation}}
            ADD CONSTRAINT `fk_usefulinformation_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{usefulinformation}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{trwalltype}}
            ADD CONSTRAINT `fk_walltype_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{walltype}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}