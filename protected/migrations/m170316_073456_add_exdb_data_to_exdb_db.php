<?php

class m170316_073456_add_exdb_data_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_unique_story_id_to_db`;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_association_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_association_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE ass_name TEXT DEFAULT '';
                DECLARE ass_contact TEXT DEFAULT '';
                DECLARE ass_contact_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ass.id, ass.name_en, ass.contacts_en, ass.contacts_raw_en FROM {$expodataRaw}.expo_assoc ass
                                            LEFT JOIN {$expodata}.{{exdbassociation}} eass ON eass.exdbId = ass.id
                                        WHERE eass.id IS NULL AND ass.id != 0 ORDER BY ass.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO ass_id, ass_name, ass_contact, ass_contact_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbassociation}} (`name`,`exdbId`,`exdbContacts`,`exdbContactsRaw`) VALUES (ass_name, ass_id, ass_contact, ass_contact_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_audit_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_audit_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE aud_contact TEXT DEFAULT '';
                DECLARE aud_name TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT au.id, au.contacts_en, au.name_en 
                                        FROM {$expodataRaw}.expo_audit au
                                            LEFT JOIN {$expodata}.{{exdbaudit}} tau ON tau.exdbId = au.id
                                        WHERE tau.id IS NULL;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO aud_id, aud_contact, aud_name;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbaudit}} (`exdbId`,`contacts_en`) VALUES (aud_id, aud_contact);
                        SET @last_insert_id := LAST_INSERT_ID();
                        INSERT INTO {$expodata}.{{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, 'ru');
                        INSERT INTO {$expodata}.{{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, 'en');
                        INSERT INTO {$expodata}.{{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, 'de');
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_audithasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_audithasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT au.id, ass.id FROM
                                        (select au.id, au.member_of_id_en AS associationId 
                                        from {$expodataRaw}.expo_audit au
                                        union
                                        select au.id, au.member_of_second_id_en AS associationId 
                                        from {$expodataRaw}.expo_audit au
                                        union
                                        select au.id, au.member_of_3_id_en AS associationId 
                                        from {$expodataRaw}.expo_audit au
                                        union
                                        select au.id, au.member_of_4_id_en AS associationId 
                                        from {$expodataRaw}.expo_audit au
                                        ) t LEFT JOIN {$expodata}.{{exdbaudit}} au ON au.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.exdbId = t.associationId
                                        LEFT JOIN {$expodata}.{{exdbaudithasassociation}} auhas ON auhas.auditId = au.id AND auhas.associationId = ass.id
                                        WHERE t.associationId != 0
                                        AND auhas.id IS NULL
                                        AND ass.id IS NOT NULL
                                        AND au.id IS NOT NULL
                                        ORDER BY au.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    
                    read_loop: LOOP
                        
                        FETCH copy INTO aud_id, ass_id;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES (aud_id, ass_id);
                        COMMIT;
                    END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_exhibitioncomplex_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_exhibitioncomplex_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE vc_name TEXT DEFAULT '';
                DECLARE vc_contact TEXT DEFAULT '';
                DECLARE vc_coordinate TEXT DEFAULT '';
                DECLARE vc_city_id INT DEFAULT 0;
                DECLARE vc_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT gr.id, gr.name_en, gr.contacts_en, gr.gr_lat_lng_en, trc.trParentId as cityId, gr.raw_en
                                        FROM {$expodataRaw}.expo_ground gr
                                        LEFT JOIN {$expodataRaw}.expo_data ed ON ed.expo_loc_id = gr.id
                                        LEFT JOIN {$db}.{{trcity}} trc ON trc.name = ed.city_en AND trc.langId = 'en'
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = gr.id
                                        WHERE ec.id IS NULL GROUP BY gr.id ORDER BY gr.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO vc_id, vc_name, vc_contact, vc_coordinate, vc_city_id, vc_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbexhibitioncomplex}} (`exdbId`,`name`,`contacts`,`coordinates`,`cityId`,`raw`) 
                            VALUES (vc_id, vc_name, vc_contact, vc_coordinate, vc_city_id, vc_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_exhibitioncomplexhasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_exhibitioncomplexhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ec.id, ass.id 
                                        FROM
                                        (SELECT gr.id, gr.member_of_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        union
                                        SELECT gr.id, gr.member_of_second_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        union
                                        SELECT gr.id, gr.member_of_3_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        union
                                        SELECT gr.id, gr.member_of_4_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        ) t LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplexhasassociation}} eha ON eha.exhibitionComplexId = ec.id AND eha.associationId = ass.id
                                        WHERE t.assId != 0 AND eha.id IS NULL AND ec.id IS NOT NULL AND ass.id IS NOT NULL ORDER BY ec.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO vc_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbexhibitioncomplexhasassociation}} (`exhibitionComplexId`,`associationId`) VALUES (vc_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairs_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairs_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_beginDate DATE DEFAULT '1970-01-01';
                DECLARE f_endDate DATE DEFAULT '1970-01-01';
                DECLARE f_beginMountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_endMountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_beginDemountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_endDemountingDate DATE DEFAULT '1970-01-01';
                DECLARE f_crc INT DEFAULT 0;
                DECLARE f_name TEXT DEFAULT '';
                DECLARE f_frequency TEXT DEFAULT '';
                DECLARE f_exhibitioncomplex_id INT DEFAULT 0;
                DECLARE f_business_sectors TEXT DEFAULT '';
                DECLARE f_costs TEXT DEFAULT '';
                DECLARE f_show_type TEXT DEFAULT '';
                DECLARE f_further_information TEXT DEFAULT '';
                DECLARE f_story_id TEXT DEFAULT '';
                DECLARE f_raw TEXT DEFAULT '';
                DECLARE f_first_year_show TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT s.id, 
                                        s.beginDate, 
                                        s.endDate,
                                        s.beginMountingDate,
                                        s.endMountingDate,
                                        s.beginDemountingDate,
                                        s.endDemountingDate, 
                                        CRC32(CONCAT(s.id, s.beginDate)) AS exdbCrc,
                                        ed.name_en,
                                        ed.frequency_en,
                                        ec.id as exhibitionComplexId,
                                        ed.business_sectors_en,
                                        ed.costs_en,
                                        ed.show_type_en,
                                        ed.further_information_en,
                                        ed.id AS storyId,
                                        ed.raw_en,
                                        ed.first_year_of_show_en
                                         FROM
                                        (SELECT p.id,
                                        CASE WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0 
                                                    AND p.beginDateYear IS NOT NULL AND p.beginDateYear != 0) 
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.beginDateMonth,'-',p.beginDateYear), '%e-%c-%Y')
                                             
                                             WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0 
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0) 
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.beginDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             
                                             WHEN (p.beginDateDay IS NOT NULL AND p.beginDateDay != 0 
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDateDay,'-',p.endDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginDate,
                                        
                                        CASE WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.endDateYear IS NOT NULL AND p.endDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.endDateMonth,'-',p.endDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.endDateMonth IS NOT NULL AND p.endDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.endDateMonth,'-',p.fyear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDateDay IS NOT NULL AND p.endDateDay != 0
                                                    AND p.beginDateMonth IS NOT NULL AND p.beginDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDateDay,'-',p.beginDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endDate,
                                        
                                        CASE WHEN (p.beginMountingDateDay IS NOT NULL AND p.beginMountingDateDay != 0
                                                    AND p.beginMountingDateMonth IS NOT NULL AND p.beginMountingDateMonth != 0
                                                    AND p.beginMountingDateYear IS NOT NULL AND p.beginMountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginMountingDateDay,'-',p.beginMountingDateMonth,'-',p.beginMountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.beginMountingDateDay IS NOT NULL AND p.beginMountingDateDay != 0
                                                    AND p.beginMountingDateMonth IS NOT NULL AND p.beginMountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginMountingDateDay,'-',p.beginMountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginMountingDate,
                                        
                                        CASE WHEN (p.endMountingDateDay IS NOT NULL AND p.endMountingDateDay != 0
                                                    AND p.endMountingDateMonth IS NOT NULL AND p.endMountingDateMonth != 0
                                                    AND p.endMountingDateYear IS NOT NULL AND p.endMountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endMountingDateDay,'-',p.endMountingDateMonth,'-',p.endMountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endMountingDateDay IS NOT NULL AND p.endMountingDateDay != 0
                                                    AND p.endMountingDateMonth IS NOT NULL AND p.endMountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endMountingDateDay,'-',p.endMountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endMountingDate,
                                        
                                        CASE WHEN (p.beginDemountingDateDay IS NOT NULL AND p.beginDemountingDateDay != 0
                                                    AND p.beginDemountingDateMonth IS NOT NULL AND p.beginDemountingDateMonth != 0
                                                    AND p.beginDemountingDateYear IS NOT NULL AND p.beginDemountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDemountingDateDay,'-',p.beginDemountingDateMonth,'-',p.beginDemountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.beginDemountingDateDay IS NOT NULL AND p.beginDemountingDateDay != 0
                                                    AND p.beginDemountingDateMonth IS NOT NULL AND p.beginDemountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.beginDemountingDateDay,'-',p.beginDemountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END beginDemountingDate,
                                        
                                        CASE WHEN (p.endDemountingDateDay IS NOT NULL AND p.endDemountingDateDay != 0
                                                    AND p.endDemountingDateMonth IS NOT NULL AND p.endDemountingDateMonth != 0
                                                    AND p.endDemountingDateYear IS NOT NULL AND p.endDemountingDateYear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDemountingDateDay,'-',p.endDemountingDateMonth,'-',p.endDemountingDateYear), '%e-%c-%Y')
                                        
                                             WHEN (p.endDemountingDateDay IS NOT NULL AND p.endDemountingDateDay != 0
                                                    AND p.endDemountingDateMonth IS NOT NULL AND p.endDemountingDateMonth != 0
                                                    AND p.fyear IS NOT NULL AND p.fyear != 0)
                                             THEN STR_TO_DATE(CONCAT(p.endDemountingDateDay,'-',p.endDemountingDateMonth,'-',p.fyear), '%e-%c-%Y')
                                             ELSE NULL END endDemountingDate
                                        FROM 
                                        (SELECT q.id, 
                                        q.fyear,
                                        
                                        q.beginDate,
                                        DAY(STR_TO_DATE(q.beginDate, '%e')) AS beginDateDay,
                                        MONTH(STR_TO_DATE(q.beginDate, '%e%b')) AS beginDateMonth,
                                        YEAR(STR_TO_DATE(q.beginDate, '%e%b%Y')) AS beginDateYear,
                                        
                                        q.endDate,
                                        DAY(STR_TO_DATE(q.endDate, '%e')) AS endDateDay,
                                        MONTH(STR_TO_DATE(q.endDate, '%e%b')) AS endDateMonth,
                                        YEAR(STR_TO_DATE(q.endDate, '%e%b%Y')) AS endDateYear,
                                        
                                        q.beginMountingDate,
                                        DAY(STR_TO_DATE(q.beginMountingDate, '%e')) AS beginMountingDateDay,
                                        MONTH(STR_TO_DATE(q.beginMountingDate, '%e%b')) AS beginMountingDateMonth,
                                        YEAR(STR_TO_DATE(q.beginMountingDate, '%e%b%Y')) AS beginMountingDateYear,
                                        
                                        q.endMountingDate,
                                        DAY(STR_TO_DATE(q.endMountingDate, '%e')) AS endMountingDateDay,
                                        MONTH(STR_TO_DATE(q.endMountingDate, '%e%b')) AS endMountingDateMonth,
                                        YEAR(STR_TO_DATE(q.endMountingDate, '%e%b%Y')) AS endMountingDateYear,
                                        
                                        q.beginDemountingDate,
                                        DAY(STR_TO_DATE(q.beginDemountingDate, '%e')) AS beginDemountingDateDay,
                                        MONTH(STR_TO_DATE(q.beginDemountingDate, '%e%b')) AS beginDemountingDateMonth,
                                        YEAR(STR_TO_DATE(q.beginDemountingDate, '%e%b%Y')) AS beginDemountingDateYear,
                                        
                                        q.endDemountingDate,
                                        DAY(STR_TO_DATE(q.endDemountingDate, '%e')) AS endDemountingDateDay,
                                        MONTH(STR_TO_DATE(q.endDemountingDate, '%e%b')) AS endDemountingDateMonth,
                                        YEAR(STR_TO_DATE(q.endDemountingDate, '%e%b%Y')) AS endDemountingDateYear,
                                        
                                        q.tHeader,
                                        q.tRow
                                        FROM
                                        (SELECT d.id, REPLACE(d.fyear,'\n','') AS fyear, 
                                        REPLACE(SUBSTRING_INDEX(d.showDates, '-', 1), '.', '') AS beginDate,
                                        REPLACE(SUBSTR(REPLACE(d.showDates, SUBSTRING_INDEX(d.showDates, '-', 1), ''), 2), '.', '') endDate,
                                        SUBSTRING_INDEX(d.buildUp, '-', 1) AS beginMountingDate,
                                        SUBSTR(REPLACE(d.buildUp, SUBSTRING_INDEX(d.buildUp, '-', 1), ''), 2) AS endMountingDate,
                                        SUBSTRING_INDEX(d.dismantling, '-', 1) AS beginDemountingDate,
                                        SUBSTR(REPLACE(d.dismantling, SUBSTRING_INDEX(d.dismantling, '-', 1), ''), 2) AS endDemountingDate,
                                        d.tHeader,
                                        d.tRow
                                        FROM
                                        (SELECT id, row_1 AS tHeader, row_2 AS tRow, SUBSTR(row_2, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_2, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_2, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_2, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',2), SUBSTRING_INDEX(ed.dates_en,'\n',1),'') AS `row_2`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_3 AS tRow, SUBSTR(row_3, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_3, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_3, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_3, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',3), SUBSTRING_INDEX(ed.dates_en,'\n',2),'') AS `row_3`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_4 AS tRow, SUBSTR(row_4, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_4, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_4, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_4, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',4), SUBSTRING_INDEX(ed.dates_en,'\n',3),'') AS `row_4`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_5 AS tRow, SUBSTR(row_5, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_5, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_5, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_5, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',5), SUBSTRING_INDEX(ed.dates_en,'\n',4),'') AS `row_5`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_6 AS tRow, SUBSTR(row_6, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_6, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_6, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_6, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',6), SUBSTRING_INDEX(ed.dates_en,'\n',5),'') AS `row_6`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_7 AS tRow, SUBSTR(row_7, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_7, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_7, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_7, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',7), SUBSTRING_INDEX(ed.dates_en,'\n',6),'') AS `row_7`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_8 AS tRow, SUBSTR(row_8, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_8, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_8, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_8, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',8), SUBSTRING_INDEX(ed.dates_en,'\n',7),'') AS `row_8`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_9 AS tRow, SUBSTR(row_9, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_9, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_9, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_9, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',9), SUBSTRING_INDEX(ed.dates_en,'\n',8),'') AS `row_9`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_10 AS tRow, SUBSTR(row_10, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_10, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_10, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_10, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',10), SUBSTRING_INDEX(ed.dates_en,'\n',9),'') AS `row_10`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        union
                                        SELECT id, row_1 AS tHeader, row_11 AS tRow, SUBSTR(row_11, 1, 5) AS fyear, SUBSTRING_INDEX(SUBSTR(row_11, 8),',',1) AS showDates,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',2), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',1), ''), 2) AS buildUp,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',3), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',2), ''), 2) AS dismantling,
                                        SUBSTR(REPLACE(SUBSTRING_INDEX(SUBSTR(row_11, 8),',',4), SUBSTRING_INDEX(SUBSTR(row_11, 8),',',3), ''), 2) AS deadline
                                        FROM
                                        (SELECT ed.id AS id,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',1), SUBSTRING_INDEX(ed.dates_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.dates_en,'\n',11), SUBSTRING_INDEX(ed.dates_en,'\n',10),'') AS `row_11`
                                        FROM {$expodataRaw}.`expo_data` ed) t
                                        ) d) q) p where p.fyear != '') s 
                                        LEFT JOIN {$expodata}.{{exdbfair}} f ON f.exdbCrc = CRC32(CONCAT(s.id, s.beginDate))
                                        LEFT JOIN {$expodataRaw}.expo_data ed ON ed.id = s.id
                                        LEFT JOIN {$expodataRaw}.expo_ground gr ON ed.expo_loc_id = gr.id
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = gr.id
                                        WHERE f.id IS NULL
                                        ORDER BY s.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, 
                        f_beginDate, 
                        f_endDate, 
                        f_beginMountingDate, 
                        f_endMountingDate, 
                        f_beginDemountingDate, 
                        f_endDemountingDate, 
                        f_crc, 
                        f_name,
                        f_frequency,
                        f_exhibitioncomplex_id,
                        f_business_sectors,
                        f_costs,
                        f_show_type,
                        f_further_information,
                        f_story_id,
                        f_raw,
                        f_first_year_show;
                    
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfair}} (
                            `exdbId`,
                            `exdbCrc`,
                            `exdbFrequency`,
                            `name`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                            `exdbBusinessSectors`,
                            `exdbCosts`,
                            `exdbShowType`,
                            `site`,
                            `storyId`,
                            `exdbRaw`,
                            `exdbFirstYearShow`) 
                            VALUES (
                            f_id,
                            f_crc,
                            f_frequency,
                            f_name,
                            f_exhibitioncomplex_id,
                            f_beginDate,
                            f_endDate,
                            f_beginMountingDate,
                            f_endMountingDate,
                            f_beginDemountingDate,
                            f_endDemountingDate,
                            f_business_sectors,
                            f_costs,
                            f_show_type,
                            f_further_information,
                            f_story_id,
                            f_raw,
                            f_first_year_show);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.id, eass.id FROM
                                        (SELECT ed.id, ed.member_of_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        UNION
                                        SELECT ed.id, ed.member_of_second_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        UNION
                                        SELECT ed.id, ed.member_of_3_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        UNION
                                        SELECT ed.id, ed.member_of_4_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        ) t LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass ON eass.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdbfairhasassociation}} efha ON efha.fairId = ef.id AND efha.associationId = eass.id
                                        WHERE t.assId != 0 AND efha.id IS NULL AND ef.id IS NOT NULL AND eass.id IS NOT NULL ORDER BY ef.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairhasassociation}} (`fairId`,`associationId`) VALUES (f_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fair_id INT DEFAULT 0;
                DECLARE audit_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.id AS fairId, eau.id AS auditId
                                        FROM {$expodataRaw}.expo_data ed 
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = ed.id
                                        LEFT JOIN {$expodata}.{{exdbaudit}} eau ON eau.exdbId = ed.audit_id_en
                                        LEFT JOIN {$expodata}.{{exdbfairhasaudit}} efhau ON efhau.fairId = ef.id AND efhau.auditId = eau.id
                                        WHERE ed.audit_id_en != 0 AND efhau.id IS NULL AND ef.id IS NOT NULL AND eau.id IS NOT NULL
                                        ORDER BY ef.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    
                    FETCH copy INTO fair_id, audit_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairhasaudit}} (`fairId`,`auditId`) VALUES (fair_id, audit_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fair_id INT DEFAULT 0;
                DECLARE organizer_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT t.fairId, t.orgId FROM
                                        (SELECT ef.id AS fairId, eorg.id AS orgId
                                        FROM {$expodataRaw}.expo_data ed 
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = ed.id
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eorg ON eorg.exdbId = ed.organizer_id_en
                                        WHERE ed.organizer_id_en != 0
                                        UNION
                                        SELECT ef.id AS fairId, eorg.id AS orgId
                                        FROM {$expodataRaw}.expo_data ed 
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = ed.id
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eorg ON eorg.exdbId = ed.co_organizer_id_en
                                        WHERE ed.co_organizer_id_en != 0
                                        ) t LEFT JOIN {$expodata}.{{exdbfairhasorganizer}} efho ON efho.fairId = t.fairId AND efho.organizerId = t.orgId
                                        WHERE efho.id IS NULL AND t.fairId IS NOT NULL AND t.orgId IS NOT NULL
                                        ORDER BY t.fairId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    FETCH copy INTO fair_id, organizer_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairhasorganizer}} (`fairId`,`organizerId`) VALUES (fair_id, organizer_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fair_statistic_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fair_statistic_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_areaSpecialExpositions INT DEFAULT 0;
                DECLARE f_squareNet INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_exhibitorsForeign INT DEFAULT 0;
                DECLARE f_exhibitorsLocal INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_visitorsForeign INT DEFAULT 0;
                DECLARE f_visitorsLocal INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statisticsDate TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT d.id,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.specialShow, ',', 4), SUBSTRING_INDEX(d.specialShow, ',', 3), ''), 2) AS areaSpecialExpositions,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.netSqm, ',', 4), SUBSTRING_INDEX(d.netSqm, ',', 3), ''), 2) AS squareNet,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 4), SUBSTRING_INDEX(d.exhibitors, ',', 3), ''), 2) AS members,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 3), SUBSTRING_INDEX(d.exhibitors, ',', 2), ''), 2) AS exhibitorsForeign,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.exhibitors, ',', 2), SUBSTRING_INDEX(d.exhibitors, ',', 1), ''), 2) AS exhibitorsLocal,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 4), SUBSTRING_INDEX(d.visitors, ',', 3), ''), 2) AS visitors,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 3), SUBSTRING_INDEX(d.visitors, ',', 2), ''), 2) AS visitorsForeign,
                                            SUBSTR(REPLACE(SUBSTRING_INDEX(d.visitors, ',', 2), SUBSTRING_INDEX(d.visitors, ',', 1), ''), 2) AS visitorsLocal,
                                            d.statistics_with_en AS statistics,
                                            d.stat_year_en AS statisticsDate
                                        FROM
                                        (SELECT t.id, t.statistics_with_en, t.stat_year_en, t.row_1, t.stats_en,
                                        CASE WHEN LOCATE('Net sqm', row_2) != 0 THEN row_2
                                             WHEN LOCATE('Net sqm', row_3) != 0 THEN row_3
                                             WHEN LOCATE('Net sqm', row_4) != 0 THEN row_4
                                             WHEN LOCATE('Net sqm', row_5) != 0 THEN row_5
                                             WHEN LOCATE('Net sqm', row_6) != 0 THEN row_6
                                             WHEN LOCATE('Net sqm', row_7) != 0 THEN row_7
                                             ELSE NULL END netSqm,
                                        CASE WHEN LOCATE('Exhibitors', row_2) != 0 THEN row_2
                                             WHEN LOCATE('Exhibitors', row_3) != 0 THEN row_3
                                             WHEN LOCATE('Exhibitors', row_4) != 0 THEN row_4
                                             WHEN LOCATE('Exhibitors', row_5) != 0 THEN row_5
                                             WHEN LOCATE('Exhibitors', row_6) != 0 THEN row_6
                                             WHEN LOCATE('Exhibitors', row_7) != 0 THEN row_7
                                             ELSE NULL END exhibitors,
                                        CASE WHEN LOCATE('Visitors', row_2) != 0 THEN row_2
                                             WHEN LOCATE('Visitors', row_3) != 0 THEN row_3
                                             WHEN LOCATE('Visitors', row_4) != 0 THEN row_4
                                             WHEN LOCATE('Visitors', row_5) != 0 THEN row_5
                                             WHEN LOCATE('Visitors', row_6) != 0 THEN row_6
                                             WHEN LOCATE('Visitors', row_7) != 0 THEN row_7
                                             ELSE NULL END visitors,
                                        CASE WHEN LOCATE('Special show', row_2) != 0 THEN row_2
                                             WHEN LOCATE('Special show', row_3) != 0 THEN row_3
                                             WHEN LOCATE('Special show', row_4) != 0 THEN row_4
                                             WHEN LOCATE('Special show', row_5) != 0 THEN row_5
                                             WHEN LOCATE('Special show', row_6) != 0 THEN row_6
                                             WHEN LOCATE('Special show', row_7) != 0 THEN row_7
                                             ELSE NULL END specialShow
                                        FROM
                                        (SELECT ed.id, ed.stats_en, ed.statistics_with_en, ed.stat_year_en,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',1), SUBSTRING_INDEX(ed.stats_en,'\n',0),'') AS `row_1`,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',2), SUBSTRING_INDEX(ed.stats_en,'\n',1),'') AS `row_2`,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',3), SUBSTRING_INDEX(ed.stats_en,'\n',2),'') AS `row_3`,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',4), SUBSTRING_INDEX(ed.stats_en,'\n',3),'') AS `row_4`,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',5), SUBSTRING_INDEX(ed.stats_en,'\n',4),'') AS `row_5`,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',6), SUBSTRING_INDEX(ed.stats_en,'\n',5),'') AS `row_6`,
                                        REPLACE(SUBSTRING_INDEX(ed.stats_en,'\n',7), SUBSTRING_INDEX(ed.stats_en,'\n',6),'') AS `row_7`
                                        FROM {$expodataRaw}.expo_data ed) t) d
                                        LEFT JOIN {$expodata}.{{exdbfairstatistic}} fs ON fs.exdbId = d.id
                                        WHERE fs.id IS NULL ORDER BY d.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, 
                        f_areaSpecialExpositions, 
                        f_squareNet, 
                        f_members, 
                        f_exhibitorsForeign, 
                        f_exhibitorsLocal,
                        f_visitors, 
                        f_visitorsForeign, 
                        f_visitorsLocal, 
                        f_statistics, 
                        f_statisticsDate;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairstatistic}} (`exdbId`,
                            `areaSpecialExpositions`,
                            `squareNet`,
                            `members`,
                            `exhibitorsForeign`,
                            `exhibitorsLocal`,
                            `visitors`,
                            `visitorsForeign`,
                            `visitorsLocal`,
                            `statistics`,
                            `statisticsDate`) 
                            VALUES (f_id,
                            f_areaSpecialExpositions,
                            f_squareNet,
                            f_members,
                            f_exhibitorsForeign,
                            f_exhibitorsLocal,
                            f_visitors,
                            f_visitorsForeign,
                            f_visitorsLocal,
                            f_statistics,
                            f_statisticsDate
                        );
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_organizercompany_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_organizercompany_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE org_name TEXT DEFAULT '';
                DECLARE org_contacts TEXT DEFAULT '';
                DECLARE org_contacts_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT org.id, org.name_en, org.contacts_en, org.contacts_raw_en FROM {$expodataRaw}.expo_org org
                                            LEFT JOIN {$expodata}.{{exdborganizercompany}} o ON o.exdbId = org.id
                                        WHERE org.id != 0 AND org.name_en IS NOT NULL AND o.id IS NULL
                                        ORDER BY org.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_id, org_name, org_contacts, org_contacts_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdborganizercompany}} (`exdbId`,`contacts_en`,`contacts_raw_en`) VALUES (org_id, org_contacts, org_contacts_raw);
                        SET @last_insert_id := LAST_INSERT_ID();
                        INSERT INTO {$expodata}.{{exdbtrorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, 'ru');
                        INSERT INTO {$expodata}.{{exdbtrorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, 'en');
                        INSERT INTO {$expodata}.{{exdbtrorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_organizercompanyhasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_organizercompanyhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT oc.id, ass.id FROM 
                                        (SELECT org.id, org.member_of_id_en AS assId 
                                        FROM {$expodataRaw}.expo_org org
                                        UNION
                                        SELECT org.id, org.member_of_second_id_en AS assId 
                                        FROM {$expodataRaw}.expo_org org
                                        UNION
                                        SELECT org.id, org.member_of_3_id_en AS assId 
                                        FROM {$expodataRaw}.expo_org org
                                        UNION
                                        SELECT org.id, org.member_of_4_id_en AS assId 
                                        FROM {$expodataRaw}.expo_org org
                                        ) t LEFT JOIN {$expodata}.{{exdborganizercompany}} oc ON oc.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdborganizercompanyhasassociation}} eocha ON eocha.organizerCompanyId = oc.id AND eocha.associationId = ass.id
                                        WHERE t.assId != 0 AND eocha.id IS NULL AND oc.id IS NOT NULL AND ass.id IS NOT NULL ORDER BY oc.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES (org_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_associationhasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_associationhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_one_id INT DEFAULT 0;
                DECLARE ass_second_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ass1.id, ass2.id FROM
                                        (SELECT ass.id, ass.member_of_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        UNION
                                        SELECT ass.id, ass.member_of_second_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        UNION
                                        SELECT ass.id, ass.member_of_3_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        UNION
                                        SELECT ass.id, ass.member_of_4_id_en AS assId
                                        FROM {$expodataRaw}.expo_assoc ass
                                        ) t LEFT JOIN {$expodata}.{{exdbassociation}} ass1 ON ass1.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass2 ON ass2.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdbassociationhasassociation}} aha ON aha.associationId = ass1.id AND aha.hasAssociationId = ass2.id
                                        WHERE t.id != 0 AND t.assId != 0 AND aha.id IS NULL AND ass1.id IS NOT NULL AND ass2.id IS NOT NULL ORDER BY ass1.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO ass_one_id, ass_second_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbassociationhasassociation}} (`associationId`,`hasAssociationId`) VALUES (ass_one_id, ass_second_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_association_to_exdb_db`();
            CALL {$expodata}.`add_exdb_audit_to_exdb_db`();
            CALL {$expodata}.`add_exdb_exhibitioncomplex_to_exdb_db`();
            CALL {$expodata}.`add_exdb_organizercompany_to_exdb_db`();
            CALL {$expodata}.`add_exdb_organizercompanyhasassociation_to_exdb_db`();
            CALL {$expodata}.`add_exdb_exhibitioncomplexhasassociation_to_exdb_db`();
            CALL {$expodata}.`add_exdb_audithasassociation_to_exdb_db`();
            CALL {$expodata}.`add_exdb_associationhasassociation_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fair_statistic_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairs_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairhasorganizer_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairhasaudit_to_exdb_db`();
            CALL {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}