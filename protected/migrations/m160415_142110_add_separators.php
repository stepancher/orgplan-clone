<?php

class m160415_142110_add_separators extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES (NULL, \'n9_e10_separator\', \'double\', \'0\', \'0\', \'\', \'762\', \'663\', \'9\', \'headerH2\', \'0\', \'\');

	    ';
	}
}