<?php

class m161017_100652_new_document extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('3', '1', '184');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('59', 'ru', 'Видеоотчет АГРОСАЛОН', 'https://www.youtube.com/embed/fzK60GKRHZ8');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}