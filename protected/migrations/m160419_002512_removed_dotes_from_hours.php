<?php

class m160419_002512_removed_dotes_from_hours extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='899';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='900';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='901';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='902';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='903';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='904';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='889';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='890';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='891';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='892';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='893';
			UPDATE {{attributeclassproperty}} SET `value`='час' WHERE `id`='894';
	    ";
	}
}