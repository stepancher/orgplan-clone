<?php

class m170504_095900_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1095', 'grasbrunn');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1563', 'ru', 'Грасбрунн');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1563', 'en', 'Grasbrunn');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1563', 'de', 'Grasbrunn');


            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('236', 'stadtallendorf');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1564', 'ru', 'Штадталлендорф');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1564', 'en', 'Stadtallendorf');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1564', 'de', 'Stadtallendorf');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('157', 'schermbeck');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1565', 'ru', 'Шермбек');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1565', 'en', 'Schermbeck');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1565', 'de', 'Schermbeck');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}