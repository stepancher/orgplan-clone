<?php

class m160219_135648_add_organizeusers_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{organizerusers}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			DROP TABLE IF EXISTS {{organizerusers}};
			CREATE TABLE {{organizerusers}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `organizerId` INT NULL,
			  `userId` INT NULL,
			  `role` INT NULL,
			  `isMain` TINYINT NULL,
			  `position` varchar(255) NULL,
			  `surname` varchar(255) NULL,
			  `name` varchar(255) NULL,
			  `patronymic` varchar(255) NULL,
			  `phone` varchar(255) NULL,
			  `mail` varchar(255) NULL,
			  PRIMARY KEY (`id`));
			  ';
	}
}