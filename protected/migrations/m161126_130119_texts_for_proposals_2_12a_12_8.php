<?php

class m161126_130119_texts_for_proposals_2_12a_12_8 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 21 октября 2017г. Факс: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='204';
            UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 16 октября 2017 г. Факс: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='2308';
            UPDATE {{trattributeclass}} SET `label`='Отправьте заполенную заявку в дирекцию выставки не позднее 11 ноября 2017 г. Факс: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='2400';
            UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 ноября 2017 г. факс: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='2776';
            UPDATE {{trattributeclass}} SET `label`='Senden Sie bitte das ausgefüllte Antragsformular  bis spätestens 16.Oktober 2017 an die Messeleitung. Fax: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='3393';
            UPDATE {{trattributeclass}} SET `label`='Senden Sie bitte das ausgefüllte Antragsformular  bis spätestens 11.November 2017 an die Messeleitung. Fax: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='3346';
            UPDATE {{trattributeclass}} SET `label`='Senden Sie bitte das ausgefüllte Antragsformular bis spätestens 29. November 2017 an die Messeleitung. Fax: +7(861)299-52-22, e-mail: contact@protoplan.pro' WHERE `id`='3226';
            UPDATE {{trattributeclass}} SET `label`='Die Besitzer von VIP-Namensschildern können die Messe Metalloobrabotka und sämtliche Veranstaltungen der Messe besuchen. Die Anzahl der VIP-Namensschilder, die dem Aussteller zur Verfügung gestellt werden, hängt von der Größe des Messestandes ab und wird wie folgt bestimmt:<br> Messestand bis 100 m² - 1 St. <br> Messestand zwischen 100 м² und 500м² - 2 St. <br> \nMessestand ab 500м² - 3 St.' WHERE `id`='3484';
            UPDATE {{trattributeclass}} SET `label`='Die Anzahl der Namensschilder, die dem Aussteller zur Verfügung gestellt werden, hängt von der Größe des Messestandes ab und wird wie folgt bestimmt:<br> Messestand 9m² - 3 St. <br> Messestand zwischen 9m² und 30m² - 10 St. <br> \nMessestand zwischen 30m² und 100m² - 20 St. <br> \nMessestand ab 100m² - 20 St. + 2 Namensschilder für jede zusätlziche 100m² ' WHERE `id`='3388';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}