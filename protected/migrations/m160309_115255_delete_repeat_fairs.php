<?php

class m160309_115255_delete_repeat_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE `id` IN (
			10047,
			10051,
			10075,
			10079,
			10050,
			10046,
			10048,
			10043,
			10052,
			10053,
			10054,
			10055,
			10056,
			10057,
			10058,
			10067,
			10059,
			10061,
			10062,
			10064,
			10049,
			10065,
			10042,
			10068,
			10076,
			10083,
			10084,
			10088,
			10093,
			10119,
			10105,
			10118,
			10120,
			10044,
			10112,
			10116,
			10089,
			10117,
			10111
			);
			DELETE FROM {{trfair}} WHERE `trParentId` IN (
			10047,
			10051,
			10075,
			10079,
			10050,
			10046,
			10048,
			10043,
			10052,
			10053,
			10054,
			10055,
			10056,
			10057,
			10058,
			10067,
			10059,
			10061,
			10062,
			10064,
			10049,
			10065,
			10042,
			10068,
			10076,
			10083,
			10084,
			10088,
			10093,
			10119,
			10105,
			10118,
			10120,
			10044,
			10112,
			10116,
			10089,
			10117,
			10111
			);
		";
	}
}