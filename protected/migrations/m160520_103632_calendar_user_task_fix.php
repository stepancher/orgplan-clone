<?php

class m160520_103632_calendar_user_task_fix extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		return true;
	}

	private function upSql() {

		return "
			ALTER TABLE {{calendarfairuserstasks}} DROP PRIMARY KEY;

			ALTER TABLE {{calendarfairuserstasks}} ADD PRIMARY KEY (`uuid`, `userId`);
			
			DELETE FROM {{calendarfairuserstasks}} WHERE taskId = 0;

			UPDATE {{calendarfairuserstasks}} user_task
			LEFT JOIN {{calendarfairtasks}} task ON task.id = user_task.taskId
			SET user_task.`uuid` = task.`uuid`;
  		";
	}
}