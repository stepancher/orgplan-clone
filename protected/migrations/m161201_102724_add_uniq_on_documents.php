<?php

class m161201_102724_add_uniq_on_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        Yii::import('application.modules.proposal.components.ShardedAR');
        ShardedAR::$_db = Yii::app()->db;
        $trDocuments = TrDocuments::model()->findAll();

        foreach($trDocuments as $trDocument){

            $file1 = Yii::app()->basePath.$trDocument->value;

            $trDocument->value = str_replace('static', 'uploads', $trDocument->value);

            $file2 = Yii::app()->basePath.$trDocument->value;

            if($file1 == $file2){
                continue;
            }

            $contentx =@file_get_contents($file1);
            $dirname = dirname($file2);
            if (!is_dir($dirname))
            {
                mkdir($dirname, 0755, true);
            }

            $openedfile = fopen($file2, "w");
            fwrite($openedfile, $contentx);
            fclose($openedfile);

            if ($contentx === FALSE) {
                $status=false;
            }else{
                $status=true;
            };

            $trDocument->save();
        }

        return "
            ALTER TABLE {{documents}}
            ADD CONSTRAINT `fk_tbl_documents_translates`
              FOREIGN KEY (`id`)
              REFERENCES {{trdocuments}} (`trParentId`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}