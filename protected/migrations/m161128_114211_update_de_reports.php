<?php

class m161128_114211_update_de_reports extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalName) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            
            UPDATE {$dbProposalName}.tbl_trreport SET columnDefs = '[{\"field\":\"firstName\",\"displayName\":\"Name\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\":true},
            {\"field\":\"lastName\",\"displayName\":\"Vorname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"countryRu\",\"displayName\":\"Land\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"companyRu\",\"displayName\":\"Firmenname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]' where trParentId = 9 AND langId = 'de';
            
            UPDATE {$dbProposalName}.tbl_trreport SET exportColumns = '[{\"firstName\":\"Name\",\"lastName\":\"Vorname\",\"countryRu\":\"Land\",\"companyRu\":\"Firmenname\"}]' where trParentId = 9 AND langId = 'de';
            
            
            
            UPDATE {$dbProposalName}.tbl_trreport SET columnDefs = '[{\"field\":\"firstName\",\"displayName\":\"Name\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"lastName\",\"displayName\":\"Vorname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"surname\",\"displayName\":\"Mittelname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"countryRu\",\"displayName\":\"Land\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"regionRu\",\"displayName\":\"Region\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"cityRu\",\"displayName\":\"Stadt\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"company\",\"displayName\":\"Firmenname\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"post\",\"displayName\":\"Position\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"street\",\"displayName\":\"Straße\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"house\",\"displayName\":\"Hausnummer\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"index\",\"displayName\":\"PLZ\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"phone\",\"displayName\":\"Tel.\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},
            {\"field\":\"fax\",\"displayName\":\"Fax\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]' where trParentId = 10 AND langId = 'de';
            
            UPDATE {$dbProposalName}.tbl_trreport SET exportColumns = '[{
            \"firstName\":\"Name\",
            \"lastName\":\"Vorname\",
            \"surname\":\"Mittelname\",
            \"countryRu\":\"Land\",
            \"regionRu\":\"Region\",
            \"cityRu\":\"Stadt\",
            \"company\":\"Firmenname\",
            \"post\":\"Position\",
            \"street\":\"Straße\",
            \"house\":\"Hausnummer\",
            \"index\":\"PLZ\",
            \"phone\":\"Tel.\",
            \"fax\":\"PLZ\"
            }]' where trParentId = 10 AND langId = 'de';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}