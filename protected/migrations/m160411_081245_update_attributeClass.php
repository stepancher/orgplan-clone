<?php

class m160411_081245_update_attributeClass extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET
				`tooltip`='<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг
				 увеличивается:<br/> \nна 50% при заказе после 25.08.16<br/>\nна 100% при заказе
				 после 23.09.16'
			WHERE `id`='318';
		";
	}
}