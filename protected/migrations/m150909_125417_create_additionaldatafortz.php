<?php

class m150909_125417_create_additionaldatafortz extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAdditionalDataForTz();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{additionaldatafortz}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAdditionalDataForTz(){
		return '
			DROP TABLE IF EXISTS {{additionaldatafortz}};
			CREATE TABLE {{additionaldatafortz}}(
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`name` varchar(255),
				`status` int(11)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;
		';
	}

}