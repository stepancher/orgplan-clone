<?php

class m161021_075045_copy_fairs_from_2016_to_2015 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_fair_2016_to_2015`()
            BEGIN

				DECLARE done BOOL DEFAULT FALSE;
                DECLARE reverse_year_interval INT DEFAULT 1;
                DECLARE source_fair_id INT DEFAULT 0;
                DECLARE uniq_text_save_pattern VARCHAR(50) DEFAULT \"<p>Раздел%\";
				DECLARE copy CURSOR FOR SELECT f16.id FROM {{fair}} f16 
                        LEFT JOIN {{fair}} f15 ON f15.storyId = f16.storyId AND YEAR(f15.beginDate) = 2015
                        WHERE YEAR(f16.beginDate) = 2016 AND 
                            f16.active = 1 AND 
                            f16.statistics LIKE \"%2015%\" AND 
                            f15.id IS NULL;
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;

    read_loop: LOOP
	    FETCH copy INTO source_fair_id;

        IF done THEN
            LEAVE read_loop;
        END IF;
                    
-- 1. Скопировать Выставки 2016-ого года, у которых поле {{fair}}.statistics содержит текст \"2015\".
-- 3. Установить даты у новых Выставок 2015-й год
-- 4. Установить дату статистики у новых Выставок 2015-01-01 (поле {{fair}}.statisticsDate)

            START TRANSACTION; 
                    INSERT INTO {{fair}} (
                    `logoId`,
                    `userId`,
                            `rating`,
                            `participationPrice`,
                            `registrationFee`,
                    `price`,
                            `organizerId`,
                            `active`,
                    `fairIsForum`,
                            `frequency`,
                            `description`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                    `squareGross`,
                            `squareNet`,
                            `members`,
                            `visitors`,
                    `activateControl`,
                    `originalId`,
                            `keyword`,
                            `uniqueText`,
                            `statistics`,
                            `currencyId`,
                            `fairTypeId`,
                    `statusId`,
                            `site`,
                    `mailLogoId`,
                    `lang`,
                    `shard`,
                            `storyId`,
                            `statisticsDate`
                    ) SELECT
                    `logoId`,
                    `userId`,
                            `rating`,
                            `participationPrice`,
                            `registrationFee`,
                    `price`,
                            `organizerId`,
                            `active`,
                    `fairIsForum`,
                            `frequency`,
                            `description`,
                            `exhibitionComplexId`,
                            `beginDate`            - INTERVAL `reverse_year_interval` YEAR,
                            `endDate`              - INTERVAL `reverse_year_interval` YEAR,
                            `beginMountingDate`    - INTERVAL `reverse_year_interval` YEAR,
                            `endMountingDate`      - INTERVAL `reverse_year_interval` YEAR,
                            `beginDemountingDate`  - INTERVAL `reverse_year_interval` YEAR,
                            `endDemountingDate`    - INTERVAL `reverse_year_interval` YEAR,
                    `squareGross`,
                            `squareNet`,
                            `members`,
                            `visitors`,
                    `activateControl`,
                    `originalId`,
                            `keyword`,
                            `uniqueText`,
                            `statistics`,
                            `currencyId`,
                            `fairTypeId`,
                    `statusId`,
                            `site`,
                    `mailLogoId`,
                    `lang`,
                    `shard`,
                            `storyId`,
                            '2015-01-01'
                        FROM {{fair}}
                        WHERE id = source_fair_id;
            COMMIT;

                        SET @new_fair_id = LAST_INSERT_ID();
                        
-- 2.1. Статистика РСВЯ (таблица {{fairinfo}}) 

            START TRANSACTION;
                        UPDATE {{fairinfo}} SET `fairId` = @new_fair_id WHERE `fairId` = source_fair_id;
            COMMIT;

-- 2.2. Ассоциации (таблица {{fairhasassociation}})
                        
            START TRANSACTION; 
                        UPDATE {{fairhasassociation}} SET `fairId` = @new_fair_id WHERE `fairId` = source_fair_id;
            COMMIT;

-- 2.3. Переводы (таблица {{trfair}})
                        
            START TRANSACTION; 
                        INSERT INTO {{trfair}} (`trParentId`,`langID`,`name`,`description`,`shortUrl`)
                        SELECT @new_fair_id, `langId`,`name`,`description`,`shortUrl` FROM {{trfair}} WHERE `trParentId` = source_fair_id;
            COMMIT;

-- 2.4. Отрасли (таблица {{fairhasindustry}})
                       
            START TRANSACTION;  
                        INSERT INTO {{fairhasindustry}} (`fairId`,`industryId`)
                        SELECT @new_fair_id, industryId FROM {{fairhasindustry}} WHERE `fairId` = source_fair_id;
            COMMIT;

-- 5. Установить рейтинг 3 у Выставок 2016-ого года (поле {{fair}}.rating)
                        
            START TRANSACTION; 
                        UPDATE {{fair}} SET `rating` = 3 WHERE `id` = source_fair_id;
            COMMIT;

-- 6. Установить значение NULL для поля {{fair}}.uniqueText у Выставок 2015-ого года, в случае если текст не начинается с фрагмента \"<p>Раздел\"
                        
            START TRANSACTION; 
                        UPDATE {{fair}} SET `uniqueText` = NULL WHERE `id` = @new_fair_id AND `uniqueText` NOT LIKE uniq_text_save_pattern;
            COMMIT;
                    
                    END LOOP;    
	CLOSE copy;
END;
                                
                CALL copy_fair_2016_to_2015();
                DROP PROCEDURE IF EXISTS `copy_fair_2016_to_2015`;  
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}