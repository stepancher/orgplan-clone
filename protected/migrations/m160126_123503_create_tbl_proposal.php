<?php

class m160126_123503_create_tbl_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{proposal}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS {{proposal}} (
				`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`companyName` varchar(255),
				`cityId` int(11),
				`product` varchar(255),
				`productDescription` text,
				`companyNameEn` varchar(255),
				`cityIdEn` int(11),
				`productEn` varchar(255),
				`productDescriptionEn` text,
				`currentFair` int(11),
				`ozerFair` varchar(255),
				`ozerContest` varchar(255),
				`exponentName` varchar(255),
				`exponentPost` varchar(255)
			) ENGINE = INNODB DEFAULT CHARSET = utf8;

			ALTER TABLE {{proposal}} ADD `exponentEmail` text;
			ALTER TABLE {{proposal}} ADD `exponentPhone` text;
			ALTER TABLE {{proposal}} ADD `fairId` int(11);
			ALTER TABLE {{proposal}} ADD `userId` int(11);
		';
	}
}