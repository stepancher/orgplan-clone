<?php

class m160608_101729_delete_industries extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{industry}} WHERE `id` in (157, 158);
			DELETE FROM {{trindustry}} WHERE `trParentId` in (157, 158);
			
			DELETE FROM {{fair}} WHERE `id` in (10842, 10610, 907);
			DELETE FROM {{trfair}} WHERE `trParentId` in (10842, 10610, 907);
		";
	}
}