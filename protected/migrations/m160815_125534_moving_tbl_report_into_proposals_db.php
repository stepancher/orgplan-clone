<?php

class m160815_125534_moving_tbl_report_into_proposals_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql  = $this->getAlterTable();
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getAlterTable(){

        list($peace1, $peace2, $dbProposalName) = explode('=', Yii::app()->dbProposal->connectionString);

        return "
            RENAME TABLE {{report}} TO {$dbProposalName}.tbl_report;
            
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"rowLabel\",\"attributeValue\":\"attributeValue\",\"currency\":\"currency\"}]' WHERE `id`='3';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"dependencyVal\":\"доп . единицы значение\",\"dependencyValUnitTitle\":\"доп . единицы название\",\"unitTitle\":\"ед . \",\"attributeValue\":\"кол . -во\",\"price\":\"цена\",\"currency\":\"валюта\",\"discount\":\"скидка\",\"sumRUB\":\"рубли\",\"sumUSD\":\"доллары\"}]' WHERE `id`='1';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"dependencyVal\":\"доп . единицы значение\",\"dependencyValUnitTitle\":\"доп . единицы название\",\"unitTitle\":\"ед . \",\"attributeValue\":\"кол . -во\",\"price\":\"цена\",\"currency\":\"валюта\",\"discount\":\"скидка\",\"sumRUB\":\"рубли\",\"sumUSD\":\"доллары\"}]' WHERE `id`='2';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"attributeValue\":\"кол . -во\",\"currency\":\"валюта\"}]' WHERE `id`='3';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"attributeValue\":\"кол . -во\", \"status\":\"Статус\"}]' WHERE `id`='4';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"attributeValue\":\"кол . -во\",\"currency\":\" % выполнения\"}]' WHERE `id`='3';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"attributeValue\":\"кол . -во\",\"status\":\"Статус\"}]' WHERE `id`='4';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"attributeValue\":\"Статус\"}]' WHERE `id`='4';
            UPDATE {$dbProposalName}.`tbl_report` SET `exportColumns`='[{\"rowLabel\":\"Название\",\"status\":\"Статус\"}]' WHERE `id`='4';
        ";
    }
}