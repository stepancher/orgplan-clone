<?php

class m160517_075835_add_new_exhibitioncomplex_shorttype extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{exhibitioncomplexshorttype}} (`name`) VALUES ('МВДЦ');
			UPDATE {{exhibitioncomplex}} SET `exhibitionComplexShortTypeId`='97' WHERE `id`='16';
		";
	}
}