<?php

class m170227_115746_add_exdb_fair_id_to_fairinfo extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            ALTER TABLE {$db}.{{fairinfo}} 
            ADD COLUMN `exdbFairId` INT(11) NULL DEFAULT NULL AFTER `statisticsDate`,
            ADD COLUMN `exdbCountriesCount` TEXT NULL DEFAULT NULL AFTER `exdbFairId`,
            ADD COLUMN `exdbStatisticsDate` TEXT NULL DEFAULT NULL AFTER `exdbFairId`;
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairstatistic}};
            CREATE TABLE {$expodata}.{{exdbfairstatistic}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` BIGINT NULL DEFAULT NULL,
              `areaSpecialExpositions` INT(11) NULL DEFAULT NULL,
              `squareNet` INT(11) NULL DEFAULT NULL,
              `members` INT(11) NULL DEFAULT NULL,
              `exhibitorsForeign` INT(11) NULL DEFAULT NULL,
              `exhibitorsLocal` INT(11) NULL DEFAULT NULL,
              `visitors` INT(11) NULL DEFAULT NULL,
              `visitorsForeign` INT(11) NULL DEFAULT NULL,
              `visitorsLocal` INT(11) NULL DEFAULT NULL,
              `statistics` TEXT NULL DEFAULT NULL,
              `statisticsDate` TEXT NULL DEFAULT NULL,
              `exdbCountriesCount` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            DROP TABLE IF EXISTS {$expodata}.{{exdbfair}};
            CREATE TABLE {$expodata}.{{exdbfair}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` BIGINT NULL DEFAULT NULL,
              `exdbFrequency` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `name_de` TEXT NULL DEFAULT NULL,
              `exhibitionComplexId` INT(11) NULL DEFAULT NULL,
              `beginDate` DATE NULL DEFAULT NULL,
              `endDate` DATE NULL DEFAULT NULL,
              `beginMountingDate` DATE NULL DEFAULT NULL,
              `endMountingDate` DATE NULL DEFAULT NULL,
              `beginDemountingDate` DATE NULL DEFAULT NULL,
              `endDemountingDate` DATE NULL DEFAULT NULL,
              `exdbBusinessSectors` TEXT NULL DEFAULT NULL,
              `exdbCosts` TEXT NULL DEFAULT NULL,
              `exdbShowType` TEXT NULL DEFAULT NULL,
              `site` TEXT NULL DEFAULT NULL,
              `storyId` TEXT NULL DEFAULT NULL,
              `infoId` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            ALTER TABLE {$db}.{{fair}} 
            ADD COLUMN `exdbFrequency` TEXT NULL DEFAULT NULL AFTER `proposalVersion`,
            ADD COLUMN `exdbBusinessSectors` TEXT NULL DEFAULT NULL AFTER `exdbFrequency`,
            ADD COLUMN `exdbCosts` TEXT NULL DEFAULT NULL AFTER `exdbBusinessSectors`,
            ADD COLUMN `exdbShowType` TEXT NULL DEFAULT NULL AFTER `exdbCosts`;
            
            ALTER TABLE {$db}.{{fair}} 
            CHANGE COLUMN `exdbCrc` `exdbCrc` BIGINT NULL DEFAULT NULL;
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairhasorganizer}};
            CREATE TABLE {$expodata}.{{exdbfairhasorganizer}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `fairId` INT(11) NULL DEFAULT NULL,
              `organizerId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`),
              INDEX `fairIdidx` (`fairId` ASC),
              INDEX `organizerIdidx` (`organizerId` ASC));

            DELETE FROM {$expodata}.{{exdbfairhasaudit}};
            
            ALTER TABLE {$db}.{{fair}} 
            ADD INDEX `exdbIdidx` (`exdbId` ASC),
            ADD INDEX `exdbCrcidx` (`exdbCrc` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}