<?php

class m160706_061242_9_proposal_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2644', '0', '0', '45');
			UPDATE {{attributeclass}} SET `parent`='2657' WHERE `id`='2658';
			UPDATE {{attributeclass}} SET `parent`='2657' WHERE `id`='2659';
			INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('25', 'ru', 'Заявка №9а', 'ПРОПУСКА НА ТРАНСПОРТ');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2632', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2632', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2632', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2632', '1600', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2632', NULL, 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2640', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2640', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2640', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2640', '5100', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2640', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2641', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2641', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2641', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2641', '26500', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2641', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2645', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2645', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2645', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2645', '4000', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2645', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2646', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2646', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2646', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2646', '6250', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2646', 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2647', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2647', 'selfCollapseLabel');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2647', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2647', '7000', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2647', 'currency');
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2158';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2159';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2160';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2161';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2162';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2659', '2500', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2656', '2500', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2653', '2500', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2650', '2500', 'price');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2659', NULL, 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2656', NULL, 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2653', NULL, 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2650', NULL, 'currency');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2659', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2656', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2653', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2650', 'unitTitle');
			UPDATE {{trattributeclass}} SET `label`='Indicate the sections where it is necessary to allocate the information about your company; also indicate the sections for allocation of your logo.<br><br>Information about the participating company is allocated in one of the sections of the catalogue free for charge.' WHERE `id`='2932';
			UPDATE {{trattributeclass}} SET `label`='Logos' WHERE `id`='2988';
			UPDATE {{trattributeclass}} SET `label`='Advertising layout in No.5 should be provided up to September 15, 2016. Issue date September 27, 2016.' WHERE `id`='3006';
			UPDATE {{proposalelementclass}} SET `active`='1' WHERE `id`='24';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2159', 'ru', 'Легковые автомобили');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2160', 'ru', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2162', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2164', 'ru', 'Грузовые автомобили');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2165', 'ru', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2167', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2169', 'ru', 'Мульткран (манипулятор и пр.)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2170', 'ru', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2172', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2174', 'ru', 'На период монтажа и демонтажа');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2175', 'ru', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2177', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2179', 'ru', 'На период проведения мероприятия');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2180', 'ru', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2182', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2184', 'ru', 'На общий период проведения мероприятия включая монтаж и демонтаж');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2185', 'ru', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2187', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2192', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2193', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2194', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2195', 'ru', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2196', 'ru', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2197', 'ru', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2198', 'ru', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2199', 'ru', 'шт.');
			DELETE FROM {{trattributeclass}} WHERE `id`='3041';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3021';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3022';
			UPDATE {{trattributeclass}} SET `label`=NULL WHERE `id`='3023';
			UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='2657';
			UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='2654';
			UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='2651';
			UPDATE {{attributeclass}} SET `collapse`='1' WHERE `id`='2648';
			UPDATE {{trattributeclass}} SET `label`='Норматив времени нахождения транспорта в зоне погрузки-разгрузки: для легковых автомобилей — 0,5 часа, для грузовых автомобилей — 1 час. При превышении норматива необходимы повторный заказ и оплата пропуска.\r\n\r\nДля проезда в зону разгрузки-погрузки транспортных средств, в отношении которых заказана разгрузка/погрузка силами «Крокус Экспо» (заполнена и оплачена заявка №9) необходимо получить БЕСПЛАТНЫЙ пропуск для получения которого представителю компании необходимо обратиться в сервис-центр «Крокус Экспо» не позже чем за один час до начала работ в соответствии с графиком разгрузки погрузки.' WHERE `id`='3019';
			DELETE FROM {{trattributeclass}} WHERE `id`='3035';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3015';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3016';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3017';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2645';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2646';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2647';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='3025';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='3028';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='3031';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='3034';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2662', 'currency');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2200', 'ru', 'руб');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n9a', 'int', '1', '0', '0', '2666', 'partiesSignature', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n9a', 'int', '1', '2666', '2666', '1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_right_block_n9a', 'int', '1', '2666', '2666', '2', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9a_1', 'string', '1', '2667', '2666', '1', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9a_2', 'string', '1', '2667', '2666', '2', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9a_3', 'string', '1', '2667', '2666', '3', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9a_1', 'string', '1', '2668', '2666', '1', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9a_2', 'string', '1', '2668', '2666', '2', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9a_3', 'string', '1', '2668', '2666', '3', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n9a_4', 'string', '1', '2667', '2666', '5', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n9a_4', 'string', '1', '2668', '2666', '5', 'text', '0');
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2129' WHERE `id`='2770';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2130' WHERE `id`='2771';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2132' WHERE `id`='2772';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2134' WHERE `id`='2773';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2135' WHERE `id`='2774';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2137' WHERE `id`='2775';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2139' WHERE `id`='2776';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2140' WHERE `id`='2777';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2142' WHERE `id`='2778';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2144' WHERE `id`='2779';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2145' WHERE `id`='2780';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2147' WHERE `id`='2781';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2149' WHERE `id`='2782';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2150' WHERE `id`='2783';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2152' WHERE `id`='2784';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2154' WHERE `id`='2785';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2155' WHERE `id`='2786';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2157' WHERE `id`='2787';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2162' WHERE `id`='2788';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2163' WHERE `id`='2789';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2164' WHERE `id`='2790';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2165' WHERE `id`='2791';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2166' WHERE `id`='2792';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2167' WHERE `id`='2793';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2168' WHERE `id`='2794';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2169' WHERE `id`='2795';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='2170' WHERE `id`='2796';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2128';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2129';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2130';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2131';
			UPDATE {{attributeclassproperty}} SET `attributeClass`='2639' WHERE `id`='2132';
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '2623', '2623', 'increaseByLeftDay', '{1000:1, 21:2}', '1');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2641', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2640', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2639', '2623', 'inherit', '0');

		";
	}
}