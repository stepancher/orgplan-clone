<?php

class m161121_101838_delete_fairs_13060_12778_12750 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE `id` = 13060;
            DELETE FROM {{fair}} WHERE `id` = 12778;
            DELETE FROM {{fair}} WHERE `id` = 12750;
            
            DELETE FROM {{fairinfo}} WHERE `id` = '1717';
            DELETE FROM {{fairinfo}} WHERE `id` = '1297';
            DELETE FROM {{fairinfo}} WHERE `id` = '1425';
            
            ALTER TABLE {{fairhasindustry}} 
            DROP FOREIGN KEY `fk_fhi_industryid`,
            DROP FOREIGN KEY `fk_fhi_fairid`;
            ALTER TABLE {{fairhasindustry}} 
            DROP INDEX `fhi_industryid` ,
            DROP INDEX `fairIdAndIndustryId` ;

            ALTER TABLE {{fairhasindustry}} 
            CHANGE COLUMN `fairId` `fairId` INT(11) NOT NULL ;
            
            ALTER TABLE {{fairhasindustry}} 
            CHANGE COLUMN `industryId` `industryId` INT(11) NOT NULL ;
            
            ALTER TABLE {{fairhasindustry}} 
            ADD UNIQUE INDEX `fairIdAndIndustryId` (`fairId` ASC, `industryId` ASC);

            ALTER TABLE {{fairhasindustry}} 
            ADD CONSTRAINT `fk_fhi_fairid`
              FOREIGN KEY (`fairId`)
              REFERENCES {{fair}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
            ALTER TABLE {{fairhasindustry}} 
            ADD INDEX `fhi_industryId` (`industryId` ASC);

            ALTER TABLE {{fairhasindustry}} 
            ADD CONSTRAINT `fk_fhi_industryid`
              FOREIGN KEY (`industryId`)
              REFERENCES {{industry}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
              ALTER TABLE {{fair}} 
            ADD INDEX `infoId_index` (`infoId` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}