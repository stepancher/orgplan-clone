<?php

class m151030_104154_alter_task extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{

		echo "alter_Task does not support migration down.\n";
		return false;
	}

	public function getAlterTable(){
		return '
			UPDATE  {{task}} SET  `eventType` =  "3" WHERE  {{task}}.`name` LIKE "%Демонтаж выставки%" AND {{task}}.`eventType` IS NULL;
			UPDATE  {{task}} SET  `eventType` =  "1" WHERE  {{task}}.`name` LIKE "%Монтаж выставки%" AND {{task}}.`eventType` IS NULL;
			UPDATE  {{task}} SET  `eventType` =  "2" WHERE  {{task}}.`name` LIKE "%Дни работы выставки%" AND {{task}}.`eventType` IS NULL;
			UPDATE  {{task}} SET  `eventType` =  "0" WHERE  {{task}}.`eventType` IS NULL;
		';
	}
}