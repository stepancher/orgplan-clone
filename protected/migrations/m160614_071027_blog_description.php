<?php

class m160614_071027_blog_description extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{route}} SET `description`='Как участвовать в выставках, как пригласить клиентов, планирование работы персонала,  идеи для выставочных стендов, другие темы от экспертов блога protoplan.pro' WHERE `id`='93591';
			UPDATE {{route}} SET `description`='Как участвовать в выставках, как пригласить клиентов, планирование работы персонала,  идеи для выставочных стендов, другие темы от экспертов блога protoplan.pro' WHERE `id`='93626';
			UPDATE {{route}} SET `description`='С помощью поиска PROTOPLAN вы можете найти любую выставку на территории России. Архив с января 2015 года.', `header`='Краткое руководство | Protoplan' WHERE `id`='93580';
			UPDATE {{route}} SET `description`='Могу ли я доверять статистике PROTOPLAN? Все статистические данные взяты из официальных источников, ссылка на источник публикуется на странице, вы можете проверить.', `header`='Вопрос-ответ | Protoplan' WHERE `id`='93587';
			UPDATE {{route}} SET `header`='Каталог отраслей | Protoplan' WHERE `id`='93588';
			UPDATE {{route}} SET `header`='Политика конфиденциальности | Protoplan' WHERE `id`='93589';
			UPDATE {{route}} SET `header`='Блог | Protoplan' WHERE `id`='93591';
			UPDATE {{route}} SET `header`='Выставки и конференции в городах России, СНГ и Мира - PROTOPLAN.PRO' WHERE `id`='93593';
			UPDATE {{route}} SET `header`='Каталог цен на товары и услуги | Protoplan' WHERE `id`='93596';
			UPDATE {{route}} SET `header`='Пользовательское соглашение | Protoplan' WHERE `id`='93598';
			UPDATE {{route}} SET `header`='Краткое руководство | Protoplan' WHERE `id`='93615';
			UPDATE {{route}} SET `header`='Вопрос-ответ | Protoplan' WHERE `id`='93622';
			UPDATE {{route}} SET `header`='Каталог отраслей | Protoplan' WHERE `id`='93623';
			UPDATE {{route}} SET `header`='Политика конфиденциальности | Protoplan' WHERE `id`='93624';
			UPDATE {{route}} SET `header`='Блог | Protoplan' WHERE `id`='93626';
			UPDATE {{route}} SET `header`='Каталог цен на товары и услуги | Protoplan' WHERE `id`='93630';
			UPDATE {{route}} SET `header`='Пользовательское соглашение | Protoplan' WHERE `id`='93632';
			UPDATE {{route}} SET `description`='С помощью поиска PROTOPLAN вы можете найти любую выставку на территории России. Архив с января 2015 года.' WHERE `id`='93615';
			UPDATE {{route}} SET `description`='Могу ли я доверять статистике PROTOPLAN? Все статистические данные взяты из официальных источников, ссылка на источник публикуется на странице, вы можете проверить.' WHERE `id`='93622';
		";
	}
}