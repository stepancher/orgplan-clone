<?php

class m160114_144133_tbl_documents_add_data extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{documents}} WHERE
			name = "Список экспонатов" AND
			description = "Описание формы Список экспонатов" AND
			type = 2 AND
			value = "/ru/site/DocumentExhibits/";
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAlterTable(){
		return '
			INSERT INTO {{documents}} (`name`, `description`, `type`, `value`) VALUES ("Список экспонатов", "Описание формы Список экспонатов", 2, "/ru/site/DocumentExhibits/");
		';
	}
}