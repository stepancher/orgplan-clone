<?php

class m160428_073051_update_usersetting_notification extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			ALTER TABLE {{usersetting}}
			CHANGE COLUMN `dailyEventNotification` `dailyEventNotification` TINYINT(1) NOT NULL DEFAULT 0,
			CHANGE COLUMN `notificationTime` `notificationTime` INT(11) NOT NULL DEFAULT 8;

			UPDATE {{usersetting}}
			SET `dailyEventNotification` = '0', `notificationTime` = '8';
		";
	}

	private function downSql() {

		return "
			ALTER TABLE {{usersetting}}
			CHANGE COLUMN `dailyEventNotification` `dailyEventNotification` TINYINT(1) NULL DEFAULT NULL,
			CHANGE COLUMN `notificationTime` `notificationTime` INT(11) NULL DEFAULT NULL;

			UPDATE {{usersetting}}
			SET `dailyEventNotification` = NULL, `notificationTime` = NULL;
		";
	}
}