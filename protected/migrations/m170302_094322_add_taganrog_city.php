<?php

class m170302_094322_add_taganrog_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('60', 'taganrog');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1533', 'ru', 'Таганрог');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1533', 'en', 'Taganrog');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1533', 'de', 'Taganrog');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('31', 'balashikha');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1534', 'ru', 'Балашиха');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1534', 'en', 'Balashikha');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1534', 'de', 'Balaschicha');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('60', 'chkalovsky');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1535', 'ru', 'Чкаловский');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1535', 'en', 'Чкаловский');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1535', 'de', 'Чкаловский');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}