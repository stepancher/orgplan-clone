<?php

class m161028_110400_add_new_calendarfairtasks_for10943 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            
            ALTER TABLE {{calendarfairtasks}}
              AUTO_INCREMENT = 5010 ;
            
            INSERT INTO {{dependences}} (`action`, params, run) VALUES
            
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":5, \"save\":1}', 'calendar/gantt/add/id/5001'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":5, \"send\":1}', 'calendar/gantt/add/id/5001'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":5, \"send\":1}', 'calendar/gantt/complete/id/5001'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":5, \"send\":1}', 'calendar/gantt/complete/id/5003'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":5, \"send\":1}', 'calendar/gantt/complete/id/5005'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":5}', 'calendar/gantt/add/id/5001'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":5}', 'calendar/gantt/complete/id/5001'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":5}', 'calendar/gantt/complete/id/5003'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":5}', 'calendar/gantt/complete/id/5005'),
            
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/reject/id/5001/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/reject/id/5002/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/reject/id/5003/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/reject/id/5004/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/reject/id/5005/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/reject/id/5006/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/add/id/5001'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/add/id/5003'),
            ('proposal/proposal/reject', '{\"ecId\":5}', 'calendar/gantt/add/id/5005'),
            
            ('proposal/proposal/reject', '{\"ecId\":21}', 'calendar/gantt/reject/id/5008/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":21}', 'calendar/gantt/add/id/5008'),
            
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":21, \"save\":1}', 'calendar/gantt/add/id/5008'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":21, \"send\":1}', 'calendar/gantt/add/id/5008'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":21, \"send\":1}', 'calendar/gantt/complete/id/5008'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":21}', 'calendar/gantt/add/id/5008'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":21}', 'calendar/gantt/complete/id/5008'),
            
            ('proposal/proposal/reject', '{\"ecId\":22}', 'calendar/gantt/reject/id/5007/userId/{:userId}'),
            ('proposal/proposal/reject', '{\"ecId\":22}', 'calendar/gantt/add/id/5007'),
            
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":22, \"save\":1}', 'calendar/gantt/add/id/5007'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":22, \"send\":1}', 'calendar/gantt/add/id/5007'),
            ('proposal/proposal/create', '{\"fairId\":10943, \"ecId\":22, \"send\":1}', 'calendar/gantt/complete/id/5007'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":22}', 'calendar/gantt/add/id/5007'),
            ('proposal/proposal/send',   '{\"fairId\":10943, \"ecId\":22}', 'calendar/gantt/complete/id/5007');
            

            INSERT INTO {{calendarfairtasks}} (id, fairId, uuid, parent, `group`, duration, `date`, color, completeButton, `first`)
            VALUES
            ('5001', '10943', '33755881984295008', NULL,                '33755881984295008', NULL, '2017-07-01 00:00:01', '', '0', '0'),
            ('5002', '10943', '33755881984295009', '33755881984295008', '33755881984295009', '10',  NULL,                 '#f6a800 ', '1', '0'),
            ('5003', '10943', '33755881984295010', NULL,                '33755881984295008', '29', '2017-10-22 00:00:00', '', '0', '0'),
            ('5004', '10943', '33755881984295011', '33755881984295010', '33755881984295011', '5',   NULL,                 '#f6a800 ', '1', '0'),
            ('5005', '10943', '33755881984295012', NULL,                '33755881984295008', '10', '2017-11-20 00:00:00', '', '0', '0'),
            ('5006', '10943', '33755881984295013', '33755881984295012', '33755881984295013', '3',   NULL,                 '#f96a0e', '1', '0'),
            
            ('5007', '10943', '33755881984295014', NULL,                '33755881984295014', NULL, '2017-11-11 23:59:59', NULL, '0', '0'),
            ('5008', '10943', '33755881984295015', NULL,                '33755881984295015', NULL, '2017-10-16 23:59:59', NULL, '0', '0');


            INSERT INTO {{trcalendarfairtasks}} (trParentId, langId, `name`, `desc`)
            VALUES
            ('33755881984295008', 'ru', 'Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ', 'Задача завершится автоматически, когда будет отправлена заявка №3'),
            ('33755881984295008', 'en', 'Send the Application No.3 ANCILLARY EQUIPMENT', 'The task will be completed automatically after sending the Application No.3'),
            ('33755881984295008', 'de', 'Send the Application No.3 ANCILLARY EQUIPMENT', 'The task will be completed automatically after sending the Application No.3'),
            
            ('33755881984295009', 'ru', 'Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ', 'Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295009', 'en', 'Pay for the service ordered (Application No.3)', 'Click COMPLETE if you have paid'),
            ('33755881984295009', 'de', 'Pay for the service ordered (Application No.3)', 'Click COMPLETE if you have paid'),
            
            ('33755881984295010', 'ru', 'Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)', 'Задача завершится автоматически, когда будет отправлена заявка №3'),
            ('33755881984295010', 'en', 'Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 50%)', 'The task will be completed automatically after sending the Application No.3'),
            ('33755881984295010', 'de', 'Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 50%)', 'The task will be completed automatically after sending the Application No.3'),
            
            ('33755881984295011', 'ru', 'Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 50%)', 'Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295011', 'en', 'Pay for the service ordered (Application No.3) (increase of price 50%)', 'Click COMPLETE if you have paid'),
            ('33755881984295011', 'de', 'Pay for the service ordered (Application No.3) (increase of price 50%)', 'Click COMPLETE if you have paid'),
            
            ('33755881984295012', 'ru', 'Отправить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)', 'Задача завершится автоматически, когда будет отправлена заявка №3'),
            ('33755881984295012', 'en', 'Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 100%)', 'The task will be completed automatically after sending the Application No.3'),
            ('33755881984295012', 'de', 'Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 100%)', 'The task will be completed automatically after sending the Application No.3'),
            
            ('33755881984295013', 'ru', 'Оплатить заявку №3 ДОПОЛНИТЕЛЬНОЕ ОБОРУДОВАНИЕ (с наценкой 100%)', 'Нажмите \"Завершить\", когда будет произведена оплата'),
            ('33755881984295013', 'en', 'Pay for the service ordered (Application No.3) (increase of price 100%)', 'Click COMPLETE if you have paid'),
            ('33755881984295013', 'de', 'Pay for the service ordered (Application No.3) (increase of price 100%)', 'Click COMPLETE if you have paid'),
            
            ('33755881984295014', 'ru', 'Отправить заявку №12 \"БЕЙДЖИ УЧАСТНИКОВ\"', 'Задача завершится автоматически, когда будет отправлена заявка №12'),
            ('33755881984295014', 'en', 'Send the Application No.12 PARTICIPANT BADGES', 'The task will be completed automatically after sending the Application No.12'),
            ('33755881984295014', 'de', 'Send the Application No.12 PARTICIPANT BADGES', 'The task will be completed automatically after sending the Application No.12'),
            
            ('33755881984295015', 'ru', 'Отправить заявку №12a \"VIP БЕЙДЖИ\"', 'Задача завершится автоматически, когда будет отправлена заявка №12a'),
            ('33755881984295015', 'en', 'Send the Application No.12a VIP BADGES', 'The task will be completed automatically after sending the Application No.12a'),
            ('33755881984295015', 'de', 'Send the Application No.12a VIP BADGES', 'The task will be completed automatically after sending the Application No.12a');
            
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}