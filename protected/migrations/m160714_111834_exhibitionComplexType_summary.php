<?php

class m160714_111834_exhibitionComplexType_summary extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			CREATE TABLE {{exhibitioncomplextypesummary}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `exhibitionComplexTypeId` INT(11) NOT NULL,
			  `exhibitionComplexCount` INT(11) NULL DEFAULT NULL,
			  `datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  PRIMARY KEY (`id`),
			  UNIQUE INDEX `exhibitionComplexTypeId_UNIQUE` (`exhibitionComplexTypeId` ASC));
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}