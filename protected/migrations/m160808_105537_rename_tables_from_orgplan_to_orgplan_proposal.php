<?php

class m160808_105537_rename_tables_from_orgplan_to_orgplan_proposal extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->getCreateTable();
        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getCreateTable()
    {

        list($peace1, $peace2, $dbProposalName)  = explode('=', Yii::app()->dbProposal->connectionString);

        return "            
            RENAME TABLE {{attribute}} TO {$dbProposalName}.tbl_attribute;
            RENAME TABLE {{attributeclass}} TO {$dbProposalName}.tbl_attributeclass;
            RENAME TABLE {{attributeclassdependency}} TO {$dbProposalName}.tbl_attributeclassdependency;
            RENAME TABLE {{attributeclassproperty}} TO {$dbProposalName}.tbl_attributeclassproperty;
            RENAME TABLE {{attributeenum}} TO {$dbProposalName}.tbl_attributeenum;
            RENAME TABLE {{attributeenumproperty}} TO {$dbProposalName}.tbl_attributeenumproperty;
            RENAME TABLE {{attributefile}} TO {$dbProposalName}.tbl_attributefile;
            RENAME TABLE {{attributemodel}} TO {$dbProposalName}.tbl_attributemodel;
            RENAME TABLE {{attributemulty}} TO {$dbProposalName}.tbl_attributemulty;
            RENAME TABLE {{organizerexponent}} TO {$dbProposalName}.tbl_organizerexponent;
            RENAME TABLE {{pcity}} TO {$dbProposalName}.tbl_pcity;
            RENAME TABLE {{pcountry}} TO {$dbProposalName}.tbl_pcountry;
            RENAME TABLE {{pregion}} TO {$dbProposalName}.tbl_pregion;
            RENAME TABLE {{proposalelement}} TO {$dbProposalName}.tbl_proposalelement;
            RENAME TABLE {{proposalelementclass}} TO {$dbProposalName}.tbl_proposalelementclass;
            RENAME TABLE {{proposalelementclasshasnotification}} TO {$dbProposalName}.tbl_proposalelementclasshasnotification;
            RENAME TABLE {{proposalelementhasnotification}} TO {$dbProposalName}.tbl_proposalelementhasnotification;
            RENAME TABLE {{proposalelementjson}} TO {$dbProposalName}.tbl_proposalelementjson;
            RENAME TABLE {{trattributeclass}} TO {$dbProposalName}.tbl_trattributeclass;
            RENAME TABLE {{trattributeclassproperty}} TO {$dbProposalName}.tbl_trattributeclassproperty;
            RENAME TABLE {{trpcity}} TO {$dbProposalName}.tbl_trpcity;
            RENAME TABLE {{trpcountry}} TO {$dbProposalName}.tbl_trpcountry;
            RENAME TABLE {{trpregion}} TO {$dbProposalName}.tbl_trpregion;
            RENAME TABLE {{trproposalelementclass}} TO {$dbProposalName}.tbl_trproposalelementclass;
        ";
    }
}