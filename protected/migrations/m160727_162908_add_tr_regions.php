<?php

class m160727_162908_add_tr_regions extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('124', 'en', 'Baku-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('125', 'en', 'Yerevan-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('126', 'en', 'Minsk-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('127', 'en', 'Mogilev-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('128', 'en', 'Grodno-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('129', 'en', 'Astana-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('130', 'en', 'Almaty-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('131', 'en', 'Atyrau-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('132', 'en', 'Mangystau-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('133', 'en', 'Aktobe-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('134', 'en', 'Pavlodar-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('135', 'en', 'Bishkek-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('136', 'en', 'Kishinev-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('137', 'en', 'Karaganda-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('138', 'en', 'Tashkent-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('139', 'en', 'Fergana-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('140', 'en', 'Ashgabat-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('141', 'en', 'Balkan-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('142', 'en', 'Tbilisi-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('143', 'en', 'Ulaanbaatar-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('144', 'en', 'Kiev-region');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('145', 'en', 'Odessa-oblast');

			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('124', 'ru', 'Регион Баку');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('125', 'ru', 'Регион Ереван');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('126', 'ru', 'Минская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('127', 'ru', 'Могилёвская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('128', 'ru', 'Гродненская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('129', 'ru', 'Регион Астана');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('130', 'ru', 'Регион Алматы');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('131', 'ru', 'Атырауская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('132', 'ru', 'Мангистауская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('133', 'ru', 'Актюбинская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('134', 'ru', 'Павлодарская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('135', 'ru', 'Регион Бишкек');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('136', 'ru', 'Кишиневская агломерация');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('137', 'ru', 'Карагандинская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('138', 'ru', 'Ташкентская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('139', 'ru', 'Ферганская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('140', 'ru', 'Регион Ашхабад');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('141', 'ru', 'Балканский велаят');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('142', 'ru', 'Регион Тбилиси');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('143', 'ru', 'Регион Улан-Батор');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('144', 'ru', 'Киевская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('145', 'ru', 'Одесская область');
			
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('124', 'de', 'Регион Баку');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('125', 'de', 'Регион Ереван');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('126', 'de', 'Минская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('127', 'de', 'Могилёвская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('128', 'de', 'Гродненская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('129', 'de', 'Регион Астана');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('130', 'de', 'Регион Алматы');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('131', 'de', 'Атырауская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('132', 'de', 'Мангистауская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('133', 'de', 'Актюбинская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('134', 'de', 'Павлодарская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('135', 'de', 'Регион Бишкек');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('136', 'de', 'Кишиневская агломерация');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('137', 'de', 'Карагандинская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('138', 'de', 'Ташкентская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('139', 'de', 'Ферганская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('140', 'de', 'Регион Ашхабад');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('141', 'de', 'Балканский велаят');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('142', 'de', 'Регион Тбилиси');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('143', 'de', 'Регион Улан-Батор');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('144', 'de', 'Киевская область');
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('145', 'de', 'Одесская область');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}