<?php

class m161025_142453_add_new_protoplan_proposal_deadline extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            UPDATE  {$dbProposalF10943}.tbl_proposalelementclass SET `deadline`='2017-12-03 23:59:59' WHERE `id`='4';
            UPDATE  {$dbProposalF10943}.tbl_proposalelementclass SET `deadline`='2017-12-03 23:59:59' WHERE `id`='24';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}