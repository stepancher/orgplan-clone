<?php

class m160623_094705_update_tr_12a_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12a_hint', 'string', '0', '0', '0', '2296', '10', 'hintWithView', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12a_agreement', 'bool', '0', '2296', '2296', '20', 'checkboxAgreement', '0');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2221', '0', '0', '30');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2236', '0', '0', '40');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2251', '0', '0', '50');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2266', '0', '0', '60');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2281', '0', '0', '70');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2296', '0', '0', '80');
			
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2205', 'ru', 'Отправьте заполненную форму в дирекцию выставки не позднее 20 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`) VALUES ('2296', 'ru', 'VIP бейдж дает право посещения выставки АГРОСАЛОН и всех мероприятий деловой программы. <br><br> Количество VIP бейджей, предоставляемых Экспоненту зависит от площади его стенда и определяется следующим образом: <br> Стенд до 100м² - 1 шт. <br> Стенд от 100м² до 500м² - 2 шт. <br> Стенд более 500м² - 3 шт. ', '');
			
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2206', 'ru', '1. VIP бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2207', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2208', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2209', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2210', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2211', 'ru', 'Компания');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2212', 'ru', 'Почтовый адрес');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2213', 'ru', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2214', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2215', 'ru', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2216', 'ru', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2217', 'ru', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2218', 'ru', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2219', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2220', 'ru', 'Факс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2221', 'ru', '2. VIP бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2222', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2223', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2224', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2225', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2226', 'ru', 'Компания');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2227', 'ru', 'Почтовый адрес');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2228', 'ru', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2229', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2230', 'ru', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2231', 'ru', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2232', 'ru', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2233', 'ru', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2234', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2235', 'ru', 'Факс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2236', 'ru', '3. VIP бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2237', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2238', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2239', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2240', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2241', 'ru', 'Компания');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2242', 'ru', 'Почтовый адрес');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2243', 'ru', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2244', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2245', 'ru', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2246', 'ru', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2247', 'ru', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2248', 'ru', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2249', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2250', 'ru', 'Факс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2251', 'ru', '4. VIP бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2252', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2253', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2254', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2255', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2256', 'ru', 'Компания');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2257', 'ru', 'Почтовый адрес');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2258', 'ru', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2259', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2260', 'ru', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2261', 'ru', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2262', 'ru', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2263', 'ru', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2264', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2265', 'ru', 'Факс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2266', 'ru', '5. VIP бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2267', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2268', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2269', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2270', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2271', 'ru', 'Компания');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2272', 'ru', 'Почтовый адрес');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2273', 'ru', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2274', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2275', 'ru', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2276', 'ru', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2277', 'ru', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2278', 'ru', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2279', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2280', 'ru', 'Факс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2281', 'ru', '6. VIP бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2282', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2283', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2284', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2285', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2286', 'ru', 'Компания');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2287', 'ru', 'Почтовый адрес');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2288', 'ru', 'Индекс');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2289', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2290', 'ru', 'Регион');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2291', 'ru', 'Город');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2292', 'ru', 'Улица');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2293', 'ru', 'Дом');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2294', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2295', 'ru', 'Факс');
";
	}
}