<?php

class m160419_034206__alter_pcountry_pregion_pcity_tables_update_columns_and_keys extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{pcountry}}
			CHANGE COLUMN `country_id` `countryId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
			CHANGE COLUMN `city_id` `cityId` INT(11) NOT NULL DEFAULT '0' ,
			DROP INDEX `city_id` ,
			ADD INDEX `cityId` (`cityId` ASC);



			ALTER TABLE {{pregion}}
			CHANGE COLUMN `region_id` `regionId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
			CHANGE COLUMN `country_id` `countryId` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
			CHANGE COLUMN `city_id` `cityId` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
			DROP INDEX `country_id` ,
			ADD INDEX `countryId` (`countryId` ASC),
			DROP INDEX `city_id` ,
			ADD INDEX `cityId` (`cityId` ASC);


			ALTER TABLE {{pcity}}
			CHANGE COLUMN `city_id` `cityId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
			CHANGE COLUMN `country_id` `countryId` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
			CHANGE COLUMN `region_id` `regionId` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
			DROP INDEX `country_id` ,
			ADD INDEX `countryId` (`countryId` ASC),
			DROP INDEX `region_id` ,
			ADD INDEX `regionId` (`regionId` ASC);

			ALTER TABLE {{pcity}}
			CHANGE COLUMN `cityId` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;

			ALTER TABLE {{pregion}}
			CHANGE COLUMN `regionId` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

			ALTER TABLE {{pcountry}}
			CHANGE COLUMN `countryId` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;

	    ";
	}
}