<?php

class m160406_152620_create_new_tables_for_calendar extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return '
			DROP TABLE IF EXISTS `tbl_calendarfairtasks`;
			CREATE TABLE `tbl_calendarfairtasks` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `fairId` int(11) DEFAULT NULL,
			  `calendar` tinyint(1) NOT NULL DEFAULT \'0\',
			  `name` varchar(245) NOT NULL,
			  `duration` mediumint(8) DEFAULT NULL,
			  `startDate` datetime DEFAULT NULL,
			  `endDate` datetime DEFAULT NULL,
			  `color` varchar(45) DEFAULT NULL,
			  `userComplete` tinyint(1) NOT NULL DEFAULT \'0\',
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
			
			DROP TABLE IF EXISTS `tblcalendarfairuserstasks`;
			CREATE TABLE `tbl_calendarfairuserstasks` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `taskId` int(11) NOT NULL,
			  `userId` int(11) NOT NULL,
			  `startDate` datetime DEFAULT NULL,
			  `completeDate` datetime DEFAULT NULL,
			  `color` varchar(45) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
			
			DROP TABLE IF EXISTS `tbl_dependences`;
			CREATE TABLE `tbl_dependences` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `action` varchar(145) NOT NULL,
			  `params` varchar(145) DEFAULT \'{}\',
			  `run` varchar(145) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `action` (`action`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
		';
	}

	private function downSql() {

		return '
			DROP TABLE IF EXISTS `tbl_calendarfairtasks`;
			DROP TABLE IF EXISTS `tbl_calendarfairuserstasks`;
			DROP TABLE IF EXISTS `tbl_dependences`;
		';
	}
}