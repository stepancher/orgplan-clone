<?php

class m151127_112818_tbl_trCity extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{trcity}}
			WHERE trParentId IN (
				1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
				37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,
				70,71,72,73,74,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,
				106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,
				132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,
				158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182
			)
			AND langId = "de";
		';


		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("1", "de", "Moskau");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("2", "de", "Dserschinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("3", "de", "Kaluga");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("4", "de", "Sankt Petersburg");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("5", "de", "Jekaterinburg");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("6", "de", "Surgut");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("7", "de", "Omsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("8", "de", "Rostow am Don");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("9", "de", "Krasnodar");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("10", "de", "Juschno-Sachalinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("11", "de", "Petropawlowsk-Kamtschatski");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("12", "de", "Adler");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("13", "de", "Sotschi");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("14", "de", "Jaroslawl");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("15", "de", "Tscheljabinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("16", "de", "Uljanowsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("17", "de", "Tula");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("18", "de", "Tomsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("19", "de", "Twer");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("20", "de", "Smolensk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("21", "de", "Saratow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("22", "de", "Rjasan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("23", "de", "Pskow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("24", "de", "Pskow, Dorf Borisewitschi");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("25", "de", "Pensa");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("26", "de", "Orenburg");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("27", "de", "Weliki Nowgorod");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("28", "de", "Murmansk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("29", "de", "Lipezk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("30", "de", "Kostroma");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("31", "de", "Kirow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("32", "de", "Kemerowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("33", "de", "Woronesch");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("34", "de", "Wologda");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("35", "de", "Wolgograd");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("36", "de", "Brjansk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("37", "de", "Wladimir");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("38", "de", "Astrachan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("39", "de", "Archangelsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("40", "de", "Blagoweschtschensk ");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("41", "de", "Chabarowsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("42", "de", "Wladiwostok");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("43", "de", "Barnaul");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("44", "de", "Tscheboksary");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("45", "de", "Grosny");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("46", "de", "Abakan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("47", "de", "Ischewsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("48", "de", "Kysyl");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("49", "de", "Jakutsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("50", "de", "Joschkar-Ola ");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("51", "de", "Syktywkar");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("52", "de", "Petrosawodsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("53", "de", "Machatschkala");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("54", "de", "Ulan-Ude");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("55", "de", "Ufa");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("56", "de", "Tschita");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("57", "de", "Chanty-Mansijsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("58", "de", "Tjumen");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("59", "de", "Stawropol");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("60", "de", "Saransk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("61", "de", "Samara");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("62", "de", "Perm");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("63", "de", "Nowosibirsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("64", "de", "Nowokusnezk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("65", "de", "Nischni Nowgorod");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("66", "de", "Kursk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("67", "de", "Krasnojarsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("68", "de", "Kaliningrad");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("69", "de", "Kasan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("70", "de", "Irkutsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("71", "de", "Belgorod");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("72", "de", "Krasnogorsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("73", "de", "Krasnogorsky Chutor");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("74", "de", "Kubinka");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("79", "de", "Elisowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("80", "de", "Almetjewsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("81", "de", "Anadyr");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("82", "de", "Anapa");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("83", "de", "Beslan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("84", "de", "Bijsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("85", "de", "Birobidschan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("86", "de", "Bologoe");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("87", "de", "Weliki Ustjug");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("88", "de", "Wladikawkas");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("89", "de", "Workuta");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("90", "de", "Gatschina");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("91", "de", "Gdow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("92", "de", "Gelendschik");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("93", "de", "Gorno-Altaisk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("94", "de", "Gudermes");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("95", "de", "Gus-Chrustalny");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("96", "de", "Dmitrow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("97", "de", "Dubna");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("98", "de", "Jeisk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("99", "de", "Elabuga");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("100", "de", "Jelez");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("101", "de", "Essentuki");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("102", "de", "Slatoust");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("103", "de", "Iwanowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("104", "de", "Kislowodsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("105", "de", "Komsomolsk am Amur");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("106", "de", "Kotlas");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("107", "de", "Kurgan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("108", "de", "Lenogorsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("109", "de", "Lensk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("110", "de", "Luga");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("111", "de", "Ljuban");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("112", "de", "Ljuberzy");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("113", "de", "Magadan");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("114", "de", "Maikop");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("115", "de", "Miass");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("116", "de", "Mineralnyje Wody");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("117", "de", "Mirny");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("118", "de", "Murom");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("119", "de", "Mytischtschi");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("120", "de", "Nabereschnyje Tschelny");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("121", "de", "Nadym");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("122", "de", "Naltschik");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("123", "de", "Nasran");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("124", "de", "Narjan-Mar");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("125", "de", "Nachodka");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("126", "de", "Nischnewartowsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("127", "de", "Nischnekamsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("128", "de", "Nischni Tagil");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("129", "de", "Nowy Urengoi");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("130", "de", "Norilsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("131", "de", "Obninsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("132", "de", "Oktjabrski");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("133", "de", "Orechowo-Sujewo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("134", "de", "Orel");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("135", "de", "Podolsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("136", "de", "Pjatigorsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("137", "de", "Rybinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("138", "de", "Salechard");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("139", "de", "Sewerodwinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("140", "de", "Sol-Ilezk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("141", "de", "Tambow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("142", "de", "Tobolsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("143", "de", "Togliatti");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("144", "de", "Tuapse");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("145", "de", "Tynda");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("146", "de", "Ulan-Ude");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("147", "de", "Tschebarkul");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("148", "de", "Tscherepowez");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("149", "de", "Tscherkessk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("150", "de", "Tschistopol");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("151", "de", "Schadrinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("152", "de", "Schatura");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("153", "de", "Schuja");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("154", "de", "Elista");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("155", "de", "Engels");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("156", "de", "Welikije Luki ");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("157", "de", "Magnitogorsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("158", "de", "Kolomna");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("159", "de", "Jalta");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("160", "de", "Bezirk Nowoanninsky");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("161", "de", "city.name");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("162", "de", "Bezirk Wsewolschski, Kreuzung pr. Engelsa und Autobahnring");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("163", "de", "Dorf Makarje");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("164", "de", "Dorf Schatilowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("165", "de", "Dorf Jelezkaja Losowka");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("166", "de", "Dorf Schestakowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("167", "de", "Dorf Nowaja Usman");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("168", "de", "Stary Oskol");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("169", "de", "Siedlung Solnetschny");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("170", "de", "Sysran");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("171", "de", "Dorf Tschitschkowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("172", "de", "Reutow");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("173", "de", "Odinzowo");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("174", "de", "Welsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("175", "de", "Nowodwinsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("176", "de", "Uchta");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("177", "de", "Sosnogorsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("178", "de", "Bratsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("179", "de", "Nojabrsk");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("180", "de", "Schukowski");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("181", "de", "Sarapul");
			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ("182", "de", "Korjamscha");
		';
	}
}
