<?php

class m170213_111821_add_exdborganizercompanyhasassociation extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{exdborganizercompanyhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `organizerCompanyId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('56', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5511', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4019', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('84', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('83', '6601');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('118', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('119', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('131', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('140', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('154', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('156', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('161', '5734');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('163', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('468', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('17295', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('180', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('183', '5734');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('189', '5734');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('188', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('195', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('177', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('198', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('747', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('108', '6647');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('219', '6647');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('166', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('236', '11681');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('143', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('247', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('22', '6648');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('57', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('260', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('12234', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('249', '6648');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4209', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5512', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('32', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('153', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('324', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9684', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('333', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14', '6648');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('337', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('106', '6647');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('248', '6648');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('341', '6648');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('254', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('357', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('231', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('348', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('197', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('406', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('411', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('88', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('425', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('332', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('427', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('141', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('433', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('446', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('455', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('419', '6651');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('548', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1115', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('338', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('246', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('330', '11681');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('556', '6651');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('64', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5580', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('157', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('544', '6600');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('892', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('690', '6586');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('73', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('709', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('721', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('802', '6600');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('404', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3274', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5066', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('320', '6647');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('960', '6600');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('378', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('213', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('509', '6581');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1504', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14524', '6586');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2958', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('49', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1010', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('504', '6600');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1065', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('799', '6651');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1101', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('575', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1266', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1432', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('80', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('35', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('191', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('267', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1477', '5734');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1539', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14823', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('531', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1605', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('12062', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1804', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6193', '6586');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1576', '6644');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('17', '5734');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1943', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('280', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5670', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('749', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4446', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('17054', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('8383', '6586');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('12424', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2206', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2954', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2475', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2529', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('15859', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('16379', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2586', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2038', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('552', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9592', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2766', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1679', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1309', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4426', '6635');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1342', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4814', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2523', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('15184', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10665', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3048', '6587');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5153', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9144', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2285', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('89', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6839', '6580');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3654', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6464', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14016', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10666', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9613', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3211', '6648');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('692', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5033', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3459', '6587');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('27', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1655', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3710', '6587');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11842', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('44', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4442', '6644');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3755', '6587');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('901', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1001', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4006', '6635');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2739', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3944', '6644');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3454', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4107', '6635');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5048', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2665', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4168', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4076', '6587');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('8414', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('7210', '5337');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3082', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5528', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6897', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('113', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3647', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9433', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2970', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4035', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5297', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10268', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4177', '18156');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3156', '6603');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11089', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1801', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9629', '5337');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2480', '7519');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('7607', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4615', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6280', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2522', '6606');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('93', '6647');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('16209', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5578', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('7594', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11778', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3409', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('16600', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10095', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10705', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6415', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4240', '6596');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13270', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10874', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11132', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1927', '6644');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10129', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2933', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11001', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10140', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('12208', '6601');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13611', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11542', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4943', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13122', '6616');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14281', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5061', '6654');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11865', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13067', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3064', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13627', '11881');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14673', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('945', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13570', '12358');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2280', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5455', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('15932', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('17730', '6576');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('16380', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11402', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5351', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('8463', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('56', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5511', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('119', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('140', '5734');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('154', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('156', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('161', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('163', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('17295', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('183', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('195', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('108', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('219', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('166', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('143', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('247', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('22', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('249', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('32', '11681');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('333', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('106', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('248', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('341', '6667');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('254', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('348', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('197', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('406', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('425', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('332', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('427', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('433', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('446', '6640');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('455', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('419', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('548', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('338', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('246', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('556', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('64', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('157', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('544', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('892', '11681');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('690', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('73', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('721', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('802', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('404', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('320', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('960', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('378', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('213', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('509', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14524', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2958', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1010', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('504', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('799', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1101', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1266', '6587');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('267', '6593');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1539', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1605', '6635');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1804', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1576', '11881');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('17', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2954', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('15859', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('552', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2766', '6599');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4426', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4814', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('15184', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3048', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9144', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('89', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6839', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3654', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6464', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('44', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3755', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1001', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4006', '7712');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3944', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3454', '5337');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('7210', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6897', '6631');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('113', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2970', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4177', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('9629', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2480', '11881');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('6280', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2522', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('93', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('7594', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4240', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11132', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1927', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('10129', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2933', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('11001', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('5061', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13067', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14673', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('945', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('13570', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('8463', '6686');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('56', '6635');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('119', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('140', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('154', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('161', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('163', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('166', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('143', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('22', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('249', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('32', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('341', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('406', '10987');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('425', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('427', '9182');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('548', '6665');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('157', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('892', '6672');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('73', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('378', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2958', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1266', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1605', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1576', '3116');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2766', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4006', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('3454', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('113', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('4177', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('2480', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14673', '11881');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('56', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('22', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('249', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('32', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('341', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('406', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('427', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('892', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('1576', '6689');
            INSERT INTO {{exdborganizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES ('14673', '12358');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}