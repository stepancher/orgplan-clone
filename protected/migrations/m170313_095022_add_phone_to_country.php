<?php

class m170313_095022_add_phone_to_country extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{country}} 
            ADD COLUMN `telephoneCode` VARCHAR(255) NULL DEFAULT NULL AFTER `code`;
            
            UPDATE {{country}} SET `telephoneCode`='+7' WHERE `id`='1';
            UPDATE {{country}} SET `telephoneCode`='+49' WHERE `id`='2';
            UPDATE {{country}} SET `telephoneCode`='+994' WHERE `id`='3';
            UPDATE {{country}} SET `telephoneCode`='+374' WHERE `id`='4';
            UPDATE {{country}} SET `telephoneCode`='+375' WHERE `id`='5';
            UPDATE {{country}} SET `telephoneCode`='+77' WHERE `id`='6';
            UPDATE {{country}} SET `telephoneCode`='+996' WHERE `id`='7';
            UPDATE {{country}} SET `telephoneCode`='+373' WHERE `id`='8';
            UPDATE {{country}} SET `telephoneCode`='+998' WHERE `id`='9';
            UPDATE {{country}} SET `telephoneCode`='+993' WHERE `id`='10';
            UPDATE {{country}} SET `telephoneCode`='+995' WHERE `id`='11';
            UPDATE {{country}} SET `telephoneCode`='+976' WHERE `id`='12';
            UPDATE {{country}} SET `telephoneCode`='+380' WHERE `id`='13';
            UPDATE {{country}} SET `telephoneCode`='+381' WHERE `id`='14';
            UPDATE {{country}} SET `telephoneCode`='+84' WHERE `id`='15';
            UPDATE {{country}} SET `telephoneCode`='+61' WHERE `id`='16';
            UPDATE {{country}} SET `telephoneCode`='+43' WHERE `id`='17';
            UPDATE {{country}} SET `telephoneCode`='+213' WHERE `id`='18';
            UPDATE {{country}} SET `telephoneCode`='+54' WHERE `id`='19';
            UPDATE {{country}} SET `telephoneCode`='+880' WHERE `id`='20';
            UPDATE {{country}} SET `telephoneCode`='+973' WHERE `id`='21';
            UPDATE {{country}} SET `telephoneCode`='+32' WHERE `id`='22';
            UPDATE {{country}} SET `telephoneCode`='+359' WHERE `id`='23';
            UPDATE {{country}} SET `telephoneCode`='+591' WHERE `id`='24';
            UPDATE {{country}} SET `telephoneCode`='+387' WHERE `id`='25';
            UPDATE {{country}} SET `telephoneCode`='+267' WHERE `id`='26';
            UPDATE {{country}} SET `telephoneCode`='+55' WHERE `id`='27';
            UPDATE {{country}} SET `telephoneCode`='+44' WHERE `id`='28';
            UPDATE {{country}} SET `telephoneCode`='+36' WHERE `id`='29';
            UPDATE {{country}} SET `telephoneCode`='+233' WHERE `id`='30';
            UPDATE {{country}} SET `telephoneCode`='+502' WHERE `id`='31';
            UPDATE {{country}} SET `telephoneCode`='+852' WHERE `id`='32';
            UPDATE {{country}} SET `telephoneCode`='+30' WHERE `id`='33';
            UPDATE {{country}} SET `telephoneCode`='+45' WHERE `id`='34';
            UPDATE {{country}} SET `telephoneCode`='+243' WHERE `id`='35';
            UPDATE {{country}} SET `telephoneCode`='+20' WHERE `id`='36';
            UPDATE {{country}} SET `telephoneCode`='+260' WHERE `id`='37';
            UPDATE {{country}} SET `telephoneCode`='+263' WHERE `id`='38';
            UPDATE {{country}} SET `telephoneCode`='+972' WHERE `id`='39';
            UPDATE {{country}} SET `telephoneCode`='+91' WHERE `id`='40';
            UPDATE {{country}} SET `telephoneCode`='+62' WHERE `id`='41';
            UPDATE {{country}} SET `telephoneCode`='+962' WHERE `id`='42';
            UPDATE {{country}} SET `telephoneCode`='+964' WHERE `id`='43';
            UPDATE {{country}} SET `telephoneCode`='+98' WHERE `id`='44';
            UPDATE {{country}} SET `telephoneCode`='+353' WHERE `id`='45';
            UPDATE {{country}} SET `telephoneCode`='+354' WHERE `id`='46';
            UPDATE {{country}} SET `telephoneCode`='+34' WHERE `id`='47';
            UPDATE {{country}} SET `telephoneCode`='+39' WHERE `id`='48';
            UPDATE {{country}} SET `telephoneCode`='+855' WHERE `id`='49';
            UPDATE {{country}} SET `telephoneCode`='+237' WHERE `id`='50';
            UPDATE {{country}} SET `telephoneCode`='+1' WHERE `id`='51';
            UPDATE {{country}} SET `telephoneCode`='+974' WHERE `id`='52';
            UPDATE {{country}} SET `telephoneCode`='+254' WHERE `id`='53';
            UPDATE {{country}} SET `telephoneCode`='+86' WHERE `id`='54';
            UPDATE {{country}} SET `telephoneCode`='+886' WHERE `id`='55';
            UPDATE {{country}} SET `telephoneCode`='+57' WHERE `id`='56';
            UPDATE {{country}} SET `telephoneCode`='+53' WHERE `id`='57';
            UPDATE {{country}} SET `telephoneCode`='+965' WHERE `id`='58';
            UPDATE {{country}} SET `telephoneCode`='+371' WHERE `id`='59';
            UPDATE {{country}} SET `telephoneCode`='+961' WHERE `id`='60';
            UPDATE {{country}} SET `telephoneCode`='+370' WHERE `id`='61';
            UPDATE {{country}} SET `telephoneCode`='+423' WHERE `id`='62';
            UPDATE {{country}} SET `telephoneCode`='+352' WHERE `id`='63';
            UPDATE {{country}} SET `telephoneCode`='+230' WHERE `id`='64';
            UPDATE {{country}} SET `telephoneCode`='+853' WHERE `id`='65';
            UPDATE {{country}} SET `telephoneCode`='+60' WHERE `id`='66';
            UPDATE {{country}} SET `telephoneCode`='+356' WHERE `id`='67';
            UPDATE {{country}} SET `telephoneCode`='+212' WHERE `id`='68';
            UPDATE {{country}} SET `telephoneCode`='+52' WHERE `id`='69';
            UPDATE {{country}} SET `telephoneCode`='+258' WHERE `id`='70';
            UPDATE {{country}} SET `telephoneCode`='+377' WHERE `id`='71';
            UPDATE {{country}} SET `telephoneCode`='+95' WHERE `id`='72';
            UPDATE {{country}} SET `telephoneCode`='+234' WHERE `id`='73';
            UPDATE {{country}} SET `telephoneCode`='+31' WHERE `id`='74';
            UPDATE {{country}} SET `telephoneCode`='+64' WHERE `id`='75';
            UPDATE {{country}} SET `telephoneCode`='+47' WHERE `id`='76';
            UPDATE {{country}} SET `telephoneCode`='+971' WHERE `id`='77';
            UPDATE {{country}} SET `telephoneCode`='+968' WHERE `id`='78';
            UPDATE {{country}} SET `telephoneCode`='+92' WHERE `id`='79';
            UPDATE {{country}} SET `telephoneCode`='+507' WHERE `id`='80';
            UPDATE {{country}} SET `telephoneCode`='+595' WHERE `id`='81';
            UPDATE {{country}} SET `telephoneCode`='+51' WHERE `id`='82';
            UPDATE {{country}} SET `telephoneCode`='+48' WHERE `id`='83';
            UPDATE {{country}} SET `telephoneCode`='+351' WHERE `id`='84';
            UPDATE {{country}} SET `telephoneCode`='+242' WHERE `id`='85';
            UPDATE {{country}} SET `telephoneCode`='+82' WHERE `id`='86';
            UPDATE {{country}} SET `telephoneCode`='+383' WHERE `id`='87';
            UPDATE {{country}} SET `telephoneCode`='+389' WHERE `id`='88';
            UPDATE {{country}} SET `telephoneCode`='+250' WHERE `id`='89';
            UPDATE {{country}} SET `telephoneCode`='+40' WHERE `id`='90';
            UPDATE {{country}} SET `telephoneCode`='+966' WHERE `id`='91';
            UPDATE {{country}} SET `telephoneCode`='+221' WHERE `id`='92';
            UPDATE {{country}} SET `telephoneCode`='+65' WHERE `id`='93';
            UPDATE {{country}} SET `telephoneCode`='+421' WHERE `id`='94';
            UPDATE {{country}} SET `telephoneCode`='+386' WHERE `id`='95';
            UPDATE {{country}} SET `telephoneCode`='+1' WHERE `id`='96';
            UPDATE {{country}} SET `telephoneCode`='+249' WHERE `id`='97';
            UPDATE {{country}} SET `telephoneCode`='+597' WHERE `id`='98';
            UPDATE {{country}} SET `telephoneCode`='+66' WHERE `id`='99';
            UPDATE {{country}} SET `telephoneCode`='+255' WHERE `id`='100';
            UPDATE {{country}} SET `telephoneCode`='+216' WHERE `id`='101';
            UPDATE {{country}} SET `telephoneCode`='+90' WHERE `id`='102';
            UPDATE {{country}} SET `telephoneCode`='+256' WHERE `id`='103';
            UPDATE {{country}} SET `telephoneCode`='+63' WHERE `id`='104';
            UPDATE {{country}} SET `telephoneCode`='+358' WHERE `id`='105';
            UPDATE {{country}} SET `telephoneCode`='+33' WHERE `id`='106';
            UPDATE {{country}} SET `telephoneCode`='+385' WHERE `id`='107';
            UPDATE {{country}} SET `telephoneCode`='+420' WHERE `id`='108';
            UPDATE {{country}} SET `telephoneCode`='+56' WHERE `id`='109';
            UPDATE {{country}} SET `telephoneCode`='+41' WHERE `id`='110';
            UPDATE {{country}} SET `telephoneCode`='+46' WHERE `id`='111';
            UPDATE {{country}} SET `telephoneCode`='+94' WHERE `id`='112';
            UPDATE {{country}} SET `telephoneCode`='+593' WHERE `id`='113';
            UPDATE {{country}} SET `telephoneCode`='+372' WHERE `id`='114';
            UPDATE {{country}} SET `telephoneCode`='+251' WHERE `id`='115';
            UPDATE {{country}} SET `telephoneCode`='+27' WHERE `id`='116';
            UPDATE {{country}} SET `telephoneCode`='+81' WHERE `id`='117';
            UPDATE {{country}} SET `telephoneCode`='+244' WHERE `id`='118';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}