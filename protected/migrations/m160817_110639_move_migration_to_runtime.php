<?php

class m160817_110639_move_migration_to_runtime extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->dbRuntime->beginTransaction();
		try
		{
			Yii::app()->dbRuntime->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return file_get_contents(Yii::getPathOfAlias('app.data').'/17_08_2016_dump_migration_table.sql');
	}
}