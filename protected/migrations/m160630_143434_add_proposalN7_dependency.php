<?php

class m160630_143434_add_proposalN7_dependency extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return '
			DELETE FROM {{calendarfairtasks}} WHERE `id` IN (141, 142);

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":23}\', 	"calendar/gantt/reject/id/139/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":23}\', 	"calendar/gantt/reject/id/140/userId/{:userId}");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":23}\', 	"calendar/gantt/reject/id/143/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":23}\', 	"calendar/gantt/reject/id/144/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":23}\', 	"calendar/gantt/add/id/139");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", \'{"ecId":23}\', 	"calendar/gantt/add/id/143");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":23, "save":1}\', 	"calendar/gantt/add/id/139");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":23, "send":1}\', 	"calendar/gantt/add/id/139");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", \'{"fairId":184, "ecId":23}\', 	"calendar/gantt/add/id/139");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":23, "send":1}\', 	"calendar/gantt/complete/id/139");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":23, "send":1}\', 	"calendar/gantt/add/id/140");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", \'{"fairId":184, "ecId":23}\', 	"calendar/gantt/complete/id/139");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", \'{"fairId":184, "ecId":23}\', 	"calendar/gantt/add/id/140");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":23, "send":1}\', 	"calendar/gantt/complete/id/143");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":23, "send":1}\', 	"calendar/gantt/add/id/144");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", \'{"fairId":184, "ecId":23}\', 	"calendar/gantt/complete/id/143");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", \'{"fairId":184, "ecId":23}\', 	"calendar/gantt/add/id/144");
		';
	}
}