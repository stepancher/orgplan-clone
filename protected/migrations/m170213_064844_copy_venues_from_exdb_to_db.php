<?php

class m170213_064844_copy_venues_from_exdb_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `copy_venues_from_exdb_to_db`;
            
            CREATE PROCEDURE `copy_venues_from_exdb_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_city_id INT DEFAULT 0;
                DECLARE exdb_coordinates VARCHAR(55) DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE temp_name TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT DISTINCT v.id, v.cityId, v.coordinates, trv.langId, trv.name FROM {{exdbvenue}} v
                                            LEFT JOIN {{exdbtrvenue}} trv ON trv.trParentId = v.id
                                            LEFT JOIN {{exhibitioncomplex}} ec ON ec.cityId = v.cityId AND ec.coordinates = v.coordinates
                                            LEFT JOIN {{trexhibitioncomplex}} trec ON trec.trParentId = ec.id AND trec.langId = trv.langId AND trec.name = trv.name
                                        WHERE ec.id IS NULL
                                        GROUP BY trv.name, trv.langId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                    OPEN copy;
                    
                    read_loop: LOOP
                        
                        FETCH copy INTO exdb_id, exdb_city_id, exdb_coordinates, exdb_lang_id, exdb_name;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                                                    
                        IF temp_name != exdb_name THEN
                            
                            START TRANSACTION;
                                INSERT INTO {{exhibitioncomplex}} (`cityId`,`coordinates`) VALUES (exdb_city_id, exdb_coordinates);
                            COMMIT;
                            
                            SET @last_insert_id := LAST_INSERT_ID();
                        
                        END IF;
                        
                        SET temp_name := exdb_name;
                        
                        START TRANSACTION;
                            INSERT INTO {{trexhibitioncomplex}} (`name`,`trParentId`,`langId`) VALUES (exdb_name, @last_insert_id, exdb_lang_id);
                        COMMIT;
                        
                    END LOOP;
                    CLOSE copy;
            END;
                    
            CALL `copy_venues_from_exdb_to_db`();
            DROP PROCEDURE IF EXISTS `copy_venues_from_exdb_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}