<?php

class m170330_062503_add_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('56');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1091', 'ru', 'Шлезвиг-Гольштейн', 'schleswig-holstein');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1091', 'en', 'Schleswig-Holstein', 'schleswig-holstein');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1091', 'de', 'Schleswig-Holstein', 'schleswig-holstein');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1091', 'oststeinbek');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1540', 'ru', 'Остштайнбек');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1540', 'en', 'Oststeinbek');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1540', 'de', 'Oststeinbek');
            INSERT INTO {{region}} (`districtId`) VALUES ('13');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1092', 'ru', 'Эслинген', 'esslingen');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1092', 'en', 'Esslingen', 'esslingen');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1092', 'de', 'Landkreis Esslingen', 'esslingen');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1092', 'frickenhausen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1541', 'ru', 'Фриккенхаузен');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1541', 'en', 'Frickenhausen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1541', 'de', 'Frickenhausen');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}