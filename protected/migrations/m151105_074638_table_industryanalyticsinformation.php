<?php

class m151105_074638_table_industryanalyticsinformation extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{industryanalyticsinformation}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return '
			DROP TABLE IF EXISTS {{industryanalyticsinformation}};

			CREATE TABLE IF NOT EXISTS {{industryanalyticsinformation}} (
			  `id` int(11) NOT NULL,
			  `header` varchar(200) DEFAULT NULL,
			  `headerColor` varchar(45) DEFAULT NULL,
			  `priority` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			INSERT INTO `tbl_industryanalyticsinformation` (`id`, `header`, `headerColor`, `priority`) VALUES
				(11, "Сельское хозяйство", "#a0d468", 1);

			';
	}
}