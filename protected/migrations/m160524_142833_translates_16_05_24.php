<?php

class m160524_142833_translates_16_05_24 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{trexhibitioncomplex}} (`id`, `trParentId`, `langId`, `name`, `street`, `description`, `services`) VALUES (NULL, '2', 'ru', 'Крокус Экспо', 'Международная ул., д. 16', 'International exhibition center “Crocus Expo”', '');
			UPDATE {{trexhibitioncomplex}} SET `langId`='en' WHERE `id`='421';
			INSERT INTO {{trcity}} (`id`, `trParentId`, `langId`, `name`) VALUES (NULL, '1', 'en', 'Moscow');
			INSERT INTO {{trcountry}} (`id`, `trParentId`, `langId`, `name`) VALUES (NULL, '1', 'en', 'Russia');
			UPDATE {{trattributeclassproperty}} SET `langId`='en' WHERE `id`='803';
		";
	}
}