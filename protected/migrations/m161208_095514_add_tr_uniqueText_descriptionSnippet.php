<?php

class m161208_095514_add_tr_uniqueText_descriptionSnippet extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{trexhibitioncomplex}} 
            ADD COLUMN `descriptionSnippet` TEXT NULL DEFAULT NULL AFTER `services`;
            
            ALTER TABLE {{trexhibitioncomplex}} 
            ADD COLUMN `uniqueText` TEXT NULL DEFAULT NULL AFTER `descriptionSnippet`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}