<?php

class m160930_093414_gradoteka_final2_checks extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{chartcolumn}} SET `statId`='57a447ac75d8974e5b8b4ea4' WHERE `id`='156';
            UPDATE {{chartcolumn}} SET `statId`='57a447ac75d8974e5b8b4ea4' WHERE `id`='168';
            
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500211', 'Хозяйства всех категорий', 'Производство молока в хозяйствах всех категорий', 'derivative', 'тонна', '0');
            INSERT INTO {{gstattypehasformula}} (`id`, `gId`, `formulaType`, `formula`) VALUES (NULL, '500211', 'custom', '[1]*1000');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`, `key`, `statId`) VALUES ('104', '1', '56d02ef7f7a9ad01088b5796');
            
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83541', '283', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83542', '284', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83543', '285', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83544', '286', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83545', '287', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83546', '288', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83547', '289', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83548', '290', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83549', '291', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83550', '292', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83551', '293', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83552', '294', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83553', '295', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83554', '296', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83555', '297', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83556', '298', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83557', '299', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83558', '300', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83559', '301', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83560', '302', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83561', '303', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83562', '304', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83563', '305', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83564', '306', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83565', '307', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83566', '308', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83567', '309', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83568', '310', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83569', '311', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83570', '312', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83571', '313', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83572', '314', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83573', '316', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83574', '317', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83575', '318', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83576', '319', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83577', '320', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83578', '321', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83579', '322', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83580', '323', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83581', '324', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83582', '325', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83583', '326', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83584', '327', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83585', '328', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83586', '329', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83587', '330', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83588', '331', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83589', '332', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83590', '333', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83591', '334', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83592', '335', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83593', '336', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83594', '337', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83595', '338', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83596', '339', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83597', '340', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83598', '341', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83599', '342', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83600', '343', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83601', '344', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83602', '345', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83603', '346', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83604', '347', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83605', '348', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83606', '349', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83607', '350', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83608', '351', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83609', '352', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83610', '353', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83611', '354', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83612', '355', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83613', '356', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83614', '357', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83615', '358', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83616', '359', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83617', '360', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83618', '361', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83619', '362', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83620', '363', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83621', '364', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83622', '365', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83623', '366', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83624', '367', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83625', '368', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83626', '369', '500211', 'region');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83627', '370', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83628', '371', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83629', '372', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83630', '373', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83631', '374', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83632', '375', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83633', '376', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83634', '377', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83635', '378', '500211', 'state');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('83636', '379', '500211', 'country');
            
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750767', '83541', '2016-09-30 12:40:03', '1364000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750768', '83541', '2016-09-30 12:40:03', '1414900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750769', '83541', '2016-09-30 12:40:03', '1414900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750770', '83542', '2016-09-30 12:40:03', '1319400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750771', '83542', '2016-09-30 12:40:03', '1302100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750772', '83542', '2016-09-30 12:40:03', '1327600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750773', '83543', '2016-09-30 12:40:03', '708100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750774', '83543', '2016-09-30 12:40:03', '724500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750775', '83543', '2016-09-30 12:40:03', '739800', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750776', '83544', '2016-09-30 12:40:03', '119500', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750777', '83544', '2016-09-30 12:40:03', '118600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750778', '83544', '2016-09-30 12:40:03', '123500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750779', '83545', '2016-09-30 12:40:03', '681300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750780', '83545', '2016-09-30 12:40:03', '686800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750781', '83545', '2016-09-30 12:40:03', '687400', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750782', '83546', '2016-09-30 12:40:03', '46500', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750783', '83546', '2016-09-30 12:40:03', '43400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750784', '83546', '2016-09-30 12:40:03', '39100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750785', '83547', '2016-09-30 12:40:03', '165100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750786', '83547', '2016-09-30 12:40:03', '143600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750787', '83547', '2016-09-30 12:40:03', '148600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750788', '83548', '2016-09-30 12:40:03', '113000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750789', '83548', '2016-09-30 12:40:03', '114700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750790', '83548', '2016-09-30 12:40:03', '118100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750791', '83549', '2016-09-30 12:40:03', '116100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750792', '83549', '2016-09-30 12:40:03', '117800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750793', '83549', '2016-09-30 12:40:03', '121300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750794', '83550', '2016-09-30 12:40:03', '3100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750795', '83550', '2016-09-30 12:40:03', '3200', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750796', '83550', '2016-09-30 12:40:03', '3200', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750797', '83551', '2016-09-30 12:40:03', '171400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750798', '83551', '2016-09-30 12:40:03', '172000', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750799', '83551', '2016-09-30 12:40:03', '172900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750800', '83552', '2016-09-30 12:40:03', '542700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750801', '83552', '2016-09-30 12:40:03', '544200', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750802', '83552', '2016-09-30 12:40:03', '531500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750803', '83553', '2016-09-30 12:40:03', '332000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750804', '83553', '2016-09-30 12:40:03', '312700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750805', '83553', '2016-09-30 12:40:03', '291100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750806', '83554', '2016-09-30 12:40:03', '346600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750807', '83554', '2016-09-30 12:40:03', '345100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750808', '83554', '2016-09-30 12:40:03', '354700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750809', '83555', '2016-09-30 12:40:03', '529600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750810', '83555', '2016-09-30 12:40:03', '523000', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750811', '83555', '2016-09-30 12:40:03', '511300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750812', '83556', '2016-09-30 12:40:03', '430200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750813', '83556', '2016-09-30 12:40:03', '444600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750814', '83556', '2016-09-30 12:40:03', '469600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750815', '83557', '2016-09-30 12:40:04', '755900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750816', '83557', '2016-09-30 12:40:04', '788500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750817', '83557', '2016-09-30 12:40:04', '807700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750818', '83558', '2016-09-30 12:40:04', '611900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750819', '83558', '2016-09-30 12:40:04', '619800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750820', '83558', '2016-09-30 12:40:04', '619800', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750821', '83559', '2016-09-30 12:40:04', '150600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750822', '83559', '2016-09-30 12:40:04', '149200', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750823', '83559', '2016-09-30 12:40:04', '154500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750824', '83560', '2016-09-30 12:40:04', '458100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750825', '83560', '2016-09-30 12:40:04', '467400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750826', '83560', '2016-09-30 12:40:04', '460100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750827', '83561', '2016-09-30 12:40:04', '67900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750828', '83561', '2016-09-30 12:40:04', '66700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750829', '83561', '2016-09-30 12:40:04', '74400', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750830', '83562', '2016-09-30 12:40:04', '148800', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750831', '83562', '2016-09-30 12:40:04', '156200', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750832', '83562', '2016-09-30 12:40:04', '170300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750833', '83563', '2016-09-30 12:40:04', '223200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750834', '83563', '2016-09-30 12:40:04', '212600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750835', '83563', '2016-09-30 12:40:04', '213500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750836', '83564', '2016-09-30 12:40:04', '219700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750837', '83564', '2016-09-30 12:40:04', '228300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750838', '83564', '2016-09-30 12:40:04', '253800', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750839', '83565', '2016-09-30 12:40:04', '17200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750840', '83565', '2016-09-30 12:40:04', '17500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750841', '83565', '2016-09-30 12:40:04', '18000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750842', '83566', '2016-09-30 12:40:04', '368900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750843', '83566', '2016-09-30 12:40:04', '375900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750844', '83566', '2016-09-30 12:40:04', '380100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750845', '83567', '2016-09-30 12:40:04', '523800', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750846', '83567', '2016-09-30 12:40:04', '541800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750847', '83567', '2016-09-30 12:40:04', '579500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750848', '83568', '2016-09-30 12:40:04', '111300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750849', '83568', '2016-09-30 12:40:04', '106900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750850', '83568', '2016-09-30 12:40:04', '108100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750851', '83569', '2016-09-30 12:40:04', '286700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750852', '83569', '2016-09-30 12:40:04', '243300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750853', '83570', '2016-09-30 12:40:04', '421400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750854', '83570', '2016-09-30 12:40:04', '434900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750855', '83570', '2016-09-30 12:40:04', '440600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750856', '83571', '2016-09-30 12:40:04', '343500', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750857', '83571', '2016-09-30 12:40:04', '300300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750858', '83571', '2016-09-30 12:40:04', '234200', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750859', '83572', '2016-09-30 12:40:04', '359400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750860', '83572', '2016-09-30 12:40:04', '324900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750861', '83572', '2016-09-30 12:40:04', '310000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750862', '83573', '2016-09-30 12:40:04', '556700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750863', '83573', '2016-09-30 12:40:04', '567800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750864', '83573', '2016-09-30 12:40:04', '592500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750865', '83574', '2016-09-30 12:40:04', '253300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750866', '83574', '2016-09-30 12:40:04', '248100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750867', '83574', '2016-09-30 12:40:04', '254600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750868', '83575', '2016-09-30 12:40:04', '5900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750869', '83575', '2016-09-30 12:40:04', '5900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750870', '83575', '2016-09-30 12:40:04', '6000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750871', '83576', '2016-09-30 12:40:04', '29600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750872', '83576', '2016-09-30 12:40:04', '30100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750873', '83576', '2016-09-30 12:40:04', '30500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750874', '83577', '2016-09-30 12:40:04', '643900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750875', '83577', '2016-09-30 12:40:04', '637400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750876', '83577', '2016-09-30 12:40:04', '631100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750877', '83578', '2016-09-30 12:40:04', '27200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750878', '83578', '2016-09-30 12:40:04', '22000', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750879', '83578', '2016-09-30 12:40:04', '18800', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750880', '83579', '2016-09-30 12:40:04', '87900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750881', '83579', '2016-09-30 12:40:04', '82200', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750882', '83579', '2016-09-30 12:40:04', '79400', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750883', '83580', '2016-09-30 12:40:04', '654200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750884', '83580', '2016-09-30 12:40:04', '660500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750885', '83580', '2016-09-30 12:40:04', '661500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750886', '83581', '2016-09-30 12:40:04', '696900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750887', '83581', '2016-09-30 12:40:05', '709400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750888', '83581', '2016-09-30 12:40:05', '702700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750889', '83582', '2016-09-30 12:40:05', '815300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750890', '83582', '2016-09-30 12:40:05', '811000', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750891', '83582', '2016-09-30 12:40:05', '797500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750892', '83583', '2016-09-30 12:40:05', '214700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750893', '83583', '2016-09-30 12:40:05', '191600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750894', '83583', '2016-09-30 12:40:05', '183900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750895', '83584', '2016-09-30 12:40:05', '350200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750896', '83584', '2016-09-30 12:40:05', '326700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750897', '83584', '2016-09-30 12:40:05', '331800', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750898', '83585', '2016-09-30 12:40:05', '460900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750899', '83585', '2016-09-30 12:40:05', '472300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750900', '83585', '2016-09-30 12:40:05', '482300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750901', '83586', '2016-09-30 12:40:05', '197400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750902', '83586', '2016-09-30 12:40:05', '195400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750903', '83586', '2016-09-30 12:40:05', '199000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750904', '83587', '2016-09-30 12:40:05', '1079100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750905', '83587', '2016-09-30 12:40:05', '1079800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750906', '83587', '2016-09-30 12:40:05', '1080600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750907', '83588', '2016-09-30 12:40:05', '354900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750908', '83588', '2016-09-30 12:40:05', '365100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750909', '83588', '2016-09-30 12:40:05', '374900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750910', '83589', '2016-09-30 12:40:05', '826400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750911', '83589', '2016-09-30 12:40:05', '777400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750912', '83589', '2016-09-30 12:40:05', '728300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750913', '83590', '2016-09-30 12:40:05', '26200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750914', '83590', '2016-09-30 12:40:05', '27600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750915', '83590', '2016-09-30 12:40:05', '27900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750916', '83591', '2016-09-30 12:40:05', '613600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750917', '83591', '2016-09-30 12:40:05', '652500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750918', '83591', '2016-09-30 12:40:05', '654000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750919', '83592', '2016-09-30 12:40:05', '298300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750920', '83592', '2016-09-30 12:40:05', '235600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750921', '83592', '2016-09-30 12:40:05', '218100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750922', '83593', '2016-09-30 12:40:05', '4900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750923', '83593', '2016-09-30 12:40:05', '4800', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750924', '83594', '2016-09-30 12:40:05', '221600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750925', '83594', '2016-09-30 12:40:05', '223800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750926', '83594', '2016-09-30 12:40:05', '220300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750927', '83595', '2016-09-30 12:40:05', '144300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750928', '83595', '2016-09-30 12:40:05', '138800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750929', '83595', '2016-09-30 12:40:05', '140300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750930', '83596', '2016-09-30 12:40:05', '173100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750931', '83596', '2016-09-30 12:40:05', '176700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750932', '83596', '2016-09-30 12:40:05', '187300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750933', '83597', '2016-09-30 12:40:05', '544100', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750934', '83597', '2016-09-30 12:40:05', '531900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750935', '83597', '2016-09-30 12:40:05', '522500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750936', '83598', '2016-09-30 12:40:05', '572000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750937', '83598', '2016-09-30 12:40:05', '561300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750938', '83598', '2016-09-30 12:40:05', '552000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750939', '83599', '2016-09-30 12:40:05', '25900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750940', '83599', '2016-09-30 12:40:05', '27400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750941', '83599', '2016-09-30 12:40:05', '27500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750942', '83600', '2016-09-30 12:40:05', '2000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750943', '83600', '2016-09-30 12:40:05', '2100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750944', '83600', '2016-09-30 12:40:05', '2000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750945', '83601', '2016-09-30 12:40:05', '267400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750946', '83601', '2016-09-30 12:40:05', '232500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750947', '83601', '2016-09-30 12:40:05', '211100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750948', '83602', '2016-09-30 12:40:05', '494700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750949', '83602', '2016-09-30 12:40:05', '484100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750950', '83602', '2016-09-30 12:40:05', '466500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750951', '83603', '2016-09-30 12:40:05', '335900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750952', '83603', '2016-09-30 12:40:05', '344600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750953', '83603', '2016-09-30 12:40:05', '340900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750954', '83604', '2016-09-30 12:40:06', '0', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750955', '83604', '2016-09-30 12:40:06', '0', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750956', '83604', '2016-09-30 12:40:06', '0', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750957', '83605', '2016-09-30 12:40:06', '263300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750958', '83605', '2016-09-30 12:40:06', '272400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750959', '83605', '2016-09-30 12:40:06', '280700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750960', '83606', '2016-09-30 12:40:06', '113900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750961', '83606', '2016-09-30 12:40:06', '115300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750962', '83606', '2016-09-30 12:40:06', '117900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750963', '83607', '2016-09-30 12:40:06', '1711000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750964', '83607', '2016-09-30 12:40:06', '1773100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750965', '83607', '2016-09-30 12:40:06', '1812300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750966', '83608', '2016-09-30 12:40:06', '223200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750967', '83608', '2016-09-30 12:40:06', '208700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750968', '83608', '2016-09-30 12:40:06', '205600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750969', '83609', '2016-09-30 12:40:06', '755300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750970', '83609', '2016-09-30 12:40:06', '791900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750971', '83609', '2016-09-30 12:40:06', '820200', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750972', '83610', '2016-09-30 12:40:06', '451400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750973', '83610', '2016-09-30 12:40:06', '461600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750974', '83610', '2016-09-30 12:40:06', '469600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750975', '83611', '2016-09-30 12:40:06', '92700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750976', '83611', '2016-09-30 12:40:06', '91600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750977', '83611', '2016-09-30 12:40:06', '89700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750978', '83612', '2016-09-30 12:40:06', '91300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750979', '83612', '2016-09-30 12:40:06', '88000', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750980', '83612', '2016-09-30 12:40:06', '78900', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750981', '83613', '2016-09-30 12:40:06', '62900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750982', '83613', '2016-09-30 12:40:06', '66100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750983', '83613', '2016-09-30 12:40:06', '68300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750984', '83614', '2016-09-30 12:40:06', '57500', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750985', '83614', '2016-09-30 12:40:06', '56600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750986', '83614', '2016-09-30 12:40:06', '56500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750987', '83615', '2016-09-30 12:40:06', '194300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750988', '83615', '2016-09-30 12:40:06', '195800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750989', '83615', '2016-09-30 12:40:06', '186500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750990', '83616', '2016-09-30 12:40:06', '452000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750991', '83616', '2016-09-30 12:40:06', '408800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750992', '83616', '2016-09-30 12:40:06', '404300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750993', '83617', '2016-09-30 12:40:06', '217000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750994', '83617', '2016-09-30 12:40:06', '219100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750995', '83617', '2016-09-30 12:40:06', '205700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750996', '83618', '2016-09-30 12:40:06', '242000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750997', '83618', '2016-09-30 12:40:06', '237000', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750998', '83618', '2016-09-30 12:40:06', '231300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('750999', '83619', '2016-09-30 12:40:06', '1712200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751000', '83619', '2016-09-30 12:40:06', '1728300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751001', '83619', '2016-09-30 12:40:06', '1753700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751002', '83620', '2016-09-30 12:40:06', '62600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751003', '83620', '2016-09-30 12:40:06', '61600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751004', '83620', '2016-09-30 12:40:06', '62400', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751005', '83621', '2016-09-30 12:40:06', '711700', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751006', '83621', '2016-09-30 12:40:06', '724100', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751007', '83621', '2016-09-30 12:40:06', '720600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751008', '83622', '2016-09-30 12:40:06', '190300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751009', '83622', '2016-09-30 12:40:06', '191300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751010', '83622', '2016-09-30 12:40:06', '188400', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751011', '83623', '2016-09-30 12:40:06', '261200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751012', '83623', '2016-09-30 12:40:06', '262800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751013', '83623', '2016-09-30 12:40:06', '266000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751014', '83624', '2016-09-30 12:40:06', '422800', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751015', '83624', '2016-09-30 12:40:06', '420900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751016', '83624', '2016-09-30 12:40:06', '424200', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751017', '83625', '2016-09-30 12:40:06', '170400', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751018', '83625', '2016-09-30 12:40:06', '168400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751019', '83625', '2016-09-30 12:40:06', '164600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751020', '83626', '2016-09-30 12:40:06', '14300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751021', '83626', '2016-09-30 12:40:06', '11500', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751022', '83626', '2016-09-30 12:40:06', '9600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751023', '83627', '2016-09-30 12:40:06', '5494000', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751024', '83627', '2016-09-30 12:40:06', '5393400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751025', '83627', '2016-09-30 12:40:06', '5406300', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751026', '83628', '2016-09-30 12:40:07', '1684600', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751027', '83628', '2016-09-30 12:40:07', '1708700', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751028', '83628', '2016-09-30 12:40:07', '1775600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751029', '83629', '2016-09-30 12:40:07', '3304500', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751030', '83629', '2016-09-30 12:40:07', '3280200', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751031', '83629', '2016-09-30 12:40:07', '3289200', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751032', '83630', '2016-09-30 12:40:07', '2676200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751033', '83630', '2016-09-30 12:40:07', '2725800', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751034', '83630', '2016-09-30 12:40:07', '2754600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751035', '83631', '2016-09-30 12:40:07', '9481300', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751036', '83631', '2016-09-30 12:40:07', '9467300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751037', '83631', '2016-09-30 12:40:07', '9492600', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751038', '83632', '2016-09-30 12:40:07', '2023900', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751039', '83632', '2016-09-30 12:40:07', '1998300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751040', '83632', '2016-09-30 12:40:07', '1906700', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751041', '83633', '2016-09-30 12:40:07', '5299200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751042', '83633', '2016-09-30 12:40:07', '5389300', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751043', '83633', '2016-09-30 12:40:07', '5386500', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751044', '83634', '2016-09-30 12:40:07', '565200', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751045', '83634', '2016-09-30 12:40:07', '536400', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751046', '83634', '2016-09-30 12:40:07', '537200', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751047', '83635', '2016-09-30 12:40:07', '291600', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751048', '83635', '2016-09-30 12:40:07', '248100', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751049', '83636', '2016-09-30 12:40:07', '30528800', '2013');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751050', '83636', '2016-09-30 12:40:07', '30790900', '2014');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('751051', '83636', '2016-09-30 12:40:07', '30796900', '2015');
            
            UPDATE {{chartcolumn}} SET `statId`='500211' WHERE `id`='124';
            UPDATE {{chartcolumn}} SET `statId`='500211' WHERE `id`='456';
            UPDATE {{chartcolumn}} SET `statId`='500211' WHERE `id`='211';
            UPDATE {{chartcolumn}} SET `statId`='500211' WHERE `id`='416';
            
            UPDATE {{gstattypes}} SET `unit`='тысяча гектаров' WHERE `id`='500490';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}