<?php

class m170120_095237_move_organizer_site_adress_to_company extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `copy_organizer_site_address_to_company`;
            
            CREATE PROCEDURE `copy_organizer_site_address_to_company`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_site_address TEXT DEFAULT '';
                DECLARE org_company_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT linkToTheSiteOrganizers, companyId FROM {{organizer}};
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_site_address, org_company_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{organizercompany}} SET `linkToTheSiteOrganizers` = org_site_address WHERE `id` = org_company_id;
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
           END;
           
           CALL copy_organizer_site_address_to_company();
           DROP PROCEDURE IF EXISTS `copy_organizer_site_address_to_company`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}