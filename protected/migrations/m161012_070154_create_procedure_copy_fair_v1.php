<?php

class m161012_070154_create_procedure_copy_fair_v1 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return TRUE;
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_fair_v1`()
            BEGIN
            
                DECLARE n INT DEFAULT 0;
                DECLARE i INT DEFAULT 0;
                DECLARE reverse_year_interval  INT         DEFAULT 1;
                DECLARE uniq_text_save_pattern VARCHAR(50) DEFAULT \"<p>Раздел%\";
                DECLARE source_fair_id         INT         DEFAULT 0;
            
            
                SELECT count(*)
                FROM {{fair}} f
                WHERE
                f.storyId = f.id AND
                YEAR(f.beginDate) = 2015 AND
                (f.statistics IS NULL OR f.statistics = '' OR f.statistics LIKE \"%2014%\") AND
                f.active = 1
                INTO n;
            
            
            
                SET i=0;
                WHILE i<n DO
            
                SELECT f.id
                FROM {{fair}} f
                WHERE
                f.storyId = f.id AND
                YEAR(f.beginDate) = 2015 AND
                (f.statistics IS NULL OR f.statistics = '' OR f.statistics LIKE \"%2014%\") AND
                f.active = 1
                LIMIT i, 1
                INTO source_fair_id;
            
            
            
                INSERT INTO {{fair}} (
                    `rating`,
                    `participationPrice`,
                    `registrationFee`,
                    `organizerId`,
                    `active`,
                    `frequency`,
                    `description`,
                    `exhibitionComplexId`,
                    `beginDate`,
                    `endDate`,
                    `beginMountingDate`,
                    `endMountingDate`,
                    `beginDemountingDate`,
                    `endDemountingDate`,
                    `squareNet`,
                    `members`,
                    `visitors`,
                    `keyword`,
                    `uniqueText`,
                    `currencyId`,
                    `fairTypeId`,
                    `datetimeModified`,
                    `site`,
                    `storyId`
                )
                SELECT
                    `rating`,
                    `participationPrice`,
                    `registrationFee`,
                    `organizerId`,
                    `active`,
                    `frequency`,
                    `description`,
                    `exhibitionComplexId`,
                    `beginDate`            - INTERVAL `reverse_year_interval` YEAR,
                    `endDate`              - INTERVAL `reverse_year_interval` YEAR,
                    `beginMountingDate`    - INTERVAL `reverse_year_interval` YEAR,
                    `endMountingDate`      - INTERVAL `reverse_year_interval` YEAR,
                    `beginDemountingDate`  - INTERVAL `reverse_year_interval` YEAR,
                    `endDemountingDate`    - INTERVAL `reverse_year_interval` YEAR,
                    `squareNet`,
                    `members`,
                    `visitors`,
                    `keyword`,
                    `uniqueText`,
                    `currencyId`,
                    `fairTypeId`,
                    `datetimeModified`,
                    `site`,
                    `storyId`
                FROM {{fair}}
                WHERE id = `source_fair_id`;
            
            
                SET @new_fair_id = LAST_INSERT_ID();
            
                INSERT INTO {{trfair}} (trParentId, langId, name, description, shortUrl)
                SELECT @new_fair_id, langId, name, description, shortUrl FROM {{trfair}} WHERE trParentId = `source_fair_id`;
            
                INSERT INTO {{fairhasindustry}} (fairId, industryId)
                SELECT @new_fair_id, industryId FROM {{fairhasindustry}} WHERE fairId = `source_fair_id`;
            
                UPDATE {{fair}} SET uniqueText = NULL WHERE id = @new_fair_id AND uniqueText NOT LIKE `uniq_text_save_pattern`;
            
            --    SELECT @new_fair_id;
            
            
            
            
                SET i = i + 1;
            
                END WHILE;
            
            END;
            
            CALL copy_fair_v1();
            
            DROP PROCEDURE IF EXISTS `copy_fair_v1`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}