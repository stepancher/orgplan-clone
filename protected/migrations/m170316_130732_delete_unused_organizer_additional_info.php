<?php

class m170316_130732_delete_unused_organizer_additional_info extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`=NULL WHERE `id`='46812';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`=NULL WHERE `id`='46813';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`=NULL WHERE `id`='48852';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`=NULL WHERE `id`='48854';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`=NULL WHERE `id`='48850';
            DELETE FROM {{organizercontactlist}} WHERE `id`='46193';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.133' WHERE `id`='46196';
            DELETE FROM {{organizercontactlist}} WHERE `id`='55248';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `countryId`) VALUES ('681', '9192362778', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='55249';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='55250';
            DELETE FROM {{organizercontactlist}} WHERE `id`='55251';
            DELETE FROM {{organizercontactlist}} WHERE `id`='55252';
            DELETE FROM {{organizercontactlist}} WHERE `id`='55247';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}