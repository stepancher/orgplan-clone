<?php

class m160712_123848_translate_additionalexpensies extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			UPDATE {{tradditionalexpenses}} SET name = 'Exhibition space'
			WHERE trParentId = 1 AND langId = 'en';
			
			UPDATE {{tradditionalexpenses}} SET name = 'Stand construction'
			WHERE trParentId = 2 AND langId = 'en';
			
			UPDATE {{tradditionalexpenses}} SET name = 'Logistics'
			WHERE trParentId = 3 AND langId = 'en';
			
			UPDATE {{tradditionalexpenses}} SET name = 'Venue services'
			WHERE trParentId = 4 AND langId = 'en';
			
			UPDATE {{tradditionalexpenses}} SET name = 'Advertising and promotion'
			WHERE trParentId = 5 AND langId = 'en';
			
			UPDATE {{tradditionalexpenses}} SET name = 'Travel expenses, hotel'
			WHERE trParentId = 6 AND langId = 'en';
			
			UPDATE {{tradditionalexpenses}} SET name = 'Other expenses'
			WHERE trParentId = 7 AND langId = 'en';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}