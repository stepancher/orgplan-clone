<?php

class m170316_104726_copy_exdb_fair_raw_data_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_association_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_association_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE a_id INT DEFAULT 0;
                DECLARE a_name TEXT DEFAULT '';
                DECLARE a_contact TEXT DEFAULT '';
                DECLARE a_contact_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ea.exdbId, ea.name, ea.exdbContacts, ea.exdbContactsRaw FROM {$expodata}.{{exdbassociation}} ea
                                        LEFT JOIN {$db}.{{association}} a ON ea.exdbId = a.exdbId
                                        WHERE a.id IS NULL ORDER BY ea.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO a_id, a_name, a_contact, a_contact_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{association}} (`exdbId`,`name`,`exdbContacts`,`exdbContactsRaw`) VALUES (a_id, a_name, a_contact, a_contact_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_audit_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_audit_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE aud_contact TEXT DEFAULT '';
                DECLARE aud_name TEXT DEFAULT '';
                DECLARE aud_lang TEXT DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT tau.exdbId, tau.contacts_en, trtau.name, trtau.langId FROM {$expodata}.{{exdbaudit}} tau
                                            LEFT JOIN {$expodata}.{{exdbtraudit}} trtau ON trtau.trParentId = tau.id
                                            LEFT JOIN {$db}.{{audit}} au ON au.exdbId = tau.id
                                        WHERE au.id IS NULL
                                        ORDER BY tau.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO aud_id, aud_contact, aud_name, aud_lang;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                        IF aud_id != temp_id THEN
                            START TRANSACTION;
                                INSERT INTO {$db}.{{audit}} (`exdbId`,`contacts_en`) VALUES (aud_id, aud_contact);
                            COMMIT;
                            SET @last_insert_id := LAST_INSERT_ID();
                            SET temp_id := aud_id;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{traudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, aud_lang);
                        COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_exhibitioncomplex_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_exhibitioncomplex_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE vc_name TEXT DEFAULT '';
                DECLARE vc_city_id INT DEFAULT 0;
                DECLARE vc_coordinate TEXT DEFAULT '';
                DECLARE vc_contact TEXT DEFAULT '';
                DECLARE vc_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT eec.exdbId, eec.name, eec.cityId, eec.coordinates, eec.contacts, eec.raw
                                        FROM {$expodata}.{{exdbexhibitioncomplex}} eec
                                        LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON eec.exdbId = ec.exdbId
                                        WHERE ec.id IS NULL ORDER BY eec.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO vc_id, vc_name, vc_city_id, vc_coordinate, vc_contact, vc_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{exhibitioncomplex}} (`exdbId`,`cityId`,`coordinates`,`exdbContact`,`exdbRaw`) VALUES (vc_id, vc_city_id, vc_coordinate, vc_contact, vc_raw);
                    COMMIT;
                    
                    SET @last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trexhibitioncomplex}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, vc_name, 'ru');
                        INSERT INTO {$db}.{{trexhibitioncomplex}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, vc_name, 'en');
                        INSERT INTO {$db}.{{trexhibitioncomplex}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, vc_name, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_organizer_company_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_organizer_company_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT exdbo.id, exdbtro.name, exdbtro.langId 
                                        FROM {$expodata}.{{exdborganizercompany}} exdbo
                                            LEFT JOIN {$expodata}.{{exdbtrorganizercompany}} exdbtro ON exdbtro.trParentId = exdbo.id
                                            LEFT JOIN {$db}.{{trorganizercompany}} tro ON tro.name = exdbtro.name AND tro.langId = exdbtro.langId
                                        WHERE exdbtro.name IS NOT NULL AND exdbtro.name != '' AND tro.id IS NULL
                                        GROUP BY exdbtro.name, exdbtro.langId ORDER BY exdbo.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO exdb_id, exdb_name, exdb_lang_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != exdb_id THEN
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{organizercompany}} (`active`) VALUES (NULL);
                        COMMIT;
                        
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := exdb_id;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, exdb_name, exdb_lang_id);
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_organizercompanyhasassociation_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_organizercompanyhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT org.id AS orgId, orgass.id AS assId FROM {$expodata}.{{exdborganizercompanyhasassociation}} eoha
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eo ON eo.id = eoha.organizerCompanyId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.id = eoha.associationId
                                        LEFT JOIN {$db}.{{organizercompany}} org ON org.exdbId = eo.exdbId
                                        LEFT JOIN {$db}.{{association}} orgass ON orgass.exdbId = ass.exdbId
                                        LEFT JOIN {$db}.{{organizercompanyhasassociation}} orgoha ON orgoha.organizerCompanyId = org.id AND orgoha.associationId = orgass.id
                                        WHERE orgoha.id IS NULL ORDER BY orgId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{organizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES (org_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_exhibitioncomplexhasassociation_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_exhibitioncomplexhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ec_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ec.id, a.id FROM {$expodata}.{{exdbexhibitioncomplexhasassociation}} eeha
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} eec ON eec.id = eeha.exhibitionComplexId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ea ON ea.id = eeha.associationId
                                        LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON ec.exdbId = eec.exdbId
                                        LEFT JOIN {$db}.{{association}} a ON a.exdbId = ea.exdbId
                                        LEFT JOIN {$db}.{{exhibitioncomplexhasassociation}} eha ON eha.exhibitionComplexId = ec.id AND eha.associationId = a.id
                                        WHERE eha.id IS NULL ORDER BY ec.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO ec_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{exhibitioncomplexhasassociation}} (`exhibitionComplexId`,`associationId`) VALUES (ec_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_audithasassociation_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_audithasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT aud.id, assoc.id FROM {$expodata}.{{exdbaudithasassociation}} aha
                                            LEFT JOIN {$expodata}.{{exdbaudit}} au ON au.id = aha.auditId
                                            LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.id = aha.associationId
                                            LEFT JOIN {$db}.{{audit}} aud ON aud.exdbId = au.exdbId
                                            LEFT JOIN {$db}.{{association}} assoc ON assoc.exdbId = ass.exdbId
                                            LEFT JOIN {$db}.{{audithasassociation}} oaha ON oaha.auditId = aud.id AND oaha.associationId = assoc.id
                                            WHERE oaha.id IS NULL
                                        ORDER BY aud.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    
                    read_loop: LOOP
                        
                        FETCH copy INTO aud_id, ass_id;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{audithasassociation}} (`auditId`,`associationId`) VALUES (aud_id, ass_id);
                        COMMIT;
                    END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_associationhasassociation_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_associationhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_first INT DEFAULT 0;
                DECLARE ass_second INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ass1.id, ass2.id FROM {$expodata}.{{exdbassociationhasassociation}} eaha
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass1 ON eass1.id = eaha.associationId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass2 ON eass2.id = eaha.hasAssociationId
                                        LEFT JOIN {$db}.{{association}} ass1 ON ass1.exdbId = eass1.exdbId
                                        LEFT JOIN {$db}.{{association}} ass2 ON ass2.exdbId = eass2.exdbId
                                        LEFT JOIN {$db}.{{associationhasassociation}} aha ON aha.associationId = ass1.id AND aha.hasAssociationId = ass2.id
                                        WHERE aha.id IS NULL ORDER BY ass1.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO ass_first, ass_second;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{associationhasassociation}} (`associationId`,`hasAssociationId`) VALUES (ass_first, ass_second);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairs_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairs_to_db`()
            BEGIN
            
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_exdb_id INT DEFAULT 0;
                DECLARE f_exdb_crc BIGINT DEFAULT 0;
                DECLARE f_exdb_frequency TEXT DEFAULT '';
                DECLARE f_name TEXT DEFAULT '';
                DECLARE f_name_de TEXT DEFAULT '';
                DECLARE f_exhibition_complex_id INT DEFAULT '';
                DECLARE f_begin_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_date DATE DEFAULT '1970-01-01';
                DECLARE f_begin_mounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_mounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_begin_demounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_end_demounting_date DATE DEFAULT '1970-01-01';
                DECLARE f_exdb_business_sectors TEXT DEFAULT '';
                DECLARE f_exdb_costs TEXT DEFAULT '';
                DECLARE f_exdb_show_type TEXT DEFAULT '';
                DECLARE f_site TEXT DEFAULT '';
                DECLARE f_story_id INT DEFAULT 0;
                DECLARE f_area_special_expositions INT DEFAULT 0;
                DECLARE f_square_net INT DEFAULT 0;
                DECLARE f_members INT DEFAULT 0;
                DECLARE f_exhibitors_foreign INT DEFAULT 0;
                DECLARE f_exhibitors_local INT DEFAULT 0;
                DECLARE f_visitors INT DEFAULT 0;
                DECLARE f_visitors_foreign INT DEFAULT 0;
                DECLARE f_visitors_local INT DEFAULT 0;
                DECLARE f_statistics TEXT DEFAULT '';
                DECLARE f_statistics_date TEXT DEFAULT '';
                DECLARE f_countries_count TEXT DEFAULT '';
                DECLARE f_raw TEXT DEFAULT '';
                DECLARE f_first_year_show TEXT DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT 
                                        ef.exdbId, 
                                        ef.exdbCrc, 
                                        ef.exdbFrequency, 
                                        ef.name, 
                                        ef.name_de, 
                                        ec.id AS exhibitionComplexId,
                                        ef.beginDate,
                                        ef.endDate,
                                        ef.beginMountingDate,
                                        ef.endMountingDate,
                                        ef.beginDemountingDate,
                                        ef.endDemountingDate,
                                        ef.exdbBusinessSectors,
                                        ef.exdbCosts,
                                        ef.exdbShowType,
                                        ef.site,
                                        ef.storyId,
                                        efs.areaSpecialExpositions,
                                        efs.squareNet,
                                        efs.members,
                                        efs.exhibitorsForeign,
                                        efs.exhibitorsLocal,
                                        efs.visitors,
                                        efs.visitorsForeign,
                                        efs.visitorsLocal,
                                        efs.statistics,
                                        efs.statisticsDate,
                                        efs.exdbCountriesCount,
                                        ef.exdbRaw,
                                        ef.exdbFirstYearShow
                                        FROM {$expodata}.{{exdbfair}} ef
                                            LEFT JOIN {$expodata}.{{exdbfairstatistic}} efs ON efs.id = ef.infoId
                                            LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} eec ON eec.id = ef.exhibitionComplexId
                                            LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON ec.exdbId = eec.exdbId
                                            LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((f.exdbCrc IS NOT NULL AND f.exdbCrc = ef.exdbCrc) OR f.exdbCrc IS NULL)
                                        WHERE f.id IS NULL
                                        ORDER BY ef.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_exdb_id,
                                    f_exdb_crc,
                                    f_exdb_frequency,
                                    f_name,
                                    f_name_de,
                                    f_exhibition_complex_id,
                                    f_begin_date,
                                    f_end_date,
                                    f_begin_mounting_date,
                                    f_end_mounting_date,
                                    f_begin_demounting_date,
                                    f_end_demounting_date,
                                    f_exdb_business_sectors,
                                    f_exdb_costs,
                                    f_exdb_show_type,
                                    f_site,
                                    f_story_id,
                                    f_area_special_expositions,
                                    f_square_net,
                                    f_members,
                                    f_exhibitors_foreign,
                                    f_exhibitors_local,
                                    f_visitors,
                                    f_visitors_foreign,
                                    f_visitors_local,
                                    f_statistics,
                                    f_statistics_date,
                                    f_countries_count,
                                    f_raw,
                                    f_first_year_show;
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != f_exdb_id THEN
                        START TRANSACTION;
                            INSERT INTO {$db}.{{fairinfo}} (`exdbFairId`,
                                `areaSpecialExpositions`,
                                `squareNet`,
                                `members`,
                                `exhibitorsForeign`,
                                `exhibitorsLocal`,
                                `visitors`,
                                `visitorsForeign`,
                                `visitorsLocal`,
                                `statistics`,
                                `exdbStatisticsDate`,
                                `exdbCountriesCount`) VALUES (f_exdb_id,
                                f_area_special_expositions,
                                f_square_net,
                                f_members,
                                f_exhibitors_foreign,
                                f_exhibitors_local,
                                f_visitors,
                                f_visitors_foreign,
                                f_visitors_local,
                                f_statistics,
                                f_statistics_date,
                                f_countries_count);
                        COMMIT;
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := f_exdb_id;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fair}} (`exdbId`,
                            `exdbCrc`,
                            `exdbFrequency`,
                            `exhibitionComplexId`,
                            `beginDate`,
                            `endDate`,
                            `beginMountingDate`,
                            `endMountingDate`,
                            `beginDemountingDate`,
                            `endDemountingDate`,
                            `exdbBusinessSectors`,
                            `exdbCosts`,
                            `exdbShowType`,
                            `site`,
                            `storyId`,
                            `infoId`,
                            `exdbRaw`,
                            `exdbFirstYearShow`) VALUES (f_exdb_id,
                            f_exdb_crc,
                            f_exdb_frequency,
                            f_exhibition_complex_id,
                            f_begin_date,
                            f_end_date,
                            f_begin_mounting_date,
                            f_end_mounting_date,
                            f_begin_demounting_date,
                            f_end_demounting_date,
                            f_exdb_business_sectors,
                            f_exdb_costs,
                            f_exdb_show_type,
                            f_site,
                            f_story_id,
                            @last_insert_id,
                            f_raw,
                            f_first_year_show);
                    COMMIT;
                    
                    SET @fair_last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name, 'ru');
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name, 'en');
                        INSERT INTO {$db}.{{trfair}} (`trParentId`,`name`,`langId`) VALUES (@fair_last_insert_id, f_name_de, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairhasaudit_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairhasaudit_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE au_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT f.id, au.id FROM {$expodata}.{{exdbfairhasaudit}} efhau
                                            LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.id = efhau.fairId
                                            LEFT JOIN {$expodata}.{{exdbaudit}} eau ON eau.id = efhau.auditId
                                            LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND ef.exdbCrc = f.exdbCrc) OR ef.exdbCrc IS NULL)
                                            LEFT JOIN {$db}.{{audit}} au ON eau.exdbId = au.exdbId
                                            LEFT JOIN {$db}.{{fairhasaudit}} fhau ON fhau.fairId = f.id AND fhau.auditId = au.id
                                            WHERE fhau.id IS NULL ORDER BY f.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, au_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fairhasaudit}} (`fairId`,`auditId`) VALUES (f_id, au_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairhasassociation_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT f.id, ass.id FROM {$expodata}.{{exdbfairhasassociation}} efha
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.id = efha.fairId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass ON eass.id = efha.associationId
                                        LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND ef.exdbCrc = f.exdbCrc) OR ef.exdbCrc IS NULL)
                                        LEFT JOIN {$db}.{{association}} ass ON ass.exdbId = eass.exdbId
                                        LEFT JOIN {$db}.{{fairhasassociation}} fha ON fha.fairId = f.id AND fha.associationId = ass.id
                                        WHERE fha.id IS NULL ORDER BY f.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, ass_id;
                    
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fairhasassociation}} (`fairId`,`associationId`) VALUES (f_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairhasorganizer_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairhasorganizer_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE oc_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT f.id, oc.id
                                        FROM {$expodata}.{{exdbfairhasorganizer}} efho
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.id = efho.fairId
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eo ON eo.id = efho.organizerId
                                        LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND f.exdbCrc = ef.exdbCrc) OR ef.exdbCrc IS NULL)
                                        LEFT JOIN {$db}.{{organizercompany}} oc ON oc.exdbId = eo.exdbId
                                        LEFT JOIN {$db}.{{fairhasorganizer}} fho ON fho.fairId = f.id
                                        WHERE fho.id IS NULL
                                        AND oc.id IS NOT NULL
                                        AND f.id IS NOT NULL
                                        ORDER BY f.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, oc_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{organizer}} (`companyId`) VALUES (oc_id);
                    COMMIT;
                    
                    SET @last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fairhasorganizer}} (`fairId`,`organizerId`) VALUES (f_id, @last_insert_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fair_raw_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fair_raw_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_raw TEXT DEFAULT '';
                DECLARE f_first_year_show TEXT DEFAULT '';
                DECLARE f_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.exdbRaw, ef.exdbFirstYearShow, f.id FROM {$expodata}.{{exdbfair}} ef
                                        LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND ef.exdbCrc = f.exdbCrc) OR ef.exdbCrc IS NULL)
                                        WHERE f.exdbRaw IS NULL
                                        AND ef.exdbRaw IS NOT NULL
                                        AND f.id IS NOT NULL;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_raw, f_first_year_show, f_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {$db}.{{fair}} SET `exdbRaw` = f_raw WHERE `id` = f_id;
                        UPDATE {$db}.{{fair}} SET `exdbFirstYearShow` = f_first_year_show WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            CALL {$expodata}.`copy_exdb_fair_raw_to_db`();
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fair_raw_to_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}