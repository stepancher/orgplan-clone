<?php

class m160720_073051_update_documents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "			
			UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/184/instruction.pdf' WHERE `id`='13';
			DELETE FROM {{trdocuments}} WHERE `id`='27';
			DELETE FROM {{documents}} WHERE `id`='14';
		";
	}
}