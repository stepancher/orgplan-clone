<?php

class m161004_104736_add_capital_regions extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('11', 'baku-district');
            INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('53', 'baku');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('148', 'ru', 'Баку', 'baku');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('148', 'en', 'Baku');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('148', 'de', 'Baku');
            INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('27', 'erevan');
            INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('46', 'kiev');
            INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('28', 'minsk');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('149', 'ru', 'Ереван', 'erevan');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('149', 'en', 'Erevan');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('149', 'de', 'Erevan');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('150', 'ru', 'Киев', 'kyev');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('150', 'en', 'Kyev');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('150', 'de', 'Kyev');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('151', 'ru', 'Минск', 'minsk');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('151', 'en', 'Minsk');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('151', 'de', 'Minsk');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}