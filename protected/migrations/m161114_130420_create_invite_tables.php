<?php

class m161114_130420_create_invite_tables extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE `tbl_invite` (
                `id` INT(11) NOT NULL,
                `email` VARCHAR(255) NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                `company` TEXT NOT NULL,
                `hash` VARCHAR(255) NOT NULL,
                `siteVisitTime` DATETIME NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;


            CREATE TABLE `tbl_invitedata` (
                `id` INT(11) NOT NULL,
                `inviteId` INT(11) NOT NULL,
                `timeOfArrival` DATETIME NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}