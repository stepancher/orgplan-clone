<?php

class m151203_132549_create_charttable_with_data extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{charttable}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			DROP TABLE IF EXISTS {{charttable}};

			CREATE TABLE IF NOT EXISTS {{charttable}} (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`name` varchar(500) DEFAULT NULL,
				`type` int(11) DEFAULT NULL,
				`industryId` int(11) DEFAULT NULL,
				`granularity` varchar(45) DEFAULT NULL,
				`area` varchar(45) DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

			INSERT INTO {{charttable}} VALUES (4,'ПРОДУКЦИЯ СЕЛЬСКОГО ХОЗЯЙСТВА / РЕГИОНЫ',3,11,'','region'),
			(5,'ПРОДУКЦИЯ СЕЛЬСКОГО ХОЗЯЙСТВА / СЕЛЬСКО-ХОЗЯЙСТВЕННЫЕ ОРГАНИЗАЦИИ',1,11,NULL,'region'),
			(6,'ПРОДУКЦИЯ СЕЛЬСКОГО ХОЗЯЙСТВА / КФХ И ИП',1,11,NULL,'region'),
			(7,'СТРУКТУРА СЕЛЬСКОГО ХОЗЯЙСТВА / РАСТЕНИЕВОДСТВО',2,11,NULL,'region'),
			(8,'СТРУКТУРА СЕЛЬСКОГО ХОЗЯЙСТВА / ЖИВОТНОВОДСТВО',2,11,NULL,'region'),
			(9,'ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ И СЕЛЬСКОХОЗЯЙСТВЕННОЕ СЫРЬЕ',3,11,NULL,'region'),
			(10,'ОСНОВНЫЕ ПОКАЗАТЕЛИ СЕЛЬСКОГО ХОЗЯЙСТВА / РАСТЕНИЕВОДСТВО',2,11,NULL,'region'),
			(11,'ОСНОВНЫЕ ПОКАЗАТЕЛИ СЕЛЬСКОГО ХОЗЯЙСТВА/ ЖИВОТНОВОДСТВО',2,11,NULL,'region'),
			(15,'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ',1,2,'','state'),
			(16,'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ',3,2,'','state'),
			(17,'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ',3,2,'','state'),
			(18,'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ / ТОП-10 ПО РЕГИОНАМ',1,2,'','region'),
			(19,'ОСНОВНЫЕ ПОКАЗАТЕЛИ ОТРАСЛИ / ТОП-10 ПО РЕГИОНАМ',3,2,'','region'),
			(20,'ЖИЛИЩНЫЕ И ИПОТЕЧНЫЕ КРЕДИТЫ / ТОП-10 ПО РЕГИОНАМ',3,2,'','region'),
			(21,'ПРОИЗВОДСТВО ОСНОВНЫХ СТРОИТЕЛЬНЫХ МАТЕРИАЛОВ В РФ',3,2,'','state');
		";
	}
}