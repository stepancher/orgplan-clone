<?php

class m160622_140232_fix_link extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		UPDATE {{trattributeclass}} SET `label`='При ввозе экспонатов в страну, оформление грузовой таможенной декларации (далее ГТД) является обязательным, свяжитесь с официальным экспедитором (<a target=\"_blank\" href=\"/static/documents/fairs/184/freight_forwarder.pdf\">Официальный экспедитор</a>, <a target=\"_blank\" href=\"/static/documents/fairs/184/quotation_request.pdf\">Запрос квоты</a>).  \nПри  наличии ГТД заполните форму №9 (ниже).' WHERE `id`='2306';
		UPDATE {{trattributeclass}} SET `label`='При ввозе экспонатов в страну, оформление грузовой таможенной декларации (далее ГТД) является обязательным, свяжитесь с официальным экспедитором (<a target=\"_blank\" href=\"/static/documents/fairs/184/freight_forwarder.pdf\">Официальный экспедитор</a>, <a target=\"_blank\" href=\"/static/documents/fairs/184/quotation_request.pdf\">Запрос квоты</a>).  \nПри  наличии ГТД заполните форму №9 (ниже).' WHERE `id`='2307';
		UPDATE {{trattributeclass}} SET `label`='If you have Customs declaration, please fill the Application form No.9 (below). Otherwise please get in contact with the nominated official sole On-Site-Exhibition Freight Forwarder for all questions regarding, Customs-Clearance- On-Site-Handling- Transport-Service & to and from the fairgrounds (<a target=\"_blank\" href=\"/static/documents/fairs/184/freight_forwarder.pdf\">Freight-Forwarder</a>, <a target=\"_blank\" href=\"/static/documents/fairs/184/quotation_request.pdf\">Quotation Request</a>).' WHERE `id`='2307';
";
	}
}