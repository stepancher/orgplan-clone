<?php

class m160727_152820_delete_krym_district extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			DELETE FROM {{trdistrict}} WHERE `id`='75';
			DELETE FROM {{trdistrict}} WHERE `id`='76';
			DELETE FROM {{trdistrict}} WHERE `id`='77';
			DELETE FROM {{trdistrict}} WHERE `id`='100';
			DELETE FROM {{trdistrict}} WHERE `id`='101';
			DELETE FROM {{trdistrict}} WHERE `id`='102';
			DELETE FROM {{trdistrict}} WHERE `id`='50';
			DELETE FROM {{trdistrict}} WHERE `id`='51';
			DELETE FROM {{trdistrict}} WHERE `id`='52';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}