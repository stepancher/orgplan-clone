<?php

class m170504_131551_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('276', 'schönwalde-glien');

            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1566', 'ru', 'Шёнвальде-Глин');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1566', 'en', 'Schönwalde-Glien');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1566', 'de', 'Schönwalde-Glien');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('169', 'holzwickede');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1567', 'ru', 'Хольцвиккеде');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1567', 'en', 'Holzwickede');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1567', 'de', 'Holzwickede');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('163', 'westhausen');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1568', 'ru', 'Вестхаузен (Вюртемберг)');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1568', 'en', 'Westhausen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1568', 'de', 'Westhausen (Württemberg)');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('267', 'langenau');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1569', 'ru', 'Лангенау');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1569', 'en', 'Langenau');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1569', 'de', 'Langenau');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('282', 'Bodolz');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1570', 'ru', 'Бодольц');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1570', 'en', 'Bodolz');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1570', 'de', 'Bodolz');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('157', 'xanten');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1571', 'ru', 'Ксантен');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1571', 'en', 'Xanten');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1571', 'de', 'Xanten');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('331', 'rum');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1572', 'ru', 'Рум');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1572', 'en', 'Rum');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1572', 'de', 'Rum');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('162', 'trossingen');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1573', 'ru', 'Троссинген');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1573', 'en', 'Trossingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1573', 'de', 'Trossingen');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}