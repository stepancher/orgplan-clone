<?php

class m160826_123922_trproductcategory extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{trproductcategory}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `langId` VARCHAR(255) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `description` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            INSERT INTO {{trproductcategory}} (`trParentId`, `langId`, `name`, `description`)
            (SELECT `id`, 'ru', `name`, `description` FROM {{productcategory}});

            INSERT INTO {{trproductcategory}} (`trParentId`, `langId`, `name`, `description`)
            (SELECT `id`, 'en', `name`, `description` FROM {{productcategory}});
            
            INSERT INTO {{trproductcategory}} (`trParentId`, `langId`, `name`, `description`)
            (SELECT `id`, 'de', `name`, `description` FROM {{productcategory}});

            UPDATE {{trproductcategory}} SET `name` = 'Аudiovisual equipment' WHERE `langId` =  'en' AND `name` = 'Аудио-визуальное оборудование';
            UPDATE {{trproductcategory}} SET `name` = 'Catering services' WHERE `langId` =  'en' AND `name` = 'Услуги кейтеринга';
            UPDATE {{trproductcategory}} SET `name` = 'Printing services' WHERE `langId` =  'en' AND `name` = 'Полиграфические услуги';
            UPDATE {{trproductcategory}} SET `name` = 'Promotional staff services' WHERE `langId` =  'en' AND `name` = 'Услуги промо персонала';
            UPDATE {{trproductcategory}} SET `name` = 'Mobile equipment' WHERE `langId` =  'en' AND `name` = 'Мобильное оборудование';
            UPDATE {{trproductcategory}} SET `name` = 'Promotional products' WHERE `langId` =  'en' AND `name` = 'Сувенирная продукция';
            
            UPDATE {{trproductcategory}} SET `description` = 'Audio set, Plasma Display Panel' WHERE `langId` = 'en' AND `description` = 'Звуковой комплект, плазменная панель';
            UPDATE {{trproductcategory}} SET `description` = 'Refreshment break, standing buffet, dinner event' WHERE `langId` = 'en' AND `description` = 'Кофе-брейк, фуршет, банкет.';
            UPDATE {{trproductcategory}} SET `description` = 'Printing and design of business cards, flyers, leaflets, complimentary tickets, posters, banners.' WHERE `langId` = 'en' AND `description` = 'Печать и дизайн визиток, листовок, лифлетов, пригласительных, плакатов, баннеров.';
            UPDATE {{trproductcategory}} SET `description` = 'Stand-assistants, hostesses, entertainment representatives. Balloon garlands' WHERE `langId` = 'en' AND `description` = 'Услуги стентистов, хостес, аниматоров. Гирлянды из шаров.';
            UPDATE {{trproductcategory}} SET `description` = 'Portable speaker stand, Pop-up, Roll-up, Fold-up stands.' WHERE `langId` = 'en' AND `description` = 'Мобильная трибуна, Pop-up, Roll-up, планшетный стенд.';
		
		    UPDATE {{trproductcategory}} SET `description` = 'Услуги стендистов, хостес, аниматоров. Гирлянды из шаров.' WHERE `description` = 'Услуги стентистов, хостес, аниматоров. Гирлянды из шаров.';
		
            ALTER TABLE {{productcategory}} 
            DROP COLUMN `description`,
            DROP COLUMN `name`;

            ALTER TABLE {{trproductcategory}} 
            ADD UNIQUE INDEX `UNIQ_trPArentAndLangId` (`trParentId` ASC, `langId` ASC);

            ALTER TABLE {{trproductcategory}} 
            ADD CONSTRAINT `fk_productcategory_trParentId`
              FOREIGN KEY (`trParentId`)
              REFERENCES {{productcategory}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}