<?php

class m170119_074752_add_fair_to_expodatabase extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->expodatabase->beginTransaction();
        try {
            Yii::app()->expodatabase->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->expodatabase->beginTransaction();
        try {
            Yii::app()->expodatabase->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE IF NOT EXISTS {{fair}} (
              `id` INT(11) NOT NULL,
              `name_en` TEXT NULL DEFAULT NULL,
              `name_de` TEXT NULL DEFAULT NULL,
              `expo_loc` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}