<?php

class m170328_140544_remove_organizer_contact_country_code extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{organizercontactlist}} SET `countryId` = 1 WHERE `value` LIKE '+7%';
            UPDATE {{organizercontactlist}} SET `countryId` = 5 WHERE `value` LIKE '+375%';
            UPDATE {{organizercontactlist}} SET `countryId` = 8 WHERE `value` LIKE '+373%';
            UPDATE {{organizercontactlist}} SET `countryId` = 9 WHERE `value` LIKE '+998%';
            UPDATE {{organizercontactlist}} SET `countryId` = 3 WHERE `value` LIKE '+99(4%';
            UPDATE {{organizercontactlist}} SET `value`='(920)459-99-39agro@vfcenter.ru' WHERE `id`='31193';
            UPDATE {{organizercontactlist}} SET `value`='(920)459-99-39agro@vfcenter.ru' WHERE `id`='31195';
            UPDATE {{organizercontactlist}} SET `value`='(920)459-99-39agro@vfcenter.ru' WHERE `id`='31197';
            UPDATE {{organizercontactlist}} SET `value`='(920)459-99-39agro@vfcenter.ru' WHERE `id`='31199';
            UPDATE {{organizercontactlist}} SET `value`='(926)580-71-82.info@expobroker.ru' WHERE `id`='53973';
            UPDATE {{organizercontactlist}} SET `value`='(926)580-71-82.info@expobroker.ru' WHERE `id`='53975';
            UPDATE {{organizercontactlist}} SET `value`='(926)580-71-82.info@expobroker.ru' WHERE `id`='53977';
            UPDATE {{organizercontactlist}} SET `value`='(926)580-71-82.info@expobroker.ru' WHERE `id`='53979';
            UPDATE {{organizercontactlist}} SET `value`='(926)580-71-82.info@expobroker.ru' WHERE `id`='53981';
            UPDATE {{organizercontactlist}} SET `value`='(905)338-36-33.marina@zarexpo.ru' WHERE `id`='54185';
            UPDATE {{organizercontactlist}} SET `value`='(905)338-36-33.marina@zarexpo.ru' WHERE `id`='54187';
            UPDATE {{organizercontactlist}} SET `value`='(905)338-36-33.marina@zarexpo.ru' WHERE `id`='54189';
            UPDATE {{organizercontactlist}} SET `value`='(905)338-36-33.marina@zarexpo.ru' WHERE `id`='54191';
            UPDATE {{organizercontactlist}} SET `value`='(905)338-36-33.marina@zarexpo.ru' WHERE `id`='54193';
            UPDATE {{organizercontactlist}} SET `value`='(861)299-58-46' WHERE `id`='54390';
            UPDATE {{organizercontactlist}} SET `value`='(861)299-58-46' WHERE `id`='54391';
            UPDATE {{organizercontactlist}} SET `value`='(861)299-58-46' WHERE `id`='54392';
            UPDATE {{organizercontactlist}} SET `value`='(861)299-58-46' WHERE `id`='54393';
            UPDATE {{organizercontactlist}} SET `value`='(861)299-58-46' WHERE `id`='54394';
            UPDATE {{organizercontactlist}} SET `value`='(906)420-57-76' WHERE `id`='54402';
            UPDATE {{organizercontactlist}} SET `value`='(495)268-04-54' WHERE `id`='56254';
            UPDATE {{organizercontactlist}} SET `value`='(495)988-28-01' WHERE `id`='56256';
            UPDATE {{organizercontactlist}} SET `value`='(495)268-04-54' WHERE `id`='56267';
            UPDATE {{organizercontactlist}} SET `value`='(495)268-04-54' WHERE `id`='56268';
            UPDATE {{organizercontactlist}} SET `value`='(495)268-04-54' WHERE `id`='56269';
            UPDATE {{organizercontactlist}} SET `value`='(495)268-04-54' WHERE `id`='56273';
            UPDATE {{organizercontactlist}} SET `value`='(495)988-28-01' WHERE `id`='56274';
            UPDATE {{organizercontactlist}} SET `value`='(495)988-28-01' WHERE `id`='56275';
            UPDATE {{organizercontactlist}} SET `value`='(17)204-63-99. minsk.mill@gmail.com' WHERE `id`='49759';
            UPDATE {{organizercontactlist}} SET `value`='(17)204-63-99. minsk.mill@gmail.com' WHERE `id`='49760';
            UPDATE {{organizercontactlist}} SET `value`='(17)204-63-99. minsk.mill@gmail.com' WHERE `id`='49761';
            UPDATE {{organizercontactlist}} SET `value`='(17)204-63-99. minsk.mill@gmail.com' WHERE `id`='49762';
            UPDATE {{organizercontactlist}} SET `value`='(17)204-63-99. minsk.mill@gmail.com' WHERE `id`='49763';
            UPDATE {{organizercontactlist}} SET `value`='(17)240-29-65. kd@exponent.by' WHERE `id`='49816';
            UPDATE {{organizercontactlist}} SET `value`='(17)240-29-65. kd@exponent.by' WHERE `id`='49817';
            UPDATE {{organizercontactlist}} SET `value`='(17)240-29-65. kd@exponent.by' WHERE `id`='49818';
            UPDATE {{organizercontactlist}} SET `value`='(17)240-29-65. kd@exponent.by' WHERE `id`='49819';
            UPDATE {{organizercontactlist}} SET `value`='(17)240-29-65. kd@exponent.by' WHERE `id`='49820';
            UPDATE {{organizercontactlist}} SET `value`='(22)74-74-20.info@moldexpo.md' WHERE `id`='51856';
            UPDATE {{organizercontactlist}} SET `value`='(22)74-74-20.info@moldexpo.md' WHERE `id`='51858';
            UPDATE {{organizercontactlist}} SET `value`='(22)74-74-20.info@moldexpo.md' WHERE `id`='51860';
            UPDATE {{organizercontactlist}} SET `value`='(22)74-74-20.info@moldexpo.md' WHERE `id`='51862';
            UPDATE {{organizercontactlist}} SET `value`='(22)74-74-20.info@moldexpo.md' WHERE `id`='51864';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51869';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51871';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51873';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51875';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51877';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51932';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51934';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51936';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51938';
            UPDATE {{organizercontactlist}} SET `value`='(22)81-04-08.ivanov@moldexpo.md' WHERE `id`='51940';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. caitme@ite-uzbekistan.uz ' WHERE `id`='52143';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. caitme@ite-uzbekistan.uz ' WHERE `id`='52144';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. caitme@ite-uzbekistan.uz ' WHERE `id`='52145';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. caitme@ite-uzbekistan.uz ' WHERE `id`='52146';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. caitme@ite-uzbekistan.uz ' WHERE `id`='52147';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. mca@ite-uzbekistan.uz ' WHERE `id`='52162';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. mca@ite-uzbekistan.uz ' WHERE `id`='52163';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. mca@ite-uzbekistan.uz ' WHERE `id`='52164';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. mca@ite-uzbekistan.uz ' WHERE `id`='52165';
            UPDATE {{organizercontactlist}} SET `value`='(71)113-01-80. mca@ite-uzbekistan.uz ' WHERE `id`='52166';
            UPDATE {{organizercontactlist}} SET `value`='(17)70-01-71.vystavki@minskexpo.com' WHERE `id`='54294';
            UPDATE {{organizercontactlist}} SET `value`='(17)70-01-71.vystavki@minskexpo.com' WHERE `id`='54296';
            UPDATE {{organizercontactlist}} SET `value`='(17)70-01-71.vystavki@minskexpo.com' WHERE `id`='54298';
            UPDATE {{organizercontactlist}} SET `value`='(17)70-01-71.vystavki@minskexpo.com' WHERE `id`='54300';
            UPDATE {{organizercontactlist}} SET `value`='(17)70-01-71.vystavki@minskexpo.com' WHERE `id`='54302';
            UPDATE {{organizercontactlist}} SET `value`='(412)447-47-74' WHERE `id`='54372';
            UPDATE {{organizercontactlist}} SET `value`='(412)447-47-74' WHERE `id`='54373';
            UPDATE {{organizercontactlist}} SET `value`='(412)447-47-74' WHERE `id`='54374';
            UPDATE {{organizercontactlist}} SET `value`='(455)400-05-77' WHERE `id`='56240';
            UPDATE {{organizercontactlist}} SET `value`='(455)400-05-77' WHERE `id`='56242';
            UPDATE {{organizercontactlist}} SET `value`='(455)400-05-77' WHERE `id`='56244';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}