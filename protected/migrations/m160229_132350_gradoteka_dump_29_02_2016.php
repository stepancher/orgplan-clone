<?php

class m160229_132350_gradoteka_dump_29_02_2016 extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down(){
		return true;
	}

	public function getCreateTable(){
		return '
			DROP TABLE IF EXISTS {{gvalue}};
			DROP TABLE IF EXISTS {{gobjectshasgstattypes}};
			DROP TABLE IF EXISTS {{gobjects}};
			DROP TABLE IF EXISTS {{gstattypes}};
		'.file_get_contents(Yii::getPathOfAlias('app').'/data/gradoteka_dump_29_02_2016___16_22.sql');
	}
}