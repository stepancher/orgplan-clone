<?php

class m170123_102045_add_organizercontactlist_uniq extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
        
            ALTER TABLE {{organizercontactlist}} 
            CHANGE COLUMN `value` `value` VARCHAR(255) NULL DEFAULT NULL ;

            ALTER TABLE {{organizercontactlist}}
            ADD UNIQUE INDEX `valuetypeunq` (`value` ASC, `type` ASC, `organizerId` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}