<?php

class m170221_133153_copy_exhibitioncomplexhasassociation_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_exhibitioncomplexhasassociation_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_exhibitioncomplexhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ec_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ec.id, a.id FROM {$expodata}.{{exdbexhibitioncomplexhasassociation}} eeha
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} eec ON eec.id = eeha.exhibitionComplexId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ea ON ea.id = eeha.associationId
                                        LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON ec.exdbId = eec.exdbId
                                        LEFT JOIN {$db}.{{association}} a ON a.exdbId = ea.exdbId
                                        LEFT JOIN {$db}.{{exhibitioncomplexhasassociation}} eha ON eha.exhibitionComplexId = ec.id AND eha.associationId = a.id
                                        WHERE eha.id IS NULL ORDER BY ec.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO ec_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{exhibitioncomplexhasassociation}} (`exhibitionComplexId`,`associationId`) VALUES (ec_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_exhibitioncomplexhasassociation_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}