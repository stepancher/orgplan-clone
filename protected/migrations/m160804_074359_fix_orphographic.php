<?php

class m160804_074359_fix_orphographic extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 25 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='255';
			UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 24 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='433';
			UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='520';
			UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 29 июля 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='1996';
			UPDATE {{trattributeclass}} SET `label`='Отправьте заполненную форму в дирекцию выставки не позднее 18 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru' WHERE `id`='2154';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}