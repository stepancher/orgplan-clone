<?php

class m161206_082338_recovery_trreports extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {

        return true;
    }


    public function upSql()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        list($peace1, $peace2, $dbF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        $sql= '';

        /**
         * @var Fair $fair
         */
        foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){
                if($fair->id == 10943){
                    continue;
                }

                $dbTargetName = Yii::app()->dbMan->createShardName($fair->id);

                $sql = $sql."
                    DROP TABLE IF EXISTS `{$dbTargetName}`.tbl_trreport;
                    CREATE TABLE `{$dbTargetName}`.tbl_trreport LIKE `{$dbF10943}`.tbl_trreport;
                    INSERT INTO  `{$dbTargetName}`.tbl_trreport SELECT * FROM `{$dbF10943}`.tbl_trreport; 
                ";
            }
        }

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}