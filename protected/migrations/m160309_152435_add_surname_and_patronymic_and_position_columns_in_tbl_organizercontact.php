<?php

class m160309_152435_add_surname_and_patronymic_and_position_columns_in_tbl_organizercontact extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{organizercontact}}
			DROP COLUMN `surname`,
			DROP COLUMN `patronymic`,
			DROP COLUMN `position`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			ALTER TABLE {{organizercontact}}
			ADD COLUMN `surname` VARCHAR(45) NULL AFTER `email`,
			ADD COLUMN `patronymic` VARCHAR(45) NULL AFTER `surname`,
			ADD COLUMN `position` INT NULL AFTER `patronymic`;
		';
	}
}

