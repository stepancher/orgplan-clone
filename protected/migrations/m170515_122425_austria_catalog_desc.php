<?php

class m170515_122425_austria_catalog_desc extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{catalogdescription}} (`countryId`) VALUES ('17');
            INSERT INTO {{trcatalogdescription}} (`trParentId`, `langId`, `header`, `text`) VALUES ('26', 'ru', 'Выставки в Австрии решают задачи экономики страны', '<p>Одна из богатейших стран Европы – Австрия. Это развитое европейское государство, имеющее индустриально-аграрную направленность экономики. Мощная промышленность с широкой производственной базой, квалифицированной рабочей силой требует:\n</p>\n<ul>\n	<li>постоянного расширения рынков сбыта;</li>\n	<li>установления новых хозяйственных связей;</li>\n	<li>определения конкурентоспособности услуг и товаров на этапе их внедрения на рынок;</li>\n	<li>получения информации о новейших мировых разработках;</li>\n	<li>регулярного обмена опытом между профессионалами.</li>\n</ul>\n<p>Все эти задачи решают выставки в Австрии.\n</p>\n<p>На выставочных площадках страны:\n</p>\n<ul>\n	<li>определяется спрос и формируются предложения и знакомство с новыми разработками в отрасли;</li>\n	<li>проводятся многочисленные маркетинговые мероприятия;</li>\n	<li>в выгодном свете представляются услуги и продукция компании;</li>\n	<li>определяется стратегия дальнейшего развития предприятия или компании;</li>\n	<li>заключаются долгосрочные выгодные контракты;</li>\n	<li>развивается бизнес.</li>\n</ul>\n<p>Крупнейшие выставочные центры, в которых проходят выставки Австрии межрегионального и международного значения, отражающие работу отраслей хозяйства страны, расположены в крупных мегаполисах в Вене, Зальцбурге, Клагенфурте, Граце.\n</p>');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}