<?php

class m160627_062746_translates_12a_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('21', 'en', 'APPLICATION No.12a', 'VIP BADGES');


		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES (NULL, '2205', 'en', 'This form should be filled in until August 20, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru', NULL);
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `unitTitle`) VALUES (NULL, '2296', 'en', 'VIP badge entitles visiting the exhibition \"AGROSALON\" and all the activities of the business program.<br><br>Available number of VIP badges depends on the area of Exhibitor\'s stand. It\'s defined as follows: <br> stand up to 100 m² - 1 pcs. <br> Stand from 100sq.m. up to 500sq.m. - 2 pcs. <br> Stand more than 500sq.m. - 3 pcs.', NULL);
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2206', 'en', '1. VIP badges');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2207', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2208', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2209', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2210', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2211', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2212', 'en', 'Post address');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2213', 'en', 'ZIP code');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2214', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2215', 'en', 'Region');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2216', 'en', 'City');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2217', 'en', 'Street');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2218', 'en', 'House');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2219', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2220', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2221', 'en', '2. VIP badges');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2222', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2223', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2224', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2225', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2226', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2227', 'en', 'Post address');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2228', 'en', 'ZIP code');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2229', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2230', 'en', 'Region');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2231', 'en', 'City');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2232', 'en', 'Street');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2233', 'en', 'House');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2234', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2235', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2236', 'en', '3. VIP badges');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2237', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2238', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2239', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2240', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2241', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2242', 'en', 'Post address');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2243', 'en', 'ZIP code');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2244', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2245', 'en', 'Region');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2246', 'en', 'City');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2247', 'en', 'Street');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2248', 'en', 'House');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2249', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2250', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2251', 'en', '4. VIP badges');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2252', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2253', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2254', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2255', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2256', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2257', 'en', 'Post address');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2258', 'en', 'ZIP code');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2259', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2260', 'en', 'Region');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2261', 'en', 'City');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2262', 'en', 'Street');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2263', 'en', 'House');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2264', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2265', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2266', 'en', '5. VIP badges');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2267', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2268', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2269', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2270', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2271', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2272', 'en', 'Post address');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2273', 'en', 'ZIP code');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2274', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2275', 'en', 'Region');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2276', 'en', 'City');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2277', 'en', 'Street');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2278', 'en', 'House');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2279', 'en', 'Telephone');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2280', 'en', 'Fax');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2281', 'en', '6. VIP badges');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2282', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2283', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2284', 'en', 'Middle name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2285', 'en', 'Position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2286', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2287', 'en', 'Post address');
		
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2399', 'en', 'Exhibitor');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2400', 'en', 'position');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2401', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2402', 'en', 'signature');
";
	}
}

