<?php

class m161216_135131_insert_de_trregioninformation extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO tbl_trregioninformation (trParentId, text, descriptionSnippet, langId)
            SELECT trParentId, text, descriptionSnippet, 'de' FROM tbl_trregioninformation WHERE langId = 'ru' AND text IS NOT NULL AND text != '';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}