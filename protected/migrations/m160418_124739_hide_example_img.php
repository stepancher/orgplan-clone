<?php

class m160418_124739_hide_example_img extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{documents}} 
			ADD COLUMN `active` INT NULL AFTER `value`;
			ALTER TABLE {{documents}} 
			CHANGE COLUMN `active` `active` INT(11) NULL DEFAULT 1 ;
			UPDATE {{documents}} SET `active`='0' WHERE `id`='1';
			UPDATE {{documents}} SET `active`='0' WHERE `id`='2';
			UPDATE {{documents}} SET `active`='0' WHERE `id`='3';
			INSERT INTO {{documents}} (`name`, `type`, `value`, `active`) VALUES ('Положение о конкурсе', '2', '/static/documents/fairs/184/polozhenie_o_konkurse.pdf', NULL);
			INSERT INTO {{documents}} (`name`, `value`) VALUES ('Правила выставки', '/static/documents/fairs/184/pravila_vystavki.pdf');
			INSERT INTO {{documents}} (`name`, `value`) VALUES ('Схема проезда', '/static/documents/fairs/184/shema-proezda.pdf');
			UPDATE {{documents}} SET `type`='2' WHERE `id`='5';
			UPDATE {{documents}} SET `type`='2' WHERE `id`='6';
			UPDATE {{documents}} SET `active`='1' WHERE `id`='4';
	    ";
	}
}