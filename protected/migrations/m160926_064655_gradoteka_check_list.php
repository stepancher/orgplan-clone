<?php

class m160926_064655_gradoteka_check_list extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{chartcolumn}} SET `position`='1' WHERE `id`='277';
            UPDATE {{chartcolumn}} SET `position`='2' WHERE `id`='278';
            UPDATE {{chartcolumn}} SET `position`='3' WHERE `id`='362';
            UPDATE {{chartcolumn}} SET `position`='4' WHERE `id`='363';
            
            ALTER TABLE {{gvalue}} 
            CHANGE COLUMN `value` `value` DOUBLE NULL DEFAULT NULL ;

            UPDATE {{charttable}} SET `position`='2' WHERE `id`='57';
            UPDATE {{charttable}} SET `position`='3' WHERE `id`='56';
            
            DELETE FROM {{charttable}} WHERE `id`='56';
            UPDATE {{charttable}} SET `position`='3' WHERE `id`='58';

            UPDATE {{charttable}} SET `name`='Товарный объем и доля отраслей сельского хозяйства по округам' WHERE `id`='3';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем и количество организаций, КФХ и ИП' WHERE `id`='4';
            
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500081', 'Число ИП и КФХ', 'Количество КФХ/ИП организаций', 'stat-type', 'ед.', '1');
            
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `wholeFlag`, `formula`) VALUES ('500081', 'custom', '1', '100-[1]');
            
            UPDATE {{chartcolumn}} SET `statId`='56d03122f7a9add4068f84a2' WHERE `id`='36';
            DELETE FROM {{gstattypehasformula}} WHERE `id`='38';
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('56d03122f7a9add4068f84a2', 'Хозяйства всех категорий # Картофель - всего', 'Валовой сбор сельскохозяйственных культур', 'stat-type', 'stat-type', '0');
            DELETE FROM {{gstattypes}} WHERE `id`='503243';
            UPDATE {{gstattypes}} SET `unit`='тысяча центнеров' WHERE `id`='503244';
            UPDATE {{chartcolumn}} SET `statId`='571e458d77d89702498c74a2' WHERE `id`='60';
            UPDATE {{chartcolumn}} SET `statId`='571e458d77d89702498c74a2' WHERE `id`='59';
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('571e458d77d89702498c74a2', '26.51.12 Портландцемент, цемент глиноземистый, цемент шлаковый и аналогичные цементы гидравлические', 'Производство основных видов продукции в натуральном выражении с 2010 г.(в соответствии с ОКПД)', 'stat-type', 'Тысяча тонн', '0');
            UPDATE {{chartcolumn}} SET `statId`='571e458d77d89702498c74a2' WHERE `id`='400';
            UPDATE {{chartcolumn}} SET `statId`='571e458d77d89702498c74a2' WHERE `id`='399';
            
            UPDATE {{chartcolumn}} SET `statId`='571e43f277d89702498b7330' WHERE `id`='126';
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('571e43f277d89702498b7330', '15.91.10.111 Водка', 'Производство основных видов продукции в натуральном выражении с 2010 г.(в соответствии с ОКПД)', 'stat-type', 'Тысяча декалитров', '1');
            UPDATE {{chartcolumn}} SET `statId`='577e7c79bd4c562e658b4636' WHERE `id`='129';
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('577e7c79bd4c562e658b4636', 'Вина игристые и шампанские', 'Производство основных видов продукции в натуральном выражении с 2010 г.(в соответствии с ОКПД)', 'stat-type', 'Тысяча декалитров', '1');
            
            ALTER TABLE {{chartcolumn}} 
            ADD COLUMN `accessory` VARCHAR(45) NULL DEFAULT NULL AFTER `related`;
            
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='136';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='132';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='133';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='134';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='135';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='137';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='138';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='140';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='142';
            UPDATE {{chartcolumn}} SET `accessory`='state' WHERE `id`='144';
            UPDATE {{chartcolumn}} SET `header`='Производство скота и птицы на убой' WHERE `id`='225';
            UPDATE {{chartcolumn}} SET `statId`='5784b781bd4c56dc4f8b5d48' WHERE `id`='249';
            
            UPDATE {{charttablegroup}} SET `color` = '#e0891f';
            
            DELETE FROM {{gvalue}};
            DELETE FROM {{gobjectshasgstattypes}};
            
            UPDATE {{gstattypes}} SET `name`='Машины сельскохозяйственные' WHERE `id`='500832';
            UPDATE {{gstattypes}} SET `name`='Машины сельскохозяйственные' WHERE `id`='500833';
            UPDATE {{gstattypes}} SET `name`='Машины или механизмы для уборки или обмолота' WHERE `id`='501034';
            UPDATE {{gstattypes}} SET `name`='Машины или механизмы для уборки или обмолота' WHERE `id`='501035';
            UPDATE {{gstattypes}} SET `name`='Установки и аппараты доильные' WHERE `id`='501128';
            UPDATE {{gstattypes}} SET `name`='Установки и аппараты доильные' WHERE `id`='501129';
            UPDATE {{gstattypes}} SET `name`='Оборудование для виноделия' WHERE `id`='501178';
            UPDATE {{gstattypes}} SET `name`='Оборудование для виноделия' WHERE `id`='501179';
            UPDATE {{gstattypes}} SET `name`='Машины для очистки, сортировки или калибровки семян' WHERE `id`='501300';
            UPDATE {{gstattypes}} SET `name`='Машины для очистки, сортировки или калибровки семян' WHERE `id`='501301';
            UPDATE {{gstattypes}} SET `name`='Древесина топливная' WHERE `id`='501400';
            UPDATE {{gstattypes}} SET `name`='Древесина топливная' WHERE `id`='501401';
            UPDATE {{gstattypes}} SET `name`='Лесоматериалы, необработанные' WHERE `id`='501402';
            UPDATE {{gstattypes}} SET `name`='Лесоматериалы, необработанные' WHERE `id`='501403';
            UPDATE {{gstattypes}} SET `name`='Отходы и лом черных металлов' WHERE `id`='501463';
            UPDATE {{gstattypes}} SET `name`='Отходы и лом черных металлов' WHERE `id`='501464';
            UPDATE {{gstattypes}} SET `name`='ТОВАРООБОРОТ ЧЕРНЫХ МЕТАЛЛОВ' WHERE `id`='502412';
            UPDATE {{gstattypes}} SET `name`='ТОВАРООБОРОТ ЧЕРНЫХ МЕТАЛЛОВ' WHERE `id`='502413';
            UPDATE {{gstattypes}} SET `name`='ТОВАРООБОРОТ КОРРОЗИОННОСТОЙКОЙ СТАЛИ' WHERE `id`='502721';
            UPDATE {{gstattypes}} SET `name`='ТОВАРООБОРОТ КОРРОЗИОННОСТОЙКОЙ СТАЛИ' WHERE `id`='502722';
            UPDATE {{gstattypes}} SET `name`='Станки металлорежущие' WHERE `id`='503089';
            UPDATE {{gstattypes}} SET `name`='Станки металлорежущие' WHERE `id`='503090';
            UPDATE {{gstattypes}} SET `name`='ТОВАРООБОРОТ ЛЕГИРОВАННЫХ СТАЛЕЙ' WHERE `id`='503231';
            UPDATE {{gstattypes}} SET `name`='ТОВАРООБОРОТ ЛЕГИРОВАННЫХ СТАЛЕЙ' WHERE `id`='503232';
            UPDATE {{gstattypes}} SET `name`='Нефть' WHERE `id`='503233';
            UPDATE {{gstattypes}} SET `name`='Нефть' WHERE `id`='503234';
            UPDATE {{gstattypes}} SET `name`='Газы' WHERE `id`='503235';
            UPDATE {{gstattypes}} SET `name`='Газы' WHERE `id`='503236';
            UPDATE {{gstattypes}} SET `name`='Цемент' WHERE `id`='503237';
            UPDATE {{gstattypes}} SET `name`='Цемент' WHERE `id`='503238';
            UPDATE {{gstattypes}} SET `name`='Изделия из гипса' WHERE `id`='503239';
            UPDATE {{gstattypes}} SET `name`='Изделия из гипса' WHERE `id`='503240';
            UPDATE {{gstattypes}} SET `name`='Колбасы и аналогичные продукты из мяса' WHERE `id`='503241';
            UPDATE {{gstattypes}} SET `name`='Колбасы и аналогичные продукты из мяса' WHERE `id`='503242';
            
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82924' WHERE `id`='1';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82925' WHERE `id`='2';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82926' WHERE `id`='3';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82927' WHERE `id`='4';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82928' WHERE `id`='5';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82929' WHERE `id`='6';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82930' WHERE `id`='7';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82931' WHERE `id`='8';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82932' WHERE `id`='9';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82933' WHERE `id`='10';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82934' WHERE `id`='11';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82935' WHERE `id`='12';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82936' WHERE `id`='13';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82937' WHERE `id`='14';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82938' WHERE `id`='15';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82939' WHERE `id`='16';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82940' WHERE `id`='17';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82941' WHERE `id`='18';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82942' WHERE `id`='19';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82943' WHERE `id`='20';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82944' WHERE `id`='21';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82945' WHERE `id`='22';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82946' WHERE `id`='25';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82947' WHERE `id`='26';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82948' WHERE `id`='27';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82949' WHERE `id`='28';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82950' WHERE `id`='29';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82951' WHERE `id`='30';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82952' WHERE `id`='31';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82953' WHERE `id`='32';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82954' WHERE `id`='33';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82955' WHERE `id`='34';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82956' WHERE `id`='35';
            UPDATE {{gstatgrouphasstat}} SET `gObjectHasGStatTypeId`='82957' WHERE `id`='36';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}