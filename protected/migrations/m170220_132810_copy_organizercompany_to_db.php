<?php

class m170220_132810_copy_organizercompany_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_organizer_company_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_organizer_company_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE org_contact TEXT DEFAULT '';
                DECLARE org_contact_raw TEXT DEFAULT '';
                DECLARE org_name TEXT DEFAULT '';
                DECLARE org_lang VARCHAR(55) DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT eorg.exdbId, eorg.contacts_en, eorg.contacts_raw_en, etrorg.name, etrorg.langId
                                        FROM {$expodata}.{{exdborganizercompany}} eorg
                                            LEFT JOIN {$expodata}.{{exdbtrorganizercompany}} etrorg ON etrorg.trParentId = eorg.id
                                            LEFT JOIN {$db}.{{organizercompany}} org ON org.exdbId = eorg.exdbId
                                        WHERE org.id IS NULL ORDER BY eorg.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_id, org_contact, org_contact_raw, org_name, org_lang;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != org_id THEN
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{organizercompany}} (`exdbId`,`exdbContacts`,`exdbContactsRaw`) VALUES (org_id, org_contact, org_contact_raw);
                        COMMIT;
                        
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := org_id;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trorganizercompany}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, org_name, org_lang);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_organizer_company_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}