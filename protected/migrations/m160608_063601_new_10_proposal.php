<?php

class m160608_063601_new_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{

		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){

		return "
		UPDATE {{proposalelementclass}} SET `active`='0' WHERE `id`='18';
		
		INSERT INTO {{proposalelementclass}} (`id`, `name`, `fixed`, `fairId`, `active`) VALUES ('20', 'zayavka_n10', '0', '184', '0');

		INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('15', '20', 'ru', 'Заявка №10', 'ДЕЛОВОЕ МЕРОПРИЯТИЕ');
		INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('16', '20', 'en', 'Заявка №10', 'ДЕЛОВОЕ МЕРОПРИЯТИЕ');
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('18', 'ru', 'Заявка №6', 'АУДИО- И ВИДЕОТЕХНИКА');
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('18', 'en', 'Заявка №6', 'АУДИО- И ВИДЕОТЕХНИКА');
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('19', 'ru', 'Заявка №11', 'ОБРАЗОВАТЕЛЬНОЕ МЕРОПРИЯТИЕ');
		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('19', 'en', 'Заявка №11', 'ОБРАЗОВАТЕЛЬНОЕ МЕРОПРИЯТИЕ');


		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2132', 'n10_event', 'string', '0', '0', '2132', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2133', 'n10_event_dropdown', 'enum', '0', '0', '2132', '2132', '10', 'dropDownMultiply', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2134', 'n10_event_dropdown_option', 'bool', '0', '2133', '2132', '10', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2135', 'n10_event_dropdown_option2', 'bool', '0', '2133', '2132', '20', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2136', 'n10_event_dropdown_option3', 'bool', '0', '2133', '2132', '30', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2137', 'n10_event_dropdown_option4', 'bool', '0', '2133', '2132', '40', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2138', 'n10_event_dropdown_option5', 'bool', '0', '2133', '2132', '50', 'dropDownOption', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2140', 'n10_event_checkbox', 'bool', '0', '2132', '2132', '20', 'checkboxSwitcher', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2141', 'n10_event_checkbox_input', 'string', '0', '0', '2140', '2132', '10', 'text', '1');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2142', 'n10_event_programm', 'string', '0', '0', '2132', '2132', '30', 'textarea', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2143', 'n10_event_count', 'string', '0', '0', '2132', '2132', '40', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2144', 'n10_event_time', 'string', '0', '0', '2132', '2132', '50', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2145', 'n10_event_datetime', 'string', '0', '0', '2132', '2132', '60', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2146', 'n10_conf', 'string', '0', '0', '2146', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2147', 'n10_conf_dropdown', 'int', '0', '2146', '2146', '10', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2148', 'n10_conf_checkbox', 'bool', '0', '2146', '2146', '20', 'checkboxSwitcher', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2149', 'n10_worker', 'string', '0', '0', '2149', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2150', 'n10_worker_position', 'string', '0', '0', '2149', '2149', '10', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2151', 'n10_worker_surname', 'string', '0', '0', '2149', '2149', '20', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2152', 'n10_worker_name', 'string', '0', '0', '2149', '2149', '30', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2153', 'n10_worker_lastname', 'string', '0', '0', '2149', '2149', '40', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2154', 'n10_worker_phone', 'string', '0', '0', '2149', '2149', '50', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2155', 'n10_worker_fax', 'string', '0', '0', '2149', '2149', '60', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2156', 'n10_worker_email', 'string', '0', '0', '2149', '2149', '70', 'text', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2157', 'n10_equip', 'string', '0', '0', '2157', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2158', 'n10_equip_lcd', 'int', '0', '2157', '2157', '10', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2159', 'n10_equip_lcd2', 'int', '0', '2157', '2157', '20', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2160', 'n10_equip_screen', 'int', '0', '2157', '2157', '30', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2161', 'n10_equip_note', 'int', '0', '2157', '2157', '40', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2162', 'n10_equip_mobile', 'int', '0', '2157', '2157', '50', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2163', 'n10_equip_sound1', 'int', '0', '2157', '2157', '60', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2164', 'n10_equip_sound2', 'int', '0', '2157', '2157', '70', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2165', 'n10_equip_mic', 'int', '0', '2157', '2157', '80', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2166', 'n10_equip_radiomic', 'int', '0', '2157', '2157', '90', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2167', 'n10_equip_radio_area', 'int', '0', '2157', '2157', '100', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2168', 'n10_equip_support', 'int', '0', '2157', '2157', '110', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2169', 'n10_permission', 'string', '0', '0', '2169', '0', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2170', 'n10_permission_hint', 'string', '0', '2169', '2169', '10', 'hint', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2171', 'n10_permission_price', 'int', '0', '2169', '2169', '20', 'coefficientDouble', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2172', 'n10_summary', 'string', '0', '0', '2172', '0', 'summary', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2173', 'n10_summary_hint', 'string', '0', '2172', '2172', '10', 'hint', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2174', 'n10_summary_hint_danger', 'string', '0', '0', '2174', '20', 'hintDanger', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2175', 'n10_shadow_time', 'string', '0', '0', '2175', '0', 'hiddenDiscount', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2176', 'n10_hint_danger', 'string', '0', '0', '2176', '0', 'hintDanger', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2177', 'n10_agreement', 'bool', '0', '0', '2177', '0', 'checkboxAgreement', '0');

		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1846', '2143', 'чел.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1847', '2144', 'час', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1848', '2160', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1849', '2161', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1850', '2162', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1851', '2163', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1852', '2164', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1853', '2165', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1854', '2166', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1855', '2167', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1856', '2168', 'час', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1857', '2171', 'шт.', 'unitTitile');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1858', '2147', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1859', '2158', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1860', '2159', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1861', '2160', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1862', '2161', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1863', '2162', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1864', '2163', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1865', '2164', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1866', '2165', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1867', '2166', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1868', '2167', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1869', '2168', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1870', '2171', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1871', '2172', 'currency');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1872', '2147', '1260', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1873', '2158', '196', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1874', '2159', '196', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1875', '2160', '58', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1876', '2161', '130', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1877', '2162', '137', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1878', '2163', '196', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1879', '2164', '345', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1880', '2165', '16', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1881', '2166', '33', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1882', '2167', '80', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1883', '2168', '65', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1884', '2171', '200', 'price');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1885', '2147', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1886', '2158', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1887', '2159', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1888', '2160', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1889', '2161', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1890', '2162', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1891', '2163', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1892', '2164', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1893', '2165', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1894', '2166', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1895', '2167', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1896', '2168', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1897', '2171', '1', 'selfCollapse');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1898', '2147', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1899', '2158', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1900', '2159', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1901', '2160', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1902', '2161', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1903', '2162', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1904', '2163', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1905', '2164', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1906', '2165', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1907', '2166', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1908', '2167', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1909', '2168', 'selfCollapseLabel');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1910', '2171', 'selfCollapseLabel');

		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('173', '20', '2175', '0', '0', '10');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('174', '20', '2132', '0', '0', '20');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('175', '20', '2146', '0', '0', '30');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('176', '20', '2149', '0', '0', '40');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('177', '20', '2157', '0', '0', '50');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('178', '20', '2169', '0', '0', '60');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('179', '20', '2172', '0', '0', '70');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('180', '20', '2174', '0', '0', '80');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('181', '20', '2176', '0', '0', '9');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('182', '20', '2177', '0', '0', '90');

		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2111', '2132', 'ru', 'Тип мероприятия:');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2112', '2133', 'ru', 'Тип мероприятия');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2113', '2134', 'ru', 'Конференция');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2114', '2135', 'ru', 'Семинар');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2115', '2136', 'ru', 'Мастер-класс');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2116', '2137', 'ru', 'Круглый стол');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2117', '2138', 'ru', 'Презентация');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2118', '2140', 'ru', 'Другое');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `placeholder`) VALUES ('2119', '2141', 'ru', 'Укажите тип мероприятия');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('2120', '2142', 'ru', 'Тема мероприятия', 'Введите');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2121', '2143', 'ru', 'Количество участников');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2122', '2144', 'ru', 'Продолжительность мероприятия');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2123', '2145', 'ru', 'Желательное время проведения');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2124', '2146', 'ru', 'Конференц-зал');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `tooltipCustom`) VALUES ('2125', '2147', 'ru', '<div class=\"header\">\nПЕРИОД АРЕНДЫ\n</div>\n<div class=\"body\">\nПод термином \"день\" понимается период с 10:00 до 18:00. \"1 / 2\" дня - 4 часа в течении этого периода. Период \"1 час\"  применяется при пользовании залом сверх периода \"1 / 2 дня\" или \"день\".\n</div>');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2126', '2148', 'ru', 'Питание участников', '<div class=\"header\">ПИТАНИЕ УЧАСТНИКОВ</div><div class=\"body\">Стоимость аренды конференц-зала увеличивается на 50%, если компания планирует организовать питание участников мероприятия.</div>');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2127', '2149', 'ru', 'Ответственный сотрудник:');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2128', '2150', 'ru', 'Должность');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2129', '2151', 'ru', 'Фамилия');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2130', '2152', 'ru', 'Имя');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2131', '2153', 'ru', 'Отчество');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2132', '2154', 'ru', 'Телефон');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2133', '2155', 'ru', 'Факс');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2134', '2156', 'ru', 'Почта');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2135', '2157', 'ru', 'Аренда оборудования:');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2136', '2158', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2137', '2159', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2138', '2160', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2139', '2161', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2140', '2162', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2141', '2163', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2142', '2164', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2143', '2165', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2144', '2166', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2145', '2167', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2146', '2168', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2147', '2169', 'ru', 'Разрешение');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2148', '2170', 'ru', 'Разрешение на использование презентационного электрооборудования в конференц-залах.');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`) VALUES ('2149', '2171', 'ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2150', '2172', 'ru', 'n10_summary');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2151', '2173', 'ru', 'Все цены включают НДС 18%. Оплата производится в российских рублях по курсу ЦБ РФ на дату платежа.');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2152', '2174', 'ru', 'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. \nПри заказе работ и услуг после 29 июля 2016г. Организатор оставляет за собой право отказать Экспоненту в приеме заявки и предоставлении услуги.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2153', '2175', 'ru', 'n10_shadow_time');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2154', '2176', 'ru', 'Отправьте запоненную форму в дирекцию выставки не позднее 18 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');

		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2404', '1846', 'ru', 'чел.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2405', '1847', 'ru', 'час');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2406', '1848', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2407', '1849', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2408', '1850', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2409', '1851', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2410', '1852', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2411', '1853', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2412', '1854', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2413', '1855', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2414', '1856', 'ru', 'час');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2415', '1857', 'ru', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2416', '1846', 'en', 'чел.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2417', '1847', 'en', 'час');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2418', '1848', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2419', '1849', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2420', '1850', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2421', '1851', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2422', '1852', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2423', '1853', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2424', '1854', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2425', '1855', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2426', '1856', 'en', 'час');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2427', '1857', 'en', 'шт.');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2428', '1858', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2429', '1859', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2430', '1860', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2431', '1861', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2432', '1862', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2433', '1863', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2434', '1864', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2435', '1865', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2436', '1866', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2437', '1867', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2438', '1868', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2439', '1869', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2440', '1870', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2441', '1871', 'en', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2442', '1858', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2443', '1859', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2444', '1860', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2445', '1861', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2446', '1862', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2447', '1863', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2448', '1864', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2449', '1865', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2450', '1866', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2451', '1867', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2452', '1868', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2453', '1869', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2454', '1870', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2455', '1871', 'ru', 'USD');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2456', '1898', 'ru', 'Период аренды');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2457', '1899', 'ru', 'LED проектор (4000 Lum)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2458', '1900', 'ru', 'LED проектор (8000 Lum)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2459', '1901', 'ru', 'Проекционный экран');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2460', '1902', 'ru', 'Ноутбук (комплкт презентационных прграмм)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2461', '1903', 'ru', 'Мобильный комплект звукоусиления (600 Вт)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2462', '1904', 'ru', 'Комплект звукоусиления (1 кВт)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2463', '1905', 'ru', 'Комплект звукоусиления (2 кВт)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2464', '1906', 'ru', 'Проводной микрофон');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2465', '1907', 'ru', 'Радиомикрофон');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2466', '1908', 'ru', 'Радиофицированная трибуна');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2467', '1909', 'ru', 'Техническое сопровождение (1 чел)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2468', '1910', 'ru', 'Комплект участника');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2469', '1898', 'en', 'Период аренды');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2470', '1899', 'en', 'LED проектор (4000 Lum)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2471', '1900', 'en', 'LED проектор (8000 Lum)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2472', '1901', 'en', 'Проекционный экран');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2473', '1902', 'en', 'Ноутбук (комплкт презентационных прграмм)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2474', '1903', 'en', 'Мобильный комплект звукоусиления (600 Вт)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2475', '1904', 'en', 'Комплект звукоусиления (1 кВт)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2476', '1905', 'en', 'Комплект звукоусиления (2 кВт)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2477', '1906', 'en', 'Проводной микрофон');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2478', '1907', 'en', 'Радиомикрофон');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2479', '1908', 'en', 'Радиофицированная трибуна');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2480', '1909', 'en', 'Техническое сопровождение (1 чел)');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2481', '1910', 'en', 'Комплект участника');
		";
	}

}