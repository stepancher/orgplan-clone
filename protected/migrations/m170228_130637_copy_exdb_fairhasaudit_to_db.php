<?php

class m170228_130637_copy_exdb_fairhasaudit_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_fairhasaudit_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_fairhasaudit_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE au_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT f.id, au.id FROM {$expodata}.{{exdbfairhasaudit}} efhau
                                            LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.id = efhau.fairId
                                            LEFT JOIN {$expodata}.{{exdbaudit}} eau ON eau.id = efhau.auditId
                                            LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND ef.exdbCrc = f.exdbCrc) OR ef.exdbCrc IS NULL)
                                            LEFT JOIN {$db}.{{audit}} au ON eau.exdbId = au.exdbId
                                            LEFT JOIN {$db}.{{fairhasaudit}} fhau ON fhau.fairId = f.id AND fhau.auditId = au.id
                                            WHERE fhau.id IS NULL ORDER BY f.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, au_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fairhasaudit}} (`fairId`,`auditId`) VALUES (f_id, au_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_fairhasaudit_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}