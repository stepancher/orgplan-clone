<?php

class m160326_092534_restructuring_proposal_tables extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{attributemulty}};
			DROP TABLE IF EXISTS {{attributeclassdependency}};
			DROP TABLE IF EXISTS {{attributeclassproperty}};
			DROP TABLE IF EXISTS {{attributeenumproperty}};
			DROP TABLE IF EXISTS {{attribute}};
			DROP TABLE IF EXISTS {{proposalelement}};
			DROP TABLE IF EXISTS {{attributemodel}};
			DROP TABLE IF EXISTS {{attributeenum}};
			DROP TABLE IF EXISTS {{attributeclass}};
			DROP TABLE IF EXISTS {{proposalelementclass}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function getCreateTable(){
		return file_get_contents(Yii::getPathOfAlias('app.data').'/proposal_dump_29_03_2016__09_18.sql');
	}

}

