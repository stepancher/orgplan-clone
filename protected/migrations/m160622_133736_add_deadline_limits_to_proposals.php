<?php

class m160622_133736_add_deadline_limits_to_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{16:2, 46:1.5}' WHERE `id`='47';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 43:1.5}' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 43:1.5}' WHERE `id`='65';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 43:1.5}' WHERE `id`='641';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 43:1.5}' WHERE `id`='684';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:0, 71:1, 22:2}' WHERE `id`='687';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:0, 71:1, 22:2}' WHERE `id`='688';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{71:1.5, 22:2}' WHERE `id`='689';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 50:1.5, 15:2}' WHERE `id`='921';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 51:2}' WHERE `id`='957';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 51:2}' WHERE `id`='978';

			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 42:1.5}' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 42:1.5}' WHERE `id`='641';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 42:1.5}' WHERE `id`='684';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:0, 70:1, 21:2}' WHERE `id`='687';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:0, 70:1, 21:2}' WHERE `id`='688';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{70:1.5, 21:2}' WHERE `id`='689';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:1.5, 14:2}' WHERE `id`='921';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 50:2}' WHERE `id`='957';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 50:2}' WHERE `id`='978';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 45:2}' WHERE `id`='979';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 45:2}' WHERE `id`='980';


			UPDATE {{attributeclassdependency}} SET `functionArgs`='{16:2, 44:1.5}' WHERE `id`='47';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 44:1.5}' WHERE `id`='47';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{15:2, 44:1.5}' WHERE `id`='47';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{21:2}' WHERE `id`='641';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{20:2}' WHERE `id`='641';

			UPDATE {{attributeclassdependency}} SET `functionArgs`='{21:2, 51:1.5}' WHERE `id`='684';

			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:0, 69:1, 21:2}' WHERE `id`='687';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:0, 69:1, 21:2}' WHERE `id`='688';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{69:1.5, 20:2}' WHERE `id`='689';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:2}' WHERE `id`='957';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:2}' WHERE `id`='978';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:2}' WHERE `id`='979';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 49:2}' WHERE `id`='980';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{72:0, 192:10, 258:15, 1000:20}' WHERE `id`='31';

			UPDATE {{attributeclassdependency}} SET `functionArgs`='{14:2, 42:1.5}' WHERE `id`='65';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{13:2, 42:1.5}' WHERE `id`='65';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{1000:1, 48:1.5, 13:2}' WHERE `id`='921';


		";
	}
}