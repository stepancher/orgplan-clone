<?php

class m160905_140126_create_help_module_routes extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{route}} SET `url`='/ru/help/default/quickGuide' WHERE `id`='93580';
            UPDATE {{route}} SET `url`='/ru/help/default/questionAnswer' WHERE `id`='93587';
            UPDATE {{route}} SET `url`='/ru/help/default/confidentiality' WHERE `id`='93589';
            UPDATE {{route}} SET `url`='/ru/help/default/termsOfUse' WHERE `id`='93598';
            UPDATE {{route}} SET `url`='/en/help/default/quickGuide' WHERE `id`='93615';
            UPDATE {{route}} SET `url`='/en/help/default/questionAnswer' WHERE `id`='93622';
            UPDATE {{route}} SET `url`='/en/help/default/confidentiality' WHERE `id`='93624';
            UPDATE {{route}} SET `url`='/en/help/default/termsOfUse' WHERE `id`='93632';
            UPDATE {{route}} SET `url`='/de/help/default/quickGuide' WHERE `id`='93642';
            UPDATE {{route}} SET `url`='/de/help/default/questionAnswer' WHERE `id`='93646';
            UPDATE {{route}} SET `url`='/de/help/default/confidentiality' WHERE `id`='93647';
            UPDATE {{route}} SET `url`='/de/help/default/termsOfUse' WHERE `id`='93650';
            UPDATE {{route}} SET `url`='/ru/help/questionAnswer', `route`='help/default/questionAnswer' WHERE `id`='93587';
            UPDATE {{route}} SET `url`='/ru/help/quickGuide', `route`='help/default/quickGuide' WHERE `id`='93580';
            UPDATE {{route}} SET `url`='/ru/help/confidentiality', `route`='help/default/confidentiality' WHERE `id`='93589';
            UPDATE {{route}} SET `url`='/ru/help/termsOfUse', `route`='help/default/termsOfUse' WHERE `id`='93598';
            UPDATE {{route}} SET `url`='/en/help/quickGuide', `route`='help/default/quickGuide' WHERE `id`='93615';
            UPDATE {{route}} SET `url`='/en/help/questionAnswer', `route`='help/default/questionAnswer' WHERE `id`='93622';
            UPDATE {{route}} SET `url`='/en/help/confidentiality', `route`='help/default/confidentiality' WHERE `id`='93624';
            UPDATE {{route}} SET `url`='/en/help/termsOfUse', `route`='help/default/termsOfUse' WHERE `id`='93632';
            UPDATE {{route}} SET `url`='/de/help/quickGuide', `route`='help/default/quickGuide' WHERE `id`='93642';
            UPDATE {{route}} SET `url`='/de/help/questionAnswer', `route`='help/default/questionAnswer' WHERE `id`='93646';
            UPDATE {{route}} SET `url`='/de/help/confidentiality', `route`='help/default/confidentiality' WHERE `id`='93647';
            UPDATE {{route}} SET `url`='/de/help/termsOfUse', `route`='help/default/termsOfUse' WHERE `id`='93650';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}