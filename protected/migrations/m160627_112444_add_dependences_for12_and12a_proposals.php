<?php

class m160627_112444_add_dependences_for12_and12a_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":22, "save":1}\', 	"calendar/gantt/add/id/137");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":22, "send":1}\', 	"calendar/gantt/add/id/137");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", 	\'{"fairId":184, "ecId":22}\', 				"calendar/gantt/add/id/137");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":22, "send":1}\', 	"calendar/gantt/complete/id/137");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", 	\'{"fairId":184, "ecId":22}\', 				"calendar/gantt/complete/id/137");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", "{\"ecId\":22}", "calendar/gantt/reject/id/137/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", "{\"ecId\":22}", "calendar/gantt/add/id/137");

			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":21, "save":1}\', 	"calendar/gantt/add/id/138");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":21, "send":1}\', 	"calendar/gantt/add/id/138");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", 	\'{"fairId":184, "ecId":21}\', 				"calendar/gantt/add/id/138");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/create", \'{"fairId":184, "ecId":21, "send":1}\', 	"calendar/gantt/complete/id/138");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/send", 	\'{"fairId":184, "ecId":21}\', 				"calendar/gantt/complete/id/138");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", "{\"ecId\":21}", "calendar/gantt/reject/id/138/userId/{:userId}");
			INSERT INTO {{dependences}} (action, params, run) VALUES("proposal/proposal/reject", "{\"ecId\":21}", "calendar/gantt/add/id/138");

			DELETE FROM {{dependences}} WHERE `id`="222";
			DELETE FROM {{dependences}} WHERE `id`="223";
		';
	}
}