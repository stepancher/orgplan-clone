<?php

class m170220_105555_add_exdb_audithasassociation extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbaudithasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `auditId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('1', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('3', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('5', '27');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('5', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('6', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('8', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('12', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('13', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('14', '8');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('14', '25');
            INSERT INTO {$expodata}.{{exdbaudithasassociation}} (`auditId`,`associationId`) VALUES ('16', '25');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}