<?php

class m161028_135202_add_trdoc_for_agrosalon_video extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {

        list($peace1, $peace2, $dbProposalProtoplan) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('59', 'en', 'Видеоотчет АГРОСАЛОН', 'https://www.youtube.com/embed/fzK60GKRHZ8');
            
            UPDATE {$dbProposalProtoplan}.tbl_proposalelementclass SET `deadline`='2016-12-04 03:00:00' WHERE `id`='5';
            UPDATE {$dbProposalProtoplan}.tbl_proposalelementclass SET `deadline`='2016-12-04 03:00:00' WHERE `id`='21';
            UPDATE {$dbProposalProtoplan}.tbl_proposalelementclass SET `deadline`='2016-12-04 03:00:00' WHERE `id`='22';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}