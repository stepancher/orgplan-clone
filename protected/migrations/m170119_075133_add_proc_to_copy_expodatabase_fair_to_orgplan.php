<?php

class m170119_075133_add_proc_to_copy_expodatabase_fair_to_orgplan extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `copy_expodatabase_fair_to_orgplan`;
        
            CREATE PROCEDURE `copy_expodatabase_fair_to_orgplan`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_fair_id VARCHAR(255) DEFAULT '';
                DECLARE exdb_fair_name_en VARCHAR(255) DEFAULT '';
                DECLARE exdb_fair_name_de VARCHAR(255) DEFAULT '';
                DECLARE orgdb_fair_id INT;
                DECLARE expodatabase CURSOR FOR SELECT DISTINCT id, name_en, name_de FROM `expodatabase`.`tbl_fair`;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN expodatabase;
                
                copy_loop: LOOP
                
                    SET orgdb_fair_id := 0;
                
                    FETCH expodatabase INTO 
                        exdb_fair_id, 
                        exdb_fair_name_en, 
                        exdb_fair_name_de;
                    
                    IF done THEN 
                        LEAVE copy_loop; 
                    END IF;
                    
                        SELECT trParentId INTO orgdb_fair_id FROM `orgplan`.`tbl_trfair` WHERE (`langId` = 'en' AND `name` = exdb_fair_name_en) LIMIT 1;
                    
                        IF orgdb_fair_id = 0 THEN
                            
                            START TRANSACTION;
                                INSERT INTO `orgplan`.`tbl_fair` (`expodatabaseId`) VALUES (exdb_fair_id);
                            COMMIT;
                            
                            SET @new_fair_id := LAST_INSERT_ID();
                            
                            START TRANSACTION;
                                INSERT INTO `orgplan`.`tbl_trfair` (`trParentId`,`langId`,`name`) VALUES (@new_fair_id, 'en', exdb_fair_name_en);
                                INSERT INTO `orgplan`.`tbl_trfair` (`trParentId`,`langId`,`name`) VALUES (@new_fair_id, 'de', exdb_fair_name_de);
                            COMMIT;
                        END IF;
                    
                END LOOP;
                
                CLOSE expodatabase;
            END;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}