<?php

class m170221_070224_copy_exdb_association_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DELETE FROM {$db}.{{association}} WHERE `exdbId` IS NOT NULL;
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_association_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_association_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE a_id INT DEFAULT 0;
                DECLARE a_name TEXT DEFAULT '';
                DECLARE a_contact TEXT DEFAULT '';
                DECLARE a_contact_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ea.exdbId, ea.name, ea.exdbContacts, ea.exdbContactsRaw FROM {$expodata}.{{exdbassociation}} ea
                                        LEFT JOIN {$db}.{{association}} a ON ea.exdbId = a.exdbId
                                        WHERE a.id IS NULL ORDER BY ea.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO a_id, a_name, a_contact, a_contact_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{association}} (`exdbId`,`name`,`exdbContacts`,`exdbContactsRaw`) VALUES (a_id, a_name, a_contact, a_contact_raw);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_association_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}