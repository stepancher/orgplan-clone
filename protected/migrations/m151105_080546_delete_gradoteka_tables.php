<?php

class m151105_080546_delete_gradoteka_tables extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		echo 'doesn support migrate down, but return true';

		return true;
	}

	public function getCreateTable()
	{
		return '
			DROP TABLE IF EXISTS {{gvalue}};
			DROP TABLE IF EXISTS {{gobjectshasgstattypes}};
			DROP TABLE IF EXISTS {{gobjects}};
			DROP TABLE IF EXISTS {{gstattypes}};
			';
	}
}