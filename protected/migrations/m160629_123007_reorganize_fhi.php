<?php

class m160629_123007_reorganize_fhi extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return TRUE;
		
		
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			UPDATE {{route}} SET `route`='search/industrySummaryByFairs', `params`='[]' WHERE `id`='93590';
			UPDATE {{route}} SET `route`='search/industrySummaryByFairs', `params`='[]' WHERE `id`='93625';
			
			DELETE FROM {{fairhasindustry}} 
			WHERE
				id IN (
				'1196', '1197', '1198', '1199', '1200', '1202', '1203',
				'1204', '1205', '1206', '1207', '1208', '1209', '1210',
				'1211',	'1212',	'1213',	'1214',	'1215',	'1216',	'1217',
				'1218',	'1219',	'1220',	'1221',	'1222',	'1223',	'1224',
				'1225',	'1226',	'1227',	'1228',	'1229',	'1230',	'1231',
				'1232',	'1233',	'1234',	'1235',	'1236',	'1237',	'1240'
			);
			
			DELETE FROM {{fairhasindustry}}
			WHERE
				id IN (
				'224',  '476',  '477',  '537',	'576',  '577',  '597',  '598',
				'599',  '600',  '601',  '602',	'603',  '604',  '605',  '606',
				'759',  '760',  '844',  '913',  '1014', '1019', '1238', '1239',
				'1321', '1367', '1391', '1429', '1496', '2303', '2329', '2408',
				'2577', '3410', '4182', '4183', '4412', '4675', '4691', '4693',
				'6721', '7322', '7323', '8243', '8244', '8739', '8740', '8920',
				'8921', '9180', '9181', '9216', '9665', '9666', '10400','10953',
				'11076','11078','11176','11177','11780','12255','12672',
				'12673','12674','12676','12678','12679','12680','12681',
				'12688','12692','12705','12756','13264','13265','13266'
			);
			
			ALTER TABLE {{fairhasindustry}} 
			ADD INDEX `fhi_fairid` (`fairId` ASC),
			ADD INDEX `fhi_industryid` (`industryId` ASC);
			
			
			ALTER TABLE {{fairhasindustry}} 
			ADD CONSTRAINT `fk_fhi_fairid`
			  FOREIGN KEY (`fairId`)
			  REFERENCES {{fair}} (`id`)
			  ON DELETE CASCADE
			  ON UPDATE CASCADE,
			ADD CONSTRAINT `fk_fhi_industryid`
			  FOREIGN KEY (`industryId`)
			  REFERENCES {{industry}} (`id`)
			  ON DELETE CASCADE
			  ON UPDATE CASCADE;
		";
	}

	public function downSql()
	{
		return "";
	}
}