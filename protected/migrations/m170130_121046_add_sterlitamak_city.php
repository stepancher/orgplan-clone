<?php

class m170130_121046_add_sterlitamak_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('46', 'sterlitamak');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('273', 'ru', 'Стерлитамак');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('273', 'en', 'Sterlitamak');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('273', 'de', 'Sterlitamak');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}