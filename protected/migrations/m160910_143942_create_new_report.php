<?php

class m160910_143942_create_new_report extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposal) = explode('=', Yii::app()->dbProposal->connectionString);

        return "
            INSERT INTO {$dbProposal}.tbl_report (`reportId`, `reportName`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('5', 'report-5', 'Сдача заявок №2', '[{\"field\": \"standNumber\",\"displayName\": \"\u041a\u043e\u043b - \u0432\u043e\",\"sortable\": true,\"aggregate\": true,\"sortingType\": \"number\"}, {\"field\": \"companyName\",\"displayName\": \"\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c RUB\",\"sortable\": true,\"aggregate\": true,\"sortingType\": \"number\"}, {\"field\": \"standSquare\",\"displayName\": \"\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c USD\",\"sortable\": true,\"aggregate\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"\u0412\u0430\u043b\u044e\u0442\u0430\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"\u0412\u0430\u043b\u044e\u0442\u0430\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"\u0412\u0430\u043b\u044e\u0442\u0430\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"\u0412\u0430\u043b\u044e\u0442\u0430\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"\u0412\u0430\u043b\u044e\u0442\u0430\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"\u0412\u0430\u043b\u044e\u0442\u0430\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]', '[{\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"buildingType\":\"тип застройки\",\"buildingSquare\":\"площадь застройки\",\"letters\":\"Фризовая надпись\",\"logo\":\"Размещение логотипа\",\"thirdProposal\":\"ЗАявка №3\",\"fourthProposal\":\"Заявка №4\"}]');
            UPDATE {$dbProposal}.tbl_report SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"standNumber\",\"sortable\": true,\"aggregate\": true,\"sortingType\": \"number\"}, {\"field\": \"companyName\",\"displayName\": \"companyName\",\"sortable\": true,\"aggregate\": true,\"sortingType\": \"number\"}, {\"field\": \"standSquare\",\"displayName\": \"standSquare\",\"sortable\": true,\"aggregate\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"buildingType\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"buildingSquare\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"letters\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"logo\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"thirdProposal\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"fourthProposal\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"standNumber\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"companyName\",\"displayName\": \"companyName\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"standSquare\",\"displayName\": \"standSquare\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"buildingType\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"buildingSquare\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"letters\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"logo\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"thirdProposal\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"fourthProposal\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Тип застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Площадь застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Фризовая надпись\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Размещение логотипа\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Заявка №3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Заявка №4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `exportColumns`='[{\"rowLabel\":\"Экспонент\",\"standNumber\":\"Номер стенда\",\"companyName\":\"Название компании\",\"standSquare\":\"площадь стенда\",\"buildingType\":\"тип застройки\",\"buildingSquare\":\"площадь застройки\",\"letters\":\"Фризовая надпись\",\"logo\":\"Размещение логотипа\",\"thirdProposal\":\"ЗАявка №3\",\"fourthProposal\":\"Заявка №4\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `columnDefs`='[{\"field\": \"rowLabel\",\"displayName\": \"Экспонент\",\"sortable\": true,\"sortingType\": \"string\"},{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Тип застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Площадь застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Фризовая надпись\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Размещение логотипа\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Заявка №3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Заявка №4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Тип застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Площадь застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Фризовая надпись\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Размещение логотипа\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Заявка №3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Заявка №4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `columnDefs`='[{\"field\": \"standNumber\",\"displayName\": \"Номер стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"companyName\",\"displayName\": \"Название компании\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"standSquare\",\"displayName\": \"Площадь стенда\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"number\"}, {\"field\": \"buildingType\",\"displayName\": \"Тип застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"buildingSquare\",\"displayName\": \"Площадь застройки\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"letters\",\"displayName\": \"Фризовая надпись\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"logo\",\"displayName\": \"Размещение логотипа\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"thirdProposal\",\"displayName\": \"Заявка №3\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}, {\"field\": \"fourthProposal\",\"displayName\": \"Заявка №4\",\"sortable\": true,\"filterable\": true,\"sortingType\": \"string\"}]' WHERE `id`='5';
            UPDATE {$dbProposal}.tbl_report SET `reportLabel`='Стандартный стенд и фризовая надпись' WHERE `id`='5';
        ";
    }

    public function downSql()
    {
        return TRUE;
    }
}