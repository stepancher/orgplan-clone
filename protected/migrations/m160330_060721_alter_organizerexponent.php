<?php

class m160330_060721_alter_organizerexponent extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return '
			ALTER TABLE {{organizerexponent}}
			DROP COLUMN `number`;
		';
	}

	private function downSql() {

		return '
			ALTER TABLE {{organizerexponent}}
			ADD COLUMN `number` VARCHAR(45) NOT NULL DEFAULT 0 AFTER `name`;
		';
	}
}