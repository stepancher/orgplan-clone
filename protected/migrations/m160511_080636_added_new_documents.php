<?php

class m160511_080636_added_new_documents extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		return true;
	}

	private function upSql() {

		return "
			UPDATE {{documents}} SET `value`='/static/documents/fairs/184/stand_plan.pdf' WHERE `id`='7';

			INSERT INTO {{documents}} (`name`, `type`, `value`, `active`, `fairId`) VALUES
			('Requirements and Rules', '2', '/static/documents/fairs/184/requirements_and_rules.pdf', '1', '184'),
			('Инструкция для Экспонента', '2', '/static/documents/fairs/184/instruction_for_exponent.pdf', '1', '184');
		";
	}
}