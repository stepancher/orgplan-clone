<?php

class m170221_062042_add_association_contact_field_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            ALTER TABLE {$db}.{{association}}
            ADD COLUMN `exdbContacts` TEXT NULL DEFAULT NULL AFTER `exdbId`,
            ADD COLUMN `exdbContactsRaw` TEXT NULL DEFAULT NULL AFTER `exdbContacts`;
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbassociation}} ;
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbContacts` TEXT NULL DEFAULT NULL,
              `exdbContactsRaw` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}