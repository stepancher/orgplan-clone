<?php

class m160428_233128_add_de_reg_and_auth_routes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{route}} (`id`, `title`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Orgplan | Авторизация', '/de/login', 'site/auth', '[]', 'Авторизация', 'de', NULL);
			INSERT INTO {{route}} (`id`, `title`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Orgplan | Регистрация', '/de/registration', 'site/reg', '[]', 'Регистрация', 'de', NULL);
		";
	}
}