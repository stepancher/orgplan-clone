<?php

class m160905_082928_new_routes_for_login extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{route}} (`title`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Protoplan | Авторизация', '/ru/login/agrosalon', 'site/auth', '{\"fairId\":\"184\"}', 'Авторизация', 'ru');
            INSERT INTO {{route}} (`title`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Protoplan | Авторизация', '/de/login/agrosalon', 'site/auth', '{\"fairId\":\"184\"}', 'Авторизация', 'de');
            INSERT INTO {{route}} (`title`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Protoplan I Authorisation', '/en/login/agrosalon', 'site/auth', '{\"fairId\":\"184\"}', 'Authorisation', 'en');
            INSERT INTO {{route}} (`title`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Protoplan | Авторизация', '/ru/login/agrorus', 'site/auth', '{\"fairId\":\"11699\"}', 'Авторизация', 'ru');
            INSERT INTO {{route}} (`title`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Protoplan | Авторизация', '/de/login/agrorus', 'site/auth', '{\"fairId\":\"11699\"}', 'Авторизация', 'de');
            INSERT INTO {{route}} (`title`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Protoplan I Authorisation', '/en/login/agrorus', 'site/auth', '{\"fairId\":\"11699\"}', 'Authorisation', 'en');
            DELETE FROM {{seo}} WHERE url LIKE '%login%';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}