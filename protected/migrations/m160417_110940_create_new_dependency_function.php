<?php

class m160417_110940_create_new_dependency_function extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclassdependency}} SET `function`='getFromVariants', `functionArgs`='{60:223,120:210,240:198}' WHERE `id`='46';
			UPDATE {{attributeclassdependency}} SET `function`='getFromVariants', `functionArgs`='{60:217,120:204,240:193}' WHERE `id`='45';
			UPDATE {{attributeclassdependency}} SET `function`='getFromVariants', `functionArgs`='{60:211,120:199,240:187}' WHERE `id`='44';
			UPDATE {{attributeclassdependency}} SET `function`='getFromVariants', `functionArgs`='{60:198,120:187,240:176}' WHERE `id`='43';

			UPDATE {{attributeclassdependency}} SET `functionArgs`='{60:60, 120:120, 240:240}' WHERE `id`='31';

			UPDATE {{attributeclassdependency}} SET `functionArgs`='{60:10, 120:15, 240:20}' WHERE `id`='31';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{10:198,15:187,20:176}' WHERE `id`='43';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{10:211,15:199,20:187}' WHERE `id`='44';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{10:217,15:204,20:193}' WHERE `id`='45';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{10:223,15:210,20:198}' WHERE `id`='46';

			UPDATE {{attributeclassproperty}} SET `class`='price' WHERE `id`='40';
	    ";
	}
}