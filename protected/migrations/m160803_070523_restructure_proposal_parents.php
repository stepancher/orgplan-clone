<?php

class m160803_070523_restructure_proposal_parents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			update {{attributeclass}} set parent = 2465 where id in (2466,2470,2474,2478);
			update {{attributeclass}} set parent = 2452 where id in (2453,2456,2459,2462);
			update {{attributeclass}} set parent = 2407 where id in (2408,2413,2418,2423);
			update {{attributeclass}} set parent = 2429 where id in (2430,2435,2440,2445);
			
			update {{attributeclass}} set parent = 77 where id in (141, 148);
			update {{attributeclass}} set parent = 78 where id in (149, 150);
			update {{attributeclass}} set parent = 79 where id in (151, 152, 153);
			
			update {{attributeclass}} set super = 1044 where super = 577;
			update {{attributeclass}} set parent = 1044 where id = 577;
			delete from {{attributemodel}} where attributeClass = 577;
			
			update {{attributeclass}} set super = 1044 where super = 585;
			update {{attributeclass}} set parent = 1044 where id = 585;
			delete from {{attributemodel}} where attributeClass = 585;
			
			update {{attributeclass}} set super = 1044 where super = 594;
			update {{attributeclass}} set parent = 1044 where id = 594;
			delete from {{attributemodel}} where attributeClass = 594;
			
			update {{attributeclass}} set super = 1045 where super = 616;
			update {{attributeclass}} set parent = 1045 where id = 616;
			delete from {{attributemodel}} where attributeClass = 616;
			
			update {{attributeclass}} set super = 1045 where super = 619;
			update {{attributeclass}} set parent = 1045 where id = 619;
			delete from {{attributemodel}} where attributeClass = 619;
			
			update {{attributeclass}} set super = 1045 where super = 628;
			update {{attributeclass}} set parent = 1045 where id = 628;
			delete from {{attributemodel}} where attributeClass = 628;
			
			update {{attributeclass}} set super = 1045 where super = 643;
			update {{attributeclass}} set parent = 1045 where id = 643;
			delete from {{attributemodel}} where attributeClass = 643;
			
			update {{attributeclass}} set super = 1045 where super = 654;
			update {{attributeclass}} set parent = 1045 where id = 654;
			delete from {{attributemodel}} where attributeClass = 654;
		";
	}
}