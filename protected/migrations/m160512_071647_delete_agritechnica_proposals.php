<?php

class m160512_071647_delete_agritechnica_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{proposalelementclass}} WHERE `id`='11';
			DELETE FROM {{proposalelementclass}} WHERE `id`='12';
			DELETE FROM {{proposalelementclass}} WHERE `id`='13';
			DELETE FROM {{proposalelementclass}} WHERE `id`='14';
			DELETE FROM {{proposalelementclass}} WHERE `id`='15';
			DELETE FROM {{proposalelementclass}} WHERE `id`='16';
			DELETE FROM {{proposalelementclass}} WHERE `id`='17';
	    ";
	}
}