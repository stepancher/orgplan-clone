<?php

class m160513_065153_dates_for_datepickers extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('672', '672', 'getDates', '0');
		UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='754';
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('672', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('672', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES (NULL, '671', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES (NULL, '671', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('681', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('681', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('680', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('680', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('690', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('690', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('689', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('689', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('699', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('699', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('698', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('698', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('708', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('707', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('717', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('716', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('726', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('725', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('735', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('735', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('734', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('734', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('744', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('744', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('743', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('743', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('753', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('753', 'endDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('752', 'startDate');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('752', 'endDate');
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '671', '671', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '681', '681', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('680', '680', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '690', '690', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('689', '689', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('699', '699', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('698', '698', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('708', '708', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('707', '707', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('717', '717', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('716', '716', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('726', '726', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('725', '725', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('735', '735', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('734', '734', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('744', '744', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('743', '743', 'getDemountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('753', '753', 'getMountingDates', '0');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('752', '752', 'getDemountingDates', '0');

	    ";
	}
}