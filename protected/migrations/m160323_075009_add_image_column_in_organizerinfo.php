<?php

class m160323_075009_add_image_column_in_organizerinfo extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = '
			ALTER TABLE {{organizerinfo}}
			DROP COLUMN `image`;
			ALTER TABLE {{organizercontact}}
			DROP COLUMN `image`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{organizerinfo}}
			ADD COLUMN `image` VARCHAR(45) NULL AFTER `signRulesPosition`;
			ALTER TABLE {{organizercontact}}
			ADD COLUMN `image` VARCHAR(45) NULL;
		";
	}
}