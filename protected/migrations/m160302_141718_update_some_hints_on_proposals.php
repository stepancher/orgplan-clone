<?php

class m160302_141718_update_some_hints_on_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			UPDATE {{attributeclass}} SET `label`=\'Подключение потребителей, расположенных не далее 10 метров от точки подключения включительно, производится шлангами, предоставляемыми без дополнительной оплаты. Если длина подключения превышает 10 метров, шланги необходимой длины предоставляются в аренду.\' WHERE `id`=\'212\';
			UPDATE {{attributeclass}} SET `class`=\'hint_danger\' WHERE `id`=\'213\';
			UPDATE {{attributeclass}} SET `class`=\'hint_danger\' WHERE `id`=\'185\';
			UPDATE {{attributeclass}} SET `class`=\'hint_danger\' WHERE `id`=\'237\';

		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			UPDATE {{attributeclass}} SET `label`=\'<div class=\"margin-top-20\">Подключение потребителей, расположенных не далее 10 метров от точки подключения включительно, производится шлангами, предоставляемыми без дополнительной оплаты. Если длина подключения превышает 10 метров, шланги необходимой длины предоставляются в аренду.</div>\' WHERE `id`=\'212\';
			UPDATE {{attributeclass}} SET `class`=\'hint_danger_margin_top_20\' WHERE `id`=\'213\';
			UPDATE {{attributeclass}} SET `class`=\'hint_danger_margin_top_20\' WHERE `id`=\'185\';
			UPDATE {{attributeclass}} SET `class`=\'hint_danger_margin_top_20\' WHERE `id`=\'237\';
		';
	}
}