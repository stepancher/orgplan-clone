<?php

class m160707_103021_add_de_routes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return " 
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог отраслей | Protoplan', 'В каталоге представлены 9 отраслей промышленности России. В каждой отрасли содержится краткий обзор с основными показателями развития.', '/de/industries', 'search/index', '{\"sector\":\"industry\"}', 'Каталог отраслей', 'de', '2016-06-14 22:29:43');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Регионы России', 'В каталоге представлены регионы России, в каждом опубликованы экономические показатели развития, KPI отраслей и рейтинг инвестиционной привлекательности', '/de/regions', 'search/index', '{\"sector\":\"region\"}', 'Регионы России', 'de', '2016-06-01 09:53:55');
			DELETE FROM {{route}} WHERE `id`='93629';
			DELETE FROM {{route}} WHERE `id`='93595';
			DELETE FROM {{route}} WHERE `id`='93613';
			DELETE FROM {{route}} WHERE `id`='93578';
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог цен на товары и услуги | Protoplan', 'Каталог цен по городам России', '/de/prices', 'search/index', '{\"sector\":\"price\"}', 'Каталог цен на товары и услуги | Protoplan', 'de', '2016-06-14 22:29:43');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Проекты выставочных стендов', 'В каталоге представлены готовые макеты выставочных стендов. Воспользовавшись фильтром, можно найти макет стенда по площади и типу планировки.', '/de/stands', 'search/index', '{\"sector\":\"stand\"}', 'Каталог стендов', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Краткое руководство | Protoplan', 'С помощью поиска PROTOPLAN вы можете найти любую выставку на территории России. Архив с января 2015 года.', '/de/help/quickGuide', 'help/quickGuide', '[]', 'Краткое руководство | Protoplan', 'de', '2016-06-14 22:29:43');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог регионов по алфавиту', 'Каталог регионов по алфавиту', '/de/regions/alphabet', 'search/index', '{\"by\":\"byAlphabet\",\"sector\":\"region\"}', 'Каталог регионов по алфавиту', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог отраслей по алфавиту', 'Каталог отраслей по алфавиту', '/de/industries/alphabet', 'search/index', '{\"by\":\"byAlphabet\",\"sector\":\"industry\"}', 'Каталог отраслей по алфавиту', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог Выставок по дате', 'Каталог Выставок по дате', '/de/fairs/date', 'search/index', '{\"by\":\"byDate\",\"sector\":\"fair\"}', 'Каталог Выставок по дате', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Вопрос-ответ | Protoplan', 'Могу ли я доверять статистике PROTOPLAN? Все статистические данные взяты из официальных источников, ссылка на источник публикуется на странице, вы можете проверить.', '/de/help/questionAnswer', 'help/questionAnswer', '[]', 'Вопрос-ответ | Protoplan', 'de', '2016-06-14 22:29:43');
			DELETE FROM {{route}} WHERE `id`='93623';
			DELETE FROM {{route}} WHERE `id`='93588';
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Политика конфиденциальности | Protoplan', 'Настоящий документ «Политика конфиденциальности» (далее — по тексту — «Политика») представляет собой правила использования персональной информации Пользователя.', '/de/help/confidentiality', 'help/confidentiality', '[]', 'Политика конфиденциальности | Protoplan', 'de', '2016-06-14 22:29:43');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Блог | Protoplan', 'Как участвовать в выставках, как пригласить клиентов, планирование работы персонала,  идеи для выставочных стендов, другие темы от экспертов блога protoplan.pro', '/de/blog', 'blog/index', '[]', 'Блог | Protoplan', 'de', '2016-06-14 22:29:43');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог площадок по категориям', 'На странице представлен каталог площадок Российской Федерации по категориям.', '/de/venues/category', 'search/index', '{\"by\":\"byCategory\",\"sector\":\"exhibitionComplex\"}', 'Каталог площадок по категориям', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Пользовательское соглашение | Protoplan', 'Настоящий документ «Пользовательское соглашение» представляет собой предложение ООО «Кубэкс Рус» (далее — «Правообладатель») заключить договор на изложенных ниже условиях.', '/de/help/termsOfUse', 'help/termsOfUse', '[]', 'Пользовательское соглашение | Protoplan', 'de', '2016-06-14 22:29:43');
			INSERT INTO {{route}} (`id`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, '/de/logout', 'site/logout', '[]', '', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`id`, `title`, `description`, `url`, `route`, `params`, `header`, `lang`, `lastmode`) VALUES (NULL, 'Каталог событий', 'В каталоге представлены анонсы выставок и отчеты прошедших мероприятий в России, СНГ, мире. При помощи фильтра можно найти выставку по стране, городу, отрасли.', '/de/fairs', 'search/index', '{\"sector\":\"fair\"}', 'Выставки Российской Федерации', 'de', '2016-06-01 09:53:55');
			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`, `lang`) VALUES ('Площадки России, СНГ, мира', 'В каталоге представлены площадки России, СНГ, мира. Воспользовавшись фильтром, можно найти площадку по региону и городу', '/de/venues', 'search/index', '{\"sector\":\"exhibitionComplex\"}', 'Площадки России, СНГ, мира', 'de');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}