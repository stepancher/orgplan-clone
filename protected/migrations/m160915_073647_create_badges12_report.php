<?php

class m160915_073647_create_badges12_report extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalName)  = explode('=', Yii::app()->dbProposal->connectionString);

        return "
            INSERT INTO {$dbProposalName}.tbl_report (`id`, `reportId`, `reportName`, `reportLabel`, `columnDefs`, `dateCreated`, `exportColumns`) VALUES ('7', '7', 'report-7', 'Бэйджы. Заявка №12', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"companyRu\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"}]', '2016-09-15 10:28:23', '[{\"firstName\":\"Имя\",\"lastName\":\"Фамилия\",\"countryRu\":\"Страна\",\"companyRu\":\"Компания\"}]');
            
            INSERT INTO {$dbProposalName}.tbl_report (`id`, `reportId`, `reportName`, `reportLabel`, `columnDefs`, `dateCreated`, `exportColumns`) VALUES ('8', '8', 'report-8', 'Бэйджы. Заявка №12a', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"}]', '2016-09-15 10:43:13', '[{\"firstName\":\"Имя\",\"lastName\":\"Фамилия\",\"surname\":\"Отчество\",\"countryRu\":\"Страна\",\"cityRu\":\"Город\",\"companyRu\":\"Компания\"},\"post\":\"Должность\"}]');

            UPDATE {$dbProposalName}.tbl_report SET `columnDefs`='[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"regionRu\",\"displayName\":\"Регион\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"street\",\"displayName\":\"Индекс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"house\",\"displayName\":\"Дом\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"phone\",\"displayName\":\"Телефон\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},{\"field\":\"fax\",\"displayName\":\"Факс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"}]', `exportColumns`='[{\"firstName\":\"Имя\",\"lastName\":\"Фамилия\",\"surname\":\"Отчество\",\"countryRu\":\"Страна\",\"regionRu\":\"Регион\",\"cityRu\":\"Город\",\"companyRu\":\"Компания\",\"post\":\"Должность\",\"index\":\"Индекс\",\"street\":\"Улица\",\"house\":\"Дом\",\"phone\":\"Телефон\",\"fax\":\"Факс\"}]' WHERE `id`='8';
            UPDATE {$dbProposalName}.tbl_report SET `columnDefs`='[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"regionRu\",\"displayName\":\"Регион\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"street\",\"displayName\":\"Улица\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"house\",\"displayName\":\"Дом\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"index\",\"displayName\":\"Индекс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"phone\",\"displayName\":\"Телефон\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"},\n{\"field\":\"fax\",\"displayName\":\"Факс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\"}]' WHERE `id`='8';
            UPDATE {$dbProposalName}.tbl_report SET `exportColumns`='[{\n\"firstName\":\"Имя\",\n\"lastName\":\"Фамилия\",\n\"surname\":\"Отчество\",\n\"countryRu\":\"Страна\",\n\"regionRu\":\"Регион\",\n\"cityRu\":\"Город\",\n\"company\":\"Компания\",\n\"post\":\"Должность\",\n\"street\":\"Улица\",\n\"house\":\"Дом\",\n\"index\":\"Индекс\",\n\"phone\":\"Телефон\",\n\"fax\":\"Факс\"\n}]' WHERE `id`='8';
            UPDATE {$dbProposalName}.tbl_report SET `columnDefs`='[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\":true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"companyRu\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]' WHERE `id`='7';
            UPDATE {$dbProposalName}.tbl_report SET `columnDefs`='[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"regionRu\",\"displayName\":\"Регион\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"street\",\"displayName\":\"Улица\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"house\",\"displayName\":\"Дом\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"index\",\"displayName\":\"Индекс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"phone\",\"displayName\":\"Телефон\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"fax\",\"displayName\":\"Факс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]' WHERE `id`='8';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}