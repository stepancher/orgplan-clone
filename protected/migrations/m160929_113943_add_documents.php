<?php

class m160929_113943_add_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{documents}} WHERE `id`='38';
            DELETE FROM {{documents}} WHERE `id`='39';
            DELETE FROM {{documents}} WHERE `id`='40';
            DELETE FROM {{documents}} WHERE `id`='41';
            
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('38', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('39', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('40', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('41', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('42', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('43', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('44', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('45', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('46', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('47', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('48', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('49', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('50', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('51', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('52', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('53', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('54', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('55', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('56', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('57', '2', '1');
            INSERT INTO {{documents}} (`id`, `type`, `active`) VALUES ('58', '2', '1');
            
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('38', 'ru', 'Анкета посетителя стенда (шаблон)', '/static/documents/fairs/ancata_posetitelya_stend.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('39', 'ru', 'Инструктаж сотрудников (шаблон)', '/static/documents/fairs/instructazh sotrudnikov.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('40', 'ru', 'Меню кофе-брейка, фуршета (шаблон)', '/static/documents/fairs/menu cofe-breika.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('41', 'ru', 'Перечень продуктов на стенд (шаблон)', '/static/documents/fairs/perechen productov na stend.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('42', 'ru', 'Советы участнику для цели PR, ВЫСТРАИВАНИЕ БРЕНДА', '/static/documents/fairs/soveti uchastnike dlya celi pr, vystraivanie brenda.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('43', 'ru', 'Советы участнику для цели МАРКЕТИНГОВЫЕ ИССЛЕДОВАНИЯ', '/static/documents/fairs/soveti uchastnike dlya celi marketingovye issledovania.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('44', 'ru', 'Советы участнику для цели ОТНОШЕНИЕ С ПОТРЕБИТЕЛЯМИ', '/static/documents/fairs/soveti uchastnike dlya celi otnoshenie s potrebitelyami.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('45', 'ru', 'Советы участнику для цели ПОДДЕРЖКА КАНАЛОВ СБЫТА', '/static/documents/fairs/soveti uchastnike dlya celi podderzhka kanalov sbyta.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('46', 'ru', 'Советы участнику для цели ПРОДАЖА', '/static/documents/fairs/soveti uchastnike dlya celi prodazha.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('47', 'ru', 'Список экспонатов (шаблон)', '/static/documents/fairs/spisok eksponatov.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('48', 'ru', 'Форма обработки контактов на стенде (шаблон)', '/static/documents/fairs/forma obrabotki kontaktov na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('49', 'ru', 'Чек-лист демонстрации продукции на стенде (шаблон)', '/static/documents/fairs/check list demonstracii producta na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('50', 'ru', 'Чек-лист конкурса (шаблон)', '/static/documents/fairs/check list konkursa.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('51', 'ru', 'Чек-лист конференции (шаблон)', '/static/documents/fairs/check list konferencii.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('52', 'ru', 'Чек-лист мастер-класса (шаблон)', '/static/documents/fairs/check list master classa.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('53', 'ru', 'Чек-лист меню питания на стенде (шаблон)', '/static/documents/fairs/check list menu pitaniya na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('54', 'ru', 'Чек-лист мероприятий на выставке (шаблон)', '/static/documents/fairs/check list meropriyatii na vystavke.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('55', 'ru', 'Чек-лист мероприятий на выставке (шаблон)', '/static/documents/fairs/check list meropryiatii na vistavke.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('56', 'ru', 'Чек-лист пресс-конференции (шаблон)', '/static/documents/fairs/check list pressconferencii.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('57', 'ru', 'Чек-лист пресс-релиза (шаблон)', '/static/documents/fairs/check list press reliza.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('58', 'ru', 'Чек-лист пресс-релиза (шаблон)', '/static/documents/fairs/check list press reliza.pdf');
            
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('38', 'en', 'Анкета посетителя стенда (шаблон)', '/static/documents/fairs/ancata_posetitelya_stend.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('39', 'en', 'Инструктаж сотрудников (шаблон)', '/static/documents/fairs/instructazh sotrudnikov.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('40', 'en', 'Меню кофе-брейка, фуршета (шаблон)', '/static/documents/fairs/menu cofe-breika.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('41', 'en', 'Перечень продуктов на стенд (шаблон)', '/static/documents/fairs/perechen productov na stend.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('42', 'en', 'Советы участнику для цели PR, ВЫСТРАИВАНИЕ БРЕНДА', '/static/documents/fairs/soveti uchastnike dlya celi pr, vystraivanie brenda.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('43', 'en', 'Советы участнику для цели МАРКЕТИНГОВЫЕ ИССЛЕДОВАНИЯ', '/static/documents/fairs/soveti uchastnike dlya celi marketingovye issledovania.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('44', 'en', 'Советы участнику для цели ОТНОШЕНИЕ С ПОТРЕБИТЕЛЯМИ', '/static/documents/fairs/soveti uchastnike dlya celi otnoshenie s potrebitelyami.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('45', 'en', 'Советы участнику для цели ПОДДЕРЖКА КАНАЛОВ СБЫТА', '/static/documents/fairs/soveti uchastnike dlya celi podderzhka kanalov sbyta.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('46', 'en', 'Советы участнику для цели ПРОДАЖА', '/static/documents/fairs/soveti uchastnike dlya celi prodazha.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('47', 'en', 'Список экспонатов (шаблон)', '/static/documents/fairs/spisok eksponatov.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('48', 'en', 'Форма обработки контактов на стенде (шаблон)', '/static/documents/fairs/forma obrabotki kontaktov na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('49', 'en', 'Чек-лист демонстрации продукции на стенде (шаблон)', '/static/documents/fairs/check list demonstracii producta na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('50', 'en', 'Чек-лист конкурса (шаблон)', '/static/documents/fairs/check list konkursa.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('51', 'en', 'Чек-лист конференции (шаблон)', '/static/documents/fairs/check list konferencii.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('52', 'en', 'Чек-лист мастер-класса (шаблон)', '/static/documents/fairs/check list master classa.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('53', 'en', 'Чек-лист меню питания на стенде (шаблон)', '/static/documents/fairs/check list menu pitaniya na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('54', 'en', 'Чек-лист мероприятий на выставке (шаблон)', '/static/documents/fairs/check list meropriyatii na vystavke.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('55', 'en', 'Чек-лист мероприятий на выставке (шаблон)', '/static/documents/fairs/check list meropryiatii na vistavke.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('56', 'en', 'Чек-лист пресс-конференции (шаблон)', '/static/documents/fairs/check list pressconferencii.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('57', 'en', 'Чек-лист пресс-релиза (шаблон)', '/static/documents/fairs/check list press reliza.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('58', 'en', 'Чек-лист пресс-релиза (шаблон)', '/static/documents/fairs/check list press reliza.pdf');
            
            DELETE FROM {{trdocuments}} WHERE `id`='95';
            DELETE FROM {{trdocuments}} WHERE `id`='98';
            DELETE FROM {{documents}} WHERE `id`='55';
            DELETE FROM {{documents}} WHERE `id`='58';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}