<?php

class m160309_154847_change_columns_tbl_organizerinfo extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{organizerinfo}}
			CHANGE COLUMN `postCountry` `postCountry` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `postRegion` `postRegion` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `postCity` `postCity` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalCountry` `legalCountry` INT(11) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalRegion` `legalRegion` INT(11) NULL DEFAULT NULL ;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			ALTER TABLE {{organizerinfo}}
			CHANGE COLUMN `postCountry` `postCountry` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postRegion` `postRegion` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `postCity` `postCity` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalCountry` `legalCountry` VARCHAR(45) NULL DEFAULT NULL ,
			CHANGE COLUMN `legalRegion` `legalRegion` VARCHAR(45) NULL DEFAULT NULL ;

		';
	}
}

