<?php

class m160404_165734_add_proposal_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
{
	$sql  = $this->getCreateTable();

	$transaction = Yii::app()->db->beginTransaction();
	try
	{
		Yii::app()->db->createCommand($sql)->execute();
		$transaction->commit();
	}
	catch(Exception $e)
	{
		$transaction->rollback();

		echo $e->getMessage();

		return false;
	}

	return true;
}

	public function down()
{
	return true;
}

	public function getCreateTable(){
	return "
		UPDATE {{attributeclass}} SET `class`='textareaWithLimit' WHERE `id`='318';
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', 'Дополнительно', 'counterLabel');
		DELETE FROM {{attributeclass}} WHERE `id`='319';
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', 'зн.', 'counterUnit');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', '1.2', 'price');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', 'USD', 'priceUnit');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', '1', 'discount');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', '0', 'additionalCharacters');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('318', '306', 'inherit');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('318', '15', 'maxLength');
	";
}

}