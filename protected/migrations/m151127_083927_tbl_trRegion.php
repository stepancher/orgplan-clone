<?php

class m151127_083927_tbl_trRegion extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{trregion}}
			WHERE trParentId IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
						 31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,
						 58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,88,
						 89,93,94,95,96,97,98)
			AND langId = "de";
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("1", "de", "Region Altai");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("2", "de", "Gebiet Amur");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("3", "de", "Gebiet Archangelsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("4", "de", "Gebiet Astrachan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("5", "de", "Gbiet Belgorod");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("6", "de", "Gebiet Brjansk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("7", "de", "Gebiet Wladimir");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("8", "de", "Gebiet Wolgograd");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("9", "de", "Gebiet Wologda");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("10", "de", "Gebiet Woronesch");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("11", "de", "Jüdisches Autonomes Gebiet");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("12", "de", "Region Transbaikalien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("13", "de", "Gebiet Iwanowo");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("14", "de", "Gebiet Irkutsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("15", "de", "Republik Kabardino- Balkarien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("16", "de", "Gebiet Kaliningrad");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("17", "de", "Gebiet Kaluga");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("18", "de", "Region Kamtschatka");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("19", "de", "Republik Karatschai-Tscherkessien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("20", "de", "Gebiet Kemerowo");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("21", "de", "Gebiet Kirow");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("22", "de", "Gebiet Kostroma");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("23", "de", "Region Krasnodar");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("24", "de", "Region Krasnojarsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("25", "de", "Gebiet Kurgan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("26", "de", "Gebiet Kursk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("27", "de", "Gebiet Leningrad");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("28", "de", "Gebiet Lipezk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("29", "de", "Gebiet Magadan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("30", "de", "Moskau");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("31", "de", "Gebiet Moskau");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("32", "de", "Gebiet Murmansk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("33", "de", "Autonomer Kreis der Nenzen");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("34", "de", "Gebiet Nischni Nowgorod");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("35", "de", "Gebiet Nowgorod");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("36", "de", "Gebiet Nowosibirsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("37", "de", "Gebiet Omsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("38", "de", "Gebiet Orenburg");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("39", "de", "Gebiet Orjol");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("40", "de", "Gebiet Pensa");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("41", "de", "Region Perm");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("42", "de", "Region Primorje");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("43", "de", "Gebiet Pskow");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("44", "de", "Republik Adygeja");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("45", "de", "Republik Altai");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("46", "de", "Republik Baschkortostan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("47", "de", "Republik Burjatien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("48", "de", "Republik Dagestan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("49", "de", "Republik Inguschetien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("50", "de", "Republik Kalmückien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("51", "de", "Republik Karelien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("52", "de", "Republik Komi");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("53", "de", "Republik Mari El");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("54", "de", "Republik Mordwinien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("55", "de", "Republik Sacha (Jakutien) ");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("56", "de", "Republik Nordossetien-Alanien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("57", "de", "Republik Tatarstan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("58", "de", "Republik Tuwa");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("59", "de", "Republik Chakassien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("60", "de", "Gebiet Rostow am Don");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("61", "de", "Gebiet Rjasan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("62", "de", "Gebiet Samara");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("63", "de", "Sankt-Petersburg");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("64", "de", "Gebiet Saratow");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("65", "de", "Gebiet Sachalin");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("66", "de", "Gebiet Swerdlowsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("67", "de", "Gebiet Smolensk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("68", "de", "Region Stawropol");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("69", "de", "Gebiet Tambow");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("70", "de", "Gebiet Twer");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("71", "de", "Gebiet Tomsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("72", "de", "Gebiet Tula");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("73", "de", "Gebiet Tjumen");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("74", "de", "Republik Udmurtien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("75", "de", "Gebiet Uljanowsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("76", "de", "Region Chabarowsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("77", "de", "Autonomer Kreis der Chanten und Mansen (Jugra)");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("78", "de", "Gebiet Tscheljabinsk");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("79", "de", "Republik Tschetschenien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("80", "de", "Republik Tschuwaschien");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("81", "de", "Autonomer Kreis der Tschuktschen");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("82", "de", "Autonomer Kreis der Jamal-Nenzen");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("83", "de", "Ярославская область");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("88", "de", "Gebiet Archangelsk mit dem Autonomen Kreis der Nenzen");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("89", "de", "Gebiet Archangelsk mit dem Autonomen Kreis der Nenzen");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("93", "de", "Republik Krim");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("94", "de", "region.name");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("95", "de", "Republik Tatarstan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("96", "de", "Republik Baschkortostan");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("97", "de", "Autonomer Kreis der Chanten und Mansen (Jugra)");
			INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ("98", "de", "Republik Nordossetien");
		';
	}
}