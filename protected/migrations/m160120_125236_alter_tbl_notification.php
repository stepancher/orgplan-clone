<?php

class m160120_125236_alter_tbl_notification extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
//		$sql = '
//			ALTER TABLE  {{notification}} CHANGE  `content`  `content` VARCHAR(255);
//		';
//
//		$transaction = Yii::app()->db->beginTransaction();
//		try
//		{
//			Yii::app()->db->createCommand($sql)->execute();
//			$transaction->commit();
//		}
//		catch(Exception $e)
//		{
//			$transaction->rollback();
//
//			echo $e->getMessage();
//
//			return false;
//		}

		return true;
	}

	public function getCreateTable(){

		return "
			ALTER TABLE  {{notification}} CHANGE  `content`  `content` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
		";
	}
}