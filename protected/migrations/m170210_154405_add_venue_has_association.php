<?php

class m170210_154405_add_venue_has_association extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{exdbvenuehasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `venueId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1', '26');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('3', '19');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('5', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('5', '3');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('7', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('7', '21');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('7', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('7', '19');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('8', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('10', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('11', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('11', '29');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('11', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('11', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('13', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('16', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('16', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('16', '16');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('16', '26');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('19', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('19', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('19', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('21', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('21', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('22', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('23', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('25', '18');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('25', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('27', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('28', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('28', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('29', '18');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('29', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('30', '18');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('30', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('32', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('32', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('32', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('33', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('33', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('33', '26');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('35', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('36', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('37', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('37', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('37', '3');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('38', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('38', '8');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('38', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('39', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('39', '18');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('40', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('40', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('41', '17');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('41', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('41', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('43', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('43', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('44', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('45', '3');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('45', '24');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('45', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('46', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('46', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('46', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('48', '3');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('48', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('49', '3');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('50', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('51', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('53', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('53', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('54', '18');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('54', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('56', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('56', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('57', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('57', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('57', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('58', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('58', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('59', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('59', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('60', '19');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('60', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('61', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('61', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('61', '19');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('61', '21');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('62', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('62', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('63', '20');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('63', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('64', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('65', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('65', '3');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('66', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('69', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('70', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('70', '18');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('72', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('73', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('75', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('75', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('76', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('76', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('77', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('78', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('78', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('79', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('79', '21');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('79', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('79', '19');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('80', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('80', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('84', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('84', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('85', '21');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('85', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('85', '1');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('86', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('86', '28');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('86', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('86', '26');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('87', '17');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('88', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('88', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('88', '1');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('89', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('89', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('89', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('89', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('90', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('90', '17');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('91', '17');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('92', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('92', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('95', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('96', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('96', '1');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('99', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('102', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('102', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('103', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('103', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('105', '29');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('105', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('105', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('105', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('110', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('110', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('113', '17');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('115', '21');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('115', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('116', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('116', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('123', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('127', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('127', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('131', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('131', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('133', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('135', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('139', '14');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('142', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('145', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('147', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('147', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('147', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('153', '1');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('153', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('153', '13');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('155', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('155', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('158', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('159', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('161', '19');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('169', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('171', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('172', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('173', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('174', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('174', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('180', '16');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('180', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('180', '26');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('183', '16');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('186', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('188', '1');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('188', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('188', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('191', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('191', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('192', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('196', '23');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('202', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('206', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('212', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('212', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('215', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('242', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('253', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('255', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('255', '6');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('259', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('262', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('266', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('271', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('271', '17');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('271', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('275', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('276', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('276', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('276', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('277', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('280', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('282', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('283', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('284', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('286', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('286', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('336', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('381', '21');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('381', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('387', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('391', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('414', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('419', '20');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('425', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('429', '5');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('429', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('429', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('434', '4');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('437', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('438', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('548', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('557', '20');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('583', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('608', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('623', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('629', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('629', '29');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('655', '6');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('657', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('685', '27');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('685', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('715', '2');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('715', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('718', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('718', '30');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('718', '31');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('742', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('753', '20');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('753', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('754', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('759', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('766', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('766', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('788', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('797', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('797', '10');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('797', '1');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('806', '9');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('806', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('833', '11');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('833', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('836', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('840', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('840', '30');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('850', '22');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('850', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('850', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('866', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('875', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('897', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('897', '30');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('901', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('937', '15');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('944', '7');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('963', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('966', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('966', '30');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('968', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1023', '12');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1059', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1061', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1098', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1410', '25');
            INSERT INTO {{exdbvenuehasassociation}} (`venueId`,`associationId`) VALUES ('1719', '25');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}