<?php

class m170213_092131_add_organizercompanyhasassociation extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{organizercompanyhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `organizerCompanyId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}