<?php

class m160711_090856_deleting_proposals_and_exponents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			delete from {{proposalelement}} where userId not in (select id from {{user}});
			delete from {{proposalelement}} where userId in (select u.id from {{user}} u left join {{organizerexponent}} e on (u.id = e.userId) where e.id is null);
			delete from {{organizerexponent}} where id in(25, 26, 161);
			delete from {{proposalelement}} where userId in(823, 838, 1186, 848);
		';
	}

}