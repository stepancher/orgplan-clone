<?php

class m160830_071813_add_new_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{documents}} (`type`, `active`, `fairId`) VALUES ('2', '1', '184');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('19', 'ru', 'Список сотрудников на монтаж/демонтаж экспонатов', '/static/documents/fairs/184/f16.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('19', 'en', 'Staff list for installation/dismantle of exhibits.', '/static/documents/fairs/184/f16.pdf');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}