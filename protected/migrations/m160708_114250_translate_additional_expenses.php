<?php

class m160708_114250_translate_additional_expenses extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return " 
 			ALTER TABLE {{additionalexpenses}} 
			DROP COLUMN `name`;
			
			INSERT INTO {{tradditionalexpenses}} VALUES 
			(190,1,'ru','Выставочная площадь'),
			(191,2,'ru','Застройка стенда'),
			(192,3,'ru','Логистика'),
			(193,4,'ru','Услуги ВЦ'),
			(194,5,'ru','Реклама и продвижение'),
			(195,6,'ru','Авиаперелет, гостиницы'),
			(196,7,'ru','Другие расходы'),
			(197,1,'en','Выставочная площадь'),
			(198,2,'en','Застройка стенда'),
			(199,3,'en','Логистика'),
			(200,4,'en','Услуги ВЦ'),
			(201,5,'en','Реклама и продвижение'),
			(202,6,'en','Авиаперелет, гостиницы'),
			(203,7,'en','Другие расходы'),
			(204,1,'de','Выставочная площадь'),
			(205,2,'de','Застройка стенда'),
			(206,3,'de','Логистика'),
			(207,4,'de','Услуги ВЦ'),
			(208,5,'de','Реклама и продвижение'),
			(209,6,'de','Авиаперелет, гостиницы'),
			(210,7,'de','Другие расходы');
 		";
	}

	public function downSql()
	{
		return TRUE;
	}
}