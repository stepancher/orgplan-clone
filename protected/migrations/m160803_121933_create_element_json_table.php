<?php

class m160803_121933_create_element_json_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			CREATE TABLE IF NOT EXISTS {{proposalelementjson}} (
			`elementId` INT NOT NULL,
			`JSON` MEDIUMTEXT NULL,
			PRIMARY KEY (`elementId`));

			ALTER TABLE {{proposalelement}}
			DROP COLUMN `treeJSON`;
		";
	}
}