<?php

class m160525_110039_translates_16_05_25 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '73', 'en', 'symb.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('790', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1120', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Phone line (tone extension dialing)' WHERE `id`='2062';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('791', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1119', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Telephone set' WHERE `id`='2063';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('792', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1118', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Fax machine' WHERE `id`='2064';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('799', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1111', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='IP-address' WHERE `id`='2071';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('793', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1117', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 1 Mbit/s)' WHERE `id`='2065';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('794', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1116', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 2 Mbit/s)' WHERE `id`='2066';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('795', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1115', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 5 Mbit/s)' WHERE `id`='2067';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('796', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1114', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 10 Mbit/s)' WHERE `id`='2068';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('797', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1113', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 100 Mbit/s)' WHERE `id`='2069';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('798', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1112', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='WI-FI (data transfer speed up to 512 kbit/s)' WHERE `id`='2070';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('800', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1110', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet router' WHERE `id`='2072';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('801', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1109', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet switch' WHERE `id`='2073';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('802', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1108', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Wi-Fi USB-adapter' WHERE `id`='2074';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('803', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1107', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Wi-Fi access point' WHERE `id`='2075';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('827', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1106', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Participant set' WHERE `id`='2076';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('804', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1105', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Personal computer' WHERE `id`='2077';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('805', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1104', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Laser printer (format A4)' WHERE `id`='2078';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('806', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1103', 'en', 'pcs.');
			UPDATE {{trattributeclassproperty}} SET `value`='Copy machine (format A4)' WHERE `id`='2079';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('807', 'en', 'rub');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1144', 'en', 'Date');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1145', 'en', 'Date');
		";
	}
}