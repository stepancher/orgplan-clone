<?php

class m160519_093443_9_proposal_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='664';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='673';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='682';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='691';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='700';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='709';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='718';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='727';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='736';
		UPDATE {{attributeclass}} SET `dataType`='string' WHERE `id`='745';
		ALTER TABLE {{attributeclass}} 
		CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL ;
		ALTER TABLE {{trattributeclass}} 
		CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;
		ALTER TABLE {{trattributeclass}} 
		CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ,
		CHANGE COLUMN `trParentId` `trParentId` INT(11) UNSIGNED NULL DEFAULT NULL ;
		DELETE FROM {{trattributeclass}} WHERE `id`='257';
		DELETE FROM {{trattributeclass}} WHERE `id`='258';
		ALTER TABLE {{trattributeclass}} 
		ADD CONSTRAINT `fk_tbl_trattributeclass_1`
		FOREIGN KEY (`trParentId`)
		REFERENCES {{attributeclass}} (`id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE;
		DELETE FROM {{attributeclass}} WHERE `id`='763';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>' WHERE `id`='467';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается: <br>\nна 50% при заказе после 29.07.16 <br>\nна 100% при заказе после 16.09.16 <br>\n' WHERE `id`='467';
		UPDATE {{attributeclassproperty}} SET `defaultValue`='м³' WHERE `id`='888';
		UPDATE {{attributeclass}} SET `priority`='-1' WHERE `id`='607';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='696';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='848';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='849';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='850';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='851';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='852';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='853';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='854';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='855';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='856';
		UPDATE {{attributeclassdependency}} SET `attributeClass`='632' WHERE `id`='857';
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('651', '651', 'min', '{\"min\": 5}');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('653', '653', 'min', '{\"min\": 5}');
		DELETE FROM {{attributeclassdependency}} WHERE `id`='859';
		DELETE FROM {{attributeclassdependency}} WHERE `id`='860';
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('651', '5', 'min');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('653', '5', 'min');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4_hint_danger', 'string', '0', '75', '75', '1.5', 'hintDanger', '0');
		UPDATE {{attributeclass}} SET `priority`='1.5' WHERE `id`='2006';
		UPDATE {{attributeclass}} SET `priority`='9' WHERE `id`='2006';
		DELETE FROM {{attributeclass}} WHERE `id`='0';
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4_hint_danger', 'string', '0', '75', '75', '1.5', 'hintDanger', '0');
		ALTER TABLE {{attributeclass}} 
		CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2007', 'ru', 'Отправьте запоненную форму в дирекцию выставки не позднее 23 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
		UPDATE {{attributeclass}} SET `parent`='0', `super`='2007', `priority`='0' WHERE `id`='2007';
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('2', '2007', '0', '0', '0');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'contest_hint_danger', 'string', '0', '0', '2008', '0', 'hintDanger', '0');
		INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES (NULL, '3', '2008', '0', '0', '0');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2008', 'ru', 'Отправьте запоненную форму в дирекцию выставки не позднее 1 июня 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
		UPDATE {{attributeclass}} SET `name`='n9_hint_danger_last' WHERE `id`='2006';
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_hint_last', 'string', '0', '0', '2009', '9', 'hint', '0');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('10', '2009', '0', '0', '101');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2009', 'ru', 'Все цены включают НДС 18%.\nОплата настоящей Заявки производится в российских рублях.');
		UPDATE {{attributemodel}} SET `priority`='99' WHERE `id`='170';
		UPDATE {{attributeclass}} SET `parent`='654', `super`='654', `priority`='11' WHERE `id`='2009';
		DELETE FROM {{attributemodel}} WHERE `id`='170';
		UPDATE {{attributeclass}} SET `priority`='9' WHERE `id`='2009';
		UPDATE {{attributeclass}} SET `priority`='9.5' WHERE `id`='2005';
		UPDATE {{attributeclass}} SET `priority`='9.6' WHERE `id`='2009';
		UPDATE `orgplan`.`tbl_trattributeclassproperty` SET `value`='м³' WHERE `id`='579';
		UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.\n</div>' WHERE `id`='497';
		UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.\n</div>' WHERE `id`='476';
		UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ОБЪЕМА\n</div>\n<div class=\"body\">\nПри расчетах каждый начатый кубометр фактического объема принимаются за полный кубометр.\n</div>' WHERE `id`='593';
		UPDATE {{attributeclass}} SET `tooltipCustom`='' WHERE `id`='10';
		UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>' WHERE `id`='589';
		UPDATE {{attributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>' WHERE `id`='591';
		UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>' WHERE `id`='475';
		UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВЕСА\n</div>\n<div class=\"body\">\nПри расчетах каждые начатые 100 кг фактического веса принимаются за полные 100 кг.\n</div>' WHERE `id`='474';
		";
	}
}