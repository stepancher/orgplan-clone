<?php

class m151022_113418_create_chart_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{chart}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS {{chart}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(500) DEFAULT NULL,
			  `typeId` int(11) DEFAULT NULL,
			  `setId` int(11) DEFAULT NULL,
			  `widget` int(11) DEFAULT NULL,
			  `widgetType` int(11) DEFAULT NULL,
			  `header` varchar(200) DEFAULT NULL,
			  `measure` varchar(50) DEFAULT NULL,
			  `color` varchar(100) DEFAULT NULL,
			  `groupId` int(11) DEFAULT NULL,
			  `related` int(11) DEFAULT NULL,
			  `parent` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
		';
	}
}