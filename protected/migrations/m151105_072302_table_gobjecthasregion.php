<?php

class m151105_072302_table_gobjecthasregion extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{gobjecthasregion}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return '
			DROP TABLE IF EXISTS {{gobjecthasregion}};

			CREATE TABLE IF NOT EXISTS {{gobjecthasregion}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `objId` int(11) DEFAULT NULL,
			  `regionId` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

			INSERT INTO {{gobjecthasregion}} (`id`, `objId`, `regionId`) VALUES
				(1, 1, 5),
				(2, 10, 30),
				(3, 21, 3),
				(4, 33, 4),
				(5, 2, 6),
				(6, 3, 7),
				(7, 34, 8),
				(8, 22, 9),
				(9, 4, 10),
				(10, 82, 11),
				(11, 68, 12),
				(12, 5, 13),
				(13, 70, 14),
				(14, 38, 15),
				(15, 23, 16),
				(16, 6, 17),
				(17, 77, 18),
				(18, 39, 19),
				(19, 71, 20),
				(20, 50, 21),
				(21, 7, 22),
				(22, 32, 23),
				(23, 69, 24),
				(24, 57, 25),
				(25, 8, 26),
				(26, 24, 27),
				(27, 9, 28),
				(28, 80, 29),
				(29, 11, 31),
				(30, 26, 32),
				(31, 29, 33),
				(32, 51, 34),
				(33, 27, 35),
				(34, 72, 36),
				(35, 73, 37),
				(36, 52, 38),
				(37, 12, 39),
				(38, 53, 40),
				(39, 49, 41),
				(40, 76, 42),
				(41, 28, 43),
				(42, 30, 44),
				(43, 63, 45),
				(44, 43, 46),
				(45, 64, 47),
				(46, 36, 48),
				(47, 37, 49),
				(48, 31, 50),
				(49, 19, 51),
				(50, 20, 52),
				(51, 86, 93),
				(52, 33, 53),
				(53, 45, 54),
				(54, 75, 55),
				(55, 40, 56),
				(56, 46, 57),
				(57, 65, 58),
				(58, 66, 59),
				(59, 35, 60),
				(60, 13, 61),
				(61, 54, 62),
				(62, 55, 64),
				(63, 81, 65),
				(64, 58, 66),
				(65, 14, 67),
				(66, 42, 68),
				(67, 15, 69),
				(68, 16, 70),
				(69, 74, 71),
				(70, 17, 72),
				(71, 59, 73),
				(72, 47, 74),
				(73, 56, 75),
				(74, 78, 76),
				(75, 61, 77),
				(76, 60, 78),
				(77, 41, 79),
				(78, 48, 80),
				(79, 83, 81),
				(80, 62, 82),
				(81, 18, 83),
				(82, 67, 1),
				(83, 25, 63),
				(84, 79, 2),
				(85, 21, 88),
				(86, 21, 89),
				(87, 61, 97),
				(88, 40, 98);
			';
	}
}