<?php

class m160928_160742_update_exponentPrepare_table extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{exponentprepare}} 
            ADD COLUMN `pressConf` INT(11) NULL AFTER `fairId`,
            ADD COLUMN `productDemonstration` INT(11) NULL AFTER `pressConf`,
            ADD COLUMN `masterClass` INT(11) NULL AFTER `productDemonstration`,
            ADD COLUMN `conference` INT(11) NULL AFTER `masterClass`,
            ADD COLUMN `companyPresentation` INT(11) NULL AFTER `conference`,
            ADD COLUMN `actionsConcurs` INT(11) NULL AFTER `companyPresentation`;
            
            ALTER TABLE {{exponentprepare}} 
            ADD COLUMN `liflet` INT(11) NULL AFTER `actionsConcurs`,
            ADD COLUMN `createLists` INT(11) NULL AFTER `liflet`,
            ADD COLUMN `createLiflet` INT(11) NULL AFTER `createLists`,
            ADD COLUMN `invites` INT(11) NULL AFTER `createLiflet`,
            ADD COLUMN `visits` INT(11) NULL AFTER `invites`,
            ADD COLUMN `posters` INT(11) NULL AFTER `visits`,
            ADD COLUMN `guestsLists` INT(11) NULL AFTER `posters`,
            ADD COLUMN `lists` INT(11) NULL AFTER `guestsLists`;
            
            ALTER TABLE {{exponentprepare}} 
            ADD COLUMN `promoPersonal` INT(11) NULL AFTER `lists`,
            ADD COLUMN `stendists` INT(11) NULL AFTER `promoPersonal`,
            ADD COLUMN `animators` INT(11) NULL AFTER `stendists`,
            ADD COLUMN `hostests` INT(11) NULL AFTER `animators`,
            ADD COLUMN `tamada` INT(11) NULL AFTER `hostests`,
            ADD COLUMN `barmen` INT(11) NULL AFTER `tamada`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}