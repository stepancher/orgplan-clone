<?php

class m160701_065251_create_summary_table_for_city extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			CREATE TABLE {{citysummary}} (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`cityId` INT(11) NOT NULL,
			`fairsCount` INT(11) NOT NULL,
			`datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`),
			UNIQUE INDEX `region_summary_uniq_regionid` (`cityId` ASC));
		
			UPDATE {{route}} SET `route`='search/citySummaryByFairs', `params`='[]' WHERE `id`='93581';
			UPDATE {{route}} SET `route`='search/citySummaryByFairs', `params`='[]' WHERE `id`='93616';

			INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`)
			SELECT c.id, 'en', c.name FROM {{city}} c
			LEFT JOIN {{trcity}} trc ON (trc.trParentId = c.id AND trc.langId = 'en')
			WHERE trc.trParentId IS NULL;
";
	}

	public function downSql()
	{
		return "
		 	DROP TABLE {{citysummary}};

		 	UPDATE {{route}} SET `route`='search/index', `params`='{\"by\":\"byCity\",\"sector\":\"fair\"}' WHERE `id`='93581';
		 	UPDATE {{route}} SET `route`='search/index', `params`='{\"by\":\"byCity\",\"sector\":\"fair\"}' WHERE `id`='93616';
		";
	}
}