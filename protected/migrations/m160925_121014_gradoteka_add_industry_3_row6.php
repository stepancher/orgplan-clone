<?php

class m160925_121014_gradoteka_add_industry_3_row6 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500057', '7224+7225+7226+7227+7228+7229', 'Экспорт', 'derivative', 'ДолларСША', '1');
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500059', '7224+7225+7226+7227+7228+7229', 'Импорт', 'derivative', 'ДолларСША', '1');
            
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500057', 'aggregatable', 'SUM');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500059', 'aggregatable', 'SUM');

            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','1','5745edc174d897cb0890f98b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','2','5745ec6d74d897cb088def89');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','3','5745eb9c74d897cb088bca59');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','4','5745eb9c74d897cb088bca55');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','5','5745ec6d74d897cb088def8f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','6','5745ec6d74d897cb088defad');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','7','5745eb9c74d897cb088bca5d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','8','5745eb9c74d897cb088bca61');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','9','5745ec6d74d897cb088def99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','10','5745ee5274d897cb08924051');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','11','5745ed7a74d897cb08905a8e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','12','5745eb9c74d897cb088bca69');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','13','5745eb9c74d897cb088bca6d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','14','5745ec2474d897cb088d3e30');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','15','5745ed3574d897cb088fbd76');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','16','5745edc174d897cb0890f9a3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','17','5745ec2474d897cb088d3e36');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','18','5745eb9c74d897cb088bca71');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','19','5745eb9c74d897cb088bca75');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','20','5745eb9c74d897cb088bca79');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','21','5745eb9c74d897cb088bca7d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','22','5745ee5274d897cb08924063');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','23','5745eb9c74d897cb088bca81');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','24','5745ecb174d897cb088e8e28');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','25','5745eb9c74d897cb088bca8d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','26','5745ef1e74d897cb0894039b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','27','5745ecb174d897cb088e8e3a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','28','5745ee5274d897cb0892406b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','29','5745eb9c74d897cb088bca85');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','30','5745ee9c74d897cb0892e76c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','31','5745eb9c74d897cb088bca89');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','32','5745eb9c74d897cb088bca65');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','33','5745ecb174d897cb088e8e1e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','34','5745eb9c74d897cb088bcaa1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','35','5745eb9c74d897cb088bca9d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','36','5745eb9c74d897cb088bcaa5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','37','5745eb9c74d897cb088bca99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','38','5745eb9c74d897cb088bca95');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','39','5745edc174d897cb0890f9b9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','40','5745ec2474d897cb088d3e50');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','41','5745eb9c74d897cb088bcab5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','42','5745eb9c74d897cb088bcaa9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','43','5745eb9c74d897cb088bcaad');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','44','5745ecf274d897cb088f22bc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','45','5745eb9c74d897cb088bcab1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','46','5745eb9c74d897cb088bcadd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','47','5745eb9c74d897cb088bcb25');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','48','5745eb9c74d897cb088bcac9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','49','5745ec6d74d897cb088defd7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','50','5745ed3574d897cb088fbda2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','51','5745eb9c74d897cb088bcad1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','52','5745ec2474d897cb088d3e70');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','53','5745eb9c74d897cb088bcae1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','54','5745ebde74d897cb088c8a74');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','55','5745eb9c74d897cb088bcae5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','56','5745eb9c74d897cb088bcad5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','57','5745eb9c74d897cb088bcae9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','58','5745eb9c74d897cb088bcaf5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','59','5745ec6d74d897cb088defc5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','60','5745eb9c74d897cb088bcab9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','61','5745eb9c74d897cb088bcaed');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','62','5745eb9c74d897cb088bcabd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','63','5745eb9c74d897cb088bcac1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','64','5745eb9c74d897cb088bcaf1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','65','5745ec2474d897cb088d3e5e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','66','5745eb9c74d897cb088bcaf9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','67','5745ebde74d897cb088c8a82');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','68','5745ecb174d897cb088e8e70');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','69','5745eb9c74d897cb088bcb01');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','70','5745eb9c74d897cb088bcafd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','71','5745ec6d74d897cb088deff1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','72','5745eb9c74d897cb088bca91');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','73','5745ec2474d897cb088d3e82');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','74','5745eb9c74d897cb088bcb05');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','75','5745ee9c74d897cb0892e7ac');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','76','5745eb9c74d897cb088bcb09');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','77','5745ecf274d897cb088f22f0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','78','5745ebde74d897cb088c8a90');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','79','5745ef1e74d897cb089403e1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','80','5745eb9c74d897cb088bcb1d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','81','5745edc174d897cb0890f9fb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','82','5745eb9c74d897cb088bcb0d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','83','5745eb9c74d897cb088bcb15');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','84','5745ef1e74d897cb089403e9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','85','5745eb9c74d897cb088bcb11');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','86','5745eb9c74d897cb088bcb21');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','87','5745eb9c74d897cb088bcb19');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','88','5745eb9c74d897cb088bcad9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','89','5745ebde74d897cb088c8a6e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','90','5745eb9c74d897cb088bcac5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','91','5745ee0a74d897cb08919f6e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','92','5745eb9c74d897cb088bcacd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','93','5745ecf274d897cb088f2304');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','94','5745ec6d74d897cb088defe3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','95','5745ecb174d897cb088e8e8a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','96','5745ebde74d897cb088c8aa0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','97','5745edc174d897cb0890fa09');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','98','5745ec2474d897cb088d3e9a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','99','5745eb9c74d897cb088bcb2d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','100','5745eb9c74d897cb088bcb29');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','101','5745ec2574d897cb088d3e9e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','102','5745eb9c74d897cb088bcb39');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','103','5745ecf274d897cb088f2316');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','104','5745eb9c74d897cb088bcb41');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','105','5745ebde74d897cb088c8ab2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','106','5745eb9c74d897cb088bcb45');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','107','5745eb9c74d897cb088bcb4d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','108','5745eb9c74d897cb088bcb31');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','109','5745eb9c74d897cb088bcb49');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','110','5745ebde74d897cb088c8aaa');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','111','5745ec2574d897cb088d3eb4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','112','5745ee0b74d897cb08919fb2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','113','5745ec6d74d897cb088df021');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','114','5745eb9c74d897cb088bcb51');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','115','5745ec2574d897cb088d3ebc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','116','5745ef6574d897cb0894a17a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','117','5745ee9d74d897cb0892e7de');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','118','5745ecf274d897cb088f2322');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','119','5745eb9c74d897cb088bcb55');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','120','5745ebde74d897cb088c8aba');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','121','5745ee9d74d897cb0892e7be');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','122','5745eede74d897cb089379ec');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','123','5745ecf274d897cb088f232a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','124','5745ecf274d897cb088f2326');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','125','5745ecf274d897cb088f232e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','126','5745eb9c74d897cb088bcb59');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','127','5745ee0b74d897cb08919fc8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','128','5745ebde74d897cb088c8ac0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','129','5745eb9c74d897cb088bcb65');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','130','5745ecb174d897cb088e8ea6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','131','5745ec6d74d897cb088df02d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','132','5745eb9c74d897cb088bcb5d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','133','5745ec2574d897cb088d3eca');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','134','5745eb9c74d897cb088bcb61');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','135','5745eb9c74d897cb088bcb3d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','136','5745ec2574d897cb088d3eac');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','137','5745eb9c74d897cb088bcb35');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','138','5745ee5274d897cb089240c5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','139','5745ee0b74d897cb08919fc4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','140','5745ecb174d897cb088e8eae');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','141','5745eb9c74d897cb088bcb69');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','142','5745ec2574d897cb088d3ed0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','143','5745edc174d897cb0890fa31');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','144','5745eb9c74d897cb088bcb71');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','145','5745eb9c74d897cb088bcb6d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','146','5745ef6574d897cb0894a18e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','147','5745ecf274d897cb088f2340');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','148','5745eede74d897cb08937a02');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','149','5745ecb174d897cb088e8eb8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','150','5745ec2574d897cb088d3ed8');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','151','5745eb9c74d897cb088bcb75');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','152','5745ec6d74d897cb088df03f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','153','5745ebde74d897cb088c8ad2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','154','5745eb9c74d897cb088bcb79');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','155','5745ee9d74d897cb0892e7ee');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','156','5745eb9c74d897cb088bcb7d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','157','5745ebde74d897cb088c8ada');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','158','5745eb9c74d897cb088bcb81');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','159','5745ec6d74d897cb088df049');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','160','5745ef1e74d897cb08940423');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','161','5745ebde74d897cb088c8ad6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','162','5745ecf274d897cb088f2350');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','163','5745edc174d897cb0890fa45');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','164','5745eb9c74d897cb088bcb91');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','165','5745eb9c74d897cb088bcb89');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','166','5745eb9c74d897cb088bcb8d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','167','5745eb9c74d897cb088bcb85');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','168','5745ebde74d897cb088c8ae6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','169','5745eb9c74d897cb088bcbad');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','170','5745ebde74d897cb088c8b2e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','171','5745eb9c74d897cb088bcb99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','172','5745edc174d897cb0890fa61');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','173','5745ecf274d897cb088f2378');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','174','5745ebde74d897cb088c8af6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','175','5745eb9c74d897cb088bcbb1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','176','5745ef1e74d897cb08940447');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','177','5745ecf274d897cb088f236e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','178','5745eb9c74d897cb088bcbb5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','179','5745eb9c74d897cb088bcbbd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','180','5745eb9c74d897cb088bcbb9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','181','5745ec6d74d897cb088df059');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','182','5745ecb174d897cb088e8ed4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','183','5745eb9c74d897cb088bcbc5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','184','5745ef6674d897cb0894a1c6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','185','5745eb9c74d897cb088bcbc1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','186','5745ef1e74d897cb08940453');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','187','5745eb9c74d897cb088bcbc9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','188','5745ebde74d897cb088c8b10');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','189','5745eb9c74d897cb088bcbcd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','190','5745eb9c74d897cb088bcbd1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','191','5745eb9c74d897cb088bcbd5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','192','5745eb9c74d897cb088bcbd9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','193','5745ef1e74d897cb08940467');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','194','5745eb9c74d897cb088bcbdd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','195','5745eb9c74d897cb088bcba9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','196','5745eb9c74d897cb088bcbf5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','197','5745eb9c74d897cb088bcbe1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','198','5745ec2574d897cb088d3f16');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','199','5745eb9c74d897cb088bcbed');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','200','5745eb9c74d897cb088bcbe5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','201','5745eb9c74d897cb088bcbe9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','202','5745eb9c74d897cb088bcbf9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','203','5745eb9c74d897cb088bcbf1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','204','5745eb9c74d897cb088bcba1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','205','5745eb9c74d897cb088bcba5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','206','5745eb9c74d897cb088bcb95');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','207','5745ebde74d897cb088c8aec');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','208','5745ebde74d897cb088c8b1a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','209','5745eb9c74d897cb088bcb9d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','210','5745ee0b74d897cb0891a00a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','211','5745ecf274d897cb088f23aa');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','212','5745ec6e74d897cb088df09b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','213','5745ef6674d897cb0894a1ee');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','214','5745eb9c74d897cb088bcbfd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','215','5745ee0b74d897cb0891a028');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','216','5745eb9c74d897cb088bcc09');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','217','5745edc174d897cb0890fa95');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','218','5745ebde74d897cb088c8b38');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','219','5745eb9c74d897cb088bcc0d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','220','5745ec6e74d897cb088df0a1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','221','5745ed3674d897cb088fbe52');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','222','5745ec2574d897cb088d3f2a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','223','5745ebde74d897cb088c8b3e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','224','5745ed7b74d897cb08905b72');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','225','5745ef6674d897cb0894a1fc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','226','5745eede74d897cb08937a52');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','227','5745ecf274d897cb088f23a6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','228','5745ee5274d897cb08924139');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','229','5745ec2574d897cb088d3f38');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','230','5745ecb174d897cb088e8f1a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','231','5745ec2574d897cb088d3f3c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','232','5745ee5274d897cb0892413f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','233','5745ebde74d897cb088c8b44');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','234','5745eb9c74d897cb088bcc11');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','235','5745edc174d897cb0890fa99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','236','5745eb9c74d897cb088bcc05');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','237','5745ed3674d897cb088fbe56');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','238','5745ebde74d897cb088c8b34');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','239','5745ec6e74d897cb088df097');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('26','240','5745eb9c74d897cb088bcc01');
                        
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','1','5745f08174d897cb0897ac85');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','2','5745efbe74d897cb0895ad4f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','3','5745f02174d897cb0896c683');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','4','5745efbe74d897cb0895ad4b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','5','5745efbe74d897cb0895ad53');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','6','5745f31b74d897cb089d73f3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','7','5745f14074d897cb08995c3f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','8','5745f2bc74d897cb089ca331');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','9','5745efbe74d897cb0895ad57');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','10','5745f31b74d897cb089d73ff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','11','5745efbe74d897cb0895ad5b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','12','5745f3ac74d897cb089e2f75');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','13','5745f46474d897cb089fbc76');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','14','5745efbe74d897cb0895ad63');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','15','5745f1ff74d897cb089b0383');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','16','5745f14074d897cb08995c57');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','17','5745f40574d897cb089eeebe');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','18','5745f2bc74d897cb089ca349');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','19','5745efbe74d897cb0895ad77');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','20','5745efbe74d897cb0895ad5f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','21','5745efbe74d897cb0895ad73');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','22','5745f25e74d897cb089bd4d6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','23','5745f0e174d897cb08988921');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','24','5745f08174d897cb0897aca1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','25','5745f14074d897cb08995c53');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','26','5745f0e174d897cb0898892d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','27','5745f08174d897cb0897aca9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','28','5745efbe74d897cb0895ad7f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','29','5745efbe74d897cb0895ad83');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','30','5745efbe74d897cb0895ad67');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','31','5745efbe74d897cb0895ad6b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','32','5745f14074d897cb08995c47');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','33','5745efbe74d897cb0895ad7b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','34','5745f02174d897cb0896c691');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','35','5745efbe74d897cb0895ad6f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','36','5745f0e174d897cb08988933');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','37','5745efbe74d897cb0895ad87');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','38','5745f0e174d897cb08988937');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','39','5745efbe74d897cb0895ad8f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','40','5745efbe74d897cb0895ad9b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','41','5745f08174d897cb0897acb7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','42','5745f08174d897cb0897acbf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','43','5745efbe74d897cb0895ad8b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','44','5745f0e174d897cb0898894f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','45','5745f2bc74d897cb089ca367');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','46','5745f14074d897cb08995c77');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','47','5745f0e174d897cb08988949');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','48','5745f14074d897cb08995c81');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','49','5745f02174d897cb0896c6b1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','50','5745f0e174d897cb08988955');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','51','5745f14074d897cb08995c7d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','52','5745efbe74d897cb0895ad93');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','53','5745efbe74d897cb0895ad97');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','54','5745f02174d897cb0896c6a7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','55','5745f0e174d897cb0898893b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','56','5745efbe74d897cb0895ad9f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','57','5745f19e74d897cb089a2eb9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','58','5745f40574d897cb089eeee0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','59','5745f02174d897cb0896c6b5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','60','5745f08174d897cb0897acc7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','61','5745f31b74d897cb089d7429');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','62','5745f1ff74d897cb089b03ab');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','63','5745efbe74d897cb0895ada3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','64','5745f19e74d897cb089a2ecd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','65','5745efbe74d897cb0895ada7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','66','5745f2bc74d897cb089ca373');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','67','5745f0e174d897cb0898895d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','68','5745efbe74d897cb0895adab');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','69','5745efbe74d897cb0895adaf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','70','5745f40574d897cb089eeefa');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','71','5745efbe74d897cb0895adbb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','72','5745f0e174d897cb0898896d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','73','5745f25e74d897cb089bd502');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','74','5745f08174d897cb0897ace3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','75','5745f08174d897cb0897acdf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','76','5745efbe74d897cb0895adbf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','77','5745efbf74d897cb0895adcf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','78','5745f0e274d897cb0898897f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','79','5745efbe74d897cb0895adb3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','80','5745f02174d897cb0896c6d7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','81','5745f0e274d897cb08988987');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','82','5745f0e274d897cb08988983');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','83','5745f3ac74d897cb089e2fb7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','84','5745f14074d897cb08995ca5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','85','5745efbf74d897cb0895add3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','86','5745efbf74d897cb0895add7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','87','5745efbf74d897cb0895addf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','88','5745efbf74d897cb0895adcb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','89','5745f02174d897cb0896c6e9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','90','5745efbf74d897cb0895ade3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','91','5745f02174d897cb0896c6e3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','92','5745efbf74d897cb0895ade7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','93','5745efbe74d897cb0895adc3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','94','5745efbf74d897cb0895adc7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','95','5745efbe74d897cb0895adb7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','96','5745f02174d897cb0896c6bf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','97','5745efbf74d897cb0895addb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','98','5745f02174d897cb0896c6ed');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','99','5745f02174d897cb0896c6d3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','100','5745efbf74d897cb0895adeb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','101','5745efbf74d897cb0895adef');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','102','5745f0e274d897cb089889bb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','103','5745efbf74d897cb0895adfb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','104','5745f19f74d897cb089a2f11');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','105','5745f08174d897cb0897ad09');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','106','5745efbf74d897cb0895ae03');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','107','5745f02174d897cb0896c709');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','108','5745f0e274d897cb0898899b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','109','5745efbf74d897cb0895adf3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','110','5745efbf74d897cb0895ae07');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','111','5745f0e274d897cb089889af');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','112','5745f2bc74d897cb089ca3af');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','113','5745efbf74d897cb0895ae0b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','114','5745f02174d897cb0896c70f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','115','5745f02174d897cb0896c715');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','116','5745f1ff74d897cb089b03f7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','117','5745efbf74d897cb0895adff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','118','5745f02174d897cb0896c71b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','119','5745efbf74d897cb0895ae13');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','120','5745efbf74d897cb0895ae17');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','121','5745f08174d897cb0897ad01');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','122','5745f02174d897cb0896c6ff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','123','5745efbf74d897cb0895adf7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','124','5745f02174d897cb0896c6f5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','125','5745efbf74d897cb0895ae0f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('27','126','5745f19f74d897cb089a2f17');
            
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500057', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500059', 'country');
            
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68803', '2016-09-25 16:14:33', '1502620000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68803', '2016-09-25 16:14:33', '310367000', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68804', '2016-09-25 16:19:17', '316589000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68804', '2016-09-25 16:19:17', '48769100', '2016');

            DELETE FROM {{gstattypes}} WHERE `id`='503229';
            DELETE FROM {{gstattypes}} WHERE `id`='503230';
            
            UPDATE {{gstattypes}} SET `name`='8458+8459' WHERE `id`='503089';
            UPDATE {{gstattypes}} SET `name`='8458+8459' WHERE `id`='503090';
            
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`, `industryId`) VALUES ('379', 'ТОВАРООБОРОТ ЛЕГИРОВАННЫХ СТАЛЕЙ', '5', '3');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('14', '68803');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('14', '68804');

            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('7', '500057', 'Доллар США');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('8', '500059', 'Доллар США');
            
            DELETE FROM {{gobjectshasgstattypes}} WHERE `id` = 68663;
            DELETE FROM {{gobjectshasgstattypes}} WHERE `id` = 68664;
            DELETE FROM {{gobjectshasgstattypes}} WHERE `id` = 68803;
            DELETE FROM {{gobjectshasgstattypes}} WHERE `id` = 68804;
            
            DELETE FROM {{gvalue}} WHERE `setId` = 68663;
            DELETE FROM {{gvalue}} WHERE `setId` = 68664;
            DELETE FROM {{gvalue}} WHERE `setId` = 68803;
            DELETE FROM {{gvalue}} WHERE `setId` = 68804;
            
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('68663', '379', '500053', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('68664', '379', '500055', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('68803', '379', '500057', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`id`,`objId`,`statId`,`gType`) VALUES ('68804', '379', '500059', 'country');
                        
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708527', '68663', '2016-09-25 16:31:19', '10138000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708528', '68663', '2016-09-25 16:31:19', '1669280', '2016');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708529', '68664', '2016-09-25 16:31:19', '50414900', '2016');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708530', '68664', '2016-09-25 16:31:19', '276274000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708740', '68803', '2016-09-25 19:14:33', '1498430000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708741', '68803', '2016-09-25 19:14:33', '309382000', '2016');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708742', '68804', '2016-09-25 19:19:17', '316589000', '2015');
            INSERT INTO {{gvalue}} (`id`,`setId`,`timestamp`,`value`,`epoch`) VALUES ('708743', '68804', '2016-09-25 19:19:17', '48769100', '2016');
                        
            UPDATE {{gstatgroup}} SET `id`='14' WHERE `id`='15';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}