<?php

class m161108_131833_delete_fairs_13060_12778_12750 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE `id` = 12774;
            DELETE FROM {{fair}} WHERE `id` = 12917;
            DELETE FROM {{fair}} WHERE `id` = 12749;
            
            DELETE FROM {{fairinfo}} WHERE `id` = 1743;
            DELETE FROM {{fairinfo}} WHERE `id` = 10302;
            DELETE FROM {{fairinfo}} WHERE `id` = 1241;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}