<?php

class m160416_120133_change_some_content_in_blog extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		UPDATE {{contact}} SET `post`='руководитель отдела маркетинга Protoplan' WHERE `id`='8';
		UPDATE {{contact}} SET `post`='основатель Protoplan' WHERE `id`='37';
		UPDATE {{contact}} SET `post`='управляющий партнер Protoplan' WHERE `id`='38';
		UPDATE {{seo}} SET `title`='Блог | Protoplan' WHERE `url`='/ru/blog';
		";
	}
}