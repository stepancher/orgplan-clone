<?php

class m161103_124816_add_fair_statistics_to_fairinfo extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `add_fair_statistics_to_fairinfo`()
            BEGIN
            
                DECLARE done BOOL DEFAULT FALSE;
                
                DECLARE squareGross INT DEFAULT NULL;
                DECLARE squareNet INT DEFAULT NULL;
                DECLARE members INT DEFAULT NULL;
                DECLARE visitors INT DEFAULT NULL;
                DECLARE statistics TEXT DEFAULT NULL;
                DECLARE statisticsDate DATE DEFAULT NULL;
                DECLARE fairId INT DEFAULT NULL;
                
                DECLARE copy CURSOR FOR SELECT 
                        f.squareGross,
                        f.squareNet,
                        f.members,
                        f.visitors,
                        f.statistics,
                        f.statisticsDate,
                        f.id as fairId
                    FROM {{fair}} f
                    LEFT JOIN {{fairinfo}} fi ON fi.fairId = f.id WHERE fi.id IS NULL;
                    
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    FETCH copy INTO 
                        squareGross,
                        squareNet,
                        members,
                        visitors,
                        statistics,
                        statisticsDate,
                        fairId;
                
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {{fairinfo}} (`fairId`,`squareGross`,`squareNet`,`members`,`visitors`,`statistics`,`statisticsDate`) VALUES (
                            fairId,squareGross,squareNet,members,visitors,statistics,statisticsDate);
                    COMMIT;
                    
                    END LOOP;
                CLOSE copy;
            END;
    
    CALL add_fair_statistics_to_fairinfo();
    DROP PROCEDURE IF EXISTS `add_fair_statistics_to_fairinfo`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}