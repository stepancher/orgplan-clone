<?php

class m161227_100830_delete_unused_routes extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{route}} WHERE `id`='93575';
            DELETE FROM {{route}} WHERE `id`='93583';
            DELETE FROM {{route}} WHERE `id`='93586';
            DELETE FROM {{route}} WHERE `id`='93594';
            DELETE FROM {{route}} WHERE `id`='93597';
            DELETE FROM {{route}} WHERE `id`='93618';
            DELETE FROM {{route}} WHERE `id`='93621';
            DELETE FROM {{route}} WHERE `id`='93628';
            DELETE FROM {{route}} WHERE `id`='93631';
            DELETE FROM {{route}} WHERE `id`='93633';
            DELETE FROM {{route}} WHERE `id`='93637';
            DELETE FROM {{route}} WHERE `id`='93649';
            DELETE FROM {{route}} WHERE `id`='93653';
            
            DELETE FROM {{route}} WHERE `id`='93577';
            DELETE FROM {{route}} WHERE `id`='93582';
            DELETE FROM {{route}} WHERE `id`='93612';
            DELETE FROM {{route}} WHERE `id`='93617';
            DELETE FROM {{route}} WHERE `id`='93639';
            DELETE FROM {{route}} WHERE `id`='93643';

            DELETE FROM {{route}} WHERE `id`='93601';
            DELETE FROM {{route}} WHERE `id`='93604';
            DELETE FROM {{route}} WHERE `id`='93606';

            DELETE FROM {{route}} WHERE `id`='93576';
            DELETE FROM {{route}} WHERE `id`='93584';
            DELETE FROM {{route}} WHERE `id`='93596';
            DELETE FROM {{route}} WHERE `id`='93611';
            DELETE FROM {{route}} WHERE `id`='93619';
            DELETE FROM {{route}} WHERE `id`='93630';
            DELETE FROM {{route}} WHERE `id`='93638';
            DELETE FROM {{route}} WHERE `id`='93640';
            DELETE FROM {{route}} WHERE `id`='93644';
            
            DELETE FROM {{route}} WHERE `id`='93574';
            DELETE FROM {{route}} WHERE `id`='93579';
            DELETE FROM {{route}} WHERE `id`='93581';
            DELETE FROM {{route}} WHERE `id`='93585';
            DELETE FROM {{route}} WHERE `id`='93590';
            DELETE FROM {{route}} WHERE `id`='93592';
            DELETE FROM {{route}} WHERE `id`='93609';
            DELETE FROM {{route}} WHERE `id`='93614';
            DELETE FROM {{route}} WHERE `id`='93616';
            DELETE FROM {{route}} WHERE `id`='93620';
            DELETE FROM {{route}} WHERE `id`='93625';
            DELETE FROM {{route}} WHERE `id`='93627';
            DELETE FROM {{route}} WHERE `id`='93634';
            DELETE FROM {{route}} WHERE `id`='93635';
            DELETE FROM {{route}} WHERE `id`='93636';
            DELETE FROM {{route}} WHERE `id`='93641';
            DELETE FROM {{route}} WHERE `id`='93645';
            DELETE FROM {{route}} WHERE `id`='93652';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}