<?php

class m160829_112805_add_new_organizer extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql(){

        list($peace1, $peace2, $dbProposalAgrorusName) = explode('=', Yii::app()->dbProposalAgrorus->connectionString);

        return "
            UPDATE {{myfair}} SET `fairId`='11699' WHERE `id`='189';
            UPDATE {{user}} SET `role`='7' WHERE `id`='978';
            INSERT INTO {{userhasorganizer}} (`organizerId`, `userId`) VALUES ('53', '978');
            
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='1';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='2';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='3';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='4';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='5';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='7';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='10';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='18';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='19';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='20';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='21';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='22';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='23';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='24';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='25';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='26';
            UPDATE {$dbProposalAgrorusName}.tbl_proposalelementclass SET `fairId`='11699' WHERE `id`='27';

            ALTER TABLE {{fair}} 
            ADD COLUMN `shard` VARCHAR(45) NULL AFTER `lang`;
            UPDATE {{fair}} SET `shard`='dbProposal' WHERE `id`='184';
            UPDATE {{fair}} SET `shard`='dbProposalAgrorus' WHERE `id`='11699';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}