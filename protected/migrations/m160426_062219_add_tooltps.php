<?php

class m160426_062219_add_tooltps extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `tooltipCustom`='Для печати необходимо предоставить макет в формате TIFF 300 dpi' WHERE `id`='483';
			UPDATE {{attributeclass}} SET `tooltipCustom`='Для печати необходимо предоставить макет в формате TIFF 300 dpi' WHERE `id`='484';
			UPDATE {{attributeclass}} SET `tooltipCustom`='Для печати необходимо предоставить макет в формате TIFF 300 dpi' WHERE `id`='485';
			
			CREATE TABLE IF NOT EXISTS {{proposalelementclasshasnotification}} (
			  `id` INT NOT NULL,
			  `elementId` INT NULL,
			  `notificationId` INT NULL,
		  	  PRIMARY KEY (`id`));
		  	  
		  	ALTER TABLE {{proposalelementclasshasnotification}} 
			CHANGE COLUMN `elementId` `proposalElementClassId` INT(11) NULL DEFAULT NULL ;
			
			ALTER TABLE {{proposalelementclasshasnotification}} 
			CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
	    ";
	}
}