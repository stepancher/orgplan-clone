<?php

class m160925_153341_gradoteka_industry_2 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500069', '2523', 'Экспорт', 'derivative', 'ДолларСША', '1');
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500071', '2523', 'Импорт', 'derivative', 'ДолларСША', '1');
            
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500069', 'aggregatable', 'SUM');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500071', 'aggregatable', 'SUM');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','1','5745eb7e74d897cb088b60bf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','2','5745ee7a74d897cb0892967d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','3','5745eb7e74d897cb088b60c3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','4','5745ef4474d897cb08945322');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','5','5745ec9474d897cb088e4830');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','6','5745eb7e74d897cb088b60c7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','7','5745ebbe74d897cb088c32ef');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','8','5745ebbe74d897cb088c32f7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','9','5745ebbe74d897cb088c32fb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','10','5745eb7e74d897cb088b60cb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','11','5745ec4c74d897cb088d9cfb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','12','5745ec4c74d897cb088d9cff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','13','5745ec9474d897cb088e4848');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','14','5745ec9474d897cb088e4838');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','15','5745ed1774d897cb088f7606');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','16','5745ec9474d897cb088e484e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','17','5745ec0474d897cb088ce991');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','18','5745ecd374d897cb088eda3a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','19','5745ee7a74d897cb0892968f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','20','5745ebbe74d897cb088c3305');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','21','5745ed5c74d897cb089012c9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','22','5745ed5c74d897cb089012cd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','23','5745ebbe74d897cb088c3309');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','24','5745eb7e74d897cb088b60cf');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','25','5745ebbe74d897cb088c330d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','26','5745ec0474d897cb088ce98b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','27','5745ecd374d897cb088eda32');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','28','5745ebbe74d897cb088c32f3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('32','29','5745ebbe74d897cb088c3301');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','1','5745f0b574d897cb08981f06');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','2','5745f0b574d897cb08981f0a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','3','5745f23274d897cb089b703a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','4','5745ef9074d897cb08950a91');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','5','5745f0b574d897cb08981f0e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','6','5745ef9074d897cb08950a99');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','7','5745eff574d897cb08965334');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','8','5745f0b574d897cb08981f1c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','9','5745f23274d897cb089b7050');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','10','5745ef9074d897cb08950aa9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','11','5745f05574d897cb08973feb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','12','5745ef9074d897cb08950aad');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','13','5745ef9074d897cb08950ab5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','14','5745ef9074d897cb08950a95');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','15','5745ef9074d897cb08950ab1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','16','5745ef9074d897cb08950abd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','17','5745ef9074d897cb08950ab9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','18','5745f05574d897cb08973fff');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','19','5745ef9174d897cb08950ac1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','20','5745eff574d897cb0896534e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','21','5745f0b574d897cb08981f02');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','22','5745ef9174d897cb08950ac5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','23','5745ef9074d897cb08950aa5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','24','5745eff574d897cb08965360');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','25','5745f11674d897cb0898f9c1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','26','5745eff574d897cb08965358');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','27','5745eff574d897cb0896535c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','28','5745f05574d897cb0897400d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','29','5745f11674d897cb0898f9a3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','30','5745ef9074d897cb08950aa1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','31','5745eff574d897cb0896533e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','32','5745f05574d897cb08973fe3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','33','5745eff574d897cb08965354');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','34','5745ef9074d897cb08950a9d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('33','35','5745f29174d897cb089c3f35');
            
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500069', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500071', 'country');
            
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68809', '2016-09-25 18:41:21', '102525000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68809', '2016-09-25 18:41:21', '7278130', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68810', '2016-09-25 18:42:38', '148871000', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68810', '2016-09-25 18:42:38', '12718900', '2016');
            
            UPDATE {{charttablegroup}} SET `name`='Основные показатели отрасли' WHERE `id`='15';
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Показатели производства', '2', '2');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Косвенные показатели отрасли (ипотека)', '2', '3');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Экспорт\\Импорт', '2', '4');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Таможенная справка', '2', '5');
            
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по округам' WHERE `id`='15';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по регионам', `position`='2' WHERE `id`='18';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по округам/рентабельность', `groupId`='15', `position`='3' WHERE `id`='16';
            UPDATE {{charttable}} SET `name`='Жилищные и ипотечные кредиты по округам', `groupId`='25', `position`='1' WHERE `id`='17';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли/рентабельность по регионам', `groupId`='15', `position`='4' WHERE `id`='19';
            UPDATE {{charttable}} SET `name`='Жилищные и ипотечные кредиты по регионам', `groupId`='25', `position`='2' WHERE `id`='20';
            UPDATE {{charttable}} SET `name`='Производство строительных материалов по округам', `groupId`='24', `position`='1' WHERE `id`='21';
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Производство строительных материалов по регионам', '3', '2', 'region', '24', '2');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b78bbd4c56dc4f8b72e7', '', '', '4', '2015', '87', '#4ec882', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `epoch`, `tableId`, `color`, `position`) VALUES ('5784b78bbd4c56dc4f8b72e7', 'Производство цемента', '', '2015', '87', '#4ec882', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e448d77d89702498bdb5d', '', '', '4', '2015', '87', '#42c0b4', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e448d77d89702498bdb5d', 'Производство железобетонных конструкций', '', '2015', '87', '#a0d468', '4');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e446c77d89702498bc5ed', '', '', '4', '2015', '87', '#2b98d5', '5');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e446c77d89702498bc5ed', 'Производство кирпича', '', '2015', '87', '#a0d468', '6');
            
            DELETE FROM {{charttablegroup}} WHERE `id`='27';
            
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Экспорт\\Импорт', '3', '2', 'country', '26', '1');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('11', 'Экспорт РФ', '2015', '88', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('12', 'Импорт РФ', '2015', '88', '#a0d468', '2');
            
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`, `industryId`) VALUES ('379', 'Цемент', '1', '2');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('18', '68809');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('18', '68810');
            
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('11', '500069', 'Доллар США');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('12', '500071', 'Доллар США');
            
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500073', '6809', 'Экспорт', 'derivative', 'ДолларСША', '1');
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500075', '6809', 'Импорт', 'derivative', 'ДолларСША', '1');
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503238';
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503237';
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503236';
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503235';
            
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500073', 'aggregatable', 'SUM');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500075', 'aggregatable', 'SUM');
            
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','1','5745eb9774d897cb088bbb5b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','2','5745ec2074d897cb088d31dc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','3','5745eced74d897cb088f17bb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','4','5745ebda74d897cb088c7e3a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','5','5745eb9874d897cb088bbb5f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','6','5745ec6874d897cb088de3ae');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','7','5745eb9874d897cb088bbb67');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','8','5745ecac74d897cb088e83bc');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','9','5745eedb74d897cb08937158');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','10','5745ee9874d897cb0892dbc9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','11','5745ed7674d897cb08904fbd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','12','5745eb9874d897cb088bbb6f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','13','5745ec6874d897cb088de3aa');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','14','5745eced74d897cb088f17c1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','15','5745eb9874d897cb088bbb6b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','16','5745eedb74d897cb08937152');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','17','5745ee4e74d897cb0892360b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','18','5745ee0674d897cb0891949f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','19','5745ec6874d897cb088de3b6');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','20','5745eb9874d897cb088bbb73');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','21','5745eced74d897cb088f17cb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','22','5745ef6174d897cb089495da');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','23','5745ee9874d897cb0892dbdb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','24','5745ee0674d897cb08919493');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','25','5745ec6874d897cb088de3c4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','26','5745ec2074d897cb088d31ee');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','27','5745ee0674d897cb089194a9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','28','5745ecac74d897cb088e83ce');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','29','5745ec6874d897cb088de3c0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','30','5745ed3174d897cb088fb282');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','31','5745ec2074d897cb088d31e2');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','32','5745ebda74d897cb088c7e40');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','33','5745eb9874d897cb088bbb63');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('34','34','5745ebda74d897cb088c7e4a');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','1','5745f3a874d897cb089e25d7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','2','5745f07c74d897cb08979eeb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','3','5745f07c74d897cb08979eef');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','4','5745efb974d897cb089599a1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','5','5745f1f974d897cb089af767');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','6','5745efb974d897cb089599a5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','7','5745f45e74d897cb089fb059');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','8','5745f0dc74d897cb08987c6a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','9','5745f1f974d897cb089af76b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','10','5745f01b74d897cb0896b884');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','11','5745efb974d897cb089599a9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','12','5745f2b774d897cb089c9705');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','13','5745efb974d897cb0895999d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','14','5745f1f974d897cb089af773');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','15','5745efb974d897cb089599ad');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','16','5745f19974d897cb089a225f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','17','5745f25874d897cb089bc866');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','18','5745f01b74d897cb0896b896');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','19','5745efb974d897cb089599b1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','20','5745f31674d897cb089d6828');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','21','5745f25874d897cb089bc86c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','22','5745f01b74d897cb0896b890');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','23','5745efb974d897cb089599b9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','24','5745f0dc74d897cb08987c78');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','25','5745f01b74d897cb0896b8a4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','26','5745f01b74d897cb0896b89c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','27','5745f01b74d897cb0896b8a0');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','28','5745efb974d897cb089599b5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','29','5745f01b74d897cb0896b888');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','30','5745f01b74d897cb0896b88c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','31','5745f07c74d897cb08979ef9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','32','5745f07c74d897cb08979ef3');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('35','33','5745f0dc74d897cb08987c70');
            
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68811', '2016-09-25 19:09:39', '23528200', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68811', '2016-09-25 19:09:39', '4435100', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68812', '2016-09-25 19:10:56', '708324', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68812', '2016-09-25 19:10:56', '6098010', '2015');
            
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500073', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500075', 'country');
            
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`, `industryId`) VALUES ('379', 'Изделия из гипса', '2', '2');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('19', '68811');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('19', '68812');
            
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('11', '500073', 'Доллар США');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('12', '500075', 'Доллар США');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}