<?php

class m160627_124223_fix_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES (\'651\', \'unitTitle\');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES (\'1990\', \'ru\', \'м³\');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES (\'1990\', \'en\', \'м³\');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES (\'653\', NULL, \'unitTitle\');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, \'1991\', \'ru\', \'м³\');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, \'1991\', \'en\', \'м³\');

		';
	}
}