<?php

class m160727_120400_add_tr_country extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('3', 'ru', 'Азербайджан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('4', 'ru', 'Армения');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('5', 'ru', 'Беларусь');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('6', 'ru', 'Казахстан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('7', 'ru', 'Киргизия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('8', 'ru', 'Молдова');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('9', 'ru', 'Узбекистан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('10', 'ru', 'Туркменистан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('11', 'ru', 'Грузия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('12', 'ru', 'Монголия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('13', 'ru', 'Украина');
			
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('3', 'en', 'Азербайджан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('4', 'en', 'Армения');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('5', 'en', 'Беларусь');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('6', 'en', 'Казахстан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('7', 'en', 'Киргизия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('8', 'en', 'Молдова');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('9', 'en', 'Узбекистан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('10', 'en', 'Туркменистан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('11', 'en', 'Грузия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('12', 'en', 'Монголия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('13', 'en', 'Украина');
			
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('3', 'de', 'Азербайджан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('4', 'de', 'Армения');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('5', 'de', 'Беларусь');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('6', 'de', 'Казахстан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('7', 'de', 'Киргизия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('8', 'de', 'Молдова');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('9', 'de', 'Узбекистан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('10', 'de', 'Туркменистан');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('11', 'de', 'Грузия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('12', 'de', 'Монголия');
			INSERT INTO {{trcountry}} (`trParentId`, `langId`, `name`) VALUES ('13', 'de', 'Украина');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}