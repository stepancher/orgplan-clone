<?php

class m151016_082146_changeFields extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down(){

		$sql = '
			ALTER TABLE {{equipmentparams}} CHANGE `width` `width` INT DEFAULT NULL;
			ALTER TABLE {{equipmentparams}} CHANGE `height` `height` INT DEFAULT NULL;
			ALTER TABLE {{equipmentparams}} CHANGE `length` `length` INT DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `width` `width` INT DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `square` `square` INT DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `length` `length` INT DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `locationType` `locationType` INT DEFAULT NULL;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getAlterTable(){
		return '
			ALTER TABLE {{equipmentparams}} CHANGE `width` `width` float DEFAULT NULL;
			ALTER TABLE {{equipmentparams}} CHANGE `height` `height` float DEFAULT NULL;
			ALTER TABLE {{equipmentparams}} CHANGE `length` `length` float DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `width` `width` float DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `square` `square` float DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `length` `length` float DEFAULT NULL;
			ALTER TABLE {{identityofstand}} CHANGE `locationType` `locationType` varchar(255) DEFAULT NULL;
		';
	}
}