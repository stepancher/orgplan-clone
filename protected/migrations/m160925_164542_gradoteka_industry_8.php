<?php

class m160925_164542_gradoteka_industry_8 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{charttablegroup}} SET `name`='Основные показатели отрасли' WHERE `id`='16';
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Показатели производства', '8', '2');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Сбытовая статистика (расходы населения на питание)', '8', '3');

            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по округам/рентабельность' WHERE `id`='22';
            UPDATE {{charttable}} SET `name`='Количественные показатели по округам', `groupId`='16', `position`='3' WHERE `id`='23';
            UPDATE {{charttable}} SET `name`='Основные показатели отрасли по регионам/рентабельность', `groupId`='16', `position`='2' WHERE `id`='24';
            UPDATE {{charttable}} SET `name`='Количественные показатели по регионам', `groupId`='16', `position`='4' WHERE `id`='25';
            DELETE FROM {{charttablegroup}} WHERE `id`='28';
            DELETE FROM {{charttablegroup}} WHERE `id`='29';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}