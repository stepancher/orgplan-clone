<?php

class m170313_074601_add_exhibitioncomplex_status extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{exhibitioncomplexstatus}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `color` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            ALTER TABLE {{exhibitioncomplex}} 
            ADD COLUMN `statusId` INT(11) NULL DEFAULT NULL AFTER `regionId`;
            
            INSERT INTO {{exhibitioncomplexstatus}} (`name`) VALUES ('Статус отсутствует');
            INSERT INTO {{exhibitioncomplexstatus}} (`name`) VALUES ('На перевод');
            INSERT INTO {{exhibitioncomplexstatus}} (`name`) VALUES ('В работу');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}