<?php

class m160526_154745_create_tr_for_documents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			CREATE TABLE {{trdocuments}} (
				`id` INT NOT NULL AUTO_INCREMENT,
				`trParentId` VARCHAR(255) NULL,
				`name` VARCHAR(255) NULL,
				`description` TEXT(65535) NULL,
				`value` VARCHAR(255) NULL,
			PRIMARY KEY (`id`))
			ENGINE = InnoDB
			DEFAULT CHARACTER SET = utf8;
			ALTER TABLE {{trdocuments}} 
			ADD COLUMN `langId` VARCHAR(45) NULL AFTER `trParentId`;
			
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('1', 'ru', 'Анкета посетителя выставки', 'Описание анкеты посетителя выставки', '/ru/site/questionnaire/');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('2', 'ru', 'Форма обработки конткатов на выставке', 'Описание формы обработки конткатов на выставке', '/ru/site/DocumentContacts/');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('3', 'ru', 'Список экспонатов', 'Описание формы Список экспонатов', '/ru/site/DocumentExhibits/');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('4', 'ru', 'Положение о конкурсе', '/static/documents/fairs/184/polozhenie_o_konkurse.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('5', 'ru', 'Правила выставки', '/static/documents/fairs/184/pravila_vystavki.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('6', 'ru', 'Схема проезда', '/static/documents/fairs/184/shema-proezda.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('7', 'ru', 'Схема размещения оборудования на стенде', '/static/documents/fairs/184/stand_plan.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('8', 'ru', 'AUMA manual', '/static/documents/fairs/10556/AUMA_manual.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('9', 'ru', 'Fairgrounds with Icons', '/static/documents/fairs/10556/Fachgruppenplan_Agritechnica-2015.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('10', 'ru', 'The Key Areas of the Exhibition', '/static/documents/fairs/10556/AGT-Key-Areas_2015_EN.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('11', 'ru', 'Fairgrounds with parking places and bus', '/static/documents/fairs/10556/Gelaendeplan_AgriTechnica2015_Bus.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('12', 'ru', 'Requirements and Rules', '/static/documents/fairs/184/requirements_and_rules.pdf');
			INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('13', 'ru', 'Инструкция для Экспонента', '/static/documents/fairs/184/instruction_for_exponent.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `description`, `value`) VALUES (NULL, '1', 'en', 'Анкета посетителя выставки', 'Описание анкеты посетителя выставки', '/ru/site/questionnaire/');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `description`, `value`) VALUES (NULL, '2', 'en', 'Форма обработки конткатов на выставке', 'Описание формы обработки конткатов на выставке', '/ru/site/DocumentContacts/');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `description`, `value`) VALUES (NULL, '3', 'en', 'Список экспонатов', 'Описание формы Список экспонатов', '/ru/site/DocumentExhibits/');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '4', 'en', 'Положение о конкурсе', '/static/documents/fairs/184/polozhenie_o_konkurse.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '5', 'en', 'Правила выставки', '/static/documents/fairs/184/pravila_vystavki.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '6', 'en', 'Схема проезда', '/static/documents/fairs/184/shema-proezda.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '7', 'en', 'Схема размещения оборудования на стенде', '/static/documents/fairs/184/stand_plan.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '8', 'en', 'AUMA manual', '/static/documents/fairs/10556/AUMA_manual.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '9', 'en', 'Fairgrounds with Icons', '/static/documents/fairs/10556/Fachgruppenplan_Agritechnica-2015.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '10', 'en', 'The Key Areas of the Exhibition', '/static/documents/fairs/10556/AGT-Key-Areas_2015_EN.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '11', 'en', 'Fairgrounds with parking places and bus', '/static/documents/fairs/10556/Gelaendeplan_AgriTechnica2015_Bus.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '12', 'en', 'Requirements and Rules', '/static/documents/fairs/184/requirements_and_rules.pdf');
			INSERT INTO {{trdocuments}} (`id`, `trParentId`, `langId`, `name`, `value`) VALUES (NULL, '13', 'en', 'Инструкция для Экспонента', '/static/documents/fairs/184/instruction_for_exponent.pdf');
			
			ALTER TABLE {{documents}} 
			DROP COLUMN `value`,
			DROP COLUMN `description`,
			DROP COLUMN `name`;
		";
	}
}