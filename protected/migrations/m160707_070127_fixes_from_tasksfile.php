<?php

class m160707_070127_fixes_from_tasksfile extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">УБОРЩИК ДЛЯ ОЧИСТКИ ЭКСПОНАТОВ И ОБОРУДОВАНИЯ</div><div class=\"body\">Предполагает мойку твердого покрытия пола (ламинат, плитка), чистку стекол, удаление пыли с экспонатовю</div>' WHERE `id`='2649';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2148', '0', 'showTooltip');
			UPDATE {{trattributeclass}} SET `label`='Ноутбук (включая комплект презентационных программ)' WHERE `id`='1465';
			UPDATE {{trattributeclass}} SET `label`='Ноутбук (включая комплект презентационных программ)' WHERE `id`='1588';
			UPDATE {{trattributeclass}} SET `label`='Стойка (восьмигранный опорный профиль) Н=1100(c), 750(b), 480(a)' WHERE `id`='404';
			UPDATE {{trattributeclass}} SET `label`='Стойка (восьмигранный опорный профиль) Н=2480(f), 2070(e), 1600(d)' WHERE `id`='403';
			UPDATE {{trattributeclass}} SET `label`='Европанель с перфорацией, навесная, 900 х 2000мм(1000мм) без крючков' WHERE `id`='353';
			UPDATE {{trattributeclass}} SET `label`='Дополнительная стеклянная полка (500 х 1000, 500 х 500) в витрину 398, 398а' WHERE `id`='342';
			UPDATE {{trattributeclass}} SET `unitTitle`='sq.m.' WHERE `id`='1237';
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2624', 'en', 'Отправьте заполненную форму в дирекцию выставки не позднее 17 сентября 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2625', 'en', 'Пропуск в зону разгрузки-погрузки');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2638', 'en', 'На период монтажа-демонтажа мероприятия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2639', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2640', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2641', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2642', 'en', 'При заказе пропуска на мультикран (манипулятор и пр.) разрешается разгрузка-погрузка только груза прибывшего на самом транспортном средстве.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2643', 'en', 'Норматив времени нахождения транспорта в зоне погрузки-разгрузки: для легковых автомобилей — 0,5 часа, для грузовых автомобилей — 1 час. При превышении норматива необходимы повторный заказ и оплата пропуска.\r\n\r\nДля проезда в зону разгрузки-погрузки транспортных средств, в отношении которых заказана разгрузка/погрузка силами «Крокус Экспо» (заполнена и оплачена заявка №9) необходимо получить БЕСПЛАТНЫЙ пропуск для получения которого представителю компании необходимо обратиться в сервис-центр «Крокус Экспо» не позже чем за один час до начала работ в соответствии с графиком разгрузки погрузки.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2644', 'en', 'Пропуск на VIP стоянку (только легковые автомобили)');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2645', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2646', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2647', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2648', 'en', 'На один день');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `placeholder`) VALUES ('2649', 'en', 'Выберите дату');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2650', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2651', 'en', 'На один день');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `placeholder`) VALUES ('2652', 'en', 'Выберите дату');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2653', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2654', 'en', 'На один день');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `placeholder`) VALUES ('2655', 'en', 'Выберите дату');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2656', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2657', 'en', 'На один день');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `placeholder`) VALUES ('2658', 'en', 'Выберите дату');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2660', 'en', 'Транспортные средства с нанесенными рекламными изображениями, логотипами, а также оборудованные рекламными конструкциями на стоянку не допускаются.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2661', 'en', 'До 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ заказываются у Организатора.\r\nПосле 17 сентября 2016 г. пропуска в зону проведения разгрузо-погрузочных работ могут быть приобретены как у Организатора, так и через сервис-центр МВЦ «Крокус Экспо». При приобретении пропусков через сервис-центр их стоимость определяется непосредственно МВЦ «Крокус Экспо». Для оформления пропусков необходимо указать тип и государственный номер транспортного средства.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2663', 'en', 'Все цены включают НДС 18%.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2664', 'en', 'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n<br><br>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.\n<br><br>\nСтоимость услуг увеличивается на 100% при заказе после 17.09.16 г.\n<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 07 октября 2016г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2659', 'en', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2129', 'en', 'Легковые автомобили');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2130', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2132', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2134', 'en', 'Грузовые автомобили');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2135', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2137', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2139', 'en', 'Мульткран (манипулятор и пр.)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2140', 'en', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2142', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2144', 'en', 'На период монтажа и демонтажа');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2145', 'en', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2147', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2149', 'en', 'На период проведения мероприятия');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2150', 'en', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2152', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2154', 'en', 'На общий период проведения мероприятия включая монтаж и демонтаж');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2155', 'en', 'шт. ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2157', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2162', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2163', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2164', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2165', 'en', 'руб');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2166', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2167', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2168', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2169', 'en', 'шт.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2170', 'en', 'руб');
			UPDATE {{proposalelementclass}} SET `active`='1' WHERE `id`='25';

		";
	}
}