<?php

class m160818_073857_delete_search_query extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbRuntime->beginTransaction();
        try {
            Yii::app()->dbRuntime->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbRuntime->beginTransaction();
        try {
            Yii::app()->dbRuntime->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP TABLE {{searchquery}};
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}