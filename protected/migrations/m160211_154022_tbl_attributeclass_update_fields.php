<?php

class m160211_154022_tbl_attributeclass_update_fields extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			UPDATE {{attributeclass}} SET `unitTitle`=\'шт.\' WHERE `id`=\'151\';
			UPDATE {{attributeclass}} SET `unitTitle`=\'шт.\' WHERE `id`=\'152\';
			UPDATE {{attributeclass}} SET `unitTitle`=\'шт.\' WHERE `id`=\'153\';
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `unitTitle`='п.м.' WHERE `id`='151';
			UPDATE {{attributeclass}} SET `unitTitle`='п.м.' WHERE `id`='152';
			UPDATE {{attributeclass}} SET `unitTitle`='п.м.' WHERE `id`='153';
		";
	}
}