<?php

class m160706_061434_add_countrysummary extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
				CREATE TABLE {{countrysummary}} (
				`id` INT(11) NOT NULL,
				`countryId` INT(11) NOT NULL,
				`fairsCount` INT(11) NULL,
				`exhibitionComplexCount` INT(11) NULL,
				`datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`),
				UNIQUE INDEX `countryId_UNIQUE` (`countryId` ASC),
				CONSTRAINT `fk_countryId`
				FOREIGN KEY (`countryId`)
				REFERENCES {{country}} (`id`)
				ON DELETE CASCADE
				ON UPDATE CASCADE);
				
				ALTER TABLE {{countrysummary}} 
				CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

				ALTER TABLE {{citysummary}} 
				CHANGE COLUMN `fairsCount` `fairsCount` INT(11) NULL ,
				CHANGE COLUMN `exhibitionComplexCount` `exhibitionComplexCount` INT(11) NULL ;
				
				ALTER TABLE {{regionsummary}} 
				CHANGE COLUMN `fairsCount` `fairsCount` INT(11) NULL ,
				CHANGE COLUMN `exhibitionComplexCount` `exhibitionComplexCount` INT(11) NULL ;

				ALTER TABLE {{industrysummary}} 
				CHANGE COLUMN `fairsCount` `fairsCount` INT(11) NULL ;
";
	}

	public function downSql()
	{
		return "
				DROP TABLE {{countrysummary}};
				
				ALTER TABLE {{citysummary}} 
				CHANGE COLUMN `fairsCount` `fairsCount` INT(11) NOT NULL ,
				CHANGE COLUMN `exhibitionComplexCount` `exhibitionComplexCount` INT(11) NOT NULL ;

				ALTER TABLE {{regionsummary}} 
				CHANGE COLUMN `fairsCount` `fairsCount` INT(11) NOT NULL ,
				CHANGE COLUMN `exhibitionComplexCount` `exhibitionComplexCount` INT(11) NOT NULL ;

				ALTER TABLE {{industrysummary}} 
				CHANGE COLUMN `fairsCount` `fairsCount` INT(11) NOT NULL ;
		";
	}
}