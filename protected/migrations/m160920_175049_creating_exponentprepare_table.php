<?php

class m160920_175049_creating_exponentprepare_table extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{exponentprepare}} (
              `id` INT NOT NULL AUTO_INCREMENT,
              `participationTarget` INT NULL,
              `guestsCount` INT NULL,
              `staffCount` INT NULL,
              `standSquare` INT NULL,
              `standExponats` INT NULL,
              `fairEvents` INT NULL,
              `fairAdvertisement` INT NULL,
              `katering` INT NULL,
              `standaloneKatering` INT NULL,
              `polygraphy` INT NULL,
              `hiredStaff` INT NULL,
              `stand` INT NULL,
              `standType` INT NULL,
              `technicalCommunications` INT NULL,
              `multimedia` INT NULL,
              `stand3d` INT NULL,
              `compaign` INT NULL,
              `printInvites` INT NULL,
              `internetAdvance` INT NULL,
              `massMediaAdvance` INT NULL,
              `briefing` INT NULL,
              `debriefing` INT NULL,
              `souvenirProduction` INT NULL,
              `sendingThankYouLetter` INT NULL,
              `visitorsBase` INT NULL,
              PRIMARY KEY (`id`));
              
              
            ALTER TABLE {{exponentprepare}}
            ADD COLUMN `userId` INT NULL AFTER `visitorsBase`;
            ALTER TABLE {{exponentprepare}}
            ADD COLUMN `fairId` INT NULL AFTER `userId`;

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}