<?php

class m160417_124643_proposal_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `class`='double' WHERE `id`='797';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('653', '797', 'multiple');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`) VALUES ('651', '798', 'multiple');
			UPDATE {{attributeclass}} SET `label`='Выбранное оборудование:' WHERE `id`='520';
		";
	}
}