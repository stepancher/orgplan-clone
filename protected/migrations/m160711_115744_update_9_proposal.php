<?php

class m160711_115744_update_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "			
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='663';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='754';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='755';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='756';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='757';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='758';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='759';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='760';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='761';
			UPDATE {{attributeclass}} SET `class`='spoiler' WHERE `id`='762';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='663';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='754';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='755';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='756';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='757';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='758';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='759';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='760';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='761';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='762';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='754';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='755';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='756';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='757';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='758';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='759';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='760';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='761';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='762';
			UPDATE {{attributeclass}} SET `class`='spoilerWithWrap' WHERE `id`='663';
		";
	}
}