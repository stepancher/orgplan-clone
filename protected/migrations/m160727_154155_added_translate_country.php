<?php

class m160727_154155_added_translate_country extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{trcountry}} SET `name`='Azerbaijan' WHERE `id`='18';
			UPDATE {{trcountry}} SET `name`='Aserbaidschan' WHERE `id`='29';
			UPDATE {{trcountry}} SET `name`='Armenia' WHERE `id`='19';
			UPDATE {{trcountry}} SET `name`='Armenien' WHERE `id`='30';
			UPDATE {{trcountry}} SET `name`='Belarus' WHERE `id`='20';
			UPDATE {{trcountry}} SET `name`='Weißrussland' WHERE `id`='31';
			UPDATE {{trcountry}} SET `name`='Kazakhstan' WHERE `id`='21';
			UPDATE {{trcountry}} SET `name`='Kasachstan' WHERE `id`='32';
			UPDATE {{trcountry}} SET `name`='Kyrgyzstan' WHERE `id`='22';
			UPDATE {{trcountry}} SET `name`='Kirgisistan' WHERE `id`='33';
			UPDATE {{trcountry}} SET `name`='Moldova' WHERE `id`='23';
			UPDATE {{trcountry}} SET `name`='Moldawien' WHERE `id`='34';
			UPDATE {{trcountry}} SET `name`='Uzbekistan' WHERE `id`='24';
			UPDATE {{trcountry}} SET `name`='Usbekistan' WHERE `id`='35';
			UPDATE {{trcountry}} SET `name`='Turkmenistan' WHERE `id`='36';
			UPDATE {{trcountry}} SET `name`='Turkmenistan' WHERE `id`='25';
			UPDATE {{trcountry}} SET `name`='Georgia' WHERE `id`='26';
			UPDATE {{trcountry}} SET `name`='Georgien' WHERE `id`='37';
			UPDATE {{trcountry}} SET `name`='Mongolia' WHERE `id`='27';
			UPDATE {{trcountry}} SET `name`='Mongolei' WHERE `id`='38';
			UPDATE {{trcountry}} SET `name`='Ukraine' WHERE `id`='28';
			UPDATE {{trcountry}} SET `name`='Ukraine' WHERE `id`='39';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}