<?php

class m161003_200343_hotfix_for_unavailable_stats extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{gobjectshasgstattypes}} (`id`, `objId`, `statId`, `gType`) VALUES ('83904', '319', '577e7c79bd4c562e658b4684', 'region');
            INSERT INTO {{gvalue}} (`setId`, `value`, `epoch`) VALUES ('83904', NULL, '2015');
            INSERT INTO {{gobjectshasgstattypes}} (`id`, `objId`, `statId`, `gType`) VALUES ('83905', '319', '577e6f17bd4c5654298b540e', 'region');
            INSERT INTO {{gvalue}} (`setId`, `value`, `epoch`) VALUES ('83905', NULL, '2015');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}