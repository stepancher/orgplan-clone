<?php

class m170512_062044_add_new_cities extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('565', 'geelong');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1589', 'ru', 'Джилонг');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1589', 'en', 'Geelong');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1589', 'de', 'Geelong');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1091', 'molln');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1590', 'ru', 'Мёльн');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1590', 'en', 'Molln');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1590', 'de', 'Mölln');
            
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('157', 'grevenbroich');
            
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1591', 'ru', 'Гревенброх');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1591', 'en', 'Grevenbroich');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1591', 'de', 'Grevenbroich');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}