<?php

class m170220_104459_copy_exdb_association_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_associations_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_associations_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE ass_name TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT exa.exdbId, exa.name FROM {$expodata}.{{exdbassociation}} exa
                                        LEFT JOIN {$db}.{{association}} a ON a.exdbId = exa.id
                                        WHERE a.id IS NULL
                                        ORDER BY exa.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                    read_loop: LOOP
                        
                        FETCH copy INTO ass_id, ass_name;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{association}} (`exdbId`,`name`) VALUES (ass_id, ass_name);
                        COMMIT;
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_associations_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}