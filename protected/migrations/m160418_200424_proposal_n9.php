<?php

class m160418_200424_proposal_n9 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_e1_checkbox', 'bool', '0', 'Кран', '663', '663', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e1_checkbox2', 'bool', '0', 'Погрузчик', '663', '663', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e1_checkbox3', 'bool', '0', 'Самоходный', '663', '663', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e2_checkbox', 'bool', '0', 'Кран', '754', '754', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e2_checkbox2', 'bool', '0', 'Погрузчик', '754', '754', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e2_checkbox3', 'bool', '0', 'Самоходный', '754', '754', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e3_checkbox', 'bool', '0', 'Кран', '755', '755', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e3_checkbox2', 'bool', '0', 'Погрузчик', '755', '755', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e3_checkbox3', 'bool', '0', 'Самоходный', '755', '755', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e4_checkbox', 'bool', '0', 'Кран', '756', '756', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e4_checkbox2', 'bool', '0', 'Погрузчик', '756', '756', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e4_checkbox3', 'bool', '0', 'Самоходный', '756', '756', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e5_checkbox', 'bool', '0', 'Кран', '757', '757', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e5_checkbox2', 'bool', '0', 'Погрузчик', '757', '757', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e5_checkbox3', 'bool', '0', 'Самоходный', '757', '757', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e6_checkbox', 'bool', '0', 'Кран', '758', '758', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e6_checkbox2', 'bool', '0', 'Погрузчик', '758', '758', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e6_checkbox3', 'bool', '0', 'Самоходный', '758', '758', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e7_checkbox', 'bool', '0', 'Кран', '759', '759', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e7_checkbox2', 'bool', '0', 'Погрузчик', '759', '759', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e7_checkbox3', 'bool', '0', 'Самоходный', '759', '759', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e8_checkbox', 'bool', '0', 'Кран', '760', '760', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e8_checkbox2', 'bool', '0', 'Погрузчик', '760', '760', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e8_checkbox3', 'bool', '0', 'Самоходный', '760', '760', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e9_checkbox', 'bool', '0', 'Кран', '761', '761', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e9_checkbox2', 'bool', '0', 'Погрузчик', '761', '761', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e9_checkbox3', 'bool', '0', 'Самоходный', '761', '761', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e10_checkbox', 'bool', '0', 'Кран', '762', '762', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e10_checkbox2', 'bool', '0', 'Погрузчик', '762', '762', '0', 'checkboxSwitcher', '1');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_e10_checkbox3', 'bool', '0', 'Самоходный', '762', '762', '0', 'checkboxSwitcher', '1');
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='930';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='931';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='932';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='933';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='934';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='935';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='936';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='937';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='938';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='939';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='940';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='941';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='942';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='943';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='944';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='945';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='946';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='947';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='948';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='949';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='950';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='951';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='952';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='953';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='954';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='955';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='956';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='957';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='958';
		UPDATE {{attributeclass}} SET `priority`='5' WHERE `id`='959';
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES (NULL, 'n9_e1_hours', 'double', '0', '0', '', '930', '663', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e1_hours2', 'double', '0', '0', '', '931', '663', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e2_hours', 'double', '0', '0', '', '933', '754', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e2_hours2', 'double', '0', '0', '', '934', '754', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e3_hours', 'double', '0', '0', '', '936', '755', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e3_hours2', 'double', '0', '0', '', '937', '755', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e4_hours', 'double', '0', '0', '', '939', '756', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e4_hours2', 'double', '0', '0', '', '940', '756', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e5_hours', 'double', '0', '0', '', '942', '757', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e5_hours2', 'double', '0', '0', '', '943', '757', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e6_hours', 'double', '0', '0', '', '945', '758', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e6_hours2', 'double', '0', '0', '', '946', '758', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e7_hours', 'double', '0', '0', '', '948', '759', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e7_hours2', 'double', '0', '0', '', '949', '759', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e8_hours', 'double', '0', '0', '', '951', '760', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e8_hours2', 'double', '0', '0', '', '952', '760', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e9_hours', 'double', '0', '0', '', '954', '761', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e9_hours2', 'double', '0', '0', '', '955', '761', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e10_hours', 'double', '0', '0', '', '957', '762', '0', 'double', '0', 'час');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`) VALUES ('n9_e10_hours2', 'double', '0', '0', '', '958', '762', '0', 'double', '0', 'час');
		DELETE FROM {{attributeclass}} WHERE `id`='678';
		DELETE FROM {{attributeclass}} WHERE `id`='687';
		DELETE FROM {{attributeclass}} WHERE `id`='696';
		DELETE FROM {{attributeclass}} WHERE `id`='705';
		DELETE FROM {{attributeclass}} WHERE `id`='714';
		DELETE FROM {{attributeclass}} WHERE `id`='723';
		DELETE FROM {{attributeclass}} WHERE `id`='732';
		DELETE FROM {{attributeclass}} WHERE `id`='741';
		DELETE FROM {{attributeclass}} WHERE `id`='750';
	    ";
	}
}