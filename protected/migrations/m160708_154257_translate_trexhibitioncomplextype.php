<?php

class m160708_154257_translate_trexhibitioncomplextype extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`) VALUES ('1', 'en', 'Business, trade-recreation centers');
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`) VALUES ('2', 'en', 'Sport, science, culture centers');
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`) VALUES ('3', 'en', 'Hotels, congress halls');
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`) VALUES ('4', 'en', 'Exhibition centers');
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`) VALUES ('5', 'en', 'Theaters, museums, galleries');
			INSERT INTO {{trexhibitioncomplextype}} (`trParentId`, `langId`, `name`) VALUES ('6', 'en', 'Specific venues');
 		";
	}

	public function downSql()
	{
		return TRUE;
	}
}