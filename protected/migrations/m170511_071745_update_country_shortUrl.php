<?php

class m170511_071745_update_country_shortUrl extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{country}} SET shortUrl = 'russia'               WHERE id = 1;
            UPDATE {{country}} SET shortUrl = 'germany'              WHERE id = 2;
            UPDATE {{country}} SET shortUrl = 'azerbaijan'           WHERE id = 3;
            UPDATE {{country}} SET shortUrl = 'armenia'              WHERE id = 4;
            UPDATE {{country}} SET shortUrl = 'belarus'              WHERE id = 5;
            UPDATE {{country}} SET shortUrl = 'kazakhstan'           WHERE id = 6;
            UPDATE {{country}} SET shortUrl = 'kyrgyzstan'           WHERE id = 7;
            UPDATE {{country}} SET shortUrl = 'moldova'              WHERE id = 8;
            UPDATE {{country}} SET shortUrl = 'uzbekistan'           WHERE id = 9;
            UPDATE {{country}} SET shortUrl = 'turkmenistan'         WHERE id = 10;
            UPDATE {{country}} SET shortUrl = 'georgia'              WHERE id = 11;
            UPDATE {{country}} SET shortUrl = 'mongolia'             WHERE id = 12;
            UPDATE {{country}} SET shortUrl = 'ukraine'              WHERE id = 13;
            UPDATE {{country}} SET shortUrl = 'serbia'               WHERE id = 14;
            UPDATE {{country}} SET shortUrl = 'vietnam'              WHERE id = 15;
            UPDATE {{country}} SET shortUrl = 'australia'            WHERE id = 16;
            UPDATE {{country}} SET shortUrl = 'austria'              WHERE id = 17;
            UPDATE {{country}} SET shortUrl = 'algeria'              WHERE id = 18;
            UPDATE {{country}} SET shortUrl = 'argentina'            WHERE id = 19;
            UPDATE {{country}} SET shortUrl = 'bangladesh'           WHERE id = 20;
            UPDATE {{country}} SET shortUrl = 'bahrain'              WHERE id = 21;
            UPDATE {{country}} SET shortUrl = 'belgium'              WHERE id = 22;
            UPDATE {{country}} SET shortUrl = 'bulgaria'             WHERE id = 23;
            UPDATE {{country}} SET shortUrl = 'bolivia'              WHERE id = 24;
            UPDATE {{country}} SET shortUrl = 'bosnia-hercegovina'   WHERE id = 25;
            UPDATE {{country}} SET shortUrl = 'botswana'             WHERE id = 26;
            UPDATE {{country}} SET shortUrl = 'brazil'               WHERE id = 27;
            UPDATE {{country}} SET shortUrl = 'united-kingdom'       WHERE id = 28;
            UPDATE {{country}} SET shortUrl = 'hungary'              WHERE id = 29;
            UPDATE {{country}} SET shortUrl = 'ghana'                WHERE id = 30;
            UPDATE {{country}} SET shortUrl = 'guatemala'            WHERE id = 31;
            UPDATE {{country}} SET shortUrl = 'hong-kong'            WHERE id = 32;
            UPDATE {{country}} SET shortUrl = 'greece'               WHERE id = 33;
            UPDATE {{country}} SET shortUrl = 'denmark'              WHERE id = 34;
            UPDATE {{country}} SET shortUrl = 'congo'                WHERE id = 35;
            UPDATE {{country}} SET shortUrl = 'egypt'                WHERE id = 36;
            UPDATE {{country}} SET shortUrl = 'zambia'               WHERE id = 37;
            UPDATE {{country}} SET shortUrl = 'zimbabwe'             WHERE id = 38;
            UPDATE {{country}} SET shortUrl = 'israel'               WHERE id = 39;
            UPDATE {{country}} SET shortUrl = 'india'                WHERE id = 40;
            UPDATE {{country}} SET shortUrl = 'indonesia'            WHERE id = 41;
            UPDATE {{country}} SET shortUrl = 'jordan'               WHERE id = 42;
            UPDATE {{country}} SET shortUrl = 'iraq'                 WHERE id = 43;
            UPDATE {{country}} SET shortUrl = 'iran'                 WHERE id = 44;
            UPDATE {{country}} SET shortUrl = 'ireland'              WHERE id = 45;
            UPDATE {{country}} SET shortUrl = 'iceland'              WHERE id = 46;
            UPDATE {{country}} SET shortUrl = 'spain'                WHERE id = 47;
            UPDATE {{country}} SET shortUrl = 'italy'                WHERE id = 48;
            UPDATE {{country}} SET shortUrl = 'cambodia'             WHERE id = 49;
            UPDATE {{country}} SET shortUrl = 'cameroon'             WHERE id = 50;
            UPDATE {{country}} SET shortUrl = 'canada'               WHERE id = 51;
            UPDATE {{country}} SET shortUrl = 'qatar'                WHERE id = 52;
            UPDATE {{country}} SET shortUrl = 'kenya'                WHERE id = 53;
            UPDATE {{country}} SET shortUrl = 'china'                WHERE id = 54;
            UPDATE {{country}} SET shortUrl = 'taiwan'               WHERE id = 55;
            UPDATE {{country}} SET shortUrl = 'colombia'             WHERE id = 56;
            UPDATE {{country}} SET shortUrl = 'cuba'                 WHERE id = 57;
            UPDATE {{country}} SET shortUrl = 'kuwait'               WHERE id = 58;
            UPDATE {{country}} SET shortUrl = 'latvia'               WHERE id = 59;
            UPDATE {{country}} SET shortUrl = 'lebanon'              WHERE id = 60;
            UPDATE {{country}} SET shortUrl = 'lithuania'            WHERE id = 61;
            UPDATE {{country}} SET shortUrl = 'liechtenstein'        WHERE id = 62;
            UPDATE {{country}} SET shortUrl = 'luxembourg'           WHERE id = 63;
            UPDATE {{country}} SET shortUrl = 'mauritius'            WHERE id = 64;
            UPDATE {{country}} SET shortUrl = 'macau'                WHERE id = 65;
            UPDATE {{country}} SET shortUrl = 'malaysia'             WHERE id = 66;
            UPDATE {{country}} SET shortUrl = 'malta'                WHERE id = 67;
            UPDATE {{country}} SET shortUrl = 'morocco'              WHERE id = 68;
            UPDATE {{country}} SET shortUrl = 'mexico'               WHERE id = 69;
            UPDATE {{country}} SET shortUrl = 'mozambique'           WHERE id = 70;
            UPDATE {{country}} SET shortUrl = 'monaco'               WHERE id = 71;
            UPDATE {{country}} SET shortUrl = 'myanmar'              WHERE id = 72;
            UPDATE {{country}} SET shortUrl = 'nigeria'              WHERE id = 73;
            UPDATE {{country}} SET shortUrl = 'netherlands'          WHERE id = 74;
            UPDATE {{country}} SET shortUrl = 'new-zealand'          WHERE id = 75;
            UPDATE {{country}} SET shortUrl = 'norway'               WHERE id = 76;
            UPDATE {{country}} SET shortUrl = 'united-arab-emirates' WHERE id = 77;
            UPDATE {{country}} SET shortUrl = 'oman'                 WHERE id = 78;
            UPDATE {{country}} SET shortUrl = 'pakistan'             WHERE id = 79;
            UPDATE {{country}} SET shortUrl = 'panama'               WHERE id = 80;
            UPDATE {{country}} SET shortUrl = 'paraguay'             WHERE id = 81;
            UPDATE {{country}} SET shortUrl = 'peru'                 WHERE id = 82;
            UPDATE {{country}} SET shortUrl = 'poland'               WHERE id = 83;
            UPDATE {{country}} SET shortUrl = 'portugal'             WHERE id = 84;
            UPDATE {{country}} SET shortUrl = 'congo-republic'       WHERE id = 85;
            UPDATE {{country}} SET shortUrl = 'korea-republic'       WHERE id = 86;
            UPDATE {{country}} SET shortUrl = 'kosovo'               WHERE id = 87;
            UPDATE {{country}} SET shortUrl = 'macedonia'            WHERE id = 88;
            UPDATE {{country}} SET shortUrl = 'rwanda'               WHERE id = 89;
            UPDATE {{country}} SET shortUrl = 'romania'              WHERE id = 90;
            UPDATE {{country}} SET shortUrl = 'saudi-arabia'         WHERE id = 91;
            UPDATE {{country}} SET shortUrl = 'senegal'              WHERE id = 92;
            UPDATE {{country}} SET shortUrl = 'singapore'            WHERE id = 93;
            UPDATE {{country}} SET shortUrl = 'slovak-republic'      WHERE id = 94;
            UPDATE {{country}} SET shortUrl = 'slovenia'             WHERE id = 95;
            UPDATE {{country}} SET shortUrl = 'usa'                  WHERE id = 96;
            UPDATE {{country}} SET shortUrl = 'sudan'                WHERE id = 97;
            UPDATE {{country}} SET shortUrl = 'surinam'              WHERE id = 98;
            UPDATE {{country}} SET shortUrl = 'thailand'             WHERE id = 99;
            UPDATE {{country}} SET shortUrl = 'tanzania'             WHERE id = 100;
            UPDATE {{country}} SET shortUrl = 'tunisia'              WHERE id = 101;
            UPDATE {{country}} SET shortUrl = 'turkey'               WHERE id = 102;
            UPDATE {{country}} SET shortUrl = 'uganda'               WHERE id = 103;
            UPDATE {{country}} SET shortUrl = 'philippines'          WHERE id = 104;
            UPDATE {{country}} SET shortUrl = 'finland'              WHERE id = 105;
            UPDATE {{country}} SET shortUrl = 'france'               WHERE id = 106;
            UPDATE {{country}} SET shortUrl = 'croatia'              WHERE id = 107;
            UPDATE {{country}} SET shortUrl = 'czech-republic'       WHERE id = 108;
            UPDATE {{country}} SET shortUrl = 'chile'                WHERE id = 109;
            UPDATE {{country}} SET shortUrl = 'switzerland'          WHERE id = 110;
            UPDATE {{country}} SET shortUrl = 'sweden'               WHERE id = 111;
            UPDATE {{country}} SET shortUrl = 'sri-lanka'            WHERE id = 112;
            UPDATE {{country}} SET shortUrl = 'ecuador'              WHERE id = 113;
            UPDATE {{country}} SET shortUrl = 'estonia'              WHERE id = 114;
            UPDATE {{country}} SET shortUrl = 'ethiopia'             WHERE id = 115;
            UPDATE {{country}} SET shortUrl = 'south-africa'         WHERE id = 116;
            UPDATE {{country}} SET shortUrl = 'japan'                WHERE id = 117;
            UPDATE {{country}} SET shortUrl = 'angola'               WHERE id = 118;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}