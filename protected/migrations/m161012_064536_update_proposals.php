<?php

class m161012_064536_update_proposals extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
             UPDATE {{proposalelement}} SET `userId` = 954, `status` = 2 WHERE `id` = 2450;
             UPDATE {{proposalelement}} SET `userId` = 1245, `status` = 2 WHERE `id` = 2451;
             UPDATE {{proposalelement}} SET `userId` = 1211, `status` = 2 WHERE `id` = 2452;
             UPDATE {{proposalelement}} SET `userId` = 1204, `status` = 2 WHERE `id` = 2453;
             UPDATE {{proposalelement}} SET `userId` = 1209, `status` = 2 WHERE `id` = 2454;
             UPDATE {{proposalelement}} SET `userId` = 1209, `status` = 2 WHERE `id` = 2455;
             UPDATE {{proposalelement}} SET `userId` = 2044, `status` = 2 WHERE `id` = 2456;
             UPDATE {{proposalelement}} SET `userId` = 1690, `status` = 2 WHERE `id` = 2457;
             UPDATE {{proposalelement}} SET `userId` = 1795, `status` = 2 WHERE `id` = 2458;
             UPDATE {{proposalelement}} SET `userId` = 860, `status` = 2 WHERE `id` = 2459;
             UPDATE {{proposalelement}} SET `userId` = 1651, `status` = 2 WHERE `id` = 2460;
             UPDATE {{proposalelement}} SET `userId` = 1239, `status` = 2 WHERE `id` = 2461;
             UPDATE {{proposalelement}} SET `userId` = 883, `status` = 2 WHERE `id` = 2462;
             UPDATE {{proposalelement}} SET `userId` = 886, `status` = 2 WHERE `id` = 2463;
             UPDATE {{proposalelement}} SET `userId` = 887, `status` = 2 WHERE `id` = 2464;
             UPDATE {{proposalelement}} SET `userId` = 1397, `status` = 2 WHERE `id` = 2465;
             UPDATE {{proposalelement}} SET `userId` = 1305, `status` = 2 WHERE `id` = 2466;
             UPDATE {{proposalelement}} SET `userId` = 1163, `status` = 2 WHERE `id` = 2467;
             UPDATE {{proposalelement}} SET `userId` = 922, `status` = 2 WHERE `id` = 2468;
             UPDATE {{proposalelement}} SET `userId` = 1528, `status` = 2 WHERE `id` = 2469;
             UPDATE {{proposalelement}} SET `userId` = 1528, `status` = 2 WHERE `id` = 2470;
             UPDATE {{proposalelement}} SET `userId` = 1381, `status` = 2 WHERE `id` = 2471;
             UPDATE {{proposalelement}} SET `userId` = 1381, `status` = 2 WHERE `id` = 2472;
             UPDATE {{proposalelement}} SET `userId` = 936, `status` = 2 WHERE `id` = 2473;
             UPDATE {{proposalelement}} SET `userId` = 1398, `status` = 2 WHERE `id` = 2474;
             UPDATE {{proposalelement}} SET `userId` = 950, `status` = 2 WHERE `id` = 2475;
             UPDATE {{proposalelement}} SET `userId` = 2092, `status` = 2 WHERE `id` = 2476;
             UPDATE {{proposalelement}} SET `userId` = 1796, `status` = 2 WHERE `id` = 2477;
             UPDATE {{proposalelement}} SET `userId` = 958, `status` = 2 WHERE `id` = 2478;
             UPDATE {{proposalelement}} SET `userId` = 945, `status` = 2 WHERE `id` = 2479;
             UPDATE {{proposalelement}} SET `userId` = 945, `status` = 2 WHERE `id` = 2480;
             UPDATE {{proposalelement}} SET `userId` = 943, `status` = 2 WHERE `id` = 2481;
             UPDATE {{proposalelement}} SET `userId` = 931, `status` = 2 WHERE `id` = 2482;
             UPDATE {{proposalelement}} SET `userId` = 931, `status` = 2 WHERE `id` = 2483;
             UPDATE {{proposalelement}} SET `userId` = 931, `status` = 2 WHERE `id` = 2484;
             UPDATE {{proposalelement}} SET `userId` = 1358, `status` = 2 WHERE `id` = 2485;
             UPDATE {{proposalelement}} SET `userId` = 917, `status` = 2 WHERE `id` = 2486;
             UPDATE {{proposalelement}} SET `userId` = 905, `status` = 2 WHERE `id` = 2487;
             UPDATE {{proposalelement}} SET `userId` = 912, `status` = 2 WHERE `id` = 2488;
             UPDATE {{proposalelement}} SET `userId` = 1926, `status` = 2 WHERE `id` = 2489;
             UPDATE {{proposalelement}} SET `userId` = 1403, `status` = 2 WHERE `id` = 2490;
             UPDATE {{proposalelement}} SET `userId` = 1636, `status` = 2 WHERE `id` = 2491;
             UPDATE {{proposalelement}} SET `userId` = 880, `status` = 2 WHERE `id` = 2492;
             UPDATE {{proposalelement}} SET `userId` = 880, `status` = 2 WHERE `id` = 2493;
             UPDATE {{proposalelement}} SET `userId` = 1404, `status` = 2 WHERE `id` = 2494;
             UPDATE {{proposalelement}} SET `userId` = 1404, `status` = 2 WHERE `id` = 2495;
             UPDATE {{proposalelement}} SET `userId` = 864, `status` = 2 WHERE `id` = 2496;
             UPDATE {{proposalelement}} SET `userId` = 1147, `status` = 2 WHERE `id` = 2497;
             UPDATE {{proposalelement}} SET `userId` = 858, `status` = 2 WHERE `id` = 2498;
             UPDATE {{proposalelement}} SET `userId` = 1208, `status` = 2 WHERE `id` = 2499;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}