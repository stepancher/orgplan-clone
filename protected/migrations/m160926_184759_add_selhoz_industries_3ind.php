<?php

class m160926_184759_add_selhoz_industries_3ind extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56d030def7a9ad0b078f11aa', '1', '4', 'Продукция сельского хозяйства всех категорий', 'млн.рублей', '#a0d468', '11', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56d0311df7a9add4068f7bb7', '1', '4', 'Производство зерна', 'тыс.центнеров', '#a0d468', '11', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56ec84ee74d8974605a991cd', '1', '4', 'Скот и птица в убойном весе', 'тыс.тонн', '#a0d468', '11', '3', 'region');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}