<?php

class m160920_081741_add_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{district}} (`countryId`, `shortUrl`) VALUES ('6', 'shymkent-district');
            
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('52', 'ru', 'Округ Южно-Казахстанская область');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('52', 'en', 'Округ Южно-Казахстанская область');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('52', 'de', 'Округ Южно-Казахстанская область');

            INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('52', 'shymkent-oblast');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('147', 'ru', 'Южно-Казахстанская область');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('147', 'en', 'Южно-Казахстанская область');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`) VALUES ('147', 'de', 'Южно-Казахстанская область');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('147', 'shymkent');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('268', 'ru', 'Шымкент');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('268', 'en', 'Шымкент');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('268', 'de', 'Шымкент');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}