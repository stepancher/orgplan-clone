<?php

class m160301_150839_proposal_element_add_column_status extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			ALTER TABLE {{proposalelement}}
			DROP COLUMN `status`;
			ALTER TABLE {{proposalelement}}
			DROP COLUMN `datetimeModified`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			ALTER TABLE {{proposalelement}}
			ADD COLUMN `status` INT NULL DEFAULT 1 AFTER `userId`;

			ALTER TABLE {{proposalelement}}
			CHANGE COLUMN `status` `status` INT(11) NOT NULL DEFAULT 1 ;

			update {{proposalelement}} set `status` = 1;

			ALTER TABLE {{proposalelement}}
			ADD COLUMN `datetimeModified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() AFTER `status`;

			update {{proposalelement}} set `datetimeModified` = current_timestamp();

			ALTER TABLE {{proposalelement}}
			CHANGE COLUMN `datetimeModified` `datetimeModified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

		';
	}






}