<?php

class m160616_062812_update_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}



	public function getCreateTable(){
		return "
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1941', 'en', 'LCD projector (4 000 lumen, 1 day)');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1940', 'en', 'LCD projector (8 000 lumen, 1 hour)');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1939', 'en', 'LCD projector (4 000 lumen, 1 day)');
			UPDATE {{trattributeclassproperty}} SET `value`='LCD проектор 8000 Lum (1 день)' WHERE `id`='2613';
			UPDATE {{trattributeclassproperty}} SET `value`='LCD projector (8 000 lumen, 1 day)' WHERE `id`='2617';
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1938', 'en', 'LCD projector (4 000 lumen, 1 hour)');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1933', 'en', 'Rental period (1 hour)');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1932', 'en', 'Rental period (1/2 day)');
			UPDATE {{trattributeclassproperty}} SET `value`='Rental period (1 day)' WHERE `id`='2469';
			
			UPDATE {{trattributeclass}} SET `label`=NULL, `tooltip`='<div class=\"hint - header\">Cost of services</div> increases<br>by 100% if ordered after August 08, 2016.' WHERE `id`='2171';
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `tooltip`, `tooltipCustom`) VALUES (NULL, '2179', 'en', '<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16', '<div class=\"hint - header\">Cost of services</div> increases<br>by 100% if ordered after August 08, 2016.');
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">Cost of services</div> increases by 100% if ordered after August 08, 2016.' WHERE `id`='2171';
			DELETE FROM {{attributeclassproperty}} WHERE `id`='1846';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1954', 'ru', 'чел.');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1954', 'en', 'persons');
			DELETE FROM {{attributeclassproperty}} WHERE `id`='1847';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1955', 'en', 'hour');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1955', 'ru', 'час');
			UPDATE {{trproposalelementclass}} SET `label`='Application No.10', `pluralLabel`='BUSINESS EVENT' WHERE `id`='16';
			UPDATE {{trattributeclassproperty}} SET `value`='LCD projector 4 000 lumen (1 hour)' WHERE `id`='2620';
			UPDATE {{trattributeclassproperty}} SET `value`='LCD projector 4 000 lumen (1 day)' WHERE `id`='2619';
			UPDATE {{trattributeclassproperty}} SET `value`='LCD projector 8 000 lumen (1 hour)' WHERE `id`='2618';
			UPDATE {{trattributeclassproperty}} SET `value`='LCD projector 8 000 lumen (1 day)' WHERE `id`='2617';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nRENTAL PERIOD</div><div class=\"body\">The \"day time\" is from 10:00 a.m. to 18:00 p.m. \"1 / 2 of the day\" is four hours within the aforementioned period of time. Per hour rent is possible beyond the \"1 / 2 of the day\" or the \"day time\" period.</div>' WHERE `id`='2171';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">MEALS FOR PARTICIPANTS</div><div class=\"body\">The cost of conference hall rental increases by 50% if the company is planning to organize meals for the event participants.</div>' WHERE `id`='2172';
			UPDATE {{trattributeclass}} SET `placeholder`='Please enter the event type' WHERE `id`='2165';
			UPDATE {{trattributeclass}} SET `placeholder`='Enter' WHERE `id`='2166';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=NULL WHERE `id`='2250';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=NULL WHERE `id`='2125';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=NULL WHERE `id`='2171';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nПЕРИОД АРЕНДЫ\n</div>\n<div class=\"body\">\nПод термином \"день\" понимается период с 10:00 до 18:00. \"1 / 2\" дня - 4 часа в течении этого периода. Период \"1 час\"  применяется при пользовании залом сверх периода \"1 / 2 дня\" или \"день\".\n</div>' WHERE `id`='2170';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nПЕРИОД АРЕНДЫ\n</div>\n<div class=\"body\">\nПод термином \"день\" понимается период с 10:00 до 18:00. \"1 / 2\" дня - 4 часа в течении этого периода. Период \"1 час\"  применяется при пользовании залом сверх периода \"1 / 2 дня\" или \"день\".\n</div>' WHERE `id`='2124';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nRENTAL PERIOD\n</div>\n<div class=\"body\">\nThe \"day time\" is from 10:00 a.m. to 18:00 p.m. \"1 / 2 of the day\" is four hours within the aforementioned period of time. Per hour rent is possible beyond the \"1 / 2 of the day\" or the \"day time\" period.\n</div>' WHERE `id`='2170';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2180', '2148', 'partialInherit', '{\"price\":{\"percent\":30}}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2179', '2148', 'partialInherit', '{\"price\":{\"percent\":30}}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2147', '2148', 'partialInherit', '{\"price\":{\"percent\":30}}', '0');
			UPDATE {{attributeclassdependency}} SET `function`='increase', `functionArgs`='{\"price\":{\"multiply\": 1.5}}' WHERE `id`='975';
			UPDATE {{attributeclassdependency}} SET `function`='increase', `functionArgs`='{\"price\":{\"multiply\": 1.5}}' WHERE `id`='976';
			UPDATE {{attributeclassdependency}} SET `function`='increase', `functionArgs`='{\"price\":{\"multiply\": 1.5}}' WHERE `id`='977';
			UPDATE {{attributeclassdependency}} SET `function`='calc' WHERE `id`='975';
			UPDATE {{attributeclassdependency}} SET `function`='calc' WHERE `id`='976';
			UPDATE {{attributeclassdependency}} SET `function`='calc' WHERE `id`='977';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[\n{\n	\"if\": [\n	    [\"VAL\",\"abc\",\" = \",\"VAL\",\"abc\"],\n	    [\"VAL\",\"abc\",\" = \",\"VAL\",\"abc\"]\n	],\n	\"then\": [\n	    [\"VAL\",1.5,\"multiple\",\"PROP\",\"price\"]\n	]\n    }\n]' WHERE `id`='975';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[{\n	\"if\": [\n		[\"VAL\", \"abc\", \" = \", \"VAL\", \"abc\"],\n		[\"VAL\", \"abc\", \" = \", \"VAL\", \"abc\"]\n	],\n	\"then\": [\n		[\"VAL\", 1.5, \"multiple\", \"PROP\", \"price\"]\n	]\n}]' WHERE `id`='975';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[{\n	\"if\": [\n		[\"VAL\", \"abc\", \" = \", \"VAL\", \"abc\"],\n		[\"VAL\", \"abc\", \" = \", \"VAL\", \"abc\"]\n	],\n	\"then\": [\n		[\"VAL\", 1.5, \"multiple\", \"PROP\", \"price\"]\n	]\n}]' WHERE `id`='976';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[{\n	\"if\": [\n		[\"VAL\", \"abc\", \" = \", \"VAL\", \"abc\"],\n		[\"VAL\", \"abc\", \" = \", \"VAL\", \"abc\"]\n	],\n	\"then\": [\n		[\"VAL\", 1.5, \"multiple\", \"PROP\", \"price\"]\n	]\n}]' WHERE `id`='977';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2147';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2179';
			UPDATE {{attributeclass}} SET `class`='coefficient' WHERE `id`='2180';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[\n{\n    \"if\": [\n	[\"VAL\", true, \"=\", \"PROP\", \"value\"]\n    ],\n    \"then\": [\n	[\"PROP\", \"price\", \"multiply\", \"VAL\", 1.5]\n    ]\n}\n]' WHERE `id`='975';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[\n{\n    \"if\": [\n	[\"VAL\", true, \"=\", \"PROP\", \"value\"]\n    ],\n    \"then\": [\n	[\"PROP\", \"price\", \"multiply\", \"VAL\", 1.5]\n    ]\n}\n]' WHERE `id`='976';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='[\n{\n    \"if\": [\n	[\"VAL\", true, \"=\", \"PROP\", \"value\"]\n    ],\n    \"then\": [\n	[\"PROP\", \"price\", \"multiply\", \"VAL\", 1.5]\n    ]\n}\n]' WHERE `id`='977';
		";
	}
}