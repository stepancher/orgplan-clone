<?php

class m151103_160033_table_tztarget extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{tztarget}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
				CREATE TABLE IF NOT EXISTS {{tztarget}} (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(255) DEFAULT NULL,
				  `description` text,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

			INSERT INTO {{tztarget}} (`id`, `name`, `description`) VALUES
				(2, "Продажи", NULL),
				(3, "PR,Выстраивание бренда", NULL),
				(4, "Поддержка каналов сбыта", NULL),
				(5, "Отношения с потребителями", NULL),
				(6, "Маркетинговые исследования", NULL),
				(7, "Другое", NULL);

				DELETE FROM {{tztarget}} WHERE id = 1;
		';
	}
}