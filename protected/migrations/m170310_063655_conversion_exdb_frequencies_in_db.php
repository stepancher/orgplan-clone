<?php

class m170310_063655_conversion_exdb_frequencies_in_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `conversion_exdb_frequencies_in_db`;
            CREATE PROCEDURE `conversion_exdb_frequencies_in_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_freq FLOAT DEFAULT '0.0';
                DECLARE copy CURSOR FOR SELECT t.id, t.freq FROM
                                        (SELECT f.id,
                                        CASE WHEN f.exdbFrequency LIKE 'Annual%' OR f.exdbFrequency LIKE 'Annula%' THEN 1
                                             WHEN f.exdbFrequency LIKE 'Biennial%' OR f.exdbFrequency LIKE 'Every 2 years%' THEN 0.5
                                             WHEN f.exdbFrequency = 'Semi-annual' OR f.exdbFrequency LIKE 'Twice a year%' THEN 2
                                             WHEN f.exdbFrequency LIKE 'Every 4 years%' THEN 0.25
                                             WHEN f.exdbFrequency LIKE 'Every 3 years%' THEN 0.33
                                             WHEN f.exdbFrequency LIKE 'Every 5 years%' THEN 0.2
                                             WHEN f.exdbFrequency LIKE 'Every 6 years%' THEN 0.17
                                             WHEN f.exdbFrequency LIKE 'Every 10 years' THEN 0.1
                                             WHEN f.exdbFrequency = 'Every 1,5 years' OR f.exdbFrequency = 'Every 1.5 years' OR f.exdbFrequency LIKE 'Every 18 month%' THEN 0.66
                                             WHEN f.exdbFrequency LIKE 'Three times a year%' THEN 3
                                             WHEN f.exdbFrequency LIKE 'Four times a year%' THEN 4
                                             WHEN f.exdbFrequency LIKE 'Five times a year%' THEN 5
                                        ELSE NULL END freq
                                        FROM {{fair}} f
                                        WHERE f.exdbId IS NOT NULL
                                        ) t WHERE t.freq IS NOT NULL ORDER BY t.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, f_freq;
                    
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{fair}} SET `frequency` = f_freq WHERE `id` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `conversion_exdb_frequencies_in_db`();
            DROP PROCEDURE IF EXISTS `conversion_exdb_frequencies_in_db`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}