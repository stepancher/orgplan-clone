<?php

class m161003_065524_analytic_edits extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{analyticschartpreview}} SET `stat` = '56d0311df7a9add4068f7bb7' WHERE `id` = 2;
            UPDATE {{analyticschartpreview}} SET `stat` = '56d0311df7a9add4068f7bb7' WHERE `id` = 47;
            UPDATE {{analyticschartpreview}} SET `stat` = '56ec84ee74d8974605a991cd' WHERE `id` = 3;
            UPDATE {{analyticschartpreview}} SET `stat` = '56ec84ee74d8974605a991cd' WHERE `id` = 48;
            
            UPDATE {{analyticschartpreview}} SET `stat` = '500211' WHERE `id` = 35;
            UPDATE {{analyticschartpreview}} SET `stat` = '500211' WHERE `id` = 38;
            
            UPDATE {{analyticschartpreview}} SET `stat` = '500007' WHERE `id` = 9;
            UPDATE {{analyticschartpreview}} SET `stat` = '500211' WHERE `id` = 35;
            UPDATE {{analyticschartpreview}} SET `stat` = '500211' WHERE `id` = 38;
            
            UPDATE {{analyticschartpreview}} SET `measure` = 'Тонна' WHERE `id` = 4;
            UPDATE {{analyticschartpreview}} SET `measure` = 'Тонна' WHERE `id` = 5;
            
            UPDATE {{analyticschartpreview}} SET `measure` = 'тысяча рублей' WHERE `id` = 9;
            UPDATE {{analyticschartpreview}} SET `measure` = 'тыс м³' WHERE `id` = 10;
            UPDATE {{analyticschartpreview}} SET `measure` = 'тыс м³' WHERE `id` = 13;
            UPDATE {{analyticschartpreview}} SET `measure` = 'м³' WHERE `id` = 16;
            UPDATE {{analyticschartpreview}} SET `measure` = 'тыс м³' WHERE `id` = 17;
            UPDATE {{analyticschartpreview}} SET `measure` = 'тыс м³' WHERE `id` = 19;
            UPDATE {{analyticschartpreview}} SET `measure` = 'тыс м²' WHERE `id` = 22;
            UPDATE {{analyticschartpreview}} SET `measure` = '%' WHERE `id` = 24;
            UPDATE {{analyticschartpreview}} SET `measure` = '%' WHERE `id` = 27;
            UPDATE {{analyticschartpreview}} SET `measure` = 'тыс м²' WHERE `id` = 25;
            UPDATE {{analyticschartpreview}} SET `measure` = 'млн. м³' WHERE `id` = 29;
            UPDATE {{analyticschartpreview}} SET `measure` = 'млн. м³' WHERE `id` = 32;
            UPDATE {{analyticschartpreview}} SET `measure` = 'рублей' WHERE `id` = 41;
            UPDATE {{analyticschartpreview}} SET `measure` = 'рублей' WHERE `id` = 44;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}