<?php

class m160804_092056_add_missed_exhibition_complexes_to_sng_fair extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			UPDATE {{fair}} SET exhibitionComplexId = 1143 WHERE id in (
				'11153',
				'11154',
				'11155',
				'11156',
				'11157',
				'11158',
				'11159',
				'11160',
				'11161',
				'11162',
				'11163',
				'11164',
				'11165',
				'11166',
				'11167',
				'11168',
				'11169',
				'11170',
				'11171',
				'11172',
				'11173',
				'11174');
				
			UPDATE {{fair}} SET exhibitionComplexId = 1144 WHERE id in (
				'11539',
				'1897',
				'11546',
				'11547');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}