<?php

class m160419_133944_update_dependences extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			TRUNCATE {{dependences}};

			INSERT INTO {{dependences}} VALUES
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/send','{\"fairId\":184, \"ecId\":1}','calendarm/gantt/complete/id/1'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(5,'proposal/proposal/send','{\"fairId\":184, \"ecId\":1}','calendarm/gantt/complete/id/6'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(7,'proposal/proposal/send','{\"fairId\":184, \"ecId\":1}','calendarm/gantt/complete/id/10'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/29'),
			(9,'proposal/proposal/send','{\"fairId\":184, \"ecId\":1}','calendarm/gantt/complete/id/29'),
			(10,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"save\":1}','calendarm/gantt/add/id/20'),
			(11,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/add/id/20'),
			(12,'proposal/proposal/send','{\"fairId\":184, \"ecId\":4}','calendarm/gantt/add/id/20'),
			(13,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/20'),
			(14,'proposal/proposal/send','{\"fairId\":184, \"ecId\":4}','calendarm/gantt/complete/id/20'),
			(15,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/22'),
			(16,'proposal/proposal/send','{\"fairId\":184, \"ecId\":4}','calendarm/gantt/complete/id/22'),
			(17,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/24'),
			(18,'proposal/proposal/send','{\"fairId\":184, \"ecId\":4}','calendarm/gantt/complete/id/24'),
			(19,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"save\":1}','calendarm/gantt/add/id/31'),
			(20,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/add/id/31'),
			(21,'proposal/proposal/send','{\"fairId\":184, \"ecId\":5}','calendarm/gantt/add/id/31'),
			(22,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/31'),
			(23,'proposal/proposal/send','{\"fairId\":184, \"ecId\":5}','calendarm/gantt/complete/id/31'),
			(24,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/33'),
			(25,'proposal/proposal/send','{\"fairId\":184, \"ecId\":5}','calendarm/gantt/complete/id/33'),
			(26,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/35'),
			(27,'proposal/proposal/send','{\"fairId\":184, \"ecId\":5}','calendarm/gantt/complete/id/35'),
			(28,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"save\":1}','calendarm/gantt/add/id/37'),
			(29,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/add/id/37'),
			(30,'proposal/proposal/send','{\"fairId\":184, \"ecId\":2}','calendarm/gantt/add/id/37'),
			(31,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/37'),
			(32,'proposal/proposal/send','{\"fairId\":184, \"ecId\":2}','calendarm/gantt/complete/id/37'),
			(33,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/39'),
			(34,'proposal/proposal/send','{\"fairId\":184, \"ecId\":2}','calendarm/gantt/complete/id/39'),
			(35,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/41'),
			(36,'proposal/proposal/send','{\"fairId\":184, \"ecId\":2}','calendarm/gantt/complete/id/41'),
			(37,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"save\":1}','calendarm/gantt/add/id/43'),
			(38,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/add/id/43'),
			(39,'proposal/proposal/send','{\"fairId\":184, \"ecId\":7}','calendarm/gantt/add/id/43'),
			(40,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/43'),
			(41,'proposal/proposal/send','{\"fairId\":184, \"ecId\":7}','calendarm/gantt/complete/id/43'),
			(42,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/45'),
			(43,'proposal/proposal/send','{\"fairId\":184, \"ecId\":7}','calendarm/gantt/complete/id/45'),
			(44,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/47'),
			(45,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/49'),
			(46,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/50'),
			(47,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/56'),
			(48,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/49'),
			(49,'proposal/proposal/send','{\"fairId\":184, \"ecId\":10}','calendarm/gantt/add/id/49'),
			(50,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/complete/id/49'),
			(51,'proposal/proposal/send','{\"fairId\":184, \"ecId\":10}','calendarm/gantt/complete/id/49'),
			(52,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/50'),
			(53,'proposal/proposal/send','{\"fairId\":184, \"ecId\":10}','calendarm/gantt/add/id/50'),
			(54,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/56'),
			(55,'proposal/proposal/send','{\"fairId\":184, \"ecId\":10}','calendarm/gantt/add/id/56'),
			(56,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/49'),
			(57,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/50'),
			(58,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/56'),
			(59,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/49'),
			(60,'proposal/proposal/send','{\"fairId\":184, \"ecId\":8}','calendarm/gantt/add/id/49'),
			(61,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/50'),
			(62,'proposal/proposal/send','{\"fairId\":184, \"ecId\":8}','calendarm/gantt/add/id/50'),
			(63,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/56'),
			(64,'proposal/proposal/send','{\"fairId\":184, \"ecId\":8}','calendarm/gantt/add/id/56'),
			(65,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/50'),
			(66,'proposal/proposal/send','{\"fairId\":184, \"ecId\":8}','calendarm/gantt/complete/id/50'),
			(67,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/52'),
			(68,'proposal/proposal/send','{\"fairId\":184, \"ecId\":8}','calendarm/gantt/complete/id/52'),
			(69,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/54'),
			(70,'proposal/proposal/send','{\"fairId\":184, \"ecId\":8}','calendarm/gantt/complete/id/54'),
			(71,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/49'),
			(72,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/50'),
			(73,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/56'),
			(74,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/49'),
			(75,'proposal/proposal/send','{\"fairId\":184, \"ecId\":9}','calendarm/gantt/add/id/49'),
			(76,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/50'),
			(77,'proposal/proposal/send','{\"fairId\":184, \"ecId\":9}','calendarm/gantt/add/id/50'),
			(78,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/56'),
			(79,'proposal/proposal/send','{\"fairId\":184, \"ecId\":9}','calendarm/gantt/add/id/56'),
			(80,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/56'),
			(81,'proposal/proposal/send','{\"fairId\":184, \"ecId\":9}','calendarm/gantt/complete/id/56'),
			(82,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/58'),
			(83,'proposal/proposal/send','{\"fairId\":184, \"ecId\":9}','calendarm/gantt/complete/id/58'),
			(84,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/60'),
			(85,'proposal/proposal/send','{\"fairId\":184, \"ecId\":9}','calendarm/gantt/complete/id/60'),
			(86,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"save\":1}','calendarm/gantt/add/id/62'),
			(87,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"send\":1}','calendarm/gantt/add/id/62'),
			(88,'proposal/proposal/send','{\"fairId\":184, \"ecId\":3}','calendarm/gantt/add/id/62'),
			(89,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"send\":1}','calendarm/gantt/complete/id/62');

			UPDATE {{calendarfairdays}} SET `startDate`='2016-09-29', `endDate`='2016-09-30' WHERE `id`='1';
			UPDATE {{calendarfairdays}} SET `startDate`='2016-09-30' WHERE `id`='2';
		";
	}

	private function downSql() {

		return "
			TRUNCATE {{dependences}};

			INSERT INTO {{dependences}} VALUES 
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(5,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/29'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"save\":1}','calendarm/gantt/add/id/20'),
			(7,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/add/id/20'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/20'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/22'),
			(10,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/24'),
			(11,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"save\":1}','calendarm/gantt/add/id/31'),
			(12,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/add/id/31'),
			(13,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/31'),
			(14,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/33'),
			(15,'proposal/proposal/create','{\"fairId\":184, \"ecId\":5, \"send\":1}','calendarm/gantt/complete/id/35'),
			(16,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"save\":1}','calendarm/gantt/add/id/37'),
			(17,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/add/id/37'),
			(18,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/37'),
			(19,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/39'),
			(20,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/41'),
			(21,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"save\":1}','calendarm/gantt/add/id/43'),
			(22,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/add/id/43'),
			(23,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/43'),
			(24,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/45'),
			(25,'proposal/proposal/create','{\"fairId\":184, \"ecId\":7, \"send\":1}','calendarm/gantt/complete/id/47'),
			(26,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/49'),
			(27,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/50'),
			(28,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"save\":1}','calendarm/gantt/add/id/56'),
			(29,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/49'),
			(30,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/complete/id/49'),
			(31,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/50'),
			(32,'proposal/proposal/create','{\"fairId\":184, \"ecId\":10, \"send\":1}','calendarm/gantt/add/id/56'),
			(33,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/49'),
			(34,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/50'),
			(35,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"save\":1}','calendarm/gantt/add/id/56'),
			(36,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/49'),
			(37,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/50'),
			(38,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/add/id/56'),
			(39,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/50'),
			(40,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/52'),
			(41,'proposal/proposal/create','{\"fairId\":184, \"ecId\":8, \"send\":1}','calendarm/gantt/complete/id/54'),
			(42,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/49'),
			(43,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/50'),
			(44,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"save\":1}','calendarm/gantt/add/id/56'),
			(45,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/49'),
			(46,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/50'),
			(47,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/add/id/56'),
			(48,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/56'),
			(49,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/58'),
			(50,'proposal/proposal/create','{\"fairId\":184, \"ecId\":9, \"send\":1}','calendarm/gantt/complete/id/60'),
			(51,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"save\":1}', 'calendarm/gantt/add/id/62'),
			(52,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"send\":1}', 'calendarm/gantt/add/id/62'),
			(53,'proposal/proposal/create','{\"fairId\":184, \"ecId\":3, \"send\":1}', 'calendarm/gantt/complete/id/62');
		";
	}

}