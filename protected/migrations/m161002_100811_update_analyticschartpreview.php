<?php

class m161002_100811_update_analyticschartpreview extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{analyticschartpreview}} SET `stat`='56ec84ee74d8974605a991cd', `header`='Производство скота и птицы на убой' WHERE `id`='37';
            UPDATE {{analyticschartpreview}} SET `stat`='56d0518ff7a9adf30997bab2', `header`='Проирзводство яиц' WHERE `id`='39';
            UPDATE {{analyticschartpreview}} SET `measure`='миллионов штук' WHERE `id`='39';
            UPDATE {{analyticschartpreview}} SET `measure`='тысяч тонн' WHERE `id`='37';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}