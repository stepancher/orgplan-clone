<?php

class m160624_153543_added_signes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n12', 'int', '1', '0', '0', '2188', 'partiesSignature', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n12', 'int', '1', '2188', '2188', '1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12_1', 'string', '1', '2189', '2188', '1', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12_2', 'string', '1', '2189', '2188', '2', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12_2.5', 'string', '1', '2189', '2188', '4', 'headerH2', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12_3', 'string', '1', '2189', '2188', '3', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12_4', 'string', '1', '2189', '2188', '5', 'text', '0');
			UPDATE {{attributeclass}} SET `super`='2390' WHERE `id`='2390';
			UPDATE {{attributeclass}} SET `parent`='2390', `super`='2390' WHERE `id`='2391';
			UPDATE {{attributeclass}} SET `parent`='2391', `super`='2390' WHERE `id`='2392';
			UPDATE {{attributeclass}} SET `parent`='2391', `super`='2390', `class`='headerH2' WHERE `id`='2393';
			UPDATE {{attributeclass}} SET `parent`='2391', `super`='2390', `priority`='3', `class`='text' WHERE `id`='2394';
			UPDATE {{attributeclass}} SET `parent`='2391', `super`='2390', `priority`='4', `class`='text' WHERE `id`='2395';
			DELETE FROM {{attributeclass}} WHERE `id`='2396';
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2390', '0', '0', '140');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2392', 'ru', 'Экспонент');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2393', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2394', 'ru', 'ФИО');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2395', 'ru', 'подпись');
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='2393';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n12a', 'int', '1', '0', '0', '2396', 'partiesSignature', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n12a', 'int', '1', '2396', '2396', '1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12a_1', 'string', '1', '2397', '2396', '1', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12a_2', 'string', '1', '2397', '2396', '2', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12a_2.5', 'string', '1', '2397', '2396', '3', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n12a_3', 'string', '1', '2397', '2396', '4', 'text', '0');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2397', '0', '0', '100');
			UPDATE {{attributeclass}} SET `super`='2397' WHERE `id`='2397';
			UPDATE {{attributeclass}} SET `parent`='2397', `super`='2397' WHERE `id`='2398';
			UPDATE {{attributeclass}} SET `parent`='2398', `super`='2397' WHERE `id`='2399';
			UPDATE {{attributeclass}} SET `parent`='2398', `super`='2397' WHERE `id`='2400';
			UPDATE {{attributeclass}} SET `parent`='2398', `super`='2397' WHERE `id`='2401';
			UPDATE {{attributeclass}} SET `parent`='2398', `super`='2397' WHERE `id`='2402';
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2399', 'ru', 'Экспонент');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2400', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2401', 'ru', 'ФИО');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2402', 'ru', 'подпись');
";
	}
}