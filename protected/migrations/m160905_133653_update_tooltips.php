<?php

class m160905_133653_update_tooltips extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2125';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2138';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2139';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2140';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2141';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2142';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2143';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2144';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2145';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2146';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2149';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2246';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2247';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2248';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2249';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2286';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе с 21.09.16' WHERE `id`='2287';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}