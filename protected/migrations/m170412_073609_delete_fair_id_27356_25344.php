<?php

class m170412_073609_delete_fair_id_27356_25344 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{fair}} WHERE `id` = '27356';
            DELETE FROM {{fair}} WHERE `id` = '25344';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '67811';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '27946';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '28167';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}