<?php

class m151204_105613_chartcolumn_header_names extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{

//		$transaction = Yii::app()->db->beginTransaction();
//		try
//		{
//			Yii::app()->db->createCommand($sql)->execute();
//			$transaction->commit();
//		}
//		catch(Exception $e)
//		{
//			$transaction->rollback();
//
//			echo $e->getMessage();
//
//			return false;
//		}

		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{chartcolumn}} SET `header`='Производство цемента' WHERE `id`='60';
			UPDATE {{chartcolumn}} SET `header`='Производство железобетонных конструкций' WHERE `id`='62';
			UPDATE {{chartcolumn}} SET `header`='Строительный объем зданий' WHERE `id`='64';
			UPDATE {{chartcolumn}} SET `header`='Число зданий' WHERE `id`='65';
			UPDATE {{chartcolumn}} SET `header`='Построенных населением' WHERE `id`='66';
			UPDATE {{chartcolumn}} SET `header`='Построенных организациями' WHERE `id`='68';
			UPDATE {{chartcolumn}} SET `header`='Людей, занятых в строительстве' WHERE `id`='70';
			UPDATE {{chartcolumn}} SET `header`='Строительных организаций' WHERE `id`='72';
			UPDATE {{chartcolumn}} SET `header`='Жилищные кредиты' WHERE `id`='74';
			UPDATE {{chartcolumn}} SET `header`='Ипотечные кредиты' WHERE `id`='76';
			UPDATE {{chartcolumn}} SET `header`='Строительный объем зданий' WHERE `id`='78';
			UPDATE {{chartcolumn}} SET `header`='Число зданий' WHERE `id`='79';
			UPDATE {{chartcolumn}} SET `header`='Построенных населением' WHERE `id`='80';
			UPDATE {{chartcolumn}} SET `header`='Построенных организациями' WHERE `id`='82';
			UPDATE {{chartcolumn}} SET `header`='Людей, занятых в строительстве' WHERE `id`='84';
			UPDATE {{chartcolumn}} SET `header`='Строительных организаций' WHERE `id`='86';
			UPDATE {{chartcolumn}} SET `header`='Жилищные кредиты' WHERE `id`='88';
			UPDATE {{chartcolumn}} SET `header`='Ипотечные кредиты' WHERE `id`='90';
			UPDATE {{chartcolumn}} SET `header`='Производство кирпича' WHERE `id`='92';
		";
	}

}