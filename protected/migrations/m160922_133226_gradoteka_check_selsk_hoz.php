<?php

class m160922_133226_gradoteka_check_selsk_hoz extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{gobjects}};
            DELETE FROM {{gobjectshasgstattypes}};
            DELETE FROM {{gstattypes}};
            DELETE FROM {{gvalue}};
            
            ALTER TABLE {{gobjectshasgstattypes}} 
            DROP COLUMN `frequency`,
            DROP COLUMN `groupName`,
            DROP COLUMN `groupId`;
            
            ALTER TABLE {{gstattypes}} 
            DROP COLUMN `category`,
            DROP COLUMN `epochType`,
            CHANGE COLUMN `name` `name` VARCHAR(2000) NULL DEFAULT NULL AFTER `gId`,
            CHANGE COLUMN `gType` `type` VARCHAR(45) NULL DEFAULT NULL ,
            CHANGE COLUMN `units` `unit` VARCHAR(45) NULL DEFAULT NULL ,
            ADD COLUMN `parentName` VARCHAR(2000) NULL DEFAULT NULL AFTER `name`,
            ADD COLUMN `autoUpdate` INT(11) NULL DEFAULT NULL AFTER `unit`;
            
            ALTER TABLE {{gstattypes}} 
            CHANGE COLUMN `gId` `gId` VARCHAR(255) NULL DEFAULT NULL ;

            UPDATE {{charttable}} SET `name`='Объем и доля отраслей сельского хозяйства', `groupId`='1', `position`='1' WHERE `id`='3';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем сельского хозяйства', `groupId`='1', `position`='2' WHERE `id`='4';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем и доля отраслей сельского хозяйства в с/х организациях', `groupId`='1', `position`='3' WHERE `id`='5';
            UPDATE {{charttable}} SET `name`='Региональный товарный объем и доля отраслей сельского хозяйства в КФХ и ИП', `groupId`='1', `position`='4' WHERE `id`='6';
            UPDATE {{charttable}} SET `name`='Основные сельскохозяйственные культуры', `groupId`='2', `position`='1' WHERE `id`='7';
            UPDATE {{charttable}} SET `name`='Основная продукция животноводства', `groupId`='2', `position`='2' WHERE `id`='8';
            UPDATE {{charttable}} SET `groupId`='2', `position`='3' WHERE `id`='10';
            UPDATE {{charttable}} SET `groupId`='2', `position`='4' WHERE `id`='11';
            UPDATE {{charttable}} SET `groupId`='3', `position`='1' WHERE `id`='9';
            
            UPDATE {{charttable}} SET `name`='Экспорт / Импорт сельскохозяйственной продукции' WHERE `id`='9';
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('ТН ВЭД', '3', '11', 'country', '3', '2');

            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f11aa' WHERE `statId` = '103781';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f11a4' WHERE `statId` = '103783';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f21bd4c5654298b7a01' WHERE `statId` = '223019';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f16bd4c5654298b4f36' WHERE `statId` = '223021';
            UPDATE {{chartcolumn}} SET `statId` ='56d0311df7a9add4068f7bb7' WHERE `statId` = '220769';
            UPDATE {{chartcolumn}} SET `statId` ='57a447a775d8974e5b8b4568' WHERE `statId` = '220770';
            UPDATE {{chartcolumn}} SET `statId` ='56d0314bf7a9add4068fc78b' WHERE `statId` = '220772';
            UPDATE {{chartcolumn}} SET `statId` ='56d0315bf7a9add4068fe07f' WHERE `statId` = '220773';
            UPDATE {{chartcolumn}} SET `statId` ='56d03160f7a9add4068fe988' WHERE `statId` = '220774';
            UPDATE {{chartcolumn}} SET `statId` ='56d03183f7a9add4069020ad' WHERE `statId` = '220775';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d4c' WHERE `statId` = '223010';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d4e' WHERE `statId` = '223006';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d3e' WHERE `statId` = '220843';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d3e' WHERE `statId` = '220843';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d4a' WHERE `statId` = '223003';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d4a' WHERE `statId` = '223003';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d50' WHERE `statId` = '223009';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d42' WHERE `statId` = '220844';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d42' WHERE `statId` = '220844';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6c' WHERE `statId` = '220848';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d44' WHERE `statId` = '220848';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f70' WHERE `statId` = '223004';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d48' WHERE `statId` = '223004';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d3c' WHERE `statId` = '220842';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d3c' WHERE `statId` = '220842';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d39' WHERE `statId` = '220846';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d56' WHERE `statId` = '223008';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d46' WHERE `statId` = '220847';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d46' WHERE `statId` = '220847';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d40' WHERE `statId` = '220845';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d40' WHERE `statId` = '220845';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d54' WHERE `statId` = '223005';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d52' WHERE `statId` = '223007';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f74' WHERE `statId` = '223018';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f76' WHERE `statId` = '223012';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f66' WHERE `statId` = '220854';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f66' WHERE `statId` = '220854';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f72' WHERE `statId` = '223017';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f72' WHERE `statId` = '223017';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f78' WHERE `statId` = '223016';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6a' WHERE `statId` = '220853';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6a' WHERE `statId` = '220853';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6c' WHERE `statId` = '220849';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6c' WHERE `statId` = '220849';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6c' WHERE `statId` = '220849';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f70' WHERE `statId` = '223015';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f70' WHERE `statId` = '223015';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f61' WHERE `statId` = '220851';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f64' WHERE `statId` = '220855';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f64' WHERE `statId` = '220855';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f7e' WHERE `statId` = '223014';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6e' WHERE `statId` = '220850';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f6e' WHERE `statId` = '220850';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f7c' WHERE `statId` = '223011';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f68' WHERE `statId` = '220852';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f68' WHERE `statId` = '220852';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f7a' WHERE `statId` = '223013';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb59c' WHERE `statId` = '221549';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb59c' WHERE `statId` = '221322';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb5dc' WHERE `statId` = '221550';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb5dc' WHERE `statId` = '221323';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb5ec' WHERE `statId` = '221551';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb5ec' WHERE `statId` = '221324';
            UPDATE {{chartcolumn}} SET `statId` ='56d0a110f7a9adb827ce75ab' WHERE `statId` = '221547';
            UPDATE {{chartcolumn}} SET `statId` ='56d0a110f7a9adb827ce75ab' WHERE `statId` = '221326';
            UPDATE {{chartcolumn}} SET `statId` ='571e43f477d89702498b771f' WHERE `statId` = '221554';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f13bd4c5654298b4569' WHERE `statId` = '221544';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f13bd4c5654298b4569' WHERE `statId` = '221319';
            UPDATE {{chartcolumn}} SET `statId` ='571e452577d89702498c346f' WHERE `statId` = '221555';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f14bd4c5654298b48eb' WHERE `statId` = '221545';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f14bd4c5654298b48eb' WHERE `statId` = '221320';
            UPDATE {{chartcolumn}} SET `statId` ='5784b483bd4c56591c8b5af1' WHERE `statId` = '220732';
            UPDATE {{chartcolumn}} SET `statId` ='5784b484bd4c56591c8b5f4f' WHERE `statId` = '220736';
            UPDATE {{chartcolumn}} SET `statId` ='5784b484bd4c56591c8b5f4f' WHERE `statId` = '221370';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59da' WHERE `statId` = '220840';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59da' WHERE `statId` = '221501';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59da' WHERE `statId` = '220840';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59da' WHERE `statId` = '221501';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59db' WHERE `statId` = '221500';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59d9' WHERE `statId` = '221499';
            UPDATE {{chartcolumn}} SET `statId` ='56d0311df7a9add4068f7bb7' WHERE `statId` = '220769';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f16bd4c5654298b4f36' WHERE `statId` = '223021';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f15bd4c5654298b4c04' WHERE `statId` = '221421';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f15bd4c5654298b4c04' WHERE `statId` = '220837';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f16bd4c5654298b4f36' WHERE `statId` = '223021';
            UPDATE {{chartcolumn}} SET `statId` ='5784b5dbbd4c56124d8b5467' WHERE `statId` = '221579';
            UPDATE {{chartcolumn}} SET `statId` ='5784b5dbbd4c56124d8b5467' WHERE `statId` = '147194';
            UPDATE {{chartcolumn}} SET `statId` ='577e574ebd4c56f26c8b4923' WHERE `statId` = '221577';
            UPDATE {{chartcolumn}} SET `statId` ='577e574ebd4c56f26c8b4923' WHERE `statId` = '147197';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59db' WHERE `statId` = '220841';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59db' WHERE `statId` = '221500';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59db' WHERE `statId` = '220841';
            UPDATE {{chartcolumn}} SET `statId` ='57a447a775d8974e5b8b4568' WHERE `statId` = '220770';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59d9' WHERE `statId` = '220839';
            UPDATE {{chartcolumn}} SET `statId` ='56ec79f574d8974605996ffe' WHERE `statId` = '220784';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f17bd4c5654298b540e' WHERE `statId` = '221428';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f17bd4c5654298b540e' WHERE `statId` = '148324';
            UPDATE {{chartcolumn}} SET `statId` ='5776391e77d897b008acc728' WHERE `statId` = '221484';
            UPDATE {{chartcolumn}} SET `statId` ='5776391e77d897b008acc728' WHERE `statId` = '140517';
            UPDATE {{chartcolumn}} SET `statId` ='56ec847874d8974605a89f97' WHERE `statId` = '220778';
            UPDATE {{chartcolumn}} SET `statId` ='56d063e9f7a9ad73099fc3ff' WHERE `statId` = '220782';
            UPDATE {{chartcolumn}} SET `statId` ='56d02ef7f7a9ad01088b5796' WHERE `statId` = '220776';
            UPDATE {{chartcolumn}} SET `statId` ='56ec731777d8970e07922590' WHERE `statId` = '221552';
            UPDATE {{chartcolumn}} SET `statId` ='56ec731777d8970e07922590' WHERE `statId` = '220856';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0276d89734208b83ee' WHERE `statId` = '221539';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ec076d89734208b4ae7' WHERE `statId` = '220727';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ec076d89734208b4ae7' WHERE `statId` = '221341';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ec076d89734208b4ae7' WHERE `statId` = '220727';
            UPDATE {{chartcolumn}} SET `statId` ='5784b5dbbd4c56124d8b5467' WHERE `statId` = '221464';
            UPDATE {{chartcolumn}} SET `statId` ='5784b5dbbd4c56124d8b5467' WHERE `statId` = '220951';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8c2f7a9ad68148cdff1' WHERE `statId` = '221498';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8c2f7a9ad68148cdff1' WHERE `statId` = '220819';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb5bc' WHERE `statId` = '221548';
            UPDATE {{chartcolumn}} SET `statId` ='56d02eaaf7a9ad0b078bb5bc' WHERE `statId` = '221321';
            UPDATE {{chartcolumn}} SET `statId` ='56d0314bf7a9add4068fc78b' WHERE `statId` = '220772';
            UPDATE {{chartcolumn}} SET `statId` ='56ec79f574d897460599707c' WHERE `statId` = '220785';
            UPDATE {{chartcolumn}} SET `statId` ='56d06405f7a9ad7309a0051c' WHERE `statId` = '220783';
            UPDATE {{chartcolumn}} SET `statId` ='577e7e2ebd4c5654298b8509' WHERE `statId` = '221519';
            UPDATE {{chartcolumn}} SET `statId` ='577e7e2ebd4c5654298b8509' WHERE `statId` = '147165';
            UPDATE {{chartcolumn}} SET `statId` ='5784b47fbd4c56591c8b4d1d' WHERE `statId` = '223029';
            UPDATE {{chartcolumn}} SET `statId` ='56d03160f7a9add4068fe988' WHERE `statId` = '220774';
            UPDATE {{chartcolumn}} SET `statId` ='56ec7b3a74d89746059bf830' WHERE `statId` = '220781';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1abd4c5654298b5f95' WHERE `statId` = '221434';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1abd4c5654298b5f95' WHERE `statId` = '220816';
            UPDATE {{chartcolumn}} SET `statId` ='577e7e2fbd4c5654298b8ad2' WHERE `statId` = '220623';
            UPDATE {{chartcolumn}} SET `statId` ='577e7e2fbd4c5654298b8ad2' WHERE `statId` = '70605';
            UPDATE {{chartcolumn}} SET `statId` ='57a447a775d8974e5b8b4568' WHERE `statId` = '220709';
            UPDATE {{chartcolumn}} SET `statId` ='57a447a775d8974e5b8b4568' WHERE `statId` = '148328';
            UPDATE {{chartcolumn}} SET `statId` ='56d02ef7f7a9ad01088b5796' WHERE `statId` = '220717';
            UPDATE {{chartcolumn}} SET `statId` ='56d02ef7f7a9ad01088b5796' WHERE `statId` = '148330';
            UPDATE {{chartcolumn}} SET `statId` ='57a447af75d8974e5b8b5340' WHERE `statId` = '220711';
            UPDATE {{chartcolumn}} SET `statId` ='57a447af75d8974e5b8b5340' WHERE `statId` = '148331';
            UPDATE {{chartcolumn}} SET `statId` ='57a447c075d8974e5b8b57dc' WHERE `statId` = '220713';
            UPDATE {{chartcolumn}} SET `statId` ='57a447c075d8974e5b8b57dc' WHERE `statId` = '148333';
            UPDATE {{chartcolumn}} SET `statId` ='57a447aa75d8974e5b8b4a08' WHERE `statId` = '148329';
            UPDATE {{chartcolumn}} SET `statId` ='57a447aa75d8974e5b8b4a08' WHERE `statId` = '220710';
            UPDATE {{chartcolumn}} SET `statId` ='57a447c575d8974e5b8b60ad' WHERE `statId` = '220719';
            UPDATE {{chartcolumn}} SET `statId` ='57a447c575d8974e5b8b60ad' WHERE `statId` = '148335';
            UPDATE {{chartcolumn}} SET `statId` ='57a447c775d8974e5b8b6549' WHERE `statId` = '220715';
            UPDATE {{chartcolumn}} SET `statId` ='57a447c775d8974e5b8b6549' WHERE `statId` = '148337';
            UPDATE {{chartcolumn}} SET `statId` ='57a447ca75d8974e5b8b69e5' WHERE `statId` = '220716';
            UPDATE {{chartcolumn}} SET `statId` ='57a447ca75d8974e5b8b69e5' WHERE `statId` = '148338';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0676d89734208b9330' WHERE `statId` = '221532';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0676d89734208b9330' WHERE `statId` = '221329';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ef776d89734208b5ced' WHERE `statId` = '221529';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ef776d89734208b5ced' WHERE `statId` = '221327';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5676d89734208be205' WHERE `statId` = '221522';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5676d89734208be205' WHERE `statId` = '149043';
            UPDATE {{chartcolumn}} SET `statId` ='572b3efc76d89734208b6ceb' WHERE `statId` = '221534';
            UPDATE {{chartcolumn}} SET `statId` ='572b3efc76d89734208b6ceb' WHERE `statId` = '221331';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5676d89734208be543' WHERE `statId` = '221526';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5676d89734208be543' WHERE `statId` = '149041';
            UPDATE {{chartcolumn}} SET `statId` ='572b3efd76d89734208b719c' WHERE `statId` = '221535';
            UPDATE {{chartcolumn}} SET `statId` ='572b3efd76d89734208b719c' WHERE `statId` = '221332';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5776d89734208be881' WHERE `statId` = '221523';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5776d89734208be881' WHERE `statId` = '149044';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0876d89734208b992c' WHERE `statId` = '221540';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0876d89734208b992c' WHERE `statId` = '221337';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5976d89734208bf01b' WHERE `statId` = '221528';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5976d89734208bf01b' WHERE `statId` = '149048';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0376d89734208b8897' WHERE `statId` = '221541';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5876d89734208bebbf' WHERE `statId` = '221524';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0376d89734208b8897' WHERE `statId` = '221338';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5876d89734208bebbf' WHERE `statId` = '149045';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f0276d89734208b83ee' WHERE `statId` = '221336';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5976d89734208beefd' WHERE `statId` = '221530';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5976d89734208beefd' WHERE `statId` = '149046';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5576d89734208bdec7' WHERE `statId` = '221527';
            UPDATE {{chartcolumn}} SET `statId` ='572b3f5576d89734208bdec7' WHERE `statId` = '149042';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f11a4' WHERE `statId` = '103783';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f119e' WHERE `statId` = '103784';
            UPDATE {{chartcolumn}} SET `statId` ='571e45e177d89702498ca67f' WHERE `statId` = '221514';
            UPDATE {{chartcolumn}} SET `statId` ='571e451c77d89702498c2d61' WHERE `statId` = '221515';
            UPDATE {{chartcolumn}} SET `statId` ='577e7c79bd4c562e658b465a' WHERE `statId` = '221502';
            UPDATE {{chartcolumn}} SET `statId` ='577e7c79bd4c562e658b465a' WHERE `statId` = '221502';
            UPDATE {{chartcolumn}} SET `statId` ='572b3efc76d89734208b6ceb' WHERE `statId` = '221503';
            UPDATE {{chartcolumn}} SET `statId` ='572b3efc76d89734208b6ceb' WHERE `statId` = '221503';
            UPDATE {{chartcolumn}} SET `statId` ='571e43f377d89702498b7475' WHERE `statId` = '221505';
            UPDATE {{chartcolumn}} SET `statId` ='571e43f377d89702498b7475' WHERE `statId` = '221505';
            UPDATE {{chartcolumn}} SET `statId` ='577e7c79bd4c562e658b4684' WHERE `statId` = '221423';
            UPDATE {{chartcolumn}} SET `statId` ='577e7c79bd4c562e658b4684' WHERE `statId` = '220834';
            UPDATE {{chartcolumn}} SET `statId` ='571e448d77d89702498bdb5d' WHERE `statId` = '221430';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ef776d89734208b5ced' WHERE `statId` = '221504';
            UPDATE {{chartcolumn}} SET `statId` ='572b3ef776d89734208b5ced' WHERE `statId` = '221504';
            UPDATE {{chartcolumn}} SET `statId` ='571e446c77d89702498bc5ed' WHERE `statId` = '221429';
            UPDATE {{chartcolumn}} SET `statId` ='571e443077d89702498ba0e3' WHERE `statId` = '221506';
            UPDATE {{chartcolumn}} SET `statId` ='571e443077d89702498ba0e3' WHERE `statId` = '221506';
            UPDATE {{chartcolumn}} SET `statId` ='571e449a77d89702498bdec6' WHERE `statId` = '221507';
            UPDATE {{chartcolumn}} SET `statId` ='571e449a77d89702498bdec6' WHERE `statId` = '221507';
            UPDATE {{chartcolumn}} SET `statId` ='571e44ae77d89702498befb9' WHERE `statId` = '221508';
            UPDATE {{chartcolumn}} SET `statId` ='571e44ae77d89702498befb9' WHERE `statId` = '221508';
            UPDATE {{chartcolumn}} SET `statId` ='571e44c477d89702498bfc67' WHERE `statId` = '221425';
            UPDATE {{chartcolumn}} SET `statId` ='571e44d677d89702498c0a4c' WHERE `statId` = '221509';
            UPDATE {{chartcolumn}} SET `statId` ='571e44d677d89702498c0a4c' WHERE `statId` = '221509';
            UPDATE {{chartcolumn}} SET `statId` ='571e44d577d89702498c073f' WHERE `statId` = '221510';
            UPDATE {{chartcolumn}} SET `statId` ='571e44d577d89702498c073f' WHERE `statId` = '221510';
            UPDATE {{chartcolumn}} SET `statId` ='571e467977d89702498cca97' WHERE `statId` = '221557';
            UPDATE {{chartcolumn}} SET `statId` ='56d02ef7f7a9ad01088b5796' WHERE `statId` = '221419';
            UPDATE {{chartcolumn}} SET `statId` ='56d02ef7f7a9ad01088b5796' WHERE `statId` = '220776';
            UPDATE {{chartcolumn}} SET `statId` ='56d02ef7f7a9ad01088b5796' WHERE `statId` = '221419';
            UPDATE {{chartcolumn}} SET `statId` ='571e451377d89702498c26ff' WHERE `statId` = '221511';
            UPDATE {{chartcolumn}} SET `statId` ='571e451377d89702498c26ff' WHERE `statId` = '221511';
            UPDATE {{chartcolumn}} SET `statId` ='571e451c77d89702498c2d61' WHERE `statId` = '221515';
            UPDATE {{chartcolumn}} SET `statId` ='571e451d77d89702498c2dd7' WHERE `statId` = '221516';
            UPDATE {{chartcolumn}} SET `statId` ='571e451d77d89702498c2dd7' WHERE `statId` = '221516';
            UPDATE {{chartcolumn}} SET `statId` ='571e456577d89702498c5a15' WHERE `statId` = '221512';
            UPDATE {{chartcolumn}} SET `statId` ='571e456577d89702498c5a15' WHERE `statId` = '221512';
            UPDATE {{chartcolumn}} SET `statId` ='571e45d777d89702498c9e6a' WHERE `statId` = '221513';
            UPDATE {{chartcolumn}} SET `statId` ='571e45d777d89702498c9e6a' WHERE `statId` = '221513';
            UPDATE {{chartcolumn}} SET `statId` ='571e45e177d89702498ca67f' WHERE `statId` = '221514';
            UPDATE {{chartcolumn}} SET `statId` ='571e467377d89702498cc8a3' WHERE `statId` = '221556';
            UPDATE {{chartcolumn}} SET `statId` ='571e46b177d89702498cf042' WHERE `statId` = '221558';
            UPDATE {{chartcolumn}} SET `statId` ='56ec847874d8974605a89f97' WHERE `statId` = '220778';
            UPDATE {{chartcolumn}} SET `statId` ='571e46d877d89702498d0936' WHERE `statId` = '221426';
            UPDATE {{chartcolumn}} SET `statId` ='571e46e777d89702498d109a' WHERE `statId` = '221517';
            UPDATE {{chartcolumn}} SET `statId` ='571e46e777d89702498d109a' WHERE `statId` = '221517';
            UPDATE {{chartcolumn}} SET `statId` ='571e46e877d89702498d12eb' WHERE `statId` = '221427';
            UPDATE {{chartcolumn}} SET `statId` ='5784b78bbd4c56dc4f8b72e7' WHERE `statId` = '221431';
            UPDATE {{chartcolumn}} SET `statId` ='571e45b677d89702498c8a03' WHERE `statId` = '221559';
            UPDATE {{chartcolumn}} SET `statId` ='56ec853b74d8974605aa82e3' WHERE `statId` = '220779';
            UPDATE {{chartcolumn}} SET `statId` ='577e7e31bd4c5654298b9031' WHERE `statId` = '221553';
            UPDATE {{chartcolumn}} SET `statId` ='577e7e31bd4c5654298b9031' WHERE `statId` = '114202';
            UPDATE {{chartcolumn}} SET `statId` ='56d0518ff7a9adf30997bab2' WHERE `statId` = '220780';
            UPDATE {{chartcolumn}} SET `statId` ='5784b485bd4c56591c8b6399' WHERE `statId` = '220731';
            UPDATE {{chartcolumn}} SET `statId` ='5784b485bd4c56591c8b6176' WHERE `statId` = '220735';
            UPDATE {{chartcolumn}} SET `statId` ='5784b485bd4c56591c8b6176' WHERE `statId` = '221355';
            UPDATE {{chartcolumn}} SET `statId` ='56ec858b74d8974605ab44c6' WHERE `statId` = '220788';
            UPDATE {{chartcolumn}} SET `statId` ='5784b47dbd4c56591c8b4568' WHERE `statId` = '223026';
            UPDATE {{chartcolumn}} SET `statId` ='572facee73d897e665f8a38f' WHERE `statId` = '221470';
            UPDATE {{chartcolumn}} SET `statId` ='572fad0773d897e665f8f8b4' WHERE `statId` = '221469';
            UPDATE {{chartcolumn}} SET `statId` ='572fad0773d897e665f8f8b4' WHERE `statId` = '220937';
            UPDATE {{chartcolumn}} SET `statId` ='572facee73d897e665f8a38f' WHERE `statId` = '220925';
            UPDATE {{chartcolumn}} SET `statId` ='572fad0c73d897e665f909a6' WHERE `statId` = '221479';
            UPDATE {{chartcolumn}} SET `statId` ='572fad0c73d897e665f909a6' WHERE `statId` = '220939';
            UPDATE {{chartcolumn}} SET `statId` ='5784b47ebd4c56591c8b4958' WHERE `statId` = '223025';
            UPDATE {{chartcolumn}} SET `statId` ='56ec84ee74d8974605a991cd' WHERE `statId` = '220777';
            UPDATE {{chartcolumn}} SET `statId` ='5784b480bd4c56591c8b4faf' WHERE `statId` = '223027';
            UPDATE {{chartcolumn}} SET `statId` ='56d03183f7a9add4069020ad' WHERE `statId` = '220775';
            UPDATE {{chartcolumn}} SET `statId` ='56ec79f574d89746059970ad' WHERE `statId` = '220787';
            UPDATE {{chartcolumn}} SET `statId` ='56ec84ee74d8974605a991cd' WHERE `statId` = '220777';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8d4f7a9ad68148d1530' WHERE `statId` = '221432';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8d4f7a9ad68148d1530' WHERE `statId` = '220815';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1ebd4c5654298b6eb8' WHERE `statId` = '221449';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1ebd4c5654298b6eb8' WHERE `statId` = '220831';
            UPDATE {{chartcolumn}} SET `statId` ='5784b487bd4c56591c8b6a26' WHERE `statId` = '221546';
            UPDATE {{chartcolumn}} SET `statId` ='5784b487bd4c56591c8b6a26' WHERE `statId` = '221325';
            UPDATE {{chartcolumn}} SET `statId` ='56d0315bf7a9add4068fe07f' WHERE `statId` = '220773';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f11aa' WHERE `statId` = '221343';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f11aa' WHERE `statId` = '103781';
            UPDATE {{chartcolumn}} SET `statId` ='56ec793a75d897c6069d7b82' WHERE `statId` = '150772';
            UPDATE {{chartcolumn}} SET `statId` ='56ec793a75d897c6069d7b82' WHERE `statId` = '70294';
            UPDATE {{chartcolumn}} SET `statId` ='56ec793a75d897c6069d7b82' WHERE `statId` = '150772';
            UPDATE {{chartcolumn}} SET `statId` ='56ec793a75d897c6069d7b82' WHERE `statId` = '70294';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f19bd4c5654298b5c48' WHERE `statId` = '221424';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f19bd4c5654298b5c48' WHERE `statId` = '220836';
            UPDATE {{chartcolumn}} SET `statId` ='5776396277d897b008ad7798' WHERE `statId` = '221491';
            UPDATE {{chartcolumn}} SET `statId` ='5776396277d897b008ad7798' WHERE `statId` = '140516';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8aff7a9ad68148caaa4' WHERE `statId` = '221433';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8aff7a9ad68148caaa4' WHERE `statId` = '103707';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1cbd4c5654298b64e0' WHERE `statId` = '221594';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1cbd4c5654298b64e0' WHERE `statId` = '114199';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1dbd4c5654298b6b70' WHERE `statId` = '221593';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1dbd4c5654298b6b70' WHERE `statId` = '114201';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1fbd4c5654298b736b' WHERE `statId` = '221422';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1fbd4c5654298b736b' WHERE `statId` = '220835';
            UPDATE {{chartcolumn}} SET `statId` ='5745b180bd4c56b4668b4713' WHERE `statId` = '220728';
            UPDATE {{chartcolumn}} SET `statId` ='56ec853b74d8974605aa82e3' WHERE `statId` = '220779';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f21bd4c5654298b7a01' WHERE `statId` = '223019';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f20bd4c5654298b76f6' WHERE `statId` = '221420';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f20bd4c5654298b76f6' WHERE `statId` = '220838';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f21bd4c5654298b7a01' WHERE `statId` = '223019';
            UPDATE {{chartcolumn}} SET `statId` ='56d0518ff7a9adf30997bab2' WHERE `statId` = '220780';
            UPDATE {{chartcolumn}} SET `statId` ='572facee73d897e665f8a38f' WHERE `statId` = '221470';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f13bd4c5654298b4569' WHERE `statId` = '221544';
            UPDATE {{chartcolumn}} SET `statId` ='572facee73d897e665f8a38f' WHERE `statId` = '220925';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f13bd4c5654298b4569' WHERE `statId` = '221319';
            UPDATE {{chartcolumn}} SET `statId` ='572fad0c73d897e665f909a6' WHERE `statId` = '221479';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f14bd4c5654298b48eb' WHERE `statId` = '221545';
            UPDATE {{chartcolumn}} SET `statId` ='572fad0c73d897e665f909a6' WHERE `statId` = '220939';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f14bd4c5654298b48eb' WHERE `statId` = '221320';
            UPDATE {{chartcolumn}} SET `statId` ='5784b485bd4c56591c8b6176' WHERE `statId` = '221355';
            UPDATE {{chartcolumn}} SET `statId` ='5784b485bd4c56591c8b6399' WHERE `statId` = '220731';
            UPDATE {{chartcolumn}} SET `statId` ='5784b485bd4c56591c8b6176' WHERE `statId` = '220735';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1abd4c5654298b5f95' WHERE `statId` = '221434';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1abd4c5654298b5f95' WHERE `statId` = '220816';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1fbd4c5654298b736b' WHERE `statId` = '221422';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1fbd4c5654298b736b' WHERE `statId` = '220835';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1cbd4c5654298b64e0' WHERE `statId` = '221594';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1cbd4c5654298b64e0' WHERE `statId` = '114199';
            UPDATE {{chartcolumn}} SET `statId` ='5745b180bd4c56b4668b4713' WHERE `statId` = '220728';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1ebd4c5654298b6eb8' WHERE `statId` = '221449';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1ebd4c5654298b6eb8' WHERE `statId` = '220831';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1dbd4c5654298b6b70' WHERE `statId` = '221593';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f1dbd4c5654298b6b70' WHERE `statId` = '114201';
            UPDATE {{chartcolumn}} SET `statId` ='571e46d877d89702498d0936' WHERE `statId` = '221426';
            UPDATE {{chartcolumn}} SET `statId` ='577e574ebd4c56f26c8b4923' WHERE `statId` = '221577';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59d9' WHERE `statId` = '221499';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59d9' WHERE `statId` = '220839';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f11aa' WHERE `statId` = '221343';
            UPDATE {{chartcolumn}} SET `statId` ='56d030def7a9ad0b078f119e' WHERE `statId` = '103784';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59db' WHERE `statId` = '221500';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59da' WHERE `statId` = '221501';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59da' WHERE `statId` = '220840';
            UPDATE {{chartcolumn}} SET `statId` ='56fd125409eed4ef758b59db' WHERE `statId` = '220841';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f20bd4c5654298b76f6' WHERE `statId` = '221420';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f15bd4c5654298b4c04' WHERE `statId` = '221421';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f15bd4c5654298b4c04' WHERE `statId` = '220837';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f20bd4c5654298b76f6' WHERE `statId` = '220838';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f21bd4c5654298b7a01' WHERE `statId` = '223019';
            UPDATE {{chartcolumn}} SET `statId` ='577e6f16bd4c5654298b4f36' WHERE `statId` = '223021';
            UPDATE {{chartcolumn}} SET `statId` ='571e43f477d89702498b771f' WHERE `statId` = '221554';
            UPDATE {{chartcolumn}} SET `statId` ='571e446c77d89702498bc5ed' WHERE `statId` = '221429';
            UPDATE {{chartcolumn}} SET `statId` ='571e46e877d89702498d12eb' WHERE `statId` = '221427';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8c2f7a9ad68148cdff1' WHERE `statId` = '221498';
            UPDATE {{chartcolumn}} SET `statId` ='5784b781bd4c56dc4f8b5d56' WHERE `statId` = '223008';
            UPDATE {{chartcolumn}} SET `statId` ='5784b789bd4c56dc4f8b6f7e' WHERE `statId` = '223014';
            UPDATE {{chartcolumn}} SET `statId` ='571e44c477d89702498bfc67' WHERE `statId` = '221425';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8d4f7a9ad68148d1530' WHERE `statId` = '221432';
            UPDATE {{chartcolumn}} SET `statId` ='56d2f8d4f7a9ad68148d1530' WHERE `statId` = '220815';
            UPDATE {{chartcolumn}} SET `statId` ='571e448d77d89702498bdb5d' WHERE `statId` = '221430';
            UPDATE {{chartcolumn}} SET `statId` ='577e7c79bd4c562e658b4684' WHERE `statId` = '221423';
            UPDATE {{chartcolumn}} SET `statId` ='577e7c79bd4c562e658b4684' WHERE `statId` = '220834';
            UPDATE {{chartcolumn}} SET `statId` ='571e452577d89702498c346f' WHERE `statId` = '221555';
            UPDATE {{chartcolumn}} SET `statId` ='5784b78bbd4c56dc4f8b72e7' WHERE `statId` = '221431';
            UPDATE {{chartcolumn}} SET `statId` ='5776391e77d897b008acc728' WHERE `statId` = '221484';
            UPDATE {{chartcolumn}} SET `statId` ='5776391e77d897b008acc728' WHERE `statId` = '140517';
            UPDATE {{chartcolumn}} SET `statId` ='5776396277d897b008ad7798' WHERE `statId` = '221491';
            UPDATE {{chartcolumn}} SET `statId` ='5776396277d897b008ad7798' WHERE `statId` = '140516';

            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('1', 'Экспорт РФ', '2015', '81', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('2', 'Импорт РФ', '2015', '81', '#a0d468', '2');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}