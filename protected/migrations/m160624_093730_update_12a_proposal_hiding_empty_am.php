<?php

class m160624_093730_update_12a_proposal_hiding_empty_am extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{attributeclass}} (`name`) VALUES ('n12a_super1');
			INSERT INTO {{attributeclass}} (`name`) VALUES ('n12a_super2');
			INSERT INTO {{attributeclass}} (`name`) VALUES ('n12a_super3');
			INSERT INTO {{attributeclass}} (`name`) VALUES ('n12a_super4');
			INSERT INTO {{attributeclass}} (`name`) VALUES ('n12a_super5');
			INSERT INTO {{attributeclass}} (`name`) VALUES ('n12a_super6');
			UPDATE {{attributemodel}} SET `attributeClass`='2298' WHERE `id`='192';
			UPDATE {{attributemodel}} SET `attributeClass`='2299' WHERE `id`='193';
			UPDATE {{attributemodel}} SET `attributeClass`='2300' WHERE `id`='194';
			UPDATE {{attributemodel}} SET `attributeClass`='2301' WHERE `id`='195';
			UPDATE {{attributemodel}} SET `attributeClass`='2302' WHERE `id`='196';
			UPDATE {{attributemodel}} SET `attributeClass`='2303' WHERE `id`='197';
			UPDATE {{attributeclass}} SET `parent`='2298', `super`='2298' WHERE `id`='2281';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2282';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2283';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2284';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2285';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2286';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2287';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2288';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2289';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2290';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2291';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2292';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2293';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2294';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2295';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2281';
			UPDATE {{attributeclass}} SET `parent`='2298', `super`='2298' WHERE `id`='2206';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2207';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2208';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2209';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2210';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2211';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2212';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2213';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2214';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2215';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2216';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2217';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2218';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2219';
			UPDATE {{attributeclass}} SET `super`='2298' WHERE `id`='2220';
			UPDATE {{attributeclass}} SET `parent`='2299', `super`='2299' WHERE `id`='2221';
			UPDATE {{attributeclass}} SET `parent`='2221', `super`='2299' WHERE `id`='2222';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2223';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2224';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2225';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2226';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2227';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2228';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2229';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2230';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2231';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2232';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2233';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2234';
			UPDATE {{attributeclass}} SET `super`='2299' WHERE `id`='2235';
			UPDATE {{attributeclass}} SET `parent`='2300', `super`='2300' WHERE `id`='2236';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2237';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2238';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2239';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2250';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2249';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2248';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2247';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2246';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2244';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2245';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2243';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2242';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2241';
			UPDATE {{attributeclass}} SET `super`='2300' WHERE `id`='2240';
			UPDATE {{attributeclass}} SET `parent`='2301', `super`='2301' WHERE `id`='2251';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2252';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2253';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2254';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2255';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2256';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2257';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2258';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2259';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2260';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2261';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2262';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2263';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2264';
			UPDATE {{attributeclass}} SET `super`='2301' WHERE `id`='2265';
			UPDATE {{attributeclass}} SET `parent`='2302', `super`='2302' WHERE `id`='2266';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2267';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2268';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2269';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2270';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2271';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2272';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2273';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2274';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2275';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2276';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2277';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2278';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2279';
			UPDATE {{attributeclass}} SET `super`='2302' WHERE `id`='2280';
			UPDATE {{attributeclass}} SET `parent`='2303', `super`='2303' WHERE `id`='2281';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2282';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2283';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2284';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2285';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2286';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2287';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2288';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2289';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2290';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2291';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2292';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2293';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2294';
			UPDATE {{attributeclass}} SET `super`='2303' WHERE `id`='2295';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2266';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2251';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2236';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2221';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2206';
			UPDATE {{attributeclass}} SET `class`='headerH1WithWrap' WHERE `id`='2281';
			UPDATE {{attributeclass}} SET `class`='headerH1WithWrap' WHERE `id`='2266';
			UPDATE {{attributeclass}} SET `class`='headerH1WithWrap' WHERE `id`='2251';
			UPDATE {{attributeclass}} SET `class`='headerH1WithWrap' WHERE `id`='2236';
			UPDATE {{attributeclass}} SET `class`='headerH1WithWrap' WHERE `id`='2221';
			UPDATE {{attributeclass}} SET `class`='headerH1WithWrap' WHERE `id`='2206';
";
	}
}