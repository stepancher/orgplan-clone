<?php

class m160616_141219_add_translates_to_calendar extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR (discount 20%)' WHERE `id`='382';
			UPDATE {{trcalendarfairtasks}} SET `name`='Sign the Contract (discount 20%) ' WHERE `id`='383';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 10%)' WHERE `id`='384';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 40%)' WHERE `id`='385';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='386';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR (discount 15%)' WHERE `id`='387';
			UPDATE {{trcalendarfairtasks}} SET `name`='Sign the Contract (discount 15%) ' WHERE `id`='388';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='389';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='390';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR (discount 10%)' WHERE `id`='391';
			UPDATE {{trcalendarfairtasks}} SET `name`='Sign the Contract (discount 10%) ' WHERE `id`='392';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 100%)' WHERE `id`='393';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.2 STANDARD STAND' WHERE `id`='394';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.2)' WHERE `id`='395';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.2 STANDARD STAND (increase of price 50%)' WHERE `id`='396';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.2) (increase of price 50%)' WHERE `id`='397';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.2 STANDARD STAND (increase of price 100%)' WHERE `id`='398';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.2) (increase of price 100%)' WHERE `id`='399';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='400';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='401';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='402';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR' WHERE `id`='403';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.3 ANCILLARY EQUIPMENT' WHERE `id`='404';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.3)' WHERE `id`='405';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 50%)' WHERE `id`='406';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.3) (increase of price 50%)' WHERE `id`='407';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 100%)' WHERE `id`='408';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.3) (increase of price 100%)' WHERE `id`='409';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS' WHERE `id`='410';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.4)' WHERE `id`='411';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 50%)' WHERE `id`='412';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.4) (increase of price 50%)' WHERE `id`='413';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 100%)' WHERE `id`='414';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.4) (increase of price 100%)' WHERE `id`='415';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.5 COMMUNICATION SERVICES' WHERE `id`='416';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.5)' WHERE `id`='417';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.5 COMMUNICATION SERVICES' WHERE `id`='418';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.5)' WHERE `id`='419';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.5 COMMUNICATION SERVICES (increase of price 100%)' WHERE `id`='420';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.5) (increase of price 100%)' WHERE `id`='421';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.9 Freight forwarding services including reservation of loading times' WHERE `id`='422';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Contest Application form including photos and descriptions' WHERE `id`='423';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='424';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 100%)' WHERE `id`='425';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR (discount 20%)' WHERE `id`='426';
			UPDATE {{trcalendarfairtasks}} SET `name`='Sign the Contract (discount 20%) ' WHERE `id`='427';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 10%)' WHERE `id`='428';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 40%)' WHERE `id`='429';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='430';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR (discount 15%)' WHERE `id`='431';
			UPDATE {{trcalendarfairtasks}} SET `name`='Sign the Contract (discount 15%) ' WHERE `id`='432';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='433';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='434';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR (discount 10%)' WHERE `id`='435';
			UPDATE {{trcalendarfairtasks}} SET `name`='Sign the Contract (discount 10%) ' WHERE `id`='436';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 100%)' WHERE `id`='437';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.2 STANDARD STAND' WHERE `id`='438';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.2)' WHERE `id`='439';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.2 STANDARD STAND (increase of price 50%)' WHERE `id`='440';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.2) (increase of price 50%)' WHERE `id`='441';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.2 STANDARD STAND (increase of price 100%)' WHERE `id`='442';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.2) (increase of price 100%)' WHERE `id`='443';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='444';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='445';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='446';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.1 EXHIBITOR' WHERE `id`='447';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.3 ANCILLARY EQUIPMENT' WHERE `id`='448';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.3)' WHERE `id`='449';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 50%)' WHERE `id`='450';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.3) (increase of price 50%)' WHERE `id`='451';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.3 ANCILLARY EQUIPMENT (increase of price 100%)' WHERE `id`='452';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.3) (increase of price 100%)' WHERE `id`='453';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS' WHERE `id`='454';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.4)' WHERE `id`='455';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 50%)' WHERE `id`='456';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.4) (increase of price 50%)' WHERE `id`='457';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.4 TECHNICAL AND ENGINEERING CONNECTIONS (increase of price 100%)' WHERE `id`='458';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.4) (increase of price 100%)' WHERE `id`='459';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Contest Application form including photos and descriptions' WHERE `id`='479';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 50%)' WHERE `id`='480';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for Participation (down payment 100%)' WHERE `id`='481';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.9)' WHERE `id`='482';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.9 Freight forwarding services including reservation of loading times (increase of price 50%)' WHERE `id`='483';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.9) (increase of price 50%)' WHERE `id`='484';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.9 Freight forwarding services including reservation of loading times (increase of price 100%)' WHERE `id`='485';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.9) (increase of price 100%)' WHERE `id`='486';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.5 COMMUNICATION SERVICES' WHERE `id`='460';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.5)' WHERE `id`='461';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.5 COMMUNICATION SERVICES' WHERE `id`='462';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.5)' WHERE `id`='463';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.5 COMMUNICATION SERVICES (increase of price 100%)' WHERE `id`='464';
			UPDATE {{trcalendarfairtasks}} SET `name`='Pay for the service odered (Application No.5) (increase of price 100%)' WHERE `id`='465';
			UPDATE {{trcalendarfairtasks}} SET `name`='Send the Application No.9 Freight forwarding services including reservation of loading times' WHERE `id`='466';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='382';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH after signature and sending the Contract' WHERE `id`='383';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='384';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='385';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='386';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='387';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH after signature and sending the Contract' WHERE `id`='388';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='389';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='390';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='391';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH after signature and sending the Contract' WHERE `id`='392';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='393';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='395';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='397';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='399';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='400';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='401';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='402';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='405';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='407';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='409';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='411';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='413';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='415';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='417';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='419';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='421';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='424';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='425';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='428';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='429';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='430';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='433';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='434';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='437';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='439';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='441';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='443';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='444';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='445';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='446';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='449';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='451';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='453';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='455';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='457';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='459';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='461';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='463';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='465';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='468';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='470';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='472';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='474';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='476';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='478';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='480';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='481';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='482';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='484';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH if you have paid' WHERE `id`='486';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.9' WHERE `id`='483';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.9' WHERE `id`='485';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Contest Application' WHERE `id`='479';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.9' WHERE `id`='466';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.5' WHERE `id`='464';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.5' WHERE `id`='462';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.5' WHERE `id`='460';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.4' WHERE `id`='458';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.4' WHERE `id`='456';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.4' WHERE `id`='454';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.3' WHERE `id`='452';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.3' WHERE `id`='450';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.3' WHERE `id`='448';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='447';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.2' WHERE `id`='442';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.2' WHERE `id`='440';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.2' WHERE `id`='438';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='435';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='431';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH after signature and sending the Contract' WHERE `id`='432';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH after signature and sending the Contract' WHERE `id`='436';
			UPDATE {{trcalendarfairtasks}} SET `desc`='Click FINISH after signature and sending the Contract' WHERE `id`='427';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='426';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Contest Application' WHERE `id`='423';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.9' WHERE `id`='422';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.5' WHERE `id`='420';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.5' WHERE `id`='418';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.5' WHERE `id`='416';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.4' WHERE `id`='414';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.4' WHERE `id`='412';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.4' WHERE `id`='410';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.3' WHERE `id`='408';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.3' WHERE `id`='406';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.3' WHERE `id`='404';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.1' WHERE `id`='403';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.2' WHERE `id`='398';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.2' WHERE `id`='396';
			UPDATE {{trcalendarfairtasks}} SET `desc`='The task will be completed automatically after sending the Application No.2' WHERE `id`='394';
		";
	}
}