<?php

class m160707_125858_tasks_from_file extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
	UPDATE {{trattributeclass}} SET `label`='Премиум (от 9 до 54 м2)' WHERE `id`='208';
	UPDATE {{trattributeclass}} SET `label`='Классик (от 9 до 36 м2)' WHERE `id`='207';
	UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.' WHERE `id`='1077';
	UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> \nСтоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.' WHERE `id`='425';
	UPDATE {{trdocuments}} SET `name`='Contest terms & conditions' WHERE `id`='17';
	UPDATE {{trdocuments}} SET `name`='Requirements and Rules-rus' WHERE `id`='18';
	UPDATE {{trdocuments}} SET `name`='Passage scheme' WHERE `id`='19';
	UPDATE {{trdocuments}} SET `name`='Stand layout' WHERE `id`='20';
	UPDATE {{trdocuments}} SET `name`='User manual' WHERE `id`='26';
	UPDATE {{attributeclass}} SET `class`='headerH1Invisible' WHERE `id`='520';
	UPDATE {{attributeclass}} SET `class`='headerH1' WHERE `id`='336';
	INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('336', 'labelForView');
	INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2180', 'ru', 'Заказанное оборудование');
	INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2180', 'en', 'Ordered equipment');
	UPDATE {{trattributeclassproperty}} SET `value`='Заказанное оборудование:' WHERE `id`='2824';
	UPDATE {{trattributeclassproperty}} SET `value`='Ordered equipment:' WHERE `id`='2825';
	UPDATE {{attributeclass}} SET `class`='spinner' WHERE `id`='646';
	INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('646', '100', 'step');
	UPDATE {{attributeclass}} SET `class`='spinner' WHERE `id`='625';
	UPDATE {{attributeclass}} SET `class`='spinner' WHERE `id`='623';
	UPDATE {{attributeclass}} SET `class`='spinner' WHERE `id`='2591';
	UPDATE {{attributeclass}} SET `class`='spinner' WHERE `id`='589';
	INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '625', '100', 'step');
	INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '623', '100', 'step');
	INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '591', '100', 'step');
	INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '589', '100', 'step');

		";
	}
}