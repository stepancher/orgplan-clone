<?php

class m160317_082239_insert_into_route_auth_and_login extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
			DELETE FROM {{route}} where
			`url` = '/ru/login' AND
			`route` = 'site/auth' AND
			`params` = '[]';

			DELETE FROM {{route}} where
			`url` = '/ru/registration' AND
			`route` = 'site/reg' AND
			`params` = '[]';
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{route}} (`title`, `url`, `route`, `header`, `lang`,  `params`) VALUES (
				'Orgplan | Авторизация',
				'/ru/login',
				'site/auth',
				'Авторизация',
				'ru',
				'[]'
			);

			INSERT INTO {{route}} (`title`, `url`, `route`, `header`, `lang`, `params`) VALUES (
				'Orgplan | Регистрация',
				'/ru/registration',
				'site/reg',
				'Регистрация',
				'ru',
				'[]'
			);
		";
	}
}