<?php

class m160610_062527_insert_new_fairtasks_for_calendar extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295253',NULL,'23755881984295253',NULL,'2016-08-19 23:59:59',NULL,0,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295253','ru','Отправить заявку №6 \"АУДИО- И ВИДЕОТЕХНИКА\"','Задача завершится автоматически, когда будет отправлена заявка №6');

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295253','en','Send the Application No.6 AUDIO & VIDEO EQUIPMENT','The task will be completed automatically after sending the Application No.6');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295254','23755881984295254','23755881984295253',10,NULL,'#f6a800'	,1,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295254','ru','Оплатить заявку №6 \"АУДИО- И ВИДЕОТЕХНИКА\"','Нажмите \"Завершить\", когда будет произведена оплата');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295254','en','Pay for the service odered (Application No.6)','Click FINISH if you have paid');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295255',NULL,'23755881984295253',35,'2016-08-20 00:00:00',NULL,0,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295255','ru','Отправить заявку №6 \"АУДИО- И ВИДЕОТЕХНИКА\" (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №6');

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295255','en','Send the Application No.6 AUDIO & VIDEO EQUIPMENT (increase of price 50%)','The task will be completed automatically after sending the Application No.6');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295256','23755881984295256','23755881984295255',5,NULL,'#f6a800',1,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295256','ru','Оплатить заявку №6 \"АУДИО- И ВИДЕОТЕХНИКА\" (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата');

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295256','en','Pay for the service odered (Application No.6) (increase of price 50%)','Click FINISH if you have paid');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295257',NULL,'23755881984295253',10,'2016-09-24 00:00:00',NULL,0,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295257','ru','Отправить заявку №6 \"АУДИО- И ВИДЕОТЕХНИКА\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №6');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295257','en','Send the Application No.6 AUDIO & VIDEO EQUIPMENT (increase of price 100%)	','The task will be completed automatically after sending the Application No.6	');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295258','23755881984295258','23755881984295257',3,NULL,'#f96a0e'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295258','ru','Оплатить заявку №6 \"АУДИО- И ВИДЕОТЕХНИКА\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295258','en','Pay for the service odered (Application No.6) (increase of price 100%)','Click FINISH if you have paid');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295259',NULL,'23755881984295259',NULL,'2016-08-18 23:59:59',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295259','ru','Отправить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\"','Задача завершится автоматически, когда будет отправлена заявка №10');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295259','en','Send the Application No.10 BUSINESS EVENT','The task will be completed automatically after sending the Application No.10');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295260','23755881984295260','23755881984295259',10,NULL,'#f6a800'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295260','ru','Оплатить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\"','Нажмите \"Завершить\", когда будет произведена оплата');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295260','en','Pay for the service odered (Application No.10)','Click FINISH if you have paid');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295263',NULL,'23755881984295259',46,'2016-08-19 00:00:00',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295263','ru','Отправить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №10');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295263','en','Send the Application No.10 BUSINESS EVENT (increase of price 100%)','The task will be completed automatically after sending the Application No.10');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295264','23755881984295264','23755881984295263',3,NULL,'#f96a0e'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295264','ru','Оплатить заявку №10 \"ДЕЛОВОЕ МЕРОПРИЯТИЕ\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295264','en','Pay for the service odered (Application No.10) (increase of price 100%)','Click FINISH if you have paid');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295265',NULL,'23755881984295265',NULL,'2016-08-18 23:59:59',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295265','ru','Отправить заявку №11 \"ОБРЗОВАТЕЛЬНОЕ МЕРОПРИЯТИЕ\"','Задача завершится автоматически, когда будет отправлена заявка №11');
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295265','en','Send the Application No.11 EDUCATIONAL EVENT','The task will be completed automatically after sending the Application No.11');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295271',NULL,'23755881984295271',NULL,'2016-09-15 23:59:59',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295271','ru','Отправить заявку №12 \"БЕЙДЖИ УЧАСТНИКОВ\"','Задача завершится автоматически, когда будет отправлена заявка №12');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295271','en','Send the Application No.12 PARTICIPANT BADGES','The task will be completed automatically after sending the Application No.12');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295277',NULL,'23755881984295277',NULL,'2016-08-20 23:59:59',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295277','ru','Отправить заявку №12a \"VIP БЕЙДЖИ\"','Задача завершится автоматически, когда будет отправлена заявка №12a');
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295277','en','Send the Application No.12a VIP BADGES','The task will be completed automatically after sending the Application No.12a');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295278',NULL,'23755881984295278',NULL,'2016-08-22 23:59:59',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295278','ru','Отправить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Задача завершится автоматически, когда будет отправлена заявка №7');
 						INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295278','en','Send the Application No.7 SECURITY, CLEANING, STAFF','The task will be completed automatically after sending the Application No.7');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295279','23755881984295279','23755881984295278',10,NULL,'#f6a800'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295279','ru','Оплатить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Нажмите \"Завершить\", когда будет произведена оплата');
 					INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295279','en','Pay for the service odered (Application No.7)','Click FINISH if you have paid');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295280',NULL,'23755881984295278',24,'2016-08-23 00:00:00',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295280','ru','Отправить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Задача завершится автоматически, когда будет отправлена заявка №7');
 					INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295280','en','Send the Application No.7 SECURITY, CLEANING, STAFF','The task will be completed automatically after sending the Application No.7');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295281','23755881984295281','23755881984295280',5,NULL,'#f6a800'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295281','ru','Оплатить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\"','Нажмите \"Завершить\", когда будет произведена оплата');
 						INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295281','en','Pay for the service odered (Application No.7)','Click FINISH if you have paid');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295282',NULL,'23755881984295278',18,'2016-09-16 00:00:00',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295282','ru','Отправить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №7');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295282','en','Send the Application No.7 SECURITY, CLEANING, STAFF (increase of price 100%)','The task will be completed automatically after sending the Application No.7');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295283','23755881984295283','23755881984295282',3,NULL,'#f96a0e'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295283','ru','Оплатить заявку №7 \"ОХРАНА, УБОРКА, ПЕРСОНАЛ\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295283','en','Pay for the service odered (Application No.7) (increase of price 100%)','Click FINISH if you have paid');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295284',NULL,'23755881984295284',NULL,'2016-08-04 23:59:59',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295284','ru','Отправить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ. ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Задача завершится автоматически, когда будет отправлена заявка №8');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295284','en','Send the Application No.8 INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION','The task will be completed automatically after sending the Application No.12a');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295285','23755881984295285','23755881984295284',10,NULL,'#f6a800'	,1,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295285','ru','Оплатить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ. ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Нажмите \"Завершить\", когда будет произведена оплата');
			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295285','en','Pay for the service odered (Application No.8)','Click FINISH if you have paid');
			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295286',NULL,'23755881984295284',22,'2016-08-05 00:00:00',NULL		,0,0);
 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295286','ru','Отправить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ. ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Задача завершится автоматически, когда будет отправлена заявка №8.<br/><br/>Если в срок до 26.08.16 заявка не будет получена/принята Организатором, Организатор оставляетс за собой право не публиковать информацию в каталог.'); 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`) 			VALUES('23755881984295286','en','Send the Application No.8 INFORMATION SUPPORT. OFFICIAL CATALOGUE OF THE EXHIBITION','The task will be completed automatically after sending the Application No.8.<br/><br/>Should this Application be not received/accepted by the Organizer by August 26, 2016, the Organizer shall reserv the right not to publish any information on the particioant.');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295287','23755881984295287','23755881984295286',5,NULL,'#f6a800'	,1,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295287','ru','Оплатить заявку №8 \"ИНФОРМАЦИОННОЕ СОПРОВОЖДЕНИЕ. ОФИЦИАЛЬНЫЙ КАТАЛОГ ВЫСТАВКИ\"','Нажмите \"Завершить\", когда будет произведена оплата');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295287','en','Pay for the service odered (Application No.8)','Click FINISH if you have paid');


			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295290',NULL,'23755881984295290',NULL,'2016-09-17 23:59:59',NULL,0,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295290','ru','Отправить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\"','Задача завершится автоматически, когда будет отправлена заявка №9а.<br/><br/>До 17.09.16 пропуска в зону проведения разгрузочно-погрузочных работ заказываются у Организатора.');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295290','en','Send the Application No.9a','The task will be completed automatically after sending the Application No.9а.<br/><br/>Note! Before Sept. 17, 2016 you can order the pass only through Organizers service.');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295291','23755881984295291','23755881984295290',10,NULL,'#f6a800'	,1,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295291','ru','Оплатить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\"','Нажмите \"Завершить\", когда будет произведена оплата');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295291','en','Pay for the service odered (Application No.9а)','Click FINISH if you have paid');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295292',NULL,'23755881984295290',29,'2016-09-18 00:00:00',NULL,0,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295292','ru','Отправить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\" (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №9а.<br/><br/>После 17.09 2016 пропуска в зону разгрузочно-погрузочных работ могут быть приобретены как у Организатора, так и через сервис-центр МВЦ \"Крокус Экспо\".');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295292','en','Send the Application No.9а (increase of price 100%)','The task will be completed automatically after sending the Application No.9а<br/><br/>Note! You can order the pass through Organizer\'s or Crocus Expo service after Sept. 17, 2016.');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295293','23755881984295293','23755881984295292',5,NULL,'#f6a800',1,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295293','ru','Оплатить заявку №9а \"ПРОПУСКА НА АВТОТРАНСПОРТ\" (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата');

			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
			VALUES('23755881984295293','en','Pay for the service odered (Application No.9а) (increase of price 100%)','Click FINISH if you have paid');

			INSERT INTO {{calendarfairtasks}} (`fairId`, `uuid`, `parent`, `group`, `duration`, `date`, `color`, `completeButton`, `first`)
			VALUES(184,'23755881984295296',NULL,'23755881984295296',NULL,'2016-09-02 23:59:59',NULL,0,0);

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295296','ru','Отправить заявку №13 \"ОРГАНИЗАЦИЯ ГРУППОВОЙ ПОЕЗДКИ\"','Задача завершится автоматически, когда будет отправлена заявка №13');

 			INSERT INTO {{trcalendarfairtasks}} (`trParentId`, `langId`, `name`, `desc`)
 			VALUES('23755881984295296','en','Send the Application No.13','The task will be completed automatically after sending the Application No.13');

 			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295253', `group`='23755881984295254' WHERE `id`='127';
			UPDATE {{calendarfairtasks}} SET `parent`=NULL WHERE `id`='128';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295255', `group`='23755881984295256' WHERE `id`='129';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295257', `group`='23755881984295258' WHERE `id`='131';


			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295259', `group`='23755881984295260' WHERE `id`='133';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295263', `group`='23755881984295264' WHERE `id`='135';


			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295278', `group`='23755881984295279' WHERE `id`='140';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295280', `group`='23755881984295281' WHERE `id`='142';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295282', `group`='23755881984295283' WHERE `id`='144';

			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295284', `group`='23755881984295285' WHERE `id`='146';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295286', `group`='23755881984295287' WHERE `id`='148';

			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295290', `group`='23755881984295291' WHERE `id`='150';
			UPDATE {{calendarfairtasks}} SET `parent`='23755881984295292', `group`='23755881984295293' WHERE `id`='152';
		";
	}
}