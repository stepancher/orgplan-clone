<?php

class m160810_113539_create_report_date_column extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql  = $this->getAlterTable();
        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getAlterTable(){
        return <<<EOL
            ALTER TABLE {{report}} 
                ADD COLUMN `dateCreated` TIMESTAMP NULL AFTER `columnDefs`;
EOL;
    }
}