<?php

class m161114_174440_alter_invite_tables extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{invite}}
	        CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;

	        ALTER TABLE {{invitedata}}
	        CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;

	        ALTER TABLE {{invitedata}}
	        CHANGE COLUMN `timeOfArrival` `timeOfArrival` TIMESTAMP NULL DEFAULT NULL AFTER `inviteId`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}