<?php

class m160816_072212_update_sup_in_trattributeclass extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql  = $this->getAlterTable();
        $transaction = Yii::app()->dbProposal->beginTransaction();
        try
        {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getAlterTable(){

        return "
            UPDATE {{trattributeclass}} SET `unitTitle`='м2' WHERE `id`='93';
            UPDATE {{trattributeclass}} SET `unitTitle`='м2' WHERE `id`='100';
            UPDATE {{trattributeclass}} SET `unitTitle`='м2' WHERE `id`='238';
            UPDATE {{trattributeclass}} SET `unitTitle`='м2' WHERE `id`='259';
            UPDATE {{trattributeclass}} SET `unitTitle`='м3' WHERE `id`='513';
            UPDATE {{trattributeclass}} SET `unitTitle`='м3' WHERE `id`='515';
            UPDATE {{trattributeclass}} SET `unitTitle`='m2' WHERE `id`='1058';
            UPDATE {{trattributeclass}} SET `unitTitle`='м3' WHERE `id`='1989';
            UPDATE {{trattributeclass}} SET `unitTitle`='м3' WHERE `id`='1991';
            
            UPDATE {{attributeclassproperty}} SET `defaultValue`='м2' WHERE `id`='960';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='м2' WHERE `id`='1747';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='м3' WHERE `id`='888';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='м3' WHERE `id`='898';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='м3' WHERE `id`='906';
            UPDATE {{attributeclassproperty}} SET `defaultValue`='м3' WHERE `id`='909';
            
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='579';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='589';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='597';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='598';
            UPDATE {{trattributeclassproperty}} SET `value`='м2' WHERE `id`='642';
            
            UPDATE {{trattributeclass}} SET `label`='На открытом складе (min 5 м3)' WHERE `id`='512';
            UPDATE {{trattributeclass}} SET `label`='На закрытом слкаде (min 5 м3)' WHERE `id`='514';
            UPDATE {{trattributeclass}} SET `label`='На открытом складе (min 5 м3)' WHERE `id`='1988';
            UPDATE {{trattributeclass}} SET `label`='На закрытом слкаде (min 5 м3)' WHERE `id`='1990';
            
            UPDATE {{trattributeclass}} SET `label`='Плазменная панель 42\" (на 4 дня)' WHERE `id`='1426';
            UPDATE {{trattributeclass}} SET `label`='Плазменная панель 42\" (на 1 день)' WHERE `id`='1429';
            UPDATE {{trattributeclass}} SET `label`='Плазменная панель 50\" (на 4 дня)' WHERE `id`='1432';
            UPDATE {{trattributeclass}} SET `label`='Плазменная панель 50\" (на 1 день)' WHERE `id`='1435';
            UPDATE {{trattributeclass}} SET `label`='Плазменная панель 60\" (на 1 день)' WHERE `id`='1438';
            UPDATE {{trattributeclass}} SET `label`='Плазменная панель 60\" (на 4 дня)' WHERE `id`='1441';
            
            UPDATE {{attributeclassproperty}} SET `defaultValue`='Доставка груза (м3)' WHERE `id`='1407';
            UPDATE {{trattributeclassproperty}} SET `value`='Доставка груза (м3)' WHERE `id`='1084';
            UPDATE {{trattributeclassproperty}} SET `value`='Доставка груза(м3)' WHERE `id`='2101';
            UPDATE {{trattributeclassproperty}} SET `value`='м2' WHERE `id`='2342';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2642';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2643';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2644';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2645';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2660';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2661';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2662';
            UPDATE {{trattributeclassproperty}} SET `value`='м3' WHERE `id`='2663';
            UPDATE {{trattributeclassproperty}} SET `value`='m2' WHERE `id`='2341';
        ";
    }
}