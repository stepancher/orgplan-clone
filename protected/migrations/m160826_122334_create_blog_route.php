<?php

class m160826_122334_create_blog_route extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{route}} SET `route`='blog/default/index' WHERE `id`='93591';
            UPDATE {{route}} SET `route`='blog/default/index' WHERE `id`='93626';
            UPDATE {{route}} SET `route`='blog/default/index' WHERE `id`='93648';
            
            ALTER TABLE {{blog}} 
              ADD COLUMN `shortUrl` VARCHAR(1024) NULL AFTER `seoDescription`;
              
            UPDATE {{blog}} SET `theme`='Раздаточный материал к выставке: что посеешь, то и пожнешь0' WHERE `id`='45';
            UPDATE {{blog}} SET `theme`='test0' WHERE `id`='14';
            UPDATE {{blog}} SET `shortUrl`='kak-rabotat-na-vystavke-1' WHERE `id`='23';
            UPDATE {{blog}} SET `shortUrl`='kak-uchastvovat-v-vystavkakh-1' WHERE `id`='30';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}