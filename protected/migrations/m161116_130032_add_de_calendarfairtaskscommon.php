<?php

class m161116_130032_add_de_calendarfairtaskscommon extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{trcalendarfairtaskscommon}} (`id`, `trParentId`, `langId`, `name`, `desc`) VALUES
                (1863, 1, 'de', 'Messebeteiligungskonzept unter Berücksichtigung Ihres Teilnahmeziels entwickeln', 'Schauen Sie sich unsere Vorlagen an'),
                (1864, 2, 'de', 'Standfläche-Mietvertrag mit dem Veranstalter abschließen', NULL),
                (1865, 3, 'de', 'Antrag auf die Schaltung der Außen-, Medien- und Innenwerbung stellen', NULL),
                (1866, 4, 'de', 'Antrag auf die Veröffentlichung von Informationen über Ihr Unternehmen auf der Messe-Webseite wie auch im offiziellen Katalog stellen', NULL),
                (1867, 5, 'de', 'Stellen Sie einen Antrag und reservieren Sie einen Veranstaltungsraum samt Ausstattung für Ihre Veranstaltung im Rahmen der Messe', NULL),
                (1868, 6, 'de', 'Wählen Sie einen Dienstleister und schließen Sie einen Speditionsvertrag ab, falls eine Zollabfertigung erforderlich ist', NULL),
                (1869, 7, 'de', 'Wählen Sie einen Dienstleister und schließen Sie einen Vertrag für Zustellung Ihrer Exponate zum Messestand', NULL),
                (1870, 8, 'de', 'Einlasskarten/Namensschilder für Ihre Mitarbeiter und Einlasskarten zum Messegelände für Transportmittel bestellen', NULL),
                (1871, 9, 'de', 'Akkreditierung im Projektteam der Messe erhalten', NULL),
                (1872, 10, 'de', 'Liste des Standpersonals festlegen, Hotel, Flug- oder Zugtickets buchen', NULL),
                (1873, 11, 'de', 'Wählen Sie einen Dienstleister und schließen Sie einen Vertrag mit einer Catering-Firma ab, um Ihre Besucher wie auch Mitarbeiter am Stand mit Essen zu versorgen', NULL),
                (1874, 12, 'de', 'Einkaufsliste für Lebensmittel am Stand erstellen', 'Checkliste für Lebensmittel am Stand'),
                (1875, 13, 'de', 'Lebensmittel für Stand kaufen', NULL),
                (1876, 14, 'de', 'Checkliste mit wichtigsten Artikeln für Arbeit am Stand zusammenstellen', 'Heftklammer, Preislisten etc.'),
                (1877, 15, 'de', 'Wählen Sie einen Dienstleister und schließen Sie einen Vertrag über die Dienstleistungen des Mietpersonals ab (Dolmetscher, Hostessen, Promotionspersonal, Künstler, Moderatoren…)', 'Wenn Ihre Firma ein Fremdunternehmen mit dem Aufbau des Standes wie auch Montagearbeiten beauftragt oder sich selbstständig mit der Standausstattung beschäftigt, ist eine Genehmigung vom Veranstalter für Montagearbeiten wie auch eine Akkreditierung erforderlich.'),
                (1878, 16, 'de', 'Standkonzept aufgrund des Teilnahmeziels erarbeiten wie auch technische Aufgabe für den Stand festlegen', NULL),
                (1879, 17, 'de', 'Wählen Sie einen Dienstleister und bestellen Sie ein Standprojekt oder stellen Sie einen Antrag auf den Standbau durch den Veranstalter', NULL),
                (1880, 18, 'de', 'Wählen Sie einen Dienstleister und bestellen Sie erforderliche Anschlüsse durch den Veranstalter: Elektrizität, Wasser, Internet und vieles mehr.', NULL),
                (1881, 19, 'de', 'Multimedia-Ausstattung für den Stand bestellen', NULL),
                (1882, 20, 'de', 'Entwickeln Sie einen Plan für die Promotion Ihrer Teilnahme und wählen Sie Kommunikationskanäle zwischen Ihnen und Besuchern vor und nach der Messe', NULL),
                (1883, 21, 'de', 'Anleitung für Standpersonal erstellen', NULL),
                (1884, 22, 'de', 'Weisungen an Standpersonal erteilen', NULL),
                (1885, 23, 'de', 'Anmeldeformular für Standbesucher erstellen', NULL),
                (1886, 24, 'de', 'Newsletter erstellen und elektronische/gedruckte Einladungskarte gestalten', NULL),
                (1887, 25, 'de', 'Starten Sie eine Werbekampagne für Ihre Messeteilnahme: News in den Sozialen Medien, Newsletter, Mitteilungen in den Medien', NULL),
                (1888, 26, 'de', 'POS-Materialien/Souvenirs/Präsentationsmaterialien bestellen', NULL),
                (1889, 27, 'de', 'Messekleidungfür Standpersonal und Mitarbeiter kaufen/herstellen lassen', NULL),
                (1890, 28, 'de', 'Liste der Standbesucher nach der Messe erstellen', NULL),
                (1891, 29, 'de', 'Dankschreiben senden', NULL);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}