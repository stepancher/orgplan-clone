<?php

class m161208_095717_copy_vc_descriptionSnippet_uniqueText_to_tr extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_exhibitionComplex_descriptionSnippet_uniqueText`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE vc_unique_text TEXT DEFAULT '';
                DECLARE vc_description_snippet TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT descriptionSnippet, uniqueText, id  from {{exhibitioncomplex}} ec;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO vc_description_snippet, vc_unique_text, vc_id;
                    
                    IF done THEN LEAVE read_loop; END IF;
                    
                    START TRANSACTION;
                        UPDATE {{trexhibitioncomplex}} SET `descriptionSnippet` = vc_description_snippet, `uniqueText` = vc_unique_text 
                            WHERE `trParentId` = vc_id;
                    COMMIT;
                    
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL copy_exhibitionComplex_descriptionSnippet_uniqueText();
            DROP PROCEDURE IF EXISTS `copy_exhibitionComplex_descriptionSnippet_uniqueText`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}