<?php

class m160930_143816_gradoteka_hints extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{gstatgroupconstituents}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `gstatgroupId` INT(11) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Плуги');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Бороны');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Рыхлители');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Культиваторы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Полольники');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Мотыги');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('1', 'Сеялки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Косилки для газонов');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Косилки для парков');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Косилки для спортплощадок');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Прессы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Пресс-подборщики');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Машины для уборки клубней и корнеплодов');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Комбайны зерноуборочные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Комбайны виноградоуборочные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('2', 'Машины для очистки, сортировки, калибровки яиц');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('3', 'Установки доильные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('3', 'Оборудование для обработки и переработки молока');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('4', 'Прессы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('4', 'Дробилки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('5', 'Машины для очистки, сортировки, калибровки семян, зерна или сухих бобовых культур');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('6', 'Древесина топливная в виде бревен, поленьев, ветвей, вязанок, хвороста');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('6', 'Древесина топливная в виде цепок или стружки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('6', 'Опилки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('7', 'Лесоматериалы необработанные, с удаленной или неудаленной корой');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('7', 'Лесоматериалы обработанные краской, травителями, преозотом или другими консервантами');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('10', 'Отходды и лом литейного чугуна');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('10', 'Отходды и лом легированной стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('10', 'Отходды и лом коррозионностойкой стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('10', 'Отходды и лом черных металлов');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('10', 'Токарная тружка, обрезки, обломки, отходы фрезерного производства, опилки, отходы штамповки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('11', 'Железо и нелегированная сталь в слитках');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('11', 'Полуфабрикаты из железа или нелегированной стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('11', 'Прокат плоский из железа или нелегированной стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('11', 'Прутки горячекатаные в свободно смотанных бухтах из железа или нелегированной стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('11', 'Уголки, фасонные и специальные профили из железа или нелегированной стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('11', 'Проволока из железа или нелегированной стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('12', 'Сталь коррозионностойкая в слитках или прочих первичных формах');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('12', 'Прокат плоский из коррозионностойкой стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('12', 'Прутки горячекатаные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('12', 'Уголки, фасонные и специальные профили из коррозионностойкой стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('12', 'Проволока из коррозионностойкой стали');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('13', 'Станки токарные металлорежущие');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('13', 'Станки металлорежущие для сверления, растачивания, фрезерования, нарезания наружной или внутренней резьбы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('14', 'Сталь легированная в слитках или других первичных формах прочая');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('14', 'Прокат плоский из прочих легированных сталей');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('14', 'Прутки, уголки из прочих легированных сталей');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('14', 'Проволока из прочих легированных сталей');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Нефть сырая');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Газовый конденсат');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Легкие дистилляты');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Бензины моторные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Керосин');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Тяжелые дистилляты');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Газойли');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Дизельное топливо');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Мазуты');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('16', 'Масла смазочные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('17', 'Пропан');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('17', 'Бутаны');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('17', 'Этилен, пропилен, бутилен и бутадиен');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('18', 'Портландцемент');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('18', 'Цемент глиноземистый');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('18', 'Цемент шлаковый');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('18', 'Цемент суперсульфатный');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('19', 'Изделия из гипса');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('20', 'Колбасы и аналогичные продукты из мяса, мясных субпродуктов или крови; готовые пищевые продукты, изготовленные на их основе');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('21', 'Готовые или консервированные продукты из мяса, мясных субпродуктов или крови прочие');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('22', 'Готовая или консервированная рыба; икра осетровых и ее заменители, изготовленные из икринок рыбы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('23', 'Сахар тростниковый или свекловичный и химически чистая сахароза, в твердом состоянии');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('24', 'Какао-бобы, целые или дробленые, сырые или жареные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('25', 'Шоколад и прочие готовые пищевые продукты, содержащие какао');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Макароны');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Спагетти');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Лапша');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Рожки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Клецки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Равиоли');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Каннеллони');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('26', 'Кускус');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Хлеб');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Мучные кондитерские изделия');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Пирожные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Печенье');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Вафельные пластины');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Пустые капсулы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Вафельные облатки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('27', 'Рисовая бумага');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('28', 'Овощи, фрукты, орехи и другие съедобные части растений, приготовленные или консервированные с добавлением уксуса или уксусной кислоты');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('29', 'Соки фруктовые (включая виноградное сусло) и соки овощные, несброженные и не содержащие добавок спирта, с добавлением или без добавления сахара или других подслащивающих веществ');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('30', 'Мороженое и прочие виды пищевого льда, не содержащие или содержащие какао');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('31', 'Воды, включая природные или искусственные минеральные, газированные, без добавления сахара или других подслащивающих или вкусо-ароматических веществ; лед и снег');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('32', 'Воды, включая минеральные и газированные, содержащие добавки сахара или других подслащивающих или вкусо-ароматических веществ');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('33', 'Пиво солодовое');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('34', 'Вина виноградные натуральные, включая крепленые');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('34', 'Cусло виноградное');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('35', 'Табачное сырье');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('35', 'Табачные отходы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('36', 'Сигары');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('36', 'Сигариллы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('36', 'Сигареты из табака или его заменителей');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('37', 'Свинина');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('38', 'Баранина');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('38', 'Козлятина');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('39', 'Мясо и пищевые субпродукты домашней птицы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('40', 'Кондитерские изделия из сахара (включая белый шоколад), не содержащие какао');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('44', 'Листы для облицовки');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('45', 'Пиломатериалы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('46', 'Плиты древесно-стружечные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('46', 'Плиты с ориентированной стружкой');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('46', 'Вафельные плиты');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('47', 'Плиты древесно-волокнистые из древесины');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('48', 'Фанера клееная');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('48', 'Панели фанерованные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('49', 'Мебель металлическая типа используемой в учреждениях столы письменные');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('49', 'Шкафы, снабженные дверями, задвижками или откидными досками');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('49', 'Шкафы для хранения документов, картотечные и прочие шкафы');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('49', 'Кровати');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('49', 'Мебель кухонной секционная');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('49', 'Мебель спальная');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('50', 'Хирургическая');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('50', 'Стоматологическая');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('50', 'Ветеринарная');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('50', 'Столы для осмотра');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('50', 'Больничные койки с механическими приспособлениями');
            INSERT INTO {{gstatgroupconstituents}} (`gstatgroupId`, `name`) VALUES ('50', 'Парикмахерские кресла');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}