<?php

class m160622_104101_add_NDS_to_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n4_summary_hint', 'string', '0', '0', '2201', '10', 'hintWithView', '0');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('2', '2201', '0', '0', '19');
		UPDATE {{attributeclass}} SET `class`='hintWithView' WHERE `id`='515';
		UPDATE {{trattributeclass}} SET `label`='Все цены включают НДС 18%. Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента.<br><br>\nСтоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.' WHERE `id`='427';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением\nк Договору на участие в выставке и\nдолжна быть заполнена в двух экземплярах.\n<br/>\n<br/>\nПодписывая настоящую заявку Экспонент\nподтверждает согласие с Правилами выставки\nи гарантирует оплату заказанных услуг.\n<br/>\n<br/>\nСтоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.\n<br/>\n<br/>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 7 октября 2016 г.\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги,\nуказанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='429';
		UPDATE {{trattributeclass}} SET `label`='Все цены включают НДС 18%. Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента.<br><br>' WHERE `id`='427';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies. <br><br>\nBy signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered. <br><br>\nCost of services increases by 50% if ordered after August 25, 2016, by 100% if ordered after Sept. 23, 2016.<br><br>\nAny claims related to the perfomance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be perfomed with the proper quality and accepted by the Exhibitor.' WHERE `id`='1081';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br/><br/>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br/><br/>\nСтоимость услуг увеличивается на 50% при заказе после 25.08.16г. и на 100% при заказе после 23.09.16г.<br/><br/>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 4 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='1422';
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n3_hint_nds', 'string', '0', '518', '518', '1.5', 'hintWithView', '0');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2202', 'ru', 'Все цены включают НДС 18%. Оплата настоящей Заявки производится в рублях РФ по курсу ЦБ РФ, действующему на день списания денежных средств с расчетного счета Экспонента.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2202', 'en', 'Payments shall be effected in USD. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.');
		UPDATE {{attributeclass}} SET `priority`='0.9' WHERE `id`='2202';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>\nBy signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>\nCost of services increases by 50% if ordered after August 25, 2016, by 100% if ordered after Sept. 23, 2016.<br/><br/>\nAny claims related to the performance of this Application are accepted by the Organizer until October 04, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='1423';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br><br>\nСтоимость услуг увеличивается на 50% при заказе после 23.08.16г. и на 100% при заказе после 21.09.16г.\n<br><br>\n\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг. Все претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00, 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='172';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br><br>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br><br> \nСтоимость услуг увеличивается на 50% при заказе после 23.08.16г. и на 100% при заказе после 21.09.16г.<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00, 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='172';
		UPDATE {{attributeclass}} SET `priority`='20' WHERE `id`='217';
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('2', '217', '0', '0', '18');
		UPDATE {{attributeclass}} SET `parent`='0', `super`='217', `priority`='10' WHERE `id`='217';
		UPDATE {{attributemodel}} SET `priority`='19.5' WHERE `id`='188';
		UPDATE {{attributeclass}} SET `priority`='0' WHERE `id`='217';
		UPDATE {{attributemodel}} SET `priority`='6' WHERE `id`='187';
		UPDATE {{attributemodel}} SET `priority`='7' WHERE `id`='188';
		DELETE FROM {{attributemodel}} WHERE `id`='188';
		UPDATE {{attributeclass}} SET `parent`='238', `super`='238', `priority`='10' WHERE `id`='217';
		UPDATE {{attributeclass}} SET `parent`='241', `super`='241' WHERE `id`='217';
		UPDATE {{attributeclass}} SET `priority`='10' WHERE `id`='241';
		UPDATE {{attributeclass}} SET `priority`='20' WHERE `id`='242';
		UPDATE {{attributeclass}} SET `priority`='30' WHERE `id`='243';
		UPDATE {{attributeclass}} SET `priority`='25' WHERE `id`='217';
		UPDATE {{attributeclass}} SET `class`='hintWithView' WHERE `id`='242';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>\nBy signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>\nCost of services increases by 50% if ordered after August 23, 2016, by 100% if ordered after Sept. 21, 2016.<br/><br/>\nAny claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='1171';
		UPDATE {{attributeclass}} SET `class`='hintWithView' WHERE `id`='572';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. <br> \n<br> \nСтоимость услуг увеличивается на 100% при заказе после 16.09.16г.<br> <br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 04 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ' WHERE `id`='463';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br><br>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br><br>\nCost of services increases by 100% if ordered after Sept.16, 2016<br><br>\nAny claims related to the performance of this Application are accepted by the Organizer until October 04, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='1220';
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n4_hint', 'string', '0', '0', '75', '75', '1', 'hintWithView', '0');
		UPDATE {{attributeclass}} SET `parent`='0', `super`='2203', `priority`='0' WHERE `id`='2203';
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('2', '2203', '0', '0', '1');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2203', 'ru', 'Отправьте заполненную форму в дирекцию выставки не позднее 23 августа 2016г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
		UPDATE {{attributeclass}} SET `class`='hintDanger' WHERE `id`='2203';
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2203', 'en', 'This form should be filled in until August 23, 2016. Please return the application form by fax +7(495)781-37-05, or e-mail: agrosalon@agrosalon.ru');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2006', 'en', 'Заявка является приложение к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br><br>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br><br>\nСтоимость услуг увеличивается на 50% при заказе после 29.07.16г. и на 100% при заказе после 16.09.16г.<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 18:00 дня проведения работ.<br><br>\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке, считаются выполненными надлежащего качества и принятыми Экспонентом.\n');
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложение к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br><br>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br><br>\nСтоимость услуг увеличивается на 50% при заказе после 29.07.16г. и на 100% при заказе после 16.09.16г.<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 18:00 дня проведения работ.<br><br>\nВ случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке, считаются выполненными надлежащего качества и принятыми Экспонентом.\n' WHERE `id`='1095';
		UPDATE {{attributeclass}} SET `class`='hintWithView' WHERE `id`='2007';
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2007', 'en', 'Все цены включают НДС 18%.<br>Оплата настоящей Заявки производится в российских рублях.');
		DELETE FROM {{attributeclass}} WHERE `id`='904';
		DELETE FROM {{attributeclass}} WHERE `id`='905';
		DELETE FROM {{attributeclass}} WHERE `id`='906';
		DELETE FROM {{attributeclass}} WHERE `id`='907';
		DELETE FROM {{attributeclass}} WHERE `id`='908';
		DELETE FROM {{attributeclass}} WHERE `id`='909';
		DELETE FROM {{attributeclass}} WHERE `id`='911';
		DELETE FROM {{attributeclass}} WHERE `id`='912';
		DELETE FROM {{attributeclass}} WHERE `id`='913';
		DELETE FROM {{attributeclass}} WHERE `id`='914';
		DELETE FROM {{attributeclass}} WHERE `id`='915';
		DELETE FROM {{attributeclass}} WHERE `id`='916';
		
		DELETE FROM {{attributeclass}} WHERE `id`='891';
		DELETE FROM {{attributeclass}} WHERE `id`='892';
		DELETE FROM {{attributeclass}} WHERE `id`='893';
		DELETE FROM {{attributeclass}} WHERE `id`='894';
		DELETE FROM {{attributeclass}} WHERE `id`='895';
		DELETE FROM {{attributeclass}} WHERE `id`='896';
		DELETE FROM {{attributeclass}} WHERE `id`='897';
		DELETE FROM {{attributeclass}} WHERE `id`='898';
		DELETE FROM {{attributeclass}} WHERE `id`='899';
		DELETE FROM {{attributeclass}} WHERE `id`='900';
		DELETE FROM {{attributeclass}} WHERE `id`='901';
		DELETE FROM {{attributeclass}} WHERE `id`='902';
		DELETE FROM {{attributeclass}} WHERE `id`='903';
		DELETE FROM {{attributeclass}} WHERE `id`='910';
		UPDATE {{attributemodel}} SET `priority`='21.5' WHERE `id`='96';
		UPDATE {{attributemodel}} SET `priority`='22.5' WHERE `id`='96';
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '925', 'organizerPost', 'envVariable');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '927', 'organizerName', 'envVariable');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('920', 'en', 'Экспонент');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('921', 'en', 'должность');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('923', 'en', 'ФИО');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('924', 'en', 'Организатор');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('925', 'en', 'должность');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('927', 'en', 'ФИО');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('928', 'en', 'подпись');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('929', 'en', 'подпись');
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_hint_danger2', 'string', '0', '0', '661', '0', 'hintDangerNotView', '0');
		UPDATE {{attributeclass}} SET `parent`='661' WHERE `id`='2204';
		UPDATE {{attributeclass}} SET `parent`='0', `super`='2204', `priority`='0' WHERE `id`='2204';
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('10', '2204', '0', '0', '-1.5');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('2204', 'ru');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`) VALUES ('2204', 'en');
		UPDATE {{trattributeclass}} SET `label`='При ввозе экспонатов в страну, оформление грузовой таможенной декларации (далее ГТД) является обязательным, свяжитесь с официальным экспедитором (<a href=\"/static/documents/fairs/184/ freight_forwarder . pdf\">Официальный экспедитор</a>, <a href=\" /static/documents/fairs/184/quotation_request.pdf\">Запрос квоты</a>).  \nПри  наличии ГТД заполните форму №9 (ниже).' WHERE `id`='2306';
		UPDATE {{trattributeclass}} SET `label`='При ввозе экспонатов в страну, оформление грузовой таможенной декларации (далее ГТД) является обязательным, свяжитесь с официальным экспедитором (<a href=\" /static/documents / fairs / 184 / freight_forwarder . pdf\">Официальный экспедитор</a>, <a href=\" /static/documents/fairs/184/quotation_request.pdf\">Запрос квоты</a>).  \nПри  наличии ГТД заполните форму №9 (ниже).' WHERE `id`='2307';
		UPDATE {{trattributeclass}} SET `label`='If you have Customs declaration, please fill the Application form No.9 (below). Otherwise please get in contact with the nominated official sole On-Site-Exhibition Freight Forwarder for all questions regarding, Customs-Clearance- On-Site-Handling- Transport-Service & to and from the fairgrounds (<a href=\" /static/documents/fairs/184/freight_forwarder.pdf\">Freight-Forwarder</a>, <a href=\" /static/documents/fairs/184/quotation_request.pdf\">Quotation Request</a>).' WHERE `id`='2307';
";
	}
}