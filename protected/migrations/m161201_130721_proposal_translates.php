<?php

class m161201_130721_proposal_translates extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1133', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1134', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1137', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1138', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1139', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1140', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1146', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1147', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1148', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1149', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1150', 'en', 'date');
             INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1151', 'en', 'date');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}