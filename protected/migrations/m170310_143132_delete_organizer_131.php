<?php

class m170310_143132_delete_organizer_131 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{organizercompany}} WHERE `id` = '131';

            DELETE FROM {{organizer}} WHERE `id` = '391';
            DELETE FROM {{organizer}} WHERE `id` = '427';
            
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20608';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20609';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20610';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20611';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20612';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20613';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20614';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20615';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20616';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20617';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20618';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20619';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20620';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20621';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20622';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20900';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20901';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20902';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20903';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20904';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20905';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20906';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20907';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20908';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20909';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20910';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20911';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20912';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20913';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20914';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20915';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20916';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20917';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20918';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20919';
            
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2950';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2915';
            
            DELETE FROM {{fair}} WHERE `id` = '4419';
            DELETE FROM {{fair}} WHERE `id` = '4359';
            
            DELETE FROM {{fairhasindustry}} WHERE `id` = '13893';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '13889';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}