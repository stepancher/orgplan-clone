<?php

class m151105_080547_table_gobject extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{gobjects}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable()
	{
		return '

			CREATE TABLE IF NOT EXISTS {{gobjects}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `gType` varchar(45) DEFAULT NULL,
			  `parentType` varchar(45) DEFAULT NULL,
			  `parentId` int(11) DEFAULT NULL,
			  `name` varchar(100) DEFAULT NULL,
			  `gId` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `index2` (`gId`,`name`)
			) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8;

			INSERT INTO {{gobjects}} (`id`, `gType`, `parentType`, `parentId`, `name`, `gId`) VALUES
				(175, "region", "state", 1, "Белгородская область", 1),
				(176, "region", "state", 1, "Брянская область", 2),
				(177, "region", "state", 1, "Владимирская область", 3),
				(178, "region", "state", 1, "Воронежская область", 4),
				(179, "region", "state", 1, "Ивановская область", 5),
				(180, "region", "state", 1, "Калужская область", 6),
				(181, "region", "state", 1, "Костромская область", 7),
				(182, "region", "state", 1, "Курская область", 8),
				(183, "region", "state", 1, "Липецкая область", 9),
				(184, "region", "state", 1, "Город Москва", 10),
				(185, "region", "state", 1, "Московская область", 11),
				(186, "region", "state", 1, "Орловская область", 12),
				(187, "region", "state", 1, "Рязанская область", 13),
				(188, "region", "state", 1, "Смоленская область", 14),
				(189, "region", "state", 1, "Тамбовская область", 15),
				(190, "region", "state", 1, "Тверская область", 16),
				(191, "region", "state", 1, "Тульская область", 17),
				(192, "region", "state", 1, "Ярославская область", 18),
				(193, "region", "state", 3, "Республика Карелия", 19),
				(194, "region", "state", 3, "Республика Коми", 20),
				(195, "region", "state", 3, "Архангельская область", 21),
				(196, "region", "state", 3, "Вологодская область", 22),
				(197, "region", "state", 3, "Калининградская область", 23),
				(198, "region", "state", 3, "Ленинградская область", 24),
				(199, "region", "state", 3, "Город Санкт-Петербург", 25),
				(200, "region", "state", 3, "Мурманская область", 26),
				(201, "region", "state", 3, "Новгородская область", 27),
				(202, "region", "state", 3, "Псковская область", 28),
				(203, "region", "state", 3, "Ненецкий автономный округ", 29),
				(204, "region", "state", 2, "Республика Адыгея", 30),
				(205, "region", "state", 2, "Республика Калмыкия", 31),
				(206, "region", "state", 2, "Краснодарский край", 32),
				(207, "region", "state", 2, "Астраханская область", 33),
				(208, "region", "state", 2, "Волгоградская область", 34),
				(209, "region", "state", 2, "Ростовская область", 35),
				(210, "region", "state", 8, "Республика Дагестан", 36),
				(211, "region", "state", 8, "Республика Ингушетия", 37),
				(212, "region", "state", 8, "Кабардино-Балкарская Республика", 38),
				(213, "region", "state", 8, "Карачаево-Черкесская Республика", 39),
				(214, "region", "state", 8, "Республика Северная Осетия-Алания", 40),
				(215, "region", "state", 8, "Чеченская Республика", 41),
				(216, "region", "state", 8, "Ставропольский край", 42),
				(217, "region", "state", 7, "Республика Башкортостан", 43),
				(218, "region", "state", 7, "Республика Марий Эл", 44),
				(219, "region", "state", 7, "Республика Мордовия", 45),
				(220, "region", "state", 7, "Республика Татарстан", 46),
				(221, "region", "state", 7, "Удмуртская Республика", 47),
				(222, "region", "state", 7, "Чувашская Республика", 48),
				(223, "region", "state", 7, "Пермский край", 49),
				(224, "region", "state", 7, "Кировская область", 50),
				(225, "region", "state", 7, "Нижегородская область", 51),
				(226, "region", "state", 7, "Оренбургская область", 52),
				(227, "region", "state", 7, "Пензенская область", 53),
				(228, "region", "state", 7, "Самарская область", 54),
				(229, "region", "state", 7, "Саратовская область", 55),
				(230, "region", "state", 7, "Ульяновская область", 56),
				(231, "region", "state", 6, "Курганская область", 57),
				(232, "region", "state", 6, "Свердловская область", 58),
				(233, "region", "state", 6, "Тюменская область", 59),
				(234, "region", "state", 6, "Челябинская область", 60),
				(235, "region", "state", 6, "Ханты-Мансийский автономный округ — Югра", 61),
				(236, "region", "state", 6, "Ямало-Ненецкий автономный округ", 62),
				(237, "region", "state", 5, "Республика Алтай", 63),
				(238, "region", "state", 5, "Республика Бурятия", 64),
				(239, "region", "state", 5, "Республика Тыва", 65),
				(240, "region", "state", 5, "Республика Хакасия", 66),
				(241, "region", "state", 5, "Алтайский край", 67),
				(242, "region", "state", 5, "Забайкальский край", 68),
				(243, "region", "state", 5, "Красноярский край", 69),
				(244, "region", "state", 5, "Иркутская область", 70),
				(245, "region", "state", 5, "Кемеровская область", 71),
				(246, "region", "state", 5, "Новосибирская область", 72),
				(247, "region", "state", 5, "Омская область", 73),
				(248, "region", "state", 5, "Томская область", 74),
				(249, "region", "state", 4, "Республика Саха (Якутия)", 75),
				(250, "region", "state", 4, "Приморский край", 76),
				(251, "region", "state", 4, "Камчатский край", 77),
				(252, "region", "state", 4, "Хабаровский край", 78),
				(253, "region", "state", 4, "Амурская область", 79),
				(254, "region", "state", 4, "Магаданская область", 80),
				(255, "region", "state", 4, "Сахалинская область", 81),
				(256, "region", "state", 4, "Еврейская автономная область", 82),
				(257, "region", "state", 4, "Чукотский автономный округ", 83),
				(258, "region", "state", 3, "Архангельская область (включая Ненецкий АО)", 84),
				(259, "region", "state", 6, "Тюменская область (включая Ханты-Мансийский АО и Ямало-Ненецкий АО)", 85),
				(260, "region", "state", 9, "Республика Крым", 86),
				(261, "region", "state", 9, "Город Севастополь", 87);
			';
	}
}