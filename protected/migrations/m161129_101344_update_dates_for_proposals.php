<?php

class m161129_101344_update_dates_for_proposals extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{proposalelementclass}} SET `deadline`='2017-12-04 06:00:00' WHERE `id`='5';
            UPDATE {{proposalelementclass}} SET `deadline`='2017-12-04 06:00:00' WHERE `id`='21';
            UPDATE {{proposalelementclass}} SET `deadline`='2017-12-04 06:00:00' WHERE `id`='22';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}

/*
Russia
Ukraine
Belarus
Kazakhstan
Germany
Abkhazia
Australia
Austria
Azerbaijan
Albania
Algeria
Angola
Anguilla
Andorra
Antigua and Barbuda
Antilles
Argentina
Armenia
Arulco
Afghanistan
The Bahamas
Bangladesh
Barbados
Bahrain
Belize
Belgium
Benin
Bermuda
Bulgaria
Bolivia
Bosnia and Herzegovina
Botswana
Brazil
British Virgin Islands
Brunei
Burkina Faso
Burundi
Bhutan
Wallis and Futuna
Vanuatu
The United Kingdom (UK)
Hungary
Venezuela
East Timor
Vietnam
Gabon
Haiti
Guyana
The Gambia
Ghana
Guadeloupe
Guatemala
Guinea
Guinea-Bissau
Guernsey
Gibraltar
Honduras
Hong Kong
Grenada
Greenland
Greece
Georgia
Denmark
Jersey
Djibouti
The Dominican Republic
Europa Island
Egypt
Zambia
Western Sahara
Zimbabwe
Israel
India
Indonesia
Jordan
Iraq
Iran
Republic of Ireland
Iceland
Spain
Italy
Yemen
Cape Verde
Cambodia
Cameroon
Canada
Qatar
Kenya
Cyprus
Kiribati
China
Colombia
Comoros
Congo (Brazzaville)
Congo (Kinshasa)
Costa Rica
Ivory Coast
Cuba
Kuwait
Cook Islands
Kyrgyzstan
Laos
Latvia
Lesotho
Liberia
Lebanon
Libya
Lithuania
Liechtenstein
Luxembourg
Mauritius
Mauritania
Madagascar
Macedonia
Malawi
Malaysia
Mali
Maldives
Malta
Martinique
Mexico
Mozambique
Moldova
Monaco
Mongolia
Morocco
Myanmar (Burma)
Isle of Man
Namibia
Nauru
Nepal
Niger
Nigeria
Netherlands (Holland)
Nicaragua
New Zealand
New Caledonia
Norway
Norfolk Island
United Arab Emirates
Oman
Pakistan
Panama
Papua New Guinea
Paraguay
Peru
Pitcairn Islands
Poland
Portugal
Puerto Rico
Réunion
Rwanda
Romania
The United States
El Salvador
Samoa
San Marino
São Tomé and Príncipe
Saudi Arabia
Swaziland
Saint Lucia
Saint Helena
North Korea
Seychelles
Saint Pierre and Miquelon
Senegal
Saint Kitts and Nevis
Saint Vincent and the Grenadines
Serbia
Singapore
Syria
Slovakia
Slovenia
Solomon Islands
Somalia
Sudan
Suriname
Sierra Leone
Tajikistan
Taiwan
Thailand
Tanzania
Togo
Tokelau
Tonga
Trinidad and Tobago
Tuvalu
Tunisia
Turkmenistan
Turks and Caicos Islands
Turkey
Uganda
Uzbekistan
Uruguay
Faroe Islands
Fiji
Philippines
Finland
France
Guinea
French polynesia
Croatia
Chad
Montenegro
Czech Republic
Chile
Switzerland
Sweden
Sri Lanka
Ecuador
Equatorial Guinea
Eritrea
Estonia
Ethiopia
South Africa
South Korea
South Ossetia
Jamaica
Japan
Macau
*/