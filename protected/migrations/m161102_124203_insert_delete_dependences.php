<?php

class m161102_124203_insert_delete_dependences extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":4, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4990/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":4, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4991/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":4, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4992/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":4, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4993/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":4, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4994/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":4, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4995/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":24, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4996/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":24, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/4997/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":5, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5001/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":5, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5002/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":5, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5003/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":5, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5004/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":5, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5005/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":5, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5006/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":22, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5007/userId/{:userId}');
            INSERT INTO {{dependences}} (`action`, `params`, `run`) VALUES ('proposal/proposal/delete', '{\"ecId\":21, \"last\":1}', 'calendar/gantt/deleteUserTaskById/id/5008/userId/{:userId}');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}