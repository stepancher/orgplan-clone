<?php

class m160422_135041_merge_9_proposals extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributemodel}} SET `priority`='1' WHERE `id`='87';
			UPDATE {{attributemodel}} SET `priority`='2' WHERE `id`='96';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='4' WHERE `id`='77';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='5' WHERE `id`='78';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='6' WHERE `id`='79';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='7' WHERE `id`='80';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='8' WHERE `id`='81';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='9' WHERE `id`='83';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='10' WHERE `id`='84';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='11' WHERE `id`='85';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='12' WHERE `id`='95';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='13' WHERE `id`='68';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='14' WHERE `id`='69';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='15' WHERE `id`='70';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='16' WHERE `id`='71';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='17' WHERE `id`='72';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='18' WHERE `id`='73';
			UPDATE {{attributemodel}} SET `elementClass`='10', `priority`='19' WHERE `id`='74';
			UPDATE {{attributemodel}} SET `elementClass`='10' WHERE `id`='75';
			UPDATE {{attributemodel}} SET `elementClass`='10' WHERE `id`='76';
			UPDATE {{attributemodel}} SET `elementClass`='10' WHERE `id`='94';
			DELETE FROM {{attributeclass}} WHERE `id`='615';
			DELETE FROM {{attributeclass}} WHERE `id`='575';
			DELETE FROM {{attributeclass}} WHERE `id`='660';
			DELETE FROM {{attributeclass}} WHERE `id`='766';
			DELETE FROM {{attributeclass}} WHERE `id`='608';
			DELETE FROM {{attributeclass}} WHERE `id`='657';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='630';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='632';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='634';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='636';
			UPDATE {{attributeclass}} SET `priority`='1' WHERE `id`='638';
			UPDATE {{attributeclass}} SET `priority`='2' WHERE `id`='639';
			UPDATE {{attributeclass}} SET `parent`='594', `super`='594' WHERE `id`='607';
			DELETE FROM {{attributemodel}} WHERE `id`='73';
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9_bold1', 'string', '0', '0', 'Экспонаты', '661', '661', '1', 'headerH2Bold', '0');
			UPDATE {{attributeclass}} SET `class`='headerH1Bold' WHERE `id`='990';
			UPDATE {{attributeclass}} SET `label`='ЭКСПОНАТЫ' WHERE `id`='990';
	    ";
	}
}