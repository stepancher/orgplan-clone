<?php

class m160304_153024_create_tbl_organizerexponent extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{productcategoryhasnotification}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE IF NOT EXISTS {{organizerexponent}} (
			`id` INT NOT NULL AUTO_INCREMENT,
			`organizerId` INT NULL DEFAULT NULL,
			`email` VARCHAR(45) NULL DEFAULT NULL,
			`name` VARCHAR(45) NULL DEFAULT NULL,
			`bill` VARCHAR(45) NULL DEFAULT NULL,
			`number` VARCHAR(45) NULL DEFAULT NULL,
			`stand` VARCHAR(45) NULL DEFAULT NULL,
			`square` VARCHAR(45) NULL DEFAULT NULL,
			  PRIMARY KEY (`id`));
		';
	}
}

