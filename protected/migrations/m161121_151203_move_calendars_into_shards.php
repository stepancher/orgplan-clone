<?php

class m161121_151203_move_calendars_into_shards extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);
        list($peace1, $peace2, $dbProposal) = explode('=', Yii::app()->dbProposal->connectionString);
        list($peace1, $peace2, $dbProposalAgrorus) = explode('=', Yii::app()->dbProposalAgrorus->connectionString);
        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);

        return "
            CREATE TABLE {$dbProposal}.tbl_dependences LIKE {$db}.tbl_dependences;
            INSERT INTO {$dbProposal}.tbl_dependences SELECT * FROM {$db}.tbl_dependences;

            CREATE TABLE {$dbProposal}.tbl_calendarfairdays LIKE {$db}.tbl_calendarfairdays;
            INSERT INTO {$dbProposal}.tbl_calendarfairdays SELECT * FROM {$db}.tbl_calendarfairdays;

            CREATE TABLE {$dbProposal}.tbl_calendarfairtasks LIKE {$db}.tbl_calendarfairtasks;
            INSERT INTO {$dbProposal}.tbl_calendarfairtasks SELECT * FROM {$db}.tbl_calendarfairtasks;

            CREATE TABLE {$dbProposal}.tbl_calendarfairtemptasks LIKE {$db}.tbl_calendarfairtemptasks;
            INSERT INTO {$dbProposal}.tbl_calendarfairtemptasks SELECT * FROM {$db}.tbl_calendarfairtemptasks;

            CREATE TABLE {$dbProposal}.tbl_calendaruserstasks LIKE {$db}.tbl_calendaruserstasks;
            INSERT INTO {$dbProposal}.tbl_calendaruserstasks SELECT * FROM {$db}.tbl_calendaruserstasks;

            CREATE TABLE {$dbProposal}.tbl_calendarfairuserstasks LIKE {$db}.tbl_calendarfairuserstasks;
            INSERT INTO {$dbProposal}.tbl_calendarfairuserstasks SELECT * FROM {$db}.tbl_calendarfairuserstasks;

            CREATE TABLE {$dbProposal}.tbl_trcalendarfairtasks LIKE {$db}.tbl_trcalendarfairtasks;
            INSERT INTO {$dbProposal}.tbl_trcalendarfairtasks SELECT * FROM {$db}.tbl_trcalendarfairtasks;


            CREATE TABLE {$dbProposalAgrorus}.tbl_dependences LIKE {$db}.tbl_dependences;
            INSERT INTO {$dbProposalAgrorus}.tbl_dependences SELECT * FROM {$db}.tbl_dependences;

            CREATE TABLE {$dbProposalAgrorus}.tbl_calendarfairdays LIKE {$db}.tbl_calendarfairdays;
            INSERT INTO {$dbProposalAgrorus}.tbl_calendarfairdays SELECT * FROM {$db}.tbl_calendarfairdays;

            CREATE TABLE {$dbProposalAgrorus}.tbl_calendarfairtasks LIKE {$db}.tbl_calendarfairtasks;
            INSERT INTO {$dbProposalAgrorus}.tbl_calendarfairtasks SELECT * FROM {$db}.tbl_calendarfairtasks;

            CREATE TABLE {$dbProposalAgrorus}.tbl_calendarfairtemptasks LIKE {$db}.tbl_calendarfairtemptasks;
            INSERT INTO {$dbProposalAgrorus}.tbl_calendarfairtemptasks SELECT * FROM {$db}.tbl_calendarfairtemptasks;

            CREATE TABLE {$dbProposalAgrorus}.tbl_calendaruserstasks LIKE {$db}.tbl_calendaruserstasks;
            INSERT INTO {$dbProposalAgrorus}.tbl_calendaruserstasks SELECT * FROM {$db}.tbl_calendaruserstasks;

            CREATE TABLE {$dbProposalAgrorus}.tbl_calendarfairuserstasks LIKE {$db}.tbl_calendarfairuserstasks;
            INSERT INTO {$dbProposalAgrorus}.tbl_calendarfairuserstasks SELECT * FROM {$db}.tbl_calendarfairuserstasks;

            CREATE TABLE {$dbProposalAgrorus}.tbl_trcalendarfairtasks LIKE {$db}.tbl_trcalendarfairtasks;
            INSERT INTO {$dbProposalAgrorus}.tbl_trcalendarfairtasks SELECT * FROM {$db}.tbl_trcalendarfairtasks;


            CREATE TABLE {$dbProposalF10943}.tbl_dependences LIKE {$db}.tbl_dependences;
            INSERT INTO {$dbProposalF10943}.tbl_dependences SELECT * FROM {$db}.tbl_dependences;

            CREATE TABLE {$dbProposalF10943}.tbl_calendarfairdays LIKE {$db}.tbl_calendarfairdays;
            INSERT INTO {$dbProposalF10943}.tbl_calendarfairdays SELECT * FROM {$db}.tbl_calendarfairdays;

            CREATE TABLE {$dbProposalF10943}.tbl_calendarfairtasks LIKE {$db}.tbl_calendarfairtasks;
            INSERT INTO {$dbProposalF10943}.tbl_calendarfairtasks SELECT * FROM {$db}.tbl_calendarfairtasks;

            CREATE TABLE {$dbProposalF10943}.tbl_calendarfairtemptasks LIKE {$db}.tbl_calendarfairtemptasks;
            INSERT INTO {$dbProposalF10943}.tbl_calendarfairtemptasks SELECT * FROM {$db}.tbl_calendarfairtemptasks;

            CREATE TABLE {$dbProposalF10943}.tbl_calendaruserstasks LIKE {$db}.tbl_calendaruserstasks;
            INSERT INTO {$dbProposalF10943}.tbl_calendaruserstasks SELECT * FROM {$db}.tbl_calendaruserstasks;

            CREATE TABLE {$dbProposalF10943}.tbl_calendarfairuserstasks LIKE {$db}.tbl_calendarfairuserstasks;
            INSERT INTO {$dbProposalF10943}.tbl_calendarfairuserstasks SELECT * FROM {$db}.tbl_calendarfairuserstasks;

            CREATE TABLE {$dbProposalF10943}.tbl_trcalendarfairtasks LIKE {$db}.tbl_trcalendarfairtasks;
            INSERT INTO {$dbProposalF10943}.tbl_trcalendarfairtasks SELECT * FROM {$db}.tbl_trcalendarfairtasks;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}