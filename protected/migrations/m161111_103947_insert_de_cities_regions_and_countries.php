<?php

class m161111_103947_insert_de_cities_regions_and_countries extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {

        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);
        list($peace1, $peace2, $dbProposal) = explode('=', Yii::app()->dbProposal->connectionString);

        return "
            INSERT INTO $dbProposalF10943.tbl_trpcity (trParentId, langId, name) 
            SELECT trParentId, 'de', name FROM $dbProposalF10943.tbl_trpcity where langId = 'en';
            
            INSERT INTO $dbProposalF10943.tbl_trpcountry (trParentId, langId, name) 
            SELECT trParentId, 'de', name FROM $dbProposalF10943.tbl_trpcountry where langId = 'en';
            
            INSERT INTO $dbProposalF10943.tbl_trpregion (trParentId, langId, name) 
            SELECT trParentId, 'de', name FROM $dbProposalF10943.tbl_trpregion where langId = 'en';
            
            INSERT INTO $dbProposal.tbl_trpcity (trParentId, langId, name) 
            SELECT trParentId, 'de', name FROM $dbProposal.tbl_trpcity where langId = 'en';
            
            INSERT INTO $dbProposal.tbl_trpcountry (trParentId, langId, name) 
            SELECT trParentId, 'de', name FROM $dbProposal.tbl_trpcountry where langId = 'en';
            
            INSERT INTO $dbProposal.tbl_trpregion (trParentId, langId, name) 
            SELECT trParentId, 'de', name FROM $dbProposal.tbl_trpregion where langId = 'en';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}