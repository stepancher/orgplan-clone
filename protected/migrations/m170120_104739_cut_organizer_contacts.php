<?php

class m170120_104739_cut_organizer_contacts extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `cut_organizer_contacts`;
            
            CREATE PROCEDURE `cut_organizer_contacts`()
            BEGIN
                
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE contact_id INT DEFAULT 0;
                DECLARE contact_organizer_id INT DEFAULT 0;
                DECLARE contact_type INT DEFAULT 0;
                DECLARE contact_value TEXT DEFAULT '';
                
                DECLARE value_string TEXT DEFAULT '';
                DECLARE str_path TEXT DEFAULT '';
                DECLARE additional_phone TEXT DEFAULT '';
                DECLARE delimiter_pos INT DEFAULT 0;
                
                DECLARE copy CURSOR FOR SELECT id, organizerId, type, value FROM {{organizercontactlist}};
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO contact_id, contact_organizer_id, contact_type, contact_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    SET value_string := contact_value;
                    SET delimiter_pos := LOCATE(',', value_string);
                    
                    IF delimiter_pos != 0 THEN
                    
                        while_loop: WHILE delimiter_pos != 0 DO
                        
                            SET str_path := REPLACE(LEFT(value_string, delimiter_pos - 1), ' ', '');
                            
                            IF LOCATE('доб.', str_path) != 0 THEN
                                
                                SET additional_phone := CONCAT(@val, ', ', str_path);
                                
                                START TRANSACTION;
                                    UPDATE {{organizercontactlist}} SET `value` = additional_phone WHERE `id` = @id;
                                COMMIT;
                                
                                SET value_string := SUBSTR(value_string, delimiter_pos + 1);
                                SET delimiter_pos := LOCATE(',', value_string);
                                
                                ITERATE while_loop;
                            END IF;
                            
                            START TRANSACTION;
                                INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES (contact_organizer_id, str_path, contact_type);
                            COMMIT;
                            
                            SET @id := LAST_INSERT_ID();
                            SET @val := str_path;
                            
                            SET value_string := SUBSTR(value_string, delimiter_pos + 1);
                            SET delimiter_pos := LOCATE(',', value_string);
                            
                            IF delimiter_pos = 0 THEN
                            
                                IF LOCATE('доб.', value_string) != 0 THEN
                                
                                    SET str_path := REPLACE(value_string, ' ', '');
                                
                                    SET additional_phone := CONCAT(@val, ', ', str_path);
                                    
                                    START TRANSACTION;
                                        UPDATE {{organizercontactlist}} SET `value` = additional_phone WHERE `id` = @id;
                                    COMMIT;
                                ELSE 
                                
                                    SET str_path := REPLACE(value_string, ' ', '');
                                    
                                    START TRANSACTION;
                                        INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES (contact_organizer_id, str_path, contact_type);
                                    COMMIT;
                                END IF;
                            
                            END IF;
                            
                        END WHILE;
                    
                    ELSE 
                        
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES (contact_organizer_id, value_string, contact_type);
                        COMMIT;
                        
                    END IF;
                    
                    START TRANSACTION;
                        DELETE FROM {{organizercontactlist}} WHERE `id` = contact_id;
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            CALL cut_organizer_contacts();
            DROP PROCEDURE IF EXISTS `cut_organizer_contacts`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}