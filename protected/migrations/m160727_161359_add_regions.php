<?php

class m160727_161359_add_regions extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('26', 'Baku-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('27', 'Yerevan-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('28', 'Minsk-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('29', 'Mogilev-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('30', 'Grodno-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('31', 'Astana-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('32', 'Almaty-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('33', 'Atyrau-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('34', 'Mangystau-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('35', 'Aktobe-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('36', 'Pavlodar-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('37', 'Bishkek-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('38', 'Kishinev-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('39', 'Karaganda-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('40', 'Tashkent-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('41', 'Fergana-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('42', 'Ashgabat-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('43', 'Balkan-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('44', 'Tbilisi-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('45', 'Ulaanbaatar-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('46', 'Kiev-region');
			INSERT INTO {{region}} (`districtId`, `shortUrl`) VALUES ('47', 'Odessa-oblast');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}