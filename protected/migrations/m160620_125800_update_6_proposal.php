<?php

class m160620_125800_update_6_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. <br/>\nСтоимость услуг увеличивается на 100% при заказе услуг после 18.08.16г.<br> \n<br> \nСтоимость услуг увеличивается на 50% при заказе услуг после 19.08.16г. и на 100% при заказе после 23.09.16г.<br> \n<br> \nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ' WHERE `id`='2152';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>\nBy signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>\n<div class=\"hint - header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016<br/><br/>\nAny claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='2198';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>\nBy signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>\nCost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016<br/><br/>\nAny claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='2198';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах. <br>\n<br> \nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату услуг. <br/>\nСтоимость услуг увеличивается на 100% при заказе услуг после 18.08.16г.<br> \n<br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом. ' WHERE `id`='2152';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>\nBy signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>\nCost of services increases by 100% if ordered after Aug.08, 2016<br/><br/>\nAny claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 12 а.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='2198';
		UPDATE {{trattributeclass}} SET `label`='Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.<br/><br/>\nПодписывая настоящую заявку Экспонент подтверждает согласие с Правилами выставки и гарантирует оплату заказанных услуг.<br/><br/>\nСтоимость услуг увеличивается на 50% при заказе услуг после 19.08.16г. и на 100% при заказе после 23.09.16г.<br/><br/>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 14:00 06 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.' WHERE `id`='1489';
		UPDATE {{trattributeclass}} SET `label`='This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>\nCost of services increases by 50% if ordered after Aug.19, 2016, by 100% if ordered after Sep.23, 2016<br/><br/>\nAny claims related to the performance of this Application are accepted by the Organizer until October 04, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application are considered to be performed with the proper quality and accepted by the Exhibitor.' WHERE `id`='1558';
		UPDATE {{attributeclass}} SET `class`='hintWithView' WHERE `id`='2173';
		UPDATE {{attributeclass}} SET `dataType`='int' WHERE `id`='2100';
";
	}
}