<?php

class m160526_081207_create_trorganizerinfo extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			CREATE TABLE {{trorganizerinfo}} (
			`id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `name` VARCHAR(255) NULL,
			  `fullName` VARCHAR(255) NULL,
			  `postPostcode` VARCHAR(255) NULL,
			  `postCountry` VARCHAR(255) NULL,
			  `postRegion` VARCHAR(255) NULL,
			  `postCity` VARCHAR(255) NULL,
			  `postStreet` VARCHAR(255) NULL,
			  `postPhone` VARCHAR(255) NULL,
			  `postSite` VARCHAR(255) NULL,
			  `legalPostcode` VARCHAR(255) NULL,
			  `legalCountry` VARCHAR(255) NULL,
			  `legalRegion` VARCHAR(255) NULL,
			  `legalCity` VARCHAR(255) NULL,
			  `legalStreet` VARCHAR(255) NULL,
			  `requisitesSettlementAccountRUR` VARCHAR(255) NULL,
			  `requisitesBank` VARCHAR(255) NULL,
			  `requisitesBic` VARCHAR(255) NULL,
			  `requisitesItn` VARCHAR(255) NULL,
			  `requisitesIec` VARCHAR(255) NULL,
			  `requisitesCorrespondentAccount` VARCHAR(255) NULL,
			  `signRulesName` VARCHAR(255) NULL,
			  `signRulesPosition` VARCHAR(255) NULL,
			  `requisitesSettlementAccountEUR` VARCHAR(255) NULL,
			  `requisitesSettlementAccountUSD` VARCHAR(255) NULL,
			  PRIMARY KEY (`id`)) ENGINE = INNODB DEFAULT CHARSET = utf8;
			
			INSERT INTO {{trorganizerinfo}} (`trParentId`, `langId`, `name`, `fullName`, `postPostcode`, `postCountry`, `postRegion`, `postCity`, `postStreet`, `postPhone`, `postSite`, `legalPostcode`, `legalCountry`, `legalRegion`, `legalCity`, `legalStreet`, `requisitesSettlementAccountRUR`, `requisitesBank`, `requisitesBic`, `requisitesItn`, `requisitesIec`, `requisitesCorrespondentAccount`, `signRulesName`, `signRulesPosition`) VALUES ('1', 'ru', 'РОСАГРОМАШ АССОЦИАЦИЯ', 'ООО ', '121609', 'Россия', 'г. Москва', 'г. Москва, Осенний бульвар, д. 23', 'Осенний бульвар', '', 'http://www.agrosalon.ru/', '121609', 'Россия', 'г. Москва', 'г. Москва, Осенний бульвар, д. 23', 'Осенний бульвар', '40702 810 5 3826 01 10487', 'Московский банк Сбербанка России ОАО г. Москва', '044525225', '7731535639', '773101001', '30101810400000000225', 'А.В. Елизарова', 'Заместитель Генерального директора по Выставочной деятельности');
			INSERT INTO {{trorganizerinfo}} (`trParentId`, `langId`, `name`, `fullName`, `postPostcode`, `postCountry`, `postRegion`, `postCity`, `postStreet`, `postPhone`, `postSite`, `legalPostcode`, `legalCountry`, `legalRegion`, `legalCity`, `legalStreet`, `requisitesSettlementAccountRUR`, `requisitesBank`, `requisitesBic`, `requisitesItn`, `requisitesIec`, `requisitesCorrespondentAccount`, `signRulesName`, `signRulesPosition`, `requisitesSettlementAccountEUR`, `requisitesSettlementAccountUSD`) VALUES ('2', 'ru', 'DLG e.V', 'Deutsche Landwirtschafts-Gesellschaft', '60489', 'Deutschland', '', 'Frankfurt', 'Eschborner Landstr. 122', '+49(0)69/24 788-(0)', 'www.dlg.org', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
			INSERT INTO {{trorganizerinfo}} (`id`, `trParentId`, `langId`, `name`, `fullName`, `postPostcode`, `postCountry`, `postRegion`, `postCity`, `postStreet`, `postPhone`, `postSite`, `legalPostcode`, `legalCountry`, `legalRegion`, `legalCity`, `legalStreet`, `requisitesSettlementAccountRUR`, `requisitesBank`, `requisitesBic`, `requisitesItn`, `requisitesIec`, `requisitesCorrespondentAccount`, `signRulesName`, `signRulesPosition`) VALUES (NULL, '1', 'en', 'РОСАГРОМАШ АССОЦИАЦИЯ', 'ООО ', '121609', 'Россия', 'г. Москва', 'г. Москва, Осенний бульвар, д. 23', 'Осенний бульвар', '', 'http://www.agrosalon.ru/', '121609', 'Россия', 'г. Москва', 'г. Москва, Осенний бульвар, д. 23', 'Осенний бульвар', '40702 810 5 3826 01 10487', 'Московский банк Сбербанка России ОАО г. Москва', '044525225', '7731535639', '773101001', '30101810400000000225', 'А.В. Елизарова', 'Заместитель Генерального директора по Выставочной деятельности');
			INSERT INTO {{trorganizerinfo}} (`id`, `trParentId`, `langId`, `name`, `fullName`, `postPostcode`, `postCountry`, `postRegion`, `postCity`, `postStreet`, `postPhone`, `postSite`, `legalPostcode`, `legalCountry`, `legalRegion`, `legalCity`, `legalStreet`, `requisitesSettlementAccountRUR`, `requisitesBank`, `requisitesBic`, `requisitesItn`, `requisitesIec`, `requisitesCorrespondentAccount`, `signRulesName`, `signRulesPosition`, `requisitesSettlementAccountEUR`, `requisitesSettlementAccountUSD`) VALUES (NULL, '2', 'en', 'DLG e.V', 'Deutsche Landwirtschafts-Gesellschaft', '60489', 'Deutschland', '', 'Frankfurt', 'Eschborner Landstr. 122', '+49(0)69/24 788-(0)', 'www.dlg.org', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
			UPDATE {{trorganizerinfo}} SET `signRulesPosition`='Deputy of General Director for exhibitions' WHERE `id`='3';
			UPDATE {{trorganizerinfo}} SET `signRulesName`='Alla Elizarova' WHERE `id`='3';
		";
	}
}