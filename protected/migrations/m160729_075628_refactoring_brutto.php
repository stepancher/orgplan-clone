<?php

class m160729_075628_refactoring_brutto extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			ALTER TABLE {{fair}} 
			ADD COLUMN `squareGross` INT(11) NULL DEFAULT NULL AFTER `endDemountingDate`;

			UPDATE {{fair}} SET squareGross = squere WHERE squereIsBrutto = 1;
			
			UPDATE {{fair}} SET squere = NULL WHERE squereIsBrutto = 1;
			
			ALTER TABLE {{fair}}
			CHANGE COLUMN `squere` `squareNet` INT(11) NULL DEFAULT NULL;

			ALTER TABLE {{fair}}
			DROP COLUMN `squereIsBrutto`;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}