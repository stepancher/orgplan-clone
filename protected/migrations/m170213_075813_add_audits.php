<?php

class m170213_075813_add_audits extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{audit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `active` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            CREATE TABLE {{traudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `langId` VARCHAR(55) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}