<?php

class m161212_085059_insert_translates extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        $sql= '';

        /**
         * @var Fair $fair
         */
        foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){
                $dbTargetName = Yii::app()->dbMan->createShardName($fair->id);

                $sql = $sql."
                    UPDATE {$dbTargetName}.`tbl_trproposalelementclass` SET `label`='Application form No.9', `pluralLabel`='OFFICIAL FREIGHT-FORWARDER' WHERE `trParentId`='10' AND `langId` = 'en';
                    INSERT INTO {$dbTargetName}.`tbl_trproposalelementclass` (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('25', 'en', 'PASSES FOR TRANSPORT', 'PASSES FOR TRANSPORT');
                    INSERT INTO {$dbTargetName}.`tbl_trproposalelementclass` (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('27', 'en', 'TECHNICAL AND ENGINEERING CONNECTION. Compressed air connection', 'TECHNICAL AND ENGINEERING CONNECTION. Compressed air connection');
                    INSERT INTO {$dbTargetName}.`tbl_trproposalelementclass` (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('10', 'de', 'Antrag Nr. 9', 'OFFIZIELLER SPEDITEUR');
                    INSERT INTO {$dbTargetName}.`tbl_trproposalelementclass` (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('25', 'de', 'Antrag Nr. 9a', 'PASSEN FÜR DEN TRANSPORT');
                    INSERT INTO {$dbTargetName}.`tbl_trproposalelementclass` (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('27', 'de', 'Antrag Nr. 4a', 'TECHNISCHE ANSCHLÜSSE. Druckluftanschluss');
                ";
            }
        }

        $sql =  $sql."
            UPDATE {{trcity}} SET `name`='Makarye' WHERE `id`='1662';
            UPDATE {{trcity}} SET `name`='Makarye' WHERE `id`='1528';
            UPDATE {{trcity}} SET `name`='Prutskoi ' WHERE `id`='1557';
            UPDATE {{trcity}} SET `name`='Prutskoi ' WHERE `id`='1370';
        ";

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}