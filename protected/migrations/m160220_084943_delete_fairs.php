<?php

class m160220_084943_delete_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE `id` IN (
				10109,
				10108,
				1051,
				1078,
				10029,
				10030,
				10033,
				10034,
				10039,
				10040,
				10106,
				10107,
				10125,
				10126,
				10025,
				10026,
				10027,
				10028,
				10035,
				10036,
				10037,
				10038,
				10121,
				10122,
				10123,
				10124
			);
		";
	}
}