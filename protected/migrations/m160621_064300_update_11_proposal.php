<?php

class m160621_064300_update_11_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1956', 'en', 'hour');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1957', 'en', 'persons');
		INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '1958', 'en', 'persons');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2087', 'en', 'This form should be filled in until August 18, 2016. Please return the application form by fax +7(495) 781 37 08 or e-mail: agrosalon@agrosalon.ru');
		UPDATE {{trattributeclass}} SET `placeholder`='Enter' WHERE `id`='2211';
		UPDATE {{trattributeclass}} SET `placeholder`='Enter' WHERE `id`='2214';
		UPDATE {{trattributeclass}} SET `placeholder`='Please enter the event type' WHERE `id`='2210';
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES ('parties_signature_n11', 'int', '1', '0', '0', '2188', 'partiesSignature', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('parties_signature_left_block_n11', 'int', '1', '800', '800', '1', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('parties_signature_right_block_n11', 'int', '1', '800', '800', '2', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_left_n11_1', 'string', '1', '801', '800', '1', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_left_n11_2', 'string', '1', '801', '800', '2', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_left_n11_2.5', 'string', '1', '801', '800', '4', 'headerH2', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_left_n11_3', 'string', '1', '801', '800', '3', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_right_n11_1', 'string', '1', '802', '800', '1', 'headerH1', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_right_n11_2', 'string', '1', '802', '800', '2', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_right_n11_2.5', 'string', '1', '802', '800', '4', 'headerH2', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_right_n11_3', 'string', '1', '802', '800', '3', 'envVariable', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_left_n11_4', 'string', '1', '801', '800', '5', 'text', '0');
		INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('parties_signature_row_right_n11_4', 'string', '1', '802', '800', '5', 'text', '0');
		INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('19', '2188', '0', '0', '60');
		UPDATE {{attributeclass}} SET `parent`='2188', `super`='2188' WHERE `id`='2189';
		UPDATE {{attributeclass}} SET `parent`='2188', `super`='2188' WHERE `id`='2190';
		UPDATE {{attributeclass}} SET `parent`='2189', `super`='2188' WHERE `id`='2191';
		UPDATE {{attributeclass}} SET `parent`='2189', `super`='2188' WHERE `id`='2192';
		UPDATE {{attributeclass}} SET `parent`='2189', `super`='2188' WHERE `id`='2193';
		UPDATE {{attributeclass}} SET `parent`='2189', `super`='2188' WHERE `id`='2194';
		UPDATE {{attributeclass}} SET `parent`='2190', `super`='2188' WHERE `id`='2195';
		UPDATE {{attributeclass}} SET `parent`='2190', `super`='2188' WHERE `id`='2196';
		UPDATE {{attributeclass}} SET `parent`='2190', `super`='2188' WHERE `id`='2197';
		UPDATE {{attributeclass}} SET `parent`='2190', `super`='2188' WHERE `id`='2198';
		UPDATE {{attributeclass}} SET `parent`='2189', `super`='2188' WHERE `id`='2199';
		UPDATE {{attributeclass}} SET `parent`='2190', `super`='2188' WHERE `id`='2200';
		
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'Exhibitor');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'Экспонент');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'должность');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'ФИО');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'Organizer');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'Организатор');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'должность');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'ФИО');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'Signature');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'подпись');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'en', 'Signature');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES (NULL, 'ru', 'подпись');
		UPDATE {{trattributeclass}} SET `label`='Organizer' WHERE `id`='2278';
		UPDATE {{trattributeclass}} SET `label`='Организатор' WHERE `id`='2279';
		UPDATE {{trattributeclass}} SET `label`='position' WHERE `id`='2276';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2277';
		UPDATE {{trattributeclass}} SET `label`='position' WHERE `id`='2280';
		UPDATE {{trattributeclass}} SET `label`='Name' WHERE `id`='2272';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2273';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2281';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2275';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2275';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2273';
		UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2273';
		UPDATE {{trattributeclass}} SET `label`='position' WHERE `id`='2272';
		UPDATE {{trattributeclass}} SET `label`='Name' WHERE `id`='2276';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2277';
		UPDATE {{trattributeclass}} SET `label`='Name' WHERE `id`='2282';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2283';
		UPDATE {{trattributeclass}} SET `label`='Signature' WHERE `id`='2282';
		UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2283';
		UPDATE {{trattributeclass}} SET `label`='Name' WHERE `id`='2284';
		UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2285';
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2196', 'organizerPost', 'envVariable');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2198', 'organizerName', 'envVariable');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1979', 'ru', 'organizerPost');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1979', 'en', 'organizerName');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1980', 'ru', 'organizerPost');
		INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1980', 'en', 'organizerName');
		UPDATE {{trattributeclassproperty}} SET `value`='organizerPost' WHERE `id`='2631';
		
		DELETE FROM {{trattributeclass}} WHERE `id`='2254';
		DELETE FROM {{trattributeclass}} WHERE `id`='2255';
		DELETE FROM {{trattributeclass}} WHERE `id`='2256';
		DELETE FROM {{trattributeclass}} WHERE `id`='2257';
		DELETE FROM {{trattributeclass}} WHERE `id`='2258';
		DELETE FROM {{trattributeclass}} WHERE `id`='2259';
		DELETE FROM {{trattributeclass}} WHERE `id`='2260';
		DELETE FROM {{trattributeclass}} WHERE `id`='2261';
		DELETE FROM {{trattributeclass}} WHERE `id`='2262';
		DELETE FROM {{trattributeclass}} WHERE `id`='2263';
		DELETE FROM {{trattributeclass}} WHERE `id`='2264';
		DELETE FROM {{trattributeclass}} WHERE `id`='2265';
		DELETE FROM {{trattributeclass}} WHERE `id`='2266';
		DELETE FROM {{trattributeclass}} WHERE `id`='2267';
		DELETE FROM {{trattributeclass}} WHERE `id`='2268';
		DELETE FROM {{trattributeclass}} WHERE `id`='2269';
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2270', '2191', 'en', 'Exhibitor');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2271', '2191', 'ru', 'Экспонент');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2272', '2192', 'en', 'position');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2273', '2192', 'ru', 'должность');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2274', '2193', 'en', 'Signature');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2275', '2193', 'ru', 'подпись');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2276', '2194', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2277', '2194', 'ru', 'ФИО');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2278', '2195', 'en', 'Organizer');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2279', '2195', 'ru', 'Организатор');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2280', '2196', 'en', 'position');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2281', '2196', 'ru', 'должность');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2282', '2197', 'en', 'Signature');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2283', '2197', 'ru', 'подпись');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2284', '2198', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('2285', '2198', 'ru', 'ФИО');
";
	}
}