<?php

class m170221_121956_copy_exdb_exhibitioncomplex_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_exhibitioncomplex_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_exhibitioncomplex_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE vc_name TEXT DEFAULT '';
                DECLARE vc_city_id INT DEFAULT 0;
                DECLARE vc_coordinate TEXT DEFAULT '';
                DECLARE vc_contact TEXT DEFAULT '';
                DECLARE vc_raw TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT eec.exdbId, eec.name, eec.cityId, eec.coordinates, eec.contacts, eec.raw
                                        FROM {$expodata}.{{exdbexhibitioncomplex}} eec
                                        LEFT JOIN {$db}.{{exhibitioncomplex}} ec ON eec.exdbId = ec.exdbId
                                        WHERE ec.id IS NULL ORDER BY eec.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO vc_id, vc_name, vc_city_id, vc_coordinate, vc_contact, vc_raw;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{exhibitioncomplex}} (`exdbId`,`cityId`,`coordinates`,`exdbContact`,`exdbRaw`) VALUES (vc_id, vc_city_id, vc_coordinate, vc_contact, vc_raw);
                    COMMIT;
                    
                    SET @last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{trexhibitioncomplex}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, vc_name, 'ru');
                        INSERT INTO {$db}.{{trexhibitioncomplex}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, vc_name, 'en');
                        INSERT INTO {$db}.{{trexhibitioncomplex}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, vc_name, 'de');
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_exhibitioncomplex_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}