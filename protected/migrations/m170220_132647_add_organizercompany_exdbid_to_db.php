<?php

class m170220_132647_add_organizercompany_exdbid_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{organizercompany}} 
            ADD COLUMN `exdbId` INT(11) NULL DEFAULT NULL AFTER `coordinates`,
            ADD COLUMN `exdbContacts` TEXT NULL DEFAULT NULL AFTER `exdbId`,
            ADD COLUMN `exdbContactsRaw` TEXT NULL DEFAULT NULL AFTER `exdbContacts`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}