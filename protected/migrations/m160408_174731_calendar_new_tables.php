<?php

class m160408_174731_calendar_new_tables extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			DROP TABLE IF EXISTS {{dependences}};

			CREATE TABLE {{dependences}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `action` varchar(145) NOT NULL,
			  `params` varchar(145) DEFAULT '{}',
			  `run` varchar(145) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `action` (`action`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

			INSERT INTO {{dependences}} VALUES
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(5,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/15'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"save\":1}','calendarm/gantt/add/id/20'),
			(7,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/20'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/22'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/24');

			DROP TABLE IF EXISTS {{calendarfairdays}};

			CREATE TABLE {{calendarfairdays}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `fairId` int(11) NOT NULL,
			  `name` varchar(245) DEFAULT NULL,
			  `startDate` date NOT NULL,
			  `endDate` date NOT NULL,
			  `cssClass` varchar(45) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

			INSERT INTO {{calendarfairdays}} VALUES
			(1,184,'Первый день монтажа (по согласованию с Организатором)','2016-09-30','2016-10-01','gray'),
			(2,184,'Дни монтажа','2016-10-01','2016-10-04','black'),
			(3,184,'Дни работы выставки','2016-10-04','2016-10-08','green'),
			(4,184,'Дни демонтажа','2016-10-07','2016-10-09','black'),
			(5,184,'Последний день подачи заявки N1 со скидкой 20%','2015-12-23','2015-12-24','red'),
			(6,184,'Последний день подачи заявки N1 со скидкой 15%','2016-03-31','2016-04-01','red'),
			(7,184,'Последний день подачи заявки N1 со скидкой 10%','2016-07-29','2016-07-30','red'),
			(8,184,'Последний день подачи заявок N1','2016-08-31','2016-09-01','red');

			DROP TABLE IF EXISTS {{calendarfairtasks}};

			CREATE TABLE {{calendarfairtasks}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `parent` int(11) DEFAULT NULL,
			  `group` int(11) DEFAULT NULL,
			  `fairId` int(11) DEFAULT NULL,
			  `name` varchar(245) NOT NULL,
			  `desc` text,
			  `duration` mediumint(8) DEFAULT NULL,
			  `date` datetime DEFAULT NULL,
			  `color` varchar(45) DEFAULT NULL,
			  `completeButton` tinyint(1) NOT NULL DEFAULT '0',
			  `first` tinyint(1) NOT NULL DEFAULT '0',
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

			INSERT INTO {{calendarfairtasks}} VALUES
			(1,NULL,1,184,'Подать заявку №1 (скидка 20%)','Задача завершится автоматически, когда будет отправлена заявка №1',NULL,'2015-12-23 23:59:59','',0,1),
			(2,1,NULL,184,'Подписать договор (скидка 20%)','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(3,2,NULL,184,'Оплатить участие (аванс 10%)','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,NULL,1,0),
			(4,3,NULL,184,'Оплатить участие (аванс 40%)','Нажмите \"Завершить\", когда будет произведена оплата',15,'2016-04-16 00:00:00',NULL,1,0),
			(5,4,NULL,184,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',17,'2016-07-15 00:00:00',NULL,1,0),
			(6,NULL,1,184,'Подать заявку №1 (скидка 15%)','Задача завершится автоматически, когда будет отправлена заявка №1',99,'2015-12-24 00:00:00',NULL,0,0),
			(7,6,NULL,184,'Подписать договор (скидка 15%)','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(8,7,NULL,184,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,NULL,1,0),
			(9,8,NULL,184,'Оплатить участие (аванс 50%)','Нажмите \"Завершить\", когда будет произведена оплата',17,'2016-07-15 00:00:00',NULL,1,0),
			(10,NULL,1,184,'Подать заявку №1 (скидка 10%)','Задача завершится автоматически, когда будет отправлена заявка №1',120,'2016-04-01 00:00:00',NULL,0,0),
			(11,10,NULL,184,'Подписать договор (скидка 10%)','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(15,NULL,1,184,'Подать заявку №1','Задача завершится автоматически, когда будет отправлена заявка №1',32,'2016-07-30 00:00:00',NULL,0,0),
			(16,15,NULL,184,'Подписать договор ','Нажмите \"Завершить\", когда подпишите и отправите договор',10,NULL,NULL,1,0),
			(20,NULL,2,184,'Отправить заявку №2','Задача завершится автоматически, когда будет отправлена заявка №2',NULL,'2016-08-25 23:59:59',NULL,0,0),
			(21,20,NULL,184,'Оплатить заявку №2','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800',1,0),
			(22,NULL,2,184,'Отправить заявку №2 (с наценкой 50%)','Задача завершится автоматически, когда будет отправлена заявка №2',29,'2016-08-26 00:00:00',NULL,0,0),
			(23,22,NULL,184,'Оплатить заявку №2 (с наценкой 50%)','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800',1,0),
			(24,NULL,2,184,'Отправить заявку №2 (с наценкой 100%)','Задача завершится автоматически, когда будет отправлена заявка №2',10,'2016-09-24 00:00:00',NULL,0,0),
			(25,24,NULL,184,'Оплатить заявку №2 (с наценкой 100%)','Нажмите \"Завершить\", когда будет произведена оплата',10,NULL,'#f6a800',1,0);

			DROP TABLE IF EXISTS {{calendarfairuserstasks}};

			CREATE TABLE {{calendarfairuserstasks}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `taskId` int(11) NOT NULL,
			  `userId` int(11) NOT NULL,
			  `date` datetime DEFAULT NULL,
			  `completeDate` datetime DEFAULT NULL,
			  `color` varchar(45) DEFAULT NULL,
			  `duration` mediumint(8) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

			DROP TABLE IF EXISTS {{calendarexhibition}};
		";
	}

	private function downSql() {

		return "
			DROP TABLE IF EXISTS {{calendarfairdays}};
			DROP TABLE IF EXISTS {{calendarfairtasks}};
			DROP TABLE IF EXISTS {{calendarfairuserstasks}};
		";
	}
}