<?php

class m161003_150242_industry_analytic_tooltips extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{gstatgroupconstituents}} SET `name`='Плуги 8432' WHERE `id`='1';
            UPDATE {{gstatgroupconstituents}} SET `name`='Бороны 8432' WHERE `id`='2';
            UPDATE {{gstatgroupconstituents}} SET `name`='Рыхлители 8432' WHERE `id`='3';
            UPDATE {{gstatgroupconstituents}} SET `name`='Культиваторы 8432' WHERE `id`='4';
            UPDATE {{gstatgroupconstituents}} SET `name`='Полольники 8432' WHERE `id`='5';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мотыги 8432' WHERE `id`='6';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сеялки 8432' WHERE `id`='7';
            UPDATE {{gstatgroupconstituents}} SET `name`='Косилки для газонов 8433' WHERE `id`='8';
            UPDATE {{gstatgroupconstituents}} SET `name`='Косилки для парков 8433' WHERE `id`='9';
            UPDATE {{gstatgroupconstituents}} SET `name`='Косилки для спортплощадок 8433' WHERE `id`='10';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прессы 8433' WHERE `id`='11';
            UPDATE {{gstatgroupconstituents}} SET `name`='Пресс-подборщики 8433' WHERE `id`='12';
            UPDATE {{gstatgroupconstituents}} SET `name`='Машины для уборки клубней и корнеплодов 8433' WHERE `id`='13';
            UPDATE {{gstatgroupconstituents}} SET `name`='Комбайны зерноуборочные 8433' WHERE `id`='14';
            UPDATE {{gstatgroupconstituents}} SET `name`='Комбайны виноградоуборочные 8433' WHERE `id`='15';
            UPDATE {{gstatgroupconstituents}} SET `name`='Машины для очистки, сортировки, калибровки яиц 8433' WHERE `id`='16';
            UPDATE {{gstatgroupconstituents}} SET `name`='Установки доильные 8434' WHERE `id`='17';
            UPDATE {{gstatgroupconstituents}} SET `name`='Оборудование для обработки и переработки молока 8434' WHERE `id`='18';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прессы 8435' WHERE `id`='19';
            UPDATE {{gstatgroupconstituents}} SET `name`='Дробилки 8435' WHERE `id`='20';
            UPDATE {{gstatgroupconstituents}} SET `name`='Машины для очистки, сортировки, калибровки семян, зерна или сухих бобовых культур 8437' WHERE `id`='21';
            UPDATE {{gstatgroupconstituents}} SET `name`='Древесина топливная в виде бревен, поленьев, ветвей, вязанок, хвороста 4401' WHERE `id`='22';
            UPDATE {{gstatgroupconstituents}} SET `name`='Древесина топливная в виде цепок или стружки 4401' WHERE `id`='23';
            UPDATE {{gstatgroupconstituents}} SET `name`='Опилки 4401' WHERE `id`='24';
            UPDATE {{gstatgroupconstituents}} SET `name`='Лесоматериалы необработанные, с удаленной или неудаленной корой 4403' WHERE `id`='25';
            UPDATE {{gstatgroupconstituents}} SET `name`='Лесоматериалы обработанные краской, травителями, преозотом или другими консервантами 4403' WHERE `id`='26';
            UPDATE {{gstatgroupconstituents}} SET `name`='Отходды и лом литейного чугуна 7204' WHERE `id`='27';
            UPDATE {{gstatgroupconstituents}} SET `name`='Отходды и лом легированной стали 7204' WHERE `id`='28';
            UPDATE {{gstatgroupconstituents}} SET `name`='Отходды и лом коррозионностойкой стали 7204' WHERE `id`='29';
            UPDATE {{gstatgroupconstituents}} SET `name`='Отходды и лом черных металлов 7204' WHERE `id`='30';
            UPDATE {{gstatgroupconstituents}} SET `name`='Токарная тружка, обрезки, обломки, отходы фрезерного производства, опилки, отходы штамповки 7204' WHERE `id`='31';
            UPDATE {{gstatgroupconstituents}} SET `name`='Железо и нелегированная сталь в слитках 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217' WHERE `id`='32';
            UPDATE {{gstatgroupconstituents}} SET `name`='Полуфабрикаты из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217' WHERE `id`='33';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прокат плоский из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217' WHERE `id`='34';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прутки горячекатаные в свободно смотанных бухтах из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217' WHERE `id`='35';
            UPDATE {{gstatgroupconstituents}} SET `name`='Уголки, фасонные и специальные профили из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217' WHERE `id`='36';
            UPDATE {{gstatgroupconstituents}} SET `name`='Проволока из железа или нелегированной стали 7206,7207,7208,7209,7210,7211,7212,7213,7214,7215,7216,7217' WHERE `id`='37';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сталь коррозионностойкая в слитках или прочих первичных формах 7218,7219,7220,7221,7222,7223' WHERE `id`='38';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прокат плоский из коррозионностойкой стали 7218,7219,7220,7221,7222,7223' WHERE `id`='39';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прутки горячекатаные 7218,7219,7220,7221,7222,7223' WHERE `id`='40';
            UPDATE {{gstatgroupconstituents}} SET `name`='Уголки, фасонные и специальные профили из коррозионностойкой стали 7218,7219,7220,7221,7222,7223' WHERE `id`='41';
            UPDATE {{gstatgroupconstituents}} SET `name`='Проволока из коррозионностойкой стали 7218,7219,7220,7221,7222,7223' WHERE `id`='42';
            UPDATE {{gstatgroupconstituents}} SET `name`='Станки токарные металлорежущие 7224,7225,7226,7227,7228,7229' WHERE `id`='43';
            UPDATE {{gstatgroupconstituents}} SET `name`='Станки металлорежущие для сверления, растачивания, фрезерования, нарезания наружной или внутренней резьбы 7224,7225,7226,7227,7228,7229' WHERE `id`='44';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сталь легированная в слитках или других первичных формах прочая 8458,8459' WHERE `id`='45';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прокат плоский из прочих легированных сталей 8458,8459' WHERE `id`='46';
            UPDATE {{gstatgroupconstituents}} SET `name`='Прутки, уголки из прочих легированных сталей 8458,8459' WHERE `id`='47';
            UPDATE {{gstatgroupconstituents}} SET `name`='Проволока из прочих легированных сталей 8458,8459' WHERE `id`='48';
            UPDATE {{gstatgroupconstituents}} SET `name`='Нефть сырая 2709,2710' WHERE `id`='49';
            UPDATE {{gstatgroupconstituents}} SET `name`='Газовый конденсат 2709,2710' WHERE `id`='50';
            UPDATE {{gstatgroupconstituents}} SET `name`='Легкие дистилляты 2709,2710' WHERE `id`='51';
            UPDATE {{gstatgroupconstituents}} SET `name`='Бензины моторные 2709,2710' WHERE `id`='52';
            UPDATE {{gstatgroupconstituents}} SET `name`='Керосин 2709,2710' WHERE `id`='53';
            UPDATE {{gstatgroupconstituents}} SET `name`='Тяжелые дистилляты 2709,2710' WHERE `id`='54';
            UPDATE {{gstatgroupconstituents}} SET `name`='Газойли 2709,2710' WHERE `id`='55';
            UPDATE {{gstatgroupconstituents}} SET `name`='Дизельное топливо 2709,2710' WHERE `id`='56';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мазуты 2709,2710' WHERE `id`='57';
            UPDATE {{gstatgroupconstituents}} SET `name`='Масла смазочные 2709,2710' WHERE `id`='58';
            UPDATE {{gstatgroupconstituents}} SET `name`='Пропан 2711' WHERE `id`='59';
            UPDATE {{gstatgroupconstituents}} SET `name`='Бутаны 2711' WHERE `id`='60';
            UPDATE {{gstatgroupconstituents}} SET `name`='Этилен, пропилен, бутилен и бутадиен 2711' WHERE `id`='61';
            UPDATE {{gstatgroupconstituents}} SET `name`='Портландцемент 2523' WHERE `id`='62';
            UPDATE {{gstatgroupconstituents}} SET `name`='Цемент глиноземистый 2523' WHERE `id`='63';
            UPDATE {{gstatgroupconstituents}} SET `name`='Цемент шлаковый 2523' WHERE `id`='64';
            UPDATE {{gstatgroupconstituents}} SET `name`='Цемент суперсульфатный 2523' WHERE `id`='65';
            UPDATE {{gstatgroupconstituents}} SET `name`='Изделия из гипса 6809' WHERE `id`='66';
            UPDATE {{gstatgroupconstituents}} SET `name`='Колбасы и аналогичные продукты из мяса, мясных субпродуктов или крови; готовые пищевые продукты, изготовленные на их основе 1601' WHERE `id`='67';
            UPDATE {{gstatgroupconstituents}} SET `name`='Готовые или консервированные продукты из мяса, мясных субпродуктов или крови прочие 1602' WHERE `id`='68';
            UPDATE {{gstatgroupconstituents}} SET `name`='Готовая или консервированная рыба; икра осетровых и ее заменители, изготовленные из икринок рыбы 1604' WHERE `id`='69';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сахар тростниковый или свекловичный и химически чистая сахароза, в твердом состоянии 1701' WHERE `id`='70';
            UPDATE {{gstatgroupconstituents}} SET `name`='Какао-бобы, целые или дробленые, сырые или жареные 1801' WHERE `id`='71';
            UPDATE {{gstatgroupconstituents}} SET `name`='Шоколад и прочие готовые пищевые продукты, содержащие какао 1806' WHERE `id`='72';
            UPDATE {{gstatgroupconstituents}} SET `name`='Макароны 1902' WHERE `id`='73';
            UPDATE {{gstatgroupconstituents}} SET `name`='Спагетти 1902' WHERE `id`='74';
            UPDATE {{gstatgroupconstituents}} SET `name`='Лапша 1902' WHERE `id`='75';
            UPDATE {{gstatgroupconstituents}} SET `name`='Рожки 1902' WHERE `id`='76';
            UPDATE {{gstatgroupconstituents}} SET `name`='Клецки 1902' WHERE `id`='77';
            UPDATE {{gstatgroupconstituents}} SET `name`='Равиоли 1902' WHERE `id`='78';
            UPDATE {{gstatgroupconstituents}} SET `name`='Каннеллони 1902' WHERE `id`='79';
            UPDATE {{gstatgroupconstituents}} SET `name`='Кускус 1902' WHERE `id`='80';
            UPDATE {{gstatgroupconstituents}} SET `name`='Хлеб 1905' WHERE `id`='81';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мучные кондитерские изделия 1905' WHERE `id`='82';
            UPDATE {{gstatgroupconstituents}} SET `name`='Пирожные 1905' WHERE `id`='83';
            UPDATE {{gstatgroupconstituents}} SET `name`='Печенье 1905' WHERE `id`='84';
            UPDATE {{gstatgroupconstituents}} SET `name`='Вафельные пластины 1905' WHERE `id`='85';
            UPDATE {{gstatgroupconstituents}} SET `name`='Пустые капсулы 1905' WHERE `id`='86';
            UPDATE {{gstatgroupconstituents}} SET `name`='Вафельные облатки 1905' WHERE `id`='87';
            UPDATE {{gstatgroupconstituents}} SET `name`='Рисовая бумага 1905' WHERE `id`='88';
            UPDATE {{gstatgroupconstituents}} SET `name`='Овощи, фрукты, орехи и другие съедобные части растений, приготовленные или консервированные с добавлением уксуса или уксусной кислоты 2001' WHERE `id`='89';
            UPDATE {{gstatgroupconstituents}} SET `name`='Соки фруктовые (включая виноградное сусло) и соки овощные, несброженные и не содержащие добавок спирта, с добавлением или без добавления сахара или других подслащивающих веществ 2009' WHERE `id`='90';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мороженое и прочие виды пищевого льда, не содержащие или содержащие какао 2105' WHERE `id`='91';
            UPDATE {{gstatgroupconstituents}} SET `name`='Воды, включая природные или искусственные минеральные, газированные, без добавления сахара или других подслащивающих или вкусо-ароматических веществ; лед и снег 2201' WHERE `id`='92';
            UPDATE {{gstatgroupconstituents}} SET `name`='Воды, включая минеральные и газированные, содержащие добавки сахара или других подслащивающих или вкусо-ароматических веществ 2202' WHERE `id`='93';
            UPDATE {{gstatgroupconstituents}} SET `name`='Пиво солодовое 2203' WHERE `id`='94';
            UPDATE {{gstatgroupconstituents}} SET `name`='Вина виноградные натуральные, включая крепленые 2204' WHERE `id`='95';
            UPDATE {{gstatgroupconstituents}} SET `name`='Табачное сырье 2401' WHERE `id`='97';
            UPDATE {{gstatgroupconstituents}} SET `name`='Cусло виноградное 2204' WHERE `id`='96';
            UPDATE {{gstatgroupconstituents}} SET `name`='Табачные отходы 2401' WHERE `id`='98';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сигары 2402' WHERE `id`='99';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сигариллы 2402' WHERE `id`='100';
            UPDATE {{gstatgroupconstituents}} SET `name`='Сигареты из табака или его заменителей 2402' WHERE `id`='101';
            UPDATE {{gstatgroupconstituents}} SET `name`='Свинина 0203' WHERE `id`='102';
            UPDATE {{gstatgroupconstituents}} SET `name`='Баранина 0204' WHERE `id`='103';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мясо и пищевые субпродукты домашней птицы 0207' WHERE `id`='105';
            UPDATE {{gstatgroupconstituents}} SET `name`='Козлятина 0204' WHERE `id`='104';
            UPDATE {{gstatgroupconstituents}} SET `name`='Кондитерские изделия из сахара (включая белый шоколад), не содержащие какао 1704' WHERE `id`='106';
            UPDATE {{gstatgroupconstituents}} SET `name`='Листы для облицовки 4408' WHERE `id`='107';
            UPDATE {{gstatgroupconstituents}} SET `name`='Пиломатериалы 4409' WHERE `id`='108';
            UPDATE {{gstatgroupconstituents}} SET `name`='Плиты древесно-стружечные 4410' WHERE `id`='109';
            UPDATE {{gstatgroupconstituents}} SET `name`='Плиты с ориентированной стружкой 4410' WHERE `id`='110';
            UPDATE {{gstatgroupconstituents}} SET `name`='Вафельные плиты 4410' WHERE `id`='111';
            UPDATE {{gstatgroupconstituents}} SET `name`='Плиты древесно-волокнистые из древесины 4411' WHERE `id`='112';
            UPDATE {{gstatgroupconstituents}} SET `name`='Фанера клееная 4412' WHERE `id`='113';
            UPDATE {{gstatgroupconstituents}} SET `name`='Панели фанерованные 4412' WHERE `id`='114';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мебель металлическая типа используемой в учреждениях столы письменные 9403' WHERE `id`='115';
            UPDATE {{gstatgroupconstituents}} SET `name`='Шкафы, снабженные дверями, задвижками или откидными досками 9403' WHERE `id`='116';
            UPDATE {{gstatgroupconstituents}} SET `name`='Шкафы для хранения документов, картотечные и прочие шкафы 9403' WHERE `id`='117';
            UPDATE {{gstatgroupconstituents}} SET `name`='Кровати 9403' WHERE `id`='118';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мебель кухонной секционная 9403' WHERE `id`='119';
            UPDATE {{gstatgroupconstituents}} SET `name`='Мебель спальная 9403' WHERE `id`='120';
            UPDATE {{gstatgroupconstituents}} SET `name`='Хирургическая 9402' WHERE `id`='121';
            UPDATE {{gstatgroupconstituents}} SET `name`='Стоматологическая 9402' WHERE `id`='122';
            UPDATE {{gstatgroupconstituents}} SET `name`='Ветеринарная 9402' WHERE `id`='123';
            UPDATE {{gstatgroupconstituents}} SET `name`='Столы для осмотра 9402' WHERE `id`='124';
            UPDATE {{gstatgroupconstituents}} SET `name`='Больничные койки с механическими приспособлениями 9402' WHERE `id`='125';
            UPDATE {{gstatgroupconstituents}} SET `name`='Парикмахерские кресла 9402' WHERE `id`='126';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}