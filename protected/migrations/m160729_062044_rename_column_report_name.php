<?php

class m160729_062044_rename_column_report_name extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAdditionalDataForTz();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAdditionalDataForTz(){
		return '
			ALTER TABLE {{report}}
				CHANGE COLUMN `reportJSON` `reportName` VARCHAR(255) NULL DEFAULT NULL;
		';
	}
}