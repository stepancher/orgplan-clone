<?php

class m161027_070751_add_tr_de_prices extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trproduct}} SET `name` = 'Ton & Beschallung' WHERE `trParentId` = '11' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Plasmabildwand' WHERE `trParentId` = '12' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Kaffeepause' WHERE `trParentId` = '13' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Büfett' WHERE `trParentId` = '14' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Bankett' WHERE `trParentId` = '15' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Visitenkarten-Druck' WHERE `trParentId` = '16' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Flyer-Druck' WHERE `trParentId` = '17' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Flyer-Druck DIN A4' WHERE `trParentId` = '18' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Flyer-Druck DIN A5' WHERE `trParentId` = '19' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Faltblätter-Druck' WHERE `trParentId` = '20' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Posterdruck' WHERE `trParentId` = '21' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Einladungskarten-Druck' WHERE `trParentId` = '22' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Plakatdruck' WHERE `trParentId` = '23' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Flyerdesign' WHERE `trParentId` = '24' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Faltblattdesign' WHERE `trParentId` = '25' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Bannerdesign' WHERE `trParentId` = '26' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Bannerdruck' WHERE `trParentId` = '27' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Bannerdruck' WHERE `trParentId` = '28' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Dienstleistungen des Promotionspersonals' WHERE `trParentId` = '29' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Dienstleistungen der Messehostessen' WHERE `trParentId` = '30' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Dienstleistungen der Hostessen' WHERE `trParentId` = '89' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Animateur-Dienstleistungen' WHERE `trParentId` = '90' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Luftballongirlanden' WHERE `trParentId` = '91' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Pop-Up-Counter' WHERE `trParentId` = '92' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Pop-Up-Display' WHERE `trParentId` = '93' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Roll-Up-Display' WHERE `trParentId` = '94' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `name` = 'Tablet-Ständer' WHERE `trParentId` = '95' AND `langId` = 'de';
            
            UPDATE {{trproduct}} SET `param` = 'Mischpult, Verstärker, Mikrofon, Tonanlage, Leistung bis 1 kW' WHERE `trParentId` = '11' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'Plasmabildwand 50\"' WHERE `trParentId` = '12' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'pro Person, insges. 50 Personen' WHERE `trParentId` = '13' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'pro Person, insges. 50 Personen' WHERE `trParentId` = '14' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'pro Person, insges. 50 Personen' WHERE `trParentId` = '15' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '90x50mm, Karton, 250g/m², 100 St.' WHERE `trParentId` = '16' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '100x210 Euroformat, 115 g/m², beidseitig, 1.000 St.' WHERE `trParentId` = '17' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A4, 115 g/m², Kreidepapier, beidseitig, 1.000 St.' WHERE `trParentId` = '18' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A5, 115 g/m², Kreidepapier, beidseitig, 1.000 St.' WHERE `trParentId` = '19' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A4, Zweifalz, vollfarbig, 1.000 St.' WHERE `trParentId` = '20' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A3, einseitig, farbig, für Innenbereich, 1.000 St.' WHERE `trParentId` = '21' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '200x210, Digitaldruck, 100 St.' WHERE `trParentId` = '22' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A2, Zeitungspapier, 50 St.' WHERE `trParentId` = '23' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A5, beiderseitig' WHERE `trParentId` = '24' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'A4, gefalzt bis 1/3 Blattes, beiderseitig' WHERE `trParentId` = '25' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '3x6 m, einseitig' WHERE `trParentId` = '26' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '3x6 m, einseitig, 300 g/qm Stärke, Breitformatdruck, 1,2 cm Ösen' WHERE `trParentId` = '27' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = 'Netz, Sektion 500x1000,m², Ösen' WHERE `trParentId` = '28' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '1 Promotionshostess/Host' WHERE `trParentId` = '29' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '1 Messehostess/Host' WHERE `trParentId` = '30' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '1 Hostess/Host' WHERE `trParentId` = '89' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '1 Animateur/-in' WHERE `trParentId` = '90' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '1 m' WHERE `trParentId` = '91' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '' WHERE `trParentId` = '92' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '3x3 Sektionen, mit Beklebung' WHERE `trParentId` = '93' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '100x200 cm, mit Beklebung' WHERE `trParentId` = '94' AND `langId` = 'de';
            UPDATE {{trproduct}} SET `param` = '8 Sektionen 70x100 cm, Frontblende 70x30 cm, mit Beklebung' WHERE `trParentId` = '95' AND `langId` = 'de';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}