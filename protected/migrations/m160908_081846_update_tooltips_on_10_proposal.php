<?php

class m160908_081846_update_tooltips_on_10_proposal extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2171';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2184';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2185';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2186';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2187';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2188';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2189';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2190';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2191';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2192';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2195';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2250';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2288';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2289';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2290';
            UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">COST OF SERVICES</div> increases: <br> by 100% if ordered after September 21, 2016' WHERE `id`='2291';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}