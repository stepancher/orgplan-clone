<?php

class m160808_095011_created_databases_for_proposal extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->getCreateTable();
        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        return true;
    }

    public function getCreateTable()
    {
        return "
            CREATE DATABASE IF NOT EXISTS `orgplan_proposal` /*!40100 DEFAULT CHARACTER SET utf8 */;
            CREATE DATABASE IF NOT EXISTS `orgplan_proposal_dev` /*!40100 DEFAULT CHARACTER SET utf8 */;
        ";
    }
}