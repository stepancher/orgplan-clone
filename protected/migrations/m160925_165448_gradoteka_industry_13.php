<?php

class m160925_165448_gradoteka_industry_13 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{charttablegroup}} SET `name`='Основные показатели отрасли' WHERE `id`='19';
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Показатели производства', '13', '2');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Движение основных продуктов питания', '13', '3');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Экспорт\\Импорт', '13', '4');
            INSERT INTO {{charttablegroup}} (`name`, `industryId`, `position`) VALUES ('Таможенная справка', '13', '5');
            
            UPDATE {{charttable}} SET `name`='Основные продукты животноводства по регионам', `position`='1' WHERE `id`='49';
            UPDATE {{charttable}} SET `name`='Экспорт/импорт продовольственных товаров по регионам', `groupId`='32', `position`='2' WHERE `id`='50';
            UPDATE {{charttable}} SET `name`='Движение основных продуктов  питания / растениеводство', `groupId`='31', `position`='1' WHERE `id`='51';
            UPDATE {{charttable}} SET `name`='Движение основных продуктов  питания / растениеводство (продолжение)', `groupId`='31', `position`='2' WHERE `id`='52';
            UPDATE {{charttable}} SET `name`='Движение основных продуктов питания / животноводство', `groupId`='31', `position`='3' WHERE `id`='53';
            UPDATE {{charttable}} SET `name`='Движение основных продуктов питания / животноводство (продолжение)', `groupId`='31', `position`='4' WHERE `id`='54';
            UPDATE {{charttable}} SET `name`='Основные сельскохозяйственные культуры по регионам', `groupId`='19', `position`='2' WHERE `id`='48';
            UPDATE {{charttable}} SET `name`='Производство напитков по округам', `groupId`='30', `position`='5' WHERE `id`='47';
            UPDATE {{charttable}} SET `name`='Производство основных пищевых продуктов/ животноводство/по округам', `groupId`='30', `position`='3' WHERE `id`='46';
            UPDATE {{charttable}} SET `name`='Производство основных пищевых продуктов/ растениеводство/по округам', `groupId`='30', `position`='1' WHERE `id`='45';
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Производство основных пищевых продуктов/ растениеводство/по регионам', '2', '13', 'region', '30', '2');
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Производство основных пищевых продуктов/ животноводство/по регионам', '2', '13', 'region', '30', '4');
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Производство напитков по регионам', '2', '13', 'region', '30', '6');
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('Экспорт/импорт продовольственных товаров по округам', '2', '13', 'state', '32', '1');
            INSERT INTO {{charttable}} (`name`, `type`, `industryId`, `area`, `groupId`, `position`) VALUES ('ТН ВЭД', '2', '13', 'country', '33', '1');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e451377d89702498c26ff', 'Производство муки', '', '6', '2015', '89', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e44ae77d89702498befb9', 'Производство крупы', '', '6', '2015', '89', '#a0d468', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e46e777d89702498d109a', 'Производство хлеба', '', '6', '2015', '89', '#a0d468', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e45e177d89702498ca67f', 'Производство сахара', '', '6', '2015', '89', '#a0d468', '4');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e44d677d89702498c0a4c', 'Производство масла растительного', '', '6', '2015', '89', '#a0d468', '5');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e451d77d89702498c2dd7', 'Производство мяса и субпродуктов пищевых убойных животных', '', '6', '2015', '90', '#fa694c', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e451c77d89702498c2d61', 'Производство мяса и субпродуктов пищевых домашней птицы', '', '6', '2015', '90', '#fa694c', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e443077d89702498ba0e3', 'Производство колбасных изделий в натуральном выражении', '', '6', '2015', '90', '#fa694c', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e45d777d89702498c9e6a', 'Производство рыбы и рыбных продуктов', '', '6', '2015', '90', '#fa694c', '4');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('56d02ef7f7a9ad01088b5796', 'Производство молока', '', '6', '2015', '90', '#fa694c', '5');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e44d577d89702498c073f', 'Производство масла сливочного', '', '6', '2015', '90', '#fa694c', '6');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('572b3efc76d89734208b6ceb', 'Производство водки', '', '6', '2015', '91', '#43ade3', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e449a77d89702498bdec6', 'Производство коньяка', '', '6', '2015', '91', '#43ade3', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('577e7c79bd4c562e658b465a', 'Производство вина', '', '6', '2015', '91', '#43ade3', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('572b3ef776d89734208b5ced', 'Производство игристых и газированных вин', '', '6', '2015', '91', '#43ade3', '4');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e456577d89702498c5a15', 'Производство пива', '', '6', '2015', '91', '#43ade3', '5');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('571e43f377d89702498b7475', 'Производство воды минеральной', '', '6', '2015', '91', '#43ade3', '6');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('577e6f21bd4c5654298b7a01', '', '', '4', '2015', '92', '#43ade3', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `epoch`, `tableId`, `color`, `position`) VALUES ('577e6f21bd4c5654298b7a01', 'Экспорт продовольственных товаров и сельскохозяйственного сырья', '', '2015', '92', '#a0d468', '2');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `widgetType`, `epoch`, `tableId`, `color`, `position`) VALUES ('577e6f16bd4c5654298b4f36', '', '', '4', '2015', '92', '#4ec882', '3');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `count`, `epoch`, `tableId`, `color`, `position`) VALUES ('577e6f16bd4c5654298b4f36', 'Импорт продовольственных товаров и сельскохозяйственного сырья', '', '2015', '92', '#a0d468', '4');
            
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('13', 'Экспорт РФ', '2015', '93', '#a0d468', '1');
            INSERT INTO {{chartcolumn}} (`statId`, `header`, `epoch`, `tableId`, `color`, `position`) VALUES ('14', 'Импорт РФ', '2015', '93', '#a0d468', '2');
            
            UPDATE {{charttable}} SET `granularity`='' WHERE `id`='89';
            UPDATE {{charttable}} SET `granularity`='' WHERE `id`='90';
            UPDATE {{charttable}} SET `granularity`='' WHERE `id`='91';
            UPDATE {{charttable}} SET `granularity`='' WHERE `id`='92';
            UPDATE {{charttable}} SET `granularity`=NULL WHERE `id`='93';
            
            INSERT INTO {{gstatgroup}} (`objId`, `name`, `position`, `industryId`) VALUES ('379', 'Колбасы и аналогичные продукты из мяса', '1', '13');
            
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500077', '1601', 'Экспорт', 'derivative', 'ДолларСША', '1');
            INSERT INTO {{gstattypes}} (`gId`, `name`, `parentName`, `type`, `unit`, `autoUpdate`) VALUES ('500079', '1601', 'Импорт', 'derivative', 'ДолларСША', '1');
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503240';
            UPDATE {{gstattypes}} SET `autoUpdate`='0' WHERE `id`='503239';
            
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500077', 'aggregatable', 'SUM');
            INSERT INTO {{gstattypehasformula}} (`gId`, `formulaType`, `formula`) VALUES ('500079', 'aggregatable', 'SUM');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','1','5745eb7a74d897cb088b51e1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','2','5745eb7a74d897cb088b51e5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','3','5745ec4774d897cb088d91a7');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','4','5745eb7a74d897cb088b51e9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','5','5745ebba74d897cb088c2638');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','6','5745eb7a74d897cb088b51fd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','7','5745ed5774d897cb0890085d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','8','5745ebba74d897cb088c263c');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','9','5745eef974d897cb0893b097');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','10','5745eb7a74d897cb088b51f5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','11','5745eb7a74d897cb088b51ed');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','12','5745ebba74d897cb088c2640');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','13','5745ec4774d897cb088d91af');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','14','5745ee7574d897cb08928ae5');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','15','5745eb7a74d897cb088b51f1');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','16','5745ed5774d897cb08900865');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','17','5745ec4874d897cb088d91b9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','18','5745ebba74d897cb088c2646');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','19','5745ec4874d897cb088d91bd');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','20','5745ed5774d897cb0890086d');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','21','5745ebba74d897cb088c264a');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','22','5745ed5774d897cb08900871');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','23','5745eb7a74d897cb088b51f9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','24','5745ee7574d897cb08928af9');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','25','5745ebff74d897cb088cde1e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','26','5745ebba74d897cb088c2654');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','27','5745ebff74d897cb088cde22');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','28','5745ede574d897cb08914764');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','29','5745ebba74d897cb088c264e');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','30','5745ee7574d897cb08928aeb');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','31','5745eec174d897cb0893373f');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('36','32','5745ef3f74d897cb089448a4');
            
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','1','5745f1cd74d897cb089a90a4');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','2','5745ef8b74d897cb0894f649');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','3','5745ef8b74d897cb0894f651');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','4','5745f43274d897cb089f4b6b');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','5','5745ef8b74d897cb0894f655');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','6','5745efef74d897cb08964561');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','7','5745ef8b74d897cb0894f659');
            INSERT INTO {{gstattypeformulahasstat}} (`formulaId`,`key`,`statId`) VALUES ('37','8','5745ef8b74d897cb0894f64d');
            
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500077', 'country');
            INSERT INTO {{gobjectshasgstattypes}} (`objId`,`statId`,`gType`) VALUES ('379', '500079', 'country');
            
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68813', '2016-09-25 20:22:28', '62146700', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68813', '2016-09-25 20:22:28', '10972000', '2016');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68814', '2016-09-25 20:22:45', '38474700', '2015');
            INSERT INTO {{gvalue}} (`setId`,`timestamp`,`value`,`epoch`) VALUES ('68814', '2016-09-25 20:22:45', '11981500', '2016');
            
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('20', '68813');
            INSERT INTO {{gstatgrouphasstat}} (`statGroupId`, `gObjectHasGStatTypeId`) VALUES ('20', '68814');
            
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('13', '500077', 'Доллар США');
            INSERT INTO {{gstattypegroup}} (`groupId`, `gId`, `unit`) VALUES ('14', '500079', 'Доллар США');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}