<?php

class m170228_133418_add_fairhasassociation_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ef.id, eass.id FROM
                                        (SELECT ed.id, ed.member_of_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        UNION
                                        SELECT ed.id, ed.member_of_second_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        UNION
                                        SELECT ed.id, ed.member_of_3_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        UNION
                                        SELECT ed.id, ed.member_of_4_id_en AS assId 
                                        FROM {$expodataRaw}.expo_data ed
                                        ) t LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass ON eass.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdbfairhasassociation}} efha ON efha.fairId = ef.id AND efha.associationId = eass.id
                                        WHERE t.assId != 0 ORDER BY ef.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbfairhasassociation}} (`fairId`,`associationId`) VALUES (f_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_fairhasassociation_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}