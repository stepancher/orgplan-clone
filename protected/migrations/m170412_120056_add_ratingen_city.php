<?php

class m170412_120056_add_ratingen_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('157', 'ratingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1555', 'ru', 'Ратинген');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1555', 'en', 'Ratingen');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1555', 'de', 'Ratingen');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}