<?php

class m160704_120023_create_9a_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			INSERT INTO {{proposalelementclass}} (`name`, `fixed`, `fairId`, `active`, `deadline`) VALUES ('zayavka_n9a', '0', '184', '0', '2016-10-08 09:00:00');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_shadow_time', 'string', '0', '0', '2623', '10', 'hiddenDiscount', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_hint_danger', 'int', '0', '0', '2624', '10', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone', 'int', '0', '0', '2625', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n9a_zone_hint', 'int', '0', '2625', '2625', '20', 'hintWithView', '0');
			UPDATE {{trattributeclassproperty}} SET `value`='pcs.' WHERE `id`='2748';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2619', 'USD', 'unitTitle');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2122', 'ru', 'USD');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2122', 'en', 'USD');
			UPDATE {{attributeclassproperty}} SET `defaultValue`=NULL WHERE `id`='2122';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `class`, `collapse`) VALUES (NULL, 'parties_signature_n8', 'int', '1', '0', '0', '2627', 'partiesSignature', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_left_block_n8', 'int', '1', '2627', '2627', '1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES (NULL, 'parties_signature_right_block_n8', 'int', '1', '2627', '2627', '2', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n8_1', 'string', '1', '2628', '2627', '1', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n8_2', 'string', '1', '2628', '2627', '2', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n8_3', 'string', '1', '2628', '2627', '3', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n8_1', 'string', '1', '2629', '2627', '1', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n8_2', 'string', '1', '2629', '2627', '2', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n8_3', 'string', '1', '2629', '2627', '3', 'envVariable', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_left_n8_4', 'string', '1', '2628', '2627', '5', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'parties_signature_row_right_n8_4', 'string', '1', '2629', '2627', '5', 'text', '0');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('24', '2627', '0', '0', '80');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2630', 'ru', 'Экспонат');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2633', 'ru', 'Организатор');
			UPDATE {{trattributeclass}} SET `label`='Экспонент' WHERE `id`='2893';
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2636', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2637', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2631', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2632', 'ru', 'должность');
			UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2895';
			UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2896';
			UPDATE {{trattributeclass}} SET `trParentId`='2634' WHERE `id`='2900';
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2634', 'ru', 'ФИО');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2635', 'ru', 'ФИО');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2634', 'organizerPost', 'envVariable');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2635', 'organizerName', 'envVariable');
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='2528';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='2529';
			UPDATE {{attributeclass}} SET `class`='text' WHERE `id`='2530';
			UPDATE {{trattributeclassproperty}} SET `value`='шт.' WHERE `id`='2748';
			UPDATE {{trattributeclassproperty}} SET `value`='psc.' WHERE `id`='2749';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('2538', '2537', 'charsCount', '{chars: 430}');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2538', '1', 'readonly');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2523', '1', 'readonly');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2623', '0', '0', '10');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2624', '0', '0', '20');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2625', '0', '0', '30');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2626', '0', '0', '40');
			UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2897';
			UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2895';
			UPDATE {{trattributeclass}} SET `label`='ФИО' WHERE `id`='2896';
			UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2895';
			UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2897';
			UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2893';
			UPDATE {{trattributeclass}} SET `label`='подпись' WHERE `id`='2894';
			UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2895';
			UPDATE {{trattributeclass}} SET `label`='должность' WHERE `id`='2897';

		";
	}
}