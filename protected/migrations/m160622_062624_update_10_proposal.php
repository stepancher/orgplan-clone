<?php

class m160622_062624_update_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2147';
		UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2179';
		UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2180';
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2147', '1', 'discount');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2179', '1', 'discount');
		INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2180', '1', 'discount');
";
	}
}