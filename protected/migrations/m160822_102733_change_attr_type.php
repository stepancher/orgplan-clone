<?php

class m160822_102733_change_attr_type extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "";
    }

    public function downSql()
    {
        return TRUE;
    }
}