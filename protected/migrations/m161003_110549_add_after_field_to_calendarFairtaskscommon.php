<?php

class m161003_110549_add_after_field_to_calendarFairtaskscommon extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        return "
            ALTER TABLE {{calendarfairtaskscommon}}
              ADD COLUMN `after` INT(11) NULL AFTER `before`;
              
            UPDATE {{calendarfairtaskscommon}} SET `before`=NULL, `after`='4' WHERE `id`='586';
            UPDATE {{calendarfairtaskscommon}} SET `before`=NULL, `after`='5' WHERE `id`='587';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}