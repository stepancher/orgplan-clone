<?php

class m160727_144818_add_tr_districts extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('26', 'en', 'Baku-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('27', 'en', 'Yerevan-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('28', 'en', 'Minsk-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('29', 'en', 'Mogilev-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('30', 'en', 'Grodno-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('31', 'en', 'Astana-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('32', 'en', 'Almaty-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('33', 'en', 'Atyrau-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('34', 'en', 'Mangystau-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('35', 'en', 'Aktobe-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('36', 'en', 'Pavlodar-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('37', 'en', 'Bishkek-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('38', 'en', 'Kishinev-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('39', 'en', 'Karaganda-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('40', 'en', 'Tashkent-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('41', 'en', 'Fergana-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('42', 'en', 'Ashgabat-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('43', 'en', 'Balkan-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('44', 'en', 'Tbilisi-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('45', 'en', 'Ulaanbaatar-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('46', 'en', 'Kiev-region');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('47', 'en', 'Odessa-oblast');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('48', 'en', 'Simferopol-municipality');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('49', 'en', 'Yalta-municipality');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('50', 'en', 'Sevastopol-municipality');
			
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('26', 'ru', 'Округ Баку');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('27', 'ru', 'Округ Ереван');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('28', 'ru', 'Минская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('29', 'ru', 'Могилёвская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('30', 'ru', 'Гродненская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('31', 'ru', 'Округ Астана');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('32', 'ru', 'Округ Алматы');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('33', 'ru', 'Атырауская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('34', 'ru', 'Мангистауская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('35', 'ru', 'Актюбинская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('36', 'ru', 'Павлодарская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('37', 'ru', 'Округ Бишкек');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('38', 'ru', 'Кишиневская агломерация');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('39', 'ru', 'Карагандинская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('40', 'ru', 'Ташкентская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('41', 'ru', 'Ферганская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('42', 'ru', 'Округ Ашхабад');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('43', 'ru', 'Балканский велаят');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('44', 'ru', 'Округ Тбилиси');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('45', 'ru', 'Округ Улан-Батор');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('46', 'ru', 'Киевская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('47', 'ru', 'Одесская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('48', 'ru', 'Республика Крым');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('49', 'ru', 'Республики Крым');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('50', 'ru', 'Республики Крым');
			
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('26', 'de', 'Округ Баку');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('27', 'de', 'Округ Ереван');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('28', 'de', 'Минская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('29', 'de', 'Могилёвская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('30', 'de', 'Гродненская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('31', 'de', 'Округ Астана');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('32', 'de', 'Округ Алматы');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('33', 'de', 'Атырауская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('34', 'de', 'Мангистауская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('35', 'de', 'Актюбинская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('36', 'de', 'Павлодарская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('37', 'de', 'Округ Бишкек');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('38', 'de', 'Кишиневская агломерация');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('39', 'de', 'Карагандинская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('40', 'de', 'Ташкентская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('41', 'de', 'Ферганская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('42', 'de', 'Округ Ашхабад');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('43', 'de', 'Балканский велаят');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('44', 'de', 'Округ Тбилиси');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('45', 'de', 'Округ Улан-Батор');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('46', 'de', 'Киевская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('47', 'de', 'Одесская область');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('48', 'de', 'Республика Крым');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('49', 'de', 'Республики Крым');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('50', 'de', 'Республики Крым');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}