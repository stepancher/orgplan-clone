<?php

class m170405_140319_create_tbl_currency_course extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{currencycourse}} (
                `id` INT NOT NULL AUTO_INCREMENT,
                `source` INT NULL,
                `target` INT NULL,
                `value` FLOAT NULL,
                PRIMARY KEY (`id`),
                UNIQUE INDEX `source_UNIQUE` (`source` ASC),
                UNIQUE INDEX `target_UNIQUE` (`target` ASC));
              
            ALTER TABLE {{currency}} 
                ADD COLUMN `code` VARCHAR(45) NULL AFTER `icon`;
                
            UPDATE {{currency}} SET `code`='RUB' WHERE `id`='1';
            UPDATE {{currency}} SET `code`='USD' WHERE `id`='2';
            UPDATE {{currency}} SET `code`='EUR' WHERE `id`='3';
            UPDATE {{currency}} SET `code`='CNY' WHERE `id`='6';
            UPDATE {{currency}} SET `code`='UAH' WHERE `id`='7';
            UPDATE {{currency}} SET `code`='GBP' WHERE `id`='8';
                
            ALTER TABLE {{currencycourse}} 
                ADD UNIQUE INDEX `source_target_unique` (`source` ASC, `target` ASC),
                DROP INDEX `target_UNIQUE` ,
                DROP INDEX `source_UNIQUE` ;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}