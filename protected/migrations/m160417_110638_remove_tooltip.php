<?php

class m160417_110638_remove_tooltip extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `tooltipCustom`=NULL WHERE `id`='506';
			UPDATE {{attributeclass}} SET `class`='imageNotForView' WHERE `id`='266';
			UPDATE {{attributeclass}} SET `unitTitle`='м²' WHERE `id`='305';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('305', 'м²', 'unitTitle');
			UPDATE {{attributeclass}} SET `parent`='678' WHERE `id`='769';
			UPDATE {{attributeclass}} SET `parent`='678' WHERE `id`='770';
			UPDATE {{attributeclass}} SET `parent`='678' WHERE `id`='771';
			UPDATE {{attributeclass}} SET `parent`='732' WHERE `id`='789';
			UPDATE {{attributeclass}} SET `parent`='741' WHERE `id`='790';
			UPDATE {{attributeclass}} SET `parent`='741' WHERE `id`='791';

	    ";
	}
}