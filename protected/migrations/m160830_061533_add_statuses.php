<?php

class m160830_061533_add_statuses extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{fairstatus}} (
              `id` INT(11) NOT NULL,
              `status` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            ALTER TABLE {{fairstatus}} 
            ADD UNIQUE INDEX `UNIQ_status` (`status` ASC);
            
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('1', 'Для передачи на копирайтинг');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('2', 'Копирайтер 1');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('3', 'Копирайтер 2');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('4', 'Текст готов');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('5', 'Менеджер 1 - без дат');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('6', 'Менеджер 2 - на звонок');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('7', 'Менеджер 3 - в работе');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('8', 'Менеджер 4 - х');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('9', 'Копирайтер 3');
            INSERT INTO {{fairstatus}} (`id`, `status`) VALUES ('10', 'Отменена');
            
            ALTER TABLE {{fairstatus}} 
            ADD COLUMN `color` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;
            
            UPDATE {{fairstatus}} SET `color`='00bfff' WHERE `id`='1';
            UPDATE {{fairstatus}} SET `color`='ff7538' WHERE `id`='2';
            UPDATE {{fairstatus}} SET `color`='b00000' WHERE `id`='3';
            UPDATE {{fairstatus}} SET `color`='ff335c' WHERE `id`='4';
            UPDATE {{fairstatus}} SET `color`='f0e68c' WHERE `id`='5';
            UPDATE {{fairstatus}} SET `color`='c7b61a' WHERE `id`='6';
            UPDATE {{fairstatus}} SET `color`='e28b00' WHERE `id`='7';
            UPDATE {{fairstatus}} SET `color`='b7410e' WHERE `id`='8';
            UPDATE {{fairstatus}} SET `color`='65A561' WHERE `id`='9';
            UPDATE {{fairstatus}} SET `color`='c10020' WHERE `id`='10';
            
            ALTER TABLE {{fair}} 
            CHANGE COLUMN `status` `statusId` INT(11) NULL DEFAULT '0' ;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}