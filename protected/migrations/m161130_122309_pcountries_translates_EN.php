<?php

class m161130_122309_pcountries_translates_EN extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposal->beginTransaction();
        try {
            Yii::app()->dbProposal->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trpcountry}} SET `name`='Russland' WHERE `id`='511';
            UPDATE {{trpcountry}} SET `name`='Die Ukraine' WHERE `id`='512';
            UPDATE {{trpcountry}} SET `name`='Weißrussland' WHERE `id`='513';
            UPDATE {{trpcountry}} SET `name`='Kasachstan' WHERE `id`='514';
            UPDATE {{trpcountry}} SET `name`='Deutschland' WHERE `id`='515';
            UPDATE {{trpcountry}} SET `name`='Abchasien' WHERE `id`='516';
            UPDATE {{trpcountry}} SET `name`='Australien' WHERE `id`='517';
            UPDATE {{trpcountry}} SET `name`='Österreich' WHERE `id`='518';
            UPDATE {{trpcountry}} SET `name`='Aserbaidschan' WHERE `id`='519';
            UPDATE {{trpcountry}} SET `name`='Albanien' WHERE `id`='520';
            UPDATE {{trpcountry}} SET `name`='Algerien' WHERE `id`='521';
            UPDATE {{trpcountry}} SET `name`='Angola' WHERE `id`='522';
            UPDATE {{trpcountry}} SET `name`='Ангуилья' WHERE `id`='523';
            UPDATE {{trpcountry}} SET `name`='Andorra' WHERE `id`='524';
            UPDATE {{trpcountry}} SET `name`='Antigua und Barbuda' WHERE `id`='525';
            UPDATE {{trpcountry}} SET `name`='Antillen' WHERE `id`='526';
            UPDATE {{trpcountry}} SET `name`='Argentinien' WHERE `id`='527';
            UPDATE {{trpcountry}} SET `name`='Armenien' WHERE `id`='528';
            UPDATE {{trpcountry}} SET `name`='Арулько' WHERE `id`='529';
            UPDATE {{trpcountry}} SET `name`='Afghanistan' WHERE `id`='530';
            UPDATE {{trpcountry}} SET `name`='Die Bahamas' WHERE `id`='531';
            UPDATE {{trpcountry}} SET `name`='Bangladesh' WHERE `id`='532';
            UPDATE {{trpcountry}} SET `name`='Barbados' WHERE `id`='533';
            UPDATE {{trpcountry}} SET `name`='Bahrain' WHERE `id`='534';
            UPDATE {{trpcountry}} SET `name`='Belize' WHERE `id`='535';
            UPDATE {{trpcountry}} SET `name`='Belgien' WHERE `id`='536';
            UPDATE {{trpcountry}} SET `name`='Benin' WHERE `id`='537';
            UPDATE {{trpcountry}} SET `name`='Bermuda' WHERE `id`='538';
            UPDATE {{trpcountry}} SET `name`='Bulgarien' WHERE `id`='539';
            UPDATE {{trpcountry}} SET `name`='Bolivien' WHERE `id`='540';
            UPDATE {{trpcountry}} SET `name`='Bosnien und Herzegowina' WHERE `id`='541';
            UPDATE {{trpcountry}} SET `name`='Botswana' WHERE `id`='542';
            UPDATE {{trpcountry}} SET `name`='Brasilien' WHERE `id`='543';
            UPDATE {{trpcountry}} SET `name`='Die Britische Jungferninseln' WHERE `id`='544';
            UPDATE {{trpcountry}} SET `name`='Brunei' WHERE `id`='545';
            UPDATE {{trpcountry}} SET `name`='Burkina Faso' WHERE `id`='546';
            UPDATE {{trpcountry}} SET `name`='Burundi' WHERE `id`='547';
            UPDATE {{trpcountry}} SET `name`='Bhutan' WHERE `id`='548';
            UPDATE {{trpcountry}} SET `name`='Wallis und Futuna die Inseln' WHERE `id`='549';
            UPDATE {{trpcountry}} SET `name`='Vanuatu' WHERE `id`='550';
            UPDATE {{trpcountry}} SET `name`='Großbritannien' WHERE `id`='551';
            UPDATE {{trpcountry}} SET `name`='Ungarn' WHERE `id`='552';
            UPDATE {{trpcountry}} SET `name`='Venezuela' WHERE `id`='553';
            UPDATE {{trpcountry}} SET `name`='Osttimor' WHERE `id`='554';
            UPDATE {{trpcountry}} SET `name`='Vietnam' WHERE `id`='555';
            UPDATE {{trpcountry}} SET `name`='Gabun' WHERE `id`='556';
            UPDATE {{trpcountry}} SET `name`='Haiti' WHERE `id`='557';
            UPDATE {{trpcountry}} SET `name`='Guyana' WHERE `id`='558';
            UPDATE {{trpcountry}} SET `name`='Gambia' WHERE `id`='559';
            UPDATE {{trpcountry}} SET `name`='Ghana' WHERE `id`='560';
            UPDATE {{trpcountry}} SET `name`='Guadeloupe' WHERE `id`='561';
            UPDATE {{trpcountry}} SET `name`='Guatemala' WHERE `id`='562';
            UPDATE {{trpcountry}} SET `name`='Guinea' WHERE `id`='563';
            UPDATE {{trpcountry}} SET `name`='Guinea-Bissau' WHERE `id`='564';
            UPDATE {{trpcountry}} SET `name`='Guernsey die Insel' WHERE `id`='565';
            UPDATE {{trpcountry}} SET `name`='Gibraltar' WHERE `id`='566';
            UPDATE {{trpcountry}} SET `name`='Honduras' WHERE `id`='567';
            UPDATE {{trpcountry}} SET `name`='Hongkong' WHERE `id`='568';
            UPDATE {{trpcountry}} SET `name`='Grenada' WHERE `id`='569';
            UPDATE {{trpcountry}} SET `name`='Grönland' WHERE `id`='570';
            UPDATE {{trpcountry}} SET `name`='Griechenland' WHERE `id`='571';
            UPDATE {{trpcountry}} SET `name`='Georgien' WHERE `id`='572';
            UPDATE {{trpcountry}} SET `name`='Dänemark' WHERE `id`='573';
            UPDATE {{trpcountry}} SET `name`='Der Strickstoff die Insel' WHERE `id`='574';
            UPDATE {{trpcountry}} SET `name`='Djibouti' WHERE `id`='575';
            UPDATE {{trpcountry}} SET `name`='Die dominikanische Republik' WHERE `id`='576';
            UPDATE {{trpcountry}} SET `name`='Europas die Insel' WHERE `id`='577';
            UPDATE {{trpcountry}} SET `name`='Ägypten' WHERE `id`='578';
            UPDATE {{trpcountry}} SET `name`='Sambia' WHERE `id`='579';
            UPDATE {{trpcountry}} SET `name`='Westliche Sahara' WHERE `id`='580';
            UPDATE {{trpcountry}} SET `name`='Simbabwe' WHERE `id`='581';
            UPDATE {{trpcountry}} SET `name`='Israel' WHERE `id`='582';
            UPDATE {{trpcountry}} SET `name`='Indien' WHERE `id`='583';
            UPDATE {{trpcountry}} SET `name`='Indonesien' WHERE `id`='584';
            UPDATE {{trpcountry}} SET `name`='Jordanien' WHERE `id`='585';
            UPDATE {{trpcountry}} SET `name`='Der Irak' WHERE `id`='586';
            UPDATE {{trpcountry}} SET `name`='Iran' WHERE `id`='587';
            UPDATE {{trpcountry}} SET `name`='Irland' WHERE `id`='588';
            UPDATE {{trpcountry}} SET `name`='Island' WHERE `id`='589';
            UPDATE {{trpcountry}} SET `name`='Spanien' WHERE `id`='590';
            UPDATE {{trpcountry}} SET `name`='Italien' WHERE `id`='591';
            UPDATE {{trpcountry}} SET `name`='Jemen' WHERE `id`='592';
            UPDATE {{trpcountry}} SET `name`='Kap Verde' WHERE `id`='593';
            UPDATE {{trpcountry}} SET `name`='Kambodscha' WHERE `id`='594';
            UPDATE {{trpcountry}} SET `name`='Kamerun' WHERE `id`='595';
            UPDATE {{trpcountry}} SET `name`='Kanada' WHERE `id`='596';
            UPDATE {{trpcountry}} SET `name`='Katar' WHERE `id`='597';
            UPDATE {{trpcountry}} SET `name`='Kenia' WHERE `id`='598';
            UPDATE {{trpcountry}} SET `name`='Zypern' WHERE `id`='599';
            UPDATE {{trpcountry}} SET `name`='Kiribati' WHERE `id`='600';
            UPDATE {{trpcountry}} SET `name`='China' WHERE `id`='601';
            UPDATE {{trpcountry}} SET `name`='Kolumbien' WHERE `id`='602';
            UPDATE {{trpcountry}} SET `name`='Die Komoren' WHERE `id`='603';
            UPDATE {{trpcountry}} SET `name`='Kongo (Brazzaville)' WHERE `id`='604';
            UPDATE {{trpcountry}} SET `name`='Kongo (Kinshasa)' WHERE `id`='605';
            UPDATE {{trpcountry}} SET `name`='Costa Rica' WHERE `id`='606';
            UPDATE {{trpcountry}} SET `name`='Cote-D-Ivoire' WHERE `id`='607';
            UPDATE {{trpcountry}} SET `name`='Kuba' WHERE `id`='608';
            UPDATE {{trpcountry}} SET `name`='Kuweit' WHERE `id`='609';
            UPDATE {{trpcountry}} SET `name`='Cooks die Inseln' WHERE `id`='610';
            UPDATE {{trpcountry}} SET `name`='Kirgisistan' WHERE `id`='611';
            UPDATE {{trpcountry}} SET `name`='Laos' WHERE `id`='612';
            UPDATE {{trpcountry}} SET `name`='Lettland' WHERE `id`='613';
            UPDATE {{trpcountry}} SET `name`='Lesotho' WHERE `id`='614';
            UPDATE {{trpcountry}} SET `name`='Liberia' WHERE `id`='615';
            UPDATE {{trpcountry}} SET `name`='Libanon' WHERE `id`='616';
            UPDATE {{trpcountry}} SET `name`='Libyen' WHERE `id`='617';
            UPDATE {{trpcountry}} SET `name`='Litauen' WHERE `id`='618';
            UPDATE {{trpcountry}} SET `name`='Liechtenstein' WHERE `id`='619';
            UPDATE {{trpcountry}} SET `name`='Luxemburg' WHERE `id`='620';
            UPDATE {{trpcountry}} SET `name`='Mauritius' WHERE `id`='621';
            UPDATE {{trpcountry}} SET `name`='Mauretanien' WHERE `id`='622';
            UPDATE {{trpcountry}} SET `name`='Madagaskar' WHERE `id`='623';
            UPDATE {{trpcountry}} SET `name`='Makedonien' WHERE `id`='624';
            UPDATE {{trpcountry}} SET `name`='Malawi' WHERE `id`='625';
            UPDATE {{trpcountry}} SET `name`='Malaysia' WHERE `id`='626';
            UPDATE {{trpcountry}} SET `name`='Mali' WHERE `id`='627';
            UPDATE {{trpcountry}} SET `name`='Die maledivischen Inseln' WHERE `id`='628';
            UPDATE {{trpcountry}} SET `name`='Malta' WHERE `id`='629';
            UPDATE {{trpcountry}} SET `name`='Martinique die Insel' WHERE `id`='630';
            UPDATE {{trpcountry}} SET `name`='Mexiko' WHERE `id`='631';
            UPDATE {{trpcountry}} SET `name`='Mosambik' WHERE `id`='632';
            UPDATE {{trpcountry}} SET `name`='Moldova' WHERE `id`='633';
            UPDATE {{trpcountry}} SET `name`='Monaco' WHERE `id`='634';
            UPDATE {{trpcountry}} SET `name`='Die Mongolei' WHERE `id`='635';
            UPDATE {{trpcountry}} SET `name`='Marokko' WHERE `id`='636';
            UPDATE {{trpcountry}} SET `name`='Myanmar (Birma)' WHERE `id`='637';
            UPDATE {{trpcountry}} SET `name`='Maine die Insel' WHERE `id`='638';
            UPDATE {{trpcountry}} SET `name`='Namibia' WHERE `id`='639';
            UPDATE {{trpcountry}} SET `name`='Nauru' WHERE `id`='640';
            UPDATE {{trpcountry}} SET `name`='Nepal' WHERE `id`='641';
            UPDATE {{trpcountry}} SET `name`='Der Niger' WHERE `id`='642';
            UPDATE {{trpcountry}} SET `name`='Nigeria' WHERE `id`='643';
            UPDATE {{trpcountry}} SET `name`='Die Niederlande (Holland)' WHERE `id`='644';
            UPDATE {{trpcountry}} SET `name`='Nicaragua' WHERE `id`='645';
            UPDATE {{trpcountry}} SET `name`='Neuseeland' WHERE `id`='646';
            UPDATE {{trpcountry}} SET `name`='Neukaledonien die Insel' WHERE `id`='647';
            UPDATE {{trpcountry}} SET `name`='Norwegen' WHERE `id`='648';
            UPDATE {{trpcountry}} SET `name`='Norfolk die Insel' WHERE `id`='649';
            UPDATE {{trpcountry}} SET `name`='DIE INSEL А.Э.' WHERE `id`='650';
            UPDATE {{trpcountry}} SET `name`='Oman' WHERE `id`='651';
            UPDATE {{trpcountry}} SET `name`='Pakistan' WHERE `id`='652';
            UPDATE {{trpcountry}} SET `name`='Panama' WHERE `id`='653';
            UPDATE {{trpcountry}} SET `name`='Папуа Neuguinea' WHERE `id`='654';
            UPDATE {{trpcountry}} SET `name`='Paraguay' WHERE `id`='655';
            UPDATE {{trpcountry}} SET `name`='Peru' WHERE `id`='656';
            UPDATE {{trpcountry}} SET `name`='Die Insel Pitkern' WHERE `id`='657';
            UPDATE {{trpcountry}} SET `name`='Polen' WHERE `id`='658';
            UPDATE {{trpcountry}} SET `name`='Portugal' WHERE `id`='659';
            UPDATE {{trpcountry}} SET `name`='Puerto Riko' WHERE `id`='660';
            UPDATE {{trpcountry}} SET `name`='Reunion' WHERE `id`='661';
            UPDATE {{trpcountry}} SET `name`='Ruanda' WHERE `id`='662';
            UPDATE {{trpcountry}} SET `name`='Rumänien' WHERE `id`='663';
            UPDATE {{trpcountry}} SET `name`='DIE USA' WHERE `id`='664';
            UPDATE {{trpcountry}} SET `name`='El Salvador' WHERE `id`='665';
            UPDATE {{trpcountry}} SET `name`='Die Samoainseln' WHERE `id`='666';
            UPDATE {{trpcountry}} SET `name`='San Marino' WHERE `id`='667';
            UPDATE {{trpcountry}} SET `name`='São Tomé und Príncipe' WHERE `id`='668';
            UPDATE {{trpcountry}} SET `name`='Saudi-Arabien' WHERE `id`='669';
            UPDATE {{trpcountry}} SET `name`='Swasiland' WHERE `id`='670';
            UPDATE {{trpcountry}} SET `name`='Heiliger Ljussija' WHERE `id`='671';
            UPDATE {{trpcountry}} SET `name`='Heiliger Jelena die Insel' WHERE `id`='672';
            UPDATE {{trpcountry}} SET `name`='Nordkorea' WHERE `id`='673';
            UPDATE {{trpcountry}} SET `name`='Сейшеллы' WHERE `id`='674';
            UPDATE {{trpcountry}} SET `name`='St. Pierre und Miquelon' WHERE `id`='675';
            UPDATE {{trpcountry}} SET `name`='Senegal' WHERE `id`='676';
            UPDATE {{trpcountry}} SET `name`='Sent Kits und Newis' WHERE `id`='677';
            UPDATE {{trpcountry}} SET `name`='Das Saint-Vincent und die Grenadinen' WHERE `id`='678';
            UPDATE {{trpcountry}} SET `name`='Serbien' WHERE `id`='679';
            UPDATE {{trpcountry}} SET `name`='Singapur' WHERE `id`='680';
            UPDATE {{trpcountry}} SET `name`='Syrien' WHERE `id`='681';
            UPDATE {{trpcountry}} SET `name`='Die Slowakei' WHERE `id`='682';
            UPDATE {{trpcountry}} SET `name`='Slowenien' WHERE `id`='683';
            UPDATE {{trpcountry}} SET `name`='Die Salomonen' WHERE `id`='684';
            UPDATE {{trpcountry}} SET `name`='Somalia' WHERE `id`='685';
            UPDATE {{trpcountry}} SET `name`='Der Sudan' WHERE `id`='686';
            UPDATE {{trpcountry}} SET `name`='Suriname' WHERE `id`='687';
            UPDATE {{trpcountry}} SET `name`='Sierra Leone' WHERE `id`='688';
            UPDATE {{trpcountry}} SET `name`='Tadschikistan' WHERE `id`='689';
            UPDATE {{trpcountry}} SET `name`='Taiwan' WHERE `id`='690';
            UPDATE {{trpcountry}} SET `name`='Thailand' WHERE `id`='691';
            UPDATE {{trpcountry}} SET `name`='Tansania' WHERE `id`='692';
            UPDATE {{trpcountry}} SET `name`='Togo' WHERE `id`='693';
            UPDATE {{trpcountry}} SET `name`='Tokelau die Inseln' WHERE `id`='694';
            UPDATE {{trpcountry}} SET `name`='Tonga' WHERE `id`='695';
            UPDATE {{trpcountry}} SET `name`='Trinidad und Tobago' WHERE `id`='696';
            UPDATE {{trpcountry}} SET `name`='Tuvalu' WHERE `id`='697';
            UPDATE {{trpcountry}} SET `name`='Tunesien' WHERE `id`='698';
            UPDATE {{trpcountry}} SET `name`='Turkmenistan' WHERE `id`='699';
            UPDATE {{trpcountry}} SET `name`='Туркс und Kejkos' WHERE `id`='700';
            UPDATE {{trpcountry}} SET `name`='Die Türkei' WHERE `id`='701';
            UPDATE {{trpcountry}} SET `name`='Uganda' WHERE `id`='702';
            UPDATE {{trpcountry}} SET `name`='Usbekistan' WHERE `id`='703';
            UPDATE {{trpcountry}} SET `name`='Uruguay' WHERE `id`='704';
            UPDATE {{trpcountry}} SET `name`='Die Faröerinseln' WHERE `id`='705';
            UPDATE {{trpcountry}} SET `name`='Fidschi' WHERE `id`='706';
            UPDATE {{trpcountry}} SET `name`='Die Philippinen' WHERE `id`='707';
            UPDATE {{trpcountry}} SET `name`='Finnland' WHERE `id`='708';
            UPDATE {{trpcountry}} SET `name`='Frankreich' WHERE `id`='709';
            UPDATE {{trpcountry}} SET `name`='Französisches Guinea' WHERE `id`='710';
            UPDATE {{trpcountry}} SET `name`='Französisch-Polynesien' WHERE `id`='711';
            UPDATE {{trpcountry}} SET `name`='Kroatien' WHERE `id`='712';
            UPDATE {{trpcountry}} SET `name`='Der Tschad' WHERE `id`='713';
            UPDATE {{trpcountry}} SET `name`='Montenegro' WHERE `id`='714';
            UPDATE {{trpcountry}} SET `name`='Tschechien' WHERE `id`='715';
            UPDATE {{trpcountry}} SET `name`='Chile' WHERE `id`='716';
            UPDATE {{trpcountry}} SET `name`='Die Schweiz' WHERE `id`='717';
            UPDATE {{trpcountry}} SET `name`='Schweden' WHERE `id`='718';
            UPDATE {{trpcountry}} SET `name`='Sri Lanka' WHERE `id`='719';
            UPDATE {{trpcountry}} SET `name`='Ecuador' WHERE `id`='720';
            UPDATE {{trpcountry}} SET `name`='Äquatorialguinea' WHERE `id`='721';
            UPDATE {{trpcountry}} SET `name`='Eritrea' WHERE `id`='722';
            UPDATE {{trpcountry}} SET `name`='Estland' WHERE `id`='723';
            UPDATE {{trpcountry}} SET `name`='Äthiopien' WHERE `id`='724';
            UPDATE {{trpcountry}} SET `name`='DIE REPUBLIK SÜDAFRIKA' WHERE `id`='725';
            UPDATE {{trpcountry}} SET `name`='Südkorea' WHERE `id`='726';
            UPDATE {{trpcountry}} SET `name`='Südossetien' WHERE `id`='727';
            UPDATE {{trpcountry}} SET `name`='Jamaika' WHERE `id`='728';
            UPDATE {{trpcountry}} SET `name`='Japan' WHERE `id`='729';
            UPDATE {{trpcountry}} SET `name`='Macao' WHERE `id`='730';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}