<?php

class m160630_110322_create_summary_table_for_region extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			CREATE TABLE {{regionsummary}} (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`regionId` INT(11) NOT NULL,
			`fairsCount` INT(11) NOT NULL,
			`datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`),
			UNIQUE INDEX `region_summary_uniq_regionid` (`regionId` ASC));
			
			UPDATE {{route}} SET `route`='search/regionSummaryByFairs', `params`='[]' WHERE `id`='93592';
			UPDATE {{route}} SET `route`='search/regionSummaryByFairs', `params`='[]' WHERE `id`='93627';

		";
	}

	public function downSql()
	{
		return "
		  DROP TABLE {{regionsummary}};
		";
	}
}