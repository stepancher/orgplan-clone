<?php

class m170118_072113_add_new_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('10', 'shestakovo');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('271', 'ru', 'район Бобровский, село Шестаково');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('271', 'en', 'Shestackovo');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('271', 'de', 'Shestackovo');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}