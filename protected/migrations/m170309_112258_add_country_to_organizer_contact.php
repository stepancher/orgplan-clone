<?php

class m170309_112258_add_country_to_organizer_contact extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `add_country_to_organizer_contact`;
            CREATE PROCEDURE `add_country_to_organizer_contact`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_cn_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT t.id, t.cnId FROM
                                        (SELECT ocl.id,
                                        CASE WHEN (SUBSTR(ocl.value, 1, 1) = '7' 
                                                    OR SUBSTR(ocl.value, 1, 2) = '+7' 
                                                    OR SUBSTR(ocl.value, 1, 3) = ' +7' 
                                                    OR SUBSTR(ocl.value, 1, 1) = '8'
                                                    OR SUBSTR(ocl.value, 1, 1) = '(') THEN 1
                                             WHEN (SUBSTR(ocl.value, 1, 4) = '+373' OR SUBSTR(ocl.value, 1, 3) = '373') THEN 8
                                             WHEN (SUBSTR(ocl.value, 1, 4) = '+374') THEN 4
                                             WHEN (SUBSTR(ocl.value, 1, 4) = '+375' OR SUBSTR(ocl.value, 1, 3) = '375') THEN 5
                                             WHEN (SUBSTR(ocl.value, 1, 3) = '+49' OR SUBSTR(ocl.value, 1, 2) = '49') THEN 2
                                             WHEN (SUBSTR(ocl.value, 1, 3) = '+44' OR SUBSTR(ocl.value, 1, 2) = '44') THEN 28
                                             WHEN (SUBSTR(ocl.value, 1, 4) = '+380' 
                                                    OR SUBSTR(ocl.value, 1, 6) = '+38 (0' 
                                                    OR SUBSTR(ocl.value, 1, 5) = '+38(0'
                                                    OR SUBSTR(ocl.value, 1, 4) = '38(0'
                                                    OR SUBSTR(ocl.value, 1, 3) = '380') THEN 13
                                             WHEN (SUBSTR(ocl.value, 1, 4) = '99(4') THEN 3
                                             WHEN (SUBSTR(ocl.value, 1, 4) = '99(6' OR SUBSTR(ocl.value, 1, 3) = '996') THEN 7
                                             WHEN (SUBSTR(ocl.value, 1, 3) = '998') THEN 9
                                            ELSE 1 END cnId
                                        FROM {{organizercontactlist}} ocl
                                        WHERE ocl.value NOT LIKE '%@%'
                                        AND ocl.value != ''
                                        ) t WHERE t.cnId IS NOT NULL
                                        ORDER BY t.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO c_id, c_cn_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{organizercontactlist}} SET `countryId` = c_cn_id WHERE `id` = c_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `add_country_to_organizer_contact`();
            DROP PROCEDURE IF EXISTS `add_country_to_organizer_contact`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}