<?php

class m170313_115007_separate_organizer_contact_double_phones extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{organizercontactlist}}
            DROP INDEX `orgIdvalueTypeAddinfo` ;

            DROP PROCEDURE IF EXISTS `separate_organizer_contact_double_phones`;
            CREATE PROCEDURE `separate_organizer_contact_double_phones`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_country_id INT DEFAULT 0;
                DECLARE c_value TEXT DEFAULT '';
                DECLARE c_organizer_id INT DEFAULT 0;
                DECLARE c_type INT DEFAULT 0;
                DECLARE c_additional_information TEXT DEFAULT '';
                DECLARE value_string TEXT DEFAULT '';
                DECLARE delim INT DEFAULT 0;
                DECLARE left_part TEXT DEFAULT '';
                DECLARE right_part TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ocl.id, ocl.organizerId, ocl.type, ocl.additionalContactInformation, countryId, ocl.value
                                        FROM {{organizercontactlist}} ocl
                                        WHERE ocl.value NOT LIKE '%@%'
                                        AND ocl.type != 2
                                        AND LOCATE(',', ocl.value) != 0;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO c_id, c_organizer_id, c_type, c_additional_information, c_country_id, c_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    SET value_string := c_value;
                    SET delim := LOCATE(',', value_string);
                    SET left_part := SUBSTR(value_string, 1, delim - 1);
                    SET right_part := SUBSTR(value_string, delim);
                    
                    START TRANSACTION;
                        DELETE FROM {{organizercontactlist}} WHERE `id` = c_id;
                    COMMIT;
                    
                    IF left_part != right_part THEN
                        START TRANSACTION;
                            INSERT INTO {{organizercontactlist}} (`organizerId`,`value`,`type`,`additionalContactInformation`,`countryId`) 
                            VALUES (c_organizer_id, left_part, c_type, c_additional_information, c_country_id);
                            
                            INSERT INTO {{organizercontactlist}} (`organizerId`,`value`,`type`,`additionalContactInformation`,`countryId`) 
                            VALUES (c_organizer_id, right_part, c_type, c_additional_information, c_country_id);
                        COMMIT;
                    END IF;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `separate_organizer_contact_double_phones`();
            DROP PROCEDURE IF EXISTS `separate_organizer_contact_double_phones`;
            
            DELETE FROM {{organizercontactlist}} WHERE `value` = '';
            DELETE FROM {{organizercontactlist}} WHERE `value` = ',';
            
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.6224' WHERE `id`='26812';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('947', 'Natalia.Kalmykova@ite-russia.ru', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='7(499)681-33-83', `additionalContactInformation`='доб.324' WHERE `id`='54880';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '1', '1');
            UPDATE {{organizercontactlist}} SET `value`='7(495)937-40-81', `additionalContactInformation`='доб.324' WHERE `id`='54879';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24830';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('779', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24827';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('779', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24824';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('779', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='26102';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('890', '7(926)527-44-31', '7', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24822';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('779', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='26099';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('890', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24819';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('778', '7(926)527-44-31', '7', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='26096';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('890', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24816';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('778', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='26093';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('890', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24813';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('778', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='26091';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('890', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24810';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('778', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24808';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('778', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `value`='7(499)681-33-83', `additionalContactInformation`='доб.324' WHERE `id`='54884';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('778', '7(919)236-27-78', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='54883';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='7(499)681-33-83', `additionalContactInformation`='доб.324' WHERE `id`='54882';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('681', '7(919)236-27-78', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.324' WHERE `id`='54881';
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24889';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('783', '7(926)527-44-31', '7', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24886';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('783', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24883';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('783', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24880';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('783', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24878';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('783', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24875';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('782', '7(926)527-44-31', '7', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24872';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('782', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24869';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('782', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24866';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('782', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24864';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('782', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24861';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('781', '7(926)527-44-31', '7', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24858';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('781', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24855';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('781', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24852';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('781', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24850';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('781', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24847';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('780', '7(926)527-44-31', '7', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24844';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('780', '7(926)527-44-31', '6', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24841';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('780', '7(926)527-44-31', '5', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24838';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('780', '7(926)527-44-31', '4', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24836';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('780', '7(926)527-44-31', '1', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.287' WHERE `id`='24833';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('779', '7(926)527-44-31', '7', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3065', '7(499)750-08-28', '1', 'доб.6235', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.6233' WHERE `id`='43720';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3716', '7(495)935-87-07', '1', 'доб.110', '1');
            UPDATE {{organizercontactlist}} SET `additionalContactInformation`='доб.109' WHERE `id`='49020';
            UPDATE {{organizercontactlist}} SET `value`='(495)9214407', `additionalContactInformation`='доб.133, ПетроваЭмилия', `countryId`='1' WHERE `id`='46193';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('3384', 'v.kosikhin@rte-expo.ru', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='+7 (906) 182-31-48', `countryId`='1' WHERE `id`='54401';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', 'maria@ekspolider.ru', '7', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', '+7 (960) 455-37-39', '7', '1');
            UPDATE {{organizercontactlist}} SET `value`='+7 (906) 182-31-48', `countryId`='1' WHERE `id`='54400';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', 'maria@ekspolider.ru', '6', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', '+7 (960) 455-37-39', '6', '1');
            UPDATE {{organizercontactlist}} SET `value`='+7 (906) 182-31-48', `countryId`='1' WHERE `id`='54399';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', 'maria@ekspolider.ru', '4', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', '+7 (960) 455-37-39', '4', '1');
            UPDATE {{organizercontactlist}} SET `value`='+7 (906) 182-31-48', `countryId`='1' WHERE `id`='54397';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', 'maria@ekspolider.ru', '3', '1');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `countryId`) VALUES ('4135', '+7 (960) 455-37-39', '3', '1');
            UPDATE {{organizercontactlist}} SET `value`='ShapkinaE@messe-duesseldorf.ru' WHERE `id`='29169';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('1222', '7(495)955-91-99', '2', 'доб.636');
            UPDATE {{organizercontactlist}} SET `value`='+99 (412) 447-47-74' WHERE `id`='54374';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4131', 'telecoms@ceo.az', '5');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4131', '+99 (455) 400-05-77', '5');
            UPDATE {{organizercontactlist}} SET `value`='+99 (412) 447-47-74' WHERE `id`='54373';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4131', 'telecoms@ceo.az', '4');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4131', '+99 (455) 400-05-77', '4');
            UPDATE {{organizercontactlist}} SET `value`='+99 (412) 447-47-74' WHERE `id`='54372';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4131', 'telecoms@ceo.az', '3');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4131', '+99 (455) 400-05-77', '3');
            UPDATE {{organizercontactlist}} SET `value`='irina.troitskaya@messe-russia.ru' WHERE `id`='27977';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1097', '74959214407', '2', 'доб.133', '1');
            UPDATE {{organizercontactlist}} SET `value`='olga.egorova@ite-russia.ru' WHERE `id`='45907';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3349', '7(495)974-34-05', '2', 'доб.3829', '1');
            UPDATE {{organizercontactlist}} SET `value`='bordachev@ite-expo.ru' WHERE `id`='28376';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1142', '7(495)9358100', '2', 'доб.6249', '1');
            UPDATE {{organizercontactlist}} SET `value`='o.borzova@rte-expo.ru' WHERE `id`='22989';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('612', '7(495)789-49-01', '2', 'доб.133', '1');
            UPDATE {{organizercontactlist}} SET `value`='kashirina@krasfair.ru' WHERE `id`='27472';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1040', '7(391)228-85-13', '2', 'доб.331', '1');
            UPDATE {{organizercontactlist}} SET `value`='Gazaryan@ite-expo.ru' WHERE `id`='47887';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('3603', '7(495)9358100', '2', 'доб.6249', '1');
            UPDATE {{organizercontactlist}} SET `value`='ShapkinaE@messe-duesseldorf.ru', `additionalContactInformation`=NULL WHERE `id`='29175';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`, `countryId`) VALUES ('1222', '7(495)955-91-99', '5', 'доб.636', '1');
            UPDATE {{organizercontactlist}} SET `value`='ShapkinaE@messe-duesseldorf.ru', `additionalContactInformation`=NULL WHERE `id`='29172';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('1222', '7(495)955-91-99', '4', 'доб.636');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2266', '7(812)320-80-15', '2', 'доб.7310');
            UPDATE {{organizercontactlist}} SET `value`='mtishkova@spe.org' WHERE `id`='53959';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4105', '+7(495)268-04-54', '2', 'доб.103');
            UPDATE {{organizercontactlist}} SET `value`='mirauto@farexpo.ru' WHERE `id`='38296';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2189', '7(812)777-04-07', '2', 'доб.633');
            UPDATE {{organizercontactlist}} SET `value`='ky@bd-event.ru' WHERE `id`='53951';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4104', '+7(495)988-28-01', '2', 'доб.70201');
            UPDATE {{organizercontactlist}} SET `value`='nafar@farexpo.ru' WHERE `id`='38288';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2187', '7(812)777-04-07', '2', 'доб.633');
            UPDATE {{organizercontactlist}} SET `value`='pvo_74@mail.ru' WHERE `id`='22351';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('560', '7(351)755-55-10', '2', 'доб.116');
            UPDATE {{organizercontactlist}} SET `value`='+7 (861) 299-58-46' WHERE `id`='54394';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4134', 'figner@cityexpo23.ru', '7');
            UPDATE {{organizercontactlist}} SET `value`='+7 (861) 299-58-46' WHERE `id`='54393';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4134', 'figner@cityexpo23.ru', '6');
            UPDATE {{organizercontactlist}} SET `value`='+7 (861) 299-58-46' WHERE `id`='54392';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4134', 'figner@cityexpo23.ru', '5');
            UPDATE {{organizercontactlist}} SET `value`='+7 (861) 299-58-46' WHERE `id`='54391';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4134', 'figner@cityexpo23.ru', '4');
            UPDATE {{organizercontactlist}} SET `value`='+7 (861) 299-58-46' WHERE `id`='54390';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4134', 'figner@cityexpo23.ru', '3');
            UPDATE {{organizercontactlist}} SET `value`='reclama@sochi-expo.ru', `additionalContactInformation`=NULL WHERE `id`='22987';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('610', '7(495)789-49-01', '5', 'доб.135');
            UPDATE {{organizercontactlist}} SET `value`='+7 (906) 420-57-76' WHERE `id`='54402';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`) VALUES ('4135', 'expo_gold@mail.ru ', '5');
            UPDATE {{organizercontactlist}} SET `value`='woodex@ite-expo.ru', `additionalContactInformation`=NULL WHERE `id`='47606';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('3568', '7(499)750-08-28', '4', 'доб.6249');
            UPDATE {{organizercontactlist}} SET `value`='mtishkova@spe.org', `additionalContactInformation`=NULL WHERE `id`='53965';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4105', '+7(495)268-04-54', '6', 'доб.103');
            UPDATE {{organizercontactlist}} SET `value`='mtishkova@spe.org', `additionalContactInformation`=NULL WHERE `id`='53963';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4105', '+7(495)268-04-54', '4', 'доб.103');
            UPDATE {{organizercontactlist}} SET `value`='mtishkova@spe.org', `additionalContactInformation`=NULL WHERE `id`='53961';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4105', '+7(495)268-04-54', '3', 'доб.103');
            UPDATE {{organizercontactlist}} SET `value`='mtishkova@spe.org', `additionalContactInformation`=NULL WHERE `id`='38832';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2266', '7(812)320-80-15', '6', 'доб.7310');
            UPDATE {{organizercontactlist}} SET `value`='offshore@restec.ru', `additionalContactInformation`=NULL WHERE `id`='38830';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2266', '7(812)320-80-15', '4', 'доб.7310');
            UPDATE {{organizercontactlist}} SET `value`='foodind@restec.ru', `additionalContactInformation`=NULL WHERE `id`='38826';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2265', '7(812)320-80-15', '7', 'доб.7304');
            UPDATE {{organizercontactlist}} SET `value`='ky@bd-event.ru', `additionalContactInformation`=NULL WHERE `id`='53957';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4104', '+7(495)268-04-54', '7', 'доб.102');
            UPDATE {{organizercontactlist}} SET `value`='ky@bd-event.ru', `additionalContactInformation`=NULL WHERE `id`='53955';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4104', '+7(495)988-28-01', '6', 'доб.70201');
            UPDATE {{organizercontactlist}} SET `value`='ky@bd-event.ru', `additionalContactInformation`=NULL WHERE `id`='53953';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('4104', '+7(495)988-28-01', '3', 'доб.70201');
            UPDATE {{organizercontactlist}} SET `value`='7745541@list.ru', `additionalContactInformation`=NULL WHERE `id`='40993';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('2631', '7(351)755-55-10', '7', 'доб.101');
            UPDATE {{organizercontactlist}} SET `value`='olga@unexpo.ru', `additionalContactInformation`=NULL WHERE `id`='26860';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('953', '7(499)681-33-83', '7', 'доб.324');
            UPDATE {{organizercontactlist}} SET `value`='port@restec.ru, ', `additionalContactInformation`=NULL WHERE `id`='29411';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `value`, `type`, `additionalContactInformation`) VALUES ('1251', '7(812)320-6363', '4', 'доб.7251');

            DROP PROCEDURE IF EXISTS `delete_first_comma`;
            CREATE PROCEDURE `delete_first_comma`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_new_value TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ocl.id, REPLACE(ocl.value, ',', '') AS newValue
                                        FROM {{organizercontactlist}} ocl
                                        WHERE ocl.type != 2
                                        AND ocl.value NOT LIKE '%@%'
                                        AND SUBSTR(ocl.value, 1, 1) = ','
                                        ORDER BY ocl.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO c_id, c_new_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{organizercontactlist}} SET `value` = c_new_value WHERE `id` = c_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            CALL `delete_first_comma`();
            DROP PROCEDURE IF EXISTS `delete_first_comma`;
    
            DROP PROCEDURE IF EXISTS `delete_spaces`;
            CREATE PROCEDURE `delete_spaces`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE c_id INT DEFAULT 0;
                DECLARE c_new_value TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ocl.id, REPLACE(ocl.value, ' ', '') AS newValue
                                        FROM {{organizercontactlist}} ocl
                                        WHERE ocl.type != 2
                                        AND ocl.value NOT LIKE '%@%'
                                        AND LOCATE(' ', ocl.value) != 0
                                        ORDER BY ocl.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO c_id, c_new_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{organizercontactlist}} SET `value` = c_new_value WHERE `id` = c_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            CALL `delete_spaces`();
            DROP PROCEDURE IF EXISTS `delete_spaces`;
            
            UPDATE {{organizercontactlist}} SET `value`='7(347)253-75-00', `additionalContactInformation`='АзнабаевБуранбайРустамович' WHERE `id`='46812';
            UPDATE {{organizercontactlist}} SET `value`='7(347)253-79-57', `additionalContactInformation`='КнязеваВикторияСергеевна' WHERE `id`='46813';
            UPDATE {{organizercontactlist}} SET `value`='+7(495)727-2524', `additionalContactInformation`='Сервис-центрIIIпавильона' WHERE `id`='48854';
            UPDATE {{organizercontactlist}} SET `value`='+7(495)727-1138', `additionalContactInformation`='Сервис-центрIIпавильона' WHERE `id`='48852';
            UPDATE {{organizercontactlist}} SET `value`='+7(495)727-2626', `additionalContactInformation`='Сервис-центрIпавильона:' WHERE `id`='48850';
            UPDATE {{organizercontactlist}} SET `value`='(495)9214407', `additionalContactInformation`='доб.133 ПетроваЭмилия' WHERE `id`='46196';
            UPDATE {{organizercontactlist}} SET `value`='+998(71)238-59-87' WHERE `id`='52361';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3985', '1', '7', '+998(93)381-07-81');
            UPDATE {{organizercontactlist}} SET `value`='+998(71)238-59-87' WHERE `id`='52358';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3985', '1', '6', '+998(93)381-07-81');
            UPDATE {{organizercontactlist}} SET `value`='+998(71)238-59-87' WHERE `id`='52355';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3985', '1', '5', '+998(93)381-07-81');
            UPDATE {{organizercontactlist}} SET `value`='+998(71)238-59-87' WHERE `id`='52352';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3985', '1', '4', '+998(93)381-07-81');
            UPDATE {{organizercontactlist}} SET `value`='+998(71)238-59-87' WHERE `id`='52349';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3985', '1', '3', '+998(93)381-07-81');
            UPDATE {{organizercontactlist}} SET `value`='+998(71)238-59-87' WHERE `id`='52346';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3985', '1', '1', '+998(93)381-07-81');
            UPDATE {{organizercontactlist}} SET `value`='7(499)681-33-83', `additionalContactInformation`='доб.324' WHERE `id`='54892';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('681', '1', '1', '7(919)236-27-78');
            UPDATE {{organizercontactlist}} SET `value`='7(812)596-38-64' WHERE `id`='38128';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('2169', '1', '7', '7(812)324-64-16');
            UPDATE {{organizercontactlist}} SET `value`='7(812)596-38-64' WHERE `id`='38125';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('2169', '1', '6', '7(812)324-64-16');
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('2169', '1', '1', '7(812)324-64-16');
            UPDATE {{organizercontactlist}} SET `value`='7(812)596-38-64' WHERE `id`='38116';
            UPDATE {{organizercontactlist}} SET `value`='7(4012)34-11-06' WHERE `id`='47301';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3531', '1', '7', '7(4012)34-10-01');
            UPDATE {{organizercontactlist}} SET `value`='7(4012)34-11-06' WHERE `id`='47299';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3531', '1', '6', '7(4012)34-10-01');
            UPDATE {{organizercontactlist}} SET `value`='7(4012)34-11-06' WHERE `id`='47297';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3531', '1', '5', '7(4012)34-10-01');
            UPDATE {{organizercontactlist}} SET `value`='7(4012)34-11-06' WHERE `id`='47295';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3531', '1', '4', '7(4012)34-10-01');
            UPDATE {{organizercontactlist}} SET `value`='7(351)230-45-70' WHERE `id`='34010';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('1761', '1', '1', '7(351)200-34-52');
            UPDATE {{organizercontactlist}} SET `value`='7(347)246-42-02' WHERE `id`='48144';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('3633', '1', '1', '7(347)246-42-19');
            UPDATE {{organizercontactlist}} SET `value`='7(495)937-40-81', `additionalContactInformation`='доб.324' WHERE `id`='54891';
            INSERT INTO {{organizercontactlist}} (`organizerId`, `countryId`, `type`, `value`) VALUES ('681', '1', '1', '7(919)236-27-78');

            DELETE FROM {{organizercontactlist}} WHERE `id` = '37683';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '37688';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '37707';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '37711';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '42026';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '42030';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '42492';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '42497';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '42513';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '42519';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '48283';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '48284';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '48430';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '48434';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '49034';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '49038';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '49297';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '49298';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}