<?php

class m161103_125931_add_infoid_to_fair extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `add_info_id_to_fair`()
            BEGIN
            
                DECLARE done BOOL DEFAULT FALSE;
                
                DECLARE fairInfoId INT DEFAULT NULL;
                DECLARE fairId INT DEFAULT NULL;
                
                DECLARE copy CURSOR FOR SELECT 
                        fi.id AS fairInfoId,
                        f.id AS fairId
                    FROM {{fair}} f
                    JOIN {{fairinfo}} fi ON fi.fairId = f.id;
                    
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    FETCH copy INTO 
                        fairInfoId,
                        fairId;
                
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{fair}} SET `infoId` = fairInfoId WHERE `id` = fairId;
                    COMMIT;
                    
                    END LOOP;
                CLOSE copy;
            END;
    
    CALL add_info_id_to_fair();
    DROP PROCEDURE IF EXISTS `add_info_id_to_fair`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}