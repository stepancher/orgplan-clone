<?php

class m160624_100605_create_12_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{proposalelementclass}} (`id`, `name`, `fixed`, `fairId`, `active`) VALUES (NULL, 'zayavka_n12', '0', '184', '0');
			INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('22', 'ru', 'Заявка №12', 'БЕЙДЖИ УЧАСТНИКОВ');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12_hint', 'int', '0', '0', '2304', '10', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12_company', 'string', '0', '0', '0', '2305', '10', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12_company_ru', 'string', '0', '0', '2305', '2305', '10', 'text', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12_company_en', 'string', '0', '0', '2305', '2305', '10', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12_super1', 'string', '0', NULL, '0', '2308', '10', NULL, '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge1', 'string', '0', NULL, '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge1_name', 'string', '0', NULL, '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12_badge1_surname', 'string', '0', NULL, '2308', '2308', '40', 'text', NULL);
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12_badge1_country', 'int', '0', NULL, '2308', '2308', '50', 'dropDownCountry', NULL);
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge1_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge1_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge1_country_en', 'int', '0', NULL, '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super2', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge2_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super3', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge3_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super4', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge4_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super5', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge5_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super6', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge6_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super7', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge7_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super8', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge8_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super9', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge9_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `collapse`) VALUES ('n12_super10', 'string', '0', '0', '2308', '10', '0');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10', 'string', '0', '2308', '2308', '20', 'headerH1WithWrap');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10_name', 'string', '0', '2308', '2308', '30', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10_surname', 'string', '0', '2308', '2308', '40', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10_country', 'int', '0', '2308', '2308', '50', 'dropDownCountry');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10_name_en', 'string', '0', '2308', '2308', '60', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10_surname_en', 'string', '0', '2308', '2308', '70', 'text');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`) VALUES ('n12_badge10_country_en', 'int', '0', '2308', '2308', '80', 'dropDownCountry');
			DELETE FROM {{attributeclass}} WHERE `id`='2307';
			DELETE FROM {{attributeclass}} WHERE `id`='2313';
			DELETE FROM {{attributeclass}} WHERE `id`='2314';
			DELETE FROM {{attributeclass}} WHERE `id`='2315';
			DELETE FROM {{attributeclass}} WHERE `id`='2321';
			DELETE FROM {{attributeclass}} WHERE `id`='2322';
			DELETE FROM {{attributeclass}} WHERE `id`='2323';
			DELETE FROM {{attributeclass}} WHERE `id`='2329';
			DELETE FROM {{attributeclass}} WHERE `id`='2330';
			DELETE FROM {{attributeclass}} WHERE `id`='2331';
			DELETE FROM {{attributeclass}} WHERE `id`='2337';
			DELETE FROM {{attributeclass}} WHERE `id`='2338';
			DELETE FROM {{attributeclass}} WHERE `id`='2339';
			DELETE FROM {{attributeclass}} WHERE `id`='2345';
			DELETE FROM {{attributeclass}} WHERE `id`='2346';
			DELETE FROM {{attributeclass}} WHERE `id`='2347';
			DELETE FROM {{attributeclass}} WHERE `id`='2353';
			DELETE FROM {{attributeclass}} WHERE `id`='2354';
			DELETE FROM {{attributeclass}} WHERE `id`='2355';
			DELETE FROM {{attributeclass}} WHERE `id`='2361';
			DELETE FROM {{attributeclass}} WHERE `id`='2362';
			DELETE FROM {{attributeclass}} WHERE `id`='2363';
			DELETE FROM {{attributeclass}} WHERE `id`='2369';
			DELETE FROM {{attributeclass}} WHERE `id`='2370';
			DELETE FROM {{attributeclass}} WHERE `id`='2371';
			DELETE FROM {{attributeclass}} WHERE `id`='2377';
			DELETE FROM {{attributeclass}} WHERE `id`='2378';
			DELETE FROM {{attributeclass}} WHERE `id`='2379';
			DELETE FROM {{attributeclass}} WHERE `id`='2385';
			DELETE FROM {{attributeclass}} WHERE `id`='2386';
			DELETE FROM {{attributeclass}} WHERE `id`='2387';
			UPDATE {{attributeclass}} SET `super`='2380' WHERE `id`='2380';
			UPDATE {{attributeclass}} SET `parent`='2380', `super`='2380' WHERE `id`='2381';
			UPDATE {{attributeclass}} SET `parent`='2381', `super`='2380' WHERE `id`='2382';
			UPDATE {{attributeclass}} SET `parent`='2381', `super`='2380' WHERE `id`='2383';
			UPDATE {{attributeclass}} SET `parent`='2381', `super`='2380' WHERE `id`='2384';
			UPDATE {{attributeclass}} SET `super`='2372' WHERE `id`='2372';
			UPDATE {{attributeclass}} SET `parent`='2372', `super`='2372' WHERE `id`='2373';
			UPDATE {{attributeclass}} SET `parent`='2373', `super`='2372' WHERE `id`='2374';
			UPDATE {{attributeclass}} SET `parent`='2373', `super`='2372' WHERE `id`='2375';
			UPDATE {{attributeclass}} SET `parent`='2373', `super`='2372' WHERE `id`='2376';
			UPDATE {{attributeclass}} SET `super`='2364' WHERE `id`='2364';
			UPDATE {{attributeclass}} SET `parent`='2364', `super`='2364' WHERE `id`='2365';
			UPDATE {{attributeclass}} SET `parent`='2365', `super`='2364' WHERE `id`='2366';
			UPDATE {{attributeclass}} SET `parent`='2365', `super`='2364' WHERE `id`='2367';
			UPDATE {{attributeclass}} SET `parent`='2365', `super`='2364' WHERE `id`='2368';
			UPDATE {{attributeclass}} SET `super`='2356' WHERE `id`='2356';
			UPDATE {{attributeclass}} SET `parent`='2356', `super`='2356' WHERE `id`='2357';
			UPDATE {{attributeclass}} SET `parent`='2357', `super`='2356' WHERE `id`='2358';
			UPDATE {{attributeclass}} SET `parent`='2357', `super`='2356' WHERE `id`='2359';
			UPDATE {{attributeclass}} SET `parent`='2357', `super`='2356' WHERE `id`='2360';
			UPDATE {{attributeclass}} SET `super`='2348' WHERE `id`='2348';
			UPDATE {{attributeclass}} SET `parent`='2348', `super`='2348' WHERE `id`='2349';
			UPDATE {{attributeclass}} SET `parent`='2349', `super`='2348' WHERE `id`='2350';
			UPDATE {{attributeclass}} SET `parent`='2349', `super`='2348' WHERE `id`='2351';
			UPDATE {{attributeclass}} SET `parent`='2349', `super`='2348' WHERE `id`='2352';
			UPDATE {{attributeclass}} SET `super`='2340' WHERE `id`='2340';
			UPDATE {{attributeclass}} SET `parent`='2340', `super`='2340' WHERE `id`='2341';
			UPDATE {{attributeclass}} SET `parent`='2341', `super`='2340' WHERE `id`='2342';
			UPDATE {{attributeclass}} SET `parent`='2341', `super`='2340' WHERE `id`='2343';
			UPDATE {{attributeclass}} SET `parent`='2341', `super`='2340' WHERE `id`='2344';
			UPDATE {{attributeclass}} SET `super`='2332' WHERE `id`='2332';
			UPDATE {{attributeclass}} SET `parent`='2332', `super`='2332' WHERE `id`='2333';
			UPDATE {{attributeclass}} SET `parent`='2333', `super`='2332' WHERE `id`='2334';
			UPDATE {{attributeclass}} SET `parent`='2333', `super`='2332' WHERE `id`='2335';
			UPDATE {{attributeclass}} SET `parent`='2333', `super`='2332' WHERE `id`='2336';
			UPDATE {{attributeclass}} SET `super`='2324' WHERE `id`='2324';
			UPDATE {{attributeclass}} SET `parent`='2324', `super`='2324' WHERE `id`='2325';
			UPDATE {{attributeclass}} SET `parent`='2325', `super`='2324' WHERE `id`='2326';
			UPDATE {{attributeclass}} SET `parent`='2325', `super`='2324' WHERE `id`='2327';
			UPDATE {{attributeclass}} SET `parent`='2325', `super`='2324' WHERE `id`='2328';
			UPDATE {{attributeclass}} SET `super`='2316' WHERE `id`='2316';
			UPDATE {{attributeclass}} SET `parent`='2316', `super`='2316' WHERE `id`='2317';
			UPDATE {{attributeclass}} SET `parent`='2317', `super`='2316' WHERE `id`='2318';
			UPDATE {{attributeclass}} SET `parent`='2317', `super`='2316' WHERE `id`='2319';
			UPDATE {{attributeclass}} SET `parent`='2317', `super`='2316' WHERE `id`='2320';
			UPDATE {{attributeclass}} SET `parent`='2309' WHERE `id`='2310';
			UPDATE {{attributeclass}} SET `parent`='2309' WHERE `id`='2311';
			UPDATE {{attributeclass}} SET `parent`='2309' WHERE `id`='2312';
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2308', '0', '0', '30');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2316', '0', '0', '40');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2324', '0', '0', '50');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2332', '0', '0', '60');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2340', '0', '0', '70');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2348', '0', '0', '80');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2356', '0', '0', '90');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2364', '0', '0', '100');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2372', '0', '0', '110');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2381', '0', '0', '120');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2304', '0', '0', '10');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('22', '2305', '0', '0', '20');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2304', 'ru', 'Отправьте заполенную заявку в дирекцию выставки не позднее 15 сентября 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2305', 'ru', 'Название компании');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2309', 'ru', '1. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2310', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2311', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2312', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2317', 'ru', '2. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2318', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2319', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2320', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2325', 'ru', '3. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2326', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2327', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2328', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2333', 'ru', '4. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2334', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2335', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2336', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2341', 'ru', '5. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2342', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2343', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2344', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2349', 'ru', '6. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2350', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2351', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2352', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2357', 'ru', '7. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2358', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2359', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2360', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2365', 'ru', '8. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2366', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2367', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2368', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2373', 'ru', '9. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2374', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2375', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2376', 'ru', 'Страна');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2381', 'ru', '10. Бейдж');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2382', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2383', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2384', 'ru', 'Страна');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('n12_hint_last', 'int', '0', '0', '2385', '10', 'hintWithView', '0');
			UPDATE {{attributeclass}} SET `super`='2388' WHERE `id`='2388';
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('21', '2388', '0', '0', '90');
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2388', 'ru', 'Количество бейджей, предоставляемых Экспоненту зависит от площади его стенда и определяется следующим образом:<br> Стенд 9м² - 3 шт. <br> Стенд от 9м² до 30м² - 10 шт. <br> Стенд более 100м² - 20 шт. + 2 бейджа на каждые дополнительные 100м² площади стенда.');
			UPDATE {{attributemodel}} SET `elementClass`='22' WHERE `id`='211';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2309';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2317';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2325';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2333';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2341';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2349';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2357';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2365';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2373';
			UPDATE {{attributeclass}} SET `isArray`='1' WHERE `id`='2381';
			UPDATE {{attributemodel}} SET `priority`='130' WHERE `id`='211';
			UPDATE {{attributemodel}} SET `attributeClass`='2380' WHERE `id`='208';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n12_agreement', 'bool', '0', '2388', '2388', '20', 'checkboxAgreement', '0');

";
	}
}