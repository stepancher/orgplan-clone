<?php

class m170214_130237_move_exdb_venues_copy_procedure_to_orgplan_expodata extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            CREATE SCHEMA IF NOT EXISTS `orgplan_expodata` ;
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbvenue}}
            SELECT * FROM {$db}.{{exdbvenue}};
            DROP TABLE IF EXISTS {$db}.{{exdbvenue}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbtrvenue}}
            SELECT * FROM {$db}.{{exdbtrvenue}};
            DROP TABLE IF EXISTS {$db}.{{exdbtrvenue}};
            
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_venues_from_exdb_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_venues_from_exdb_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_city_id INT DEFAULT 0;
                DECLARE exdb_coordinates VARCHAR(55) DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT exdbv.id, exdbtrv.name, exdbtrv.langId, exdbv.cityId, exdbv.coordinates
                                        FROM orgplan_expodata.{{exdbvenue}} exdbv
                                            LEFT JOIN orgplan_expodata.{{exdbtrvenue}} exdbtrv on exdbtrv.trParentId = exdbv.id
                                            LEFT JOIN orgplan.{{trexhibitioncomplex}} trec ON trec.name = exdbtrv.name AND trec.langId = exdbtrv.langId
                                            LEFT JOIN orgplan.{{exhibitioncomplex}} ec ON ec.id = trec.trParentId AND ec.cityId = exdbv.cityId
                                        WHERE ec.id IS NULL AND trec.id IS NULL
                                        AND exdbtrv.name IS NOT NULL
                                        AND exdbtrv.name != ''
                                        GROUP BY exdbv.id ORDER BY exdbv.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    
                    FETCH copy INTO exdb_id, exdb_name, exdb_lang_id, exdb_city_id, exdb_coordinates;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        IF temp_id != exdb_id THEN
                            INSERT INTO orgplan.{{exhibitioncomplex}} (`cityId`,`coordinates`) VALUES (exdb_city_id, exdb_coordinates);
                            SET @last_insert_id := LAST_INSERT_ID();
                            SET temp_id := exdb_id;
                        END IF;
                        
                        INSERT INTO orgplan.{{trexhibitioncomplex}} (`name`,`trParentId`,`langId`) VALUES (exdb_name, @last_insert_id, exdb_lang_id);
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}