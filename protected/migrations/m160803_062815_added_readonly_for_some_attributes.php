<?php

class m160803_062815_added_readonly_for_some_attributes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2597', '1', 'readonly');
 			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2598', '1', 'readonly');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}