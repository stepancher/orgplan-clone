<?php

class m160427_113327_dependencies_for_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `parent`='594' WHERE `id`='606';
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `value`, `class`) VALUES (NULL, '606', '1', 'selfCollapse');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `value`, `class`) VALUES (NULL, '606', 'Такелажник, стропальщик', 'selfCollapseLabel');
			UPDATE {{attributeclass}} SET `label`='Такелажник, стропальщик' WHERE `id`='639';
			UPDATE {{attributeclassdependency}} SET `attributeClass`='606', `attributeClassRelated`='604' WHERE `id`='708';
			UPDATE {{attributeclassdependency}} SET `attributeClassRelated`='602' WHERE `id`='708';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('606', '604', 'summary', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('606', '602', 'round', '{\"min\": 2}');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('606', '604', 'round', '{\"min\": 2}');
			DELETE FROM {{attributeclassdependency}} WHERE `id`='710';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='711';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('640', '638', 'summary', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('640', '636', 'summary', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('753', '978', 'get', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('752', '1020', 'get', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('743', '1018', 'get', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('744', '976', 'get', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('602', '752', 'summaryAndRound', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('602', '743', 'summaryAndRound', '0');
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"min\": 2}' WHERE `id`='719';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"min\": 2}' WHERE `id`='718';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='753';
			UPDATE {{attributeclassdependency}} SET `function`='summary', `functionArgs`='' WHERE `id`='709';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"min\": 2}' WHERE `id`='717';



	    ";
	}
}