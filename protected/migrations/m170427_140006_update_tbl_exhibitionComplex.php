<?php

class m170427_140006_update_tbl_exhibitionComplex extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE tbl_exhibitioncomplex SET active = 0 WHERE id IN (
                SELECT id FROM (
                  SELECT DISTINCT ec.id 
                    FROM tbl_exhibitioncomplex ec
                    LEFT JOIN tbl_fair f ON f.exhibitionComplexId = ec.id
                    LEFT JOIN tbl_city c ON c.id = ec.cityId
                    LEFT JOIN tbl_region r ON r.id = c.regionId
                    LEFT JOIN tbl_district d ON d.id = r.districtId
                    LEFT JOIN tbl_country cn ON cn.id = d.countryId
                    LEFT JOIN tbl_trcountry trcn ON trcn.trParentId = cn.id
                    WHERE f.active IS NULL AND f.id IS NOT NULL AND ec.active = 1 AND trcn.langId = 'ru'
                ) ec2
            );
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}