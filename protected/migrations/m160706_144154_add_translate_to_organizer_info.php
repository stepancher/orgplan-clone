<?php

class m160706_144154_add_translate_to_organizer_info extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return '
			ALTER TABLE {{organizerinfo}}
			DROP COLUMN `signRulesPosition`,
			DROP COLUMN `signRulesName`,
			DROP COLUMN `requisitesBank`,
			DROP COLUMN `legalStreet`,
			DROP COLUMN `legalCity`,
			DROP COLUMN `legalRegion`,
			DROP COLUMN `legalCountry`,
			DROP COLUMN `postPhone`,
			DROP COLUMN `postStreet`,
			DROP COLUMN `postCity`,
			DROP COLUMN `postRegion`,
			DROP COLUMN `postCountry`,
			DROP COLUMN `fullName`,
			DROP COLUMN `name`;

		';
	}
}