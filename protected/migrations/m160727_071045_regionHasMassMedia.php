<?php

class m160727_071045_regionHasMassMedia extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			CREATE TABLE {{regionhasmassmedia}} (
			  `id` INT(11) NOT NULL AUTO_INCREMENT,
			  `regionId` INT(11) NULL DEFAULT NULL,
			  `massMediaId` INT(11) NULL DEFAULT NULL,
			  `isGeneral` TINYINT(4) NULL DEFAULT NULL,
			  PRIMARY KEY (`id`));

			ALTER TABLE {{regionhasmassmedia}} 
			ADD UNIQUE INDEX `region_massmedia` (`regionId` ASC, `massMediaId` ASC);
			
			ALTER TABLE {{region}} 
			ADD COLUMN `sponsorPos` VARCHAR(9999) NULL DEFAULT NULL AFTER `shortUrl`;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}