<?php

class m160924_073651_gradoteka_limit_objects extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }




    public function upSql()
    {
        return "
            ALTER TABLE {{gobjects}} 
            ADD COLUMN `visible` INT(11) NULL DEFAULT NULL AFTER `gId`;
            
            UPDATE {{gobjects}} SET `visible` = 1;
            
            UPDATE {{gobjects}} SET `visible`=NULL WHERE `id`='378';
            UPDATE {{gobjects}} SET `visible`=NULL WHERE `id`='336';

            UPDATE {{gstattypes}} SET `unit` = '%' WHERE `unit` LIKE '%процент%';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}