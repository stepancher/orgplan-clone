<?php

class m160412_232726_alter_table_attribute_class_dependency_add_column_auto extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			ALTER TABLE {{attributeclassdependency}}
			DROP COLUMN `auto`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{attributeclassdependency}}
			ADD COLUMN `auto` TINYINT NOT NULL DEFAULT 0 AFTER `functionArgs`;

			UPDATE {{attributeclassdependency}} SET `auto`='1' WHERE `id`='31';
			UPDATE {{attributeclassdependency}} SET `auto`='1' WHERE `id`='47';
			UPDATE {{attributeclassdependency}} SET `auto`='1' WHERE `id`='62';
			UPDATE {{attributeclassdependency}} SET `auto`='1' WHERE `id`='65';
			UPDATE {{attributeclassdependency}} SET `auto`='1' WHERE `id`='641';
		";
	}
}