<?php

class m170111_101357_delete_agrorus_proposal extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        Yii::import('application.modules.proposal.components.ShardedAR');
        Yii::import('application.modules.calendar.models.CalendarFairUsersTasks');
        Yii::import('application.modules.calendar.models.CalendarFairTasks');
        ShardedAR::$_db = Yii::app()->db;

        $criteria = new CDbCriteria();
        $criteria->addCondition('fairId = :fairId');
        $criteria->params[':fairId'] = 11699;//Agrorus fair

        $calendarFairTasks = CalendarFairTasks::model()->findAll($criteria);

        foreach($calendarFairTasks as $calendarFairTask){

            CalendarFairUsersTasks::model()->deleteAllByAttributes(array('uuid' => $calendarFairTask->uuid));

            $calendarFairTask->delete();
        }

        return true;
    }

    public function down()
    {
        return true;
    }
}