<?php

class m160225_074231_delete_fair extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return '
			DELETE FROM {{fair}} WHERE `id` = 1077;
			DELETE FROM {{trfair}} WHERE `trParentId` = 1077;

			DELETE FROM {{trfair}} WHERE `id` in (
				25,
				88,
				787,
				848,
				921,
				982,
				1072,
				1073,
				1248,
				1249,
				1361,
				1362,
				1383,
				1384,
				1422,
				1423,
				1483,
				1484,
				1603,
				1604,
				1605,
				1606,
				1608,
				1609,
				1931,
				1932,
				1937,
				1938,
				1939,
				1940,
				1941,
				1942,
				2952,
				2953,
				2954,
				2955,
				3445,
				3446,
				3447,
				3448,
				3449,
				3450
			)
		';
	}
}