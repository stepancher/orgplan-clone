<?php

class m160816_142117_deleted_double_fairhasindustries extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			DELETE FROM {{fairhasindustry}} WHERE `id`='19711';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19361';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19460';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19350';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19358';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19500';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19615';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19739';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19738';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19390';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19421';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19363';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19461';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19398';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19443';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19696';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19380';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19686';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19756';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19444';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19425';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19438';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19585';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19382';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19488';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19543';
			DELETE FROM {{fairhasindustry}} WHERE `id`='19664';
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}