<?php

class m170125_105613_add_geo_relate_to_organizercompany extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{organizercompany}} 
            ADD COLUMN `cityId` INT(11) NULL DEFAULT NULL AFTER `email`;
            
            ALTER TABLE {{trorganizercompany}} 
            ADD COLUMN `street` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;
            
            ALTER TABLE {{organizercompany}} 
            ADD COLUMN `coordinates` VARCHAR(255) NULL DEFAULT NULL AFTER `cityId`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}