<?php

class m170221_125303_add_exdb_exhibitioncomplexhasassociation_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_exhibitioncomplexhasassociation_to_exdb_db`;
            
            CREATE PROCEDURE {$expodata}.`add_exdb_exhibitioncomplexhasassociation_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE vc_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ec.id, ass.id 
                                        FROM
                                        (SELECT gr.id, gr.member_of_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        union
                                        SELECT gr.id, gr.member_of_second_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        union
                                        SELECT gr.id, gr.member_of_3_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        union
                                        SELECT gr.id, gr.member_of_4_id_en as assId
                                        FROM {$expodataRaw}.expo_ground gr
                                        ) t LEFT JOIN {$expodata}.{{exdbexhibitioncomplex}} ec ON ec.exdbId = t.id
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.exdbId = t.assId
                                        LEFT JOIN {$expodata}.{{exdbexhibitioncomplexhasassociation}} eha ON eha.exhibitionComplexId = ec.id AND eha.associationId = ass.id
                                        WHERE t.assId != 0 AND eha.id IS NULL ORDER BY ec.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO vc_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbexhibitioncomplexhasassociation}} (`exhibitionComplexId`,`associationId`) VALUES (vc_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_exhibitioncomplexhasassociation_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}