<?php

class m160406_153313_add_data_for_calendar extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			INSERT INTO {{calendarfairtasks}} VALUES
			(1,184,1,'Первый день монтажа (по согласованию с Организатором)',NULL,'2016-09-30 00:00:00','2016-10-01 00:00:00','gray',0),
			(2,184,1,'Дни монтажа',NULL,'2016-10-01 00:00:00','2016-10-04 00:00:00','black',0),
			(3,184,1,'Дни работы выставки',NULL,'2016-10-04 00:00:00','2016-10-08 00:00:00','green',0),
			(4,184,1,'Дни демонтажа',NULL,'2016-10-08 00:00:00','2016-10-09 00:00:00','blue',0),
			(5,184,1,'Последний день подачи заявки N1 со скидкой 20%',NULL,'2015-12-23 00:00:00','2015-12-24 00:00:00','red',0),
			(6,184,1,'Последний день подачи заявки N1 со скидкой 15%',NULL,'2016-03-31 00:00:00','2016-04-01 00:00:00','red',0),
			(7,184,1,'Последний день подачи заявки N1 со скидкой 10%',NULL,'2016-07-29 00:00:00','2016-07-30 00:00:00','red',0),
			(8,184,1,'Последний день подачи заявок N1',NULL,'2016-08-31 00:00:00','2016-09-01 00:00:00','red',0),
			(9,184,0,'Подать заявку N1 со скидкой 20%',NULL,NULL,'2015-12-24 00:00:00',NULL,0),
			(10,184,0,'Подать заявку N1 со скидкой 15%',NULL,NULL,'2016-04-01 00:00:00',NULL,0),
			(11,184,0,'Подать заявку N1 со скидкой 10%',NULL,NULL,'2016-07-30 00:00:00',NULL,0),
			(12,184,0,'Подать заявку N1',NULL,NULL,'2016-09-01 00:00:00',NULL,0),
			(13,184,0,'Подписать договор',10,NULL,NULL,NULL,1),
			(14,184,0,'Оплатить участие (аванс 10%)',10,NULL,NULL,NULL,1),
			(15,184,0,'Оплатить участие (аванс 40%)',NULL,'2016-04-16 00:00:00','2016-05-01 00:00:00',NULL,1),
			(16,184,0,'Оплатить участие (аванс 50%)',NULL,'2016-07-15 00:00:00','2016-07-30 00:00:00',NULL,1),
			(17,184,0,'Отправить заявку N2',NULL,NULL,'2016-08-26 00:00:00',NULL,0),
			(18,184,0,'Отправить заявку N2 с наценкой 50%',NULL,NULL,'2016-09-24 00:00:00',NULL,0),
			(19,184,0,'Отправить заявку N2 с наценкой 100%',NULL,NULL,'2016-10-04 00:00:00',NULL,0),
			(20,184,0,'Оплатить заявку N2',10,NULL,NULL,'#f6a800',1);

			INSERT INTO {{calendarfairuserstasks}} VALUES (1,9,2,NULL,NULL,NULL);

			INSERT INTO {{dependences}} VALUES
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/9'),
			(2,'calendarm/gantt/index','{}','calendarm/gantt/check/id/9'),
			(3,'calendarm/gantt/index','{}','calendarm/gantt/check/id/10'),
			(4,'calendarm/gantt/index','{}','calendarm/gantt/check/id/11'),
			(5,'calendarm/gantt/index','{}','calendarm/gantt/check/id/12'),
			(6,'calendarm/gantt/delete','{\"id\":9}','calendarm/gantt/add/id/10'),
			(7,'calendarm/gantt/delete','{\"id\":10}','calendarm/gantt/add/id/11'),
			(8,'calendarm/gantt/delete','{\"id\":11}','calendarm/gantt/add/id/12'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/9'),
			(10,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(11,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/11'),
			(12,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/12'),
			(13,'calendarm/gantt/complete','{\"id\":9}','calendarm/gantt/add/id/13'),
			(14,'calendarm/gantt/complete','{\"id\":10}','calendarm/gantt/add/id/13'),
			(15,'calendarm/gantt/complete','{\"id\":11}','calendarm/gantt/add/id/13'),
			(16,'calendarm/gantt/complete','{\"id\":12}','calendarm/gantt/add/id/13'),
			(17,'calendarm/gantt/complete','{\"id\":13}','calendarm/gantt/add/id/14'),
			(18,'calendarm/gantt/complete','{\"id\":14}','calendarm/gantt/add/id/15'),
			(19,'calendarm/gantt/complete','{\"id\":15}','calendarm/gantt/add/id/16'),
			(20,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"save\":1}','calendarm/gantt/add/id/17'),
			(21,'calendarm/gantt/index','{}','calendarm/gantt/check/id/17'),
			(22,'calendarm/gantt/index','{}','calendarm/gantt/check/id/18'),
			(23,'calendarm/gantt/index','{}','calendarm/gantt/check/id/19'),
			(24,'calendarm/gantt/delete','{\"id\":17}','calendarm/gantt/add/id/18'),
			(25,'calendarm/gantt/delete','{\"id\":18}','calendarm/gantt/add/id/19'),
			(26,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/17'),
			(27,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/18'),
			(28,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/19'),
			(29,'calendarm/gantt/complete','{\"id\":17}','calendarm/gantt/add/id/20'),
			(30,'calendarm/gantt/complete','{\"id\":18}','calendarm/gantt/add/id/20'),
			(31,'calendarm/gantt/complete','{\"id\":19}','calendarm/gantt/add/id/20'),
			(32,'calendarm/gantt/index','{}','calendarm/gantt/check/id/20/days/7/color/#fa694c');
		";
	}

	private function downSql() {

		return '
			TRUNCATE TABLE `tbl_calendar_fair_tasks`;
			TRUNCATE TABLE `tbl_calendar_fair_users_tasks`;
			TRUNCATE TABLE `tbl_dependences`;
		';
	}
}