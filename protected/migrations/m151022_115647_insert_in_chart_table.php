<?php

class m151022_115647_insert_in_chart_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getInsertInTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{chart}} WHERE id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,38);
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getInsertInTable(){
		return '
			INSERT INTO {{chart}} (`id`, `name`, `typeId`, `setId`, `widget`, `widgetType`, `header`, `measure`, `color`, `groupId`, `related`, `parent`) VALUES
			(1, "", 220777, 0, 1, 2, "Скот и птица <br/> в убойном весе", "место", "#fa694c", 5, NULL, NULL),
			(2, NULL, 220776, NULL, 1, 2, "Молоко", "место", "#fa694c", 5, NULL, NULL),
			(3, NULL, 220780, NULL, 1, 2, "Яйца", "место", "#fa694c", 5, NULL, NULL),
			(4, NULL, 220779, NULL, 1, 2, "Шерсть", "место", "#fa694c", 5, NULL, NULL),
			(5, NULL, 220778, NULL, 1, 2, "Мед", "место", "#fa694c", 5, NULL, NULL),
			(6, NULL, 220784, NULL, 1, 2, "Крупный <br/> рогатый скот", "место", "#a0d468", 7, NULL, NULL),
			(7, NULL, 220787, NULL, 1, 2, "Свиньи", "место", "#a0d468", 7, NULL, NULL),
			(8, NULL, 220785, NULL, 1, 2, "Овцы и козы", "место", "#a0d468", 7, NULL, NULL),
			(9, NULL, 220788, NULL, 1, 2, "Расход корма <br/> на 1 голову скота", "место", "#a0d468", 7, NULL, NULL),
			(10, NULL, 0, NULL, 1, 2, "Рентабельность <br/> живот-во", "место", "#a0d468", 7, NULL, NULL),
			(11, NULL, 220769, NULL, 1, 2, "Зерно", "место", "#a0d468", 4, NULL, NULL),
			(12, NULL, 220775, NULL, 1, 2, "Сахарная <br/> свекла", "место", "#a0d468", 4, NULL, NULL),
			(13, NULL, 220774, NULL, 1, 2, "Подсолнечник", "место", "#a0d468", 4, NULL, NULL),
			(14, NULL, 220770, NULL, 1, 2, "Картофель", "место", "#a0d468", 4, NULL, NULL),
			(15, NULL, 220772, NULL, 1, 2, "Овощи", "место", "#a0d468", 4, NULL, NULL),
			(16, NULL, 0, NULL, 1, 2, "Фрукты", "место", "#a0d468", 4, NULL, NULL),
			(17, NULL, 103781, NULL, 1, 3, "Продукция Сельского <br/>хозяйства всех<br/> категорий", "место <br/>в России", "#a0d468", 1, NULL, NULL),
			(18, NULL, 103781, NULL, 2, 2, "Количество <br/> организация", "ед.", "#a0d468", 1, NULL, NULL),
			(19, NULL, 103783, NULL, 1, 4, "Продукция сельскохозяйственных организаций", "место <br/>в России", "#43ade3", 2, NULL, NULL),
			(20, NULL, 220731, NULL, 2, 3, "Количество организация", "раст-во", "#f6a800", 2, 23, NULL),
			(21, NULL, 103784, NULL, 1, 4, "Продукция крестьянско-фермерских хозяйств и ИП", "место <br/>в России", "#f6a800", 3, NULL, NULL),
			(22, NULL, 220735, NULL, 2, 3, "Количество организация", "живот-во", "#f6a800", 3, 24, NULL),
			(23, NULL, 220732, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
			(24, NULL, 220736, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
			(25, NULL, 220777, NULL, 1, 2, "Сельскохозяйст&shy;венные угодья", "место", "#a0d468", 6, NULL, NULL),
			(26, NULL, 220781, NULL, 1, 2, "в том числе: Пашни", "место", "#a0d468", 6, NULL, NULL),
			(27, NULL, 220780, NULL, 1, 2, "С/з угодья от общей площади земель", "место", "#a0d468", 6, NULL, NULL),
			(28, NULL, 220779, NULL, 1, 2, "Рентабельность раст-ва", "место", "#43ade3", 6, NULL, NULL),
			(29, NULL, 220782, NULL, 2, 2, "Минеральные <br/> удобрения", "", "#42c0b4", NULL, NULL, 38),
			(30, NULL, 220783, NULL, 2, 2, "Органические <br/> удобрения", NULL, "#a0d468", NULL, NULL, 38),
			(38, "", 0, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL);
		';
	}
}