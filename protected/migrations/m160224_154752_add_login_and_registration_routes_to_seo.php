<?php

class m160224_154752_add_login_and_registration_routes_to_seo extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{seo}} WHERE `url`="/ru/login";
			DELETE FROM {{seo}} WHERE `url`="/ru/registration";
			DELETE FROM {{seo}} WHERE `url`="/de/login";
			DELETE FROM {{seo}} WHERE `url`="/de/registration";
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{seo}} (`title`, `url`, `route`, `header`, `lang`) VALUES ("Вход | Orgplan", "/ru/login", "site/auth", "Вход", "ru");
			INSERT INTO {{seo}} (`title`, `url`, `route`, `header`, `lang`) VALUES ("Регистрация | Orgplan", "/ru/registration", "site/registration", "Регистрация", "ru");
			INSERT INTO {{seo}} (`title`, `url`, `route`, `header`, `lang`) VALUES ("Anmelden | Orgplan", "/de/login", "site/auth", "Anmelden", "de");
			INSERT INTO {{seo}} (`title`, `url`, `route`, `header`, `lang`) VALUES ("Anmeldung | Orgplan", "/de/registration", "site/registration", "Anmeldung", "de");
			  ';
	}
}