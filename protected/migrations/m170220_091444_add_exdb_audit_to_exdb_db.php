<?php

class m170220_091444_add_exdb_audit_to_exdb_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $expodataRaw) = explode('=', Yii::app()->expodataRaw->connectionString);
        
        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`add_exdb_audit_to_exdb_db`;
            CREATE PROCEDURE {$expodata}.`add_exdb_audit_to_exdb_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE aud_contact TEXT DEFAULT '';
                DECLARE aud_name TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT au.id, au.contacts_en, au.name_en 
                                        FROM {$expodataRaw}.expo_audit au
                                            LEFT JOIN {$expodata}.{{exdbaudit}} tau ON tau.exdbId = au.id
                                        WHERE tau.id IS NULL;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO aud_id, aud_contact, aud_name;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$expodata}.{{exdbaudit}} (`exdbId`,`contacts_en`) VALUES (aud_id, aud_contact);
                        SET @last_insert_id := LAST_INSERT_ID();
                        INSERT INTO {$expodata}.{{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, 'ru');
                        INSERT INTO {$expodata}.{{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, 'en');
                        INSERT INTO {$expodata}.{{exdbtraudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, 'de');
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`add_exdb_audit_to_exdb_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}