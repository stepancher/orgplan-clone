<?php

class m170214_140356_copy_exdb_audits_to_db_procedure extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);
        
        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_audits_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_audits_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE exdb_active INT DEFAULT 0;
                DECLARE exdb_name TEXT DEFAULT '';
                DECLARE exdb_lang_id VARCHAR(55) DEFAULT '';
                DECLARE exdb_id INT DEFAULT 0;
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT a.id, a.active, tra.name, tra.langId 
                                        FROM {$expodata}.{{exdbaudit}} a
                                            LEFT JOIN {$expodata}.{{exdbtraudit}} tra ON tra.trParentId = a.id
                                            LEFT JOIN {$db}.{{traudit}} traud ON traud.langId = tra.langId AND traud.name = tra.name
                                        WHERE traud.id IS NULL
                                        GROUP BY tra.name, tra.langId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO exdb_id, exdb_active, exdb_name, exdb_lang_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    IF temp_id != exdb_id THEN
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{audit}} (`active`) VALUES (exdb_active);
                        COMMIT;
                        
                        SET @last_insert_id := LAST_INSERT_ID();
                        SET temp_id := exdb_id;
                    END IF;
                
                    START TRANSACTION;
                        INSERT INTO {$db}.{{traudit}} (`name`,`trParentId`,`langId`) VALUES (exdb_name, @last_insert_id, exdb_lang_id);
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}