<?php

class m160316_180728_descriptionSnippet_for_tbl_regioninformation extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
			ALTER TABLE {{regioninformation}} DROP COLUMN `descriptionSnippet`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{regioninformation}}
			ADD COLUMN `descriptionSnippet` TEXT NULL DEFAULT NULL AFTER `sourceUrl`;

			update {{regioninformation}} set `descriptionSnippet`='Крупнейший производитель экологически чистого продовольствия в России' where `id` = 1;
			update {{regioninformation}} set `descriptionSnippet`='Направления сельского хозяйства: зерновое земледелие и мясомолочное животноводство, выращивают сою.' where `id` = 2;
			update {{regioninformation}} set `descriptionSnippet`='Треть производства страны картона и целлюлозы, значительная часть – пиломатериалов, и древесины необработанной.' where `id` = 3;
			update {{regioninformation}} set `descriptionSnippet`='Важную роль в экономике области играет сельское хозяйство, прежде всего растениеводство' where `id` = 4;
			update {{regioninformation}} set `descriptionSnippet`='Треть общероссийской добычи концентрата железорудного, окатышей железорудных (окисленных)' where `id` = 5;
			update {{regioninformation}} set `descriptionSnippet`='Половина производства страны тепловозов маневровых и промышленных, пятую часть составляет выпуск автокранов' where `id` = 6;
			update {{regioninformation}} set `descriptionSnippet`='Пятая часть производства страны многофазных электродвигателей переменного тока мощностью более 750 Вт' where `id` = 7;
			update {{regioninformation}} set `descriptionSnippet`='Пятая часть производства в стране плит газовых бытовых. Выращивают твердые сорта пшеницы, овощи. Развито животноводство' where `id` = 8;
			update {{regioninformation}} set `descriptionSnippet`='Треть производства в стране подшипников, производство готового проката черных металлов, стали' where `id` = 9;
			update {{regioninformation}} set `descriptionSnippet`='Производство машин зерноочистительных, синтетических каучуков, сахара. Развиты отрасли сельского хозяйства и животноводства' where `id` = 10;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство: мясомолочное скотоводство, свиноводство.' where `id` = 14;
			update {{regioninformation}} set `descriptionSnippet`='Является крупным сельскохозяйственным регионом, специализированным на тонко-рунном овцеводстве.' where `id` = 15;
			update {{regioninformation}} set `descriptionSnippet`='Более четверти производства в стране тканей, более трети производства автокранов.' where `id` = 16;
			update {{regioninformation}} set `descriptionSnippet`='Пятая часть производства в стране целлюлозы, значительная часть – необработанной древесины, пиломатериалов' where `id` = 17;
			update {{regioninformation}} set `descriptionSnippet`='Треть производства в стране огурцов, корнишонов и томатов' where `id` = 18;
			update {{regioninformation}} set `descriptionSnippet`='Половина общероссийского производства консервов рыбных, аппаратуры приемной телевизионной.' where `id` = 19;
			update {{regioninformation}} set `descriptionSnippet`='Производство маневровых и промышленных тепловозов, турбин на водя-ном паре, легковых автомобилей.' where `id` = 20;
			update {{regioninformation}} set `descriptionSnippet`='Производство рыбы живой, свежей и охлажденной. Развито мясомолочное животноводство, оленеводство' where `id` = 21;
			update {{regioninformation}} set `descriptionSnippet`='Производство вод минеральных и газированных. Развито мясомолочное животноводство' where `id` = 22;
			update {{regioninformation}} set `descriptionSnippet`='Добыча угля, производство готового проката черных металлов, выплавка чугуна и стали.' where `id` = 23;
			update {{regioninformation}} set `descriptionSnippet`='Производство деревообрабатывающих станков, шин, покрышек и камер резиновых новых, древесноволокнистых плит.' where `id` = 24;
			update {{regioninformation}} set `descriptionSnippet`='Производство прядильных машин и автокранов. Развито животноводство, растениеводство' where `id` = 25;
			update {{regioninformation}} set `descriptionSnippet`='Высокоразвито сельское хозяйство, производство вин столовых' where `id` = 26;
			update {{regioninformation}} set `descriptionSnippet`='Добыча угля. Производство зерна, картофеля и овощей. Развито животноводство' where `id` = 27;
			update {{regioninformation}} set `descriptionSnippet`='Выращивают зерновые и кормовые культуры. Развито животноводство мясомолочного направления, птицеводство.' where `id` = 28;
			update {{regioninformation}} set `descriptionSnippet`='Производство окатышей железорудных окисленных, концентрата железорудного, аккумуляторов.' where `id` = 29;
			update {{regioninformation}} set `descriptionSnippet`='Производство чая, сигарет, картона, бумаги, целлюлозы, автомобилей легковых.' where `id` = 30;
			update {{regioninformation}} set `descriptionSnippet`='Производство бытовых холодильников и морозильников, чугуна, проката черных металлов, стали' where `id` = 31;
			update {{regioninformation}} set `descriptionSnippet`='Развито оленеводство, звероводство, охотничий и рыболовный промысел.' where `id` = 32;
			update {{regioninformation}} set `descriptionSnippet`='Производство легковых автомобилей, лифтов, изделий колбасных, коньяка, сигарет, какао, шоколада.' where `id` = 11;
			update {{regioninformation}} set `descriptionSnippet`='Производство магистральных тепловозов, лекарственных средств, лакокрасочных материалов' where `id` = 33;
			update {{regioninformation}} set `descriptionSnippet`='На долю области приходится весь выпуск апатитового концентрата в Российской Федерации' where `id` = 34;
			update {{regioninformation}} set `descriptionSnippet`='Развито молочное скотоводство, оленеводство. Растениеводство представлено слабо' where `id` = 35;
			update {{regioninformation}} set `descriptionSnippet`='Производство автобусов, грузовых автомобилей, машин для городского коммунального хозяйства' where `id` = 36;
			update {{regioninformation}} set `descriptionSnippet`='Развито растениеводство, животноводство молочного направления, птицеводство' where `id` = 37;
			update {{regioninformation}} set `descriptionSnippet`='Производство льноволокна. Развито сельское хозяйство, мясомолочное животноводство, птицеводство, пчеловодство' where `id` = 38;
			update {{regioninformation}} set `descriptionSnippet`='Производство шин, покрышек и камер резиновых, первичная переработка нефти. Развиты сельское хозяйство, пушной промысел, звероводство' where `id` = 39;
			update {{regioninformation}} set `descriptionSnippet`='Производство электродвигателей, выработки соли молотой.' where `id` = 40;
			update {{regioninformation}} set `descriptionSnippet`='Производство насосов центробежных, погрузчиков самоходных, машин для городского и коммунального хозяйства' where `id` = 41;
			update {{regioninformation}} set `descriptionSnippet`='Производство сахара, зерна (рожь, пшеница). Мясомолочное животноводство.' where `id` = 42;
			update {{regioninformation}} set `descriptionSnippet`='Производство минеральных удобрений. Развиты птицеводство, пчеловодство, пригородное хозяйство.' where `id` = 43;
			update {{regioninformation}} set `descriptionSnippet`='Производство рыбы и продуктов рыбных переработанных и консервированных. Край имеет довольно развитое сельское хозяйство.' where `id` = 44;
			update {{regioninformation}} set `descriptionSnippet`='В сельском хозяйстве преобладает молочное животноводство; растениеводство специализируется на выращивании кормовых культур.' where `id` = 45;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство: мясомолочное животноводство, овцеводство, козоводство. Растениеводство: выращивание кормовых культур.' where `id` = 47;
			update {{regioninformation}} set `descriptionSnippet`='Производство бумаги, целлюлозы, фанеры. Сельское хозяйство: животноводство. Развито оленеводство.' where `id` = 54;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство имеет зерновую и мясомолочную специализацию. Также развиты овощеводство и садоводство.' where `id` = 59;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство: мясное скотоводство, овцеводство, козоводство и коневодство.' where `id` = 61;
			update {{regioninformation}} set `descriptionSnippet`='Производство кальцинированной соды, пластмасс в первичных формах, первичная переработка нефти' where `id` = 48;
			update {{regioninformation}} set `descriptionSnippet`='Производство коньяка. Развито виноградарство, садоводство, овощеводство, овцеводство мясо-шерстного направления, козоводство.' where `id` = 50;
			update {{regioninformation}} set `descriptionSnippet`='Преобладает мясомолочное животноводство, овцеводство, птицеводство; Растеневодство: зерновые и кормовые культуры, картофель' where `id` = 62;
			update {{regioninformation}} set `descriptionSnippet`='Развиты тонкорунное овцеводство, разведение крупного рогатого скота. Сельское хозяйство: выращивание зерновых культур.' where `id` = 51;
			update {{regioninformation}} set `descriptionSnippet`='Развиты земледелие, животноводство, а также птицеводство.' where `id` = 56;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство играет главную роль в экономике республики. Животноводство: тонкорунное овцеводство, крупный рогатый скот.' where `id` = 52;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство: животноводство мясомолочного направления и выращивание зерновых и кормовых культур. Развито птицеводство.' where `id` = 57;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство специализируется на земледелии. В животноводстве преобладает разведение крупного рогатого скота' where `id` = 46;
			update {{regioninformation}} set `descriptionSnippet`='Производство бумаги, целлюлозы и концентрата железорудного. Сельское хозяйство: молочное и мясное животноводство.' where `id` = 53;
			update {{regioninformation}} set `descriptionSnippet`='Широко развиты оленеводство, звероводство и пушной промысел.' where `id` = 58;
			update {{regioninformation}} set `descriptionSnippet`='Производство каучуков синтетических, шин, покрышек и камер, пластмасс в первичных формах.' where `id` = 60;
			update {{regioninformation}} set `descriptionSnippet`='Добыча антрацита, производства магистральных электровозов и зерноуборочных комбайнов.' where `id` = 63;
			update {{regioninformation}} set `descriptionSnippet`='Развито животноводство (крупный рогатый скот, свиноводство, птицеводство), растениеводство (пшеница, рожь, ячмень, гречиха)' where `id` = 64;
			update {{regioninformation}} set `descriptionSnippet`='Производство легковых автомобилей. Сельское хозяйство области специализируется на производстве зерна и животноводстве' where `id` = 65;
			update {{regioninformation}} set `descriptionSnippet`='Производство гидравлических турбин, турбин на водяном паре, сигарет, пресервов из разделанной рыбы' where `id` = 12;
			update {{regioninformation}} set `descriptionSnippet`='Производство троллейбусов. В сельском хозяйстве преобладают мясомолочное скотоводство' where `id` = 66;
			update {{regioninformation}} set `descriptionSnippet`='Производство рыбы живой свежей или охлажденной, консервов рыбных натуральных. Развито животноводство' where `id` = 67;
			update {{regioninformation}} set `descriptionSnippet`='Производство труб стальных, прокатного оборудования, концентрата железорудного, выплавки чугуна и стали.' where `id` = 68;
			update {{regioninformation}} set `descriptionSnippet`='Производство ламп (накаливания, газоразрядных, дуговых).' where `id` = 69;
			update {{regioninformation}} set `descriptionSnippet`='Производство коньяка, вин столовых, вод минеральных и газированных' where `id` = 70;
			update {{regioninformation}} set `descriptionSnippet`='Производство зерновых и кормовых культур, сахарной свеклы, подсолнечника.' where `id` = 71;
			update {{regioninformation}} set `descriptionSnippet`='Сельское хозяйство имеет животноводческую направленность. Растениеводство специализируется на выращивании льна-долгунца' where `id` = 72;
			update {{regioninformation}} set `descriptionSnippet`='В сельском хозяйстве развиты мясомолочное животноводство, звероводство, растениеводство' where `id` = 73;
			update {{regioninformation}} set `descriptionSnippet`='Производство спирта этилового и кранов мостовых электрических общего назначения.' where `id` = 74;
			update {{regioninformation}} set `descriptionSnippet`='Добыча нефти, природного и попутного газа, значительная часть выработки электроэнергии.' where `id` = 75;
			update {{regioninformation}} set `descriptionSnippet`='Производство электродвигателей. Развито животноводство, льноводство, успешно занимаются пчеловодством.' where `id` = 76;
			update {{regioninformation}} set `descriptionSnippet`='Производство автобусов, грузовых автомобилей.' where `id` = 77;
			update {{regioninformation}} set `descriptionSnippet`='Животноводство имеет мясомолочное направление. Развиты оленеводство, звероводство и охотничий промысел.' where `id` = 78;
			update {{regioninformation}} set `descriptionSnippet`='На долю округа приходится почти половина добычи нефти в стране.' where `id` = 79;
			update {{regioninformation}} set `descriptionSnippet`='Производство тракторов, бульдозеров, грейдеров, готового проката черных металлов, выплавки чугуна и стали' where `id` = 80;
			update {{regioninformation}} set `descriptionSnippet`='Производство винограда, зерновых культур. Развиты тонкорунное овцеводство, птицеводство, разводят крупный рогатый скот.' where `id` = 81;
			update {{regioninformation}} set `descriptionSnippet`='В сельском хозяйстве развито мясомолочное животноводство, птицеводство и производство зерна, хмеля, картофеля.' where `id` = 82;
			update {{regioninformation}} set `descriptionSnippet`='В сельском хозяйстве развиты оленеводство, звероводство, пушной промысел, охота на морского зверя' where `id` = 83;
			update {{regioninformation}} set `descriptionSnippet`='Добыча природного и попутного газа. В сельском хозяйстве развиты оленеводство, звероводство, пушной промысел.' where `id` = 84;
			update {{regioninformation}} set `descriptionSnippet`='Производство двигателей внутреннего сгорания, электродвигателей, лакокрасочных материалов' where `id` = 85;
		";
	}
}