<?php

class m160622_082343_complete_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		DELETE FROM {{attributeclassdependency}} WHERE `id`='959';
		DELETE FROM {{attributeclassdependency}} WHERE `id`='960';
		DELETE FROM {{attributeclassdependency}} WHERE `id`='958';
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2180', '2180', 'increaseByLeftDay', '{1000:1, 46:2}', '1');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2179', '2179', 'increaseByLeftDay', '{1000:1, 46:2}', '1');
		INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('2147', '2147', 'increaseByLeftDay', '{1000:1, 46:2}', '1');
		UPDATE {{proposalelementclass}} SET `active`='1' WHERE `id`='20';
";
	}
}