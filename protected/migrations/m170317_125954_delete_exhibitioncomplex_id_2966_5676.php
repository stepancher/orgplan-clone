<?php

class m170317_125954_delete_exhibitioncomplex_id_2966_5676 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{exhibitioncomplex}} WHERE `id` = '5676';
            
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14741';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14742';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14743';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14744';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14926';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14936';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14937';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14938';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14939';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14945';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14991';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '14992';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15060';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15111';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15112';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15301';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15476';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15515';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15516';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15762';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15763';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15835';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15836';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '15888';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '16013';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '16054';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '16055';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '16056';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17151';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17152';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17393';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17408';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17647';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17800';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17948';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17955';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17956';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17957';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17997';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '17998';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '18505';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '18506';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '18688';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '18694';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '18695';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '19785';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '19840';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '19972';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '19973';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '20556';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '20562';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '20563';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '20564';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '20565';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '20566';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '21104';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '21366';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '21377';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '21597';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '21956';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '22235';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '22454';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '23011';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '23012';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '23508';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '23589';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '24780';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '24781';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '24782';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '24783';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '25680';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '25681';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '25758';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '25759';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '25909';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '26241';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '26440';
            UPDATE {{fair}} SET `exhibitionComplexId` = '2176' WHERE `id` = '26649';
            
            DELETE FROM {{exhibitioncomplex}} WHERE `id` = '2966';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}