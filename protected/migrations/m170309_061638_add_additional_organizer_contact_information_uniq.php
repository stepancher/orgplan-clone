<?php

class m170309_061638_add_additional_organizer_contact_information_uniq extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{organizercontactlist}} WHERE `organizerId` IS NULL;
            DELETE FROM {{organizercontactlist}} WHERE `value` = '#ERROR!';
            
            ALTER TABLE {{organizercontactlist}} 
            DROP INDEX `orgIdTypeValue` ;
            
            DROP PROCEDURE IF EXISTS `add_additionalinformation_to_organizercontactlist`;
            CREATE PROCEDURE `add_additionalinformation_to_organizercontactlist`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE o_id INT DEFAULT 0;
                DECLARE o_org_id INT DEFAULT 0;
                DECLARE o_type INT DEFAULT 0;
                DECLARE o_value TEXT DEFAULT '';
                
                DECLARE value_string TEXT DEFAULT '';
                DECLARE delimiter_pos INT DEFAULT 0;
                
                DECLARE copy CURSOR FOR SELECT ocl.id, ocl.organizerId, ocl.type, ocl.value
                                        FROM {{organizercontactlist}} ocl 
                                        WHERE ocl.value LIKE '%до%'
                                        AND ocl.type != 2
                                        ORDER BY ocl.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    
                    FETCH copy INTO o_id, o_org_id, o_type, o_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    SET value_string := o_value;
                    SET delimiter_pos := LOCATE('до', value_string);
                    
                    IF delimiter_pos != 0 THEN
                    
                        START TRANSACTION;
                            UPDATE {{organizercontactlist}} SET `value` = SUBSTR(value_string, 1, delimiter_pos - 1) WHERE `id` = o_id;
                            UPDATE {{organizercontactlist}} SET `additionalContactInformation` = SUBSTR(value_string, delimiter_pos) WHERE `id` = o_id;
                        COMMIT;
                    
                    END IF;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `add_additionalinformation_to_organizercontactlist`();
            DROP PROCEDURE IF EXISTS `add_additionalinformation_to_organizercontactlist`;
            
            CREATE PROCEDURE `add_additionalinformation_to_organizercontactlist`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE o_id INT DEFAULT 0;
                DECLARE o_new_value TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT ocl.id,
                                        SUBSTR(ocl.value, 1, LENGTH(ocl.value) - 2) AS newValue
                                        FROM {{organizercontactlist}} ocl 
                                        WHERE ocl.type != 2
                                        AND ocl.additionalContactInformation LIKE '%до%'
                                        AND BINARY SUBSTR(ocl.value, LENGTH(ocl.value) - 1) = ', '
                                        ORDER BY ocl.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                read_loop: LOOP
                    
                    FETCH copy INTO o_id, o_new_value;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{organizercontactlist}} SET `value` = o_new_value WHERE `id` = o_id;
                    COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `add_additionalinformation_to_organizercontactlist`();
            DROP PROCEDURE IF EXISTS `add_additionalinformation_to_organizercontactlist`;
            
            ALTER TABLE {{organizercontactlist}} 
            ADD UNIQUE INDEX `orgIdvalueTypeAddinfo` (`organizerId` ASC, `value` ASC, `type` ASC, `additionalContactInformation` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}