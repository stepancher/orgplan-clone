<?php

class m160516_075202_proposal_9_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='980';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='981';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='982';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='983';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='984';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='985';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='986';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='987';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='988';
			UPDATE {{attributeclass}} SET `priority`='5.7' WHERE `id`='989';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='992';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='993';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='994';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='995';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='996';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='997';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='998';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='999';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='1000';
			UPDATE {{attributeclass}} SET `priority`='4.5' WHERE `id`='1001';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='671';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='672';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='680';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='681';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='689';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='690';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='698';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='699';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='707';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='708';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='716';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='717';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='725';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='726';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='734';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='735';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='743';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='744';
			UPDATE {{attributeclass}} SET `priority`='5.65' WHERE `id`='752';
			UPDATE {{attributeclass}} SET `priority`='7' WHERE `id`='753';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='671';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='680';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='689';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='698';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='707';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='716';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='725';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='734';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='743';
			UPDATE {{attributeclass}} SET `priority`='4.55' WHERE `id`='752';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='672';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='681';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='690';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='699';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='708';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='717';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='726';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='735';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='744';
			UPDATE {{attributeclass}} SET `priority`='5.75' WHERE `id`='753';
			UPDATE {{attributeclass}} SET `priority`='0.5' WHERE `id`='621';
			UPDATE {{attributeclass}} SET `priority`='0.6' WHERE `id`='623';
			UPDATE {{attributeclass}} SET `priority`='0.6' WHERE `id`='625';
			UPDATE {{attributeclass}} SET `priority`='0.6' WHERE `id`='627';
			UPDATE {{attributeclass}} SET `priority`='0.6' WHERE `id`='628';
			
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('671', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('672', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('680', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('681', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('689', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('690', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('698', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('699', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('707', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('708', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('716', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('717', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('725', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('726', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('734', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('735', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('743', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('744', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('752', '1', 'summaryGroup');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('753', '1', 'summaryGroup');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '753', '978', 'get', '0');
			
			DELETE FROM {{attributeclassdependency}} WHERE `id`='713';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='714';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='715';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='716';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='717';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='718';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='719';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='720';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='721';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='722';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='723';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='724';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='725';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='726';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='727';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='728';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='729';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='730';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='731';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='732';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='733';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='734';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='735';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='736';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='737';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='738';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='739';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='740';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='741';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='742';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='743';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='744';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='745';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='746';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='747';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='748';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='749';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='750';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='751';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='752';
			DELETE FROM {{attributeclassdependency}} WHERE `id`='753';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('960', '671', 'get', '0');
			
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('961', '671', 'get', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('962', '680', 'get', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('963', '680', 'get', '0');
			UPDATE {{attributeclassdependency}} SET `function`='aggregate', `functionArgs`=NULL WHERE `id`='775';
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '960', '671', 'aggregate', '0');
			UPDATE {{attributeclassdependency}} SET `function`='aggregate' WHERE `id`='776';
			UPDATE {{attributeclassdependency}} SET `function`='aggregate' WHERE `id`='777';
			UPDATE {{attributeclassdependency}} SET `functionArgs`=NULL WHERE `id`='775';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('602', '960', 'summaryAndRound', '{\"min\": 2}');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('602', '962', 'summaryAndRound', '{\"min\": 2}');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('598', '963', 'summaryAndRound', '{\"min\": 2}');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`) VALUES ('598', '961', 'summaryAndRound', '{\"min\": 2}');
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"group\": 1}' WHERE `id`='775';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"group\": 1}' WHERE `id`='778';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"group\": 2}' WHERE `id`='777';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"group\": 2}' WHERE `id`='776';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"group\": 1}' WHERE `id`='776';
			UPDATE {{attributeclassdependency}} SET `functionArgs`='{\"group\": 2}' WHERE `id`='775';
			UPDATE {{attributeclassdependency}} SET `functionArgs`=NULL WHERE `id`='775';
			UPDATE {{attributeclassdependency}} SET `functionArgs`=NULL WHERE `id`='776';
			UPDATE {{attributeclassdependency}} SET `functionArgs`=NULL WHERE `id`='777';
			UPDATE {{attributeclassdependency}} SET `functionArgs`=NULL WHERE `id`='778';
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '1002', '672', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '1003', '672', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '1004', '681', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES (NULL, '1005', '681', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('964', '689', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('965', '689', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1006', '690', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1007', '690', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('966', '698', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('967', '698', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1008', '699', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1009', '699', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('968', '707', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('969', '707', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1010', '708', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1011', '708', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('970', '716', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('971', '716', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1012', '717', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1013', '717', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('972', '725', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('973', '725', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1014', '726', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1015', '726', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('974', '734', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('975', '734', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1016', '735', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1017', '735', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('976', '743', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('977', '743', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1018', '744', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1019', '744', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('978', '752', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('979', '752', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1020', '753', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('1021', '753', 'aggregate', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '602', '960', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '602', '962', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '964', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '966', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '968', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '970', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '972', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '974', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '976', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('602', '978', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '965', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '967', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '969', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '971', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '973', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '975', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '977', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('598', '979', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '636', '1002', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1004', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1008', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1006', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1010', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1012', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1014', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1016', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1018', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES ('636', '1020', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1003', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1005', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1009', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1007', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1011', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1013', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1015', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1017', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1019', 'summaryAndRound', '{\"min\": 2}', '0');
			INSERT INTO {{attributeclassdependency}} (`id`, `attributeClass`, `attributeClassRelated`, `function`, `functionArgs`, `auto`) VALUES (NULL, '634', '1021', 'summaryAndRound', '{\"min\": 2}', '0');
			UPDATE {{attributeclass}} SET `placeholder`='____/__/__' WHERE `id`='752';
			UPDATE {{attributeclass}} SET `placeholder`='____/__/__' WHERE `id`='753';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='754';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='755';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='756';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='758';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='760';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='762';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='764';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='766';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='768';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='770';
			UPDATE {{attributeclassdependency}} SET `function`='getDemountingDates' WHERE `id`='772';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='757';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='759';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='761';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='763';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='765';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='767';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='769';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='771';
			UPDATE {{attributeclassdependency}} SET `function`='getMountingDates' WHERE `id`='773';
	    ";
	}
}