<?php

class m161027_141601_translates_for_8_proposal extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
        INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('24', 'de', 'Antragsformular Nr. 8', 'INFORMATIONSUNTERSTÜTZUNG. AUSSTELLERKATALOG');

        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2508', 'de', 'Senden Sie bitte das ausgefüllte Antragsformular bis spätestens 4. August 2016 an die Messeleitung. Fax: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2509', 'de', 'Ausstellerinformation (russische Version)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2510', 'de', 'Ausstellerinformation in Russisch wird nur im russischen Katalogteil eingetragen.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2511', 'de', 'Firmenname');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2512', 'de', 'Postleitzahl');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2513', 'de', 'Land');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2514', 'de', 'Region');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2515', 'de', 'Stadt');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2516', 'de', 'Strasse');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2517', 'de', 'Hausnummer');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2518', 'de', 'Telefon');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2519', 'de', 'Fax');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2520', 'de', 'Web-Adresse');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2521', 'de', 'E-mail');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2522', 'de', 'Ausstellerinformation', 'Max. 430 Zeichen kostenlos, inkl. Leerzeichen');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2523', 'de', 'Zusätzliche Zeichen');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2524', 'de', 'Ausstellerinformation (englische Version)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2525', 'de', 'Ausstellerinformation in Englisch wird nur im englischen Katalogteil eingetragen.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2526', 'de', 'Company\'s Name');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2527', 'de', 'Zip code/Postcode');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2528', 'de', 'Country');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2529', 'de', 'Region');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2530', 'de', 'City');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2531', 'de', 'Street');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2532', 'de', 'House');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2533', 'de', 'Telephone');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2534', 'de', 'Fax');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2535', 'de', 'WebSite');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2536', 'de', 'E-mail');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2537', 'de', 'Company information', 'Up to 430 symbols for free, including spaces');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2538', 'de', 'Additional symbol');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2539', 'de', 'Falls das Antragsformular über den Firmeneintrag nicht vollständig oder nicht ausgefüllt ist, behält sich der Veranstalter das Recht vor, keinen Firmeneintrag zu veröffentlichen, oder nach seinem Ermessen die fehlenden Informationen einzutragen und/oder die vom Austeller angegebenen Informationen zu übersetzen, und/oder im entsprechenden Katalogteil und/oder auf der Web-Seite der Messe einzutragen. Falls keine Kategorie oder mehrere Kategorien aus dem Ausstellerkatalog, wie gemäss dem Antrag bezahlt, für den Ausstellerkatalogeintrag angegeben sind, behält sich der Veranstalter das Recht vor, die Kategorien für den Ausstellerkatalogeintrag selbst auszuwählen.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2540', 'de', 'Katalogkategorien');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2541', 'de', 'Wählen Sie bitte die Katalogkategorien für Ihren Firmeneintrag aus, zusätzlich wählen Sie bitte die Katalogkategorien für Ihren Logoeintrag aus.<br><br>Die Ausstellerinformation wird nur in einer Katalogkategorie kostenlos eingetragen');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2542', 'de', '1. Traktoren', '1. TRAKTOREN<br>1.1 Radtraktoren<br>1.2 Kettentraktoren<br>1.3 Spezialtraktoren/Sondertraktoren<br>1.4 Sonstiges aus der Gruppe \"Traktoren\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2543', 'de', '2. Mobile Ladetechnik', '2. MOBILE LADETECHNIK<br>2.1 Kompaktlader/-lademaschinen/-ladegerät/-stapler für Tierzuchtbetriebe<br>2.2 Teleskoplader<br>2.3 Radlader<br>2.4 Front- und Rücklader<br>2.5 Gabellader<br>2.6 Bagger, Ladekrane<br>2.7 Sonstiges aus der Gruppe \"Mobile Ladetechnik\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2544', 'de', '3. Bodenbearbeitungs- und Saattechnik', '3. BODENBEARBEITUNGS- UND SAATTECHNIK<br> 3.1 Pflüge<br>3.2 Tiefenlockerer<br>3.3 Kultivatoren<br>3.4 Eggen<br>3.5 Bodenfräsen<br>3.6 Kombinierte Bodenbearbeitungsgeräte<br>3.7 Bodenbearbeitungsgeräte mit Zapfwellenantrieb<br>3.8 Walzen<br>3.9 Sonstiges aus der Gruppe \"Bodenbearbeitungs - und Saattechnik\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2545', 'de', '4. Maschinen und Technik für Saat und Reihensaat', '4. MASCHINEN UND TECHNIK FÜR SAAT UND REIHENSAAT<br>4.1 Reihensämaschinen<br>4.2 Präzisionsdrillmaschinen<br>4.3 Sätechnik für Pflanzen in Mulchfolie<br>4.4 Stoppelsämaschinen<br>4.5 Drillmaschinenkombinationen<br>4.6 Grasreihensämaschinen<br>4.7 Sämaschinen für Abersaat<br>4.8 Sä- und Pflanzmaschinen<br>4.9 Sämaschinen für Forschungsstätten<br>4.10 Sonstiges aus der Gruppe \"Maschinen und Technik für Saat und Reihensaat\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2546', 'de', '5. Düngemaschinen und -technik', '5. DÜNGEMASCHINEN UND -TECHNIK<br>5.1 Naturdüngergabe<br>5.2 Mineraldüngergabe<br>5.3 Düngerstreuer<br>5.4 Flüssigdüngergabe<br>5.5 Sonstiges aus der Gruppe \"Düngemaschinen und - technik\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2547', 'de', '6. Pflanzenschutzmaschinen und -ausrüstung', '6. PFLANZENSCHUTZMASCHINEN UND -AUSRÜSTUNG<br>6.1 Technik für den chemischen Pflanzenschutz<br>6.2 Technik für den mechanischen Pflanzenschutz<br>6.3 Technik für den thermischen Pflanzenschutz<br>6.4 Sonstiges aus der Gruppe \"Pflanzenschutzmaschinen und - ausrüstung\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2548', 'de', '7. Maschinen und Technik für Bewässerung und Wasserabführung', '7.  MASCHINEN UND TECHNIK FÜR BEWÄSSERUNG UND WASSERABFÜHRUNG<br>7.1 Bewässerungssysteme und Zubehör<br>7.2 Technik für Wasserabführung<br>7.3 Sonstiges aus der Gruppe \"Maschinen und Technik für Bewässerung und Wasserabführung\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2549', 'de', '8. Erntemaschinen und -ausrüstung', '8. ERNTEMASCHINEN UND -AUSRÜSTUNG<br>8.1 Ernte mit Mähdrescher, Zubehör<br>8.2 Erntemaschinen für Wurzelfrüchte<br>8.3 Vereinzelungstechnik<br>8.4 Mähtechnik<br>8.5 Verarbeitungstechnik für Mähprodukte<br>        8.6 Ballenpressen<br>8.7 Sonstiges aus der Gruppe \"Erntemaschinen und - ausrüstung\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2550', 'de', '9. Maschinen und Technik für Auslese, Transportierung, Bearbeitung und Lagerung der Ernte', '9. MASCHINEN UND TECHNIK FÜR AUSLESE, TRANSPORTIERUNG, BEARBEITUNG UND LAGERUNG DER ERNTE<br>9.1 Technik für Getreide<br>9.2 Technik für Kartoffel<br>9.3 Technik für Rübe<br>9.4 Technik für Silo und Stroh<br>9.5 Sonstige Maschinen und Ausrüstungen<br>        9.6 Sonstiges aus der Gruppe \"Maschinen und Technik für Auslese, Transportierung, Bearbeitung und Lagerung der Ernte\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2551', 'de', '10. Maschinen und Technik für Verarbeitung von Obst, Gemüse und anderen Kulturen', '10. MASCHINEN UND TECHNIK FÜR VERARBEITUNG VON OBST, GEMÜSE UND ANDEREN KULTUREN<br>10.1 Ausrüstung für Gartenbau<br>10.2 Gemüseanbau<br>10.3 Ausrüstung für andere Kulturen<br>10.4 Sonstige Maschinen und Ausrüstungen<br>        10.5 Sonstiges aus der Gruppe \"Maschinen und Technik für Verarbeitung von Obst, Gemüse und anderen Kulturen\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2552', 'de', '11. Ersatzteile und Zubehör ', '11. ERSATZTEILE UND ZUBEHÖR<br>11.1 Gehäuse, Zubauten, Kabinen, Sitze<br>11.2 Elektrotechnik und Elektronik für Fahrzeuge/Transportmittel<br>11.3 Antriebsausrüstung<br>11.4 Hydraulik<br>11.5 Reifenmäntel und Räder, Bremsen und Lenkanlagen<br>11.6 Luftkonditionierung<br>11.7 Ersatzteilversorgung<br>        11.8 Sonstiges aus der Gruppe \"Ersatzteile und Zubehör\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2553', 'de', '12. Landwirtschaftliche Elektronik, Mess- und Wiegetechnik', '12. LANDWIRTSCHAFTLICHE ELEKTRONIK, MESS- UND WIEGETECHNIK<br>12.1 Landwirtschaftliche Elektronik<br>12.2 Mess- und Wiegetechnik<br>12.3 Precision Farming (Präzisionsackerbau)<br>12.4 Sonstiges aus der Gruppe \"Landwirtschaftliche Elektronik, Mess - und Wiegetechnik\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2554', 'de', '13. Management, Dienste, Organisationen', '13. MANAGEMENT, DIENSTE, ORGANISATIONEN<br>13.1 Management<br>13.2 Beratung<br>13.3 Software<br>13.4 Finanzierung<br>13.5 Dienste<br>13.6 Technische Bücher, Branchenzeitschriften, Verläge<br>13.7 Forschungen, Wissenschaft – wissenschaftliche Vereine<br>13.8 Schulung<br>13.9 Vereine, Organisationen<br>13.10 Sonstiges aus der Gruppe \"Management, Dienste, Organisationen\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2555', 'de', '14. Sondermessen', '14. SONDERMESSEN<br>14.1 Informationsvorführungen (Ministerien)<br>14.2 Sondervorführungen (Vereine, Gesellschaften)<br>14.3 Gemeinsame Präsentationen (Firmen)<br>14.4 Handelsmessen und Ausstellungen<br>14.5 Sonstiges aus der Gruppe \"Sondermessen\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2556', 'de', '15. Handel mit Gebrauchtanlagen', '15. HANDEL MIT GEBRAUCHTANLAGEN<br>15.1 Gebrauchtanlagen zum Verkauf (Zeitschriften, Internet)<br>15.2 Finanzierung (Gebrauchtanlagen)<br>15.3 Versorgung (Gebrauchtanlagen)<br>15.4 Spediteure (Gebrauchtanlagen)<br>15.5 Zubehör gebraucht (Zeitschriften, Internet)<br>15.6 Zollabfertigung<br>15.7 Sonstiges aus der Gruppe \"Handel mit Gebrauchtanlagen\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2557', 'de', '16. Bioenergetik', '16. BIOENERGETIK<br>16.1 Bioenergieanlagen<br>16.2 Sonstiges aus der Gruppe \"Bioenergetik\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2558', 'de', '17. Biokraftstoff', '17. BIOKRAFTSTOFF<br>17.1 Herstellung und Lagerung<br>17.2 Biokraftstoffverwendung<br>17.3 Biokraftstoffhandel<br>17.4 Sonstiges aus der Gruppe \"Biokraftstoff\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2560', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2561', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2562', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2563', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2564', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2565', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2566', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2567', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2568', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2569', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2570', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2571', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2572', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2573', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2574', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2575', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2576', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2577', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2578', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2579', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2580', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2581', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2582', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2583', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2584', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2585', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2586', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2587', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2588', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2589', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2590', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2591', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2592', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2593', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2594', 'de', 'Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2595', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2596', 'de', 'insgesamt ausgewählt');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2597', 'de', 'Eintrag Ausstellerinformation');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2598', 'de', 'Eintrag Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2599', 'de', 'Logo');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2600', 'de', 'Anforderungen für die Druckversion des Ausstellerkatalogs: Logo – schwarz-weiss, Formate *.eps oder *.tif, Auflösung 300dpi, Bildgrösse 1:1 ');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2601', 'de', 'Werbung im Ausstellerkatalog');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2602', 'de', 'Werbeschaltungen im Ausstellerkatalog werden vom Veranstalter festgelegt. Aussteller, die ihre Werbeschaltung im ersten Teil oder in einem anderen bestimmten Teil des Ausstellerkatalogs platzieren möchten, können die kostenpflichtige Dienstleistung „Sonderveröffentlichung“ zusätzlich buchen. Falls die Dienstleistung „Sonderveröffentlichung“ gebucht wurde, aber kein bestimmter Katalogteil ausgewählt wurde, wird die Werbeschaltung des Ausstellers im ersten Katalogteil veröffentlicht.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2603', 'de', 'Farb-Inserat (140х215 mm)');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2604', 'de', 'Werbeschaltung');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2605', 'de', 'Anforderungen für die Druckversion des Ausstellerkatalogs: Werbeschaltung - CMYK-Abbildung, Grösse 140x215mm, +5 mm umlaufend (für Beschnitt),  Formate *.eps oder *.tif, Auflösung 350dpi');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2606', 'de', 'Dienstleistung \"Sonderveröffentlichung\"');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `placeholder`) VALUES ('2607', 'de', 'Katalogteil', 'Geben Sie bitte den Katalogteil für eine Sonderveröffentlichung ein.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2608', 'de', 'Für Logo- und Werbeschaltung im Ausstellerkatalog sind die entsprechenden Dateien bis 04. August 2016 per E-mail zu senden. Falls die erforderlichen Unterlagen bis zu dieser Frist nicht eintreffen oder den Anforderungen nicht entsprechen, werden keine Beschwerden akzeptiert und es erfolgt keine Kostenerstattung für die bezahlten Leistungen.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `tooltipCustom`) VALUES ('2609', 'de', 'Werbeschaltung in der Zeitschrift AGROREPORT', '<div class=\"header\">ZEITSCHRIFT AGROREPORT</div><div class=\"body\">Agroreport – Fachzeitschrift für Landwirtschaftstechnik (Neuheiten/Exklusiv-Reportagen/ Vergleichstests/unabhängige Tests). Das Angebot gilt für die Aussteller, die Ihre Werbeschaltungen in jenen Ausgaben veröffentlichen, welche während der Messe AGROSALON 2016 erscheinen. Zusätzliche Informationen erhalten Sie per Telefon: +7(495)784-37-56 Durchwahl 251</div>');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2610', 'de', 'Anforderungen für die Druckversion des Ausstellerskatalogs: Inserat – CMYK Abbildung, Grösse 210x297 mm, +5 mm umlaufend (für Beschnitt), Formate *.eps oder *.tif, Auflösung 350dpi. Der Anzeigeentwurf muss mit dem Vermerk „WERBUNG“ versehen sein (entsprechend dem Föderationsgesetz vom 13.März 2006 „Über Werbung“).');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2611', 'de', 'Werbeschaltung A4 in der Ausgabe Nr.4, Juli/August');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2612', 'de', 'Werbeschaltung');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2613', 'de', 'Anzeigenentwürfe für die Zeitschrift Nr.4 sollen bis zum 15.Juli 2016 eingehen. Datum der Veröffentlichung ist der 27.Juli 2016.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2614', 'de', 'Werbeschaltung A4 in der Ausgabe Nr.5, September/Oktober');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2615', 'de', 'Werbeschaltung');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2616', 'de', 'Anzeigenentwürfe für die Zeitschrift Nr.5 sollen bis zum 15.September 2016 eingehen. Datum der Veröffentlichung ist der 27.September 2016.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2617', 'de', 'Werbeschaltung A4 in den Ausgaben Nr.4 und Nr.5');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2618', 'de', 'Werbeschaltung');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2620', 'de', 'Der Antrag ist die Anlage zum Vertrag über die Messeteilnahme und in zwei Exemplaren auszufüllen.<br/><br/>Mit der Unterschrift des Antrages erklärt sich der Aussteller mit der Messeordnung einverstanden und garantiert die Bezahlung der bestellten Dienstleistungen.<br/><br/>Alle Beschwerden, die die Ausführung dieser Bestellung betreffen, werden vom Veranstalter bis zum 7.Oktober 2016 14:00 akzeptiert.Falls eine begründete Beschwerde nicht termingerecht beim Veranstalter eingeht, gelten alle Arbeiten und Dienstleistungen, die in diesem Antrag angegeben sind, als entsprechend qualitativ ausgeführt und vom Aussteller akzeptiert.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2622', 'de', 'Alle Preise verstehen sich inkl. MwSt. 18%. Die Bezahlung erfolgt in Rubel zum Kurs der Zentralbank der Russischen Föderation, der am Tag der Überweisung vom Konto des Ausstellers gilt.');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2630', 'de', 'Aussteller');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2631', 'de', 'Position');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2632', 'de', 'Vollständiger Name');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2633', 'de', 'Veranstalter');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2634', 'de', 'Position');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2635', 'de', 'Vollständiger Name');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2636', 'de', 'Unterschrift');
        INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2637', 'de', 'Unterschrift');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}