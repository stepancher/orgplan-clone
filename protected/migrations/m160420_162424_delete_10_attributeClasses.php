<?php

class m160420_162424_delete_10_attributeClasses extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{attributeclass}} WHERE `id`='670';
			DELETE FROM {{attributeclass}} WHERE `id`='679';
			DELETE FROM {{attributeclass}} WHERE `id`='688';
			DELETE FROM {{attributeclass}} WHERE `id`='697';
			DELETE FROM {{attributeclass}} WHERE `id`='706';
			DELETE FROM {{attributeclass}} WHERE `id`='715';
			DELETE FROM {{attributeclass}} WHERE `id`='724';
			DELETE FROM {{attributeclass}} WHERE `id`='733';
			DELETE FROM {{attributeclass}} WHERE `id`='742';
			DELETE FROM {{attributeclass}} WHERE `id`='751';
	    ";
	}
}