<?php

class m161115_073604_translaes_12_proposal_de extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{trproposalelementclass}} (`id`, `trParentId`, `langId`, `label`, `pluralLabel`) VALUES (NULL, '22', 'de', 'Antrag Nr. 12', 'AUSSTELLER-NAMENSSCHILDER');

            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2304', 'de', 'Senden Sie bitte das ausgefüllte Antragsformular  bis spätestens 15. September 2016 an die Messeleitung. Fax: +7(812)-240-40-40, e-mail: LebedevaUE@expocentr.ru');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2305', 'de', 'Firmenname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2309', 'de', '1. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2310', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2311', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2312', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2317', 'de', '2. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2318', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2319', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2320', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2325', 'de', '3. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2326', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2327', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2328', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2333', 'de', '4. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2334', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2335', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2336', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2341', 'de', '5. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2342', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2343', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2344', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2349', 'de', '6. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2350', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2351', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2352', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2357', 'de', '7. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2358', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2359', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2360', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2365', 'de', '8. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2366', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2367', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2368', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2373', 'de', '9. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2374', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2375', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2376', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2381', 'de', '10. Namensschild');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2382', 'de', 'Vorname');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2383', 'de', 'Name');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2384', 'de', 'Land');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2388', 'de', 'Die Anzahl der Namensschilder, die dem Aussteller zur Verfügung gestellt werden, hängt von der Größe des Messestandes ab und wird wie folgt bestimmt:<br> Messestand 9m² - 3 St. <br> Messestand zwischen 9м² und 30м² - 10 St. <br> \nMessestand zwischen 30м² und 100м² - 20 St. <br> \nMessestand ab 100м² - 20 St. + 2 Namensschilder für jede zusätlziche 100m² ');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2392', 'de', 'Aussteller');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2393', 'de', 'Position');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2394', 'de', 'Name, Vormane');
            INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2395', 'de', 'Unterschrift');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}