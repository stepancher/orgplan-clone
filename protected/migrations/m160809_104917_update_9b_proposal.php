<?php

class m160809_104917_update_9b_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();
		$transaction = Yii::app()->dbProposal->beginTransaction();
		try {
			Yii::app()->dbProposal->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "            
		UPDATE {{attributeclass}} SET `class`='timePicker' WHERE `id`='2685';
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2702', 'organizerPost', 'envVariable');
		INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2703', 'organizerName', 'envVariable');
        ";
	}
}