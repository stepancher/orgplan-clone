<?php

class m160805_110611_delete_tbl_userReport extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->getCreateTable();
        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        echo 'doesn support migrate down, but return true';

        return true;
    }

    public function getCreateTable()
    {
        return '
            DROP TABLE IF EXISTS {{userreport}};
        ';
    }
}