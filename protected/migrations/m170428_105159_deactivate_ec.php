<?php

class m170428_105159_deactivate_ec extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            update tbl_exhibitioncomplex set active = 0 where id in (
                select id from (
                    select ex.id, count(f.id) fCount, ex.name, ex.square, ex.active, ex.trcnName
                    from 
                    (
                        select ec.id, ec.active, trec.name, ec.square, trcn.name trcnName
                        from tbl_exhibitioncomplex ec
                        left join tbl_trexhibitioncomplex trec on trec.trParentId = ec.id and trec.langId = 'ru'
                        left join tbl_city ct on ct.id = ec.cityId
                        left join tbl_region r on r.id = ct.regionId
                        left join tbl_district d on d.id = r.districtId
                        left join tbl_country cn on cn.id = d.countryId
                        left join tbl_trcountry trcn on trcn.trParentId = cn.id and trcn.langId = 'ru'
                        where ec.active = 1 and trcn.name not in ('Россия', 'Германия')
                    ) ex
                    left join tbl_fair f on f.exhibitionComplexId = ex.id and f.active = 1
                    group by ex.id
                    having fCount = 0
                ) tbl1
            );
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}