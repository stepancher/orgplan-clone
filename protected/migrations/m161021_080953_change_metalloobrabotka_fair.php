<?php

class m161021_080953_change_metalloobrabotka_fair extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();

        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trfair}} SET `name`='Protoplan.pro', `description`='Protoplan.pro', `shortUrl`='protoplan-2017' WHERE `id`='17620';
            UPDATE {{trfair}} SET `name`='Protoplan.pro', `description`='Protoplan.pro', `shortUrl`='protoplan-2017' WHERE `id`='13204';
            UPDATE {{trfair}} SET `name`='Protoplan.pro', `description`='Protoplan.pro', `shortUrl`='protoplan-2017' WHERE `id`='12896';
            UPDATE {{userhasorganizer}} SET `organizerId`='4066' WHERE `id`='12';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}