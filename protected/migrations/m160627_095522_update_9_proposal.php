<?php

class m160627_095522_update_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		DELETE FROM {{attributeclass}} WHERE `id`='1022';
		DELETE FROM {{attributeclass}} WHERE `id`='1023';
		DELETE FROM {{attributeclass}} WHERE `id`='1024';
		DELETE FROM {{attributeclass}} WHERE `id`='1025';
		DELETE FROM {{attributeclass}} WHERE `id`='1026';
		DELETE FROM {{attributeclass}} WHERE `id`='1027';
		DELETE FROM {{attributeclass}} WHERE `id`='1028';
		DELETE FROM {{attributeclass}} WHERE `id`='1029';
		DELETE FROM {{attributeclass}} WHERE `id`='1030';
		DELETE FROM {{attributeclass}} WHERE `id`='1031';

		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nБесплатно при заказе до 29.07.2016г. <br>\n1 500 руб. при заказе после 29.07.16г.<br>\nПри заказе после 16.09.16г. стоимость увеличивается на 100%<br>' WHERE `id`='469';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nБесплатно при заказе до 29.07.2016г. <br>\n1 500 руб. при заказе после 29.07.16г.<br>\nПри заказе после 16.09.16г. стоимость увеличивается на 100%<br>' WHERE `id`='1945';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nБесплатно при заказе до 29.07.2016г. <br>\n7 300 руб. при заказе после 29.07.16г.<br>\nПри заказе после 16.09.16г. стоимость увеличивается на 100%' WHERE `id`='470';
		UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint - header\">СТОИМОСТЬ УСЛУГ</div> \nБесплатно при заказе до 29.07.2016г. <br>\n7 300 руб. при заказе после 29.07.16г.<br>\nПри заказе после 16.09.16г. стоимость увеличивается на 100%' WHERE `id`='1946';
		
		INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'n9_1_powered_header_h2_hint', 'string', '0', '580', '577', '1', 'hintNotEdit', '0');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2403', 'ru', 'Бесплатно при заказе до 29.07.2016г.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2403', 'en', 'Бесплатно при заказе до 29.07.2016г.');
		
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='589';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='591';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='593';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='623';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='625';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='627';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='646';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='648';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='798';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='797';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='651';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='653';
		UPDATE {{attributeclass}} SET `dataType`='double' WHERE `id`='656';
		
		UPDATE {{attributeclass}} SET `dataType`='int' WHERE `id`='656';
";
	}
}