<?php

class m161103_061402_move_fair_statistic_to_fairinfo extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{fairinfo}} 
            ADD COLUMN `squareGross` INT(11) NULL DEFAULT NULL AFTER `visitorsForeign`,
            ADD COLUMN `squareNet` INT(11) NULL DEFAULT NULL AFTER `squareGross`,
            ADD COLUMN `members` INT(11) NULL DEFAULT NULL AFTER `squareNet`,
            ADD COLUMN `visitors` INT(11) NULL DEFAULT NULL AFTER `members`,
            ADD COLUMN `statistics` INT(11) NULL DEFAULT NULL AFTER `visitors`,
            ADD COLUMN `statisticsDate` INT(11) NULL DEFAULT NULL AFTER `statistics`;
            
            ALTER TABLE {{fairinfo}} 
            CHANGE COLUMN `statistics` `statistics` TEXT NULL DEFAULT NULL ,
            CHANGE COLUMN `statisticsDate` `statisticsDate` DATE NULL DEFAULT NULL ;
            
            ALTER TABLE {{fair}} 
            ADD COLUMN `infoId` INT(11) NULL DEFAULT NULL AFTER `officialComment`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}
