<?php

class m151203_133558_delete_gradoteka_tables extends CDbMigration
{/**
 * @return bool
 * @throws CDbException
 */
	public function up()
	{
		$sql  = $this->getSql();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
//		$sql = '
//			DROP TABLE IF EXISTS {{gvalue}};
//			DROP TABLE IF EXISTS {{gobjectshasgstatypes}};
//			DROP TABLE IF EXISTS {{gobjects}};
//			DROP TABLE IF EXISTS {{gstatypes}};
//		';
//
//		$transaction = Yii::app()->db->beginTransaction();
//		try
//		{
//			Yii::app()->db->createCommand($sql)->execute();
//			$transaction->commit();
//		}
//		catch(Exception $e)
//		{
//			$transaction->rollback();
//
//			echo $e->getMessage();
//
//			return false;
//		}

		return true;
	}

	public function getSql(){
		return "
			DROP TABLE IF EXISTS {{gvalue}};
			DROP TABLE IF EXISTS {{gobjectshasgstattypes}};
			DROP TABLE IF EXISTS {{gobjects}};
			DROP TABLE IF EXISTS {{gstattypes}};
		";
	}
}