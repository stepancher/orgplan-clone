<?php

class m160418_113629_insert_unitTitle_into_attributeClassProperty_for_proposal_n5 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('567','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('565','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('563','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('560','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('557','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('555','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('553','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('551','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('548','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('545','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('543','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('541','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('539','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('537','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('535','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('531','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('529','шт.', 'unitTitle');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `value`, `class`) VALUES ('526','шт.', 'unitTitle');
	    ";
	}
}