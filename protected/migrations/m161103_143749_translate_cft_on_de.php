<?php

class m161103_143749_translate_cft_on_de extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 2 STANDARDSTAND senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 2 automatisch abgeschlossen' WHERE `id`='4475';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 2 STANDARDSTAND bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4478';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 2 STANDARDSTAND (mit 50% Aufpreis) senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 2 automatisch abgeschlossen' WHERE `id`='4481';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 2 STANDARDSTAND (mit 50% Aufpreis) bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4484';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 2 STANDARDSTAND (mit 100% Aufpreis) senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 2 automatisch abgeschlossen' WHERE `id`='4487';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 2 STANDARDSTAND (mit 100% Aufpreis) bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4490';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 8 INFORMATIONSUNTERSTÜTZUNG. OFFIZIELLER MESSEKATALOG senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 8 automatisch abgeschlossen' WHERE `id`='4493';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 8 INFORMATIONSUNTERSTÜTZUNG. OFFIZIELLER MESSEKATALOG bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4496';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 3 ZUSÄTZLICHE AUSSTATTUNG senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 3 automatisch abgeschlossen' WHERE `id`='4499';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 3 ZUSÄTZLICHE AUSSTATTUNG bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4502';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 3 ZUSÄTZLICHE AUSSTATTUNG (mit 50% Aufpreis) senden', `desc`='Der Vorgagn wird nach Absenden des Antrags Nr. 3 automatisch abgeschlossen' WHERE `id`='4505';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 3 ZUSTÄZLICHE AUSSTATTUNG (mit 50% Aufpreis) bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4508';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 3 ZUSÄTZLICHE AUSSTATTUNG (mit 100% Aufpreis) senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 3 automatisch abgeschlossen' WHERE `id`='4511';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 3 ZUSÄTZLICHE AUSSTATTUNG (mit 100% Aufpreis) bezahlen', `desc`='Klicken Sie nach der Zahlung auf ABSCHLIESSEN' WHERE `id`='4514';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 12 TEILNEHMER-NAMENSSCHILDER senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 12 automatisch abgeschlossen' WHERE `id`='4517';
            UPDATE {{trcalendarfairtasks}} SET `name`='Antrag Nr. 12a VIP NAMENSSCHILDER senden', `desc`='Der Vorgang wird nach Absenden des Antrags Nr. 12 automatisch abgeschlossen' WHERE `id`='4520';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}