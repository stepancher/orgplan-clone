<?php

class m160924_135259_create_gobjecthascountry_table extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE {{gobjecthascountry}} (
             `id` INT NOT NULL AUTO_INCREMENT,
             `objId` INT NOT NULL DEFAULT '0',
             `countryId` INT NOT NULL DEFAULT '0',
             PRIMARY KEY (`id`),
             UNIQUE INDEX `objIdAndCountryId` (`objId`, `countryId`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;
            
            INSERT INTO {{gobjecthascountry}} (`objId`, `countryId`) VALUES ('1', '1');
            UPDATE {{gobjecthascountry}} SET `objId`='379' WHERE `id`='1';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}