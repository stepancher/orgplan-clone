<?php

class m160601_115927_change_exponents_proposals_area_ids extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{attribute}} SET `valueInt`='18072' WHERE `id`='12592';
			UPDATE {{attribute}} SET `valueInt`='18072' WHERE `id`='9933';
			UPDATE {{attribute}} SET `valueInt`='17849' WHERE `id`='21244';
			UPDATE {{attribute}} SET `valueInt`='17849' WHERE `id`='21251';
			UPDATE {{attribute}} SET `valueInt`='19737' WHERE `id`='18170';
			UPDATE {{attribute}} SET `valueInt`='19737' WHERE `id`='18220';
			UPDATE {{attribute}} SET `valueInt`='19898' WHERE `id`='21365';
			UPDATE {{attribute}} SET `valueInt`='19898' WHERE `id`='21415';
			UPDATE {{attribute}} SET `valueInt`='19898' WHERE `id`='21372';
			UPDATE {{attribute}} SET `valueInt`='19898' WHERE `id`='21422';

			UPDATE {{attribute}} SET `valueInt`='1613' WHERE `id`='12591';

			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='16743';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18655';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18705';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18755';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18805';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18855';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='16750';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18662';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18712';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18762';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18812';
			UPDATE {{attribute}} SET `valueInt`='223' WHERE `id`='18862';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='12590';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='18169';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='18219';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='21243';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='21364';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='21414';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='21250';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='21371';
			UPDATE {{attribute}} SET `valueInt`='219' WHERE `id`='21421';
		";
	}
}