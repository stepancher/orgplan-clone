<?php

class m161202_081246_add_new_organizer_to_organizercompany extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{organizercompany}} (`id`) VALUES ('563');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('564');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('565');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('566');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('567');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('568');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('569');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('570');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('571');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('572');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('573');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('574');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('575');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('576');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('577');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('578');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('579');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('580');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('581');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('582');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('583');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('584');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('585');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('586');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('587');
            INSERT INTO {{organizercompany}} (`id`) VALUES ('588');
            
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КАЗАНСКАЯ ЯРМАРКА ОАО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','563','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КАЗАНСКАЯ ЯРМАРКА ОАО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','564','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('НТА (НАЦИОНАЛЬНАЯ ТОРГОВАЯ АССОЦИАЦИЯ)','565','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('EMTG (EXHIBITION MANAGEMENT TECHNOLOGY GROUP)','566','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('B2B CONFERENCE GROUP (BBCG)','567','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('WORLD ART & DANCE ALLIANCE (WADA)','568','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('СИБ-ИНФО ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ (ПРОЕКТ КОМПАНИИ ЗАО «ИНФОЦЕНТР ПЛЮС»)','569','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ОРГКОМИТЕТ КОНФЕРЕНЦИИ РАЗРАБОТЧИКОВ КОМПЬЮТЕРНЫХ ФИРМ (КРИ)','570','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA)','571','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('MEDIA GLOBE (МЕДИА ГЛОБ)','572','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, ','573','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('СПРИНГ МЕССЕ МЕНЕДЖЕМЕНТ ГМБХ (ГЕРМАНИЯ)','574','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('АССОЦИАЦИЯ РОССИЙСКИХ БАНКОВ (АРБ)','575','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('РОССИЙСКАЯ АССОЦИАЦИЯ ПАРКОВ И ПРОИЗВОДИТЕЛЕЙ АТТРАКЦИОНОВ (РАППА)','576','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО','577','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ЭКСПОЦЕНТР АО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','578','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('БИЗОН ОБЪЕДИНЕНИЕ ВЫСТАВОЧНЫХ КОМПАНИЙ, МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО','579','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КОРПОРАЦИЯ ПЕННВЭЛЛ (США, ВЕЛИКОБРИТАНИЯ)','580','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('АССОЦИАЦИЯ РАЗВИТИЯ МЕДИЦИНСКИХ ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ (АРМИТ)','581','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('MEDIA GLOBE (МЕДИА ГЛОБ), КРОКУС ЭКСПО МЕЖДУНАРОДНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР','582','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA), ПРИМЭКСПО ООО В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE','583','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, REED EXHIBITIONS (RUSSIA)','584','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE МОСКВА ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, REED EXHIBITIONS (RUSSIA)','585','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ГАО ВВЦ ОАО, ДЛГ Е.Ф. (НЕМЕЦКОЕ СЕЛЬСКОХОЗЯЙСТВЕННОЕ ОБЩЕСТВО)','586','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA), ГРОТЕК ООО','587','ru');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ), СИБИРЬ ЭКСПО ООО','588','ru');
            
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КАЗАНСКАЯ ЯРМАРКА ОАО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','563','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КАЗАНСКАЯ ЯРМАРКА ОАО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','564','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('НТА (НАЦИОНАЛЬНАЯ ТОРГОВАЯ АССОЦИАЦИЯ)','565','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('EMTG (EXHIBITION MANAGEMENT TECHNOLOGY GROUP)','566','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('B2B CONFERENCE GROUP (BBCG)','567','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('WORLD ART & DANCE ALLIANCE (WADA)','568','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('СИБ-ИНФО ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ (ПРОЕКТ КОМПАНИИ ЗАО «ИНФОЦЕНТР ПЛЮС»)','569','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ОРГКОМИТЕТ КОНФЕРЕНЦИИ РАЗРАБОТЧИКОВ КОМПЬЮТЕРНЫХ ФИРМ (КРИ)','570','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA)','571','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('MEDIA GLOBE (МЕДИА ГЛОБ)','572','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, ','573','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('СПРИНГ МЕССЕ МЕНЕДЖЕМЕНТ ГМБХ (ГЕРМАНИЯ)','574','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('АССОЦИАЦИЯ РОССИЙСКИХ БАНКОВ (АРБ)','575','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('РОССИЙСКАЯ АССОЦИАЦИЯ ПАРКОВ И ПРОИЗВОДИТЕЛЕЙ АТТРАКЦИОНОВ (РАППА)','576','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО','577','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ЭКСПОЦЕНТР АО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','578','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('БИЗОН ОБЪЕДИНЕНИЕ ВЫСТАВОЧНЫХ КОМПАНИЙ, МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО','579','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КОРПОРАЦИЯ ПЕННВЭЛЛ (США, ВЕЛИКОБРИТАНИЯ)','580','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('АССОЦИАЦИЯ РАЗВИТИЯ МЕДИЦИНСКИХ ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ (АРМИТ)','581','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('MEDIA GLOBE (МЕДИА ГЛОБ), КРОКУС ЭКСПО МЕЖДУНАРОДНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР','582','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA), ПРИМЭКСПО ООО В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE','583','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, REED EXHIBITIONS (RUSSIA)','584','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE МОСКВА ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, REED EXHIBITIONS (RUSSIA)','585','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ГАО ВВЦ ОАО, ДЛГ Е.Ф. (НЕМЕЦКОЕ СЕЛЬСКОХОЗЯЙСТВЕННОЕ ОБЩЕСТВО)','586','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA), ГРОТЕК ООО','587','en');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ), СИБИРЬ ЭКСПО ООО','588','en');
            
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КАЗАНСКАЯ ЯРМАРКА ОАО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','563','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КАЗАНСКАЯ ЯРМАРКА ОАО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','564','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('НТА (НАЦИОНАЛЬНАЯ ТОРГОВАЯ АССОЦИАЦИЯ)','565','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('EMTG (EXHIBITION MANAGEMENT TECHNOLOGY GROUP)','566','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('B2B CONFERENCE GROUP (BBCG)','567','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('WORLD ART & DANCE ALLIANCE (WADA)','568','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('СИБ-ИНФО ЭКСПО ВЫСТАВОЧНАЯ КОМПАНИЯ (ПРОЕКТ КОМПАНИИ ЗАО «ИНФОЦЕНТР ПЛЮС»)','569','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ОРГКОМИТЕТ КОНФЕРЕНЦИИ РАЗРАБОТЧИКОВ КОМПЬЮТЕРНЫХ ФИРМ (КРИ)','570','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA)','571','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('MEDIA GLOBE (МЕДИА ГЛОБ)','572','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, ','573','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('СПРИНГ МЕССЕ МЕНЕДЖЕМЕНТ ГМБХ (ГЕРМАНИЯ)','574','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('АССОЦИАЦИЯ РОССИЙСКИХ БАНКОВ (АРБ)','575','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('РОССИЙСКАЯ АССОЦИАЦИЯ ПАРКОВ И ПРОИЗВОДИТЕЛЕЙ АТТРАКЦИОНОВ (РАППА)','576','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО','577','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ЭКСПОЦЕНТР АО, IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ)','578','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('БИЗОН ОБЪЕДИНЕНИЕ ВЫСТАВОЧНЫХ КОМПАНИЙ, МЕЖДУНАРОДНЫЕ КОНГРЕССЫ И ВЫСТАВКИ (МКВ) ООО','579','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('КОРПОРАЦИЯ ПЕННВЭЛЛ (США, ВЕЛИКОБРИТАНИЯ)','580','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('АССОЦИАЦИЯ РАЗВИТИЯ МЕДИЦИНСКИХ ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ (АРМИТ)','581','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('MEDIA GLOBE (МЕДИА ГЛОБ), КРОКУС ЭКСПО МЕЖДУНАРОДНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР','582','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA), ПРИМЭКСПО ООО В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE','583','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE СИБИРЬ ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, REED EXHIBITIONS (RUSSIA)','584','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ITE МОСКВА ВЫСТАВОЧНАЯ КОМПАНИЯ В СОСТАВЕ ГРУППЫ КОМПАНИЙ ITE, REED EXHIBITIONS (RUSSIA)','585','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('ГАО ВВЦ ОАО, ДЛГ Е.Ф. (НЕМЕЦКОЕ СЕЛЬСКОХОЗЯЙСТВЕННОЕ ОБЩЕСТВО)','586','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('REED EXHIBITIONS (RUSSIA), ГРОТЕК ООО','587','de');
            INSERT INTO {{trorganizercompany}} (`name`,`trParentId`,`langId`) VALUES ('IFWEXPO (ИФВЭКСПО ГЕЙДЕЛЬБЕРГ ГМБХ), СИБИРЬ ЭКСПО ООО','588','de');
            
            UPDATE {{organizer}} SET `companyId` = '563' WHERE `id` = '12';
            UPDATE {{organizer}} SET `companyId` = '565' WHERE `id` = '48';
            UPDATE {{organizer}} SET `companyId` = '566' WHERE `id` = '440';
            UPDATE {{organizer}} SET `companyId` = '567' WHERE `id` = '465';
            UPDATE {{organizer}} SET `companyId` = '565' WHERE `id` = '496';
            UPDATE {{organizer}} SET `companyId` = '568' WHERE `id` = '505';
            UPDATE {{organizer}} SET `companyId` = '569' WHERE `id` = '815';
            UPDATE {{organizer}} SET `companyId` = '570' WHERE `id` = '1001';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '1120';
            UPDATE {{organizer}} SET `companyId` = '572' WHERE `id` = '1122';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '1125';
            UPDATE {{organizer}} SET `companyId` = '566' WHERE `id` = '1292';
            UPDATE {{organizer}} SET `companyId` = '573' WHERE `id` = '1293';
            UPDATE {{organizer}} SET `companyId` = '574' WHERE `id` = '1489';
            UPDATE {{organizer}} SET `companyId` = '575' WHERE `id` = '1678';
            UPDATE {{organizer}} SET `companyId` = '576' WHERE `id` = '1825';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '1946';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '1947';
            UPDATE {{organizer}} SET `companyId` = '570' WHERE `id` = '2219';
            UPDATE {{organizer}} SET `companyId` = '569' WHERE `id` = '2500';
            UPDATE {{organizer}} SET `companyId` = '572' WHERE `id` = '2973';
            UPDATE {{organizer}} SET `companyId` = '566' WHERE `id` = '3099';
            UPDATE {{organizer}} SET `companyId` = '577' WHERE `id` = '3123';
            UPDATE {{organizer}} SET `companyId` = '578' WHERE `id` = '3142';
            UPDATE {{organizer}} SET `companyId` = '579' WHERE `id` = '3163';
            UPDATE {{organizer}} SET `companyId` = '577' WHERE `id` = '3188';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '3237';
            UPDATE {{organizer}} SET `companyId` = '580' WHERE `id` = '3240';
            UPDATE {{organizer}} SET `companyId` = '581' WHERE `id` = '3290';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '3304';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '3334';
            UPDATE {{organizer}} SET `companyId` = '582' WHERE `id` = '3343';
            UPDATE {{organizer}} SET `companyId` = '583' WHERE `id` = '3347';
            UPDATE {{organizer}} SET `companyId` = '584' WHERE `id` = '3348';
            UPDATE {{organizer}} SET `companyId` = '585' WHERE `id` = '3349';
            UPDATE {{organizer}} SET `companyId` = '586' WHERE `id` = '3350';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '3587';
            UPDATE {{organizer}} SET `companyId` = '572' WHERE `id` = '3588';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '3590';
            UPDATE {{organizer}} SET `companyId` = '587' WHERE `id` = '3593';
            UPDATE {{organizer}} SET `companyId` = '588' WHERE `id` = '3723';
            UPDATE {{organizer}} SET `companyId` = '571' WHERE `id` = '4056';
            
            UPDATE {{organizer}} SET `companyId`='260' WHERE `id`='4074';
            UPDATE {{organizer}} SET `companyId`='84' WHERE `id`='4075';
            UPDATE {{organizer}} SET `companyId`='84' WHERE `id`='4076';
            UPDATE {{organizer}} SET `companyId`='84' WHERE `id`='4077';
            UPDATE {{organizer}} SET `companyId`='84' WHERE `id`='4078';
            UPDATE {{organizer}} SET `companyId`='84' WHERE `id`='4079';
            UPDATE {{organizer}} SET `companyId`='202' WHERE `id`='4080';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}