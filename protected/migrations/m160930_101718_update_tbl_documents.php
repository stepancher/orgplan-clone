<?php

class m160930_101718_update_tbl_documents extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trdocuments}} SET `description`=NULL WHERE `id`='72';
            UPDATE {{trdocuments}} SET `description`='Анкета посетителя стенда' WHERE `id`='78';
            UPDATE {{trdocuments}} SET `description`='Инструктаж сотрудников' WHERE `id`='79';
            UPDATE {{trdocuments}} SET `description`='Меню кофе-брейка, фуршета' WHERE `id`='80';
            UPDATE {{trdocuments}} SET `description`='Перечень продуктов на стенд' WHERE `id`='81';
            UPDATE {{trdocuments}} SET `description`='Цель PR, ВЫСТРАИВАНИЕ БРЕНДА' WHERE `id`='82';
            UPDATE {{trdocuments}} SET `description`='Цель МАРКЕТИНГОВЫЕ ИССЛЕДОВАНИЯ' WHERE `id`='83';
            UPDATE {{trdocuments}} SET `description`='Цель ОТНОШЕНИЕ С ПОТРЕБИТЕЛЯМИ' WHERE `id`='84';
            UPDATE {{trdocuments}} SET `description`='Цель ПОДДЕРЖКА КАНАЛОВ СБЫТА' WHERE `id`='85';
            UPDATE {{trdocuments}} SET `description`='Цель ПРОДАЖА' WHERE `id`='86';
            UPDATE {{trdocuments}} SET `description`='Список экспонатов' WHERE `id`='87';
            UPDATE {{trdocuments}} SET `description`='Форма обработки контактов на стенде' WHERE `id`='88';
            UPDATE {{trdocuments}} SET `description`='Чек-лист демонстрации продукции на стенде' WHERE `id`='89';
            UPDATE {{trdocuments}} SET `description`='Чек-лист конкурса' WHERE `id`='90';
            UPDATE {{trdocuments}} SET `description`='Чек-лист конференции' WHERE `id`='91';
            UPDATE {{trdocuments}} SET `description`='Чек-лист мастер-класса' WHERE `id`='92';
            UPDATE {{trdocuments}} SET `description`='Чек-лист меню питания на стенде' WHERE `id`='93';
            UPDATE {{trdocuments}} SET `description`='Чек-лист мероприятий на выставке' WHERE `id`='94';
            UPDATE {{trdocuments}} SET `description`='Чек-лист пресс-конференции' WHERE `id`='96';
            UPDATE {{trdocuments}} SET `description`='Чек-лист пресс-релиза' WHERE `id`='97';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}