<?php

class m160627_082131_translates_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
		UPDATE {{trattributeclass}} SET `label`='Количество бейджей, предоставляемых Экспоненту зависит от площади его стенда и определяется следующим образом:<br> \nСтенд 9м² - 3 шт. <br> \nСтенд от 9м² до 30м² - 10 шт. <br> \nСтенд от 30м² до 100м² - 20 шт. <br> \nСтенд более 100м² - 20 шт. + 2 бейджа на каждые дополнительные 100м² площади стенда.' WHERE `id`='2442';

		INSERT INTO {{trproposalelementclass}} (`trParentId`, `langId`, `label`, `pluralLabel`) VALUES ('22', 'en', 'APPLICATION No.12', 'BADGES');

		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2304', 'en', 'This form should be filled in until Sept. 15, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2305', 'en', 'Company');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2309', 'en', '1. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2310', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2311', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2312', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2317', 'en', '2. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2318', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2319', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2320', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2325', 'en', '3. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2326', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2327', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2328', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2333', 'en', '4. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2334', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2335', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2336', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2341', 'en', '5. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2342', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2343', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2344', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2349', 'en', '6. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2350', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2351', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2352', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2357', 'en', '7. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2358', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2359', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2360', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2365', 'en', '8. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2366', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2367', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2368', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2373', 'en', '9. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2374', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2375', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2376', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2381', 'en', '10. Badge');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2382', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2383', 'en', 'Surname');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2384', 'en', 'Country');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2388', 'en', 'Available number of badges depends on the area of Exhibitor\'s stand. It\'s defined as follows: <br> stand 9 sq.m. - 3 pcs. <br> Stand from 9 sq.m. up to 30 sq.m. - 10 pcs. <br> Stand from 30 sq.m. up to 100 sq.m. - 20 pcs.<br>Stand more than 100 sq.m. - 20 pcs.+2 badges per every additional 100 sq.m.');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2392', 'en', 'Exhibitor');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2393', 'en', 'position');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2394', 'en', 'Name');
		INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`) VALUES ('2395', 'en', 'Signature');

		UPDATE {{proposalelementclass}} SET `active`='1' WHERE `id`='21';
		UPDATE {{proposalelementclass}} SET `active`='1' WHERE `id`='22';
";
	}
}