<?php

class m160924_123013_update_analyticscharttablepreview extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{analyticschartpreview}}
              CHANGE COLUMN `stat` `stat` VARCHAR(256) NULL DEFAULT NULL;

            UPDATE {{analyticschartpreview}} SET `stat`='56d030def7a9ad0b078f11aa' WHERE `id`='1';
            UPDATE {{analyticschartpreview}} SET `stat`='56d0311df7a9add4068f7bb7' WHERE `id`='2';
            UPDATE {{analyticschartpreview}} SET `stat`='56ec84ee74d8974605a991cd' WHERE `id`='3';
            UPDATE {{analyticschartpreview}} SET `stat`='56d2f8d4f7a9ad68148d1530' WHERE `id`='4';
            UPDATE {{analyticschartpreview}} SET `stat`='56d2f8aff7a9ad68148caaa4' WHERE `id`='5';
            UPDATE {{analyticschartpreview}} SET `stat`='56fd125409eed4ef758b59da' WHERE `id`='6';
            UPDATE {{analyticschartpreview}} SET `stat`='577e7c79bd4c562e658b4684' WHERE `id`='7';
            UPDATE {{analyticschartpreview}} SET `stat`='577e6f1fbd4c5654298b736b' WHERE `id`='8';
            UPDATE {{analyticschartpreview}} SET `stat`='577e6f17bd4c5654298b540e' WHERE `id`='9';
            
            ALTER TABLE {{analyticschartpreview}}
            ADD COLUMN `gType` VARCHAR(45) NULL AFTER `position`;

            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='1';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='2';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='3';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='4';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='5';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='6';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='7';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='8';
            UPDATE {{analyticschartpreview}} SET `gType`='region' WHERE `id`='9';
            
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `industryId`, `gType`) VALUES ('56d030def7a9ad0b078f11aa', 'Объем сельскохозяйственной отрасли  РФ', '11', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `industryId`, `gType`) VALUES ('5784b485bd4c56591c8b6176', 'Доля растениеводства в РФ', '11', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `industryId`, `gType`) VALUES ('5784b484bd4c56591c8b5f4f', 'Доля животноводства в РФ', '11', 'country');
            
            INSERT INTO {{documents}} (`type`, `active`) VALUES ('2', '1');
            INSERT INTO {{documents}} (`type`, `active`) VALUES ('2', '1');
            INSERT INTO {{documents}} (`type`, `active`) VALUES ('2', '1');
            INSERT INTO {{documents}} (`type`, `active`) VALUES ('2', '1');
            
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('38', 'ru', 'Схема проезда', '/static/documents/fairs/stand_plan_ru.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('39', 'ru', 'Правила участия', '/static/documents/fairs/stand_plan_ru.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('40', 'ru', 'Каталог услуг', '/static/documents/fairs/stand_plan_ru.pdf');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `value`) VALUES ('41', 'ru', 'Правила организации работ застройщиков', '/static/documents/fairs/stand_plan_ru.pdf');

            UPDATE {{analyticschartpreview}} SET `header`='Продукция Сельского \\n хозяйства всех \\n категорий', `measure`='млн.рублей' WHERE `id`='1';
            UPDATE {{analyticschartpreview}} SET `header`='Производство зерна', `measure`='тыс.центнеров' WHERE `id`='2';
            UPDATE {{analyticschartpreview}} SET `header`='Скот и птица в убойном весе', `measure`='тыс.тонн' WHERE `id`='3';
            
            DELETE FROM {{analyticschartpreview}} WHERE `id`='4';
            DELETE FROM {{analyticschartpreview}} WHERE `id`='5';
            DELETE FROM {{analyticschartpreview}} WHERE `id`='6';
            DELETE FROM {{analyticschartpreview}} WHERE `id`='7';
            DELETE FROM {{analyticschartpreview}} WHERE `id`='8';
            DELETE FROM {{analyticschartpreview}} WHERE `id`='9';
            
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e467377d89702498cc8a3', 'Производства стали', 'тыс. т.', '3', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e45b677d89702498c8a03', 'Производства черных металлов', 'тыс. т.', '3', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e467977d89702498cca97', 'Производство металлорежущих станков', 'шт', '3', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('577e6f13bd4c5654298b4569', '1', '4', 'Добыча металлов, минералов, строиматериалов', '%', '#4b77be', '3', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('572facee73d897e665f8a38f', '1', '4', 'Рентабельность организаций', '%', '#4b77be', '3', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `color`, `industryId`, `position`, `gType`) VALUES ('500006', '1', '4', 'Геологоразвед. работы (металлы)', '#4b77be', '3', '3', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('56d2f8d4f7a9ad68148d1530', 'Строительный объем зданий', 'млн, метров кубических', '2', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('56d2f8aff7a9ad68148caaa4', 'Число построенных зданий  ', 'ед', '2', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('577e6f1ebd4c5654298b6eb8', 'Строительных организаций', 'ед', '2', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56d2f8d4f7a9ad68148d1530', '1', '4', 'Строительный объем зданий', 'млн, метров кубических', '#4b77be', '2', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56d2f8aff7a9ad68148caaa4', '1', '4', 'Число построенных зданий  ', 'ед', '#4b77be', '2', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('577e6f1ebd4c5654298b6eb8', '1', '4', 'Строительных организаций', 'ед', '#4b77be', '2', '3', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e46d877d89702498d0936', 'Производство фанеры', 'куб. метр', '5', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e44c477d89702498bfc67', 'Производство лесоматериалов', ' тыс. куб. метров', '5', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e46e877d89702498d12eb', 'Производство  целлюлозы', 'тонны', '5', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('577e7c79bd4c562e658b4684', '1', '4', 'Производство древесины необработанной', 'тыс.плотн. метров кубических', '#f6a800', '5', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('577e6f17bd4c5654298b540e', '1', '4', 'Лесистость территории', '%', '#f6a800', '5', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('577e6f1fbd4c5654298b736b', '1', '4', 'Число предприятий', 'ед', '#f6a800', '5', '3', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('56d2f8c2f7a9ad68148cdff1', 'Общая площадь введенных зданий', 'млн. м.квадратных', '1', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('500004', 'Потребительские кредиты', 'млн.руб', '1', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('500003', 'Численность населения с доходом выше прожиточного минимума', 'млн. чел', '1', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56d2f8c2f7a9ad68148cdff1', '1', '4', 'Общая площадь введенных зданий', 'млн. м.квадратных', '#f6a800', '1', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('500004', '1', '4', 'Потребительские кредиты', 'млн.руб', '#f6a800', '1', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('500003', '1', '4', 'Численность населения с доходом выше прожиточного минимума', 'млн. чел', '#f6a800', '1', '3', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e452577d89702498c346f', 'Добыча нефти', 'тыс тонн', '37', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e43f477d89702498b771f', 'Добыча газа', 'млн. метров кубических', '37', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('577e6f1cbd4c5654298b64e0', 'Число организаций', 'ед.', '37', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('571e452577d89702498c346f', '1', '4', 'Добыча нефти', 'тыс тонн', '#2b98d5', '37', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('571e43f477d89702498b771f', '1', '4', 'Добыча газа', 'млн. метров кубических', '#2b98d5', '37', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('577e6f1cbd4c5654298b64e0', '1', '4', 'Число организаций', 'ед.', '#2b98d5', '37', '3', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e46e777d89702498d109a', 'Производство хлеба', 'тонн', '13', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('56d02ef7f7a9ad01088b5796', 'Производство молока', 'тонн', '13', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('571e443077d89702498ba0e3', 'Производство колбасных изделий', 'тонн', '13', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('571e46e777d89702498d109a', '1', '4', 'Производство хлеба', 'тонн', '#a0d468', '13', '1', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('56d02ef7f7a9ad01088b5796', '1', '4', 'Производство молока', 'тонн', '#a0d468', '13', '2', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `position`, `gType`) VALUES ('571e443077d89702498ba0e3', '1', '4', 'Производство колбасных изделий', 'тонн', '#a0d468', '13', '3', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('572b3ec076d89734208b4ae7', 'Оборот розничной торговли', 'млн. руб', '14', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('577e7e2fbd4c5654298b8ad2', 'Потребительские расходы', 'млн. руб', '14', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `header`, `measure`, `industryId`, `gType`) VALUES ('577e7e2ebd4c5654298b8509', 'Пищевые продукты в розничной торговле', '%', '14', 'country');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `gType`) VALUES ('572b3ec076d89734208b4ae7', '1', '4', 'Оборот розничной торговли', 'млн. руб', '#f6a800', '14', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `gType`) VALUES ('577e7e2fbd4c5654298b8ad2', '1', '4', 'Потребительские расходы', 'млн. руб', '#f6a800', '14', 'region');
            INSERT INTO {{analyticschartpreview}} (`stat`, `widget`, `widgetType`, `header`, `measure`, `color`, `industryId`, `gType`) VALUES ('577e7e2ebd4c5654298b8509', '1', '4', 'Пищевые продукты в розничной торговле', '%', '#f6a800', '14', 'region');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}