<?php

class m170221_094526_copy_associationhasassociation_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_associationhasassociation_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_associationhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE ass_first INT DEFAULT 0;
                DECLARE ass_second INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT ass1.id, ass2.id FROM {$expodata}.{{exdbassociationhasassociation}} eaha
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass1 ON eass1.id = eaha.associationId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} eass2 ON eass2.id = eaha.hasAssociationId
                                        LEFT JOIN {$db}.{{association}} ass1 ON ass1.exdbId = eass1.exdbId
                                        LEFT JOIN {$db}.{{association}} ass2 ON ass2.exdbId = eass2.exdbId
                                        LEFT JOIN {$db}.{{associationhasassociation}} aha ON aha.associationId = ass1.id AND aha.hasAssociationId = ass2.id
                                        WHERE aha.id IS NULL ORDER BY ass1.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO ass_first, ass_second;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{associationhasassociation}} (`associationId`,`hasAssociationId`) VALUES (ass_first, ass_second);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_associationhasassociation_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}