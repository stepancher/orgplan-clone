<?php

class m150904_094645_alter_table_product_create_table_productprice extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getProductPrice();
		$sql .= $this->getAlterTableProduct();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{productprice}};
		  	ALTER TABLE {{product}}
			ADD COLUMN `cost` INT(11) NULL DEFAULT NULL AFTER `param`,
			ADD COLUMN `cityId` INT(11) NULL DEFAULT NULL AFTER `categoryId`;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getProductPrice(){
		return '
			DROP TABLE IF EXISTS {{productprice}};
			CREATE TABLE {{productprice}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `cost` int(11) DEFAULT NULL,
			  `cityId` int(11) NOT NULL,
			  `productId` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `unique` (`cityId`,`productId`)
			) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
		';
	}

	public function getAlterTableProduct(){
		return '
			ALTER TABLE {{product}}
			DROP FOREIGN KEY `fk_tbl_product_1`,
			DROP FOREIGN KEY `fk_tbl_product_2`;
			ALTER TABLE {{product}}
			DROP COLUMN `cityId`,
			DROP COLUMN `cost`,
			DROP INDEX `fk_tbl_product_2_idx` ,
			DROP INDEX `fk_tbl_product_1_idx` ;
		';
	}
}