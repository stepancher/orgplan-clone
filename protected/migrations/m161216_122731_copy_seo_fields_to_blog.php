<?php

class m161216_122731_copy_seo_fields_to_blog extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_seo_fields_to_blog`()
            BEGIN
                
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE seo_title TEXT DEFAULT '';
                DECLARE seo_description TEXT DEFAULT '';
                DECLARE blog_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT distinct s.title, s.description, b.id from {{seo}} s
                                        left join {{blog}} b on s.refId = b.id
                                        where b.id is not null
                                        and s.url like '%blog%'
                                        and s.description != ''
                                        and s.lang = 'ru';
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    
                    read_loop: LOOP
                    
                    FETCH copy INTO seo_title, seo_description, blog_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{blog}} SET `title` = seo_title, `description` = seo_description WHERE `id` = blog_id;
                    COMMIT;
                    
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL copy_seo_fields_to_blog();
            DROP PROCEDURE IF EXISTS `copy_seo_fields_to_blog`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}