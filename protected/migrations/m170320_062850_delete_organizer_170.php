<?php

class m170320_062850_delete_organizer_170 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DELETE FROM {{organizercompany}} WHERE `id` = '170';
            DELETE FROM {{organizer}} WHERE `id` = '406';
            
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20747';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20748';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20749';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20750';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20751';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20752';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20753';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20754';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20755';
            DELETE FROM {{organizercontactlist}} WHERE `id` = '20756';
            
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2936';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2937';
            DELETE FROM {{fairhasorganizer}} WHERE `id` = '2938';
            
            DELETE FROM {{fair}} WHERE `id` = '4389';
            DELETE FROM {{fair}} WHERE `id` = '4390';
            DELETE FROM {{fair}} WHERE `id` = '4391';
            
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14379';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14380';
            DELETE FROM {{fairhasindustry}} WHERE `id` = '14381';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}