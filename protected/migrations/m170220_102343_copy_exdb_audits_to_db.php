<?php

class m170220_102343_copy_exdb_audits_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_audits_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_audits_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE aud_contact TEXT DEFAULT '';
                DECLARE aud_name TEXT DEFAULT '';
                DECLARE aud_lang TEXT DEFAULT '';
                DECLARE temp_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT tau.exdbId, tau.contacts_en, trtau.name, trtau.langId FROM {$expodata}.{{exdbaudit}} tau
                                            LEFT JOIN {$expodata}.{{exdbtraudit}} trtau ON trtau.trParentId = tau.id
                                            LEFT JOIN {$db}.{{audit}} au ON au.exdbId = tau.id
                                        WHERE au.id IS NULL
                                        ORDER BY tau.exdbId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO aud_id, aud_contact, aud_name, aud_lang;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                        IF aud_id != temp_id THEN
                            START TRANSACTION;
                                INSERT INTO {$db}.{{audit}} (`exdbId`,`contacts_en`) VALUES (aud_id, aud_contact);
                            COMMIT;
                            SET @last_insert_id := LAST_INSERT_ID();
                            SET temp_id := aud_id;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{traudit}} (`trParentId`,`name`,`langId`) VALUES (@last_insert_id, aud_name, aud_lang);
                        COMMIT;
                    
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_audits_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}