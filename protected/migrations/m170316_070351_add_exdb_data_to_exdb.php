<?php

class m170316_070351_add_exdb_data_to_exdb extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP TABLE IF EXISTS {$expodata}.{{exdbassociation}};
            CREATE TABLE {$expodata}.{{exdbassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbContacts` TEXT NULL DEFAULT NULL,
              `exdbContactsRaw` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            DROP TABLE IF EXISTS {$expodata}.{{exdbassociationhasassociation}};
            CREATE TABLE {$expodata}.{{exdbassociationhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `associationId` INT(11) NULL DEFAULT NULL,
              `hasAssociationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            DROP TABLE IF EXISTS {$expodata}.{{exdbaudit}};
            CREATE TABLE {$expodata}.{{exdbaudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `active` INT(11) NULL DEFAULT NULL,
              `contacts_en` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            DROP TABLE IF EXISTS {$expodata}.{{exdbaudithasassociation}};
            CREATE TABLE {$expodata}.{{exdbaudithasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `auditId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));

            DROP TABLE IF EXISTS {$expodata}.{{exdbexhibitioncomplex}};
            CREATE TABLE {$expodata}.{{exdbexhibitioncomplex}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `name` VARCHAR(255) NULL DEFAULT NULL,
              `cityId` INT(11) NULL DEFAULT NULL,
              `coordinates` VARCHAR(255) NULL DEFAULT NULL,
              `contacts` TEXT NULL DEFAULT NULL,
              `raw` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbexhibitioncomplexhasassociation}};
            CREATE TABLE {$expodata}.{{exdbexhibitioncomplexhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exhibitionComplexId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfair}};
            CREATE TABLE {$expodata}.{{exdbfair}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` BIGINT(20) NULL DEFAULT NULL,
              `exdbFrequency` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `name_de` TEXT NULL DEFAULT NULL,
              `exhibitionComplexId` INT(11) NULL DEFAULT NULL,
              `beginDate` DATE NULL DEFAULT NULL,
              `endDate` DATE NULL DEFAULT NULL,
              `beginMountingDate` DATE NULL DEFAULT NULL,
              `endMountingDate` DATE NULL DEFAULT NULL,
              `beginDemountingDate` DATE NULL DEFAULT NULL,
              `endDemountingDate` DATE NULL DEFAULT NULL,
              `exdbBusinessSectors` TEXT NULL DEFAULT NULL,
              `exdbCosts` TEXT NULL DEFAULT NULL,
              `exdbShowType` TEXT NULL DEFAULT NULL,
              `site` TEXT NULL DEFAULT NULL,
              `storyId` INT(11) NULL DEFAULT NULL,
              `infoId` INT(11) NULL DEFAULT NULL,
              `exdbRaw` TEXT NULL DEFAULT NULL,
              `exdbFirstYearShow` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairhasassociation}};
            CREATE TABLE {$expodata}.{{exdbfairhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `fairId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairhasaudit}};
            CREATE TABLE {$expodata}.{{exdbfairhasaudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `fairId` INT(11) NULL DEFAULT NULL,
              `auditId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairhasorganizer}};
            CREATE TABLE {$expodata}.{{exdbfairhasorganizer}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `fairId` INT(11) NULL DEFAULT NULL,
              `organizerId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbfairstatistic}};
            CREATE TABLE {$expodata}.{{exdbfairstatistic}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `exdbCrc` BIGINT(20) NULL DEFAULT NULL,
              `areaSpecialExpositions` INT(11) NULL DEFAULT NULL,
              `squareNet` INT(11) NULL DEFAULT NULL,
              `members` INT(11) NULL DEFAULT NULL,
              `exhibitorsForeign` INT(11) NULL DEFAULT NULL,
              `exhibitorsLocal` INT(11) NULL DEFAULT NULL,
              `visitors` INT(11) NULL DEFAULT NULL,
              `visitorsForeign` INT(11) NULL DEFAULT NULL,
              `visitorsLocal` INT(11) NULL DEFAULT NULL,
              `statistics` TEXT NULL DEFAULT NULL,
              `statisticsDate` TEXT NULL DEFAULT NULL,
              `exdbCountriesCount` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdborganizercompany}};
            CREATE TABLE {$expodata}.{{exdborganizercompany}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `exdbId` INT(11) NULL DEFAULT NULL,
              `active` INT(11) NULL DEFAULT NULL,
              `linkToTheSiteOrganizers` VARCHAR(255) NULL DEFAULT NULL,
              `phone` VARCHAR(255) NULL DEFAULT NULL,
              `email` VARCHAR(255) NULL DEFAULT NULL,
              `cityId` INT(11) NULL DEFAULT NULL,
              `coordinates` VARCHAR(255) NULL DEFAULT NULL,
              `contacts_en` TEXT NULL DEFAULT NULL,
              `contacts_raw_en` TEXT NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdborganizercompanyhasassociation}};
            CREATE TABLE {$expodata}.{{exdborganizercompanyhasassociation}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `organizerCompanyId` INT(11) NULL DEFAULT NULL,
              `associationId` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbtraudit}};
            CREATE TABLE {$expodata}.{{exdbtraudit}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `langId` VARCHAR(55) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
            
            DROP TABLE IF EXISTS {$expodata}.{{exdbtrorganizercompany}};
            CREATE TABLE {$expodata}.{{exdbtrorganizercompany}} (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `trParentId` INT(11) NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `langId` VARCHAR(255) NULL DEFAULT NULL,
              PRIMARY KEY (`id`));
              
            ALTER TABLE {$db}.{{fair}} 
            ADD COLUMN `exdbFirstYearShow` TEXT NULL DEFAULT NULL AFTER `exdbRaw`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}