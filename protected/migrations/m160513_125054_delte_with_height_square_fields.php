<?php

class m160513_125054_delte_with_height_square_fields extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{attributeclass}} WHERE `id`='325';
			DELETE FROM {{attributeclass}} WHERE `id`='326';
			UPDATE {{attributeclass}} SET `class`='double' WHERE `id`='327';
	    ";
	}
}