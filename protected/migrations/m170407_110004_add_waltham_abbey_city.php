<?php

class m170407_110004_add_waltham_abbey_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('399', 'waltham-abbey');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1548', 'ru', 'Уолтем Абби');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1548', 'en', 'Waltham Abbey');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1548', 'de', 'Waltham Abbey');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}