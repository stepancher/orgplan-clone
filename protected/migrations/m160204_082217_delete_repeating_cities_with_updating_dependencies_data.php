<?php

class m160204_082217_delete_repeating_cities_with_updating_dependencies_data extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='203';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='230';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='285';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='286';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='287';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='294';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='321';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='329';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='333';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='371';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='378';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='389';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='403';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='233';
			UPDATE {{user}} SET `cityId`=NULL WHERE `id`='235';

			UPDATE {{exhibitioncomplex}} SET `cityId`='223' WHERE `id`='294';

			DELETE FROM {{city}} WHERE `id`='237';
			DELETE FROM {{trcity}} WHERE `id`='220';

			UPDATE {{exhibitioncomplex}} SET `cityId`='219' WHERE `id`='324';
			DELETE FROM {{city}} WHERE `id`='233';
			DELETE FROM {{trcity}} WHERE `id`='216';

			UPDATE {{exhibitioncomplex}} SET `cityId`='218' WHERE `id`='325';
			DELETE FROM {{city}} WHERE `id`='232';
			DELETE FROM {{trcity}} WHERE `id`='215';

			UPDATE {{exhibitioncomplex}} SET `cityId`='210' WHERE `id`='380';
			DELETE FROM {{city}} WHERE `id`='224';
			DELETE FROM {{trcity}} WHERE `id`='207';

			UPDATE {{exhibitioncomplex}} SET `cityId`='217' WHERE `id`='326';
			DELETE FROM {{city}} WHERE `id`='231';
			DELETE FROM {{trcity}} WHERE `id`='214';

			UPDATE {{exhibitioncomplex}} SET `cityId`='212' WHERE `id`='348';
			DELETE FROM {{city}} WHERE `id`='229';
			DELETE FROM {{trcity}} WHERE `id`='212';
			UPDATE {{exhibitioncomplex}} SET `cityId`='212' WHERE `id`='354';
			DELETE FROM {{city}} WHERE `id`='226';
			DELETE FROM {{trcity}} WHERE `id`='209';

			DELETE FROM {{city}} WHERE `id`='215';
			DELETE FROM {{trcity}} WHERE `id`='198';

			UPDATE {{exhibitioncomplex}} SET `cityId`='213' WHERE `id`='352';
			DELETE FROM {{city}} WHERE `id`='227';
			DELETE FROM {{trcity}} WHERE `id`='210';

			UPDATE {{exhibitioncomplex}} SET `cityId`='214' WHERE `id`='351';
			DELETE FROM {{city}} WHERE `id`='228';
			DELETE FROM {{trcity}} WHERE `id`='211';

			UPDATE {{exhibitioncomplex}} SET `cityId`='216' WHERE `id`='336';
			DELETE FROM {{city}} WHERE `id`='230';
			DELETE FROM {{trcity}} WHERE `id`='213';

			DELETE FROM {{city}} WHERE `id`='161';

			UPDATE {{exhibitioncomplex}} SET `cityId`='221' WHERE `id`='322';
			DELETE FROM {{city}} WHERE `id`='235';
			DELETE FROM {{trcity}} WHERE `id`='218';

			DELETE FROM {{city}} WHERE `id`='146';
			DELETE FROM {{trcity}} WHERE `id`='362';

			UPDATE {{exhibitioncomplex}} SET `cityId`='220' WHERE `id`='320';
			DELETE FROM {{city}} WHERE `id`='236';
			DELETE FROM {{trcity}} WHERE `id`='219';

			UPDATE {{exhibitioncomplex}} SET `cityId`='220' WHERE `id`='323';
			DELETE FROM {{city}} WHERE `id`='234';
			DELETE FROM {{trcity}} WHERE `id`='217';

			DELETE FROM {{city}} WHERE `id`='222';
			DELETE FROM {{trcity}} WHERE `id`='205';

			UPDATE {{exhibitioncomplex}} SET `cityId`='211' WHERE `id`='375';
			DELETE FROM {{city}} WHERE `id`='225';
			DELETE FROM {{trcity}} WHERE `id`='208';

			DELETE FROM {{city}} WHERE `id`='209';
			DELETE FROM {{city}} WHERE `id`='197';
			DELETE FROM {{city}} WHERE `id`='198';
			DELETE FROM {{city}} WHERE `id`='199';
			DELETE FROM {{city}} WHERE `id`='200';
			DELETE FROM {{city}} WHERE `id`='201';
			DELETE FROM {{city}} WHERE `id`='202';
			DELETE FROM {{city}} WHERE `id`='203';
			DELETE FROM {{city}} WHERE `id`='204';
			DELETE FROM {{city}} WHERE `id`='205';
			DELETE FROM {{city}} WHERE `id`='206';
			DELETE FROM {{city}} WHERE `id`='207';
			DELETE FROM {{city}} WHERE `id`='208';
		";
	}
}