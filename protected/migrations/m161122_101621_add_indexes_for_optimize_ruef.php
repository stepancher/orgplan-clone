<?php

class m161122_101621_add_indexes_for_optimize_ruef extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{region}} 
            ADD INDEX `districtIdIdx` (`districtId` ASC);
            
            ALTER TABLE {{city}} 
            ADD INDEX `regionIdIdx` (`regionId` ASC);
            
            ALTER TABLE {{exhibitioncomplex}} 
            ADD INDEX `cityIdIdx` (`cityId` ASC);
            
            ALTER TABLE {{fair}} 
            ADD INDEX `exhibitionComplexIdIdx` (`exhibitionComplexId` ASC);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}