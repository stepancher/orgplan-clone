<?php

class m160602_135432_dop_old_pareas_and_rename_new extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			DROP TABLE {{pregion}};

			ALTER TABLE {{pregion_}}
			RENAME TO  {{pregion}} ;



			DROP TABLE {{pcountry}};

			ALTER TABLE {{pcountry_}}
			RENAME TO  {{pcountry}} ;



			DROP TABLE {{pcity}};

			ALTER TABLE {{pcity_}}
			RENAME TO  {{pcity}} ;




			ALTER TABLE {{pcountry}}
			CHANGE COLUMN `country_name_ru` `countryNameRu` VARCHAR(50) NOT NULL ,
			CHANGE COLUMN `country_name_en` `countryNameEn` VARCHAR(50) NOT NULL ;



			ALTER TABLE {{pregion}}
			CHANGE COLUMN `id_country` `idCountry` MEDIUMINT(8) UNSIGNED NOT NULL ,
			CHANGE COLUMN `region_name_ru` `regionNameRu` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `region_name_en` `regionNameEn` VARCHAR(255) NOT NULL ;



			ALTER TABLE {{pcity}}
			CHANGE COLUMN `id_region` `idRegion` INT(10) UNSIGNED NOT NULL ,
			CHANGE COLUMN `id_country` `idCountry` MEDIUMINT(8) UNSIGNED NOT NULL ,
			CHANGE COLUMN `city_name_ru` `cityNameRu` VARCHAR(255) NULL DEFAULT NULL ,
			CHANGE COLUMN `city_name_en` `cityNameEn` VARCHAR(255) NOT NULL ;


			CREATE TABLE {{trpcity}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `name` VARCHAR(200) NULL,
			  PRIMARY KEY (`id`));


			CREATE TABLE {{trpregion}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `name` VARCHAR(200) NULL,
			PRIMARY KEY (`id`));

			CREATE TABLE {{trpcountry}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `trParentId` INT NULL,
			  `langId` VARCHAR(45) NULL,
			  `name` VARCHAR(200) NULL,
			PRIMARY KEY (`id`));



			insert into {{trpcity}} (trParentId, langId, name)
			select id, 'en', cityNameEn from {{pcity}};

			insert into {{trpcity}} (trParentId, langId, name)
			select id, 'ru', cityNameRu from {{pcity}};


			insert into {{trpcountry}} (trParentId, langId, name)
			select id, 'en', countryNameEn from {{pcountry}};

			insert into {{trpcountry}} (trParentId, langId, name)
			select id, 'ru', countryNameRu from {{pcountry}};


			insert into {{trpregion}} (trParentId, langId, name)
			select id, 'en', regionNameEn from {{pregion}};

			insert into {{trpregion}} (trParentId, langId, name)
			select id, 'ru', regionNameRu from {{pregion}};


			ALTER TABLE {{pcity}}
			DROP COLUMN `cityNameEn`,
			DROP COLUMN `cityNameRu`;


			ALTER TABLE {{pcountry}}
			DROP COLUMN `countryNameEn`,
			DROP COLUMN `countryNameRu`;


			ALTER TABLE {{pregion}}
			DROP COLUMN `regionNameEn`,
			DROP COLUMN `regionNameRu`;

		";
	}
}