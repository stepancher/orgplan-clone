<?php

class m170228_113549_copy_fairhasorganizer_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_fairhasorganizer_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_fairhasorganizer_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE oc_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT f.id, oc.id
                                        FROM {$expodata}.{{exdbfairhasorganizer}} efho
                                        LEFT JOIN {$expodata}.{{exdbfair}} ef ON ef.id = efho.fairId
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eo ON eo.id = efho.organizerId
                                        LEFT JOIN {$db}.{{fair}} f ON f.exdbId = ef.exdbId AND ((ef.exdbCrc IS NOT NULL AND f.exdbCrc = ef.exdbCrc) OR ef.exdbCrc IS NULL)
                                        LEFT JOIN {$db}.{{organizercompany}} oc ON oc.exdbId = eo.exdbId
                                        LEFT JOIN {$db}.{{fairhasorganizer}} fho ON fho.fairId = f.id
                                        WHERE fho.id IS NULL
                                        AND oc.id IS NOT NULL
                                        AND f.id IS NOT NULL
                                        ORDER BY f.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO f_id, oc_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{organizer}} (`companyId`) VALUES (oc_id);
                    COMMIT;
                    
                    SET @last_insert_id := LAST_INSERT_ID();
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{fairhasorganizer}} (`fairId`,`organizerId`) VALUES (f_id, @last_insert_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_fairhasorganizer_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}