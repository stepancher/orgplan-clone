<?php

class m160328_110316_create_table_organizerinfohasfile extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			DROP TABLE IF EXISTS {{organizerinfohasfile}};

			ALTER TABLE {{organizerinfo}}
				ADD COLUMN `image` VARCHAR(45) NULL AFTER `signRulesPosition`;

			  ALTER TABLE {{organizercontact}}
				ADD COLUMN `image` VARCHAR(45) NULL AFTER `position`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			DROP TABLE IF EXISTS {{organizerinfohasfile}};
			CREATE TABLE {{organizerinfohasfile}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `organizerInfoId` INT NULL,
			  `fileId` INT NULL,
			  PRIMARY KEY (`id`));

		  	DROP TABLE IF EXISTS {{organizercontacthasfile}};
			CREATE TABLE {{organizercontacthasfile}} (
			  `id` INT NOT NULL AUTO_INCREMENT,
			  `organizerContactId` INT NULL,
			  `fileId` INT NULL,
			  PRIMARY KEY (`id`));

			  ALTER TABLE {{organizerinfo}}
				DROP COLUMN `image`;

			  ALTER TABLE {{organizercontact}}
				DROP COLUMN `image`;
		";
	}
}