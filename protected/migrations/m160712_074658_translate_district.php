<?php

class m160712_074658_translate_district extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			ALTER TABLE {{district}}
			DROP COLUMN `name`;
			
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('1', 'ru', 'Центральный федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('2', 'ru', 'Южный федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('3', 'ru', 'Северо-Западный федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('4', 'ru', 'Дальневосточный федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('5', 'ru', 'Сибирский федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('6', 'ru', 'Уральский федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('7', 'ru', 'Приволжский федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('8', 'ru', 'Северо-Кавказский федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('9', 'ru', 'Крымский федеральный округ');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('1', 'en', 'Central Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('2', 'en', 'Southern Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('3', 'en', 'Northwestern Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('4', 'en', 'Far Eastern Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('5', 'en', 'Siberian Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('6', 'en', 'Ural Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('7', 'en', 'Volga Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('8', 'en', 'North Caucasian Federal District');
			INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('9', 'en', 'Crimean Federal District');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}