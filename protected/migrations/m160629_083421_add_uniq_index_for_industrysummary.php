<?php

class m160629_083421_add_uniq_index_for_industrysummary extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			ALTER TABLE {{industrysummary}} 
			ADD UNIQUE INDEX `industry_summary_uniq_industryid` (`industryId` ASC);
		";
	}

	public function downSql()
	{
		return "
		  	ALTER TABLE {{industrysummary}} 
		  	DROP INDEX `industry_summary_uniq_industryid` ;
		";
	}
}