<?php

class m170331_141614_add_indexes extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{traudit}} 
            ADD INDEX `trParentIdidx` (`trParentId` ASC);
            
            ALTER TABLE {{organizercompanyhasassociation}} 
            ADD INDEX `organizerCompanyIdx` (`organizerCompanyId` ASC),
            ADD INDEX `associationidx` (`associationId` ASC);


		";
    }

    public function downSql()
    {
        return TRUE;
    }
}