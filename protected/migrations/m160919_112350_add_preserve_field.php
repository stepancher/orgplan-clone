<?php

class m160919_112350_add_preserve_field extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE TABLE IF NOT EXISTS {{analyticschartpreview}} (
              `id` INT NOT NULL,
              `stat` INT NULL,
              `widget` INT NULL,
              `widgetType` INT NULL,
              `header` VARCHAR(500) NULL,
              `measure` VARCHAR(145) NULL,
              `color` VARCHAR(45) NULL,
              `industryId` INT NULL,
              `position` INT NULL,
              PRIMARY KEY (`id`));

            ALTER TABLE {{analyticschartpreview}} 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
            
            ALTER TABLE {{trfair}}
            ADD COLUMN `preserveUrl` TINYINT NULL AFTER `shortUrl`;
            
            UPDATE {{trfair}} SET shortUrl = NULL;
            
            
            INSERT INTO {{analyticschartpreview}}  VALUES (1,103781,1,3,'Продукция Сельского <br/>хозяйства всех<br/> категорий','место <br/>в России','#a0d468',11,1),(2,220769,1,2,'Зерно','место','#a0d468',11,2),(3,220777,1,2,'Скот и птица <br/> в убойном весе','место','#fa694c',11,3),(4,220815,NULL,NULL,'Строительный объем',NULL,NULL,2,NULL),(5,103707,NULL,NULL,'Число зданий',NULL,NULL,2,NULL),(6,220840,NULL,NULL,'Жилищные кредиты',NULL,NULL,2,NULL),(7,220834,NULL,NULL,'Производство древесины',NULL,NULL,5,NULL),(8,220835,NULL,NULL,'Число предприятий',NULL,NULL,5,NULL),(9,148324,NULL,NULL,'Лесистость территорий',NULL,NULL,5,NULL);
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}