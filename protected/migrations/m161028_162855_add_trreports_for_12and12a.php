<?php

class m161028_162855_add_trreports_for_12and12a extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $dbProposalF10943) = explode('=', Yii::app()->dbProposalF10943->connectionString);
        
        return "
            INSERT INTO {$dbProposalF10943}.tbl_report (`id`, `reportId`, `reportName`) VALUES (9, '7', 'report-7');
            INSERT INTO {$dbProposalF10943}.tbl_report (`id`, `reportId`, `reportName`) VALUES (10, '8', 'report-8');

            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('9', 'ru', 'Бэйджы. Заявка №12', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\":true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"companyRu\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', '[{\"firstName\":\"Имя\",\"lastName\":\"Фамилия\",\"countryRu\":\"Страна\",\"companyRu\":\"Компания\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('10', 'ru', 'Бэйджы. Заявка №12a', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"regionRu\",\"displayName\":\"Регион\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"street\",\"displayName\":\"Улица\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"house\",\"displayName\":\"Дом\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"index\",\"displayName\":\"Индекс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"phone\",\"displayName\":\"Телефон\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"fax\",\"displayName\":\"Факс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', '[{\n\"firstName\":\"Имя\",\n\"lastName\":\"Фамилия\",\n\"surname\":\"Отчество\",\n\"countryRu\":\"Страна\",\n\"regionRu\":\"Регион\",\n\"cityRu\":\"Город\",\n\"company\":\"Компания\",\n\"post\":\"Должность\",\n\"street\":\"Улица\",\n\"house\":\"Дом\",\n\"index\":\"Индекс\",\n\"phone\":\"Телефон\",\n\"fax\":\"Факс\"\n}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('9', 'en', 'Бэйджы. Заявка №12', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\":true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"companyRu\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', '[{\"firstName\":\"Имя\",\"lastName\":\"Фамилия\",\"countryRu\":\"Страна\",\"companyRu\":\"Компания\"}]');
            INSERT INTO {$dbProposalF10943}.tbl_trreport (`trParentId`, `langId`, `reportLabel`, `columnDefs`, `exportColumns`) VALUES ('10', 'en', 'Бэйджы. Заявка №12a', '[{\"field\":\"firstName\",\"displayName\":\"Имя\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"lastName\",\"displayName\":\"Фамилия\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"surname\",\"displayName\":\"Отчество\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"countryRu\",\"displayName\":\"Страна\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"regionRu\",\"displayName\":\"Регион\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"cityRu\",\"displayName\":\"Город\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"company\",\"displayName\":\"Компания\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"post\",\"displayName\":\"Должность\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"street\",\"displayName\":\"Улица\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"house\",\"displayName\":\"Дом\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"index\",\"displayName\":\"Индекс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"phone\",\"displayName\":\"Телефон\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true},\n{\"field\":\"fax\",\"displayName\":\"Факс\",\"sortable\":true,\"aggregate\":false,\"sortingType\":\"string\",\"filterable\": true}]', '[{\n\"firstName\":\"Имя\",\n\"lastName\":\"Фамилия\",\n\"surname\":\"Отчество\",\n\"countryRu\":\"Страна\",\n\"regionRu\":\"Регион\",\n\"cityRu\":\"Город\",\n\"company\":\"Компания\",\n\"post\":\"Должность\",\n\"street\":\"Улица\",\n\"house\":\"Дом\",\n\"index\":\"Индекс\",\n\"phone\":\"Телефон\",\n\"fax\":\"Факс\"\n}]');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}