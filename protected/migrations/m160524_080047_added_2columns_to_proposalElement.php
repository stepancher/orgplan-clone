<?php

class m160524_080047_added_2columns_to_proposalElement extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			ALTER TABLE {{proposalelement}}
			DROP COLUMN `payment`;
			ALTER TABLE {{proposalelement}}
			DROP COLUMN `documents`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "

			ALTER TABLE {{proposalelement}}
			ADD COLUMN `payment` INT NULL AFTER `rejectMessage`,
			ADD COLUMN `documents` INT NULL AFTER `payment`;

			ALTER TABLE {{proposalelement}}
			CHANGE COLUMN `payment` `payment` INT(11) NULL DEFAULT 0 ,
			CHANGE COLUMN `documents` `documents` INT(11) NULL DEFAULT 0 ;

		";
	}
}