<?php

class m160615_124116_10_proposal_update extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}



	public function getCreateTable(){
		return "
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2145', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2145', '2016-10-07', 'endDate');
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">АРЕНДА ОБОРУДОВАНИЯ</div><div class=\"body\">Стоимость аренды включает монтажные работы и все входящие в комплект коммутационные кабели.</div>' WHERE `id`='450';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=' <div class=\"header\">АРЕНДА ОБОРУДОВАНИЯ</div><div class=\"body\">Стоимость аренды включает монтажные работы и все входящие в комплект коммутационные кабели.</div>' WHERE `id`='1425';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=' <div class=\"header\">АРЕНДА ОБОРУДОВАНИЯ</div><div class=\"body\">Стоимость аренды включает монтажные работы и все входящие в комплект коммутационные кабели.</div>' WHERE `id`='1446';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=' <div class=\"header\">АРЕНДА ОБОРУДОВАНИЯ</div><div class=\"body\">Стоимость аренды включает монтажные работы и все входящие в комплект коммутационные кабели.</div>' WHERE `id`='1468';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nНАПОЛЬНАЯ ПОДСТАВКА\n</div>\n<div class=\"body\">\nНапольная подставка предоставляется по желанию заказчика бесплатно.<br>\n</div> ' WHERE `id`='1445';
			UPDATE {{trattributeclass}} SET `tooltipCustom`=' <div class=\"header\">FLOOR SUPPORT FOR DISPLAY</div><div class=\"body\">Table support is provided free of charge as an option.' WHERE `id`='1514';
			
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1428';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1431';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1434';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1437';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1440';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1443';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1444';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1445';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1485';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1486';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 50% при заказе после 19.08.16 <br> на 100% при заказе после 23.09.16' WHERE `id`='1492';
			
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2125';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2138';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2139';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2140';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2141';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2142';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2143';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2144';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2145';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2146';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2149';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2248';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2249';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2246';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">СТОИМОСТЬ УСЛУГ</div> увеличивается: <br> на 100% при заказе после 18.08.16' WHERE `id`='2247';

		";
	}
}