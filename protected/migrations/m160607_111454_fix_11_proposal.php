<?php

class m160607_111454_fix_11_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{

		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){

		return "
			DELETE FROM {{attributeclass}} WHERE `id`='2087';
			DELETE FROM {{attributeclass}} WHERE `id`='2088';
			DELETE FROM {{attributeclass}} WHERE `id`='2089';
			DELETE FROM {{attributeclass}} WHERE `id`='2090';
			DELETE FROM {{attributeclass}} WHERE `id`='2091';
			DELETE FROM {{attributeclass}} WHERE `id`='2092';
			DELETE FROM {{attributeclass}} WHERE `id`='2093';
			DELETE FROM {{attributeclass}} WHERE `id`='2094';
			DELETE FROM {{attributeclass}} WHERE `id`='2095';
			DELETE FROM {{attributeclass}} WHERE `id`='2097';
			DELETE FROM {{attributeclass}} WHERE `id`='2098';
			DELETE FROM {{attributeclass}} WHERE `id`='2099';
			DELETE FROM {{attributeclass}} WHERE `id`='2100';
			DELETE FROM {{attributeclass}} WHERE `id`='2101';
			DELETE FROM {{attributeclass}} WHERE `id`='2102';
			DELETE FROM {{attributeclass}} WHERE `id`='2103';
			DELETE FROM {{attributeclass}} WHERE `id`='2104';
			DELETE FROM {{attributeclass}} WHERE `id`='2105';
			DELETE FROM {{attributeclass}} WHERE `id`='2106';
			DELETE FROM {{attributeclass}} WHERE `id`='2107';
			DELETE FROM {{attributeclass}} WHERE `id`='2108';
			DELETE FROM {{attributeclass}} WHERE `id`='2109';
			DELETE FROM {{attributeclass}} WHERE `id`='2110';
			DELETE FROM {{attributeclass}} WHERE `id`='2111';
			DELETE FROM {{attributeclass}} WHERE `id`='2112';
			DELETE FROM {{attributeclass}} WHERE `id`='2113';
			DELETE FROM {{attributeclass}} WHERE `id`='2114';
			DELETE FROM {{attributeclass}} WHERE `id`='2115';
			DELETE FROM {{attributeclass}} WHERE `id`='2116';
			DELETE FROM {{attributeclass}} WHERE `id`='2117';
			DELETE FROM {{attributeclass}} WHERE `id`='2118';
			DELETE FROM {{attributeclass}} WHERE `id`='2119';
			DELETE FROM {{attributeclass}} WHERE `id`='2120';
			DELETE FROM {{attributeclass}} WHERE `id`='2121';
			DELETE FROM {{attributeclass}} WHERE `id`='2122';
			DELETE FROM {{attributeclass}} WHERE `id`='2123';
			DELETE FROM {{attributeclass}} WHERE `id`='2124';
			DELETE FROM {{attributeclass}} WHERE `id`='2125';
			DELETE FROM {{attributeclass}} WHERE `id`='2126';
			DELETE FROM {{attributeclass}} WHERE `id`='2127';
			DELETE FROM {{attributeclass}} WHERE `id`='2128';
			DELETE FROM {{attributeclass}} WHERE `id`='2129';
			DELETE FROM {{attributeclass}} WHERE `id`='2130';
			DELETE FROM {{attributeclass}} WHERE `id`='2131';
			
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2087', 'n11_hint_danger', 'string', '0', '0', '2087', '0', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2088', 'n11_event', 'string', '0', '0', '2088', '0', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2089', 'n11_event_dropdown', 'enum', '0', '0', '2088', '2088', '10', 'dropDownMultiply', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2090', 'n11_event_dropdown_option', 'bool', '0', '2089', '2088', '10', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2091', 'n11_event_dropdown_option2', 'bool', '0', '2089', '2088', '20', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2092', 'n11_event_dropdown_option3', 'bool', '0', '2089', '2088', '30', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2093', 'n11_event_dropdown_option4', 'bool', '0', '2089', '2088', '40', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2094', 'n11_event_dropdown_option5', 'bool', '0', '2089', '2088', '50', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2095', 'n11_event_dropdown_option6', 'bool', '0', '2089', '2088', '60', 'dropDownOption', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2097', 'n11_event_checkbox', 'bool', '0', '2088', '2088', '20', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2098', 'n11_event_checkbox_type', 'string', '0', '0', '2097', '2088', '10', 'text', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2099', 'n11_event_checkbox_theme', 'string', '0', '0', '2097', '2088', '20', 'textarea', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2100', 'n11_event_checkbox_time', 'string', '0', '0', '2097', '2088', '30', 'text', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2101', 'n11_event_checkbox_count', 'string', '0', '0', '2097', '2088', '40', 'text', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2102', 'n11_event_checkbox_programm', 'string', '0', '0', '2097', '2088', '50', 'textarea', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2103', 'n11_event_checkbox_listener', 'string', '0', '0', '2097', '2088', '60', 'text', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2104', 'n11_event_checkbox_visitiors', 'string', '0', '0', '2097', '2088', '70', 'text', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2105', 'n11_hall', 'string', '0', '0', '2105', '0', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2106', 'n11_hall_checkbox', 'bool', '0', '2105', '2105', '10', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2107', 'n11_hall_checkbox1', 'bool', '0', '2105', '2105', '20', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2108', 'n11_equip', 'string', '0', '0', '2108', '0', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2109', 'n11_equip_checkbox', 'bool', '0', '2108', '2108', '10', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2110', 'n11_equip_checkbox2', 'bool', '0', '2108', '2108', '20', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2111', 'n11_equip_checkbox3', 'bool', '0', '2108', '2108', '30', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2112', 'n11_equip_checkbox4', 'bool', '0', '2108', '2108', '40', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2113', 'n11_equip_checkbox5', 'bool', '0', '2108', '2108', '50', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2114', 'n11_equip_checkbox6', 'bool', '0', '2108', '2108', '60', 'checkboxSwitcher', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2115', 'n11_require', 'string', '0', '0', '2115', '0', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2116', 'n11_require_hint', 'string', '0', '2115', '2115', '10', 'hint', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2117', 'n11_require_hint2', 'string', '0', '2115', '2115', '20', 'hint', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2118', 'n11_require_hint3', 'string', '0', '2115', '2115', '30', 'hint', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2119', 'n11_require_hint4', 'string', '0', '2115', '2115', '40', 'hint', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2120', 'n11_require_hint5', 'string', '0', '2115', '2115', '50', 'hint', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2121', 'n11_manager', 'string', '0', '0', '2121', '0', 'headerH1', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2122', 'n11_manager_position', 'string', '0', '0', '2121', '2121', '10', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2123', 'n11_manager_surname', 'string', '0', '0', '2121', '2121', '20', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2124', 'n11_manager_name', 'string', '0', '0', '2121', '2121', '30', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2125', 'n11_manager_lastname', 'string', '0', '0', '2121', '2121', '40', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2126', 'n11_manager_phone', 'string', '0', '0', '2121', '2121', '50', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2127', 'n11_manager_fax', 'string', '0', '0', '2121', '2121', '60', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2128', 'n11_manager_mail', 'string', '0', '0', '2121', '2121', '70', 'text', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2129', 'n11_manager_hint_danger', 'string', '0', '2121', '2121', '80', 'hintDanger', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2130', 'n11_manager_agree', 'bool', '0', '2121', '2121', '90', 'checkboxAgreement', '0');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('2131', 'n11_manager_empty', 'string', '0', '2121', '2121', '71', 'headerH1', '0');
			
			INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('168', '19', '2088', '0', '0', '10');
			INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('169', '19', '2105', '0', '0', '20');
			INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('170', '19', '2108', '0', '0', '30');
			INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('171', '19', '2115', '0', '0', '40');
			INSERT INTO {{attributemodel}} (`id`, `elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('172', '19', '2121', '0', '0', '50');
			
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1828', '2100', 'unitTitile');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1829', '2101', 'unitTitile');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `class`) VALUES ('1830', '2104', 'unitTitile');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1831', '2109', 'proposal-checkbox_disabled proposal-checkbox_disabled_checked', 'modifier');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1832', '2110', 'proposal-checkbox_disabled proposal-checkbox_disabled_checked', 'modifier');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1833', '2111', 'proposal-checkbox_disabled proposal-checkbox_disabled_checked', 'modifier');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1834', '2112', 'proposal-checkbox_disabled proposal-checkbox_disabled_checked', 'modifier');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1835', '2113', 'proposal-checkbox_disabled proposal-checkbox_disabled_checked', 'modifier');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1836', '2109', '1', 'disabled');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1837', '2110', '1', 'disabled');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1838', '2111', '1', 'disabled');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1839', '2112', '1', 'disabled');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1840', '2113', '1', 'disabled');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1841', '2109', '1', 'value');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1842', '2110', '1', 'value');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1843', '2111', '1', 'value');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1844', '2112', '1', 'value');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES ('1845', '2113', '1', 'value');
			
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1562', '2087', 'ru', 'Отпрвьте заполненную форму в дирекцию выставку не позднее 19 августа 2016 г. Факс: +7(495)781-37-08, e-mail: agrosalon@agrosalon.ru');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1563', '2088', 'ru', 'Тип мероприятия:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1564', '2089', 'ru', 'Тип мероприятия');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1565', '2090', 'ru', 'Конференция');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1566', '2091', 'ru', 'Презентация');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1567', '2092', 'ru', 'Мастер-класс');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1568', '2093', 'ru', 'Круглый стол');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1569', '2094', 'ru', 'Семинар');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1570', '2095', 'ru', 'Пресс-конференция');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1571', '2097', 'ru', 'Другое:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `placeholder`) VALUES ('1572', '2098', 'ru', 'Укажите тип мероприятия');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('1573', '2099', 'ru', 'Тема мероприятия', 'Введите');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1574', '2100', 'ru', 'Продолжительность мероприятия');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1575', '2101', 'ru', 'Количество докладчиков');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`, `placeholder`) VALUES ('1576', '2102', 'ru', 'Тезисы докладов / программа мероприятия ', 'Введите');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1577', '2103', 'ru', 'Целевая аудитория');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1578', '2104', 'ru', 'Количество посетителей');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1579', '2105', 'ru', 'Зал для мероприятия:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1580', '2106', 'ru', 'Коференц-зал');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1581', '2107', 'ru', 'Пресс-центр');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1582', '2108', 'ru', 'Оборудование:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1583', '2109', 'ru', 'LCD проектор');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1584', '2110', 'ru', 'Проводной микрофон');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1585', '2111', 'ru', 'Комплект звукоусиления');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1586', '2112', 'ru', 'Экран');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1587', '2113', 'ru', 'Радиомикрофон');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1588', '2114', 'ru', 'Ноутбук (комплект презентационных программ)');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1589', '2115', 'ru', 'Обязательные условия:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1590', '2116', 'ru', '1. Цель мероприятия - обучение, демонстрация новых технологий, обсуждение актуальных проблем.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1591', '2117', 'ru', '2. Тема - актуальная, интереесная для целевой группы, позволяющая применить получаемые опыт, знания, технологии на практике.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1592', '2118', 'ru', '3. Целевая аудитория - фермеры, специалисты средних и крупных фирм, руководители хозяйств.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1593', '2119', 'ru', '4. Продолжительность 1-2 часа, наличие PowerPoint презентации.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1594', '2120', 'ru', '5. Бесплатное участие в мероприятии для посетителей выставки.');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1595', '2121', 'ru', 'Менеджер проекта:');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1596', '2122', 'ru', 'Должность');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1597', '2123', 'ru', 'Фамилия');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1598', '2124', 'ru', 'Имя');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1599', '2125', 'ru', 'Отчество');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1600', '2126', 'ru', 'Телефон');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1601', '2127', 'ru', 'Факс');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1602', '2128', 'ru', 'Почта');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES ('1603', '2129', 'ru', 'Заявка является приложением к Договору на участие в выставке и должна быть заполнена в двух экземплярах.\n<br><br>\nПодписывая настоящую заявку стороны подтверждают свое согласие со следующими условиями Организатор берет на себя все расходы, связанные с арендой конференц-залов и заявленного оборудования для проведения мероприятия. Организатор анонсирует мероприятие в СМИ и на рекламно-информационных носителях , а также ведет привлечение посетителей. Экспонент берет на себя все расходы, связанные с формированием программы мероприятия, и привлечением докладчиков и участников.\n<br><br>\nОрганизатор оставляет за собой право отказать экспоненту в проведении мероприятия, если его программа будет изменена или тема мероприятия не будет соответствовать тематике выставки АГРОСАЛОН.\n<br><br>\nВсе претензии касающиеся исполнения настоящей заявки принимаются Организатором в срок до 12:00 07 октября 2016 г. В случае если мотивированная претензия не получена Организатором в установленный срок, все работы и услуги, указанные в настоящей заявке считаются выполненными надлежащего качества и принятыми Экспонентом.');
			
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2401', '1828', 'ru', 'час');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2402', '1829', 'ru', 'чел.');
			INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES ('2403', '1830', 'ru', 'чел.');
";
	}

}