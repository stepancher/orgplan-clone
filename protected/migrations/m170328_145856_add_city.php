<?php

class m170328_145856_add_city extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{region}} (`districtId`) VALUES ('68');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1089', 'ru', 'Шефтларн', 'schäftlarn');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1089', 'en', 'Schäftlarn', 'schäftlarn');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1089', 'de', 'Schäftlarn', 'schäftlarn');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1089', 'hohenschäftlarn');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1538', 'ru', 'Хоэншефтларн');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1538', 'en', 'Hohenschäftlarn');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1538', 'de', 'Hohenschäftlarn');
            
            INSERT INTO {{region}} (`districtId`) VALUES ('68');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1090', 'ru', 'Хоф', 'hof');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1090', 'en', 'Hof', 'hof');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1090', 'de', 'Landkreis Hof', 'hof');
            
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1090', 'köditz');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1539', 'ru', 'Кёдиц');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1539', 'en', 'Köditz');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1539', 'de', 'Köditz');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}