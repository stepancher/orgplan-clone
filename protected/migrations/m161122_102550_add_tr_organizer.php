<?php

class m161122_102550_add_tr_organizer extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `add_tr_organizer_rows`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE organizerName TEXT DEFAULT '';
                DECLARE organizerId INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT name, id FROM {{organizer}};
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    FETCH copy INTO organizerName, organizerId;
                    
                    IF done THEN
                        LEAVE read_loop;
                        
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {{trorganizer}} (`trParentId`,`langId`,`name`) VALUES (organizerId, 'ru', organizerName);
                        INSERT INTO {{trorganizer}} (`trParentId`,`langId`,`name`) VALUES (organizerId, 'en', organizerName);
                        INSERT INTO {{trorganizer}} (`trParentId`,`langId`,`name`) VALUES (organizerId, 'de', organizerName);
		            COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL add_tr_organizer_rows();
            DROP PROCEDURE `add_tr_organizer_rows`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}