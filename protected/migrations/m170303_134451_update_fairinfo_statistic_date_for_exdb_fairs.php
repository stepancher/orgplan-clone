<?php

class m170303_134451_update_fairinfo_statistic_date_for_exdb_fairs extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `update_fairinfo_statistic_date_for_exdb_fairs`;
            CREATE PROCEDURE `update_fairinfo_statistic_date_for_exdb_fairs`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fi_id INT DEFAULT 0;
                DECLARE fi_stat DATE DEFAULT NULL;
                DECLARE copy CURSOR FOR SELECT fi.id,
                                        CASE WHEN STR_TO_DATE(fi.exdbStatisticsDate, '%Y') IS NOT NULL AND LENGTH(fi.exdbStatisticsDate) = 4
                                             THEN STR_TO_DATE(CONCAT(fi.exdbStatisticsDate, '-01-01'), '%Y-%m-%d')
                                             WHEN STR_TO_DATE(fi.exdbStatisticsDate, '%M %Y') IS NOT NULL
                                             THEN STR_TO_DATE(CONCAT(fi.exdbStatisticsDate,'-01'), '%M %Y-%d')
                                        ELSE NULL END exdbStat
                                        FROM {{fairinfo}} fi 
                                        WHERE fi.exdbFairId IS NOT NULL
                                        AND fi.exdbStatisticsDate != '' 
                                        AND CASE WHEN STR_TO_DATE(fi.exdbStatisticsDate, '%Y') IS NOT NULL AND LENGTH(fi.exdbStatisticsDate) = 4
                                             THEN STR_TO_DATE(CONCAT(fi.exdbStatisticsDate, '-01-01'), '%Y-%m-%d')
                                             WHEN STR_TO_DATE(fi.exdbStatisticsDate, '%M %Y') IS NOT NULL
                                             THEN STR_TO_DATE(CONCAT(fi.exdbStatisticsDate,'-01'), '%M %Y-%d')
                                        ELSE NULL END IS NOT NULL
                                        AND (fi.statisticsDate IS NULL OR 
                                        fi.statisticsDate != CASE WHEN STR_TO_DATE(fi.exdbStatisticsDate, '%Y') IS NOT NULL AND LENGTH(fi.exdbStatisticsDate) = 4
                                             THEN STR_TO_DATE(CONCAT(fi.exdbStatisticsDate, '-01-01'), '%Y-%m-%d')
                                             WHEN STR_TO_DATE(fi.exdbStatisticsDate, '%M %Y') IS NOT NULL
                                             THEN STR_TO_DATE(CONCAT(fi.exdbStatisticsDate,'-01'), '%M %Y-%d')
                                        ELSE NULL END)
                                        ORDER BY fi.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO fi_id, fi_stat;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{fairinfo}} SET `statisticsDate` = fi_stat WHERE `id` = fi_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `update_fairinfo_statistic_date_for_exdb_fairs`();
            DROP PROCEDURE IF EXISTS `update_fairinfo_statistic_date_for_exdb_fairs`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}