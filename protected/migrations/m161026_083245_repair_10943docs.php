<?php

class m161026_083245_repair_10943docs extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/10943/scheme.pdf' WHERE `id`='62';
            UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/10943/scheme.pdf' WHERE `id`='63';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}