<?php

class m170214_135323_move_exdb_data_to_orgplan_expodata extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);
        
        return "
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbassociation}}
            SELECT * FROM {$db}.{{exdbassociation}};
            DROP TABLE IF EXISTS {$db}.{{exdbassociation}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbaudit}}
            SELECT * FROM {$db}.{{exdbaudit}};
            DROP TABLE IF EXISTS {$db}.{{exdbaudit}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdborganizercompany}}
            SELECT * FROM {$db}.{{exdborganizercompany}};
            DROP TABLE IF EXISTS {$db}.{{exdborganizercompany}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdborganizercompanyhasassociation}}
            SELECT * FROM {$db}.{{exdborganizercompanyhasassociation}};
            DROP TABLE IF EXISTS {$db}.{{exdborganizercompanyhasassociation}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbtraudit}}
            SELECT * FROM {$db}.{{exdbtraudit}};
            DROP TABLE IF EXISTS {$db}.{{exdbtraudit}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbtrorganizercompany}}
            SELECT * FROM {$db}.{{exdbtrorganizercompany}};
            DROP TABLE IF EXISTS {$db}.{{exdbtrorganizercompany}};
            
            CREATE TABLE IF NOT EXISTS {$expodata}.{{exdbvenuehasassociation}}
            SELECT * FROM {$db}.{{exdbvenuehasassociation}};
            DROP TABLE IF EXISTS {$db}.{{exdbvenuehasassociation}};
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}