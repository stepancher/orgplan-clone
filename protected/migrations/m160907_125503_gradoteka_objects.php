<?php

class m160907_125503_gradoteka_objects extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{gobjectshasgstattypes}} 
            DROP FOREIGN KEY `fk_{{gobjectshasgstattypes_2}}`,
            DROP FOREIGN KEY `fk_{{gobjectshasgstattypes_1}}`,
            DROP INDEX `fk_{{gobjectshasgstattypes_2_idx}}`,
            CHANGE COLUMN `statId` `statId` VARCHAR(45) NOT NULL;
            
            ALTER TABLE {{gvalue}} 
            DROP FOREIGN KEY `fk_{{gvalue_1}}`;

            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Краснодарского края', `gId` = '3000000' WHERE `id` = '206';
            UPDATE {{gobjects}} SET `name` = 'Красноярский край', `gId` = '4000000' WHERE `id` = '243';
            UPDATE {{gobjects}} SET `name` = 'Приморский край', `gId` = '5000000' WHERE `id` = '250';
            UPDATE {{gobjects}} SET `name` = 'Ставропольский край', `gId` = '7000000' WHERE `id` = '216';
            UPDATE {{gobjects}} SET `name` = 'Хабаровский край', `gId` = '8000000' WHERE `id` = '252';
            UPDATE {{gobjects}} SET `name` = 'Амурская область', `gId` = '10000000' WHERE `id` = '253';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Архангельской области', `gId` = '11000000' WHERE `id` = '195';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Ненецкого автономного округа', `gId` = '11800000' WHERE `id` = '203';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Астраханской области', `gId` = '12000000' WHERE `id` = '207';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Белгородской области', `gId` = '14000000' WHERE `id` = '175';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Брянской области', `gId` = '15000000' WHERE `id` = '176';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Владимирской области', `gId` = '17000000' WHERE `id` = '177';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Волгоградской области', `gId` = '18000000' WHERE `id` = '208';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Вологодской области', `gId` = '19000000' WHERE `id` = '196';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Воронежской области', `gId` = '20000000' WHERE `id` = '178';
            UPDATE {{gobjects}} SET `name` = 'Нижегородская область', `gId` = '22000000' WHERE `id` = '225';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Ивановской области', `gId` = '24000000' WHERE `id` = '179';
            UPDATE {{gobjects}} SET `name` = 'Иркутская область', `gId` = '25000000' WHERE `id` = '244';
            UPDATE {{gobjects}} SET `name` = 'Республика Ингушетия', `gId` = '26000000' WHERE `id` = '211';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Калининградской области', `gId` = '27000000' WHERE `id` = '197';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Тверской области', `gId` = '28000000' WHERE `id` = '190';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Калужской области', `gId` = '29000000' WHERE `id` = '180';
            UPDATE {{gobjects}} SET `name` = 'Камчатский край', `gId` = '30000000' WHERE `id` = '251';
            UPDATE {{gobjects}} SET `name` = 'Кемеровская область', `gId` = '32000000' WHERE `id` = '245';
            UPDATE {{gobjects}} SET `name` = 'Кировская область', `gId` = '33000000' WHERE `id` = '224';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Костромской области', `gId` = '34000000' WHERE `id` = '181';
            UPDATE {{gobjects}} SET `name` = 'Самарская область', `gId` = '36000000' WHERE `id` = '228';
            UPDATE {{gobjects}} SET `name` = 'Курганская область', `gId` = '37000000' WHERE `id` = '231';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Курской области', `gId` = '38000000' WHERE `id` = '182';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Города Санкт-Петербурга (города федерального значения)', `gId` = '40000000' WHERE `id` = '199';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Ленинградской области', `gId` = '41000000' WHERE `id` = '198';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Липецкой области', `gId` = '42000000' WHERE `id` = '183';
            UPDATE {{gobjects}} SET `name` = 'Магаданская область', `gId` = '44000000' WHERE `id` = '254';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования города Москвы (столицы Российской Федерации города федерального значения)', `gId` = '45000000' WHERE `id` = '184';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Московской области', `gId` = '46000000' WHERE `id` = '185';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Мурманской области', `gId` = '47000000' WHERE `id` = '200';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Новгородской области', `gId` = '49000000' WHERE `id` = '201';
            UPDATE {{gobjects}} SET `name` = 'Новосибирская область', `gId` = '50000000' WHERE `id` = '246';
            UPDATE {{gobjects}} SET `name` = 'Омская область', `gId` = '52000000' WHERE `id` = '247';
            UPDATE {{gobjects}} SET `name` = 'Оренбургская область', `gId` = '53000000' WHERE `id` = '226';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Орловской области', `gId` = '54000000' WHERE `id` = '186';
            UPDATE {{gobjects}} SET `name` = 'Пензенская область', `gId` = '56000000' WHERE `id` = '227';
            UPDATE {{gobjects}} SET `name` = 'Пермский край', `gId` = '57000000' WHERE `id` = '223';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Псковской области', `gId` = '58000000' WHERE `id` = '202';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Ростовской области', `gId` = '60000000' WHERE `id` = '209';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Рязанской области', `gId` = '61000000' WHERE `id` = '187';
            UPDATE {{gobjects}} SET `name` = 'Саратовская область', `gId` = '63000000' WHERE `id` = '229';
            UPDATE {{gobjects}} SET `name` = 'Сахалинская область', `gId` = '64000000' WHERE `id` = '255';
            UPDATE {{gobjects}} SET `name` = 'Свердловская область', `gId` = '65000000' WHERE `id` = '232';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Смоленской области', `gId` = '66000000' WHERE `id` = '188';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Тамбовской области', `gId` = '68000000' WHERE `id` = '189';
            UPDATE {{gobjects}} SET `name` = 'Томская область', `gId` = '69000000' WHERE `id` = '248';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Тульской области', `gId` = '70000000' WHERE `id` = '191';
            UPDATE {{gobjects}} SET `name` = 'Тюменская область', `gId` = '71000000' WHERE `id` = '233';
            UPDATE {{gobjects}} SET `name` = 'Ханты-Мансийский автономный округ – Югра', `gId` = '71800000' WHERE `id` = '235';
            UPDATE {{gobjects}} SET `name` = 'Ямало-Ненецкий автономный округ', `gId` = '71900000' WHERE `id` = '236';
            UPDATE {{gobjects}} SET `name` = 'Ульяновская область', `gId` = '73000000' WHERE `id` = '230';
            UPDATE {{gobjects}} SET `name` = 'Челябинская область', `gId` = '75000000' WHERE `id` = '234';
            UPDATE {{gobjects}} SET `name` = 'Забайкальский край', `gId` = '76000000' WHERE `id` = '242';
            UPDATE {{gobjects}} SET `name` = 'Чукотский автономный округ', `gId` = '77000000' WHERE `id` = '257';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Ярославской области', `gId` = '78000000' WHERE `id` = '192';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Республики Адыгеи (Адыгеи)', `gId` = '79000000' WHERE `id` = '204';
            UPDATE {{gobjects}} SET `name` = 'Республика Башкортостан', `gId` = '80000000' WHERE `id` = '217';
            UPDATE {{gobjects}} SET `name` = 'Республика Бурятия', `gId` = '81000000' WHERE `id` = '238';
            UPDATE {{gobjects}} SET `name` = 'Республика Дагестан', `gId` = '82000000' WHERE `id` = '210';
            UPDATE {{gobjects}} SET `name` = 'Кабардино-Балкарская Республика', `gId` = '83000000' WHERE `id` = '212';
            UPDATE {{gobjects}} SET `name` = 'Республика Алтай', `gId` = '84000000' WHERE `id` = '237';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Республики Калмыкии', `gId` = '85000000' WHERE `id` = '205';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Республики Карелии', `gId` = '86000000' WHERE `id` = '193';
            UPDATE {{gobjects}} SET `name` = 'Муниципальные образования Республики Коми', `gId` = '87000000' WHERE `id` = '194';
            UPDATE {{gobjects}} SET `name` = 'Республика Марий Эл', `gId` = '88000000' WHERE `id` = '218';
            UPDATE {{gobjects}} SET `name` = 'Республика Мордовия', `gId` = '89000000' WHERE `id` = '219';
            UPDATE {{gobjects}} SET `name` = 'Республика Северная Осетия - Алания', `gId` = '90000000' WHERE `id` = '214';
            UPDATE {{gobjects}} SET `name` = 'Карачаево-Черкесская Республика', `gId` = '91000000' WHERE `id` = '213';
            UPDATE {{gobjects}} SET `name` = 'Республика Татарстан', `gId` = '92000000' WHERE `id` = '220';
            UPDATE {{gobjects}} SET `name` = 'Республика Тыва', `gId` = '93000000' WHERE `id` = '239';
            UPDATE {{gobjects}} SET `name` = 'Удмуртская Республика', `gId` = '94000000' WHERE `id` = '221';
            UPDATE {{gobjects}} SET `name` = 'Республика Хакасия', `gId` = '95000000' WHERE `id` = '240';
            UPDATE {{gobjects}} SET `name` = 'Чеченская Республика', `gId` = '96000000' WHERE `id` = '215';
            UPDATE {{gobjects}} SET `name` = 'Чувашская Республика - Чувашия', `gId` = '97000000' WHERE `id` = '222';
            
            UPDATE {{gobjects}} SET `gId` = '2' WHERE `id` = '264';
            UPDATE {{gobjects}} SET `gId` = '3' WHERE `id` = '268';
            UPDATE {{gobjects}} SET `gId` = '4' WHERE `id` = '270';
            UPDATE {{gobjects}} SET `gId` = '5' WHERE `id` = '267';
            UPDATE {{gobjects}} SET `gId` = '7' WHERE `id` = '265';
            UPDATE {{gobjects}} SET `gId` = '8' WHERE `id` = '269';
            UPDATE {{gobjects}} SET `gId` = '1000000' WHERE `id` = '241';
            UPDATE {{gobjects}} SET `gId` = '4000000' WHERE `id` = '243';
            UPDATE {{gobjects}} SET `gId` = '5000000' WHERE `id` = '250';
            UPDATE {{gobjects}} SET `gId` = '7000000' WHERE `id` = '216';
            UPDATE {{gobjects}} SET `gId` = '8000000' WHERE `id` = '252';
            UPDATE {{gobjects}} SET `gId` = '10000000' WHERE `id` = '253';
            UPDATE {{gobjects}} SET `gId` = '22000000' WHERE `id` = '225';
            UPDATE {{gobjects}} SET `gId` = '25000000' WHERE `id` = '244';
            UPDATE {{gobjects}} SET `gId` = '26000000' WHERE `id` = '211';
            UPDATE {{gobjects}} SET `gId` = '30000000' WHERE `id` = '251';
            UPDATE {{gobjects}} SET `gId` = '32000000' WHERE `id` = '245';
            UPDATE {{gobjects}} SET `gId` = '33000000' WHERE `id` = '224';
            UPDATE {{gobjects}} SET `gId` = '36000000' WHERE `id` = '228';
            UPDATE {{gobjects}} SET `gId` = '37000000' WHERE `id` = '231';
            UPDATE {{gobjects}} SET `gId` = '44000000' WHERE `id` = '254';
            UPDATE {{gobjects}} SET `gId` = '50000000' WHERE `id` = '246';
            UPDATE {{gobjects}} SET `gId` = '52000000' WHERE `id` = '247';
            UPDATE {{gobjects}} SET `gId` = '53000000' WHERE `id` = '226';
            UPDATE {{gobjects}} SET `gId` = '56000000' WHERE `id` = '227';
            UPDATE {{gobjects}} SET `gId` = '57000000' WHERE `id` = '223';
            UPDATE {{gobjects}} SET `gId` = '63000000' WHERE `id` = '229';
            UPDATE {{gobjects}} SET `gId` = '64000000' WHERE `id` = '255';
            UPDATE {{gobjects}} SET `gId` = '65000000' WHERE `id` = '232';
            UPDATE {{gobjects}} SET `gId` = '69000000' WHERE `id` = '248';
            UPDATE {{gobjects}} SET `gId` = '71000000' WHERE `id` = '233';
            UPDATE {{gobjects}} SET `gId` = '71900000' WHERE `id` = '236';
            UPDATE {{gobjects}} SET `gId` = '73000000' WHERE `id` = '230';
            UPDATE {{gobjects}} SET `gId` = '75000000' WHERE `id` = '234';
            UPDATE {{gobjects}} SET `gId` = '76000000' WHERE `id` = '242';
            UPDATE {{gobjects}} SET `gId` = '77000000' WHERE `id` = '257';
            UPDATE {{gobjects}} SET `gId` = '80000000' WHERE `id` = '217';
            UPDATE {{gobjects}} SET `gId` = '81000000' WHERE `id` = '238';
            UPDATE {{gobjects}} SET `gId` = '82000000' WHERE `id` = '210';
            UPDATE {{gobjects}} SET `gId` = '83000000' WHERE `id` = '212';
            UPDATE {{gobjects}} SET `gId` = '84000000' WHERE `id` = '237';
            UPDATE {{gobjects}} SET `gId` = '88000000' WHERE `id` = '218';
            UPDATE {{gobjects}} SET `gId` = '89000000' WHERE `id` = '219';
            UPDATE {{gobjects}} SET `gId` = '91000000' WHERE `id` = '213';
            UPDATE {{gobjects}} SET `gId` = '92000000' WHERE `id` = '220';
            UPDATE {{gobjects}} SET `gId` = '93000000' WHERE `id` = '239';
            UPDATE {{gobjects}} SET `gId` = '94000000' WHERE `id` = '221';
            UPDATE {{gobjects}} SET `gId` = '95000000' WHERE `id` = '240';
            UPDATE {{gobjects}} SET `gId` = '96000000' WHERE `id` = '215';
            UPDATE {{gobjects}} SET `gId` = '98000000' WHERE `id` = '249';
            UPDATE {{gobjects}} SET `gId` = '99000000' WHERE `id` = '256';

            INSERT INTO {{gobjects}} (`name`, `gId`, `gType`) VALUES ('Крымский федеральный округ', '9', 'state');
            INSERT INTO {{gobjects}} (`name`, `gId`, `gType`, `parentId`, `parentType`) VALUES ('Муниципальные образования Архангельской области (включая Ненецкий АО)', '11001000', 'region', '3', 'state');
            INSERT INTO {{gobjects}} (`name`, `gId`, `gType`, `parentId`, `parentType`) VALUES ('Республика Крым', '35000000', 'region', '9', 'state');
            INSERT INTO {{gobjects}} (`name`, `gId`, `gType`, `parentId`, `parentType`) VALUES ('Город федерального значения Севастополь', '67000000', 'region', '9', 'state');
            INSERT INTO {{gobjects}} (`name`, `gId`, `gType`, `parentId`, `parentType`) VALUES ('Тюменская область (включая Ханты-Мансийский АО и Ямало-Ненецкий АО)', '71001000', 'region', '6', 'state');
            
            UPDATE {{gobjecthasregion}} SET `objId` = '175' WHERE `id` = '1';
            UPDATE {{gobjecthasregion}} SET `objId` = '184' WHERE `id` = '2';
            UPDATE {{gobjecthasregion}} SET `objId` = '195' WHERE `id` = '3';
            UPDATE {{gobjecthasregion}} SET `objId` = '207' WHERE `id` = '4';
            UPDATE {{gobjecthasregion}} SET `objId` = '176' WHERE `id` = '5';
            UPDATE {{gobjecthasregion}} SET `objId` = '177' WHERE `id` = '6';
            UPDATE {{gobjecthasregion}} SET `objId` = '208' WHERE `id` = '7';
            UPDATE {{gobjecthasregion}} SET `objId` = '196' WHERE `id` = '8';
            UPDATE {{gobjecthasregion}} SET `objId` = '178' WHERE `id` = '9';
            UPDATE {{gobjecthasregion}} SET `objId` = '256' WHERE `id` = '10';
            UPDATE {{gobjecthasregion}} SET `objId` = '242' WHERE `id` = '11';
            UPDATE {{gobjecthasregion}} SET `objId` = '179' WHERE `id` = '12';
            UPDATE {{gobjecthasregion}} SET `objId` = '244' WHERE `id` = '13';
            UPDATE {{gobjecthasregion}} SET `objId` = '212' WHERE `id` = '14';
            UPDATE {{gobjecthasregion}} SET `objId` = '197' WHERE `id` = '15';
            UPDATE {{gobjecthasregion}} SET `objId` = '180' WHERE `id` = '16';
            UPDATE {{gobjecthasregion}} SET `objId` = '251' WHERE `id` = '17';
            UPDATE {{gobjecthasregion}} SET `objId` = '213' WHERE `id` = '18';
            UPDATE {{gobjecthasregion}} SET `objId` = '245' WHERE `id` = '19';
            UPDATE {{gobjecthasregion}} SET `objId` = '224' WHERE `id` = '20';
            UPDATE {{gobjecthasregion}} SET `objId` = '181' WHERE `id` = '21';
            UPDATE {{gobjecthasregion}} SET `objId` = '206' WHERE `id` = '22';
            UPDATE {{gobjecthasregion}} SET `objId` = '243' WHERE `id` = '23';
            UPDATE {{gobjecthasregion}} SET `objId` = '231' WHERE `id` = '24';
            UPDATE {{gobjecthasregion}} SET `objId` = '182' WHERE `id` = '25';
            UPDATE {{gobjecthasregion}} SET `objId` = '198' WHERE `id` = '26';
            UPDATE {{gobjecthasregion}} SET `objId` = '183' WHERE `id` = '27';
            UPDATE {{gobjecthasregion}} SET `objId` = '254' WHERE `id` = '28';
            UPDATE {{gobjecthasregion}} SET `objId` = '185' WHERE `id` = '29';
            UPDATE {{gobjecthasregion}} SET `objId` = '200' WHERE `id` = '30';
            UPDATE {{gobjecthasregion}} SET `objId` = '203' WHERE `id` = '31';
            UPDATE {{gobjecthasregion}} SET `objId` = '225' WHERE `id` = '32';
            UPDATE {{gobjecthasregion}} SET `objId` = '201' WHERE `id` = '33';
            UPDATE {{gobjecthasregion}} SET `objId` = '246' WHERE `id` = '34';
            UPDATE {{gobjecthasregion}} SET `objId` = '247' WHERE `id` = '35';
            UPDATE {{gobjecthasregion}} SET `objId` = '226' WHERE `id` = '36';
            UPDATE {{gobjecthasregion}} SET `objId` = '186' WHERE `id` = '37';
            UPDATE {{gobjecthasregion}} SET `objId` = '227' WHERE `id` = '38';
            UPDATE {{gobjecthasregion}} SET `objId` = '223' WHERE `id` = '39';
            UPDATE {{gobjecthasregion}} SET `objId` = '250' WHERE `id` = '40';
            UPDATE {{gobjecthasregion}} SET `objId` = '202' WHERE `id` = '41';
            UPDATE {{gobjecthasregion}} SET `objId` = '204' WHERE `id` = '42';
            UPDATE {{gobjecthasregion}} SET `objId` = '237' WHERE `id` = '43';
            UPDATE {{gobjecthasregion}} SET `objId` = '217' WHERE `id` = '44';
            UPDATE {{gobjecthasregion}} SET `objId` = '238' WHERE `id` = '45';
            UPDATE {{gobjecthasregion}} SET `objId` = '210' WHERE `id` = '46';
            UPDATE {{gobjecthasregion}} SET `objId` = '211' WHERE `id` = '47';
            UPDATE {{gobjecthasregion}} SET `objId` = '205' WHERE `id` = '48';
            UPDATE {{gobjecthasregion}} SET `objId` = '193' WHERE `id` = '49';
            UPDATE {{gobjecthasregion}} SET `objId` = '194' WHERE `id` = '50';
            UPDATE {{gobjecthasregion}} SET `objId` = '207' WHERE `id` = '52';
            UPDATE {{gobjecthasregion}} SET `objId` = '219' WHERE `id` = '53';
            UPDATE {{gobjecthasregion}} SET `objId` = '249' WHERE `id` = '54';
            UPDATE {{gobjecthasregion}} SET `objId` = '214' WHERE `id` = '55';
            UPDATE {{gobjecthasregion}} SET `objId` = '220' WHERE `id` = '56';
            UPDATE {{gobjecthasregion}} SET `objId` = '239' WHERE `id` = '57';
            UPDATE {{gobjecthasregion}} SET `objId` = '240' WHERE `id` = '58';
            UPDATE {{gobjecthasregion}} SET `objId` = '209' WHERE `id` = '59';
            UPDATE {{gobjecthasregion}} SET `objId` = '187' WHERE `id` = '60';
            UPDATE {{gobjecthasregion}} SET `objId` = '228' WHERE `id` = '61';
            UPDATE {{gobjecthasregion}} SET `objId` = '229' WHERE `id` = '62';
            UPDATE {{gobjecthasregion}} SET `objId` = '255' WHERE `id` = '63';
            UPDATE {{gobjecthasregion}} SET `objId` = '232' WHERE `id` = '64';
            UPDATE {{gobjecthasregion}} SET `objId` = '188' WHERE `id` = '65';
            UPDATE {{gobjecthasregion}} SET `objId` = '216' WHERE `id` = '66';
            UPDATE {{gobjecthasregion}} SET `objId` = '189' WHERE `id` = '67';
            UPDATE {{gobjecthasregion}} SET `objId` = '190' WHERE `id` = '68';
            UPDATE {{gobjecthasregion}} SET `objId` = '248' WHERE `id` = '69';
            UPDATE {{gobjecthasregion}} SET `objId` = '191' WHERE `id` = '70';
            UPDATE {{gobjecthasregion}} SET `objId` = '233' WHERE `id` = '71';
            UPDATE {{gobjecthasregion}} SET `objId` = '221' WHERE `id` = '72';
            UPDATE {{gobjecthasregion}} SET `objId` = '230' WHERE `id` = '73';
            UPDATE {{gobjecthasregion}} SET `objId` = '252' WHERE `id` = '74';
            UPDATE {{gobjecthasregion}} SET `objId` = '235' WHERE `id` = '75';
            UPDATE {{gobjecthasregion}} SET `objId` = '234' WHERE `id` = '76';
            UPDATE {{gobjecthasregion}} SET `objId` = '215' WHERE `id` = '77';
            UPDATE {{gobjecthasregion}} SET `objId` = '222' WHERE `id` = '78';
            UPDATE {{gobjecthasregion}} SET `objId` = '257' WHERE `id` = '79';
            UPDATE {{gobjecthasregion}} SET `objId` = '236' WHERE `id` = '80';
            UPDATE {{gobjecthasregion}} SET `objId` = '192' WHERE `id` = '81';
            UPDATE {{gobjecthasregion}} SET `objId` = '241' WHERE `id` = '82';
            UPDATE {{gobjecthasregion}} SET `objId` = '199' WHERE `id` = '83';
            UPDATE {{gobjecthasregion}} SET `objId` = '253' WHERE `id` = '84';
            UPDATE {{gobjecthasregion}} SET `objId` = '195' WHERE `id` = '85';
            UPDATE {{gobjecthasregion}} SET `objId` = '195' WHERE `id` = '86';
            
            CREATE TABLE IF NOT EXISTS {{oldgstattypes}} SELECT * FROM {{gstattypes}};
            DELETE FROM {{gstattypes}};
            
            CREATE TABLE IF NOT EXISTS {{oldgvalue}} SELECT * FROM {{gvalue}};
            DELETE FROM {{gvalue}};
            
            CREATE TABLE IF NOT EXISTS {{oldgobjectshasgstattypes}} SELECT * FROM {{gobjectshasgstattypes}};
            DELETE FROM {{gobjectshasgstattypes}};
            
            ALTER TABLE {{gstattypes}}
            DROP COLUMN `category`,
            DROP COLUMN `epochType`,
            CHANGE COLUMN `gId` `gId` VARCHAR(255) NULL DEFAULT NULL,
            CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL AFTER `gId`,
            CHANGE COLUMN `gType` `type` VARCHAR(45) NULL DEFAULT NULL ,
            CHANGE COLUMN `units` `unit` VARCHAR(45) NULL DEFAULT NULL ,
            ADD COLUMN `parentName` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;
            
            ALTER TABLE {{gvalue}} 
            CHANGE COLUMN `epoch` `epoch` INT(11) NULL DEFAULT NULL AFTER `setId`,
            CHANGE COLUMN `value` `value` FLOAT NULL DEFAULT NULL AFTER `epoch`,
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT; 
            
            ALTER TABLE {{chartcolumn}}
            CHANGE COLUMN `statId` `statId` VARCHAR(45) NULL DEFAULT NULL;
            
            UPDATE {{chartcolumn}} SET `statId` = '56d030def7a9ad0b078f11aa' WHERE `statId` = '103781';
            UPDATE {{chartcolumn}} SET `statId` = '56d030def7a9ad0b078f11a4' WHERE `statId` = '103783';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f21bd4c5654298b7a01' WHERE `statId` = '223019';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f16bd4c5654298b4f36' WHERE `statId` = '223021';
            UPDATE {{chartcolumn}} SET `statId` = '56d0311df7a9add4068f7bb7' WHERE `statId` = '220769';
            UPDATE {{chartcolumn}} SET `statId` = '56d03122f7a9add4068f84a2' WHERE `statId` = '220770';
            UPDATE {{chartcolumn}} SET `statId` = '56d0314bf7a9add4068fc78b' WHERE `statId` = '220772';
            UPDATE {{chartcolumn}} SET `statId` = '56d0315bf7a9add4068fe07f' WHERE `statId` = '220773';
            UPDATE {{chartcolumn}} SET `statId` = '56d03160f7a9add4068fe988' WHERE `statId` = '220774';
            UPDATE {{chartcolumn}} SET `statId` = '56d03183f7a9add4069020ad' WHERE `statId` = '220775';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d4c' WHERE `statId` = '223010';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d4e' WHERE `statId` = '223006';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d3e' WHERE `statId` = '220843';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d4a' WHERE `statId` = '223003';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d50' WHERE `statId` = '223009';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d42' WHERE `statId` = '220844';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f6c' WHERE `statId` = '220848';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d44' WHERE `statId` = '220848';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f70' WHERE `statId` = '223004';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d48' WHERE `statId` = '223004';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d3c' WHERE `statId` = '220842';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d39' WHERE `statId` = '220846';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d56' WHERE `statId` = '223008';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d46' WHERE `statId` = '220847';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d40' WHERE `statId` = '220845';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d54' WHERE `statId` = '223005';
            UPDATE {{chartcolumn}} SET `statId` = '5784b781bd4c56dc4f8b5d52' WHERE `statId` = '223007';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f74' WHERE `statId` = '223018';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f76' WHERE `statId` = '223012';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f66' WHERE `statId` = '220854';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f72' WHERE `statId` = '223017';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f78' WHERE `statId` = '223016';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f6a' WHERE `statId` = '220853';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f6c' WHERE `statId` = '220849';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f70' WHERE `statId` = '223015';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f61' WHERE `statId` = '220851';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f64' WHERE `statId` = '220855';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f7e' WHERE `statId` = '223014';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f6e' WHERE `statId` = '220850';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f7c' WHERE `statId` = '223011';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f68' WHERE `statId` = '220852';
            UPDATE {{chartcolumn}} SET `statId` = '5784b789bd4c56dc4f8b6f7a' WHERE `statId` = '223013';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb59c' WHERE `statId` = '221549';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb59c' WHERE `statId` = '221322';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb5dc' WHERE `statId` = '221550';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb5dc' WHERE `statId` = '221323';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb5ec' WHERE `statId` = '221551';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb5ec' WHERE `statId` = '221324';
            UPDATE {{chartcolumn}} SET `statId` = '56d0a110f7a9adb827ce75ab' WHERE `statId` = '221547';
            UPDATE {{chartcolumn}} SET `statId` = '56d0a110f7a9adb827ce75ab' WHERE `statId` = '221326';
            UPDATE {{chartcolumn}} SET `statId` = '571e43f477d89702498b771f' WHERE `statId` = '221554';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f13bd4c5654298b4569' WHERE `statId` = '221544';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f13bd4c5654298b4569' WHERE `statId` = '221319';
            UPDATE {{chartcolumn}} SET `statId` = '571e452577d89702498c346f' WHERE `statId` = '221555';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f14bd4c5654298b48eb' WHERE `statId` = '221545';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f14bd4c5654298b48eb' WHERE `statId` = '221320';
            UPDATE {{chartcolumn}} SET `statId` = '5784b483bd4c56591c8b5af1' WHERE `statId` = '220732';
            UPDATE {{chartcolumn}} SET `statId` = '5784b484bd4c56591c8b5f4f' WHERE `statId` = '220736';
            UPDATE {{chartcolumn}} SET `statId` = '5784b484bd4c56591c8b5f4f' WHERE `statId` = '221370';
            UPDATE {{chartcolumn}} SET `statId` = '56fd125409eed4ef758b59da' WHERE `statId` = '220840';
            UPDATE {{chartcolumn}} SET `statId` = '56fd125409eed4ef758b59da' WHERE `statId` = '221501';
            UPDATE {{chartcolumn}} SET `statId` = '56fd125409eed4ef758b59db' WHERE `statId` = '221500';
            UPDATE {{chartcolumn}} SET `statId` = '56fd125409eed4ef758b59d9' WHERE `statId` = '221499';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f15bd4c5654298b4c04' WHERE `statId` = '221421';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f15bd4c5654298b4c04' WHERE `statId` = '220837';
            UPDATE {{chartcolumn}} SET `statId` = '5784b5dbbd4c56124d8b5467' WHERE `statId` = '221579';
            UPDATE {{chartcolumn}} SET `statId` = '5784b5dbbd4c56124d8b5467' WHERE `statId` = '147194';
            UPDATE {{chartcolumn}} SET `statId` = '577e574ebd4c56f26c8b4923' WHERE `statId` = '221577';
            UPDATE {{chartcolumn}} SET `statId` = '577e574ebd4c56f26c8b4923' WHERE `statId` = '147197';
            UPDATE {{chartcolumn}} SET `statId` = '56fd125409eed4ef758b59db' WHERE `statId` = '220841';
            UPDATE {{chartcolumn}} SET `statId` = '56fd125409eed4ef758b59d9' WHERE `statId` = '220839';
            UPDATE {{chartcolumn}} SET `statId` = '56ec79f574d8974605996ffe' WHERE `statId` = '220784';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f17bd4c5654298b540e' WHERE `statId` = '221428';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f17bd4c5654298b540e' WHERE `statId` = '148324';
            UPDATE {{chartcolumn}} SET `statId` = '5776391e77d897b008acc728' WHERE `statId` = '221484';
            UPDATE {{chartcolumn}} SET `statId` = '5776391e77d897b008acc728' WHERE `statId` = '140517';
            UPDATE {{chartcolumn}} SET `statId` = '56ec847874d8974605a89f97' WHERE `statId` = '220778';
            UPDATE {{chartcolumn}} SET `statId` = '56d063e9f7a9ad73099fc3ff' WHERE `statId` = '220782';
            UPDATE {{chartcolumn}} SET `statId` = '56d02ef7f7a9ad01088b5796' WHERE `statId` = '220776';
            UPDATE {{chartcolumn}} SET `statId` = '56ec731777d8970e07922590' WHERE `statId` = '221552';
            UPDATE {{chartcolumn}} SET `statId` = '56ec731777d8970e07922590' WHERE `statId` = '220856';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0276d89734208b83ee' WHERE `statId` = '221539';
            UPDATE {{chartcolumn}} SET `statId` = '572b3ec076d89734208b4ae7' WHERE `statId` = '220727';
            UPDATE {{chartcolumn}} SET `statId` = '572b3ec076d89734208b4ae7' WHERE `statId` = '221341';
            UPDATE {{chartcolumn}} SET `statId` = '5784b5dbbd4c56124d8b5467' WHERE `statId` = '221464';
            UPDATE {{chartcolumn}} SET `statId` = '5784b5dbbd4c56124d8b5467' WHERE `statId` = '220951';
            UPDATE {{chartcolumn}} SET `statId` = '56d2f8c2f7a9ad68148cdff1' WHERE `statId` = '221498';
            UPDATE {{chartcolumn}} SET `statId` = '56d2f8c2f7a9ad68148cdff1' WHERE `statId` = '220819';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb5bc' WHERE `statId` = '221548';
            UPDATE {{chartcolumn}} SET `statId` = '56d02eaaf7a9ad0b078bb5bc' WHERE `statId` = '221321';
            UPDATE {{chartcolumn}} SET `statId` = '56ec79f574d897460599707c' WHERE `statId` = '220785';
            UPDATE {{chartcolumn}} SET `statId` = '56d06405f7a9ad7309a0051c' WHERE `statId` = '220783';
            UPDATE {{chartcolumn}} SET `statId` = '577e7e2ebd4c5654298b8509' WHERE `statId` = '221519';
            UPDATE {{chartcolumn}} SET `statId` = '577e7e2ebd4c5654298b8509' WHERE `statId` = '147165';
            UPDATE {{chartcolumn}} SET `statId` = '5784b47fbd4c56591c8b4d1d' WHERE `statId` = '223029';
            UPDATE {{chartcolumn}} SET `statId` = '56ec7b3a74d89746059bf830' WHERE `statId` = '220781';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1abd4c5654298b5f95' WHERE `statId` = '221434';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1abd4c5654298b5f95' WHERE `statId` = '220816';
            UPDATE {{chartcolumn}} SET `statId` = '577e7e2fbd4c5654298b8ad2' WHERE `statId` = '220623';
            UPDATE {{chartcolumn}} SET `statId` = '577e7e2fbd4c5654298b8ad2' WHERE `statId` = '70605';
            UPDATE {{chartcolumn}} SET `statId` = '57a447a775d8974e5b8b4568' WHERE `statId` = '220709';
            UPDATE {{chartcolumn}} SET `statId` = '57a447a775d8974e5b8b4568' WHERE `statId` = '148328';
            UPDATE {{chartcolumn}} SET `statId` = '57a447ac75d8974e5b8b4ea4' WHERE `statId` = '220717';
            UPDATE {{chartcolumn}} SET `statId` = '57a447ac75d8974e5b8b4ea4' WHERE `statId` = '148330';
            UPDATE {{chartcolumn}} SET `statId` = '57a447af75d8974e5b8b5340' WHERE `statId` = '220711';
            UPDATE {{chartcolumn}} SET `statId` = '57a447af75d8974e5b8b5340' WHERE `statId` = '148331';
            UPDATE {{chartcolumn}} SET `statId` = '57a447c075d8974e5b8b57dc' WHERE `statId` = '220713';
            UPDATE {{chartcolumn}} SET `statId` = '57a447c075d8974e5b8b57dc' WHERE `statId` = '148333';
            UPDATE {{chartcolumn}} SET `statId` = '57a447aa75d8974e5b8b4a08' WHERE `statId` = '148329';
            UPDATE {{chartcolumn}} SET `statId` = '57a447aa75d8974e5b8b4a08' WHERE `statId` = '220710';
            UPDATE {{chartcolumn}} SET `statId` = '57a447c575d8974e5b8b60ad' WHERE `statId` = '220719';
            UPDATE {{chartcolumn}} SET `statId` = '57a447c575d8974e5b8b60ad' WHERE `statId` = '148335';
            UPDATE {{chartcolumn}} SET `statId` = '57a447c775d8974e5b8b6549' WHERE `statId` = '220715';
            UPDATE {{chartcolumn}} SET `statId` = '57a447c775d8974e5b8b6549' WHERE `statId` = '148337';
            UPDATE {{chartcolumn}} SET `statId` = '57a447ca75d8974e5b8b69e5' WHERE `statId` = '220716';
            UPDATE {{chartcolumn}} SET `statId` = '57a447ca75d8974e5b8b69e5' WHERE `statId` = '148338';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0676d89734208b9330' WHERE `statId` = '221532';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0676d89734208b9330' WHERE `statId` = '221329';
            UPDATE {{chartcolumn}} SET `statId` = '572b3ef776d89734208b5ced' WHERE `statId` = '221529';
            UPDATE {{chartcolumn}} SET `statId` = '572b3ef776d89734208b5ced' WHERE `statId` = '221327';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5676d89734208be205' WHERE `statId` = '221522';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5676d89734208be205' WHERE `statId` = '149043';
            UPDATE {{chartcolumn}} SET `statId` = '572b3efc76d89734208b6ceb' WHERE `statId` = '221534';
            UPDATE {{chartcolumn}} SET `statId` = '572b3efc76d89734208b6ceb' WHERE `statId` = '221331';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5676d89734208be543' WHERE `statId` = '221526';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5676d89734208be543' WHERE `statId` = '149041';
            UPDATE {{chartcolumn}} SET `statId` = '572b3efd76d89734208b719c' WHERE `statId` = '221535';
            UPDATE {{chartcolumn}} SET `statId` = '572b3efd76d89734208b719c' WHERE `statId` = '221332';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5776d89734208be881' WHERE `statId` = '221523';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5776d89734208be881' WHERE `statId` = '149044';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0876d89734208b992c' WHERE `statId` = '221540';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0876d89734208b992c' WHERE `statId` = '221337';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5976d89734208bf01b' WHERE `statId` = '221528';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5976d89734208bf01b' WHERE `statId` = '149048';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0376d89734208b8897' WHERE `statId` = '221541';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5876d89734208bebbf' WHERE `statId` = '221524';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0376d89734208b8897' WHERE `statId` = '221338';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5876d89734208bebbf' WHERE `statId` = '149045';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f0276d89734208b83ee' WHERE `statId` = '221336';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5976d89734208beefd' WHERE `statId` = '221530';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5976d89734208beefd' WHERE `statId` = '149046';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5576d89734208bdec7' WHERE `statId` = '221527';
            UPDATE {{chartcolumn}} SET `statId` = '572b3f5576d89734208bdec7' WHERE `statId` = '149042';
            UPDATE {{chartcolumn}} SET `statId` = '56d030def7a9ad0b078f119e' WHERE `statId` = '103784';
            UPDATE {{chartcolumn}} SET `statId` = '571e45e177d89702498ca67f' WHERE `statId` = '221514';
            UPDATE {{chartcolumn}} SET `statId` = '571e451c77d89702498c2d61' WHERE `statId` = '221515';
            UPDATE {{chartcolumn}} SET `statId` = '577e7c79bd4c562e658b465a' WHERE `statId` = '221502';
            UPDATE {{chartcolumn}} SET `statId` = '572b3efc76d89734208b6ceb' WHERE `statId` = '221503';
            UPDATE {{chartcolumn}} SET `statId` = '571e43f377d89702498b7475' WHERE `statId` = '221505';
            UPDATE {{chartcolumn}} SET `statId` = '577e7c79bd4c562e658b4684' WHERE `statId` = '221423';
            UPDATE {{chartcolumn}} SET `statId` = '577e7c79bd4c562e658b4684' WHERE `statId` = '220834';
            UPDATE {{chartcolumn}} SET `statId` = '571e448d77d89702498bdb5d' WHERE `statId` = '221430';
            UPDATE {{chartcolumn}} SET `statId` = '572b3ef776d89734208b5ced' WHERE `statId` = '221504';
            UPDATE {{chartcolumn}} SET `statId` = '571e446c77d89702498bc5ed' WHERE `statId` = '221429';
            UPDATE {{chartcolumn}} SET `statId` = '571e443077d89702498ba0e3' WHERE `statId` = '221506';
            UPDATE {{chartcolumn}} SET `statId` = '571e449a77d89702498bdec6' WHERE `statId` = '221507';
            UPDATE {{chartcolumn}} SET `statId` = '571e44ae77d89702498befb9' WHERE `statId` = '221508';
            UPDATE {{chartcolumn}} SET `statId` = '571e44c477d89702498bfc67' WHERE `statId` = '221425';
            UPDATE {{chartcolumn}} SET `statId` = '571e44d677d89702498c0a4c' WHERE `statId` = '221509';
            UPDATE {{chartcolumn}} SET `statId` = '571e44d577d89702498c073f' WHERE `statId` = '221510';
            UPDATE {{chartcolumn}} SET `statId` = '571e467977d89702498cca97' WHERE `statId` = '221557';
            UPDATE {{chartcolumn}} SET `statId` = '56d02ef7f7a9ad01088b5796' WHERE `statId` = '221419';
            UPDATE {{chartcolumn}} SET `statId` = '571e451377d89702498c26ff' WHERE `statId` = '221511';
            UPDATE {{chartcolumn}} SET `statId` = '571e451d77d89702498c2dd7' WHERE `statId` = '221516';
            UPDATE {{chartcolumn}} SET `statId` = '571e456577d89702498c5a15' WHERE `statId` = '221512';
            UPDATE {{chartcolumn}} SET `statId` = '571e45d777d89702498c9e6a' WHERE `statId` = '221513';
            UPDATE {{chartcolumn}} SET `statId` = '571e467377d89702498cc8a3' WHERE `statId` = '221556';
            UPDATE {{chartcolumn}} SET `statId` = '571e46b177d89702498cf042' WHERE `statId` = '221558';
            UPDATE {{chartcolumn}} SET `statId` = '571e46d877d89702498d0936' WHERE `statId` = '221426';
            UPDATE {{chartcolumn}} SET `statId` = '571e46e777d89702498d109a' WHERE `statId` = '221517';
            UPDATE {{chartcolumn}} SET `statId` = '571e46e877d89702498d12eb' WHERE `statId` = '221427';
            UPDATE {{chartcolumn}} SET `statId` = '5784b78bbd4c56dc4f8b72e7' WHERE `statId` = '221431';
            UPDATE {{chartcolumn}} SET `statId` = '571e45b677d89702498c8a03' WHERE `statId` = '221559';
            UPDATE {{chartcolumn}} SET `statId` = '56ec853b74d8974605aa82e3' WHERE `statId` = '220779';
            UPDATE {{chartcolumn}} SET `statId` = '577e7e31bd4c5654298b9031' WHERE `statId` = '221553';
            UPDATE {{chartcolumn}} SET `statId` = '577e7e31bd4c5654298b9031' WHERE `statId` = '114202';
            UPDATE {{chartcolumn}} SET `statId` = '56d0518ff7a9adf30997bab2' WHERE `statId` = '220780';
            UPDATE {{chartcolumn}} SET `statId` = '5784b485bd4c56591c8b6399' WHERE `statId` = '220731';
            UPDATE {{chartcolumn}} SET `statId` = '5784b485bd4c56591c8b6176' WHERE `statId` = '220735';
            UPDATE {{chartcolumn}} SET `statId` = '5784b485bd4c56591c8b6176' WHERE `statId` = '221355';
            UPDATE {{chartcolumn}} SET `statId` = '56ec858b74d8974605ab44c6' WHERE `statId` = '220788';
            UPDATE {{chartcolumn}} SET `statId` = '5784b47dbd4c56591c8b4568' WHERE `statId` = '223026';
            UPDATE {{chartcolumn}} SET `statId` = '572facee73d897e665f8a38f' WHERE `statId` = '221470';
            UPDATE {{chartcolumn}} SET `statId` = '572fad0773d897e665f8f8b4' WHERE `statId` = '221469';
            UPDATE {{chartcolumn}} SET `statId` = '572fad0773d897e665f8f8b4' WHERE `statId` = '220937';
            UPDATE {{chartcolumn}} SET `statId` = '572facee73d897e665f8a38f' WHERE `statId` = '220925';
            UPDATE {{chartcolumn}} SET `statId` = '572fad0c73d897e665f909a6' WHERE `statId` = '221479';
            UPDATE {{chartcolumn}} SET `statId` = '572fad0c73d897e665f909a6' WHERE `statId` = '220939';
            UPDATE {{chartcolumn}} SET `statId` = '5784b47ebd4c56591c8b4958' WHERE `statId` = '223025';
            UPDATE {{chartcolumn}} SET `statId` = '56ec84ee74d8974605a991cd' WHERE `statId` = '220777';
            UPDATE {{chartcolumn}} SET `statId` = '5784b480bd4c56591c8b4faf' WHERE `statId` = '223027';
            UPDATE {{chartcolumn}} SET `statId` = '56ec79f574d89746059970ad' WHERE `statId` = '220787';
            UPDATE {{chartcolumn}} SET `statId` = '56d2f8d4f7a9ad68148d1530' WHERE `statId` = '221432';
            UPDATE {{chartcolumn}} SET `statId` = '56d2f8d4f7a9ad68148d1530' WHERE `statId` = '220815';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1ebd4c5654298b6eb8' WHERE `statId` = '221449';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1ebd4c5654298b6eb8' WHERE `statId` = '220831';
            UPDATE {{chartcolumn}} SET `statId` = '5784b487bd4c56591c8b6a26' WHERE `statId` = '221546';
            UPDATE {{chartcolumn}} SET `statId` = '5784b487bd4c56591c8b6a26' WHERE `statId` = '221325';
            UPDATE {{chartcolumn}} SET `statId` = '56d030def7a9ad0b078f11aa' WHERE `statId` = '221343';
            UPDATE {{chartcolumn}} SET `statId` = '56d030def7a9ad0b078f11aa' WHERE `statId` = '103781';
            UPDATE {{chartcolumn}} SET `statId` = '56ec793a75d897c6069d7b82' WHERE `statId` = '150772';
            UPDATE {{chartcolumn}} SET `statId` = '56ec793a75d897c6069d7b82' WHERE `statId` = '70294';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f19bd4c5654298b5c48' WHERE `statId` = '221424';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f19bd4c5654298b5c48' WHERE `statId` = '220836';
            UPDATE {{chartcolumn}} SET `statId` = '5776396277d897b008ad7798' WHERE `statId` = '221491';
            UPDATE {{chartcolumn}} SET `statId` = '5776396277d897b008ad7798' WHERE `statId` = '140516';
            UPDATE {{chartcolumn}} SET `statId` = '56d2f8aff7a9ad68148caaa4' WHERE `statId` = '221433';
            UPDATE {{chartcolumn}} SET `statId` = '56d2f8aff7a9ad68148caaa4' WHERE `statId` = '103707';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1cbd4c5654298b64e0' WHERE `statId` = '221594';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1cbd4c5654298b64e0' WHERE `statId` = '114199';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1dbd4c5654298b6b70' WHERE `statId` = '221593';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1dbd4c5654298b6b70' WHERE `statId` = '114201';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1fbd4c5654298b736b' WHERE `statId` = '221422';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f1fbd4c5654298b736b' WHERE `statId` = '220835';
            UPDATE {{chartcolumn}} SET `statId` = '5745b180bd4c56b4668b4713' WHERE `statId` = '220728';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f20bd4c5654298b76f6' WHERE `statId` = '221420';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f20bd4c5654298b76f6' WHERE `statId` = '220838';
            UPDATE {{chartcolumn}} SET `statId` = '577e6f20bd4c5654298b76f6' WHERE `statId` = '220838';
            UPDATE {{chartcolumn}} SET `statId` = '500000' WHERE `statId` = '500001';
            UPDATE {{chartcolumn}} SET `statId` = '500002' WHERE `statId` = '500003';
            UPDATE {{chartcolumn}} SET `statId` = '500004' WHERE `statId` = '500005';
            UPDATE {{chartcolumn}} SET `statId` = '500006' WHERE `statId` = '500007';
            
            ALTER TABLE {{gobjectshasgstattypes}} 
            DROP COLUMN `frequency`,
            DROP COLUMN `groupName`,
            DROP COLUMN `groupId`;
            
            UPDATE {{charttable}} SET `name`='РЕГИОНАЛЬНЫЙ ТОВАРНЫЙ ОБЪЕМ И ДОЛЯ ОТРАСЛЕЙ СЕЛЬСКОГО ХОЗЯЙСТВА В С/Х ОРГАНИЗАЦИЯХ' WHERE `id`='5';
            UPDATE {{charttable}} SET `name`='РЕГИОНАЛЬНЫЙ ТОВАРНЫЙ ОБЪЕМ И ДОЛЯ ОТРАСЛЕЙ СЕЛЬСКОГО ХОЗЯЙСТВА В КФХ B ИП' WHERE `id`='6';
            UPDATE {{charttable}} SET `name`='ЭКСПОРТ / ИМПОРТ СЕЛЬСКОХОЗЯЙСТВЕННОЙ ПРОДУКЦИИ' WHERE `id`='9';
            
            ALTER TABLE {{chart}} 
            CHANGE COLUMN `typeId` `typeId` VARCHAR(50) NULL DEFAULT NULL ;
            
            UPDATE {{chart}} SET `typeId`='56ec84ee74d8974605a991cd' WHERE `typeId`='220777';
            UPDATE {{chart}} SET `typeId`='56d02ef7f7a9ad01088b5796' WHERE `typeId`='220776';
            UPDATE {{chart}} SET `typeId`='56d0518ff7a9adf30997bab2' WHERE `typeId`='220780';
            UPDATE {{chart}} SET `typeId`='56ec853b74d8974605aa82e3' WHERE `typeId`='220779';
            UPDATE {{chart}} SET `typeId`='56ec847874d8974605a89f97' WHERE `typeId`='220778';
            UPDATE {{chart}} SET `typeId`='56ec79f574d8974605996ffe' WHERE `typeId`='220784';
            UPDATE {{chart}} SET `typeId`='56ec79f574d89746059970ad' WHERE `typeId`='220787';
            UPDATE {{chart}} SET `typeId`='56ec79f574d897460599707c' WHERE `typeId`='220785';
            UPDATE {{chart}} SET `typeId`='56ec858b74d8974605ab44c6' WHERE `typeId`='220788';
            UPDATE {{chart}} SET `typeId`='5784b47dbd4c56591c8b4568' WHERE `typeId`='223026';
            UPDATE {{chart}} SET `typeId`='56d0311df7a9add4068f7bb7' WHERE `typeId`='220769';
            UPDATE {{chart}} SET `typeId`='56d03183f7a9add4069020ad' WHERE `typeId`='220775';
            UPDATE {{chart}} SET `typeId`='56d03160f7a9add4068fe988' WHERE `typeId`='220774';
            UPDATE {{chart}} SET `typeId`='56d03122f7a9add4068f84a2' WHERE `typeId`='220770';
            UPDATE {{chart}} SET `typeId`='56d0314bf7a9add4068fc78b' WHERE `typeId`='220772';
            UPDATE {{chart}} SET `typeId`='56d0315bf7a9add4068fe07f' WHERE `typeId`='220773';
            UPDATE {{chart}} SET `typeId`='56d030def7a9ad0b078f11aa' WHERE `typeId`='103781';
            UPDATE {{chart}} SET `typeId`='5745b180bd4c56b4668b4713' WHERE `typeId`='220728';
            UPDATE {{chart}} SET `typeId`='56d030def7a9ad0b078f11a4' WHERE `typeId`='103783';
            UPDATE {{chart}} SET `typeId`='5784b485bd4c56591c8b6399' WHERE `typeId`='220731';
            UPDATE {{chart}} SET `typeId`='56d030def7a9ad0b078f119e' WHERE `typeId`='103784';
            UPDATE {{chart}} SET `typeId`='5784b485bd4c56591c8b6176' WHERE `typeId`='220735';
            UPDATE {{chart}} SET `typeId`='5784b483bd4c56591c8b5af1' WHERE `typeId`='220732';
            UPDATE {{chart}} SET `typeId`='5784b484bd4c56591c8b5f4f' WHERE `typeId`='220736';
            UPDATE {{chart}} SET `typeId`='5784b47fbd4c56591c8b4d1d' WHERE `typeId`='223029';
            UPDATE {{chart}} SET `typeId`='56ec7b3a74d89746059bf830' WHERE `typeId`='220781';
            UPDATE {{chart}} SET `typeId`='5784b480bd4c56591c8b4faf' WHERE `typeId`='223027';
            UPDATE {{chart}} SET `typeId`='5784b47ebd4c56591c8b4958' WHERE `typeId`='223025';
            UPDATE {{chart}} SET `typeId`='56d063e9f7a9ad73099fc3ff' WHERE `typeId`='220782';
            UPDATE {{chart}} SET `typeId`='56d06405f7a9ad7309a0051c' WHERE `typeId`='220783';
            UPDATE {{chart}} SET `typeId`='56d2f8d4f7a9ad68148d1530' WHERE `typeId`='220815';
            UPDATE {{chart}} SET `typeId`='56d2f8aff7a9ad68148caaa4' WHERE `typeId`='103707';
            UPDATE {{chart}} SET `typeId`='577e6f1abd4c5654298b5f95' WHERE `typeId`='220816';
            UPDATE {{chart}} SET `typeId`='5776391e77d897b008acc728' WHERE `typeId`='140517';
            UPDATE {{chart}} SET `typeId`='577e6f1ebd4c5654298b6eb8' WHERE `typeId`='220831';
            UPDATE {{chart}} SET `typeId`='56fd125409eed4ef758b59da' WHERE `typeId`='220840';
            UPDATE {{chart}} SET `typeId`='56fd125409eed4ef758b59db' WHERE `typeId`='220841';
            UPDATE {{chart}} SET `typeId`='577e7c79bd4c562e658b4684' WHERE `typeId`='220834';
            UPDATE {{chart}} SET `typeId`='577e6f1fbd4c5654298b736b' WHERE `typeId`='220835';
            UPDATE {{chart}} SET `typeId`='577e6f17bd4c5654298b540e' WHERE `typeId`='148324';
            UPDATE {{chart}} SET `typeId`='577e6f19bd4c5654298b5c48' WHERE `typeId`='220836';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f7e' WHERE `typeId`='223014';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d56' WHERE `typeId`='223008';
            UPDATE {{chart}} SET `typeId`='577e6f20bd4c5654298b76f6' WHERE `typeId`='220838';
            UPDATE {{chart}} SET `typeId`='577e6f15bd4c5654298b4c04' WHERE `typeId`='220837';
            UPDATE {{chart}} SET `typeId`='56d2f8c2f7a9ad68148cdff1' WHERE `typeId`='220819';
            UPDATE {{chart}} SET `typeId`='577e574ebd4c56f26c8b4923' WHERE `typeId`='147197';
            UPDATE {{chart}} SET `typeId`='572b3ec076d89734208b4ae7' WHERE `typeId`='220727';
            UPDATE {{chart}} SET `typeId`='56ec793a75d897c6069d7b82' WHERE `typeId`='70294';
            UPDATE {{chart}} SET `typeId`='56fd125409eed4ef758b59d9' WHERE `typeId`='220839';
            UPDATE {{chart}} SET `typeId`='56fd125409eed4ef758b59da' WHERE `typeId`='220840';
            UPDATE {{chart}} SET `typeId`='56fd125409eed4ef758b59db' WHERE `typeId`='220841';
            UPDATE {{chart}} SET `typeId`='572b3ec076d89734208b4ae7' WHERE `typeId`='220727';
            UPDATE {{chart}} SET `typeId`='577e7e2fbd4c5654298b8ad2' WHERE `typeId`='70605';
            UPDATE {{chart}} SET `typeId`='577e7e2ebd4c5654298b8509' WHERE `typeId`='147165';
            UPDATE {{chart}} SET `typeId`='56ec793a75d897c6069d7b82' WHERE `typeId`='70294';
            UPDATE {{chart}} SET `typeId`='57a447a775d8974e5b8b4568' WHERE `typeId`='148328';
            UPDATE {{chart}} SET `typeId`='57a447c075d8974e5b8b57dc' WHERE `typeId`='148333';
            UPDATE {{chart}} SET `typeId`='57a447c575d8974e5b8b60ad' WHERE `typeId`='148335';
            UPDATE {{chart}} SET `typeId`='57a447aa75d8974e5b8b4a08' WHERE `typeId`='148329';
            UPDATE {{chart}} SET `typeId`='57a447c775d8974e5b8b6549' WHERE `typeId`='148337';
            UPDATE {{chart}} SET `typeId`='57a447af75d8974e5b8b5340' WHERE `typeId`='148331';
            UPDATE {{chart}} SET `typeId`='57a447ac75d8974e5b8b4ea4' WHERE `typeId`='148330';
            UPDATE {{chart}} SET `typeId`='57a447ca75d8974e5b8b69e5' WHERE `typeId`='148338';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6c' WHERE `typeId`='220849';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d3e' WHERE `typeId`='220843';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f64' WHERE `typeId`='220855';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d3c' WHERE `typeId`='220842';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f66' WHERE `typeId`='220854';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d3e' WHERE `typeId`='220843';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f68' WHERE `typeId`='220852';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d40' WHERE `typeId`='220845';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6a' WHERE `typeId`='220853';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d42' WHERE `typeId`='220844';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6c' WHERE `typeId`='220849';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6c' WHERE `typeId`='220848';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6e' WHERE `typeId`='220850';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d46' WHERE `typeId`='220847';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f70' WHERE `typeId`='223015';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f70' WHERE `typeId`='223004';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f72' WHERE `typeId`='223017';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d4a' WHERE `typeId`='223003';
            UPDATE {{chart}} SET `typeId`='572b3efc76d89734208b6ceb' WHERE `typeId`='221331';
            UPDATE {{chart}} SET `typeId`='572b3f5676d89734208be543' WHERE `typeId`='149041';
            UPDATE {{chart}} SET `typeId`='572b3efd76d89734208b719c' WHERE `typeId`='221332';
            UPDATE {{chart}} SET `typeId`='572b3f5776d89734208be881' WHERE `typeId`='149044';
            UPDATE {{chart}} SET `typeId`='572b3f0276d89734208b83ee' WHERE `typeId`='221336';
            UPDATE {{chart}} SET `typeId`='572b3f5976d89734208beefd' WHERE `typeId`='149046';
            UPDATE {{chart}} SET `typeId`='572b3f0676d89734208b9330' WHERE `typeId`='221329';
            UPDATE {{chart}} SET `typeId`='572b3f5676d89734208be205' WHERE `typeId`='149043';
            UPDATE {{chart}} SET `typeId`='572b3ef776d89734208b5ced' WHERE `typeId`='221327';
            UPDATE {{chart}} SET `typeId`='572b3f5576d89734208bdec7' WHERE `typeId`='149042';
            UPDATE {{chart}} SET `typeId`='572b3f0376d89734208b8897' WHERE `typeId`='221338';
            UPDATE {{chart}} SET `typeId`='572b3f5876d89734208bebbf' WHERE `typeId`='149045';
            UPDATE {{chart}} SET `typeId`='572b3f0876d89734208b992c' WHERE `typeId`='221337';
            UPDATE {{chart}} SET `typeId`='572b3f5976d89734208bf01b' WHERE `typeId`='149048';
            UPDATE {{chart}} SET `typeId`='56d0311df7a9add4068f7bb7' WHERE `typeId`='220769';
            UPDATE {{chart}} SET `typeId`='56d03183f7a9add4069020ad' WHERE `typeId`='220775';
            UPDATE {{chart}} SET `typeId`='56d03160f7a9add4068fe988' WHERE `typeId`='220774';
            UPDATE {{chart}} SET `typeId`='56d03122f7a9add4068f84a2' WHERE `typeId`='220770';
            UPDATE {{chart}} SET `typeId`='56d0314bf7a9add4068fc78b' WHERE `typeId`='220772';
            UPDATE {{chart}} SET `typeId`='56d0315bf7a9add4068fe07f' WHERE `typeId`='220773';
            UPDATE {{chart}} SET `typeId`='56ec84ee74d8974605a991cd' WHERE `typeId`='220777';
            UPDATE {{chart}} SET `typeId`='56d02ef7f7a9ad01088b5796' WHERE `typeId`='220776';
            UPDATE {{chart}} SET `typeId`='56d0518ff7a9adf30997bab2' WHERE `typeId`='220780';
            UPDATE {{chart}} SET `typeId`='56ec853b74d8974605aa82e3' WHERE `typeId`='220779';
            UPDATE {{chart}} SET `typeId`='56ec847874d8974605a89f97' WHERE `typeId`='220778';
            UPDATE {{chart}} SET `typeId`='577e6f21bd4c5654298b7a01' WHERE `typeId`='223019';
            UPDATE {{chart}} SET `typeId`='577e6f16bd4c5654298b4f36' WHERE `typeId`='223021';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d39' WHERE `typeId`='220846';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d3c' WHERE `typeId`='220842';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d3e' WHERE `typeId`='220843';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f61' WHERE `typeId`='220851';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f64' WHERE `typeId`='220855';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f66' WHERE `typeId`='220854';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d40' WHERE `typeId`='220845';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d42' WHERE `typeId`='220844';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f68' WHERE `typeId`='220852';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6a' WHERE `typeId`='220853';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6c' WHERE `typeId`='220848';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d46' WHERE `typeId`='220847';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f70' WHERE `typeId`='223004';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d4a' WHERE `typeId`='223003';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6c' WHERE `typeId`='220849';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f6e' WHERE `typeId`='220850';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f70' WHERE `typeId`='223015';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f72' WHERE `typeId`='223017';
            UPDATE {{chart}} SET `typeId`='577e6f1cbd4c5654298b64e0' WHERE `typeId`='114199';
            UPDATE {{chart}} SET `typeId`='577e6f14bd4c5654298b48eb' WHERE `typeId`='221320';
            UPDATE {{chart}} SET `typeId`='572fad0c73d897e665f909a6' WHERE `typeId`='220939';
            UPDATE {{chart}} SET `typeId`='5784b487bd4c56591c8b6a26' WHERE `typeId`='221325';
            UPDATE {{chart}} SET `typeId`='56d0a110f7a9adb827ce75ab' WHERE `typeId`='221326';
            UPDATE {{chart}} SET `typeId`='56d02eaaf7a9ad0b078bb5bc' WHERE `typeId`='221321';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d4c' WHERE `typeId`='223010';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d4e' WHERE `typeId`='223006';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d50' WHERE `typeId`='223009';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f74' WHERE `typeId`='223018';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f76' WHERE `typeId`='223012';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f78' WHERE `typeId`='223016';
            UPDATE {{chart}} SET `typeId`='5784b5dbbd4c56124d8b5467' WHERE `typeId`='220951';
            UPDATE {{chart}} SET `typeId`='5784b5dbbd4c56124d8b5467' WHERE `typeId`='147194';
            UPDATE {{chart}} SET `typeId`='572fad0773d897e665f8f8b4' WHERE `typeId`='220937';
            UPDATE {{chart}} SET `typeId`='56ec731777d8970e07922590' WHERE `typeId`='220856';
            UPDATE {{chart}} SET `typeId`='577e7e31bd4c5654298b9031' WHERE `typeId`='114202';
            UPDATE {{chart}} SET `typeId`='577e6f1dbd4c5654298b6b70' WHERE `typeId`='114201';
            UPDATE {{chart}} SET `typeId`='5776396277d897b008ad7798' WHERE `typeId`='140516';
            UPDATE {{chart}} SET `typeId`='577e6f13bd4c5654298b4569' WHERE `typeId`='221319';
            UPDATE {{chart}} SET `typeId`='572facee73d897e665f8a38f' WHERE `typeId`='220925';
            UPDATE {{chart}} SET `typeId`='56d02eaaf7a9ad0b078bb5ec' WHERE `typeId`='221324';
            UPDATE {{chart}} SET `typeId`='56d02eaaf7a9ad0b078bb59c' WHERE `typeId`='221322';
            UPDATE {{chart}} SET `typeId`='56d02eaaf7a9ad0b078bb5dc' WHERE `typeId`='221323';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d52' WHERE `typeId`='223007';
            UPDATE {{chart}} SET `typeId`='5784b781bd4c56dc4f8b5d54' WHERE `typeId`='223005';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f7a' WHERE `typeId`='223013';
            UPDATE {{chart}} SET `typeId`='5784b789bd4c56dc4f8b6f7c' WHERE `typeId`='223011';
            UPDATE {{chart}} SET `typeId`='500004' WHERE `typeId`='500005';
            UPDATE {{chart}} SET `typeId`='500006' WHERE `typeId`='500007';
            
            ALTER TABLE {{gobjectshasgstattypes}} 
            ADD INDEX `fk_gobjectshasgstattypes_2_idx` (`statId` ASC);
            
            ALTER TABLE {{gobjectshasgstattypes}} 
            ADD CONSTRAINT `fk_gobjectshasgstattypes_1`
              FOREIGN KEY (`objId`)
              REFERENCES {{gobjects}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE,
            ADD CONSTRAINT `fk_gobjectshasgstattypes_2`
              FOREIGN KEY (`statId`)
              REFERENCES {{gstattypes}} (`gId`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
              
            ALTER TABLE {{gvalue}} 
            ADD CONSTRAINT `fk_gvalue_1`
              FOREIGN KEY (`setId`)
              REFERENCES {{gobjectshasgstattypes}} (`id`)
              ON DELETE CASCADE
              ON UPDATE CASCADE;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}