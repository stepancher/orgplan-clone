<?php

class m160706_113531_update_9_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "
			DELETE FROM {{attributeclass}} WHERE `id`='2626';
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2649', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`id`, `attributeClass`, `defaultValue`, `class`) VALUES (NULL, '2649', '2016-10-07', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2652', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2652', '2016-10-07', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2655', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2655', '2016-10-07', 'endDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2658', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2658', '2016-10-07', 'endDate');
			INSERT INTO {{attributemodel}} (`elementClass`, `attributeClass`, `required`, `fixed`, `priority`) VALUES ('25', '2665', '0', '0', '60');
			UPDATE {{attributemodel}} SET `attributeClass`='2666' WHERE `id`='235';
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2669', 'ru', 'Экспонент');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2672', 'ru', 'Организатор');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2675', 'ru', 'подпись');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2676', 'ru', 'подпись');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2670', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2671', 'ru', 'ФИО');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2673', 'ru', 'должность');
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '2674', 'ru', 'ФИО');
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2645';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2646';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2647';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2650';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2653';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2656';
			UPDATE {{attributeclass}} SET `class`='coefficientDouble' WHERE `id`='2659';
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2645', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2646', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2647', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2650', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2653', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2656', '2623', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2659', '2623', 'inherit', '0');
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3021';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3022';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3023';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3026';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3029';
			UPDATE {{trattributeclass}} SET `tooltip`='<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.' WHERE `id`='3032';
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `tooltip`) VALUES ('2659', 'ru', '<div class=\"hint-header\">Стоимость услуг</div>Стоимость услуг увеличивается на 100% при заказе после 17.09.16 г.');

		";
	}
}