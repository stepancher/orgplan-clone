<?php

class m161213_065831_insert_trregions extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

/*(\d+);([ёa-zA-Zа-яА-я\s\(\)\-,\е́]+);([a-zA-Zа-яА-я\s\(\)\-,\–]+);([a-zA-Zа-яА-я\s\(\)\-,\–\ü\ʻ\ö\ş\ý]+)
INSERT INTO tbl_trregion (trParentId, name, langId) VALUES($1,'$3', 'en');\n
INSERT INTO tbl_trregion (trParentId, name, langId) VALUES($1,'$4', 'de');\n
delete from tbl_trregion where langId = 'en';
delete from tbl_trregion where langId = 'de';*/

    public function upSql()
    {
        return "
            DELETE FROM tbl_trregion WHERE langId = 'en';
            DELETE FROM tbl_trregion WHERE langId = 'de';

            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(1,'Altai Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(1,'Region Altai', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(2,'Amur Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(2,'Oblast Amur', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(3,'Arkhangelsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(3,'Oblast Archangelsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(4,'Astrakhan Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(4,'Oblast Astrachan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(5,'Belgorod Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(5,'Oblast Belgorod', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(6,'Bryansk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(6,'Oblast Brjansk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(7,'Vladimir Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(7,'Oblast Wladimir', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(8,'Volgograd Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(8,'Oblast Wolgograd', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(9,'Vologda Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(9,'Oblast Wologda', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(10,'Voronezh Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(10,'Oblast Woronesch', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(11,'Jewish Autonomous Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(11,'Jüdische Autonome Oblast', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(12,'Zabaykalsky Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(12,'Region Transbaikalien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(13,'Ivanovo Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(13,'Oblast Iwanowo', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(14,'Irkutsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(14,'Oblast Irkutsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(15,'Kabardino-Balkar Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(15,'Republik Kabardino- Balkarien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(16,'Kaliningrad Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(16,'Oblast Kaliningrad', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(17,'Kaluga Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(17,'Oblast Kaluga', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(18,'Kamchatka Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(18,'Region Kamtschatka', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(19,'Karachay-Cherkess Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(19,'Republik Karatschai-Tscherkessien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(20,'Kemerovo Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(20,'Oblast Kemerowo', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(21,'Kirov Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(21,'Oblast Kirow', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(22,'Kostroma Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(22,'Oblast Kostroma', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(23,'Krasnodar Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(23,'Region Krasnodar', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(24,'Krasnoyarsk Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(24,'Region Krasnojarsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(25,'Kurgan Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(25,'Oblast Kurgan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(26,'Kursk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(26,'Oblast Kursk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(27,'Leningrad Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(27,'Oblast Leningrad', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(28,'Lipetsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(28,'Oblast Lipezk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(29,'Magadan Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(29,'Oblast Magadan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(30,'Moscow', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(30,'Moskau', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(31,'Moscow Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(31,'Oblast Moskau', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(32,'Murmansk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(32,'Oblast Murmansk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(33,'Nenets Autonomous Okrug', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(33,'Autonomer Kreis der Nenzen', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(34,'Nizhny Novgorod Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(34,'Oblast Nischni Nowgorod', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(35,'Novgorod Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(35,'Oblast Nowgorod', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(36,'Novosibirsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(36,'Oblast Nowosibirsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(37,'Omsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(37,'Oblast Omsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(38,'Orenburg Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(38,'Oblast Orenburg', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(39,'Oryol Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(39,'Oblast Orjol', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(40,'Penza Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(40,'Oblast Pensa', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(41,'Perm Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(41,'Region Perm', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(42,'Primorsky Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(42,'Region Primorje', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(43,'Pskov Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(43,'Oblast Pskow', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(44,'Republic of Adygea', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(44,'Republik Adygeja', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(45,'Altai Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(45,'Republik Altai', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(46,'Republic of Bashkortostan', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(46,'Republik Baschkortostan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(47,'Republic of Buryatia', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(47,'Republik Burjatien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(48,'Republic of Dagestan', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(48,'Republik Dagestan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(49,'Republic of Ingushetia', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(49,'Republik Inguschetien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(50,'Republic of Kalmykia', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(50,'Republik Kalmückien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(51,'Republic of Karelia', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(51,'Republik Karelien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(52,'Komi Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(52,'Republik Komi', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(53,'Mari El Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(53,'Republik Mari El', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(54,'Republic of Mordovia', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(54,'Republik Mordwinien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(55,'Sakha (Yakutia) Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(55,'Republik Sacha (Jakutien)', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(56,'Republic of North Ossetia-Alania', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(56,'Republik Nordossetien-Alanien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(57,'Republic of Tatarstan', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(57,'Republik Tatarstan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(58,'Republic of Tyva', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(58,'Republik Tuwa', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(59,'Republic of Khakassia', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(59,'Republik Chakassien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(60,'Rostov Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(60,'Oblast Rostow', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(61,'Ryazan Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(61,'Oblast Rjasan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(62,'Samara Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(62,'Oblast Samara', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(63,'Saint Petersburg', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(63,'Sankt Petersburg', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(64,'Saratov Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(64,'Oblast Saratow', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(65,'Sakhalin Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(65,'Oblast Sachalin', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(66,'Sverdlovsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(66,'Oblast Swerdlowsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(67,'Smolensk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(67,'Oblast Smolensk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(68,'Stavropol Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(68,'Region Stawropol', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(69,'Tambov Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(69,'Oblast Tambow', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(70,'Tver Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(70,'Oblast Twer', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(71,'Tomsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(71,'Oblast Tomsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(72,'Tula Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(72,'Oblast Tula', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(73,'Tyumen Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(73,'Oblast Tjumen', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(74,'Udmurt Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(74,'Republik Udmurtien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(75,'Ulyanovsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(75,'Oblast Uljanowsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(76,'Khabarovsk Krai', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(76,'Region Chabarowsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(77,'Khanty-Mansiysk Autonomous Okrug – Ugra', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(77,'Autonomer Kreis der Chanten und Mansen - Jugra', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(78,'Chelyabinsk Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(78,'Oblast Tscheljabinsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(79,'Chechen Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(79,'Republik Tschetschenien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(80,'Chuvash Republic', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(80,'Republik Tschuwaschien', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(81,'Chukotka Autonomous Okrug', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(81,'Autonomer Kreis der Tschuktschen', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(82,'Yamalo-Nenets Autonomous Okrug', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(82,'Autonomer Kreis der Jamal-Nenzen', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(83,'Yaroslavl Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(83,'Oblast Jaroslawl', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(88,'Arkhangelsk Oblast with Nenets Autonomous Okrug', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(88,'Oblast Archangelsk mit dem Autonomen Kreis der Nenzen', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(89,'Arkhangelsk Oblast with Nenets Autonomous Okrug', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(89,'Oblast Archangelsk mit dem Autonomen Kreis der Nenzen', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(93,'Republic of Crimea', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(93,'Republik Krim', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(124,'Baku', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(124,'Baku', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(125,'Yerevan', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(125,'Jerewan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(126,'Minsk Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(126,'Minskaja Woblasz', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(127,'Mogilev Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(127,'Mahiljouskaja Woblasz', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(128,'Grodno Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(128,'Hrodsenskaja Woblasz', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(129,'Astana', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(129,'Astana', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(130,'Almaty', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(130,'Almaty', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(131,'Atyrau Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(131,'Gebiet Atyrau', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(132,'Mangystau Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(132,'Gebiet Mangghystau', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(133,'Aktobe Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(133,'Gebiet Aqtöbe', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(134,'Pavlodar Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(134,'Gebiet Pawlodar', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(135,'Bishkek', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(135,'Bischkek', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(136,'Municipality of Kishinev', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(136,'Munizipium Kischinau', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(137,'Karaganda Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(137,'Qaraghandy', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(138,'Tashkent Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(138,'Provinz Taschkent', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(139,'Fergana Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(139,'Provinz Fargʻona', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(140,'Ashgabat', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(140,'Aşgabat', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(141,'Balkan Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(141,'Balkan welaýaty', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(142,'Tbilisi Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(142,'Tiflis', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(143,'Ulaanbaatar', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(143,'Ulaanbaatar', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(144,'Kiev Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(144,'Oblast Kiew', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(145,'Odessa Oblast', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(145,'Oblast Odessa', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(146,'Osh Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(146,'Gebiet Osch', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(147,'South Kazakhstan Region', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(147,'Südkasachstan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(148,'Baku', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(148,'Baku', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(149,'Yerevan', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(149,'Jerewan', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(150,'Kiev', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(150,'Kiew', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(151,'Minsk', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(151,'Minsk', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(152,'Belgrade', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(152,'Belgrad', 'de');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(153,'Hanoi', 'en');
            INSERT INTO tbl_trregion (trParentId, name, langId) VALUES(153,'Hanoi', 'de');
		";
    }

    public function downSql()
    {
        return TRUE;
    }


}