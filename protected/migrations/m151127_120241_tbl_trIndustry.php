<?php

class m151127_120241_tbl_trIndustry extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DELETE FROM {{trindustry}}
			WHERE trParentId IN (
				1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,
				21,22,23,24,25,26,27,28,29,30,31,32,35,36,37,38,
				39,40,41,43,45,46,47,49,50,51,53,54,55,56,59,60,
				61,62,63,64
			)
			AND langId = "de";
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("1", "de", "Möbel für Haus und Büro, Raumgestaltung, Alltagslebengegenstände");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("2", "de", "Bauwesen, Ausbaumaterialien und Zubehör- und Ersatzteile");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("3", "de", "Maschinenbau, Metallbearbeitung, Werkzeugmaschinen, Industrieausrüstung");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("4", "de", "Sicherheit, Brandsicherheit, Arbeitsschutz");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("5", "de", "Rohholz und Holzbearbeitung");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("6", "de", "Chemie. Polymerwerkstoffe");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("7", "de", "Stahlindustrie");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("8", "de", "Energiewirtschaft");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("9", "de", "Medizin, Gesundheit, Hygiene");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("10", "de", "Luft- und Raumfahrtindustrie");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("11", "de", "Landwirtschaft");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("12", "de", "Tierwelt, Tierwaren. Tiermedizin.");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("13", "de", "Lebensmittelindustrie: Ausrüstung und Inhaltsstoffe");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("14", "de", "Lebensmittel");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("16", "de", "Schönheitsindustrie, Kosmetik, Parfümerie");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("17", "de", "Hotels- und Restaurantgewerbe");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("18", "de", "Elektrotechnik");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("19", "de", "Information und Kommunikation");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("20", "de", "Juwelen- und Uhrbranche");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("21", "de", "Elektronik und Zubehör- und Ersatzteile");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("22", "de", "Autos, Fahrrad- und Motorradtechnik");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("23", "de", "Kultur und Kunst");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("24", "de", "Religion");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("25", "de", "Pharmazeutik");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("26", "de", "Verpackung, Etiketten");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("27", "de", "Tourismus und Erholung ");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("28", "de", "Hochtechnologien. Wissenschaft. Innovationen");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("29", "de", "Transportwesen, Frachtlieferung, Lager, Logostik");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("30", "de", "Dienstleistungen für die Staat ");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("31", "de", "Hauswaren, Geschirr, Souvenirs");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("32", "de", "Blumenzucht. Floristik. Gartenbau. Landschaftsgestaltung");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("35", "de", "Freizeitaktivitäten, Jagd und Angeln");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("36", "de", "Ausbildung");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("37", "de", "Öl und Gas");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("38", "de", "Naturschätze. Bergbauindustrie");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("39", "de", "Haus des Kindes. Kinderwaren");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("40", "de", "Werbung, Design");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("41", "de", "Druckgewerbe");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("43", "de", "Umweltschutz");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("45", "de", "Kleidung, Schuhe, Leder, Pelzwaren");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("46", "de", "Cleaning Dienstleistungen");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("47", "de", "Feier-Industrie (Hochzeit. Neujahr. Weihnachten)");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("49", "de", "Antiquitäten");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("50", "de", "Messeindustrie");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("51", "de", "Lasthebemaschinen");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("53", "de", "Geistiges Eigentum, gewerbliches Urheberrecht");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("54", "de", "Motorboote, Jachten, Schiffbau");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("55", "de", "Bücher, Verlagswesen");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("56", "de", "Kleine und mittere Unternehmen");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("59", "de", "Immobilien – Vermietung und Verkauf");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("60", "de", "Optikindustrie. Laser-Geräte");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("61", "de", "Textilwaren, Geweben");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("62", "de", "Feier-Industrie (Hochzeit. Neujahr. Weihnachten)");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("63", "de", "Bände");
			INSERT INTO {{trindustry}} (`trParentId`, `langId`, `name`) VALUES ("64", "de", "Tierwelt, Tierwaren. Tiermedizin");
		';
	}
}