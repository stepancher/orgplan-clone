<?php

class m160310_134144_insert_route_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			DELETE FROM {{route}} where
			`url` = '/ru/fairs' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/venues' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/industries' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/regions' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/prices' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/stands' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/login' AND
			`route` = 'site/auth';

			DELETE FROM {{route}} where
			`url` = '/ru/registration' AND
			`route` = 'site/reg';
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{route}} where
			`url` = '/ru/fairs' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/venues' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/industries' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/regions' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/prices' AND
			`route` = 'search/index';

			DELETE FROM {{route}} where
			`url` = '/ru/stands' AND
			`route` = 'search/index';

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES (
				'Каталог событий',
				'В каталоге представлены анонсы выставок и отчеты прошедших мероприятий в России, СНГ, мире. При помощи фильтра можно найти выставку по стране, городу, отрасли.',
				'/ru/fairs',
				'search/index',
				'{\"sector\":\"fair\"}',
				'Выставки Российской Федерации'
			);

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES (
				'Площадки России, СНГ, мира',
				'В каталоге представлены площадки России, СНГ, мира. Воспользовавшись фильтром, можно найти площадку по региону и городу',
				'/ru/venues',
				'search/index',
				'{\"sector\":\"exhibitionComplex\"}',
				'Каталог площадок'
			);

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES (
				'Каталог отраслей | Orgplan',
				'В каталоге представлены 9 отраслей промышленности России. В каждой отрасли содержится краткий обзор с основными показателями развития.',
				'/ru/industries',
				'search/index',
				'{\"sector\":\"industry\"}',
				'Каталог отраслей'
			);

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES (
				'Регионы России',
				'В каталоге представлены регионы России, в каждом опубликованы экономические показатели развития, KPI отраслей и рейтинг инвестиционной привлекательности',
				'/ru/regions',
				'search/index',
				'{\"sector\":\"region\"}',
				'Каталог информации о регионах'
			);

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES (
				'Каталог цен на товары и услуги | Orgplan',
				'Каталог цен по городам России',
				'/ru/prices',
				'search/index',
				'{\"sector\":\"price\"}',
				'Каталог Цен'
			);

			INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `header`) VALUES (
				'Проекты выставочных стендов',
				'В каталоге представлены готовые макеты выставочных стендов. Воспользовавшись фильтром, можно найти макет стенда по площади и типу планировки.',
				'/ru/stands',
				'search/index',
				'{\"sector\":\"stand\"}',
				'Каталог стендов'
			);

			INSERT INTO {{route}} (`title`, `url`, `route`, `header`, `params`) VALUES (
				'Orgplan | Авторизация',
				'/ru/login',
				'site/auth',
				'Авторизация',
				'ru',
				'[]'
			);

			INSERT INTO {{route}} (`title`, `url`, `route`, `header`, `params`) VALUES (
				'Orgplan | Регистрация',
				'/ru/registration',
				'site/reg',
				'Регистрация',
				'ru',
				'[]'
			);
		";
	}
}