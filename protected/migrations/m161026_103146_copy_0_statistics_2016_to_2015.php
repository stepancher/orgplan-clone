<?php

class m161026_103146_copy_0_statistics_2016_to_2015 extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `copy_0_statistics_2016_to_2015`()
            BEGIN
                
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE target_fair_id INT DEFAULT 0;
                DECLARE sourceStatistics TEXT DEFAULT 'ERROR';                
                DECLARE copy CURSOR FOR SELECT f15.id, f16.statistics
                        FROM {{fair}} f16
                        LEFT JOIN {{fairhasassociation}} fha ON fha.fairId = f16.id
                        LEFT JOIN {{fair}} f15 ON f15.storyId = f16.storyId AND YEAR(f15.beginDate) = 2015 AND MONTH(f16.beginDate) = MONTH(f15.beginDate)
                        LEFT JOIN {{trfair}} trf16 ON trf16.trParentId = f16.id AND trf16.langId = 'ru'
                        LEFT JOIN {{trfair}} trf15 ON trf15.trParentId = f15.id AND trf15.langId = 'ru'
                        WHERE YEAR(f16.beginDate) = 2016 AND
                        f16.active = 1 AND
                        YEAR(f16.statisticsDate) = 2015 AND
                        fha.id IS NULL AND
                        f15.id IS NOT NULL AND
                        trf16.name = trf15.name AND
                        f15.statistics = '0';
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO target_fair_id, sourceStatistics;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{fair}} SET `statistics` = sourceStatistics, `statisticsDate` = '2015-01-01' WHERE `id` = target_fair_id;
                    COMMIT;
                    
                END LOOP;
            CLOSE copy;
        END;
        
        CALL copy_0_statistics_2016_to_2015();
        DROP PROCEDURE IF EXISTS `copy_0_statistics_2016_to_2015`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}