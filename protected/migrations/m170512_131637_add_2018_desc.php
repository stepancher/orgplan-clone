<?php

class m170512_131637_add_2018_desc extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO  {{catalogdescription}} (`year`) VALUES ('2018');
            INSERT INTO  {{trcatalogdescription}} (`header`, `text`) VALUES ('Выставки 2018 года', '<p>Шесть тысяч крупных выставок и десятки тысяч конференций традиционно пройдут в мире в 2018 году.\n</p>\n<p><b>Что представляют собой </b><b>выставки 2018</b><b> года?</b>\n</p>\n<p>Выставочные мероприятия 2018 года предлагают тематики на любые потребности, вкус и интерес: техника, творчество, искусство, реклама, промышленность, торговля, образование и многое другое. Они представляют продукцию определенной отрасли (отраслевые) или охватывают товары и услуги нескольких отраслей (межотраслевые).\n</p>\n<p>Масштаб и территория охвата:\n</p>\n<ul>\n	<li>проекты представляют регионы одной страны;</li>\n	<li>в выставочном мероприятии принимают участие предприятия и организации многих стран мира;</li>\n	<li>к участию привлекаются исключительно зарубежные производители.</li>\n</ul>\n<p>Проекты предназначаются для частных лиц (посещают все желающие) или для специалистов отрасли и бизнесменов.\n</p>\n<p><b>Цели и задачи </b><b>выставок 2018</b>\n</p>\n<p>Выставочные мероприятия – эффективное средство установления и поддержания связей с партнерами и клиентами. Это уникальное место, где собираются специалисты, продавцы и покупатели, развивается бизнес, реализуются потребности. Это универсальная и чрезвычайно гибкая среда, позволяющая решать множество маркетинговых задач, увеличивать объемы продаж, получать исчерпывающие сведения о продуктах и услугах, находить ответы на все интересующие вопросы, удовлетворять интересы.\n</p>\n<p>Участники и посетители выставок в короткий промежуток времени охватывают большой сегмент рынка и получают результаты, которых в обычной работе можно достичь через месяцы и даже годы.\n</p>');
            UPDATE  {{trcatalogdescription}} SET `langId`='ru' WHERE `id`='3';
            UPDATE  {{trcatalogdescription}} SET `trParentId`='3' WHERE `id`='3';
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}