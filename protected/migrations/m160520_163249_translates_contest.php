<?php

class m160520_163249_translates_contest extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '88', 'en', 'Manufacturer', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '94', 'en', 'Company', NULL, 'Company\'s name', NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '95', 'en', 'Country', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '96', 'en', 'City', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '97', 'en', 'Nominated item / product:', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '98', 'en', 'Item / Product', NULL, 'item / product', 'Specify model, product line, modification of the item/product'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '99', 'en', 'Innovative info summary', NULL, 'maximum of 430 symbols, including blank', NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '100', 'en', 'Contact person', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '101', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '102', 'en', 'Position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '103', 'en', 'E-mail', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '104', 'en', 'Telephone', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '105', 'en', 'Компания производитель', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '106', 'en', 'Компания', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '107', 'en', 'Страна', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '108', 'en', 'Город', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '154', 'en', 'Номинируемое изделие', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '155', 'en', 'Изделие/продукция', NULL, NULL, 'Обязательно укажите класс, модель, серию, модификацию изделия.'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '156', 'en', 'Краткое описание инновации', NULL, 'Maximum of 430 symbols, including blank', NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '157', 'en', 'Submitted innovation', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '158', 'en', 'Will be showcased on AgroSalon 2016', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '159', 'en', 'No', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '160', 'en', 'Yes', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '161', 'en', 'Was showcased on other exhibitions', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '162', 'en', 'No', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '163', 'en', 'Yes', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '164', 'en', NULL, NULL, 'on exhibition', NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '165', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '166', 'en', 'Was showcased on other contests', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '167', 'en', 'No', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '168', 'en', 'Yes', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '169', 'en', NULL, NULL, 'on contest', NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '170', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '171', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '225', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '226', 'en', 'This contest application must be filled in two languages (English and Russian). The contest application must be enclosed with: <br/>1. Nominated product photos (not less than four photos). <br/>2. Innovative description in Russian and English (about 5000 symbols). <br/> <br/>In addition: please, submit the engineering test record if it is involved. <br/> <br/>Thank you for your understanding. Any incomplete application forms or forms without attachments will not be processed. <br/> <br/>All products nominated for contest must be exhibited at AGROSALON 2016.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '244', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '826', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '827', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '828', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '829', 'en', 'Exhibitor', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '830', 'en', 'position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '831', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '832', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '833', 'en', 'Organizer', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '834', 'en', 'position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '835', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '836', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '837', 'en', 'Signature', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '838', 'en', 'Signature', NULL, NULL, NULL
						);
			DELETE FROM {{trattributeclass}} WHERE `id`='1111';
			UPDATE {{trattributeclass}} SET `langId`='en' WHERE `id`='82';
			INSERT INTO {{trattributeclass}} (`id`, `trParentId`, `langId`, `label`) VALUES (NULL, '106', 'ru', 'Компания');
			UPDATE {{trattributeclass}} SET `label`='Country' WHERE `id`='1112';
			UPDATE {{trattributeclass}} SET `label`='Страна' WHERE `id`='83';
			UPDATE {{trattributeclass}} SET `label`='Город' WHERE `id`='84';
			UPDATE {{trattributeclass}} SET `label`='City' WHERE `id`='1113';
			
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '75', 'en', 'Electrotechnical works and electric power', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '140', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '210', 'en', 'The tariff includes the cost of power consumed within the norm. Power consumption norm is 50 W per 1 sq.m of the stand space.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '237', 'en', 'If necessary connect the equipment with a capacity exceeding 60 kW details please order a few multiples of the above connection capacity.<br/>To connect Consumer\'s equipment to power supply mains, the cable of a relevant cross-sectional load shall be applied. For independent build stands the power cable is provided by the exhibitor\'s Customer Builder; in this case the cable length should be no less than 30 meters.<br/><br/>Equipment will only be connected to power supply mains after measuring of insulation resistance of electric network installed.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '246', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '248', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '250', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '252', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '77', 'en', 'Connection at the floor level', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '78', 'en', 'Connection at the 2nd floor level', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '79', 'en', 'Lease and laying of hoses (over 10 meters)', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '138', 'en', 'Sanitary engineering connections', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '141', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '148', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '149', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '150', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '151', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '152', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '153', 'en', NULL, NULL, NULL, '<div class=\"hint-header\">Cost of services</div>Cost of services increases by 50% if ordered after Aug.23, 2016, by 100% if ordered after Sep.21, 2016'
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '211', 'en', 'Connection of one unit of the exhibitor\'s process equipment to the water system and the drain system (includes water inlet and sewer connection) without connection of consumer.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '212', 'en', '<div class=\"margin-top-20\">Connection of consumers located no further than 10 meters from the connection point is performed using hoses provided with no additional payment. If the connection length exceeds 10 meters, hoses with the required length are leased.</div>', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '213', 'en', 'Connection of water and air consumers to the supplied hoses is performed independently by the exhibitor\'s Customer Builder using its own component materials (adapters, tapes, etc.).', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '217', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br/><br/>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br/><br/>Any claims related to the performance of this Application are accepted by the Organizer until October 07, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application are considered to be performed with the proper quality and accepted by the Exhibitor.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '218', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '241', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '242', 'en', 'Payments shall be effected in USD. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '243', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '260', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '813', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '814', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '815', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '816', 'en', 'Exhibitor', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '817', 'en', 'position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '818', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '819', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '820', 'en', 'Organizer', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '821', 'en', 'position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '822', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '823', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '824', 'en', 'Signature', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '825', 'en', 'Signature', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '522', 'en', 'This form should be filled in until August 24, 2016. Please return the application form by fax +7(495) 781 37 08, or e-mail: agrosalon@agrosalon.ru', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '523', 'en', 'Telephone line', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '524', 'en', 'Communication services are provided by the LLC \"Flexline-N\" license No.122141,122142, 122146, 122147 given by the Ministry of Communications of the Russian Federation', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '526', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '527', 'en', 'Lease of phone and facsimile equipment:', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '529', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '531', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '548', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '532', 'en', 'Internet connection', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '533', 'en', 'Connection to the telematic service network', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '535', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '537', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '539', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '541', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '543', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '545', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '546', 'en', 'Additional IP-address', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '549', 'en', 'Communication equipment (lease)', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '551', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '553', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '555', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '557', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '558', 'en', 'Permission for the use of radio-electronic and radio-frequency devices (including radio stations)', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '560', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '561', 'en', 'Computers and office equipment (lease)', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '563', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '565', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '567', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '571', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '572', 'en', 'Payments shall be effected in USD according to exchange rate of Central Bank of RF (www.cbr.ru) on the invoice date. All prices include 18% VAT. Services for the bank transfer shall be paid by the Exhibitor.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '573', 'en', 'This application is obligatory part of the Contract. The application should be filled in two copies.<br><br>By signing the present application we confirm our consent to the Exhibition Rules and guarantee the payment for the service ordered.<br><br>Any claims related to the performance of this Application are accepted by the Organizer until October 04, 2016 2 p.m. After deadline the claim is not accepted. All works and the service specified in this Application, are considered to be performed with the proper quality and accepted by the Exhibitor.', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '765', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '878', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '879', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '880', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '881', 'en', 'Exhibitor', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '882', 'en', 'position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '883', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '884', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '885', 'en', 'Organizer', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '886', 'en', 'position', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '887', 'en', NULL, NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '888', 'en', 'Name', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '889', 'en', 'Signature', NULL, NULL, NULL
						);
			INSERT INTO {{trattributeclass}} (`trParentId`, `langId`, `label`, `unitTitle`, `placeholder`, `tooltip`) VALUES (
						  '890', 'en', 'Signature', NULL, NULL, NULL
						);
						
						INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('140', 'en', 'up to 5 kW');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('246', 'en', 'up to 10 kW');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('248', 'en', 'up to 20 kW');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('250', 'en', 'up to 40 kW');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('252', 'en', 'up to 60 kW');
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1299' WHERE `id`='2048';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1301' WHERE `id`='2049';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1303' WHERE `id`='2050';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1305' WHERE `id`='2051';
			UPDATE {{trattributeclassproperty}} SET `trParentId`='1307' WHERE `id`='2052';
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1295', 'en', 'Другое');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1297', 'en', 'Другое');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1309', 'en', 'холодное водоснабжение и канализация');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1311', 'en', 'горячее водоснабжение');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1313', 'en', 'холодное водоснабжение и канализация');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1315', 'en', 'горячее водоснабжение');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1317', 'en', 'Шланг 1/2\" (для воды) по 5 п . м . ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1319', 'en', 'Канализация 38 х 42 мм по 5 п . м . ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1321', 'en', 'Прокладка воды и канализации по 5 п . м . ');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1323', 'en', 'Подключение телефонной линии');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1325', 'en', 'Телефонный аппарат');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1327', 'en', 'Факсимальный аппарат');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1329', 'en', 'Ethernet до 1 Мбит // с');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1331', 'en', 'Ethernet до 2 Мбит / с');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1333', 'en', 'Ethernet до 5 Мбит / с');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1335', 'en', 'Ethernet до 10 Мбит / с');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1337', 'en', 'Ethernet до 100 Мбит / с');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1339', 'en', 'Wi - Fi до 512 Кбит / с');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1341', 'en', 'IP - адрес');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1343', 'en', 'Маршрутизатор Ethernet');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1345', 'en', 'Коммутатор Ethernet');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1347', 'en', 'USB - адаптер Wi - Fi');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1349', 'en', 'Точка доступа Wi - Fi');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1351', 'en', 'Комплект участника');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1353', 'en', 'Офисный персональный компьютер');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1355', 'en', 'Лазерный принтер(формат А4)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1357', 'en', 'Копировально - множительный аппарат(формат А4)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1359', 'en', 'Колесный транспорт');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1361', 'en', 'Гусеничный транспорт(только резиновые гусеницы)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1363', 'en', 'Грузы на паллетах');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1365', 'en', 'Грузы не на паллетах');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1367', 'en', 'Материалы для стендов');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1369', 'en', 'Вилочный погрузчик до 3 т');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1371', 'en', 'Вилочный погрузчик до 6 т');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1373', 'en', 'Вилочный погрузчик до 10 т');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1375', 'en', 'Кран до 25 т(min 2 часа)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1377', 'en', 'Кран до 40 т(min 2 часа)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1385', 'en', 'С использованием трапа');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1387', 'en', 'С использованием трапа');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1389', 'en', 'Грузы на паллетах');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1391', 'en', 'Грузы не на паллетах');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1393', 'en', 'Материалы для стендов');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1395', 'en', 'Вилочный погрузчик до 3 т');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1397', 'en', 'Вилочный погрузчик до 6 т');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1399', 'en', 'Вилочный погрузчик до 10 т');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1401', 'en', 'Кран до 25 т(min 2 часа)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1403', 'en', 'Кран до 40 т(min 2 часа)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1405', 'en', 'Доставка груза(кг)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1407', 'en', 'Доставка груза(м³)');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1409', 'en', 'Тара многоразовая');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1415', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1417', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1419', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1421', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1423', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1425', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1427', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1429', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1431', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1433', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1435', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1437', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1439', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1441', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1443', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1445', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1447', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1449', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1451', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1453', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1598', 'en', 'Логотип');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1615', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1617', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1619', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1621', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1623', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1625', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1627', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1629', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1631', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1633', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1635', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1637', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1639', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1641', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1643', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1645', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1647', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1649', 'en', 'Погрузчик');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1651', 'en', 'Кран');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('1653', 'en', 'Погрузчик');
			UPDATE {{trattributeclassproperty}} SET `value`='cold water supply and the water drain' WHERE `id`='2055';
			UPDATE {{trattributeclassproperty}} SET `value`='hot water supply' WHERE `id`='2056';
			UPDATE {{trattributeclassproperty}} SET `value`='cold water supply and the water drain' WHERE `id`='2057';
			UPDATE {{trattributeclassproperty}} SET `value`='hot water supply' WHERE `id`='2058';
			UPDATE {{trattributeclassproperty}} SET `value`='water hose 1/2\'\' (5 long meters)' WHERE `id`='2059';
			UPDATE {{trattributeclassproperty}} SET `value`='water drain hose 38x42 mm (5 long meters) ' WHERE `id`='2060';
			UPDATE {{trattributeclassproperty}} SET `value`='Hose laying (water supply and water drain) (5 long meters)' WHERE `id`='2061';
			UPDATE {{trattributeclassproperty}} SET `value`='Phone line (tone extension dialing)' WHERE `id`='2062';
			UPDATE {{trattributeclassproperty}} SET `value`='Telephone set' WHERE `id`='2063';
			UPDATE {{trattributeclassproperty}} SET `value`='Fax machine' WHERE `id`='2064';
			UPDATE {{trattributeclassproperty}} SET `value`='IP-address' WHERE `id`='2071';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 1 Mbit/s)' WHERE `id`='2065';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 2 Mbit/s)' WHERE `id`='2066';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 5 Mbit/s)' WHERE `id`='2067';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 10 Mbit/s)' WHERE `id`='2068';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet (data transfer speed up to 100 Mbit/s)' WHERE `id`='2069';
			UPDATE {{trattributeclassproperty}} SET `value`='WI-FI (data transfer speed up to 512 kbit/s)' WHERE `id`='2070';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet router' WHERE `id`='2072';
			UPDATE {{trattributeclassproperty}} SET `value`='Ethernet switch' WHERE `id`='2073';
			UPDATE {{trattributeclassproperty}} SET `value`='Wi-Fi USB-adapter' WHERE `id`='2074';
			UPDATE {{trattributeclassproperty}} SET `value`='Wi-Fi access point' WHERE `id`='2075';
			UPDATE {{trattributeclassproperty}} SET `value`='Participant set' WHERE `id`='2076';
			UPDATE {{trattributeclassproperty}} SET `value`='Personal computer' WHERE `id`='2077';
			UPDATE {{trattributeclassproperty}} SET `value`='Laser printer (format A4)' WHERE `id`='2078';
			UPDATE {{trattributeclassproperty}} SET `value`='Copy machine (format A4)' WHERE `id`='2079';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>' WHERE `id`='478';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>' WHERE `id`='479';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nРАСЧЕТ ВРЕМЕНИ\n</div>\n<div class=\"body\">\nПри расчетах каждый начавшийся час считается, как целый час.\n</div>' WHERE `id`='480';
		";
	}
}