<?php

class m160414_150818_add_datepickers_on_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable()
	{
		return "
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='671';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='672';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='680';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='681';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='689';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='690';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='698';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='699';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='707';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='708';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='716';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='717';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='725';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='726';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='734';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='735';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='743';
			UPDATE {{attributeclass}} SET `class`='datePicker', `placeholder`='____/__/__' WHERE `id`='744';
			UPDATE {{attributeclass}} SET `class`='datePicker' WHERE `id`='752';
			UPDATE {{attributeclass}} SET `class`='datePicker' WHERE `id`='753';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='664';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='673';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='682';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='691';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='700';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='709';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='718';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='727';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='736';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите наименование машины или оборудования' WHERE `id`='745';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='670';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='679';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='688';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='697';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='706';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='715';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='724';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='733';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='742';
			UPDATE {{attributeclass}} SET `placeholder`='Укажите количество часов' WHERE `id`='751';

		";
	}
}



/*
 * 

 */