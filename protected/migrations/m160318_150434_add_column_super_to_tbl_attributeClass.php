<?php

class m160318_150434_add_column_super_to_tbl_attributeClass extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
			ALTER TABLE {{attributeclass}} DROP COLUMN `super`;
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			ALTER TABLE {{attributeclass}}
			ADD COLUMN `super` INT NULL DEFAULT NULL AFTER `parent`;

			UPDATE {{attributeclass}} SET `super`='13' WHERE `id`='12';
			UPDATE {{attributeclass}} SET `super`='13' WHERE `id`='11';
			UPDATE {{attributeclass}} SET `super`='13' WHERE `id`='10';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='15';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='16';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='17';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='18';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='19';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='20';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='21';
			UPDATE {{attributeclass}} SET `super`='22' WHERE `id`='23';
			UPDATE {{attributeclass}} SET `super`='22' WHERE `id`='24';
			UPDATE {{attributeclass}} SET `super`='22' WHERE `id`='25';
			UPDATE {{attributeclass}} SET `super`='22' WHERE `id`='26';
			UPDATE {{attributeclass}} SET `super`='22' WHERE `id`='27';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='28';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='29';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='30';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='31';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='32';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='33';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='34';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='35';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='36';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='37';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='38';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='39';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='40';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='41';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='43';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='44';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='45';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='46';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='47';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='48';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='49';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='50';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='51';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='52';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='53';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='54';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='55';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='56';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='57';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='58';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='59';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='60';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='61';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='62';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='63';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='64';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='65';
			UPDATE {{attributeclass}} SET `super`='66' WHERE `id`='67';
			UPDATE {{attributeclass}} SET `super`='66' WHERE `id`='68';
			UPDATE {{attributeclass}} SET `super`='66' WHERE `id`='69';
			UPDATE {{attributeclass}} SET `super`='66' WHERE `id`='70';
			UPDATE {{attributeclass}} SET `super`='13' WHERE `id`='133';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='172';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='173';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='174';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='178';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='175';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='176';
			UPDATE {{attributeclass}} SET `super`='66' WHERE `id`='179';
			UPDATE {{attributeclass}} SET `super`='71' WHERE `id`='72';
			UPDATE {{attributeclass}} SET `super`='71' WHERE `id`='74';
			UPDATE {{attributeclass}} SET `super`='71' WHERE `id`='73';
			UPDATE {{attributeclass}} SET `super`='75' WHERE `id`='80';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='81';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='82';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='83';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='84';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='85';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='86';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='87';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='213';
			UPDATE {{attributeclass}} SET `super`='75' WHERE `id`='139';
			UPDATE {{attributeclass}} SET `super`='75' WHERE `id`='140';
			UPDATE {{attributeclass}} SET `super`='75' WHERE `id`='210';
			UPDATE {{attributeclass}} SET `super`='75' WHERE `id`='237';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='141';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='148';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='149';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='150';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='151';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='152';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='153';
			UPDATE {{attributeclass}} SET `super`='88' WHERE `id`='94';
			UPDATE {{attributeclass}} SET `super`='88' WHERE `id`='95';
			UPDATE {{attributeclass}} SET `super`='88' WHERE `id`='96';
			UPDATE {{attributeclass}} SET `super`='97' WHERE `id`='98';
			UPDATE {{attributeclass}} SET `super`='97' WHERE `id`='99';
			UPDATE {{attributeclass}} SET `super`='100' WHERE `id`='101';
			UPDATE {{attributeclass}} SET `super`='100' WHERE `id`='102';
			UPDATE {{attributeclass}} SET `super`='100' WHERE `id`='103';
			UPDATE {{attributeclass}} SET `super`='100' WHERE `id`='104';
			UPDATE {{attributeclass}} SET `super`='105' WHERE `id`='106';
			UPDATE {{attributeclass}} SET `super`='105' WHERE `id`='107';
			UPDATE {{attributeclass}} SET `super`='105' WHERE `id`='108';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='110';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='111';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='187';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='188';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='189';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='190';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='191';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='192';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='193';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='194';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='195';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='196';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='197';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='198';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='200';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='201';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='202';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='203';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='204';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='205';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='206';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='229';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='230';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='231';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='232';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='233';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='112';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='128';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='184';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='185';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='116';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='117';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='118';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='136';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='182';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='126';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='130';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='125';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='235';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='114';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='115';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='134';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='135';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='119';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='120';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='129';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='236';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='77';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='78';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='79';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='211';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='121';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='122';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='123';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='124';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='212';
			UPDATE {{attributeclass}} SET `super`='154' WHERE `id`='155';
			UPDATE {{attributeclass}} SET `super`='154' WHERE `id`='156';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='158';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='161';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='166';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='171';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='165';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='164';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='162';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='163';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='170';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='169';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='167';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='168';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='159';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='160';
			UPDATE {{attributeclass}} SET `super`='207' WHERE `id`='209';
			UPDATE {{attributeclass}} SET `super`='218' WHERE `id`='217';
			UPDATE {{attributeclass}} SET `super`='224' WHERE `id`='223';
			UPDATE {{attributeclass}} SET `super`='225', `priority`=NULL WHERE `id`='226';
			UPDATE {{attributeclass}} SET `super`='234' WHERE `id`='177';
			UPDATE {{attributeclass}} SET `super`='13' WHERE `id`='13';
			UPDATE {{attributeclass}} SET `super`='14' WHERE `id`='14';
			UPDATE {{attributeclass}} SET `super`='22' WHERE `id`='22';
			UPDATE {{attributeclass}} SET `super`='42' WHERE `id`='42';
			UPDATE {{attributeclass}} SET `super`='66' WHERE `id`='66';
			UPDATE {{attributeclass}} SET `super`='71' WHERE `id`='71';
			UPDATE {{attributeclass}} SET `super`='75' WHERE `id`='75';
			UPDATE {{attributeclass}} SET `super`='88' WHERE `id`='88';
			UPDATE {{attributeclass}} SET `super`='97' WHERE `id`='97';
			UPDATE {{attributeclass}} SET `super`='100' WHERE `id`='100';
			UPDATE {{attributeclass}} SET `super`='105' WHERE `id`='105';
			UPDATE {{attributeclass}} SET `super`='109' WHERE `id`='109';
			UPDATE {{attributeclass}} SET `super`='113' WHERE `id`='113';
			UPDATE {{attributeclass}} SET `super`='131' WHERE `id`='131';
			UPDATE {{attributeclass}} SET `super`='138' WHERE `id`='138';
			UPDATE {{attributeclass}} SET `super`='154' WHERE `id`='154';
			UPDATE {{attributeclass}} SET `super`='157' WHERE `id`='157';
			UPDATE {{attributeclass}} SET `super`='207' WHERE `id`='207';
			UPDATE {{attributeclass}} SET `super`='218' WHERE `id`='218';
			UPDATE {{attributeclass}} SET `super`='224' WHERE `id`='224';
			UPDATE {{attributeclass}} SET `super`='225' WHERE `id`='225';
			UPDATE {{attributeclass}} SET `super`='234' WHERE `id`='234';
		";
	}
}