<?php

class m170220_151328_copy_exdb_organizerhasassociation_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_organizerhasassociation_to_db`;
            
            CREATE PROCEDURE {$expodata}.`copy_exdb_organizerhasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE org_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT org.id AS orgId, orgass.id AS assId FROM {$expodata}.{{exdborganizercompanyhasassociation}} eoha
                                        LEFT JOIN {$expodata}.{{exdborganizercompany}} eo ON eo.id = eoha.organizerCompanyId
                                        LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.id = eoha.associationId
                                        LEFT JOIN {$db}.{{organizercompany}} org ON org.exdbId = eo.exdbId
                                        LEFT JOIN {$db}.{{association}} orgass ON orgass.exdbId = ass.exdbId
                                        LEFT JOIN {$db}.{{organizercompanyhasassociation}} orgoha ON orgoha.organizerCompanyId = org.id AND orgoha.associationId = orgass.id
                                        WHERE orgoha.id IS NULL ORDER BY orgId;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                    
                    FETCH copy INTO org_id, ass_id;
                    
                    IF done THEN
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        INSERT INTO {$db}.{{organizercompanyhasassociation}} (`organizerCompanyId`,`associationId`) VALUES (org_id, ass_id);
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_organizerhasassociation_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}