<?php

class m161108_121410_tbl_region_add_active_column extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            ALTER TABLE {{region}}
              ADD COLUMN `active` INT(2) NULL AFTER `code`;

            UPDATE {{region}} SET active = 1;
            
            UPDATE {{region}} SET `active`=NULL WHERE `id`='93';
            UPDATE {{region}} SET `active`=NULL WHERE `id`='88';
            UPDATE {{region}} SET `active`=NULL WHERE `id`='3';

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}