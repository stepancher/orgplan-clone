<?php

class m160316_134951_add_de_route_to_tbl_route extends CDbMigration
{

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		$sql = "
			DELETE FROM {{route}} where
			`url` =  '/de' AND
			`route` = 'site/index' AND
			`params` = '[]';
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			INSERT INTO {{route}} (`title`, `description`, `keywords`, `url`, `route`, `params`, `header`, `lang`) VALUES
			('Orgplan', 'orgplan', NULL, '/de', 'site/index', '[]', 'Orgplan', 'de');
		";
	}





}