<?php

class m160728_111617_add_sng_tr_exhibitioncomplexes extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1084', 'ru', 'КОРМЕ', 'пр. Г.Алиева, 515', 'ЦЕНТР ВЫСТАВОК И КОНФЕРЕНЦИЙ \"БАКУ ЭКСПО ЦЕНТР\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1100', 'ru', 'ЕРЕВАН EXPO', 'ул. А. Акопяна 3', 'ВЫСТАВОЧНЫЙ КОМПЛЕКС \"ЕРЕВАН EXPO\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1092', 'ru', 'БЕЛЭКСПО', 'ул.Я.Купалы, 27', 'НАЦИОНАЛЬНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР \"БЕЛЭКСПО\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1101', 'ru', 'ФУТБОЛЬНЫЙ МАНЕЖ', 'пр-т Победителей, 20/2', 'ФУТБОЛЬНЫЙ МАНЕЖ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1133', 'ru', 'АГ. ЩОМЫСЛИЦА, 28', 'АГ. ЩОМЫСЛИЦА, 28');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1102', 'ru', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ', 'пр.Дзержинского, 83', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1096', 'ru', 'ДВОРЕЦ ИСКУССТВ', 'ул. Ульяновская, 35/31', 'ГОСУДАРСТВЕННОЕ УЧРЕЖДЕНИЕ КУЛЬТУРЫ \"ДВОРЕЦ ИСКУССТВ\" Г.БОБРУЙСКА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1103', 'ru', 'ОЛИМПИЕЦ', 'ул.30 лет Победы, 1А', 'СК \"ОЛИМПИЕЦ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1104', 'ru', 'ЮНОСТЬ', 'ул. Ленинская, 39', 'КУЛЬТУРНЫЙ ЦЕНТР \"ЮНОСТЬ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1085', 'ru', 'ДВОРЕЦ СПОРТА', 'пр. Победителей, 4', 'КС РУП «ДВОРЕЦ СПОРТА»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1138', 'ru', 'ДВОРЕЦ ЖЕЛЕЗНОДОРОЖНИКОВ', 'ул. Чкалова 7');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1139', 'ru', 'ВЫСТАВОЧНЫЙ ПАВИЛЬОН', 'бульвар Тракторостроителей');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1140', 'ru', 'ТОРГОВЫЙ ПАВИЛЬОН', 'пл. Советская');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1086', 'ru', 'КОРМЕ', 'ул.Достык, 3', 'ВЫСТАВОЧНЫЙ ЦЕНТР \"КОРМЕ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1141', 'ru', 'ДВОРЕЦ ДЕТЕЙ И МОЛОДЕЖИ', 'Старовиленский тракт, 41');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1142', 'ru', 'ДВОРЕЦ ИСКУССТВА', 'ул. Козлова, 3');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1105', 'ru', 'ГОСТИНИЦА ВИКТОРИЯ', 'пр.Победителей, 59', 'ГОСТИНИЦА ВИКТОРИЯ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1106', 'ru', 'АТАКЕНТ', 'Тимирязева, 42', 'ВЦ \"АТАКЕНТ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1107', 'ru', 'АТЫРАУ', 'УЛ СМАГУЛОВА, 5А', 'СПОРТКОМПЛЕКС \"АТЫРАУ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1108', 'ru', 'RENAISSANCE ATYRAU HOTEL', 'улица Сатпаева, 15 Б', 'ГОСТИНИЦА \"RENAISSANCE ATYRAU HOTEL\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1109', 'ru', 'ДВОРЕЦ НЕЗАВИСИМОСТИ', 'просп. Тауелсыздык, 52', 'ДВОРЕЦ НЕЗАВИСИМОСТИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1110', 'ru', 'КОРМЕ ОРТАЛЫГЫ', 'проспект Астана 12', 'ВЦ «Корме Орталыгы»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1097', 'ru', 'GRAND NUR PLAZA', '29 А мкр.', 'Grand Nur Plaza Hotel & Convention Centre');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1111', 'ru', 'КОНЫС', 'пр. Абулхаир-хана, 52', 'Дворец спорта \"Коныс\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1112', 'ru', 'ЭНЕРГЕТИК', 'пл. Конституции, 1', 'Теннисный центр \"Энергетик\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1113', 'ru', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ', 'Самал 1, д.44', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1114', 'ru', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА', 'пр. Абая, 44, уг. ул. Байтурсынова', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1115', 'ru', 'АВИАЦИОННАЯ БАЗА ВС РК', 'район Международного аэропорта “Астана”', 'АВИАЦИОННАЯ БАЗА ВС РК');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1134', 'ru', 'ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА', 'ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1087', 'ru', 'MOLDEXPO', 'ул. Гиочеилор, 1', 'МВЦ \"MOLDEXPO\" АО');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1132', 'ru', 'ALMATY TOWERS', 'ул. Байзакова, 280', 'ALMATY TOWERS');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1091', 'ru', 'ШАХТЕР', 'ул. Казахстанская 1', 'СТАДИОН \"ШАХТЕР\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1116', 'ru', 'PALATUL REPABLICII', 'М.Чиботарь, 16', 'PALATUL REPABLICII');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1117', 'ru', 'МАНЕЖ КГАФКИС', 'ул.Ахунбаева, 97', 'МАНЕЖ КГАФКИС');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1118', 'ru', 'ДВОРЕЦ СПОРТА', 'ул. Т. Молдо, 40', 'ДВОРЕЦ СПОРТА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1088', 'ru', 'УЗЭКСПОЦЕНТР', 'Амира Темура улица, 107', 'УЗЭКСПОЦЕНТР НВК');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1135', 'ru', 'АВТОТЕХХИЗМАТ-Ф', 'Салон \"Автотеххизмат-Ф\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1093', 'ru', 'УЗКУРГАЗМАСАВДО', 'ул. Беруни, 41', 'Республиканский центр выставочно-ярмарочной торговли «УзКургазмаСавдо»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1119', 'ru', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ', 'проспект Мустакиллик, 2', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1120', 'ru', 'ТПП ТУРКМЕНИСТАНА', 'проспект Чандыбиль шаёлы, 143', 'ТПП ТУРКМЕНИСТАНА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1136', 'ru', 'АВАЗА', 'НАЦИОНАЛЬНАЯ ТУРИСТИЧЕСКАЯ ЗОНА «АВАЗА»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1089', 'ru', 'EXPOGEORGIA', 'Церетели, 118', 'EXPOGEORGIA');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1137', 'ru', 'ТПП МОНГОЛИИ', 'ТПП МОНГОЛИИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1094', 'ru', 'МВЦ', 'Броварской пр. 15', 'МВЦ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1121', 'ru', 'КИЕВЭКСПОПЛАЗА', 'ул. Салютная, 2-Б', 'ВЫСТАВОЧНЫЙ ЦЕНТР КИЕВЭКСПОПЛАЗА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1122', 'ru', 'ЗАЛ ОДЕССКОГО МОРВОКЗАЛА', 'ул. Приморская, 6', 'Концертно-выставочный Зал Одесского Морвокзала');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1123', 'ru', 'OPEN-AIR', 'улица Дерибасовская', 'OPEN-AIR');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1124', 'ru', 'ОДЕССКАЯ КИНОСТУДИЯ', 'Французский бульвар 33', 'ОДЕССКАЯ КИНОСТУДИЯ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1090', 'ru', 'КИЕВЭКСПОПЛАЗА', 'ул.Салютная, 2-Б', 'КИЕВЭКСПОПЛАЗА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1125', 'ru', 'ЧЕРВОНА КАЛИНА', 'проспект Генерала Ватутина', 'Яхт-клуб \"Червона Калина\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1126', 'ru', 'ОДЕССКИЙ АКАДЕМИЧЕСКИЙ ТЕАТР', 'ул. Пантелеймоновская, 3', 'Одесский академический театр музыкальной комедии имени Михаила Водяного');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1127', 'ru', 'НСК ОЛИМПИЙСКИЙ', 'ул. Большая Васильковская, 55', 'НСК ОЛИМПИЙСКИЙ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1098', 'ru', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА', 'ул. Мазепы Ивана, 13', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1128', 'ru', 'ТПП УКРАИНЫ', 'ул. Большая Житомирская, 33', 'ТПП УКРАИНЫ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1095', 'ru', 'АККО', 'просп. Победы, 40-Б', 'ВЦ АККО ИНТЕРЕШНЛ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1099', 'ru', 'ОДЕССКИЙ МОРСКОЙ ПОРТ', 'ул. Приморская, 6', 'ОДЕССКИЙ МОРСКОЙ ПОРТ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1129', 'ru', 'ДРУЖБА', 'ул. Киевская, 115', 'ДКП СК \"ДРУЖБА\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1130', 'ru', 'ЯЛТА-ТУРИСТ', 'ул. Дражинского 50', 'ГК \"ЯЛТА-ТУРИСТ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1131', 'ru', 'МУССОН', 'Ул.Вакуленчука 29', 'ТРЦ МУССОН');
			
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1084', 'de', 'КОРМЕ', 'пр. Г.Алиева, 515', 'ЦЕНТР ВЫСТАВОК И КОНФЕРЕНЦИЙ \"БАКУ ЭКСПО ЦЕНТР\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1100', 'de', 'ЕРЕВАН EXPO', 'ул. А. Акопяна 3', 'ВЫСТАВОЧНЫЙ КОМПЛЕКС \"ЕРЕВАН EXPO\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1092', 'de', 'БЕЛЭКСПО', 'ул.Я.Купалы, 27', 'НАЦИОНАЛЬНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР \"БЕЛЭКСПО\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1101', 'de', 'ФУТБОЛЬНЫЙ МАНЕЖ', 'пр-т Победителей, 20/2', 'ФУТБОЛЬНЫЙ МАНЕЖ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1133', 'de', 'АГ. ЩОМЫСЛИЦА, 28', 'АГ. ЩОМЫСЛИЦА, 28');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1102', 'de', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ', 'пр.Дзержинского, 83', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1096', 'de', 'ДВОРЕЦ ИСКУССТВ', 'ул. Ульяновская, 35/31', 'ГОСУДАРСТВЕННОЕ УЧРЕЖДЕНИЕ КУЛЬТУРЫ \"ДВОРЕЦ ИСКУССТВ\" Г.БОБРУЙСКА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1103', 'de', 'ОЛИМПИЕЦ', 'ул.30 лет Победы, 1А', 'СК \"ОЛИМПИЕЦ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1104', 'de', 'ЮНОСТЬ', 'ул. Ленинская, 39', 'КУЛЬТУРНЫЙ ЦЕНТР \"ЮНОСТЬ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1085', 'de', 'ДВОРЕЦ СПОРТА', 'пр. Победителей, 4', 'КС РУП «ДВОРЕЦ СПОРТА»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1138', 'de', 'ДВОРЕЦ ЖЕЛЕЗНОДОРОЖНИКОВ', 'ул. Чкалова 7');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1139', 'de', 'ВЫСТАВОЧНЫЙ ПАВИЛЬОН', 'бульвар Тракторостроителей');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1140', 'de', 'ТОРГОВЫЙ ПАВИЛЬОН', 'пл. Советская');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1086', 'de', 'КОРМЕ', 'ул.Достык, 3', 'ВЫСТАВОЧНЫЙ ЦЕНТР \"КОРМЕ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1141', 'de', 'ДВОРЕЦ ДЕТЕЙ И МОЛОДЕЖИ', 'Старовиленский тракт, 41');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1142', 'de', 'ДВОРЕЦ ИСКУССТВА', 'ул. Козлова, 3');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1105', 'de', 'ГОСТИНИЦА ВИКТОРИЯ', 'пр.Победителей, 59', 'ГОСТИНИЦА ВИКТОРИЯ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1106', 'de', 'АТАКЕНТ', 'Тимирязева, 42', 'ВЦ \"АТАКЕНТ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1107', 'de', 'АТЫРАУ', 'УЛ СМАГУЛОВА, 5А', 'СПОРТКОМПЛЕКС \"АТЫРАУ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1108', 'de', 'RENAISSANCE ATYRAU HOTEL', 'улица Сатпаева, 15 Б', 'ГОСТИНИЦА \"RENAISSANCE ATYRAU HOTEL\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1109', 'de', 'ДВОРЕЦ НЕЗАВИСИМОСТИ', 'просп. Тауелсыздык, 52', 'ДВОРЕЦ НЕЗАВИСИМОСТИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1110', 'de', 'КОРМЕ ОРТАЛЫГЫ', 'проспект Астана 12', 'ВЦ «Корме Орталыгы»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1097', 'de', 'GRAND NUR PLAZA', '29 А мкр.', 'Grand Nur Plaza Hotel & Convention Centre');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1111', 'de', 'КОНЫС', 'пр. Абулхаир-хана, 52', 'Дворец спорта \"Коныс\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1112', 'de', 'ЭНЕРГЕТИК', 'пл. Конституции, 1', 'Теннисный центр \"Энергетик\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1113', 'de', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ', 'Самал 1, д.44', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1114', 'de', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА', 'пр. Абая, 44, уг. ул. Байтурсынова', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1115', 'de', 'АВИАЦИОННАЯ БАЗА ВС РК', 'район Международного аэропорта “Астана”', 'АВИАЦИОННАЯ БАЗА ВС РК');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1134', 'de', 'ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА', 'ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1087', 'de', 'MOLDEXPO', 'ул. Гиочеилор, 1', 'МВЦ \"MOLDEXPO\" АО');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1132', 'de', 'ALMATY TOWERS', 'ул. Байзакова, 280', 'ALMATY TOWERS');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1091', 'de', 'ШАХТЕР', 'ул. Казахстанская 1', 'СТАДИОН \"ШАХТЕР\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1116', 'de', 'PALATUL REPABLICII', 'М.Чиботарь, 16', 'PALATUL REPABLICII');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1117', 'de', 'МАНЕЖ КГАФКИС', 'ул.Ахунбаева, 97', 'МАНЕЖ КГАФКИС');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1118', 'de', 'ДВОРЕЦ СПОРТА', 'ул. Т. Молдо, 40', 'ДВОРЕЦ СПОРТА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1088', 'de', 'УЗЭКСПОЦЕНТР', 'Амира Темура улица, 107', 'УЗЭКСПОЦЕНТР НВК');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1135', 'de', 'АВТОТЕХХИЗМАТ-Ф', 'Салон \"Автотеххизмат-Ф\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1093', 'de', 'УЗКУРГАЗМАСАВДО', 'ул. Беруни, 41', 'Республиканский центр выставочно-ярмарочной торговли «УзКургазмаСавдо»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1119', 'de', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ', 'проспект Мустакиллик, 2', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1120', 'de', 'ТПП ТУРКМЕНИСТАНА', 'проспект Чандыбиль шаёлы, 143', 'ТПП ТУРКМЕНИСТАНА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1136', 'de', 'АВАЗА', 'НАЦИОНАЛЬНАЯ ТУРИСТИЧЕСКАЯ ЗОНА «АВАЗА»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1089', 'de', 'EXPOGEORGIA', 'Церетели, 118', 'EXPOGEORGIA');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1137', 'de', 'ТПП МОНГОЛИИ', 'ТПП МОНГОЛИИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1094', 'de', 'МВЦ', 'Броварской пр. 15', 'МВЦ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1121', 'de', 'КИЕВЭКСПОПЛАЗА', 'ул. Салютная, 2-Б', 'ВЫСТАВОЧНЫЙ ЦЕНТР КИЕВЭКСПОПЛАЗА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1122', 'de', 'ЗАЛ ОДЕССКОГО МОРВОКЗАЛА', 'ул. Приморская, 6', 'Концертно-выставочный Зал Одесского Морвокзала');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1123', 'de', 'OPEN-AIR', 'улица Дерибасовская', 'OPEN-AIR');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1124', 'de', 'ОДЕССКАЯ КИНОСТУДИЯ', 'Французский бульвар 33', 'ОДЕССКАЯ КИНОСТУДИЯ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1090', 'de', 'КИЕВЭКСПОПЛАЗА', 'ул.Салютная, 2-Б', 'КИЕВЭКСПОПЛАЗА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1125', 'de', 'ЧЕРВОНА КАЛИНА', 'проспект Генерала Ватутина', 'Яхт-клуб \"Червона Калина\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1126', 'de', 'ОДЕССКИЙ АКАДЕМИЧЕСКИЙ ТЕАТР', 'ул. Пантелеймоновская, 3', 'Одесский академический театр музыкальной комедии имени Михаила Водяного');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1127', 'de', 'НСК ОЛИМПИЙСКИЙ', 'ул. Большая Васильковская, 55', 'НСК ОЛИМПИЙСКИЙ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1098', 'de', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА', 'ул. Мазепы Ивана, 13', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1128', 'de', 'ТПП УКРАИНЫ', 'ул. Большая Житомирская, 33', 'ТПП УКРАИНЫ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1095', 'de', 'АККО', 'просп. Победы, 40-Б', 'ВЦ АККО ИНТЕРЕШНЛ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1099', 'de', 'ОДЕССКИЙ МОРСКОЙ ПОРТ', 'ул. Приморская, 6', 'ОДЕССКИЙ МОРСКОЙ ПОРТ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1129', 'de', 'ДРУЖБА', 'ул. Киевская, 115', 'ДКП СК \"ДРУЖБА\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1130', 'de', 'ЯЛТА-ТУРИСТ', 'ул. Дражинского 50', 'ГК \"ЯЛТА-ТУРИСТ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1131', 'de', 'МУССОН', 'Ул.Вакуленчука 29', 'ТРЦ МУССОН');
			
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1084', 'en', 'КОРМЕ', 'пр. Г.Алиева, 515', 'ЦЕНТР ВЫСТАВОК И КОНФЕРЕНЦИЙ \"БАКУ ЭКСПО ЦЕНТР\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1100', 'en', 'ЕРЕВАН EXPO', 'ул. А. Акопяна 3', 'ВЫСТАВОЧНЫЙ КОМПЛЕКС \"ЕРЕВАН EXPO\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1092', 'en', 'БЕЛЭКСПО', 'ул.Я.Купалы, 27', 'НАЦИОНАЛЬНЫЙ ВЫСТАВОЧНЫЙ ЦЕНТР \"БЕЛЭКСПО\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1101', 'en', 'ФУТБОЛЬНЫЙ МАНЕЖ', 'пр-т Победителей, 20/2', 'ФУТБОЛЬНЫЙ МАНЕЖ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1133', 'en', 'АГ. ЩОМЫСЛИЦА, 28', 'АГ. ЩОМЫСЛИЦА, 28');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1102', 'en', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ', 'пр.Дзержинского, 83', 'БЕЛОРУССКИЙ ГОСУДАРСТВЕННЫЙ МЕДИЦИНСКИЙ УНИВЕРСИТЕТ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1096', 'en', 'ДВОРЕЦ ИСКУССТВ', 'ул. Ульяновская, 35/31', 'ГОСУДАРСТВЕННОЕ УЧРЕЖДЕНИЕ КУЛЬТУРЫ \"ДВОРЕЦ ИСКУССТВ\" Г.БОБРУЙСКА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1103', 'en', 'ОЛИМПИЕЦ', 'ул.30 лет Победы, 1А', 'СК \"ОЛИМПИЕЦ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1104', 'en', 'ЮНОСТЬ', 'ул. Ленинская, 39', 'КУЛЬТУРНЫЙ ЦЕНТР \"ЮНОСТЬ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1085', 'en', 'ДВОРЕЦ СПОРТА', 'пр. Победителей, 4', 'КС РУП «ДВОРЕЦ СПОРТА»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1138', 'en', 'ДВОРЕЦ ЖЕЛЕЗНОДОРОЖНИКОВ', 'ул. Чкалова 7');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1139', 'en', 'ВЫСТАВОЧНЫЙ ПАВИЛЬОН', 'бульвар Тракторостроителей');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1140', 'en', 'ТОРГОВЫЙ ПАВИЛЬОН', 'пл. Советская');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1086', 'en', 'КОРМЕ', 'ул.Достык, 3', 'ВЫСТАВОЧНЫЙ ЦЕНТР \"КОРМЕ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1141', 'en', 'ДВОРЕЦ ДЕТЕЙ И МОЛОДЕЖИ', 'Старовиленский тракт, 41');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`) VALUES ('1142', 'en', 'ДВОРЕЦ ИСКУССТВА', 'ул. Козлова, 3');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1105', 'en', 'ГОСТИНИЦА ВИКТОРИЯ', 'пр.Победителей, 59', 'ГОСТИНИЦА ВИКТОРИЯ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1106', 'en', 'АТАКЕНТ', 'Тимирязева, 42', 'ВЦ \"АТАКЕНТ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1107', 'en', 'АТЫРАУ', 'УЛ СМАГУЛОВА, 5А', 'СПОРТКОМПЛЕКС \"АТЫРАУ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1108', 'en', 'RENAISSANCE ATYRAU HOTEL', 'улица Сатпаева, 15 Б', 'ГОСТИНИЦА \"RENAISSANCE ATYRAU HOTEL\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1109', 'en', 'ДВОРЕЦ НЕЗАВИСИМОСТИ', 'просп. Тауелсыздык, 52', 'ДВОРЕЦ НЕЗАВИСИМОСТИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1110', 'en', 'КОРМЕ ОРТАЛЫГЫ', 'проспект Астана 12', 'ВЦ «Корме Орталыгы»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1097', 'en', 'GRAND NUR PLAZA', '29 А мкр.', 'Grand Nur Plaza Hotel & Convention Centre');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1111', 'en', 'КОНЫС', 'пр. Абулхаир-хана, 52', 'Дворец спорта \"Коныс\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1112', 'en', 'ЭНЕРГЕТИК', 'пл. Конституции, 1', 'Теннисный центр \"Энергетик\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1113', 'en', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ', 'Самал 1, д.44', 'ЦЕНТРАЛЬНЫЙ ГОСУДАРСТВЕННЫЙ МУЗЕЙ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1114', 'en', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА', 'пр. Абая, 44, уг. ул. Байтурсынова', 'ДВОРЕЦ СПОРТА ИМ. БАЛУАНА ШОПАКА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1115', 'en', 'АВИАЦИОННАЯ БАЗА ВС РК', 'район Международного аэропорта “Астана”', 'АВИАЦИОННАЯ БАЗА ВС РК');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1134', 'en', 'ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА', 'ДВОРЕЦ СПОРТА ИМ. КОЖОМКУЛА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1087', 'en', 'MOLDEXPO', 'ул. Гиочеилор, 1', 'МВЦ \"MOLDEXPO\" АО');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1132', 'en', 'ALMATY TOWERS', 'ул. Байзакова, 280', 'ALMATY TOWERS');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1091', 'en', 'ШАХТЕР', 'ул. Казахстанская 1', 'СТАДИОН \"ШАХТЕР\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1116', 'en', 'PALATUL REPABLICII', 'М.Чиботарь, 16', 'PALATUL REPABLICII');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1117', 'en', 'МАНЕЖ КГАФКИС', 'ул.Ахунбаева, 97', 'МАНЕЖ КГАФКИС');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1118', 'en', 'ДВОРЕЦ СПОРТА', 'ул. Т. Молдо, 40', 'ДВОРЕЦ СПОРТА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1088', 'en', 'УЗЭКСПОЦЕНТР', 'Амира Темура улица, 107', 'УЗЭКСПОЦЕНТР НВК');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1135', 'en', 'АВТОТЕХХИЗМАТ-Ф', 'Салон \"Автотеххизмат-Ф\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1093', 'en', 'УЗКУРГАЗМАСАВДО', 'ул. Беруни, 41', 'Республиканский центр выставочно-ярмарочной торговли «УзКургазмаСавдо»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1119', 'en', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ', 'проспект Мустакиллик, 2', 'ДВОРЕЦ ТВОРЧЕСТВА МОЛОДЕЖИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1120', 'en', 'ТПП ТУРКМЕНИСТАНА', 'проспект Чандыбиль шаёлы, 143', 'ТПП ТУРКМЕНИСТАНА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1136', 'en', 'АВАЗА', 'НАЦИОНАЛЬНАЯ ТУРИСТИЧЕСКАЯ ЗОНА «АВАЗА»');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1089', 'en', 'EXPOGEORGIA', 'Церетели, 118', 'EXPOGEORGIA');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `description`) VALUES ('1137', 'en', 'ТПП МОНГОЛИИ', 'ТПП МОНГОЛИИ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1094', 'en', 'МВЦ', 'Броварской пр. 15', 'МВЦ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1121', 'en', 'КИЕВЭКСПОПЛАЗА', 'ул. Салютная, 2-Б', 'ВЫСТАВОЧНЫЙ ЦЕНТР КИЕВЭКСПОПЛАЗА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1122', 'en', 'ЗАЛ ОДЕССКОГО МОРВОКЗАЛА', 'ул. Приморская, 6', 'Концертно-выставочный Зал Одесского Морвокзала');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1123', 'en', 'OPEN-AIR', 'улица Дерибасовская', 'OPEN-AIR');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1124', 'en', 'ОДЕССКАЯ КИНОСТУДИЯ', 'Французский бульвар 33', 'ОДЕССКАЯ КИНОСТУДИЯ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1090', 'en', 'КИЕВЭКСПОПЛАЗА', 'ул.Салютная, 2-Б', 'КИЕВЭКСПОПЛАЗА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1125', 'en', 'ЧЕРВОНА КАЛИНА', 'проспект Генерала Ватутина', 'Яхт-клуб \"Червона Калина\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1126', 'en', 'ОДЕССКИЙ АКАДЕМИЧЕСКИЙ ТЕАТР', 'ул. Пантелеймоновская, 3', 'Одесский академический театр музыкальной комедии имени Михаила Водяного');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1127', 'en', 'НСК ОЛИМПИЙСКИЙ', 'ул. Большая Васильковская, 55', 'НСК ОЛИМПИЙСКИЙ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1098', 'en', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА', 'ул. Мазепы Ивана, 13', 'КИЕВСКИЙ ДВОРЕЦ ДЕТЕЙ И ЮНОШЕСТВА');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1128', 'en', 'ТПП УКРАИНЫ', 'ул. Большая Житомирская, 33', 'ТПП УКРАИНЫ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1095', 'en', 'АККО', 'просп. Победы, 40-Б', 'ВЦ АККО ИНТЕРЕШНЛ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1099', 'en', 'ОДЕССКИЙ МОРСКОЙ ПОРТ', 'ул. Приморская, 6', 'ОДЕССКИЙ МОРСКОЙ ПОРТ');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1129', 'en', 'ДРУЖБА', 'ул. Киевская, 115', 'ДКП СК \"ДРУЖБА\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1130', 'en', 'ЯЛТА-ТУРИСТ', 'ул. Дражинского 50', 'ГК \"ЯЛТА-ТУРИСТ\"');
			INSERT INTO {{trexhibitioncomplex}} (`trParentId`, `langId`, `name`, `street`, `description`) VALUES ('1131', 'en', 'МУССОН', 'Ул.Вакуленчука 29', 'ТРЦ МУССОН');
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}