<?php

class m160726_130140_reestablish_user_1397_pass extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function upSql()
	{
		return "
			UPDATE {{user}} SET password = '$2y$10\$h0U8AqH.PeStFznOWFzbf.EHQwfYaDZ9pmj.Zv9Qh30pcjFDhJzGS'
			WHERE id = 1397;
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}