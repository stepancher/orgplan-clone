<?php

class m160615_071956_updated_trdocuments extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/184/stand_plan_en.pdf' WHERE `id`='20';
			UPDATE {{trdocuments}} SET `value`='/static/documents/fairs/184/stand_plan_ru.pdf' WHERE `id`='7';
		";
	}
}