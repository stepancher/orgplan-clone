<?php

class m160310_105734_create_route_table extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{route}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			CREATE TABLE {{route}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `title` varchar(255) DEFAULT NULL,
			  `description` varchar(255) DEFAULT NULL,
			  `keywords` varchar(255) DEFAULT NULL,
			  `url` varchar(255) DEFAULT NULL,
			  `route` varchar(255) DEFAULT NULL,
			  `params` text,
			  `refId` int(11) DEFAULT NULL,
			  `header` varchar(255) DEFAULT NULL,
			  `lang` varchar(255) DEFAULT \'ru\',
			  `lastmode` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  `sitemapPriority` float DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=93574 DEFAULT CHARSET=utf8;

		';
	}
}