<?php

class m160329_124325_alter_attributeclass_change_proposal4 extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = "
			DELETE FROM {{attributeclass}} WHERE `name` = 'electrical_connections_connection_of_electricity_10kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'power_connection_count_10kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'electrical_connections_connection_of_electricity_20kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'power_connection_count_20kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'electrical_connections_connection_of_electricity_40kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'power_connection_count_40kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'connections_connection_of_electricity_60kv';
			DELETE FROM {{attributeclass}} WHERE `name` = 'power_connection_count_60kv';
		";

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{attributeclass}} SET `parent`='75' WHERE `id`='210';
			UPDATE {{attributeclass}} SET `priority`='2' WHERE `id`='80';
			UPDATE {{attributeclass}} SET `label`='до 5 кВт включительно' WHERE `id`='80';
			DELETE FROM {{attributeclass}} WHERE `id`='139';
			UPDATE {{attributeclass}} SET `parent`='75', `priority`='7' WHERE `id`='237';
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'electrical_connections_connection_of_electricity_10kv', 'bool', '0', '0', 'до 10 кВт включительно', '75', '75', '3', 'checkboxSwitcher', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `tooltip`, `coefficientTooltipText`) VALUES (NULL, 'power_connection_count_10kv', 'string', '0', '0', '245', '75', '1', 'coefficientDouble', '0', 'шт.', '<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 25.08.16 и на 100% после 23.09.16.', 'Подключение в соответствии с заявленной мощностью: - при заказе с 02 по 26.09.16 г. коэффициент 1,5 - при заказе после 26.09.16 г. коэффициент 2');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'electrical_connections_connection_of_electricity_20kv', 'bool', '0', '0', 'до 20 кВт включительно', '75', '75', '4', 'checkboxSwitcher', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `tooltip`, `coefficientTooltipText`) VALUES (NULL, 'power_connection_count_20kv', 'string', '0', '0', '247', '75', '1', 'coefficientDouble', '0', 'шт.', '<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 25.08.16 и на 100% после 23.09.16.', 'Подключение в соответствии с заявленной мощностью: - при заказе с 02 по 26.09.16 г. коэффициент 1,5 - при заказе после 26.09.16 г. коэффициент 2');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES (NULL, 'electrical_connections_connection_of_electricity_40kv', 'bool', '0', '0', 'до 40 кВт включительно', '75', '75', '5', 'checkboxSwitcher', '1');
			INSERT INTO {{attributeclass}} (`id`, `name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `tooltip`, `coefficientTooltipText`) VALUES (NULL, 'power_connection_count_40kv', 'string', '0', '0', '249', '75', '1', 'coefficientDouble', '0', 'шт.', '<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 25.08.16 и на 100% после 23.09.16.', 'Подключение в соответствии с заявленной мощностью: - при заказе с 02 по 26.09.16 г. коэффициент 1,5 - при заказе после 26.09.16 г. коэффициент 2');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `label`, `parent`, `super`, `priority`, `class`, `collapse`) VALUES ('electrical_connections_connection_of_electricity_60kv', 'bool', '0', '0', 'до 40 кВт включительно', '75', '75', '6', 'checkboxSwitcher', '1');
			INSERT INTO {{attributeclass}} (`name`, `dataType`, `isArray`, `isMulty`, `parent`, `super`, `priority`, `class`, `collapse`, `unitTitle`, `tooltip`, `coefficientTooltipText`) VALUES ('power_connection_count_60kv', 'string', '0', '0', '252', '75', '1', 'coefficientDouble', '0', 'шт.', '<div class=\"hint-header\">Стоимость услуг</div> Стоимость услуг на технические подключения увеличивается на 50% при заказе после 25.08.16 и на 100% после 23.09.16.', 'Подключение в соответствии с заявленной мощностью: - при заказе с 02 по 26.09.16 г. коэффициент 1,5 - при заказе после 26.09.16 г. коэффициент 2');
			UPDATE {{attributeclass}} SET `label`='до 60 кВт включительно' WHERE `name`='electrical_connections_connection_of_electricity_60kv';
			UPDATE {{attributeclass}} SET `parent`='251' WHERE `name`='power_connection_count_60kv';
		";
	}
}