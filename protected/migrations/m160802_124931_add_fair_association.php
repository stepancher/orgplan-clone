<?php

class m160802_124931_add_fair_association extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function upSql()
	{
		return "
			CREATE TABLE {{association}} (
			  `id` INT(11) NOT NULL AUTO_INCREMENT,
			  PRIMARY KEY (`id`));
			  
			ALTER TABLE {{association}} 
			ADD COLUMN `name` VARCHAR(255) NULL DEFAULT NULL AFTER `id`;
			  
			CREATE TABLE {{fairhasassociation}} (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`fairId` INT(11) NULL DEFAULT NULL,
			`associationId` INT(11) NULL DEFAULT NULL,
			PRIMARY KEY (`id`));
		";
	}

	public function downSql()
	{
		return TRUE;
	}
}