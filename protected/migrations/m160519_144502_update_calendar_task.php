<?php

class m160519_144502_update_calendar_task extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		return true;
	}

	private function upSql() {

		return "
			ALTER TABLE {{calendarfairtasks}}
			CHANGE COLUMN `uuid` `uuid` CHAR(36) NOT NULL,
			CHANGE COLUMN `group` `group` CHAR(36) NOT NULL;

			ALTER TABLE {{calendarfairuserstasks}}
			CHANGE COLUMN `completeDate` `completeDate` DATETIME NULL DEFAULT NULL AFTER `userId`,
			CHANGE COLUMN `duration` `duration` MEDIUMINT(8) NULL DEFAULT NULL AFTER `date`,
			CHANGE COLUMN `id` `uuid` CHAR(36) NOT NULL;

			UPDATE {{calendarfairuserstasks}} user_task
			LEFT JOIN {{calendarfairtasks}} task ON task.id = user_task.taskId
			SET user_task.`uuid` = task.`uuid`;
		";
	}
}