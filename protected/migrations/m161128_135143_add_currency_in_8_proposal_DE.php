<?php

class m161128_135143_add_currency_in_8_proposal_DE extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->dbProposalF10943->beginTransaction();
        try {
            Yii::app()->dbProposalF10943->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{trattributeclassproperty}} (`id`, `trParentId`, `langId`, `value`) VALUES (NULL, '2127', 'de', 'USD');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}