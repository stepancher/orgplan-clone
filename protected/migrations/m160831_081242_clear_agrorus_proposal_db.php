<?php

class m160831_081242_clear_agrorus_proposal_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {

        list($peace1, $peace2, $dbProposalAgrorusName) = explode('=', Yii::app()->dbProposalAgrorus->connectionString);

        return "
            delete from {$dbProposalAgrorusName}.tbl_organizerexponent where organizerId in (3563, 3738);
            delete from {$dbProposalAgrorusName}.tbl_proposalelement;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}