<?php

class m170424_115629_add_new_citries extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('313', 'marsfield');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1556', 'ru', 'Марсфилд');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1556', 'en', 'Marsfield');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1556', 'de', 'Marsfield');
          
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('311', 'claremont');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1557', 'ru', 'Клермонт');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1557', 'en', 'Claremont');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1557', 'de', 'Claremont');
            
            INSERT INTO {{region}} (`districtId`) VALUES ('117');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1097', 'ru', 'Суррей', 'surrey');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1097', 'en', 'Surrey', 'surrey');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1097', 'de', 'Surrey', 'surrey');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1097', 'dorking');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1558', 'ru', 'Доркинг');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1558', 'en', 'Dorking');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1558', 'de', 'Dorking');

            INSERT INTO {{region}} (`districtId`) VALUES ('117');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1098', 'ru', 'Бакингемшир', 'buckinghamshire');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1098', 'en', 'Buckinghamshire', 'buckinghamshire');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1098', 'de', 'Buckinghamshire', 'buckinghamshire');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1098', 'edlesborough');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1559', 'ru', 'Эдлсборо');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1559', 'en', 'Edlesborough');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1559', 'de', 'Edlesborough');

            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('12', 'ru', 'Северный Рейн-Вестфалия');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('12', 'en', 'North Rhine-Westphalia');
            INSERT INTO {{trdistrict}} (`trParentId`, `langId`, `name`) VALUES ('12', 'de', 'Nordrhein-Westfalen');
            INSERT INTO {{region}} (`districtId`) VALUES ('12');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1099', 'ru', 'Аахен', 'aachen');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1099', 'en', 'Aachen (district)', 'aachen');
            INSERT INTO {{trregion}} (`trParentId`, `langId`, `name`, `shortUrl`) VALUES ('1099', 'de', 'Städteregion Aachen', 'aachen');
            INSERT INTO {{city}} (`regionId`, `shortUrl`) VALUES ('1099', 'herzogenrath');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1560', 'ru', 'Херцогенрат');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1560', 'en', 'Herzogenrath');
            INSERT INTO {{trcity}} (`trParentId`, `langId`, `name`) VALUES ('1560', 'de', 'Herzogenrath');

		";
    }

    public function downSql()
    {
        return TRUE;
    }
}