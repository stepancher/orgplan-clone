<?php

class m170220_110639_copy_audithasassociation_to_db extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        list($peace1, $peace2, $expodata) = explode('=', Yii::app()->expodata->connectionString);
        list($peace1, $peace2, $db) = explode('=', Yii::app()->db->connectionString);

        return "
            DROP PROCEDURE IF EXISTS {$expodata}.`copy_exdb_audithasassociation_to_db`;
            CREATE PROCEDURE {$expodata}.`copy_exdb_audithasassociation_to_db`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE aud_id INT DEFAULT 0;
                DECLARE ass_id INT DEFAULT 0;
                DECLARE copy CURSOR FOR SELECT DISTINCT aud.id, assoc.id FROM {$expodata}.{{exdbaudithasassociation}} aha
                                            LEFT JOIN {$expodata}.{{exdbaudit}} au ON au.id = aha.auditId
                                            LEFT JOIN {$expodata}.{{exdbassociation}} ass ON ass.id = aha.associationId
                                            LEFT JOIN {$db}.{{audit}} aud ON aud.exdbId = au.exdbId
                                            LEFT JOIN {$db}.{{association}} assoc ON assoc.exdbId = ass.exdbId
                                            LEFT JOIN {$db}.{{audithasassociation}} oaha ON oaha.auditId = aud.id AND oaha.associationId = assoc.id
                                            WHERE oaha.id IS NULL
                                        ORDER BY aud.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                    
                    read_loop: LOOP
                        
                        FETCH copy INTO aud_id, ass_id;
                        
                        IF done THEN
                            LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            INSERT INTO {$db}.{{audithasassociation}} (`auditId`,`associationId`) VALUES (aud_id, ass_id);
                        COMMIT;
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL {$expodata}.`copy_exdb_audithasassociation_to_db`();
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}