<?php

class m160912_092057_update_favorites_route extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            delete from {{seo}} where route like '%favorites%';
            INSERT INTO {{route}} (`url`, `route`, `params`, `lang`, `sitemapPriority`) VALUES ('/ru/favorites', 'favorites/default/index', '[]', 'ru', '0.5');
            INSERT INTO {{route}} (`title`, `description`, `url`, `route`, `params`, `lang`) VALUES ('', '', '/ru/favorites', 'favorites/default/index', '[]', 'ru');
            INSERT INTO {{route}} (`url`, `route`, `params`, `lang`) VALUES ('/en/favorites', 'favorites/default/index', '[]', 'en');
            INSERT INTO {{route}} (`url`, `route`, `params`, `lang`) VALUES ('/de/favorites', 'favorites/default/index', '[]', 'de');
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}