<?php

class m151104_124453_add_table_documents extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
				DELETE FROM {{documents}} WHERE id = 2;
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
			INSERT INTO {{documents}} (`id`, `name`, `description`, `type`, `value`) VALUES
			(2, "Форма обработки конткатов на выставке", "Описание формы обработки конткатов на выставке", 2, "/ru/site/DocumentContacts/");
			ON DUPLICATE KEY UPDATE `name` = "Форма обработки конткатов на выставке",
									`description` = "Описание формы обработки конткатов на выставке",
									`type` = 2,
									`value` = "/ru/site/DocumentContacts/";
		';
	}
}