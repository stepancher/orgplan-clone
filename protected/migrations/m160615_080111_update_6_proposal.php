<?php

class m160615_080111_update_6_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			UPDATE {{trattributeclass}} SET `label`='LCD displays lease', `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div> ' WHERE `id`='1494';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div> ' WHERE `id`='450';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div> ' WHERE `id`='1446';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div>' WHERE `id`='1468';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div>' WHERE `id`='1515';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div>' WHERE `id`='1537';
			UPDATE {{trattributeclass}} SET `tooltipCustom`='<div class=\"header\">\nEquipment lease\n</div>\n<div class=\"body\">\nThe lease cost includes installation works, all the commutation elements and cables that make part of the set.\n</div>' WHERE `id`='1425';
			UPDATE {{trattributeclass}} SET `label`='Adjustment of the client\'s equipment is performed for additional payment.' WHERE `id`='2245';

			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1911';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1912';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1913';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1914';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1915';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1916';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1917';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1918';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1919';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1920';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1921';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1922';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1923';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1924';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1925';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1926';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1927';
			UPDATE {{attributeclassproperty}} SET `defaultValue`='2016-10-07' WHERE `id`='1928';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2019', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2016', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2022', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2025', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2031', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2028', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2037', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2040', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2043', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2046', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2049', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2052', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2055', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2059', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2062', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2065', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2068', '2016-10-04', 'startDate');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `defaultValue`, `class`) VALUES ('2071', '2016-10-04', 'startDate');
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2485';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2488';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2491';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2494';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2497';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2500';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2505';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2510';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2515';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2518';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2521';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2524';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2527';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2530';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2533';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2536';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2539';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2542';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2545';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2548';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2551';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2556';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2561';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2563';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2565';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2567';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2569';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2571';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2573';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2575';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2577';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2579';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2581';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2583';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2585';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2587';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2592';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2593';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2594';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2595';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2596';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2597';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2598';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2599';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2600';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2601';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2602';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2603';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2604';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2605';
			DELETE FROM {{trattributeclassproperty}} WHERE `id`='2606';
			
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2041', '2040', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2044', '2043', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2047', '2046', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2050', '2049', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2053', '2052', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2056', '2055', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2060', '2059', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2063', '2062', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2066', '2065', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2069', '2068', 'inherit', '0');
			INSERT INTO {{attributeclassdependency}} (`attributeClass`, `attributeClassRelated`, `function`, `auto`) VALUES ('2072', '2071', 'inherit', '0');
			
			UPDATE {{attributeclass}} SET `class`='datePicker' WHERE `id`='2031';
			UPDATE {{attributeclass}} SET `class`='datePickerDisabled' WHERE `id`='2028';
			UPDATE {{attributeclass}} SET `class`='datePickerDisabled' WHERE `id`='2031';
			UPDATE {{attributeclass}} SET `class`='datePicker' WHERE `id`='2028';
			
			
			UPDATE {{trattributeclass}} SET `placeholder`='2016/10/04 - 2016/10-07' WHERE `id`='1511';
			UPDATE {{trattributeclass}} SET `placeholder`='Выберите дату' WHERE `id`='1508';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date' WHERE `id`='1499';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date' WHERE `id`='1505';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date' WHERE `id`='1508';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date and time' WHERE `id`='1520';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1517';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1523';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1526';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1529';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1532';
			UPDATE {{trattributeclass}} SET `placeholder`='Select date or period' WHERE `id`='1535';
		";
	}
}
