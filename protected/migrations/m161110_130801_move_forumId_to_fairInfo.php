<?php

class m161110_130801_move_forumId_to_fairInfo extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            CREATE PROCEDURE `move_forumId_to_fairinfo`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE fair_id INT DEFAULT 0;
                DECLARE fair_info_id INT DEFAULT 0;
                
                DECLARE copy CURSOR FOR SELECT f.id, fi.id FROM {{fair}} f
                    LEFT JOIN {{fairinfo}} fi ON f.infoId = fi.id WHERE f.fairIsForum = 1;
		        
		        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
		        
		        OPEN copy;
		            read_loop: LOOP
		            
		                FETCH copy INTO fair_id, fair_info_id;
		                
		                IF done THEN 
		                    LEAVE read_loop;
                        END IF;
                        
                        START TRANSACTION;
                            UPDATE {{fairinfo}} SET `forumId` = fair_id WHERE `id` = fair_info_id;
                        COMMIT;
                        
                    END LOOP;
                CLOSE copy;
            END;
            
            CALL move_forumId_to_fairinfo();
            DROP PROCEDURE IF EXISTS `move_forumId_to_fairinfo`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}