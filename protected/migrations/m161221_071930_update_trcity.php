<?php

class m161221_071930_update_trcity extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function upSql()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('shard is not NULL');
        $fairs = Fair::model()->findAll($criteria);

        $sql= "
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('38', 'de', 'Besucherformular (Vorlage)', 'Besucherformular', '/uploads/documents/fairs/ancata_posetitelya_stend.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('39', 'de', 'Einweisung der Mitarbeiter (Vorlage)', 'Einweisung der Mitarbeiter', '/uploads/documents/fairs/instructazh sotrudnikov.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('40', 'de', 'Menü für Kaffeepause, Buffet (Vorlage)', 'Menü für Kaffeepause, Buffet', '/uploads/documents/fairs/menu cofe-breika.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('41', 'de', 'Produktliste für Messestand (Vorlage)', 'Produktliste für Messestand', '/uploads/documents/fairs/perechen productov na stend.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('42', 'de', 'Tipps für Aussteller: PR-Arbeit, MARKENAUFBAU', 'Ziel: PR-Arbeit, MARKENAUFBAU', '/uploads/documents/fairs/soveti uchastnike dlya celi pr, vystraivanie brenda.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('43', 'de', 'Tipps für Aussteller: MARKETINGUNTERSUCHUNGEN', 'Ziel: MARKETINGUNTERSUCHUNGEN', '/uploads/documents/fairs/soveti uchastnike dlya celi marketingovye issledovania.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('44', 'de', 'Tipps für Aussteller: AUFBAU VON KUNDENBEZIEHUNGEN', 'Ziel: AUFBAU VON KUNDENBEZIEHUNGEN', '/uploads/documents/fairs/soveti uchastnike dlya celi otnoshenie s potrebitelyami.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('45', 'de', 'Tipps für Aussteller: UNTERSTÜTZUNG DER VERTRIEBSKANÄLE', 'Ziel: UNTERSTÜTZUNG DER VERTRIEBSKANÄLE', '/uploads/documents/fairs/soveti uchastnike dlya celi podderzhka kanalov sbyta.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('46', 'de', 'Tipps für Aussteller: VERKAUF', 'Ziel: VERKAUF', '/uploads/documents/fairs/soveti uchastnike dlya celi prodazha.jpg');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('47', 'de', 'Liste der Exponate (Vorlage)', 'Liste der Exponate', '/uploads/documents/fairs/spisok eksponatov.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('48', 'de', 'Formular für Kontaktbearbeitung am Stand (Vorlage)', 'Formular für Kontaktbearbeitung am Stand', '/uploads/documents/fairs/forma obrabotki kontaktov na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('49', 'de', 'Checkliste für Produktvorführung am Stand (Vorlage)', 'Checkliste für Produktvorführung am Stand', '/uploads/documents/fairs/check list demonstracii producta na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('50', 'de', 'Checkliste zur Ausschreibung (Vorlage)', 'Checkliste zur Ausschreibung', '/uploads/documents/fairs/check list konkursa.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('51', 'de', 'Checkliste Konferenz (Vorlage)', 'Checkliste Konferenz', '/uploads/documents/fairs/check list konferencii.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('52', 'de', 'Checkliste Meisterklasse (Vorlage)', 'Checkliste Meisterklasse', '/uploads/documents/fairs/check list master classa.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('53', 'de', 'Checkliste für Mahlzeiten-Menüs am Stand (Vorlage)', 'Checkliste für Mahlzeiten-Menüs am Stand', '/uploads/documents/fairs/check list menu pitaniya na stende.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('54', 'de', 'Checkliste für Veranstaltungen am Stand (Vorlage)', 'Checkliste für Veranstaltungen am Stand', '/uploads/documents/fairs/check list meropriyatii na vystavke.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('56', 'de', 'Checkliste Pressekonferenz (Vorlage)', 'Checkliste Pressekonferenz', '/uploads/documents/fairs/check list pressconferencii.docx');
            INSERT INTO {{trdocuments}} (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('57', 'de', 'Checkliste Pressemitteilung (Vorlage)', 'Checkliste Pressemitteilung', '/uploads/documents/fairs/check list press reliza.docx');
            UPDATE {{trcity}} SET `name`='Makarye' WHERE `id`='1528';
            UPDATE {{trcity}} SET `name`='Makarye' WHERE `id`='1662';
            UPDATE {{trcity}} SET `name`='Prutskoi' WHERE `id`='1370';
            UPDATE {{trcity}} SET `name`='Prutskoi' WHERE `id`='1557';
        ";

        /**
         * @var Fair $fair
         */
        foreach($fairs as $fair){
            if(
                !empty($fair->shard) &&
                $fair->shard != '' &&
                $fair->shard != ' '
            ){
                $dbTargetName = Yii::app()->dbMan->createShardName($fair->id);

                $sql = $sql."
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('38', 'de', 'Besucherformular (Vorlage)', 'Besucherformular', '/uploads/documents/fairs/ancata_posetitelya_stend.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('39', 'de', 'Einweisung der Mitarbeiter (Vorlage)', 'Einweisung der Mitarbeiter', '/uploads/documents/fairs/instructazh sotrudnikov.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('40', 'de', 'Menü für Kaffeepause, Buffet (Vorlage)', 'Menü für Kaffeepause, Buffet', '/uploads/documents/fairs/menu cofe-breika.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('41', 'de', 'Produktliste für Messestand (Vorlage)', 'Produktliste für Messestand', '/uploads/documents/fairs/perechen productov na stend.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('42', 'de', 'Tipps für Aussteller: PR-Arbeit, MARKENAUFBAU', 'Ziel: PR-Arbeit, MARKENAUFBAU', '/uploads/documents/fairs/soveti uchastnike dlya celi pr, vystraivanie brenda.jpg');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('43', 'de', 'Tipps für Aussteller: MARKETINGUNTERSUCHUNGEN', 'Ziel: MARKETINGUNTERSUCHUNGEN', '/uploads/documents/fairs/soveti uchastnike dlya celi marketingovye issledovania.jpg');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('44', 'de', 'Tipps für Aussteller: AUFBAU VON KUNDENBEZIEHUNGEN', 'Ziel: AUFBAU VON KUNDENBEZIEHUNGEN', '/uploads/documents/fairs/soveti uchastnike dlya celi otnoshenie s potrebitelyami.jpg');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('45', 'de', 'Tipps für Aussteller: UNTERSTÜTZUNG DER VERTRIEBSKANÄLE', 'Ziel: UNTERSTÜTZUNG DER VERTRIEBSKANÄLE', '/uploads/documents/fairs/soveti uchastnike dlya celi podderzhka kanalov sbyta.jpg');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('46', 'de', 'Tipps für Aussteller: VERKAUF', 'Ziel: VERKAUF', '/uploads/documents/fairs/soveti uchastnike dlya celi prodazha.jpg');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('47', 'de', 'Liste der Exponate (Vorlage)', 'Liste der Exponate', '/uploads/documents/fairs/spisok eksponatov.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('48', 'de', 'Formular für Kontaktbearbeitung am Stand (Vorlage)', 'Formular für Kontaktbearbeitung am Stand', '/uploads/documents/fairs/forma obrabotki kontaktov na stende.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('49', 'de', 'Checkliste für Produktvorführung am Stand (Vorlage)', 'Checkliste für Produktvorführung am Stand', '/uploads/documents/fairs/check list demonstracii producta na stende.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('50', 'de', 'Checkliste zur Ausschreibung (Vorlage)', 'Checkliste zur Ausschreibung', '/uploads/documents/fairs/check list konkursa.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('51', 'de', 'Checkliste Konferenz (Vorlage)', 'Checkliste Konferenz', '/uploads/documents/fairs/check list konferencii.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('52', 'de', 'Checkliste Meisterklasse (Vorlage)', 'Checkliste Meisterklasse', '/uploads/documents/fairs/check list master classa.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('53', 'de', 'Checkliste für Mahlzeiten-Menüs am Stand (Vorlage)', 'Checkliste für Mahlzeiten-Menüs am Stand', '/uploads/documents/fairs/check list menu pitaniya na stende.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('54', 'de', 'Checkliste für Veranstaltungen am Stand (Vorlage)', 'Checkliste für Veranstaltungen am Stand', '/uploads/documents/fairs/check list meropriyatii na vystavke.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('56', 'de', 'Checkliste Pressekonferenz (Vorlage)', 'Checkliste Pressekonferenz', '/uploads/documents/fairs/check list pressconferencii.docx');
                    INSERT INTO `{$dbTargetName}`.`tbl_trdocuments` (`trParentId`, `langId`, `name`, `description`, `value`) VALUES ('57', 'de', 'Checkliste Pressemitteilung (Vorlage)', 'Checkliste Pressemitteilung', '/uploads/documents/fairs/check list press reliza.docx');
                ";
            }
        }

        return $sql;
    }

    public function downSql()
    {
        return TRUE;
    }
}