<?php

class m170328_074217_move_fair_statistic_to_tr extends CDbMigration
{
    /**
     * @return bool
     * @throws CDbException
     */
    public function up()
    {
        $sql = $this->upSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }

    public function down()
    {
        $sql = $this->downSql();

        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand($sql)->execute();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            echo $e->getMessage();

            return false;
        }

        return true;
    }


    public function upSql()
    {
        return "
            DROP PROCEDURE IF EXISTS `copy_fair_statistic_to_tr`;
            CREATE PROCEDURE `copy_fair_statistic_to_tr`()
            BEGIN
                DECLARE done BOOL DEFAULT FALSE;
                DECLARE f_id INT DEFAULT 0;
                DECLARE f_stat TEXT DEFAULT '';
                DECLARE copy CURSOR FOR SELECT f.id, fi.statistics FROM {{fair}} f
                                        LEFT JOIN {{fairinfo}} fi ON f.infoId = fi.id;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                
                OPEN copy;
                
                read_loop: LOOP
                
                    FETCH copy INTO f_id, f_stat;
                    
                    IF done THEN 
                        LEAVE read_loop;
                    END IF;
                    
                    START TRANSACTION;
                        UPDATE {{trfair}} SET `statistics` = f_stat WHERE `trParentId` = f_id;
                    COMMIT;
                END LOOP;
                CLOSE copy;
            END;
            
            CALL `copy_fair_statistic_to_tr`();
            DROP PROCEDURE IF EXISTS `copy_fair_statistic_to_tr`;
            
            ALTER TABLE {{fairinfo}} 
            DROP COLUMN `statistics`;
		";
    }

    public function downSql()
    {
        return TRUE;
    }
}