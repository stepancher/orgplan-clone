<?php

class m160317_122057_delete_fairs_without_organizers extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}


	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE `id` IN (
				2261,
				10018,
				10020,
				10021,
				10022,
				10023,
				10024
			);

			DELETE FROM {{trfair}} WHERE `trParentId` IN (
				2261,
				10018,
				10020,
				10021,
				10022,
				10023,
				10024
			);
		";
	}
}