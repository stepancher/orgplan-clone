<?php

class m160417_163411_update_dependences extends CDbMigration {

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up() {

		$sql = $this->upSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function down() {

		$sql = $this->downSql();

		$transaction = Yii::app()->db->beginTransaction();
		try {
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollback();
			echo $e->getMessage();

			return false;
		}

		return true;
	}

	private function upSql() {

		return "
			TRUNCATE {{dependences}};

			INSERT INTO {{dependences}} VALUES
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(5,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/15'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"save\":1}','calendarm/gantt/add/id/20'),
			(7,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/add/id/20'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/20'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/22'),
			(10,'proposal/proposal/create','{\"fairId\":184, \"ecId\":4, \"send\":1}','calendarm/gantt/complete/id/24');
		";
	}

	private function downSql() {

		return "
			TRUNCATE {{dependences}};

			INSERT INTO {{dependences}} VALUES
			(1,'workspace/addFair','{\"id\":184}','calendarm/gantt/add/id/1'),
			(2,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/1'),
			(3,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/6'),
			(4,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/10'),
			(5,'proposal/proposal/create','{\"fairId\":184, \"ecId\":1, \"send\":1}','calendarm/gantt/complete/id/15'),
			(6,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"save\":1}','calendarm/gantt/add/id/20'),
			(7,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/20'),
			(8,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/22'),
			(9,'proposal/proposal/create','{\"fairId\":184, \"ecId\":2, \"send\":1}','calendarm/gantt/complete/id/24');
		";
	}
}