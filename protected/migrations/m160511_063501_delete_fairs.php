<?php

class m160511_063501_delete_fairs extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getCreateTable(){
		return "
			DELETE FROM {{fair}} WHERE `id` IN (10073, 10074, 4740, 4166, 408, 3007, 1827, 531);
			DELETE FROM {{trfair}} WHERE `trParentId` IN (10073, 10074, 4740, 4166, 408, 3007, 1827, 531);
	    ";
	}
}