<?php

class m150912_131212_create_massmedia extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE {{massmedia}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return '
				CREATE TABLE {{massmedia}} (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(500) DEFAULT NULL,
				  `description` varchar(500) DEFAULT NULL,
				  `imageId` int(11) DEFAULT NULL,
				  `url` varchar(500) DEFAULT NULL,
				  `active` tinyint(4) DEFAULT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;

		';
	}
}