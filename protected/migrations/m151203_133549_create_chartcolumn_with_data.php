<?php

class m151203_133549_create_chartcolumn_with_data extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		$sql = '
			DROP TABLE IF EXISTS {{chartcolumn}};
		';

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function getCreateTable(){
		return "
			DROP TABLE IF EXISTS {{chartcolumn}};

			CREATE TABLE IF NOT EXISTS {{chartcolumn}} (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `statId` int(11) DEFAULT NULL,
			  `header` varchar(200) DEFAULT NULL,
			  `count` varchar(45) DEFAULT NULL,
			  `parent` int(11) DEFAULT NULL,
			  `widgetType` int(11) DEFAULT NULL,
			  `epoch` int(11) DEFAULT NULL,
			  `tableId` int(11) DEFAULT NULL,
			  `color` varchar(45) DEFAULT NULL,
			  `position` varchar(45) DEFAULT NULL,
			  `related` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

			INSERT INTO {{chartcolumn}} VALUES (19,103781,NULL,NULL,NULL,4,2014,4,'#43ade3','1',NULL),(20,103781,'Хозяйства всех категорий',NULL,NULL,NULL,2014,4,'','2',NULL),(21,220728,'',NULL,NULL,4,2014,4,'#43ade3','3',NULL),(22,220728,'Число С/Х организаций',NULL,NULL,NULL,2014,4,NULL,'4',NULL),(23,103783,NULL,NULL,NULL,4,2014,5,'#43ade3','1',0),(24,103783,'Продукция сельского хозяйства',NULL,NULL,NULL,2014,5,NULL,'2',NULL),(25,220731,'Растение- водство',NULL,NULL,NULL,2014,5,'','3',NULL),(26,220731,'',NULL,NULL,5,2014,5,'','4',27),(27,220732,'Животно- водство',NULL,NULL,0,2014,5,NULL,'5',NULL),(28,103784,'',NULL,NULL,4,2014,6,'#43ade3','1',NULL),(29,103784,'Продукция сельского хозяйства',NULL,NULL,NULL,2014,6,NULL,'2',NULL),(30,220735,'Растение- водство',NULL,NULL,0,2014,6,NULL,'3',NULL),(31,220735,'',NULL,NULL,5,2014,6,NULL,'4',32),(32,220736,'Животно- водство',NULL,NULL,NULL,2014,6,NULL,'5',NULL),(33,220769,'Зерно',NULL,NULL,6,2014,7,'#43ade3','1',0),(34,220775,'Сахарная свекла',NULL,NULL,6,2014,7,'#43ade3','2',NULL),(35,220774,'Подсол- нечник',NULL,NULL,6,2014,7,'#43ade3','3',NULL),(36,220770,'Картофель',NULL,NULL,6,2014,7,'#43ade3','4',NULL),(37,220772,'Овощи',NULL,NULL,6,2014,7,'#43ade3','5',NULL),(38,220773,'Фрукты',NULL,NULL,6,2014,7,'#43ade3','6',NULL),(39,220777,'Скот и птица',NULL,NULL,6,2014,8,'#43ade3','1',NULL),(40,220776,'Молоко','',NULL,6,2014,8,'#43ade3','2',NULL),(41,220780,'Яйца',NULL,NULL,6,2014,8,'#43ade3','3',NULL),(42,220779,'Шерсть',NULL,NULL,6,2014,8,'#43ade3','4',NULL),(43,220778,'Мед','',NULL,6,2014,8,'#43ade3','5',NULL),(44,223029,'Площадь С/Х угодий',NULL,NULL,6,2014,10,'#43ade3','1',NULL),(45,220781,'Посевные площади',NULL,NULL,6,2014,10,'#43ade3','2',NULL),(46,223027,'С/Х угодья',NULL,NULL,6,2014,10,'#43ade3','3',NULL),(47,220782,'Минеральные удобрения',NULL,NULL,6,2014,10,'#43ade3','4',NULL),(48,220783,'Органические удобрения',NULL,NULL,6,2014,10,'#43ade3','5',NULL),(49,223025,'Рентабельность растениеводства',NULL,NULL,6,2014,10,'#43ade3','6',NULL),(50,220784,'Крупный рогатый скот',NULL,NULL,6,2014,11,'#43ade3','1',NULL),(51,220787,'Свиньи',NULL,NULL,6,2014,11,'#43ade3','2',NULL),(52,220785,'Овцы и козы',NULL,NULL,6,2014,11,'#43ade3','3',NULL),(53,220788,'Расход кормов',NULL,NULL,6,2014,11,'#43ade3','4',NULL),(54,223026,'Рентабель- ность',NULL,NULL,6,2014,11,'#43ade3','5',NULL),(55,223019,NULL,NULL,NULL,4,2014,9,'#43ade3','1',NULL),(56,223019,'Экспорт',NULL,NULL,NULL,2014,9,NULL,'2',NULL),(57,223021,NULL,NULL,NULL,4,2014,9,'#43ade3','3',NULL),(58,223021,'Импорт',NULL,NULL,NULL,2014,9,'#43ade3','4',NULL),(59,221431,'','',NULL,4,2014,21,'#4ec882','1',NULL),(60,221431,'221431','',NULL,NULL,2014,21,'#4ec882','2',NULL),(61,221430,'','',NULL,4,2014,21,'#42c0b4','3',NULL),(62,221430,'221430','',NULL,NULL,2014,21,'#a0d468','4',NULL),(63,221432,'221432','',NULL,4,2014,15,'#43ade3','1',NULL),(64,221432,'221432','',NULL,NULL,2014,15,'#a0d468','2',NULL),(65,221433,'221433','',NULL,NULL,2014,15,'#a0d468','3',NULL),(66,221434,'221434','',NULL,NULL,2014,15,'#a0d468','4',NULL),(67,221434,'','',NULL,5,2014,15,'#a0d468','5',NULL),(68,70294,'???','',NULL,NULL,2014,15,'#a0d468','6',NULL),(69,221484,'','',NULL,4,2014,16,'#4ec882','1',NULL),(70,221484,'221484','',NULL,NULL,2014,16,'#a0d468','2',NULL),(71,221449,'','',NULL,4,2014,16,'#2b98d5','3',NULL),(72,221449,'221449','',NULL,NULL,2014,16,'#a0d468','4',NULL),(73,221501,'','',NULL,4,2014,17,'#4ec882','1',NULL),(74,221501,'221501','',NULL,NULL,2014,17,'#a0d468','2',NULL),(75,221500,'','',NULL,4,2014,17,'#2b98d5','3',NULL),(76,221500,'221500','',NULL,NULL,2014,17,'#a0d468','4',NULL),(77,220815,'','',NULL,4,2014,18,'#2b98d5','1',NULL),(78,220815,'220815','',NULL,NULL,2014,18,'#a0d468','2',NULL),(79,103707,'103707','',NULL,NULL,2014,18,'#a0d468','3',NULL),(80,220816,'220816','',NULL,NULL,2014,18,'#a0d468','4',NULL),(81,220816,'','',NULL,5,2014,18,'#a0d468','5',NULL),(82,70294,'???','',NULL,NULL,2014,18,'#a0d468','6',NULL),(83,140517,'','',NULL,4,2014,19,'#4ec882','1',NULL),(84,140517,'140517','',NULL,NULL,2014,19,'#a0d468','2',NULL),(85,220831,'','',NULL,4,2014,19,'#43ade3','3',NULL),(86,220831,'220831','',NULL,NULL,2014,19,'#a0d468','4',NULL),(87,220840,'','',NULL,4,2014,20,'#4ec882','1',NULL),(88,220840,'220840','',NULL,NULL,2014,20,'#a0d468','2',NULL),(89,220841,'','',NULL,4,2014,20,'#43ade3','3',NULL),(90,220841,'220841','',NULL,NULL,2014,20,'#a0d468','4',NULL),(91,221429,'','',NULL,4,2014,21,'#2b98d5','5',NULL),(92,221429,'221429','',NULL,NULL,2014,21,'#a0d468','6',NULL);
		";
	}
}