<?php

class m160219_092842_create_gradoteka_tables_withChartTable_and_ChartColumn extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getCreateTable();

		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down(){
		return true;
	}

	public function getCreateTable(){
		return '
			DROP TABLE IF EXISTS {{gvalue}};
			DROP TABLE IF EXISTS {{gobjectshasgstattypes}};
			DROP TABLE IF EXISTS {{gobjects}};
			DROP TABLE IF EXISTS {{gstattypes}};
			DROP TABLE IF EXISTS {{chartcolumn}};
			DROP TABLE IF EXISTS {{charttable}};
		'.file_get_contents(Yii::getPathOfAlias('app').'/data/gradoteka_tables_with_charttablesAndChartcolumns.sql');
	}
}