<?php

class m160711_105533_update_10_proposal extends CDbMigration
{
	/**
	 * @return bool
	 * @throws CDbException
	 */
	public function up()
	{
		$sql  = $this->getAlterTable();
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			Yii::app()->db->createCommand($sql)->execute();
			$transaction->commit();
		}
		catch(Exception $e)
		{
			$transaction->rollback();

			echo $e->getMessage();

			return false;
		}

		return true;
	}

	public function down()
	{
		return true;
	}

	public function getAlterTable(){
		return "			
			UPDATE {{trattributeclass}} SET `label`='Аренда оборудования для презентаций' WHERE `id`='1446';
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2148', 'labelYes');
			INSERT INTO {{attributeclassproperty}} (`attributeClass`, `class`) VALUES ('2148', 'labelNo');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2186', 'ru', 'разрешено');
			INSERT INTO {{trattributeclassproperty}} (`trParentId`, `langId`, `value`) VALUES ('2187', 'ru', 'запрещено');
		";
	}
}