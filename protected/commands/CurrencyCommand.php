<?php

class CurrencyCommand extends CConsoleCommand{

    const CURRENCY_LAYER_SITE = 'http://apilayer.net/api/live?access_key=47c745418a606d21fcad268092eed34a';
    const SOURCE_PARAM = '&source=';
    const CURRENCIES_PARAM = '&currencies=';

    public function actionUpdateCurrencyCourses(){
        $sourceCurrencies = Currency::model()->findAll();

        /**
         * @var Currency $sourceCurrency
         */
        foreach($sourceCurrencies as $sourceCurrency){
            $criteria = new CDbCriteria();
            $criteria->addNotInCondition('id', array($sourceCurrency->id));

            $targetCurrencies = Currency::model()->findAll($criteria);
            $targetCurrenciesCodes = array_keys(CHtml::listData($targetCurrencies, 'code', 'code'));

            $currencyCourseValues = $this->getRemoteCurrencyCourses($sourceCurrency->code, $targetCurrenciesCodes);

            if(empty($currencyCourseValues['quotes'])){
                continue;
            }
            $currencyCourseValuesArray = $currencyCourseValues['quotes'];

            /**
             * @var Currency $targetCurrency
             */
            foreach($targetCurrencies as $targetCurrency){

                if(empty($currencyCourseValuesArray[$sourceCurrency->code.$targetCurrency->code])){
                    continue;
                }

                $currencyCourse = CurrencyCourse::model()->findByAttributes(array("source" => $sourceCurrency->id, "target" => $targetCurrency->id));

                if(empty($currencyCourse)){
                    $currencyCourse = new CurrencyCourse();
                }

                $currencyCourse->source = $sourceCurrency->id;
                $currencyCourse->target = $targetCurrency->id;
                $currencyCourse->value = $currencyCourseValuesArray[$sourceCurrency->code.$targetCurrency->code];
                $currencyCourse->datetime = date('Y-m-d H:i:s');

                $currencyCourse->save();
            }
        }
    }

    public function getRemoteCurrencyCourses($currencyCode, $targetCurrenciesCodes){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => self::CURRENCY_LAYER_SITE.self::SOURCE_PARAM.$currencyCode.self::CURRENCIES_PARAM.implode(',', $targetCurrenciesCodes),
        ));

        $response = json_decode(curl_exec($curl), TRUE);

        curl_close($curl);

        return $response;
    }
}