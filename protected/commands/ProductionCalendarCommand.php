<?php
/**
 * Created by PhpStorm.
 * User: HolyDemon
 * Date: 07.10.2015
 * Time: 16:13
 */
class ProductionCalendarCommand extends CConsoleCommand
{
    public $year, $data, $region, $url, $status, $currentYear;
    public $language = 'ru';
    private $_regions, $_holidays, $_days;

    public function setStartData()
    {
        if($this->currentYear != $this->year) {
            $this->url = 'http://xmlcalendar.ru/data/'.$this->language.'/'.$this->year.'/calendar.xml';
        } else {
            $this->url = 'http://xmlcalendar.ru/data/'.$this->language.'/reg/'.$this->year.'/calendar.xml';
        }
        $headers = get_headers($this->url);
        if($headers && isset($headers[0])) {
            $this->status = stristr($headers[0], '200');
        }
        if($this->status) {
            $this->data = file_get_contents($this->url);
            $this->data = str_replace(["\n", "\r"], '', $this->data);
        }
    }

    public function actionIndex($year = null)
    {
        $this->currentYear = date('Y');
        $this->year = $year !== null ? $year : $this->currentYear;
        $this->setStartData();
        if($this->status) {
            $this->getData();
            $this->saveRegions();
            $this->saveHolidays();
            $this->saveDays();
            echo 'Работа выполнена успешно.';
        } else {
            echo 'Произошла ошибка. Данные для '.$this->year.' года не найдены.';
        }
    }

    public function saveHolidays()
    {
        if(!empty($this->_holidays)) {
            foreach ($this->_holidays as $holidayKey => $holidayData) {
                $model = ProductionCalendar::model()->findByAttributes(['holidayId' => $holidayKey, 'year' => $this->year]);
                if(null === $model) {
                    $model = new ProductionCalendar();
                }
                $model->holidayId = $holidayKey;
                $model->year = $this->year;
                $model->holiday = $holidayData['name'];
                if($model->save()) {
                    if(isset($holidayData['data']) && !empty($holidayData['data'])) {
                        foreach ($holidayData['data'] as $day => $data) {
                            $dayModel = ProductionCalendarDay::model()->findByAttributes(['productionCalendarId' => $model->id, 'day' => $this->formatDate($day)]);
                            if(null === $dayModel) {
                                $dayModel = new ProductionCalendarDay();
                            }
                            $dayModel->productionCalendarId = $model->id;
                            $dayModel->type = isset($data['type']) ? $data['type'] : '';
                            $dayModel->region = isset($data['reg']) ? $data['reg'] : '';
                            $dayModel->day = $this->formatDate($day);
                            $dayModel->save();
                        }
                    }
                }
            }
        }
    }

    public function saveRegions()
    {
        if(!empty($this->_regions)) {
            foreach ($this->_regions as $key => $name) {
                $model = ProductionCalendarRegion::model()->findByAttributes(['regionId' => $key]);
                if(null === $model){
                    $model = new ProductionCalendarRegion();
                }
                $model->regionId = $key;
                $model->name = $name;
                $model->save();
            }
        }
    }

    public function saveDays()
    {
        if(!empty($this->_days)) {
            foreach ($this->_days as $day => $data) {
                $dayModel = ProductionCalendarDay::model()->findByAttributes(['productionCalendarId' => -1, 'day' => $this->formatDate($day)]);
                if(null === $dayModel) {
                    $dayModel = new ProductionCalendarDay();
                }
                $dayModel->productionCalendarId = -1;
                $dayModel->type = isset($data['type']) ? $data['type'] : '';
                $dayModel->region = isset($data['reg']) ? $data['reg'] : '';
                $dayModel->day = $this->formatDate($day);
                $dayModel->save();
            }
        }
    }

    public function formatDate($day)
    {
        $result = null;
        if(!empty($day)) {
            $data = explode('.', $day);
            $result = date('Y-m-d', strtotime($data[1].'.'.$data[0].'.'.$this->year));
        }
        return $result;
    }

    public function getRegions()
    {
        $this->_regions = [];
        preg_match('#<regions>\s+(<region .+?[^<]/>)\s+</regions>#', $this->data, $regions);
        if(isset($regions, $regions[1])) {
            $regions = preg_replace('#/>\s+<#', '##', $regions[1]);
            $regions = explode('##', $regions);
            foreach ($regions as $region) {
                preg_match('#id="(.*)"\s?title="(.*)"#', $region, $regionData);
                if(isset($regionData, $regionData[1], $regionData[2])) {
                    $this->_regions[$regionData[1]] = $regionData[2];

                }
            }
        }
    }

    public function getHolidays()
    {
        $this->_holidays = [];
        preg_match('#<holidays>\s+(<holiday .+?[^<]/>)\s+</holidays>#', $this->data, $holidays);
        if(isset($holidays, $holidays[1])) {
            $holidays = preg_replace('#/>\s+<#', '##', $holidays[1]);
            $holidays = explode('##', $holidays);
            foreach ($holidays as $holiday) {
                preg_match('#id="(.*)"\s?title="(.*)"#', $holiday, $holidayData);
                if(isset($holidayData, $holidayData[1], $holidayData[2])) {
                    $this->_holidays[$holidayData[1]]['name'] = $holidayData[2];

                }
            }
        }
    }

    public function getData()
    {
        $this->getHolidays();
        $this->getRegions();
        $this->_days = [];
        preg_match('#<days>\s+(<day .+?[^<]/>)\s+</days>#', $this->data, $days);
        if(isset($days, $days[1])) {
            $days = preg_replace('#/>\s+<#', '##', $days[1]);
            $days = explode('##', $days);
            foreach ($days as $day) {
                preg_match('#h="(\d?[,\d+])+"#', $day, $h);
                preg_match('#d="(\d+\.\d+)"#', $day, $d);
                $oneDay = '';
                if(isset($d, $d[1])) {
                    $oneDay = $d[1];
                }
                preg_match('#t="(\d?[,\d+])+"#', $day, $t);
                $type = '';
                if(isset($t, $t[1])) {
                    $type = $t[1];
                }
                preg_match('#r="(\d?[,\d+])+"#', $day, $r);
                $reg = '';
                if(isset($r, $r[1])) {
                    $reg = $r[1];
                }
                if(isset($h, $h[1], $this->_holidays[$h[1]])) {
                    $this->_holidays[$h[1]]['data'][$oneDay] = [
                        'type' => $type,
                        'reg' => $reg
                    ];
                } else {
                    $this->_days[$oneDay] = [
                        'type' => $type,
                        'reg' => $reg
                    ];
                }
            }
        }
    }
}