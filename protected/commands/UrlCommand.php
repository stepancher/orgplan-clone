<?php

class UrlCommand extends CConsoleCommand {

    public function actionCreateBlog(){

        Yii::import('application.modules.blog.models.Blog');

        echo "\nCreate URL's from Blog articles...";

        $updated = 0;
        $skipped = 0;
        $errors  = [];
        foreach (Blog::model()->findAll() as $blog){
            if(!empty($blog->shortUrl)){
                $skipped++;
                continue;
            }

            $blog->shortUrl = $blog->createShortUrl();
            if($blog->save()){
                $updated++;
            }else{
                $errors[] = $blog->getErrors();
            }
        }

        echo "\n\tSkipped:{$skipped}";
        echo "\n\tUpdated:{$updated}";
        echo "\n\tErrors :".count($errors);

        if(count($errors)){
            echo "\n\n";
            var_dump($errors);
            echo "\n";
        }

        echo "\nDone.\n\n";
    }

}