<?php
Yii::import('application.components.ParserExcel');

class CurrentCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        $curDate = date('d/m/Y');
        $link = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='.$curDate;
        $content = file_get_contents($link);
        /** @var SimpleXMLElement $xml */
        $xml = simplexml_load_string($content);
        $usd = $xml->xpath('Valute[@ID="R01235"]')[0];
        $eur = $xml->xpath('Valute[@ID="R01239"]')[0];
        $pound = $xml->xpath('Valute[@ID="R01035"]')[0];
        $grivna = $xml->xpath('Valute[@ID="R01720"]')[0];
        $cny = $xml->xpath('Valute[@ID="R01375"]')[0];

        $result = [];
        $result['usd'] = [
            'id' => 2,
            'value' => str_replace(',','.',$usd->Value)
        ];
        $result['eur'] = [
            'id' => 3,
            'value' => str_replace(',','.',$eur->Value)
        ];
        $result['pound'] = [
            'id' => 8,
            'value' => str_replace(',','.',$pound->Value)
        ];
        $result['grivna'] = [
            'id' => 7,
            'value' => str_replace(',','.',$grivna->Value)
        ];
        $result['cny'] = [
            'id' => 6,
            'value' => str_replace(',','.',$cny->Value)
        ];

        foreach($result as $val) {
            $currencyModel = Currency::model()->findByPk($val['id']);
            if(null != $currencyModel) {
                $currencyModel->course = $val['value'];
                $currencyModel->update('course');
            }
        }

    }
}