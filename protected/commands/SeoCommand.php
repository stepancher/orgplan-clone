<?php

class SeoCommand extends CConsoleCommand
{
	public $baseUrl;

	public function actionGetSeoFairs()
	{
		$fairs = Fair::model()->findAll();
		if (!empty($fairs)) {
			$route = 'fair/default/view';
			foreach ($fairs as $fair) {
				$params = [
					'id' => $fair->id
				];

				$seo = Seo::model()->findByAttributes([
					'route' => $route,
					'params' => json_encode($params),
				]);
				if (null !== $seo) {
					$seo->title = null;
					$seo->header = null;
					$seo->description = null;
					$seo->save(false);
				}
				$this->createUrl($route, $params);
			}
		}
	}

	public function actionGetSeoExhibitionComplexes()
	{
		$exComplexes = ExhibitionComplex::model()->findAll();
		if (!empty($exComplexes)) {
			$route = 'exhibitionComplex/default/view';
			foreach ($exComplexes as $exComplex) {
				$params = [
					'id' => $exComplex->id
				];

				$seo = Seo::model()->findByAttributes([
					'route' => $route,
					'params' => json_encode($params),
				]);
				if (null !== $seo) {
					$seo->title = null;
					$seo->header = null;
					$seo->description = null;
					$seo->save(false);
				}
				$this->createUrl($route, $params);
			}
		}
	}

	/**
	 * получения базового пути
	 * @return CUrlManager
	 */
	public function getUrlManager()
	{
		$config = require(Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'config/web.php');
		Yii::app()->setComponent(
			'urlManager',
			array_merge(
				$config['components']['urlManager'],
				array(
					'baseUrl' => $this->baseUrl
				)
			)
		);
		return Yii::app()->urlManager;
	}

	/**
	 * Создание пути
	 * @param $route
	 * @param array $params
	 * @return string
	 */
	public function createUrl($route, $params = array())
	{
		return $this->getUrlManager()->createUrl($route, $params);
	}

	public function actionClear(){

		$result =  "\n Deleting Seo: ";

		$affected = Seo::model()->deleteAll();

		$result .=  "{$affected} rows.\n\n";

		echo $result;
	}
}