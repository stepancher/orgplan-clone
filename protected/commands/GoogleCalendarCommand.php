<?php

/**
 * Created by PhpStorm.
 * User: user93
 * Date: 12.10.15
 * Time: 12:01
 */
class GoogleCalendarCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        // Action for all task all user sync
    }

    public function actionSync($userId, $fairId=null)
    {
        $calendarId = 'primary';

        Yii::trace('Start sync for userId ' . $userId);
        Yii::trace('Start sync for fairId ' . $fairId);
        Yii::trace('Active calendar ' . $calendarId);

        $user = User::model()->findByPk($userId);
        if (!$user) {
            exit;
        }

        $client = new Google_Client();
        $client->setAccessToken($user->googleAPI);

        $service = new Google_Service_Calendar($client);

        /*$tasks = Task::findAllByUser($userId, $fairId);*/

        /*Yii::trace('Found ' . sizeof($tasks) . ' tasks');*/

        /*foreach ($tasks as $task) {
            // Print the next 10 events on the user's calendar.
            $results = $service->events->listEvents($calendarId, [
                'maxResults' => 10,
                'q' => $task->name
            ]);

            Yii::trace('Search ' . $task->name);

            $exist = false;
            $events = $results->getItems();
            foreach ($events as $event) {
                if (date('Y-m-d', strtotime($task->startDate)) == $event->getStart()->date) {
                    $exist = true;
                    break;
                }
            }

            Yii::trace('Result: ' . ($exist ? 'true' : 'false'));

            if (!$exist) {
                $event = new Google_Service_Calendar_Event(array(
                    'summary' => $task->name,
                    'description' => $task->description,
                    'start' => array(
                        'date' => date('Y-m-d', strtotime($task->startDate)),
                    ),
                    'end' => array(
                        'date' => date('Y-m-d', strtotime($task->endDate)),
                    ),
                ));

                $insert = $service->events->insert($calendarId, $event);

                Yii::trace('Insert id: ' . $insert->id);

            }
        }*/
    }
}