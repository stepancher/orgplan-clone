<?php

class CacheCommand extends CConsoleCommand
{
    public function actionFlush(){

        Yii::app()->cache->flush();

        echo "\n";
        echo "Done \n\n";
    }
}