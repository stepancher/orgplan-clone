<?php
/**
 */

/**
 * Class SiteMapCommand
 */
class SiteMapCommand extends CConsoleCommand
{
    public $defaultAction = 'create';
    public $urlPrefix = 'http://orgplan.pro';
    public $langs = array(
        'ru',
        'en',
        'de',
    );
    public $sites = array(
        'sitemap-fair',
        'sitemap-venue',
        'sitemap-price',
        'sitemap-article',
        'sitemap-help',
        'sitemap-catalog',
        'sitemap-auth',
        'sitemap-region',
    );

    public $iterator = 1;
    public $nextFileVersion = 2;

    const INDEX_FILE_NAME = 'sitemap.xml';
    const DIR_NAME = 'sitemap';
    const URL_PREFIX = 'https://protoplan.pro';
    const XML_POSTFIX = '.xml';
    const STATUS_POSTED = 2;
    const REGION_LIMIT = 100;
    const SITEMAP_URL_LIMIT = 50000;

    const FAIR_PRIORITY = 0;
    const FAIR_CHANGE_FREQ = '';

    const VENUE_PRIORITY = 0;
    const VENUE_CHANGE_FREQ = '';

    const PRICES_PRIORITY = 0;
    const PRICES_CHANGE_FREQ = '';

    const ARTICLE_PRIORITY = 0;
    const ARTICLE_CHANGE_FREQ = '';

    const HELP_PRIORITY = 0;
    const HELP_CHANGE_FREQ = '';

    const CATALOG_PRIORITY = 0;
    const CATALOG_CHANGE_FREQ = '';

    const AUTH_PRIORITY = 0;
    const AUTH_CHANGE_FREQ = '';

    const REGION_PRIORITY = 0;
    const REGION_CHANGE_FREQ = '';

    private $_cache = [];
    private $_cacheUrl = [];

    public function siteMapIndex(){

        if(file_exists($this->dirPath() . self::INDEX_FILE_NAME)){
            unlink($this->dirPath() . self::INDEX_FILE_NAME);
        }

        $file = $this->dirPath() . self::INDEX_FILE_NAME;
        file_put_contents($file, '');
        $this->write($file, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

        foreach ($this->sites as $site){
            $this->write($file, '<sitemap>');
            $this->write($file, "<loc>".self::URL_PREFIX . DIRECTORY_SEPARATOR . self::DIR_NAME . DIRECTORY_SEPARATOR . $site . self::XML_POSTFIX . "</loc>");
            $this->write($file, '</sitemap>');
        }

        $this->write($file, '</sitemapindex>');
    }
    
    public function baseUrl($lang){
        return self::URL_PREFIX . DIRECTORY_SEPARATOR . $lang . DIRECTORY_SEPARATOR;
    }

    public function dirPath(){
        return Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . self::DIR_NAME . DIRECTORY_SEPARATOR;
    }

    public function writeWithIteration($site, &$file, $url, $changeFreq = '', $priority = 0, $lastModify = '')
    {
        if($this->iterator > self::SITEMAP_URL_LIMIT){
            $this->write($file, '</urlset>');
            $file = $this->dirPath() . $site . $this->nextFileVersion . self::XML_POSTFIX;
            $this->sites[] = $site . $this->nextFileVersion;
            $this->nextFileVersion++;
            $this->iterator = 1;
            file_put_contents($file, '');
            $this->write($file, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
        }

        file_put_contents($file, '<url><loc>', FILE_APPEND);
        file_put_contents($file, $url, FILE_APPEND);
        file_put_contents($file, '</loc>', FILE_APPEND);

        if(!empty($lastModify))
            file_put_contents($file, "<lastmod>".$lastModify."</lastmod>", FILE_APPEND);

        if(!empty($changeFreq))
            file_put_contents($file, "<changefreq>".$changeFreq."</changefreq>", FILE_APPEND);

        if(!empty($priority))
            file_put_contents($file, "<priority>".$priority."</priority>", FILE_APPEND);

        file_put_contents($file, '</url>' . "\r\n", FILE_APPEND);

        $this->iterator++;
    }

    public function prepareDir(){

        $dir = $this->dirPath();

        if(is_dir($dir)){
            $files = array_filter(
                scandir($dir),
                function ($file){
                    if($file == '.' || $file == '..')
                        return FALSE;
                    if(array_search(str_replace(self::XML_POSTFIX, '', $file), $this->sites) === FALSE)
                        return TRUE;
                    return FALSE;
                }
            );

            foreach ($files as $file)
                if(file_exists($this->dirPath() . $file) && strpos($file, 'sitemap-') !== FALSE)
                    unlink($this->dirPath() . $file);
        } else {
            mkdir($dir);
        }
    }

    public function actionCreate(){

        $this->prepareDir();

        foreach ($this->sites as $site){

            if(file_exists($this->dirPath() . $site . self::XML_POSTFIX))
                unlink($this->dirPath() . $site . self::XML_POSTFIX);

            $file = $this->dirPath() . $site . self::XML_POSTFIX;
            file_put_contents($file, '');

            $this->write($file, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

            $this->iterator = 1;
            $this->nextFileVersion = 2;

            if($site == 'sitemap-fair'){

                foreach (TrFair::model()->findAllBySql('SELECT * FROM tbl_trfair WHERE shortUrl IS NOT NULL ORDER BY langId') as $model){
                    $url = $this->baseUrl($model->langId) . "fair" . DIRECTORY_SEPARATOR . $model->shortUrl;
                    $this->writeWithIteration($site, $file, $url, self::FAIR_CHANGE_FREQ, self::FAIR_PRIORITY);
                }

            } elseif ($site == 'sitemap-venue'){

                foreach (TrExhibitionComplex::model()->findAllBySql('SELECT * FROM tbl_trexhibitioncomplex WHERE shortUrl IS NOT NULL ORDER BY langId') as $model){
                    $url = $this->baseUrl($model->langId) . "venue" . DIRECTORY_SEPARATOR . $model->shortUrl;
                    $this->writeWithIteration($site, $file, $url, self::VENUE_CHANGE_FREQ, self::VENUE_PRIORITY);
                }
                
            } elseif ($site == 'sitemap-price'){

                $query = "
                    SELECT DISTINCT c.shortUrl, trc.langId FROM tbl_productprice pp
                        LEFT JOIN tbl_city c ON c.id = pp.cityId
                        LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id
                    ORDER BY trc.langId, c.shortUrl
                ";
                $prices = Yii::app()->db->createCommand($query)->queryAll();

                foreach ($prices as $model){
                    $url = $this->baseUrl($model['langId']) . "prices" . DIRECTORY_SEPARATOR . $model['shortUrl'];
                    $this->writeWithIteration($site, $file, $url, self::PRICES_CHANGE_FREQ, self::PRICES_PRIORITY);
                }

            } elseif($site == 'sitemap-article'){

                $query = "SELECT DISTINCT b.shortUrl FROM tbl_blog b WHERE b.status = :status ORDER BY b.shortUrl";
                $blogs = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':status' => self::STATUS_POSTED));

                $query = "SELECT DISTINCT t.shortUrl FROM tbl_tag t WHERE t.shortUrl IS NOT NULL ORDER BY t.shortUrl";
                $tags = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':status' => self::STATUS_POSTED));

                foreach ($this->langs as $lang){

                    foreach ($blogs as $model){
                        $url = $this->baseUrl($lang) . "blog" . DIRECTORY_SEPARATOR . $model['shortUrl'];
                        $this->writeWithIteration($site, $file, $url, self::ARTICLE_CHANGE_FREQ, self::ARTICLE_PRIORITY);
                    }

                    foreach ($tags as $model){
                        $url = $this->baseUrl($lang) . "blog" . DIRECTORY_SEPARATOR . $model['shortUrl'];
                        $this->writeWithIteration($site, $file, $url, self::ARTICLE_CHANGE_FREQ, self::ARTICLE_PRIORITY);
                    }
                }
            } elseif ($site == 'sitemap-help'){

                foreach ($this->langs as $lang){
                    $url = $this->baseUrl($lang) . "help" . DIRECTORY_SEPARATOR . "termsOfUse";
                    $this->writeWithIteration($site, $file, $url, self::HELP_CHANGE_FREQ, self::HELP_PRIORITY);
                }

                foreach ($this->langs as $lang){
                    $url = $this->baseUrl($lang) . "help" . DIRECTORY_SEPARATOR . "confidentiality";
                    $this->writeWithIteration($site, $file, $url, self::HELP_CHANGE_FREQ, self::HELP_PRIORITY);
                }

            } elseif ($site == 'sitemap-catalog'){

                /** Каталоги */
                foreach ($this->langs as $lang){
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "industriesList", self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "venues", self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "industries", self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "regions", self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "prices", self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                }

                /** Выбор отрасли в каталоге выставок */
                foreach ($this->langs as $lang){
                    $query = 'SELECT DISTINCT i.id, tri.shortNameUrl FROM tbl_industry i
                                LEFT JOIN tbl_fairhasindustry fhi ON fhi.industryId = i.id
                                LEFT JOIN tbl_trindustry tri ON i.id = tri.trParentId AND tri.langId = :langId 
                                LEFT JOIN tbl_fair f ON f.id = fhi.fairId
                            WHERE f.active = :active AND YEAR(f.beginDate) >= YEAR(NOW()) ORDER BY i.id';

                    $industries = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':active' => Fair::ACTIVE_ON, ':langId' => $lang));

                    foreach ($industries as $industry){
                        $url = $this->baseUrl($lang) . "catalog" . DIRECTORY_SEPARATOR . "industry" . DIRECTORY_SEPARATOR . $industry['shortNameUrl'];
                        $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    }
                }

                foreach ($this->langs as $lang){

                    $countryQuery = 'SELECT DISTINCT cntry.id, cntry.shortUrl FROM tbl_country cntry
                                        LEFT JOIN tbl_district d ON d.countryId = cntry.id
                                        LEFT JOIN tbl_region r ON r.districtId = d.id
                                        LEFT JOIN tbl_city c ON c.regionId = r.id
                                        LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                                    WHERE ec.id IS NOT NULL ORDER BY cntry.id';

                    /** Выбор страны в каталоге площадок */
                    foreach (Yii::app()->db->createCommand($countryQuery)->queryAll() as $country){
                        $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'];
                        $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                        $regionQuery = 'SELECT DISTINCT r.id, trr.shortUrl FROM tbl_region r
                                            LEFT JOIN tbl_trregion trr ON trr.id = r.id AND trr.langId = :langId 
                                            LEFT JOIN tbl_district d ON d.id = r.districtId
                                            LEFT JOIN tbl_city c ON c.regionId = r.id
                                            LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                                        WHERE d.countryId = :countryId AND r.active = 1 AND ec.id IS NOT NULL ORDER BY r.id';

                        /** Выбор страны, затем выбор региона в каталоге площадок */
                        foreach (Yii::app()->db->createCommand($regionQuery)->queryAll(TRUE, array(':countryId' => $country['id'], ':langId' => $lang)) as $region){
                            $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'];
                            $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                            $cityQuery = 'SELECT DISTINCT c.id FROM tbl_city c
                                                LEFT JOIN tbl_region r ON r.id = c.regionId
                                                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                                            WHERE c.regionId = :regionId AND ec.id IS NOT NULL ORDER BY c.id';

                            /** Выбор страны, затем выбор региона, затем выбор города в каталоге площадок */
                            foreach (Yii::app()->db->createCommand($cityQuery)->queryAll(TRUE, array(':regionId' => $region['id'])) as $city){
                                $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'] . DIRECTORY_SEPARATOR . "city" . DIRECTORY_SEPARATOR . $city['id'];
                                $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                                /** Выбор станы, затем выбор региона, затем выбор города, затем выбор категории в каталоге площадок */
                                foreach (Yii::app()->db->createCommand('SELECT DISTINCT ect.id FROM tbl_exhibitioncomplextype ect ORDER BY ect.id')->queryAll() as $category){
                                    $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'] . DIRECTORY_SEPARATOR . "city" . DIRECTORY_SEPARATOR . $city['id'] . DIRECTORY_SEPARATOR . "category" . DIRECTORY_SEPARATOR . $category['id'];
                                    $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                                }
                            }

                            /** Выбор станы, затем выбор региона, затем выбор категории в каталоге площадок */
                            foreach (Yii::app()->db->createCommand('SELECT DISTINCT ect.id FROM tbl_exhibitioncomplextype ect ORDER BY ect.id')->queryAll() as $category){
                                $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'] . DIRECTORY_SEPARATOR . "category" . DIRECTORY_SEPARATOR . $category['id'];
                                $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                            }
                        }

                        $cityQuery = 'SELECT DISTINCT c.id FROM tbl_city c
                                                LEFT JOIN tbl_region r ON r.id = c.regionId
                                                LEFT JOIN tbl_district d ON r.districtId = d.id
                                                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                                            WHERE d.countryId = :countryId AND ec.id IS NOT NULL ORDER BY c.id';

                        /** Выбор страны, затем выбор города в каталоге площадок */
                        foreach (Yii::app()->db->createCommand($cityQuery)->queryAll(TRUE, array(':countryId' => $country['id'])) as $city){
                            $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "city" . DIRECTORY_SEPARATOR . $city['id'];
                            $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                            /** Выбор станы, затем выбор города, затем выбор категории в каталоге площадок */
                            foreach (Yii::app()->db->createCommand('SELECT DISTINCT ect.id FROM tbl_exhibitioncomplextype ect ORDER BY ect.id')->queryAll() as $category){
                                $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "city" . DIRECTORY_SEPARATOR . $city['id'] . DIRECTORY_SEPARATOR . "category" . DIRECTORY_SEPARATOR . $category['id'];
                                $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                            }
                        }

                        /** Выбор станы, затем выбор категории в каталоге площадок */
                        foreach (Yii::app()->db->createCommand('SELECT DISTINCT ect.id FROM tbl_exhibitioncomplextype ect ORDER BY ect.id')->queryAll() as $category){
                            $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $country['id'] . DIRECTORY_SEPARATOR . "category" . DIRECTORY_SEPARATOR . $category['id'];
                            $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                        }
                    }
                }

                /** Выбор региона в каталоге площадок */
                foreach ($this->langs as $lang){
                    $query = 'SELECT DISTINCT r.id FROM tbl_region r LEFT JOIN tbl_city c ON c.regionId = r.id LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                                        WHERE r.active = :active AND ec.id IS NOT NULL ORDER BY r.id';

                    foreach (Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':active' => Region::ACTIVE_ON)) as $region){
                        $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'];
                        $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                        $cityQuery = 'SELECT DISTINCT c.id FROM tbl_city c
                                                LEFT JOIN tbl_region r ON r.id = c.regionId
                                                LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id
                                            WHERE c.regionId = :regionId AND ec.id IS NOT NULL ORDER BY c.id';

                        /** Выбор региона, затем выбор города в каталоге площадок */
                        foreach (Yii::app()->db->createCommand($cityQuery)->queryAll(TRUE, array(':regionId' => $region['id'])) as $city){
                            $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'] . DIRECTORY_SEPARATOR . "city" . $city['id'];
                            $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                            /** Выбор региона, затем выбор города, затем выбор категории в каталоге площадок */
                            foreach (Yii::app()->db->createCommand('SELECT DISTINCT ect.id FROM tbl_exhibitioncomplextype ect ORDER BY ect.id')->queryAll() as $category){
                                $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['id'] . DIRECTORY_SEPARATOR . "city" . DIRECTORY_SEPARATOR . $city['id'] . DIRECTORY_SEPARATOR . "category" . DIRECTORY_SEPARATOR . $category['id'];
                                $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                            }
                        }
                    }
                }

                /** Выбор города в каталоге площадок */
                foreach ($this->langs as $lang){
                    $query = 'SELECT DISTINCT c.id FROM tbl_city c LEFT JOIN tbl_exhibitioncomplex ec ON ec.cityId = c.id WHERE ec.id IS NOT NULL ORDER BY c.id';

                    foreach (Yii::app()->db->createCommand($query)->queryAll() as $city){
                        $url = $this->baseUrl($lang) . "venues" . DIRECTORY_SEPARATOR . "city" . DIRECTORY_SEPARATOR . $city['id'];
                        $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    }
                }

                /** Выбор категории в каталоге площадок */
                foreach ($this->langs as $lang){
                    foreach (Yii::app()->db->createCommand('SELECT DISTINCT ect.id FROM tbl_exhibitioncomplextype ect ORDER BY ect.id')->queryAll() as $category){
                        $url = $this->baseUrl($lang) ."venues" . DIRECTORY_SEPARATOR . "category" . DIRECTORY_SEPARATOR . $category['id'];
                        $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                    }
                }

                /** Каталог выставок */
                foreach (TrCountry::model()->findAllBySql('SELECT DISTINCT trcn.trParentId AS id, trcn.langId FROM tbl_trcountry trcn ORDER BY trcn.langId, trcn.trParentId') as $country){
                    $countryParent = Country::model()->findByPk($country->id);

                    $countryShortUrl = $country['id'];

                    if ($countryParent != null) {
                        $countryShortUrl = $countryParent->shortUrl;
                    }

                    foreach (Fair::getFairYears() as $year){
                        $url = $this->baseUrl($country['langId']) . "catalog" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $countryShortUrl . DIRECTORY_SEPARATOR . "year" . DIRECTORY_SEPARATOR . $year;
                        $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);

                        $regions = TrRegion::model()->findAllBySql(
                            'SELECT DISTINCT trr.trParentId, trr.shortUrl FROM tbl_trregion trr 
                            LEFT JOIN tbl_region r ON trr.trParentId = r.id
                            LEFT JOIN tbl_district d ON r.districtId = d.id
                            WHERE r.active = :active AND d.countryId = :countryId ORDER BY trr.trParentId LIMIT :limit',
                            array(
                                ':active' => Region::ACTIVE_ON,
                                ':countryId' => $country['id'],
                                ':limit' => self::REGION_LIMIT,
                            )
                        );

                        foreach ($regions as $region){
                            $url = $this->baseUrl($country['langId']) . "catalog" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $countryShortUrl . DIRECTORY_SEPARATOR . "year" . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['shortUrl'];
                            $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                            
                            foreach (TrIndustry::model()->findAllBySql('SELECT DISTINCT tri.trParentId, tri.shortNameUrl FROM tbl_trindustry tri ORDER BY tri.trParentId') as $industry){
                                $url = $this->baseUrl($country['langId']) . "catalog" . DIRECTORY_SEPARATOR . "country" . DIRECTORY_SEPARATOR . $countryShortUrl . DIRECTORY_SEPARATOR . "year" . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . "region" . DIRECTORY_SEPARATOR . $region['shortUrl'] . DIRECTORY_SEPARATOR . "industry" . DIRECTORY_SEPARATOR . $industry['shortNameUrl'];
                                $this->writeWithIteration($site, $file, $url, self::CATALOG_CHANGE_FREQ, self::CATALOG_PRIORITY);
                            }
                        }
                    }
                }
            } elseif ($site == 'sitemap-auth'){
                foreach ($this->langs as $lang){
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "login", self::AUTH_CHANGE_FREQ, self::AUTH_PRIORITY);
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "registration", self::AUTH_CHANGE_FREQ, self::AUTH_PRIORITY);
                    $this->writeWithIteration($site, $file, $this->baseUrl($lang) . "auth" .DIRECTORY_SEPARATOR . "default" . DIRECTORY_SEPARATOR . "recovery", self::AUTH_CHANGE_FREQ, self::AUTH_PRIORITY);
                }
            } elseif ($site == 'sitemap-region'){

                $query = 'SELECT DISTINCT trr.shortUrl, trr.langId
                            FROM tbl_regioninformation ri
                            LEFT JOIN tbl_region r ON r.id = ri.regionId
                            LEFT JOIN tbl_trregion trr ON r.id = trr.trParentId
                            LEFT JOIN tbl_regioninformationhasfile rihf ON rihf.regionInformationId = ri.id
                            LEFT JOIN tbl_objectfile objf ON objf.id = rihf.fileId
                        WHERE ri.active = :active AND objf.type = :fileType ORDER BY trr.langId, trr.shortUrl';

                foreach (Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':active' => RegionInformation::ACTIVE_ON, ':fileType' => ObjectFile::TYPE_REGION_INFORMATION_MAIN_IMAGE)) as $region){
                    $url = $this->baseUrl($region['langId']) . "region" . DIRECTORY_SEPARATOR . $region['shortUrl'];
                    $this->writeWithIteration($site, $file, $url, self::REGION_CHANGE_FREQ, self::REGION_PRIORITY);
                }
            }
            $this->write($file, '</urlset>');
        }
        $this->siteMapIndex();
    }

    public function actionIndex($outPath = '.')
    {
        if (substr($outPath, -1) != DIRECTORY_SEPARATOR) {
            $outPath .= DIRECTORY_SEPARATOR;
        }

        $outFile = 'sitemap.xml';
        $tmpFile = $outPath . DIRECTORY_SEPARATOR . $outFile . '.tmp';

        $routes = [
            'fair/default/view',
            'exhibitionComplex/default/view',
            'regionInformation/default/view',
            'blog/view',
            'blog/index',
            'search/index',
        ];

        $links = Seo::model()->findAll([
            'condition' => 'route IN ("' . implode('","', $routes) . '")',
            'order' => 'route'
        ]);

        // clear
        file_put_contents($tmpFile, '');

        $this->write($tmpFile, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

        for ($i = 0, $ln = sizeof($links); $i < $ln; $i++) {
            $seo = $links[$i];

            if (!empty($seo->url) && !stristr($seo->url, '?')) {

                $params = json_decode($seo->params, true);

                $success = false;

                switch ($seo->route) {
                    case 'search/index':
                    case 'blog/index':
                    case 'fair/default/view':
                    case 'exhibitionComplex/default/view':
                    case 'regionInformation/default/view':
                    case 'blog/view':
                        $success = $this->filterView($seo->route, $params);

                }

                if ($success) {
                    $url = $seo->url;
                    if (substr($url, -1) != '/') {
                        $url .= '/';
                    }
                    if (!in_array($url, $this->_cacheUrl)) {
                        $this->_cacheUrl[] = $url;

                        if (substr($url, -3) != '-1/') {
                            $this->write($tmpFile, '<sitemap>' . '<loc>' . $this->urlPrefix . $url . '</loc>' . '</sitemap>');
                        }
                    }
                }
            }

            unset($seo);
        }

        $this->write($tmpFile, '</sitemapindex>');

        copy($tmpFile, $outPath . $outFile);
        unlink($tmpFile);
    }

    public function write($file, $line)
    {
        file_put_contents($file, $line . "\r\n", FILE_APPEND);
        //echo $line . PHP_EOL;
    }

    /**
     * @param string $route
     * @param array $params
     * @return bool
     */
    public function filterIndex($route, $params)
    {
        if ($route == 'blog/index') {
            try {
                $db = Blog::dataProvider($params);
                return $db->getTotalItemCount(true) > 3;
            } catch (Exception $e) {
            }
        }
        return false;
    }

    /**
     * @param string $route
     * @param array $params
     * @return bool
     */
    public function filterView($route, $params)
    {
        $sign = null;

        if ($route == 'fair/default/view') {

            $params = array_intersect_key($params, array_flip(Fair::model()->attributeNames()));
            $rec = Fair::model()->findByAttributes($params);
            if (null !== $rec) {
                $sign = strip_tags($rec->uniqueText);
            }

        } elseif ($route == 'exhibitionComplex/default/view') {

            $params = array_intersect_key($params, array_flip(ExhibitionComplex::model()->attributeNames()));
            $rec = ExhibitionComplex::model()->findByAttributes($params);
            if (null !== $rec) {
                $sign = strip_tags($rec->uniqueText);
            }

        } elseif ($route == 'regionInformation/default/view') {

            $params = array_intersect_key($params, array_flip(RegionInformation::model()->attributeNames()));
            $rec = RegionInformation::model()->findByAttributes($params);
            if (null !== $rec) {
                $sign = strip_tags($rec->text);
            }

        } elseif ($route == 'blog/view') {

            $params = array_intersect_key($params, array_flip(Blog::model()->attributeNames()));
            $rec = Blog::model()->findByAttributes($params);
            if (null !== $rec) {
                $sign = strip_tags($rec->theme . $rec->text);
            }

        }

        if (null !== $sign) {
            $sign = preg_replace('#\s+#m', '', $sign);
            if (strlen($sign) > 150) {
                if (!in_array($sign, $this->_cache)) {
                    $this->_cache[] = $sign;
                    return true;
                }
            }
        }

        return false;
    }
}