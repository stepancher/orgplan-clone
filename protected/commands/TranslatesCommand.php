<?php

Yii::import('application.modules.proposal.models.TrAttributeClassProperty');

class TranslatesCommand extends CConsoleCommand
{
    protected $messagesPath = 'application.modules.organizer.messages.';

    protected function getFiles($lang = 'ru')
    {
        return array_slice(scandir(Yii::getPathOfAlias($this->messagesPath . $lang)), 2);
    }
    
    protected function getFileKeys($fileName, $lang = 'ru')
    {
        $keys = array_keys($this->getFilePairs($fileName, $lang));

        array_map(
            function ($key) use ($fileName, $lang) {
                if (substr_count($key, "\n")) {
                    throw new Exception("\n\n\nFound \\n\nFile:\t{$lang}/{$fileName}\nKey:\n\n\"{$key}\"\n\n\n");
                }
            },
            $keys
        );
        
        
        foreach ($keys as $k=>$v){
            $keys[$k]=addcslashes($v,'\\');
        }

        return $keys;

    }
    
    protected function getFilePairs($fileName, $lang = 'ru'){
        return include(Yii::getPathOfAlias($this->messagesPath . $lang) . '/' . $fileName);
    }
    
    protected function getValue($lang, $fileName, $key){

        $key = str_replace('\\\\', '\\', $key);

        $pairs = $this->getFilePairs($fileName, $lang);
                
        if(!isset($pairs[$key])){
            //throw new Exception("\n\n\nKey:\n\n'{$key}'\n\n not found in file '{$fileName}' for '{$lang}' lang.\n\n\n");
            return 'KEY_NOT_FOUND';
        }

        $pairs[$key] = preg_replace('|[\s]{2,}|', ' ', $pairs[$key]);
        $pairs[$key] = preg_replace('|[\n]{1,}|', ' ', $pairs[$key]);
        $pairs[$key] = str_replace('\\', '\\\\', $pairs[$key]);
        
        return $pairs[$key];
    }

    public function actionExport(){
        
        $messages = new stdClass;
        $messages->defaultLang = 'ru';
        $messages->langs = [
            'ru',
            'en',
            'de',
        ];

        fputcsv(
            fopen(Yii::getPathOfAlias('application.data') . '/translates_' . date('Y-m-d') . '.csv', 'w'),
            array(
                'file',
                'key',
                'value_ru',
                'value_en',
                'value_de',
            )       
        );
        
        array_map(
            function($fileName) use($messages){               
                array_map(
                    function($key) use($fileName, $messages){
                        fputcsv(
                            fopen(Yii::getPathOfAlias('application.data') . '/translates_' . date('Y-m-d') . '.csv', 'a+'),
                            array_merge(
                                array(
                                    'file' => $fileName,
                                    'key' => $key,
                                ),
                                array_reduce(
                                    array_map(
                                        function($lang) use($fileName, $key){
                                            return array(
                                                'lang'  => $lang,
                                                'value' => $this->getValue($lang, $fileName, $key),
                                            );
                                        },
                                        $messages->langs
                                    ),
                                    function($result, $item){
                                        $result['value_'.$item['lang']] = $item['value'];
                                        return $result;
                                    }
                                )

                            )
                        );
                    },
                    $this->getFileKeys($fileName, $messages->defaultLang)
                );
            },
            $this->getFiles($messages->defaultLang)
        );
    }
    
    public function actionImport($file)
    {

        $langs = [
            'ru',
            'en',
            'de'
        ];
        

        $d = 1;
        
        array_map(
            function($langIndex, $lang) use(&$d, $langs, $file){

                $csv = file(Yii::getPathOfAlias('application.data') . '/' . $file . '.csv');

                $keys = array_flip(str_getcsv(reset($csv)));

                $dictionaries = array_keys(array_flip(array_map(
                    function($row){
                        return $row['file'];
                    },
                    array_map(
                        function($row) use($csv){
                            return array_combine(
                                str_getcsv(reset($csv)),
                                str_getcsv($row)
                            );
                        },
                        array_slice($csv, 1)
                    )
                )));
                
                array_map(
                    function($dK, $dictionary) use(&$d, $langIndex, $dictionaries, $langs, $keys, $lang, $file){
                        
                        $path = Yii::getPathOfAlias('application.data').'/';
                        
                        echo 
                            "Lang: [".($langIndex+1)."/".count($langs)."]\t".
                            "File: [{$dK}:{$d}/".(count($dictionaries)*count($langs))."]\t{$dictionary}\n"
                        ;

                        if (!file_exists(Yii::getPathOfAlias('application.messages.' . $lang))) {
                            mkdir(Yii::getPathOfAlias('application.messages.' . $lang) , 0777, TRUE);
                        }

                        file_put_contents(
                            Yii::getPathOfAlias('application.messages.' . $lang) .'/'. $dictionary,
                            implode('', array(
                                "<?php return [\n",
                                implode('', array_map(
                                    function($row) use($dictionary, $keys, $lang){
                                        $row   = str_getcsv($row);

                                        $key   = str_replace('\'', '\\\'', $row[$keys['key']]);
                                        $value = str_replace('\'', '\\\'', $row[$keys['value_'.$lang]]);

                                        $key   = str_replace('\\\\', '\\', $key);
                                        $value = str_replace('\\\\', '\\', $value);

                                        if ($dictionary === $row[0]) {
                                            return "\t" . '\'' . $key . '\'=>' . '\'' . $value . '\',' . "\n";
                                        }
                                    },
                                    file($path . $file . '.csv')
                                )),
                                '];',
                            ))
                        );
                        
                        $d++;
                    },
                    array_keys($dictionaries),
                    array_values($dictionaries)
                );
            },
            array_keys($langs),
            array_values($langs)
        );
    }

    public function actionImportAttributes($file)
    {
        Yii::import('app.modules.proposal.models.*');
        $file = file(Yii::getPathOfAlias('application.data') . '/' . $file . '.csv');
        $keys = array_flip(str_getcsv(reset($file)));
        $file = array_slice($file, 1);

        $pecs = [];
        $acs  = [];
        $aps  = [];

        foreach ($file as $row) {
            $row = str_getcsv($row);
            
            $pecs[$row[$keys['pec_id']]] = [
                'trLabel' => $row[$keys['trpec_label_EN']],
                'trPluralLabel' => $row[$keys['trpec_pluralLabel_EN']]
            ];

            $acs[$row[$keys['ac_id']]] = $row[$keys['trac_label_EN']];

            $aps[$row[$keys['ap_id']]] = $row[$keys['trap_value_EN']];
        }

        foreach ($pecs as $key => $pec) {
            $model = TrProposalElementClass::model()->findByAttributes(['trParentId' => $key, 'langId' => 'en']);

            $sql = "INSERT INTO `orgplan`.`tbl_trproposalelementclass` (`trParentId`, `langId`, `label`, 'pluralLabel') VALUES ('$key', 'en', '{$pec['trLabel']}', '{$pec['trPluralLabel']}')";

            if ($model !== NULL) {
                $sql = "UPDATE `orgplan`.`tbl_trproposalelementclass` SET `label`='{$pec['trLabel']}', `pluralLabel`='{$pec['trPluralLabel']}' WHERE `id`='$model->id'";
            }

            echo $sql . ";\n";
        }

        echo "\n";

        foreach ($acs as $key => $ac) {
            $model = TrAttributeClass::model()->findByAttributes(['trParentId' => $key, 'langId' => 'en']);

            $sql = "INSERT INTO `orgplan`.`tbl_trattributeclass` (`trParentId`, `langId`, `label`) VALUES ('$key', 'en', '$ac')";

            if ($model !== NULL) {
                $sql = "UPDATE `orgplan`.`tbl_trattributeclass` SET `label`='$ac' WHERE `id`='$model->id'";
            }

            echo $sql . ";\n";
        }

        echo "\n";

        foreach ($aps as $key => $ap) {
            $model = TrAttributeClassProperty::model()->findByAttributes(['trParentId' => $key, 'langId' => 'en']);

            $sql = "INSERT INTO `orgplan`.`tbl_trattributeclassproperty` (`trParentId`, `langId`, `value`) VALUES ('$key', 'en', '$ap')";

            if ($model !== NULL) {
                $sql = "UPDATE `orgplan`.`tbl_trattributeclassproperty` SET `value`='$ap' WHERE `id`='$model->id'";
            }

            echo $sql . ";\n";
        }
    }

    public function actionImportProperties($file)
    {
        $file = file(Yii::getPathOfAlias('application.data') . '/' . $file . '.csv');

        array_shift($file);

        $output = [];

        foreach ($file as $row) {
            $row = str_getcsv($row);

            $key   = $row[0];
            $value = $row[1];

            if ($key != '' && $value != '') {
                $output[$key] = $value;
            }
        }

        foreach ($output as $key => $value) {
            $model = TrAttributeClassProperty::model()->findByAttributes(['trParentId' => $key, 'langId' => 'en']);

            $sql = "INSERT INTO `orgplan`.`tbl_trattributeclassproperty` (`trParentId`, `langId`, `value`) VALUES ('$key', 'en', '$value')";

            if ($model !== NULL) {
                $sql = "UPDATE `orgplan`.`tbl_trattributeclassproperty` SET `value`='$value' WHERE `id`='$model->id'";
            }

            echo $sql . ";\n";
        }
    }

    public function actionImportRegions($file)
    {
        $file = file(Yii::getPathOfAlias('application.data') . '/' . $file . '.csv');

        array_shift($file);

        $output = [];

        foreach ($file as $row) {
            $row = str_getcsv($row);

            $regionId = $row[0] !== '' ? '\'' . $row[0] . '\'' : 'NULL';
            $name     = $row[1] !== '' ? '\'' . $row[1] . '\'' : 'NULL';

            $output[$regionId] = [$name];
        }

        foreach ($output as $key => $value) {
            echo "INSERT INTO `orgplan`.`tbl_trregion` (`trParentId`, `langId`, `name`) VALUES ($key, 'en', $value[0]);\n";
        }
    }

    public function actionImportIndustries($file)
    {
        $file = file(Yii::getPathOfAlias('application.data') . '/' . $file . '.csv');

        array_shift($file);

        $output = [];

        foreach ($file as $key => $row) {
            $row = str_getcsv($row);

            $industriId = $row[0] !== '' ? '\'' . $row[0] . '\'' : 'NULL';
            $name       = $row[1] !== '' ? '\'' . $row[1] . '\'' : 'NULL';

            $output[$industriId] = [$name];
        }

        foreach ($output as $key => $value) {
            echo "INSERT INTO `orgplan`.`tbl_trindustry` (`trParentId`, `langId`, `name`) VALUES ($key, 'en', $value[0]);\n";
        }
    }

    public function actionFairUpdateShortUrl($offset = FALSE, $active = FALSE){

        $langs = [
            'ru',
            'en',
            'de',
        ];

        $criteria = new CDbCriteria();

        if($active == TRUE){
            $criteria->addCondition('active = :active');
            $criteria->params[':active'] = Fair::ACTIVE_ON;
        }

        $fairs = Fair::model()->findAll($criteria);
        $count = count($fairs);
        $langCount = count($langs);

        echo "\n\n";
	
	    $start = microtime(TRUE);
        $avg = 0;

        /** @var Fair $fair */
        foreach($fairs as $k => $fair){
            $ki = $k+1;

            if($offset && $offset > $ki){
                continue;
            }

            foreach ($langs as $lk => $lang){
                $t = microtime(TRUE);

                Yii::app()->language = $lang;
                $fair->save();

                $pass = ($ki * $langCount) - ($langCount - ($lk+1));
                $last = microtime(TRUE);
                $sec  = round($last - $start, 0);
                $avg  = round(($avg + ($last - $t)) / $pass, 4);

                $lki = $lk+1;
                echo "\r";
                echo "[{$ki}/{$count}]:{$lki}/{$langCount} - ".number_format(round(($ki*100)/$count,2), 2, '.', '').'% '.
                     "Time spend: ".gmdate("H:i:s", $sec).'s ';
            }
        }

        echo "\n";

    }

    public function actionRegionUpdateShortUrl($offset = FALSE){

        $langs = [
            'ru',
            'en',
            'de',
        ];

        $regions = Region::model()->findAll();
        $count = count($regions);
        $langCount = count($langs);

        echo "\n\n";

	    $start = microtime(TRUE);
        $avg = 0;

        /** @var Region $region */
        foreach($regions as $k => $region){
            $ki = $k+1;

            if($offset && $offset > $ki){
                continue;
            }

            foreach ($langs as $lk => $lang){
                $t = microtime(TRUE);

                Yii::app()->language = $lang;
                $region->save();

                $pass = ($ki * $langCount) - ($langCount - ($lk+1));
                $last = microtime(TRUE);
                $sec  = round($last - $start, 0);
                $avg  = round(($avg + ($last - $t)) / $pass, 4);

                $lki = $lk+1;
                echo "\r";
                echo "[{$ki}/{$count}]:{$lki}/{$langCount} - ".number_format(round(($ki*100)/$count,2), 2, '.', '').'% '.
                     "Time spend: ".gmdate("H:i:s", $sec).'s ';
            }
        }

        echo "\n";

    }

    public function actionExhibitionComplexUpdateShortUrl($offset = FALSE){

        $langs = [
            'ru',
            'en',
            'de',
        ];

        $exhibitionComplexes = ExhibitionComplex::model()->findAll();
        $count = count($exhibitionComplexes);
        $langCount = count($langs);

        echo "\n";

        $start = microtime(TRUE);
        $avg = 0;

        foreach ($exhibitionComplexes as $k => $exhibitionComplex){

            $ki = $k + 1;

            if($offset && $offset > $ki)
                continue;

            foreach ($langs as $lk => $lang){

                $t = microtime(TRUE);

                Yii::app()->language = $lang;
                $exhibitionComplex->save();

                $pass = ($ki * $langCount) - ($langCount - ($lk+1));
                $last = microtime(TRUE);
                $sec  = round($last - $start, 0);
                $avg  = round(($avg + ($last - $t)) / $pass, 4);

                $lki = $lk + 1;
                echo "\r";
                echo "[{$ki}/{$count}]:{$lki}/{$langCount} - ".number_format(round(($ki*100)/$count,2), 2, '.', '').'% '.
                    "Time spend: ".gmdate("H:i:s", $sec).'s ';
            }
        }

        echo "\n";
    }

    public function actionCheckShortUrl(){

        $langs = [
            'ru',
            'en',
            'de',
        ];

        foreach (Fair::model()->findAll() as $fair){

            foreach ($langs as $lang){

                $trFair = TrFair::model()->findByAttributes(['trParentId' => $fair->id, 'langId' => $lang]);

                if($trFair === NULL){
                    continue;
                }

                $firstShortUrl = $trFair->shortUrl;

                Yii::app()->language = $lang;
                $fairUrl = $fair->getSeoNameForNewVersion();

                if($firstShortUrl != $fairUrl){
                    echo "'{$fairUrl}'   "; echo "'{$trFair->id}'\n";
                }
            }
        }
    }

    public function actionCheckExhibitionComplexShortUrl(){

        $langs = [
            'ru',
            'en',
            'de',
        ];

        foreach (ExhibitionComplex::model()->findAll() as $exhibitionComplex){

            foreach ($langs as $lang){

                $trExhibitionComplex = TrExhibitionComplex::model()->findByAttributes(['trParentId' => $exhibitionComplex->id, 'langId' => $lang]);

                if($trExhibitionComplex === NULL){
                    continue;
                }

                $firstShortUrl = $trExhibitionComplex->shortUrl;

                if(Yii::app()->language != $lang)
                    Yii::app()->language = $lang;

                $exhibitionComplexUrl = $exhibitionComplex->getSeoNameForNewVersion();

                if($firstShortUrl != $exhibitionComplexUrl){
                    echo "'{$exhibitionComplexUrl}'   "; echo "'{$trExhibitionComplex->id}'\n";
                }
            }
        }
    }
}