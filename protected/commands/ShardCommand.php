<?php

class ShardCommand extends CConsoleCommand {

    const PROTOTYPE_FAIR_ID  = 10943;
    const DEFAULT_PROPOSAL_VERSION = 'acamar';

    public function actionCreate($fairId, $userEmail = 'test@protoplan.pro', $shardVersion = NULL){
        echo "\n";
        echo "\n";
        echo "Creating new shard...";
        echo "\n";

        $targetFair = Fair::model()->findByPk($fairId);

        if($targetFair === NULL){
            echo "TARGET FAIR NOT FOUND.";
            echo "\n";
            echo "\n";
            exit;
        }

        $prototypeFair = Fair::model()->findByPk(self::PROTOTYPE_FAIR_ID);

        if($prototypeFair === NULL){
            echo "PROTOTYPE FAIR NOT FOUND.";
            echo "\n";
            echo "\n";
            exit;
        }

        $dbSourceName = Yii::app()->dbMan->createShardName($prototypeFair->id);
        $dbTargetName = Yii::app()->dbMan->createShardName($fairId);

        self::createShard($targetFair, $dbTargetName, $shardVersion);
        self::copyTables($dbSourceName, $dbTargetName);
        self::changeProposalDbData($dbTargetName, $targetFair->id);
        self::createUser($userEmail, $fairId);
        self::updateTrDocuments($fairId, $dbTargetName);

        echo "\n";
        echo "Done.";
        echo "\n";
        echo "\n";
    }

    public static function createShard($targetFair, $dbName, $shardVersion){

        if($shardVersion == NULL){
            $shardVersion = self::DEFAULT_PROPOSAL_VERSION;
        }

        $targetFair->shard = $dbName;
        $targetFair->proposalVersion = $shardVersion;
        $targetFair->save();

        Yii::app()->dbMan->createShardDb($dbName);
    }

    public function copyTables($dbSourceName, $dbTargetName){

        /** @var CDbConnection $sourceConn */
        $sourceConn = Yii::app()->dbMan->getShardConnection($dbSourceName);

        /** @var CDbConnection $targetConn */
        $targetConn = Yii::app()->dbMan->getShardConnection($dbTargetName);

        $sourceTablesList = $sourceConn->createCommand("SHOW TABLES")->queryColumn();

        foreach($sourceTablesList as $tblName){
            Yii::app()->dbMan->copyTableToDb($tblName, $dbSourceName, $dbTargetName, $targetConn);
        }
    }

    public function createUser($userEmail, $fairId){

        Yii::import('application.modules.organizer.models.OrganizerContact');

        $fair = Fair::model()->findByPk($fairId);

        $contact = Contact::model()->findByAttributes(array('email' => $userEmail));

        $contactSave = "User contact existing.\n";

        if(empty($contact)){
            $contact = new Contact();
            $contact->email = $userEmail;
            $contact->save();

            $contactSave = "User contact saved... \n";
        }

        echo $contactSave;

        $user = User::model()->findByAttributes(array('contactId' => $contact->id));

        $userSave = "User updated... \n";
        if(empty($user)){
            $user = new User();
            $user->password = password_hash($userEmail, PASSWORD_DEFAULT);
            $user->contactId = $contact->id;
            $userSave = "New user created and saved... \n";
        }

        $user->role = USER::ROLE_ORGANIZERS;
        $user->active = 1;

        if($user->save()){
            echo $userSave;
        }

        $FHO = FairHasOrganizer::model()->findByAttributes(array('fairId' => $fairId));

        $organizer = Organizer::model()->findByPk($FHO->organizerId);
        $organizer->active = 1;
        $organizer->proposalEmail = $userEmail;

        if($organizer->save()){
            echo "Organizer proposal email updated... \n";
        }

        $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(array('userId' => $user->id, 'organizerId' => $organizer->id));

        if(empty($userHasOrganizer)){

            $userHasOrganizer = new UserHasOrganizer();
            $userHasOrganizer->userId = $user->id;
            $userHasOrganizer->organizerId = $organizer->id;
            $userHasOrganizer->save();

            echo "Added user as organizer of fair ... \n";
        }

        echo "User already is organizer of this fair ... \n";

        $myFair = MyFair::model()->findByAttributes(array('userId' => $user->id, 'fairId' => $fair->id));

        if(empty($myFair)){

            $myFair = new MyFair;
            $myFair->userId = $user->id;
            $myFair->fairId = $fair->id;
            $myFair->createdAt = date('Y-m-d H:i:s');
            $myFair->status = MyFair::STATUS_ACTIVE;
            $myFair->updatedAt = date('Y-m-d H:i:s');

            $myFair->save();

            echo "MyFair saved... \n";
        }

        echo "User already have this fair in MyFair";

    }

    public function changeProposalDbData($dbTargetName, $fairId){


        $fair = Fair::model()->findByPk($fairId);

        /** @var CDbConnection $targetConn */
        $targetConn = Yii::app()->dbMan->getShardConnection($dbTargetName);
        $prototypeFairId = self::PROTOTYPE_FAIR_ID;
        $sql = "
            DELETE FROM {$dbTargetName}.tbl_organizerexponent;
            DELETE FROM {$dbTargetName}.tbl_calendarfairtasks WHERE fairId != {$prototypeFairId};
            DELETE FROM {$dbTargetName}.tbl_proposalelement;

            UPDATE {$dbTargetName}.tbl_proposalelementclass SET fairId = {$fairId};

            UPDATE {$dbTargetName}.tbl_calendarfairdays SET fairId = {$fairId};
            UPDATE {$dbTargetName}.tbl_calendarfairtasks SET fairId = {$fairId};
            UPDATE {$dbTargetName}.tbl_calendarfairtemptasks SET fairId = {$fairId};
            UPDATE {$dbTargetName}.tbl_calendaruserstasks SET fairId = {$fairId};
        ";

        $targetConn->createCommand($sql)->execute();

        ShardedAR::$_db = $targetConn;

        $dependences = Dependences::model()->findAll();

        foreach($dependences as $dependence){
            $paramArr = json_decode($dependence->params, TRUE);

            if(
                !empty($paramArr) &&
                is_array($paramArr) &&
                !empty($paramArr['fairId'])
            ){
                $paramArr['fairId'] = $fairId;
                $dependence->params = json_encode($paramArr);
                $dependence->save();
            }
        }

        echo "Proposal DB data in new shard updated... \n";
    }

    public function updateTrDocuments($fairId, $dbTargetName){



        ShardedAR::$_db = Yii::app()->dbMan->getShardConnection($dbTargetName);

        $trDocuments = TrDocuments::model()->findAll();

        foreach($trDocuments as $trDocument){

            $file1 = Yii::app()->basePath.$trDocument->value;

            $trDocument->value = str_replace(self::PROTOTYPE_FAIR_ID, $fairId, $trDocument->value);

            $file2 = Yii::app()->basePath.$trDocument->value;

            if($file1 == $file2){
                continue;
            }

            $contentx =@file_get_contents($file1);
            $dirname = dirname($file2);
            if (!is_dir($dirname))
            {
                mkdir($dirname, 0755, true);
            }

            $openedfile = fopen($file2, "w");
            fwrite($openedfile, $contentx);
            fclose($openedfile);

            if ($contentx === FALSE) {
                $status=false;
            }else{
                $status=true;
            };

            $trDocument->save();
        }

        $documents = Documents::model()->findAllByAttributes(array('fairId' => self::PROTOTYPE_FAIR_ID));
        foreach($documents as $document){
            $document->fairId = $fairId;

            $document->save();
        }


        echo "Documents copy completed... \n";
    }

}