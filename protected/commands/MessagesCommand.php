<?php

class MessagesCommand extends CConsoleCommand
{

    public $languages = array(
        'ru',
        'en',
        'de',
    );

    /**
     * @param $scanDir
     * @param null $oneDict
     */
    public function actionLoad($scanDir, $oneDict = NULL){
        
        foreach ($this->languages as $lang) {

            $dir = scandir(Yii::getPathOfAlias($scanDir).DIRECTORY_SEPARATOR.$lang);
            $dir = array_filter($dir,
                function ($row) {
                    if ($row == '.' || $row == '..')
                        return FALSE;
                    return TRUE;
                }
            );
            
            foreach ($dir as $dict) {

                $result = '<?php 
return array(
';
                $autoDictionaryString = file_get_contents(Yii::getPathOfAlias($scanDir).DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.$dict);

                $autoDictionary = str_replace('<?php', '', $autoDictionaryString);
                $autoDictionary = eval($autoDictionary);

                if(file_exists(Yii::getPathOfAlias('application.messages.' . $lang) . '/' . $dict)){

                    $manualDictionary = file_get_contents(Yii::getPathOfAlias('application.messages.' . $lang) . '/' . $dict);
                } elseif (file_exists(Yii::getPathOfAlias('application.vendor.yiisoft.yii.framework.messages.' . $lang) . '/' . $dict)){

                    $manualDictionary = file_get_contents(Yii::getPathOfAlias('application.vendor.yiisoft.yii.framework.messages.' . $lang) . '/' . $dict);
                }
                else {

                    $manualDictionary = '';
                }

                $manualDictionary = str_replace('<?php', '', $manualDictionary);
                $manualDictionary = eval($manualDictionary);

                foreach ($autoDictionary as $key => $value) {

                    if(!empty($value))
                        continue;
                    
                    if(!isset($manualDictionary[$key]))
                        continue;

                    $manualValue = $manualDictionary[$key];

                    if (!empty($manualValue)) {

                        $key2 = preg_quote($key);
                        $pattern = '\''.$key2.'.+\'';
//                        var_dump($pattern);
//                        var_dump($manualValue);
//                        var_dump($autoDictionaryString);
                        $autoDictionaryString = preg_replace($pattern, "$key' => '$manualValue',", $autoDictionaryString);


                    }

                }
                preg_match_all('(.+=>.+)', $autoDictionaryString, $matches);

                array_reduce(
                    $matches[0],
                    function ($res, $match) use(&$result){

                        $result .= $match . '
';
                        return $result;
                    }
                );

                if($oneDict === NULL){

                    $result = $result . ');';
                    file_put_contents(Yii::getPathOfAlias($scanDir).DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.$dict, $result);
                }
            }

            if(isset($oneDict)){

                $result = $result . ');';
                file_put_contents(Yii::getPathOfAlias($scanDir).DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.'MyDictionary', $result);
            }
        }
    }

    /**
     * @param $module
     * @param $fileName
     */
    public function actionTFile($module, $fileName = NULL){

        $dir = scandir(Yii::getPathOfAlias('application.modules' . '/' . $module . '/' . 'components'));
        $dir = array_filter($dir,
            function ($row) use($module) {
                if ($row == '.' || $row == '..')
                    return FALSE;
                return TRUE;
            }
        );

        if(isset($fileName)){

            $fileName = $fileName.'.php';
            $this->putTrs($module, $fileName);
        } else {

            foreach ($dir as $fName){

                $this->putTrs($module, $fName);
            }
        }


    }

    protected function putTrs($module, $fName){

        $fileText = file_get_contents(Yii::getPathOfAlias('application.modules') . '/' . $module . '/' . 'components' . '/' . $fName);
        $pattern = '(Yii::t\(\'([a-zA-Z\s]+)\',\'([a-zA-Z\s]+)\'\))';
        preg_match_all($pattern, $fileText, $matches);
        foreach ($matches[0] as $key => $match){

            $trValue = Yii::t($matches[1][$key], $matches[2][$key]);
            file_put_contents(Yii::getPathOfAlias('application.modules') . '/' . $module . '/' . 'messages' . '/' . 'ru' . '/' . $module . '.php', '\'' . $matches[2][$key] . '\'' . ' => ' . '\'' . $trValue . '\'' . ',' . "\n", FILE_APPEND);
            file_put_contents(Yii::getPathOfAlias('application.modules') . '/' . $module . '/' . 'messages' . '/' . 'en' . '/' . $module . '.php', '\'' . $matches[2][$key] . '\'' . ' => ' . '\'' . $trValue . '\'' . ',' . "\n", FILE_APPEND);
            file_put_contents(Yii::getPathOfAlias('application.modules') . '/' . $module . '/' . 'messages' . '/' . 'de' . '/' . $module . '.php', '\'' . $matches[2][$key] . '\'' . ' => ' . '\'' . $trValue . '\'' . ',' . "\n", FILE_APPEND);
        }
    }
}