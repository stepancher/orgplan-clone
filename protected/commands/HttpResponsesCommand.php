<?php

/**
 * Class SummaryCommand
 */
class HttpResponsesCommand extends CConsoleCommand
{
    const RETURNTRANSFER = 1;
    const CONNECTTIMEOUT = 10;
    /**
     * Выводит на экран сайты выставок, у которых http response code != 200
     */
    public function actionFairs($offset = NULL){

        echo "\n";
        echo "Fairs sites checking...\n\n";

        $criteria = new CDbCriteria;
        $criteria->select = array('t.site','t.id', 't.active', 't.beginDate');

        $fairs = Fair::model()->findAll($criteria);

        foreach ($fairs as $k => $item) {

            if(is_numeric($offset) && $k < (int)$offset)
                continue;

            echo "\r{$item->id}... [{$k}/".count($fairs)."]";

            $ch = curl_init($item->site);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, self::RETURNTRANSFER); // Отключение вывода результатов запроса на экран
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CONNECTTIMEOUT); // Количетсов секунд ожидания при запросе

            curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($code >= 400){
                echo "\r";
                print_r($item->id . ', ' . $item->active . ', ' . $item->beginDate . ', ' . $item->site. ', ' . $code . ', ' . "https://protoplan.pro/ru/admin/fair/update/?id=$item->id" . "\n");
            }

        }
        echo "\n";
        echo "Fairs sites checked.\n";
        echo "\n\n";
    }

    /**
     * Выводит на экран сайты выставочных центров, у которых http response code != 200
     */
    public function actionVenues(){

        echo "\n";
        echo "ExhibitionComplex sites checking...\n";

        $criteria = new CDbCriteria;
        $criteria->select = array('t.site','t.id');

        $fairs = ExhibitionComplex::model()->findAll($criteria);

        foreach ($fairs as $item) {

            $ch = curl_init($item->site);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, self::RETURNTRANSFER); // Отключение вывода результатов запроса на экран
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CONNECTTIMEOUT); // Количетсов секунд ожидания при запросе

            curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($code >= 400)
                print_r($item->id . ',' . $item->site. ',' . $code . "\n");

        }
        echo "\n";
        echo "ExhibitionComplex sites checked.\n";
        echo "\n\n";
    }

    /**
     * Выводит на экран сайты организаторов, у которых http response code != 200
     */
    public function actionOrganizer(){

        echo "\n";
        echo "Organizer sites checking...\n";

        $criteria = new CDbCriteria;
        $criteria->select = array('t.linkToTheSiteOrganizers','t.id');

        $fairs = Organizer::model()->findAll($criteria);

        foreach ($fairs as $item) {

            $ch = curl_init($item->linkToTheSiteOrganizers);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, self::RETURNTRANSFER); // Отключение вывода результатов запроса на экран
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CONNECTTIMEOUT); // Количетсов секунд ожидания при запросе

            curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($code >= 400)
                print_r($item->id . ',' . $item->linkToTheSiteOrganizers. ',' . $code . "\n");

        }
        echo "\n";
        echo "Organizer sites checked.\n";
        echo "\n\n";
    }
}