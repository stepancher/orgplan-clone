<?php
/**
 * Консольные команды для парсинга и записи информации о регионе
 * Class RaexpertCommand
 */
defined('IS_WIN') or define('IS_WIN', strtoupper(substr(PHP_OS, 0, 3)) == 'WIN');

class RaexpertCommand extends CConsoleCommand
{
	static $ratings = array(
		'1A',
		'2A',
		'2A',
		'1B',
		'2B',
		'3B1',
		'3B2',
		'1C',
		'2C',
		'3C1',
		'3C2',
		'3D'
	);

	/**
	 * получение ответа от запроса
	 * @param $url
	 * @param $post
	 * @param bool $ssl
	 * @param string $headers
	 * @param string $uagent
	 * @return bool|mixed
	 */
	public function makeHttpPostRequest($url, $post, $ssl = false, $headers = '', $uagent = '')
	{
		if (empty($url)) {
			return false;
		}
		$_post = Array();
		if (is_array($post)) {
			foreach ($post as $name => $value) {
				$_post[] = $name . '=' . urlencode($value);
			}
		}
		$ch = curl_init($url);
		if ($ssl) { /*если соединяемся с https*/
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if (is_array($post)) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
		}
		if (is_array($headers)) { /*если заданы какие-то заголовки для браузера*/
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		if (!empty($uagent)) { /*если задан UserAgent*/
			curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
		}
		$result = curl_exec($ch);
		if (curl_errno($ch) != 0 && empty($result)) {
			$result = false;
		}
		curl_close($ch);
		return $result;
	}

	/**
	 * обработка ответа для получния нужной информации
	 * @throws Exception
	 */
	public function actionRun()
	{

		$opts = array(
			'http' => array(
				'method' => "GET",
				'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
			)
		);
		$context = stream_context_create($opts);
		$link = 'http://www1.raexpert.ru/rankings/#r_1108';
		$content = file_get_contents($link, null, $context);
		$i = 0;
		$ratingRegion = null;
		$document = phpQuery::newDocumentHTML($content)->find('#rating_list_1108 tbody tr')->elements;
		if (isset($document)) {
			echo "document exists \n";
			$counter = 0;
			foreach ($document as $doc) {
				$counter++;
				echo "Сохранение региона №".$counter." \n";
				$regionName = pq($doc)->find('td:nth-child(1)')->text();
				if (($raexpert = Raexpert::model()->findByAttributes(array('name' => $regionName))) === null){
					$raexpert = new Raexpert();

					echo $regionName.' not found in raexpert';
					continue;
				}
				$linkRegion = 'http://www1.raexpert.ru' . pq($doc)->find('a')->attr('href');
				$rating = pq($doc)->find('div.rating-img-container')->text();
				$ratingRegion = $rating;

				$region = Region::model()->findByPk($raexpert->regionId);
				if ($region != null) {
					$raexpert->regionId = $region->id;
				}else{
					echo $regionName.' not found in regions';
					continue;
				}

				$linkRegionDescription = $linkRegion;
				$raexpert->regionLink = $linkRegion;
				$raexpert->rating = $ratingRegion;
				$raexpert->name = $regionName;
				$contentRegion = file_get_contents($linkRegion, null, $context);
				$contentRegionDescription = file_get_contents($linkRegionDescription, null, $context);
				preg_match('/renderChart3\((\d+)/', $contentRegion, $match);
				$post = Array('cid' => $match[1], 'year' => '2011', 'name' => $regionName);
				$headers = Array();
				$headers[] = "Content-type: application/x-www-form-urlencoded";
				$contentChart = $this->makeHttpPostRequest('http://www.raexpert.ru/application/database/views/scripts/regions/chart3.php', $post, false, $headers);
				$linkEthernet = phpQuery::newDocumentHTML($contentRegionDescription)->find('.company-info-numbers-item .company-info-name:not(:first-child)')->next()->text();
				$description = phpQuery::newDocumentHTML($contentRegionDescription)->find('.company-descr p');
				if (!empty($description)) {
					$description = pq($description)->text();
					if (!empty($description)) {
						$raexpert->generalInformation = $description;
					} else {
						$raexpert->generalInformation = null;
					}
				}
				$imageLink = phpQuery::newDocumentHTML($contentRegionDescription)->find('.company-info-container .db-company-logo img')->attr('src');
				$raexpert->generalInformationComment = null;
				$raexpert->linkEthernet = !empty($linkEthernet) ? $linkEthernet : null;
				$raexpert->linkImage = !empty($imageLink) ? 'http://www1.raexpert.ru' . $imageLink : null;
				if ($contentChart !== false) {
					$raexpert->chart = $contentChart;
				} else {
					throw new Exception('Данные не найдены для ' . $regionName);
				}
				$raexpert->save(false);
			}
		}
	}

	public function actionSynchronizeRatings(){
		$raexperts = Raexpert::model()->findAll();

		foreach($raexperts as $raexpert){
			$regionInformation = RegionInformation::model()->findByAttributes(array('regionId' => $raexpert->regionId));
			if(!empty($regionInformation) && !empty($raexpert->rating)){

				$regionInformation->rating = $raexpert->rating;
				$regionInformation->save();
			}

		}
	}
}