<?php

/**
 * Class MaintainCommand
 */
class MaintainCommand extends CConsoleCommand
{
    public $tables = array(
        'db' => array(),
        'dbSession' => array(
            'session'
        ),
        'dbProposal' => array(),
    );

    public function actionTables(){

        foreach ($this->tables as $key => $val){
            echo "\n";

            if (count($val) != 0){
                echo "DATABASE: $key\n";
                echo "TABLES: ";
            }

            foreach ($val as $k => $tables){

                if($k == count($val) - 1) {
                    echo "tbl_$tables\n";
                } else {
                    echo 'tbl_' . $tables . ', ';
                }
            }
        }

        echo "\n";
    }

    public function actionOptimize(){

        foreach ($this->tables as $key => $val){

            if (count($val) != 0){

                foreach ($val as $k => $table){

                    $transaction = Yii::app()->{$key}->beginTransaction();
                    echo "\n";
                    echo "OPTIMIZING TABLE tbl_{$table}...\n";

                    try {
                        echo "Size table tbl_{$table} before Optimization = " . $this->getTableSize($key, $table) . "\n";
                        echo "...\n";
                        Yii::app()->{$key}->createCommand("OPTIMIZE TABLE tbl_{$table};")->execute();
                        echo "Done.\n";
                        echo "Size table tbl_{$table} after Optimization = " . $this->getTableSize($key, $table) . "\n";

                        $transaction->commit();

                    } catch (Exception $e) {

                        $transaction->rollback();
                        echo $e->getMessage();
                    }

                }

            }

        }
        echo "\n";

    }

    protected function getTableSize($db = 'db', $table){

        $connection = Yii::app()->{$db}->getSchema()->getDbConnection()->connectionString;
        $schema = substr($connection, strrpos($connection,'=') + 1);

        $oCommand = Yii::app()->{$db}->createCommand("SELECT ROUND(((data_length + index_length) / 1024 / 1024), 2) AS size FROM information_schema.TABLES WHERE table_schema = \"{$schema}\" AND table_name = \"tbl_{$table}\";");
        $oCDbDataReader = $oCommand->queryAll();

        return $oCDbDataReader[0]['size'] . ' Mb';

    }

}