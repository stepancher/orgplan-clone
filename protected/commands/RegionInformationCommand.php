<?php
//TODO проверить надобность этого файла
/**
 * Консольные команды для парсинга и записи информации о регионе
 * Class RegionInformationCommand
 */
class RegionInformationCommand extends CConsoleCommand
{

	public function actionIndex($regionName)
	{
		$regionName = rawurldecode($regionName);
		$regionIsset = Region::model()->findByAttributes(array('name' => $regionName));
		if ($regionIsset && $regionIsset->regionInformation) {
			$currentRegionInfo = $regionIsset->regionInformation;

			if ($currentRegionInfo && !$currentRegionInfo->rating) {
				$regionFind = RegionRating::model()->find();

				$update = 0;
				if ($regionFind && $regionFind->update) {
					$update = $regionFind->update;
				}

				$isActual = time() >= strtotime($update) + 3600 * 24 * 30; //Проверка на актуальность информации (сравниваем текущую дату с датой записанной в бд).
				if ($isActual) {
					$ch = curl_init();

					curl_setopt($ch, CURLOPT_URL, "http://www.raexpert.ru/modules/ratings/out.php");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,
						http_build_query(array(
							'selected_ratings' => '28',
							'selected_type' => 'csv',
							'rating[/ratings/regions/][path]' => '/ratings/regions/',
							'rating[/ratings/regions/][name]' => 'Инвестиционный рейтинг регионов',
						))
					);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$response = curl_exec($ch);

					curl_close($ch);

					$response = iconv('WINDOWS-1251', 'UTF-8', $response);
					$lines = explode("\r\n", $response);
					foreach ($lines as $row) {
						$row = explode(';', $row);
						if (!empty($row[0])) {
							$record = RegionRating::model()->findByAttributes(array('name' => $row[0]));

							if ($record == null) {
								$record = new RegionRating;
							}
							$record->setAttributes(
								array(
									'name' => $row[0],
									'rating' => $row[1],
									'dynamic' => $row[2],
									'license' => $row[3],
									'identificationNumber' => $row[4],
									'modifiedRating' => $row[5],
									'pressRelease' => date('Y-m-d H:i:s', strtotime($row[6])),
									'update' => date('Y-m-d H:i:s'),
								),
								false
							);
							$record->save(false);
						}
					}
				}
				$regionRating = RegionRating::model()->findByAttributes(array('name' =>$regionName));
				$currentRegionInfo->rating = $regionRating ? $regionRating->rating : -1;
				$currentRegionInfo->save(false);
			}
		}
	}
}