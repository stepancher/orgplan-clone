<?php
/**
 */

/**
 * Class SiteMapCommand
 */
class SiteMap1Command extends CConsoleCommand
{
    const MAX_ITEMS = 10000;

    public $mainFile;
    
    public $baseUrl;

    public $sectorToCategory = [
        'fair' => 'catalogFairs',
        'exhibitionComplex' => 'catalogExhibitions',
        'industry' => 'catalogIndustries',
        'region' => 'catalogRegions',
    ];

    public $routeToCategory = [
        'fair/default/view' => 'fairs',
        'exhibitionComplex/default/view' => 'exhibitionComplexes',
        'region/default/view' => 'regions',
        'industry/view' => 'industries',
        'blog/view' => 'blogs',
        'blog/index' => 'blogTags',
    ];

    public $siteMapCategories = [
        'catalogFairs' => [],
        'catalogExhibitions' => [],
        'catalogRegions' => [],
        'catalogIndustries' => [],
        'fairs' => [],
        'exhibitionComplexes' => [],
        'regions' => [],
        'industries' => [],
        'blogs' => [],
        'blogTags' => [],
        'other' => [],
    ];

    public $changeFreq = [
        'search/index' => 'weekly',
        'blog/index' => 'weekly',
        'exhibitionComplex/tour' => 'annual',
        'help/default/index' => 'annual',
        'regionInformation/default/view' => 'monthly',
        'blog/view' => 'monthly',
        'stand/default/view' => 'monthly',
        'fair/default/view' => 'monthly',
        'fair/fairComment' => 'daily',
        'exhibitionComplex/default/view' => 'monthly',
        'organizer/view' => 'monthly',
    ];

    public function actionIndex()
    {
        /** @var Seo[] $seoAll */
        $seoAll = Seo::model()->findAll();
        foreach ($seoAll as $seo) {
            $this->setCatalogItem($seo);
        }
        $this->createSiteMapFiles();
    }


    /**
     * @param Seo $seo
     * @return array
     */
    public function setCatalogItem($seo)
    {
        $params = json_decode($seo->params, true);
        if ($seo->route == 'search/index' && isset($params['sector']) && !empty($params['sector']) && !stristr($seo->url, '?')) {
            if (isset($this->sectorToCategory[$params['sector']])) {
                $category = $this->sectorToCategory[$params['sector']];
                $this->siteMapCategories[$category][] = $this->setData($seo);
            }
        } else {
            if (isset($this->routeToCategory[$seo->route])) {
                $category = $this->routeToCategory[$seo->route];
                $params = json_decode($seo->params, true);
                if ($seo->route == 'blog/index' && !isset($params['tag'])) {
                    $this->siteMapCategories['other'][] = $this->setData($seo);
                } else {
                    $this->siteMapCategories[$category][] = $this->setData($seo);
                }

            } else {
                $this->siteMapCategories['other'][] = $this->setData($seo);
            }
        }
    }

    public function setData($seo)
    {
        return [
            'loc' => $this->baseUrl.$seo->url,
            'lastmod' => $this->getLastMode($seo->lastmode),
            'changefreq' => $this->getChangeFreq($seo->route),
            'priority' => $seo->sitemapPriority
        ];
    }

    /**
     * Получение changefreq в зависимости от роута
     * @param $route
     * @return null
     */
    public function getChangeFreq($route)
    {
        if (isset($this->changeFreq[$route])) {
            return $this->changeFreq[$route];
        }
        return null;
    }

    public function getLastMode($time = null)
    {
        $time = null == $time ? time() : strtotime($time);
        return date('c', $time);
    }

    public function write($file, $line)
    {
        file_put_contents($file, $line . "\r\n", FILE_APPEND);
        //echo $line . PHP_EOL;
    }

    public function createSiteMapFiles()
    {
        foreach ($this->siteMapCategories as $category => $items) {
            if (count($items) > static::MAX_ITEMS) {
                $this->multipleForCategory($category, $items);
            } else {
                $this->simpleForCategory($category, $items);
            }
        }
        $this->createMainFile();
    }

    public function createMainFile()
    {
        $fileName =  'sitemap.xml';
        $file = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$fileName;
        if (file_exists($file)) {
            unlink($file);
        }
        $this->write($file, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
        foreach ($this->mainFile as $category => $data) {
            if (is_array($data)) {
                $fileName1 =  'sitemap-'.$category.'.xml';
                $file1 = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$fileName1;
                if (file_exists($file1)) {
                    unlink($file1);
                }
                $this->write($file1, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
                foreach ($data as $name) {
                    $this->writeCategoryData($file1, $name);
                }
                $this->write($file1, '</sitemapindex>');
            } else {
                $this->writeCategoryData($file, $data);
            }
        }
        $this->write($file, '</sitemapindex>');

    }

    public function writeCategoryData($file, $fileName)
    {
        $this->write($file, '<sitemap>');
        $this->write($file, '<loc>'.$this->baseUrl.'/'.$fileName.'</loc>');
        $this->write($file, '<lastmod>'.$this->getLastMode().'</lastmod>');
        $this->write($file, '</sitemap>');
    }

    public function multipleForCategory($category, $data)
    {
        $count = ceil(count($data) / static::MAX_ITEMS);
        $result = [];
        for ($i = 1; $i <= $count; $i++) {
            $start = static::MAX_ITEMS*$i-static::MAX_ITEMS;
            $fileName =  'sitemap-p'.$i.'-'.$category.'.xml';
            $file = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$fileName;
            if (file_exists($file)) {
                unlink($file);
            }
            $this->write($file, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
            for ($ii = $start; $ii <= (static::MAX_ITEMS*$i); $ii++) {
                if(isset($data[$ii])) {
                    $this->write($file, '<url>');
                    $this->writeUrlData($file, $data[$ii]);
                    $this->write($file, '</url >');
                } else {
                    break;
                }

            }
            $this->write($file, '</urlset>');

            $this->mainFile[$category][] = $fileName;
        }
        return $result;
    }

    public function simpleForCategory($category, $data)
    {
        if(!empty($data)) {
            $fileName =  'sitemap-'.$category.'.xml';
            $file = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$fileName;
            if (file_exists($file)) {
                unlink($file);
            }
            $this->write($file, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
            foreach($data as $item){
                $this->write($file, '<url>');
                $this->writeUrlData($file, $item);
                $this->write($file, '</url >');
            }
            $this->write($file, '</urlset>');
            $this->mainFile[$category] = $fileName;
        }
    }

    public function writeUrlData($file, $data)
    {
        $this->write($file, '<loc>'.$data['loc'].'</loc>');
        $this->write($file, '<lastmod>'.$data['lastmod'].'</lastmod>');
        $this->write($file, '<changefreq>'.$data['changefreq'].'</changefreq>');
        $this->write($file, '<priority>'.$data['priority'].'</priority>');
    }
}