<?php

Yii::import('application.modules.proposal.models.TrAttributeClassProperty');

class MuninCommand extends CConsoleCommand{

    public function actionCalendar($config = 0){

        if($config){
            $params = [
                "graph_title Protoplan - Calendar",
                "graph_vlabel count",
                "graph_printf %6.3lf",
                "protoplan_calendar_fair_tasks_count.label calendar fair tasks",
            ];
            echo implode("\n", $params);
            exit;
        }

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_calendarfairtasks` `cft`";
        $row = Yii::app()->db->createCommand($sql)->queryRow();
        echo "protoplan_calendar_fair_tasks_count.value ".$row['count']."\n";

    }

    public function actionProposal($config = 0){

        if($config){
            $params = [
                "graph_title Protoplan - Proposal",
                "graph_vlabel count",
                "graph_printf %6.3lf",
                "protoplan_proposal_attribute_count.label attribute",
                "protoplan_proposal_attribute_class_count.label attribute class",
            ];
            echo implode("\n", $params);
            exit;
        }

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_attribute` `a`";
        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_proposal_attribute_count.value ".$row['count']."\n";

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_attributeclass` `ac`";
        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_proposal_attribute_class_count.value ".$row['count']."\n";

    }

    public function actionUsers($config = 0){

        if($config){
            $params = [
                "graph_title Protoplan - Users",
                "graph_vlabel count",
                "graph_printf %6.3lf",
                "protoplan_users_count.label users total",
                "protoplan_users_daily.label users daily",
                "protoplan_users_weekly.label users weekly",
                "protoplan_users_monthly.label users monthly",
                "protoplan_users_yearly.label users yearly",
                "protoplan_users_online.label users online",
            ];
            echo implode("\n", $params)."\n";
            exit;
        }

        $getVal = function($name, $sql){
            $row = Yii::app()->db->createCommand($sql)->queryRow();
            return $name.".value ".$row['count']."\n";
        };

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_user` `u`";
        echo $getVal("protoplan_users_count", $sql);

        ////////

        $sql = "
            SELECT COUNT(0) AS `count` FROM `tbl_user` `u`
            WHERE u.lastEntry > now() - interval 1 day
        ";
        echo $getVal("protoplan_users_daily", $sql);

        ////////

        $sql = "
            SELECT COUNT(0) AS `count` FROM `tbl_user` AS `u`
            WHERE u.lastEntry > now() - interval 1 month
        ";
        echo $getVal("protoplan_users_monthly", $sql);

        /////////

        $sql = "
            SELECT COUNT(0) AS `count` FROM `tbl_user` AS `u`
            WHERE u.lastEntry > now() - interval 1 week
        ";
        echo $getVal("protoplan_users_weekly", $sql);

        /////////

        $sql = "
            SELECT COUNT(0) AS `count` FROM `tbl_user` `u`
            WHERE u.lastEntry > now() - interval 1 year
        ";
        echo $getVal("protoplan_users_yearly", $sql);

        $sql = "
            SELECT COUNT(0) AS `count` FROM `tbl_user` `u`
            WHERE u.lastEntry > NOW() - INTERVAL 5 MINUTE
        ";
        echo $getVal("protoplan_users_online", $sql);

    }

    public function actionVenues($config = 0){

        if($config){
            $params = [
                "graph_title Protoplan - Venues",
                "graph_vlabel count",
                "graph_printf %6.3lf",
                "protoplan_venues_count.label venues active",
            ];
            echo implode("\n", $params);
            exit;
        }

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_exhibitioncomplex` `ec` WHERE `ec`.`active` = 1";
        $row = Yii::app()->db->createCommand($sql)->queryRow();
        echo "protoplan_venues_count.value ".$row['count']."\n";

    }

    public function actionFairs($config = 0){

        if($config){
            $params = [
                "graph_title Protoplan - Fairs",
                "graph_vlabel count",
                "graph_printf %6.3lf",
                "protoplan_fairs_count.label fairs active",
            ];
            echo implode("\n", $params);
            exit;
        }

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_fair` `f` WHERE `f`.`active` = 1";
        $row = Yii::app()->db->createCommand($sql)->queryRow();
        echo "protoplan_fairs_count.value ".$row['count']."\n";

    }

    public function actionFairsYear($config = 0){

        if($config){
            $params = [
                "graph_category protoplan_db",
                "graph_title Protoplan - FairsYear",
                "graph_vlabel count",
                "graph_printf %6.3lf",
            ];
            $draw = 'AREA';

            foreach (Fair::getFairYears() as $year){
                $params[] = "protoplan_fairs_published_$year.label fairs {$year} active";
                $params[] = "protoplan_fairs_published_$year.draw $draw";
                $params[] = "protoplan_fairs_unpublished_$year.label fairs {$year} inactive";
                $params[] = "protoplan_fairs_unpublished_$year.draw STACK";
                $draw = 'STACK';
            }
            $params[] = "protoplan_fairs_unpublished_without_date.label inactive N/O";
            $params[] = "protoplan_fairs_unpublished_without_date.draw STACK";

            echo implode("\n", $params);
            exit;
        }

        foreach (Fair::getFairYears() as $year){

            $sql = "SELECT COUNT(*) AS `count` FROM `tbl_fair` `f` WHERE `f`.`active` = 1 AND YEAR(`f`.`beginDate`) = $year";
            $row = Yii::app()->db->createCommand($sql)->queryRow();
            echo "protoplan_fairs_published_$year.value ".$row['count']."\n";

            $sql = "SELECT COUNT(*) AS `count` FROM `tbl_fair` `f` WHERE `f`.`active` = 0 AND YEAR(`f`.`beginDate`) = $year";
            $row = Yii::app()->db->createCommand($sql)->queryRow();
            echo "protoplan_fairs_unpublished_$year.value ".$row['count']."\n";
        }

        $sql = "SELECT COUNT(*) AS `count` FROM `tbl_fair` `f` WHERE `f`.`active` = 0 AND YEAR(`f`.`beginDate`) < 2000";
        $row = Yii::app()->db->createCommand($sql)->queryRow();
        echo "protoplan_fairs_unpublished_without_date.value ".$row['count']."\n";
    }

    public function actionFairsCountry($config = 0){

        $params = [
            "graph_category protoplan_db",
            "graph_title Protoplan - FairsCountry",
            "graph_vlabel count",
            "graph_printf %6.3lf",
        ];
        $draw = 'AREA';

        $sql = "SELECT COUNT(`f`.`id`) AS `count`, `f`.`active` as `active`, `cn`.`shortUrl` AS `shortUrl`, `trcn`.`name` AS `name` 
                    FROM `tbl_fair` AS `f`
                        LEFT JOIN `tbl_exhibitioncomplex` AS `ec` ON `f`.`exhibitionComplexId` = `ec`.`id`
                        LEFT JOIN `tbl_city` AS `c` ON `ec`.`cityId` = `c`.`id`
                        LEFT JOIN `tbl_region` AS `r` ON `c`.`regionId` = `r`.`id`
                        LEFT JOIN `tbl_district` AS `d` ON `r`.`districtId` = `d`.`id`
                        LEFT JOIN `tbl_country` AS `cn` ON `d`.`countryId` = `cn`.`id`
                        LEFT JOIN `tbl_trcountry` AS `trcn` ON `trcn`.`trParentId` = `cn`.`id` AND `trcn`.`langId` = 'en'
                    GROUP BY `cn`.`id`, `f`.`active`
                    ORDER BY `shortUrl`, `active`";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        array_map(
            function($row) use($config, &$draw, &$params){

                if($row['shortUrl'] === NULL && $row['active'] == '1'){

                    if($config){

                        $params[] = "protoplan_fairs_published_without_country.label fairs without country active";
                        $params[] = "protoplan_fairs_published_without_country.draw {$draw}";
                    } else{

                        echo "protoplan_fairs_published_without_country.value {$row['count']}\n";
                    }

                } elseif ($row['shortUrl'] === NULL && $row['active'] == '0'){

                    if($config){

                        $params[] = "protoplan_fairs_unpublished_without_country.label fairs without country inactive";
                        $params[] = "protoplan_fairs_unpublished_without_country.draw {$draw}";
                    } else{

                        echo "protoplan_fairs_unpublished_without_country.value {$row['count']}\n";
                    }

                } elseif ($row['active'] == '1'){

                    if($config){

                        $params[] = "protoplan_fairs_published_{$row['shortUrl']}.label fairs {$row['name']} active";
                        $params[] = "protoplan_fairs_published_{$row['shortUrl']}.draw {$draw}";
                    } else{

                        echo "protoplan_fairs_published_{$row['shortUrl']}.value {$row['count']}\n";
                    }

                } elseif ($row['active'] == '0'){

                    if($config){

                        $params[] = "protoplan_fairs_unpublished_{$row['shortUrl']}.label fairs {$row['name']} inactive";
                        $params[] = "protoplan_fairs_unpublished_{$row['shortUrl']}.draw {$draw}";
                    } else{

                        echo "protoplan_fairs_unpublished_{$row['shortUrl']}.value {$row['count']}\n";
                    }

                }

                if($config){

                    echo implode("\n", $params);
                }

                $draw = 'STACK';
            },
            $rows
        );
    }

    public function actionStatisticsByYear($config = 0){

        $params = [
            "graph_category protoplan_db",
            "graph_title Protoplan - StatisticsByYear",
            "graph_vlabel count",
            "graph_printf %6.3lf",
        ];
        $draw = 'AREA';

        $sql = "SELECT YEAR(fi.statisticsDate) AS statisticsDate, f.active, COUNT(f.id) AS count FROM tbl_fair f
                LEFT JOIN tbl_fairinfo fi ON fi.id = f.infoId
                WHERE YEAR(fi.statisticsDate) != 0 AND YEAR(fi.statisticsDate) IS NOT NULL
                GROUP BY YEAR(fi.statisticsDate), f.active";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        array_map(
            function ($row) use($config, &$draw, &$params){

                if($row['active'] == 1){

                    if($config){

                        $params[] = "protoplan_{$row['statisticsDate']}_statisticsDate_fairs_published.label fairs {$row['statisticsDate']} active";
                        $params[] = "protoplan_{$row['statisticsDate']}_statisticsDate_fairs_published.draw {$draw}";
                    } else{

                        echo "protoplan_{$row['statisticsDate']}_statisticsDate_fairs_published.value {$row['count']}\n";
                    }
                } elseif($row['active'] == 0) {

                    if($config){

                        $params[] = "protoplan_{$row['statisticsDate']}_statisticsDate_fairs_unpublished.label fairs {$row['statisticsDate']} inactive";
                        $params[] = "protoplan_{$row['statisticsDate']}_statisticsDate_fairs_unpublished.draw {$draw}";
                    } else{

                        echo "protoplan_{$row['statisticsDate']}_statisticsDate_fairs_unpublished.value {$row['count']}\n";
                    }
                }

                $draw = 'STACK';
            },
            $rows
        );

        $sql = "SELECT SUM(tb.count) AS count, tb.active FROM
                    (SELECT YEAR(fi.statisticsDate) AS statisticsDate, f.active, COUNT(f.id) AS count FROM tbl_fair f
                        LEFT JOIN tbl_fairinfo fi ON fi.id = f.infoId
                        WHERE YEAR(fi.statisticsDate) = 0 OR YEAR(fi.statisticsDate) IS NULL
                        GROUP BY YEAR(fi.statisticsDate), f.active) AS tb 
                GROUP BY tb.active";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        array_map(
            function ($row) use($config, &$draw, &$params){

                if($row['active'] == 1){

                    if($config){

                        $params[] = "protoplan_without_statisticsDate_fairs_published.label fairs N/O active";
                        $params[] = "protoplan_without_statisticsDate_fairs_published.draw {$draw}";
                    } else{

                        echo "protoplan_without_statisticsDate_fairs_published.value {$row['count']}\n";
                    }
                } elseif($row['active'] == 0) {

                    if($config){

                        $params[] = "protoplan_without_statisticsDate_fairs_unpublished.label fairs N/O inactive";
                        $params[] = "protoplan_without_statisticsDate_fairs_unpublished.draw {$draw}";
                    } else{

                        echo "protoplan_without_statisticsDate_fairs_unpublished.value {$row['count']}\n";
                    }
                }

                $draw = 'STACK';
            },
            $rows
        );

        if($config){

            echo implode("\n", $params);
        }
    }

    public function actionAgrosalon($configMode = 0){

        if($configMode){
            $params = [
                "graph_title Protoplan - Agrosalon 2016",
                "graph_vlabel count",
                "graph_printf %6.3lf",
                "protoplan_exponents_count.label exponents",
                "protoplan_exponents_authorized.label exponents authorized",
                "protoplan_exponents_application_forms.label exponents application forms",
                "protoplan_proposal_count.label forms total",
                "protoplan_proposal_sent.label forms sent",
                "protoplan_proposal_accepted.label forms accepted",
                "protoplan_proposal_rejected.label forms rejected",
            ];
            echo implode("\n", $params);
            exit;
        }

        $sql = "
              SELECT COUNT(*) AS `count`
              FROM tbl_organizerexponent oe
              WHERE oe.organizerId = 3563 AND oe.userId NOT IN (823, 838,848,1013)
        ";

        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_exponents_count.value ".$row['count']."\n";

        ///////

        $sql = "
              SELECT COUNT(*) AS `count`
              FROM orgplan_proposal.tbl_organizerexponent oe
              LEFT JOIN tbl_user u ON oe.userId = u.id
              WHERE u.lastEntry > '0000-00-00' AND oe.id NOT IN (25,26,29,144,148,145,146,147,149) AND oe.organizerId = 3563
        ";

        $row = Yii::app()->db->createCommand($sql)->queryRow();
        echo "protoplan_exponents_authorized.value ".$row['count']."\n";

        ///////

        $sql = "
              SELECT COUNT(DISTINCT(pe.userId)) AS `count`
              FROM tbl_proposalelement pe
              LEFT JOIN tbl_organizerexponent oe ON pe.userId = oe.userId
              LEFT JOIN tbl_proposalelementclass pec ON pe.class = pec.id
              WHERE oe.userId IS NOT NULL AND pe.id NOT IN (377,378,379,386) AND pe.userId NOT IN (823,848,1013) AND oe.organizerId = 3563
        ";

        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_exponents_application_forms.value ".$row['count']."\n";

        ///////

        $sql = "
              SELECT COUNT(*) `count`
              FROM tbl_proposalelement pe
              LEFT JOIN tbl_organizerexponent oe ON pe.userId = oe.userId
              LEFT JOIN tbl_proposalelementclass pec ON pe.class = pec.id
              WHERE oe.userId IS NOT NULL AND pe.id NOT IN (377,378,379,386) AND pe.userId NOT IN (823,848,1013) AND oe.organizerId = 3563
        ";

        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_proposal_count.value ".$row['count']."\n";

        ///////

        $sql = "
              SELECT COUNT(*) `count`
              FROM tbl_proposalelement pe
              LEFT JOIN tbl_organizerexponent oe ON pe.userId = oe.userId
              LEFT JOIN tbl_proposalelementclass pec ON pe.class = pec.id
              WHERE oe.userId IS NOT NULL
              AND pe.id NOT IN (377,378,379,386)
              AND pe.userId NOT IN (823,848,1013)
              AND oe.organizerId = 3563
              AND pe.status = 2
        ";

        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_proposal_sent.value ".$row['count']."\n";

        ///////

        $sql = "
              SELECT COUNT(*) `count`
              FROM tbl_proposalelement pe
              LEFT JOIN tbl_organizerexponent oe ON pe.userId = oe.userId
              LEFT JOIN tbl_proposalelementclass pec ON pe.class = pec.id
              WHERE oe.userId IS NOT NULL
              AND pe.id NOT IN (377,378,379,386)
              AND pe.userId NOT IN (823,848,1013)
              AND oe.organizerId = 3563
              AND pe.status = 4
        ";

        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_proposal_accepted.value ".$row['count']."\n";

        ///////

        $sql = "
              SELECT COUNT(*) `count`
              FROM tbl_proposalelement pe
              LEFT JOIN tbl_organizerexponent oe ON pe.userId = oe.userId
              LEFT JOIN tbl_proposalelementclass pec ON pe.class = pec.id
              WHERE oe.userId IS NOT NULL
              AND pe.id NOT IN (377,378,379,386)
              AND pe.userId NOT IN (823,848,1013)
              AND oe.organizerId = 3563
              AND pe.status = 3
        ";

        $row = Yii::app()->dbProposal->createCommand($sql)->queryRow();
        echo "protoplan_proposal_rejected.value ".$row['count']."\n";
    }

    public function actionFairStatisticYear($config = 0){
        $params = array(
            "graph_category protoplan_db",
            "graph_title Protoplan - FairStatistic",
            "graph_vlabel count",
            "graph_printf %6.3lf",
        );
        $draw = 'AREA';
        $sql = "SELECT YEAR(f.beginDate) AS fairYear, 
                YEAR(fi.statisticsDate) AS fairStatisticYear,
                COUNT(f.id) AS fairCount
                FROM tbl_fair f
                LEFT JOIN tbl_fairinfo fi ON f.infoId = fi.id
                WHERE f.active = 1
                GROUP BY YEAR(f.beginDate), YEAR(fi.statisticsDate)
                ORDER BY fairYear, fairStatisticYear";
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        array_map(
            function ($data) use($config, &$draw, &$params){

                if($config){
                    $params[] = "protoplan_fair_{$data['fairYear']}_statistic_{$data['fairStatisticYear']}.label fair {$data['fairYear']}/statistic {$data['fairStatisticYear']}";
                    $params[] = "protoplan_fair_{$data['fairYear']}_statistic_{$data['fairStatisticYear']}.draw {$draw}";
                } else{
                    echo "protoplan_fair_{$data['fairYear']}_statistic_{$data['fairStatisticYear']}.value {$data['fairCount']}\n";
                }

                $draw = 'STACK';
            },
            $data
        );

        if($config){
            echo implode("\n", $params);
        }
    }
}