<div class='custom_progress_bar simple-progress' title="<?=$epoch?>">
    <div class="progress <?=$options['progress']?>"><div class="progress-bar" style="width: <?=$percent?>%;background:<?=$color?>;"></div></div>
</div>

<?php if(false):?>
<div class="simple-line-table">
    <div class='custom_progress_bar simple-progress'>
        <div class="progress-container">
            <div class="progress <?php // echo $options['progress']?>">
                <div class="progress-bar" style="width: <?php// echo $percent?>%;background:<?php //echo $color?>;"></div>
            </div>
        </div>
        <div class="value"><?php //echo $value?></div>
    </div>
</div>
<?php endif;?>