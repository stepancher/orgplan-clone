<div class='custom_progress_bar' title="<?=$epoch?>">
    <h3 class="panel-title line_layout">
        <table style="height:100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td align="left" valign="bottom">
                    <?=$name?>
                </td>
            </tr>
        </table>
    </h3>
    <div class="stat-info">
        <h3 class="value"><span class="digit"><?=$value?></span> <?php echo AnalyticsService::filterUnits($valueType);?></h3>
        <div class="rating-place"><?=$ratingPlace?></div>
        <div class="rating-icon"></div>
    </div>
    <div class="progress <?=$options['progress']?>"><div class="progress-bar" style="width: <?=$percent?>%;background:<?=$color?>;"></div></div>
</div>