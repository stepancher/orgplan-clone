<div class='custom_progress_bar simple-progress' title="<?=$epoch?>">
    <div class="value"><?=$value?></div>
    <div class="progress <?=$options['progress']?>"><div class="progress-bar" style="width: <?=$percent?>%;background:<?=$color?>;"></div></div>
</div>