<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.03.14
 * Time: 19:26
 */
class TimeLineTrackerLogRoute extends CLogRoute
{
	/**
	 * Project name
	 * @var string
	 */
	public $project;

	/**
	 * Request URL
	 * @var
	 */
	public $requestUrl;

	/**
	 * Sends log messages to specified email addresses.
	 * @param array $logs list of log messages
	 */
	protected function processLogs($logs)
	{
		$ch = curl_init();
		curl_setopt_array($ch,
			array(
				CURLOPT_URL => $this->requestUrl,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/x-www-form-urlencoded',
					'Content-Charset: UTF-8',
				),
				CURLOPT_POST => 1,
			)
		);
		// Устанавливаем параметры сессии
		foreach ($logs as $log) {
			$data = json_encode(array(
				'project' => $this->project,
				'message' => $log[0],
				'level' => $log[1],
				'category' => $log[2],
				'timestamp' => $log[3]
			));
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(
					array(
						'payload' => $data
					)
				)
			);
			curl_exec($ch);
		}
		curl_close($ch);
	}
} 