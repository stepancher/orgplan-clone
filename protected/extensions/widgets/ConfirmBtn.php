<?php

Yii::import('ext.widgets.Modal');

class ConfirmBtn extends Modal 
{
    /**
     * @throws CException
     */

    public $modalId        = 'modal-remove';
    public $header    = '<span class="header-offset h1-header">Внимание!</span>';
    public $body      = '';
    public $path      = '';
    public $after     = '';
    public $ok        = 'Удалить';
    public $cancel    = 'Отмена';
    public $class     = '';
    public $label     = '';
    public $id        = '';
    

    public function run()
    {
        $modal = $this->widget('bootstrap.widgets.TbModal', array(
            'id' => $this->modalId,
            'closeText' => '<i class="icon icon--white-close"></i>',
            'htmlOptions' => array(
                'class' => 'modal--sign-up modal-danger'
            ),
            'header' => $this->header,
            'content' => $this->renderContent()
        ), true);

        $js = $this->registerClientScript([
            'new Protoplan.Modal().confirm({
                confirmText: "' . $this->body . '",
                path: "' . $this->path . '",
                id: "' . $this->id . '",
                after: ' . $this->after . '
            });'
        ]);

        echo
            '<label id="' . $this->id . '" data-target="#' . $this->modalId . '" class="' . $this->class . '" style="background:#fa755a !important; margin-left: 10px;">'
            .$this->label.
            '</label>' . $modal . $js;
    }

    private function renderContent()
    {
        return
            '<span>' . $this->body . '</span>
            <button class="btn btn-red js-modal-ok">' . $this->ok . '</button>
            <button class="btn btn-default js-modal-cancel">' . $this->cancel . '</button>';
    }
}