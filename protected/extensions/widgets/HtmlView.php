<?php
/**
 * File HtmlView.php
 */

/**
 * Class HtmlView
 */
class HtmlView extends CWidget
{
	/**
	 * Содержимое вида, может быть передана функция
	 * @var string
	 */
	public $content;

	/**
	 * Запуск отрисовки
	 */
	public function run()
	{
		if (is_callable($this->content)) {
			call_user_func($this->content, $this->getOwner());
		}
		else {
			echo $this->content;
		}
	}
}