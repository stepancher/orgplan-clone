<?php

/**
 * Class HeaderMenu
 */
class HeaderMenuProfile extends CWidget
{
    public $publishUrl;
    public $app;
    public $fairId;

    public function run()
    {
        $avatar = H::getImageUrl(User::model()->findByPk(Yii::app()->user->id), 'logo', 'noAvatar');

        return [
            'label' => '<img src="' . $avatar . '" class="avatar">',
            'class' => 'bootstrap.widgets.TbNav header-menu__item header-menu_profile',
            'url' => $this->app->createUrl('auth/default/logout'),
            'visible' => !Yii::app()->user->isGuest,
            'type' => TbHtml::NAV_TYPE_LIST,
            'encodeLabel' => false,
            'items' => [
                [
                    'label' => '<i class="icon"></i><span>' . Yii::t('site', 'profile') . '</span>',
                    'url' => Yii::app()->user->getState('role') == User::ROLE_ORGANIZERS ?
                        '/acamar/'.Yii::app()->language.'/organizer' :
                        $this->app->createUrl('profile/'),
                ],
                [
                    'label' => '<i class="icon"></i><span>Управление</span>',
                    'url' => $this->app->createUrl('admin/list/index'),
                    'visible' => Yii::app()->user->role == User::ROLE_ADMIN,
                ],
                [
                    'label' => '<i class="icon"></i><span>' . Yii::t('system', 'Exit')  . '</span>',
                    'url' => $this->app->createUrl('auth/default/logout'),
                ],
            ],
        ];
    }
}

