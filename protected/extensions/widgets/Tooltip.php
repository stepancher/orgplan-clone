<?php
/**
 * Class Tooltip
 */
class Tooltip extends CWidget
{
    public $id = 0;
    public $top = 10;
    public $right = -510;
    public $width = 310;
    public $target = '';
    public $content = '';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->renderView();
        $this->registerClientScript();
    }

    private function renderView()
    {
        $top = $this->top . 'px';
        $id = $this->getId();
        $right = $this->right . 'px';
        $width = $this->width . 'px';
        $content = $this->content;

        echo "
<div 
    style=\"
        position: absolute; 
        top: $top; 
        right: $right; 
        width: $width; 
        display: none;\" 
    class=\"p-tooltip-$id p-tooltip\">
    
    $content
    
</div>
";
    }
    
    public function getOptions() 
    {
        return [
            'target' => $this->getTarget(),
            'selector' => '.p-tooltip-' . $this->getId(),
        ];
    }

    private function registerClientScript()
    {
        $app = Yii::app();
        $cs = $app->getClientScript();

        $options = CJavaScript::encode($this->getOptions());

        $cs->registerScript($this->getId(), "new Protoplan.Tooltip({$options});", CClientScript::POS_READY);
    }

    /**
     * @return int
     */
    public function getId($autoGenerate = true)
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return int
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return int
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }
}
