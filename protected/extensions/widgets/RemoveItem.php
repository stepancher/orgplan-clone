<?php

Yii::import('ext.widgets.Modal');

class RemoveItem extends Modal 
{
    /**
     * @throws CException
     */

    public $body = '';
    public $path = '';
    public $getParams = '';
    public $after = '';
    public $itemId = '';
    
    public function run() 
    {
        $this->widget('bootstrap.widgets.TbModal', array(
            'id' => 'modal-remove',
            'closeText' => '<i class="icon icon--white-close"></i>',
            'htmlOptions' => array(
                'class' => 'modal--sign-up modal-danger'
            ),
            'header' => '<span class="header-offset h1-header">Внимание!</span>',
            'content' => '
                    <span>' . $this->body . '</span>
                    <button class="btn btn-red js-modal-ok">Удалить</button>
                    <button class="btn btn-default js-modal-cancel">Отмена</button>
                    '
        ));
        
        $this->registerClientScript([
            'new Protoplan.Modal().confirm({
                lang: "' . Yii::app()->getLanguage() . '",
                confirmText: "' . $this->body . '",
                path: "' . $this->path . '",
                getParams: "' . $this->getParams . '",
                after: ' . $this->after . ',
                itemId: "' . $this->itemId . '"
            });'
        ]);
    }
}