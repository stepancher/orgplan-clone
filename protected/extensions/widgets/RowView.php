<?php
/**
 * File RowView.php
 */

/**
 * Class RowView
 */
class RowView extends CWidget
{

	public $tagName = 'div';

	public $widgetCss = 'widget-row-view';
	public $widgetItemCss = 'widget-row-view-item';
	public $widgetItemWrapCss = 'widget-row-view-wrap-items';
	public $htmlOptions = array(
		'class' => 'row-fluid'
	);

	public $items = array();
	public $itemsTagName = 'div';
	public $itemsOptions = array();

	public $itemsWrapTagName = 'div';
	public $itemsWrapOptions = array();

	/**
	 * Initializes the widget.
	 * This method is called by {@link CBaseController::createWidget}
	 * and {@link CBaseController::beginWidget} after the widget's
	 * properties have been initialized.
	 */
	public function init()
	{
		TbHtml::addCssClass($this->widgetCss, $this->htmlOptions);
		TbHtml::addCssClass('widget-' . get_class($this), $this->htmlOptions);
		TbHtml::addCssClass($this->widgetItemCss, $this->itemsOptions);
		TbHtml::addCssClass($this->widgetItemWrapCss, $this->itemsWrapOptions);
	}

	public function run()
	{
		echo CHtml::openTag($this->tagName, $this->htmlOptions) . "\n";

		$this->renderContent();

		echo CHtml::closeTag($this->tagName);
	}

	public function renderContent()
	{
		$this->renderItems();
	}

	public function renderItems()
	{
		if (!empty($this->items)) {
			echo CHtml::openTag($this->itemsWrapTagName, $this->itemsWrapOptions) . "\n";
			foreach ($this->items as &$item) {
				if (!empty($item)) {
					$cls = $item[0];
					unset($item[0]);

					echo CHtml::openTag($this->itemsTagName, $this->itemsOptions) . "\n";

					$this->getOwner()->widget($cls, $item);

					echo CHtml::closeTag($this->itemsTagName);
				}
			}
			echo CHtml::closeTag($this->itemsWrapTagName);
		}
	}
}