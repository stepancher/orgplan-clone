<?php

class Modal extends TbModal {
    /**
     * @param array $scripts
     * @return null
     * @throws CException
     */
    public function registerClientScript($scripts = [])
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        $am = $app->getAssetManager();
        $cs = $app->getClientScript();
        $publishUrl = $am->publish($app->theme->basePath . '/assets', false, -1, YII_DEBUG);
        
        foreach ($scripts as $script) {
            $cs->registerScript($this->id, $script, CClientScript::POS_END);
        }
        
        return NULL;
    }
}