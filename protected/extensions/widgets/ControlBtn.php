<?php

Yii::import('ext.widgets.Modal');

class ControlBtn extends Modal
{
    /**
     * @throws CException
     */

    public $modalId        = 'modal-reject';
    public $header    = '';
    public $body      = '';
    public $path      = '';
    public $after     = '';
    public $ok        = 'Удалить';
    public $cancel    = 'Отмена';
    public $class     = '';
    public $label     = '';
    public $id        = '';
    public $method    = 'get';
    public $type      = 'ajax';
    public $html    = '';

    public function run()
    {
        $this->ok = Yii::t('system', 'Delete');
        $this->cancel = Yii::t('system', 'Cancel');

        $modal = $this->widget('bootstrap.widgets.TbModal', array(
            'id' => $this->modalId,
            'closeText' => '<i class="icon icon--white-close"></i>',
            'htmlOptions' => array(
                'class' => 'modal--sign-up modal-danger modal hide fade'
            ),
            'header' => $this->header,
            'content' => $this->renderContent()
        ), true);

        $header = $this->header;
        $content = $this->renderContent();
        $id = $this->modalId;

        $modal =

<<<HTML
<div id="$id" class="modal--sign-up modal-danger modal hide fade"
     role="dialog"
     tabindex="-1"
     aria-hidden="false"
     style="display: none;"
     ng-class="params.name">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button"><i class="icon icon--white-close"></i></button><h3 style="text-decoration: none"><span class="header-offset h1-header">Внимание!</span></h3></div>
    <div class="modal-body">
        <span>$header</span>
        $content
    </div>
    <div class="modal-footer">
    </div>
</div>
HTML;

        $js = $this->registerClientScript([
            'new Protoplan.Modal().confirm({
                confirmText: "' . $this->body . '",
//                path: "' . $this->path . '",
                  id: "' . $this->id . '",
//                after: ' . $this->after . '
            });'
        ]);

        if($this->type == 'ajax'){

            $js = $this->registerClientScript([
                'new Protoplan.Modal().confirm({
                    confirmText: "' . $this->body . '",
                    path: "' . $this->path . '",
                    id: "' . $this->id . '",
                    after: ' . $this->after . ',
                    method: "'.$this->method.'",
                    formId:"'.$this->modalId.'_form"
                });'
            ]);
        }

        echo
            '<label id="' . $this->id . '" data-target="#' . $this->modalId . '" class="' . $this->class . '" style="background:#fa755a; margin-left: 10px;cursor:pointer;">'
            .$this->label.
            '</label>' . $modal . $js;
    }

    private function renderContent(){

        if($this->type == 'form'){
            return '<span>' . $this->body . '</span>
                <form action="'.$this->path.'" method="'.$this->method.'">
                    '.$this->html.'<br/>
                    <div class="modal-reject__button-wrapper" style="float: left;margin-left: 142px;">
                        <button class="btn btn-default js-modal-ok" type="submit" style="width: auto !important;padding-top: 12px;">' . $this->ok . '</button>
                        <a class="btn btn-red js-modal-cancel" style="width: auto !important;padding-top: 12px;">' . $this->cancel . '</a>
                    </div>
                </form>
            ';
        }

        if($this->type == 'ajax'){
            return '<span>' . $this->body . '</span>
                <form id="'.$this->modalId.'_form">
                    '.$this->html.'<br/>
                    <div class="modal-reject__button-wrapper" style="float: left;margin-left: 142px;">
                        <a class="btn btn-default js-modal-ok" style="width: auto !important;padding-top: 12px;">' . $this->ok . '</a>
                        <a class="btn btn-red js-modal-cancel" style="width: auto !important;padding-top: 12px;">' . $this->cancel . '</a>
                    </div>
                </form>
            ';
        }

        if($this->type == 'link'){
            return '<span>' . $this->body . '</span>
                '.$this->html.'<br/>
                <div class="modal-reject__button-wrapper" style="float: left;margin-left: 142px;">
                    <a class="btn btn-default js-modal-ok" href="'.$this->path.'" style="width: auto !important;padding-top: 12px;">' . $this->ok . '</a>
                    <button class="btn btn-red js-modal-cancel" style="width: auto !important;padding-top: 12px;">' . $this->cancel . '</button>
                </div>
            ';
        }
    }
}