<?php
/**
 * File ARDetailView.php
 */

/**
 * Class ARDetailView
 */

class ARDetailView
{
	/**
	 * Создаем аттрибуты для вида
	 * @param $model
	 * @param $description
	 * @return array
	 */
	static function createAttributes($model, $description)
	{
		$attributes = array();
		foreach ($description as $name => &$field) {
			if (isset($field[0])) {
				$attribute = array(
					'name' => $name,
				);
				if (isset($field['label'])) {
					$attribute['label'] = $field['label'];
				}
				if ($field[0] == 'datetime' || $field[0] == 'timestamp') {
					$attribute['value'] = date('d.m.Y H:i:s', strtotime($model->{$name}));
				}
				$attributes[$name] = $attribute;
			}
		}
		return $attributes;
	}
}