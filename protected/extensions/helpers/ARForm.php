<?php
/**
 * File ARForm.php
 */

/**
 * Class ARFormView
 */
class ARForm
{
	/**
	 * @param CController $ctrl
	 * @param AR $model
	 * @param array $description
	 * @param string $elementGroupName
	 * @return array
	 */
	static function createElements($ctrl, $model, $description, $elementGroupName)
	{
		$elements = array();
		$safeAttributes = ARForm::getScenarioSafeAttributes($model);
		foreach ($description as $name => $field) {
			if (isset($field[0])) {
				$elm = array(
					'type' => TbHtml::INPUT_TYPE_TEXT,
					'attributes' => array(
						'class' => 'input-block-level',
					)
				);

				if (isset($field['label'])) {
					$elm['attributes']['label'] = $field['label'];
				}

				if ($field[0] == 'datetime' || $field[0] == 'timestamp') {
					$elmValue = null;
					if ($model->{$name}) {
						$elmValue = date('d.m.Y H:i:s', $model->{$name} ? strtotime($model->{$name}) : time());
					}
					$elm['input'] = $ctrl->widget(
						'yiiwheels.widgets.datetimepicker.WhDateTimePicker',
						array(
							'model' => $model,
							'attribute' => $elementGroupName . $name,
							'htmlOptions' => array(
								'value' => $elmValue,
							),
							'pluginOptions' => array(
								'format' => 'dd.MM.yyyy hh:mm:ss'
							),
						),
						true
					);
				} elseif ($field[0] == 'date') {
					$elmValue = null;
					if ($model->{$name}) {
						$elmValue = date('d.m.Y', $model->{$name} ? strtotime($model->{$name}) : time());
					}
					$elm['input'] = $ctrl->widget(
						'yiiwheels.widgets.datepicker.WhDatePicker',
						array(
							'model' => $model,
							'attribute' => $elementGroupName . $name,
							'htmlOptions' => array(
								'value' => $elmValue,
							),
							'pluginOptions' => array(
								'format' => 'dd.mm.yyyy'
							),
						),
						true
					);
				} elseif ($field[0] == 'bool' || $field[0] == 'boolean') {
					$elm['type'] = TbHtml::INPUT_TYPE_CHECKBOX;
					$elm['class'] = '';
				}
				if (isset($field['relation'], $field['relation'][0]) && $field['relation'][0] == AR::BELONGS_TO) {
					$elm['type'] = TbHtml::INPUT_TYPE_DROPDOWNLIST;
					$elm['empty'] = Yii::t('global', 'Select');
				}

				$elm['visible'] = in_array($name, $safeAttributes);

				$elements[$name] = $elm;
			}
		}

		return $elements;
	}

	/**
	 * @param AR $model
	 * @return array
	 */
	static function getScenarioSafeAttributes($model)
	{
		$attributes = array();
		$scenario = $model->getScenario();
		foreach ($model->getValidators() as $validator) {
			if ($validator->safe) {
				if (isset($validator->on[$scenario])) {
					foreach ($validator->attributes as $name) {
						$attributes[$name] = true;
					}
				}
			}
		}
		return array_keys($attributes);
	}
}