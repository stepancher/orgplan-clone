<?php
/**
 * File ARGridView.php
 */

/**
 * Class ARGridView
 */
class ARGridView
{
	/**
	 * Создаем фильтр
	 * @param AR $model
	 * @param boolean $enable
	 * @return null
	 */
	static function createFilter($model, $enable)
	{
		$filter = null;
		if ($enable) {
			$filter = $model;
			if (isset($_GET[get_class($model)])) {
				$filter->setAttributes(
					$_GET[get_class($model)]
				);
			}
		}
		return $filter;
	}

	/**
	 * Создаем провайдер данных
	 * @param CActiveDataProvider|IDataProvider $dataProvider
	 * @param AR $model
	 * @param string $providerMethod
	 * @param array $compare
	 * @return CActiveDataProvider
	 */
	static function createDataProvider($dataProvider, $model, $providerMethod, $compare)
	{
		$criteria = new \CDbCriteria();
		foreach ($compare as $compareArgs) {
			if (!empty($compareArgs)) {
				call_user_func_array(array($criteria, 'compare'), $compareArgs);
			}
		}
		if (!$dataProvider) {
			$dataProvider = call_user_func(array($model, $providerMethod), $criteria);
		} else {
			$dataProvider->getCriteria()->mergeWith($criteria);
		}
		return $dataProvider;
	}

	/**
	 * Создаем колонки таблицы
	 * @param array $columns
	 * @param array $prepend
	 * @param array $append
	 * @param array $compare
	 * @return array
	 */
	static function createColumns($columns, $prepend, $append, $compare)
	{
		return self::addColumns(
			CMap::mergeArray(
				$columns, self::getColumnsByCompare($compare)
			),
			$prepend, $append
		);
	}

	/**
	 * Скрываем колонки попадающие под условие compare
	 * @param array $compare
	 * @return array
	 */
	static function getColumnsByCompare($compare)
	{
		$columns = array();
		foreach ($compare as $compareArgs) {
			if (!empty($compareArgs)) {
				$columns[$compareArgs[0]] = array(
					'name' => $compareArgs[0],
					'visible' => false
				);
			}
		}
		return $columns;
	}

	/**
	 * Добавляем дополнительные колонки в начало и колнец таблицы
	 * @param array $columns
	 * @param array $prepend
	 * @param array $append
	 * @return array
	 */
	static function addColumns($columns, $prepend, $append)
	{
		if ($prepend) {
			$columns = $prepend + $columns;
		}
		if ($append) {
			$columns += $append;
		}
		return $columns;
	}}