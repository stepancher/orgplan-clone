<?php
/**
 * Синхронизация БД на основе описания свойств моделей
 * @author Igor Sapegin aka Rendol sapegin.in@gmail.com
 */
/**
 * Class DbSyncCommand
 */
class DbSyncCommand extends CConsoleCommand {

	/**
	 * @var string
	 */
	public $config;

	/**
	 * Запуск
	 * @param array $args
	 * @return int|void
	 */
	public function run($args) {
		Yii::app()->getModule('WebService');

		if (!$this->config) {
			$this->config =\Yii::getPathOfAlias('application.config.dbsync').'.php';
		}
		if (!is_array($this->config)) {
			$this->config = include($this->config);
		}
		$classes = $this->config;

		foreach ($classes as $cls) {
			if (!is_string($cls) && is_callable($cls)) {
				call_user_func($cls);
			}
			else {
				/** @var $cls string|AR */
				$this->process(new $cls(null));
			}
		}
	}

	/**
	 * Синхронизация таблицы класса
	 * @param AR $model
	 * @return array
	 */
	static function process($model) {
		/**
		 * Проверяем наличие явно заданного первичного ключа
		 */
		$pkExists   = false;
		$properties = $model->description();
		foreach ($properties as $prop) {
			if (isset($prop[0]) && FALSE !== stristr(strtoupper($prop[0]), 'PRIMARY KEY')) {
				$pkExists = true;
			}
		}
		/**
		 * Создаем запись о первичном ключе
		 */
		if (!$pkExists && $model->primaryKey()) {
			$properties = array_merge(
				array(
					array(
						'PRIMARY KEY ('.implode(',', $model->primaryKey()).')'
					)
				),
				$properties
			);
		}

		$fields = array();
		foreach ($properties as $name=>$params) {
			if (isset($params[0])) {
				$notNull = '';
				if (isset($params['notNull'])) {
					$notNull =  ' NOT NULL ';
				}
				$fields[ $name ] = $params[0].$notNull;
			}
		}

		static::dbTableUpdate($model, $fields);
	}

	/**
	 * Обновление структуры таблицы
	 * @param AR $model
	 * @param array $fields
	 */
	static function dbTableUpdate($model, $fields) {
		$db = $model->getDbConnection();
		$tableSchema = static::dbTableSchema(
			$model->getDbConnection(),
			$model->tableName()
		);

		/**
		 * SQL Log
		 */
		$sql = array();

		$tblName = $tableSchema->name;

		// Создание таблицы
		if (!count($tableSchema->columns)) {
			if (empty($fields)) {
				return;
			}
			elseif (count($fields) == 1 && isset($fields['id'])) {
				return;
			}
			$sql[] = $query = $db->getSchema()->createTable($tblName, $fields, 'ENGINE = INNODB DEFAULT CHARSET = utf8');
			$db->createCommand()->setText($query)->execute();
			print 'CREATE TABLE: '.$tblName."\r\n";
		}
		// Проверка изменений
		else {
			// Сравниваем данные ОПИСАНИЯ с данными ТАБЛИЦЫ
			$columnsDesc  = array_keys($fields);
			$columnsDesc  = array_filter($columnsDesc, function($val){
				return intval($val) === 0 && $val !== 0;
			});
			$columnsTable = $tableSchema->getColumnNames();

			/**
			 * Ищем новые колонки в ОПИСАНИИ
			 */
			$newFields = array();
			foreach ($columnsDesc as $index=>$name) {
				if (!in_array($name, $columnsTable)) {
					$newFields[$index] = $name;
				}
			}
			/**
			 * Ищем удаленные колонки из ТАБЛИЦЫ
			 */
			$delFields = array();
			foreach ($columnsTable as $index=>$name) {
				if (!in_array($name, $columnsDesc)) {
					$delFields[$index] = $name;
				}
			}

			/**
			 * Проверяем на вариант переименовывания
			 */
			foreach ($delFields as $index=>$name) {
				if (isset($newFields[$index])) {
					// Колонка переименована
					$renameFields[$name] = $newFields[$index];
					unset($newFields[$index]);
					unset($delFields[$index]);
				}
				else {
					// Значит колонка удалена
				}
			}

			/**
			 * Либо переименовываем
			 */
			if (!empty($renameFields)) {
				foreach ($renameFields as $oldName => $newName) {
					$sql[] = $query = $db->getSchema()->renameColumn($tblName, $oldName, $newName);
					$db->createCommand()->setText($query)->execute();
				}
				print 'RENAME FIELDS FOR TABLE: '.$tblName."\r\n";
			}
			/**
			 * Либо добавляем/удаляем
			 */
			else {
				// Добавляем
				if (!empty($newFields)) {
					foreach ($newFields as $index=>$name) {
						$sql[] = $query = $db->getSchema()->addColumn($tblName, $name, $fields[$name], isset($columnsDesc[$index-1]) ? $columnsDesc[$index-1] : null);
						$db->createCommand()->setText($query)->execute();

					}
					print 'NEW FIELDS '.implode(',',$newFields).' FOR TABLE: '.$tblName."\r\n";
				}
				// Удаляем таблицу
				if (count($columnsTable) == count($delFields) || (count($columnsTable) == 1 && $columnsTable[0] == 'id')) {
					$sql[] = $query = $db->getSchema()->dropTable($tblName);
					$db->createCommand()->setText($query)->execute();
					print 'DROP TABLE '.$tblName."\r\n";
				}
				// Удаляем колонки
				elseif (!empty($delFields)) {
					foreach ($delFields as $name) {
						$sql[] = $query = $db->getSchema()->dropColumn($tblName, $name);
						$db->createCommand()->setText($query)->execute();
					}
					print 'DEL FIELDS '.implode(',',$delFields).' FOR TABLE: '.$tblName."\r\n";
				}
			}

		}
		if (!empty($sql)) {
			/**
			 * Создаем SQL-патч
			 */
			//*
			$sqlLogDir = Yii::getPathOfAlias('application.runtime.sql');
			if (!is_dir($sqlLogDir)) {
				mkdir($sqlLogDir, 0777, true);
			}
			$fo = fopen($sqlLogDir.'/'.time().'_'.$tblName.'.sql', 'a+');
			fwrite($fo, ' -- Creation date: '.date('Y-m-d H:i:s')."\r\n");
			fwrite($fo, implode(";\r\n", $sql).";\r\n");
			fclose($fo);
			//*/
		}
	}

	/**
	 * Имя таблицы в разобранном виде
	 * @param CDbConnection $db
	 * @param $tblName
	 * @return array
	 */
	static public function dbTableName($db, $tblName) {
		$parts = array('', $tblName);
		if ($db->tablePrefix !== null) {
			if (stristr($tblName, $db->tablePrefix)) {
				$parts[0] = $db->tablePrefix;
				$parts[1]  = str_replace($db->tablePrefix, '', $tblName);
			}
			elseif (strpos($tblName,'{{')!==false) {
				$parts[0] = $db->tablePrefix;
				$parts[1]  = preg_replace('/\{\{(.*?)\}\}/', '$1', $tblName);
			}
		}
		return $parts;
	}

	/**
	 * Схема таблицы
	 * @param CDbConnection $db
	 * @param $tblName
	 * @return CDbTableSchema
	 */
	static function dbTableSchema($db, $tblName) {
		$tableSchema = $db->getSchema()->getTable($tblName, true);
		if (!$tableSchema) {
			$tableSchema = new CDbTableSchema();
			$tableSchema->name = implode('', static::dbTableName($db, $tblName));
		}
		return $tableSchema;
	}
}
