<?php
/**
 * class CorrValidator
 */

class CorrValidator extends CValidator{
    public $bik;

    protected function validateAttribute($object,$attribute)
    {
        if ($object->{$this->bik}) {

            if (substr($object->{$this->bik},6,3) != substr($object->$attribute,17,3)) {
                $this->addError($object,$attribute,'Неверный Кор/счет.');
            } else {
                return true;
            }
        } else {
            $this->addError($object,$attribute,'Для проверки Кор/счет необходим БИК.');
            return false;
        }
    }
} 