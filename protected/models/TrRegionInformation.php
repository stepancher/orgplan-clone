<?php
/**
 * Class TrRegionInformation
 */
class TrRegionInformation extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'label' => 'Информация о регионе',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'RegionInformation',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'string',
				'label' => 'Язык',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'text' => array(
				'label' => 'Описание',
				'text',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'descriptionSnippet' => array(
				'label' => 'descriptionSnippet',
				'text',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}