<?php
/**
 * Class FairStatus
 */

class FairStatus extends \AR
{
	public $id;
	public $status;
	public $color;
	
	const STATUS_CANCEL = 10;

	public function tableName()
	{
		return '{{fairstatus}}';
	}

	public function rules()
	{
		return array(
			array('id,
			status, 
			color',
				'safe', 'on' => 'search,insert,update'),
		);
	}

	/**
	 * @return array
	 */
	public static function getFairStatuses(){
		/** @noinspection PhpUndefinedClassInspection */
		return CHtml::listData(self::model()->findAll(),'id','status');
	}
}