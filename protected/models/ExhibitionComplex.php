<?php

/**
 * Class ExhibitionComplex
 */
class ExhibitionComplex extends \AR
{
    public static $_description = NULL;

    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;
    const ASSOCIATION_VARIABLE = 'fairAssociation';
    const CITY_VARIABLE = 'cityId';

    /** Search properties  */
    public $cityName = '';
    public $regionName = '';
    public $districtName = '';
    public $exhibitionComplexTypeName = '';
    public $mainImageUrl = '';
    public $fairsCount = '';
    public $associationName = '';
    public $statusName = '';
    public $uploads;
    public $_uniqueText = NULL;
    public $_street = NULL;
    public $_descriptionSnippet = NULL;
    protected $_name = NULL;
    protected $_shortUrl = NULL;

    /**
     * @var string
     */
    public $searchableDefaultSort = ['square' => 'desc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'name' => 'name',
        'square' => 'square',
        'category' => 'exhibitionComplexTypeName',
        'region' => 'regionName',
        'city' => 'cityName',
    ];

    /**
     * Нужен для формирования title и description в SEO по вкладкам
     * @var array
     */
    public static $tabsName = [
        'venueContacts' => 'Контакты',
        'venueMap' => 'Карта',
        'venueRegion' => 'Регион',
        'venueReviews' => 'Отзывы',
    ];

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        $prop = 'shortUrl';
        ${$prop} = '';
        if($this->{'_'.$prop} !== NULL){
            ${$prop} = $this->{'_'.$prop};
        }else{
            $translate = $this->translate;
            if(isset($translate->{$prop})) {
                $this->{'_'.$prop} = $translate->{$prop};
                ${$prop} = $this->{'_'.$prop};
            }
        }

        return ${$prop};
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        $street = '';
        if($this->_street !== NULL){
            $street = $this->_street;
        }else{
            $translate = $this->translate;
            if(isset($translate->street)) {
                $this->_street = $translate->street;
                $street = $this->_street;
            }
        }

        return $street;
    }

    /**
     * @return string
     */
    public function getUniqueText()
    {
        $uniqueText = '';
        if($this->_uniqueText !== NULL){
            $uniqueText = $this->_uniqueText;
        }else{
            $translate = $this->translate;
            if(isset($translate->uniqueText)) {
                $this->_uniqueText = $translate->uniqueText;
                $uniqueText = $this->_uniqueText;
            }
        }

        return $uniqueText;
    }

    /**
     * @return string
     */
    public function getDescriptionSnippet()
    {
        $descriptionSnippet = '';
        if($this->_descriptionSnippet !== NULL){
            $descriptionSnippet = $this->_descriptionSnippet;
        }else{
            $translate = $this->translate;
            if(isset($translate->descriptionSnippet)) {
                $this->_descriptionSnippet = $translate->descriptionSnippet;
                $descriptionSnippet = $this->_descriptionSnippet;
            }
        }

        return $descriptionSnippet;
    }

    /**
     * @param $name
     */
    public function setName($name){
        $this->_name = $name;
    }

    public function setShortUrl($value){
        $prop = 'shortUrl';
        $this->{'_'.$prop} = $value;
    }

    /**
     * @param $street
     */
    public function setStreet($street){
        $this->_street = $street;
    }

    /**
     * @param $uniqueText
     */
    public function setUniqueText($uniqueText){
        $this->_uniqueText = $uniqueText;
    }

    /**
     * @param $descriptionSnippet
     */
    public function setDescriptionSnippet($descriptionSnippet){
        $this->_descriptionSnippet = $descriptionSnippet;
    }

    public static function getActive($active = null)
    {
        if (null != $active) {
           $data= self::getActive();
            if(isset($data[$active])) {
                return $data[$active];
            }
        }

        return array(
            self::ACTIVE_ON => Yii::t('fair', 'Active'),
            self::ACTIVE_OFF => Yii::t('fair', 'Not active')
        );
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            ),
            'SitemapBehavior' => array(
                'class' => 'application.components.SitemapBehavior'
            ),
        );
    }

    /**
     * List of properties stored in related tr-table.
     */
    public function trAttributeNames(){
        return [
            'name',
            'street',
            'descriptionSnippet',
            'uniqueText',
            'shortUrl',
        ];
    }

    public function updateShortUrl(){
        $this->shortUrl = $this->getSeoNameForNewVersion();
    }

    public function getSeoNameForNewVersion(){

        $cityName = '';
        $exhibitionComplexName = '';

        if(isset($this->city)){
            $cityName = $this->city->name;
        }

        $trExhibitionComplex = TrExhibitionComplex::model()->findByAttributes(
            [
                'trParentId' => $this->id,
                'langId' => Yii::app()->language
            ]
        );

        if($trExhibitionComplex !== NULL){
            $exhibitionComplexName = $trExhibitionComplex->name;
        }

        $shortUrl = ToTransliteration::getInstance()->replace($exhibitionComplexName.'-'.$cityName);
        return $shortUrl;
    }

    static function clearSeoTemplate($result, $isTemplate = false)
    {
        if (!$isTemplate) {
            $result = preg_replace('#\{.*\}#', '', $result);
        }
        return $result;
    }

    public function getSeoTitle($isTemplate = false)
    {
        return static::clearSeoTemplate(
            $this->exhibitionComplexType
                ? $this->exhibitionComplexType->fullNameSingle . ' "' . $this->name . '"{afterContent}' .
                $this->getCityForTitle() . ' | '.Yii::app()->params['titleName']
                : '"' . $this->name . '"{afterContent}' . $this->getCityForTitle() . ' | '.Yii::app()->params['titleName'],
            $isTemplate
        );
    }

    /**
     * Если есть ВЦ с таким же именем, то возвращаем часть title с городом
     * @return string
     */
    public function getCityForTitle()
    {
        $result = '';

        $criteria = new CDbCriteria();
        $criteria->with['translate'] = [
            'together' => TRUE,
        ];
        $criteria->addCondition('t.id <> ' . $this->id);
        $criteria->compare('translate.name', $this->name);
        $model = static::model()->find($criteria);
        if (null != $model) {
            $result = $this->city ? ' - ' . $this->city->name : '';
        }

        return $result;
    }

    public function getSeoDescription()
    {
        return $this->descriptionSnippet;
    }

    public function getSeoHeader($isTemplate = false)
    {
        return static::clearSeoTemplate(
            $this->exhibitionComplexShortType
                ? $this->exhibitionComplexShortType->name . ' ' . $this->name
                : $this->name,
            $isTemplate
        );
    }

    /**
     * @return array
     */
    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'active' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Active'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'userId' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Users'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'exhibitionHasFiles' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'description' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'street' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Street'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'coordinates' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Coordinates'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'regionId' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'statusId' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Status'),
                'relation' => array(
                    'CBelongsToRelation',
                    'ExhibitionComplexStatus',
                    array(
                        'statusId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'statusName' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Status'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'cityId' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'City',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
            'square' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Area'),
                'rules' => array(
                    'search,insert,update,test' => array(
                        array('safe')
                    ),
                ),
            ),
            'members' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Number of halls'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'visitors' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Conference rooms'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'rsva' => array(
                'bool',
                'label' => Yii::t('exhibitionComplex', 'RUEF members'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'uniqueText' => array(
                'label' => Yii::t('exhibitionComplex', 'Exhibition complex description'),
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exhibitionComplexTypeId' => array(
                'integer',
                'label' => 'тип площадки',
                'relation' => array(
                    'CBelongsToRelation',
                    'ExhibitionComplexType',
                    array(
                        'exhibitionComplexTypeId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'accreditationPrice' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Price for accreditation'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'accreditationContact' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Accreditation contact'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'advertisingContact' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Advertising contact'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'standsConstructionContact' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Stands construction contact'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'cateringContact' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Catering contact'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'phone' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Phone number'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'email' => array(
                'string',
                'label' => 'Email',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'site' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'ETI website'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'postcode' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Postcode'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'imageId' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Logo'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'galleryImages' => array(
                'label' => Yii::t('exhibitionComplex', 'Logo'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'services' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Services Computing Center'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'yaMap' => array(
                'label' => Yii::t('exhibitionComplex', 'Map'),
                'rules' => array(
                    'insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'FoundationYear' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Year of foundation'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'plainHash' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Panorama'),
                'rules' => array(
                    'insert,update,search' => array(
                        array('safe')
                    ),
                ),
            ),
            'number' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Fairs'),
                'rules' => array(
                    'insert,update,search' => array(
                        array('safe')
                    ),
                ),
            ),
            'fair' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'city_name' => array(
                'label' => Yii::t('exhibitionComplex', 'City'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'region_name' => array(
                'label' => Yii::t('exhibitionComplex', 'Region'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'district_name' => array(
                'label' => Yii::t('exhibitionComplex', 'District'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'country_name' => array(
                'label' => Yii::t('exhibitionComplex', 'Country'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'descriptionSnippet' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'SEO description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exdbId' => array(
                'integer',
                'label' => Yii::t('exhibitionComplex', 'Expodatabase exhibition complex id'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exdbContact' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Expodatabase exhibition complex contact'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exdbRaw' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Expodatabase exhibition raw'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'fairsCount' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Fairs count'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'associationName' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'Association'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exhibitionComplexShortTypeId' => array(
                'integer',
                'label' => 'Короткий тип',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ExhibitionComplexShortType',
                    array(
                        'exhibitionComplexShortTypeId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplexHasNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'ExhibitionComplexHasNotification',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'selectedExhibitionComplices' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'SelectedExhibitionComplex',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplexHasFiles' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'ExhibitionComplexHasFile',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'trExhibitionComplices' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrExhibitionComplex',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrExhibitionComplex',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
            'translateRaw' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrExhibitionComplex',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'fairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Fair',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'fairPreviousYear' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Fair',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplexHasAssociation' => array(
                'label' => 'exhibitionComplexHasAssociation',
                'relation' => array(
                    'CHasManyRelation',
                    'ExhibitionComplexHasAssociation',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'datetimeModified' => array(
                'timestamp',
                'label' => Yii::t('exhibitionComplex', 'datetimeModified'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'lastRedactorEmail' => array(
                'string',
                'label' => Yii::t('exhibitionComplex', 'lastRedactorId'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    ),
                ),
            ),
            'logoExists' => array(
                'label' => Yii::t('AdminModule.admin', 'Is exhibitionComplex logo loaded'),
                'string',
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    ),
                ),
            ),
        );
    }

    /**
     * Получение объекта для вывода заглавного изображения
     * @return bool|ObjectFile
     */
    public function getMainImage()
    {
        $result = false;
        if ($this->exhibitionComplexHasFiles) {
            foreach ($this->exhibitionComplexHasFiles as $file) {
                if ($file->file && $file->file->type == ObjectFile::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE) {
                    $result = $file;
                }
            }
        }
        return $result;
    }

    /**
     * Получение емайл ссылок из строки с емайломи
     * @param $emailString
     * @return string
     */
    public static function getMailLinks($emailString)
    {
        $parts = explode(',', $emailString);
        foreach ($parts as $k => $part) {
            if (stristr($part, '@')) {
                $trimmed = trim($part);
                $parts[$k] = '<span itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="email">
                    <a rel="nofollow" href="mailto:' . $trimmed . '">' . $trimmed . '</a>
                    </span>
                </span>';
            } else {
                $trimmed = trim($part);
                $parts[$k] =  '<span itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="telephone">' . $trimmed . '</span>
                </span>';
            }
        }
        return implode(', ', $parts);
    }

    public function getSeoName()
    {
        $nameParts = [];


        $params = Yii::app()->controller->actionParams;
        if (in_array(Yii::app()->controller->action->id,
                    ['view', 'venueMap', 'venueContacts', 'venueRegion', 'venueReviews']
                )
            || (!empty($params) && isset($params['sector']) && $params['sector'] == 'exhibitionComplex')
        ) {
            if ($this->cityId && $this->city) {
                $nameParts[] = null != $this->city->shortUrl
                    ? $this->city->shortUrl
                    : 'city-' . $this->city->id;
            }
            $nameParts[] = 'venues';
        }

        $cityForName = '';
        if ($this->checkDubbingName()) {
            if(isset($this->city) && !empty($this->city->name)) {
                $cityForName = '-' . $this->city->name;
            }
        }

        $name = ToTransliteration::getInstance()->replace($this->name . $cityForName);
        if (empty($name)) {
            $name = 'exhibition-center-' . $this->id;
        }
        $nameParts[] = ToTransliteration::getInstance()->replace($name);

        return implode('/', $nameParts);
    }

    /**
     * Проверяем, есть ли ВЦ с таким же именем в другом городе.
     * @return bool
     */
    public function checkDubbingName()
    {
        $criteria = new CDbCriteria();
        $criteria->select = 't.id AS id';
        $criteria->with['translate'] = [
            'together' => TRUE,
        ];
        $criteria->compare('translate.name', $this->name);
        $criteria->compare('t.id', '<>'.$this->id);
        if(!empty($this->cityId)){
            $criteria->compare('t.cityId', '<>'.$this->cityId);
        }

        $model = static::model()->find($criteria);

        return null != $model;
    }

    public function getRevertCoordinates()
    {
        $result = '';
        if(!empty($this->coordinates)) {
            $coordinates = explode(',', $this->coordinates);
            if (isset($coordinates[0], $coordinates[1])) {
                $result = $coordinates[1].','.$coordinates[0];
            }
        }
        return $result;
    }

    public function getFullDescription(){
        return $this->loadDescription();
    }

    /**
     * @return null|string
     */
    public function loadDescription()
    {
        return $this->loadTr('TrExhibitionComplex', 'description');
    }

    /**
     * @return string
     */
    public function loadName()
    {
        return $this->loadTr('TrExhibitionComplex', 'name');
    }

    /**
     * @return string
     */
    public function loadStreet()
    {
        return $this->loadTr('TrExhibitionComplex', 'street');
    }

    public function beforeSave()
    {
        $this->datetimeModified = new CDbExpression('CURRENT_TIMESTAMP');

        return parent::beforeSave();
    }

    /**
     * @param integer $exhibitionComplexId
     * @return bool
     */
    public static function checkWithDeactivation($exhibitionComplexId){

        $query = "SELECT f.id FROM tbl_exhibitioncomplex ec
                    LEFT JOIN tbl_fair f ON f.exhibitionComplexId = ec.id
                  WHERE ec.id = :exhibitionComplexId AND f.active = :fairActive";

        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':exhibitionComplexId' => $exhibitionComplexId, ':fairActive' => Fair::ACTIVE_ON));

        if(empty($data)){
            
            $model = ExhibitionComplex::model()->findByPk($exhibitionComplexId);

            if(!empty($model)){
                $model->active = self::ACTIVE_OFF;
                $model->save();
            }
            return TRUE;
        }
        return FALSE;
    }
}