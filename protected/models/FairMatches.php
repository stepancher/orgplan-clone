<?php
/**
 * Class FairMatches
 */
class FairMatches extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'userId' => array(
				'label' => Yii::t('fairMatches', 'User'),
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CBelongsToRelation',
					'User',
					array(
						'userId' => 'id',
					),
				),
			),
			'fairId' => array(
				'label' => Yii::t('fairMatches', 'Exhibition'),
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CBelongsToRelation',
					'Fair',
					array(
						'fairId' => 'id',
					),
				),
			),
		);
	}
}