<?php

/**
 * Class Analytics
 */
class Analytics extends \AR
{
    public static $_description = NULL;
    /**
     * @var string
     */
    public $searchableDefaultSort = ['name' => 'asc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'name' => 'name'
    ];

    public function getSeoDescription()
    {
        $description = 'В каталоге представлены 9 отраслей промышленности России. В каждой отрасли содержится краткий обзор с основными показателями развития.';

        return $description;
    }

    public function getName()
    {
        $name = null;
        if ($this->industryId && $this->industry) {
            $name = $this->industry->name;
        }
        return $name;
    }

    public function description(){
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }
        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'imageId' => array(
                'label' => Yii::t('analytics', 'Image'),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'theme' => array(
                'string',
                'label' => Yii::t('analytics', 'Theme'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'description' => array(
                'text',
                'label' => Yii::t('analytics', 'Description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industryId' => array(
                'label' => Yii::t('analytics', 'Branch'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Industry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'industryTypeId' => array(
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'integer',
                'label' => 'Active',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'image' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'trAnalytics' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrAnalytics',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
        );
    }
}