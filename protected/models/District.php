<?php
/**
 * Class District
 */
class District extends \AR
{
	protected $_name = NULL;

	public static $_description = NULL;
	
	/**
	 * @return string
	 */
	public function getName()
	{
		$name = '';
		$translate = $this->translate;
		if($this->_name !== NULL){
			$name = $this->_name;
		}elseif(isset($translate->name)) {
			$this->_name = $translate->name;
			$name = $this->_name;
		}

		return $name;
	}

	public function setName($name){
		$this->_name = $name;
	}

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'countryId' => array(
				'label' => Yii::t('district', 'Country'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'Country',
					array(
						'countryId' => 'id',
					),
				),
			),
			'shortUrl' => array(
				'label' => 'Короткая ссылка',
				'string',
			),
			'trDistricts' => array(
				'relation' => array(
					'CHasManyRelation',
					'TrDistrict',
					array(
						'trParentId' => 'id',
					),
				),
			),
			'translate' => array(
				'relation' => array(
					'CHasOneRelation',
					'TrDistrict',
					array(
						'trParentId' => 'id',
					),
					'on' => 'translate.langId = "'.Yii::app()->language.'"',
				),
			),
			'translateRaw' => array(
				'relation' => array(
					'CHasOneRelation',
					'TrDistrict',
					array(
						'trParentId' => 'id',
					),
				),
			),
			'regionInformation' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'regions' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasManyRelation',
					'Region',
					array(
						'districtId' => 'id',
					),
				),
			),
			'name' => array(
				'string',
				'label' => Yii::t('fair', 'Name'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					)
				),
			),
		);
	}

	public function getSeoName() {
		return strtolower($this->shortUrl);
	}

	public function behaviors()
	{
		return array(
			'TranslateBehavior' => array(
				'class' => 'application.components.TranslateBehavior'
			)
		);
	}
}