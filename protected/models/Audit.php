<?php

/**
 * Class Audit
 *
 * @property    integer $id
 */
class Audit extends \AR
{
    protected $_name = NULL;

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    public static $_description = NULL;

    public function description() {

        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }
        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exdbId' => array(
                'integer',
                'label' => 'exdbId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'active' => array(
                'integer',
                'label' => 'active',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrAudit',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }

    public static function listAudits(){
        return CHtml::listData(TrAudit::model()->findAll(), 'trParentId', 'name');
    }
}