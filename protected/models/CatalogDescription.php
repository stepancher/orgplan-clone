<?php

/**
 * Class CatalogDescription
 */
class CatalogDescription extends \AR
{
    public static $_description = NULL;
    public $id;
    public $industryId;
    public $countryId;
    public $regionId;
    public $year;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'industryId' => array(
                'integer',
            ),
            'countryId' => array(
                'integer',
            ),
            'regionId' => array(
                'integer',
            ),
            'year' => array(
                'integer',
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrCatalogDescription',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }

    public function getHeader()
    {
        $result = '';

        if (isset($this->translate->header)) {
            $result = $this->translate->header;
        }

        return $result;
    }

    public function getText()
    {
        $result = '';

        if (isset($this->translate->text)) {
            $result = $this->translate->text;
        }

        return $result;
    }
}