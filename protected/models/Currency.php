<?php
/**
 * Class Currency
 *
 * @property $id
 * @property $name
 * @property $course
 * @property $code
 */
class Currency extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'course' => array(
				'label' => 'Соотношение к рублю',
				'string',
			),
			'code' => array(
				'label' => 'code',
				'string',
			),
			'fair' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasOneRelation',
					'Fair',
					array(
						'currencyId' => 'id',
					),
				),
			),
		);

	}

	public static function getCurrencyIconById($id){

		$icon = '';

		if(isset($id) && !empty($id)){

			$currency = self::model()->findByPk($id);

			if(!empty($currency))
				$icon = $currency->icon;
		}
		return $icon;
	}
}