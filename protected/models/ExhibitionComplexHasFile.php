<?php
/**
 * Class ExhibitionComplexHasFile
 */
class ExhibitionComplexHasFile extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'exhibitionComplexId' => array(
				'integer',
				'label' => 'Комплекс',
				'relation' => array(
					'CBelongsToRelation',
					'ExhibitionComplex',
					array(
						'exhibitionComplexId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'fileId' => array(
				'integer',
				'label' => 'Файл',
				'relation' => array(
					'CBelongsToRelation',
					'ObjectFile',
					array(
						'fileId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}