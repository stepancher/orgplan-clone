<?php
/**
 * Class ExhibitionComplexHasNotification
 */
Yii::import('application.modules.notification.models.Notification');
class ExhibitionComplexHasNotification extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'exhibitioncomplexId' => array(
				'label' => 'Выставочный комплекс',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'ExhibitionComplex',
					array(
						'exhibitionComplexId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'notificationId' => array(
				'integer',
				'label' => 'Уведомление',
				'relation' => array(
					'CBelongsToRelation',
					'Notification',
					array(
						'notificationId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}