<?php

/**
 * Class FairHasIndustry
 * 
 * @property    integer     $id
 * @property    integer     $fairId
 * @property    integer     $industryId
 * @property    Industry    $industry   Relation to Industry model.
 */
class FairHasIndustry extends \AR
{
    public static $_description = NULL;

    /**
     * @var integer $fairCount  Summary Fair Count for industry
     */
    public $fairsCount;
    
    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fairId' => array(
                'label' => 'Выставка',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Fair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fairPreviousYear' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'Fair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'industryId' => array(
                'label' => Yii::t('system', 'Industry'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Industry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
        );
    }

    /**
     * Получение всех идентификаторов индустрий по идентификатору выставки
     * @param $fairId
     * @return array
     */
    public static function getIndustryIds($fairId)
    {
        $result = [];
		$models = self::model()->findAllByAttributes(['fairId' => $fairId]);
        if (null != $models) {
            $result = CHtml::listData($models, 'industryId', 'industryId');
        }

        return $result;
    }

    /**
     * @return array|mixed|null
     */
    public static function loadFairsCountByIndustry() {

        $criteria = new CDbCriteria;
        $criteria->select = array('COUNT(t.fairId) AS fairsCount', 't.industryId');
        $criteria->with = [
            'fair' => [
                'together' => TRUE,
            ],
        ];
        $criteria->addCondition('t.industryId IS NOT NULL');
        $criteria->addCondition('fair.active = :active');
        $criteria->params[':active'] = Fair::ACTIVE_ON;
        $criteria->group = 't.industryId';

        return self::model()->findAll($criteria);
    }

    /**
     * @param   integer $industryId
     * @param   integer $cityId
     * @param   integer $active
     *
     * @return  string
     */
    public static function countFairsByIndustryAndCity($industryId, $cityId, $active = 1){
        $criteria = new CDbCriteria;
        $criteria->with = [
            'fair' => [
                'select' => FALSE,
                'together' => TRUE,
                'with' => [
                    'exhibitionComplex' => [
                        'select' => FALSE,
                        'together' => TRUE,
                    ],
                ],
            ],
        ];
        $criteria->addCondition('exhibitionComplex.cityId = :cityId');
        $criteria->addCondition('t.industryId = :industryId');
        $criteria->addCondition('fair.active = :active');
        $criteria->params[':cityId'] = $cityId;
        $criteria->params[':industryId'] = $industryId;
        $criteria->params[':active']     = $active;


        return self::model()->count($criteria);
    }

    /**
     * @param   integer $industryId
     * @param   integer $regionId
     * @param   integer $active
     *
     * @return  string
     */
    public static function countFairsByIndustryAndRegion($industryId, $regionId, $active = 1){
        $criteria = new CDbCriteria;
        $criteria->with = [
            'fair' => [
                'select' => FALSE,
                'together' => TRUE,
                'with' => [
                    'exhibitionComplex' => [
                        'select' => FALSE,
                        'together' => TRUE,
                        'with' => [
                            'city' => [
                                'select' => FALSE,
                                'together' => TRUE,
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $criteria->addCondition('city.regionId = :regionId');
        $criteria->addCondition('t.industryId = :industryId');
        $criteria->addCondition('fair.active = :active');
        $criteria->params[':regionId']   = $regionId;
        $criteria->params[':industryId'] = $industryId;
        $criteria->params[':active']     = $active;


        return self::model()->count($criteria);
    }
}