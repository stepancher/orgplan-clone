<?php
/**
 * Class ProductCategory
 */
class ProductCategory extends \AR
{
    public $id = NULL;
    
    protected $_name = NULL;
    protected $_description = NULL;
    
    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    public function getDescription()
    {
        $trDescription = '';
        if($this->_description !== NULL){
            $trDescription = $this->_description;
        }else{
            $translate = $this->translate;
            if(isset($translate->description)) {
                $this->_description = $translate->description;
                $trDescription = $this->_description;
            }
        }

        return $trDescription;
    }

    public function setDescription($trDescription){
        $this->_description = $trDescription;
    }

    public function tableName()
    {
        return '{{productcategory}}';
    }

    public function rules()
    {
        return array(
            array('id',
                'safe', 'on' => 'search,insert,update'),
        );
    }

    public function relations()
    {
        return array(
            'translate' => array(
                self::HAS_ONE,
                'TrProductCategory',
                'trParentId',
                'on' => 'translate.langId = "'.Yii::app()->language.'"',
            ),
        );
    }

    /**
     * @return string
     */
    public function getSeoName() {
        return strtolower($this->shortUrl);
    }

    /**
     * @return array
     */
    public static function getCategoriesList(){

        return  CHtml::listData(
            self::model()->findAll(
                array(
                    'order'=>'id'
                )
            ),
            'id',
            function($el){
                return isset($el->translate->name) ? $el->translate->name : '';
            }
        );
    }

    /**
     * @param $cityId
     * @return array
     */
    public static function getCategoriesByCity($cityId){

        $categories = array();
        if(!empty($cityId) && is_numeric($cityId)){
            $criteria = new CDbCriteria();
            $criteria->addCondition('cityId = :cityId');
            $criteria->params = array(':cityId' => $cityId);
            $criteria->group = 'productId';
            $products = ProductPrice::model()->findAll($criteria);

            $categories = array();
            foreach($products as $product){
                $categories[$product->getCategoryId()] = $product->getCategoryName();
            }

            $categories = array_unique($categories);
        }

        return $categories;
    }
}