<?php

class TrCatalogDescription extends \AR
{
    public static $_description = NULL;
    public $id;
    public $trParentId;
    public $langId;
    public $header;
    public $text;

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'trParentId' => array(
                'integer',
            ),
            'langId' => array(
                'string',
            ),
            'header' => array(
                'string',
            ),
            'text' => array(
                'string',
            ),
        );
    }
}