<?php
/**
 * Class ProductCategoryHasNotification
 */
Yii::import('application.modules.notification.models.Notification');
class ProductCategoryHasNotification extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'productCategoryId' => array(
				'label' => 'Категория',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'ProductCategory',
					array(
						'productCategoryId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'notificationId' => array(
				'integer',
				'label' => 'Уведомление',
				'relation' => array(
					'CBelongsToRelation',
					'Notification',
					array(
						'notificationId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}