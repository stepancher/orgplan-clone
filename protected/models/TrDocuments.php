<?php
/**
 * Class TrDocuments
 * 
 * @property int $id
 * @property int $trParentId
 * @property string $langId
 * @property string $name
 * @property string $description
 * @property string $value
 */

class TrDocuments extends ShardedAR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }     

        return self::$_description = array(
            'id' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer'
            ),
            'trParentId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer'
            ),
            'langId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'string'
            ),
            'name' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'string'
            ),
            'description' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'string'
            ),
            'value' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'string'
            ),
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTrParentId()
    {
        return $this->trParentId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLangId()
    {
        return $this->langId;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $langId
     */
    public function setLangId($langId)
    {
        $this->langId = $langId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param int $trParentId
     */
    public function setTrParentId($trParentId)
    {
        $this->trParentId = $trParentId;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}