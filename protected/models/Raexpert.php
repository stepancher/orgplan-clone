<?php
/**
 * Class Raexpert
 */
class Raexpert extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;    
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'label' => Yii::t('raexpert', 'Name'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'rating' => array(
				'string',
				'label' => Yii::t('raexpert', 'The investment rating of the region'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'generalInformation' => array(
				'text',
				'label' => Yii::t('raexpert', 'General information'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'linkImage' => array(
				'string',
				'label' => Yii::t('raexpert', 'Link to this image'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'industry' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'regionLink' => array(
				'string',
				'label' => Yii::t('raexpert', 'Link region'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'linkEthernet' => array(
				'string',
				'label' => Yii::t('raexpert', 'Web Address'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'generalInformationComment' => array(
				'text',
				'label' => Yii::t('raexpert', 'General information comments'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'chart' => array(
				'text',
				'label' => Yii::t('raexpert', 'Chart'),
			),
			'regionId' => array(
				'integer',
				'label' => Yii::t('raexpert', 'Region'),
				'relation' => array(
					'CBelongsToRelation',
					'Region',
					array(
						'regionId' => 'id',
					),
				),
			),
		);
	}
}