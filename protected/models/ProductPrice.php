<?php
Yii::import('application.models.Product');

/**
 * Class ProductPrice
 */
class ProductPrice extends \AR
{
    public static $_description = NULL;

    protected static $_uniqRecords = array();
    public function getName() {
        return $this->productName;
    }

    public function getSeoName() {
        return strtolower($this->shortUrl);
    }

    /**
     * @var string
     */
    public $searchableDefaultSort = ['productName' => 'asc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'productName' => 'productName',
        'productCategoryName' => 'productCategoryName',
        'productCategoryId' => 'productCategoryId',
    ];

    public function description(){
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'cost' => array(
                'integer',
                'label' => 'Цена',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'productId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                    'insert' => array(
                        array('safe'),
                        array('uniqueValidation'),
                    ),
                ),
                'integer',
                'label' => 'Продукт',
                'relation' => array(
                    'CBelongsToRelation',
                    'Product',
                    array(
                        'productId' => 'id',
                    )
                )
            ),
            'cityId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                    'insert' => array(
                        array('safe'),
                        array('uniqueValidation'),
                    ),
                ),
                'integer',
                'label' => 'Город',
                'relation' => array(
                    'CBelongsToRelation',
                    'City',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
        );
    }

    /**
     * @return string
     */
    public function getCityName(){
        if(isset($this->city->name) && !empty($this->city->name)){
            return $this->city->name;
        }else{
            return "";
        }
    }

    /**
     * @return string
     */
    public function getCategoryName(){
        if(isset($this->product->category->name) && !empty($this->product->category->name)){
            return $this->product->category->name;
        }else{
            return "";
        }
    }

    /**
     * @return string
     */
    public function getProductName(){
        if(isset($this->product->name) && !empty($this->product->name)){
            return $this->product->name;
        }else{
            return "";
        }
    }

    public function getCategoryId(){
        if(isset($this->product->category->id) && !empty($this->product->category->id)){
            return $this->product->category->id;
        }else{
            return "";
        }
    }

    /**
     * TODO unique validation for insert & update
     * @param $attribute
     */
    public function uniqueValidation($attribute){

        $uniqKey = $this->attributes['productId'].":".$this->attributes['cityId'];

        if(!isset(self::$_uniqRecords[$uniqKey])){

            $criteria = new CDbCriteria();
            $criteria->addCondition('productId = :productId');
            $criteria->addCondition('cityId = :cityId');
            $criteria->params = array(
                ':productId' => $this->attributes['productId'],
                ':cityId' => $this->attributes['cityId']
            );

            if(self::$_uniqRecords[$uniqKey] = parent::model()->exists($criteria)){
                $this->addError($attribute,'uniqueValidation','unique key constraint violation');
            }
        }
    }

    /**
     * @param  string $cityId
     * @return string
     */
    public static function getCustomSeoDescription($cityId){

        $query = "SELECT pp.id AS productPriceId,
                         trpc.name AS productCategoryName,
                         trc.name AS cityName
                FROM tbl_productprice pp
                    LEFT JOIN tbl_city c ON pp.cityId = c.id
                    LEFT JOIN tbl_trcity trc ON trc.trParentId = c.id AND trc.langId = :langId
                    LEFT JOIN tbl_product p ON pp.productId = p.id
                    LEFT JOIN tbl_trproductcategory trpc ON trpc.trParentId = p.categoryId AND trpc.langId = :langId
                WHERE c.shortUrl = :cityId
                GROUP BY trpc.trParentId";

        $productPrices = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':cityId' => $cityId, ':langId' => Yii::app()->language));

        if(count($productPrices) == 0){

            $query = str_replace('WHERE c.shortUrl = :cityId', 'WHERE c.id = :cityId', $query);
            $productPrices = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':cityId' => $cityId, ':langId' => Yii::app()->language));
        }

        $description = 'Цены на';

        if(count($productPrices) != 0){

            $count = 0;
            foreach($productPrices as $productPrice){

                $descriptionLength = mb_strlen($description . ' ' . mb_strtolower($productPrice['productCategoryName'] . ' в городе ' . $productPrice['cityName'], 'UTF-8'), 'UTF-8');

                if($descriptionLength >= 170){

                    $description = mb_substr($description, 0, -1, 'UTF-8');
                    $description .= ' в городе '.$productPrice['cityName'];
                    break;
                } else{

                    if($count == count($productPrices) - 1){
                        $description .= ' '.mb_strtolower($productPrice['productCategoryName'], 'UTF-8').' в городе '.$productPrice['cityName'];
                    }else{
                        $description .= ' '.mb_strtolower($productPrice['productCategoryName'], 'UTF-8').',';
                    }
                    $count++;
                }
            }

        }

        return $description;
    }
}