<?php
/**
 * Class Country
 *
 * @property	string	$name
 */
class Country extends \AR
{
	public static $_description = NULL;
	
	public $fairsCount;
	public $exhibitionComplexCount;

	const CODE_RU = 'ru';
	const CODE_DE = 'de';

	public function loadName()
	{
		return $this->loadTr('TrCountry', 'name');
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		$name = '';
		$translate = $this->translate;
		if (isset($translate->name)) {
			$name = $translate->name;
		}

		return $name;
	}

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'shortUrl' => array(
				'label' => 'Короткая ссылка',
				'string',
			),
			'regions' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'districts' => array(
				'relation' => array(
					'CHasManyRelation',
					'District',
					array(
						'countryId' => 'id',
					),
				),
			),
			'trCountries' => array(
				'relation' => array(
					'CHasManyRelation',
					'TrCountry',
					array(
						'trParentId' => 'id',
					),
				),
			),
			'translate' => array(
				'relation' => array(
					'CHasOneRelation',
					'TrCountry',
					array(
						'trParentId' => 'id',
					),
					'on' => 'translate.langId = "'.Yii::app()->language.'"',
				),
			),
			'translateRaw' => array(
				'relation' => array(
					'CHasOneRelation',
					'TrCountry',
					array(
						'trParentId' => 'id',
					),
				),
			),
		);

	}

	public function getSeoName() {
		return strtolower($this->shortUrl);
	}

	public function behaviors()
	{
		return array(
			'TranslateBehavior' => array(
				'class' => 'application.components.TranslateBehavior'
			)
		);
	}

	public static function countries(){

        $data = array();
	    $sql = "SELECT name FROM tbl_trcountry WHERE langId = :langId ORDER BY name";

	    $countries = Yii::app()->db->createCommand($sql)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));

	    if(empty($countries)){
            return $data;
        }

        foreach ($countries as $country){
	        $data[$country['name']] = $country['name'];
        }

        return $data;
    }

	public static function getCountries(){
		$data = array();
		$query = "SELECT trParentId AS id, name FROM tbl_trcountry WHERE langId = :langId ORDER BY name";

		$countries = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':langId' => Yii::app()->getLanguage()));

		if(empty($countries)){
			return $data;
		}

		foreach ($countries as $country){
			$data[$country['id']] = $country['name'];
		}

		return $data;
	}
}