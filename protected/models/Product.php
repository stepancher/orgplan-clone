<?php
Yii::import('application.models.ProductCategory');

/**
 * Class Product
 */
class Product extends \AR
{
    protected $_name = NULL;
    protected $_param = NULL;

    public static $_description = NULL;

    /**
     * @return mixed|string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    public function getParam()
    {
        $param = '';
        if($this->_param !== NULL){
            $param = $this->_param;
        }else{
            $translate = $this->translate;
            if(isset($translate->param)) {
                $this->_param = $translate->param;
                $param = $this->_param;
            }
        }

        return $param;
    }

    public function setParam($param){
        $this->_param = $param;
    }

    public function getSeoName() {
        return strtolower($this->shortUrl);
    }

    /**
     * @var string
     */
    public $searchableDefaultSort = ['name' => 'asc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'name' => 'id',
//        'city' => 'cityName',
//        'categoryId' => 'categoryName',
//        'cost' => 'cost',
    ];

    public function description(){
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrProduct',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
            'description' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'currency' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'costType' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'cost' => array(
                'integer',
                'label' => 'Цена',
            ),
            'categoryId' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ProductCategory',
                    array(
                        'categoryId' => 'id'
                    )
                )
            )
        );
    }

    /**
     * @return array
     */
    public static function getCurrencies(){

            return array(
                '0' => Yii::t('product','(empty)'),
                '1' => Yii::t('product', 'rub.'),
            );
    }

    /**
     * @return array
     */
    public static function getCostTypes(){
            return array(
                '0' => Yii::t('product','(empty)'),
                '1' => Yii::t('product','day'),
                '2' => Yii::t('product','month'),
                '3' => Yii::t('product','year'),
                '4' => Yii::t('product','hour'),
                '5' => Yii::t('product','10 pieces'),
                '6' => Yii::t('product','50 pieces'),
                '7' => Yii::t('product','1000 pieces'),
                '8' => Yii::t('product','person'),
                '9' => Yii::t('product','100 pieces'),
                '10' => Yii::t('product','m2'),
            );
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getCostTypeById($id){
        if(!empty($id) && is_numeric($id)){
            $costTypes = self::getCostTypes();
            if(isset($costTypes[$id]) && !empty($costTypes[$id])){
                return $costTypes[$id];
            }
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getCurrencyById($id){
        if(!empty($id) && is_numeric($id)){
            $currencies = self::getCurrencies();
            if(isset($currencies[$id]) && !empty($currencies[$id])){
                return $currencies[$id];
            }
        }
    }

    /**
     * @return string
     */
    public function getCategoryName(){
        if(isset($this->category->name) && !empty($this->category->name)){
            return $this->category->name;
        }else{
            return "";
        }
    }

}