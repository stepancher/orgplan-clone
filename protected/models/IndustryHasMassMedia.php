<?php

/**
 * Class IndustryHasMassMedia
 */

class IndustryHasMassMedia extends \AR{

    public static $_description = NULL;

    public function description(){
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'industryId' => array(
                'integer',
                'label' => Yii::t('industryHasMassMedia', 'industryId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'massMediaId' => array(
                'integer',
                'label' => Yii::t('industryHasMassMedia', 'massMediaId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'MassMedia',
                    array(
                        'massMediaId' => 'id',
                    ),
                ),
            ),
            'isGeneral' => array(
                'integer',
                'label' => Yii::t('industryHasMassMedia', 'isGeneral'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'industry' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'Industry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
        );
    }

}


