<?php

/**
 * Class Region
 *
 *
 * @property \Country $country
 */
class Region extends \AR
{

    protected $_name;

    public $fairsCount;
    public $exhibitionComplexCount;
    public $district_name;
    public static $_description = NULL;
    protected $_shortUrl = NULL;

    const SMI_VARIABLE = 'regionMassMedia';
    const ACTIVE_ON = 1;

    public function loadName()
    {
        return $this->loadTr('TrRegion', 'name');
    }

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        $translate = $this->translate;
        if($this->_name !== NULL){
            $name = $this->_name;
        }elseif(isset($translate->name)) {
            $this->_name = $translate->name;
            $name = $this->_name;
        }

        return $name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('region', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'districtId' => array(
                'label' => 'Округ',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'District',
                    array(
                        'districtId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'district_name' => array(
                'string',
                'label' => 'Округ',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'text' => array(
                'text',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'legalEntities' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'LegalEntity',
                    array(
                        'factRegionId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'cities' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'City',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'trRegions' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrRegion',
                    array(
                        'trParentId' => 'id',
                    ),
                )
            ),
            'regionHasMassMedia' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'RegionHasMassMedia',
                    array(
                        'regionId' => 'id',
                    ),
                    'order'=>'regionHasMassMedia.id asc',
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrRegion',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
            'translateRaw' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrRegion',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'raexpert' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Raexpert',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'regionInformation' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'RegionInformation',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplex' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'ExhibitionComplex',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'district' => array(
                'label' => 'district',
                'relation' => array(
                    'CBelongsToRelation',
                    'District',
                    array(
                        'districtId' => 'id',
                    ),
                ),
            ),
            'sponsorPos' => array(
                'string',
                'label' => 'Позиция спонсоров',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairsCount' => array(
                'label' => Yii::t('district', 'fairsCount'),
                'integer',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'exhibitionComplexCount' => array(
                'label' => Yii::t('district', 'fairsCount'),
                'integer',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
        );
    }

    public function getSeoName()
    {
        return strtolower($this->shortUrl);
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            ),
            'SitemapBehavior' => array(
                'class' => 'application.components.SitemapBehavior'
            ),
        );
    }

    public function search($criteria = null, $ignoreCompareFields = array())
    {
        $dCriteria = new CDbCriteria;

        $dCriteria->compare('district_translate.name', $this->{'district_name'}, true);
        $dCriteria->compare('translate.name', $this->{'name'}, true);
        $dCriteria->compare('translate.shortUrl', $this->{'shortUrl'}, true);
        $dCriteria->compare('t.id', $this->{'id'}, false);

        $dCriteria->select = [
            't.id',
            'district_translate.name AS district_name',
            'translate.name AS name',
            'translate.shortUrl AS shortUrl',
        ];

        $dCriteria->with['district'] = [
            'together' => TRUE,
            'select' => FALSE,
            'with' => [
                'translateRaw' => [
                    'together' => TRUE,
                    'alias' => 'district_translate',
                    'on' => 'district_translate.langId = "'.Yii::app()->language.'"',
                ]
            ]
        ];

        $dCriteria->with['translate'] = [
            'together' => TRUE,
            'select' => FALSE,
        ];

        $sort['district_name'] = [
            'asc'   => "district_translate.name",
            'desc'  => "district_translate.name DESC"
        ];

        $sort['name'] = [
            'asc'   => "translate.name",
            'desc'  => "translate.name DESC"
        ];

        $sort['shortUrl'] = [
            'asc'   => "translate.shortUrl",
            'desc'  => "translate.shortUrl DESC"
        ];

        $sort['id'] = [
            'asc'   => "t.id",
            'desc'  => "t.id DESC"
        ];

        return new CActiveDataProvider($this, array(
            'criteria' => $dCriteria,
            'sort' => array(
                'attributes' => $sort,
            )
        ));
    }

    /**
     * @param $q
     * @return array
     */
    public static function getRegionName($q) {

        $criteria = new CDbCriteria;
        $criteria->with['translate'] = [
            'together' => TRUE,
        ];
        $criteria->compare('translate.name', $q, true);

        $regions = CHtml::listData(
            Region::model()->findAll($criteria),
            'id',
            function($el){
                return $el;
            });

        $items = array_map(
            function($region_id, $region){
                return array(
                    'id' => $region_id,
                    'name' => $region->name,
                );
            },
            array_keys($regions),
            array_values($regions)
        );

        return $items;
    }

    public static function getMassMediaByRegionId($regionId){

        if(empty($regionId)){
            return;
        }

        $criteria = new CDbCriteria;
        $criteria->with['massMedia'] = [
            'together' => TRUE,
        ];
        $criteria->addCondition("t.regionId = :regionId");
        $criteria->params[':regionId'] = $regionId;

        $massMedia = CHtml::listData(
            RegionHasMassMedia::model()->findAll($criteria),
            'massMediaId',
            function($el){
                return isset($el->massMedia->name) ? $el->massMedia->name : '';
            }
        );

        $items = array_map(
            function($massMediaId, $massMedia) {
                return array(
                    'id'   => $massMediaId,
                    'name' => $massMedia,
                );
            },
            array_keys($massMedia),
            array_values($massMedia)
        );

        return $items;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        $prop = 'shortUrl';
        ${$prop} = '';
        if($this->{'_'.$prop} !== NULL){
            ${$prop} = $this->{'_'.$prop};
        }else{
            $translate = $this->translate;
            if(isset($translate->{$prop})) {
                $this->{'_'.$prop} = $translate->{$prop};
                ${$prop} = $this->{'_'.$prop};
            }
        }

        return ${$prop};
    }

    public function setShortUrl($value){
        $prop = 'shortUrl';
        $this->{'_'.$prop} = $value;
    }

    /**
     * List of properties stored in related tr-table.
     */
    public function trAttributeNames(){
        return [
            'shortUrl',
        ];
    }

    public function updateShortUrl(){
        $this->shortUrl = $this->getSeoNameForNewVersion();
    }

    public function getSeoNameForNewVersion(){

        $regionName = '';

        $trRegion = TrRegion::model()->findByAttributes(
            [
                'trParentId' => $this->id,
                'langId' => Yii::app()->language
            ]
        );

        if($trRegion !== NULL){
            $regionName = $trRegion->name;
        }

        $shortUrl = ToTransliteration::getInstance()->replace($regionName);

        return $shortUrl;
    }

}