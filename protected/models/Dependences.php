<?php

/**
 * This is the model class for table "{{dependences}}".
 *
 * The followings are the available columns in table '{{dependences}}':
 * @property integer $id
 * @property string $action
 * @property string $params
 * @property string $run
 */
class Dependences extends ShardedAR {

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('action, run', 'required'),
			array('action, params, run', 'length', 'max'=>145),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, action, params, run', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {

		return array(
			'id' => 'ID',
			'action' => 'Action',
			'params' => 'Params',
			'run' => 'Run'
		);
	}

	/**
	 * @return array
	 */
	public static function getParamsNames(){
		return array(
			'userId',
		);
	}
}
