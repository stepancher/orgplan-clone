<?php

/**
 * Class Calendar
 */
class Calendar extends \AR
{
	public static $_description = NULL;

	const TYPE_MY_FAIR = 1;

	public static function reads()
	{
		return  array(
			self::TYPE_MY_FAIR => Yii::t('calendar', 'My exhibitions')
		);
	}

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'userId' => array(
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'User',
					array(
						'userId' => 'id',
					),
				),
				'label' => Yii::t('calendar', 'User'),
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'events' => array(
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasManyRelation',
					'Event',
					array(
						'calendarId' => 'id',
					),
				),
			),
			'tasks' => array(
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasManyRelation',
					'Task',
					array(
						'calendarId' => 'id',
					),
				),
			),
			'type' => array(
				'string',
				'label' => Yii::t('calendar', 'Type'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
						array('required')
					)
				),
			),
			'calendarHasNotifications' => array(
				'relation' => array(
					'CHasManyRelation',
					'CalendarHasNotification',
					array(
						'calendarId' => 'id',
					),
				),
			),
		);

	}

	/**
	 * @return array
	 * Задаем цвет задачам в календаре
	 */
	public function getData()
	{
		$calendar = $this;
		$data = array();
		$i = 0;
		if (!empty($calendar->events)) {
			foreach ($calendar->events as $event) {
				$text = md5($event->name);
				$r = substr(preg_replace("([^0-9])", "", $text), 0, 6);
				$b = substr(preg_replace("([^0-9])", "", $text), 1, 6);
				$g = substr(preg_replace("([^0-9])", "", $text), 2, 6);
				$data[$i]['title'] = $event->name;
				$data[$i]['start'] = strtotime($event->startDate);
				$data[$i]['end'] = strtotime($event->endDate);
				$data[$i]['color'] = sprintf('#%02X%02X%02X', $r % 255, $g % 255, $b % 255);
				$i++;
			}
		}
		if (!empty($calendar->tasks)) {
			foreach ($calendar->tasks as $task) {
				if ($task->fairId && $task->fair) {
					if ($task->name == $task->fair->name . Yii::t('calendar', ' During the exhibition') || $task->name == $task->fair->name . Yii::t('calendar', ' Installation of an exhibition') ||
						$task->name == $task->fair->name . Yii::t('calendar', ' Dismantling') || stristr($task->name, Yii::t('calendar', ' Task:') . Yii::t('calendar', ' During the exhibition'))
						|| stristr($task->name, Yii::t('calendar', ' Task:') . Yii::t('calendar', ' Installation of an exhibition')) ||
						stristr($task->name, Yii::t('calendar', ' Task:') . Yii::t('calendar', ' Dismantling'))
					) {
						$text = $task->fairId && $task->fair ? md5($task->fair->name . Yii::t('calendar', ' During the exhibition')) : md5($task->name);
						$r = substr(preg_replace("([^0-9])", "", $text), 0, 6);
						$b = substr(preg_replace("([^0-9])", "", $text), 1, 6);
						$g = substr(preg_replace("([^0-9])", "", $text), 2, 6);
						$data[$i]['title'] = $task->name;
						$data[$i]['start'] = strtotime($task->startDate);
						$data[$i]['end'] = strtotime($task->endDate);
						$data[$i]['color'] = sprintf('#%02X%02X%02X', $r % 255, $g % 255, $b % 255);
						$i++;
					} else {
						$text = $task->fairId && $task->fair ? md5($task->fair->name) : md5($task->name);
						$r = substr(preg_replace("([^0-9])", "", $text), 0, 6);
						$b = substr(preg_replace("([^0-9])", "", $text), 1, 6);
						$g = substr(preg_replace("([^0-9])", "", $text), 2, 6);
						$data[$i]['title'] = $task->name;
						$data[$i]['start'] = strtotime($task->startDate);
						$data[$i]['end'] = strtotime($task->endDate);
						$data[$i]['color'] = sprintf('#%02X%02X%02X', $r % 255, $g % 255, $b % 255);
						$i++;
					}
				}
			}
		}

		return $data;
	}
}