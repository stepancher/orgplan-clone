<?php

/**
 * Class ProductionCalendarDay
 * @property string $newHoliday
 * @property \ProductionCalendarRegion $hasRegion
 */
class ProductionCalendarDay extends \AR
{
    public static $_description = NULL;

    const TYPE_HOLIDAY = 1;
    const TYPE_SHORT = 2;
    const TYPE_WORK = 3;

    public $newHoliday;

    public static $types = [
        self::TYPE_HOLIDAY => 'Выходной день',
        self::TYPE_SHORT => 'Короткий день',
        self::TYPE_WORK => 'Рабочий день'
    ];

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'productionCalendarId' => array(
                'label' => 'Производственный календарь',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ProductionCalendar',
                    array(
                        'productionCalendarId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'label' => 'Тип',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'day' => array(
                'label' => 'Дата',
                'date',
                'rules' => array(
                    'search,insert,update' => array(
                        array('required'),
                        array('safe')
                    )
                ),
            ),
            'region' => array(
                'label' => 'Регион',
                'integer',
                'relation' => array(
                    'CHasOneRelation',
                    'ProductionCalendarRegion',
                    array(
                        'regionId' => 'region',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
			'productionCalendar' => array(
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
				'label' => 'Производственный календарь'
			),
            'hasRegion' => [
                'relation' => array(
                    'CHasOneRelation',
                    'ProductionCalendarRegion',
                    array(
                        'regionId' => 'region',
                    ),
                ),
            ],
            'newHoliday' => [
                'label' => 'Название праздника'
            ]
        );
    }

    public static function getCalendarData()
    {
        $result = [];
        /** @var ProductionCalendarDay[] $models */
        $models = self::model()->findAll();
        if (null != $models) {
            foreach ($models as $key => $model) {
                $result[$key]['id'] = $model->id;
                $result[$key]['name']
                    = $result[$key]['text']
                    = isset($model->productionCalendar) && $model->productionCalendar->holiday
                    ? $model->productionCalendar->holiday . ' (' . self::$types[$model->type] . ')'
                    : self::$types[$model->type];
                $result[$key]['start_date'] = date('Y-m-d H:i', strtotime($model->day));
                $result[$key]['end_date'] = date('Y-m-d H:i', strtotime($model->day . '+ 1 day'));
            }
        }

        return $result;
    }

    public static function getHolidaysData()
    {
        $result = [];
        /** @var ProductionCalendarDay $models */
        $models = self::model()->findAll();
        if (null != $models) {
            foreach ($models as $model) {
                $y = date('Y', strtotime($model->day));
                $m = date('n', strtotime($model->day)) - 1;
                $d = date('j', strtotime($model->day));
                $date = $y . '-' . $m . '-' . $d;
                $result[$date] = $date;
            }
        }
        return json_encode($result);
    }

    public function saveAll()
    {
        if($this->validate()) {
            if(!empty($this->newHoliday)) {
                if($this->isNewRecord) {
                    $holidayModel = new ProductionCalendar();
                    $holidayModel->holiday = $this->newHoliday;
                    $holidayModel->year = date('Y', strtotime($this->day));
                } else {
                    if ($this->productionCalendarId > 0) {
                        $holidayModel = ProductionCalendar::model()->findByPk($this->productionCalendarId);
                    } else {
                        $holidayModel = new ProductionCalendar();
                    }
                    $holidayModel->holiday = $this->newHoliday;
                    $holidayModel->year = date('Y', strtotime($this->day));
                }
                if(!$holidayModel->save()) {
                    return false;
                }
                $this->productionCalendarId = $holidayModel->id;
            } else {
                if(empty($this->productionCalendarId) && $this->isNewRecord) {
                    $this->productionCalendarId = -1;
                }
            }
            $this->day = date('Y-m-d', strtotime($this->day));
            return $this->save();
        }
        return false;
    }
}