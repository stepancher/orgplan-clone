<?php

/**
 * Class ChartGroup
 * @property array $purifyAttributes
 * @property integer id
 * @property string header
 * @property integer type
 * @property integer industryId
 */
class ChartGroup extends \AR{

    public static $_description = NULL;

    protected $_header = NULL;

    /**
     * @return string
     */
    public function getHeader()
    {
        $header = '';
        if($this->_header !== NULL){
            $header = $this->_header;
        }else{
            $translate = $this->translate;
            if(isset($translate->header)) {
                $this->_header = $translate->header;
                $header = $this->_header;
            }
        }

        return $header;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'integer',
                'label' => 'type id',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industryId' => array(
                'integer',
                'label' => 'industry id',
                'relation' => array(
                    'CBelongsToRelation',
                    'IndustryAnalyticsInformation',
                    array(
                        'industryId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrChartGroup',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );

    }

    public static function getChartsGroups(){
        return array(
            '1' => 'Маленькая карточка',
            '3' => 'Средняя карточка',
            '2' => 'Большая карточка',
        );
    }
}