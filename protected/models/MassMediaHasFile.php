<?php
/**
 * Class MassMediaHasFile
 */
class MassMediaHasFile extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'massMediaId' => array(
                'integer',
                'label' => 'СМИ',
                'relation' => array(
                    'CBelongsToRelation',
                    'MassMedia',
                    array(
                        'massMediaId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fileId' => array(
                'integer',
                'label' => 'Файл',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }
}