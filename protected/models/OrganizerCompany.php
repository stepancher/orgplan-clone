<?php

/**
 * Class OrganizerCompany
 *
 * @property    integer $id
 */
class OrganizerCompany extends \AR
{
    const ASSOCIATION_VARIABLE = 'association';
    const CITY_VARIABLE = 'cityId';

    public static $_description = NULL;
    public $cityName = '';
    public $regionName = '';
    public $districtName = '';
    public $countryName = '';
    public $associationName = '';
    public $fairsCount = '';
    public $statusName = '';
    protected $_name = NULL;
    protected $_street = NULL;

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        $street = '';
        if($this->_street !== NULL){
            $street = $this->_street;
        }else{
            $translate = $this->translate;
            if(isset($translate->street)) {
                $this->_street = $translate->street;
                $street = $this->_street;
            }
        }

        return $street;
    }

    /**
     * @param $street
     */
    public function setStreet($street){
        $this->_street = $street;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'string',
                'label' => 'active',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'linkToTheSiteOrganizers' => array(
                'string',
                'label' => Yii::t('organizer', 'Link to the website of the organizers'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe'),
                        array('required')
                    ),
                    'fair' => array(
                        array('safe'),
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('fair', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'email' => array(
                'string',
                'label' => Yii::t('fair', 'email'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'phone' => array(
                'string',
                'label' => Yii::t('fair', 'phone'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrOrganizerCompany',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
            'contacts' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Organizer',
                    array(
                        'companyId' => 'id',
                    ),
                ),
            ),
            'cityId' => array(
                'integer',
                'label' => Yii::t('organizer', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'City',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
            'cityName' => array(
                'string',
                'label' => Yii::t('organizer', 'City'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'regionName' => array(
                'string',
                'label' => Yii::t('organizer', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'districtName' => array(
                'string',
                'label' => Yii::t('organizer', 'District'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'countryName' => array(
                'string',
                'label' => Yii::t('organizer', 'Country'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'street' => array(
                'string',
                'label' => Yii::t('organizer', 'Street'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'coordinates' => array(
                'string',
                'label' => Yii::t('organizer', 'Coordinates'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exdbId' => array(
                'integer',
                'label' => Yii::t('organizer', 'Expodatabase organizer id'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exdbContacts' => array(
                'string',
                'label' => Yii::t('organizer', 'Expodatabase organizer contacts'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'exdbContactsRaw' => array(
                'string',
                'label' => Yii::t('organizer', 'Expodatabase organizer contacts raw'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'associationName' => array(
                'string',
                'label' => Yii::t('organizer', 'Organizer association'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'fairsCount' => array(
                'integer',
                'label' => Yii::t('organizer', 'Fairs count'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'organizerCompanyHasAssociations' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'OrganizerCompanyHasAssociation',
                    array(
                        'organizerCompanyId' => 'id',
                    ),
                ),
            ),
            'statusId' => array(
                'integer',
                'label' => Yii::t('organizer', 'Status'),
                'relation' => array(
                    'CBelongsToRelation',
                    'OrganizerCompanyStatus',
                    array(
                        'statusId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'statusName' => array(
                'string',
                'label' => Yii::t('organizer', 'Status'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
        );
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            ),
        );
    }

    public function trAttributeNames(){
        return [
            'name',
            'street',
        ];
    }
}