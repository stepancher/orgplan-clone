<?php

/**
 * Class Organizer
 *
 * @property array $fairs
 * @property OrganizerInfo $organizerInfo
 */
class Organizer extends \AR
{
	public static $_description = NULL;
	public $organizerName = '';
	public $companyId = '';
	public $phone = '';
	public $email = '';
	public $servicesBuilding = '';
	public $advertisingServicesTheFair = '';
	public $servicesLoadingAndUnloading = '';
	public $foodServices = '';
	public $exhibitionManagement = '';
	public $linkToTheSiteOrganizers = '';

	const ORGANIZER_COMPANY_ID = 'companyId';
	const FAIR_VARIABLE = 'fairId';

	public function behaviors()
	{
		return array(
			'SitemapBehavior' => array(
				'class' => 'application.components.SitemapBehavior'
			),
		);
	}

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'active' => array(
				'integer',
				'label' => Yii::t('organizer', 'active'),
				'rules' => array(
					'search,insert,update,fair,load' => array(
						array('safe'),
					)
				),
			),
			'companyId' => array(
				'integer',
				'label' => Yii::t('organizer', 'organizer company id'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'company' => array(
				'relation' => array(
					'CHasOneRelation',
					'OrganizerCompany',
					array(
						'id' => 'companyId',
					),
				),
			),
			'organizerName' => array(
				'string',
				'label' => Yii::t('organizer', 'Organizer'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					)
				),
			),
			'linkToTheSiteOrganizers' => array(
				'string',
				'label' => Yii::t('organizer', 'Link to the website of the organizers'),
				'rules' => array(
					'search,insert,update,load' => array(
						array('safe'),
					),
					'fair' => array(
						array('safe'),
					)
				),
			),
            'phone' => array(
                'string',
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
                'label' => Yii::t('organizer', 'Directorate phone'),
            ),
            'email' => array(
                'string',
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    ),
                ),
                'label' => Yii::t('organizer', 'Management of e-mail'),
            ),
            'servicesBuilding' => array(
                'string',
                'label' => Yii::t('organizer', 'Services building'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
            'advertisingServicesTheFair' => array(
                'string',
                'label' => Yii::t('organizer', 'Services in advertising on exhibition'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
            'servicesLoadingAndUnloading' => array(
                'string',
                'label' => Yii::t('organizer', 'Services for loading and unloading'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
            'foodServices' => array(
                'string',
                'label' => Yii::t('organizer', 'Catering services'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
            'exhibitionManagement' => array(
                'string',
                'label' => Yii::t('organizer', 'Exhibition Management'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
			'fair' => array(
				'rules' => array(
					'search,insert,update,fair' => array(
						array('safe')
					)
				),
			),
			'fairs' => array(
				'rules' => array(
					'search,insert,update,fair' => array(
						array('safe')
					)
				),
			),
			'proposalEmail' => array(
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				)
			),
			'organizerInfo' => array(
				'relation' => array(
					'CHasOneRelation',
					'OrganizerInfo',
					array(
						'organizerId' => 'id',
					),
				),
			),
			'organizerContacts' => array(
				'relation' => array(
					'CHasManyRelation',
					'OrganizerContactList',
					array(
						'organizerId' => 'id',
					),
				),
			),
		);
	}

	/**
	 * @return string
	 */
	public function loadName()
	{
		return $this->loadTr('TrOrganizer', 'name');
	}

    /**
     * @TODO refactor. Use method with parameters to concat any values.
     * @return string
     */
    public function getPhoneWithEmail(){

        $phoneWithEmail = 'Отсутствуют данные';

        if(isset($this->phone) && !empty($this->phone)){
            $phoneWithEmail = $this->phone;
        }

        if(isset($this->email) && !empty($this->email)){
            $phoneWithEmail = $phoneWithEmail.', email: '.$this->email.'.';
        }

        return $phoneWithEmail;
    }

    /**
     * @TODO refactor. Use method with parameters to concat any values.
     * @return string
     */
    public function getNameWithContacts(){

        $organizerWithContacts = 'Отсутствуют данные';
        if(isset($this->id) && !empty($this->id)){
            $organizerWithContacts = '('.$this->id.') ';
        }

        if(isset($this->name) && !empty($this->name)){
            $organizerWithContacts = $organizerWithContacts.$this->name;
        }

        if(isset($this->phone) && !empty($this->phone)){
            $organizerWithContacts = $organizerWithContacts.', тел.: '.$this->phone;
        }

        if(isset($this->email) && !empty($this->email)){
            $organizerWithContacts = $organizerWithContacts.', email: '.$this->email.'.';
        }

        return $organizerWithContacts;
    }
}