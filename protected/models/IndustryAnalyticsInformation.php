<?php

/**
 * Class IndustryAnalyticsInformation
 * @property array $purifyAttributes
 * @property integer id
 * @property string header
 * @property string headerColor
 * @property integer priority
 */
class IndustryAnalyticsInformation extends \AR{

    public static $_description = NULL;

    protected $_header = NULL;

    /**
     * @return string
     */
    public function getHeader()
    {
        $header = '';
        if($this->_header !== NULL){
            $header = $this->_header;
        }else{
            $translate = $this->translate;
            if(isset($translate->header)) {
                $this->_header = $translate->header;
                $header = $this->_header;
            }
        }

        return $header;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'headerColor' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'priority' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'icon' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'iconClass' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industry' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'Industry',
                    array(
                        'id' => 'id',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrIndustryAnalyticsInformation',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }
}