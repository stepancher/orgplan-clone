<?php

/**
 * Class ExhibitionComplexHasAssociation
 *
 * @property    integer $id
 */
class ExhibitionComplexHasAssociation extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exhibitionComplexId' => array(
                'label' => 'exhibitionComplexId',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ExhibitionComplex',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'associationId' => array(
                'label' => Yii::t('fair', 'Fair association'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Association',
                    array(
                        'associationId' => 'id',
                    ),
                ),
            ),
        );
    }
}