<?php
/**
 * Class TrAnalytics
 */
class TrAnalytics extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'label' => 'Аналитика',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'Analytics',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'string',
				'label' => 'Язык',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'theme' => array(
				'string',
				'label' => 'Тема',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'description' => array(
				'text',
				'label' => 'Описание',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}