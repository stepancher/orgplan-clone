<?php

/**
 * Class GStatTypeGroup
 *
 */
class GStatTypeGroup extends \AR{

    public static $_description = NULL;

    protected $_unit = NULL;

    /**
     * @return string
     */
    public function getUnit()
    {
        $unit = '';
        if($this->_unit !== NULL){
            $unit = $this->_unit;
        }else{
            $translate = $this->translate;
            if(isset($translate->unit)) {
                $this->_unit = $translate->unit;
                $unit = $this->_unit;
            }
        }

        return $unit;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'groupId' => array(
                'integer',
                'label' => 'groupId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'gId' => array(
                'string',
                'label' => 'gId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrGStatTypeGroup',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }
}