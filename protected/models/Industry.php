<?php

/**
 * Class Industry
 *
 * @property    integer $id
 */
class Industry extends \AR
{
    public static $_description = NULL;
    protected $_name;

    public function getName()
    {
        $name = '';
        $translate = $this->translate;
        if($this->_name !== NULL){
            $name = $this->_name;
        }elseif(isset($translate->name)) {
            $this->_name = $translate->name;
            $name = $this->_name;
        }

        return $name;
    }

    public function setName($name){
        $this->_name = $name;
    }

    public function loadName()
    {
        return $this->loadTr('TrIndustry', 'name');
    }

    public function getSeoName()
    {
        return strtolower($this->shortNameUrl);
    }

    /**
     * Получение короткой ссылки индустрии по ее идентификатору
     * @param $industryId
     * @return string
     */
    public static function getShortUrl($industryId)
    {
        $model = self::model()->findByPk($industryId);
        if (null != $model) {
            return $model->getSeoName();
        }

        return $industryId;
    }

    public function getSeoDescription()
    {
        $pattern = 'На сайте представлены основные показатели развития отрасли: "{:industryName}". Данные содержатся по федеральным округам и регионам России.';
        $description = str_replace('{:industryName}', $this->name, $pattern);

        return $description;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('industry', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industryTypeId' => array(
            ),
            'trIndustries' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrIndustry',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'exponent' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Exponent',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'synonyms' => array(
                'string',
                'label' => Yii::t('industry', 'Synonyms'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'sponsorPos' => array(
                'string',
                'label' => 'Позиция спонсоров',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'keywords' => array(
                'string',
                'label' => 'keywords',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairs' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'massMedias' => array(
                'label' => 'СМИ',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'statTypes' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'standHasIndustries' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'StandHasIndustry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'auctionTechnicalData' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasOneRelation',
                    'AuctionTechnicalData',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'stands' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industryHasMassMedia' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'IndustryHasMassMedia',
                    array(
                        'industryId' => 'id',
                    ),
                    'order'=>'industryHasMassMedia.id asc',
                ),
            ),
            'industryAnalyticsInformation' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'IndustryAnalyticsInformation',
                    array(
                        'id' => 'industryId',
                    ),
                ),
            ),
            'analyticsTable' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'ChartTable',
                    array(
                        'id' => 'industryId',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrIndustry',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                )
            ),
            'translateRaw' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrIndustry',
                    array(
                        'trParentId' => 'id',
                    ),
                )
            ),
            'shortNameUrl' => array(
                'label' => 'Короткое имя ссылки',
                'string',
            ),
            'analytics' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Analytics',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'fairHasIndustries' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasIndustry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'fairHasIndustriesPreviousYear' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasIndustry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
        );
    }

    /**
     * List of properties stored in related tr-table.
     */
    public function trAttributeNames(){
        return [
            'name',
        ];
    }

    /**
     * @param $industries
     * @return string
     * @var FairHasIndustry $FHI
     */
    static function linkIndustries($industries)
    {
        /**
         * @var FairHasIndustry $FHI
         */

        $link = '';
        foreach ($industries as $FHI) {
            if ($FHI->industry) {
                $link .= !IS_MODE_DEV ? $FHI->industry->name . '<br>' : CHtml::link($FHI->industry->name, Yii::app()->createUrl('analytics/default/view', array('id' => $FHI->industry->id))) . '<br>';
            }
        }
        return $link;
    }

    /**
     * Получение строки с именами индустрий для SEO
     * @param $ids
     * @return string
     */
    public static function getNamesForSeo($ids)
    {
        $result = '';
        $count = 1;
        foreach ($ids as $id) {
            $model = static::model()->findByPk($id);
            if(null != $model) {
                if($count != 2) {
                    $result .= $model->name . ', ';
                } else {
                    $result .= $model->name;
                }
            }
            $count++;
        }
        return $result;
    }

    /**
     * @TODO Refactor. Models must return only raw data
     * @return array
     */
    public function getSelectedMassMedia(){
        $arr = array();
        foreach($this->industryHasMassMedia as $massMedia){
            $arr[$massMedia->massMediaId] = array('selected' => true);
        }

        return $arr;
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            ),
            'SitemapBehavior' => array(
                'class' => 'application.components.SitemapBehavior'
            ),
        );
    }

    /**
     * @return array
     */
    public static function getAllowedIndustries(){

        return array(
            11 //Сельское хозяйство
        , 2 //Строительство, отделочные материалы и комплектация
        , 8 //Энергетика
        , 1 //Мебель для дома и офиса, оформление интерьера, предметы быта
        , 5 //Лес и деревообработка
        , 14 //Продукты питания
        , 13 //Пищевая промышленность: оборудование и ингредиенты
        , 3 //Машиностроение, металлообработка, станки, промышленное оборудование
        , 37 //Нефть и газ
        );
    }

    /**
     * @return bool
     */
    public function checkAccess(){
        return in_array($this->id,self::getAllowedIndustries());
    }

    /**
     * @param array $industryFields
     * @param int $pageSize
     * @param null $sortColumn
     * @return CActiveDataProvider
     */
    public function search($industryFields = [], $pageSize = 10, $sortColumn = NULL)
    {
        $criteria = new CDbCriteria;
        $criteria->with['translate'] = [
            'together' => TRUE,
        ];

        $criteria->compare('translate.name', $this->name, TRUE);
        $criteria->compare('t.synonyms', $this->synonyms, TRUE);

        $sort['name'] = [
            'asc'   => '`translate`.`name`',
            'desc'  => '`translate`.`name` DESC'
        ];

        $sort['synonyms'] = [
            'asc'   => '`translate`.`name`',
            'desc'  => '`translate`.`name` DESC'
        ];

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => $sort,
            ),
            'pagination' => [
                'pageSize' => $pageSize
            ]
        ));
    }
}