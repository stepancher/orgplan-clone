<?php

/**
 * Class Documents
 * @var $file ObjectFile
 * @var $fairId
 */

class Documents extends ShardedAR
{
    const TYPE_TEMPLATE = 1;
    const TYPE_QUESTIONNAIRE = 2;
    const TYPE_VIDEO = 3;

    public static function getType($type = null)
    {
        if (null != $type) {
            $data = static::getType();
            if(isset($data[$type])) {
                return $data[$type];
            }

            return null;
        }

        return [
            static::TYPE_TEMPLATE => 'Шаблон',
            static::TYPE_QUESTIONNAIRE => 'Анкета',
            static::TYPE_VIDEO => 'Видео',
        ];
    }

    public function description()     {
        return array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'label' => 'Название',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'description' => array(
                'label' => 'Описание',
                'text',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'label' => 'Тип',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fairId' => array(
                'label' => 'Id выставки',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'value' => array(
                'string',
                'label' => 'Значение',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'file' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'value' => 'id',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrDocuments',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
//                    'condition' => 'translate.langId = :langId OR translate.langId IS NULL',
//                    'params' => array(':langId' => Yii::app()->language),
                ),
            ),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        $trString = '';

        if(!empty($this->translate->name)){
            $trString = $this->translate->name;
        }

        return $trString;
    }



    /**
     * @return string
     */
    public function getDescription()
    {
        $trString = '';

        if(!empty($this->translate->description)){
            $trString = $this->translate->description;
        }

        return $trString;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        $trString = '';

        if(!empty($this->translate->value)){
            $trString = $this->translate->value;
        }

        return $trString;

    }
}