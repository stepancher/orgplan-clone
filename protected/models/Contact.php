<?php
/**
 * Class Contact
 */
class Contact extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'export,import' => array(
						array('safe')
					)
				),
			),
			'companyName' => array(
				'string',
				'label' => Yii::t('contact', 'Company'),
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'email' => array(
				'string',
				'label' => Yii::t('contact', 'email'),
				'rules' => array(
					'search' => array(
						array('safe'),
					),
                    'update' => array(
                        array('required'),
                        array('myUnique', 'message' => Yii::t('user', 'This email already exists.')),
                    ),
					'insert,recoverPassword, getSocialEmail, calendarSend' => array(
						array('required'),
						array('unique', 'message' => Yii::t('user', 'This email already exists.')),
					),
					'export,import' => array(
						array('safe')
					),
					'registration' => array(
						array('unique', 'message' => Yii::t('contact', 'This email already exists.')),
					),
					'registration-main' => array(
						array('unique', 'message' => Yii::t('contact', 'This email already exists.')),
					),
					'registration-left-menu' => array(
						array('unique', 'message' => Yii::t('contact', 'This email already exists.')),
					),
					'recoverPassword' => array(
						array('safe'), 
					),
				),
			),
			'post' => array(
				'string',
				'label' => Yii::t('contact', 'Post'),
				'rules' => array(
					'search,update' => array(
						array('safe'),
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'phone' => array(
				'string',
				'label' => Yii::t('contact', 'Mobile phone'),
				'rules' => array(
					'search,update' => array(
						array('safe'),
					),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'workPhone' => array(
				'string',
				'label' => Yii::t('contact', 'Work Phone'),
				'rules' => array(
					'search,update' => array(
						array('safe'),
                    ),
					'export,import' => array(
						array('safe')
					)
				),
			),
			'user' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasOneRelation',
					'User',
					array(
						'contactId' => 'id',
					),
				),
			),
			'content' => array(
				'rules' => array(
					'calendarSend' => array(
						array('safe')
					)
				),
			),
		);

	}

	public function myUnique($fieldName, $params){

	    $criteria = new CDbCriteria;

        $criteria->addCondition('email = :email');
        $criteria->params[':email'] = $this->email;

        if(!$this->isNewRecord) {
            $criteria->addCondition('id != :id');
            $criteria->params[':id'] = $this->id;
        }

        $count = self::model()->count($criteria);

        if($count){
            $this->addError('email', Yii::t('contact', 'This email already exists.'));
        }
    }
}