<?php


/**
 * Class FairInfo
 */
class FairInfo extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'forumId' => array(
                'label' => Yii::t('fair','forum'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'Fair',
                    array(
                        'forumId' => 'id',
                    ),
                ),
            ),
            'exhibitorsLocal' => array(
                'label' => Yii::t('fair','exhibitorsLocal'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exhibitorsForeign' => array(
                'label' => Yii::t('fair','exhibitorsForeign'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'countriesCount' => array(
                'label' => Yii::t('fair','countriesCount'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'areaClosedExhibitorsLocal' => array(
                'label' => Yii::t('fair','areaClosedExhibitorsLocal'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'areaClosedExhibitorsForeign' => array(
                'label' => Yii::t('fair','areaClosedExhibitorsForeign'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'areaOpenExhibitorsLocal' => array(
                'label' => Yii::t('fair','areaOpenExhibitorsLocal'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'areaOpenExhibitorsForeign' => array(
                'label' => Yii::t('fair','areaOpenExhibitorsForeign'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'areaSpecialExpositions' => array(
                'label' => Yii::t('fair','areaSpecialExpositions'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'visitorsLocal' => array(
                'label' => Yii::t('fair','visitorsLocal'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'visitorsForeign' => array(
                'label' => Yii::t('fair','visitorsForeign'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'squareGross' => array(
                'integer',
                'label' => Yii::t('fair', 'Square gross'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'squareNet' => array(
                'integer',
                'label' => Yii::t('fair', 'Square net'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'members' => array(
                'integer',
                'label' => Yii::t('fair', 'Participants'),
                'rules' => array(
                    'search,insert,update,load,sortMatch' => array(
                        array('safe')
                    )
                ),
            ),
            'visitors' => array(
                'integer',
                'label' => Yii::t('fair', 'Visitors'),
                'rules' => array(
                    'search,insert,update,load,sortMatch' => array(
                        array('safe')
                    )
                ),
            ),
            'statistics' => array(
                'label' => 'Статистика',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'statisticsDate' => array(
                'date',
                'label' => Yii::t('AdminModule.admin','statisticsDate'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fair' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Fair',
                    array(
                        'infoId' => 'id',
                    ),
                ),
            ),
            'exdbFairId' => array(
                'integer',
                'label' => 'exdbFairId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exdbStatisticsDate' => array(
                'string',
                'label' => 'exdbStatisticsDate',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'exdbCountriesCount' => array(
                'string',
                'label' => 'exdbCountriesCount',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }

    public function beforeSave()
    {
        $fairs = Fair::model()->findAllByAttributes(array('infoId' => $this->id));

        if(empty($fairs)){
            return parent::beforeSave(); // TODO: Change the autogenerated stub
        }

        foreach ($fairs as $fair){

            /** @var Fair $fair */
            $fair->saveAttributes(array('datetimeModified' => new CDbExpression('CURRENT_TIMESTAMP')));
        }
        return parent::beforeSave(); // TODO: Change the autogenerated stub
    }

}