<?php

/**
 * Class UserHasOrganizer
 *
 * @property integer $id
 * @property integer $organizerId
 * @property integer $userId
 */
class UserHasOrganizer extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'userId' => array(
                'integer',
                'label' => Yii::t('user', 'UserId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerId' => array(
                'integer',
                'label' => Yii::t('user', 'OrganizerId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }
}