<?php

/**
 * Class RegionRating
 */
class RegionRating extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'label' => 'Название',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'rating' => array(
				'string',
				'label' => 'Рейтинг',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'dynamic' => array(
				'string',
				'label' => 'Динамика',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'license' => array(
				'label' => 'Лицензия ЦБ РФ',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'identificationNumber' => array(
				'label' => 'Идентификационный номер',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'modifiedRating' => array(
				'label' => 'Дата изменения рейтинга',
				'datetime',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'pressRelease' => array(
				'string',
				'label' => 'Пресс-релиз',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'update' => array(
				'label' => 'Дата обновления бд',
				'datetime',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'regionInformation' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}