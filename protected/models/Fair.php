<?php

Yii::import('application.modules.RUEF.RUEFModule');
Yii::import('application.modules.admin.AdminModule');

/**
 * Class Fair
 * @property array $filterIds
 * @property string $transLink
 * @property array $purifyAttributes
 * @property string $shard
 * @property string $proposalVersion
 * @property string $userRedactorId
 */
class Fair extends \AR
{
    public static $_description = NULL;
    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;
    const DEFAULT_ACTIVE = 0;
    const CANCELED_YES = 1;
    const CANCELED_NO = 0;
    const DEFAULT_CANCELED = 0;

    const MULTISELECT_SIZE = 9;
    const RATING_DEFAULT = 3;
    const RATING_AUDIT = 2;
    const IS_FORUM = 1;

    const TYPE_MODERATION_OK = 1;
    const TYPE_MODERATION_NO = 0;
    
    const INDUSTRY_VARIABLE = 'fairHasIndustries_industryId';
    const AUDIT_VARIABLE = 'auditId';
    const ORGANIZER_VARIABLE = 'organizerId';
    const EXHIBITION_VARIABLE = 'exhibitionComplexId';
    const INFO_VARIABLE = 'infoFairId';
    const ASSOCIATION_VARIABLE = 'fairAssociation';

    public $forumFairs = '';
    public $year;
    public $fairTranslateName;

    public static $days = array(
        'Вс',
        'Пн',
        'Вт',
        'Ср',
        'Чт',
        'Пт',
        'Сб',
    );

    const RUEF_PAGESIZE = 25;

    public static $frequencies = array(
        '1' => '1 раз в год',
        '2' => '2 раза в год',
        '3' => '3 раза в год',
        '4' => '4 раза в год',
        '5' => '5 раз в год',
        '6' => '6 раз в год',
        '7' => '7 раз в год',
        '8' => '8 раз в год',
        '9' => '9 раз в год',
        '10' => '10 раз в год',
        '16' => '16 раз в год',
        '0.5' => '1 раз в 2 года',
        '0.25' => '1 раз в 4 года',
        '0.33' => '1 раз в 3 года',
        '0.66' => '1 раз в 1.5 года',
        '0.2' => '1 раз в 5 лет',
        '0.1' => '1 раз в 10 лет',
        '0.17' => '1 раз в 6 лет',
    );

    public static $ratings = array(
        '1' => 'Рейтинг',
        '2' => 'Аудит',
        '3' => 'Организатор',
    );

    public $buttons;
    public $beginDateRange;
    public $endDateRange;
    public $datetimeModifiedRange;

    /** Search properties  */
    public $exhibitionComplexName = '';
    public $fairIsForum = '';
    public $cityId = '';
    public $cityName = '';
    public $regionId = '';
    public $regionInformationId = '';
    public $regionName = '';
    public $districtId = '';
    public $districtName = '';
    public $countryId = '';
    public $countryName = '';
    public $countryShortUrl = '';
    public $industryId = [];
    public $industry = [];
    //public $industryName = []; // @TODO Resolve duplicate names with $industry

    public static $months = [
        '01' => 'january',
        '02' => 'february',
        '03' => 'march',
        '04' => 'april',
        '05' => 'may',
        '06' => 'june',
        '07' => 'jule',
        '08' => 'august',
        '09' => 'september',
        '10' => 'october',
        '11' => 'november',
        '12' => 'december',
    ];

    protected $_name = NULL;
    protected $_uniqueText = NULL;
    protected $_duration = NULL;
    protected $_shortUrl = NULL;
    protected $_today = NULL;
    protected $description = NULL;
    public $fairStatus = NULL;
    public $fairStatusColor = NULL;
    public $exhibitorsLocal = NULL;
    public $exhibitorsForeign = NULL;
    public $countriesCount = NULL;
    public $areaClosedExhibitorsLocal = NULL;
    public $areaClosedExhibitorsForeign = NULL;
    public $areaOpenExhibitorsLocal = NULL;
    public $areaOpenExhibitorsForeign = NULL;
    public $areaSpecialExpositions = NULL;
    public $visitorsLocal = NULL;
    public $visitorsForeign = NULL;
    public $fairShortUrl = '';
    public $statistics = '';
    public $statisticsDate = '';
    public $infoId = '';
    public $forumId = '';
    public $forumName = '';
    public $lastYearFair = '';
    public $organizerCompanyId = '';
    public $fairEnName = '';
    public $fairDeName = '';
    public $fairDiscription = '';
    public $fairUniqueText = '';
    public $organizerId = '';
    public $organizerCompanyCount = '';
    public $organizerContactCount = '';
    public $userRedactorEmail = '';

    public $fairHasAssociationId = '';
    public $squareGross = '';
    public $squareNet = '';
    public $members = '';
    public $visitors = '';
    public $industryName = '';
    public $commonStatisticFair = '';
    public $commonStatisticFairAssociation = '';
    public $associationName = '';
    public $auditName = '';
    public $organizerName = '';
    public $organizerEmail = '';
    public $leasedArea = '';

    public $fairsYear = '';

    public function getToday(){

        return date('Y-m-d');
    }

    public static function getThisYear(){

        return date('Y');
    }
    /**
     * @return int|mixed|string
     */
    public function getDuration() {

        if($this->_duration === NULL && $this->id !== NULL){

            $datetime1 = new DateTime($this->endDate);
            $datetime2 = new DateTime($this->beginDate);
            $this->_duration = $datetime1->diff($datetime2)->days;
            $this->_duration += 1;

            if(strtotime($this->endDate) < strtotime($this->beginDate))
                $this->_duration = -($this->_duration);
        }

        return $this->_duration;
    }

    public function setDuration($value){
        $this->_duration = $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getUniqueText()
    {
        $uniqueText = '';
        if($this->_uniqueText !== NULL){
            $uniqueText = $this->_uniqueText;
        }else{
            $translate = $this->translate;
            if(isset($translate->uniqueText)) {
                $this->_uniqueText = $translate->uniqueText;
                $uniqueText = $this->_uniqueText;
            }
        }

        return $uniqueText;
    }

    public function setUniqueText($uniqueText){
        $this->_uniqueText = $uniqueText;
    }

    public function setName($name){
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        $description = '';
        if($this->description !== NULL){
            $description = $this->description;
        }else{
            $translate = $this->translate;
            if(isset($translate->description)) {
                $this->description = $translate->description;
                $description = $this->description;
            }
        }

        return $description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        $prop = 'shortUrl';
        ${$prop} = '';
        if($this->{'_'.$prop} !== NULL){
            ${$prop} = $this->{'_'.$prop};
        }else{
            $translate = $this->translate;
            if(isset($translate->{$prop})) {
                $this->{'_'.$prop} = $translate->{$prop};
                ${$prop} = $this->{'_'.$prop};
            }
        }

        return ${$prop};
    }

    public function setShortUrl($value){
        $prop = 'shortUrl';
        $this->{'_'.$prop} = $value;
    }

    /**
     * Месяцы в родительном подеже с маленькой буквы
     * @param null $month - целые числа от 1 до 12, без нуля впереди
     * @return array || string
     */
    public static function getMonthData($month = null)
    {
        if (null != $month) {
            $data = self::getMonthData();
            if (isset($data[$month])) {
                return $data[$month];
            }
        }

        return [
            1 => Yii::t('system', 'january'),
            2 => Yii::t('system', 'february'),
            3 => Yii::t('system', 'march'),
            4 => Yii::t('system', 'april'),
            5 => Yii::t('system', 'may'),
            6 => Yii::t('system', 'june'),
            7 => Yii::t('system', 'jule'),
            8 => Yii::t('system', 'august'),
            9 => Yii::t('system', 'september'),
            10 => Yii::t('system', 'october'),
            11 => Yii::t('system', 'november'),
            12 => Yii::t('system', 'december'),
        ];
    }


    /**
     * @var string
     */
    public $searchableDefaultSort = ['rating' => 'asc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'name' => 'name',
        'rating' => 'rating',
        'date' => 'beginDate',
        'industry' => 'industryName',
        'region' => 'regionName',
        'city' => 'cityName',
    ];

    /**
     * @return array
     */
    public static function getMonths()
    {
        return array(
            '01' => Yii::t('fair', 'January'),
            '02' => Yii::t('fair', 'February'),
            '03' => Yii::t('fair', 'March'),
            '04' => Yii::t('fair', 'April'),
            '05' => Yii::t('fair', 'May'),
            '06' => Yii::t('fair', 'June'),
            '07' => Yii::t('fair', 'Jule'),
            '08' => Yii::t('fair', 'August'),
            '09' => Yii::t('fair', 'September'),
            '10' => Yii::t('fair', 'October'),
            '11' => Yii::t('fair', 'November'),
            '12' => Yii::t('fair', 'December'),
        );
    }

    /**
     * Нужен для формирования title и description в SEO по вкладкам
     * @var array
     */
    public static $tabsName = [
        'fairRegion' => 'Регион',
        'fairMassMedia' => 'СМИ',
        'fairIndustry' => 'Отрасль',
        'fairContact' => 'Контакты',
        'fairMap' => 'Карта',
        'fairComment' => 'Отзывы',
    ];

    static function reads()
    {
        return array(
            self::ACTIVE_ON => Yii::t('fair', 'Active'),
            self::ACTIVE_OFF => Yii::t('fair', 'Not active')
        );
    }

    static function getRead($active)
    {
        $statuses = self::reads();
        if(isset($statuses[$active])){
            return $statuses[$active];
        }
        return $statuses[self::DEFAULT_ACTIVE];
    }

    // TODO perhaps artifact - тогда и два метода выше не нужны
    static $reads = array(
        self::ACTIVE_ON => 'Активный',
        self::ACTIVE_OFF => 'Не активный'
    );

    public static function cancelStatus()
    {
        return array(
            self::CANCELED_YES => Yii::t('fair', 'Canceled'),
            self::CANCELED_NO => Yii::t('fair', 'Not canceled'),
        );
    }

    public static function getCancelStatus($status)
    {
        $statuses = self::cancelStatus();
        if(isset($statuses[$status])){
            return $statuses[$status];
        }
        return $statuses[self::DEFAULT_CANCELED];


    }

    static function moderation()
    {
        return array(
            self::TYPE_MODERATION_OK => Yii::t('fair', 'Allowed'),
            self::TYPE_MODERATION_NO => Yii::t('fair', 'Forbidden')
        );
    }

    static function getModeration($type)
    {
        $types = self::moderation();
        return $types[$type];
    }

    // TODO perhaps artifact - тогда и два метода выше не нужны
    static $moderation = array(
        self::TYPE_MODERATION_OK => 'Разрешено',
        self::TYPE_MODERATION_NO => 'Запрещено'
    );

    static function clearSeoTemplate($result, $isTemplate = false)
    {
        if (!$isTemplate) {
            $result = preg_replace('#\{.*\}#', '', $result);
        }
        return $result;
    }

    public function getSeoName($isTemplate = false)
    {
        $nameParts = [];
        if ($this->exhibitionComplexId && $this->exhibitionComplex) {
            if ($this->exhibitionComplex->cityId && $this->exhibitionComplex->city) {
                $nameParts[] = null != $this->exhibitionComplex->city->shortUrl
                    ? $this->exhibitionComplex->city->shortUrl
                    : 'city-' . $this->exhibitionComplex->city->id;
            }
        }
        $nameParts[] = 'fairs';
        $monthForName = '';
        if ($this->checkRepeatFairs()) {
            $monthForName = '-' . static::$months[date('m', strtotime($this->beginDate))];
            if ($this->checkRepeatFairs(true)) {
                $monthForName = '-' . static::$months[date('m', strtotime($this->beginDate))] . '-' . date('j', strtotime($this->beginDate)) . 'th';
            }
        }
        $nameParts[] = ToTransliteration::getInstance()->replace(static::clearSeoTemplate($this->getSeoHeader($isTemplate)) . $monthForName);

        return static::clearSeoTemplate(implode('/', $nameParts), $isTemplate);
    }

    public function getSeoNameForNewVersion($isTemplate = false)
    {

        $nameParts = [];
        $cityName = 'unknownCity'.crc32($this->name);
        if ($this->exhibitionComplexId && $this->exhibitionComplex) {
            if ($this->exhibitionComplex->cityId && $this->exhibitionComplex->city) {
                $cityName = null != $this->exhibitionComplex->city->shortUrl
                    ? $this->exhibitionComplex->city->shortUrl
                    : 'city-' . $this->exhibitionComplex->city->id;
            }
        }
//        $nameParts[] = 'fairs';
        $monthForName = '';
//        if ($this->checkRepeatFairs()) {
//            if ($this->checkRepeatFairs(true)) {


        $monthForName = '-' . static::$months[date('m', strtotime($this->beginDate))];

        $number = date('j', strtotime($this->beginDate));
        $numberSuffix = 'th';

        if($number == 1){
            $numberSuffix = 'st';
        }

        if($number == 2){
            $numberSuffix = 'nd';
        }

        if($number == 3){
            $numberSuffix = 'rd';
        }

        $number.=$numberSuffix;

        $year = date('Y', strtotime($this->beginDate));

        $monthForName = '-'.$year.'-'.$cityName.'-' . $number . '-' . static::$months[date('m', strtotime($this->beginDate))] ;
//            }
//        }
        $shortUrl = ToTransliteration::getInstance()->replace($this->name . $monthForName);

        return $shortUrl;
    }

    public function getSeoTitle($isTemplate = false, $city = '')
    {
        $titleDatePart = $this->dateForTitle();
        return static::clearSeoTemplate($this->getContent($isTemplate) . ' ' . $titleDatePart . ' ' . $city . ' | '.Yii::app()->params['titleName'], $isTemplate);
    }

    /**
     * Получение дополнительной части с датами для повторяющихся выставок
     * @return string
     */
    public function dateForTitle()
    {
        $result = '';
        if ($this->checkRepeatFairs()) {
            $result = static::getFirstBlockDescriptionSeo(true);
        }
        return $result;
    }

    /**
     * Проверка. Проходит ли выставка более одного раза в год.
     * @param bool $month
     * $month Проверка. Проходит ли выставка более одного раза в месяц.
     * @return bool
     */
    public function checkRepeatFairs($month = false)
    {
        return FALSE;
    }

    public function getSeoHeader($isTemplate = false)
    {
        return static::clearSeoTemplate($this->getContent($isTemplate), $isTemplate);
    }

    public function getContent($isTemplate = false)
    {
        if(!isset($this->translate) || empty($this->translate)){
            return '';
        }

        $nameParts = [];
        $beginDateTimestamp = strtotime($this->beginDate);
        if (stristr($this->name, date('Y', $beginDateTimestamp))) {
            $nameNotDate = str_replace(date('Y', $beginDateTimestamp), '', $this->name);
            if (stristr($nameNotDate, '-')) {
                $nameNotDate = str_replace('-', '', $nameNotDate);
            }
            $nameParts[] = rtrim($nameNotDate) . ' - ' . date('Y', $beginDateTimestamp);
        } else {
            $nameParts[] = $this->name . ' - ' . date('Y', $beginDateTimestamp);
        }

        $result = implode(' ', $nameParts) . '{afterContent}';
        if (!$isTemplate) {
            $result = preg_replace('#\{.*\}#', '', $result);
        }
        return $result;
    }

    public function getSeoDescription()
    {
        $countCompany = H::getCountText($this->members, ['компаний. ', 'компания. ', 'компании. ']);
        $fullSeoDescription = $this->getFirstBlockDescriptionSeo() .
            '.' . ' Участники:' . ' ' . ($this->members === null ? 0 . ' компаний. ' : $countCompany) .
            'Посетители:' . ' ' . ($this->visitors === null ? 0 . ' чел.' : $this->visitors . ' чел.');
        if (mb_strlen($fullSeoDescription, 'UTF-8') > 145) {
            $SeoDescription = $this->getFirstBlockDescriptionSeo() .
                '.' . ' Посетители:' . ' ' . ($this->visitors === null ? 0 . ' чел.' : $this->visitors . ' чел.');
            if (mb_strlen($SeoDescription, 'UTF-8') > 145) {
                return $this->getFirstBlockDescriptionSeo() . '.';
            } else {
                return $SeoDescription;
            }
        } else {
            return $fullSeoDescription;
        }
    }

    public function getFirstBlockDescriptionSeo($title = false)
    {
        $date = null;
        $bMonth = date('n', strtotime($this->beginDate));
        $eMonth = date('n', strtotime($this->endDate));
        $eYear = date('Y', strtotime($this->endDate));
        $arrayMonth = array(
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        );

        if ($arrayMonth[$bMonth] == $arrayMonth[$eMonth]) {
            $date = ($title ? ' ' . Yii::t('fair', 'year') . ', ' : '') . date('j', strtotime($this->beginDate)) . '-' . date('j', strtotime($this->endDate)) . ' ' .
                $arrayMonth[$eMonth] . ' ' . ($title ? '' : $eYear);
        } else {
            $date = Fair::getDateRange($this->beginDate, $this->endDate);
        }

        if ($title) {
            $result = $date;
        } else {
            $result = $date . ' ' . 'в' . ' ' .
                ($this->exhibitionComplex && $this->exhibitionComplex->exhibitionComplexShortType ? $this->exhibitionComplex->exhibitionComplexShortType->name : null) .
                ' ' . ($this->exhibitionComplex ? '"' . $this->exhibitionComplex->name . '"' . ($this->endDate < date('Y-m-d') ? ' прошла ' :
                        ' пройдет ') : null) . ($this->fairType && $this->fairTypeId ? $this->fairType->name : null) . ' '
                . '"' . $this->getSeoHeader() . '"';
        }

        return $result;
    }

    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'integer',
                'label' => Yii::t('fair', 'Active'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'duration' => array(
                'integer',
                'label' => Yii::t('fair', 'Duration'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    ),
                ),
            ),
            'fairIsForum' => array(
                'integer',
                'label' => Yii::t('fair', 'Forum'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
            'frequency' => array(
                'label' => Yii::t('AdminModule.admin', 'Fair frequency'),
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'shard' => array(
                'label' => 'Shard',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'logoId' => array(
                'integer',
                'label' => Yii::t('fair', 'Logo'),
                'rules' => array(
                    'search,sortMatch' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'logoId' => 'id',
                    ),
                ),
            ),
            'infoId' => array(
                'integer',
                'label' => Yii::t('fair', 'infoId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fairInfo' => array(
                'integer',
                'label' => Yii::t('fair', 'infoId'),
                'relation' => array(
                    'CBelongsToRelation',
                    'FairInfo',
                    array(
                        'infoId' => 'id',
                    ),
                ),
            ),
            'mailLogoId' => array(
                'label' => Yii::t('fair', 'Mail logo'),
                'integer',
                'rules' => array(
                    'search,sortMatch' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'mailLogoId' => 'id',
                    ),
                ),
            ),
            'fairTypeId' => array(
                'integer',
                'label' => 'Тип выставки',
                'relation' => array(
                    'CBelongsToRelation',
                    'FairType',
                    array(
                        'fairTypeId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'standId' => array(
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'userId' => array(
                'integer',
                'label' => Yii::t('fair', 'Users'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('fair', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairTranslateName' => array(
                'string',
                'label' => Yii::t('fair', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairEnName' => array(
                'string',
                'label' => 'fairEnName',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairDeName' => array(
                'string',
                'label' => 'fairDeName',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairDiscription' => array(
                'string',
                'label' => 'fairDiscription',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairUniqueText' => array(
                'string',
                'label' => 'fairUniqueText',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'organizerCompanyCount' => array(
                'string',
                'label' => 'organizerCompanyCount',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'organizerContactCount' => array(
                'string',
                'label' => 'organizerContactCount',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'userRedactorEmail' => array(
                'string',
                'label' => 'userRedactorEmail',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'fairHasAssociationId' => array(
                'string',
                'label' => Yii::t('fair', 'fairHasAssociationId'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'statistics' => array(
                'label' => 'Статистика',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'exhibitionComplexId' => array(
                'integer',
                'label' => Yii::t('fair', 'Complex'),
                'rules' => array(
                    'insert,update,load,search' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ExhibitionComplex',
                    array(
                        'exhibitionComplexId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplex_name' => array(
                'label' => Yii::t('fair', 'Exhibition Complex'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'exhibitionComplexName' => array(
                'label' => Yii::t('fair', 'Exhibition Complex'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'cityName' => array(
                'label' => Yii::t('fair', 'City'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'regionName' => array(
                'label' => Yii::t('fair', 'Region'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'districtName' => array(
                'label' => Yii::t('fair', 'Region'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'countryName' => array(
                'label' => Yii::t('fair', 'Country'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'beginDate' => array(
                'date',
                'label' => Yii::t('fair', 'Start date'),
                'rules' => array(
                    'search-form,flow,search,insert,update,load,sortMatch' => array(
                        array('required'),
                        array('safe')
                    )
                ),
            ),
            'endDate' => array(
                'date',
                'label' => Yii::t('fair', 'Expiration date'),
                'rules' => array(
                    'search-form,flow,search,insert,update,load,sortMatch' => array(
                        array('safe')
                    )
                ),
            ),
            'beginMountingDate' => array(
                'date',
                'label' => Yii::t('fair', 'Date of commencement of installation'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe')
                    )
                ),
            ),
            'endMountingDate' => array(
                'date',
                'label' => Yii::t('fair', 'End date of installation'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe')
                    )
                ),
            ),
            'beginDemountingDate' => array(
                'date',
                'label' => Yii::t('fair', 'Date of commencement of dismantling'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe')
                    )
                ),
            ),
            'endDemountingDate' => array(
                'date',
                'label' => Yii::t('fair', 'Date of completion of dismantling'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe')
                    )
                ),
            ),
            'squareGross' => array(
                'integer',
                'label' => Yii::t('fair', 'Square gross'),
                'rules' => array(
                    'search,insert,update,load,sortMatch' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'squareNet' => array(
                'integer',
                'label' => Yii::t('fair', 'Square net'),
                'rules' => array(
                    'search,insert,update,load,sortMatch' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'members' => array(
                'integer',
                'label' => Yii::t('fair', 'Participants'),
                'rules' => array(
                    'search,insert,update,load,sortMatch' => array(
                        array('safe')
                    )
                ),
            ),
            'visitors' => array(
                'integer',
                'label' => Yii::t('fair', 'Visitors'),
                'rules' => array(
                    'search,insert,update,load,sortMatch' => array(
                        array('safe')
                    )
                ),
            ),
            'rating' => array(
                'integer',
                'label' => Yii::t('fair', 'Rating'),
                'rules' => array(
                    'search, insert, update' => array(
                        array('safe'),
                    )
                ),
            ),
            'description' => array(
                'text',
                'label' => Yii::t('fair', 'Description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'datetimeModified' => array(
                'timestamp',
                'label' => Yii::t('fair', 'datetimeModified'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe'),
                    )
                ),
            ),
            'price' => array(
                'integer',
                'label' => Yii::t('fair', 'Cost of accreditation'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'registrationFee' => array(
                'integer',
                'label' => Yii::t('fair', 'Registration fee'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'participationPrice' => array(
                'integer',
                'label' => Yii::t('fair', 'Cost of participation'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'organizerId' => array(
                'integer',
                'label' => Yii::t('fair', 'Organizer'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe')
                    )
                ),
            ),
            'fairHasOrganizer' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasOrganizer',
                    array(
                        'fairId' => 'id',
                    ),
                ),
                'label' => Yii::t('fair', 'Organizer'),
            ),
            'fairHasAudit' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasAudit',
                    array(
                        'fairId' => 'id',
                    ),
                ),
                'label' => Yii::t('fair', 'Fair audit'),
            ),
            'checkBox' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'lang' => array(
                'label' => Yii::t('fair', 'fair lang'),
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerName' => array(
                'label' => Organizer::model()->getAttributeLabel('name'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizerCompanyId' => array(
                'label' => 'organizerCompanyId',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizer_linkToTheSiteOrganizers' => array(
                'label' => Organizer::model()->getAttributeLabel('linkToTheSiteOrganizers'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizer_exhibitionManagement' => array(
                'label' => Organizer::model()->getAttributeLabel('exhibitionManagement'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizer_foodServices' => array(
                'label' => Organizer::model()->getAttributeLabel('foodServices'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizer_advertisingServicesTheFair' => array(
                'label' => Organizer::model()->getAttributeLabel('advertisingServicesTheFair'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizer_servicesLoadingAndUnloading' => array(
                'label' => Organizer::model()->getAttributeLabel('servicesLoadingAndUnloading'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'organizer_servicesBuilding' => array(
                'label' => Organizer::model()->getAttributeLabel('servicesBuilding'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'activateControl' => array(
                'integer',
                'label' => Yii::t('fair', 'Access to Management'),
            ),
            'organizer_phone' => array(
                'label' => Organizer::model()->getAttributeLabel('phone'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                )
            ),
            'organizerEmail' => array(
                'label' => Organizer::model()->getAttributeLabel('email'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                )
            ),
            'exhibitionComplex_cityId' => array(
                'label' => ExhibitionComplex::model()->getAttributeLabel('cityId'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'industryName' => array(
                'label' => Industry::model()->getAttributeLabel('name'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'commonStatisticFair' => array(
                'label' => 'commonStatisticFair',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'commonStatisticFairAssociation' => array(
                'label' => 'commonStatisticFairAssociation',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'fairHasIndustries_industryId' => array(
                'label' => Industry::model()->getAttributeLabel('name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'forumId' => array(
                'label' => Yii::t('fair', 'Fair forum'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'forumName' => array(
                'label' => Yii::t('fair', 'Fair forum'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'exhibitorsLocal' => array(
                'label' => Yii::t('fair','exhibitorsLocal'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'exhibitorsForeign' => array(
                'label' => Yii::t('fair','exhibitorsForeign'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'countriesCount' => array(
                'label' => Yii::t('fair','countriesCount'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'areaClosedExhibitorsLocal' => array(
                'label' => Yii::t('fair','areaClosedExhibitorsLocal'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'areaClosedExhibitorsForeign' => array(
                'label' => Yii::t('fair','areaClosedExhibitorsForeign'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'areaOpenExhibitorsLocal' => array(
                'label' => Yii::t('fair','areaOpenExhibitorsLocal'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'areaOpenExhibitorsForeign' => array(
                'label' => Yii::t('fair','areaOpenExhibitorsForeign'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'areaSpecialExpositions' => array(
                'label' => Yii::t('fair','areaSpecialExpositions'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'visitorsLocal' => array(
                'label' => Yii::t('fair','visitorsLocal'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'visitorsForeign' => array(
                'label' => Yii::t('fair','visitorsForeign'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'fairShortUrl' => array(
                'string',
                'label' => 'fairShortUrl',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'fairHasIndustries' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasIndustry',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'fairHasAssociations' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasAssociation',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'currencyId' => array(
                'label' => 'Валюта',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Currency',
                    array(
                        'currencyId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'originalId' => array(
                'integer',
                'label' => Yii::t('fair', 'Original'),
            ),
            'exhibitionComplex_regionId' => array(
                'label' => ExhibitionComplex::model()->getAttributeLabel('regionId'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'uniqueText' => array(
                'label' => 'Уникальный текст',
                'text',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'filterIds' => array(
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'keyword' => array(
                'label' => 'Ключевое слово',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'statusId' => array(
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'FairStatus',
                    array(
                        'statusId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'fairStatus' => array(
                'label' => 'Статус',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'fairStatusColor' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'auctionTechnicalDataHasFairs' => array(
                'label' => Yii::t('fair', 'exhibition'),
                'relation' => array(
                    'CHasManyRelation',
                    'AuctionTechnicalDataHasFair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'tZs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TZ',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'myFairViewDatas' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'MyFairViewData',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'fairHasComments' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasComment',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'tasks' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Task',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'fairNominations' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairNomination',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'calendarExhibitions' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'CalendarExhibition',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'fairMatches' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairMatches',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'myFairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'MyFair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'documentExhibits' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'DocumentExhibits',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'documentContacts' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'DocumentContacts',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'userHasFairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'UserHasFair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'standHasFairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'StandHasFair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'proposals' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Proposal',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'fairHasFiles' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairHasFile',
                    array(
                        'fairId' => 'id',
                    ),
                ),
            ),
            'buttons' => array(
                'label' => Yii::t('system', 'Actions'),
            ),
            'site' => array(
                'string',
                'label' => Yii::t('fair', 'Site'),
                'rules' => array(
                    'search-form,search,insert,update' => array(
                        array('safe')
                    )
                )
            ),
            'associationName' => array(
                'string',
                'label' => 'Ассоциация',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'auditName' => array(
                'string',
                'label' => Yii::t('fair', 'Fair audit'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrFair',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
//                    'condition' => 'translate.langId = :langId OR translate.langId IS NULL',
//                    'params' => array(':langId' => Yii::app()->language),
                ),
            ),
            'translateRaw' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrFair',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'trFairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrFair',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'leasedArea' => array(
                'label' => Yii::t('RUEFModule.ruef', 'leased area'),
                'integer',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'storyId' => array(
                'label' => 'storyId',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('numerical')
                    )
                ),
            ),
            'statisticsDate' => array(
                'date',
                'label' => Yii::t('AdminModule.admin','statisticsDate'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'userRedactorId' => array(
                'label' => Yii::t('AdminModule.admin','user redactor email'),
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasOneRelation',
                    'User',
                    array(
                        'id' => 'userRedactorId'
                    ),
                )
            ),
            'officialComment' => array(
                'label' => Yii::t('AdminModule.admin','official comment'),
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'lastYearFair' => array(
                'label' => 'lastYearFair',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                )
            ),
            'canceled' => array(
                'label' => Yii::t('fair', 'Canceled'),
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'proposalVersion' => array(
                'label' => 'Proposal version',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbId' => array(
                'label' => 'exdbId',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbCrc' => array(
                'label' => 'exdbCrc',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbFrequency' => array(
                'label' => 'exdbFrequency',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbBusinessSectors' => array(
                'label' => 'exdbBusinessSectors',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbCosts' => array(
                'label' => 'exdbCosts',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbShowType' => array(
                'label' => 'exdbShowType',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'exdbFirstYearShow' => array(
                'label' => 'exdbFirstYearShow',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
            'logoExists' => array(
                'label' => Yii::t('AdminModule.admin', 'Is fair logo loaded'),
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    ),
                ),
            ),
        );
    }

    public function __set($name, $value)
    {
        if (in_array($name, [
                'beginDate',
                'endDate',
                'beginMountingDate',
                'endMountingDate',
                'beginDemountingDate',
                'endDemountingDate',
                'statisticsDate',
            ]) && $value != null
        ) {
            $value = date('Y-m-d', strtotime($value));
        }
        return parent::__set($name, $value);
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            ),
            'SitemapBehavior' => array(
                'class' => 'application.components.SitemapBehavior'
            ),
        );
    }

    static function getDateRange($beginDate, $endDate, $detailView = null)
    {
        $defaultDate = '1970-01-01';
        $bMonth = date('n', strtotime($beginDate));
        $bYear = date('Y', strtotime($beginDate));
        $eYear = date('Y', strtotime($endDate));
        $eMonth = date('n', strtotime($endDate));
        $beginDateNull = $beginDate == null || $beginDate == $defaultDate;
        $endDateNull = $endDate == null || $endDate == $defaultDate;
        if (!$beginDateNull && !$endDateNull) {
            if ($detailView) {
                return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate)) . ' ' . ($bMonth != $eMonth ? static::getMonthData($bMonth) : '') . ' - ' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($eMonth) . ' ' . $eYear;
            } else {
                return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate)) . ' ' . ($bMonth != $eMonth ? static::getMonthData($bMonth) : '') . ' - ' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($eMonth) . ' ' . $eYear;
            }
        } elseif ($beginDateNull) {
            return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate)) . ' - ' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($eMonth) . ' ' . $eYear;
        } elseif ($endDateNull) {
            return (date('j', strtotime($beginDate)) == date('j', strtotime($endDate)) ? date('j', strtotime($beginDate)) : date('j', strtotime($beginDate)) . ' - ' . date('j', strtotime($endDate))) . ' ' . static::getMonthData($bMonth) . ' / ' . '' . $bYear;
        }
    }

    public function getSelfDateRange(){
        return self::getDateRange($this->beginDate, $this->endDate);
    }

    /**
     * @param   string  $beginDate
     * @param   string  $endDate
     * @return string
     */
    public static function getDatesRus($beginDate, $endDate){

        $datetime1 = new DateTime($beginDate);
        $datetime2 = new DateTime($endDate);
        $interval = $datetime1->diff($datetime2);
        $diffDays = $interval->days;

        $date = explode("-", $beginDate);
        $day = date("w", mktime(0, 0, 0, $date[1], $date[2], $date[0]));

        $result = '';

        if($diffDays > 7){

            for ($i = 0; $i <= 6; $i++){
                if($i == 6){
                    $result .= self::$days[$i];
                } else {
                    $result .= self::$days[$i] . ', ';
                }
            }

        } else{
            $k = 0;
            for ($i = 0; $i <= $diffDays; $i++){

                if($day + $k == 7)
                    $k = -$day;

                if($i == $diffDays){
                    $result .= self::$days[$day + $k];
                } else {
                    $result .= self::$days[$day + $k] . ', ';
                }
                $k++;
            }

        }

        return '(' . $result . ')';
    }

    static function getTitleRatingIcon($model)
    {
        if ($model->rating == 1) {
            return Yii::t('AdminModule.admin', 'Statistics confirmed exhibition rating.');
        } elseif ($model->rating == 2) {
            return Yii::t('AdminModule.admin', 'Statistics confirmed exhibition audit.');
        } elseif ($model->rating == 3) {
            return Yii::t('AdminModule.admin', 'Statistics confirmed only organizer of the exhibition.');
        } else {
            return false;
        }
    }

    public function getTitleForRatingIcon(){

        return self::getTitleRatingIcon($this);
    }

    static function getTitleRatingIconByRatingId($ratingId){
        
        if ($ratingId == 1) {
            return Yii::t('AdminModule.admin', 'Statistics confirmed exhibition rating.');
        } elseif ($ratingId == 2) {
            return Yii::t('AdminModule.admin', 'Statistics confirmed exhibition audit.');
        } elseif ($ratingId == 3) {
            return Yii::t('AdminModule.admin', 'Statistics confirmed only organizer of the exhibition.');
        } else {
            return false;
        }
    }

    static function getFairParticipationPrice($participationPrice, $course, $currency)
    {
        if (!Yii::app()->user->isGuest) {
            return $participationPrice ?
                Yii::t('system', 'from ') . number_format($participationPrice * $course, 0, '.', ' ') . ' ' . $currency . '/' . Yii::t('system', 'sqm') :
                Yii::t('system', 'Data not available');
        } else {
            return 'Доступно после ' . CHtml::link('регистрации', '#', ['data-toggle' => 'modal', 'data-target' => '#modal-sign-up']);
        }
    }

    static function getFairRegistrationFee($registrationFee, $course, $currency)
    {
        if (!Yii::app()->user->isGuest) {
            return $registrationFee ?
                Yii::t('system', 'from ') . number_format($registrationFee * $course, 0, '.', ' ') . ' ' . $currency :
                Yii::t('system', 'Data not available');
        } else {
            return 'Доступно после ' . CHtml::link('регистрации', '#', ['data-toggle' => 'modal', 'data-target' => '#modal-sign-up']);
        }
    }

    /**
     * Получение емайл ссылок из строки с емайломи
     * @param $emailString
     * @return string
     */
    public static function getMailLinks($emailString)
    {
        $parts = explode(',', $emailString);
        foreach ($parts as $k => $part) {
            if (stristr($part, '@')) {
                $trimmed = trim($part);
                $parts[$k] = '<a rel="nofollow" href="mailto:' . $trimmed . '">' . $trimmed . '</a>';
            }
        }
        return implode(', ', $parts);
    }

    /**
     * Проверяем, есть ли выставка с таким же именем в другом городе.
     * @return bool
     */
    public function checkDubbingName()
    {
        return FALSE;
    }

    /**
     * Получение имени выставки по ее идентификатору
     * @param $id
     * @return string
     */
    public static function getFairName($id)
    {
        $result = '';
        $model = static::model()->findByPk($id);
        if (null != $model) {
            $result = $model->name;
        }
        
        if (strlen($result) > 20) {
            $result = mb_substr($result, 0, 17, 'utf-8') . '...';
        }

        return $result;
    }

    public static function getBottomButtonClass($id)
    {
        $result = 'b-switch-list-view__cell--bottom-button';
        if (
            Yii::app()->controller->id != 'workspace'
            || (Yii::app()->controller->action->id == 'view' && Yii::app()->controller->id == 'workspace')
        ) {
            $result = 'b-switch-list-view__cell--bottom-button';
        } else {
            $model = self::model()->findByPk($id);
            $currentTime = time();
            $beginDate = strtotime($model->beginDate);
            $endDate = strtotime($model->endDate);
            if (null != $model) {
                if ($beginDate < $currentTime && $endDate > $currentTime) {
                    $result = 'b-switch-list-view__cell--bottom-button-active';
                } elseif ($endDate < $currentTime) {
                    $result = 'b-switch-list-view__cell--bottom-button-completed';
                    if (Yii::app()->controller->id == 'workspace' && Yii::app()->controller->action->id == 'archive') {
                        $result .= '-archive';
                    }
                } elseif ($beginDate > $currentTime) {
                    $result = 'b-switch-list-view__cell--bottom-button-soon';
                }
            }
        }
        return $result . ' b-switch-list-view__cell--bottom-block';
    }

    /**
     * Получение дней для еще не начавшихся выставок
     * @param $beginDate
     * @return string
     */
    public static function getSoonTime($beginDate)
    {
        $currentTime = time();
        $beginDate = strtotime($beginDate);
        if ($currentTime < $beginDate) {
            $d = ($beginDate - $currentTime) / (24 * 60 * 60);
            return H::getCountText(floor($d), array('дней', 'день', 'дня'));

        }
    }

    /**
     * Получение дней для уже идущей выставки
     * @param $beginDate
     * @return string
     */
    public static function getActiveTime($beginDate)
    {
        $currentTime = time();
        $beginDate = strtotime($beginDate);
        if ($currentTime > $beginDate) {
            $d = ($currentTime - $beginDate) / (24 * 60 * 60);
            return floor($d) . ' день';

        }
    }

    public static function getDataForBottomButton($id, $beginDate)
    {
        $data = [
            'text',
            'icon-time'
        ];

        if (stristr(Fair::getBottomButtonClass($id), 'active')) {
            $data['text'] = Yii::t('fair', 'The exhibition runs') . ' ';
            $data['icon-time'] = Fair::getActiveTime($beginDate);;
        } elseif (stristr(Fair::getBottomButtonClass($id), 'soon')) {
            $data['text'] = Yii::t('fair', 'Left until start') . ' ';
            $data['icon-time'] = Fair::getSoonTime($beginDate);
        } elseif (stristr(Fair::getBottomButtonClass($id), 'completed-archive')) {
            $data['text'] = Yii::t('fair', 'The archive');
            $data['icon-time'] = '';
        } elseif (stristr(Fair::getBottomButtonClass($id), 'completed')) {
            $icon = '';
            $data['text'] = CHtml::link(Yii::t('fair', 'Completed'), 'javascript://');
            $data['icon-time'] = CHtml::link(
                '',
                'javascript://',
                [
                    'class' => 'btn b-button js-add-to-archive',
                    'data-icon' => $icon,
                    'data-id' => $id
                ]);
        } else {
            if (Yii::app()->user->isGuest) {
                // @TODO add back url to add workspace/addFair
                $icon = '';
                $data['text'] = CHtml::link(
                    'Начать подготовку к выставке',
                    'javascript://',
                    [
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-sign-up',
                    ]
                );
                $data['icon-time'] = CHtml::link(
                    '',
                    'javascript://',
                    [
                        'class' => 'btn b-button',
                        'data-icon' => $icon,
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-sign-up',
                    ]
                );
            } else {
                $icon = '';
                $tariff = User::getTariff();
                $url = 'javascript://';
                /*if ($tariff && $tariff == 1) {*/
                $url = Yii::app()->createUrl('workspace/panel/addFair', ['id' => $id]);
                /*} else {
                    $url = Yii::app()->createUrl('tariff/choice', ['fairId' => $id]);
                }*/

                $data['text'] = CHtml::link('Начать подготовку к выставке', $url);
                $data['icon-time'] = CHtml::link(
                    '',
                    $url,
                    ['class' => 'btn b-button', 'data-icon' => $icon]
                );
            }
        }

        return $data;
    }

    public function getCountryStyle(){
        // @TODO Remove hardcode !!! Use shortUrl of country.
        return 'rus';
    }

    public function beforeSave()
    {
        $this->datetimeModified = new CDbExpression('CURRENT_TIMESTAMP');

        if(empty($this->shard) || $this->shard == '' || $this->shard == ' '){
            $this->shard = NULL;
        }

        return parent::beforeSave();
    }

    public function afterSave()
    {
        parent::afterSave(); // TODO: Change the autogenerated stub
    }

    /**
     * @param $lang
     * @return mixed|null|static
     */
    public function getTrNameByLang($lang){

        if(empty($lang)){
            return NULL;
        }

        $trName = TrFair::model()->findByAttributes(array(
            'langId' => $lang,
            'trParentId' => $this->id,
        ));

        if($trName === NULL){
            return NULL;
        }

        return $trName->name;
    }

    /**
     * @return bool|string
     */
    public function getMailLogoPath(){
        $path = FALSE;

        if(!empty($this->mailLogoId) && !empty($logotype = ObjectFile::model()->findByPk($this->mailLogoId))) {
            $path = Yii::app()->getBaseUrl(true).'/uploads/fairMail/' . $this->id . '/logo/' . $logotype->name;
        }

        return $path;
    }

    public function getFullName(){
        return $this->loadName();
    }

    public function getFullDescription(){
        return $this->loadDescription();
    }

    /**
     * @return null|string
     */
    public function loadName()
    {
        return $this->loadTr('TrFair', 'name');
    }

    /**
     * @return null|string
     */
    public function loadDescription()
    {
        return $this->loadTr('TrFair', 'description');
    }

    /**
     * @TODO move it into application component (get languages list())
     * @return array
     */
    public static function getLanguageList(){
        return array(
            'ru',
            'en',
            'de',
        );
    }

    /**
     * @return array
     */
    public function combineIndustries(){
        return CHtml::listData($this->fairHasIndustries, 'industryId', function($fairHasIndustry){
            $res = NULL;

            if(!empty($fairHasIndustry->industry->name)){
                $res = $fairHasIndustry->industry->name;
            }

            return $res;
        });
    }

    public function getCombineIndustries(){
        return $this->combineIndustries();
    }

    public function getFairRegionName(){
        return !empty($this->exhibitionComplex->city->region->name)?$this->exhibitionComplex->city->region->name:NULL;
    }

    public function getFairRegionInformationId(){
        return !empty($this->exhibitionComplex->city->region->regionInformation->id)?$this->exhibitionComplex->city->region->regionInformation->id:NULL;
    }

    public function getFairCityName(){
        return !empty($this->exhibitionComplex->city->name)?$this->exhibitionComplex->city->name:NULL;
    }

    public function trAttributeNames(){
        return [
            'name',
            'shortUrl',
            'description',
            'uniqueText',
            'statistics',
        ];
    }

    public function updateShortUrl(){
        $this->shortUrl = $this->getSeoNameForNewVersion();
    }

    /**
     * @param   integer   $fairId
     * @return string
     */
    public static function getAssociationsByString($fairId){

        $result = '';
        $fairAssociations = FairAdminService::getAssociationsByFairId($fairId);
        $countFairAssociations = count($fairAssociations);

        for ($i=0; $i < $countFairAssociations; $i++){
            if($i == $countFairAssociations - 1) {
                $result .= $fairAssociations[$i]['name'];
            } else {
                $result .= $fairAssociations[$i]['name'] . ',';
            }
        }
        
        return $result;
    }

    /**
     * @return array|CDbDataReader
     */
    public static function fairsYear() {

        $sql = "SELECT DISTINCT YEAR(f.beginDate) AS fairsYear FROM tbl_fair f 
                    WHERE f.active = :active AND f.canceled = :canceled ORDER BY fairsYear DESC";

        $res = Yii::app()->db->createCommand($sql)->queryColumn(
            array(
                ':active' => Fair::ACTIVE_ON,
                ':canceled' => Fair::CANCELED_NO,
            )
        );

        return $res;
    }

    public static function getWorkFairsIds(){
        $criteria = new CDbCriteria();
        $criteria->condition = 'shard IS NOT NULL AND shard != "" AND shard != " "';

        return array_keys(CHtml::listData(Fair::model()->findAll($criteria), 'id', 'id'));
    }

    public static function getFairYears(){
        $years = array();
        $query = "SELECT DISTINCT YEAR(f.beginDate) AS ye FROM tbl_fair f WHERE f.active = :active AND f.beginDate IS NOT NULL ORDER BY YEAR(f.beginDate)";
        $data = Yii::app()->db->createCommand($query)->queryAll(TRUE, array(':active' => Fair::ACTIVE_ON));

        foreach ($data as $year)
            $years[] = $year['ye'];
        return $years;
    }
}