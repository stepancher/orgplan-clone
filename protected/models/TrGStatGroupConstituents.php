<?php

/**
 * Class TrGStatGroupConstituents
 *
 */
class TrGStatGroupConstituents extends \AR{

    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'trParentId' => array(
                'label' => 'trParentId',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'GStatGroupConstituents',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'langId' => array(
                'string',
                'label' => 'lang',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => 'name',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
        );
    }
}