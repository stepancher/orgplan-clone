<?php


/**
 * Class RegionInformation
 */
class RegionInformation extends \AR
{
    public static $_description = NULL;

    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;

    public $logo;
    public $mainImage;

    const SQUARE_STAT = 1;
    const REGIONAL_PRODUCT_STAT = 2;
    const PEOPLE_COUNT_STAT = 3;
    const TRADE_TURNOVER_STAT = 4;
    const PAGE_SIZE = 3;

    /**
     * @var string
     */
    public $searchableDefaultSort = ['name' => 'asc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'name' => 'name'
    ];

    /**
     * Поля-исключения, в которых не нужно делать проверку purify
     * @var array
     */
    public $notPurifyAttributes = [
        'text',
        'analytics'
    ];

    static $ratings = array(
        '0' => 'Рейтинг обновляется',
        '-1' => 'Рейтинг не задан',
        '1A' => '1A - максимальный потенциал - минимальный риск',
        '2A' => '2A - средний потенциал - минимальный риск',
        '3A' => '3A - (Низкий потенциал - минимальный риск)',
        '1B' => '1B - высокий потенциал - умеренный риск',
        '2B' => '2B - средний потенциал - умеренный риск',
        '3B1' => '3B1 - пониженный потенциал - умеренный риск',
        '3B2' => '3B2 - незначительный потенциал - умеренный риск',
        '1C' => '1C - высокий потенциал - высокий риск',
        '2C' => '2C - средний потенциал - высокий риск',
        '3C1' => '3C1 - пониженный потенциал - высокий риск',
        '3C2' => '3C2 - незначительный потенциал - высокий риск',
        '3D' => '3D - низкий потенциал - экстремальный риск',
        '3A1' => '3A1 - пониженный потенциал - минимальный риск',
        '3A2' => '3A2 - незначительный потенциал - минимальный риск',
    );

    public static function getStats(){

       return array(
            self::SQUARE_STAT => '577e6f18bd4c5654298b5794',
            self::REGIONAL_PRODUCT_STAT => '56ec7dff75d897c606a45c51',
            self::PEOPLE_COUNT_STAT => '56ec793a75d897c6069d7b82',
            self::TRADE_TURNOVER_STAT => '572b3ec076d89734208b4ae7',
        );
    }

    public function getSeoName()
    {
        $nameParts = [];
        if ($this->regionId && $this->region) {
            if ($this->region->district && $this->region->district->country) {
                $nameParts[] = null != $this->region->district->country->shortUrl
                    ? $this->region->district->country->shortUrl
                    : 'country-' . $this->region->district->country->id;

            }
            $nameParts[] = null != $this->region->shortUrl
                ? $this->region->shortUrl
                : 'region-' . $this->id;
        }

        return implode('/', $nameParts);
    }

    public function getSeoTitle()
    {
        return $this->getName();
    }

    public function getName()
    {
        $name = null;
        if ($this->regionId && $this->region) {
            $name = $this->region->name;
        }
        return $name;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'countryId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'districtId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'regionId' => array(
                'integer',
                'label' => 'Регион',
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'regionId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                        array('unique', 'message' => Yii::t('regionInformation','For this region, the information has already been created.')
                        ),
                    )
                ),
            ),
            'region_name' => array(
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    )
                ),
            ),
            'name' => array(
                'label' => Yii::t('regionInformation', 'Name'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'integer',
                'label' => 'Статус',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'analytics' => array(
                'text',
                'label' => 'Аналитика',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'area' => array(
                'label' => 'Площадь',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'population' => array(
                'string',
                'label' => 'Население',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'regionalProduct' => array(
                'integer',
                'label' => 'Валовый региональный продукт',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'tradeTurnover' => array(
                'label' => 'Оборот розничной торговли',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fair' => array(
                'string',
                'label' => 'Выставки',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'members' => array(
                'label' => 'Выставочные центры',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'rating' => array(
                'label' => 'Рейтинг',
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'header' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'imageId' => array(
                'label' => 'Заглавное изображение',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'imageId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'label' => 'Активный',
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'sourceUrl' => array(
                'string',
                'label' => 'Источник',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'logo' => [
                'label' => Yii::t('regionInformation', 'Logo')
            ],
            'mainImage' => [
                'label' => Yii::t('regionInformation', 'MainImage')
            ],
			'hasGregion' => array(
                'label' => 'gRegion',
                'relation' => array(
                    'CBelongsToRelation',
                    'GObjectHasRegion',
                    array(
                        'regionId' => 'regionId',
                    ),
                ),
            ),
            'trRegionInformations' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrRegionInformation',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'regionInformationHasFiles' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'RegionInformationHasFile',
                    array(
                        'regionInformationId' => 'id',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrRegionInformation',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }

    /**
     * @return bool
     * Рейтинг 0 после создания новой записи
     */
    public function beforeSave()
    {
        if ($this->rating == null) {
            $this->rating = 0;
        }
        return parent::beforeSave();
    }

    public function getRating($attribute)
    {
        /**
         * RegionInformation[] $models
         */
        $arr = [];
        $models = RegionInformation::model()->findAll();
        foreach ($models as $model) {
            if ($model->{$attribute}) {
                if ($model->region) {
                    $arr[$model->region->name] = $model->{$attribute};
                }
            }
        }
        arsort($arr, SORT_NUMERIC);

        $rating = [];
        $i = 1;
        foreach ($arr as $key => $val) {
            $rating[$key] = $i++;
        }

        return ($rating);
    }

    public static function getActive($active = null)
    {
        if (null != $active) {
            $data = self::getActive();
            if (isset($data[$active])) {
                return $data[$active];
            } else {
                return null;
            }
        }

        return [
            self::ACTIVE_ON => Yii::t('system', 'Active'),
            self::ACTIVE_OFF => Yii::t('system', 'Not active')
        ];
    }

    public function getLogo()
    {
        if (isset($this->regionInformationHasFiles[1]) && $this->regionInformationHasFiles[1]->file->type == ObjectFile::TYPE_REGION_INFORMATION_LOGO) {
            $linkAssets = H::getImageUrl($this->regionInformationHasFiles[1], 'file');
        } elseif (isset($this->regionInformationHasFiles[0]) && $this->regionInformationHasFiles[0]->file->type == ObjectFile::TYPE_REGION_INFORMATION_LOGO) {
            $linkAssets = H::getImageUrl($this->regionInformationHasFiles[0], 'file');
        } else {
            $linkAssets = H::getImageUrl(null, 'file');
        }

        return CHtml::image($linkAssets, '', ['style' => 'max-width:300px;max-height:300pxp;']);
    }

    public function search($criteria = null, $ignoreCompareFields = array())
    {
        $dCriteria = new CDbCriteria;
        $dCriteria->compare('region.name', $this->{'region_name'}, true);
        if (null !== $criteria) {
            $dCriteria->mergeWith($criteria);
        }
        return parent::search($dCriteria, ['region.name']);
    }

    /**
     * @param $statId
     * @return mixed|null|static
     */
    public function getStatisticObject($statId){
        Yii::import('app.modules.gradoteka.models.*');

        if(!isset(self::getStats()[$statId]) || empty($stat = self::getStats()[$statId])){
            return NULL;
        }

        if(!isset($this->hasGregion, $this->hasGregion->obj, $this->hasGregion->obj->id) || empty($this->hasGregion->obj->id)){
            return NULL;
        }

        $set = GObjectsHasGStatTypes::model()->findByAttributes(array(
            'objId' => $this->hasGregion->objId,
            'statId' => $stat,
            'gType' => 'region',
        ));

        if($set === NULL){
            return NULL;
        }

        $gValue = GValue::model()->findByAttributes(array(
            'setId' => $set->id,
            'epoch' => AnalyticsService::DEFAULT_EPOCH,
        ));
        
        if($gValue === NULL){

            for($i = 1; $i <= 3; $i++){

                $gValue = GValue::model()->findByAttributes(array(
                    'setId' => $set->id,
                    'epoch' => AnalyticsService::DEFAULT_EPOCH - $i,
                ));

                if($gValue !== NULL){
                    break;
                }
            }
        }

        return $gValue;
    }

    public function getSeoDescription(){

        return $this->name.': площадь '.$this->area.' тыс.км². Население: '.$this->population.' тыс.чел. '.$this->descriptionSnippet;
    }

    /**
     * @return string
     */
    public function getText(){

        $trString = '';

        if(!empty($this->translate->text)){
            $trString = $this->translate->text;
        }

        return $trString;
    }

    /**
     * @return string
     */
    public function getDescriptionSnippet(){

        $trString = '';

        if(!empty($this->translate->descriptionSnippet)){
            $trString = $this->translate->descriptionSnippet;
        }

        return $trString;
    }
}