<?php

/**
 * @property integer $imageId
 * @property \ObjectFile $image
 *
 * Class MassMedia
 */

class MassMedia extends \AR
{
    public static $_description = NULL;

    /**
     * Поля-исключения, в которых не нужно делать проверку purify
     * @var array
     */
    public $notPurifyAttributes = [
        'description'
    ];

    /**
     * @param $result
     * @param bool|false $isTemplate
     * @return mixed
     */
    static function clearSeoTemplate($result, $isTemplate = false)
    {
        if (!$isTemplate) {
            $result = preg_replace('#\{.*\}#', '', $result);
        }
        return $result;
    }

    /**
     * @param bool|false $isTemplate
     * @return mixed
     */
    public function getSeoTitle($isTemplate = false)
    {
        return static::clearSeoTemplate(
            $this->exhibitionComplexType
                ? $this->name . ' "' . $this->name . '"{afterContent}' .
                $this->getCityForTitle() . ' | '.Yii::app()->params['titleName']
                : '"' . $this->name . '"{afterContent}' . $this->getCityForTitle() . ' | '.Yii::app()->params['titleName'],
            $isTemplate
        );
    }

    /**
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->description;
    }

    /**
     * @param bool|false $isTemplate
     * @return mixed
     */
    public function getSeoHeader($isTemplate = false)
    {
        return static::clearSeoTemplate(
            $this->name
                ? $this->name . ' ' . $this->name
                : $this->name,
            $isTemplate
        );
    }

    /**
     * @return array
     */
    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('massMedia', 'name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'description' => array(
                'string',
                'label' => Yii::t('massMedia', 'description'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'url' => array(
                'string',
                'label' => Yii::t('massMedia', 'url'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'massMediaHasFiles' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'MassMediaHasFile',
                    array(
                        'massMediaId' => 'id',
                    ),
                ),
            ),
            'active' => array(
                'integer',
                'label' => Yii::t('massMedia', 'active'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'imageId' => array(
                'label' => Yii::t('massMedia', 'imageId'),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'industryHasMassMedia' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'IndustryHasMassMedia',
                    array(
                        'massMediaId' => 'id',
                    ),
                ),
            ),
        );
    }

    /**
     * Получение объекта для вывода заглавного изображения
     * @return bool|ObjectFile
     */
    public function getMainImage()
    {
        $result = false;
        if ($this->massMediaHasFiles) {
            foreach ($this->massMediaHasFiles as $file) {
                if ($file->file && $file->file->type == ObjectFile::TYPE_MASS_MEDIA_IMAGE) {
                    $result = $file;
                }
            }
        }
        return $result;
    }

	public function beforeDelete(){

		if (parent::beforeDelete()) {
			IndustryHasMassMedia::model()->deleteAllByAttributes(array('massMediaId' => $this->id));

			return true;
		}
	}

    /**
     * @return string
     */
    public function getSeoName(){
        return 'seo name';
    }

    public static function getMassMediaList(){
        return CHtml::listData(MassMedia::model()->findAll(),'id','name');
    }


    /**
     * @return array
     */
    public static function getSizes(){
        return array(
//            620 => 348,
//            300 => 168,
            420 => 300,
            300 => 220,
            280 => 200,
            140 => 100,
        );
    }
}


