<?php
/**
 * Class Tariff
 */
class Tariff extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'label' => 'Название',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'created' => array(
				'date',
				'label' => 'Создан',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'price' => array(
				'integer',
				'label' => 'Цена',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'modified' => array(
				'timestamp',
				'label' => 'Обновлен',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'users' => array(
				'relation' => array(
					'CHasManyRelation',
					'User',
					array(
						'tariffId' => 'id',
					),
				),
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'tariffDiscountId' => array(
				'label' => 'Скидка',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'TariffDiscount',
					array(
						'tariffDiscountId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'tariffReports' => array(
				'relation' => array(
					'CHasManyRelation',
					'TariffReport',
					array(
						'tariffId' => 'id',
					),
				),
			),
		);
	}
}