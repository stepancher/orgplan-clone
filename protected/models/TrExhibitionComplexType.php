<?php
/**
 * Class TrExhibitionComplexType
 */
class TrExhibitionComplexType extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'label' => 'Округ',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'ExhibitionComplexType',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'label' => 'Язык',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'label' => 'Наименование',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'fullNameSingle' => array(
				'string',
				'label' => 'Полное имя в едином числе',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'shortUrl' => array(
				'label' => 'Короткая ссылка',
				'string',
			),
		);
	}
}