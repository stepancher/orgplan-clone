<?php
/**
 * Class RegionInformationHasFile
 */
class RegionInformationHasFile extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'regionInformationId' => array(
				'label' => 'Регион',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'RegionInformation',
					array(
						'regionInformationId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'fileId' => array(
				'integer',
				'label' => 'Файл',
				'relation' => array(
					'CBelongsToRelation',
					'ObjectFile',
					array(
						'fileId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}