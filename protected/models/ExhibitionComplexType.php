<?php
/**
 * Class ExhibitionComplexType
 */
class ExhibitionComplexType extends \AR
{
	public static $_description = NULL;
	public $exhibitionComplexCount;

	/**
	 * @return string
	 */
	public function getName()
	{
		$name = '';
		$translate = $this->translate;
		if (isset($translate->name)) {
			$name = $translate->name;
		}
		return $name;
	}

	/**
	 * @return string
	 */
	public function getShortUrl()
	{
		$name = '';
		$translate = $this->translate;
		if (isset($translate->shortUrl)) {
			$name = $translate->shortUrl;
		}
		return $name;
	}

    public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'fullNameSingle' => array(
				'string',
				'label' => 'Полное имя в едином числе',
			),
			'trExhibitionComplexTypes' => array(
				'relation' => array(
					'CHasManyRelation',
					'TrExhibitionComplexType',
					array(
						'trParentId' => 'id',
					),
				),
			),
			'translate' => array(
				'relation' => array(
					'CHasOneRelation',
					'TrExhibitionComplexType',
					array(
						'trParentId' => 'id',
					),
					'on' => 'translate.langId = "'.Yii::app()->language.'"',
				),
			),
			'exhibitionComplex' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasOneRelation',
					'ExhibitionComplex',
					array(
						'exhibitionComplexTypeId' => 'id',
					),
				),
			),
		);
	}

	public function getSeoName() {
		return strtolower($this->shortUrl);
	}

	public function behaviors()
	{
		return array(
			'TranslateBehavior' => array(
				'class' => 'application.components.TranslateBehavior'
			)
		);
	}

	public static function getTypes(){
		return CHtml::listData(self::model()->findAll(), 'id', 'fullNameSingle');
	}
}