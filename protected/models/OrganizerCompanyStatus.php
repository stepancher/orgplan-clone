<?php
/**
 * Class OrganizerCompanyStatus
 */

class OrganizerCompanyStatus extends \AR
{
	public $id;
	public $name;
	public $color;

	public function tableName()
	{
		return '{{organizercompanystatus}}';
	}

	public function rules()
	{
		return array(
			array('id,
			name, 
			color',
				'safe', 'on' => 'search,insert,update'),
		);
	}

	public static function statuses(){
		$data = array();
		$query = "SELECT name FROM tbl_organizercompanystatus ORDER BY name";

		$statuses = Yii::app()->db->createCommand($query)->queryAll();

		if(empty($statuses)){
			return array();
		}

		foreach ($statuses as $status){
			$data[$status['name']] = $status['name'];
		}

		return $data;
	}

	/**
	 * @return array
	 */
	public static function getStatuses(){
		return CHtml::listData(self::model()->findAll(), 'id', 'name');
	}
}