<?php

/**
 * Class RegionHasMassMedia
 */

class RegionHasMassMedia extends \AR{

    public static $_description = NULL;

    public function description(){
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'regionId' => array(
                'integer',
                'label' => 'regionId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'massMediaId' => array(
                'integer',
                'label' => 'massMediaId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'MassMedia',
                    array(
                        'massMediaId' => 'id',
                    ),
                ),
            ),
            'isGeneral' => array(
                'integer',
                'label' => 'isGeneral',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
            'region' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
        );
    }

}


