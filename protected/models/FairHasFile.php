<?php
/**
 * Class FairHasFile
 */
class FairHasFile extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'fairId' => array(
				'integer',
				'label' => 'Выставка',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CBelongsToRelation',
					'Fair',
					array(
						'fairId' => 'id',
					),
				),
			),
			'fileId' => array(
				'integer',
				'label' => 'Файл',
				'relation' => array(
					'CBelongsToRelation',
					'ObjectFile',
					array(
						'fileId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}