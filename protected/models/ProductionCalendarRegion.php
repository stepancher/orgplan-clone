<?php
/**
 * Class ProductionCalendarRegion
 */
class ProductionCalendarRegion extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'productionCalendarId' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'region' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'day' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'regionId' => array(
				'label' => 'Идентификатор региона',
				'integer',
				'relation' => array(
					'CHasManyRelation',
					'ProductionCalendarDay',
					array(
						'region' => 'regionId',
					),
				),
			),
			'name' => array(
				'string',
				'label' => 'Имя',
			),
		);
	}

	public static function getAllRegions()
	{
		$result = [];
		$models = self::model()->findAll();
		if(null != $models) {
			$result = CHtml::listData($models, 'regionId', 'name');
		}
		return $result;
	}
}