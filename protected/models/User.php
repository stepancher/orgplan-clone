<?php

/**
 * Пользователь системы
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property integer $role
 * @property integer $active
 * @property boolean $rememberMe
 * @property string $email
 * @property string $termsOfUse
 * @property Contact $contact
 */
class User extends \AR
{
    public static $_description = NULL;

    const ROLE_ADMIN = 1;
    const ROLE_OPERATOR = 2;
    const ROLE_EXPONENT = 3;
    const ROLE_DEVELOPER = 4;
    const ROLE_FREELANCER = 5;
    const ROLE_EXPERT = 6;
    const ROLE_ORGANIZERS = 7;
    const ROLE_NOT_FILLED_EXPONENT = 9;
    const ROLE_NOT_FILLED_DEVELOPER = 10;
    const ROLE_NOT_FILLED_FREELANCER = 11;
    const ROLE_USER = 12;
    const ROLE_GUEST = 'guest';
    const ROLE_RUEF = 13;
    const ROLE_EXPONENT_SIMPLE = 14;
    const ROLE_MANAGER = 15;

    const BANNED_ON = 1;
    const BANNED_OFF = 0;

    const ACTIVE_ON = 1;
    const ACTIVE_OFF = 0;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * Символы разрешенные для использования в пароле
     * @var array
     */
    public $passwordLengthsMap = [
        'A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e',
        'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i', 'J', 'j',
        'K', 'k','L', 'l','M', 'm','N', 'n','O', 'o','P',
        'p','Q', 'q','R', 'r','S', 's','T', 't','U', 'u',
        'V', 'v','W', 'w','X', 'x','Y', 'y','Z', 'z', 1,
        2, 3, 4, 5, 6, 7, 8, 9, 0
    ];

    /**
     * @var string
     */
    public $searchableDefaultSort = ['name' => 'asc'];

    /**
     * @var array
     */
    public $searchableSortFields = [
        'name' => 'firstName',
        'rating' => 'rating',
        'category' => 'servicesName',
        'region' => 'regionName',
        'city' => 'cityName',
    ];

    public $_cityId;

    public static function roles()
    {
        return array(
            self::ROLE_ADMIN => Yii::t('user', 'Administrator'),
            self::ROLE_RUEF => Yii::t('user', 'RUEF'),
            self::ROLE_OPERATOR => Yii::t('user', 'Operator'),
            self::ROLE_EXPONENT => Yii::t('user', 'Exhibitor'),
            self::ROLE_DEVELOPER => Yii::t('user', 'Developers'),
            self::ROLE_FREELANCER => Yii::t('user', 'Freelancer'),
            self::ROLE_EXPERT => Yii::t('user', 'Expert'),
            self::ROLE_ORGANIZERS => Yii::t('user', 'Organizer'),
            self::ROLE_USER => Yii::t('user', 'User'),
            self::ROLE_MANAGER => Yii::t('user', 'Manager'),
        );
    }

    /**
     * Получение названия Роли пользователя
     * @param $roleId
     * @return null
     */
    public static function getRole($roleId)
    {
        $result = null;
        $roles = self::roles();

        if(isset($roles[$roleId])) {
            $result = $roles[$roleId];
        }

        return $result;
    }

    public static function notFilledRoles()
    {
        return array(
            self::ROLE_NOT_FILLED_EXPONENT => Yii::t('user', 'Exhibitor (limited)'),
            self::ROLE_NOT_FILLED_DEVELOPER => Yii::t('user', 'Developer (limited)'),
            self::ROLE_NOT_FILLED_FREELANCER => Yii::t('user', 'Freelancer (limited)'),
            self::ROLE_GUEST => Yii::t('user', 'Guest'),
            self::ROLE_EXPONENT_SIMPLE => Yii::t('user', 'Exponent simple'),
        );
    }

    static $rolesConstantString = array(
        self::ROLE_ADMIN => 'User::ROLE_ADMIN',
        self::ROLE_RUEF => 'User::ROLE_RUEF',
        self::ROLE_OPERATOR => 'User::ROLE_OPERATOR',
        self::ROLE_EXPONENT => 'User::ROLE_EXPONENT',
        self::ROLE_DEVELOPER => 'User::ROLE_DEVELOPER',
        self::ROLE_FREELANCER => 'User::ROLE_FREELANCER',
        self::ROLE_EXPERT => 'User::ROLE_EXPERT',
        self::ROLE_ORGANIZERS => 'User::ROLE_ORGANIZERS',
        self::ROLE_NOT_FILLED_EXPONENT => 'User::ROLE_NOT_FILLED_EXPONENT',
        self::ROLE_NOT_FILLED_DEVELOPER => 'User::ROLE_NOT_FILLED_DEVELOPER',
        self::ROLE_NOT_FILLED_FREELANCER => 'User::ROLE_NOT_FILLED_FREELANCER',
        self::ROLE_USER => 'User::ROLE_USER',
        self::ROLE_GUEST => 'User::ROLE_GUEST',
        self::ROLE_EXPONENT_SIMPLE  => 'User::ROLE_EXPONENT_SIMPLE',
    );

    public static function actives()
    {
        return array(
            self::ACTIVE_ON => Yii::t('user', 'Activated'),
            self::ACTIVE_OFF => Yii::t('user', 'Disabled'),
        );
    }

    /**
     * Получение значения активности пользователя
     * @param $activeId
     * @return null
     */
    public static function getActiveStatus($activeId)
    {
        $result = null;
        $actives = self::actives();

        if(isset($actives[$activeId])) {
            $result = $actives[$activeId];
        }

        return $result;
    }

    public static function bannes()
    {
        return array(
            self::BANNED_ON => Yii::t('user', 'Disabled'),
            self::BANNED_OFF => Yii::t('user', 'Activated'),
        );
    }

    /**
     * Получение статуса бана (активен/забанен)
     * @param $bannedId
     * @return null
     */
    public static function getBannedStatus($bannedId)
    {
        $result = null;
        $actives = self::bannes();

        if(isset($actives[$bannedId])) {
            $result = $actives[$bannedId];
        }

        return $result;
    }

    public static function genders()
    {
        return array(
            self::GENDER_MALE => Yii::t('user', 'male'),
            self::GENDER_FEMALE => Yii::t('user', 'female'),
        );
    }

    /**
     * @param $genderId
     * @return string
     */
    public static function getGender($genderId)
    {
        $genders = static::genders();
        if (isset($genders[$genderId])) {
            return $genders[$genderId];
        }
        return '';
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,searchByTariff' => array(
                        array('safe')
                    ),
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                )
            ),
            'banned' => array(
                'bool',
                'label' => Yii::t('user', 'Banned'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                        array('numerical', 'integerOnly' => true),
                        array('default', 'value' => User::BANNED_OFF, 'setOnEmpty' => true)
                    ),
                    'insert,update' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'login' => array(
                'string',
                'label' => Yii::t('user', 'Login'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('length', 'min' => 4, 'max' => 20),
                        array('unique', 'message' => Yii::t('user', 'This username is already taken.')),
                    ),
                    'export,import,searchByTariff,another-forms' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'password' => array(
                'string',
                'label' => Yii::t('user', 'Password'),
                'rules' => array(
                    'search,another-forms' => array(
                        array('safe'),
                    ),
                    'login,registration,insert,update' => array(
                            array('required'),
                    ),
                    'registration-main,registration,registration-left-menu' => array(
                        array('length', 'min' => 6),
                        array('checkPassword')
                    ),
                    'registration-main' => array(
                        array('required'),
                    ),
                    'registration-left-menu' => array(
                        array('required'),
                    ),
                    'admin' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'role' => array(
                'integer',
                'label' => Yii::t('user', 'User type'),
                'rules' => array(
                    'search,insert,update,search-user' => array(
                        array('required'),
                        array('numerical', 'integerOnly' => true),
                    ),
                    'export,import,searchByTariff' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'firstName' => array(
                'string',
                'label' => Yii::t('user', 'First Name'),
                'rules' => array(
                    'update' => array(
                        array('safe'),
                    ),
                    'registration' => array(
                        array('required'),
                    ),
                    'registration-main' => array(
                        array('required'),
                    ),
                    'registration-left-menu' => array(
                        array('required'),
                    ),
                    'search,insert,update,search-user' => array(
                        array('safe'),
                    ),
                    'export,import,searchByTariff' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                )
            ),
            'lastName' => array(
                'string',
                'label' => Yii::t('user', 'Last Name'),
                'rules' => array(
                    'search,search-user' => array(
                        array('safe'),
                    ),
                    'update' => array(
                        array('safe'),
                    ),
                    'export,import,searchByTariff' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'middleName' => array(
                'string',
                'label' => Yii::t('user', 'Middle Name'),
                'rules' => array(
                    'search,search-user' => array(
                        array('safe'),
                    ),
                    'update' => array(
                        array('safe'),
                    ),
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'gender' => array(
                'integer',
                'label' => Yii::t('user', 'Gender'),
                'rules' => array(
                    'search,search-user,insert,update,export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                )
            ),
            'dob' => array(
                'datetime',
                'label' => Yii::t('user', 'Date of birth'),
                'rules' => array(
                    'search,search-user,insert,update,another-forms' => array(
                        array('safe'),
                    ),
                    'export,import,another-forms' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                )
            ),
            'logoId' => array(
                'label' => Yii::t('user', 'Logo'),
                'rules' => array(
                    'another-forms' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'ObjectFile',
                    array(
                        'logoId' => 'id',
                    ),
                ),
            ),
            'cityId' => array(
                'integer',
                'label' => Yii::t('user', 'City'),
                'rules' => array(
                    'search,insert,update,search-user' => array(
                        array('safe'),
                        array('numerical', 'integerOnly' => true),
                    ),
                    'export,import,view' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'City',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
            'city_name' => array(
                'label' => Yii::t('user', 'City'),
                'rules' => array(
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'active' => array(
                'integer',
                'label' => Yii::t('user', 'Active'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                        array('numerical', 'integerOnly' => true),
                        array('default', 'value' => User::ACTIVE_ON, 'setOnEmpty' => true)
                    ),
                    'insert,update' => array(
                        array('safe'),
                    ),
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'lastEntry' => array(
                'datetime',
                'label' => Yii::t('user', 'Last visit'),
                'search' => array(
                    array('safe'),
                ),
            ),
            'calculatorData' => array(
                'text',
                'label' => Yii::t('user', 'Data calculator'),
            ),
            'rememberMe' => array(
                'label' => Yii::t('user', 'Remember me'),
                'rules' => array(
                    'login' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'contact_companyName' => array(
                'label' => Contact::model()->getAttributeLabel('companyName'),
                'rules' => array(
                    'search,search-user' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'contact_email' => array(
                'label' => Contact::model()->getAttributeLabel('email'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'contact_phone' => array(
                'label' => Contact::model()->getAttributeLabel('phone'),
                'rules' => array(
                    'search' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'balance' => array(
                'float',
                'label' => Yii::t('user', 'Balance'),
                'rules' => array(
                    'search,insert,update,search-user' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'description' => array(
                'string',
                'label' => Yii::t('user', 'Description'),
                'rules' => array(
                    'search,update,another-forms' => array(
                        array('safe'),
                    ),
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'exponent_industryId' => array(
                'label' => Yii::t('user', 'Branch'),
                'rules' => array(
                    'search,update,insert' => array(
                        array('numerical', 'integerOnly' => true),
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'legalEntityId' => array(
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'LegalEntity',
                    array(
                        'legalEntityId' => 'id',
                    ),
                ),
                'label' => Yii::t('user', 'Jur. face'),
                'rules' => array(
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'bankAccountId' => array(
                'integer',
                'rules' => array(
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
                'label' => Yii::t('user', 'Bank account'),
            ),
            'contactId' => array(
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Contact',
                    array(
                        'contactId' => 'id',
                    ),
                ),
                'label' => Yii::t('user', 'Contact details'),
                'rules' => array(
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'legalEntity' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'LegalEntity',
                    array(
                        'legalEntityId' => 'id',
                    ),
                ),
                'rules' => array(
                    'export,import' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'contact' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'Contact',
                    array(
                        'contactId' => 'id',
                    ),
                ),
                'rules' => array(
                    'export,import' => array(
                        array('safe'),
                    )
                ),
            ),
            'bankAccount' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'BankAccount',
                    array(
                        'bankAccountId' => 'id',
                    ),
                ),
            ),
            'services' => array(
                'rules' => array(
                    'search,update' => array(
//						array('required'),
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
                'label' => Yii::t('user', 'Services'),
            ),
            'created' => array(
                'datetime',
                'label' => Yii::t('user', 'Created'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'modified' => array(
                'datetime',
                'label' => Yii::t('user', 'Updated'),
                'rules' => array(
                    'search' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'exhibitionComplex' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'ExhibitionComplex',
                    array(
                        'userId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,update,insert' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'email' => array(
                'label' => Yii::t('user', 'email'),
                'rules' => array(
                    'login,registration' => array(
                        array('required'),
                    ),
                    'registration-main' => array(
                        array('required'),
                    ),
                    'registration-left-menu' => array(
                        array('required'),
                    ),
                    'registration, resetPassword, registration-main,registration-left-menu' => array(
                        array('email'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'termsOfUse' => array(
                'label' => Yii::t('system', 'Have read'),
                'rules' => array(
                    'registration' => array(
                        array('required',
                            'requiredValue' => 1,
                            'message' => ''
                        ),
                    ),
                    'registration-main' => array(
                        array('safe'),
                    ),
                    'registration-left-menu' => array(
                        array('required',
                            'requiredValue' => 1,
                            'message' => ''
                        ),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'post' => array(
                'string',
                'label' => 'Должность',
                'rules' => array(
                    'view' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'searchByTariffId' => array(
                'rules' => array(
                    'searchByTariff' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'tariffId' => array(
                'integer',
                'label' => 'Тариф',
                'relation' => array(
                    'CBelongsToRelation',
                    'Tariff',
                    array(
                        'tariffId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search, insert, update' => array(
                        array('safe'),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'tariff_name' => array(
                'label' => 'Тариф',
                'rules' => array(
                    'search, insert, update' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                )
            ),
            'repeatPassword' => array(
                'label' => Yii::t('user', 'repeatPassword'),
                'rules' => array(
                    'registration' => array(
                        array('required'),
                        array('compare', 'compareAttribute'=>'password', 'on' => 'registration', 'message' => Yii::t('user', 'Passwords do not match')),
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerExponent' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'OrganizerExponent',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'googleAPI' => array(
                'text',
                'label' => 'Google API',
            ),
            'myFairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'MyFair',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'passport' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Passport',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'notifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Notification',
                    array(
                        'senderId' => 'id',
                    ),
                ),
            ),
            'fairMatches' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'FairMatches',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'userHasNotifications' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'UserHasNotification',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'blogs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Blog',
                    array(
                        'authorId' => 'id',
                    ),
                ),
            ),
            'auctionHasQuestions' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'AuctionHasQuestion',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'recalls' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Recall',
                    array(
                        'authorId' => 'id',
                    ),
                ),
            ),
            'tZs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TZ',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'userLog' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'UserLog',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'auctions' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Auction',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'contracts' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Contract',
                    array(
                        'exponentId' => 'id',
                    ),
                ),
            ),
            'fair' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Fair',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'documentContacts' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'DocumentContacts',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'tariffReports' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TariffReport',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'requests' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Request',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'proposals' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Proposal',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'messages' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Message',
                    array(
                        'senderId' => 'id',
                    ),
                ),
            ),
            'bids' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Bid',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'exponent' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Exponent',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'calendar' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Calendar',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'socialAuth' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'SocialAuth',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'userSetting' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'UserSetting',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'myFairViewDatas' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'MyFairViewData',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'userHasCities' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'UserHasCity',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'selectedExhibitionComplex' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'SelectedExhibitionComplex',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'portfolio' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Portfolio',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'comments' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Comment',
                    array(
                        'authorId' => 'id',
                    ),
                ),
            ),
            'reviews' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Review',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'userHasFairs' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'UserHasFair',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'auctionTechnicalDataHasUser' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'AuctionTechnicalDataHasUser',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'documentExhibits' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'DocumentExhibits',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'userHasExhibitionComplices' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'UserHasExhibitionComplex',
                    array(
                        'userId' => 'id',
                    ),
                ),
            ),
            'lastAuthFailDatetime' => array(
                'timestamp',
                'label' => Yii::t('user', 'lastAuthFailDatetime'),
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'authFailCount' => array(
                'datetime',
                'label' => Yii::t('user', 'authFailCount'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
            'authFailTotalCount' => array(
                'datetime',
                'label' => Yii::t('user', 'authFailTotalCount'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                    'searchOnAdmin' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     * Автологин
     */
    public function login()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        if($this->validate()) {
            $identity = new UserIdentity($this->email, $this->password);
            if($identity->authenticate() === true && $app->user->login($identity, 3600 * 24 * 7)) {
                $matches = $app->session->get('matches', []);
                if(!empty($matches)) {
                    $criteria = new CDbCriteria();
                    $criteria->compare('userId', $app->user->id);
                    FairMatches::model()->deleteAll($criteria);
                    foreach ($matches as $fairId => $checked) {
                        $match = new FairMatches();
                        $match->fairId = $fairId;
                        $match->userId = $app->user->id;
                        $match->save(false);
                    }
                    $app->session->add('matches', []);
                    return ['matches' => true];
                }

                return true;
            } else {

                if($identity->errorCode == 300){
                    $this->addError('password', Yii::t('user', 'User account blocked for 5 minutes.'));
                }else {
                    $this->addError('email', '');
                    $this->addError('password', Yii::t('user', 'The username or password you entered is incorrect.'));
                }
            }
        }
        return false;
    }

    /**
     * @param $role
     * @param $user
     * @return bool
     * Сохранение ролей
     */
    static function getSaveByRoles($role, $user)
    {
        if($user == 'exponent' || $user == 'developer') {
            if($role == User::ROLE_EXPONENT || $role == User::ROLE_DEVELOPER) {
                return true;
            }
        } elseif($user == 'freelancer') {
            if($role == User::ROLE_FREELANCER) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     * Сохранение роли при регистрации
     */
    static function getRegistrationRoles()
    {
        $organizers = isset($_GET['type']) && $_GET['type'] == 'organizer';
        $exponent = isset($_GET['type']) && $_GET['type'] == '3';
        $developer = isset($_GET['type']) && $_GET['type'] == '4';

        $roles = self::roles();

        unset($roles[self::ROLE_ADMIN]);
        unset($roles[self::ROLE_OPERATOR]);

        if(self::checkAdministrationRoles()) {
            return self::roles();
        } else if($exponent || $developer) {
            unset($roles[self::ROLE_ORGANIZERS]);
            unset($roles[self::ROLE_EXPERT]);
        }

        return $roles;
    }

    /**
     * @return bool
     * Доступ для Экспонента
     */
    public function checkAccessForExponent()
    {
        /** @var $app CWebApplication */
        $app = Yii::app();
        if(!empty($this->bids)) {
            $exponentIds = array();
            foreach ($this->bids as $bid) {
                if($bid->auctionId && $bid->auction && $bid->status == Bid::STATUS_WINNER) {
                    $exponentIds[] = $bid->auction->userId;
                }
            }
            if(in_array($app->user->id, $exponentIds)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     * Доступ для Администрации
     */
    static function checkAdministrationRoles()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        if($app->user->getState('role') == self::ROLE_ADMIN || $app->user->getState('role') == self::ROLE_OPERATOR) {
            return true;
        }
        return false;
    }

    /**
     * @param $model AR
     * @return array
     */
    public function getDbFieldNames($model)
    {
        $dbFields = array();
        foreach ($model->description() as $field => $v) {
            if(isset($v[0])) {
                $dbFields[] = $field;
            }
        }
        return $dbFields;
    }

    public function checkFieldsFill($model)
    {
        if(null !== $model) {
            foreach ($this->getDbFieldNames($model) as $field) {

                if($model->{$field} === null) {
                    return false;
                }
            }
        }
        return true;
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
                'createAttribute' => 'created',
                'updateAttribute' => 'modified',
            ),
        );
    }

    /**
     * @param $role
     * @return bool
     * Доступ к настройкам уведомлений
     */
    static function checkRolesForUserSetting($role)
    {
        if($role == User::ROLE_EXPERT || $role == User::ROLE_ORGANIZERS
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return bool
     * Сохранение коректной даты
     */
    public function beforeSave()
    {
        if(parent::beforeSave()) {
            $this->dob = date('Y-m-d', strtotime($this->dob));
            return true;
        }
        return false;
    }

    public function searchByTariff($criteria = null, $ignoreCompareFields = array())
    {
        $dataProvider = parent::search($criteria, $ignoreCompareFields);

        $dataProvider->criteria->addCondition('tariffId IS NOT NULL');

        return $dataProvider;
    }

    static function getUserBalance()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        $user = self::model()->findByPk($app->user->id);
        if($user) {
            return $user->balance ?: 0;
        }
        return null;
    }

    /**
     * Получение почти полного имени пользователя
     * @param $id
     * @return string
     */
    public static function getFullUserName($id)
    {
        $model = User::model()->findByPk($id);
        return (!empty($model->firstName) ? $model->firstName : '') . (!empty($model->lastName) ? $model->lastName : '');
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->lastName.' '.$this->firstName.' '.$this->middleName;
    }

    /**
     * @return CActiveDataProvider
     */
    public function getNearestStand()
    {
        /** @var CActiveDataProvider $dataProvider */
        $dataProvider = new CActiveDataProvider('Stand');
        if($this->portfolio) {
            $dataProvider->criteria->compare('portfolioId', $this->portfolio->id);
        } else {
            $dataProvider->criteria->compare('portfolioId', [0]);
        }
        $dataProvider->pagination = array(
            'pageSize' => 3
        );
        return $dataProvider;
    }

    public function register()
    {
        /**
         * @var CWebApplication $app
         */
        $app = Yii::app();
        if($this->validate()){
            $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        }
        $this->role = User::ROLE_USER;
        $this->active = User::ACTIVE_OFF;
        $this->banned = User::BANNED_OFF;

        $contact = new Contact($this->getScenario());
        $contact->email = $this->email;

        $transaction = $this->getDbConnection()->beginTransaction();

        $contact->save();
        $this->contactId = $contact->id;
        $this->save(false);

        $result = array();
        if(!empty($this->getErrors()) || !empty($contact->getErrors())) {
            $transaction->rollback();
            foreach ($this->getErrors() + $contact->getErrors() as $attribute => $errors) {
                $result[CHtml::activeId($this, $attribute)] = $errors;
            }
        } else {
            $transaction->commit();
            $identity = new UserIdentity($this->email, $this->password);
            $app->consoleRunner->run(
                'notification sendActivatedMessage --id=' . $this->id . ' --email=' . $this->email. ' --lang=' . Yii::app()->language,
                true
            );
            if($identity->authenticateByRecord($this)) {
                $app->user->login($identity);
            }
        }
        return $result;
    }

    /**
     * Проверка пароля на допустимые символы
     * @return bool
     */
    public function checkPassword()
    {
        preg_match('#^[a-zA-Z0-9]+$#', $this->password, $match);
        if(!isset($match[0])) {
            $this->addError('password', Yii::t('user', 'Password contains invalid characters'));
        }
    }

    /**
     * @return bool|mixed|Tariff
     */
    public static function getTariff()
    {
        $result = false;
        if (!Yii::app()->user->isGuest) {
            $result = Yii::app()->user->getTariff();
        }

        return $result;
    }

    /**
     * Проверка на соответствие тарифа
     * @param int $tariff
     * @return bool
     */
    public static function checkTariff($tariff = 1)
    {
        $userTariff = self::getTariff();
        return ($userTariff && $userTariff >= $tariff);
    }

    public function adminSearch($criteria = null, $ignoreCompareFields = array())
    {
        $dCriteria = new CDbCriteria;
        $dCriteria->with = array(
            'city' => array(
                'together' => TRUE,
                'with' => array(
                    'translate' => [
                        'together' => TRUE,
                    ],
                )
            ),
            'tariff'
        );
        $dCriteria->compare('city.name', $this->cityId, true);
        $dCriteria->compare('tariff.name', $this->tariffId);

        if (null !== $criteria) {
            $dCriteria->mergeWith($criteria);
        }

        return parent::search($dCriteria, ['t.cityId', 't.tariffId']);
    }

    /**
     * @param array $fields
     * @param int $pageSize
     * @return CActiveDataProvider
     */
    public function searchOnAdmin($fields = [], $pageSize = 10) {

        $fields = array_keys($fields);

        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $sort = ['*'];

        foreach($this->description() as $key => $description) {

            if (!isset($description['relation']) && !in_array('pk', $description) && in_array($key, $fields)) {
                if ($key == 'contact_email') {
                    $criteria->with['contact'] = [
                        'together' => true
                    ];
                    $criteria->compare('contact.email', $this->contact_email, true);
                    $sort['contact_email'] = [
                        'asc' => '`contact`.`email`',
                        'desc' => '`contact`.`email` DESC'
                    ];
                } elseif ($key == 'city_name') {
                    $criteria->with['city'] = [
                        'together' => true,
                        'with' => [
                            'translate' => [
                                'together' => TRUE,
                            ],
                        ],
                    ];
                    $criteria->compare('translate.name', $this->city_name, true);
                    $sort['city_name'] = [
                        'asc' => '`translate`.`name`',
                        'desc' => '`translate`.`name` DESC'
                    ];
                } elseif ($key == 'tariff_name') {
                    $criteria->with['tariff'] = [
                        'together' => true
                    ];
                    $criteria->compare('tariff.name', $this->tariff_name, true);
                    $sort['tariff_name'] = [
                        'asc' => '`tariff`.`name`',
                        'desc' => '`tariff`.`name` DESC'
                    ];
                } else {
                    $criteria->compare('t.' . $key, $this->$key, in_array('string', $description));
                }
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => $sort,
            ),
            'pagination' => [
                'pageSize' => $pageSize
            ]
        ));
    }

    /**
     * @param $userId
     * @param array $organizers
     * @return bool
     * @throws CHttpException
     */
    public static function saveOrganizer ($userId, $organizers = []) {
        UserHasOrganizer::model()->deleteAllByAttributes(['userId' => $userId]);

        $saved = false;

        foreach ($organizers as $key => $value) {
            $organizer = new UserHasOrganizer();

            $organizer->organizerId = $value;
            $organizer->userId = $userId;

            if ($organizer->save()) {
                $saved = true;
            } else {
                $saved = false;
                throw new CHttpException(500, '500, Organizer with id = ' . $organizer->organizerId . ' not saved');
            }
        }

        return $saved;
    }
}