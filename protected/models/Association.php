<?php

/**
 * Class Association
 *
 * @property    integer $id
 */
class Association extends \AR
{
    public static $_description = NULL;
    
    const RUEF_ASSOCIATION_ID = 1;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('fair', 'Fair association'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
        );
    }
}