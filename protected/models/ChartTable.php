<?php

/**
 * Class ChartTable
 *
 * @property integer $id
 * @property string $name
 * @property string $area
 * @property integer $type
 *
 * @property array $purifyAttributes
 */
class ChartTable extends \AR{

    public static $_description = NULL;

    const TYPE_STATE = 'state';
    const TYPE_REGION = 'region';

    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const TYPE_3 = 3;

    protected $_name = NULL;

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        if($this->_name !== NULL){
            $name = $this->_name;
        }else{
            $translate = $this->translate;
            if(isset($translate->name)) {
                $this->_name = $translate->name;
                $name = $this->_name;
            }
        }

        return $name;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'integer',
                'label' => 'Тип виджета',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'area' => array(
                'string',
                'label' => 'Федеральный округ/Регион',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'industryId' => array(
                'integer',
                'label' => 'Отрасль',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'Industry',
                    array(
                        'industryId' => 'id',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrChartTable',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );
    }

    public static function getAreas(){
        return array(
             self::TYPE_STATE => 'Федеральный округ',
             self::TYPE_REGION => 'Регион',
        );
    }

    public static function getTableTypes(){
        return array(
            self::TYPE_1 => 'Таблица с двойным виджетов (процентный по двум показателям)',
            self::TYPE_2 => 'Таблица с маленьким виджетом и значениями в одной колонке',
            self::TYPE_3 => 'Таблица с отдельной колонкой под виджет и под значение',
        );
    }
}