<?php
/**
 * Class ScdTrFair
 */
class ScdTrFair extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'scdId' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'id' => array(
				'label' => 'trFairid',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'label' => 'id Выставки',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'Fair',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'label' => 'Язык',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'shortUrl' => array(
				'label' => 'shortUrl',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'preserveUrl' => array(
				'label' => 'preserveUrl',
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'label' => 'Наименование',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'description' => array(
				'string',
				'label' => 'Описание',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'manipulateDatetime' => array(
				'string',
				'label' => 'Дата изменения',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'manipulateUser' => array(
				'string',
				'label' => 'Изменивший пользователь',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}

	/**
	 * @param $fairId
	 * @param array $fields
	 * @return CSqlDataProvider
	 */
	public function searchData($fairId, $fields = array())
	{
		$filter = '';
		if(!empty($fields)){

			foreach ($fields as $fieldKey => $fieldValue){

				if(!empty($fieldValue)){
					$filter .= "AND `{$fieldKey}` LIKE '%{$fieldValue}%'";
				}
			}
		}

		$sql = "SELECT * FROM tbl_scdtrfair WHERE `trParentId` = {$fairId}
		{$filter}";

		$sort = new CSort;
		$sort->defaultOrder = 'manipulateDatetime DESC';
		$sort->attributes = array(
			'scdId' => array(
				'asc' => 'scdId',
				'desc' => 'scdId DESC',
			),
			'id' => array(
				'asc' => 'id',
				'desc' => 'id DESC',
			),
			'trParentId' => array(
				'asc' => 'trParentId',
				'desc' => 'trParentId DESC',
			),
			'langId' => array(
				'asc' => 'langId',
				'desc' => 'langId DESC',
			),
			'name' => array(
				'asc' => 'name',
				'desc' => 'name DESC',
			),
			'description' => array(
				'asc' => 'description',
				'desc' => 'description DESC',
			),
			'shortUrl' => array(
				'asc' => 'shortUrl',
				'desc' => 'shortUrl DESC',
			),
			'preserveUrl' => array(
				'asc' => 'preserveUrl',
				'desc' => 'preserveUrl DESC',
			),
			'manipulateDatetime' => array(
				'asc' => 'manipulateDatetime',
				'desc' => 'manipulateDatetime DESC',
			),
			'manipulateUser' => array(
				'asc' => 'manipulateUser',
				'desc' => 'manipulateUser DESC',
			),
		);

		return new CSqlDataProvider($sql, array(
			'keyField' => 'scdId',
			'sort' => $sort,
		));
	}
}