<?php
/**
 * Class TrIndustry
 */
class TrIndustry extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'integer',
				'label' => 'Промышленность',
				'relation' => array(
					'CBelongsToRelation',
					'Industry',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'string',
				'label' => 'Язык',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'label' => 'Наименование',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'shortNameUrl' => array(
				'string',
				'label' => 'Короткая ссылка',
			),
		);
	}
}