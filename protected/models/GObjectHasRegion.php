<?php

/**
 * Class GObjectHasRegion
 *
 * @property integer objId
 * @property integer regionId
 */
class GObjectHasRegion extends \AR{

    public static $_description = NULL;

    public function getName()
    {
        return $this->name;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'regionId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'label' => 'Внутренний ID региона',
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'objId' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'label' => 'id города в градотеке',
                'relation' => array(
                    'CBelongsToRelation',
                    'GObjects',
                    array(
                        'objId' => 'id',
                    ),
                ),
            ),
        );
    }

}