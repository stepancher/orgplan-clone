<?php
/**
 * Class TrRegion
 */
class TrRegion extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'integer',
				'label' => 'Регион',
				'relation' => array(
					'CBelongsToRelation',
					'Region',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'string',
				'label' => 'Язык',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'label' => 'Наименование',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'shortUrl' => array(
				'string',
				'label' => 'Короткая ссылка',
			),
		);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
}