<?php

/**
 * Class FairHasOrganizer
 * 
 * @property    integer     $id
 * @property    integer     $fairId
 * @property    integer     $organizerId
 */
class FairHasOrganizer extends \AR
{
    public static $_description = NULL;
    
    public function description()
    {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fairId' => array(
                'label' => 'Выставка',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Fair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerId' => array(
                'label' => Yii::t('fair', 'Organizer'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Organizer',
                    array(
                        'organizerId' => 'id',
                    ),
                ),
            ),
        );
    }
}