<?php

/**
 * Class ChartColumn
 * @property integer id
 * @property integer statId
 * @property string header
 * @property string count
 * @property integer parent
 * @property integer epoch
 * @property integer widgetType
 * @property integer $tableId
 * @property integer $position
 */
class ChartColumn extends \AR{

    public static $_description = NULL;

    public $data;

    protected $_header = NULL;
    protected $_unit = NULL;

    /**
     * @return string
     */
    public function getHeader()
    {
        $header = '';
        if($this->_header !== NULL){
            $header = $this->_header;
        }else{
            $translate = $this->translate;
            if(isset($translate->header)) {
                $this->_header = $translate->header;
                $header = $this->_header;
            }
        }

        return $header;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        $unit = '';
        if($this->_unit !== NULL){
            $unit = $this->_unit;
        }else{
            $translate = $this->translate;
            if(isset($translate->unit)) {
                $this->_unit = $translate->unit;
                $unit = $this->_unit;
            }
        }

        return $unit;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'statId' => array(
                'integer',
                'label' => 'stat id',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'stat' => array(
                'relation' => array(
                    'CBelongsToRelation',
                    'GStatTypes',
                    array(
                        'statId' => 'gId',
                    ),
                ),
            ),
            'count' => array(
                'string',
                'label' => 'count',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'parent' => array(
                'integer',
                'label' => 'parent',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'widgetType' => array(
                'integer',
                'label' => 'widgetType',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'epoch' => array(
                'integer',
                'label' => 'epoch',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'tableId' => array(
                'integer',
                'label' => 'ID таблицы',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CBelongsToRelation',
                    'ChartTable',
                    array(
                        'tableId' => 'id',
                    ),
                ),
            ),
            'color' => array(
                'string',
                'label' => 'color',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'position' => array(
                'integer',
                'label' => 'Позиция',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'rel' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'label' => 'related',
                'relation' => array(
                    'CBelongsToRelation',
                    'ChartColumn',
                    array(
                        'related' => 'id',
                    ),
                ),
            ),
            'related' => array(
                'integer',
                'label' => 'Связанная запись',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'accessory' => array(
                'string',
                'label' => 'accessory',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrChartColumn',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );

    }

    /**
     * @param $tableId
     * @return array|mixed|null
     */
    public static function getColumnsById($tableId){
        return ChartColumn::model()->findAllByAttributes(array('tableId' => $tableId));
    }

    /**
     * @param $tableId
     * @return array|mixed|null
     */
    public static function getByTableId($tableId){

        $criteria = new CDbCriteria();
        $criteria->addCondition('tableId = :table_id');
        $criteria->params[':table_id'] = $tableId;
        $criteria->order = 'position';

        return self::model()->findAll($criteria);
    }
}