<?php
/**
 * Class TrExhibitionComplex
 * @property string $shortUrl
 */
class TrExhibitionComplex extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'label' => 'Выставочный комплес',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'ExhibitionComplex',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'label' => 'Язык',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'label' => 'Наименование',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'street' => array(
				'string',
				'label' => 'street',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'description' => array(
				'text',
				'label' => 'Описание',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'services' => array(
				'label' => 'Услуги ВЦ',
				'string',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'uniqueText' => array(
				'label' => 'Описание ВЦ',
				'text',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'descriptionSnippet' => array(
				'label' => 'descriptionSnippet',
				'text',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
            'shortUrl' => array(
                'string',
                'label' => 'Короткая ссылка',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    ),
                ),
            ),
		);
	}

	/**
	 * Поля-исключения, в которых не нужно делать проверку purify
	 * @var array
	 */
	public $notPurifyAttributes = [
		'uniqueText'
	];
}