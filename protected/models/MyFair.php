<?php

/**
 * Class MyFair
 */
class MyFair extends \AR
{
    public static $_description = NULL;

    const STATUS_DELETE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_ARCHIVE = 2;

    public function getStatuses($key = null)
    {
        If(null != $key) {
            $statuses = self::getStatuses();
            if(isset($statuses[$key])) {
                return $statuses[$key];
            }
        }

        return [
            self::STATUS_DELETE => 'Удалена',
            self::STATUS_ACTIVE => 'Активная',
            self::STATUS_ARCHIVE => 'В архиве',
        ];
    }

    /**
     * Получение всех идентификаторов "Моих выставок"
     * @param $userId
     * @return array
     */
    public static function getFairsId($userId, $status = null)
    {
        $result = [
            '-1'
        ];

        if(null != $status) {
            $models = self::model()->findAll(
                'userId = :userId AND status = :status',
                [':userId' => $userId, ':status' => $status]
            );
        } else {
            $models = self::model()->findAll(
                'userId = :userId AND status <> :status',
                [':userId' => $userId, ':status' => self::STATUS_DELETE]
            );
        }

        if(null != $models) {
            $result = CHtml::listData($models, 'fairId', 'fairId');
        }
        return $result;
    }

    /**
     * Проверка.
     * Проверяем есть ли вы выставка с идентификатором $fairId в "Моих выставках"
     * @param $fairId
     * @param $check_status
     * @return bool
     */
    public static function isMyFair($fairId, $check_status = false)
    {
        $model = static::model()->findByAttributes(
            [
                //'status' => static::STATUS_ACTIVE,
                'fairId' => $fairId,
                'userId' => Yii::app()->user->id
            ]
        );

        if ($check_status != true) {
            return $model != null;
        }

        return $model != NULL && $model->status != 0;

    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'userId' => array(
                'label' => 'Пользователь',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'User',
                    array(
                        'userId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'fairId' => array(
                'integer',
                'label' => 'Выставка',
                'relation' => array(
                    'CBelongsToRelation',
                    'Fair',
                    array(
                        'fairId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'status' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'createdAt' => array(
                'datetime',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'updatedAt' => array(
                'timestamp',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'tasks' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'Task',
                    array(
                        'myFairId' => 'id',
                    ),
                ),
            ),
        );
    }

    public static function isExponentFair($fairId){
        $fair = Fair::model()->findByPk($fairId);

        if(empty($fair)){
            return FALSE;
        }

        $isExponentOforganizer = FALSE;

        if(
            !empty($fair->shard) &&
            !empty($shardConnection = Yii::app()->dbMan->getShardConnection(Yii::app()->dbMan->createShardName($fairId)))
        ){
            Yii::import('application.modules.organizer.models.OrganizerExponent');

            ShardedAR::$_db = $shardConnection;
            $isExponentOforganizer = !empty(OrganizerExponent::model()->findByAttributes(array('userId' => Yii::app()->user->id)));
        }

        if(
//            !empty($fair->shard) &&
//            is_string($fair->shard) &&
//            !empty(Yii::app()->{$fair->shard}) &&
            Yii::app()->user->role == User::ROLE_EXPONENT &&
            self::isMyFair($fairId) &&
            $isExponentOforganizer
        ){
            return TRUE;
        }

        return FALSE;
    }

    public static function isOrganizerOfFair($fairId){

        $fair = Fair::model()->findByPk($fairId);

        if(empty($fair)){
            return FALSE;
        }

        $fairHasOrganizers = FairHasOrganizer::model()->findAllByAttributes(['fairId' => $fairId]);
        $userHasOrganizers = array();

        foreach ($fairHasOrganizers as $fairHasOrganizer){
            $userHasOrganizer = UserHasOrganizer::model()->findByAttributes(array('userId' => Yii::app()->user->id, 'organizerId' => $fairHasOrganizer->organizerId));

            if(!empty($userHasOrganizer)){
                $userHasOrganizers[] = $userHasOrganizer;
            }
        }

        if(empty($userHasOrganizers)){
            return FALSE;
        }

        if(
            Yii::app()->user->role == User::ROLE_ORGANIZERS &&
            self::isMyFair($fairId)
        ){
            return TRUE;
        }

        return FALSE;

    }

    /**
     * @param $fairId
     * @param null $userId
     * @return bool
     * @TODO REFACTOR !!! hotfix.
     */
    public static function addFairForUser($fairId, $userId = NULL){

        $result = FALSE;

        if ($userId == NULL) {
            $userId = Yii::app()->user->id;
        }

        $model = Fair::model()->findByPk($fairId);
        if (null != $model) {
            $myFair = self::model()->findByAttributes(['fairId' => $fairId, 'userId' => $userId]);
            if (null == $myFair) {
                $myFair = new MyFair;
                $myFair->userId = $userId;
                $myFair->fairId = $model->id;
                $myFair->createdAt = date('Y-m-d H:i:s');
            }
            $myFair->status = MyFair::STATUS_ACTIVE;
            $myFair->updatedAt = date('Y-m-d H:i:s');

            if($myFair->save()){
                $result = TRUE;
            }
        }

        return $result;
    }
}