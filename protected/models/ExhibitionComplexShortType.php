<?php
/**
 * Class ExhibitionComplexShortType
 */
class ExhibitionComplexShortType extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'label' => 'Наименование',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'exhibitionComplex' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
				'relation' => array(
					'CHasOneRelation',
					'ExhibitionComplex',
					array(
						'exhibitionComplexShortTypeId' => 'id',
					),
				),
			),
		);
	}
}