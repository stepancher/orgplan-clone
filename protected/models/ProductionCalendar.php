<?php
/**
 * Class ProductionCalendar
 */
class ProductionCalendar extends \AR
{
	public static $_description = NULL;

	public $rangeDate;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'year' => array(
				'label' => 'Год',
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'holiday' => array(
				'label' => 'Год',
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'holidayId' => array(
				'label' => 'Идентификатор выходного',
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'rangeDate' => array(
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
				'label' => 'Период'
			),
			'productionCalendarDays' => array(
				'relation' => array(
					'CHasManyRelation',
					'ProductionCalendarDay',
					array(
						'productionCalendarId' => 'id',
					),
				),
			),
		);
	}

	public function getProductionCalendarRangeDays(){
		$result = '';
		if(!empty($this->productionCalendarDays)) {
			$beginDate = strtotime(date('Y-m-d'));
			$endDate = 0;
			foreach($this->productionCalendarDays as $productionCalendarDay){
				if($beginDate > strtotime($productionCalendarDay->day)) {
					$beginDate = $productionCalendarDay->day;
				}
				if($endDate < strtotime($productionCalendarDay->day)) {
					$endDate = $productionCalendarDay->day;
				}
			}
			if($beginDate == 0) {
				$beginDate = '';
			}
			if($endDate == 0) {
				$endDate = '';
			}
			$result = date('d.m.Y', strtotime($beginDate)).(!empty($beginDate) && !empty($endDate) ? ' - ' : '').date('d.m.Y', strtotime($endDate));
		}
		return $result;
	}

	public static function getAllHolidays()
	{
		$result = [];
		$models = self::model()->findAll();
		if(null != $models) {
			foreach ($models as $model) {
				$result[$model->id] = $model->holiday.' - '.$model->year;
			}
		}
		return $result;
	}
}