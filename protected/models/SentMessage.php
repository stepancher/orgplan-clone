<?php

/**
 * Class SentMessage
 */
class SentMessage extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'viewLayout' => array(
                'string',
                'label' => 'viewLayout',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'from' => array(
                'string',
                'label' => 'from',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'to' => array(
                'string',
                'label' => 'to',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'subject' => array(
                'string',
                'label' => 'subject',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'content' => array(
                'string',
                'label' => 'content',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'logo' => array(
                'string',
                'label' => 'logo',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'viewPath' => array(
                'string',
                'label' => 'viewPath',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'view' => array(
                'string',
                'label' => 'view',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'mailViewPath' => array(
                'string',
                'label' => 'mailViewPath',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'attachmentData' => array(
                'string',
                'label' => 'attachmentData',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'attachmentFileName' => array(
                'string',
                'label' => 'attachmentFileName',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'attachmentContentType' => array(
                'string',
                'label' => 'attachmentContentType',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'raw' => array(
                'string',
                'label' => 'raw',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'messageId' => array(
                'integer',
                'label' => 'messageId',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'datetimeSend' => array(
                'timestamp',
                'label' => 'datetimeSend',
                'rules' => array(
                    'search,insert,update,load' => array(
                        array('safe'),
                    )
                ),
            ),
        );
    }

    public function beforeSave()
    {
        $this->datetimeSend = new CDbExpression('CURRENT_TIMESTAMP');
        return parent::beforeSave();
    }
}