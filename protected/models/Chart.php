<?php

/**
 * Class Chart
 *
 * @property integer id
 * @property string name
 * @property integer typeId
 * @property integer objId
 * @property integer epoch
 * @property integer setId
 * @property integer widget
 * @property integer widgetType
 * @property string header
 * @property string measure
 * @property string color
 * @property integer groupId
 * @property integer related
 * @property Chart rel
 * @property integer parent
 *
 * @property array $purifyAttributes
 */
class Chart extends \AR{

    public static $_description = NULL;

    public $notPurifyAttributes = [
        'measure'
    ];

    protected $_header = NULL;
    protected $_measure = NULL;
    protected $_unit = NULL;

    /**
     * @return string
     */
    public function getHeader()
    {
        $header = '';
        if($this->_header !== NULL){
            $header = $this->_header;
        }else{
            $translate = $this->translate;
            if(isset($translate->header)) {
                $this->_header = $translate->header;
                $header = $this->_header;
            }
        }

        return $header;
    }

    /**
     * @return string
     */
    public function getMeasure()
    {
        $measure = '';
        if($this->_measure !== NULL){
            $measure = $this->_measure;
        }else{
            $translate = $this->translate;
            if(isset($translate->measure)) {
                $this->_measure = $translate->measure;
                $measure = $this->_measure;
            }
        }

        return $measure;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        $unit = '';
        if($this->_unit !== NULL){
            $unit = $this->_unit;
        }else{
            $translate = $this->translate;
            if(isset($translate->unit)) {
                $this->_unit = $translate->unit;
                $unit = $this->_unit;
            }
        }

        return $unit;
    }

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('fair', 'Name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'typeId' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'objId' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'epoch' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'setId' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'widget' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'widgetType' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'color' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'groupId' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'group' => array(
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
                'label' => 'group',
                'relation' => array(
                    'CBelongsToRelation',
                    'ChartGroup',
                    array(
                        'groupId' => 'id',
                    ),
                ),
            ),
            'related' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'rel' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'label' => 'related',
                'relation' => array(
                    'CBelongsToRelation',
                    'Chart',
                    array(
                        'related' => 'id',
                    ),
                ),
            ),
            'parent' => array(
                'integer',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrChart',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
        );

    }

    /**
     * @param $groupId
     * @return array Array of Chart objects
     */
    public static function getByGroup($groupId){

        $criteria = new CDbCriteria();
        $criteria->addCondition('groupId = :groupId');
        $criteria->params = array(':groupId' => $groupId);

        return self::model()->findAll($criteria);
    }

    /**
     * @param $parentId
     * @return array Array of Chart objects
     */
    public static function getByParent($parentId){
        $criteria = new CDbCriteria();
        $criteria->addCondition('parent = :parent');
        $criteria->params = array(':parent' => $parentId);

        return self::model()->findAll($criteria);
    }
}