<?php
/**
 * Class TrDistrict
 */
class TrDistrict extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'trParentId' => array(
				'integer',
				'label' => 'Округ',
				'relation' => array(
					'CBelongsToRelation',
					'District',
					array(
						'trParentId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'langId' => array(
				'string',
				'label' => 'Язык',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'name' => array(
				'string',
				'label' => 'Нименование',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}