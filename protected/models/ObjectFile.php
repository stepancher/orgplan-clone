<?php
/**
 * class ObjectFile
 * @var ObjectFile $model
 */
Yii::import('application.modules.profile.models.LegalEntity');
class ObjectFile extends \AR
{
    public static $_description = NULL;

    const EXT_IMAGE = 1; // Изображения
    const EXT_DOC = 2; //   Документы
    const EXT_FILE = 3; //  Прочие файлы
    const EXT_SCAN = 4; //  Сканы

    const TYPE_MARKETING_MATERIALS = 1; // Маркетинговые материалы
    const TYPE_PRINTED_MATERIALS = 2; // Печатные материалы
    const TYPE_LIST_OF_TECHNICAL_SERVICES = 3; // Файлы оказываемых технических услуг
    const TYPE_FAIR_LOGOS = 4; // Логотипы выставок
    const TYPE_EXHIBITION_REQUIREMENTS = 6; // требования выставки
    const TYPE_PRESS_RELEASE = 7; // Файл с Пресс-релизом
    const TYPE_DISTRIBUTION_EXHIBITION = 8; // Файл с тех требованиями выставки
    const TYPE_PHOTOS_FROM_THE_SHOW = 9; // Фото с выставки
    const TYPE_USER_LOGO = 11; // Логотипы пользователя
    const TYPE_REQUEST_FILES = 13; // Файлы заявки
    const TYPE_STAND_DESIGN_STYLE = 14; // Стили дизайна стенда
    const TYPE_STAND_DESIGN_COLOURS = 15; //  Цветовые гаммы дизайна стенда
    const TYPE_STAND_DESIGN_POSTER = 16; //  Постеры дизайна стенда
    const TYPE_STAND_DESIGN_FLORISTICS = 18; //  лористика стенда
    const TYPE_STAND_IMAGE = 20; // Изображенние стенда
    const TYPE_STAND_IMAGE_LAYOUT = 21; // Изображенние стенда - Макета
    const TYPE_BLOG_MAIN_IMAGE = 29; // Заглавное изоброжение  - блога
    const TYPE_BLOG_IMAGES = 30; // все изоброжения блога
    const TYPE_BLOG_FILES = 31; // все файлы новостей блога
    const TYPE_REGION_INFORMATION_MAIN_IMAGE = 32; // Заглавное изображение карточки
    const TYPE_REGION_INFORMATION_IMAGES = 33; // Изображения информации о егионе
    const TYPE_REGION_INFORMATION_FILES = 34; // Файлы информации о региона
    const TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE = 39; // Заглавное изображение выставки
    const TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES = 40; // Изображения галереи
    const TYPE_TASK_FILES = 41; // Файлы задачи
    const TYPE_ANALYTICS_IMAGE = 43; // Изображение аналитики
    const TYPE_QUESTIONNAIRE_LOGO = 46; // Логотип анкеты
    const TYPE_LEGAL_ENTITY_LOGO = 47; // Логотип компании

    const TYPE_FIELD_FILES = 48; // Файлы полей
    const TYPE_ADDITIONAL_EXPENSES_LIST_IMAGE = 49; // Файлы полей
    const TYPE_ADDITIONAL_EXPENSES_CHART_IMAGE = 50; // Файлы полей
    const TYPE_REGION_INFORMATION_LOGO = 51; // Заглавное изображение карточки
    const TYPE_MASS_MEDIA_IMAGE = 54; // Заглавное изоброжение  - блога
    const TYPE_MASS_MEDIA_IMAGES = 55; // все изоброжения блога
    const TYPE_MASS_MEDIA_FILES = 56; // все файлы новостей блога
    const TYPE_ORGANIZER_INFO_LOGOS = 58; // OrganizerInfo logotypes
    const TYPE_ORGANIZER_CONTACT_AVATARS = 59; // OrganizerContact avatars

    const TYPE_FAIR_MAIL_LOGOS = 60; // Логотипы выставок в письме


    static $types = array(
        self::TYPE_MARKETING_MATERIALS => 'marketingMaterials',
        self::TYPE_PRINTED_MATERIALS => 'printedMaterials',
        self::TYPE_LIST_OF_TECHNICAL_SERVICES => 'listOfTechnicalServices',
        self::TYPE_FAIR_LOGOS => 'fairLogos',
        self::TYPE_FAIR_MAIL_LOGOS => 'fairMailLogos',
        self::TYPE_EXHIBITION_REQUIREMENTS => 'exhibitionRequirements',
        self::TYPE_PRESS_RELEASE => 'pressRelease',
        self::TYPE_DISTRIBUTION_EXHIBITION => 'distributionExhibition',
        self::TYPE_PHOTOS_FROM_THE_SHOW => 'photosFromTheShow',
        self::TYPE_REQUEST_FILES => 'requestFiles',
        self::TYPE_STAND_IMAGE => 'standImage',
        self::TYPE_STAND_IMAGE_LAYOUT => 'standImageLayouts',
        self::TYPE_BLOG_MAIN_IMAGE => 'blogMainImage',
        self::TYPE_BLOG_IMAGES => 'blogImages',
        self::TYPE_BLOG_FILES => 'blogFile',
        self::TYPE_REGION_INFORMATION_MAIN_IMAGE => 'regionInformationMainImage',
        self::TYPE_REGION_INFORMATION_IMAGES => 'regionInformationImages',
        self::TYPE_REGION_INFORMATION_FILES => 'regionInformationFiles',
        self::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE => 'exhibitionComplexMainImage',
        self::TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES => 'exhibitionComplexGalleryImages',
        self::TYPE_TASK_FILES => 'taskFiles',
        self::TYPE_ANALYTICS_IMAGE => 'analyticsImage',
        self::TYPE_QUESTIONNAIRE_LOGO => 'questionnaireLogo',
        self::TYPE_LEGAL_ENTITY_LOGO => 'legalEntityLogo',
        self::TYPE_FIELD_FILES => 'FieldFiles',
        self::TYPE_USER_LOGO => 'userLogo',
        self::TYPE_ADDITIONAL_EXPENSES_LIST_IMAGE => 'listImage',
        self::TYPE_ADDITIONAL_EXPENSES_CHART_IMAGE => 'chartImage',
        self::TYPE_REGION_INFORMATION_LOGO => 'regionInformationLogo',
        self::TYPE_MASS_MEDIA_IMAGE => 'massMediaMainImage',
        self::TYPE_MASS_MEDIA_IMAGES => 'massMediaImages',
        self::TYPE_MASS_MEDIA_FILES => 'massMediaFiles',
        self::TYPE_ORGANIZER_INFO_LOGOS => 'organizerInfoLogos',

    );

    static $paths = array(
        self::TYPE_USER_LOGO => 'user/{id}/logo',
        self::TYPE_MARKETING_MATERIALS => 'exhibitionComplex/{id}/marketingMaterials',
        self::TYPE_PRINTED_MATERIALS => 'exhibitionComplex/{id}/printedMaterials',
        self::TYPE_LIST_OF_TECHNICAL_SERVICES => 'exhibitionComplex/{id}/listOfTechnicalServices/',
        self::TYPE_FAIR_LOGOS => 'fair/{id}/logo',
        self::TYPE_FAIR_MAIL_LOGOS => 'fairMail/{id}/logo',
        self::TYPE_USER_LOGO => 'user/{id}/logo',
        self::TYPE_EXHIBITION_REQUIREMENTS => 'fair/{id}/exhibitionRequirements',
        self::TYPE_PRESS_RELEASE => 'fair/{id}/pressRelease',
        self::TYPE_DISTRIBUTION_EXHIBITION => 'fair/{id}/distributionExhibition',
        self::TYPE_PHOTOS_FROM_THE_SHOW => 'fair/{id}/photosFromTheShow',
        self::TYPE_STAND_IMAGE => 'stand/{id}/images',
        self::TYPE_STAND_IMAGE_LAYOUT => 'stand/{id}/layouts',
        self::TYPE_BLOG_MAIN_IMAGE => 'blog/{id}/mainImage',
        self::TYPE_BLOG_IMAGES => 'blog/{id}/images',
        self::TYPE_BLOG_FILES => 'blog/{id}/files',
        self::TYPE_REGION_INFORMATION_MAIN_IMAGE => 'regionInformation/{id}/mainImage',
        self::TYPE_REGION_INFORMATION_IMAGES => 'regionInformation/{id}/images',
        self::TYPE_REGION_INFORMATION_FILES => 'regionInformation/{id}/files',
        self::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE => 'exhibitionComplex/{id}/logo',
        self::TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES => 'exhibitionComplex/{id}/images',
        self::TYPE_TASK_FILES => 'task/{id}/files',
        self::TYPE_ANALYTICS_IMAGE => 'analytics/{id}/images',
        self::TYPE_QUESTIONNAIRE_LOGO => 'questionnaire/{id}/logo',
        self::TYPE_LEGAL_ENTITY_LOGO => 'legalEntity/{id}/logo',
        self::TYPE_FIELD_FILES => '',
        self::TYPE_ADDITIONAL_EXPENSES_LIST_IMAGE => 'additionalExpenses/{id}/list',
        self::TYPE_ADDITIONAL_EXPENSES_CHART_IMAGE => 'additionalExpenses/{id}/chart',
        self::TYPE_REGION_INFORMATION_LOGO => 'regionInformation/{id}/logo',
        self::TYPE_MASS_MEDIA_IMAGE  => 'massMedia/{id}/mainImage',
        self::TYPE_MASS_MEDIA_IMAGES => 'massMedia/{id}/images',
        self::TYPE_ORGANIZER_INFO_LOGOS => 'organizer/company/{id}',
        self::TYPE_ORGANIZER_CONTACT_AVATARS => 'organizer/contact/{id}',
//        self::TYPE_MASS_MEDIA_FILES => 'massMedia/{id}/files',
    );

    static $acceptedImageExt = array(
        'BMP', //(Windows or OS/2 bitmap)
        'JPEG', 'JPG', 'JPE', //(Joint Photographic Experts Group)
        'JPEG', //2000 (.jp2)
        'GIF', //2000 (.jp2)
        'PCX', //(ZSoft PaintBrush)
        'PDN', //(Paint.NET Image)
        'PNG', //(Portable Network Graphics)
        'TGA', //(Truevision Targa) (.TGA, .tpic)
        'TIFF', 'TIF', //(Tagged Image Format)
        'WDP', 'HDP', //(Windows Media Photo)
    );

    static $allowExt = [
            'ZIP', 'RAR', '7ZIP', 'DOC', 'DOCX', 'TXT', 'CSV', 'XLS', 'XLSX', 'XML', 'PDF'
        ];

    public static function getDefaultAllowedExt()
    {
        return CMap::mergeArray(static::$allowExt, static::$acceptedImageExt);
    }

    static $acceptedImageExtCUpload = array(
        'image/jpeg',
        'image/pjpeg',
        'image/gif',
        'image/png',
        'image/tiff',
        'image/vnd.wap.wbmp',
        'image/vnd.microsoft.icon',
        'image/svg+xml'
    );

    private $fileNamePrefix;
    private $_ownerId;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'name' => array(
                'string',
                'label' => Yii::t('objectFile', 'File name'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'type' => array(
                'integer',
                'label' => Yii::t('objectFile', 'Type of connection'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'productHasFile' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'invoice' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'contract' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'contractorHasFile' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'repairs' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'rentHasFile' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'label' => array(
                'string',
                'label' => Yii::t('objectFile', 'Name'),
            ),
            'ext' => array(
                'string',
                'label' => Yii::t('objectFile', 'File Type'),
            ),
            'modified' => array(
                'timestamp',
                'label' => Yii::t('objectFile', 'Updated'),
            ),
            'created' => array(
                'datetime',
                'label' => Yii::t('objectFile', 'Created'),
            ),
            'massMedia' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'MassMedia',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'massMediaHasFile' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'MassMediaHasFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
            'fair' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Fair',
                    array(
                        'logoId' => 'id',
                    ),
                ),
            ),
            'fairMail' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Fair',
                    array(
                        'mailLogoId' => 'id',
                    ),
                ),
            ),
            'blog' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Blog',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'blogHasFile' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'BlogHasFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
            'fieldHasFile' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'FieldHasFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
            'regionInformationHasFile' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'RegionInformationHasFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplexHasFile' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'ExhibitionComplexHasFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
            'regionInformation' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'RegionInformation',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'fairHasFile' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'FairHasFile',
                    array(
                        'fileId' => 'id',
                    ),
                ),
            ),
            'analytics' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Analytics',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'legalEntity' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'LegalEntity',
                    array(
                        'logoId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplex' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'ExhibitionComplex',
                    array(
                        'imageId' => 'id',
                    ),
                ),
            ),
            'user' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'User',
                    array(
                        'logoId' => 'id',
                    ),
                ),
            ),
        );
    }

//	/**
//	 * Удаление файла перед удалением записи
//	 * @return bool
//	 */
//	public function beforeDelete()
//	{
//		if (parent::beforeDelete()) {
//			$filePath = $this->getFilePath(true);
//			if (file_exists($filePath)) {
//				unlink($filePath);
//			}
//			return true;
//		}
//	}

    /**
     * @param $object
     * @return bool
     * Получить тип объекта
     */
    static function getType($object)
    {
        $search = array_search(get_class($object), self::$types);
        return $search == false ? false : self::$types[$search];
    }

    /**
     * @param $id
     * задаем id приватному свойству
     */
    public function setOwnerId($id)
    {
        $this->_ownerId = $id;
    }

    /**
     * @return int
     * задать id взависимости от типа записи
     */
    public function getOwnerId()
    {
        $ownerId = $this->_ownerId;
        if (!$ownerId) {
            switch ($this->type) {
                case self::TYPE_FAIR_LOGOS;
                    $ownerId = NULL;
                    if(
                        isset($this->fair) &&
                        !empty($this->fair->id)
                    ){
                        $ownerId = $this->fair->id;
                    }
                    break;
                case self::TYPE_FAIR_MAIL_LOGOS;
                    $ownerId = NULL;
                    if(
                        isset($this->fairMail) &&
                        !empty($this->fairMail->id)
                    ){
                        $ownerId = $this->fairMail->id;
                    }
                    break;
                case self::TYPE_USER_LOGO;
                    $ownerId = NULL;
                    if(
                        isset($this->user) &&
                        !empty($this->user->id)
                    ){
                        $ownerId = $this->user->id;
                    }
                    break;
                case self::TYPE_BLOG_MAIN_IMAGE;
                    $ownerId = NULL;
                    if(
                        isset($this->blog) &&
                        !empty($this->blog->id)
                    ){
                        $ownerId = $this->blog->id;
                    }
                    break;
                case self::TYPE_REGION_INFORMATION_MAIN_IMAGE;
                    $ownerId = NULL;
                    if(
                        isset($this->regionInformationHasFile) &&
                        !empty($this->regionInformationHasFile->regionInformationId)
                    ){
                        $ownerId = $this->regionInformationHasFile->regionInformationId;
                    }
                    break;
                case self::TYPE_REGION_INFORMATION_LOGO;
                    $ownerId = NULL;
                    if(
                        isset($this->regionInformationHasFile) &&
                        !empty($this->regionInformationHasFile->regionInformationId)
                    ){
                        $ownerId = $this->regionInformationHasFile->regionInformationId;
                    }
                    break;
                case self::TYPE_EXHIBITION_COMPLEX_MAIN_IMAGE;
                    $ownerId = NULL;
                    if(
                        isset($this->exhibitionComplexHasFile) &&
                        !empty($this->exhibitionComplexHasFile->exhibitionComplexId)
                    ){
                        $ownerId = $this->exhibitionComplexHasFile->exhibitionComplexId;
                    }
                    break;
                case self::TYPE_EXHIBITION_COMPLEX_GALLERY_IMAGES;
                    $ownerId = NULL;
                    if(
                        isset($this->exhibitionComplexHasFile) &&
                        !empty($this->exhibitionComplexHasFile->exhibitionComplexId)
                    ){
                        $ownerId = $this->exhibitionComplexHasFile->exhibitionComplexId;
                    }
                    break;
                case self::TYPE_REGION_INFORMATION_IMAGES;
                    $ownerId = NULL;
                    if(
                        isset($this->regionInformationHasFile) &&
                        !empty($this->regionInformationHasFile->regionInformationId)
                    ){
                        $ownerId = $this->regionInformationHasFile->regionInformationId;
                    }
                    break;
                case self::TYPE_REGION_INFORMATION_FILES;
                    $ownerId = NULL;
                    if(
                        isset($this->regionInformationHasFile) &&
                        !empty($this->regionInformationHasFile->regionInformationId)
                    ){
                        $ownerId = $this->regionInformationHasFile->regionInformationId;
                    }
                    break;
                case self::TYPE_BLOG_IMAGES;
                    $ownerId = NULL;
                    if(
                        isset($this->blogHasFile) &&
                        !empty($this->blogHasFile->blogId)
                    ){
                        $ownerId = $this->blogHasFile->blogId;
                    }
                    break;
                case self::TYPE_BLOG_FILES;
                    $ownerId = NULL;
                    if(
                        isset($this->blogHasFile) &&
                        !empty($this->blogHasFile->blogId)
                    ){
                        $ownerId = $this->blogHasFile->blogId;
                    }
                    break;
                case self::TYPE_EXHIBITION_REQUIREMENTS;
                    $ownerId = NULL;
                    if(
                        isset($this->fairHasFile) &&
                        !empty($this->fairHasFile->fairId)
                    ){
                        $ownerId = $this->fairHasFile->fairId;
                    }
                    break;
                case self::TYPE_PRESS_RELEASE;
                    $ownerId = NULL;
                    if(
                        isset($this->fairHasFile) &&
                        !empty($this->fairHasFile->fairId)
                    ){
                        $ownerId = $this->fairHasFile->fairId;
                    }
                    break;
                case self::TYPE_DISTRIBUTION_EXHIBITION;
                    $ownerId = NULL;
                    if(
                        isset($this->fairHasFile) &&
                        !empty($this->fairHasFile->fairId)
                    ){
                        $ownerId = $this->fairHasFile->fairId;
                    }
                    break;
                case self::TYPE_PHOTOS_FROM_THE_SHOW;
                    $ownerId = NULL;
                    if(
                        isset($this->fairHasFile) &&
                        !empty($this->fairHasFile->fairId)
                    ){
                        $ownerId = $this->fairHasFile->fairId;
                    }
                    break;
                case self::TYPE_MARKETING_MATERIALS;
                    $ownerId = NULL;
                    if(
                        isset($this->exhibitionComplexHasFile) &&
                        !empty($this->exhibitionComplexHasFile->exhibitionComplexId)
                    ){
                        $ownerId = $this->exhibitionComplexHasFile->exhibitionComplexId;
                    }
                    break;
                case self::TYPE_PRINTED_MATERIALS;
                    $ownerId = NULL;
                    if(
                        isset($this->exhibitionComplexHasFile) &&
                        !empty($this->exhibitionComplexHasFile->exhibitionComplexId)
                    ){
                        $ownerId = $this->exhibitionComplexHasFile->exhibitionComplexId;
                    }
                    break;
                case self::TYPE_LIST_OF_TECHNICAL_SERVICES;
                    $ownerId = NULL;
                    if(
                        isset($this->exhibitionComplexHasFile) &&
                        !empty($this->exhibitionComplexHasFile->exhibitionComplexId)
                    ){
                        $ownerId = $this->exhibitionComplexHasFile->exhibitionComplexId;
                    }
                    break;
                case self::TYPE_ANALYTICS_IMAGE;
                    $ownerId = NULL;
                    if(
                        isset($this->analytics) &&
                        !empty($this->analytics->id)
                    ){
                        $ownerId = $this->analytics->id;
                    }
                    break;
                case self::TYPE_LEGAL_ENTITY_LOGO;
                    $ownerId = NULL;
                    if(
                        isset($this->legalEntity) &&
                        !empty($this->legalEntity->id)
                    ){
                        $ownerId = $this->legalEntity->id;
                    }
                    break;
                case self::TYPE_MASS_MEDIA_IMAGES;
                    $ownerId = NULL;
                    if(
                        isset($this->massMediaHasFile) &&
                        !empty($this->massMediaHasFile->massMediaId)
                    ){
                        $ownerId = $this->massMediaHasFile->massMediaId;
                    }
                    break;
                case self::TYPE_MASS_MEDIA_IMAGE ;
                    $ownerId = NULL;
                    if(
                        isset($this->massMedia) &&
                        !empty($this->massMedia->id)
                    ){
                        $ownerId = $this->massMedia->id;
                    }
                    break;
                case self::TYPE_ORGANIZER_INFO_LOGOS ;
                    $ownerId = NULL;
                    if(
                        isset($this->organizerInfoHasFile) &&
                        !empty($this->organizerInfoHasFile->organizerInfoId)
                    ){
                        $ownerId = $this->organizerInfoHasFile->organizerInfoId;
                    }
                    break;
                case self::TYPE_ORGANIZER_CONTACT_AVATARS ;
                    $ownerId = NULL;
                    if(
                        isset($this->organizerContactHasFile) &&
                        !empty($this->organizerContactHasFile->organizerContactId)
                    ){
                        $ownerId = $this->organizerContactHasFile->organizerContactId;
                    }
                    break;
            }
        }
        return $this->_ownerId = $ownerId;
    }

    /**
     * @param bool $abs
     * @return string
     * Узнать путь директории
     */
    public function getDirPath($abs = false)
    {
        $path = '';
        if ($abs) {
            $path = Yii::getPathOfAlias('application.uploads') . DIRECTORY_SEPARATOR;
        }
        return $path . strtr(self::$paths[$this->type], array('{id}' => $this->getOwnerId()));
    }

    /**
     * @param bool $abs
     * @param string $prefix
     * @return string
     * Узнать путь файла в директории
     */
    public function getFilePath($abs = false, $prefix = '')
    {
        return $this->getDirPath($abs) . $prefix. $this->name;
    }

    /**
     * @param bool $abs
     * @return bool|string
     * Узнать полный путь url
     */
    public function getDirUrl($abs = false)
    {
        $path = $this->getDirPath();
        if ($path) {
            $url = '';
            if ($abs) {
                $url = Yii::app()->getBaseUrl(true) . '/';
            }
            return $url . $path;
        }
        return false;
    }

    /**
     * @return null
     * @param string $prefix
     * Узнать полный путь файла
     */
    public function getFileUrl($prefix = '')
    {
        if (file_exists($this->getFilePath(true, DIRECTORY_SEPARATOR))) {
            return '/uploads/'. $this->getDirPath() . '/' . $prefix . $this->name;
        }
        return null;
    }

    public function getAbsoluteUrl()
    {
        $result = null;
        $url = $this->getFileUrl();

        if ($url != null) {
            $result = Yii::app()->createAbsoluteUrl('/') . $url;
        }

        return $result;
    }

    /**
     * @param $dir
     * @return array
     * Получаем массив путей
     */
    static function getFolderPaths($dir)
    {
        $pathArray = array();
        if (is_dir($dir)) {
            $names = glob($dir . "*");
            $i = 0;
            foreach ($names as $name) {
                if (filetype($name) == 'dir') {
                    $pathArray[] = $name;
                    if ($result = self::getFolderPaths($name . DIRECTORY_SEPARATOR)) {
                        $pathArray[] = $result;
                    }
                } else {
                    $pathArray[] = $name;
                }
                $i++;
            }
        }
        return $pathArray;
    }


    /**
     * @return null
     * Если картинка не загружена
     */
    static function noImage()
    {
        $path = Yii::app()->theme->basePath . '/assets/img/empty.gif';
        return file_exists($path) ? Yii::app()->assetManager->publish($path) : null;
    }

    /**
     * @return null
     */
    static function noAvatar()
    {
        $path = Yii::app()->theme->basePath . '/assets/dist/avatar_empty.png';
        return file_exists($path) ? Yii::app()->assetManager->publish($path) : null;
    }

    static function noLogoType()
    {
        $path = Yii::app()->theme->basePath . '/assets' . '/img/Logotype1.png';
        return file_exists($path) ? Yii::app()->assetManager->publish($path) : null;
    }

    /**
     * @return null
     * Если картинка не загружена
     */
    static function noUserImage()
    {
        $path = Yii::app()->theme->basePath . '/assets' . '/img/user/no-avatar.png';
        return file_exists($path) ? Yii::app()->assetManager->publish($path) : null;
    }

    /**
     * Сохранение файла
     * @param CUploadedFile $file
     * @param bool $deleteTempFile
     * @param bool $differentSizes
     * @return bool|int
     */
    public function saveFile($file, $deleteTempFile = true, $differentSizes = false, $differentOptions = [])
    {
        $fileName = md5(microtime()) . '.' . $file->getExtensionName();

        $path = $this->getDirPath(true);
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        if($differentSizes){
            return $this->saveDifferent($fileName, $file, $deleteTempFile, $differentOptions, $path);
        } else {
            if ($this->saveAs($file, $path . DIRECTORY_SEPARATOR . $fileName, $deleteTempFile)) {
                $this->name = $fileName;
                $this->save(false);
                return $this->id;
            } else {
                return false;
            }
        }
    }

    /**
     * @param CUploadedFile $file
     * @param string $destination
     * @param bool $deleteTempFile
     * @return bool
     */
    public function saveAs($file, $destination, $deleteTempFile = true)
    {
        if (is_uploaded_file($file->getTempName())) {
            return $file->saveAs($destination, $deleteTempFile);
        } else {
            if (copy($file->getTempName(), $destination)) {
                if ($deleteTempFile) {
                    unlink($file->getTempName());
                }
                return true;
            }
        }
    }

    /**
     * @param $fileName
     * @param $file
     * @param $deleteTempFile
     * @param $differentOptions
     * @param $path
     * @return bool|int
     * @throws Exception
     */
    public function saveDifferent($fileName, $file, $deleteTempFile, $differentOptions, $path)
    {
        if ($this->saveAs($file, $path . DIRECTORY_SEPARATOR .$fileName, false)) {
            $this->name = $fileName;
            $this->save(false);
        } else {
            return false;
        }

        if($file instanceof CUploadedFile){
            $ihOrigin = new CImageHandler();
            $ihOrigin->load($file->getTempName());

            $originalWidth = $ihOrigin->getWidth();
            $originalHeight = $ihOrigin->getHeight();

            $format = $originalWidth >= $originalHeight;
            $proportional = false;

            if(isset($differentOptions['sizes'])) {
                foreach($differentOptions['sizes'] as $width => $height) {
                    $ih = new CImageHandler();
                    $ih->load($file->getTempName());
                    if($format){
                        if($width >= $height){
                            $proportional = $originalWidth >= $width;
                            $ih->resize($width, false, $proportional);
                            $crop = ($ih->getHeight() - $height) / 2;
                            $ih->crop($width, $height, false, $crop);
                        } else {
                            $proportional = $originalHeight >= $height;
                            $ih->resize(false, $height, $proportional);
                            $crop = ($ih->getWidth() - $width) / 2;
                            $ih->crop($width, $height, $crop, false);
                        }
                    } else {
                        if($width >= $height){
                            $proportional = $originalWidth >= $width;
                            $ih->resize($width, false, $proportional);
                            $crop = ($ih->getHeight() - $height) / 2;
                            $ih->crop($width, $height, false, $crop);
                        } else {
                            $proportional = $originalHeight >= $height;
                            $ih->resize(false, $height, $proportional);
                            $crop = ($ih->getWidth() - $width) / 2;
                            $ih->crop($width, $height, $crop, false);
                        }
                    }

                    if ($this->fileNamePrefix === NULL) {
                        $imageName = $width .'-'. $height . '_' . $fileName;
                    } else {
                        $imageName = $this->fileNamePrefix . '_' . $fileName;
                    }

                    $ih->save($path . DIRECTORY_SEPARATOR .$imageName, $ih::IMG_JPEG);
                }
            }
        }

        return $this->id;
    }

    /**
     * Создание нового файла
     * @param $file
     * @param $ownerObject
     * @param $type
     * @param $ext
     * @param bool $deleteTempFile
     * @param bool $differentSizes
     * @param array $differentOptions
     * @return null|ObjectFile
     */
    static function createFile(
        $file,
        $ownerObject,
        $type,
        $ext,
        $deleteTempFile = true,
        $differentSizes = false,
        $differentOptions = []
    )
    {
        if (!$file instanceof CUploadedFile) {
            return null;
        }
        $objectFile = new self();
        $objectFile->setOwnerId($ownerObject->id);
        $objectFile->type = $type;
        $objectFile->ext = $ext;
        $objectFile->label = $file->getName();

        if (isset($differentOptions['prefix'])) {
            $objectFile->fileNamePrefix = $differentOptions['prefix'];
        }

        if ($objectFile->saveFile($file, $deleteTempFile, $differentSizes, $differentOptions)) {

            $objectCls = get_class($ownerObject);
            $relationCls = $objectCls . 'HasFile';
            /** @var AR $relation */
            $relation = new $relationCls;

            $relation->{self::getRelatedField($objectCls, $relation)} = $ownerObject->id;
            $relation->fileId = $objectFile->id;
            if ($relation->save(false)) {
                return $objectFile;
            }
        }
        return null;
    }

    public function resizeAndSaveImage($file, $owner, $toWidth = false, $toHeight = false) {
        $imageHandler = new CImageHandler();
        $imageHandler->load($file->getTempName());

        $imageHandler->resize($toWidth, $toHeight, true);

        return ObjectFile::createFile(
            $file,
            $owner,
            $this->type,
            $this->ext,
            true,
            true,
            [
                'sizes' => [
                    $imageHandler->width => $imageHandler->height
                ],
                'prefix' => $this->fileNamePrefix,
            ]
        );
    }

    public function setFileNamePrefix($prefix) {
        $this->fileNamePrefix = $prefix;
    }

    public function GetFileNamePrefix() {
        return $this->fileNamePrefix;
    }

    /**
     * Удаляем связь с файлом и сам файл, если других связей нет
     * @param AR $ownerObject
     * @return bool
     * @throws CDbException
     */
    public function deleteFile($ownerObject)
    {
        $objectCls = get_class($ownerObject);
        $relationCls = $objectCls . 'HasFile';

        /** @var AR[] $relations */
        $relations = AR::model($relationCls)->findAllByAttributes(array(
            'fileId' => $this->id
        ));

        foreach ($relations as $i => $relation) {
            if ($relation->{self::getRelatedField($objectCls, $relation)} == $ownerObject->id) {
                if ($relation->delete()) {
                    unset($relations[$i]);
                }
            }
        }
        if (count($relations) == 0) {
            $this->setOwnerId($ownerObject->id);
            return $this->delete();
        }
        return false;
    }

    static function getRelatedField($objectCls, $relation)
    {
        $relatedField = lcfirst($objectCls) . 'Id';
        foreach ($relation->description() as $field => $params) {
            if (isset($params['relation'][1]) && $params['relation'][1] == $objectCls) {
                $relatedField = key($params['relation'][2]);
            }
        }
        return $relatedField;
    }

    /**
     * @param AR $ownerObject
     * @param $type
     * @param $ownerRelField
     * @return mixed|string
     */
    static function getFileList($ownerObject, $type, $ownerRelField = false)
    {
        $objectCls = lcfirst(get_class($ownerObject));
        $objectFile = self::model();
        $connection = $objectFile->getDbConnection();
        if (!$ownerRelField) {
            $relatedField = $objectCls . 'Id';
            $relationCls = $objectCls . 'HasFile';
            $cond = $connection->quoteColumnName($relationCls . '.' . $relatedField) . ' = ' . $connection->quoteValue($ownerObject->id);
        } else {
            $relatedField = 'id';

            if($type == self::TYPE_FAIR_MAIL_LOGOS){
                $relationCls = 'fairMail';
            } else {
                $relationCls = $objectCls;
            }

            $cond = $connection->quoteColumnName($relationCls . '.' . $relatedField) . ' = ' . $connection->quoteValue($ownerObject->id);
        }
        $fieldFiles = [];
        foreach ($objectFile->with($relationCls)->findAllByAttributes(
            array('type' => $type), $cond
        ) as $k => $object) {

            $fieldFiles[$k] = [
                'id' => $object->id,
                'filename' => $object->label,
            ];
            if ($object->ext == self::EXT_IMAGE) {
                $fieldFiles[$k]['assetPath'] = $object->getFileUrl(DIRECTORY_SEPARATOR);
            }
        }
        return $fieldFiles;
    }

    public function saveProposal($file, $elId) {
        $fileName = $elId . '.html';
        $path = Yii::getPathOfAlias('application.uploads') . '/proposals/';

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        return file_put_contents($path . $fileName, $file);
    }
}