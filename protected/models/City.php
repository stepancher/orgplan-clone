<?php

/**
 * Class City
 */
class City extends \AR
{
    public $fairsCount;
    public $exhibitionComplexCount;
    
    public static $_description = NULL;

    /**
     * @return mixed
     */
    public function loadName()
    {
        return $this->loadTr('TrCity', 'name');
    }

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        $translate = $this->translate;
        if (isset($translate->name)) {
            $name = $translate->name;
        }

        return $name;
    }

    public function description()
    {
        if(!empty(self::$_description) && is_array(self::$_description)){
            return self::$_description;
        }

        return array(
            'id' => array(
                'label' => '#',
                'pk',
                'rules' => array(
                    'search' => array(
                        array('safe')
                    )
                ),
            ),
            'regionId' => array(
                'label' => Yii::t('city', 'Region'),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'Region',
                    array(
                        'regionId' => 'id',
                    ),
                ),
            ),
            'stands' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'auctionTechnicalDataHasCities' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'AuctionTechnicalDataHasCity',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
            'legalEntities' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'LegalEntity',
                    array(
                        'legalCityId' => 'id',
                    ),
                ),
            ),
            'auctionTechnicalData' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'users' => array(
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
                'relation' => array(
                    'CHasManyRelation',
                    'User',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
            'shortUrl' => array(
                'string',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'region_name' => array(
                'label' => 'Регион',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'trCities' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'TrCity',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'translate' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrCity',
                    array(
                        'trParentId' => 'id',
                    ),
                    'on' => 'translate.langId = "'.Yii::app()->language.'"',
                ),
            ),
            'translateRaw' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'TrCity',
                    array(
                        'trParentId' => 'id',
                    ),
                ),
            ),
            'exhibitionComplices' => array(
                'relation' => array(
                    'CHasManyRelation',
                    'ExhibitionComplex',
                    array(
                        'cityId' => 'id',
                    ),
                ),
            ),
        );

    }

    static function linkForRegionAndCity($exhibitionComplex)
    {
        /**
         * @var ExhibitionComplex $exhibitionComplex
         */

        $link = '';
        if ($exhibitionComplex->region && $exhibitionComplex->city) {
            if ($exhibitionComplex->region->regionInformation) {
                $link .= CHtml::link($exhibitionComplex->city->loadName() . ' / ' . $exhibitionComplex->region->loadName(),
                    Yii::app()->createUrl('regionInformation/default/view', array('id' => $exhibitionComplex->region->regionInformation->id)));
            } else {
                $link = $exhibitionComplex->city->loadName() . ' / ' . $exhibitionComplex->region->loadName();
            }

        } elseif ($exhibitionComplex->region) {
            if ($exhibitionComplex->region->regionInformation) {
                $link .= CHtml::link($exhibitionComplex->region->loadName(),
                    Yii::app()->createUrl('regionInformation/default/view', array('id' => $exhibitionComplex->region->regionInformation->id)));
            } else {
                $link = $exhibitionComplex->region->loadName();
			}
        }

        return $link;
    }

    /**
     * Получение региона по идентификатору города
     * @param $cityId
     * @return string
     */
    public static function getRegionName($cityId)
    {
        $model = City::model()->findByPk($cityId);
        return $model->region->name;
    }

    public function getSeoName()
    {
        return strtolower($this->shortUrl);
    }

    public function behaviors()
    {
        return array(
            'TranslateBehavior' => array(
                'class' => 'application.components.TranslateBehavior'
            )
        );
    }

    public function search($criteria = null, $ignoreCompareFields = array())
    {
        $dCriteria = new CDbCriteria;
        $dCriteria->compare('region.name', $this->{'region_name'}, true);

        if (null !== $criteria) {
            $dCriteria->mergeWith($criteria);
        }
        return parent::search($dCriteria, ['region.name']);
    }

    /**
     * Получение данных страны (значение аттрибута)
     * @param $cityId
     * @param string $attribute
     * @return mixed|string
     */
    public static function getCountry($cityId, $attribute = 'name')
    {
        $result = '';
        $model = static::model()->findByPk($cityId);
        if (null != $model &&
            isset($model->region, $model->region->district, $model->region->district->country) &&
            $model->region->district->country->hasAttribute($attribute)) {
            $result = $model->region->district->country->$attribute;
        }
        return $result;
    }

    /**
     * @param $q
     * @return array
     */
    public static function getCityName($q) {

        $criteria = new CDbCriteria;
        $criteria->with['translate'] = [
            'together' => TRUE,
        ];
        $criteria->compare('translate.name', $q, true);

        $cities = CHtml::listData(
            City::model()->findAll($criteria),
            'id',
            function($el){
                return $el;
            });

        $items = array_map(
            function($city_id, $city){
                return array(
                    'id' => $city_id,
                    'name' => $city->name,
                );
            },
            array_keys($cities),
            array_values($cities)
        );

        return $items;
    }
    
}