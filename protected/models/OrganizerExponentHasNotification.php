<?php
/**
 * Class OrganizerExponentHasNotification
 */
Yii::import('application.modules.notification.models.Notification');
class OrganizerExponentHasNotification extends \AR
{
    public static $_description = NULL;

    public function description()     {
        if (!empty(self::$_description) && is_array(self::$_description)) {
            return self::$_description;
        }

        return self::$_description = array(
            'id' => array(
                'pk',
                'label' => '#',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'organizerExponentId' => array(
                'label' => 'organizerExponent',
                'integer',
                'relation' => array(
                    'CBelongsToRelation',
                    'OrganizerExponent',
                    array(
                        'organizerExponentId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
            'notificationId' => array(
                'integer',
                'label' => 'Уведомление',
                'relation' => array(
                    'CBelongsToRelation',
                    'Notification',
                    array(
                        'notificationId' => 'id',
                    ),
                ),
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
        );
    }
}