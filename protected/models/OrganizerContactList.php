<?php

/**
 * Class OrganizerContactList
 * @property    integer organizerId
 * @property    string  value
 * @property    integer type
 * type might be:
 *  1 - phone
 *  2 - email
 *  3 - exhibitionManagement
 *  4 - servicesBuilding
 *  5 - advertisingServicesTheFair
 *  6 - servicesLoadingAndUnloading
 *  7 - foodServices
 * @property 	string	additionalContactInformation
 */

class OrganizerContactList extends \AR
{
    const PHONE_TYPE = 1;
    const EMAIL_TYPE = 2;

	public static $_description = NULL;

	public static $requiredTypes = array(
	    'phone',
        'email',
    );

	public static $types = array(
	    1 => 'phone',
        2 => 'email',
        3 => 'exhibitionManagement',
        4 => 'servicesBuilding',
        5 => 'advertisingServicesTheFair',
        6 => 'servicesLoadingAndUnloading',
        7 => 'foodServices',
    );

	/**
	 * @param $index
	 * @return string
	 */
	public static function getType($index){
		if(isset(self::$types[$index])){
			return self::$types[$index];
		}
		return '';
	}

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search' => array(
						array('safe')
					)
				),
			),
			'organizerId' => array(
				'integer',
				'label' => 'organizerId',
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					)
				),
			),
			'value' => array(
				'string',
				'label' => 'value',
				'rules' => array(
					'search,insert,update' => array(
						array('safe'),
					)
				),
			),
            'type' => array(
                'integer',
                'label' => 'type',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'additionalContactInformation' => array(
                'string',
                'label' => 'additionalContactInformation',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe'),
                    )
                ),
            ),
            'organizer' => array(
                'relation' => array(
                    'CHasOneRelation',
                    'Organizer',
                    array(
                        'id' => 'organizerId',
                    ),
                ),
            ),
		);
	}
}