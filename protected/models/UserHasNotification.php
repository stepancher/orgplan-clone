<?php
/**
 * Class UserHasNotification
 */
class UserHasNotification extends \AR
{
	public static $_description = NULL;

	public function description()     {
		if (!empty(self::$_description) && is_array(self::$_description)) {
			return self::$_description;
		}

		return self::$_description = array(
			'id' => array(
				'label' => '#',
				'pk',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'userId' => array(
				'label' => 'Пользователь',
				'integer',
				'relation' => array(
					'CBelongsToRelation',
					'User',
					array(
						'userId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'notificationId' => array(
				'integer',
				'label' => 'Аукцион',
				'relation' => array(
					'CBelongsToRelation',
					'Notification',
					array(
						'notificationId' => 'id',
					),
				),
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'industryTypeId' => array(
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
		);
	}
}