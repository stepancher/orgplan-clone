<?php
/**
 * Class CurrencyCourse
 *
 * @property    integer $id
 * @property    integer $source
 * @property    integer $target
 * @property    float   $value
 */
class CurrencyCourse extends \AR
{
	public function description()     {

		return array(
			'id' => array(
				'pk',
				'label' => '#',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'source' => array(
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'target' => array(
				'integer',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
			'value' => array(
				'float',
				'rules' => array(
					'search,insert,update' => array(
						array('safe')
					)
				),
			),
            'datetime' => array(
                'datetime',
                'rules' => array(
                    'search,insert,update' => array(
                        array('safe')
                    )
                ),
            ),
		);
	}
}