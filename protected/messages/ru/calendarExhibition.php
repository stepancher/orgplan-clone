<?php 
return array(
  'Event type demounting' => 'Дни демонтажа',
  'Event type mounting' => 'Дни монтажа',
  'Event type other' => 'Другие дни',
  'Event type working' => 'Дни работы выставки',
  'Expiration date' => 'Дата окончания',
  'Milestone' => 'Веха',
  'Task' => 'Задача',
  'after' => 'после',
  'per' => 'за',
);