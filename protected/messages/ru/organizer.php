<?php 
return array(
  'Catering services' => 'Услуги питания',
  'Directorate phone' => 'Дирекция телефон',
  'Exhibition Management' => 'Дирекция выставки',
  'Link to the website of the organizers' => 'Ссылка на сайт организаторов',
  'Management of e-mail' => 'Дирекция электронная почта',
  'Organizer' => 'Организатор выставки',
  'Services building' => 'Услуги по застройке',
  'Services for loading and unloading' => 'Услуги по разгрузке/погрузке',
  'Services in advertising on exhibition' => 'Контакты по рекламе',
  'active' => '',
    'organizer company id' => 'id Компании',
    'City' => 'Город',
    'Region' => 'Регион',
    'District' => 'Округ',
    'Country' => 'Страна',
    'Street' => 'Улица',
    'Coordinates' => 'Координаты',
    'Expodatabase organizer id' => 'id организатора в expodatabase',
    'Expodatabase organizer contacts' => 'Контакты организатора в expodatabase',
    'Expodatabase organizer contacts raw' => 'Необработанные контакты организатора в expodatabase',
    'Organizer association' => 'Ассоциация',
    'Fairs count' => 'Количество выставок',
    'Status' => 'Статус',
);