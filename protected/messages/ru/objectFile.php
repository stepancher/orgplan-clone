<?php 
return array(
  'Created' => 'Создан',
  'File Type' => 'Тип файла',
  'File name' => 'Имя файла',
  'Invalid file type' => 'Недопустимый тип файла',
  'Limit of files is exceeded' => 'Лимит файлов превышен',
  'Name' => 'Название',
  'This file is already available' => 'Такой файл уже имеется',
  'This image is already available' => '',
  'Type of connection' => 'Тип cвязи',
  'Updated' => 'Обновлен',
);