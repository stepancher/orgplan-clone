<?php 
return array(
  'Chart image' => 'Изображение для диаграммы',
  'Exclusive Price' => 'Эксклюзивная цена',
  'Exclusive percent' => 'Эксклюзивный процент',
  'List image' => 'Изображение для списка',
  'Percent' => 'Процент',
  'Standard price' => 'Стандартная цена',
);