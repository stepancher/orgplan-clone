<?php 
return array(
  'Chart' => 'Диаграмма',
  'General information' => 'Общие сведения',
  'Link region' => 'Ссылка региона',
  'Link to this image' => 'Ссылка на изображение',
  'Name' => 'Наименнование',
  'Region' => 'Регион',
  'The investment rating of the region' => 'Инвестиционный рейтинг региона',
  'Web Address' => 'Адрес в интернете',
);