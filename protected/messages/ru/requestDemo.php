<?php 
return array(
    'Demo requesting' => 'Запросить DEMO',
    'First name' => 'Имя',
    'Last name' => 'Фамилия',
    'E-mail' => 'E-mail',
    'Company' => 'Компания',
    'Send' => 'Отправить',
    'Requesting text' => 'За 20 минут мы проведем видео-тур по Protoplan. Покажем, как работает ЛК Организатора и ЛК Экспонента, ответим на вопросы. Предоставим DEMO-доступ в ЛК Организатора на 14 дней.',
    'Requesting successful' => 'Ваш запрос на DEMO успешно отправлен. В течение 24ч с Вами свяжется проектный менеджер Protoplan. Письмо придет на указанный Вами Email.',
    'All fields are required for write.' => 'Все поля обязательны для заполнения',
    'Back' => 'ОК',
);