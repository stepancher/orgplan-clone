<?php 
return array(
  ' Dismantling' => ' Демонтаж выставки',
  ' During the exhibition' => ' Дни работы выставки',
  ' Installation of an exhibition' => ' Монтаж выставки',
  ' Task:' => ' Задача',
  'My exhibitions' => 'Мои выставки',
  'Type' => ' Тип',
  'User' => ' Пользователь',
);