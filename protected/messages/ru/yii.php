<?php 
return array(
  'Alias "{alias}" is invalid Make sure it points to an existing PHP file and the file is readable' => 'Псевдоним "{alias}" неверен. Убедитесь, что он указывает на существующий PHP файл.',
  'Alias "{alias}" is invalid Make sure it points to an existing directory or file' => 'Неправильный алиас "{alias}". Убедитесь, что он указывает на существующую директорию или файл.',
  'CDbCommand failed to execute the SQL statement: {error}' => 'CDbCommand не удалось исполнить SQL-запрос: {error}',
  'CDbCommand failed to prepare the SQL statement: {error}' => 'CDbCommand не удалось подготовить SQL-запрос: {error}',
  'CDbCommand::execute() failed: {error} The SQL statement executed was: {sql}' => 'Не удалось выполнить CDbCommand::execute(): {error}. Выполнявшийся SQL-запрос: {sql}.',
  'CDbCommand::{method}() failed: {error} The SQL statement executed was: {sql}' => 'Не удалось выполнить CDbCommand::{method}(): {error}. Выполнявшийся SQL-запрос: {sql}.',
  'Class name "{class}" does not match class file "{file}"' => 'Класс "{class}" не соответствует имени файла "{file}".',
  'Object configuration must be an array containing a "class" element' => 'Конфигурация объекта должна быть представлена массивом, содержащим элемент "class".',
  'Powered by {yii}' => 'Создано на {yii}.',
  'Unable to resolve the request "{route}"' => 'Невозможно обработать запрос "{route}".',
  'Unknown operator "{operator}"' => 'Неизвестный оператор "{operator}".',
  'Yii application can only be created once' => 'Приложение Yii может быть создано только один раз.',
);