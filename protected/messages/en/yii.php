<?php 
return array(
  'Alias "{alias}" is invalid. Make sure it points to an existing PHP file and the file is readable.' => '',
  'Alias "{alias}" is invalid. Make sure it points to an existing directory or file.' => '',
  'CDbCommand failed to execute the SQL statement: {error}' => '',
  'CDbCommand failed to prepare the SQL statement: {error}' => '',
  'CDbCommand::execute() failed: {error}. The SQL statement executed was: {sql}.' => '',
  'CDbCommand::{method}() failed: {error}. The SQL statement executed was: {sql}.' => '',
  'Class name "{class}" does not match class file "{file}".' => '',
  'Object configuration must be an array containing a "class" element.' => '',
  'Powered by {yii}.' => '',
  'Unable to resolve the request "{route}".' => '',
  'Unknown operator "{operator}".' => '',
  'Yii application can only be created once.' => '',
);