<?php 
return array(
  'Chart' => 'Chart',
  'General information' => 'General information',
  'Link region' => 'Link to a region',
  'Link to this image' => 'Link to an image',
  'Name' => 'Name',
  'Region' => 'Region',
  'The investment rating of the region' => 'Region\'s investment ranking',
  'Web Address' => 'Web address',
);