<?php 
return array(
  'Active' => 'Active',
  'Area' => 'Space',
  'Category' => 'Category',
  'Closed stands' => 'Closed stands',
  'Corner' => 'Corner',
  'Cost' => 'Price',
  'Description' => 'Description',
  'Electricity' => 'Electricity',
  'Equipment for rent' => 'Equipment for rent',
  'Exhibition' => 'Exhibition',
  'Extras service' => 'Additional services',
  'Furniture' => 'Furniture',
  'Hash project Planner5D' => 'Planner5d hash',
  'Is ideal' => 'Perfect stand',
  'Island' => 'Island',
  'Lighting' => 'Lighting',
  'Linear' => 'In-line',
  'Location' => 'Location',
  'Mobile stands for rent' => 'Portable stands for rent',
  'Multimedia' => 'Multimedia',
  'No plan' => 'No layout',
  'Open stands' => 'Open stands',
  'Peninsula' => 'Peninsula',
  'Portfolio' => 'Portfolio',
  'Production of mobile stands' => 'Construction of portable stands',
  'Stand' => 'Stand',
  'Tents for open areas' => 'Tents for outdoor events',
  'Two-storey stands' => 'Double-deck stands',
  'Type of plan' => 'Layout',
  'Upload stand image' => 'Upload stand image',
  'exclusive' => 'Exclusive',
  'standard' => 'Standard',
);