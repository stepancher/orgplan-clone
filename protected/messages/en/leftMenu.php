<?php 
return array(
    'About fair' => 'About the exhibition',
    'Calendar' => 'Calendar',
    'My documents' => 'My documents',
    'My fairs' => 'My exhibitions',
    'Proposals' => 'Application forms',
    'Reports' => 'Reports',
    'Tech tasks' => 'Terms of reference',
    'Tenders' => 'Tenders',
    'cis fairs' => 'CIS Exhibitions',
    'cis industries' => 'CIS Sectors',
    'rus districts' => 'RF Districts',
    'rus fairs' => 'RF Exhibitions',
    'rus industries' => 'RF Sectors',
    'rus regions' => 'RF Regions',
    'Catalog' => "Catalogue",
    'Bookmarks' => "Bookmarks",
    'Blog' => "Blog",
    'Authorization' => 'Authorization',
    'Authorization needed' => 'Authorization needed',
    'Enter' => 'Log in',
    'Cancel' => 'Cancel',
    'Orgplan' => 'Orgplan',
    'support' => "Support",
    'profile' => "Profile",
    'logout' => "Exit",
    'Photoreport' => 'Photo report',
    'all exponents' => 'Exhibitors',
    'My checklists' => 'ExpoPlanner',
);