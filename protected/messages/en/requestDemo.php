<?php 
return array(
    'Demo requesting' => 'Request a Demo',
    'First name' => 'First Name',
    'Last name' => 'Last Name',
    'E-mail' => 'Email Address',
    'Company' => 'Company',
    'Send' => 'Submit',
    'Requesting text' => 'Join us for 20-minute guided tour of Protoplan. We\'ll show you how Personal account for Organizer and Personal Account for Exhibitor work and answer all of your questions. We’ll provide you 14 days DEMO Account for Organizer.',
    'Requesting successful' => 'Your request for a DEMO is successfully submitted. Within 24 hours Protoplan project manager will contact you. You\'ll get Email on the editted address.',
    'All fields are required for write.' => 'All fields are required.',
    'Back' => 'OK',
);