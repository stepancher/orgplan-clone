<?php 
return array(
  'Created' => 'Created ',
  'File Type' => 'File type',
  'File name' => 'File name',
  'Invalid file type' => 'Invalid file type',
  'Limit of files is exceeded' => 'You have exceeded file limit',
  'Name' => 'Name',
  'This file is already available' => 'This file already exists',
  'This image is already available' => '',
  'Type of connection' => '',
  'Updated' => 'Updated',
);