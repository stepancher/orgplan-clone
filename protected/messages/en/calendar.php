<?php 
return array(
  ' Dismantling' => 'Exhibition dismantling',
  ' During the exhibition' => 'Event dates',
  ' Installation of an exhibition' => 'Exhibition installation',
  ' Task:' => 'Task',
  'My exhibitions' => 'My events',
  'Type' => 'Type',
  'User' => 'User',
);