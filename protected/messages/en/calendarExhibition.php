<?php 
return array(
  'Event type demounting' => 'Dismantling days',
  'Event type mounting' => 'Installation days',
  'Event type other' => 'Other days',
  'Event type working' => 'Event dates',
  'Expiration date' => 'Event end date',
  'Milestone' => 'Milestone',
  'Task' => 'Task',
  'after' => 'After',
  'per' => 'Before',
);