<?php 
return array(
  'Alias "{alias}" is invalid Make sure it points to an existing PHP file and the file is readable' => 'Alias "{alias}" ist ungültig. Stellen Sie sicher, dass er auf eine existierende PHP-Datei verweist und die Datei lesbar ist.',
  'Alias "{alias}" is invalid Make sure it points to an existing directory or file' => 'Der Alias "{alias}" ist ungültig. Stellen Sie sicher, dass er auf ein existierendes Verzeichnis oder eine existierende Datei verweist.',
  'CDbCommand failed to execute the SQL statement: {error}' => 'CDbCommand konnte das SQL-Statement nicht ausführen: {error}',
  'CDbCommand failed to prepare the SQL statement: {error}' => 'CDbCommand konnte das SQL-Statement nicht vorbereiten: {error}',
  'CDbCommand::execute() failed: {error} The SQL statement executed was: {sql}' => 'CDbCommand::execute() fehlgeschlagen: {error}. Der SQL-Ausdruck war: {sql}.',
  'CDbCommand::{method}() failed: {error} The SQL statement executed was: {sql}' => 'CDbCommand::{method} fehlgeschlagen: {error}. Der SQL-Ausdruck war: {sql}.',
  'Class name "{class}" does not match class file "{file}"' => 'Der Klassenname "{class}" passt nicht zum Dateinamen "{file}".',
  'Object configuration must be an array containing a "class" element' => 'Objekt-Konfiguration muss ein Array sein, das ein "class"-Element beinhaltet.',
  'Powered by {yii}' => 'Powered by {yii}.',
  'Unable to resolve the request "{route}"' => 'Konnte den Request "{route}" nicht auflösen.',
  'Unknown operator "{operator}"' => 'Unbekannter Operator "{operator}".',
  'Yii application can only be created once' => 'Eine Yii Applikation kann nur einmal erzeugt werden.',
);