<?php 
return array(
    'About fair' => 'Über die Messe',
    'Calendar' => 'Kalender',
    'My documents' => 'Eigene Dateien',
    'My fairs' => 'Meine Messen',
    'Proposals' => 'Anträge',
    'Reports' => 'Berichte',
    'Tech tasks' => 'Lastenhefte',
    'Tenders' => 'Ausschreibungen',
    'cis fairs' => 'Messen in der GUS',
    'cis industries' => 'Branchen in der GUS',
    'rus districts' => 'Föderationskreise der RF',
    'rus fairs' => 'Messen in Russland',
    'rus industries' => 'Branchen in Russland',
    'rus regions' => 'Regionen in Russland',
    'Catalog' => "Katalog",
    'Bookmarks' => "Favoriten",
    'Blog' => "Blog",
    'Authorization' => 'Autorisierung',
    'Authorization needed' => 'Autorisierung erforderlich',
    'Enter' => 'Anmelden',
    'Cancel' => 'Abbrechen',
    'Orgplan' => 'Organisationsplan',
    'support' => "Support",
    'profile' => "Profil",
    'logout' => "Abmelden",
    'Photoreport' => 'Photo-Bericht',
    'all exponents' => 'Aussteller',
    'My checklists' => 'ExpoPlanner',
);