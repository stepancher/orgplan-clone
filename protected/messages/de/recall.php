<?php 
return array(
  'Activation' => 'aktiv',
  'Author' => 'Verfasser',
  'Bid' => 'Bid',
  'Blog' => 'Blog',
  'Comment' => 'Bewertung hinterlassen',
  'Date of creation' => 'aktiv',
  'Exhibition' => 'aktiv',
  'Object' => 'Objekt',
  'Rating' => 'Ranking',
  'Type' => 'Typ',
  'User' => 'Auftragnehmer',
  'off' => 'nicht aktiv',
  'on' => 'aktiv',
);