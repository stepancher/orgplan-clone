<?php 
return array(
  'Active' => 'aktiv',
  'Area' => 'Fläche',
  'Category' => 'Kategorie',
  'Closed stands' => 'Geschlossene Stände',
  'Corner' => 'Eckstand',
  'Cost' => 'Kosten',
  'Description' => 'Beschreibung',
  'Electricity' => 'Elektrizität',
  'Equipment for rent' => 'Mietbare Ausrüstung',
  'Exhibition' => 'Messe',
  'Extras service' => 'Zusatzbedienung',
  'Furniture' => 'Möbel',
  'Hash project Planner5D' => 'KEY_NOT_FOUND',
  'Is ideal' => 'Idealer Stand',
  'Island' => 'Insel',
  'Lighting' => 'KEY_NOT_FOUND',
  'Linear' => 'Linear-Stand',
  'Location' => 'Lage',
  'Mobile stands for rent' => 'Mobilstände zur Vermietung',
  'Multimedia' => 'Multimedia',
  'No plan' => 'Keine Raumeinteilung',
  'Open stands' => 'Offene Stände',
  'Peninsula' => 'Halbinsel',
  'Portfolio' => 'Portfolio',
  'Production of mobile stands' => 'Anfertigung der Mobilstände',
  'Stand' => 'Stand',
  'Tents for open areas' => 'Zelte für offene Flächen',
  'Two-storey stands' => 'ZweistöckigeStände',
  'Type of plan' => 'Typ der Raumeinteilung',
  'Upload stand image' => 'Abbildung des Standes hochladen',
  'exclusive' => 'exclusiv',
  'standard' => 'standard',
);