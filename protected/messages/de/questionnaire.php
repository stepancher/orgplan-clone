<?php 
return array(
  'Color' => 'Farbe',
  'Data' => 'Angaben',
  'Logo' => 'Logo',
  'Profile exhibition visitor' => 'Fragebogen eines Messebesuchers',
  'Templates' => 'Schablonen',
  'User' => 'Benutzer',
);