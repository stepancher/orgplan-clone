<?php 
return array(
  'Company' => 'Unternehmen',
  'Mobile phone' => 'Mobiltelefon',
  'Post' => 'Position',
  'This email' => 'E-Mail-Adresse',
  'Work Phone' => 'Telefon (geschäftlich)',
  'email' => 'E-Mail-Adresse',
    'This email already exists.' => 'Diese E-Mail-Adresse wird bereits verwendet',
);