<?php 
return array(
  'Created' => 'erstellt',
  'File Type' => 'Dateityp',
  'File name' => 'Dateiname',
  'Invalid file type' => 'Unerlaubter Dateityp',
  'Limit of files is exceeded' => 'Dateihöchstgrenze ist überschritten',
  'Name' => 'Bezeichnung',
  'This file is already available' => 'Die Datei ist bereits vorhanden',
  'This image is already available' => '',
  'Type of connection' => 'Verbindungstyp',
  'Updated' => 'aktualisiert',
);