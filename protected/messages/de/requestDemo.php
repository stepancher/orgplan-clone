<?php 
return array(
    'Demo requesting' => 'LIVE-DEMO VEREINBAREN',
    'First name' => 'Vorname',
    'Last name' => 'Nachname',
    'E-mail' => 'E-Mail-Adresse',
    'Company' => 'Firma',
    'Send' => 'Absenden',
    'Requesting text' => 'Innerhalb von 20 Minuten führen wir eine Video-Tour durch PROTOPLAN durch. Wir zeigen Ihnen wie die persönlichen Bereiche eines Veranstalters und eines Ausstellers funktionieren und beantworten Ihre Fragen. Wir stellen Ihnen eine kostenlose Demoversion des persönlichen Bereichs für Veranstalter zur Verfügung. Diese können Sie 14 Tage lang testen.',
    'Requesting successful' => 'Vielen Dank! <br/>Ihre Anfrage wurde erfolgreich versendet.  Ein Projekt-Manager von Protoplan wird  sich mit Ihnen innerhalb von 24 Stunden  in Verbindung setzten. Sie erhalten eine Nachricht an die von Ihnen angegebene E-Mail-Adresse.',
    'All fields are required for write.' => 'Pflichtfelder',
    'Back' => 'OK',
);