<?php 
return array(
  'Chart image' => 'KEY_NOT_FOUND',
  'Exclusive Price' => 'Sonderpreis',
  'Exclusive percent' => 'Sonderprozent',
  'List image' => 'Listendarstellung',
  'Percent' => 'Prozent',
  'Standard price' => 'Standardpreis',
);