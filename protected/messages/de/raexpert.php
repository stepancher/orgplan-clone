<?php 
return array(
  'Chart' => 'Diagramm',
  'General information' => 'allgemeine Angaben',
  'Link region' => 'Link der Region',
  'Link to this image' => 'Bildlink',
  'Name' => 'Bezeichnung',
  'Region' => 'Region',
  'The investment rating of the region' => 'Investitionsranking der Region',
  'Web Address' => 'Adresse im Internet',
);