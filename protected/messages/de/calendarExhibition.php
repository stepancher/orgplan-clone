<?php 
return array(
  'Event type demounting' => 'Tage der Demontage',
  'Event type mounting' => 'Tage der Montage',
  'Event type other' => 'Andere Tage',
  'Event type working' => 'Öffnungstage der Messe',
  'Expiration date' => 'Beendigungsdatum',
  'Milestone' => 'Schritt',
  'Task' => 'Aufgabe',
  'after' => 'nach',
  'per' => 'für',
);