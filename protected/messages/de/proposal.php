<?php 
return array(
  'Loading more results...' => '',
  'No matches found' => '',
  'Please enter {chars} less characters' => '',
  'Please enter {chars} more characters' => '',
  'Searching...' => '',
  'You can only select {count} items' => '',
);