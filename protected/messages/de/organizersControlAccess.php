<?php 
return array(
  'Allow access' => 'Zugang erlauben',
  'Deny access' => 'Zugang sperren',
  'Exhibition' => 'Messe',
  'Organizer' => 'Veranstalter',
);