<?php 
return array(
  'Catering services' => 'Dienstleistungen der Ernährung',
  'Directorate phone' => 'Direktion Telefon',
  'Exhibition Management' => 'Messedirektion',
  'Link to the website of the organizers' => 'Link auf die Webseite der Veranstalter',
  'Management of e-mail' => 'Direktion E-Mail-Adresse',
  'Organizer' => 'Messeveranstalter',
  'Services building' => 'Aufbaudienstleistungen',
  'Services for loading and unloading' => 'Dienstleistungen von Verladung/Ausladung',
  'Services in advertising on exhibition' => 'Dienstleistungen der Werbung',
  'active' => '',
    'organizer company id' => 'id Компании',
    'City' => 'Город',
    'Region' => 'Регион',
    'District' => 'Округ',
    'Country' => 'Страна',
    'Street' => 'Улица',
    'Coordinates' => 'Координаты',
    'Expodatabase organizer id' => 'id организатора в expodatabase',
    'Expodatabase organizer contacts' => 'Контакты организатора в expodatabase',
    'Expodatabase organizer contacts raw' => 'Необработанные контакты организатора в expodatabase',
    'Organizer association' => 'Ассоциация',
    'Fairs count' => 'Количество выставок',
    'Status' => 'Статус',
);